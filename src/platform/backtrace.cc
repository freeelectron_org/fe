/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

//* TODO Windows
//* https://stackoverflow.com/questions/6205981/windows-c-stack-trace-from-a-running-app

#include <platform/platform.pmh>

#define FE_BACKTRACE_GDB	(FE_CODEGEN<=FE_DEBUG)
#define FE_BATCH_GDB		FALSE

#if FE_COMPILER==FE_GNU || FE_COMPILER==FE_INTEL
#include <cxxabi.h>
#if FE_OS!=FE_OSX
#include <execinfo.h>
#endif
#define GNU_SOURCE
#include <dlfcn.h>

fe::String feGenerateBacktrace(U32 skip)
{
	fe::String text;

#if FE_OS!=FE_OSX
	const U32 backtrace_size = 256;
	const U32 buffer_length=1024;
	const I32 skip_bottom=2;	// before main()

	void* stack[backtrace_size];
	const I32 count=backtrace(stack,backtrace_size);
	char** symbols=backtrace_symbols(stack,count);

	text.catf("[35mBACKTRACE[0m\n");

//	backtrace_symbols_fd(stack, count, 2);

	Dl_info info;
	for(I32 m=skip;m<count-skip_bottom;m++)
	{
		if(dladdr(stack[m],&info) != 0)
		{
			char sym_name[buffer_length];
			sym_name[0]=0;

			char* start=strchr(symbols[m],'(');
			if(start)
			{
				I32 symlen=strchr(start,')')-start;
				if(symlen>0)
				{
					strncpy(sym_name,start+1,symlen-1);
					sym_name[symlen-1]=0;
				}
			}

			const fe::String demangled=
					fe::System::demangle(info.dli_sname,TRUE);
/*
			text.catf("%s\n%s\n%s\n%s\n\n",
					demangled.c_str(),
					sym_name,
					info.dli_fname,
					symbols[m]);
*/
			text.catf("%d: [37m[1m%s\n",
					m-skip,!demangled.empty()? demangled.c_str(): sym_name);
			text.catf("[0m[2m");

#if FALSE
			// doesn't work on DL's and not very accurate either
			if(system(NULL))
			{
				char command[buffer_length];
				sprintf(command, "addr2line -e %s %p",
						info.dli_fname,stack[m]);
				system(command);
			}
#endif
			text.catf("%s\n",info.dli_fname);
			text.catf("[0m");
		}
	}

	text.catf("[35mEND BACKTRACE[0m\n");

	if(symbols)
	{
		free(symbols);
	}
#endif

	return text;
}

void feLogBacktrace(U32 skip)
{
	const fe::String text=feGenerateBacktrace(skip);
	feLogDirect(text.c_str());
}

void feAttachDebugger(void)
{
#if FE_OS!=FE_OSX
#if FE_BACKTRACE_GDB
	fe::String value;
	const BWORD attach=
			(fe::System::getEnvironmentVariable("FE_SEGV_ATTACH", value) &&
				atoi(value.c_str()));

#if !FE_BATCH_GDB
	if(!attach)
	{
		return;
	}
#endif

	feLogDirect("[35mSTARTING GDB[0m\n");

	const U32 backtrace_size = 256;
	const U32 buffer_length=1024;

	void* stack[backtrace_size];
	I32 count=backtrace(stack,backtrace_size);
	Dl_info info;

	if(system(NULL) && dladdr(stack[count-1],&info) != 0)
	{
//		char* exename=info.dli_fname;
		char exename[buffer_length];
		exename[readlink("/proc/self/exe", exename, sizeof(exename)-1)]='\0';

		pid_t pid=getpid();
		char command[buffer_length*2];
		if(attach)
		{
			sprintf(command,"gdb -quiet %s %d",exename,pid);
		}
		else
		{
			sprintf(command,
					"echo 'bt\ndetach\nquit\n'| gdb -batch -x /dev/stdin %s %d",
					exename,pid);
		}
//		feLogDirect("Running: %s\n",command);
		int result=system(command);
		if(result)
		{
			feLogDirect("shell returned %d\n", result);
		}
	}

	feLogDirect("[35mEND GDB[0m\n");
#endif
#endif
}

#include <signal.h>

void feSegvHandler(int signal)
{
	feLogDirect("[31mSEGMENTATION FAULT[0m\n");
	feLogDirect("Free Electron has intercepted a crash"
			" using a registered handler.\n");
	feLogDirect("Note that any loaded code may be at fault.\n");
	feLogBacktrace();
	feAttachDebugger();
	exit(139);
}

void feTerminateHandler()
{
	feLogDirect("[31mTERMINATION[0m\n");
	feLogDirect("Free Electron has intercepted a potential"
			" uncaught exception using a registered handler.\n");
	feLogDirect("See https://en.cppreference.com/w/cpp/error/terminate"
			" for potential causes.\n");
	feLogDirect("Note that any loaded code may be at fault.\n");

	feLogBacktrace();

	std::exception_ptr eptr = std::current_exception();
	try
	{
		if(eptr)
		{
			std::rethrow_exception(eptr);
		}
		else
		{
			feLogDirect("[33mNO EXCEPTION FOUND[0m\n");
		}
	}
	catch(fe::Exception &e)
	{
		feLogDirect("[33mUNCAUGHT EXCEPTION:[0m fe::Exception\n");
		e.log();
		feLogDirect("%s\n",e.getStack().c_str());
	}
	catch(const std::exception& e)
	{
		feLogDirect("[33mUNCAUGHT EXCEPTION:[0m std::exception %s\n",
				e.what());
	}
	catch(...)
	{
		feLogDirect("[33mUNCAUGHT EXCEPTION:[0m unrecognized type\n");
	}

	feLogDirect("[31maborting[0m\n");
	fflush(stdout);

	//* TODO should we call the original handler instead?
	std::abort();
//	std::_Exit(EXIT_FAILURE);
}

void feRegisterSegvHandler(void)
{
	fe::String value(1);
	const BWORD found=
			fe::System::getEnvironmentVariable("FE_SEGV_BACKTRACE",value);

	if(!found)
	{
		struct sigaction oldAction;
		int fail=sigaction(SIGSEGV,NULL,&oldAction);
		if(fail)
		{
			feLogDirect("feRegisterSegvHandler failed to get old action\n");
		}
		else if(oldAction.sa_handler)
		{
			feLogDirect("%snot replacing existing segfault handler"
					" by default (override with FE_SEGV_BACKTRACE=1)\n",
					fe::PrefixLog::prefix().c_str());
			return;
		}
	}

	if(atoi(value.c_str()))
	{
		feLogDirect("%sregistering segfault handler\n",
				fe::PrefixLog::prefix().c_str());

		struct sigaction action;
		memset(&action,0,sizeof(action));
		action.sa_handler=feSegvHandler;
		sigemptyset(&action.sa_mask);
		action.sa_flags=0;

		int fail=sigaction(SIGSEGV,&action,NULL);
		if(fail)
		{
			feLogDirect("feRegisterSegvHandler failed\n");
		}
	}

	if(!fe::System::getEnvironmentVariable("FE_E_UNCAUGHT_BACKTRACE", value) ||
			atoi(value.c_str()))
	{
//		std::terminate_handler original_terminate_handler=
				std::set_terminate(feTerminateHandler);
	}
}
#else
fe::String feGenerateBacktrace(U32 skip)
{
	return("[35mBACKTRACE[0m: not implemented\n");
}

void feLogBacktrace(U32 skip)
{
	feLogDirect("[35mBACKTRACE[0m: not implemented\n");
}

void feAttachDebugger(void)
{
	feLogDirect("[35mBACKTRACE[0m: not implemented\n");
}

void feRegisterSegvHandler(void)
{
	if(fe::System::getVerbose()=="all")
	{
		feLogDirect("feRegisterSegvHandler not implemented\n");
	}
}
#endif
