/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "beaconTimeSync.pmh"

#define FE_BEACON_TIME_SYNC_VERBOSE				FALSE
#define FE_BEACON_TOTAL_NUM_ATTEMPTS			20
#define FE_BEACON_ALLOWED_UNRESPONSIVE_ATTEMPTS	10

using namespace fe;
using namespace ext;

namespace beacon
{

BeaconTimeSync::BeaconTimeSync()
{
	m_timeSynced = false;
	m_deltaTime = 0;
	m_latency = 0;
}

BeaconTimeSync::~BeaconTimeSync()
{
	shutdown();
}

bool BeaconTimeSync::timeSyncWithBeacon(const char *ipAddress,
	const uint16_t requestPort)
{
	strcpy(m_ipAddress, ipAddress);
	m_requestPort = requestPort;

	bool result = syncWithBeaconTime();

	shutdown();

	return result;
}

bool BeaconTimeSync::init()
{
	if(!m_spSingleMaster.isValid())
	{
		m_spSingleMaster=SingleMaster::create();
	}
	sp<Master> spMaster=m_spSingleMaster->master();
	sp<Registry> spRegistry = spMaster->registry();

	Result result = spRegistry->manage("fexMessageDL");
	if(failure(result))
	{
		feLog("BeaconTimeSync::init error loading fexMessageDL result:%d\n",
				result);
		return false;
	}

	if(!m_sendMsgSystem.isValid())
	{
		m_sendMsgSystem = spRegistry->create("MessageI.MessageUDP");
	}

	if(!m_recMsgSystem.isValid())
	{
		m_recMsgSystem = spRegistry->create("MessageI.MessageUDP");
	}

	if(!m_sendMsgSystem.isValid() || !m_recMsgSystem.isValid())
	{
		feLog("BeaconTimeSync::init loading MessageUDP failed\n");
		return false;
	}

	if(!m_sendMsgSystem->postInit() || !m_recMsgSystem->postInit())
	{
		feLog("BeaconTimeSync::init initialization of UDP system failed\n");
		return false;
	}

	m_responsePort = m_recMsgSystem->bindRandom();
	if(m_responsePort == 0)
	{
		feLog("BeaconTimeSync::init"
				" error failed trying to bind to a random response port\n");
		return false;
	}

	m_timeSynced = false;
	m_deltaTime = 0;
	m_latency = 0;

	return true;
}

void BeaconTimeSync::shutdown()
{
	if(m_sendMsgSystem.isValid())
	{
		feLog("BeaconTimeSync::shutdown shutting down send msg system\n");
		m_sendMsgSystem->shutdown();
	}

	if(m_recMsgSystem.isValid())
	{
		feLog("BeaconTimeSync::shutdown shutting down rec msg system\n");
		m_recMsgSystem->shutdown();
	}
}

bool BeaconTimeSync::syncWithBeaconTime()
{
	if(!init())
	{
		return false;
	}

	Messagegram msg;

	m_deltaTime = 0;
	int64_t totalLatency = 0;
	uint16_t unresponsiveAttempts = 0;
	int latencyTests = FE_BEACON_TOTAL_NUM_ATTEMPTS;
	int adjustmentTests = FE_BEACON_TOTAL_NUM_ATTEMPTS * 10;
	int syncCount = 0;

	m_timeSynced = false;

	while(!m_timeSynced)
	{
		sendSyncRequest();

		if(!m_recMsgSystem->recvFrom(&msg, 1, 0)) {
			unresponsiveAttempts++;
			if(unresponsiveAttempts == FE_BEACON_ALLOWED_UNRESPONSIVE_ATTEMPTS)
			{
				feLog("BeaconTimeSync::syncWithBeaconTime"
						" WARNING: reached threshold for"
						" unresponsive Beacon requests\n");
				break;
			}
		}
		else
		{
			unresponsiveAttempts = 0;
			MPPmessageHeader *msgHeader = (MPPmessageHeader *)msg.data;

			switch((BeaconMessageType)msgHeader->msgType)
			{
				case BeaconMessageType::nodeList:
#if FE_BEACON_TIME_SYNC_VERBOSE
					feLog("BeaconTimeSync::syncWithBeaconTime"
							" received node list from Beacon (ignoring)\n");
#endif
					break;

				case BeaconMessageType::currentTime:
					{
#if FE_BEACON_TIME_SYNC_VERBOSE
						feLog("BeaconTimeSync::syncWithBeaconTime"
								" received time from Beacon\n");
#endif
						int64_t localTime = getCurrentTime();

						CurrentTimeMsg *currentTimeMsg =
								(CurrentTimeMsg *)msg.data;

						// Calculate latency between request sent and response
						if(latencyTests > 0)
						{
							int64_t latency =
									(localTime - currentTimeMsg->clientTime);
							totalLatency += latency;
							latencyTests--;

							if(latencyTests == 0)
							{
								// Calculate average latency
								m_latency = totalLatency /
										(FE_BEACON_TOTAL_NUM_ATTEMPTS * 2);

								// Calculate delta time to match Beacon's time
								int64_t BeaconTime =
										currentTimeMsg->BeaconTime - m_latency;
								m_deltaTime = BeaconTime - localTime;
							}
						}
						else if(adjustmentTests > 0)
						{
							// Calculate delta time between Beacon's time
							// and the estimated Beacon's time
							int64_t deltaEstimated =
									currentTimeMsg->BeaconTime -
									currentTimeMsg->estBeaconTime;
							if(deltaEstimated > 0 )
							{
								m_deltaTime++;
							}
							else if(deltaEstimated < 0)
							{
								m_deltaTime--;
							}
							else
							{
								syncCount++;
								if(syncCount == 3)
								{
									feLog("BeaconTimeSync:: time synced\n");
									m_timeSynced = true;
								}
							}

							adjustmentTests--;
							if(adjustmentTests == 0)
							{
								m_timeSynced = true;
							}
						}
					}
					break;

				default:
					feLog("BeaconTimeSync::syncWithBeaconTime"
							" WARNING: received Beacon message"
							" with unrecognized type\n");
					break;
			}
		}
	}

	if (!m_timeSynced)
	{
		feLog("BeaconTimeSync::syncWithBeaconTime"
				" WARNING: Unable to synchronize with Beacon time\n");
	}

	return m_timeSynced;
}

void BeaconTimeSync::sendSyncRequest()
{
	Messagegram syncMsg;

	TimeSyncRequestMsg *msg = (TimeSyncRequestMsg *)syncMsg.data;
	syncMsg.dataLen = sizeof(TimeSyncRequestMsg);

	msg->header.msgType = NodeMessageType::timeSyncRequest;
	msg->header.msgLen = sizeof(TimeSyncRequestMsg);
	msg->responsePort = m_responsePort;
	msg->clientTime = getCurrentTime();
	// Calculate the estimated Beacon's time when Beacon will
	// receive this message
	msg->estBeaconTime = msg->clientTime + m_deltaTime + m_latency;

	msg->CRC = calculateCRC(syncMsg.data,
							sizeof(TimeSyncRequestMsg) - sizeof(msg->CRC),
							0);
#if FE_BEACON_TIME_SYNC_VERBOSE
	feLog("BeaconTimeSync::sendSyncRequest sending sync msg\n");
#endif
	sendRequest(syncMsg);
}

bool BeaconTimeSync::sendRequest(const Messagegram &msg)
{
	return m_sendMsgSystem->sendTo(msg, m_ipAddress, m_requestPort);
}

int64_t BeaconTimeSync::getCurrentTime()
{
	int64_t time = std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	return time;
}

int64_t BeaconTimeSync::getTimeOffset()
{
	return (m_timeSynced ? m_deltaTime : 0);
}

int64_t BeaconTimeSync::getLatency()
{
	return (m_timeSynced ? m_latency : 0);
}

int64_t BeaconTimeSync::getBeaconTime()
{
	int64_t time = std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	return time + getTimeOffset();
}

} // namespace beacon
