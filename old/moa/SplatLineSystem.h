/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_SplatLineSystem_h__
#define __moa_SplatLineSystem_h__

#include <complex>

namespace fe
{
namespace ext
{

/* WHY ----------------------------------------------------------------------

why image
  * human sensory
  * RT screen
  * image tool ecosystem use
  * ML
  * fixed data size   size != fn(t)
  * lookup / cache / etc
  * common currency ?

-------------------------------------------------------------------------- */

/* WHAT ---------------------------------------------------------------------

-------------------------------------------------------------------------- */

/* HOW ----------------------------------------------------------------------


  * Add a SplatterI System to the Orchestrator

Splatters (VIEW)
	"SystemI.NyquistLineSystem.fe"
	"SystemI.SplatLineSystem.fe"

Test System
	"SystemI.MultiSweep.fe"
	"SystemI.FrequencyResponse.fe"
OR
  sending signals

overlays



Continuous State HUD Use Case
-----------------------------

Can also instrument continuous running -- HUD example

Physics Acceleration Use Case
-----------------------------

This is splatting direct -- not even as a System, but direct via solve splat
This is why core splat is located in solve (at the moment)


The Test Suite Case
-------------------

This use case involves two separate concepts:
	* capturing data and presenting clear results
	* generating data as useful test cases



VIEWING/SPLATTING

Given
	* An Orchestrator configured to run a MOA simulation
	* An available SplatterI System to view results

Summary
	* Add a splatter to the Orchestrator
	* Add splat configuration to dataset


spOrchestrator->append(spRegistry->create("SystemI.SplatLineSystem.fe"));

Multiple splat specifications can apply to the same image

splat:x_range -- minimum and maximum value of X
splat:y_range -- minimum and maximum value of Y
splat:expression -- eval script run every data point
splat:splat -- name of splat image to splat to
splat:color -- color of data applied

Cover using color via splat:expression

RECORD * alpha_splat
	name			"αFy"
	splat:splat		"αFyFzMz"
	splat:x_range	"-30.0		30.0"
	splat:y_range	"-7000.0	7000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.mnt:0.tire:force[1]')	)"
	splat:color		"1 0 0.6"







RUNNING TESTS

spOrchestrator->append(spRegistry->create("SystemI.MultiSweep.fe"));

RECORD * multisweep
	name			"alphakappa"
	composer:inner	"-20.0 20.0 1.0"
	composer:outer	"-20.0 20.0 10.0"
	composer:step	"	(set	'base_rig.rig:kappa' INNER)
						(set	'base_rig.rig:alpha' OUTER)		"


-------------------------------------------------------------------------- */

/// @brief A System to capture data via splatting
class FE_DL_EXPORT SplatterSystem :
	virtual public SystemI,
	virtual public SplatterI,
	public Initialize<SplatterSystem>
{
	public:
				SplatterSystem(void);
virtual			~SplatterSystem(void);
		void	initialize(void);

virtual	void	compile(const t_note_id &a_note_id);

virtual	bool	match(const Record &r_splat)								= 0;

virtual	void	image(void);

virtual	void	connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

		class Annotation
		{
			public:
				Annotation(t_moa_real a_x, t_moa_real a_y,
					const String &a_text) :
					m_x(a_x), m_y(a_y), m_text(a_text) {}
				t_moa_real	m_x;
				t_moa_real	m_y;
				String		m_text;
		};

		class Context : public Counted
		{
			public:
				Context(void);
				SplatterSystem					*m_parent;
				sp<Evaluator>		m_spMyEval;
				WeakRecord					r_x;
				WeakRecord					r_y;
				WeakRecord					r_r;
				WeakRecord					r_g;
				WeakRecord					r_b;
				WeakRecord					r_splat;
				bool							m_line_started;
				t_image_splat::t_span	m_loc;
				void setReal(const String &a_name, t_moa_real a_value);
				void dot(t_moa_real a_x, t_moa_real a_y);
				void mark(t_moa_real a_x, t_moa_real a_y);
				void line(t_moa_real a_x, t_moa_real a_y);
				void annotate(t_moa_real a_x, t_moa_real a_y,
					const String &a_text);
				void annotate(t_moa_real a_x, t_moa_real a_y,
					t_moa_real a_value);

				void newline(void);
				t_moa_v3						m_y_max;
				std::vector<t_moa_real>		m_x_run;
				std::vector<t_moa_real>		m_y_run;
				t_moa_real						m_x_n;
				t_moa_real						m_y_n;
				t_moa_v3						m_color;
				sp<t_image_splat>		m_splat;
				std::vector< Annotation >		m_annotations;
				String						m_splatname;
				String						m_splatlabel;
		};

		void updateContext(sp<Context> a_context);


	protected:
		class SplatTarget
		{
			public:
				sp<t_image_splat>		m_splat;
				std::vector< sp<Context> >		m_contexts;
		};
		sp<RecordGroup>									rg_dataset;
		sp<Scope>										m_spScope;
		AsSplat													m_asSplat;
		typedef	std::map< String, SplatTarget >				t_splatmap;
		t_splatmap												m_splatMap;
		AsDatasetMeta											m_asDatasetMeta;
		std::vector< sp<Context> >							m_contexts;
		t_note_id	m_note_image;
		t_note_id	m_note_datapoint;
		t_note_id	m_note_run_stop;
		t_note_id	m_note_line_start;
		t_note_id	m_note_line_stop;
		t_note_id	m_note_loop_start;
		t_note_id	m_note_loop_stop;
		t_note_id	m_note_dt_change;
};

class FE_DL_EXPORT SplatLineSystem :
	virtual public SplatterSystem,
	public Initialize<SplatLineSystem>
{
	public:
				SplatLineSystem(void);
virtual			~SplatLineSystem(void);
		void	initialize(void) {}

virtual	void	perform(const t_note_id &a_note_id);
virtual	bool	match(const Record &r_splat);

};

class FE_DL_EXPORT NyquistLineSystem :
	virtual public SplatterSystem,
	public Initialize<NyquistLineSystem>
{
	public:
				NyquistLineSystem(void);
virtual			~NyquistLineSystem(void);
		void	initialize(void);

virtual	void	perform(const t_note_id &a_note_id);
virtual	void	compile(const t_note_id &a_note_id);
virtual	bool	match(const Record &r_splat);

		class NyquistContext
		{
			public:
				t_moa_real						m_start_time;
		};

	private:
		Record						a_r_time;
		AsTime							m_asTime;
		std::vector<NyquistContext>		m_nyquistContexts;
		AsNyquistLine					m_asNyquistLine;
};

class Imager : virtual public t_image_splat::Functor
{
	public:
		Imager(FILE *a_fp)
		{
			m_fp = a_fp;
		}
		virtual ~Imager(void)
		{
		}
		virtual void process(t_image_splat &a_splat,
			typename t_image_splat::t_index &a_index)
		{
			typename t_image_splat::t_span loc;
			t_moa_v3 color = a_splat.direct(a_index);
			a_splat.location(loc, a_index);
			int R = (int)(color[0]*255.0);
			int G = (int)(color[1]*255.0);
			int B = (int)(color[2]*255.0);
			fputc(R, m_fp);
			fputc(G, m_fp);
			fputc(B, m_fp);
		}
		FILE *m_fp;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_SplatLineSystem_h__ */

