/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_DualQuaternion_h__
#define __math_DualQuaternion_h__

namespace fe
{

/**************************************************************************//**
	@brief A pair of quaternions, usable as a transform

	@ingroup math

	https://en.wikipedia.org/wiki/Dual_quaternion
	https://users.cs.utah.edu/~ladislav/kavan07skinning/kavan07skinning.pdf
	https://github.com/Hasenpfote/dualquat
*//***************************************************************************/
template<class T>
class DualQuaternion: protected Vector<4,T>
{
	public:
						DualQuaternion(void);
						DualQuaternion(const DualQuaternion<T>& a_other);
						DualQuaternion(const Quaternion<T>& a_real,
								const Quaternion<T>& a_dual);
						DualQuaternion(const Matrix<3,4,T>& a_matrix);

		DualQuaternion<T>&	operator=(const DualQuaternion<T>& a_other);
		DualQuaternion<T>&	operator=(const Matrix<3,4,T>& a_matrix);

const	Quaternion<T>&	real(void) const	{ return m_real; }
const	Quaternion<T>&	dual(void) const	{ return m_dual; }

	private:
		Quaternion<T>	m_real;
		Quaternion<T>	m_dual;
};

template<class T>
inline DualQuaternion<T>::DualQuaternion(void)
{
	set(m_real);
	set(m_dual,0);
}

template<class T>
inline DualQuaternion<T>::DualQuaternion(const DualQuaternion<T>& a_other):
	Vector<4,T>()
{
	operator=(a_other);
}

template<class T>
inline DualQuaternion<T>::DualQuaternion(const Quaternion<T>& a_real,
	const Quaternion<T>& a_dual)
{
	m_real=a_real;
	m_dual=a_dual;
}

template<class T>
inline DualQuaternion<T>& DualQuaternion<T>::operator=(
	const DualQuaternion<T>& a_other)
{
	m_real=a_other.m_real;
	m_dual=a_other.m_dual;
	return *this;
}

template<class T>
inline DualQuaternion<T>& set(DualQuaternion<T>& a_rResult)
{
	set(a_rResult.m_real);
	set(a_rResult.m_dual,0);
	return a_rResult;
}

/** @brief Normalize the first quaternion

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T>& normalize(DualQuaternion<T>& a_rResult)
{
	const Vector<4,T>& rVectorReal=
			*reinterpret_cast< const Vector<4,T>* >(&a_rResult.real());
	const T length=magnitude(rVectorReal);
	if(length<1e-6)
	{
		return a_rResult;
	}

	const Vector<4,T>& rVectorDual=
			*reinterpret_cast< const Vector<4,T>* >(&a_rResult.dual());
	const T inv=T(1)/length;

	a_rResult=DualQuaternion<T>(
			Quaternion<T>(rVectorReal[0]*inv,rVectorReal[1]*inv,
			rVectorReal[2]*inv,rVectorReal[3]*inv),
			Quaternion<T>(rVectorDual[0]*inv,rVectorDual[1]*inv,
			rVectorDual[2]*inv,rVectorDual[3]*inv));

	return a_rResult;
}


//* UNARY OPERATORS
/** @brief Add in place

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T>& operator+=(DualQuaternion<T>& a_lhs,
	const DualQuaternion<T>& a_rhs)
{
	a_lhs.m_real+=a_rhs.m_real;
	a_lhs.m_dual+=a_rhs.m_dual;
	return a_lhs;
}

/** @brief Subtract in place

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T>& operator-=(DualQuaternion<T>& a_lhs,
	const DualQuaternion<T>& a_rhs)
{
	a_lhs.m_real-=a_rhs.m_real;
	a_lhs.m_dual-=a_rhs.m_dual;
	return a_lhs;
}

/** @brief Multiply in place

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T>& operator*=(DualQuaternion<T>& a_lhs,
	const DualQuaternion<T>& a_rhs)
{
	return a_lhs=a_lhs*a_rhs;
}

/** @brief Return the inverse rotation

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T> operator-(const DualQuaternion<T>& a_lhs)
{
	DualQuaternion<T> inverse;
	set(inverse,-a_lhs.m_real,-a_lhs.m_dual);
	return inverse;
}

/** @brief Print to a string

	@relates DualQuaternion
	*/
template<class T>
inline String print(const DualQuaternion<T>& a_lhs)
{
	String s;
	s.sPrintf("%s  %s",c_print(a_lhs.real()),c_print(a_lhs.dual()));
	return s;
}

//* BINARY OPERATORS
/** @brief Equality test

	@relates DualQuaternion
	*/
template<class T>
inline bool operator==(const DualQuaternion<T>& a_lhs,
	const DualQuaternion<T>& a_rhs)
{
	for(U32 i=0; i<4; i++)
	{
		if(a_rhs.m_real[i] != a_lhs.m_real[i] ||
				a_rhs.m_dual[i] != a_lhs.m_dual[i])
		{
			return false;
		}
	}
	return true;
}

/** @brief Equivalence test within the given tolerance @em margin

	@relates DualQuaternion
	*/
template<class T>
inline bool equivalent(const DualQuaternion<T> &a_lhs,
	const DualQuaternion<T> &a_rhs,T margin)
{
	return(	equivalent(a_lhs.m_real,a_rhs.m_real,margin) &&
			equivalent(a_lhs.m_dual,a_rhs.m_dual,margin));
}

/** @brief Add two dual quaternions

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T> operator+(const DualQuaternion<T>& a_lhs,
		const DualQuaternion<T>& a_rhs)
{
	return DualQuaternion<T>(a_lhs.real()+a_rhs.real(),
			a_lhs.dual()+a_rhs.dual());
}

/** @brief Subtract two dual quaternions

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T> operator-(const DualQuaternion<T>& a_lhs,
		const DualQuaternion<T>& a_rhs)
{
	return DualQuaternion<T>(a_lhs.real()-a_rhs.real(),
			a_lhs.dual()-a_rhs.dual());
}

/** @brief Multiply two dual quaternions

	@relates DualQuaternion
	*/
template<class T>
inline DualQuaternion<T> operator*(const DualQuaternion<T>& a_lhs,
		const DualQuaternion<T>& a_rhs)
{
	return DualQuaternion<T>(a_lhs.real()*a_rhs.real(),
			a_lhs.real()*a_rhs.dual() + a_lhs.dual()*a_rhs.real());
}

/** @brief Scale the transform

	@relates DualQuaternion
	*/
template<class T,class U>
inline DualQuaternion<T> operator*(U a_lhs, const DualQuaternion<T>& a_rhs)
{
	return operator*(a_rhs,a_lhs);
}

/** @brief Scale the transform

	@relates DualQuaternion
	*/
template<class T,class U>
inline DualQuaternion<T> operator*(const DualQuaternion<T>& a_lhs,U a_rhs)
{
	return DualQuaternion<T>(a_lhs.real()*a_rhs,a_lhs.dual()*a_rhs);
}

template <typename T>
inline void transformVector(const DualQuaternion<T>& lhs,const Vector<3,T>& in,
		Vector<3,T>& out)
{
	const DualQuaternion<T> dq_in(Quaternion<T>(),
			Quaternion<T>(in[0],in[1],in[2],T(0)));
	const DualQuaternion<T> both_conjugate(
			conjugate(lhs.real()),-conjugate(lhs.dual()));

	const DualQuaternion<T> dq_out=lhs*dq_in*both_conjugate;

	const Quaternion<T>& dual_out=dq_out.dual();
	set(out,dual_out[0],dual_out[1],dual_out[2]);
}

typedef DualQuaternion<F32>		DualQuaternionf;
typedef DualQuaternion<F64>		DualQuaterniond;
typedef DualQuaternion<Real>	SpatialDualQuaternion;

} /* namespace */

#endif /* __math_DualQuaternion_h__ */

