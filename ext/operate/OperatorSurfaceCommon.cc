/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operate/operate.pmh>

#define FE_OSC_UNDO_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;
void OperatorSurfaceCommon::handleBind(sp<SignalerI> a_spSignalerI,
									sp<Layout> a_spLayout)
{
	OperateCommon::handleBind(a_spSignalerI,a_spLayout);

	m_aStartFrame.initialize(a_spLayout->scope(),
			"StartFrame");
	m_aEndFrame.initialize(a_spLayout->scope(),
			"EndFrame");
	m_aFrame.initialize(a_spLayout->scope(),
			"Frame");
	m_aTime.initialize(a_spLayout->scope(),
			"Time");
	m_aSurfaceOutput.initialize(a_spLayout->scope(),
			"SurfaceOutput");
	m_aDrawOutput.initialize(a_spLayout->scope(),
			"DrawI");
	m_aDrawGuide.initialize(a_spLayout->scope(),
			"DrawGuide");
	m_aDrawBrush.initialize(a_spLayout->scope(),
			"DrawBrush");
	m_aDrawBrushOverlay.initialize(a_spLayout->scope(),
			"DrawBrushOverlay");
	m_aCameraTransform.initialize(a_spLayout->scope(),
			"CameraTransform");
	m_aRayOrigin.initialize(a_spLayout->scope(),
			"RayOrigin");
	m_aRayDirection.initialize(a_spLayout->scope(),
			"RayDirection");
}

//* like CacheOp hoards
void OperatorSurfaceCommon::changeSharing(BWORD a_include)
{
	if(m_sharingName.empty())
	{
		return;
	}

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	Array< hp<Component> >& rGlobalArray=
			spMasterCatalog->catalog< Array< hp<Component> > >(
			m_sharingName);

	if(a_include)
	{
		rGlobalArray.push_back(sp<Component>(this));
		return;
	}

	const U32 componentCount=rGlobalArray.size();
	for(U32 componentIndex=0;componentIndex<componentCount;componentIndex++)
	{
		sp<Component> spComponent=rGlobalArray[componentIndex];
		if(spComponent.raw()==this)
		{
			rGlobalArray[componentIndex]=rGlobalArray[componentCount-1];
			rGlobalArray.resize(componentCount-1);
			return;
		}
	}
}

void OperatorSurfaceCommon::selectNode(String a_selectName)
{
	if(m_sharingName.empty())
	{
		return;
	}

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	Array< hp<Component> >& rGlobalArray=
			spMasterCatalog->catalog< Array< hp<Component> > >(
			m_sharingName);

	const U32 componentCount=rGlobalArray.size();
	for(U32 componentIndex=0;componentIndex<componentCount;componentIndex++)
	{
		sp<Component> spComponent=rGlobalArray[componentIndex];
		if(spComponent.isValid())
		{
			sp<OperatorSurfaceCommon> spOperator=spComponent;
			if(spOperator.isValid())
			{
				if(spOperator->name()==a_selectName)
				{
					spOperator->select();
				}
			}
		}
	}
}
String OperatorSurfaceCommon::generateSignature(
	sp<SurfaceAccessibleI>& a_rspAccessibleI)
{
#if FALSE
	sp<SurfaceAccessorI> spPoints;
	if(!access(spPoints,a_rspAccessibleI,e_point,e_generic))
	{
		return "";
	}

	sp<SurfaceAccessorI> spVertices;
	if(!access(spVertices,a_rspAccessibleI,e_primitive,e_vertices))
	{
		return "";
	}

	const I32 pointCount=spPoints->count();
	const I32 primitiveCount=spVertices->count();
#else
	const I32 pointCount=
			a_rspAccessibleI->count(SurfaceAccessibleI::e_point);
	const I32 primitiveCount=
			a_rspAccessibleI->count(SurfaceAccessibleI::e_primitive);
#endif

	String signature;
	signature.sPrintf("%d:%d",pointCount,primitiveCount);

	return signature;
}

BWORD OperatorSurfaceCommon::undo(String a_change,sp<Counted> a_spCounted)
{
#if FE_OSC_UNDO_DEBUG
	feLog("OperatorSurfaceCommon::undo \"%s\" %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	sp<Catalog> spCatalog(a_spCounted);
	if(spCatalog.isNull())
	{
		return FALSE;
	}

	catalogOverlay(spCatalog);
	return TRUE;
}

BWORD OperatorSurfaceCommon::redo(String a_change,sp<Counted> a_spCounted)
{
#if FE_OSC_UNDO_DEBUG
	feLog("OperatorSurfaceCommon::redo \"%s\" %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	sp<Catalog> spCatalog(a_spCounted);
	if(spCatalog.isNull())
	{
		return FALSE;
	}

	catalogOverlay(spCatalog);
	return TRUE;
}

BWORD OperatorSurfaceCommon::determineNeighborhood(
	sp<SurfaceAccessibleI>& a_rspAccessible,
	Array< std::set<I32> >& a_rPointFaces,
	Array< std::set<I32> >& a_rPointNeighbors)
{
	sp<SurfaceAccessorI> spPoint;
	if(!access(spPoint,a_rspAccessible,e_point,e_position))
		return FALSE;

	sp<SurfaceAccessorI> spVertices;
	if(!access(spVertices,a_rspAccessible,
			e_primitive,e_vertices)) return FALSE;

	const U32 pointCount=spPoint->count();

	//* set size and init values
	a_rPointFaces.assign(pointCount,std::set<I32>());
	a_rPointNeighbors.assign(pointCount,std::set<I32>());

	const U32 primitiveCount=spVertices->count();
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			return FALSE;
		}

		const U32 subCount=spVertices->subCount(primitiveIndex);
		if(!subCount)
		{
			continue;
		}

		U32 lastPointIndex=
				spVertices->integer(primitiveIndex,subCount-1);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const U32 pointIndex=
					spVertices->integer(primitiveIndex,subIndex);

			a_rPointFaces[pointIndex].insert(primitiveIndex);

			a_rPointNeighbors[pointIndex].insert(lastPointIndex);
			a_rPointNeighbors[lastPointIndex].insert(pointIndex);

			lastPointIndex=pointIndex;
		}
	}

	return TRUE;
}

sp<Catalog> OperatorSurfaceCommon::duplicateCatalog(void)
{
	Array<String> typeList;
	typeList.push_back(getTypeId<bool>().name());
	typeList.push_back(getTypeId<I32>().name());
	typeList.push_back(getTypeId<Real>().name());
	typeList.push_back(getTypeId<String>().name());
	typeList.push_back(getTypeId<SpatialVector>().name());

	sp<Catalog> spCatalog(this);
	sp<Catalog> spDuplicate=registry()->master()->createCatalog("chronicle");
	spDuplicate->catalogOverlay(spCatalog,&typeList);

	return spDuplicate;
}

BWORD OperatorSurfaceCommon::anticipate(String a_change,sp<Counted> a_spCounted)
{
#if FE_OSC_UNDO_DEBUG
	feLog("OperatorSurfaceCommon::anticipate \"%s\" counted valid %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	m_anticipation=a_change;
	m_spOldCounted=a_spCounted.isValid()? a_spCounted:
			sp<Counted>(duplicateCatalog());

	return TRUE;
}

BWORD OperatorSurfaceCommon::resolve(String a_change,sp<Counted> a_spCounted)
{
#if FE_OSC_UNDO_DEBUG
	feLog("OperatorSurfaceCommon::resolve \"%s\" counted valid %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	if(!a_change.empty() && a_change!=m_anticipation)
	{
		feLog("OperatorSurfaceCommon::resolve \"%s\" ignored"
				" after anticipated \"%s\"\n",
				a_change.c_str(),m_anticipation.c_str());

		m_anticipation="";
		m_spOldCounted=NULL;
		return FALSE;
	}

	const BWORD success=OperatorSurfaceCommon::chronicle(m_anticipation,
			m_spOldCounted,
			a_spCounted.isValid()? a_spCounted:
			sp<Counted>(duplicateCatalog()));

	m_anticipation="";
	m_spOldCounted=NULL;

	return success;
}
