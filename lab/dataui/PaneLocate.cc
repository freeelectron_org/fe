/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "PaneLocate.h"

namespace fe
{
namespace ext
{

PaneLocate::PaneLocate(void)
{
}

PaneLocate::~PaneLocate(void)
{
}


void PaneLocate::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }
	for(RecordGroup::iterator it = m_drawview.group()->begin();
		it != m_drawview.group()->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asPane.bind(spRA->layout()->scope());
		if(m_asPane.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(!m_asPane.group(spRA,i).isValid()) { continue; }
				for(RecordGroup::iterator i_pane =
					m_asPane.group(spRA,i)->begin();
					i_pane != m_asPane.group(spRA,i)->end(); i_pane++)
				{
					sp<RecordArray> ra_pane(*i_pane);
					m_asWidget.bind(ra_pane->layout()->scope());
					if(m_asWidget.check(ra_pane))
					{
						for(int j = 0; j < ra_pane->length(); j++)
						{
							convert(	m_asWidget.screen(ra_pane,j),
										m_asPane.screen(spRA,i),
										m_asWidget.start(ra_pane,j),
										m_asWidget.end(ra_pane,j));
							if(	m_asWidget.screen(ra_pane,j)[2] >
								m_asPane.screen(spRA,i)[2] ||
								m_asWidget.screen(ra_pane,j)[3] >
								m_asPane.screen(spRA,i)[3])
							{
								m_asWidget.screen(ra_pane,j)[0] = 0.0;
								m_asWidget.screen(ra_pane,j)[1] = 0.0;
								m_asWidget.screen(ra_pane,j)[2] = 0.0;
								m_asWidget.screen(ra_pane,j)[3] = 0.0;
							}
						}
					}
				}
			}
		}
	}
}

void PaneLocate::convert(Vector4 &a_v, const Vector4 &a_s, const Vector2 &a_start, const Vector2 &a_end)
{
	Real h = a_s[3] - a_s[1];
	Real w = a_s[2] - a_s[0];

	if(a_start[0] < 0.0)
	{
		a_v[0] = w + a_start[0];
	}
	else if(a_start[0] < 1.0)
	{
		a_v[0] = a_start[0] * w;
	}
	else
	{
		a_v[0] = a_start[0];
	}

	if(a_start[1] < 0.0)
	{
		a_v[1] = h + a_start[1];
	}
	else if(a_start[1] < 1.0)
	{
		a_v[1] = a_start[1] * h;
	}
	else
	{
		a_v[1] = a_start[1];
	}

	if(a_end[0] < 0.0)
	{
		a_v[2] = w + a_end[0];
	}
	else if(a_end[0] < 1.0)
	{
		a_v[2] = a_end[0] * w;
	}
	else
	{
		a_v[2] = a_end[0];
	}

	if(a_end[1] < 0.0)
	{
		a_v[3] = h + a_end[1];
	}
	else if(a_end[1] < 1.0)
	{
		a_v[3] = a_end[1] * h;
	}
	else
	{
		a_v[3] = a_end[1];
	}

	Real t;
	if(a_v[2] < a_v[0])
	{
		t = a_v[0];
		a_v[0] = a_v[2];
		a_v[2] = t;
	}

	if(a_v[3] < a_v[1])
	{
		t = a_v[1];
		a_v[1] = a_v[3];
		a_v[3] = t;
	}

	a_v[0] += a_s[0];
	a_v[2] += a_s[0];
	a_v[1] += a_s[1];
	a_v[3] += a_s[1];
}

} /* namespace ext */
} /* namespace fe */

