/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fex/pyfe.h"
#include "pyfe/Convertor.h"

#include "signal/signal.h"

using namespace boost::python;
#include "pyfe/fe_data.h"
#include "pyfe/fe_signal.h"

void signalExceptionTranslator(fe::Exception const& e)
{
	PyErr_SetString(PyExc_UserWarning, e.getMessage().c_str());
}

/* ************************************************************************* */
BOOST_PYTHON_MODULE(signal)
{
	fe::sp<fe::TypeMaster> spTypeMaster(pyfe::feMaster().typeMaster());
	fe::sp<fe::BaseType> spBT=
			spTypeMaster->assertPtr<fe::HandlerI>("HandlerI");

	register_exception_translator<fe::Exception>(&signalExceptionTranslator);
	class_<SignalerFS>("SignalerFS", init<PyObject *>())
		.def("insert", &SignalerFS::insert)
		.def("remove", &SignalerFS::remove)
		.def("signal", &SignalerFS::signal)
		.def("dump",   &SignalerFS::dump)
		.def("object", &SignalerFS::object)
	;
	object handlerClass=
			class_<HandlerBase, HandlerFS, boost::noncopyable>("HandlerFS")
		.def(init<PyObject *>())
		.def("object", &HandlerFS::object)
	;
	class_<SequencerFS>("SequencerFS", init<PyObject *>())
		.def("getCurrentTime",	&SequencerFS::getCurrentTime)
		.def("setTime",			&SequencerFS::setTime)
		.def("add",				&SequencerFS::add)
		.def("lookup",			&SequencerFS::lookup)
		.def("remove",			&SequencerFS::remove)
		.def("object",			&SequencerFS::object)
	;
	class_<ParserFS>("ParserFS", init<PyObject *>())
		.def("parse",			&ParserFS::parse)
		.def("object",			&ParserFS::object)
	;

	fe::sp<pyfe::BaseConvertor> spC(
		new pyfe::Convertor<fe::sp<fe::HandlerI>,HandlerFS>(handlerClass));
	pyfe::addConvertor(spBT, spC);
}



