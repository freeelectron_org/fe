/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaBind_h__
#define __lua_LuaBind_h__

namespace fe
{
namespace ext
{

/** use LuaExec instead?
	@copydoc LuaBind_info
	*/
class LuaBind : public HandlerI, public Initialize<LuaBind>
{
	public:
				LuaBind(void);
virtual			~LuaBind(void);
		void	initialize(void);

virtual void	bind(	sp<SignalerI> spSignaler,
						sp<Layout> l_sig);
virtual void	handle(	Record &r_sig);

	private:
		lua_State			*m_pLuaState;
		String				m_chunk;
		sp<LuaContext>		m_spContext;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __lua_LuaBind_h__ */

