import os
import re
import sys
import copy
import shutil
import subprocess

import utility
forge = sys.modules["forge"]

# NOTE: The Houdini version of boost may be different.
# The boost-regex appears to be sensitive and they don't provide the dso.
# Moving aside their include/boost seems to fix regex,
# but it leaves their other boost dso's cross-versioned with system includes.

# NOTE: Houdini ode includes also conflict
# Try moving aside their include/ode.

def prerequisites():
    return ["surface"]

def run_hcustom(houdini_bin, hcustom_arg):
    existing_HFS = None
    if "HFS" in os.environ:
        existing_HFS = os.environ["HFS"]

    os.environ["HFS"] = os.path.realpath(os.path.abspath(houdini_bin + "/.."))

    command = '"' + os.path.join(houdini_bin, "hcustom") + '" ' + hcustom_arg

    p = subprocess.Popen(forge.cleanup_command(command), shell=True, bufsize=-1,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    result = p.stdout.read()
    n = p.stdout.close()

    result = result.decode("utf-8").rstrip()

    # NOTE we will add '-c' ourselves, where needed
    result = re.sub(" -c","",result)

    if existing_HFS:
        os.environ["HFS"] = existing_HFS
    else:
        del os.environ["HFS"]

    return result

def setup(module):
    module.summary = []

    if not len(forge.houdini_sources) or not len(forge.houdini_sources[0])==4:
        module.summary += [ "no_installs" ]
        return

    module.cppmap = {}
    test_include = {}
    test_cxx = ''
    test_cpp = {}
    test_link = {}
    test_variant = forge.houdini_sources[0][0]

    #NOTE separate obj path for each version

    # force all of FE to use Houdini's version of boost
#   forge.includemap['houdini'] = os.path.join(os.getenv('HT'),'include')

    deplibs = forge.corelibs + [
                "fexDrawDLLib",
                "fexMetaDLLib",
                "fexOperateDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    houdini_oplibs_replace = utility.env_list("FE_HOUDINI_OPLIBS_REPLACE")
    houdini_oplibs_append = utility.env_list("FE_HOUDINI_OPLIBS_APPEND")

    fe_houdini_oplibs = ':'.join(houdini_oplibs_replace)
    if fe_houdini_oplibs == '':
        if os.path.exists(module.modPath + '/../grass'):
            fe_houdini_oplibs += ':fexGrassDL'
            module.summary += [ "grass" ]
        if os.path.exists(module.modPath + '/../ironworks'):
            fe_houdini_oplibs += ':fexIronWorksDL'
            module.summary += [ "ironworks" ]
        if os.path.exists(module.modPath + '/../opencl'):
            fe_houdini_oplibs += ':fexOpenCLDL'
            module.summary += [ "opencl" ]
        if os.path.exists(module.modPath + '/../operator'):
            fe_houdini_oplibs += ':fexOperatorDL'
            module.summary += [ "operator" ]
        if os.path.exists(module.modPath + '/../vegetation'):
            fe_houdini_oplibs += ':fexVegetationDL'
            module.summary += [ "vegetation" ]
    else:
        fe_houdini_oplibs = ':' + fe_houdini_oplibs
    if len(houdini_oplibs_append):
        fe_houdini_oplibs += ':' + ':'.join(houdini_oplibs_append)

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")

    for houdini_source in forge.houdini_sources:
        if not os.path.exists(houdini_source[1]) or not os.path.exists(houdini_source[2]):
            module.summary += [ "-" + houdini_source[0] ]
            continue

        if houdini_source[0] == "":
            module.summary += [ "HT" ]
        else:
            module.summary += [ houdini_source[0] ]

        houdini_bin = houdini_source[2] + "/../bin"
        houdini_cflags = run_hcustom(houdini_bin, "-c")
        houdini_ldflags = run_hcustom(houdini_bin, "-m")

        variant = houdini_source[0]

        variantSplit = variant.split('.')
        if len(variantSplit) and variantSplit[0] != "":
            houdini_major = int(variantSplit[0])
            houdini_minor = int(variantSplit[1])
        else:
            houdini_major = 12
            houdini_minor = 0
            houdini_major_string = os.getenv('HOUDINI_MAJOR_RELEASE')
            if houdini_major_string:
                houdini_major = int(houdini_major_string)

        srcList = [ "houdini.pmh",
                    "FontHoudini",
                    "HoudiniBrush",
                    "HoudiniDraw",
                    "HoudiniSOP",
                    "CacheOp",
                    "PortalOp",
                    "SurfaceAccessibleHoudini",
                    "SurfaceAccessorHoudini",
                    "SurfaceCurvesHoudini",
                    "SurfacePointsHoudini",
                    "SurfaceTrianglesHoudini",
                    "assertHoudini",
                    "houdiniDL" ]

        if houdini_major >= 15:
            srcList += [    "HoudiniPrimComponent",
                            "HoudiniRenderComponent",
                            "UnveilOp",
                            "VeilOp" ]

        if variant != test_variant:
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink('.', variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        dllname = "fexHoudiniDL" + variant

        dll = module.DLL( dllname, srcList )

        fe_houdini_libs = dllname + fe_houdini_oplibs

        alt_cxx = houdini_source[3]

        if alt_cxx != '':
            dll.cxx = alt_cxx

        # NOTE clearing all SSE flags may be excessively paranoid

        need_specialization = False
        clear_sse = False
        if forge.fe_os == "FE_LINUX":
            forge.evaluate_compiler(dll, False)
            # needed for gcc 4.2, not for gcc 4.4, but not sure otherwise
            if forge.compiler_version < 4.3:
                need_specialization = True
                clear_sse = True
            forge.evaluate_compiler(None, False)

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)
            srcTarget.includemap = { 'houdini_src' : houdini_source[1] }

            #HACK
            srcTarget.includemap['houdini_python'] = os.path.join(houdini_source[1],"python3.9")

            if alt_cxx != '':
                srcTarget.cxx = alt_cxx

            srcTarget.cppmap = {}
            srcTarget.cppmap['houdini_src'] = '-DFE_HOUDINI_MAJOR=' + str(houdini_major)
            srcTarget.cppmap['houdini_src'] += ' -DFE_HOUDINI_MINOR=' + str(houdini_minor)
            srcTarget.cppmap['houdini_src'] += ' -DFE_HOUDINI_LIBS=' + fe_houdini_libs
            srcTarget.cppmap["cxx11_abi"] = "-D_GLIBCXX_USE_CXX11_ABI=0"

            if houdini_major < 15:
                srcTarget.cppmap['std'] = forge.use_std("c++98")
            elif houdini_major < 18:
                srcTarget.cppmap['std'] = forge.use_std("c++11")
            elif houdini_major < 19:
                srcTarget.cppmap['std'] = forge.use_std("c++14")
            else:
                srcTarget.cppmap['std'] = forge.use_std("c++17")

            if need_specialization:
                srcTarget.cppmap['houdini_src'] += ' -DNEED_SPECIALIZATION_STORAGE'

            if clear_sse:
                srcTarget.cppmap['sse'] = ''

            srcTarget.cppmap['hcustom'] = houdini_cflags

            # NOTE later versions of openvdb require c++11

            openvdb_include = os.environ["FE_OPENVDB_INCLUDE"]

            if os.path.exists(houdini_source[1] + "openvdb/openvdb.h"):
                # houdini-supplied openvdb headers in 14+
                srcTarget.cppmap['houdini_src'] += ' -DFE_HOUDINI_USE_VDB=1'
            elif openvdb_include != '' and openvdb_include != 'auto' and houdini_major >= 15:
                srcTarget.cppmap['houdini_src'] += ' -DFE_HOUDINI_USE_VDB=1'

                srcTarget.includemap[ 'vdb' ] = os.path.join(openvdb_include, '..')
                default_openexr = os.environ["FE_OPENEXR_SOURCE"] + '/include'
                if os.path.exists(default_openexr):
                    srcTarget.includemap['openexr'] = default_openexr

            if variant == test_variant:
                test_include = copy.deepcopy(srcTarget.includemap)
                test_cpp = copy.deepcopy(srcTarget.cppmap)

                if alt_cxx != '':
                    test_cxx = alt_cxx

        # this overrides previous -W flags in codegen
        module.cppmap = {}
        module.cppmap['houdini'] = ' -DFE_HOUDINI_DEFAULT_URL_ROOT=\\\"' + os.environ["FE_HOUDINI_URL_ROOT"] + '\\\"'
        module.cppmap['houdini'] += ' -DFE_HOUDINI_DEFAULT_ICON_ROOT=\\\"' + os.environ["FE_HOUDINI_ICON_ROOT"] + '\\\"'

        if forge.fe_os == "FE_LINUX":
            module.cppmap['houdini'] += ' -Wno-deprecated -Wall -W -Wno-parentheses -Wno-sign-compare -Wno-reorder -Wno-uninitialized -Wunused -Wno-unused-parameter'

        module.cppmap['woe_conditionally'] = ""

        dll.cppmap = {}
        dll.linkmap = {}

        if clear_sse:
            dll.cppmap['sse'] = ''

        if forge.fe_os == "FE_LINUX":
            dll.linkmap["houdinilibs"] = " -Wl,-rpath='$ORIGIN/../../'"
            dll.linkmap["houdinilibs"] += " -shared"
#           dll.linkmap["houdinilibs"] += " -Wl,-rpath='" + houdini_source[2] + "'"

            # NOTE without explicit linkage, Mantra will whine
#           dll.linkmap["houdinilibs"] += " -L" + houdini_source[2];
#           dll.linkmap["houdinilibs"] += " -lHoudiniUI -lHoudiniOPZ -lHoudiniOP3 -lHoudiniOP2 -lHoudiniOP1 -lHoudiniSIM -lHoudiniGEO -lHoudiniPRM -lHoudiniUT -lHoudiniAPPS3"

            # missing from hcustom
            dll.linkmap["houdinilibs"] += " -L" + houdini_source[2];
            dll.linkmap["houdinilibs"] += " -lHoudiniPRM"
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            deplibs += [ "fexDataToolLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

        dll.linkmap["hcustom"] = houdini_ldflags

        if variant == test_variant:
            test_link = copy.deepcopy(dll.linkmap)

        forge.deps( [ dllname + "Lib" ], deplibs )

        # make sym links for houdini to look at
        # TODO should work on non-linux
        if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            houdiniDso = dllname + '.dll'
        else:
            houdiniDso = 'lib' + dllname + '.so'
        svgPath = os.path.join(forge.libPath, 'svg')
        houdiniPath = os.path.join(forge.libPath, 'houdini') + variant
        houdiniDsoPath = os.path.join(houdiniPath, 'dso')
        houdiniIconPath = os.path.join(houdiniPath, 'config', 'Icons')
        houdiniAppsPath = os.path.join(houdiniPath, 'config', 'Applications')
        houdiniUiPath = os.path.join(houdiniAppsPath, 'HoudiniBrush.ui')
        houdiniToolbarPath = os.path.join(houdiniPath, 'toolbar')
        houdiniPackagesPath = os.path.join(houdiniPath, 'packages')
        if os.path.isdir(svgPath) == 0:
            os.makedirs(svgPath, 0o755)
        if os.path.isdir(houdiniDsoPath) == 0:
            os.makedirs(houdiniDsoPath, 0o755)
        if os.path.isdir(houdiniIconPath) == 0:
            os.makedirs(houdiniIconPath, 0o755)
        if os.path.isdir(houdiniAppsPath) == 0:
            os.makedirs(houdiniAppsPath, 0o755)
        if os.path.isdir(houdiniToolbarPath) == 0:
            os.makedirs(houdiniToolbarPath, 0o755)
        if os.path.isdir(houdiniPackagesPath) == 0:
            os.makedirs(houdiniPackagesPath, 0o755)

        source = os.path.join('..', '..', houdiniDso)

        dest = os.path.join(houdiniDsoPath, houdiniDso)
        if os.path.lexists(dest) == 0:
            os.symlink(source, dest)

        for icon in ['FE.svg', 'FE_alpha.svg', 'FE_beta.svg', 'FreeElectron.svg']:
            source = os.path.join(forge.rootPath,'doc','image',icon)
            dest = os.path.join(svgPath, icon)
            if os.path.lexists(dest) == 0:
                shutil.copyfile(source, dest)

            source = os.path.join('..','..','..','svg',icon)
            dest = os.path.join(houdiniIconPath, icon)
            if os.path.lexists(dest) == 0:
                os.symlink(source, dest)

        open(houdiniUiPath, 'a').close()

        shelf = os.path.join(houdiniToolbarPath, 'FreeElectron.shelf')
        shutil.copyfile(module.modPath + '/FreeElectron.shelf', shelf)

        shelf = os.path.join(houdiniPackagesPath, 'FreeElectron.json')
        shutil.copyfile(module.modPath + '/FreeElectron.json', shelf)

        if houdini_major < 16:
            continue

        # don't build Houdini Engine on Windows for now
        if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            continue

        srcList = [ "houdiniEngine.pmh",
                    "SurfaceAccessibleHapi",
                    "SurfaceAccessorHapi",
                    "SurfaceTrianglesHapi",
                    "houdiniEngineDL" ]

        if variant != test_variant:
            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        dllname = "fexHoudiniEngineDL" + variant

        dll = module.DLL( dllname, srcList )

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)
            srcTarget.includemap = { 'houdini_src' : houdini_source[1] }

            if alt_cxx != '':
                srcTarget.cxx = alt_cxx

            srcTarget.cppmap = {}
            srcTarget.cppmap['houdini_src'] = '-DFE_HOUDINI_DSO="' + houdini_source[2] + '"'
            srcTarget.cppmap['houdini_src'] += ' -DFE_HOUDINI_BUILD="' + houdiniDsoPath + '"'
            srcTarget.cppmap["cxx11_abi"] = "-D_GLIBCXX_USE_CXX11_ABI=0"

        dll.cppmap = {}
        dll.linkmap = {}

        dll.linkmap["hcustom"] = houdini_ldflags

        if clear_sse:
            dll.cppmap['sse'] = ''

        if forge.fe_os == "FE_LINUX":
            dll.linkmap["houdinilibs"] = " -Wl,-rpath='$ORIGIN/../../'"
            dll.linkmap["houdinilibs"] += " -shared"
            dll.linkmap["houdinilibs"] += " -Wl,-rpath='" + houdini_source[2] + "'"
            dll.linkmap["houdinilibs"] += " -L" + houdini_source[2];
            dll.linkmap["houdinilibs"] += " -lHAPIL"

        with open(manifest, 'a') as outfile:
            suffix = ""
            if variant != "":
                suffix = "." + variant
            for form in [ "hip", "hipnc" ]:
                outfile.write('\tspManifest->catalog<String>(\n'+
                        '\t\t\t"SurfaceAccessibleI.SurfaceAccessibleHapi.fe.'+
                        form + suffix + '")=\n'+
                        '\t\t\t"fexHoudiniEngineDL' + variant + '";\n');

    if forge.fe_os == "FE_LINUX":
        test = module.Module('test')
        if test:
            test.includemap = test_include
            test.cppmap = copy.deepcopy(module.cppmap)
            if 'houdini_src' in test_cpp:
                test.cppmap['houdini'] += " " + test_cpp['houdini_src']
            if 'std' in test_cpp:
                test.cppmap['std'] = " " + test_cpp['std']

            test.cppmap['hcustom'] = houdini_cflags

            if test_cxx != '':
                test.cxx = test_cxx
                test.cppmap['sse'] = ''
            test.linkmap = copy.deepcopy(test_link)
            test_deplibs =  forge.corelibs + [
                        "fexHoudiniDL" + test_variant + "Lib" ]

            test.linkmap["hcustom"] = houdini_ldflags

            tests = [ 'xHoudiniContext' ]
            for t in tests:
                test_exe = test.Exe(t)

                test_exe.linkmap = { }
                test_exe.linkmap["houdinilibs"] = " -Wl,-rpath='$ORIGIN/../../'"
                test_exe.linkmap["houdinilibs"] += " -shared"
                test_exe.linkmap["houdinilibs"] += " -Wl,-rpath='" + houdini_source[2] + "'"
                test_exe.linkmap["houdinilibs"] += " -L" + houdini_source[2];
                test_exe.linkmap["houdinilibs"] += " -lHoudiniUI -lHoudiniOPZ -lHoudiniOP3 -lHoudiniOP2 -lHoudiniOP1 -lHoudiniSIM -lHoudiniGEO -lHoudiniPRM -lHoudiniUT -lHoudiniAPPS3"

                forge.deps([t + "Exe"], test_deplibs)

def auto(module):
    # hcustom -c clearly specifies '-MD'
    if os.getenv("FE_MS_RT") == "MT":
        return 'Windows is MD only'

    # TBB link issues (missing tbb_debug.lib)
    if forge.codegen == 'debug' and (forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64"):
        return 'TODO Windows debug'

    test_file = """
#include <SOP/SOP_API.h>
#include <SYS/SYS_Math.h>

int main(void)
{
    return(0);
}
    \n"""

    houdini_path = os.getenv('HT')
    if houdini_path:
        forge.includemap['houdini'] = os.path.join(houdini_path,'include')

        houdini_bin = houdini_path + "/../bin"
        houdini_cflags = run_hcustom(houdini_bin, "-c")
        houdini_ldflags = run_hcustom(houdini_bin, "-m")

        forge.cppmap['hcustom'] = houdini_cflags
        forge.linkmap["hcustom"] = houdini_ldflags

    result = forge.cctest(test_file)

    forge.includemap.pop('houdini', None)
    forge.cppmap.pop('hcustom', None)
    forge.linkmap.pop('hcustom', None)

    return result
