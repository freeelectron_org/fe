/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<LuaBind>("HandlerI.LuaBind.fe");
	pLibrary->add<LuaHandler>("LuaI.LuaHandler.fe");
	pLibrary->add<LuaIterate>("LuaI.LuaIterate.fe");
	pLibrary->add<SurfaceAccessibleLua>(
			"SurfaceAccessibleI.SurfaceAccessibleLua.fe.lua");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	spLibrary->registry()->prioritize("LuaI.LuaHandler.fe",1);
}

}
