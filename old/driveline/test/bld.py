import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs + [
        'fexSignalLib',
        'fexSolveDLLib',
        'fexMechanicsDLLib',
        'fexMoaLib',
        'fexDrivelineDLLib',
        'fexDataToolLib',
        'fexEvaluateDLLib',
        "fexThreadDLLib",
        "fexWindowLib",
        "fexViewerDLLib",
        'feMathLib' ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexMoaDLLib" ]

    tests = [ "xDriveline" ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    plugin_path = os.path.join(module.modPath, 'plugin')
