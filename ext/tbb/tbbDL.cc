/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <tbb/tbb.pmh>

#include "platform/dlCore.cc"

#define FE_TMX_DEBUG	FALSE
#define FE_TCD_DEBUG	FALSE
#define FE_TTH_DEBUG	FALSE

namespace fe
{
namespace ext
{

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexThreadDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	const I32 tbbVersion=tbb::TBB_runtime_interface_version();
//	feLogDirect("    tbbDL CreateLibrary() runtime version %d vs %d\n",
//			tbbVersion,TBB_INTERFACE_VERSION);
	if(tbbVersion<TBB_INTERFACE_VERSION)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLogDirect("    tbbDL CreateLibrary()"
				" can not use TBB compiled version %d > runtime verion %d\n",
				TBB_INTERFACE_VERSION,tbbVersion);
#endif
		return NULL;
	}

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<WorkTbb>("WorkForceI.WorkTbb.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	//* force this version instead of WorkGang and WorkOmp
	spLibrary->registry()->prioritize("WorkForceI.WorkTbb.fe",2);
}

}

void* tbbmutex_init(bool a_recursive)
{
	void* pMutex=NULL;
	if(a_recursive)
	{
		pMutex=new tbb::recursive_mutex();
	}
	else
	{
		pMutex=new tbb::spin_rw_mutex();
	}

#if FE_TMX_DEBUG
	feLogError("tbbmutex_init recursive %d -> %p\n",
			a_recursive,pMutex);
#endif

	return pMutex;
}

bool tbbmutex_lock(bool a_recursive,void* a_pMutex,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_TMX_DEBUG
	feLogError("tbbmutex_lock recursive %d mutex %p"
			" un %d try %d read %d\n",
			a_recursive,a_pMutex,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		tbb::recursive_mutex& rMutex=*(tbb::recursive_mutex*)(a_pMutex);

		if(a_un)
		{
			rMutex.unlock();
			return true;
		}
		else if(a_try)
		{
			return rMutex.try_lock();
		}

		rMutex.lock();
		return true;
	}

	tbb::spin_rw_mutex& rMutex=*(tbb::spin_rw_mutex*)(a_pMutex);

	if(a_un)
	{
		rMutex.unlock();
		return true;
	}
	else if(a_readOnly)
	{
		if(a_try)
		{
			return rMutex.try_lock_read();
		}
		rMutex.lock_read();
		return true;
	}
	else if(a_try)
	{
		return rMutex.try_lock();
	}
	rMutex.lock();
	return true;
}

void tbbmutex_finish(bool a_recursive,void* a_pMutex)
{
#if FE_TMX_DEBUG
	feLogError("tbbmutex_finish recursive %d mutex %p\n",
			a_recursive,a_pMutex);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		tbb::recursive_mutex* pMutex=(tbb::recursive_mutex*)(a_pMutex);
		delete pMutex;
		return;
	}

	tbb::spin_rw_mutex* pMutex=(tbb::spin_rw_mutex*)(a_pMutex);
	delete pMutex;
}

class tbbguard
{
	public:
		void*	m_pMutex;
		void*	m_pLock;
};

void* tbbguard_init(bool a_recursive,void* a_pMutex,bool a_readOnly)
{
	FEASSERT(a_pMutex);

	void* pLock=NULL;
	if(a_recursive)
	{
		tbb::recursive_mutex& rMutex=*(tbb::recursive_mutex*)(a_pMutex);
		pLock=new tbb::recursive_mutex::scoped_lock(rMutex);
	}
	else
	{
		tbb::spin_rw_mutex& rMutex=*(tbb::spin_rw_mutex*)(a_pMutex);
		pLock=new tbb::spin_rw_mutex::scoped_lock(rMutex,!a_readOnly);
	}

	tbbguard* pGuard=new tbbguard();
	pGuard->m_pMutex=a_pMutex;
	pGuard->m_pLock=pLock;

#if FE_TMX_DEBUG
	feLogError(
			"tbbguard_init recursive %d mutex %p read %d lock %p guard %p\n",
			a_recursive,a_pMutex,a_readOnly,pLock,pGuard);
#endif

	return pGuard;
}

bool tbbguard_lock(bool a_recursive,void* a_pGuard,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_TMX_DEBUG
	feLogError("tbbguard_lock recursive %d guard %p"
			" un %d try %d read %d\n",
			a_recursive,a_pGuard,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	tbbguard& rGuard=*(tbbguard*)(a_pGuard);

	if(a_recursive)
	{
		tbb::recursive_mutex& rMutex=
				*(tbb::recursive_mutex*)(rGuard.m_pMutex);
		tbb::recursive_mutex::scoped_lock& rLock=
				*(tbb::recursive_mutex::scoped_lock*)(rGuard.m_pLock);

		if(a_un)
		{
			rLock.release();
			return true;
		}
		else if(a_try)
		{
			return rLock.try_acquire(rMutex);
		}

		rLock.acquire(rMutex);
		return true;
	}

	tbb::spin_rw_mutex& rMutex=
			*(tbb::spin_rw_mutex*)(rGuard.m_pMutex);
	tbb::spin_rw_mutex::scoped_lock& rLock=
			*(tbb::spin_rw_mutex::scoped_lock*)(rGuard.m_pLock);

	if(a_un)
	{
		rLock.release();
		return true;
	}
	else if(a_try)
	{
		return rLock.try_acquire(rMutex);
	}

	rLock.acquire(rMutex);
	return true;
}

void tbbguard_finish(bool a_recursive,void* a_pGuard,bool a_readOnly)
{
#if FE_TMX_DEBUG
	feLogError("tbbguard_finish recursive %d guard %p read %d\n",
			a_recursive,a_pGuard,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	tbbguard* pGuard=(tbbguard*)(a_pGuard);

	if(a_recursive)
	{
		tbb::recursive_mutex::scoped_lock* pLock=
				(tbb::recursive_mutex::scoped_lock*)(pGuard->m_pLock);
		delete pLock;
		delete pGuard;
		return;
	}

	tbb::spin_rw_mutex::scoped_lock* pLock=
			(tbb::spin_rw_mutex::scoped_lock*)(pGuard->m_pLock);
	delete pLock;
	delete pGuard;
}

void* tbbcondition_init(void)
{
	void* pCondition=new std::condition_variable();

#if FE_TCD_DEBUG
	feLogError("tbbcondition_init -> %p\n",pCondition);
#endif

	return pCondition;
}

#if 1==0
native_handle_type native_handle() { return (native_handle_type) &my_cv; }

inline void condition_variable::wait( unique_lock<mutex>& lock )
{
    __TBB_ASSERT( lock.owns, NULL );
    lock.owns = false;
    if( pthread_cond_wait( &my_cv, lock.mutex()->native_handle() ) ) {
        lock.owns = true;
        throw_exception_v4( tbb::internal::eid_condvar_wait_failed );
    }
    // upon successful return, the mutex has been locked and is owned by the calling thread.
    lock.owns = true;
}
#endif

/// @internal
template<typename MUTEX>
void tbbcondition_lock_wait(std::condition_variable& a_rCondition,
	std::unique_lock<MUTEX>& a_rLock)
{
	std::condition_variable::native_handle_type nativeCondition=
			a_rCondition.native_handle();

//	a_rLock.owns = false;
	if(pthread_cond_wait(nativeCondition,a_rLock.mutex()->native_handle()))
	{
//		a_rLock.owns = true;
		feX("tbbcondition_lock_wait condition variable wait failed");
	}
//	a_rLock.owns = true;
}

/// @internal
bool tbbcondition_wait(void* a_pCondition,bool a_recursive,
	void* a_pGuard,bool a_readOnly)
{
#if FE_TCD_DEBUG
	feLogError("tbbcondition_wait %p guard %p\n",a_pCondition,a_pGuard);
#endif

	FEASSERT(a_pGuard);

	std::condition_variable& rCondition=
			*(std::condition_variable*)(a_pCondition);

	if(a_recursive)
	{
		std::unique_lock<tbb::recursive_mutex>& rGuard=
				*(std::unique_lock<tbb::recursive_mutex>*)(a_pGuard);

		tbbcondition_lock_wait(rCondition,rGuard);
		return true;
	}

//* TODO
//	std::unique_lock<tbb::spin_rw_mutex>& rGuard=
//			*(std::unique_lock<tbb::spin_rw_mutex>*)(a_pGuard);

	FEASSERT(FALSE);
//	tbbcondition_lock_wait(rCondition,rGuard);
	return true;
}

bool tbbcondition_notify(void* a_pCondition,bool a_all)
{
#if FE_TCD_DEBUG
	feLogError("tbbcondition_notify %p all %d\n",a_pCondition,a_all);
#endif

	FEASSERT(a_pCondition);

	std::condition_variable& rCondition=
			*(std::condition_variable*)(a_pCondition);

	if(a_all)
	{
		rCondition.notify_all();
	}
	else
	{
		rCondition.notify_one();
	}

	return true;
}

void tbbcondition_finish(void* a_pCondition)
{
#if FE_TCD_DEBUG
	feLogError("tbbcondition_finish %p\n",a_pCondition);
#endif

	FEASSERT(a_pCondition);

//	std::condition_variable* pCondition=
//			(std::condition_variable*)(a_pCondition);

//	delete pCondition;
}

extern "C"
{

FE_DL_EXPORT bool mutex_init(void)
{
#if FE_TCD_DEBUG
	feLogDirect("    tbbDL mutex_init()\n");
#endif

//	feLogDirect("    using tbb::mutex\n");

	const I32 tbbVersion=tbb::TBB_runtime_interface_version();
	if(tbbVersion<TBB_INTERFACE_VERSION)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLogDirect("    tbbDL mutex_init()"
				" can not use TBB compiled version %d > runtime verion %d\n",
				TBB_INTERFACE_VERSION,tbbVersion);
#endif
		return false;
	}

	Mutex::replaceInitFunction(tbbmutex_init);
	Mutex::replaceLockFunction(tbbmutex_lock);
	Mutex::replaceFinishFunction(tbbmutex_finish);

	Mutex::replaceGuardInitFunction(tbbguard_init);
	Mutex::replaceGuardLockFunction(tbbguard_lock);
	Mutex::replaceGuardFinishFunction(tbbguard_finish);

	Mutex::replaceConditionInitFunction(tbbcondition_init);
	Mutex::replaceConditionWaitFunction(tbbcondition_wait);
	Mutex::replaceConditionNotifyFunction(tbbcondition_notify);
	Mutex::replaceConditionFinishFunction(tbbcondition_finish);

	return true;
}

}

///////////////////////////////////////////////////////////////////////////////

void* tbbthread_default_init(void)
{
	FEASSERT(FALSE);
	void* pThread=new tbb::empty_task();

#if FE_TTH_DEBUG
	feLogError("tbbthread_default_init -> %p\n",pThread);
#endif

	return pThread;
}

void* tbbthread_init(void* a_pFunctor)
{
	FEASSERT(a_pFunctor);

//	Thread::Functor& rFunctor=*(Thread::Functor*)(a_pFunctor);

//* TODO
	FEASSERT(FALSE);
	void* pThread=NULL;

#if FE_TTH_DEBUG
	feLogError("tbbthread_init -> %p\n",pThread);
#endif

	return pThread;
}


void tbbthread_interrupt(void* a_pThread)
{
#if FE_TTH_DEBUG
	feLogError("tbbthread_interrupt thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

//	tbb::task& rThread=*(tbb::task*)(a_pThread);

//* TODO
//	rThread.interrupt();
}

void tbbthread_join(void* a_pThread)
{
#if FE_TTH_DEBUG
	feLogError("tbbthread_join thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

//	tbb::task& rThread=*(tbb::task*)(a_pThread);

//* TODO
	FEASSERT(FALSE);
//	rThread.join();
}

bool tbbthread_joinable(void* a_pThread)
{
#if FE_TTH_DEBUG
	feLogError("tbbthread_joinable thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

//	tbb::task& rThread=*(tbb::task*)(a_pThread);

//* TODO
	FEASSERT(FALSE);
//	rThread.joinable();
	return FALSE;
}

void tbbthread_finish(void* a_pThread)
{
#if FE_TTH_DEBUG
	feLogError("tbbthread_finish thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	FEASSERT(FALSE);
	tbb::task* pThread=(tbb::task*)(a_pThread);
	delete pThread;
}

void tbbthread_interruption(void)
{
#if FE_TTH_DEBUG
	feLogError("tbbthread_interruption\n");
#endif

	tbb::this_tbb_thread::yield();
}

int tbbthread_concurrency(void)
{
#if FE_TTH_DEBUG
	feLogError("tbbthread_concurrency\n");
#endif

	return tbb::tbb_thread::hardware_concurrency();
}

/// @internal
class tbbgroup_functor_task: public tbb::task
{
	public:
					tbbgroup_functor_task(Thread::Functor* a_pFunctor):
						m_pFunctor(a_pFunctor)
					{
#if FE_TTH_DEBUG
						feLogDirect("tbbgroup_functor_task "
								"construct %p functor %p ref %d\n",
								this,m_pFunctor,ref_count());
#endif
					}
virtual				~tbbgroup_functor_task(void)
					{
#if FE_TTH_DEBUG
						feLogDirect("tbbgroup_functor_task "
								"destruct %p functor %p ref %d\n",
								this,m_pFunctor,ref_count());
#endif
					}
	private:
virtual	tbb::task*	execute(void)
					{
#if FE_TTH_DEBUG
						feLogDirect("tbbgroup_functor_task "
								"execute %p functor %p ref %d\n",
								this,m_pFunctor,ref_count());
#endif

						(*m_pFunctor)();

#if FE_TTH_DEBUG
						feLogDirect("tbbgroup_functor_task "
								"executed %p functor %p ref %d\n",
								this,m_pFunctor,ref_count());
#endif
						return NULL;
					}

	Thread::Functor*	m_pFunctor;
};

void* tbbgroup_init(void)
{
#if FALSE
	//* TODO check for leak
	tbb::task_group_context* pContext=new tbb::task_group_context();

	//* root thread
	tbb::task* pThreadGroup=new(tbb::task::allocate_root(*pContext))
			tbb::empty_task();
#else
	//* root thread
	tbb::task* pThreadGroup=new(tbb::task::allocate_root())
			tbb::empty_task();
#endif

	FEASSERT(pThreadGroup);

	pThreadGroup->set_ref_count(1);

#if FE_TTH_DEBUG
	feLogError("tbbgroup_init -> %p\n",pThreadGroup);
#endif

	return pThreadGroup;
}

void* tbbgroup_create(void* a_pThreadGroup,void* a_pFunctor)
{
#if FE_TTH_DEBUG
	feLogError("tbbgroup_create group %p\n",
			a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);
	FEASSERT(a_pFunctor);

	tbb::task& rThreadGroup=*(tbb::task*)(a_pThreadGroup);
	Thread::Functor* pFunctor=(Thread::Functor*)(a_pFunctor);

	tbb::task* pThread=new(rThreadGroup.allocate_child())
			tbbgroup_functor_task(pFunctor);
	FEASSERT(pThread);

	rThreadGroup.increment_ref_count();
	tbb::task::spawn(*pThread);

#if FE_TTH_DEBUG
	feLogError("tbbgroup_create group %p functor %p thread %p\n",
			a_pThreadGroup,pFunctor,pThread);
#endif

	return pThread;
}

void tbbgroup_join_all(void* a_pThreadGroup)
{
#if FE_TTH_DEBUG
	feLogError("tbbgroup_join_all group %p\n",a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);

	tbb::task* pThreadGroup=(tbb::task*)(a_pThreadGroup);

	pThreadGroup->wait_for_all();

#if FE_TTH_DEBUG
	feLogError("tbbgroup_join_all group %p complete\n",a_pThreadGroup);
#endif
}

void tbbgroup_finish(void* a_pThreadGroup)
{
#if FE_TTH_DEBUG
	feLogError("tbbgroup_finish %p\n",a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);

	tbb::task* pThreadGroup=(tbb::task*)(a_pThreadGroup);

#if FE_TTH_DEBUG
	feLogError("tbbgroup_finish %p ref %d\n",
			a_pThreadGroup,pThreadGroup->ref_count());
#endif

	tbb::task::destroy(*pThreadGroup);

#if FE_TTH_DEBUG
	feLogError("tbbgroup_finish %p complete\n",a_pThreadGroup);
#endif
}

extern "C"
{

FE_DL_EXPORT bool thread_init(void)
{
#if FE_TTH_DEBUG
	feLogDirect("    tbbDL thread_init()\n");
#endif

	const I32 tbbVersion=tbb::TBB_runtime_interface_version();
	if(tbbVersion<TBB_INTERFACE_VERSION)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLogDirect("    tbbDL thread_init()"
				" can not use TBB compiled version %d > runtime verion %d\n",
				TBB_INTERFACE_VERSION,tbbVersion);
#endif
		return false;
	}

	Thread::replaceDefaultInitFunction(tbbthread_default_init);
	Thread::replaceInitFunction(tbbthread_init);
	Thread::replaceInterruptFunction(tbbthread_interrupt);
	Thread::replaceJoinFunction(tbbthread_join);
	Thread::replaceJoinableFunction(tbbthread_joinable);
	Thread::replaceFinishFunction(tbbthread_finish);
	Thread::replaceInterruptionFunction(tbbthread_interruption);
	Thread::replaceConcurrencyFunction(tbbthread_concurrency);

	Thread::replaceGroupInitFunction(tbbgroup_init);
	Thread::replaceGroupCreateFunction(tbbgroup_create);
	Thread::replaceGroupJoinAllFunction(tbbgroup_join_all);
	Thread::replaceGroupFinishFunction(tbbgroup_finish);

	return true;
}

}

} /* namespace ext */
} /* namespace fe */
