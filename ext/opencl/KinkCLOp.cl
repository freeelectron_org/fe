/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_vertStart,__global int* a_vertCount,__global int* a_perm,
	float a_amplitudeRoot,float a_amplitudeTip,float a_amplitudeGamma,
	float a_amplitudeRandom,float4 a_amplitudeScale,
	float a_frequencyRoot,float a_frequencyTip,float a_frequencyGamma,
	float4 a_frequencyScale,__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];
	if(subCount<1)
	{
//		if(primitiveIndex<8)
//		{
//			a_debug[primitiveIndex].x=-1;
//			a_debug[primitiveIndex].w=primitiveIndex;
//			a_debug[primitiveIndex+8].x=-1;
//			a_debug[primitiveIndex+8].w=primitiveIndex;
//		}

		return;
	}

	float amplitudeStart=a_amplitudeRoot;
//	if(a_amplitudeRandom>0)
//	{
//		amplitudeStart+=a_amplitudeRandom*
//				whiteNoise2d(13*primitiveIndex,0);
//	}
	float amplitudeAdd=a_amplitudeTip-a_amplitudeRoot;

	float4 rootPoint=a_position[subStart];
	float4 previousPoint=rootPoint;

	float disruption=1e6;	//* TODO param

	float perlinRoot=disruption*FastNoise_GetPerlin3D(
			rootPoint.x,rootPoint.y,rootPoint.z,a_perm);

	float perlinScalar=sqrt(2.0);

	float4 facing=(float4)(1,0,0,0);

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		int pointIndex=subStart+subIndex;
		float4 input=a_position[pointIndex];

		float along=subIndex/(float)(subCount-1);

		float amplitudeAlong=amplitudeStart+
				amplitudeAdd*powr(along,a_amplitudeGamma);

		float rescale=perlinScalar*amplitudeAlong;

		float frequencyAlong=a_frequencyRoot+
				(a_frequencyTip-a_frequencyRoot)*powr(along,a_frequencyGamma);

		//* TODO shouldn't this be cumulative?
		float offsetAlong=along*frequencyAlong;

		float4 perlinOutput;

		float2 perlinInput;
		perlinInput.x=perlinRoot;

		perlinInput.y=offsetAlong*a_frequencyScale.x;
		perlinOutput.x=
				FastNoise_GetPerlin2D(perlinInput.x,perlinInput.y,a_perm)*
				rescale*a_amplitudeScale.x;

		perlinInput.y=offsetAlong*a_frequencyScale.y+1e3;
		perlinOutput.y=
				FastNoise_GetPerlin2D(perlinInput.x,perlinInput.y,a_perm)*
				rescale*a_amplitudeScale.y;

		perlinInput.y=offsetAlong*a_frequencyScale.z+2e3;
		perlinOutput.z=
				FastNoise_GetPerlin2D(perlinInput.x,perlinInput.y,a_perm)*
				rescale*a_amplitudeScale.z;

		float4 tangent=normalize(input-previousPoint);
		previousPoint=input;

		//* tangentX, facingY (curve normal)

		float16 xformSelf=makeFrameNormalY(input,tangent,facing);

		float4 delta=rotateVector(xformSelf,perlinOutput);

		float4 output=input+delta;

		a_position[pointIndex]=output;

//		if(primitiveIndex<8 && subIndex==subCount-1)
//		{
//			a_debug[primitiveIndex]=input;
//			a_debug[primitiveIndex].w=primitiveIndex;
//			a_debug[primitiveIndex+8]=output;
//			a_debug[primitiveIndex+8].w=primitiveIndex;
//		}
	}
}
