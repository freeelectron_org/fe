import os
import sys
import string
forge = sys.modules["forge"]

# enet on Windows:
# install vcpkg in your FE_WINDOWS_LOCAL
# see https://github.com/Microsoft/vcpkg/
# vcpkg.exe install enet:x86-windows-static-md
# vcpkg.exe install enet:x64-windows-static-md

def prerequisites():
    return [ "network" ]

def setup(module):
    srclist = [ "enetComs.pmh",
                "EnetCatalog",
                "EnetConnection",
                "enetDL",
                ]

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexNetworkDLLib" ]

    dll = module.DLL( "fexEnetDL", srclist)

    dll.linkmap = {}

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["enet_libs"] = "-lenet"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs += [ "feDataLib" ]

        if forge.codegen == 'debug':
            dll.linkmap["enet_libs"] = "enet.lib"
        else:
            dll.linkmap["enet_libs"] = "enet.lib"

    forge.deps( ["fexEnetDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexEnetDL",    None, None) ]

    module.Module('test')

def auto(module):
    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            forge.linkmap["enet_libs"] = "enet.lib"
        else:
            forge.linkmap["enet_libs"] = "enet.lib"
    else:
        forge.linkmap["enet_libs"] = "-lenet"

    test_file = """
#include <enet/enet.h>

int main(void)
{
    return(0);
}
    \n"""

    result = forge.cctest(test_file)

    forge.linkmap.pop('enet_libs', None)

    return result
