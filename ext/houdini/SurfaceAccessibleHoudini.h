/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_SurfaceAccessibleHoudini_h__
#define __houdini_SurfaceAccessibleHoudini_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Houdini Surface Binding

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleHoudini:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleHoudini>
{
	public:
								SurfaceAccessibleHoudini(void);
virtual							~SurfaceAccessibleHoudini(void);

								//* as SurfaceAccessibleI

								using SurfaceAccessibleBase::bind;

virtual	void					bind(Instance a_instance)
								{	setGdp(a_instance.cast<GU_Detail*>()); }

virtual	BWORD					isBound(void)	{ return m_pGdp!=NULL; }

virtual	void					groupNames(Array<String>& a_rNameArray,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

//virtual	void					copy(sp<SurfaceAccessibleI>
//										a_spSurfaceAccessible);

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);

								using SurfaceAccessibleBase::discard;

virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name);

#ifdef FE_HOUDINI_USE_GA
virtual	sp<SpannedRange>		atomize(AtomicChange a_atomicChange,
										String a_group,U32 a_desiredCount);

virtual void					preparePaging(AtomicChange a_atomicChange,
										String a_group);
virtual I32						pointPage(U32 a_pointIndex) const;
virtual I32						primitivePage(U32 a_primitiveIndex) const;
#endif

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

								using SurfaceAccessibleBase::subSurface;

virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group,
										SurfaceI::Restrictions a_restrictions);

								using SurfaceAccessibleBase::append;

virtual	void					append(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										const SpatialTransform* a_pTransform);

virtual	void					instance(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										const Array<SpatialTransform>&
										a_rTransformArray);

virtual	sp<MultiGroup>			generateGroup(
										SurfaceAccessibleI::Element a_element,
										String a_groupString);

	class MultiGroupHoudini:
		public MultiGroup,
		public CastableAs<MultiGroupHoudini>
	{
		public:
							MultiGroupHoudini(void):
								m_pSolitaryGroup(NULL)						{}
	virtual					~MultiGroupHoudini(void)						{}

	virtual	void			add(I32 a_index);
	virtual	void			remove(I32 a_index);

				void		setSolitaryGroup(GA_ElementGroup* a_pSolitaryGroup)
							{	m_pSolitaryGroup=a_pSolitaryGroup; }

		private:
				GA_ElementGroup*	m_pSolitaryGroup;
	};
								//* Houdini specific
		void					setGdp(const GU_Detail* a_pGdp);
		void					setGdpChangeable(GU_Detail* a_pGdp);
const	GU_Detail*				gdp(void)			{ return m_pGdp; }
		GU_Detail*				gdpChangeable(void)	{ return m_pGdpChangeable; }

	protected:

virtual	void					reset(void);

#ifdef FE_HOUDINI_USE_GA
static	GA_AttributeOwner		attributeOwner(
										SurfaceAccessibleI::Element a_element);
#else
#endif

	private:
//		sp<SpannedRange>		atomizeCrossover(AtomicChange a_atomicChange,
//										String a_group);

		void					harden(sp<SurfaceAccessorHoudini>& rspAccessor);

		sp<SurfaceAccessorHoudini> accessor(void);
		sp<SurfaceAccessorI>	accessorOf(sp<SurfaceAccessorI> a_spAccessor);

		void					initPrimitiveMap(void);
		I32						lookupPrimitive(I32 a_surfaceIndex) const;

const	GU_Detail*							m_pGdp;
		GU_Detail*							m_pGdpChangeable;
		BWORD								m_selfGdp;

		sp<SurfaceI>						m_spSurfaceI;
		std::map<I32,I32>					m_primitiveMap;
		BWORD								m_hasDiscretePolygons;
		Array<I32>							m_pageOfPoint;
		Array<I32>							m_pageOfPrimitive;

#if FE_HOUDINI_HARDENING
	public:
		sp<SurfaceAccessorHoudini::Hardening>			hardening(String a_key);
	private:
		std::map<String, hp<SurfaceAccessorHoudini::Hardening> > m_hardeningMap;
#endif
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_SurfaceAccessibleHoudini_h__ */
