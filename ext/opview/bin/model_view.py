#! /usr/bin/python3

import sys
import re
import time

if len(sys.argv) < 2:
    print("Usage: " + sys.argv[0] + " [-frame FRAME] [-frames START END] [-count FRAMES] [-options 'OPTION=\"STRING\"...'] FILES...")
    exit()

filenames = []
frameMin = 0
frameMax = 0
frameCount = -1
live = 0
hydra = 0
zup = False
options = ""
argCount = len(sys.argv)

surfaceOptions = { 0:{}, 1:{}, 2:{}, 3:{} }
surfaceCount = 0

argIndex = 1
while True:
    arg = sys.argv[argIndex]
    argIndex += 1

    if arg == "-frame":
        frame = int(sys.argv[argIndex])
        argIndex += 1

        frameMin = frame
        frameMax = frame
    if arg == "-count":
        frameCount = int(sys.argv[argIndex])
        argIndex += 1
    elif arg == "-frames":
        arg = sys.argv[argIndex]
        argIndex += 1

        if re.compile(r'[0-9]+-[0-9]+').match(arg):
            (low, high) = arg.split("-")
            frameMin = int(low)
            frameMax = int(high)
        else:
            frameMin = int(arg)
            frameMax = int(sys.argv[argIndex])
            argIndex += 1
    elif arg == "-options":
        options = sys.argv[argIndex]
        argIndex += 1
    elif arg == "-live":
        live = 1
    elif arg == "-hydra":
        hydra = 1
    elif arg == "-zup":
        zup = True
    else:
        filenames += [ arg ]

        surfaceOptions[surfaceCount] = options
        surfaceCount += 1

    if argIndex >= argCount:
        break;

import pyfe.context

fe = pyfe.context.Context()

if hydra:
    fe.master()["hint:DrawChain"] = "*.DrawHydra"

surfaceViewerOp = fe.create("SurfaceViewerOp")
#surfaceNormalOp = fe.create("SurfaceNormalOp")

if surfaceViewerOp == None:
    print("could not create SurfaceViewerOp")
    exit()

if zup:
    surfaceViewerOp["vertical"] = [ 0.0, 0.0, 1.0 ]

surfaces = { 0:{}, 1:{}, 2:{}, 3:{} }

showing = False

frame = frameMin
frameInc = (frameMin != frameMax)
frameDir = 1
maxLoaded = frame
lastFrame = frame - 1
skipCount = 0

importer = {}

if live:
    surfaceViewerOp["live"] = True

    for surfaceIndex in range(4):
        if len(filenames) > surfaceIndex and filenames[surfaceIndex] != "":
            filename = filenames[surfaceIndex]
            importer[surfaceIndex] = fe.create("ImportOp",filename)
            importer[surfaceIndex]["loadfile"] = filename
            importer[surfaceIndex]["cacheFrames"] = False
            importer[surfaceIndex]["replaceOutput"] = True
        else:
            importer[surfaceIndex] = None

while frameCount:
    fe.setFrame(frame)
    current = {}

    if live:
        for surfaceIndex in range(4):
            if importer[surfaceIndex]:
                importer[surfaceIndex]["frame"] = float(frame)
                current[surfaceIndex] = importer[surfaceIndex]()
            else:
                current[surfaceIndex] = None
    else:
        # cache frames as used
        for surfaceIndex in range(4):
            if len(filenames) > surfaceIndex and filenames[surfaceIndex] != "":
                if not frame in surfaces[surfaceIndex]:
                    filename = filenames[surfaceIndex].replace("$F", str(frame))
                    surfaceName = filename + ":" + str(frame)
                    surfaces[surfaceIndex][frame] = fe.load(filename, surfaceName, surfaceOptions[surfaceIndex])

        # use None whenever frame is missing
        for surfaceIndex in range(4):
            if frame in surfaces[surfaceIndex]:
                current[surfaceIndex] = surfaces[surfaceIndex][frame]
#               current[surfaceIndex] = surfaceNormalOp(surfaces[surfaceIndex][frame])
            else:
                current[surfaceIndex] = None

    for surfaceIndex in range(4):
        if current[surfaceIndex]:
            showing = True

    if (live or frame != lastFrame) and (current[0] or current[1] or current[2] or current[3]):
        surfaceViewerOp(current[0], current[1], current[2], current[3])
        lastFrame = frame
    else:
        skipCount = skipCount + 1
        time.sleep(0.01)

    frameStart = float(surfaceViewerOp["startFrame"])
    frameEnd = float(surfaceViewerOp["endFrame"])

    # TODO shouldn't make tracking sluggish
#   if skipCount > 20:  # 200ms
    if skipCount > 0:
        skipCount = 0
        lastFrame = frameStart - 1

    if int(surfaceViewerOp["finished"]):
        exit()

    if maxLoaded < frame:
        maxLoaded = frame

    if len(surfaceViewerOp["keys"]):
        for key in surfaceViewerOp["keys"]:
            if key == "\x13":   # cursor left as DC3
                if frameInc:
                    frameInc = -1
                    frameDir = -1
                else:
                    frame = frame - 1
            if key == "\x14":   # cursor right as DC4
                if frameInc:
                    frameInc = 1
                    frameDir = 1
                else:
                    frame = frame + 1
            if key == " ":
                frameInc = (not frameInc) * frameDir

            # WARNING not really thread safe
            surfaceViewerOp["keys"] = ""

    stepping = max(1, int(surfaceViewerOp["stepping"]))
    zigzag = int(surfaceViewerOp["zigzag"])

    frame = frame + frameInc * stepping

    if (frame > frameMax or not frameInc) and not showing:
        print("nothing viewable")
        break

    if frame > frameEnd and (frameEnd != maxLoaded or frameEnd == frameMax):
        if zigzag:
            frame = frameEnd
            frameInc = -1
        else:
            frame = frameStart
    if frame < frameStart:
        if zigzag:
            frame = frameStart
            frameInc = 1
        else:
            frame = frameEnd

    if frameCount>0:
        frameCount = frameCount - 1
