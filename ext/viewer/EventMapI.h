/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_EventMapI_h__
#define __viewer_EventMapI_h__

#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{


/**	Interface for binding a WindowEvent to an event mapped Layout. */
class FE_DL_EXPORT EventMapI : virtual public Component
{
	public:
virtual	void	bind(	WindowEvent::Source source,
						WindowEvent::Item item,
						WindowEvent::State state,
						sp<Layout> resultSignalLayout)		= 0;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_EventMapI_h__ */
