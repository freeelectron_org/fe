import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "solve" ]

def setup(module):
    # the plugin
    srcList = [ "mechanics.pmh",
                #"SPHHandler",
                #"LinearSpringHandler",
                #"LinearSpringDeltaHandler",
                #"LinearBendHandler",
                #"LinearStrandHandler",
                #"StrandTwistHandler",
                #"LocatedFrameHandler",
                #"PhysicalFrameHandler",
                #"FrameConstraintHandler",
                #"MechanicsHandler",
                #"ContactHandler",
                #"EulerFrameHandler",
                #"BarycentricPointHandler",
                #"RelocateHandler",
                #"PointThrustHandler",
                #"SphereSphereCollisionHandler",
                "LinearSpringForce",
                #"LinearSpringForce1D",
                #"LinearSpringForce2D",
                #"Differential",
                "LinearBendForce",
                "LinearStrandForce",
                #"LinearStrandForce2D",
                #"RadialFakeForce",
                "mechanicsLib"
                ]

    #srcList.append("DrawMechanics")
    #srcList.append("NaiveTireHandler")
    srcList += [ "mechanicsDL" ]

    forge.objlists['mechanics'] = module.SrcToObj(srcList)


    dll = module.DLL( "fexMechanicsDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "feMathLib",
                "fexSolveDLLib",
                "fexDataToolDLLib" ]

    #deplibs.append("fexViewerDLLib")

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexThreadDLLib" ]

#   dll.linkmap = { "gfxlibs": forge.gfxlibs }

    forge.deps( ["fexMechanicsDLLib"], deplibs )

    module.Module('test')
