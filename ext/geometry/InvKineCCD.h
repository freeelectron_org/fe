/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_InvKineCCD_h__
#define __geometry_InvKineCCD_h__

namespace fe
{
namespace ext
{
/**************************************************************************//**
	@brief Inverse Kinematic by Cyclic Coordinate Descent

	@ingroup geometry

*//***************************************************************************/
class FE_DL_EXPORT InvKineCCD
{
	public:

static	void	solve(Array<SpatialVector>& a_location,
						U32 a_index,U32 a_count,const SpatialVector& a_target);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_InvKineCCD_h__ */
