/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_tireAS_h__
#define __tire_tireAS_h__

namespace fe
{
namespace ext
{

#define AsConstruct(AS)														\
		AS(void) {}															\
		AS(::fe::Record &r){ if(r.isValid()) { bind(r.layout()->scope()); }}\
		AS(::fe::sp<::fe::Scope> a_spScope){ bind(a_spScope); }


/// @brief Tire Model
class AsTireModel
	: public AsNamed, public Initialize<AsTireModel>
{
	public:
		AsConstruct(AsTireModel);
		void initialize(void)
		{
			add(component,				FE_USE("tire:component"));
		}
		Accessor<String>		component;
};


/// @brief Tire
class AsTire
	: public AsNamed, public Initialize<AsTire>
{
	public:
		AsConstruct(AsTire);
		void initialize(void)
		{
			add(model,				FE_USE("tire:model"));
		}
		Accessor<String>			model;
};

/// @brief Run Time Single Contact Tire Model
class AsTireLive
	: public AsTire, public Initialize<AsTireLive>
{
	public:
		AsConstruct(AsTireLive);
		void initialize(void)
		{
			add(force,				FE_USE("sim:force"));
			add(moment,				FE_USE("tire:moment"));
			add(transform,			FE_USE("spc:transform"));
			add(radius,				FE_USE("tire:radius"));
			add(width,				FE_USE("tire:width"));
			add(velocity,			FE_USE("spc:velocity"));
			add(angular_velocity,	FE_USE("tire:angular_velocity"));
			add(contact_radius,		FE_USE("tire:contact_radius"));
			add(inclination,		FE_USE("tire:inclination"));
			add(patch,				FE_USE("tire:patch"));
			add(spTireI,			FE_USE("tire:spTireI"));

			add(inv_transform,		FE_USE("spc:inv_transform"));
			add(inv_patch,			FE_USE("tire:inv_patch"));
		}

		Accessor<t_moa_v3>			force;
		Accessor<t_moa_v3>			moment;
		Accessor<t_moa_real>		radius;
		Accessor<t_moa_real>		width;
		Accessor<t_moa_xform>		transform;
		Accessor<t_moa_v3>			velocity;
		Accessor<t_moa_v3>			angular_velocity;
		Accessor<t_moa_real>		contact_radius;
		Accessor<t_moa_real>		inclination;
		Accessor<t_moa_xform>		patch;
		Accessor< sp<TireI> >	spTireI;

		Accessor<t_moa_xform>		inv_transform;
		Accessor<t_moa_xform>		inv_patch;
};

/// @brief Rig to Tire mount
class AsMount
	: public AccessorSet, public Initialize<AsMount>
{
	public:
		AsConstruct(AsMount);
		void initialize(void)
		{
			add(tire,					FE_USE("mount:tire"));
			add(rig,					FE_USE("mount:rig"));
			add(transform_inv,			FE_USE("spc:transform_inv"));
		}

		Accessor<String>			tire;
		Accessor<String>			rig;
		Accessor<t_moa_xform>			transform_inv;
};

/// @brief
class AsParticlesMount
	: public AccessorSet, public Initialize<AsParticlesMount>
{
	public:
		AsConstruct(AsParticlesMount);
		void initialize(void)
		{
			add(tire,					FE_USE("mount:tire"));
			add(p0,						FE_USE("mount:p0"));
			add(p1,						FE_USE("mount:p1"));
			add(p2,						FE_USE("mount:p2"));
			add(transform_inv,			FE_USE("spc:transform_inv"));
			add(location,				FE_USE("mount:at"));
			add(inclination,			FE_USE("tire:inclination"));
		}

		Accessor<String>			tire;
		Accessor<WeakRecord>		p0;
		Accessor<WeakRecord>		p1;
		Accessor<WeakRecord>		p2;
		Accessor<t_moa_xform>			transform_inv;
		Accessor<t_moa_v3>				location;
		Accessor<t_moa_real>			inclination;
};

/// @brief
class AsRigParticlesMount
	: public AsParticlesMount, public Initialize<AsRigParticlesMount>
{
	public:
		AsConstruct(AsRigParticlesMount);
		void initialize(void)
		{
			add(rig,					FE_USE("mount:rig"));
		}

		Accessor<String>			rig;
};

/// @brief Ground
class AsGround
	: public AccessorSet, public Initialize<AsGround>
{
	public:
		AsConstruct(AsGround);
		void initialize(void)
		{
			add(height,				FE_USE("ground:height"));
		}

		Accessor<t_moa_real>		height;
};


/// @brief Tire Test Rig
class AsRig
	: public AsNamed, public Initialize<AsRig>
{
	public:
		AsConstruct(AsRig);
		void initialize(void)
		{
			add(inclination,			FE_USE("rig:inclination"));
			add(location,				FE_USE("rig:location"));
			add(load,					FE_USE("rig:load"));
			add(pitch,					FE_USE("rig:pitch"));

			add(alpha,					FE_USE("rig:alpha"));
			add(velocity,				FE_USE("rig:velocity"));
			add(kappa,					FE_USE("rig:kappa"));
			add(free_wheel,				FE_USE("rig:free_wheel"));
			add(incline_floor,			FE_USE("rig:incline_floor"));
			add(solve_extract,			FE_USE("rig:solve_extract"));

			add(structure,				FE_USE("rig:structure"));
			add(root,					FE_USE("rig:root"));

			add(floor,					FE_USE("rig:floor"));
		}

		Accessor<t_moa_real>		inclination;
		Accessor<t_moa_v3>			location;
		Accessor<t_moa_real>		load;
		Accessor<t_moa_real>		pitch;

		Accessor<t_moa_real>		alpha;
		Accessor<t_moa_real>		velocity;
		Accessor<t_moa_real>		kappa;
		Accessor<bool>				free_wheel;
		Accessor<bool>				incline_floor;
		Accessor<bool>				solve_extract;

		Accessor< sp<RecordGroup> > structure;
		Accessor< Record >				root;

		Accessor< WeakRecord >			floor;
};

/// @brief Tire Test Rig Live
class AsRigLive
	: public AsRig, public Initialize<AsRigLive>
{
	public:
		AsConstruct(AsRigLive);
		void initialize(void)
		{
			add(alpha_frequency,		FE_USE("rig:alphaFrequency"));
			add(pnt_velocity,			FE_USE("rig:pnt_velocity"));
			add(force_accum,			FE_USE("sim:force_accum"));
			add(moment_accum,			FE_USE("sim:moment_accum"));
			add(force,					FE_USE("sim:force"));
			add(moment,					FE_USE("sim:moment"));
		}

		Accessor<t_moa_real>		alpha_frequency;
		Accessor<t_moa_v3>			pnt_velocity;
		Accessor<t_accum_v3>		force_accum;
		Accessor<t_accum_v3>		moment_accum;
		Accessor<t_moa_v3>			force;
		Accessor<t_moa_v3>			moment;
};

/// @brief
class AsSoft
	: public AsNamed, public Initialize<AsSoft>
{
	public:
		AsConstruct(AsSoft);
		void initialize(void)
		{
			add(solve_extract,			FE_USE("soft:solve_extract"));
			add(structure,				FE_USE("soft:structure"));
		}

		Accessor<bool>				solve_extract;

		Accessor< sp<RecordGroup> > structure;
};

#if 1
class AsLocatorRigBinding
	: public AccessorSet, public Initialize<AsLocatorRigBinding>
{
	public:
		AsConstruct(AsLocatorRigBinding);
		void initialize(void)
		{
			add(rig,			FE_USE("moa:rig"));
			add(locator,		FE_USE("moa:locator"));
		}

		Accessor<WeakRecord>			rig;
		Accessor<WeakRecord>			locator;
};

class AsLocatorFromPoints
	: public AsLocator, public Initialize<AsLocatorFromPoints>
{
	public:
		AsConstruct(AsLocatorFromPoints);
		void initialize(void)
		{
			add(p0,						FE_USE("mount:p0"));
			add(p1,						FE_USE("mount:p1"));
			add(p2,						FE_USE("mount:p2"));
		}
		Accessor<WeakRecord>		p0;
		Accessor<WeakRecord>		p1;
		Accessor<WeakRecord>		p2;
};

class AsForceLocator
	: public AsLocator, public Initialize<AsForceLocator>
{
	public:
		AsConstruct(AsForceLocator);
		void initialize(void)
		{
			add(force,					FE_USE("sim:force"));
			add(moment,					FE_USE("sim:moment"));
			add(velocity,				FE_USE("spc:velocity"));
		}
		Accessor<t_moa_v3>			force;
		Accessor<t_moa_v3>			moment;
		Accessor<t_moa_v3>			velocity;
};


class AsLocatorFromEuler
	: public AsLocator, public Initialize<AsLocatorFromEuler>
{
	public:
		AsConstruct(AsLocatorFromEuler);
		void initialize(void)
		{
			add(location,				FE_USE("spc:at"));
			add(zyx,					FE_USE("frame:zyx"));
		}
		Accessor<t_moa_v3>					location;
		Accessor<t_moa_v3>					zyx;
};


/// This contact data model is such that there is no memory lifecycling.
/// Contacts pre-exist, and are filled in by a system
class AsContact
	: public AccessorSet, public Initialize<AsContact>
{
	public:
		AsConstruct(AsContact);
		void initialize(void)
		{
			add(transform,			FE_USE("contact:transform"));
		}

		Accessor<t_moa_xform>				transform;

		t_moa_v3 &contact(const WeakRecord &a_r)
			{ return transform(a_r).translation(); }
		t_moa_v3 &normal(const WeakRecord &a_r)
			{ return transform(a_r).up(); }
		t_moa_v3 &tangent(const WeakRecord &a_r)
			{ return transform(a_r).direction(); }
		t_moa_v3 &binormal(const WeakRecord &a_r)
			{ return transform(a_r).left(); }
};

class AsContactLive
	: public AsContact, public Initialize<AsContactLive>
{
	public:
		AsConstruct(AsContactLive);
		void initialize(void)
		{
			add(velocity,			FE_USE("contact:velocity"));
			add(slide,				FE_USE("contact:slide"));
		}

		Accessor<t_moa_v3>					velocity;
		Accessor<t_moa_v2>					slide;
};

class AsSurface
	: public AccessorSet, public Initialize<AsSurface>
{
	public:
		AsConstruct(AsSurface);
		void initialize(void)
		{
			add(transform,			FE_USE("spc:transform"));
			add(velocity,			FE_USE("spc:velocity"));
		}
		Accessor<t_moa_xform>		transform;
		Accessor<t_moa_v3>			velocity;

};

class AsSurfacePlane
	: public AsSurface, public Initialize<AsSurfacePlane>
{
	public:
		AsConstruct(AsSurfacePlane);
		void initialize(void)
		{
			add(is,					FE_USE("is:plane"));
		}
		Accessor<void>			is;

};

class AsSurfaceLive
	: public AsSurface, public Initialize<AsSurfaceLive>
{
	public:
		AsConstruct(AsSurfaceLive);
		void initialize(void)
		{
			add(spSurfaceI,			FE_USE("tire:spSurfaceI"));
		}
		Accessor< sp<SurfaceI> >	spSurfaceI;

};

#endif

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_tireAS_h__ */

