/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_HAM_DEBUG		FALSE
#define FE_HAM_EVENT_DEBUG	FALSE
#define FE_HAM_THRESHOLD	1e-6
#define FE_HAM_MARGIN		1e-5

//* NOTE TransformOp is a anchorless derivitive

//* directly drag grips with left mouse
//* "select" grips and move with middle mouse
//* hold shift for finer control

//* TODO
//* choose Euler order
//* choose TRS order
//* driver partition (filter)
//* uv-based vs barycenter
//* smoothing
//* fix pan alpha
//* anchor attributes
//* paint points
//* choose representation: Matrix, TRS Euler, or TQS Quaternions

using namespace fe;
using namespace fe::ext;

HammerOp::HammerOp(void):
	m_anchorless(FALSE),
	m_editMode(e_moveAnchor),
	m_lastFrame(0.0),
	m_brushed(FALSE),
	m_pickTriIndex(-1)
{
	setIdentity(m_locator);
}

HammerOp::~HammerOp(void)
{
	changeSharing(FALSE);
}

void HammerOp::initialize(void)
{
	setSharingName("HammerOp:sharing");
	changeSharing(TRUE);

	catalog<String>("icon")="FE_alpha";

	//* page Config

	catalog<String>("name")="";
	catalog<String>("name","label")="Name";
	catalog<String>("name","page")="Config";
	catalog<String>("name","hint")="General short label for this node.";

	catalog<String>("pointGroups");
	catalog<String>("pointGroups","label")="Point Groups";
	catalog<String>("pointGroups","suggest")="pointGroups";
	catalog<String>("pointGroups","page")="Config";
	catalog<bool>("pointGroups","joined")=true;
	catalog<String>("pointGroups","hint")=
		"Space delimited list of point groups to manipulate.";

	catalog<bool>("pointRegex")=false;
	catalog<String>("pointRegex","label")="Regex";
	catalog<String>("pointRegex","page")="Config";
	catalog<String>("pointRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("primitiveGroups");
	catalog<String>("primitiveGroups","label")="Primitive Groups";
	catalog<String>("primitiveGroups","suggest")="primitiveGroups";
	catalog<String>("primitiveGroups","page")="Config";
	catalog<bool>("primitiveGroups","joined")=true;
	catalog<String>("primitiveGroups","hint")=
		"Space delimited list of primitive groups to manipulate.";

	catalog<bool>("primitiveRegex")=false;
	catalog<String>("primitiveRegex","label")="Regex";
	catalog<String>("primitiveRegex","page")="Config";
	catalog<String>("primitiveRegex","hint")=
			catalog<String>("pointRegex","hint");

	catalog<String>("weightAttr");
	catalog<String>("weightAttr","label")="Weight Attribute";
	catalog<String>("weightAttr","page")="Config";
	catalog<String>("weightAttr","hint")=
			"Name of real attribute describing participation in change"
			" (1.0 is full effect).";

	if(!m_anchorless)
	{
		catalog<I32>("face")= -1;
		catalog<I32>("face","min")= -1;
		catalog<I32>("face","high")=100;
		catalog<I32>("face","max")=1e9;
		catalog<bool>("face","joined")=true;
		catalog<String>("face","IO")="input output";
		catalog<String>("face","label")="Face";
		catalog<String>("face","page")="Config";
		catalog<String>("face","hint")=
				"Face index that anchor is attached to."
				"  This may be set interactively.";

		catalog<SpatialVector>("barycenter")=SpatialVector(0.0,0.0,0.0);
		catalog<String>("barycenter","IO")="input output";
		catalog<String>("barycenter","label")="Barycenter";
		catalog<String>("barycenter","page")="Config";
		catalog<String>("barycenter","hint")=
				"Location on indexed face index where anchor is attached."
				"  This may be set interactively.";
	}

	//* page Sphere

	if(!m_anchorless)
	{
		catalog<bool>("sphereWeighted")=true;
		catalog<bool>("sphereWeighted","joined")=true;
		catalog<String>("sphereWeighted","label")="Spherical Weighting";
		catalog<String>("sphereWeighted","page")="Sphere";
		catalog<String>("sphereWeighted","hint")=
				"Apply a ramped falloff from the anchor point.";

		catalog<Real>("radius")=1.0;
		catalog<Real>("radius","high")=10.0;
		catalog<bool>("radius","joined")=true;
		catalog<String>("radius","label")="Radius";
		catalog<String>("radius","enabler")="sphereWeighted";
		catalog<String>("radius","page")="Sphere";
		catalog<String>("radius","hint")=
				"Reach of the sphereical falloff.";

		catalog<Real>("power")=1.0;
		catalog<Real>("power","high")=2.0;
		catalog<String>("power","label")="Power";
		catalog<String>("power","enabler")="sphereWeighted";
		catalog<String>("power","page")="Sphere";
		catalog<String>("power","hint")=
				"Apply an exponent to the otherwise linear ramp.";

		catalog< sp<Component> >("sphereRamp");
		catalog<String>("sphereRamp","implementation")="RampI";
		catalog<String>("sphereRamp","default")="descending";
		catalog<String>("sphereRamp","label")="Sphere Falloff Ramp";
		catalog<String>("sphereRamp","enabler")="sphereWeighted";
		catalog<String>("sphereRamp","page")="Sphere";
		catalog<String>("sphereRamp","hint")=
				"If supported, apply a context-provided ramp widget."
				"  This is multiplicitive with the power parameter.";
	}

	//* page Edit

	catalog<Real>("envelope")=1.0;
	catalog<String>("envelope","label")="Envelope";
	catalog<bool>("envelope","paintable")=true;
	catalog<String>("envelope","page")="Edit";
	catalog<String>("envelope","hint")="Linearly adjust all effects.";

	catalog<String>("orientation")="driver";
	catalog<String>("orientation","label")="Orientation";
	catalog<String>("orientation","choice:0")="world";
	catalog<String>("orientation","label:0")="World Axes";
	catalog<String>("orientation","choice:1")="input";
	catalog<String>("orientation","label:1")="Input Surface";
	catalog<String>("orientation","choice:2")="driver";
	catalog<String>("orientation","label:2")="Driver Surface";
	catalog<String>("orientation","page")="Edit";
	catalog<String>("orientation","hint")=
			"Pick where the pivot point rotation is relative to.";

	catalog<SpatialVector>("pivotT")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("pivotT","IO")="input output";
	catalog<String>("pivotT","label")="Pivot Translate";
	catalog<String>("pivotT","page")="Edit";
	catalog<String>("pivotT","hint")=
			"Offset of pivot from the anchor or origin.";

	catalog<SpatialVector>("pivotR")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("pivotR","IO")="input output";
	catalog<String>("pivotR","label")="Pivot Rotate";
	catalog<String>("pivotR","page")="Edit";
	catalog<String>("pivotR","hint")=
			"Alignment of pivot from initial orientation.";

	catalog<SpatialVector>("deformT")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("deformT","IO")="input output";
	catalog<String>("deformT","label")="Deform Translate";
	catalog<String>("deformT","page")="Edit";
	catalog<String>("deformT","hint")=
			"Change of surface location.";

	catalog<SpatialVector>("deformR")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("deformR","IO")="input output";
	catalog<String>("deformR","label")="Deform Rotate";
	catalog<String>("deformR","page")="Edit";
	catalog<String>("deformR","hint")=
			"Change of surface orientation.";

	catalog<SpatialVector>("deformS")=SpatialVector(1.0,1.0,1.0);
	catalog<String>("deformS","IO")="input output";
	catalog<String>("deformS","label")="Deform Scale";
	catalog<String>("deformS","page")="Edit";
	catalog<String>("deformS","hint")=
			"Change of surface size.";

	//* page Display

	catalog<bool>("gripLabels")=false;
	catalog<String>("gripLabels","label")="Grip Labels";
	catalog<String>("gripLabels","page")="Display";
	catalog<String>("gripLabels","hint")=
			"Apply text to grips as they are pointed at.";

	if(!m_anchorless)
	{
		catalog<Real>("anchorScale")=0.05;
		catalog<Real>("anchorScale","high")=0.1;
		catalog<Real>("anchorScale","max")=1.0;
		catalog<String>("anchorScale","label")="Anchor Scale";
		catalog<String>("anchorScale","page")="Display";
		catalog<String>("anchorScale","hint")=
				"Adjust anchor size (applied to autoscaling).";
	}

	catalog<Real>("axisScale")=1.0;
	catalog<Real>("axisScale","high")=2.0;
	catalog<Real>("axisScale","max")=10.0;
	catalog<String>("axisScale","label")="Axis Scale";
	catalog<String>("axisScale","page")="Display";
	catalog<String>("axisScale","hint")=
			"Adjust axes size (applied to autoscaling).";

	catalog<Real>("weightScale")=0.1;
	catalog<Real>("weightScale","max")=1.0;
	catalog<String>("weightScale","label")="Weight Scale";
	catalog<String>("weightScale","page")="Display";
	catalog<String>("weightScale","hint")=
			"Adjust length of weight lines (applied to autoscaling).";

	catalog<Real>("dimming")=0.6;
	catalog<Real>("dimming","max")=1.0;
	catalog<String>("dimming","label")="Dimming";
	catalog<String>("dimming","page")="Display";
	catalog<String>("dimming","hint")=
			"Adjust brightness of grips not pointed at.";

	catalog<Real>("foreshadow")=0.4;
	catalog<Real>("foreshadow","max")=1.0;
	catalog<String>("foreshadow","label")="Foreshadow";
	catalog<String>("foreshadow","page")="Display";
	catalog<String>("foreshadow","hint")=
			"Adjust how much grips show from behind surfaces.";

	catalog<Real>("aura")=0.5;
	catalog<Real>("aura","max")=1.0;
	catalog<String>("aura","label")="Aura";
	catalog<String>("aura","page")="Display";
	catalog<String>("aura","hint")=
			"Adjust intensity of highlight on active grip.";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")=
			"ESC toggles surface picking or manipulation."
			"  Enter toggles pivot/deform (for manipulator),"
			"  or peer/anchor (for picking)."
			"  LMB picks peer/anchor or manipulates."
			"  MMB remanipulates last control."
			"  Shift slows manipulation.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";

	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setPointSize(6.0);
	m_spWireframe->setLineWidth(1.0);

	m_spWide=new DrawMode();
	m_spWide->setDrawStyle(DrawMode::e_wireframe);
	m_spWide->setLineWidth(4.0);

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_foreshadow);
	m_spSolid->setAntialias(TRUE);
//	m_spSolid->setZBuffering(FALSE);
	m_spSolid->setLit(FALSE);

	m_spManipulator=registry()->create("*.TransformManipulator");

	changeMode(e_manipulator);
}

void HammerOp::changeMode(EditMode a_editMode)
{
	if(m_editMode==a_editMode)
	{
		return;
	}

	m_editMode=a_editMode;

#if FALSE
	switch(m_editMode)
	{
		case e_pickAnchor:
			catalog<String>("Brush","prompt")=
					"LMB picks a different anchor.";
			break;
		case e_moveAnchor:
			catalog<String>("Brush","prompt")=
					"LMB places anchor.";
			if(anchorPicked())
			{
				catalog<String>("Brush","prompt")+=
						"  Esc to return to manipulator.";
			}
			break;
		case e_manipulator:
			catalog<String>("Brush","prompt")=
					"LMB to select grip and manipulate."
					"  MMB to moves selected grip."
					"  Shift reduces sensitivity."
					"  Enter to toggle transform and pivot."
					"  Esc to return to anchor placement.";
			break;
		default:
			catalog<String>("Brush","prompt")="Prompt Error.";
			break;
	}
#endif
}

void HammerOp::handle(Record& a_rSignal)
{
	const BWORD gripLabels=catalog<bool>("gripLabels");
	const Real weightScale=catalog<Real>("weightScale");
	const Real dimming=catalog<Real>("dimming");
	const Real foreshadow=catalog<Real>("foreshadow");
	const Real aura=catalog<Real>("aura");

	const Color black(0.0,0.0,0.0);
	const Color white(1.0,1.0,0.8);
	const Color red(0.8,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color cyan(0.0,0.8,1.0);
	const Color magenta(0.8,0.0,0.8);
	const Color yellow(1.0,1.0,0.0);
	const Color orange(1.0,0.5,0.0);
	const Color blank(0.0,0.0,0.0,0.0);
	const Color grey(0.3,0.3,0.3);

	const SpatialVector up(0.0,1.0,0.0);

	const U32 resolution=16;

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

#if FE_HAM_EVENT_DEBUG
	feLog("HammerOp::handle brush %d brushed %d\n",
			spDrawBrush.isValid(),m_brushed);
#endif

	const Real frame=currentFrame(a_rSignal);
	const BWORD frameChanged=(frame!= m_lastFrame);
	const BWORD inputReplaced=(catalog<bool>("Input Surface","replaced"));
	const BWORD driverReplaced=(catalog<bool>("Driver Surface","replaced"));
	const BWORD replace=(frameChanged || inputReplaced || driverReplaced);
	if(replace && located())
	{
		pushToManipulator();
	}
	const BWORD paramChanged=(!m_brushed && !replace);

	if(m_spManipulator.isValid())
	{
		sp<Catalog> spCatalog=m_spManipulator;
		FEASSERT(spCatalog.isValid());

		spCatalog->catalog<bool>("gripLabels")=gripLabels;
		spCatalog->catalog<Real>("dimming")=dimming;
		spCatalog->catalog<Real>("foreshadow")=foreshadow;
		spCatalog->catalog<Real>("aura")=aura;
	}

	if(m_anchorless)
	{
		changeMode(e_manipulator);
	}
	else if(!anchorPicked())
	{
		changeMode(e_moveAnchor);
	}

	sp<DrawI> spDrawGuide;

	if(!spDrawBrush.isValid())
	{
		catalog<String>("summary")=anchorLabel();

		if(m_pickTriIndex>=0)
		{
			catalog<I32>("face")=m_pickTriIndex;
			catalog<SpatialVector>("barycenter")=m_pickBarycenter;

			m_pickTriIndex= -1;

			changeMode(e_manipulator);
		}

		const Real envelope=catalog<Real>("envelope");
		const Array<Real> envelopePainting=
				catalog< Array<Real> >("envelope","painting");
		const U32 envPaintSize=envelopePainting.size();

		accessGuide(spDrawGuide,a_rSignal,e_quiet);

		if(spDrawGuide.isValid())
		{
			spDrawGuide->pushDrawMode(m_spSolid);
		}

		m_brushed=FALSE;
		m_lastFrame=frame;

		//* TODO use
//		const BWORD rebuild=(paramChanged || frameChanged || replaced);

		access(m_spInput,"Input Surface");
		if(!access(m_spDriver,"Driver Surface",e_quiet))
		{
			m_spDriver=m_spInput;
		}
		if(!m_spInput.isValid())
		{
			if(!m_spDriver.isValid())
			{
				catalog<String>("error")=
						"neither input nor driver is searchable";
				return;
			}
			m_spInput=m_spDriver;
		}

		if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

		sp<SurfaceAccessorI> spOutputPoint;
		if(!access(spOutputPoint,m_spOutputAccessible,
				e_point,e_position)) return;

		sp<SurfaceAccessorI> spOutputNormal;
		access(spOutputNormal,m_spOutputAccessible,
				e_point,e_normal,e_quiet,e_refuseMissing);

		if(located())
		{
			m_locator=evaluateLocator();

			const BWORD sphereWeighted=
					(!m_anchorless && catalog<bool>("sphereWeighted"));
			const Real radius=anchorRadius();
			const Real power=anchorPower();
			const String weightAttr=anchorWeightAttr();

			sp<SurfaceAccessorI> spInputWeight;
			if(!weightAttr.empty())
			{
				access(spInputWeight,"Input Surface",
						e_point,weightAttr,e_warning);
			}

			const SpatialTransform cat=evaluateConcatenation();

			const SpatialVector center=transformVector(m_locator,
					catalog<SpatialVector>("pivotT"));

//			feLog("\nlocator %s\n",c_print(m_locator));
//			feLog("cat\n%s\n",c_print(cat));
//			feLog("center %s\n",c_print(center));

			const U32 pointCount=spOutputPoint->count();

			String primitiveGroups=catalog<String>("primitiveGroups");
			if(!primitiveGroups.empty() && !catalog<bool>("primitiveRegex"))
			{
				primitiveGroups=primitiveGroups.convertGlobToRegex();
			}
			const BWORD primitiveGrouped=(!primitiveGroups.empty());

			sp<SurfaceAccessibleI> spInputAccessible;
			if(!access(spInputAccessible,"Input Surface")) return;

			//* TODO cache
			Array<I32> affectPoint;
			if(primitiveGrouped)
			{
				sp<SurfaceAccessorI> spPrimitiveGroup;
				if(!access(spPrimitiveGroup,spInputAccessible,
						e_primitiveGroup,primitiveGroups)) return;

				sp<SurfaceAccessorI> spInputVertices;
				if(!access(spInputVertices,spInputAccessible,
						e_primitive,e_vertices)) return;

				affectPoint.resize(pointCount,FALSE);

				const I32 primGroupCount=spPrimitiveGroup->count();
				for(I32 index=0;index<primGroupCount;index++)
				{
					const I32 primitiveIndex=spPrimitiveGroup->integer(index);
					const I32 subCount=
							spInputVertices->subCount(primitiveIndex);

					for(I32 subIndex=0;subIndex<subCount;subIndex++)
					{
						const I32 pointIndex=spInputVertices->integer(
								primitiveIndex,subIndex);

						affectPoint[pointIndex]=TRUE;
					}
				}
			}

			String pointGroups=catalog<String>("pointGroups");
			if(!pointGroups.empty() && !catalog<bool>("pointRegex"))
			{
				pointGroups=pointGroups.convertGlobToRegex();
			}
			const BWORD pointGrouped=(!pointGroups.empty());

			sp<SurfaceAccessorI> spPointGroup;
			if(pointGrouped)
			{
				if(!access(spPointGroup,spInputAccessible,
						e_pointGroup,pointGroups)) return;
			}

			const sp<RampI> spRampI=catalog< sp<Component> >("sphereRamp");

			const U32 count=pointGrouped? spPointGroup->count(): pointCount;

			for(U32 index=0;index<count;index++)
			{
				const I32 pointIndex=
						pointGrouped? spPointGroup->integer(index): index;
				if(primitiveGrouped && !affectPoint[pointIndex])
				{
					continue;
				}

				const SpatialVector point=
						spOutputPoint->spatialVector(pointIndex);

				Real ratio(1);

				if(sphereWeighted && radius>0.0)
				{
					//* relative anchor locator or pivot context?
					const Real distance=magnitude(point-center);

#if FALSE
					const Real faded=Real(1)-distance/radius;
#else
					if(distance>radius)
					{
						continue;
					}

					const Real unitDistance=distance/radius;

					const Real faded=spRampI.isValid()?
							spRampI->eval(unitDistance): Real(1)-unitDistance;
#endif

					if(faded<=0.0)
					{
						continue;
					}

					ratio=pow(faded,power);
				}

				const Real weight=(spInputWeight.isValid())?
						spInputWeight->real(pointIndex): Real(1);

				ratio*=envelope*weight*(pointIndex<I32(envPaintSize)?
					envelopePainting[pointIndex]: Real(1));

				if(ratio<=0.0)
				{
					continue;
				}

				SpatialVector transformed;
				transformVector(cat,point,transformed);

				spOutputPoint->set(pointIndex,
						ratio*transformed+(Real(1)-ratio)*point);

				SpatialVector originalNormal(0.0,1.0,0.0);

				if(spOutputNormal.isValid())
				{
					originalNormal=spOutputNormal->spatialVector(pointIndex);

					transformVector(cat,originalNormal,transformed);

					spOutputNormal->set(pointIndex,
							unitSafe(ratio*transformed+(Real(1)-ratio)*point));
				}

				if(spDrawGuide.isValid() && located())
				{
					const Color weightColor(1.0,ratio,0.0,1.0);

					SpatialVector line[2];
					line[0]=point;
					line[1]=line[0]+weightScale*originalNormal;

					spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
							FALSE,&weightColor);
				}
			}

			if(spDrawGuide.isValid())
			{
//				SpatialTransform unscaled=transform;
//				normalizeSafe(unscaled.column(0));
//				normalizeSafe(unscaled.column(1));
//				normalizeSafe(unscaled.column(2));

				spDrawGuide->drawTransformedAxes(m_locator,0.2);
//				spDrawGuide->drawTransformedAxes(pivot*anchor,0.1);
//				spDrawGuide->drawTransformedAxes(unscaled,0.2);
			}
		}

		if(spDrawGuide.isValid())
		{
			if(located())
			{
				spDrawGuide->draw(m_spInput,&grey);
			}
//			if(m_spDriver!=m_spInput)
//			{
//				spDrawGuide->draw(m_spDriver,&orange);
//			}

			spDrawGuide->popDrawMode();
		}
	}
	else
	{
		spDrawBrush->pushDrawMode(m_spSolid);
		spDrawOverlay->pushDrawMode(m_spWireframe);

		m_brushed=TRUE;

		sp<ViewI> spView=spDrawOverlay->view();
		const Box2i viewport=spView->viewport();
		const I32 width=viewport.size()[0];
		const I32 height=viewport.size()[1];

		sp<CameraI> spCameraI=spView->camera();
		SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
		SpatialTransform cameraTransform;
		invert(cameraTransform,cameraMatrix);
		const SpatialVector cameraPos=cameraTransform.column(3);
		const SpatialVector cameraDir= -cameraTransform.column(2);
		const I32 fontHeight=spDrawOverlay->font()->fontHeight(NULL,NULL);

		if(m_spDriver.isValid())
		{
			m_event.bind(windowEvent(a_rSignal));

#if FE_HAM_EVENT_DEBUG
			feLog("%s\n",c_print(m_event));
			feLog("m_editMode %d\n",m_editMode);
#endif

			if(!m_anchorless && m_event.isKeyPress(WindowEvent::e_keyEscape))
			{
				if(m_editMode==e_pickAnchor || m_editMode==e_moveAnchor)
				{
					changeMode(e_manipulator);
				}
				else
				{
					changeMode(e_pickAnchor);
				}
			}

			if(m_editMode!=e_manipulator &&
					m_event.isKeyPress(WindowEvent::e_keyCarriageReturn))
			{
				changeMode(
						(m_editMode==e_pickAnchor)? e_moveAnchor: e_pickAnchor);
			}

			const Real anchorSize=
					catalog<Real>("anchorScale")*m_spDriver->radius();

			if(anchorPicked())
			{
				m_highlightNode="";

				if(m_editMode==e_pickAnchor)
				{
					m_highlightNode=scanPeers(spDrawOverlay,
							cameraPos,cameraDir,FALSE,FALSE,
							m_event.mouseX(),m_event.mouseY());

					if(m_event.isMousePress(WindowEvent::e_itemLeft))
					{
						selectNode(m_highlightNode);
					}
				}

				const BWORD drawLabels=(m_editMode!=e_manipulator);
				scanPeers(spDrawOverlay,
						cameraPos,cameraDir,TRUE,drawLabels,0,0);

				if(drawLabels)
				{
					SpatialVector textPoint(0.5*width,
							height-4*fontHeight,1.0);
					const String label=(m_editMode==e_moveAnchor)?
							"Move Anchor": "Pick Another";

					drawLabel(spDrawOverlay,textPoint,label,
							TRUE,3,green,NULL,&black);
				}
			}

			if(m_editMode==e_moveAnchor)
			{
				const Vector2i mouse(m_event.mouseX(),m_event.mouseY());

				if(anchorPicked())
				{
					const Color anchorColor(0.8,0.8,0.4,0.5);
					drawAnchor(spDrawBrush,m_locator,anchorSize,
							resolution,anchorColor);

					const String label=anchorLabel();
					if(!label.empty())
					{
						Vector2i projected=spView->project(
								m_locator.translation()+
								SpatialVector(0.0,2.0*anchorSize,0.0),
								ViewI::e_perspective);

						const BWORD centered=TRUE;
						const U32 border=1;
						drawLabel(spDrawOverlay,projected,
								label,centered,border,white,NULL,&black);
					}
				}
			}

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			const WindowEvent::MouseButtons buttons=m_event.mouseButtons();

			if(m_editMode==e_moveAnchor)
			{
				SpatialVector intersection;
				set(intersection);
				SpatialVector hitNormal;
				set(hitNormal);

				I32 triIndex= -1;
				SpatialBary barycenter;

				const Real maxDistance(-1);
				sp<SurfaceI::ImpactI> spImpact=m_spDriver->rayImpact(
						rRayOrigin,rRayDirection,maxDistance);
				if(spImpact.isValid())
				{
					intersection=spImpact->intersection();
					hitNormal=spImpact->normal();
					barycenter=spImpact->barycenter();

					sp<SurfaceSearchable::Impact> spSearchImpact=spImpact;
					if(spSearchImpact.isValid())
					{
						triIndex=spSearchImpact->triangleIndex();
					}

					sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
					if(spTriImpact.isValid())
					{
#if FE_HAM_EVENT_DEBUG
						const I32 pickIndex=spTriImpact->triangleIndex();
						feLog("HammerOp::handle triIndex %d pick %d %s\n",
								triIndex,pickIndex,c_print(intersection));
#endif

						drawImpact(spDrawBrush,spTriImpact,0.08,&cyan);
					}
				}

				if(triIndex>=0)
				{
					const SpatialTransform locator=
							m_spDriver->sample(triIndex,barycenter);

					const Color anchorColor(0.5,0.5,1.0,0.5);
					drawAnchor(spDrawBrush,locator,anchorSize,
							resolution,anchorColor);

					if(m_event.isMousePress() &&
							buttons&WindowEvent::e_mbLeft)
					{
						m_pickTriIndex=triIndex;
						m_pickBarycenter=barycenter;

						if(anchorPicked())
						{
							pushToManipulator();
						}
						else
						{
							resetManipulator();
						}
					}
				}
			}
			else if(m_editMode==e_manipulator)
			{
				SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
				SpatialTransform cameraTransform;
				invert(cameraTransform,cameraMatrix);

//				const SpatialVector deltaX=cameraTransform.column(0);
//				const SpatialVector deltaY=cameraTransform.column(1);
//				const SpatialVector towardCamera=cameraTransform.column(2);

				FEASSERT(m_spManipulator.isValid());

				const SpatialTransform xform=
						(m_spManipulator->mode()=="deform")?
						getParamDeform()*getParamPivot()*m_locator:
						getParamPivot()*m_locator;

				const Real axisScale=catalog<Real>("axisScale");
				const Real axisPixels=0.2*axisScale*height;
				const Real gripScale=
						spView->worldSize(xform.translation(),axisPixels);

				sp<Catalog> spCatalog=m_spManipulator;
				FEASSERT(spCatalog.isValid());

				spCatalog->catalog<Real>("anchorScale")=anchorSize;
				spCatalog->catalog<Real>("gripScale")=gripScale;

				if(m_event.isMousePress())
				{
					m_lastPivot=getParamPivot();
					m_lastDeform=getParamDeform();
				}

				m_spManipulator->bindOverlay(spDrawOverlay);
				m_spManipulator->handle(a_rSignal);
				m_spManipulator->draw(spDrawBrush,NULL);

				pullFromManipulator(m_event.isMouseRelease());
			}
		}

		if(located())
		{
			String label=anchorLabel();
			if(label.empty())
			{
				label="UNNAMED";
			}
			const SpatialVector textPoint(0.5*width,
					height-2*fontHeight,1.0);
			drawLabel(spDrawOverlay,textPoint,label,
					TRUE,3,white,NULL,&black);
		}

		if(anchorPicked())
		{
			spDrawBrush->pushDrawMode(m_spWide);

			const Color sphereColor(1.0,1.0,1.0,0.2);

			const Real sphereRadius=anchorRadius();
			const SpatialVector sphereScale(sphereRadius,
					sphereRadius,sphereRadius);

			SpatialTransform xform=getParamPivot()*m_locator;
//			translate(xform,catalog<SpatialVector>("pivotT"));
			rotate(xform,-90.0*degToRad,e_xAxis);

			spDrawBrush->drawCircle(xform,&sphereScale,sphereColor);

			spDrawBrush->popDrawMode();
		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

		//* TODO more selective
		catalog<String>("Brush","cook")="once";

		return;
	}

	if(located())
	{
		if(paramChanged)
		{
			//* repick to update manipulator
			pushToManipulator();
		}
		else
		{
			pullFromManipulator();
		}
	}

	return;
}

I32 HammerOp::getTriIndex(void)
{
	return catalog<I32>("face");
}

SpatialBary HammerOp::getBarycenter(void)
{
	return catalog<SpatialVector>("barycenter");
}

SpatialTransform HammerOp::getParamPivot(void)
{
	const SpatialVector translation=catalog<SpatialVector>("pivotT");
	const SpatialVector eulerAngles=
			catalog<SpatialVector>("pivotR")*fe::degToRad;

	SpatialTransform result=SpatialEuler(eulerAngles);
	setTranslation(result,translation);

	return result;
}

void HammerOp::setParamPivot(SpatialTransform a_transform,BWORD a_anticipate)
{
	SpatialVector& rPivotT=catalog<SpatialVector>("pivotT");
	SpatialVector& rPivotR=catalog<SpatialVector>("pivotR");

	SpatialVector otherT=a_transform.translation();
	SpatialEuler otherR=SpatialEuler(a_transform)*fe::radToDeg;

	if(!equivalent(rPivotT,otherT,FE_HAM_MARGIN))
	{
		rPivotT=otherT;
		applyThreshold(rPivotT,0.0);
	}
	if(!equivalent(rPivotR,otherR,FE_HAM_MARGIN))
	{
		rPivotR=otherR;
		applyThreshold(rPivotR,0.0);
	}

	if(a_anticipate && !(a_transform==m_lastPivot))
	{
		sp<State> spOldState(new State);
		spOldState->m_space=State::e_pivot;
		spOldState->m_transform=m_lastPivot;

		sp<State> spNewState(new State);
		spNewState->m_space=State::e_pivot;
		spNewState->m_transform=a_transform;

#if FE_HAM_DEBUG
		feLog("HammerOp::setParamPivot anticipate pivot\n");
#endif
		chronicle("HammerOp pivot",spOldState,spNewState);
	}
}

SpatialTransform HammerOp::getParamDeform(void)
{
	const SpatialVector translation=catalog<SpatialVector>("deformT");
	const SpatialVector eulerAngles=
			catalog<SpatialVector>("deformR")*fe::degToRad;
	const SpatialVector scaling=catalog<SpatialVector>("deformS");

	SpatialTransform rotation=SpatialEuler(eulerAngles);

	SpatialTransform scaleMatrix;
	setIdentity(scaleMatrix);
	scale(scaleMatrix,scaling);

	SpatialTransform result=scaleMatrix*rotation;
	setTranslation(result,translation);

	return result;
}

void HammerOp::setParamDeform(SpatialTransform a_transform,BWORD a_anticipate)
{
	SpatialTransform unscaled=a_transform;
	SpatialVector scaling(
			magnitude(a_transform.column(0)),
			magnitude(a_transform.column(1)),
			magnitude(a_transform.column(2)));

	unscaled.column(0)*=Real(1)/scaling[0];
	unscaled.column(1)*=Real(1)/scaling[1];
	unscaled.column(2)*=Real(1)/scaling[2];

//	feLog("HammerOp::setParamDeform\n%s\nunscaled\n%s\nscaling %s\n",
//			c_print(a_transform),c_print(unscaled),c_print(scaling));

	SpatialVector& rDeformT=catalog<SpatialVector>("deformT");
	SpatialVector& rDeformR=catalog<SpatialVector>("deformR");
	SpatialVector& rDeformS=catalog<SpatialVector>("deformS");

	SpatialVector otherT=a_transform.translation();
	SpatialEuler otherR=SpatialEuler(unscaled)*fe::radToDeg;
	SpatialVector otherS=scaling;

	if(!equivalent(rDeformT,otherT,FE_HAM_MARGIN))
	{
		rDeformT=otherT;
		applyThreshold(rDeformT,0.0);
	}
	if(!equivalent(rDeformR,otherR,FE_HAM_MARGIN))
	{
		rDeformR=otherR;
		applyThreshold(rDeformR,0.0);
	}
	if(!equivalent(rDeformS,otherS,FE_HAM_MARGIN))
	{
		rDeformS=otherS;
		applyThreshold(rDeformS,1.0);
	}

	if(a_anticipate && !(a_transform==m_lastDeform))
	{
		sp<State> spOldState(new State);
		spOldState->m_space=State::e_deform;
		spOldState->m_transform=m_lastDeform;

		sp<State> spNewState(new State);
		spNewState->m_space=State::e_deform;
		spNewState->m_transform=a_transform;

#if FE_HAM_DEBUG
		feLog("HammerOp::setParamDeform anticipate deform\n");
#endif
		chronicle("HammerOp deform",spOldState,spNewState);
	}
}

//* TODO if values limited, should send back to manipulator

BWORD HammerOp::undo(String a_change,sp<Counted> a_spCounted)
{
#if FE_HAM_DEBUG
	feLog("HammerOp::undo \"%s\" %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif
	return restore(a_spCounted);
}

BWORD HammerOp::redo(String a_change,sp<Counted> a_spCounted)
{
#if FE_HAM_DEBUG
	feLog("HammerOp::redo \"%s\" %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif
	return restore(a_spCounted);
}

BWORD HammerOp::restore(sp<State> a_spState)
{
	if(a_spState.isNull())
	{
#if FE_HAM_DEBUG
		feLog("HammerOp::restore invalid state\n");
#endif
		return FALSE;
	}

#if FE_HAM_DEBUG
	feLog("HammerOp::restore %s\n%s\n",
			a_spState->m_space==State::e_pivot? "pivot": "deform",
			c_print(a_spState->m_transform));
#endif

	if(a_spState->m_space==State::e_pivot)
	{
		setParamPivot(a_spState->m_transform);
		dirty(FALSE);
		return TRUE;
	}

	if(a_spState->m_space==State::e_deform)
	{
		setParamDeform(a_spState->m_transform);
		dirty(FALSE);
		return TRUE;
	}

	return FALSE;
}

void HammerOp::applyThreshold(SpatialVector& a_rVector,Real a_default)
{
	if(fabs(a_rVector[0]-a_default)<FE_HAM_THRESHOLD)
	{
		a_rVector[0]=a_default;
	}
	if(fabs(a_rVector[1]-a_default)<FE_HAM_THRESHOLD)
	{
		a_rVector[1]=a_default;
	}
	if(fabs(a_rVector[2]-a_default)<FE_HAM_THRESHOLD)
	{
		a_rVector[2]=a_default;
	}
}

BWORD HammerOp::located(void)
{
	return (m_anchorless || anchorPicked());
}

BWORD HammerOp::anchorPicked(void)
{
	return (!m_anchorless && catalog<I32>("face")>=0);
}

String HammerOp::anchorLabel(void)
{
	return catalog<String>("name");
}

String HammerOp::anchorWeightAttr(void)
{
	return catalog<String>("weightAttr");
}

Real HammerOp::anchorRadius(void)
{
	return catalog<Real>("radius");
}

Real HammerOp::anchorPower(void)
{
	return catalog<Real>("power");
}

SpatialTransform HammerOp::evaluateLocator(void)
{
	if(m_anchorless)
	{
		SpatialTransform result;
		setIdentity(result);
		return result;
	}

	const I32 triIndex=getTriIndex();
	if(triIndex>=0)
	{
		const String orientation=catalog<String>("orientation");

		if(orientation=="driver" && m_spDriver.isValid())
		{
			return m_spDriver->sample(triIndex,getBarycenter());
		}

		if(m_spInput.isValid())
		{
#if TRUE
			const SpatialTransform xform=
					m_spDriver->sample(triIndex,getBarycenter());
#else
			//* TODO use nearest point from driver onto input

			//* NOTE tri, bary are on driver, not input (unless same)
			const SpatialTransform xform=
					m_spInput->sample(triIndex,getBarycenter());
#endif

			if(orientation=="world")
			{
				SpatialTransform result;
				setIdentity(result);
				setTranslation(result,xform.translation());
				return result;
			}

			return xform;
		}
	}

	SpatialTransform result;
	setIdentity(result);
	return result;
}

SpatialTransform HammerOp::evaluateConcatenation(void)
{
	const SpatialTransform pivot=getParamPivot();
	const SpatialTransform deform=getParamDeform();

	const SpatialTransform context=pivot*m_locator;

	SpatialTransform invContext;
	invert(invContext,context);

	return invContext*deform*context;
}

void HammerOp::resetManipulator(void)
{
	SpatialTransform identity;
	setIdentity(identity);
	setParamPivot(identity);
	setParamDeform(identity);

	pushToManipulator();
}

void HammerOp::pushToManipulator(void)
{
	//* copy params to manipulator catalog

	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	spCatalog->catalog<SpatialTransform>("anchor")=m_locator;
	spCatalog->catalog<SpatialTransform>("pivot")=getParamPivot();
	spCatalog->catalog<SpatialTransform>("transform")=getParamDeform();

	m_locator=evaluateLocator();
}

void HammerOp::pullFromManipulator(BWORD a_anticipate)
{
	//* copy manipulator to params

	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	setParamPivot(spCatalog->catalog<SpatialTransform>("pivot"),
			a_anticipate);
	setParamDeform(spCatalog->catalog<SpatialTransform>("transform"),
			a_anticipate);
}

String HammerOp::scanPeers(sp<DrawI>& a_rspDrawOverlay,
	const SpatialVector& a_cameraPos,const SpatialVector& a_cameraDir,
	BWORD a_draw,BWORD a_labels,I32 a_mouseX,I32 a_mouseY)
{
	const Color black(0.0,0.0,0.0);
	const Color white(1.0,1.0,1.0);
	const Color cyan(0.0,1.0,1.0);
	const Color magenta(1.0,0.0,1.0);
	const Color yellow(1.0,1.0,0.0);
	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Real dimAlpha=0.4;

//	feLog("\nHammerOp::scanPeers for \"%s\"\n",name().c_str());

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	Array< hp<Component> >& rGlobalArray=
			spMasterCatalog->catalog< Array< hp<Component> > >(
			sharingName());

	const Vector2i mousePoint(a_mouseX,a_mouseY);

	String nearestNode;
	Real nearestDistance(-1);

	const U32 componentCount=rGlobalArray.size();
	for(U32 componentIndex=0;componentIndex<componentCount;componentIndex++)
	{
		sp<Component> spComponent=rGlobalArray[componentIndex];
		if(spComponent.isValid())
		{
//			feLog("HammerOp::scanPeers %d/%d \"%s\"\n",
//					componentIndex,componentCount,spComponent->name().c_str());

			sp<HammerOp> spHammerOp=spComponent;
			if(spHammerOp.isValid())
			{
				if(spHammerOp.raw()!=this)
				{
//~					Real& rShareFrame=
//~							spHammerOp->m_shareFrame;
//~					if(rShareFrame!=m_lastFrame)
//~					{
//~						rShareFrame=Real(-1);
//~						continue;
//~					}

					const SpatialTransform& rLocator=
							spHammerOp->m_locator;
					const SpatialVector toShare=
							unitSafe(rLocator.translation()-a_cameraPos);
					const Real cameraDot=dot(toShare,a_cameraDir);
					if(cameraDot<0.0)
					{
						//* behind camera
						continue;
					}

					const String nodeName=spHammerOp->name();
					const BWORD highlighted=(nodeName==m_highlightNode);

					sp<ViewI> spView=a_rspDrawOverlay->view();
					const Vector2i projected=spView->project(
							rLocator.translation(),
							ViewI::e_perspective);

					if(a_draw)
					{
						const Real envelope=
								spHammerOp->catalog<Real>("envelope");
						const BWORD disabled=(envelope<=0.0);

						const BWORD backfacing=
								(dot(rLocator.column(1),a_cameraDir)>0.0);

						const BWORD faded=backfacing;

						Color rimColor=disabled? red:
							((envelope<Real(1))? yellow: green);
						Color centerColor=highlighted? white: black;
						Color shadowColor=magenta;
						Color sparkleColor=cyan;

						if(faded)
						{
							rimColor[3]=dimAlpha;
							centerColor[3]=dimAlpha;
							shadowColor[3]=dimAlpha;
							sparkleColor[3]=dimAlpha;
						}

						const Real multiplication=
								a_rspDrawOverlay->multiplication();
						const Color* pShadowColor=NULL;
						const Color* pSparkleColor=NULL;
						const I32 rimSize=4*multiplication;
						const BWORD bevel=TRUE;

						drawSquare(a_rspDrawOverlay,
								rLocator.translation(),
								rimSize,bevel,centerColor,rimColor,
								pShadowColor,pSparkleColor);

						if(faded || !a_labels)
						{
							continue;
						}

						const String label=
								spHammerOp->catalog<String>("name");

						const BWORD centered=FALSE;
						const U32 border=1;
						drawLabel(a_rspDrawOverlay,
								projected+Vector2i(6,0)*multiplication,
								label,centered,border,white,NULL,
								&black);

						continue;
					}

					const Real pixelDistance=
							magnitude(projected-mousePoint);
					if(nearestDistance<0.0 || pixelDistance<nearestDistance)
					{
						nearestDistance=pixelDistance;
						nearestNode=nodeName;
					}
				}
			}
		}
	}

	return nearestNode;
}
