/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceDeltaOp_h__
#define __operator_SurfaceDeltaOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Calculate Difference of Two Surfaces

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceDeltaOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceDeltaOp>
{
	public:
				SurfaceDeltaOp(void)										{}
virtual			~SurfaceDeltaOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceDeltaOp_h__ */
