/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecordView_h__
#define __datatool_RecordView_h__

#define	FE_RV_VERBOSE	(FE_CODEGEN<=FE_DEBUG)
#define	FE_RV_CHECK		(FE_CODEGEN<=FE_DEBUG)

// check that Record exists
#define FE_RV_R_CHECK	(FE_CODEGEN<=FE_DEBUG)

// check that WeakRecord's have serial numbers
#define FE_RV_WR_CHECK	(FE_CODEGEN<=FE_DEBUG)

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Bindable collection of accessor Functors

	@ingroup datatool

	RecordView is an enhanced AccessorSet that supports inheritance
	and default values.  Like an AccessorSet, a RecordView instance
	can be reused for any number of Record instances.

	For example:

@code
class FE_DL_EXPORT Point: public RecordView
{
	public:
		Functor<SpatialVector>		location;

				Point(void)			{ setName("Point"); }
virtual	void	addFunctors(void)
				{
					add(length,		"spatial_vector",
							FE_SPEC("spc:at",
							"translation from origin"));
				}
virtual	void	initializeRecord(void)
				{
					set(location());
				}
virtual	void	finalizeRecord(void)
				{
				}
};
@endcode

	A class that derives from RecordView is expected to provide several things.
	Added attributes are provided through Functors,
	an enhancement of the Accessor.

	Provide a name for the generated Layout by setting the name
	of the RecordView, using setName().

	Functors are wired up using add() during addFunctors().
	The implementation of addFunctors() should call the like-named functions
	for any direct base classes, much like a manual constructor mechanism.
	Convention insists that attributes names are provided using
	FE_USE or FE_SPEC.  FE_USE presumes that an FE_SPEC or FE_ATTRIBUTE
	elsewhere has provided a description for the atrribute.

	As a RecordFactoryI, a RecordView can be used to instantiate Records
	by name from file.  The method initializeRecord is called before
	loading data to provide basic defaults.  A method finalizeRecord
	can be used after loading a Record for producing derived values,
	such as creating a component as named by an attribute.
	Both initializeRecord and finalizeRecord may be omitted if
	nothing needs to be done for that stage.
	As with addFunctors, both initializeRecord and finalizeRecord should
	call the like-named functions for any direct base classes.

	To be available as a RecordFactoryI, each RecordView-derived type
	should be added to a library during CreateLibrary()
	like any other Component.

@code
pLibrary->add<Point>("RecordFactoryI.Point.fe");
@endcode

	File-based defaults can be provided as a template in a record group file.

@code
TEMPLATE Point ""
	spc:at "0 0 0"
@endcode

	Attributes not represented by a template retain settings from
	initializeRecord or their type's constructor.
	Derived templates can provide new defaults that override prior values.
	In the basic case, templates will parallel provided Factories,
	including multiple inheritance (the second argument to TEMPLATE).
	However, new templates can be provided purely on multiple inheritance
	where no new attributes are added.

*//***************************************************************************/
class RecordView:
	public AccessorSet,
	virtual public RecordFactoryI
{
	protected:

	/// @brief Untyped Functor base
	class RecordHolder
	{
		public:
#if FE_RV_CHECK
								RecordHolder(void):
									m_pWeakRecord(NULL)						{}
#endif
								/// NOP: RecordView will handle copying
			RecordHolder&		operator=(const RecordHolder& rRecordHolder)
								{	return *this; }

			void				bind(WeakRecord& rWeakRecord)
								{	m_pWeakRecord=&rWeakRecord; }
		protected:

			WeakRecord&			weakRecord(void) const
								{
#if FE_RV_R_CHECK
									if(!m_pWeakRecord)
									{
										feX("RecordView::RecordHolder"
												"::weakRecord",
												"m_pWeakRecord NULL");
									}
#endif
									return *m_pWeakRecord;
								}

			WeakRecord*			m_pWeakRecord;
	};

	/// @brief Bound accessor in a RecordView
	/// @ingroup datatool
	template<typename T>
	class Functor: public RecordHolder, public Accessor<T>
	{
		public:
						Functor(void)										{}
			T&			operator()(void);
	const	T			operator()(void) const;
			T&			operator()(const Record& r)
						{	return Accessor<T>::operator()(r); }
const		T			operator()(const Record& r) const
						{	return Accessor<T>::operator()(r); }
			T&			operator()(const WeakRecord& r)
						{	checkWeakRecord(r);
							return Accessor<T>::operator()(r); }
const		T			operator()(const WeakRecord& r) const
						{	checkWeakRecord(r);
							return Accessor<T>::operator()(r); }
			T&			operator()(sp<RecordArray>& rspRA, FE_UWORD index)
						{	return Accessor<T>::operator()(rspRA,index); }
const		T&			operator()(sp<RecordArray>& rspRA, FE_UWORD index) const
						{	return Accessor<T>::operator()(rspRA,index); }

			BWORD		check(void) const
						{
							FEASSERT(m_pWeakRecord);
							return Accessor<T>::check(*m_pWeakRecord);
						}
			BWORD		check(sp<RecordArray>& rspRA) const
						{	return Accessor<T>::check(rspRA); }
			BWORD		check(const Record& r) const
						{	return Accessor<T>::check(r); }

						/** @brief Get the value, checking first

							If the bound Record does not support the
							attribute, zero is returned, cast to the
							attribute type. */
			T			safeGet(void)
						{
							T* pValue=NULL;
							if(weakRecord().isValid() &&
									(pValue=Accessor<T>::queryAttribute(
									weakRecord())))
							{
								return *pValue;
							}
							return T(0);
						}

						/** @brief Set the value, checking first

							If the bound Record does not support the
							attribute, nothing occurs. */
			void		safeSet(T value)
						{
							T* pValue=NULL;
							if(weakRecord().isValid() &&
									(pValue=Accessor<T>::queryAttribute(
									weakRecord())))
							{
								*pValue=value;
							}
						}

						/** @brief Call the attribute as a method

							If the attribute cannot be cast to HandlerI,
							nothing occurs.
							If the cast is successful, the currently indexed
							Record is passed to the HandlerI using handle().

							Trying to call an attribute whose content is not
							derived from Counted or similar may cause a
							compiler error. */
			void		call(void)
						{
							sp<HandlerI> spHandlerI=safeGet();
							if(spHandlerI.isValid())
							{
								Record record=weakRecord();

								spHandlerI->handle(record);
							}
						}

						/** @brief Create a named component for the attribute

							The created component is placed in the attribute.
							If the attribute is not a component,
							a compile error should occur. */
			void		createAndSetComponent(String componentName)
						{
							sp<Scope> spScope=Accessor<T>::scope();
							FEASSERT(spScope.isValid());
							sp<Component> spComponent=
									spScope->registry()->create(componentName);
							if(!spComponent.isValid())
							{
								feX("Functor<>::createAndSetComponent",
										"\"%s\" failed to create \"%s\"\n",
										name().c_str(),
										componentName.c_str());
							}
							spComponent->setName(componentName+" "+name());
							operator()()=spComponent;
						}

						/** @brief Create a RecordGroup for the attribute

							The RecordGroup is placed in the attribute.
							If the attribute is not a RecordGroup,
							a compile error should occur. */
			void		createAndSetRecordGroup(void)
						{
							FEASSERT(m_pWeakRecord);
							sp<RecordGroup> spRecordGroup(new RecordGroup());
							FEASSERT(spRecordGroup.isValid());
#if FE_COUNTED_TRACK
							spRecordGroup->setName(name());
#endif
							operator()()=spRecordGroup;
						}

const	String&			name(void) const
						{	return BaseAccessor::attribute()->name(); }
		private:
		void			checkRecordExists(void) const
						{
#if FE_RV_R_CHECK
							if(!weakRecord().isValid())
							{
								feX("RecordView::checkRecordExists",
										"WeakRecord invalid");
							}
#endif
						}

		void			checkWeakRecord(const WeakRecord& r) const
						{
#if FE_RV_WR_CHECK
							r.demandSerialNumber();
#endif
						}
	};

	public:
					RecordView(void)										{}

virtual				~RecordView(void)
					{
#if FE_COUNTED_TRACK
						deregisterRegion(fe_cast<Counted>(this));
#endif
					}

		void		setName(const String& name)
					{
						Component::setName(name);
#if FE_COUNTED_TRACK
						registerRegion(fe_cast<Counted>(this),
							sizeof(RecordView));
#endif
					}

const	String		verboseName(void) const
					{	return "RecordView " + name(); }

					/** @brief Associate with a Scope

						This can happen automatically by binding
						to a Record, but may be neccessary
						if you need to use the Functors before an
						appropriate Layout exists. */
		void		bind(sp<Scope>& rspScope)
					{
						FEASSERT(rspScope.isValid());

						if(!m_hpScope.isValid())
						{
							AccessorSet::bind(rspScope);
							addFunctors();
						}
#if FE_RV_CHECK
						else if(m_hpScope!=rspScope)
						{
							feX("RecordView::bind(sp<Scope>&) \"%s\"",
									"changed scope",name().c_str());
						}
#endif
					}

					/// @brief Associate with a Scope by Handle
		void		bind(hp<Scope>& rhpScope)
					{
						FEASSERT(rhpScope.isValid());

						if(!m_hpScope.isValid())
						{
							AccessorSet::bind(rhpScope);
							addFunctors();
						}
#if FE_RV_CHECK
						else if(m_hpScope!=rhpScope)
						{
							feX("RecordView::bind(hp<Scope>&) \"%s\"",
									"changed scope",name().c_str());
						}
#endif
					}

					/// @brief Return associated Scope
		hp<Scope>&	scope(void)				{ return m_hpScope; }

					/** @brief Disassociate with any Record

						This should free all references. */
		void		unbind(void)
					{
						m_record=Record();
						m_weakRecord=WeakRecord();
					}

					/** @brief Associate with a specific Record

						The Record will persist while bound.
						Use unbind() to release the binding.

						Passing Record objects is not thread-safe.

						This may use the Record's scope to
						initialize the functors. */
		void		bind(const Record record,BWORD weak=FALSE)
					{
						m_record=record;
						m_weakRecord=m_record;

						if(!record.isValid())
						{
							return;
						}

#if !FE_RV_CHECK
						if(!m_hpScope.isValid())
#endif
						{
							bind(record.layout()->scope());
						}
					}

					/** @brief Associate with a specific WeakRecord

						The RecordView will not ensure that the WeakRecord
						continues to persist.

						This may use the Record's scope to
						initialize the functors. */
		void		bind(const WeakRecord weakRecord,BWORD weak=FALSE)
					{
						m_record=Record();
						m_weakRecord=weakRecord;

#if !FE_RV_CHECK
						if(!m_hpScope.isValid())
#endif
						{
							if(weakRecord.isValid())
							{
								bind(weakRecord.layout()->scope());
							}
						}
					}

					/** @brief Associate with a specific WeakRecord

						The RecordView will not ensure that the WeakRecord
						continues to persist.

						This method is faster than bind(WeakRecord) as it
						will never check the Scope and initialize the functors.
						Using bind(sp<Scope>) before this method can
						ensure that the functors are initialized. */
		void		setWeakRecord(const WeakRecord weakRecord)
					{
						m_record=Record();
						m_weakRecord=weakRecord;
					}

					/**	@brief Produce a Record using all the attributes

						The Record is unbound after initialization. */
virtual	void		produce(Record& rRecord)
					{
						if(rRecord.isValid())
						{
							bind(rRecord);
							initializeRecord();
						}
						else
						{
							rRecord=createRecord();
						}
						unbind();
					}

					/**	@brief Finalize a Record using all the attributes

						This step follows after the record is fully
						initialized and potentially serialized in.

						The Record is unbound after initialization. */
virtual	void		finalize(Record& rRecord)
					{
						if(rRecord.isValid())
						{
							bind(rRecord);
							finalizeRecord();
						}
						unbind();
					}

					/**	@brief Create a Record using all the attributes

						The Record is automatically bound. */
		Record		createRecord(void)
					{
						if(!m_hpScope.isValid())
						{
							feX("RecordView::createRecord",
									"\"%s\" has invalid scope\n",
									name().c_str());
						}

						Record record=m_hpScope->createRecord(layout());
						if(!m_hpLayout.isValid())
						{
							feX("RecordView::createRecord",
									"\"%s\" failed to create layout\n",
									name().c_str());
						}
						if(!record.isValid())
						{
							feX("RecordView::createRecord",
									"\"%s\" failed to create record\n",
									name().c_str());
						}

						bind(record);
						initializeRecord();
						return record;
					}

					///	@brief Access the record by weak (fast) reference
		WeakRecord	record(void) const
					{
						return m_weakRecord;
					}

					///	@brief Access a Layout of all the attributes
		sp<Layout>	layout(void)
					{
						if(!m_hpScope.isValid())
						{
							feLog("RecordView::layout \"%s\" invalid scope\n",
									name().c_str());
							return sp<Layout>(NULL);
						}
						if(!m_hpLayout.isValid())
						{
							m_hpLayout=createLayout();
						}
						return m_hpLayout;
					}

					///	@brief Spew attributes and state for the bound Record
		void		dump(void)
					{
						U32 number=size();
						for(U32 m=0;m<number;m++)
						{
							FEASSERT((*this)[m]);
							if(!record().isValid())
							{
								feLog("%2d %-16s %-16s <invalid record>\n",m,
										nameOf(*(*this)[m]).c_str(),
										typeOf(*(*this)[m]).c_str());
							}
// valid record should always be locked anyway
#if 0
							else if(!record().layout()->locked())
							{
								feLog("%2d %-16s %-16s <not locked>\n",m,
										nameOf(*(*this)[m]).c_str(),
										typeOf(*(*this)[m]).c_str());
							}
#endif
							else
							{
								feLog("%2d %-16s %-16s %s\n",m,
										nameOf(*(*this)[m]).c_str(),
										typeOf(*(*this)[m]).c_str(),
										printOf(*(*this)[m]).c_str());
							}
						}
					}

					/**	@brief Called at instantiation to add functors

						In addition to using add() to explicitly adding all
						the functors for a particular class,
						the implementation of this function should first call
						addFunctors() for all directly inherited classes
						that are indirectly or directly derived from
						RecordView(). */
virtual	void		addFunctors(void)										=0;

					/**	@brief Called at instantiation to initialize
						attributes

						In addition to setting any attributes, the
						implementation of this function should first call
						initializeRecord() for all directly inherited classes
						that are indirectly or directly derived from
						RecordView().

						Functors can be used as accessors at this point. */
virtual	void		initializeRecord(void)									{}

					/**	@brief Called right after instantiation to finalize
						complex attributes

						In addition to adjusting any attributes, the
						implementation of this function should first call
						finalizeRecord() for all directly inherited classes
						that are indirectly or directly derived from
						RecordView().

						Functors can be used as accessors at this point. */
virtual	void		finalizeRecord(void)									{}

					/** @brief Indicate a RecordFactoryI for a Scope

						The template type is presumably a RecordView.
						This function extracts the relevant arguments
						to pass to the Scope.

						It is usually preferable to add the RecordView
						as a Scope-generic RecordFactoryI using the
						general registry mechanism.

						This is a convenience that doesn't really relate
						to RecordView. */
					template <typename T>
static	void		registerFactory(sp<Scope> spScope)
					{
						T* pRV=new T();
						sp<RecordFactoryI> spRecordFactoryI(pRV);
						spScope->registerFactory(pRV->name(),spRecordFactoryI);
					}

					/** @brief Load a RecordGroup from a file

						This is a convenience that doesn't really relate
						to RecordView. */
static	sp<RecordGroup> loadRecordGroup(sp<Scope> spScope,
							String filename,BWORD a_binary=FALSE)
					{
						std::ifstream inFile(filename.c_str());
						if(!inFile)
						{
#if FE_RV_VERBOSE
							feLog("RecordView::loadRecordGroup"
									" could not open\n  \"%s\"\n",
									filename.c_str());
#endif
							return sp<RecordGroup>();
						}

						sp<data::StreamI> spStream(a_binary?
								sp<data::StreamI>(
								new data::BinaryStream(spScope)):
								sp<data::StreamI>(
								new data::AsciiStream(spScope)));
						sp<RecordGroup> spRecordGroup=spStream->input(inFile);
						inFile.close();

						return spRecordGroup;
					}

					/** @brief Serialize a RecordGroup from a string

						This is a convenience that doesn't really relate
						to RecordView. */
static	sp<RecordGroup> loadRecordGroupFromBuffer(sp<Scope> spScope,
							String buffer,BWORD a_binary=FALSE)
					{
						std::istringstream inString(buffer.c_str());
						if(!inString)
						{
#if FE_RV_VERBOSE
							feLog("RecordView::loadRecordGroupFromBuffer"
									" could not open string buffer\n");
#endif
							return sp<RecordGroup>();
						}

						sp<data::StreamI> spStream=a_binary?
								sp<data::StreamI>(
								new data::BinaryStream(spScope)):
								sp<data::StreamI>(
								new data::AsciiStream(spScope));
						sp<RecordGroup> spRecordGroup=spStream->input(inString);

						return spRecordGroup;
					}

					/** @brief Save a RecordGroup to a file

						This is a convenience that doesn't really relate
						to RecordView. */
static	void		saveRecordGroup(sp<Scope> spScope,
							sp<RecordGroup>& rspRecordGroup,
							String filename,BWORD a_binary=FALSE)
					{
						std::ofstream outfile(filename.c_str());
						if(!outfile)
						{
							feLog("RecordView::saveRecordGroup"
									" could not open test output file\n");
						}
						else
						{
							sp<data::StreamI> spStream=a_binary?
									sp<data::StreamI>(
									new data::BinaryStream(spScope)):
									sp<data::StreamI>(
									new data::AsciiStream(spScope));
							spStream->output(outfile, rspRecordGroup);
							outfile.close();
						}
					}

	protected:
					/**	@brief Add an attribute Functor

						The functor should be a member variable.
						The name can be arbitrary, but conventions
						may be wise. */
		void		add(BaseAccessor& rFunctor,const char* name)
					{
						rFunctor.setup(m_hpScope,name);
//						m_hpScope->support(name,type);
						AccessorSet::add(rFunctor,name);
						RecordHolder* pHolder=
								static_cast<RecordHolder*>(
								static_cast<Functor<int>*>(&rFunctor));
						pHolder->bind(m_weakRecord);
					}

					RecordView(const RecordView& rRecordView):
//* TODO check gcc, clang (warning on VS)
//						Castable(),
//						Component(),
						AccessorSet()										{}

	private:
		sp<Layout>	createLayout(void)
					{
						if(!m_hpScope.isValid())
						{
							feX("AccessorArray::createLayout",
									"\"%s\" has invalid scope\n",
									name().c_str());
						}

						sp<Layout> spLayout=m_hpScope->declare(name());
						if(!spLayout.isValid())
						{
							feX("AccessorArray::createLayout",
									"\"%s\" failed to create layout\n",
									name().c_str());
						}

						populate(spLayout);
						return spLayout;
					}

		String		nameOf(BaseAccessor& accessor) const
					{	return accessor.attribute()->name(); }

		String		typeOf(BaseAccessor& accessor) const
					{
						std::list<String> nameList;
						m_hpScope->registry()->master()->typeMaster()
								->reverseLookup(baseTypeOf(accessor),nameList);
						return nameList.front();
					}

		String		printOf(BaseAccessor& accessor)
					{
						void* pVoid=record().rawAttribute(accessor.index());

						sp<BaseType::Info> spInfo=helpOf(accessor);
						return spInfo->print(pVoid);
					}

		sp<BaseType::Info> helpOf(BaseAccessor& accessor) const
					{
						sp<BaseType::Info> spInfo=
								baseTypeOf(accessor)->getInfo();
						FEASSERT(spInfo.isValid());
						return spInfo;
					}

		sp<BaseType> baseTypeOf(BaseAccessor& accessor) const
					{
						sp<Attribute> spAttribute=accessor.attribute();
						return spAttribute->type();
					}

		hp<Layout>		m_hpLayout;

		Record			m_record;
		WeakRecord		m_weakRecord;
};

template<typename T>
inline T& RecordView::Functor<T>::operator()(void)
{
#if FE_RV_R_CHECK
	checkRecordExists();
#endif
	return Accessor<T>::operator()(weakRecord());
}

template<typename T>
inline const T RecordView::Functor<T>::operator()(void) const
{
#if FE_RV_R_CHECK
	checkRecordExists();
#endif
	return Accessor<T>::operator()(weakRecord());
}

template<>
inline WeakRecord& RecordView::Functor<WeakRecord>::operator()(void)
{
#if FE_RV_R_CHECK
	checkRecordExists();
#endif

#if FE_RV_WR_CHECK
	checkWeakRecord(Accessor<WeakRecord>::operator()(weakRecord()));
#endif
	return Accessor<WeakRecord>::operator()(weakRecord());
}

template<>
inline const WeakRecord RecordView::Functor<WeakRecord>::operator()(void) const
{
#if FE_RV_R_CHECK
	checkRecordExists();
#endif

#if FE_RV_WR_CHECK
	checkWeakRecord(Accessor<WeakRecord>::operator()(weakRecord()));
#endif
	return Accessor<WeakRecord>::operator()(weakRecord());
}

inline BWORD operator==(const RecordView& rRecordView1,
		const RecordView& rRecordView2)
{
	return rRecordView1.record()==rRecordView2.record();
}

inline BWORD operator!=(const RecordView& rRecordView1,
		const RecordView& rRecordView2)
{
	return rRecordView1.record()!=rRecordView2.record();
}

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecordView_h__ */
