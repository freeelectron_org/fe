/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

#define FE_OV_DISPLAY_FRAMERATE		TRUE

namespace fe
{
namespace ext
{

#define FRAMECOUNT_PERIOD	500.0f	//* ms

ObjectViewer::ObjectViewer(void):
	m_started(FALSE)
{
	m_frameMS=0.0f;
	m_frameRate=0.0f;
	m_frameCount=0;
	m_frameTotal=0.0f;
}

ObjectViewer::~ObjectViewer(void)
{
#ifdef FE_CHAINSIGNALER_PROFILING
	if(m_spEventSignaler.isValid())
	{
		Peeker peeker;
		m_spEventSignaler->peek(peeker);

		feLog("\n~ObjectViewer Event Signaler:\n%s\n",peeker.str().c_str());
	}
	if(m_spDrawSignaler.isValid())
	{
		Peeker peeker;
		m_spDrawSignaler->peek(peeker);

		feLog("\n~ObjectViewer Draw Signaler:\n%s\n",peeker.str().c_str());
	}
#endif
}

void ObjectViewer::initialize(void)
{
	m_spEventSignaler=registry()->create("SignalerI");
	m_spDrawSignaler=registry()->create("SignalerI");

	m_spEventSignaler->setName("ObjectViewer EventSignaler");
	m_spDrawSignaler->setName("ObjectViewer DrawSignaler");

	m_spCameraControllerI=registry()->create("CameraControllerI");
	if(!m_spCameraControllerI.isValid())
	{
		feX("ObjectViewer::initialize","needed to create an CameraControllerI");
	}

	m_spCameraOverlay=new CameraEditable();
	m_spCameraOverlay->setName("ObjectViewer.CameraEditable overlay");
	m_spCameraOverlay->setRasterSpace(TRUE);

	m_spScope=registry()->master()->catalog()->catalogComponent(
			"Scope","WinScope");
	m_asViewer.bind(m_spScope);

	if(!m_spScope.isValid() || !m_spEventSignaler.isValid() ||
			!m_spDrawSignaler.isValid())
	{
		feX("ObjectViewer::initialize","couldn't create components");
	}

	m_spBeatLayout=m_spScope->declare("beat");

	m_spRenderLayout=m_spScope->declare("render");
	m_asViewer.populate(m_spRenderLayout);

	m_spScope->declare(FE_EVENT_LAYOUT);

	m_beat=m_spScope->createRecord(m_spBeatLayout);
	m_underlay=m_spScope->createRecord(m_spRenderLayout);
	m_render=m_spScope->createRecord(m_spRenderLayout);
	m_overlay=m_spScope->createRecord(m_spRenderLayout);

	m_asViewer.viewer_layer(m_underlay)=0;
	m_asViewer.viewer_spatial(m_underlay)=0;
	m_asViewer.timestep(m_underlay)=1.0;

	m_asViewer.viewer_layer(m_render)=1;
	m_asViewer.viewer_spatial(m_render)=1;
	m_asViewer.timestep(m_render)=1.0;

	m_asViewer.viewer_layer(m_overlay)=2;
	m_asViewer.viewer_spatial(m_overlay)=0;
	m_asViewer.timestep(m_overlay)=1.0;
}

void ObjectViewer::bind(sp<DrawI> spDrawI)
{
	m_spDrawI=spDrawI;

	m_asViewer.render_draw(m_underlay)=m_spDrawI;
	m_asViewer.render_draw(m_render)=m_spDrawI;
	m_asViewer.render_draw(m_overlay)=m_spDrawI;

	if(!m_started)
	{
		start();
		m_started=TRUE;
	}
}

void ObjectViewer::start(void)
{
	sp<Layout> spEventLayout=m_spScope->lookupLayout(FE_EVENT_LAYOUT);
	FEASSERT(spEventLayout.isValid());

	sp<HandlerI> spHandlerI=m_spCameraControllerI;
	FEASSERT(spHandlerI.isValid());

	insertEventHandler(spHandlerI);
}

void ObjectViewer::bind(sp<WindowI> spWindowI)
{
	m_spDrawI->view()->setWindow(spWindowI);

	//* TODO should use signaler/scope from WindowI, not our signaler bind
//	sp<EventContextI> spEventContextI=spWindowI->getEventContextI();
}

void ObjectViewer::insertBeatHandler(sp<HandlerI> spHandlerI)
{
	m_spEventSignaler->insert(spHandlerI,m_spBeatLayout);
}

void ObjectViewer::insertEventHandler(sp<HandlerI> spHandlerI)
{
	sp<Layout> spLayout=
			m_spScope->lookupLayout(FE_EVENT_LAYOUT);
	m_spEventSignaler->insert(spHandlerI, spLayout);
}

void ObjectViewer::insertDrawHandler(sp<HandlerI> spHandlerI)
{
	m_spDrawSignaler->insert(spHandlerI,m_spRenderLayout);
}

void ObjectViewer::render(Real a_time,Real a_timestep)
{
	if(m_spEventSignaler.isValid())
	{
		m_spEventSignaler->signal(m_beat);
	}

	if(m_spDrawSignaler.isNull())
	{
		return;
	}

	m_asViewer.time(m_underlay)=a_time;
	m_asViewer.time(m_render)=a_time;
	m_asViewer.time(m_overlay)=a_time;

	m_asViewer.timestep(m_underlay)=a_timestep;
	m_asViewer.timestep(m_render)=a_timestep;
	m_asViewer.timestep(m_overlay)=a_timestep;

	m_spCameraPerspective=m_spCameraControllerI->camera();

	//* `Measure Frame Rate'
	const Real ms=m_ticker.timeMS();
	m_frameTotal+=ms-m_frameMS;
	m_frameCount++;
	if(m_frameTotal>FRAMECOUNT_PERIOD)
	{
		m_frameRate=m_frameCount*1e3f/m_frameTotal;
		m_frameTotal=0.0f;
		m_frameCount=0;
	}
	m_frameMS=ms;

	//* `Viewport'
	sp<ViewI> spViewI=m_spDrawI->view();
	sp<WindowI> spWindowI=spViewI->window();
	Box2i viewport=spWindowI->geometry();
	viewport[0]=0;
	viewport[1]=0;
	spViewI->setViewport(viewport);

	//* `Perspective Camera'

	const Real distance=m_spCameraControllerI->distance();
	const Real farplane=fe::maximum(1.0,fe::minimum(1e6,10.0*distance));
	const Real nearplane=fe::maximum(0.1,1e-5*farplane);

	const Vector2 fov=m_spCameraPerspective->fov();
	Real fovy=fov[1];
	if(fov[0]/fov[1]>width(viewport)/Real(height(viewport)))
	{
		fovy=fov[0]*height(viewport)/Real(width(viewport));
	}
	m_spCameraPerspective->setFov(Vector2(0.0,fovy));

	m_spCameraPerspective->setPlanes(nearplane,farplane);

	//* `Ortho Camera'

	//* NOTE store perspective info in ortho camera for ViewI::project()
	m_spCameraOverlay->setCameraMatrix(
			m_spCameraPerspective->cameraMatrix());
	m_spCameraOverlay->setFov(Vector2(0.0,fovy));
	m_spCameraOverlay->setPlanes(nearplane,farplane);

	//* `Layer 0: Underlay'
	spViewI->setCamera(m_spCameraOverlay);
	spViewI->use(ViewI::e_ortho);

	m_spDrawSignaler->signal(m_underlay);

	//* `Layer 1: Spatial'

	spViewI->setCamera(m_spCameraPerspective);
	spViewI->use(ViewI::e_perspective);

	m_spDrawSignaler->signal(m_render);

	//* `Layer 2: Overlay'
	spViewI->setCamera(m_spCameraOverlay);
	spViewI->use(ViewI::e_ortho);

#if FE_OV_DISPLAY_FRAMERATE
	const Color pink(1.0f,0.5f,0.5f);
	const SpatialVector corner(10.0f,10.0f,0.0f);

	String text;
	text.sPrintf("%.0f FPS",frameRate());
	m_spDrawI->drawAlignedText(corner,text,pink);
#endif

	m_spDrawSignaler->signal(m_overlay);

	spViewI->setCamera(m_spCameraPerspective);
	spViewI->use(ViewI::e_perspective);

	//* `Flush Drawing'
	m_spDrawI->flush();
}

} /* namespace ext */
} /* namespace fe */
