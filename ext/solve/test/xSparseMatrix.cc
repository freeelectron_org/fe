/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve/solve.h"

#define ELEMENTS	7

using namespace fe;
using namespace fe::ext;

void write(const std::vector<SpatialVector> &a_v, const std::string &a_filename)
{
	FILE *fp = fopen(a_filename.c_str(), "w");

	fprintf(fp, "%d\n", (int)a_v.size());
	for(unsigned int i = 0; i < a_v.size(); i++)
	{
		fprintf(fp, "%g %g %g\n", a_v[i][0], a_v[i][1], a_v[i][2]);
	}

	fclose(fp);
}

void read(std::vector<SpatialVector> &a_v, const std::string &a_filename)
{
	std::ifstream infile(a_filename.c_str());

	int m;
	infile >> m;

	a_v.resize(m);

	for(int i = 0; i < m; i++)
	{
		infile >> a_v[i][0];
		infile >> a_v[i][1];
		infile >> a_v[i][2];
	}

	infile.close();
}

void test_incompleteSqrt(void)
{
#if 0
	sp<UpperTriangularBCRS> spT(new UpperTriangularBCRS());

	spT->read("lhs_dump");
	unsigned int n = spT->rowptr().size()-1;

	spT->writeDense("input_dense");

	arma::mat arma_mat(n*3,n*3);

	arma_mat.load("input_dense");

	arma_mat.save("arma_dense", arma::arma_ascii);

	arma::mat arma_chol = arma::chol(arma_mat);

	arma_chol.save("arma_chol", arma::raw_ascii);

#if 0
	spT->incompleteSqrt();

	spT->writeDense("bloc_icc");

	spT->readDense("input_dense", n, n);
	//spT->writeDense("output_dense2");

	spT->incompleteSqrt();
	spT->writeDense("bloc_icc2");

	std::vector<SpatialVector> rhs;
	read(rhs, "rhs_dump");

	std::vector<SpatialVector> x(rhs.size());
	spT->backSolve(x, rhs);

	sp<UpperTriangularBCRS> spA(new UpperTriangularBCRS());
	spA->read("lhs_dump");
	std::vector<SpatialVector> y(rhs.size());
	spA->multiply(y, x);

	write(y, "rhs_cooked");
#endif



	spT->read("lhs_dump");
	spT->writeStructure("lhs_structure");

	spT->readDense("input_dense", n, n);
	spT->writeStructure("lhs_structure_dense");

	spT->readDense("arma_chol", n, n);
	spT->writeStructure("lhs_structure_chol");

	spT->readDense("input_dense", n, n);
	spT->incompleteSqrt();
	spT->writeStructure("lhs_structure_icc");

	spT->read("lhs_dump");
	SemiImplicit::CompileMatrix cm;
	cm.setRows(n);
	for(unsigned int i = 0; i < n; i++)
	{
		for(unsigned int k = spT->rowptr()[i]; k < spT->rowptr()[i+1]; k++)
		{
			cm.entry(i, spT->colind()[k]);
		}
	}
	cm.symbolicFill();
	spT->clear();
	for(unsigned int i = 0; i < n; i++)
	{
		spT->startrow();
		for(SemiImplicit::CompileMatrix::t_row::iterator i_row =
			cm.row(i).begin(); i_row != cm.row(i).end(); i_row++)
		{
			SpatialMatrix zero;
			setAll(zero, 0.0);
			spT->insert(i_row->first, zero);
		}
	}
	spT->done();
	spT->writeStructure("lhs_structure_copy");

#endif
}

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		{
			SparseMatrix<F32>	A(ELEMENTS,ELEMENTS);
			DenseVector<F32>	x(ELEMENTS);

			setIdentity(A);
			setAll(x,1.0f);

			A(1,2)=2.0f;
			x[4]=4.0f;

			DenseVector<F32> b=A*x;

			feLog("A=\n%s\n",print(A).c_str());
			feLog("x=%s\n",print(x).c_str());
			feLog("b=%s\n",print(b).c_str());

			// 1 3 1 1 4 1 1
			DenseVector<F32>	expected(ELEMENTS);

			setAll(expected,1.0f);
			expected[1]=3.0f;
			expected[4]=4.0f;

			feLog("expected=%s\n",print(expected).c_str());
			UNIT_TEST(equivalent(b,expected,1e-3f));
		}

		{
			UpperTriangularBCRS<Real> A;
			//FullBCRS A;
			A.startrow();

			SpatialMatrix block;
			setAll(block, 0.0);

			block(0,0) = 1.0;
			block(1,1) = 2.0;
			block(2,2) = 3.0;

			A.insert(0,block);

			block(0,1) = 2.3;
			block(0,2) = 2.4;
			block(1,0) = -3.4;
			block(1,2) = 3.4;
			block(2,0) = -2.4;
			block(2,1) = -5.4;

			A.insert(1,block);
			A.insert(2,block);

			A.startrow();

			A.insert(1,block);
			A.insert(2,block);

			A.startrow();

			block(2,0) = -20.4;
			A.insert(2,block);

			A.done();

			std::vector<SpatialVector> x,b;

			x.push_back(SpatialVector(2.0,3.0,4.0));
			x.push_back(SpatialVector(2.0,3.0,4.0));
			x.push_back(SpatialVector(2.0,3.0,4.0));

			b.resize(3);

			A.multiply(b,x);

			Real sum = 0;
			for(unsigned int i=0; i < b.size(); i++)
			{
				feLog("%f %f %f\n", b[i][0], b[i][1], b[i][2]);
				sum += b[i][0];
				sum += b[i][1];
				sum += b[i][2];
			}
			feLog("sum=%f\n", sum);
			UNIT_TEST(isZero(sum - (Real)90.1));
		}

		{
			test_incompleteSqrt();
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(3);
	UNIT_RETURN();
}
