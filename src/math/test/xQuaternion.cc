/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;
	const F32 margin=1e-3f;

	try
	{
		Quaternionf q0;
		fe::set(q0);
		feLog("q0 %s\n",print(q0).c_str());
		UNIT_TEST(equivalent(q0,Quaternionf(0.0f,0.0f,0.0f,1.0f),margin));

		Quaternionf q1;
		fe::set(q1,0.5f,e_yAxis);
		feLog("q1 %s\n",print(q1).c_str());
		UNIT_TEST(equivalent(q1,Quaternionf(0.0f,0.247f,0.0f,0.969f),margin));

		F32 angle;
		Vector3f axis;
		q1.computeAngleAxis(angle,axis);
		feLog("%g about %s\n",angle,print(axis).c_str());
		UNIT_TEST(fabsf(angle-0.5f)<margin);
		UNIT_TEST(axis[1]>0.999f);

		Quaternionf q2;
		slerp(q2,q0,q1,0.3f);
		feLog("q2 %s\n",print(q2).c_str());
		UNIT_TEST(equivalent(q2,Quaternionf(0.0f,0.075f,0.0f,0.997f),margin));

		q2=inverse(q1);
		feLog("q2=-q1  %s\n",print(q2).c_str());
		invert(q2);
		UNIT_TEST(equivalent(q1,q2,margin));

		q2=angularlyScaled(q2,1.5f);
		q2.computeAngleAxis(angle,axis);
		feLog("q2*=1.5  %g about %s\n",angle,print(axis).c_str());
		UNIT_TEST(fabsf(angle-0.75f)<margin);
		UNIT_TEST(axis[1]>0.999f);

		Matrix3x4f m0;
		fe::setIdentity(m0);
		UNIT_TEST(equivalent(q0,Quaternionf(m0),margin));

		m0=q1;
		feLog("m0 %s\n",print(m0).c_str());
		feLog("as quat  %s\n",print(Quaternionf(m0)).c_str());
		UNIT_TEST(equivalent(q1,Quaternionf(m0),margin));

		Quaternionf qx;
		Quaternionf qy;
		Quaternionf qz;
		angle=45.0f*degToRad;
		fe::set(qx,angle,e_xAxis);
		fe::set(qy,angle,e_yAxis);
		fe::set(qz,angle,e_zAxis);
		Quaternionf q3=qx*qx * qz*qz * inverse(qx)*inverse(qx) * qy*qy;
		feLog("q3 %s\n",print(q3).c_str());
		UNIT_TEST(equivalent(q3,Quaternionf(0.0f,0.0f,0.0f,1.0f),margin));

		fe::set(q1,1.0f,e_xAxis);
		fe::set(q2,0.5f,e_yAxis);
		q3=q1*q2;
		feLog("q3=q1*q2  %s\n",print(q3).c_str());
		UNIT_TEST(equivalent(q3,Quaternionf(0.4645f,0.2171f,0.1186f,0.8503f),
				margin));

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(13);
	UNIT_RETURN();
}
