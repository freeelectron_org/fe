/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "geometry/geometry.h"

using namespace fe;
using namespace fe::ext;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		feLog("matrix A = some transform\n");
		SpatialTransform A;
		setIdentity(A);
		rotate(A,30.0f*degToRad,e_xAxis);
		translate(A,SpatialVector(100.0f,200.0f,300.0f));
		rotate(A,60.0f*degToRad,e_yAxis);

		MatrixPower<SpatialTransform> matrixPower;

		feLog("matrix B = A^2.3\n");
		SpatialTransform B;
		matrixPower.solve(B,A,2.3f);

		feLog("matrix C = A^6.7\n");
		SpatialTransform C;
		matrixPower.solve(C,A,6.7f);

		feLog("matrix D = B*C\n");
		SpatialTransform D=B*C;

		feLog("matrix E = A^9 (8 multiplies)\n");
		SpatialTransform E=A;
		for(U32 m=1;m<9;m++)
		{
			E*=A;
		}

		feLog("matrix F = D-E\n");
		SpatialTransform F=D-E;

		F32 sum=0.0f;
		for(U32 m=0;m<width(F);m++)
		{
			for(U32 n=0;n<height(F);n++)
			{
				sum+=fabs(F(m,n));
			}
		}

		feLog("\nA\n%s\n",print(A).c_str());
		feLog("\nB\n%s\n",print(B).c_str());
		feLog("\nC\n%s\n",print(C).c_str());
		feLog("\nD\n%s\n",print(D).c_str());
		feLog("\nE\n%s\n",print(E).c_str());
		feLog("\nF\n%s\n",print(F).c_str());

		feLog("\nF component sum = %.6G\n",sum);
		UNIT_TEST(sum<0.011f);

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(2);
	UNIT_RETURN();
}
