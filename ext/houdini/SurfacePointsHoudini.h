/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfacePointsHoudini_h__
#define __surface_SurfacePointsHoudini_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini Point Cloud

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfacePointsHoudini:
	public SurfaceCurves,
	public CastableAs<SurfacePointsHoudini>
{
	public:
							SurfacePointsHoudini(void);
virtual						~SurfacePointsHoudini(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* Houdini specific
		void				setGdp(const GU_Detail* a_pGdp)
							{
								m_pGdp=a_pGdp;
							}
const	GU_Detail*			gdp(void) const					{ return m_pGdp; }

		void				setSubIndex(I32 a_subIndex)
							{	m_subIndex=a_subIndex; }
		I32					subIndex(void) const
							{	return m_subIndex; }

		void				setGroup(String a_group)
							{	m_group=a_group; }
		String				group(void) const
							{	return m_group; }

	protected:

virtual	void				cache(void);

	private:

const	GU_Detail*			m_pGdp;
		String				m_group;
		I32					m_subIndex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfacePointsHoudini_h__ */
