import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs =   forge.corelibs + [
                "fexSignalLib" ]

    tests = [   "xSignal" ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + 'Exe'], deplibs)

    forge.tests += [
        ("xSignal.exe",         "",                 None,       None) ]

