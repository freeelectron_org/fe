import os
import sys
import time
import sys
import getopt
import string
import re
#import thread
import subprocess

os.putenv("FE_E_ATTACH","0")
#os.putenv("FE_E_BACKTRACE","0")

forge = sys.modules["forge"]

debugging=0
validating=0
profile=0
optimize=0
help=0
cmdtest=""

if os.getenv('FE_VALGRIND'):
    debugging=1

re_exception_thrown = re.compile(r'^.*fatal(.*)$')

def cprint(color, bold, string):
    forge.color_on(bold,color)
    sys.stdout.write(string)
    forge.color_off()

def runtest(testprogram,bCheckpath):
    test = Test(testprogram,testprogram,bCheckpath,0)
    test.start()
    test.wait()
    test.time_start()
    test.time_wait()
    test.valgrind_start()
    test.valgrind_wait()
    test.callgrind_start()
    test.callgrind_wait()
    return test.process()

def print_time(time):
    color = forge.GREEN
    if time<0.1:
        cprint(color, 0, "  .  ")
        return
    if time>5:
        color = forge.RED
    elif time>1:
        color = forge.YELLOW
    cprint(color, 1, " %4.1f" % time)

def print_kilo(color,count,digits):
    prefix = " kMGT"
    index = 0
    limit = pow(10, digits) - 1
    while count>limit:
        index = index + 1
        count *= 0.001
    val_fmt = "%0" + str(digits) + "d" + prefix[index]
    cprint(color, 1, val_fmt % count)

def print_percent(percent,low,medium,high):
    color = forge.GREEN
    if percent<low:
        cprint(color, 0, "  .  ")
        return
    if percent>high:
        color = forge.RED
    elif percent>medium:
        color = forge.YELLOW
    cprint(color, 1, " %3.1f" % percent)

class Test:
    def __init__(self, testprogram, valgrindprogram, bCheckpath, level):
        binary_suffix=os.environ["FE_BINARY_SUFFIX"]
        testprogram = testprogram.replace(".exe", binary_suffix + ".exe")

        self.testprogram = testprogram
        self.bCheckpath = bCheckpath
        self.popen = None
        self.valgrindprogram = valgrindprogram

        output_test = "output/test"
        if not os.path.exists(output_test):
            os.makedirs(output_test)

        commandname = self.testprogram.split()[0]
        self.logfilename = output_test + "/tmp.{cmd}.{level}.{id}.log".format(
            cmd=commandname, level=level, id=id(self))

    def start(self):
        fullpath = os.path.join(forge.libPath, self.testprogram)
#       self.output = os.popen(fullpath, "r", -1)

        self.logfile = open(self.logfilename, "w")

        self.popen = subprocess.Popen(forge.cleanup_command(fullpath),
                shell=True,bufsize=-1,
#               stdout=subprocess.PIPE,
                stdout=self.logfile,
                stderr=subprocess.STDOUT)

    def wait(self):
        # consume all of the test output:
        self.log_output = ""
        self.exception_thrown = 0
        self.timed_out = 0
        self.err_msg = ''
        self.ec = None

        # new way sends log to temp file
        try:
            self.ec = self.popen.wait(timeout=30)
        except subprocess.TimeoutExpired:
            self.popen.kill()
            self.timed_out = 1

        self.logfile.close()
        self.logfile = open(self.logfilename, "r")
        self.log_output += self.logfile.read()
        self.logfile.close()

        try:
            os.remove(self.logfilename)
        except PermissionError:
            cprint(forge.YELLOW, 1,
                    "Permission error removing log file '%s'\n" %
                    self.logfilename)

        if self.timed_out:
            self.log_output += "\nTIMED OUT\n"
        return

        # old way used PIPE, which locks up if too long
        try:
            line = self.popen.stdout.readline().decode()
        except Exception as e:
            print(e)

        while line:
            m = re_exception_thrown.match(line)
            if m != None:
                self.err_msg = str(m.group(1)).strip()
                self.exception_thrown = 1
            #log.write("%s" % (line))
            self.log_output += line

            try:
                line = self.popen.stdout.readline().decode()
            except Exception as e:
                print(e)

        # close the pipe
        # return value is the signal (lowbyte) + exit value (high byte)
#       self.ec = self.output.close()
        self.ec = self.popen.wait()
        self.popen.stdout.close()

    def process(self):
        testprogram = self.testprogram
        bCheckpath = self.bCheckpath
        output = self.popen.stdout
        log_output = self.log_output
        exception_thrown = self.exception_thrown
        timed_out = self.timed_out
        err_msg = self.err_msg
        ec = self.ec
        forge.testlog.write("\n--------- TEST OUTPUT FOR %s ---------\n" % (testprogram))
        forge.testlog.write(log_output)
        signal=0
        exitval=0
        if ec != None:
            if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
                exitval=ec
            else:
#               signal= (int(str(ec))&255)
#               exitval= ((int(str(ec))>>8)&255)
                if ec < 0:
                    signal = -ec
                else:
                    exitval = ec
#       sys.stdout.write("ec " + str(ec) + " signal " + str(signal) + " exitval " + str(exitval) + "\n")

        retval = "UNSPECIFIED"

        fullpath = os.path.join(forge.libPath, testprogram.split()[0])

        # print result based on exit status:
        if exception_thrown == 1:
            retval = "EXCP"
            color = forge.WHITE
            ec = 1
        elif timed_out == 1:
            retval = "TIME"
            color = forge.MAGENTA
            ec = 1
            forge.faillist.append(testprogram)
        elif signal == 0 and exitval == 0:
            retval = "PASS"
            color = forge.GREEN
        elif bCheckpath and not os.path.exists(fullpath):
            retval = "MISS"
            color = forge.YELLOW
        else:
            retval = "FAIL"
            color = forge.RED

            # why does this corespond with exit return value?
    #       err_msg = os.strerror(ec) + ' (' + str(ec) + ')'

            if forge.fe_os == "FE_LINUX":
                if signal == 6: # abort signal
                    err_msg = ' abort'
                if signal == 11:
                    err_msg = ' segmentation fault'

            forge.faillist.append(testprogram)

        width = 31
        if len(testprogram) <= width:
            pad = ( width - (len(testprogram)) ) * '.'
            sys.stdout.write(testprogram + pad + '[')
        else:
            sys.stdout.write(testprogram + "\n" + width * '.' + '[')

        cprint(color, 1, retval)
        sys.stdout.write("]")
        sys.stdout.write(" " + ("%1d" % signal) + ' ' + ("%1d " % exitval))
        if err_msg != '':
            sys.stdout.write(err_msg)
        sys.stdout.flush()

        if self.ec == None and profile == 1 and forge.fe_os == "FE_LINUX":
            print_time(self.realtime)
            print_time(self.usertime)
            print_time(self.systime)
            print_percent(self.I1,  0.1, 0.7, 1.0)
            print_percent(self.L2i, 0.1, 0.5, 1.0)
            print_percent(self.D1,  0.1, 5.5, 6.0)
            print_percent(self.L2d, 0.1, 2.0, 3.0)
            print_percent(self.L2,  0.1, 0.7, 1.4)

        if self.ec == None and debugging == 1 and forge.fe_os == "FE_LINUX":
            val_rv = "UNSPECIFIED"

            fullpath = os.path.join(forge.libPath, self.valgrind.split()[0])

            # print result based on exit status:
            if self.val_ec == None:
                if self.errors > 0:
                    val_rv = "FAIL"
                    color = forge.RED
                elif self.leak > 0 or self.indirect > 0 or self.possible > 0:
                    val_rv = "LEAK"
                    color = forge.YELLOW
                else:
                    val_rv = "PASS"
                    color = forge.GREEN
            else:
                val_rv = "FAIL"
                color = forge.RED

            sys.stdout.write('[')
            cprint(color, 1, val_rv)
            sys.stdout.write("] ")

            if self.errors > 0:
                color = forge.RED
            else:
                color = forge.GREEN

            cprint(color, 1, "%01d" % self.errors)
            sys.stdout.write(" ")

            if self.leak > 0:
                color = forge.RED
            else:
                color = forge.GREEN

            print_kilo(color,self.leak,4)
            sys.stdout.write(" ")

            if self.indirect > 0:
                color = forge.YELLOW
            else:
                color = forge.GREEN

            print_kilo(color,self.indirect,4)
            sys.stdout.write(" ")

            if self.possible > 0:
                color = forge.YELLOW
            else:
                color = forge.GREEN

            print_kilo(color,self.possible,4)
            sys.stdout.write(" ")

            if self.reachable > 1000:
                color = forge.MAGENTA
            elif self.reachable > 0:
                color = forge.BLUE
            else:
                color = forge.GREEN

            print_kilo(color,self.reachable,4)
            sys.stdout.write(" ")

            if self.suppressed > 9999:
                color = forge.MAGENTA
            elif self.suppressed > 0:
                color = forge.BLUE
            else:
                color = forge.GREEN

            print_kilo(color,self.suppressed,4)

            sys.stdout.write("\n")
        else:
            sys.stdout.write("\n")

        return retval

    def time_start(self):
        if self.ec == None and profile == 1 and forge.fe_os == "FE_LINUX":
            self.timeprogram = '/usr/bin/time -f "TIME: %e %U %S" ' + self.testprogram
            (input,self.output) = os.popen4(self.timeprogram, "r", -1)
            return self.output

    def time_wait(self):
        if self.ec == None and profile == 1 and forge.fe_os == "FE_LINUX":
            exit
            line = self.output.readline().decode()
            time_expr = re.compile(r'^TIME: ([0-9\.]+) ([0-9\.]+) ([0-9\.]+)$')
            self.realtime = 0
            self.usertime = 0
            self.systime = 0
            while line:
                line = self.output.readline().decode()

                m = time_expr.match(line)
                if m != None:
                    self.realtime = float(m.group(1))
                    self.usertime = float(m.group(2))
                    self.systime = float(m.group(3))

            self.time_ec = self.output.close()

    def valgrind_start(self):
        if self.ec == None and debugging == 1 and forge.fe_os == "FE_LINUX":
            #self.valgrind = 'valgrind --trace-children=yes --suppressions=test.supp --leak-check=yes --logfile-fd=1 --logsocket=127.0.0.1:5555 ' + self.valgrindprogram
            supp = os.path.join(forge.rootPath, 'test', 'test.supp')
            fullpath = os.path.join(forge.libPath, self.valgrindprogram)
            self.valgrind = 'valgrind --trace-children=yes --suppressions=%s --leak-check=yes --log-fd=1 --num-callers=10 %s' % (supp, fullpath)
            self.output = os.popen(self.valgrind, "r", -1)
            return self.output

    def valgrind_wait(self):
        if self.ec == None and debugging == 1 and forge.fe_os == "FE_LINUX":
            line = self.output.readline().decode()
            error_expr = re.compile(r'^.*ERROR SUMMARY:\s+(\d+).*$')
            lost_expr = re.compile(r'^.*definitely lost:\s+(\d+).*$')
            indirect_expr = re.compile(r'^.*indirectly lost:\s+(\d+).*$')
            possible_expr = re.compile(r'^.*possibly lost:\s+(\d+).*$')
            reachable_expr = re.compile(r'^.*still reachable:\s+(\d+).*$')
            supp_expr = re.compile(r'^.*suppressed:\s+(\d+).*$')
            self.errors = 0
            self.leak = 0
            self.indirect = 0
            self.possible = 0
            self.reachable = 0
            self.suppressed = 0
            while line:
                line = self.output.readline().decode()
                #sys.stdout.write(line)

                m = error_expr.match(line)
                if m != None:
                    self.errors += int(m.group(1))

                m = lost_expr.match(line)
                if m != None:
                    self.leak += int(m.group(1))

                m = indirect_expr.match(line)
                if m != None:
                    self.indirect += int(m.group(1))

                m = possible_expr.match(line)
                if m != None:
                    self.possible += int(m.group(1))

                m = reachable_expr.match(line)
                if m != None:
                    self.reachable += int(m.group(1))

                m = supp_expr.match(line)
                if m != None:
                    self.suppressed += int(m.group(1))

            self.val_ec = self.output.close()

    def callgrind_start(self):
        if self.ec == None and profile == 1 and forge.fe_os == "FE_LINUX":
            self.valgrind = 'callgrind --simulate-cache=yes ' + self.valgrindprogram + ' 2>&1'
            self.output = os.popen(self.valgrind, "r", -1)
            return self.output

    def callgrind_wait(self):
        if self.ec == None and profile == 1 and forge.fe_os == "FE_LINUX":
            line = self.output.readline().decode()
            I1_expr = re.compile(r'^.*I1  miss rate:\s+([0-9\.]+).*$')
            L2i_expr = re.compile(r'^.*L2i miss rate:\s+([0-9\.]+).*$')
            D1_expr = re.compile(r'^.*D1  miss rate:\s+([0-9\.]+).*$')
            L2d_expr = re.compile(r'^.*L2d miss rate:\s+([0-9\.]+).*$')
            L2_expr = re.compile(r'^.*L2 miss rate:\s+([0-9\.]+).*$')
            self.I1 = 0
            self.L2i = 0
            self.D1 = 0
            self.L2d = 0
            self.L2 = 0
            while line:
                line = self.output.readline().decode()
#               sys.stdout.write(line)

                m = I1_expr.match(line)
                if m != None:
                    self.I1 += float(m.group(1))

                m = L2i_expr.match(line)
                if m != None:
                    self.L2i += float(m.group(1))

                m = D1_expr.match(line)
                if m != None:
                    self.D1 += float(m.group(1))

                m = L2d_expr.match(line)
                if m != None:
                    self.L2d += float(m.group(1))

                m = L2_expr.match(line)
                if m != None:
                    self.L2 += float(m.group(1))

            self.val_ec = self.output.close()


def recursetest(test, args, valgrind_args, nest, level):
    if args == '':
        pad = ''
    else:
        pad = ' '
    if valgrind_args == None:
        valgrind_args = args
    t = Test(test + ' ' + args + pad, test + ' ' + valgrind_args + pad, True, level)
    tests = [ t ]
    t.start()
    t.child = None
    if nest:
        (c, ts) = recursetest(nest[0], nest[1], nest[2], nest[3], level + 1)
        t.child = c
        tests += ts
    t.wait()
    return (t, tests)

def time_pass(t):
    t.time_start()
    if t.child:
        time_pass(t.child)
    t.time_wait()

def valgrind_pass(t):
    t.valgrind_start()
    if t.child:
        valgrind_pass(t.child)
    t.valgrind_wait()

def callgrind_pass(t):
    t.callgrind_start()
    if t.child:
        callgrind_pass(t.child)
    t.callgrind_wait()

def run_tests(target):
    forge.testlog = open(target.name, "a")

    forge.testlog.write("\n==========================================================\n")
    forge.testlog.write("=   TEST RUN AT %s\n" % (time.ctime(time.time())))
    forge.testlog.write("==========================================================\n")

    cprint(forge.CYAN, 1,"Executable Tests:              ")
    cprint(forge.BLUE, 0,"norm  sg ex")
    if forge.fe_os == "FE_LINUX" and profile == 1:
        cprint(forge.BLUE, 0," real user sys   I1  L2i D1  L2d L2")
    if forge.fe_os == "FE_LINUX" and debugging == 1:
        cprint(forge.BLUE, 0," grnd er leak  indir poss  reach supp")
    cprint(forge.BLUE, 0,"\n")

    for (test, args, valgrind_args, nest) in forge.tests:
        (c, tests) = recursetest(test, args, valgrind_args, nest, 0)
        time_pass(c)
        valgrind_pass(c)
        callgrind_pass(c)
        for t in tests:
            result = t.process()
            for i in range(len(forge.testcodes)):
                if result == forge.testcodes[i]:
                    forge.testresults[i] = forge.testresults[i] + 1
                    break
            else:
                forge.testresults[len(forge.testcodes) + 1] = forge.testresults[len(forge.testcodes) + 1] + 1

    cprint(forge.CYAN, 1, "Summary:\n")

    numtests= 0

    for i in range(len(forge.testresults)):
        numtests = numtests + forge.testresults[i]

    for i in range(len(forge.testcodes)):

        if numtests != 0:
            percent = 100*forge.testresults[i]/numtests
        else:
            percent = 0

        print("%s:   \t" % (forge.testcodes[i])),

        if forge.testresults[i] == 0:
            sys.stdout.write(str(forge.testresults[i]))
        else:
            cprint(forge.testcolors[i], 1, str(forge.testresults[i]))

        print(" (%d%s)" % (percent, '%'))

    cprint(forge.CYAN, 1, "Platform: ")
    cprint(forge.CYAN, 0, forge.api)
    if debugging:
        cprint(forge.CYAN, 0, " debug")
    cprint(forge.CYAN, 0, "\n")

    cprint(forge.CYAN, 0, "%s\n" % (time.ctime(time.time())))
    cprint(forge.CYAN, 1, "checkin one liner:\n")
    cprint(forge.WHITE, 0, forge.api)
    for i in range(len(forge.testcodes)):
        cprint(forge.WHITE, 0, " %s:%d" % (forge.testcodes[i], forge.testresults[i]))
    cprint(forge.WHITE, 0, " failed:")
    for f in forge.faillist:
        f = f.split()[0]
        cprint(forge.WHITE, 0, " %s" % (f))

    cprint(forge.WHITE, 0, "\n")

    forge.testlog.close()

    fail_count = len(forge.faillist)
    if fail_count:
        sys.exit(fail_count)

def setup(module):

    forge.testcodes = ["PASS","MISS","FAIL","EXCP","TIME"]
    forge.testcolors = [forge.GREEN, forge.YELLOW, forge.RED, forge.WHITE, forge.MAGENTA]
    forge.faillist = []
    forge.testresults = []
    for i in range(len(forge.testcodes)):
        forge.testresults.append(0)
        forge.testresults.append(0)

    output_file = forge.FileTarget( os.path.join( module.modPath, 'log.txt' ) )
    output_file.AppendOverrideRule( ".*", run_tests )
    output_file.module = module

    # The exe tests, on the other hand, should already by registered (they are
    # all part of other modules) and so we just need to use the correct
    # pre-registered name:
    # For a blind test, do not try to build missing binaries
    if not "FE_BLIND_TEST" in os.environ or os.environ["FE_BLIND_TEST"] == "0":
        for (test, args, valgrindargs, next) in forge.tests:
            if test[-4:] == ".exe":
                temp = test[:-4] + "Exe"
                output_file.AddDep( forge.targetRegistry[temp] )

    forge.targetRegistry["unit"] = output_file
