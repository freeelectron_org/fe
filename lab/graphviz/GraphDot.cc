/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <graphviz/graphviz.pmh>

#define FE_GDT_DEBUG	FALSE

//* NOTE seems to have lost access from Agnode_t to Agnodeinfo_t
#define FE_GDT_NEW_GV	TRUE	//* somewhere between 2.26.3 to 2.38.0

namespace fe
{
namespace ext
{

GraphDot::GraphDot(void):
	m_pGvc(NULL),
	m_pGraph(NULL)
{
}

GraphDot::~GraphDot()
{
	reset();
}

void GraphDot::initialize(void)
{
	m_spDelegate=registry()->create("ImageI.ImageIL");
}

void GraphDot::reset(void)
{
	if(m_pGvc)
	{
		gvFreeLayout(m_pGvc,m_pGraph);
		agclose(m_pGraph);
		m_pGraph=NULL;
		gvFreeContext(m_pGvc);
		m_pGvc=NULL;
	}
}

I32 GraphDot::interpretSelect(String a_source)
{
	reset();

	m_pGvc=gvContext();
	m_pGraph=agmemread(const_cast<char*>(a_source.c_str()));
	gvLayout(m_pGvc,m_pGraph,"dot");

	if(m_spDelegate.isValid())
	{
		render("png");
	}

	incrementSerial();
	return 0;
}

void GraphDot::render(String a_format)
{
	char* pResult=NULL;
	U32 length=0;

	gvRenderData(m_pGvc,m_pGraph,const_cast<char*>(a_format.c_str()),
			&pResult,&length);

	if(!m_spDelegate->interpretSelect(pResult,length))
	{
		m_spDelegate=NULL;
		return;
	}

	const U32 rasterWidth=m_spDelegate->width();
	const U32 rasterHeight=m_spDelegate->height();

#if FE_GDT_NEW_GV
	//* TODO
	const I32 urX=0;
	const I32 urY=0;
	const I32 llX=0;
	const I32 llY=0;
#else
	const I32 urX=m_pGraph->u.bb.UR.x;
	const I32 urY=m_pGraph->u.bb.UR.y;
	const I32 llX=m_pGraph->u.bb.LL.x;
	const I32 llY=m_pGraph->u.bb.LL.y;
#endif

	m_offsetX=5;
	m_offsetY=5;
	m_rescaleX=(rasterWidth-1-2*m_offsetX)/Real(urX-1);
	m_rescaleY=(rasterHeight-1-2*m_offsetY)/Real(urY-1);

#if FE_GDT_DEBUG
	feLog("raster %d %d\n",rasterWidth,rasterHeight);
	feLog("graph box %d %d %d %d\n",llX,llY,urX,urY);

	for(Agnode_t* pNode=agfstnode(m_pGraph);pNode;
			pNode=agnxtnode(m_pGraph,pNode))
	{
		const I32 id=pNode->id;

#if FE_GDT_NEW_GV
		const char* name=pNode->base.data->name;
#else
		const char* name=pNode->name;
#endif

		Agnodeinfo_t& rInfo=pNode->u;
		const I32 x=rInfo.coord.x;
		const I32 y=rInfo.coord.y;
		const I32 ht=rInfo.ht;
		const I32 lw=rInfo.lw;
		const I32 rw=rInfo.rw;

		feLog("render node %d %s at %d %d ht %d lw %d rw %d\n",
				id,name,x,y,ht,lw,rw);

		const F64 width=rInfo.width;
		const F64 height=rInfo.height;
		const char* label=rInfo.label->text;
		feLog("  width %.6G height %.6G label %s\n",width,height,label);

		boxf& rBox=rInfo.bb;
		feLog("  box %.6G %.6G %.6G %.6G\n",
				rBox.LL.x,rBox.LL.y,
				rBox.UR.x,rBox.UR.y);
	}
#endif
}

U32 GraphDot::regionCount(void) const
{
	return agnnodes(m_pGraph);
}

String GraphDot::regionName(U32 a_regionIndex) const
{
	U32 index=0;
	for(Agnode_t* pNode=agfstnode(m_pGraph);pNode;
			pNode=agnxtnode(m_pGraph,pNode))
	{
		if(a_regionIndex==index++)
		{
#if FE_GDT_NEW_GV
			return pNode->base.data->name;
#else
			return pNode->name;
#endif
		}
	}

	return "";
}

Box2i GraphDot::regionBox(String a_regionName) const
{
	const int createFlag=0;
	Agnode_t* pNode=agnode(m_pGraph,
			const_cast<char*>(a_regionName.c_str()),createFlag);
	if(pNode)
	{
#if FE_GDT_NEW_GV
		//* TODO
		Agnodeinfo_t rInfo;
#else
		Agnodeinfo_t& rInfo=pNode->u;
#endif
		boxf& rBox=rInfo.bb;
		const I32 x=rBox.LL.x*m_rescaleX+m_offsetX;
		const I32 y=rBox.LL.y*m_rescaleY+m_offsetY;
		const I32 x2=rBox.UR.x*m_rescaleX+m_offsetX;
		const I32 y2=rBox.UR.y*m_rescaleY+m_offsetY;
		return Box2i(Vector2i(x,y),Vector2i(x2-x,y2-y));
	}

	return Box2i(Vector2i(0,0),Vector2i(0,0));
}

String GraphDot::pickRegion(I32 a_x,I32 a_y) const
{
	for(Agnode_t* pNode=agfstnode(m_pGraph);pNode;
			pNode=agnxtnode(m_pGraph,pNode))
	{
#if FE_GDT_NEW_GV
		//* TODO
		Agnodeinfo_t rInfo;
#else
		Agnodeinfo_t& rInfo=pNode->u;
#endif
		boxf& rBox=rInfo.bb;
		const I32 x=rBox.LL.x*m_rescaleX+m_offsetX;
		const I32 y=rBox.LL.y*m_rescaleY+m_offsetY;
		const I32 x2=rBox.UR.x*m_rescaleX+m_offsetX;
		const I32 y2=rBox.UR.y*m_rescaleY+m_offsetY;
		if(a_x>=x && a_x<x2 && a_y>=y && a_y<y2)
		{
#if FE_GDT_NEW_GV
			return pNode->base.data->name;
#else
			return pNode->name;
#endif
		}
	}

	return "";
}

} /* namespace ext */
} /* namespace fe */
