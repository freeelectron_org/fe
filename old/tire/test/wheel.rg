INFO 5
ATTRIBUTE name string "=unset"
ATTRIBUTE tire:model string
ATTRIBUTE tire:radius real "=0.22"
ATTRIBUTE tire:width real "=0.3"

LAYOUT tire
	name
	tire:radius
	tire:width
	tire:model

DEFAULTGROUP 1
RECORD PT_tire tire
	name "PT tire"
	tire:radius 0.22
	tire:model "Pacejka Default"
DEFAULTGROUP 1
RECORD BT_tire tire
	name "BT tire"
	tire:radius 0.22
	tire:model "Brush Default"
END
