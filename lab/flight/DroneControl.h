/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_DroneControl_h__
#define __flight_DroneControl_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Convert controller inputs in aircraft manipulations

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT DroneControl:
	virtual public HandlerI,
	public Initialize<DroneControl>
{
	public:

				DroneControl(void);
virtual			~DroneControl(void);

		void	initialize(void);

virtual	void	handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout);
virtual	void	handle(Record& a_signal);

	private:

		AsCatalog			m_asCatalog;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_DroneControl_h__ */
