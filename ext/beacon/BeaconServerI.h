/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconServerI_h__
#define __BeaconServerI_h__

namespace beacon
{

class FE_DL_EXPORT BeaconServerI:
	virtual public fe::Component,
	public fe::CastableAs<BeaconServerI>
{
public:
	/// @brief	Startup server process port with global dictionary port
	virtual bool start(uint16_t beaconPort, uint16_t gdPort) = 0;

	/// @brief	Stop server process
	virtual void stop() = 0;

	/// @brief	Returns true if the server process is still running
	virtual bool running() = 0;
};

}

#endif // __BeaconServerI_h__
