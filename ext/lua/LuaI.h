/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaI_h__
#define __lua_LuaI_h__

namespace fe
{
namespace ext
{

/// lua execution interface
class FE_DL_EXPORT LuaI:
	virtual public Component,
	public CastableAs<LuaI>
{
	public:
virtual	bool			preloadString(const String &text)					= 0;
virtual	Result			loadFile(const String &filename)					= 0;
virtual	Result			loadString(const String &text)						= 0;

virtual	void			set(const String &key, Record r_value)				= 0;
virtual	void			set(const String &key, sp<RecordArray> ra_value)	= 0;
virtual	void			set(const String &key, const String &value)			= 0;
virtual	void			set(const String &key, const Real &value)			= 0;
virtual	void			set(const String &key, Vector2f &value)				= 0;
virtual	void			set(const String &key, Vector2d &value)				= 0;
virtual	void			set(const String &key, Vector3f &value)				= 0;
virtual	void			set(const String &key, Vector3d &value)				= 0;
virtual	void			set(const String &key, Vector4f &value)				= 0;
virtual	void			set(const String &key, Vector4d &value)				= 0;
virtual	void			set(const String &key, sp<Layout> l_value)			= 0;
virtual	void			set(const String &key, sp<Component> spValue)		= 0;
virtual	void			set(const String &key, sp<Scope> spValue)			= 0;
virtual	void			set(const String &key, sp<StateCatalog> spValue)	= 0;

virtual	Record			getRecord(const String &key)						= 0;
virtual	String			getString(const String &key)						= 0;
virtual	Real			getReal(const String &key)							= 0;
virtual	Vector2f		getVector2f(const String &key)						= 0;
virtual	Vector2d		getVector2d(const String &key)						= 0;
virtual	Vector3f		getVector3f(const String &key)						= 0;
virtual	Vector3d		getVector3d(const String &key)						= 0;
virtual	Vector4f		getVector4f(const String &key)						= 0;
virtual	Vector4d		getVector4d(const String &key)						= 0;

virtual	bool			execute(void)										= 0;
virtual	bool			execute(const String &a_fnName)						= 0;
virtual	void			flush(void)											= 0;
virtual	void			alias(String a_aliasName,String a_trueName)			= 0;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __lua_LuaI_h__ */

