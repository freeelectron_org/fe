/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_MPT_DEBUG		FALSE
#define	FE_MPT_MT_DEBUG		FALSE

namespace fe
{
namespace ext
{

MapTree::MapTree(void):
	m_refinement(0)
{
#if FE_MPT_DEBUG
	feLog("MapTree::MapTree\n");
#endif
}

MapTree::~MapTree(void)
{
}

void MapTree::populate(
	const Vector3i* a_pElementArray,
	const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,
	const Vector2* a_pUVArray,
	const Color* a_pColorArray,
	const I32* a_pTriangleIndexArray,
	const I32* a_pPointIndexArray,
	const I32* a_pPrimitiveIndexArray,
	const I32* a_pPartitionIndexArray,
	U32 a_primitives,U32 a_vertices,
	const SpatialVector& a_center,F32 a_radius)
{
#if FE_MPT_DEBUG
	feLog("MapTree::populate primitives %d vertices %d\n",
			a_primitives,a_vertices);
#endif

	I32 maxPartitionIndex= -1;
	for(U32 vertexIndex=0;vertexIndex<a_vertices;vertexIndex++)
	{
		const I32 partitionIndex=a_pPartitionIndexArray[vertexIndex];
		if(maxPartitionIndex<partitionIndex)
		{
			maxPartitionIndex=partitionIndex;
		}
	}

	const I32 treeCount=fe::maximum(I32(1),maxPartitionIndex+1);
	m_treeArray.resize(treeCount);
	m_elementArrays.resize(treeCount);

	m_pElementArray=a_pElementArray;
	m_pVertexArray=a_pVertexArray;
	m_pNormalArray=a_pNormalArray;
	m_pUVArray=a_pUVArray;
	m_pColorArray=a_pColorArray;
	m_pTriangleIndexArray=a_pTriangleIndexArray;
	m_pPointIndexArray=a_pPointIndexArray;
	m_pPrimitiveIndexArray=a_pPrimitiveIndexArray;
	m_pPartitionIndexArray=a_pPartitionIndexArray;
	m_primitives=a_primitives;
	m_vertices=a_vertices;
	m_center=a_center;
	m_radius=a_radius;

	const I32 threadCount=Thread::hardwareConcurrency();

#if FE_MPT_DEBUG
	feLog("MapTree::populate treeCount %d threadCount %d\n",
			treeCount,threadCount);
#endif

	if(threadCount<2 || treeCount<2)
	{
		for(I32 treeIndex=0;treeIndex<treeCount;treeIndex++)
		{
			populateSubTree(treeIndex);
		}
	}
	else
	{
		sp< Gang<Populator,I32> > spGang(new Gang<Populator,I32>());

		for(I32 treeIndex=0;treeIndex<treeCount;treeIndex++)
		{
			spGang->post(treeIndex);
		}

		spGang->start(sp<Counted>(this),threadCount);

		if(spGang->workers())
		{
			spGang->finish();
		}
	}

#if FE_MPT_DEBUG
	feLog("MapTree::populate done\n");
#endif
}

void MapTree::Populator::operate(void)
{
#if FE_MPT_MT_DEBUG
	const U32 thread=m_id+1;

	feLog("MapTree::Populator::operator()"
			" %p Thread %d starting\n",this,thread);
#endif

	I32 job=0;
	while(m_spJobQueue->take(job))
	{
#if FE_MPT_MT_DEBUG
		feLog("MapTree::Populator::operator()"
				" %p Thread %d Job %d\n",
				this,thread,job);
#endif

		m_spMapTree->populateSubTree(job);

#if FE_MPT_MT_DEBUG
		feLog("MapTree::Populator::operator()"
				" %p Thread %d Job %d done\n",
				this,thread,job);
#endif
	}

#if FE_MPT_MT_DEBUG
	feLog("MapTree::Populator::operator()"
			" %p Thread %d finishing\n",this,thread);
#endif
}

void MapTree::populateSubTree(I32 a_treeIndex)
{
	sp<SpatialTreeI>& rspTree=m_treeArray[a_treeIndex];
	rspTree=create("SpatialTreeI.default");
	rspTree->setAccuracy(m_accuracy);
	rspTree->setRefinement(m_refinement);

	std::vector<Vector3i>& rElementArray=m_elementArrays[a_treeIndex];
	rElementArray.resize(m_primitives);

	I32 primitiveTotal=0;

	for(U32 primitiveIndex=0;primitiveIndex<m_primitives;primitiveIndex++)
	{
		const I32 startIndex=m_pElementArray[primitiveIndex][0];
		const I32 partitionIndex=
				fe::maximum(I32(0),m_pPartitionIndexArray[startIndex]);
		if(partitionIndex!=a_treeIndex)
		{
			continue;
		}

		rElementArray[primitiveTotal++]=
				m_pElementArray[primitiveIndex];
	}

#if FE_MPT_DEBUG
	feLog("MapTree::populateSubTree %d primitives %d\n",
			a_treeIndex,primitiveTotal);
#endif

	rElementArray.resize(primitiveTotal);

	//* TODO recompute subset center and radius
	//* TODO consider performance issues of sending all vertices

	rspTree->populate(rElementArray.data(),m_pVertexArray,m_pNormalArray,
			m_pUVArray,m_pColorArray,m_pTriangleIndexArray,
			m_pPointIndexArray,m_pPrimitiveIndexArray,
			m_pPartitionIndexArray,
			primitiveTotal,m_vertices,m_center,m_radius);
}

Real MapTree::nearestPoint(const SpatialVector& a_origin,
	Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
	const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray) const
{
#if FE_MPT_DEBUG
	feLog("MapTree::nearestPoint\n");
#endif

	if(!a_hitLimit)
	{
		return 0.0;
	}

	const I32 treeCount=m_treeArray.size();
	for(I32 treeIndex=0;treeIndex<treeCount;treeIndex++)
	{
		if(a_rspPartition.isValid() && !a_rspPartition->match(treeIndex))
		{
			continue;
		}

#if FE_MPT_DEBUG
		feLog("MapTree::nearestPoint tree %d/%d\n",
				treeIndex,treeCount);
#endif

		const sp<SpatialTreeI>& rspTree=m_treeArray[treeIndex];
		Array< sp<SpatialTreeI::Hit> > hitArray;
		rspTree->nearestPoint(a_origin,a_maxDistance,a_anyHit,
				a_hitLimit,sp<PartitionI>(NULL),hitArray);

		accumulateHits(a_rHitArray,hitArray,a_hitLimit);
	}

	return a_rHitArray.size()? a_rHitArray[0]->distance(): 0.0;
}

Real MapTree::rayImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,BWORD a_anyHit,
	U32 a_hitLimit,const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray) const
{
#if FE_MPT_DEBUG
	feLog("MapTree::rayImpact\n");
#endif

	if(!a_hitLimit)
	{
		return 0.0;
	}

	const I32 treeCount=m_treeArray.size();
	for(I32 treeIndex=0;treeIndex<treeCount;treeIndex++)
	{
		if(a_rspPartition.isValid() && !a_rspPartition->match(treeIndex))
		{
			continue;
		}

#if FE_MPT_DEBUG
		feLog("MapTree::rayImpact tree %d/%d\n",
				treeIndex,treeCount);
#endif

		const sp<SpatialTreeI>& rspTree=m_treeArray[treeIndex];
		Array< sp<SpatialTreeI::Hit> > hitArray;
		rspTree->rayImpact(a_origin,a_direction,a_maxDistance,a_anyHit,
				a_hitLimit,sp<PartitionI>(NULL),hitArray);

		accumulateHits(a_rHitArray,hitArray,a_hitLimit);
	}

	return a_rHitArray.size()? a_rHitArray[0]->distance(): 0.0;
}

void MapTree::accumulateHits(Array< sp<SpatialTreeI::Hit> >& a_rDestination,
	Array< sp<SpatialTreeI::Hit> >& a_rSource,U32 a_hitLimit) const
{
#if FE_MPT_DEBUG
	feLog("MapTree::accumulateHits had %d add %d limit %d\n",
			a_rDestination.size(),a_rSource.size(),a_hitLimit);
#endif

	const I32 hitCount=a_rSource.size();
	if(!hitCount)
	{
		return;
	}

	I32 resultCount=a_rDestination.size();
	if(!resultCount)
	{
		a_rDestination.resize(hitCount);
		for(I32 hitIndex=0;hitIndex<hitCount;hitIndex++)
		{
			a_rDestination[hitIndex]=a_rSource[hitIndex];
		}
		return;
	}

	I32 checkIndex=0;
	Real checkDistance=a_rDestination[checkIndex]->distance();

	for(I32 hitIndex=0;hitIndex<hitCount;)
	{
		const Real hitDistance=a_rSource[hitIndex]->distance();
		if(hitDistance<checkDistance)
		{
			resultCount=fe::minimum(resultCount+1,I32(a_hitLimit));
			a_rDestination.resize(resultCount);
			for(I32 resultIndex=resultCount-1;resultIndex>checkIndex;
					resultIndex--)
			{
				a_rDestination[resultIndex]=a_rDestination[resultIndex-1];
			}
			a_rDestination[checkIndex]=a_rSource[hitIndex];
			hitIndex++;
#if FE_MPT_DEBUG
			feLog("MapTree::accumulateHits %d:",resultCount);
			for(I32 resultIndex=0;resultIndex<resultCount;resultIndex++)
			{
				feLog(" %.6G",a_rDestination[resultIndex]->distance());
			}
			feLog("\n");
#endif
		}
		else if((++checkIndex)==I32(a_hitLimit))
		{
			break;
		}
	}
}

} /* namespace ext */
} /* namespace fe */