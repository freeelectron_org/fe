import pyfe.context

def registerFreeElectron():

    from Katana import Nodes3DAPI
    from Katana import FnAttribute

    def buildGeoOpChain(node, interface):
        global operator_keys_map

        currentFrame = interface.getGraphState().getTime()

        interface.setMinRequiredInputs(0)

        argsGb = FnAttribute.GroupBuilder()

        nodeType = node.getType()

        param_keys = {  "fe_component":"string",
                        "location":"string" }

        operator_keys = operator_keys_map[nodeType]

        for key in operator_keys:
            if key == "":
                continue

            param_keys[key]=operator_keys[key]

        if nodeType == "FurOp":
            param_keys["curl.amplitude"]="real"
            param_keys["curl.spin"]="real"

        for key in param_keys:
            valueType = param_keys[key]

            param = node.getParameter(key)
            if param:
                if valueType == "bool" or valueType == "integer":
                    argsGb.set(key, int(param.getValue(currentFrame)))
                else:
                    argsGb.set(key, param.getValue(currentFrame))

        argsGb.set("currentFrame", currentFrame)

        interface.appendOp(nodeType, argsGb.build())

    global operator_keys_map

    operator_keys_map = {}

    fe = pyfe.context.Context()

    for nodeType in [ "FurOp", "ImportOp", "TreeOp" ]:
        component = nodeType

        # HACK
        if component == "FurOp":
            component = "ImportOp"

        operator = fe.create(component, nodeType)

        nodeTypeBuilder = Nodes3DAPI.NodeTypeBuilder(nodeType)

        gb = FnAttribute.GroupBuilder()
        gb.set("fe_component", FnAttribute.StringAttribute("ImportOp"))
        gb.set("location", FnAttribute.StringAttribute("/root/world/geo/model"))
        gb.set("currentFrame", FnAttribute.FloatAttribute(0.0))

        operator_keys = operator.keys()
        operator_keys.pop("icon", None)
        operator_keys.pop("input:0", None)
        operator_keys.pop("input:1", None)
        operator_keys.pop("input:2", None)
        operator_keys.pop("input:3", None)
        operator_keys.pop("output:0", None)

        operator_keys_map[nodeType] = operator_keys

        # TODO don't sort keys
        # TODO pages as groups
        # TODO float slider
        # TODO picklists

        for key in operator_keys:
            if key == "":
                continue

            valueType = operator_keys[key]
            if valueType == "":
                continue

            value = operator[key]

            if valueType == "bool":
                gb.set(key, FnAttribute.IntAttribute(int(value)))
            elif valueType == "string":
                gb.set(key, FnAttribute.StringAttribute(value))
            elif valueType == "integer":
                gb.set(key, FnAttribute.IntAttribute(int(value)))
            elif valueType == "real":
                gb.set(key, FnAttribute.FloatAttribute(float(value)))
            elif valueType == "vector3":
                # TODO
                pass

        if nodeType == "FurOp":
            gbCurl = FnAttribute.GroupBuilder()
            gbCurl.set("amplitude", FnAttribute.FloatAttribute(1.0))
            gbCurl.set("spin", FnAttribute.FloatAttribute(1.0))

            gb.set("curl", gbCurl.build())

        nodeTypeBuilder.setParametersTemplateAttr(gb.build())

        nodeTypeBuilder.setHintsForParameter("fe_component",
                {"widget":"null"})

        nodeTypeBuilder.setHintsForParameter("location",
                {"label":"Location",
                "widget":"scenegraphLocation"})

        for key in operator_keys:
            if key == "":
                continue

            valueType = operator_keys[key]
            if valueType == "":
                continue

            hints = {}

            if valueType == "integer" or valueType == "bool":
                hints["int"] = True

            label = operator.get(key,"label")
            if label != "":
                hints["label"] = label

            suggest = operator.get(key,"suggest")
            if suggest == "filename":
                hints["widget"] = "fileInput"
            elif suggest == "color":
                hints["widget"] = "color"
            elif suggest == "multiline":
                hints["widget"] = "text"
            elif valueType == "bool":
                hints["widget"] = "checkBox"

            nodeTypeBuilder.setHintsForParameter(key, hints)

        if nodeType == "FurOp":
            nodeTypeBuilder.setHintsForParameter("curl",
                    {"label":"Curl",
                    "open":True})

            nodeTypeBuilder.setHintsForParameter("curl.amplitude",
                    {"label":"Amplitude",
                    "min":0,
                    "sensitivity":0.001,
                    "slider":True,
                    "slidermin":0,
                    "slidermax":0.1})

            nodeTypeBuilder.setHintsForParameter("curl.spin",
                    {"label":"Spin",
                    "min":0,
                    "sensitivity":0.1,
                    "slider":True,
                    "slidermin":0,
                    "slidermax":10})

        nodeTypeBuilder.setBuildOpChainFnc(buildGeoOpChain)

        # Build the new node type
        nodeTypeBuilder.build()

# Register the node
registerFreeElectron()
