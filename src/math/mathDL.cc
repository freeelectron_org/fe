/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>
#include <plugin/plugin.pmh>

#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	return pLibrary;
}

}
