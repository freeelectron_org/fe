import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.basiclibs + [
                "feMathLib" ]

    tests = [   'xBasis',
                'xBox',
                'xDAGNode',
                'xDualQuaternion',
                'xEuler',
                'xMatrix',
                'xQuaternion',
                'xVector',
                'xVectorSpeed' ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xBasis.exe",          "",                         None,       None),
        ("xBox.exe",            "",                         None,       None),
        ("xDAGNode.exe",        "",                         None,       None),
        ("xDualQuaternion.exe", "",                         None,       None),
        ("xEuler.exe",          "",                         None,       None),
        ("xMatrix.exe",         "",                         None,       None),
        ("xQuaternion.exe",     "",                         None,       None),
        ("xVector.exe",         "",                         None,       None),
        ("xVectorSpeed.exe",    "",                         None,       None) ]
