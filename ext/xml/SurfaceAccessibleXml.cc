/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <xml/xml.pmh>

#define FE_SAX_DEBUG		FALSE

namespace fe
{
namespace ext
{

BWORD SurfaceAccessibleXml::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAX_DEBUG
	feLog("SurfaceAccessibleXml::load \"%s\"\n",a_filename.c_str());
#endif

	if(a_spSettings.isValid())
	{
		m_frame=a_spSettings->catalog<Real>("frame");
		m_spFilter=a_spSettings->catalog< sp<Component> >("filter");

		const String options=a_spSettings->catalog<String>("options");
		if(!options.empty())
		{
			setOptions(options);
		}
	}
	else
	{
		m_frame=0.0;
		m_spFilter=NULL;
	}

	pugi::xml_document document;
	pugi::xml_parse_result result=document.load_file(a_filename.c_str());
	if(!result)
	{
		return result;
	}

	startJoints();

#if FE_SAX_DEBUG
	feLog("SurfaceAccessibleXml::load result \"%s\"\n",result.description());
#endif

	//* TODO check type and version
	const pugi::xml_node root=document.first_child();

	const pugi::xml_node collection=root.first_child();

#if FALSE
	for(pugi::xml_node child=collection.first_child();child;
			child=child.next_sibling())
	{
		const String name=interpretString(child,"name");

		const String parentName=interpretString(child,"parent");

		const SpatialTransform refWorld=
				interpretTransform(child,"worldreference");

//		feLog("addTarget \"%s\" \"%s\" %s\n",
//				name.c_str(),parentName.c_str(),
//				c_print(refWorld.translation()));

		addTarget(name,parentName,animWorld,refWorld);
	}
#endif

	for(pugi::xml_node child=collection.first_child();child;
			child=child.next_sibling())
	{
		const String name=interpretString(child,"name");

		const String parentName=interpretString(child,"parent");

		const SpatialTransform refWorld=
				interpretTransform(child,"worldreference");
		const SpatialVector translation=
				interpretVector(child,"translation");
		const SpatialTransform relativeRotation=
				interpretEuler(child,"rotation","rotationorder");
		const SpatialVector localScale=interpretVector(child,"scale");
		const SpatialVector color=interpretVector(child,"color");

		SpatialTransform relativeTranslation;
		setIdentity(relativeTranslation);
		translate(relativeTranslation,translation);

		const SpatialTransform xformRelative=
				relativeRotation*relativeTranslation;

#if FE_SAX_DEBUG
		const SpatialVector degrees=interpretVector(child,"rotation");

		feLog("\n%s \"%s\" parent \"%s\"\n",
				child.name(),name.c_str(),parentName.c_str());
		feLog("  T %s R %s\n  LS %s\n",
				c_print(translation),c_print(degrees),
				c_print(localScale));
		feLog("  rot\n%s\n",c_print(relativeRotation));
		feLog("  ref\n%s\n",c_print(refWorld));
#endif

		setJoint(name,parentName,xformRelative,localScale,refWorld,color);
	}

	completeJoints();

	return result;
}

SpatialTransform SurfaceAccessibleXml::interpretEuler(
	pugi::xml_node& a_rNode,String a_attrName,String a_attrOrder)
{
	const SpatialVector degrees=interpretVector(a_rNode,a_attrName);
	const String order=interpretString(a_rNode,a_attrOrder);

	const SpatialEuler euler(
			degrees[0]*degToRad,degrees[1]*degToRad,degrees[2]*degToRad);

	if(order=="ZYX")
	{
		return euler;
	}

	//* TODO move to Euler<>

	SpatialTransform xform;
	setIdentity(xform);

	if(order=="ZXY")
	{
		rotate(xform,euler[2],e_zAxis);
		rotate(xform,euler[0],e_xAxis);
		rotate(xform,euler[1],e_yAxis);
	}
	else if(order=="XZY")
	{
		rotate(xform,euler[0],e_xAxis);
		rotate(xform,euler[2],e_zAxis);
		rotate(xform,euler[1],e_yAxis);
	}
	else if(order=="YZX")
	{
		rotate(xform,euler[1],e_yAxis);
		rotate(xform,euler[2],e_zAxis);
		rotate(xform,euler[0],e_xAxis);
	}
	else if(order=="XYZ")
	{
		rotate(xform,euler[0],e_xAxis);
		rotate(xform,euler[1],e_yAxis);
		rotate(xform,euler[2],e_zAxis);
	}
	else if(order=="YXZ")
	{
		rotate(xform,euler[1],e_yAxis);
		rotate(xform,euler[0],e_xAxis);
		rotate(xform,euler[2],e_zAxis);
	}

	return xform;
}

String SurfaceAccessibleXml::interpretString(
	pugi::xml_node& a_rNode,String a_attrName)
{
	pugi::xml_attribute xmlAttr=a_rNode.attribute(a_attrName.c_str());

	return xmlAttr.value();
}

SpatialVector SurfaceAccessibleXml::interpretVector(
	pugi::xml_node& a_rNode,String a_attrName)
{
	pugi::xml_attribute xmlAttr=a_rNode.attribute(a_attrName.c_str());

	float x=0.0;
	float y=0.0;
	float z=0.0;
	sscanf(xmlAttr.value(),"%f,%f,%f",&x,&y,&z);

	return SpatialVector(x,y,z);
}

SpatialTransform SurfaceAccessibleXml::interpretTransform(
	pugi::xml_node& a_rNode,String a_attrName)
{
	pugi::xml_attribute xmlAttr=a_rNode.attribute(a_attrName.c_str());

	float v[16];
	sscanf(xmlAttr.value(),
			"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
			&v[0],&v[1],&v[2],&v[3],
			&v[4],&v[5],&v[6],&v[7],
			&v[8],&v[9],&v[10],&v[11],
			&v[12],&v[13],&v[14],&v[15]);

	SpatialTransform matrix;
	set(matrix,v);

	return matrix;
}

} /* namespace ext */
} /* namespace fe */
