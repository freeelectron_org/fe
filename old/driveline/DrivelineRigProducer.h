/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __driveline_DrivelineRigProducer_h__
#define __driveline_DrivelineRigProducer_h__

namespace fe
{
namespace ext
{

/// @brief
class FE_DL_EXPORT DrivelineRigProducer :
	virtual public Producer,
	public Initialize<DrivelineRigProducer>
{
	public:
				DrivelineRigProducer(void);
virtual			~DrivelineRigProducer(void);
		void	initialize(void);

	protected:
virtual	void prepare(sp<RecordGroup> a_rg_dataset);
virtual	sp<OrchestratorI> createOrchestrator(sp<RecordGroup> a_rg);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_DrivelineRigProducer_h__ */

