/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <network/network.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
//	list.append(new String("fexSignalDL"));
}

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
//	assertData(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<MessageSignaler>("SignalerI.MessageSignaler.fe");

	pLibrary->add<SignalMessenger>("HandlerI.SignalMessenger.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

