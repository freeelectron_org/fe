/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <flight/flight.pmh>

#define FE_SFD_DEBUG	TRUE

namespace fe
{
namespace ext
{

SurfaceDrone::SurfaceDrone(void):
	m_frame(-1)
{
#if	FE_SFD_DEBUG
	feLog("SurfaceDrone::SurfaceDrone\n");
#endif

	setRadius(0.5);

	setIdentity(m_xformDrone);
	setIdentity(m_xformCam);
}

SurfaceDrone::~SurfaceDrone(void)
{
#if	FE_SFD_DEBUG
	feLog("SurfaceDrone::~SurfaceDrone\n");
#endif
}

Protectable* SurfaceDrone::clone(Protectable* pInstance)
{
#if	FE_SFD_DEBUG
	feLog("SurfaceDrone::clone\n");
#endif
	return NULL;
}

void SurfaceDrone::cache(void)
{
#if	FE_SFD_DEBUG
	feLog("SurfaceDrone::cache\n");
#endif

	sp<Registry> spRegistry=registry();
	sp<Master> spMaster=spRegistry->master();


	const String spaces("spaces");
	const String space("world");
	sp<Catalog> spMasterCatalog=spMaster->catalog();
	sp<Catalog> spSpacesCatalog=
			spMasterCatalog->catalog< sp<Catalog> >(spaces);
	if(spSpacesCatalog.isNull())
	{
		spSpacesCatalog=spMaster->createCatalog(spaces);
		spMasterCatalog->catalog< sp<Catalog> >(spaces)=spSpacesCatalog;
	}

	if(spSpacesCatalog->cataloged(space))
	{
		m_spStateCatalog=spSpacesCatalog->catalog< sp<Catalog> >(space);
	}
	if(m_spStateCatalog.isNull())
	{
		//* TODO load yaml
		sp<Catalog> spPreCatalog=spMaster->createCatalog("drone_config");
		if(spPreCatalog.isNull())
		{
			feX(e_invalidPointer,"SurfaceDrone::cache",
					"Catalog is invalid");
		}

		const String implementation=
				spPreCatalog->catalogOrDefault<String>(
				"net:implementation","ConnectedCatalog");
		const String address=
				spPreCatalog->catalogOrDefault<String>(
				"net:address","127.0.0.1");
		const String transport=
				spPreCatalog->catalogOrDefault<String>(
				"net:transport","tcp");
		const I32 port=
				spPreCatalog->catalogOrDefault<I32>(
				"net:port",12345);

		//* make sure catalog is set, if defaulted
		spPreCatalog->catalog<String>("net:implementation")=implementation;
		spPreCatalog->catalog<String>("net:address")=address;
		spPreCatalog->catalog<String>("net:transport")=transport;
		spPreCatalog->catalog<I32>("net:port")=port;

		m_spStateCatalog=spRegistry->create(implementation);
		if(m_spStateCatalog.isValid())
		{
			spSpacesCatalog->catalog< sp<Catalog> >(space)=m_spStateCatalog;

			m_spStateCatalog->overlayState(spPreCatalog);

			m_spStateCatalog->catalogDump();

			Result result=m_spStateCatalog->start();
			if(failure(result))
			{
				feLog("SurfaceDrone::cache failed to start networking\n");
			}

			m_spStateCatalog->setConnectionTimeout(10);

			m_spStateCatalog->waitForConnection();
		}
	}

	if(m_spStateCatalog.isNull())
	{
		feX(e_invalidPointer,"SurfaceDrone::cache","StateCatalog is invalid");
	}

	updateState();
	updateState();

	feLog("SurfaceDrone::cache catalogDump\n");
	m_spStateCatalog->catalogDump();
}

void SurfaceDrone::updateState(void)
{
	{
		static I32 flushCount(-1);
		StateCatalog::Atomic atomic(m_spStateCatalog,flushCount);

		I32 newFrame(0);
		atomic.getState<I32>("frame",newFrame);

		if(newFrame==m_frame)
		{
			feLog("frame %d repeated\n",m_frame);
		}
		m_frame=newFrame;

		atomic.getState<SpatialTransform>("instance[0].root.transform",
				m_xformDrone);

//		feLog("frame %d flushCount %d\n",m_frame,flushCount);
	}

	SpatialVector offsetCam(1.0,0.0,0.0);

	//* reverse transform from -z_forward y_up default camera
	setIdentity(m_xformCam);
	rotate(m_xformCam,0.5*fe::pi,e_yAxis);
	rotate(m_xformCam,-0.5*fe::pi,e_xAxis);
	translate(m_xformCam,-offsetCam);

	SpatialTransform invDrone;
	invert(invDrone,m_xformDrone);

	m_xformCam=invDrone*m_xformCam;

	setCenter(m_xformDrone.translation());
}

void SurfaceDrone::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const Color* a_color,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
	const_cast<SurfaceDrone*>(this)->updateState();

	SurfaceSphere::draw(a_rTransform,a_spDrawI,a_color,
			a_spDrawBuffer,a_spPartition);

	feLog("SurfaceDrone::draw\n%s\n",c_print(a_rTransform));

	const SpatialVector scale(2.0,2.0,2.0);
	a_spDrawI->drawCircle(m_xformCam,&scale,*a_color);
}

} /* namespace ext */
} /* namespace fe */
