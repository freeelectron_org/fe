import os
import sys
import shutil
forge = sys.modules["forge"]

def prerequisites():
    return [ "draw" ]

def link_line() -> str:
    freetype_gl_build = os.environ["FE_FREETYPE_GL_BUILD"]

    line = ""
    if forge.fe_os == "FE_LINUX":
        line = "-L " + freetype_gl_build
        line += " -lfreetype-gl -lGLEW -lfreetype"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        line = "freetype-gl.lib"

        if forge.codegen == 'debug':
            line += " freetyped.lib libpng16d.lib bz2d.lib zlibd.lib"
        else:
            line += " freetype.lib libpng16.lib bz2.lib zlib.lib"

        line += " brotlidec.lib brotlicommon.lib"
    return line

def setup(module):
    srcList = [ "freetype.pmh",
                "FontFreeTypeGL",
                "freetypeDL" ]

    dll = module.DLL( "fexFreeTypeDL", srcList )

    freetype_gl_include = os.environ["FE_FREETYPE_GL_INCLUDE"]

    freeTypeFontsPath = os.path.join(module.modPath, 'fonts')
    freeTypeShadersPath = os.path.join(module.modPath, 'shaders')
    libFontsPath = os.path.join(forge.libPath, 'freetype-gl', 'fonts')
    libShadersPath = os.path.join(forge.libPath, 'freetype-gl', 'shaders')

    if os.path.isdir(libFontsPath) == 0:
        os.makedirs(libFontsPath, 0o755)
    if os.path.isdir(libShadersPath) == 0:
        os.makedirs(libShadersPath, 0o755)

    for font in ['SourceCodePro-LICENSE.txt', 'SourceCodePro-Regular.ttf']:
        source = os.path.join(freeTypeFontsPath,font)
        dest = os.path.join(libFontsPath, font)
        shutil.copyfile(source,dest)
    for shader in ['v3f-t2f-c4f.frag', 'v3f-t2f-c4f.vert']:
        source = os.path.join(freeTypeShadersPath,shader)
        dest = os.path.join(libShadersPath, shader)
        shutil.copyfile(source,dest)

    module.includemap = {}
    module.includemap['freetype'] = freetype_gl_include

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs[:]

    dll.linkmap["freetype"] = link_line()

    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexFreeTypeDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexFreeTypeDL",                None,       None) ]

#   module.Module('test')

def auto(module):
    freetype_gl_include = os.environ["FE_FREETYPE_GL_INCLUDE"]

    forge.includemap['freetype'] = freetype_gl_include
    forge.linkmap['gfxlibs'] = forge.gfxlibs

    forge.linkmap["freetype"] = link_line()

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        test_file = """
#include "windows.h"
#include "winuser.h"
#include "freetype-gl/freetype-gl.h"

int main(void)
{
    texture_atlas_t* pAtlas=texture_atlas_new(512,512,1);
    return 0;
}
    \n"""
    else:
        test_file = """
#include "freetype-gl/freetype-gl.h"

int main(void)
{
    return 0;
}
    \n"""

    result = forge.cctest(test_file)

    forge.includemap.pop('freetype', None)
    forge.linkmap.pop('gfxlibs', None)
    forge.linkmap.pop('freetype', None)

    return result
