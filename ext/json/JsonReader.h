/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_JsonReader_h__
#define __json_JsonReader_h__

namespace fe
{
namespace ext
{

namespace data
{

//* NOTE wiring logic copied from AsciiStream
//* TODO refactor to share overlapping code

/**************************************************************************//**
    @brief Stream intrepreter for reading JSON files

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT JsonReader : public fe::data::AsciiReader
{
	public:
							JsonReader(sp<Scope> spScope);
virtual						~JsonReader(void);
virtual	sp<RecordGroup>		input(std::istream &istrm);

	private:

		void			fail(const String &a_message);
		void			handleGroup(String a_key,
								const Json::Value& a_jsonBlock);
		void			handleAttribute(String a_key,
								const Json::Value& a_jsonBlock);
		void			handleLayout(String a_key,
								const Json::Value& a_jsonBlock);
		void			handleTemplate(String a_key,
								const Json::Value& a_jsonBlock);
		void			handleRecord(String a_layout,String a_key,
								const Json::Value& a_jsonBlock);
		void			handleInfo(const Json::Value& a_jsonInfo);

	private:
		sp<JsonValue>				m_spJsonRoot;
};

} /* namespace data */

} /* namespace ext */
} /* namespace fe */

#endif /* __json_JsonReader_h__ */
