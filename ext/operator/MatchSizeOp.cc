/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

using namespace fe;
using namespace fe::ext;

void MatchSizeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("inputPointGroups")="";
	catalog<String>("inputPointGroups","label")="Input Point Groups";
	catalog<String>("inputPointGroups","suggest")="pointGroups";
	catalog<bool>("inputPointGroups","joined")=true;
	catalog<String>("inputPointGroups","hint")=
			"Space delimited list of point groups on first input to move.";

	catalog<bool>("inputPointRegex")=false;
	catalog<String>("inputPointRegex","label")="Regex";
	catalog<String>("inputPointRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("refPointGroups")="";
	catalog<String>("refPointGroups","label")="Reference Point Groups";
	catalog<String>("refPointGroups","suggest")="pointGroups";
	catalog<bool>("refPointGroups","joined")=true;
	catalog<String>("refPointGroups","hint")=
			"Space delimited list of point groups"
			" on reference/deformed to evaluate.";

	catalog<bool>("refPointRegex")=false;
	catalog<String>("refPointRegex","label")="Regex";
	catalog<String>("refPointRegex","hint")=
			catalog<String>("inputPointRegex","hint");

	catalog<String>("defPointGroups")="";
	catalog<String>("defPointGroups","label")="Deformed Point Groups";
	catalog<String>("defPointGroups","suggest")="pointGroups";
	catalog<bool>("defPointGroups","joined")=true;
	catalog<String>("defPointGroups","hint")=
			"Space delimited list of point groups"
			" on deference/deformed to evaluate.";

	catalog<bool>("defPointRegex")=false;
	catalog<String>("defPointRegex","label")="Regex";
	catalog<String>("defPointRegex","hint")=
			catalog<String>("inputPointRegex","hint");

	catalog<bool>("doTranslate")=true;
	catalog<String>("doTranslate","label")="Translate";
	catalog<bool>("doTranslate","joined")=true;
	catalog<String>("doTranslate","hint")=
			"Move the input surface.";

	catalog<bool>("translateX")=true;
	catalog<String>("translateX","label")="X";
	catalog<String>("translateX","enabler")="doTranslate";
	catalog<bool>("translateX","joined")=true;
	catalog<String>("translateX","hint")=
			"Move along the X axis.";

	catalog<bool>("translateY")=true;
	catalog<String>("translateY","label")="Y";
	catalog<String>("translateY","enabler")="doTranslate";
	catalog<bool>("translateY","joined")=true;
	catalog<String>("translateY","hint")=
			"Move along the Y axis.";

	catalog<bool>("translateZ")=true;
	catalog<String>("translateZ","label")="Z";
	catalog<String>("translateZ","enabler")="doTranslate";
	catalog<String>("translateZ","hint")=
			"Move along the Z axis.";

	catalog<bool>("doScale")=true;
	catalog<String>("doScale","label")="Scale";
	catalog<bool>("doScale","joined")=true;
	catalog<String>("doScale","hint")=
			"Resize the input surface.";

	catalog<bool>("uniformScale")=false;
	catalog<String>("uniformScale","label")="Uniform";
	catalog<String>("uniformScale","enabler")="doScale";
	catalog<bool>("uniformScale","joined")=true;
	catalog<String>("uniformScale","hint")=
			"Resize all axes the same amount.";

	catalog<bool>("scaleX")=true;
	catalog<String>("scaleX","label")="X";
	catalog<String>("scaleX","enabler")="doScale && !uniformScale";
	catalog<bool>("scaleX","joined")=true;
	catalog<String>("scaleX","hint")=
			"Move along the X axis.";

	catalog<bool>("scaleY")=true;
	catalog<String>("scaleY","label")="Y";
	catalog<String>("scaleY","enabler")="doScale && !uniformScale";
	catalog<bool>("scaleY","joined")=true;
	catalog<String>("scaleY","hint")=
			"Move along the Y axis.";

	catalog<bool>("scaleZ")=true;
	catalog<String>("scaleZ","label")="Z";
	catalog<String>("scaleZ","enabler")="doScale && !uniformScale";
	catalog<String>("scaleZ","hint")=
			"Move along the Z axis.";

	catalog<String>("uniformRule")="minimum";
	catalog<String>("uniformRule","label")="Uniform Rule";
	catalog<String>("uniformRule","enabler")="uniformScale";
	catalog<String>("uniformRule","choice:0")="minimum";
	catalog<String>("uniformRule","label:0")="Minimum";
	catalog<String>("uniformRule","choice:1")="maximum";
	catalog<String>("uniformRule","label:1")="Maximum";
	catalog<String>("uniformRule","choice:2")="average";
	catalog<String>("uniformRule","label:2")="Average";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");

	catalog< sp<Component> >("Deformed Surface");
	catalog<bool>("Deformed Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
}

void MatchSizeOp::handle(Record& a_rSignal)
{
	catalog<String>("summary")="OFF";

	const BWORD translateX=catalog<bool>("translateX");
	const BWORD translateY=catalog<bool>("translateY");
	const BWORD translateZ=catalog<bool>("translateZ");
	const BWORD doTranslate=(catalog<bool>("doTranslate") &&
			(translateX || translateY || translateZ));

	const BWORD uniformScale=catalog<bool>("uniformScale");
	const BWORD scaleX=catalog<bool>("scaleX");
	const BWORD scaleY=catalog<bool>("scaleY");
	const BWORD scaleZ=catalog<bool>("scaleZ");
	const BWORD doScale=(catalog<bool>("doScale") &&
			(uniformScale || scaleX || scaleY || scaleZ));

	const String uniformRule=catalog<String>("uniformRule");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spReferenceAccessible;
	if(!access(spReferenceAccessible,"Reference Surface")) return;

	sp<SurfaceAccessibleI> spDeformedAccessible;
	access(spDeformedAccessible,"Deformed Surface",e_quiet);

	sp<SurfaceAccessorI> spOutputPoints;
	if(!access(spOutputPoints,spOutputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spReferencePoints;
	if(!access(spReferencePoints,spReferenceAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spDeformedPoints;
	if(spDeformedAccessible.isValid())
	{
		if(!access(spDeformedPoints,spDeformedAccessible,
				e_point,e_position))
		{
			spDeformedAccessible=NULL;
		}
	}

	String inputPointGroups=catalog<String>("inputPointGroups");
	if(!inputPointGroups.empty() && !catalog<bool>("pointRegex"))
	{
		inputPointGroups=inputPointGroups.convertGlobToRegex();
	}

	const BWORD inputPointGrouped=(!inputPointGroups.empty());

	sp<SurfaceAccessorI> spInputPointGroup;
	if(inputPointGrouped)
	{
		if(!access(spInputPointGroup,spOutputAccessible,
				e_pointGroup,inputPointGroups)) return;
	}

	String refPointGroups=catalog<String>("refPointGroups");
	if(!refPointGroups.empty() && !catalog<bool>("pointRegex"))
	{
		refPointGroups=refPointGroups.convertGlobToRegex();
	}

	const BWORD refPointGrouped=(!refPointGroups.empty());

	sp<SurfaceAccessorI> spRefPointGroup;
	if(refPointGrouped)
	{
		if(!access(spRefPointGroup,spReferenceAccessible,
				e_pointGroup,refPointGroups)) return;
	}

	String defPointGroups=catalog<String>("defPointGroups");
	if(!defPointGroups.empty() && !catalog<bool>("pointRegex"))
	{
		defPointGroups=defPointGroups.convertGlobToRegex();
	}

	const BWORD defPointGrouped=(!defPointGroups.empty());

	sp<SurfaceAccessorI> spDefPointGroup;
	if(defPointGrouped)
	{
		if(!access(spDefPointGroup,spDeformedAccessible,
				e_pointGroup,defPointGroups)) return;
	}

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal,e_warning);

	const I32 pointCount=inputPointGrouped?
			spInputPointGroup->count(): spOutputPoints->count();
	if(pointCount<1)
	{
		catalog<String>("warning")+="No input points;";
		return;
	}

	const I32 pointCountRef=refPointGrouped?
			spRefPointGroup->count(): spReferencePoints->count();
	if(pointCountRef<1)
	{
		catalog<String>("warning")+="No reference points;";
		return;
	}

	if(!doTranslate && !doScale)
	{
		catalog<String>("warning")+="No translation or scaling selected;";
		return;
	}

	I32 reIndex=inputPointGrouped? spInputPointGroup->integer(0): 0;

	SpatialVector lowerIn=spOutputPoints->spatialVector(reIndex);
	SpatialVector upperIn=lowerIn;
	for(I32 pointIndex=1;pointIndex<pointCount;pointIndex++)
	{
		reIndex=inputPointGrouped?
				spInputPointGroup->integer(pointIndex): pointIndex;

		const SpatialVector point=spOutputPoints->spatialVector(reIndex);

		for(I32 m=0;m<3;m++)
		{
			if(lowerIn[m]>point[m])
			{
				lowerIn[m]=point[m];
			}
			if(upperIn[m]<point[m])
			{
				upperIn[m]=point[m];
			}
		}
	}

	reIndex=refPointGrouped? spRefPointGroup->integer(0): 0;

	SpatialVector lowerRef=spReferencePoints->spatialVector(reIndex);
	SpatialVector upperRef=lowerRef;
	for(I32 pointIndex=1;pointIndex<pointCountRef;pointIndex++)
	{
		const I32 reIndex=refPointGrouped?
				spRefPointGroup->integer(pointIndex): pointIndex;

		const SpatialVector point=
				spReferencePoints->spatialVector(reIndex);

		for(I32 m=0;m<3;m++)
		{
			if(lowerRef[m]>point[m])
			{
				lowerRef[m]=point[m];
			}
			if(upperRef[m]<point[m])
			{
				upperRef[m]=point[m];
			}
		}
	}

	const I32 pointCountDef=spDefPointGroup.isValid()?
			spDefPointGroup->count():
			(spDeformedPoints.isValid()? spDeformedPoints->count(): 0);
	const BWORD useDeformed(pointCountDef>0);

	SpatialVector lowerDef=lowerRef;
	SpatialVector upperDef=upperRef;
	if(useDeformed)
	{
		reIndex=defPointGrouped? spDefPointGroup->integer(0): 0;

		lowerDef=spDeformedPoints->spatialVector(reIndex);
		upperDef=lowerDef;
		for(I32 pointIndex=1;pointIndex<pointCountDef;pointIndex++)
		{
			reIndex=defPointGrouped?
					spDefPointGroup->integer(pointIndex): pointIndex;

			const SpatialVector point=
					spDeformedPoints->spatialVector(reIndex);

			for(I32 m=0;m<3;m++)
			{
				if(lowerDef[m]>point[m])
				{
					lowerDef[m]=point[m];
				}
				if(upperDef[m]<point[m])
				{
					upperDef[m]=point[m];
				}
			}
		}
	}

	const SpatialVector lowerFrom=useDeformed? lowerRef: lowerIn;
	const SpatialVector upperFrom=useDeformed? upperRef: upperIn;

	const SpatialVector lowerTo=useDeformed? lowerDef: lowerRef;
	const SpatialVector upperTo=useDeformed? upperDef: upperRef;

	const SpatialVector sizeFrom=upperFrom-lowerFrom;
	const SpatialVector sizeTo=upperTo-lowerTo;

	const SpatialVector centerFrom=0.5*(upperFrom+lowerFrom);
	const SpatialVector centerTo=0.5*(upperTo+lowerTo);

	SpatialVector scaling(1,1,1);
	if(doScale)
	{
		const SpatialVector ratio=SpatialVector(
				sizeFrom[0]>Real(0)? sizeTo[0]/sizeFrom[0]: Real(1),
				sizeFrom[1]>Real(0)? sizeTo[1]/sizeFrom[1]: Real(1),
				sizeFrom[2]>Real(0)? sizeTo[2]/sizeFrom[2]: Real(1));

		if(uniformScale)
		{
			Real scaleFactor(1);

			if(uniformRule=="minimum")
			{
				BWORD factorSet(FALSE);

				if(sizeFrom[0]>Real(0))
				{
					scaleFactor=ratio[0];
					factorSet=TRUE;
				}
				if(sizeFrom[1]>Real(0) && (!factorSet || scaleFactor>ratio[1]))
				{
					scaleFactor=ratio[1];
					factorSet=TRUE;
				}
				if(sizeFrom[2]>Real(0) && (!factorSet || scaleFactor>ratio[2]))
				{
					scaleFactor=ratio[2];
					factorSet=TRUE;
				}
			}
			else if(uniformRule=="maximum")
			{
				BWORD factorSet(FALSE);

				if(sizeFrom[0]>Real(0))
				{
					scaleFactor=ratio[0];
					factorSet=TRUE;
				}
				if(sizeFrom[1]>Real(0) && (!factorSet || scaleFactor<ratio[1]))
				{
					scaleFactor=ratio[1];
					factorSet=TRUE;
				}
				if(sizeFrom[2]>Real(0) && (!factorSet || scaleFactor<ratio[2]))
				{
					scaleFactor=ratio[2];
					factorSet=TRUE;
				}
			}
			else if(uniformRule=="average")
			{
				Real sum(0);
				scaleFactor=Real(0);

				if(sizeFrom[0]>Real(0))
				{
					scaleFactor+=ratio[0];
					sum+=Real(1);
				}
				if(sizeFrom[1]>Real(0))
				{
					scaleFactor+=ratio[1];
					sum+=Real(1);
				}
				if(sizeFrom[2]>Real(0))
				{
					scaleFactor+=ratio[2];
					sum+=Real(1);
				}

				if(sum>Real(0))
				{
					scaleFactor/=sum;
				}
			}

			scaling=SpatialVector(scaleFactor,scaleFactor,scaleFactor);
		}
		else
		{
			scaling=SpatialVector(
					scaleX? ratio[0]: Real(1),
					scaleY? ratio[1]: Real(1),
					scaleZ? ratio[2]: Real(1));
		}
	}

	const SpatialVector translateFrom(centerFrom);

	const SpatialVector translateTo(
			doTranslate && translateX? centerTo[0]: centerFrom[0],
			doTranslate && translateY? centerTo[1]: centerFrom[1],
			doTranslate && translateZ? centerTo[2]: centerFrom[2]);

	SpatialTransform xform;
	setIdentity(xform);
	translate(xform,translateTo);
	scale(xform,scaling);
	translate(xform,-translateFrom);

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const I32 reIndex=inputPointGrouped?
				spInputPointGroup->integer(pointIndex): pointIndex;

		const SpatialVector point=spOutputPoints->spatialVector(reIndex);

		const SpatialVector transformed=transformVector(xform,point);

		spOutputPoints->set(reIndex,transformed);
	}

	if(spDrawGuide.isValid())
	{
		const Color red(1,0,0);
		const Color green(0,1,0);
		const Color blue(0,0,1);
		const Color yellow(1,1,0);

		const SpatialVector sizeIn=upperIn-lowerIn;

		const SpatialVector lowerOut=transformVector(xform,lowerIn);
		const SpatialVector upperOut=transformVector(xform,upperIn);

		const Box3 boxIn(lowerIn,sizeIn);
		const Box3 boxFrom(lowerFrom,sizeFrom);
		const Box3 boxTo(lowerTo,sizeTo);
		const Box3 boxOut(lowerOut,upperOut-lowerOut);

		spDrawGuide->drawBox(boxIn,red);
		spDrawGuide->drawBox(boxTo,green);
		spDrawGuide->drawBox(boxOut,blue);

		if(useDeformed)
		{
			spDrawGuide->drawBox(boxFrom,yellow);
		}
	}

	const SpatialVector translation=translateTo-translateFrom;

	String textT;
	if(doTranslate)
	{
		textT.sPrintf("T %.1f %.1f %.1f",
				translation[0],translation[1],translation[2]);
	}

	String textS;
	if(doScale)
	{
		if(uniformScale)
		{
			textS.sPrintf("%sS %.1f",doTranslate? " ": "",scaling[0]);
		}
		else
		{
			textS.sPrintf("%sS %.1f %.1f %.1f",
					doTranslate? " ": "",scaling[0],scaling[1],scaling[2]);
		}
	}
	catalog<String>("summary")=textT+textS;
}
