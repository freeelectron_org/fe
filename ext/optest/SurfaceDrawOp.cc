/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

using namespace fe;
using namespace fe::ext;

void SurfaceDrawOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");

	m_spDrawSolid=new DrawMode();
	m_spDrawSolid->setDrawStyle(DrawMode::e_ghost);
	m_spDrawSolid->setBackfaceCulling(FALSE);
//	m_spDrawSolid->setShadows(TRUE);
	m_spDrawSolid->setRefinement(1);
}

void SurfaceDrawOp::handle(Record& a_rSignal)
{
//	catalog<String>("summary")="";

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		if(m_spInput.isValid())
		{
			m_event.bind(windowEvent(a_rSignal));
//			feLog("SurfaceDrawOp::handle %s\n",c_print(m_event));

			sp<DrawableI> spDrawable=m_spInput;
			if(spDrawable.isValid())
			{
				if(m_spDrawBuffer.isNull())
				{
					m_spDrawBuffer=spDrawBrush->createBuffer();
					if(m_spDrawBuffer.isValid())
					{
						m_spDrawBuffer->setName("SurfaceDrawOp.DrawBuffer");
					}
					else
					{
						feLog("SurfaceDrawOp::handle"
								" couldn't create draw buffer\n");
					}
				}

				if(m_spDrawBuffer.isValid())
				{
					const Color blue(0.5,0.5,1.0);

					spDrawBrush->pushDrawMode(m_spDrawSolid);
					spDrawBrush->draw(spDrawable,&blue,m_spDrawBuffer);
					spDrawBrush->popDrawMode();
				}
			}
		}

		m_keepInput=TRUE;

		return;
	}

	if(!m_keepInput)
	{
		sp<SurfaceAccessibleI> spOriginalAccessible;
		if(!access(spOriginalAccessible,"Input Surface")) return;

		sp<SurfaceAccessibleI> spCopyAccessible=
				registry()->create("*.SurfaceAccessibleCatalog");
		if(spCopyAccessible.isValid())
		{
			spCopyAccessible->copy(spOriginalAccessible);

			m_spInput=spCopyAccessible->surface();
		}
	}
	m_keepInput=FALSE;
}
