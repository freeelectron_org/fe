/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOperateDL"));

//	list.append(new String("fexRayDL"));
//	list.append(new String("fexJsonDL"));
//	list.append(new String("fexXmlDL"));
//	list.append(new String("fexOpenILDL"));
//	list.append(new String("fexTiffDL"));
//	list.append(new String("fexOiioDL"));	//* links to native boost
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<TransformManipulator>(
			"ManipulatorI.TransformManipulator.fe");
	pLibrary->add<ValueManipulator>(
			"ManipulatorI.ValueManipulator.fe");

	pLibrary->add<BlendShapeOp>(
			"OperatorSurfaceI.BlendShapeOp.fe");
	pLibrary->add<BloatOp>(
			"OperatorSurfaceI.BloatOp.fe");
	pLibrary->add<ClaspOp>(
			"OperatorSurfaceI.ClaspOp.fe");
	pLibrary->add<ConnectOp>(
			"OperatorSurfaceI.ConnectOp.fe");
	pLibrary->add<CurveCreateOp>(
			"OperatorSurfaceI.CurveCreateOp.fe");
	pLibrary->add<CurveFollowOp>(
			"OperatorSurfaceI.CurveFollowOp.fe");
	pLibrary->add<CurveSampleOp>(
			"OperatorSurfaceI.CurveSampleOp.fe");
	pLibrary->add<DeleteOp>(
			"OperatorSurfaceI.DeleteOp.fe");
	pLibrary->add<ExcarnateOp>(
			"OperatorSurfaceI.ExcarnateOp.fe");
	pLibrary->add<ExportOp>(
			"OperatorSurfaceI.ExportOp.fe");
	pLibrary->add<ExtractOp>(
			"OperatorSurfaceI.ExtractOp.fe");
	pLibrary->add<FollicleOp>(
			"OperatorSurfaceI.FollicleOp.fe");
	pLibrary->add<FuseOp>(
			"OperatorSurfaceI.FuseOp.fe");
	pLibrary->add<GroupDeleteOp>(
			"OperatorSurfaceI.GroupDeleteOp.fe");
	pLibrary->add<GroupOp>(
			"OperatorSurfaceI.GroupOp.fe");
	pLibrary->add<GroupPromoteOp>(
			"OperatorSurfaceI.GroupPromoteOp.fe");
	pLibrary->add<HammerOp>(
			"OperatorSurfaceI.HammerOp.fe");
	pLibrary->add<HingeOp>(
			"OperatorSurfaceI.HingeOp.fe");
	pLibrary->add<HobbleOp>(
			"OperatorSurfaceI.HobbleOp.fe");
	pLibrary->add<InfluenceOp>(
			"OperatorSurfaceI.InfluenceOp.fe");
	pLibrary->add<ImportOp>(
			"OperatorSurfaceI.ImportOp.fe");
	pLibrary->add<BendOp>(
			"OperatorSurfaceI.BendOp.fe");
	pLibrary->add<LengthCorrectOp>(
			"OperatorSurfaceI.LengthCorrectOp.fe");
	pLibrary->add<MapOp>(
			"OperatorSurfaceI.MapOp.fe");
	pLibrary->add<MatchSizeOp>(
			"OperatorSurfaceI.MatchSizeOp.fe");
	pLibrary->add<MergeOp>(
			"OperatorSurfaceI.MergeOp.fe");
	pLibrary->add<MirrorOp>(
			"OperatorSurfaceI.MirrorOp.fe");
	pLibrary->add<NoiseOp>(
			"OperatorSurfaceI.NoiseOp.fe");
	pLibrary->add<NullOp>(
			"OperatorSurfaceI.NullOp.fe");
	pLibrary->add<OffsetOp>(
			"OperatorSurfaceI.OffsetOp.fe");
	pLibrary->add<OpenSubdivOp>(
			"OperatorSurfaceI.OpenSubdivOp.fe");
	pLibrary->add<PartitionOp>(
			"OperatorSurfaceI.PartitionOp.fe");
	pLibrary->add<PoisonOp>(
			"OperatorSurfaceI.PoisonOp.fe");
	pLibrary->add<PuppetOp>(
			"OperatorSurfaceI.PuppetOp.fe");
	pLibrary->add<QuiltOp>(
			"OperatorSurfaceI.QuiltOp.fe");
	pLibrary->add<RasterOp>(
			"OperatorSurfaceI.RasterOp.fe");
	pLibrary->add<RulerOp>(
			"OperatorSurfaceI.RulerOp.fe");
	pLibrary->add<SmoothOp>(
			"OperatorSurfaceI.SmoothOp.fe");
	pLibrary->add<SpreadsheetOp>(
			"OperatorSurfaceI.SpreadsheetOp.fe");
	pLibrary->add<StashOp>(
			"OperatorSurfaceI.StashOp.fe");
	pLibrary->add<SubdivideOp>(
			"OperatorSurfaceI.SubdivideOp.fe");
	pLibrary->add<SurfaceAttrConformOp>(
			"OperatorSurfaceI.SurfaceAttrConformOp.fe");
	pLibrary->add<SurfaceAttrCopyOp>(
			"OperatorSurfaceI.SurfaceAttrCopyOp.fe");
	pLibrary->add<SurfaceAttrLabOp>(
			"OperatorSurfaceI.SurfaceAttrLabOp.fe");
	pLibrary->add<SurfaceAttrPromoteOp>(
			"OperatorSurfaceI.SurfaceAttrPromoteOp.fe");
	pLibrary->add<SurfaceAttrRampOp>(
			"OperatorSurfaceI.SurfaceAttrRampOp.fe");
	pLibrary->add<SurfaceBindOp>(
			"OperatorSurfaceI.SurfaceBindOp.fe");
	pLibrary->add<SurfaceCopyOp>(
			"OperatorSurfaceI.SurfaceCopyOp.fe");
	pLibrary->add<SurfaceDeltaOp>(
			"OperatorSurfaceI.SurfaceDeltaOp.fe");
	pLibrary->add<SurfaceMetricOp>(
			"OperatorSurfaceI.SurfaceMetricOp.fe");
	pLibrary->add<SurfaceNormalOp>(
			"OperatorSurfaceI.SurfaceNormalOp.fe");
	pLibrary->add<SurfaceProxyOp>(
			"OperatorSurfaceI.SurfaceProxyOp.fe");
	pLibrary->add<SurfaceSampleOp>(
			"OperatorSurfaceI.SurfaceSampleOp.fe");
	pLibrary->add<SurfaceWalkOp>(
			"OperatorSurfaceI.SurfaceWalkOp.fe");
	pLibrary->add<SurfaceWrapOp>(
			"OperatorSurfaceI.SurfaceWrapOp.fe");
	pLibrary->add<TransformOp>(
			"OperatorSurfaceI.TransformOp.fe");
	pLibrary->add<TubeOp>(
			"OperatorSurfaceI.TubeOp.fe");
	pLibrary->add<ValidateOp>(
			"OperatorSurfaceI.ValidateOp.fe");

#if FALSE
	pLibrary->add<SurfaceAttrCreateOp>(
			"OperatorSurfaceI.SurfaceAttrCreateOp.fe.nohoudini");
	pLibrary->add<SurfaceAttrDeleteOp>(
			"OperatorSurfaceI.SurfaceAttrDeleteOp.fe.nohoudini");
	pLibrary->add<SurfaceSummaryOp>(
			"OperatorSurfaceI.SurfaceSummaryOp.fe.nohoudini");
#else
	pLibrary->add<SurfaceAttrCreateOp>(
			"OperatorSurfaceI.SurfaceAttrCreateOp.fe");
	pLibrary->add<SurfaceAttrDeleteOp>(
			"OperatorSurfaceI.SurfaceAttrDeleteOp.fe");
	pLibrary->add<SurfaceSummaryOp>(
			"OperatorSurfaceI.SurfaceSummaryOp.fe");
#endif


	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
