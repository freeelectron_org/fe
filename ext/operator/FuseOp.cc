/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

//* TODO delete orphaned points

void FuseOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("snapDistance")=1e-3;
	catalog<Real>("snapDistance","high")=10;
	catalog<Real>("snapDistance","min")=0.0;
	catalog<Real>("snapDistance","max")=1e6;
	catalog<String>("snapDistance","label")="Snap Distance";

	catalog< sp<Component> >("Input Surface");
}

void FuseOp::handle(Record& a_rSignal)
{
	catalog<String>("summary")="";

	const Real snapDistance=catalog<Real>("snapDistance");
	const Real snapSquared=snapDistance*snapDistance;

	const String mode=catalog<String>("mode");

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!accessOutput(spInputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	I32 fuseCount=0;

	const I32 pointCount=spInputPoint->count();

	I32* pRemap=new I32[pointCount];
	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		pRemap[pointIndex]= -1;
	}

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(interrupted())
		{
			break;
		}

		if(pRemap[pointIndex]>=0)
		{
			continue;
		}

		const SpatialVector point=spInputPoint->spatialVector(pointIndex);

		for(I32 otherIndex=pointIndex+1;otherIndex<pointCount;otherIndex++)
		{
			if(pRemap[otherIndex]>=0)
			{
				continue;
			}

			const SpatialVector otherPoint=
					spInputPoint->spatialVector(otherIndex);

			if(magnitudeSquared(otherPoint-point)<snapSquared)
			{
//				feLog("snap %d <%s> to %d <%s>\n",
//						otherIndex,c_print(otherPoint),
//						pointIndex,c_print(point));
				pRemap[otherIndex]=pointIndex;
			}
		}
	}

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const I32 newIndex=pRemap[pointIndex];
		if(newIndex<0)
		{
			continue;
		}

		const SpatialVector point=spInputPoint->spatialVector(newIndex);

		spOutputPoint->set(pointIndex,point);
	}

	const I32 primitiveCount=spOutputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					spOutputVertices->integer(primitiveIndex,subIndex);
			const I32 newIndex=pRemap[pointIndex];
			if(newIndex>=0)
			{
				spOutputVertices->set(primitiveIndex,subIndex,newIndex);
				fuseCount++;
			}
		}
	}

	delete[] pRemap;

	String summary;
	summary.sPrintf("%d vertices fused\n",fuseCount);
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
