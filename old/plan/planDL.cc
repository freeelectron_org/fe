/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "platform/dlCore.cc"
#include "GotoMethod.h"
#include "PlanHandler.h"
#include "DrawChainOps.h"
#include "ChainOpController.h"
#include "DrawRouter.h"
#include "RouteHandler.h"
#include "RouteController.h"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
{
}

FE_DL_EXPORT fe::Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<GotoMethod>("MethodI.Goto.plan.fe");
	pLibrary->add<PlanHandler>("HandlerI.Plan.plan.fe");
	pLibrary->add<DrawChainOps>("HandlerI.DrawChainOps.plan.fe");
	pLibrary->add<ChainOpController>("HandlerI.ChainOpController.plan.fe");
	pLibrary->add<DrawRouter>("HandlerI.DrawRouter.plan.fe");
	pLibrary->add<RouteController>("HandlerI.RouteController.plan.fe");
	pLibrary->add<RouteHandler>("HandlerI.Route.plan.fe");

	pLibrary->add<AsWaypoint>("PopulateI.AsWaypoint.plan.fe");
	pLibrary->add<AsGotoOp>("PopulateI.AsGotoOp.plan.fe");
	pLibrary->add<AsPlan>("PopulateI.AsPlan.plan.fe");
	pLibrary->add<AsRouter>("PopulateI.AsRouter.plan.fe");
	pLibrary->add<AsRouted>("PopulateI.AsRouted.plan.fe");

	return pLibrary;
}

}
