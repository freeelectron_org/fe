/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>	// direct access to ChainSignaler

#include <netsignal/netsignal.pmh>

int main(int argc, char *argv[])
{
	feLogGroup("netsignal","xClient\n");
	fe::UnitTest unitTest;

	U32 complete=FALSE;

	try
	{
		if(argc<3)
			feX("Not enough arguments");

		fe::milliSleep(1000); // ensure server is up in unit tests
		fe::ext::Socket::startup();
		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Registry> spRegistry=spMaster->registry();
		fe::Result result=spRegistry->manage("fexNetSignalDL");
		if(fe::failure(result))
		{
			feX(argv[0], "could not manage fexNetSignalDL %p", result);
		}

		fe::sp<fe::Scope> spScope(new fe::Scope(*spMaster.raw()));
		fe::sp<fe::ext::SignalerI> spSignalerI(new fe::ext::ChainSignaler());
		spSignalerI->setName("ChainSignaler");

		fe::Accessor<int>	a(spScope,"a");
		fe::Accessor<int>	b(spScope,"b");
		fe::Accessor<int>	ttl(spScope, FE_USE(":TTL"));
		fe::sp<fe::Layout>	spSignal = spScope->declare("SIGNAL");
		spSignal->populate(a);
		spSignal->populate(b);
		spSignal->populate(ttl);

		fe::sp<fe::Layout>	spHB = spScope->declare("HEARTBEAT");
		fe::Record hb = spScope->createRecord(spHB);

		fe::sp<fe::ext::ClientI>	spClient(spRegistry->create("ClientI"));
		spClient->addLayout(spSignal);
		spClient->setScope(spScope);

		const fe::ext::Socket::Transport transport=
				(fe::String(argv[1])=="udp")?
				fe::ext::Socket::e_udp: fe::ext::Socket::e_tcp;

		spClient->start(spSignalerI, "127.0.0.1", (short)atol(argv[1]), spHB,
				transport);

		fe::Record signal = spScope->createRecord(spSignal);
		a(signal) = 42;
		b(signal) = 17;
		ttl(signal) = 10;
		spSignalerI->signal(signal);
		spSignalerI->signal(hb);

		spClient->stop();

		complete=TRUE;

//		spScope->shutdown();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	unitTest(complete,"incomplete");
	return unitTest.failures();
}
