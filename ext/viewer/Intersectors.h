/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_RayIntersectors_h__
#define __viewer_RayIntersectors_h__

namespace fe
{
namespace ext
{

#if 0
class FE_DL_EXPORT WorldSphereIntersector :
	virtual public IntersectorI
{
	public:
					WorldSphereIntersector(void);
virtual				~WorldSphereIntersector(void);
		// AS IntersectorI
virtual	bool		intersect(	Real &a_distance,
								Record r_object,
								const Vector2 &a_projected,
								const SpatialVector &a_root,
								const SpatialVector &a_direction);
virtual	AccessorSet	&filter(	void);

	private:
		AsSelWorldSphere	m_asSelSphere;
};

class FE_DL_EXPORT WorldTriangleIntersector :
	virtual public IntersectorI
{
	public:
					WorldTriangleIntersector(void);
virtual				~WorldTriangleIntersector(void);
		// AS IntersectorI
virtual	bool		intersect(	Real &a_distance,
								Record r_object,
								const Vector2 &a_projected,
								const SpatialVector &a_root,
								const SpatialVector &a_direction);
virtual	AccessorSet	&filter(	void);

	private:
		AsSelWorldTriangle	m_asSelTri;
};

class FE_DL_EXPORT ScreenTriangleIntersector :
	virtual public IntersectorI
{
	public:
					ScreenTriangleIntersector(void);
virtual				~ScreenTriangleIntersector(void);
		// AS IntersectorI
virtual	bool		intersect(	Real &a_distance,
								Record r_object,
								const Vector2 &a_projected,
								const SpatialVector &a_root,
								const SpatialVector &a_direction);
virtual	AccessorSet	&filter(	void);

	private:
		AsSelScreenTriangle	m_asSelTri;
};
#endif

class FE_DL_EXPORT IntersectRaySphere :
	virtual public IntersectRayI
{
	public:
					IntersectRaySphere(void);
virtual				~IntersectRaySphere(void);
		// AS WorldPickI
virtual	Record		intersect(	Real &a_distance,
								sp<RecordGroup> rg_output,
								sp<RecordGroup> rg_input,
								const SpatialVector &a_root,
								const SpatialVector &a_direction);

	private:
		AsSelWorldSphere	m_asSelSphere;
};

class FE_DL_EXPORT IntersectPointTriangle :
	virtual public IntersectPointI
{
	public:
					IntersectPointTriangle(void);
virtual				~IntersectPointTriangle(void);
		// AS ScreenPickI
virtual	Record		intersect(	Real &a_distance,
								sp<RecordGroup> rg_output,
								sp<RecordGroup> rg_input,
								const Vector2 &a_projected);

	private:
		AsSelScreenTriangle	m_asSelTri;
};

class FE_DL_EXPORT IntersectRectPoint :
	virtual public IntersectRectI
{
	public:
					IntersectRectPoint(void);
virtual				~IntersectRectPoint(void);
virtual	void		intersect(	sp<RecordGroup> rg_output,
								sp<RecordGroup> rg_input,
								const Vector2 &a_box_lo,
								const Vector2 &a_box_hi,
								sp<ViewI> spView);
	private:
		AsPoint		m_asPoint;
		AsProjected	m_asProjected;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_RayIntersectors_h__ */

