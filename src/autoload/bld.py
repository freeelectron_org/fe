import sys
import glob
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "autoload.pmh",
                "autoloadDL" ]

    dll = module.DLL( "feAutoLoadDL", srcList )

    deplibs = forge.basiclibs + [ "fePluginLib" ]

    forge.deps( ["feAutoLoadDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "feAutoLoadDL",                 None,   None) ]
