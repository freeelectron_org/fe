import sys
import os
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
        'odeLib',
        'fexSignalLib',
        "fexDataToolDLLib",
        'fexViewerDLLib',
        'fexNetworkDLLib',
        'fexNetSignalDLLib',
        'fexWindowDLLib' ]

    tests = [   'xWorld' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    plugin_path = os.path.join(module.modPath, 'plugin')

