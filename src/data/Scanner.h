/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Scanner_h__
#define __data_Scanner_h__

namespace fe
{
namespace data
{

class t_layout_info
{
	public:
		t_layout_info(void);
		int		m_id;
		bool	m_sent;
};

class t_attr_info
{
	public:
		int			m_id;
		sp<Scope>	m_scope;
};


typedef std::map<sp<Layout>, t_layout_info>		t_layout_loinfo;
typedef std::map<sp<RecordGroup>, int>			t_rg_id;
typedef std::map<sp<RecordArray>, int>			t_ra_id;
typedef std::map<FE_UWORD, int>					t_sb_id;
typedef std::map<sp<Attribute>, t_attr_info>	t_attr_id;
typedef Array<sp<Attribute> >					t_attr_array;

class FE_DL_EXPORT Scanner
{
	public:
					Scanner(sp<Scope> spScope);
virtual				~Scanner(void);

	protected:
virtual	void		scan(sp<RecordGroup> spRG);
virtual	void		scan(sp<Layout> spLayout);
virtual	void		scan(Record record);
virtual	void		scan(sp<Attribute> spAttribute, sp<Scope> spScope);
virtual	void		scan(sp<RecordArray> spRA);

virtual	void		deepscan(sp<RecordArray> spRA);

virtual	void		setupSBIDs(sp<RecordGroup> spRG);

		int			getID(sp<RecordGroup> spRG);
		int			getID(sp<RecordArray> spRA);
	protected:
		t_attr_array		m_attrArray;
		t_layout_loinfo		m_layouts;
		t_rg_id				m_rgs;
		t_ra_id				m_ras;
		t_sb_id				m_sbs;
		t_attr_id			m_attrs;
		Array<I32>			m_recordRGID;	//* first RG per record_id
		Array<I32>			m_firstRGID;	//* first RG per sb_id
		sp<RecordGroup>		m_spScannedRecords;
		U32					m_nextLayoutID;
		U32					m_nextRGID;
		U32					m_nextAttrID;
		U32					m_sentAttrID;
		sp<Scope>			m_spScope;
		sp<BaseType>		m_spRecordGroupType;
		sp<BaseType>		m_spRecordArrayType;
		sp<BaseType>		m_spRecordType;
		sp<BaseType>		m_spWeakRecordType;
		sp<BaseType>		m_spVoidType;
};

} /* namespace */
} /* namespace */

#endif /* __data_Scanner_h__ */
