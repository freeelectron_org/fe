/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <arnold/arnold.pmh>

namespace fe
{
namespace ext
{

#if FALSE
int     x;          // X raster-space coordinate of this ray tree
int     y;          // Y raster-space coordinate of this ray tree
float   sx;         // X image-space coordinate of this ray, in [-1,1)
float   sy;         // Y image-space coordinate of this ray, in [-1,1)
AtUInt16si;         // subpixel sample index
AtUInt16i;          // path index (or light sample index for shadow rays)
AtUInt16n;          // total number of samples to take
AtUInt16Rt;         // ray type
float   we;         // sample weight
AtUInt32 transp_index;// transparency index
AtPoint Ro;         // ray origin (typically the camera position)
AtVectorRd;         // ray direction
double  Rl;         // ray length (|Ro-P|)
AtByte  tid;        // thread ID
AtByte  Rr;         // recursion level for the ray that created this hit
AtByte  Rr_refl;    // ray reflection depth level
AtByte  Rr_refr;    // ray refraction depth level
AtByte  Rr_diff;    // ray diffuse depth level
AtByte  Rr_gloss;   // ray glossy depth level
bool    fhemi;      // force hemispherical lighting
float   time;       // absolute time, between shutter-open and shutter-close
AtUInt32fi;         // primitive ID (triangle, curve segment, etc)
AtNode* Op;         // object pointer
AtNode* proc;       // procedural object pointer (if exists)
AtNode* shader;     // pointer to the current shader
const AtShaderGlobals* psg;   // parent shader globals (last shaded)
AtBucket* bkt;      // bucket that the pixel being rendered belongs to
AtPoint  Po;        // shading point in object-space
AtPoint  P;         // shading point in world-space
AtVector dPdx;      // surface derivative wrt screen X-axis (not normalized)
AtVector dPdy;      // surface derivative wrt screen Y-axis (not normalized)
AtVector N;         // shading normal
AtVector Nf;        // face-forward shading normal
AtVector Ng;        // geometric normal
AtVector Ngf;       // face-forward geometric normal
AtVector Ns;        // smoothed normal (same as N but without bump)
float    bu;        // barycentric coordinate (aka alpha, or u)
float    bv;        // barycentric coordinate (aka beta, or v)
float    u;         // U surface parameter
float    v;         // V surface parameter
AtMatrix M;         // local-to-world matrix transform
AtMatrix Minv;      // world-to-local matrix transform
AtNode** lights;    // array of active lights at this shading context
AtNode*  Lp;        // pointer to current light node
int      nlights;   // number of active lights
float    Ldist;     // distance from P to light source
AtVector Ld;        // incident direction from light source
AtColor  Li;        // incoming intensity from light source
AtColor  Liu;       // unoccluded incoming intensity from light source
AtColor  Lo;        // shadow occlusion from light source
AtColor  Ci;        // input color (for volume shaders)
AtColor  Vo;        // output volume radiance
float    area;      // in \c AI_CONTEXT_SURFACE this is the world-space area
					// being shaded; in \c AI_CONTEXT_DISPLACEMENT it's the
					// average area of all un-displaced triangles shared by
					// the current vertex
AtVector dPdu;      // surface derivative wrt U parameter (not normalized)
AtVector dPdv;      // surface derivative wrt V parameter (not normalized)
AtVector dDdx;      // ray direction derivative wrt screen X-axis
					// (not normalized) */
AtVector dDdy;      // ray direction derivative wrt screen Y-axis
					// (not normalized) */
AtVector dNdx;      // derivative of surface normal with respect to X-axis
AtVector dNdy;      // derivative of surface normal with respect to Y-axis
float    dudx;      // U derivative wrt screen X-axis (not normalized)
float    dudy;      // U derivative wrt screen Y-axis (not normalized)
float    dvdx;      // V derivative wrt screen X-axis (not normalized)
float    dvdy;      // V derivative wrt screen Y-axis (not normalized)
#endif

//* static
I32 ShaderVariablesArnold::variableOffset(String a_variable)
{

	if(a_variable=="x")			return offsetof(AtShaderGlobals,x);
	else if(a_variable=="y")	return offsetof(AtShaderGlobals,y);
	else if(a_variable=="sx")	return offsetof(AtShaderGlobals,sx);
	else if(a_variable=="sy")	return offsetof(AtShaderGlobals,sy);
	else if(a_variable=="Rd")	return offsetof(AtShaderGlobals,Rd);
	else if(a_variable=="Ro")	return offsetof(AtShaderGlobals,Ro);
	else if(a_variable=="P")	return offsetof(AtShaderGlobals,P);
	else if(a_variable=="N")	return offsetof(AtShaderGlobals,N);
	else if(a_variable=="u")	return offsetof(AtShaderGlobals,u);
	else if(a_variable=="v")	return offsetof(AtShaderGlobals,v);

	return -1;
}

} /* namespace ext */
} /* namespace fe */
