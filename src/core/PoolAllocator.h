/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_PoolAllocator_h__
#define __core_PoolAllocator_h__

namespace fe
{

struct FE_PA_FREE
{
	void *m_next;
};

struct FE_PA_USED
{
	size_t m_s;
};

#define HDR_TO_BLK(x) ((void *)((char *)x + sizeof(size_t)))
#define BLK_TO_HDR(x) ((void *)((char *)x - sizeof(size_t)))

class FE_DL_EXPORT PoolAllocator : public Allocator
{
	public:
				PoolAllocator(void);
				PoolAllocator(FE_UWORD stride, FE_UWORD slots);
virtual			~PoolAllocator(void);
virtual	void	*allocate(FE_UWORD byteCount);
virtual	void	deallocate(void* pMemory);
	private:
		FE_UWORD		m_max;
		void		**m_nfList;
		FE_UWORD		m_stride;
};

inline void *PoolAllocator::allocate(FE_UWORD byteCount)
{
	if(byteCount == 0) { return NULL; }
	size_t s = (byteCount-1)/m_stride;
	if(s < m_max && m_nfList[s])
	{
		void *pBase = m_nfList[s];
		m_nfList[s] = ((FE_PA_FREE *)(pBase))->m_next;
		((FE_PA_USED *)(pBase))->m_s = s;
		return (void *)(HDR_TO_BLK(pBase));
	}
	else
	{
		void *pBase = fe::allocate(((s+1)*m_stride)+sizeof(FE_UWORD));
		((FE_PA_USED *)(pBase))->m_s = s;
		return (void *)(HDR_TO_BLK(pBase));
	}
}

inline void PoolAllocator::deallocate(void* pMemory)
{
	FE_UWORD s = ((FE_PA_USED *)BLK_TO_HDR(pMemory))->m_s;
	if(s < m_max)
	{
		void *pBase = BLK_TO_HDR(pMemory);
		((FE_PA_FREE *)(pBase))->m_next = m_nfList[s];
		m_nfList[s] = pBase;
	}
	else
	{
		fe::deallocate(BLK_TO_HDR(pMemory));
	}
}


} /* namespace */

#endif /* __core_PoolAllocator_h__ */

