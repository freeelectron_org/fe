import sys
import os
import re
import glob
import shutil
forge = sys.modules["forge"]

def prerequisites():
    return ["maya"]

def setup(module):
    module.summary = []

    patchelfDir = module.modPath + "/patchelf-0.9"
    patchelf = patchelfDir + "/src/patchelf"
    if not os.path.exists(patchelf):
        patchelf_bz2 = module.modPath + "/patchelf-0.9.tar.bz2"
        if os.path.exists(patchelf_bz2):
            os.system("cd " + module.modPath + ";tar xf patchelf-0.9.tar.bz2")
            os.system("cd " + patchelfDir + ";./configure >/dev/null")
            os.system("cd " + patchelfDir + ";make>/dev/null")

    patch = os.path.exists(patchelf)
    if patch:
        module.summary += [ "patch" ]
    else:
        module.summary += [ "-patch" ]

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")

    test_dllname = ""

    for maya_source in reversed(sorted(forge.maya_sources)):
        maya_version = maya_source[0]

        # NOTE Maya 2016 has broken XpdFile.h (no such xpd/Xpd.h)
#       if re.match("2016", maya_version):
#           continue

        if re.match("2014", maya_version):
            continue

        maya_root = maya_source[1] + "/.."

        maya_xgen = maya_root + "/plug-ins/xgen"
        if not os.path.exists(maya_xgen):
            continue

        maya_lib = maya_root + "/lib"
        xgen_lib = maya_xgen + "/lib"

        module.summary += [ maya_version ]

        xgen_major = maya_version.split(".")[0].split("sp")[0].split("pr")[0]

        srcList = [ "SurfaceAccessibleXGen",
                    "xgen.pmh",
                    "xgenDL" ]

        # forcibly rip out OpenGL dependencies from XGen API
        xgen_relib = "lib/" + forge.apiCodegen + "/xgen" + maya_version
        if os.path.isdir(xgen_relib) == 0:
            os.makedirs(xgen_relib, 0o755)
        xgenDsos = os.listdir(xgen_lib)

        for xgenDso in xgenDsos:
            dsoSource = os.path.join(xgen_lib, xgenDso)
            dsoDest = os.path.join(xgen_relib, xgenDso)
            shutil.copyfile(dsoSource, dsoDest)

            clean_patchelf = forge.cleanup_command(patchelf)

            if patch:
                os.system(clean_patchelf + " --remove-needed libGL.so.1 " + dsoDest)
                os.system(clean_patchelf + " --remove-needed libGLU.so.1 " + dsoDest)
                os.system(clean_patchelf + " --remove-needed libclew.so " + dsoDest)
        if patch:
            srcList += [ "fakeGL" ]

        variant = maya_version
        if variant != "":
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink('.', variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        dllname = "fexXGenDL" + variant

        with open(manifest, 'a') as outfile:
            suffix = ""
            if variant != "":
                suffix = "." + variant
            outfile.write('\tspManifest->catalog<String>(\n'+
                    '\t\t\t"SurfaceAccessibleI.SurfaceAccessibleXGen.fe.xgen'+
                    suffix + '")=\n'+
                    '\t\t\t"fexXGenDL' + variant + '";\n');

        dll = module.DLL(dllname, srcList)

        module.cppmap = {}
        module.cppmap['woe_conditionally'] = ""

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)

            srcTarget.cppmap = {}
            srcTarget.cppmap["xgen"] = " -D XGEN_MAJOR=" + xgen_major

            srcTarget.includemap = {}
            srcTarget.includemap['maya'] = maya_root + "/include"
            srcTarget.includemap['xgen'] = maya_xgen + "/include"
            srcTarget.includemap['xgen2'] = maya_xgen + "/include/xgen"
            srcTarget.includemap['xgen3'] = maya_xgen + "/include/XGen"
            srcTarget.includemap['xgen4'] = maya_xgen + "/include/xpd"
            srcTarget.includemap['xgen5'] = maya_xgen + "/include/xgen/src/xgcore"
            srcTarget.includemap['xgen6'] = maya_xgen + "/include/xgen/src/xgrenderer"
            srcTarget.includemap['xgen7'] = maya_xgen + "/include/xgen/src/xgtask"

            # broken include in 2014, 2016, 2017, and early 2018
            if xgen_major == "2015" or os.path.exists(maya_xgen + "/include/xpd/XpdFile.h"):
                srcTarget.cppmap["xpd"] = " -D FE_XGEN_XPD"

            # fake Ptex/PtexVersion.h
            if xgen_major >= "2017":
                srcTarget.includemap['ptex'] = module.modPath + "/include"

            # need std::shared::ptr
            srcTarget.cppmap['std'] = forge.use_std("c++0x")

        dll.linkmap = { "xgen" : "" }

        # Ptex is not directly used, but seems to get rpath to work
        dll.linkmap['xgen'] += " -Wl,--no-as-needed"
        dll.linkmap['xgen'] += " -Wl,-rpath='" + maya_lib + "/'"
        dll.linkmap['xgen'] += " -Wl,--as-needed"

        # TODO check which version stopped needing this
        if xgen_major < "2023":
            dll.linkmap['xgen'] += " -L" + maya_lib + " -lPtex"

        if patch:
            dll.linkmap['xgen'] += " -Wl,-rpath='$ORIGIN/xgen" + maya_version + "/'"
        else:
            dll.linkmap['xgen'] += " -Wl,-rpath='" + maya_xgen + "/lib/'"

        dll.linkmap['xgen'] += " -L" + xgen_relib + " -lAdskXGen"
        dll.linkmap['xgen'] += " -L" + xgen_relib + " -lAdskXpd"
        dll.linkmap['xgen'] += " -L" + xgen_relib + " -lAdskSeExpr"

        if xgen_major >= "2017":
            # for std::bad_weak_ptr
            dll.linkmap['xgen'] += " -L" + xgen_relib + " -lAdskFabricGeom"

        if re.match("2014", maya_version):
            dll.linkmap['xgen'] += " -L" + xgen_relib + " -lAdskGLee"

        deplibs = forge.corelibs + [
                    "fexSurfaceDLLib" ]

        forge.deps([ dllname + "Lib" ], deplibs)

        if test_dllname == "":
            test_dllname = dllname

    if test_dllname != "":
        forge.tests += [
            ("inspect.exe",     test_dllname,   None,       None) ]

    module.Module('test')

def auto(module):
    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        return 'TODO Windows'

    # NOTE no build test
    return None
