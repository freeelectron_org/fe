#!/usr/bin/python

import sys

import fe
import fe.data
import fe.math
print '-------------'

def Test():
    # create a scope 
    scope = fe.data.Scope()
    print '-------------'
    #sys.exit(0)

    # create a layout
    layout = scope.declare("test")
    layout.populate("attr", "int");
    layout.populate("name", "string");
    layout.populate("vec", "vector3");

    sig = scope.declare("signal")
    sig.populate(gn_handlerGroup, "RecordGroup");


    # create records
    print 1
    record = scope.createRecord(layout)
    print 2
    record2 = scope.createRecord(layout)
    print 3
    scope.dump()
    record.name = "happy"
    print 4
    record2.name = record.name
    print 5
    record.name = "sad"
    print record.name
    print record2.name
    print record.check("noexist")
    print record.check("attr", "name")


    signal = scope.createRecord(sig)

    recordarray = scope.createRecordArray(layout)
    print recordarray

    v = fe.math.vector3()
    v[0] = 10
    v[1] = 11
    v[2] = 12
    print v[1]
    v2 = fe.math.vector3()
    v2[0] = 0
    v2[1] = 1
    v2[2] = 0
    v3 = v.cross(v2)
    print 'v ' + str(v3[0])
    print 'v ' + str(v3[1])
    print 'v ' + str(v3[2])

    recordarray.add(record, record2, record)
    print len(recordarray)
    for r in recordarray:
        r.vec[1] = 10
        r.vec = v
        print r.vec[1]
    for r in recordarray:
        print '-- ' + r.name
        v = fe.math.vector3()
        print r.vec

    print recordarray.check("noexist")
    print recordarray.check("attr")

    rg = fe.data.RecordGroup()
    rg.add(record)
    rg.add(record)
    rg.add(signal)

    signal2 = scope.createRecord(sig)
    rg.put(signal2.group)

    rg2 = fe.data.RecordGroup()
    rg2.bind(signal2.group)
    rg2.add(record)

    layout = fe.data.Layout()


    for ra in rg2:
        print "have an array " + str(ra.check("attr"))
        for r in ra:
            print "record " + str(r.check("attr"))
            


    #scope.dump()

if __name__ == "__main__" :
    try:
        Test()
        print 'success'
    except Exception, e:
        print e
        sys.exit(1)
    
    sys.exit(0)
