import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "spatial" ]

def setup(module):
    # the plugin
    srcList = [ "planet.pmh",
                "CreatePlanetHandler",
                "FloraHandler",
                "planetDL"
                ]

    dll = module.DLL( "fexPlanetDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexSolveDLLib",
                "fexSpatialDLLib",
                "fexDataToolDLLib",
                "fexViewerDLLib" ]

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    forge.deps( ["fexPlanetDLLib"], deplibs )

    module.Module('test')
