/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "planet/planet.pmh"

namespace fe
{
namespace ext
{

CreatePlanetHandler::CreatePlanetHandler(void)
{
}

CreatePlanetHandler::~CreatePlanetHandler(void)
{
}

void CreatePlanetHandler::initialize(void)
{
}

void CreatePlanetHandler::createPlanet(Record r_planet)
{
	sp<ScalarFieldI> spScalarField =
		m_asScalarField.field(m_asPlanet.organics(r_planet));

	if(spScalarField.isValid())
	{
		return;
	}

	spScalarField = create("ScalarFieldI.Density");
	if(spScalarField.isValid())
	{
fe_fprintf(stderr, "created ScalarFieldI.Density\n");
		sp<GridScalarField> spGridScalarField = spScalarField;
		if(spGridScalarField.isValid())
		{
fe_fprintf(stderr, "is GridScalarField\n");
			sp<AffineSpace> spSpace = create("SpaceI.AffineSpace");
			assert(spSpace.isValid());
			spSpace->setAxisAligned(SpatialVector(0.0,0.0,0.0), SpatialVector(10.0,10.0,1.0));
			Vector3i count(10,10,1);
			spGridScalarField->create(spSpace, count);
		}
		m_asScalarField.field(m_asPlanet.organics(r_planet)) = spScalarField;


		spScalarField->add(SpatialVector(5.5,7.5,0.0), 100.0);
		spScalarField->add(SpatialVector(5.0,6.0,0.0), 10.0);
		spScalarField->add(SpatialVector(5.0,5.0,0.0), 10.0);
		spScalarField->add(SpatialVector(5.0,4.0,0.0), 10.0);
		spScalarField->add(SpatialVector(5.0,3.0,0.0), 10.0);
	}
}

void CreatePlanetHandler::handle(Record &r_sig)
{

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);

		m_asPlanet.bind(spRA->layout()->scope());
		m_asScalarField.bind(spRA->layout()->scope());

		if(m_asPlanet.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(m_asPlanet.organics(spRA, i).isValid())
				{
					createPlanet(spRA->getRecord(i));
				}
			}
		}
	}
}


} /* namespace ext */
} /* namespace fe */


