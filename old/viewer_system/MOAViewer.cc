/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer_system/viewer_system.pmh"

namespace fe
{
namespace ext
{

void MOAViewer::initialize()
{
    // Turn on x8 MSAA antialiasing to make it pretty.
    registry()->master()->catalog()->catalog<I32>("hint:multisample") = 8;

	windowScope = registry()->master()->catalog()->catalogComponent(
			"Scope","WinScope");

	viewerTask.setup();
};

void MOAViewer::open(sp<OrchestratorI> orchestrator)
{
	if(isOpen)
	{
		return;
	}

	viewerTask.orchestrator = orchestrator;

	viewerThread = new Thread(&viewerTask);

	isOpen = true;
};

void MOAViewer::close()
{
	if(isOpen)
	{
		viewerTask.poison.start();
		viewerThread->join();
		delete viewerThread;

		viewerTask.orchestrator = NULL;

		isOpen = false;
	}
};

MOAViewer::~MOAViewer()
{
	// Gotta kill the thread before we can die.
	close();
}

Color MOAViewer::getDefaultBgColor() const
{
	String bgcolorstr;
	Color bgcolor(0.0,0.0,0.0,1.0);

	if(System::getEnvironmentVariable("FE_VIEWER_BG_COLOR", bgcolorstr))
	{
		int i = 0;
		while(!bgcolorstr.empty())
		{
			bgcolor[i++] = atof(bgcolorstr.parse("\"", ",").c_str());
		}
	}
	else
	{
		bgcolor = {0.2f,0.2f,0.2f};
	}

	return bgcolor;
};

void MOAViewer::ViewerTask::setup()
{
	window = parent.registry()->create("WindowI");
	controller = parent.registry()->create("CameraControllerI.InspectController");
	signaler = parent.registry()->create("SignalerI");;

	if(!window.isValid() || !controller.isValid() ||!signaler.isValid())
	{
		feX("MOAViewer::ViewerTask::setup","couldn't create components");
	}

	heartbeatLayout = parent.windowScope->declare("heartbeat");
	//* NOTE EventContext doesn't care what signal it is
	signaler->insert(window->getEventContextI(), heartbeatLayout);

	sp<Layout> eventLayout = parent.windowScope->lookupLayout(FE_EVENT_LAYOUT);
	FEASSERT(eventLayout.isValid());
	signaler->insert(controller, eventLayout);

	camera = controller->camera();

	window->setBackground(parent.getDefaultBgColor());
}

void MOAViewer::ViewerTask::operate()
{
	t_note_id drawNote = orchestrator->note_id(FE_NOTE_DRAW);

	// Create viewer record.
	sp<RecordGroup> dataset = orchestrator->dataset();
	sp<Scope> scope = dataset->scope();
	sp<Layout> viewportLayout = scope->declare("viewport");
	viewportAccessorSet.populate(viewportLayout);
	Record viewport = scope->createRecord(viewportLayout);
	dataset->add(viewport);

	// Create window heartbeat signal
	Record heartbeat = parent.windowScope->createRecord(heartbeatLayout);

	// WindowI::open() has to be called on the same thread as it's run so that
    // the GLX context gets picked up.
	window->open("MOAViewer");

	// Also needs GLX context.
    sp<ext::DrawI> draw = parent.registry()->create("DrawI");
	sp<ext::ViewI> view = draw->view();

	view->setWindow(window);
	view->setCamera(camera);

	viewportAccessorSet.draw(viewport) = draw;
	viewportAccessorSet.threadId(viewport) = std::this_thread::get_id();

	// Tell the orchestrator about the new viewport so the draw systems know.
	orchestrator->perform(orchestrator->note_id(FE_NOTE_ADDED_VIEWPORT));

	// Thread loop.
    while (!poison.active() && !controller->closeEvent())
    {
		// Signal for window and controller event handlers.
		signaler->signal(heartbeat);

		// Update viewport size.
		Box2i viewport=window->geometry();
		viewport[0]=0;
		viewport[1]=0;
		view->setViewport(viewport);

		// For some reason View needs to get it's projection mode updated
		// every tick.
		view->use(ext::ViewI::Projection::e_perspective);

		// Perform the draw.
		orchestrator->perform(drawNote);

		// Update display.
		draw->flush();

		if(!draw->isDirect())
		{
			//* don't flood a remote connection
			milliSleep(100);
		}
    }

	poison.stop();

	dataset->remove(viewport);

	// The DrawI will be destructed at the end of this scope and all the systems
	// MUST stop using it.
	orchestrator->perform(orchestrator->note_id(FE_NOTE_REMOVED_VIEWPORT));

	// Explicitly invalidate the weak pointer since the DrawI is about to
	// get destructed.
	viewportAccessorSet.draw(viewport).invalidate();

	window->close();
}

} /* namespace ext */
} /* namespace fe */
