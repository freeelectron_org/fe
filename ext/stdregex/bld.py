import os
import sys
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "stdregex.pmh",
                "stdregexDL" ]

    module.includemap = {}
    module.cppmap = { 'std' : forge.use_std("c++11") }

    dll = module.DLL( "fexStdRegex", srcList )

    # HACK avoid problems with Houdini (but everything else breaks)
    # NOTE boost regex works fine with Houdini
#   for src in srcList:
#       srcTarget = module.FindObjTargetForSrc(src)
#       srcTarget.cppmap = { "cxx11_abi" : "-D_GLIBCXX_USE_CXX11_ABI=0" }

    deplibs = forge.basiclibs[:]

    dll.linkmap = {}

    forge.deps( ["fexStdRegexLib"], deplibs )

def auto(module):
    test_file = """
#include <regex>

int main(void)
{
    std::string input = "I know, I'll use regular expressions.";
    std::regex re("REGULAR EXPRESSIONS",std::regex_constants::icase);

    return !std::regex_search(input, re);
}
    \n"""

    std_orig = forge.cppmap.get('std', "")
    forge.cppmap['std'] = forge.use_std("c++11")

    result = forge.cctest(test_file)

    forge.cppmap['std'] = std_orig

    return result
