INFO 5
ATTRIBUTE name string
ATTRIBUTE dataset RecordGroup group
ATTRIBUTE control:active bool boolean
ATTRIBUTE aspect:label string
ATTRIBUTE unidir:gear integer I32 int I16
ATTRIBUTE unidir:drive_mode integer I32 int I16
ATTRIBUTE unidir:clutch_friction float real F32
ATTRIBUTE unidir:clutch_torque float real F32
ATTRIBUTE unidir:auto_clutch float real F32
ATTRIBUTE unidir:manual_clutch float real F32
ATTRIBUTE unidir:max_engine_rps float real F32
ATTRIBUTE unidir:throttle float real F32
ATTRIBUTE unidir:ram_current_speed float real F32
ATTRIBUTE unidir:baulk_torque float real F32
ATTRIBUTE unidir:gear_ratios realarray
ATTRIBUTE unidir:diff_preload_torque float real F32
ATTRIBUTE unidir:diff_pump_torque float real F32
ATTRIBUTE unidir:diff_power_fract float real F32
ATTRIBUTE unidir:diff_coast_fract float real F32
ATTRIBUTE unidir:diff_4wd_rear_torque_split float real F32
ATTRIBUTE unidir:diff_4wd_pump_effect vector3 vector3f spatial_vector
ATTRIBUTE unidir:diff_4wd_preload_effect vector3 vector3f spatial_vector
ATTRIBUTE unidir:diff_4wd_power_effect vector3 vector3f spatial_vector
ATTRIBUTE unidir:diff_4wd_coast_effect vector3 vector3f spatial_vector
ATTRIBUTE unidir:handbrake float real F32
ATTRIBUTE unidir:handbrake_4wd_disco float real F32
ATTRIBUTE unidir:clutch_inertia float real F32
ATTRIBUTE unidir:engine_inertia float real F32
ATTRIBUTE unidir:recip_diff_ratio float real F32
ATTRIBUTE unidir:clutch_rps float real F32
ATTRIBUTE unidir:engine_rps float real F32
ATTRIBUTE unidir:transmission_torque float real F32
ATTRIBUTE unidir:diff_prev_baulk_wheel float real F32
ATTRIBUTE unidir:wheel_rps vector4f vector4
ATTRIBUTE unidir:wheel_pitch vector4f vector4
ATTRIBUTE unidir:brake_thickness vector4f vector4
ATTRIBUTE unidir:brake_inertia vector4f vector4
ATTRIBUTE unidir:wheel_inertia vector4f vector4
ATTRIBUTE unidir:wheel_input_torque vector4f vector4
ATTRIBUTE unidir:brake_torque vector4f vector4
ATTRIBUTE splat:expression string
ATTRIBUTE splat:x_range vector2f vector2
ATTRIBUTE splat:y_range vector2f vector2
ATTRIBUTE splat:color vector3 vector3f spatial_vector
ATTRIBUTE splat:splat string
ATTRIBUTE splatline:is void
ATTRIBUTE time:time float real F32
ATTRIBUTE event:action string
ATTRIBUTE composer:step string
ATTRIBUTE composer:outer stringarray
ATTRIBUTE composer:inner stringarray
ATTRIBUTE composer:linger float real F32
ATTRIBUTE composer:frequency float real F32
ATTRIBUTE composer:runup float real F32
ATTRIBUTE driveline:model string
ATTRIBUTE driveline:contents RecordGroup group
ATTRIBUTE driveline:spDrivelineI DrivelineI
ATTRIBUTE driveline:momentOfInertia atomic
ATTRIBUTE driveline:externalTorque atomic
ATTRIBUTE spc:location1d float real F32
ATTRIBUTE spc:velocity1d float real F32
ATTRIBUTE sim:force1d float real F32
ATTRIBUTE sim:mass float real F32
ATTRIBUTE sim:index integer I32 int I16
ATTRIBUTE sim:prevVelocity1d float real F32
ATTRIBUTE sim:prevLocation1d float real F32
ATTRIBUTE sim:externalForce1d float real F32
ATTRIBUTE pair:left WeakRecord
ATTRIBUTE pair:right WeakRecord
ATTRIBUTE mat:stiffness float real F32
ATTRIBUTE mat:damping float real F32
ATTRIBUTE mat:flags integer I32 int I16
ATTRIBUTE bdy:length float real F32
ATTRIBUTE lsf:spring voidstar
ATTRIBUTE driveline:left_gear integer I32 int I16
ATTRIBUTE driveline:right_gear integer I32 int I16
ATTRIBUTE driveline:component string
ATTRIBUTE header:runcode string
ATTRIBUTE header:datacode string
ATTRIBUTE mixer:inputs stringarray
ATTRIBUTE time:dt float real F32
ATTRIBUTE eval:value double F64
ATTRIBUTE eval:string string
LAYOUT Producer.run.default.dataset
	name
	dataset
	control:active
LAYOUT MOAHEADER
	name
	header:runcode
	header:datacode
LAYOUT ScopeDummy
LAYOUT aspect
	aspect:label
LAYOUT unidir_driveline
	name
	unidir:gear
	unidir:drive_mode
	unidir:clutch_friction
	unidir:clutch_torque
	unidir:auto_clutch
	unidir:manual_clutch
	unidir:max_engine_rps
	unidir:throttle
	unidir:ram_current_speed
	unidir:baulk_torque
	unidir:gear_ratios
	unidir:diff_preload_torque
	unidir:diff_pump_torque
	unidir:diff_power_fract
	unidir:diff_coast_fract
	unidir:diff_4wd_rear_torque_split
	unidir:diff_4wd_pump_effect
	unidir:diff_4wd_preload_effect
	unidir:diff_4wd_power_effect
	unidir:diff_4wd_coast_effect
	unidir:handbrake
	unidir:handbrake_4wd_disco
	unidir:clutch_inertia
	unidir:engine_inertia
	unidir:recip_diff_ratio
	unidir:clutch_rps
	unidir:engine_rps
	unidir:transmission_torque
	unidir:diff_prev_baulk_wheel
	unidir:wheel_rps
	unidir:wheel_pitch
	unidir:brake_thickness
	unidir:brake_inertia
	unidir:wheel_inertia
	unidir:wheel_input_torque
	unidir:brake_torque
LAYOUT splat
	name
	splat:expression
	splat:x_range
	splat:y_range
	splat:color
	splat:splat
	splatline:is
LAYOUT event
	time:time
	event:action
LAYOUT multisweep
	name
	composer:step
	composer:outer
	composer:inner
	composer:linger
	composer:frequency
	composer:runup
LAYOUT driveline
	name
	driveline:model
	driveline:contents
	driveline:spDrivelineI
LAYOUT driveline_model
	name
	driveline:component
LAYOUT driveline_shaft_forced
	name
	driveline:momentOfInertia
	driveline:externalTorque
	spc:location1d
	spc:velocity1d
	sim:force1d
	sim:mass
	sim:index
	sim:prevVelocity1d
	sim:prevLocation1d
	sim:externalForce1d
LAYOUT torsion_spring
	name
	pair:left
	pair:right
	mat:stiffness
	mat:damping
	mat:flags
	bdy:length
	lsf:spring
	driveline:left_gear
	driveline:right_gear
LAYOUT dataset_mixer
	name
	mixer:inputs
	control:active
LAYOUT dataset
	name
	dataset
LAYOUT layout_time
	name
	time:time
	time:dt
LAYOUT l_eval_real
	eval:value
LAYOUT l_eval_string
	eval:string
LAYOUT X
	eval:value
LAYOUT OUTER
	eval:value
DEFAULTGROUP 1
RECORD Producer.run.default.dataset1 Producer.run.default.dataset
	name "DSM--UNIDIR overlay--DSM"
	dataset RG2
	control:active 1
DEFAULTGROUP RG2
RECORD Producer.run.default.dataset2 Producer.run.default.dataset
	name "DSM--UNIDIR overlay--DSM"
	dataset RG2
	control:active 1
RECORD Producer.run.default.dataset3 Producer.run.default.dataset
	name "DSM--New Setup--DSM"
	dataset RG3
	control:active 1
DEFAULTGROUP RG4
RECORD Producer.run.default.dataset4 Producer.run.default.dataset
	name "DSM--Old Setup--DSM"
	dataset RG5
	control:active 1
DEFAULTGROUP RG6
RECORD MOAHEADER5 MOAHEADER
	name "moa_header"
	header:runcode "UNIDIR Driveline Run"
	header:datacode "DSM--UNIDIR overlay--DSM"
RECORD ScopeDummy6 ScopeDummy
DEFAULTGROUP RG2
RECORD aspect7 aspect
	aspect:label "UNIDIR Driveline Test"
DEFAULTGROUP RG3
RECORD aspect8 aspect
	aspect:label "New Driveline Test"
DEFAULTGROUP RG5
RECORD aspect9 aspect
	aspect:label "Old Driveline Test"
DEFAULTGROUP RG7
RECORD aspect10 aspect
	aspect:label "Old Driveline Test"
DEFAULTGROUP RG8
RECORD aspect11 aspect
	aspect:label "New Driveline Test"
DEFAULTGROUP RG11
RECORD aspect12 aspect
	aspect:label "UNIDIR Driveline Test"
DEFAULTGROUP RG2
RECORD unidir_driveline13 unidir_driveline
	name "UNIDIR:driveline"
	unidir:gear 2
	unidir:drive_mode 3
	unidir:clutch_friction 0
	unidir:clutch_torque 1000
	unidir:auto_clutch 0
	unidir:manual_clutch 0
	unidir:max_engine_rps 1000000
	unidir:throttle 1
	unidir:ram_current_speed 0
	unidir:baulk_torque 1000
	unidir:gear_ratios 3,10,20,50
	unidir:diff_preload_torque 10
	unidir:diff_pump_torque 10
	unidir:diff_power_fract 0.1
	unidir:diff_coast_fract 0.1
	unidir:diff_4wd_rear_torque_split 0
	unidir:diff_4wd_pump_effect "0 0 0"
	unidir:diff_4wd_preload_effect "0 0 0"
	unidir:diff_4wd_power_effect "0 0 0"
	unidir:diff_4wd_coast_effect "0 0 0"
	unidir:handbrake 0
	unidir:handbrake_4wd_disco 0
	unidir:clutch_inertia 1
	unidir:engine_inertia 1
	unidir:recip_diff_ratio 1
	unidir:clutch_rps 7083.148
	unidir:engine_rps 7083.148
	unidir:transmission_torque 60.008
	unidir:diff_prev_baulk_wheel -1500.2
	unidir:wheel_rps "0 0 -176.1609 -107.165"
	unidir:wheel_pitch "0 0 -24841.22 -20930.04"
	unidir:brake_thickness "1 1 1 1"
	unidir:brake_inertia "1 1 1 1"
	unidir:wheel_inertia "1 1 1 1"
	unidir:wheel_input_torque "0 0 0 0"
	unidir:brake_torque "0 0 1000 2000"
DEFAULTGROUP RG11
RECORD unidir_driveline14 unidir_driveline
	name "UNIDIR:driveline"
	unidir:gear 1
	unidir:drive_mode 3
	unidir:clutch_friction 0
	unidir:clutch_torque 1000
	unidir:auto_clutch 0
	unidir:manual_clutch 0
	unidir:max_engine_rps 1000000
	unidir:throttle 1
	unidir:ram_current_speed 0
	unidir:baulk_torque 1000
	unidir:gear_ratios 3,10,20,50
	unidir:diff_preload_torque 10
	unidir:diff_pump_torque 10
	unidir:diff_power_fract 0.1
	unidir:diff_coast_fract 0.1
	unidir:diff_4wd_rear_torque_split 0
	unidir:diff_4wd_pump_effect "0 0 0"
	unidir:diff_4wd_preload_effect "0 0 0"
	unidir:diff_4wd_power_effect "0 0 0"
	unidir:diff_4wd_coast_effect "0 0 0"
	unidir:handbrake 0
	unidir:handbrake_4wd_disco 0
	unidir:clutch_inertia 1
	unidir:engine_inertia 1
	unidir:recip_diff_ratio 1
	unidir:clutch_rps 0
	unidir:engine_rps 0
	unidir:transmission_torque 0
	unidir:diff_prev_baulk_wheel 0
	unidir:wheel_rps "0 0 0 0"
	unidir:wheel_pitch "0 0 0 0"
	unidir:brake_thickness "0 0 0 0"
	unidir:brake_inertia "0 0 0 0"
	unidir:wheel_inertia "0 0 0 0"
	unidir:wheel_input_torque "0 0 0 0"
	unidir:brake_torque "0 0 0 0"
DEFAULTGROUP RG2
RECORD splat15 splat
	name "UNIDIR clutch"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:clutch_rps')	)"
	splat:x_range "0 200"
	splat:y_range "-20 10000"
	splat:color "1 1 0.6"
	splat:splat ""
RECORD splat16 splat
	name "UNIDIR RR Wheel"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:wheel_rps[3]')	)"
	splat:x_range "0 200"
	splat:y_range "-1000 1000"
	splat:color "1 1 0"
	splat:splat "UNIDIR clutch"
RECORD splat17 splat
	name "UNIDIR RL Wheel"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:wheel_rps[2]')	)"
	splat:x_range "0 200"
	splat:y_range "-1000 1000"
	splat:color "0 1 1"
	splat:splat "UNIDIR clutch"
DEFAULTGROUP RG3
RECORD splat18 splat
	name "shaft0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft0.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "1 1 1"
	splat:splat ""
RECORD splat19 splat
	name "shaft1"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "1 0.5 0.5"
	splat:splat "shaft0"
RECORD splat20 splat
	name "shaft2"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft2.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "0.5 1 0.5"
	splat:splat "shaft0"
RECORD splat21 splat
	name "shaft3"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft3.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "0.5 0.5 1"
	splat:splat "shaft0"
DEFAULTGROUP RG5
RECORD splat22 splat
	name "location"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:location1d')	)"
	splat:x_range "0 200"
	splat:y_range "-20 1000"
	splat:color "1 0.5 0.6"
	splat:splat ""
RECORD splat23 splat
	name "zoomed in"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:location1d')	)"
	splat:x_range "0 20"
	splat:y_range "-20 100"
	splat:color "1 0.5 0.6"
	splat:splat ""
RECORD splat24 splat
	name "velocity"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-20 300"
	splat:color "1 0 0.6"
	splat:splat ""
DEFAULTGROUP RG7
RECORD splat25 splat
	name "location"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:location1d')	)"
	splat:x_range "0 200"
	splat:y_range "-20 1000"
	splat:color "1 0.5 0.6"
	splat:splat ""
RECORD splat26 splat
	name "zoomed in"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:location1d')	)"
	splat:x_range "0 20"
	splat:y_range "-20 100"
	splat:color "1 0.5 0.6"
	splat:splat ""
RECORD splat27 splat
	name "velocity"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-20 300"
	splat:color "1 0 0.6"
	splat:splat ""
DEFAULTGROUP RG8
RECORD splat28 splat
	name "shaft0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft0.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "1 1 1"
	splat:splat ""
RECORD splat29 splat
	name "shaft1"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "1 0.5 0.5"
	splat:splat "shaft0"
RECORD splat30 splat
	name "shaft2"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft2.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "0.5 1 0.5"
	splat:splat "shaft0"
RECORD splat31 splat
	name "shaft3"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft3.spc:velocity1d')	)"
	splat:x_range "0 200"
	splat:y_range "-50 1000"
	splat:color "0.5 0.5 1"
	splat:splat "shaft0"
DEFAULTGROUP RG11
RECORD splat32 splat
	name "UNIDIR clutch"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:clutch_rps')	)"
	splat:x_range "0 200"
	splat:y_range "-20 10000"
	splat:color "1 1 0.6"
	splat:splat ""
RECORD splat33 splat
	name "UNIDIR RR Wheel"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:wheel_rps[3]')	)"
	splat:x_range "0 200"
	splat:y_range "-1000 1000"
	splat:color "1 1 0"
	splat:splat "UNIDIR clutch"
RECORD splat34 splat
	name "UNIDIR RL Wheel"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:wheel_rps[2]')	)"
	splat:x_range "0 200"
	splat:y_range "-1000 1000"
	splat:color "0 1 1"
	splat:splat "UNIDIR clutch"
DEFAULTGROUP RG2
RECORD event35 event
	time:time 80
	event:action "(set 'UNIDIR:driveline.unidir:gear' -1)"
RECORD event36 event
	time:time 82
	event:action "(set 'UNIDIR:driveline.unidir:gear' 2)"
RECORD event37 event
	time:time 100
	event:action "(set 'UNIDIR:driveline.unidir:brake_torque[2]' 2000.0)
					 (set 'UNIDIR:driveline.unidir:brake_torque[3]' 1000.0)"
RECORD event38 event
	time:time 120
	event:action "(set 'UNIDIR:driveline.unidir:brake_torque[2]' 1000.0)
					 (set 'UNIDIR:driveline.unidir:brake_torque[3]' 2000.0)"
DEFAULTGROUP RG3
RECORD event39 event
	time:time 0
	event:action "(set 'coupling12.driveline:right_gear' 11)"
RECORD event40 event
	time:time 0
	event:action "(set 'coupling12.driveline:left_gear' 11)"
RECORD event41 event
	time:time 50
	event:action "(set 'coupling12.driveline:right_gear' 37)"
RECORD event42 event
	time:time 2
	event:action "(set 'dummyshaft0.driveline:externalTorque' 10.0)"
RECORD event43 event
	time:time 49
	event:action "(set 'dummyshaft0.driveline:externalTorque' 0.0)"
RECORD event44 event
	time:time 51
	event:action "(set 'dummyshaft0.driveline:externalTorque' 10.0)"
DEFAULTGROUP RG5
RECORD event45 event
	time:time 0
	event:action "(set 'dummyshaft0.driveline:externalTorque' 1.0)"
RECORD event46 event
	time:time 0
	event:action "(set 'dummyshaft1.driveline:externalTorque' 1.0)"
RECORD event47 event
	time:time 0
	event:action "(set 'dummyshaft2.driveline:externalTorque' 1.0)"
RECORD event48 event
	time:time 0
	event:action "(set 'dummyshaft3.driveline:externalTorque' 1.0)"
RECORD event49 event
	time:time 100
	event:action "(set 'dummyshaft3.driveline:externalTorque' 100.0)"
RECORD event50 event
	time:time 100.1
	event:action "(set 'dummyshaft3.driveline:externalTorque' 100.0)"
RECORD event51 event
	time:time 10.5
	event:action ""
RECORD event52 event
	time:time 13.1
	event:action ""
RECORD event53 event
	time:time 10.8
	event:action ""
RECORD event54 event
	time:time 0.1
	event:action ""
RECORD event55 event
	time:time 0.01
	event:action ""
DEFAULTGROUP RG7
RECORD event56 event
	time:time 0
	event:action "(set 'dummyshaft0.driveline:externalTorque' 1.0)"
RECORD event57 event
	time:time 0
	event:action "(set 'dummyshaft1.driveline:externalTorque' 1.0)"
RECORD event58 event
	time:time 0
	event:action "(set 'dummyshaft2.driveline:externalTorque' 1.0)"
RECORD event59 event
	time:time 0
	event:action "(set 'dummyshaft3.driveline:externalTorque' 1.0)"
RECORD event60 event
	time:time 100
	event:action "(set 'dummyshaft3.driveline:externalTorque' 100.0)"
RECORD event61 event
	time:time 100.1
	event:action "(set 'dummyshaft3.driveline:externalTorque' 100.0)"
RECORD event62 event
	time:time 10.5
	event:action ""
RECORD event63 event
	time:time 13.1
	event:action ""
RECORD event64 event
	time:time 10.8
	event:action ""
RECORD event65 event
	time:time 0.1
	event:action ""
RECORD event66 event
	time:time 0.01
	event:action ""
DEFAULTGROUP RG8
RECORD event67 event
	time:time 0
	event:action "(set 'coupling12.driveline:right_gear' 11)"
RECORD event68 event
	time:time 0
	event:action "(set 'coupling12.driveline:left_gear' 11)"
RECORD event69 event
	time:time 50
	event:action "(set 'coupling12.driveline:right_gear' 37)"
RECORD event70 event
	time:time 2
	event:action "(set 'dummyshaft0.driveline:externalTorque' 10.0)"
RECORD event71 event
	time:time 49
	event:action "(set 'dummyshaft0.driveline:externalTorque' 0.0)"
RECORD event72 event
	time:time 51
	event:action "(set 'dummyshaft0.driveline:externalTorque' 10.0)"
DEFAULTGROUP RG11
RECORD event73 event
	time:time 80
	event:action "(set 'UNIDIR:driveline.unidir:gear' -1)"
RECORD event74 event
	time:time 82
	event:action "(set 'UNIDIR:driveline.unidir:gear' 2)"
RECORD event75 event
	time:time 100
	event:action "(set 'UNIDIR:driveline.unidir:brake_torque[2]' 2000.0)
					 (set 'UNIDIR:driveline.unidir:brake_torque[3]' 1000.0)"
RECORD event76 event
	time:time 120
	event:action "(set 'UNIDIR:driveline.unidir:brake_torque[2]' 1000.0)
					 (set 'UNIDIR:driveline.unidir:brake_torque[3]' 2000.0)"
DEFAULTGROUP RG2
RECORD multisweep77 multisweep
	name "UNIDIR Driveline Run"
	composer:step ""
	composer:outer "0 0 0"
	composer:inner "1 200 1"
	composer:linger 1
	composer:frequency 160
	composer:runup 1
DEFAULTGROUP RG3
RECORD multisweep78 multisweep
	name "Clean Sweep"
	composer:step ""
	composer:outer "0 0 0"
	composer:inner "1 200 1"
	composer:linger 1
	composer:frequency 160
	composer:runup 1
DEFAULTGROUP RG5
RECORD multisweep79 multisweep
	name "Driveline Run"
	composer:step "	(set	'coupling12.driveline:right_gear' 51)
						(set	'coupling12.driveline:left_gear' (* INNER 0.1))  "
	composer:outer "0 0 0"
	composer:inner "1 200 1"
	composer:linger 1
	composer:frequency 160
	composer:runup 1
DEFAULTGROUP RG7
RECORD multisweep80 multisweep
	name "Driveline Run"
	composer:step "	(set	'coupling12.driveline:right_gear' 51)
						(set	'coupling12.driveline:left_gear' (* INNER 0.1))  "
	composer:outer "0 0 0"
	composer:inner "1 200 1"
	composer:linger 1
	composer:frequency 160
	composer:runup 1
DEFAULTGROUP RG8
RECORD multisweep81 multisweep
	name "Clean Sweep"
	composer:step ""
	composer:outer "0 0 0"
	composer:inner "1 200 1"
	composer:linger 1
	composer:frequency 160
	composer:runup 1
DEFAULTGROUP RG11
RECORD multisweep82 multisweep
	name "UNIDIR Driveline Run"
	composer:step ""
	composer:outer "0 0 0"
	composer:inner "1 200 1"
	composer:linger 1
	composer:frequency 160
	composer:runup 1
DEFAULTGROUP RG3
RECORD driveline83 driveline
	name "test driveline"
	driveline:model "test driveline model"
	driveline:contents RG4
	driveline:spDrivelineI "invalid interface"
DEFAULTGROUP RG5
RECORD driveline84 driveline
	name "test driveline"
	driveline:model "test driveline model"
	driveline:contents RG6
	driveline:spDrivelineI "invalid interface"
DEFAULTGROUP RG9
RECORD driveline85 driveline
	name "test driveline"
	driveline:model "test driveline model"
	driveline:contents RG10
	driveline:spDrivelineI "invalid interface"
DEFAULTGROUP RG4
RECORD driveline_model86 driveline_model
	name "test driveline model"
	driveline:component "DrivelineI.SemiImplicit.fe"
DEFAULTGROUP RG6
RECORD driveline_model87 driveline_model
	name "test driveline model"
	driveline:component "DrivelineI.SemiImplicit.fe"
DEFAULTGROUP RG10
RECORD driveline_model88 driveline_model
	name "test driveline model"
	driveline:component "DrivelineI.SemiImplicit.fe"
DEFAULTGROUP RG4
RECORD driveline_shaft_forced89 driveline_shaft_forced
	name "dummyshaft0"
	driveline:momentOfInertia 1
	driveline:externalTorque 10
	spc:location1d 208.1785
	spc:velocity1d 799.9531
	sim:force1d 4.692087
	sim:mass 1
	sim:index 0
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced90 driveline_shaft_forced
	name "dummyshaft1"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 154.6759
	spc:velocity1d 800.8102
	sim:force1d 4.599134
	sim:mass 1
	sim:index 1
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced91 driveline_shaft_forced
	name "dummyshaft2"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 147.1285
	spc:velocity1d 801.7405
	sim:force1d 0.3981039
	sim:mass 1
	sim:index 2
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced92 driveline_shaft_forced
	name "dummyshaft3"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 107.165
	spc:velocity1d 811.4835
	sim:force1d 0.3106747
	sim:mass 1
	sim:index 3
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
DEFAULTGROUP RG6
RECORD driveline_shaft_forced93 driveline_shaft_forced
	name "dummyshaft0"
	driveline:momentOfInertia 1
	driveline:externalTorque 1
	spc:location1d 37.9963
	spc:velocity1d 1453.284
	sim:force1d 19.08788
	sim:mass 1
	sim:index 0
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced94 driveline_shaft_forced
	name "dummyshaft1"
	driveline:momentOfInertia 1
	driveline:externalTorque 1
	spc:location1d 223.9173
	spc:velocity1d 1443.06
	sim:force1d 17.207
	sim:mass 1
	sim:index 1
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced95 driveline_shaft_forced
	name "dummyshaft2"
	driveline:momentOfInertia 1
	driveline:externalTorque 1
	spc:location1d 567.5346
	spc:velocity1d 1369.59
	sim:force1d 1.690693
	sim:mass 1
	sim:index 2
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced96 driveline_shaft_forced
	name "dummyshaft3"
	driveline:momentOfInertia 1
	driveline:externalTorque 100
	spc:location1d 3157.762
	spc:velocity1d 1322.805
	sim:force1d 1.641885
	sim:mass 1
	sim:index 3
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
DEFAULTGROUP RG10
RECORD driveline_shaft_forced97 driveline_shaft_forced
	name "dummyshaft0"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 0
	spc:velocity1d 0
	sim:force1d 0
	sim:mass 1
	sim:index 0
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced98 driveline_shaft_forced
	name "dummyshaft1"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 0
	spc:velocity1d 0
	sim:force1d 0
	sim:mass 1
	sim:index 0
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced99 driveline_shaft_forced
	name "dummyshaft2"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 0
	spc:velocity1d 0
	sim:force1d 0
	sim:mass 1
	sim:index 0
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
RECORD driveline_shaft_forced100 driveline_shaft_forced
	name "dummyshaft3"
	driveline:momentOfInertia 1
	driveline:externalTorque 0
	spc:location1d 0
	spc:velocity1d 0
	sim:force1d 0
	sim:mass 1
	sim:index 0
	sim:prevVelocity1d 0
	sim:prevLocation1d 0
	sim:externalForce1d 0
DEFAULTGROUP RG4
RECORD torsion_spring101 torsion_spring
	name "coupling 01"
	pair:left driveline_shaft_forced89
	pair:right driveline_shaft_forced90
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 1
	driveline:right_gear 1
RECORD torsion_spring102 torsion_spring
	name "coupling 23"
	pair:left driveline_shaft_forced91
	pair:right driveline_shaft_forced92
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 1
	driveline:right_gear 1
RECORD torsion_spring103 torsion_spring
	name "coupling12"
	pair:left driveline_shaft_forced90
	pair:right driveline_shaft_forced91
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 11
	driveline:right_gear 37
DEFAULTGROUP RG6
RECORD torsion_spring104 torsion_spring
	name "coupling 01"
	pair:left driveline_shaft_forced93
	pair:right driveline_shaft_forced94
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 1
	driveline:right_gear 1
RECORD torsion_spring105 torsion_spring
	name "coupling 23"
	pair:left driveline_shaft_forced95
	pair:right driveline_shaft_forced96
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 1
	driveline:right_gear 1
RECORD torsion_spring106 torsion_spring
	name "coupling12"
	pair:left driveline_shaft_forced94
	pair:right driveline_shaft_forced95
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 20
	driveline:right_gear 51
DEFAULTGROUP RG10
RECORD torsion_spring107 torsion_spring
	name "coupling 01"
	pair:left driveline_shaft_forced97
	pair:right driveline_shaft_forced98
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 1
	driveline:right_gear 1
RECORD torsion_spring108 torsion_spring
	name "coupling 23"
	pair:left driveline_shaft_forced99
	pair:right driveline_shaft_forced100
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 1
	driveline:right_gear 1
RECORD torsion_spring109 torsion_spring
	name "coupling12"
	pair:left driveline_shaft_forced98
	pair:right driveline_shaft_forced99
	mat:stiffness 0.1
	mat:damping 0.05
	mat:flags 0
	bdy:length 0
	lsf:spring 
	driveline:left_gear 3
	driveline:right_gear 3
DEFAULTGROUP RG6
RECORD dataset_mixer110 dataset_mixer
	name "Old Setup"
	mixer:inputs "old_setup fe_test_driveline"
	control:active 1
RECORD dataset_mixer111 dataset_mixer
	name "New Setup"
	mixer:inputs "fe_dataset fe_test_driveline"
	control:active 1
RECORD dataset_mixer112 dataset_mixer
	name "UNIDIR overlay"
	mixer:inputs "UNIDIR_dataset"
	control:active 1
RECORD dataset113 dataset
	name "old_setup"
	dataset RG7
DEFAULTGROUP RG7
RECORD dataset114 dataset
	name "fe_dataset"
	dataset RG8
DEFAULTGROUP RG8
RECORD dataset115 dataset
	name "fe_test_driveline"
	dataset RG9
DEFAULTGROUP RG10
RECORD dataset116 dataset
	name "UNIDIR_dataset"
	dataset RG11
DEFAULTGROUP RG11
RECORD layout_time117 layout_time
	name "sim_clock"
	time:time 199.7917
	time:dt 0.00625
RECORD l_eval_real118 l_eval_real
	eval:value 1000
RECORD l_eval_real119 l_eval_real
	eval:value 2000
RECORD l_eval_real120 l_eval_real
	eval:value 2000
RECORD l_eval_real121 l_eval_real
	eval:value 1000
RECORD l_eval_real122 l_eval_real
	eval:value 2
RECORD l_eval_real123 l_eval_real
	eval:value -1
RECORD l_eval_string124 l_eval_string
	eval:string "UNIDIR:driveline.unidir:brake_torque[2]"
RECORD l_eval_string125 l_eval_string
	eval:string "UNIDIR:driveline.unidir:brake_torque[3]"
RECORD l_eval_string126 l_eval_string
	eval:string "UNIDIR:driveline.unidir:brake_torque[2]"
RECORD l_eval_string127 l_eval_string
	eval:string "UNIDIR:driveline.unidir:brake_torque[3]"
RECORD l_eval_string128 l_eval_string
	eval:string "UNIDIR:driveline.unidir:gear"
RECORD l_eval_string129 l_eval_string
	eval:string "UNIDIR:driveline.unidir:gear"
RECORD l_eval_string130 l_eval_string
	eval:string "sim_clock.time:time"
RECORD l_eval_string131 l_eval_string
	eval:string "UNIDIR:driveline.unidir:wheel_rps[2]"
RECORD l_eval_string132 l_eval_string
	eval:string "sim_clock.time:time"
RECORD l_eval_string133 l_eval_string
	eval:string "UNIDIR:driveline.unidir:wheel_rps[3]"
RECORD l_eval_string134 l_eval_string
	eval:string "sim_clock.time:time"
RECORD l_eval_string135 l_eval_string
	eval:string "UNIDIR:driveline.unidir:clutch_rps"
RECORD l_eval_string136 l_eval_string
	eval:string "sim_clock.time:time"
RECORD l_eval_string137 l_eval_string
	eval:string "UNIDIR:driveline.unidir:wheel_rps[2]"
RECORD l_eval_string138 l_eval_string
	eval:string "sim_clock.time:time"
RECORD l_eval_string139 l_eval_string
	eval:string "UNIDIR:driveline.unidir:wheel_rps[3]"
RECORD l_eval_string140 l_eval_string
	eval:string "sim_clock.time:time"
RECORD l_eval_string141 l_eval_string
	eval:string "UNIDIR:driveline.unidir:clutch_rps"
RECORD X142 X
	eval:value 0
RECORD X143 X
	eval:value 0
RECORD X144 X
	eval:value 0
RECORD X145 X
	eval:value 1
RECORD X146 X
	eval:value 1
RECORD X147 X
	eval:value 0
RECORD X148 X
	eval:value 0
RECORD X149 X
	eval:value 1
RECORD X150 X
	eval:value 1
RECORD X151 X
	eval:value 0
RECORD X152 X
	eval:value 0
RECORD X153 X
	eval:value 0
RECORD X154 X
	eval:value 1
RECORD X155 X
	eval:value 1
RECORD X156 X
	eval:value 0.600000023841858
RECORD X157 X
	eval:value 199.791732788086
RECORD X158 X
	eval:value -176.160949707031
RECORD X159 X
	eval:value 0
RECORD X160 X
	eval:value 1
RECORD X161 X
	eval:value 1
RECORD X162 X
	eval:value 199.791732788086
RECORD X163 X
	eval:value -107.164962768555
RECORD X164 X
	eval:value 1
RECORD X165 X
	eval:value 1
RECORD X166 X
	eval:value 0
RECORD X167 X
	eval:value 199.791732788086
RECORD X168 X
	eval:value 7083.1484375
RECORD X169 X
	eval:value 1
RECORD X170 X
	eval:value 1
RECORD X171 X
	eval:value 0.600000023841858
RECORD OUTER172 OUTER
	eval:value 0
RECORD OUTER173 OUTER
	eval:value 200
RECORDGROUP RG2
RECORDGROUP RG5
	Producer.run.default.dataset4
	driveline_model87
	driveline_shaft_forced93
	driveline_shaft_forced94
	driveline_shaft_forced95
	driveline_shaft_forced96
	torsion_spring104
	torsion_spring105
	torsion_spring106
RECORDGROUP 1
	Producer.run.default.dataset3
	Producer.run.default.dataset4
	MOAHEADER5
	ScopeDummy6
	aspect7
	unidir_driveline13
	splat15
	splat16
	splat17
	event35
	event36
	event37
	event38
	multisweep77
	dataset_mixer110
	dataset_mixer111
	dataset_mixer112
	dataset113
	dataset114
	dataset115
	dataset116
	layout_time117
	l_eval_real118
	l_eval_real119
	l_eval_real120
	l_eval_real121
	l_eval_real122
	l_eval_real123
	l_eval_string124
	l_eval_string125
	l_eval_string126
	l_eval_string127
	l_eval_string128
	l_eval_string129
	l_eval_string130
	l_eval_string131
	l_eval_string132
	l_eval_string133
	l_eval_string134
	l_eval_string135
	l_eval_string136
	l_eval_string137
	l_eval_string138
	l_eval_string139
	l_eval_string140
	l_eval_string141
	X142
	X143
	X144
	X145
	X146
	X147
	X148
	X149
	X150
	X151
	X152
	X153
	X154
	X155
	X156
	X157
	X158
	X159
	X160
	X161
	X162
	X163
	X164
	X165
	X166
	X167
	X168
	X169
	X170
	X171
	OUTER172
	OUTER173
RECORDGROUP RG7
RECORDGROUP RG8
RECORDGROUP RG9
	driveline_model88
	driveline_shaft_forced97
	driveline_shaft_forced98
	driveline_shaft_forced99
	driveline_shaft_forced100
	torsion_spring107
	torsion_spring108
	torsion_spring109
RECORDGROUP RG11
RECORDGROUP RG3
	Producer.run.default.dataset3
	driveline_model86
	driveline_shaft_forced89
	driveline_shaft_forced90
	driveline_shaft_forced91
	driveline_shaft_forced92
	torsion_spring101
	torsion_spring102
	torsion_spring103
RECORDGROUP RG10
RECORDGROUP RG4
RECORDGROUP RG6
END
