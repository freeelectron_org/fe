#!/usr/bin/python3

# arg examples
# FE_MOD_KILL="ext.datatool:ext.signal"
# FE_COPY_BINARIES_TO_PATH="{'my/path':'(.*\\.(dll|exe|lib|so|a)|fe)$'}"

import os
import sys

# NOTE: The utility module enforces a python version check at import time. So
# we import it as soon as possible, which causes an immediate abort if the
# check fails. This is more implicit than is desired, but we can't directly
# import the version check method, since doing so just causes the module to
# experience import processing in full, so there's no way to isolate the check
# ourselves -- we just defer to the module doing it for us. This could/would
# change if forge becomes a proper package in the future.
import utility

import subprocess
import multiprocessing
import re
import ast
import pathlib

import build_vcpkg
import doc.build_docs

SUCCESS = True
FAIL = False
SYS_SUCCESS = 0
SYS_FAIL = 1

def printHelp(directive):
    print("Usage: build.py [PRODUCT] [DIRECTIVE] [OPTION ...]")
    print("Build Free Electron or perform related tasks.")
    print("")
    print("PRODUCT selects a named subset of modules to build.")
    print("  The default product is 'default'.")
    print("")
    print("DIRECTIVE is an alternative action.")
    print("  clean     remove temporary files")
    print("  docs      generate doxygen")
    print("  vcpkg     install/export/download vcpkg packages")
    print("  list      only list available products")
    print("  test      run unit tests for specified (or default) product")
    print("")
    print("OPTION adjusts the build process.")
    print("  help      print this help")
    print("  info      print a few extra messages during the build")
    print("  ugly      print every build command called, with the results")
    print("  env       print all environment variables before starting build")
    print("  parallel  force a parallel build (default for Linux)")
    print("  serial    force a serial build (default for Windows)")
    print("  config    only generate fe/config.h for the given settings")
    print("  tests     also build unit tests (opt-in)")
    print("  tolerant  build whatever modules are buildable")

    if directive == "vcpkg":
        print("")
        print("The vcpkg directive calls a wrapper that uses the vcpkg tool.")
        print("")
        print("  vcpkg install     build prescribed set of packages locally")
        print("  vcpkg export      bundle set of locally built packages")
        print("  vcpkg upload      generate zip of local build and post")
        print("  vcpkg download    get current approved build")

def build():

    build_path = pathlib.Path(__file__).resolve().parent

    jobs = multiprocessing.cpu_count()

    directive = "here"
    product = "default"
    ugly = False
    info = False
    parallel = True
    build_tests = False
    config_only = False
    dump_env = False
    show_help = False
    result = SUCCESS

    if sys.platform == "win32":
        parallel = False

    env_pattern = re.compile(r"[A-Z_]*=[^=]*")

    env = os.environ

    for arg in sys.argv[1:]:
        if arg == "clean":
            directive = "clean here"
        elif arg == "config":
            config_only = True
        elif arg == "unit" or arg == "test":
            directive = "unit"
            build_tests = True
            env["FE_BLIND_TEST"] = "1"
        elif arg in [
            "root",
            "here",
            "docs",
            "vcpkg",
            "list",
        ]:
            directive = arg
        elif arg == "tolerant":
            env["FE_REQUIRE_ALL_MODULES"] = "0"
        elif arg == "tests":
            build_tests = True
        elif arg == "ugly":
            ugly = True
        elif arg == "info":
            info = True
        elif arg == "parallel":
            parallel = True
        elif arg == "serial":
            parallel = False
        elif arg == "env":
            dump_env = True
        elif arg == "help" or arg == "-h" or arg == "--help":
            show_help = True
        elif env_pattern.match(arg):
            tokens = arg.split('=')
            variable = tokens[0]
            env[variable] = tokens[1]
            if "FE_FORCE_ENV" in env.keys():
                env["FE_FORCE_ENV"] += ":" + variable
            else:
                env["FE_FORCE_ENV"] = variable
        else:
            product = arg

    env["FE_PRODUCT"] = product

    pretty = None
    if ugly:
        pretty = 0
    elif info:
        pretty = 1

    if config_only:
        env["FE_CONFIG_ONLY"] = "1"

    if not build_tests:
        mod_kill = []
        if "FE_MOD_KILL" in os.environ:
            mod_kill = os.environ["FE_MOD_KILL"].split(':')
        mod_kill += [ "*.test" ]
        env["FE_MOD_KILL"] = ':'.join(mod_kill)

    if dump_env:
        env_keys = sorted(env.keys())
        for key in env_keys:
            print("%s=%s" % (key, env[key]))
        print

    if show_help:
        printHelp(directive)
        return SUCCESS

    if directive == "vcpkg":
        try:
            build_vcpkg.Build(product)
        except Exception as e:
            print("build_vcpkg failed")
            result = FAIL
        return result

    if directive == "docs":
        p = multiprocessing.Process(
            target=doc.build_docs.build,
            kwargs={'directory': 'doc'},
        )
        p.start()
        p.join()
        rc = p.exitcode
        if rc != SYS_SUCCESS:
            print(f"docs process failed ({rc})")
            result = FAIL
        return result

    if directive == "list":
        env["FE_LIST_ONLY"] = "1"

    if directive == "unit":
        test_log = "test/log.txt"
        if os.path.exists(test_log):
            os.remove(test_log);

    command = [ sys.executable ]
    command += [ str(build_path / "bin" / "forge.py") ]

    if parallel:
        command += [f"-j {jobs}"]

    if pretty is not None:
        command += [f'--pretty={pretty}']

    command += [f"{directive}"]
    print(" ".join(command))

    try:
        popen = subprocess.Popen(command,env=env,bufsize=-1)
        (p_out, p_err)=popen.communicate()
        rc = popen.returncode
        if rc != SYS_SUCCESS:
            print(f"process failed ({rc})")
            result = FAIL
        return result
    except KeyboardInterrupt:
        print("\nkeyboard interrupt")
        popen.kill()
        return FAIL

    # If we get here, we did nothing useful or the args were weird.
    # This probably should get handled better.
    return FAIL

# -----------------------------------------------------------------------------
if __name__ == "__main__" :

    # 'spawn' is cross-platform compatible (Windows), but also an appropriate
    # method for how we're using child procs.
    multiprocessing.set_start_method('spawn')

    rc = SYS_SUCCESS

    try:
        result = build()
        if not result:
            rc = SYS_FAIL
            print("build failed")
        else:
            print("build completed")
    except Exception as e:
        print("build exception")
        print(e)
        rc = SYS_FAIL

    sys.exit(rc)
