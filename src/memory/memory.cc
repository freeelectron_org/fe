/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

namespace fe
{

#if FE_LOCK_MEMORY
#if FE_SAFE_COUNTED_MUTEX
//* NOTE leak
I32 Memory::ms_mutexCount=0;
RecursiveMutex* Memory::ms_pMutex=NULL;
#else
RecursiveMutex Memory::ms_mutex;
#endif
#endif

void fe_memory_lock(void)
{
#if FE_LOCK_MEMORY
#if FE_SAFE_COUNTED_MUTEX
	if(Memory::ms_pMutex)
	{
		Memory::ms_pMutex->lock();
	}
	else if(!Memory::ms_mutexCount)
	{
		Memory::ms_mutexCount++;
		Memory::ms_pMutex=new RecursiveMutex;
		Memory::ms_pMutex->lock();
	}
#else
	Memory::ms_mutex.lock();
#endif
#endif
}

void fe_memory_unlock(void)
{
#if FE_LOCK_MEMORY
#if FE_SAFE_COUNTED_MUTEX
	if(Memory::ms_pMutex)
	{
		Memory::ms_pMutex->unlock();
	}
#else
	Memory::ms_mutex.unlock();
#endif
#endif
}

#if FE_SAFE_COUNTED_MUTEX
std::vector<Memory::CountedMutex> Memory::ms_deadPool;

void Memory::markForDeath(Mutex*& a_rpMutex,I32* a_pCount)
{
	fe_memory_lock();
//	fe_fprintf(stderr,"Memory::markForDeath %p %p\n",&a_rpMutex,a_rpMutex);
	ms_deadPool.push_back(CountedMutex(a_rpMutex,a_pCount));
	fe_memory_unlock();
}

void Memory::markForDeath(RecursiveMutex*& a_rpMutex,I32* a_pCount)
{
	fe_memory_lock();
//	fe_fprintf(stderr,"Memory::markForDeath %p %p\n",&a_rpMutex,a_rpMutex);
	ms_deadPool.push_back(CountedMutex(a_rpMutex,a_pCount));
	fe_memory_unlock();
}

void Memory::clearDeadPool(void)
{
	fe_memory_lock();
	for(std::vector<CountedMutex>::iterator it=ms_deadPool.begin();
			it!=ms_deadPool.end();)
	{
		CountedMutex& rCountedMutex= *it;
		Mutex*& rpMutex= *rCountedMutex.m_ppMutex;
//		fe_fprintf(stderr,"Memory::clearDeadPool check %p %d\n",
//				rpMutex,(*rCountedMutex.m_pCount));
		if(!rpMutex)
		{
			it=ms_deadPool.erase(it);
			continue;
		}
		else if(rpMutex && !(*rCountedMutex.m_pCount))
		{
//			fe_fprintf(stderr,"Memory::clearDeadPool delete %p\n",rpMutex);
			delete rpMutex;
			rpMutex=NULL;
			it=ms_deadPool.erase(it);
			continue;
		}
		it++;
	}
	fe_memory_unlock();
}
#endif

#ifdef FE_EXPORT_MEM_VIA_FUNC
FE_DL_EXPORT void* FE_CDECL ex_allocate(FE_UWORD byteCount)
{
	fe_memory_lock();

// NOTE OSX malloc is always 16-byte aligned
#if FE_MEM_ALIGNMENT && FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64 && FE_OS!=FE_OSX
	void* pBuffer=NULL;
	int fail=posix_memalign(&pBuffer,FE_MEM_ALIGNMENT,byteCount);
//	fe_printf("ex_allocate(%d) %p %d\n",int(byteCount),pBuffer,fail);
//	FEASSERT(pBuffer<(void*)0x200000000000);

#if FE_CHECK_HEAP
	if(!fail && (!gs_pHeapBase || gs_pHeapBase>pBuffer))
	{
		gs_pHeapBase=pBuffer;
	}
#elif FE_CHECK_NEW
	gs_pHeapBase=(void*)0x1;
#endif

	if(fail)
	{
		pBuffer=NULL;
	}

#elif FE_CODEGEN<=FE_DEBUG && (FE_OS==FE_WIN32 || FE_OS==FE_WIN64)
	void* pBuffer=malloc(byteCount);

#if FE_CHECK_HEAP
	if(!gs_pHeapBase || gs_pHeapBase>pBuffer)
	{
		gs_pHeapBase=pBuffer;
	}
#elif FE_CHECK_NEW
	gs_pHeapBase=(void*)0x1;
#endif

#else
	void* pBuffer=malloc(byteCount);

#if FE_CHECK_NEW
	gs_pHeapBase=(void*)0x1;
#endif

#endif

	fe_memory_unlock();

	return pBuffer;
}

FE_DL_EXPORT void * FE_CDECL ex_reallocate(void *pMemory, FE_UWORD byteCount)
{
	fe_memory_lock();

#if FE_MEM_ALIGNMENT && FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64 && FE_OS!=FE_OSX
	void *pNew=realloc(pMemory, byteCount);
	if(pNew==pMemory)
	{
		fe_memory_unlock();

		return pNew;
	}

	void* pBuffer;
	int fail=posix_memalign(&pBuffer,FE_MEM_ALIGNMENT,byteCount);
//	fe_printf("ex_reallocate(%d) %p %d\n",int(byteCount),pBuffer,fail);
	if(fail)
	{
		free(pNew);

		fe_memory_unlock();

		return NULL;
	}
	memcpy(pBuffer,pNew,byteCount);
	free(pNew);
#else
	void* pBuffer=realloc(pMemory, byteCount);
#endif

	fe_memory_unlock();

	return pBuffer;
}

FE_DL_EXPORT void  FE_CDECL ex_deallocate(void *pMemory)
{
	fe_memory_lock();

	free(pMemory);

	fe_memory_unlock();
}

#endif

#ifdef FE_EXPORT_MEMORY
#ifdef FE_EXPORT_MEM_VIA_FUNC
allocateFunction	*gs_pAllocateFunction	= ex_allocate;
deallocateFunction	*gs_pDeallocateFunction	= ex_deallocate;
reallocateFunction	*gs_pReallocateFunction	= ex_reallocate;
void*				gs_pHeapBase			= NULL;
I32					gs_newCheck				= 0;
#else
#if FE_COMPILER==FE_DMC
allocateFunction	*gs_pAllocateFunction	= malloc;
deallocateFunction	*gs_pDeallocateFunction	= free;
reallocateFunction	*gs_pReallocateFunction	= realloc;
allocateFunction	*gs_pPrintFunction		= NULL;
void*				gs_pHeapBase			= NULL;
I32					gs_newCheck				= 0;
#else
FE_DL_EXPORT allocateFunction	*gs_pAllocateFunction	= malloc;
FE_DL_EXPORT deallocateFunction	*gs_pDeallocateFunction	= free;
FE_DL_EXPORT reallocateFunction	*gs_pReallocateFunction	= realloc;
FE_DL_EXPORT void*				gs_pHeapBase			= NULL;
FE_DL_EXPORT I32				gs_newCheck				= 0;

FE_DL_EXPORT void FE_CDECL setAllocateFunction(allocateFunction *p_fn)
//FE_DL_EXPORT void setAllocateFunction(allocateFunction *p_fn)
{
	gs_pAllocateFunction = p_fn;
}

FE_DL_EXPORT void FE_CDECL setDeallocateFunction(deallocateFunction *p_fn)
//FE_DL_EXPORT void setDeallocateFunction(deallocateFunction *p_fn)
{
	gs_pDeallocateFunction = p_fn;
}
#endif
#endif
#endif

void FE_CDECL feStdioPrintFunction(const char* pAscii)
{
	std::cout << pAscii;
	std::cout.flush();
}

FE_DL_PUBLIC FE_DL_EXPORT printFunction* gs_pPrintFunction=feStdioPrintFunction;

FE_DL_EXPORT void FE_CDECL setPrintFunction(printFunction *p_fn)
//FE_DL_EXPORT void setPrintFunction(printFunction *p_fn)
{
	gs_pPrintFunction = p_fn;
}

FE_DL_PUBLIC FE_DL_EXPORT RegexInitFunction* gs_fnRegexInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT RegexMatchFunction* gs_fnRegexMatch=NULL;
FE_DL_PUBLIC FE_DL_EXPORT RegexMatchFunction* gs_fnRegexSearch=NULL;
FE_DL_PUBLIC FE_DL_EXPORT RegexResultFunction* gs_fnRegexResult=NULL;
FE_DL_PUBLIC FE_DL_EXPORT RegexReplaceFunction* gs_fnRegexReplace=NULL;
FE_DL_PUBLIC FE_DL_EXPORT RegexFinishFunction* gs_fnRegexFinish=NULL;
FE_DL_PUBLIC FE_DL_EXPORT RegexReleaseFunction* gs_fnRegexRelease=NULL;
FE_DL_PUBLIC FE_DL_EXPORT BWORD gs_regexChecked=FALSE;

FE_DL_PUBLIC FE_DL_EXPORT MutexInitFunction* gs_fnMutexInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexLockFunction* gs_fnMutexLock=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexFinishFunction* gs_fnMutexFinish=NULL;

FE_DL_PUBLIC FE_DL_EXPORT MutexGuardInitFunction* gs_fnMutexGuardInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexGuardLockFunction* gs_fnMutexGuardLock=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexGuardFinishFunction* gs_fnMutexGuardFinish=NULL;

FE_DL_PUBLIC FE_DL_EXPORT MutexConditionInitFunction* gs_fnMutexConditionInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexConditionWaitFunction* gs_fnMutexConditionWait=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexConditionNotifyFunction* gs_fnMutexConditionNotify=NULL;
FE_DL_PUBLIC FE_DL_EXPORT MutexConditionFinishFunction* gs_fnMutexConditionFinish=NULL;

FE_DL_PUBLIC FE_DL_EXPORT String gs_mutexSupport;
FE_DL_PUBLIC FE_DL_EXPORT BWORD gs_mutexChecked=FALSE;
FE_DL_PUBLIC FE_DL_EXPORT DL_Loader* gs_pMutexLoader=NULL;

FE_DL_PUBLIC FE_DL_EXPORT ThreadDefaultInitFunction* gs_fnThreadDefaultInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadInitFunction* gs_fnThreadInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadConfigFunction* gs_fnThreadConfig=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadInterruptFunction* gs_fnThreadInterrupt=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadJoinFunction* gs_fnThreadJoin=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadJoinableFunction* gs_fnThreadJoinable=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadFinishFunction* gs_fnThreadFinish=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadInterruptionFunction* gs_fnThreadInterruption=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadConcurrencyFunction* gs_fnThreadConcurrency=NULL;

FE_DL_PUBLIC FE_DL_EXPORT ThreadGroupInitFunction* gs_fnThreadGroupInit=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadGroupCreateFunction* gs_fnThreadGroupCreate=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadGroupJoinAllFunction* gs_fnThreadGroupJoinAll=NULL;
FE_DL_PUBLIC FE_DL_EXPORT ThreadGroupFinishFunction* gs_fnThreadGroupFinish=NULL;

FE_DL_PUBLIC FE_DL_EXPORT String gs_threadSupport;
FE_DL_PUBLIC FE_DL_EXPORT BWORD gs_threadChecked=FALSE;
FE_DL_PUBLIC FE_DL_EXPORT DL_Loader* gs_pThreadLoader=NULL;

FE_DL_PUBLIC FE_DL_EXPORT String gs_logPrefix;
FE_DL_PUBLIC FE_DL_EXPORT BWORD gs_bannerPrinted=FALSE;

} /* namespace */

#if FALSE
#include <execinfo.h>
void logBacktrace(int size)
{
	const U32 backtrace_size = 16;
	void* stack[backtrace_size];
	I32 count=backtrace(stack,backtrace_size);
//	char** symbols=backtrace_symbols(stack,count);

	Dl_info info;

	for(I32 level=2;level<count;level++)
	{
		if(dladdr(stack[level],&info) && info.dli_sname)
		{
			//* fe::Mutex
			if(!strncmp(info.dli_sname,"_ZN2fe5Mutex",12))
			{
				return;
			}
			//* fe::data::AsciiReader
			if(!strncmp(info.dli_sname,"_ZN2fe4data11AsciiReader",24))
			{
				return;
			}
			//* fe::String::parse
			if(!strcmp(info.dli_sname,"_ZN2fe6String5parseES0_S0_b"))
			{
				return;
			}
			//* fe::String::vsPrintf
			if(!strcmp(info.dli_sname,
					"_ZN2fe6String8vsPrintfEPKcP13__va_list_tagRi"))
			{
				return;
			}
			//* create std::basic_string
			if(!strcmp(info.dli_sname,"_ZNSs4_Rep9_S_createEmmRKSaIcE"))
			{
				return;
			}
		}
	}

	fe_fprintf(stderr,"*new %d*\n",size);

	for(I32 level=2;level<count;level++)
	{
		if(dladdr(stack[level],&info) && info.dli_sname)
		{
			fe_fprintf(stderr,"  %s\n",info.dli_sname);

			if(!strcmp(info.dli_sname,"PyObject_Call") ||
					!strcmp(info.dli_sname,"fe_terminal_execute"))
			{
				return;
			}
		}
		else
		{
			fe_fprintf(stderr,"  ???\n");
		}
	}
}
#endif

#if FALSE
#include <malloc.h>
static void my_init_hook(void);
static void *my_malloc_hook(size_t, const void *);
static void *(*old_malloc_hook)(size_t, const void *);
void (*__malloc_initialize_hook) (void) = my_init_hook;

static void my_init_hook(void)
{
	old_malloc_hook = __malloc_hook;
	__malloc_hook = my_malloc_hook;
}

static void* my_malloc_hook(size_t size, const void *caller)
{
	__malloc_hook = old_malloc_hook;

	logBacktrace(size);
	void* result = malloc(size);

	old_malloc_hook = __malloc_hook;

	__malloc_hook = my_malloc_hook;
	return result;
}
#endif

//* NOTE ISO C++1z does not allow dynamic exception specifications
#if FE_NO_DYNAMIC_EXCEPTIONS
void* FE_CDECL operator new( size_t byteCount )
#else
void* FE_CDECL operator new( size_t byteCount ) throw (std::bad_alloc)
#endif
{
//	feLogBacktrace(byteCount);
	return fe::allocate( byteCount );
}
#if FE_NO_DYNAMIC_EXCEPTIONS
void* FE_CDECL operator new[]( size_t byteCount )
#else
void* FE_CDECL operator new[]( size_t byteCount ) throw (std::bad_alloc)
#endif
{
	return operator new( byteCount );
}

#if FE_NO_DYNAMIC_EXCEPTIONS
void FE_CDECL operator delete( void* pMemory )
#else
void FE_CDECL operator delete( void* pMemory ) throw ()
#endif
{
	fe::deallocate( pMemory );
}
#if FE_NO_DYNAMIC_EXCEPTIONS
void FE_CDECL operator delete[]( void* pMemory )
#else
void FE_CDECL operator delete[]( void* pMemory ) throw ()
#endif
{
	operator delete( pMemory );
}

#if FE_COMPILER==FE_GNU && FE_CODEGEN<=FE_DEBUG
void fe_memory_init(void) __attribute__((constructor));
void fe_memory_init(void)
{
	fe_fprintf(stderr,"[90mFE memory init[0m\n");
}

void fe_memory_finish(void) __attribute__((destructor));
void fe_memory_finish(void)
{
	fe_fprintf(stderr,"[90mFE memory finish[0m\n");
}
#endif
