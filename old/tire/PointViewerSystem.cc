/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

#define RECORD Record

PointViewerSystem::PointViewerSystem(void)
{
}

PointViewerSystem::~PointViewerSystem(void)
{
}

void PointViewerSystem::initialize(void)
{
}

void PointViewerSystem::updatePoints(const t_note_id &note_id)
{
	sp<RecordGroup> rg_dataset = m_dataset;
	m_asPoint.bind(rg_dataset->scope());
	m_asPoint.filter(m_points, rg_dataset);
};


void PointViewerSystem::connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator)
{
	ViewerSystem::connectOrchestrator(a_spOrchestrator);
	a_spOrchestrator->connect(this,&PointViewerSystem::updatePoints,FE_NOTE_COMPILE);
}

void PointViewerSystem::draw(fe::Record a_viewportRecord)
{
	const fe::Color green(0.0,1.0,0.0,1.0);
	const fe::Color yellow(1.0,1.0,0.0,1.0);
	const fe::Color red(0.8,0.0,0.0,0.4);

	fe::sp<fe::ext::DrawI> draw = m_viewportAccessorSet.draw(a_viewportRecord);
	SpatialVector scale(0.01,0.01,0.01);

	sp<DrawMode> spDrawMode(new DrawMode());
	spDrawMode->setDrawStyle(DrawMode::e_solid);
	spDrawMode->setTwoSidedLighting(true);
	spDrawMode->setBackfaceCulling(false);
	spDrawMode->setLineWidth(2.0);

	draw->setDrawMode(spDrawMode);

	AsNamed asNamed;
	asNamed.bind(m_dataset->scope());

	for(fe::RECORD r_point : m_points)
	{
		fe::SpatialTransform center;
		setIdentity(center);
		translate(center, m_asPoint.location(r_point));


		draw->drawSphere(center, &scale, yellow);

		if(asNamed.check(r_point))
		{
			draw->drawAlignedText(center.translation(), asNamed.name(r_point).c_str(), yellow);
		}
	}

	sp<RecordGroup> rg_dataset = m_dataset;


	AsContact asContact;
	asContact.bind(rg_dataset->scope());
	std::vector<Record> contacts;
	asContact.filter(contacts, rg_dataset);

	for(fe::RECORD r_contact : contacts)
	{
		fe::SpatialTransform center;
		setIdentity(center);
		translate(center, asContact.contact(r_contact));

		draw->drawSphere(center, &scale, green);
	}

	AsRigLive asRig;
	asRig.bind(rg_dataset->scope());
	std::vector<Record> rigs;
	asRig.filter(rigs, rg_dataset);

	for(fe::RECORD r_rig : rigs)
	{
		fe::SpatialTransform center;
		setIdentity(center);
		translate(center, asRig.location(r_rig));

		draw->drawSphere(center, &scale, green);
	}


	AsLinearSpring asLinearSpring;
	asLinearSpring.bind(rg_dataset->scope());
	std::vector<RECORD> springs;
	asLinearSpring.filter(springs, rg_dataset);

	for(fe::RECORD r_spring : springs)
	{
		RECORD r_left = asLinearSpring.left(r_spring);
		RECORD r_right = asLinearSpring.right(r_spring);

		draw->drawCylinder(
			m_asPoint.location(r_left),
			m_asPoint.location(r_right),
			0.01, 0.01, green, 2);
	}


	AsTireLive asTireLive;
	asTireLive.bind(rg_dataset->scope());
	std::vector<RECORD> tires;
	asTireLive.filter(tires, rg_dataset);
	fe::SpatialTransform I;
	setIdentity(I);
	for(fe::RECORD r_tire : tires)
	{
		SpatialVector tirescale(
			asTireLive.radius(r_tire),
			asTireLive.radius(r_tire),
			asTireLive.width(r_tire));
		SpatialTransform xform(asTireLive.transform(r_tire));
		SpatialVector offset(0.0,0.0,
			-0.5*asTireLive.width(r_tire));
		rotate(xform,0.5*fe::pi,e_xAxis);
		translate(xform,offset);
		draw->drawCylinder(xform, &tirescale, 1.0, red, 256);

	}
	draw->setTransform(I);

	AsBend asBend;
	asBend.bind(rg_dataset->scope());
	std::vector<RECORD> bends;
	asBend.filter(bends, rg_dataset);

	for(fe::RECORD r_bend : bends)
	{
		RECORD r_a = asBend.a(r_bend);
		RECORD r_b = asBend.b(r_bend);
		RECORD r_c = asBend.c(r_bend);
		RECORD r_d = asBend.d(r_bend);

		SpatialVector V[4];

		V[0] = m_asPoint.location(r_a);
		V[1] = m_asPoint.location(r_c);
		V[2] = m_asPoint.location(r_d);
		V[3] = m_asPoint.location(r_b);

		SpatialVector N[4];

		cross3(N[0], (V[1]-V[0]), (V[2]-V[0]));
		cross3(N[3], (V[1]-V[3]), (V[2]-V[3]));
		N[3] = - N[3];

		N[1] = (N[0]+N[3])*0.5;
		N[2] = N[1];

		for(unsigned int i = 0; i < 4; i++) { normalize(N[i]); }


		const fe::Color sheet(0.4,0.6,0.4,1.0);
		draw->drawTriangles(V, N, NULL, 4, DrawI::e_strip, false, &sheet);
	}

	AsStrand asStrand;
	asStrand.bind(rg_dataset->scope());
	std::vector<RECORD> strands;
	asStrand.filter(strands, rg_dataset);

	const fe::Color cyan(0.0,1.0,1.0,0.4);
	for(fe::RECORD r_strand : strands)
	{
		RECORD r_a = asStrand.a(r_strand);
		RECORD r_b = asStrand.b(r_strand);
		RECORD r_c = asStrand.c(r_strand);

		draw->drawCylinder(
			m_asPoint.location(r_a),
			m_asPoint.location(r_b),
			0.015, 0.015, cyan, 11);
		draw->drawCylinder(
			m_asPoint.location(r_b),
			m_asPoint.location(r_c),
			0.015, 0.015, cyan, 11);
	}

	AsLocator asLocator;
	asLocator.bind(rg_dataset->scope());
	std::vector<RECORD> locators;
	asLocator.filter(locators, rg_dataset);

	const fe::Color blue(0.0,0.0,1.0,1.0);
	const fe::Color white(1.0,1.0,1.0,1.0);
	SpatialVector line[3];
	t_moa_real L = 0.2;
	for(fe::RECORD r_locator : locators)
	{
		SpatialVector scale(0.02,0.02,0.02);
		draw->drawSphere(asLocator.transform(r_locator), &scale, white);

		line[0] = asLocator.transform(r_locator).translation();
		line[1] = transformVector<3,t_moa_real>(asLocator.transform(r_locator), SpatialVector(L,0,0));
		draw->drawLines(line,NULL,2,DrawI::e_discrete,false,&red);
		line[1] = transformVector<3,t_moa_real>(asLocator.transform(r_locator), SpatialVector(0,L,0));
		draw->drawLines(line,NULL,2,DrawI::e_discrete,false,&green);
		line[1] = transformVector<3,t_moa_real>(asLocator.transform(r_locator), SpatialVector(0,0,L));
		draw->drawLines(line,NULL,2,DrawI::e_discrete,false,&blue);
	}
}

} /* namespace ext */
} /* namespace fe */


