#! /usr/bin/python

import sys

if len(sys.argv) < 2:
    print "Usage: " + sys.argv[0] + " NAME"
    exit()

name = ""
options = ""
argCount = len(sys.argv)

argIndex = 1
while True:
    arg = sys.argv[argIndex]
    argIndex += 1

    if arg == "-options":
        options = sys.argv[argIndex]
        argIndex += 1
    else:
        name = arg

    if argIndex >= argCount:
        break;

if name == "":
    print sys.argv[0] + ": missing name"
    exit

import pyfe.context

fe = pyfe.context.Context()

operator = fe.create(name)

print
print "NAME %s" % operator.name()
print

form = "%-19s %-7s %-20s %s"

print form % ("KEY", "TYPE", "VALUE", "LABEL")

keys = operator.keys()
for key in keys:
    if key == "":
        continue

    label = operator.get(key,"label")
    value = operator[key]
    print form % (key, keys[key], str(value), label)
