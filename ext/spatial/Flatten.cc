/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Flatten.h"

namespace fe
{
namespace ext
{

Flatten::Flatten(void)
{
}

Flatten::~Flatten(void)
{
}

void Flatten::initialize(void)
{
}


void Flatten::addLimit(int a_dimension, Real a_value, Flatten::t_mode a_mode)
{
	int i = m_limits.size();
	m_limits.resize(i+1);
	m_limits[i].m_dimension = a_dimension;
	m_limits[i].m_value = a_value;
	m_limits[i].m_mode = a_mode;
}

void Flatten::handle(Record &r_sig)
{
	m_asParticle.bind(r_sig.layout()->scope());
	m_asSignal.bind(r_sig.layout()->scope());
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		if(m_asParticle.location.check(spRA))
		{
			bool have_vel = false;
			if(m_asParticle.velocity.check(spRA))
			{
				have_vel = true;
			}
			for(int i = 0; i < spRA->length(); i++)
			{
				for(unsigned int j = 0 ; j < m_limits.size(); j++)
				{
					t_limit &l = m_limits[j];
					switch(l.m_mode)
					{
						case e_eq:
							m_asParticle.location(spRA, i)[l.m_dimension]
								= l.m_value;
							if(have_vel)
							{
								m_asParticle.velocity(spRA, i)[l.m_dimension]
									= 0.0;
							}
							break;
						case e_lt:
							if(m_asParticle.location(spRA, i)[l.m_dimension]
								> l.m_value)
							{
								m_asParticle.location(spRA, i)[l.m_dimension]
									= l.m_value;
								if(have_vel && m_asParticle.velocity(spRA, i)[l.m_dimension]
									> 0.0)
								{
									m_asParticle.velocity(spRA, i)
										[l.m_dimension] = 0.0;
								}
							}
							break;
						case e_gt:
							if(m_asParticle.location(spRA, i)[l.m_dimension]
								< l.m_value)
							{
								m_asParticle.location(spRA, i)[l.m_dimension]
									= l.m_value;
								if(have_vel && m_asParticle.velocity(spRA, i)[l.m_dimension]
									< 0.0)
								{
									m_asParticle.velocity(spRA, i)
										[l.m_dimension] = 0.0;
								}
							}
							break;
						default:
							break;
					};
				}
			}
		}
	}
}


} /* namespace ext */
} /* namespace fe */


