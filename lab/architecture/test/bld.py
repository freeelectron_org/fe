import sys
import os
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
        'fexSignalLib',
        'fexDataToolLib' ]

    tests = [   'xArchitecture',
                'xExecute' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    plugin_path = os.path.join(module.modPath, 'plugin')
    arch_rg = os.path.join(module.modPath, 'architecture.rg')

    forge.tests += [
            ("xExecute.exe",        "plugin_test.rg",       None,   None) ]

    #   ("xArchitecture.exe",
    #       "count=100 app=%s architecture=%s" % (plugin_path, arch_rg),
    #       None,   None),
