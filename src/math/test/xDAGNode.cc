/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		sp<DAGNode> spNode1(new DAGNode);
		sp<DAGNode> spNode2(new DAGNode);
		sp<DAGNode> spNode3(new DAGNode);
		sp<DAGNode> spNode4(new DAGNode);

		feLog("attach 3 to 4\n");
		spNode3->attachTo(spNode1,"3->4","3<-4");
		feLog("attach 3 to 2\n");
		spNode3->attachTo(spNode2,"3->2","3<-2");
		feLog("attach 4 to 2\n");
		spNode4->attachTo(spNode2,"4->2","4<-2");

		feLog("node 1 parent count %d (vs 0)\n",
				spNode1->parentConnectionCount());
		UNIT_TEST(spNode1->parentConnectionCount()==0);
		feLog("node 1 child count %d (vs 1)\n",
				spNode1->childConnectionCount());
		UNIT_TEST(spNode1->childConnectionCount()==1);
		feLog("node 2 parent count %d (vs 0)\n",
				spNode2->parentConnectionCount());
		UNIT_TEST(spNode2->parentConnectionCount()==0);
		feLog("node 2 child count %d (vs 2)\n",
				spNode2->childConnectionCount());
		UNIT_TEST(spNode2->childConnectionCount()==2);
		feLog("node 3 parent count %d (vs 2)\n",
				spNode3->parentConnectionCount());
		UNIT_TEST(spNode3->parentConnectionCount()==2);
		feLog("node 3 child count %d (vs 0)\n",
				spNode3->childConnectionCount());
		UNIT_TEST(spNode3->childConnectionCount()==0);
		feLog("node 4 parent count %d (vs 1)\n",
				spNode4->parentConnectionCount());
		UNIT_TEST(spNode4->parentConnectionCount()==1);
		feLog("node 4 child count %d (vs 0)\n",
				spNode4->childConnectionCount());
		UNIT_TEST(spNode4->childConnectionCount()==0);

		feLog("detach 3 (two connections)\n");
		spNode3->detach();
		feLog("detach 4 from 1 (never connected)\n");
		spNode4->detachFrom(spNode1);
		feLog("detach 4 from 2\n");
		spNode4->detachFrom(spNode2);

		feLog("\nspNode1 parent count %d (vs 0)\n",
				spNode1->parentConnectionCount());
		UNIT_TEST(!spNode1->parentConnectionCount());
		feLog("node 1 child count %d (vs 0)\n",
				spNode1->childConnectionCount());
		UNIT_TEST(!spNode1->childConnectionCount());
		feLog("node 2 parent count %d (vs 0)\n",
				spNode2->parentConnectionCount());
		UNIT_TEST(!spNode2->parentConnectionCount());
		feLog("node 2 child count %d (vs 0)\n",
				spNode2->childConnectionCount());
		UNIT_TEST(!spNode2->childConnectionCount());
		feLog("node 3 parent count %d (vs 0)\n",
				spNode3->parentConnectionCount());
		UNIT_TEST(!spNode3->parentConnectionCount());
		feLog("node 3 child count %d (vs 0)\n",
				spNode3->childConnectionCount());
		UNIT_TEST(!spNode3->childConnectionCount());
		feLog("node 4 parent count %d (vs 0)\n",
				spNode4->parentConnectionCount());
		UNIT_TEST(!spNode4->parentConnectionCount());
		feLog("node 4 child count %d (vs 0)\n",
				spNode4->childConnectionCount());
		UNIT_TEST(!spNode4->childConnectionCount());

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(17);
	UNIT_RETURN();
}
