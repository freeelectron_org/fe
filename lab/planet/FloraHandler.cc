/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "planet/planet.pmh"

namespace fe
{
namespace ext
{

FloraHandler::FloraHandler(void)
{
}

FloraHandler::~FloraHandler(void)
{
}

void FloraHandler::initialize(void)
{
	m_populationDensity = create("ScalarFieldI.Density");
	if(m_populationDensity.isValid())
	{
		sp<GridScalarField> spGridScalarField = m_populationDensity;
		if(spGridScalarField.isValid())
		{
			sp<AffineSpace> spSpace = create("SpaceI.AffineSpace");
			assert(spSpace.isValid());
			spSpace->setAxisAligned(SpatialVector(0.0,0.0,0.0), SpatialVector(10.0,10.0,1.0));
			Vector3i count(10,10,1);
			spGridScalarField->create(spSpace, count);
		}
	}
}

void FloraHandler::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_asTemporal.bind(l_sig->scope());
}

void FloraHandler::handle(Record &r_sig)
{
	if(!m_asTemporal.check(r_sig))
	{
		return;
	}

	m_populationDensity->set(0.0);

//	Real dt = m_asTemporal.timestep(r_sig);
//	Real add = 0.0;
//	Real sub = 0.0;

	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");
	sp<RecordGroup> rg_new(new RecordGroup());
	sp<RecordGroup> rg_kill(new RecordGroup());

	std::map<FE_UWORD, Record> killmap;

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();


		m_asFlora.bind(spScope);
		m_asPlanet.bind(spScope);
		m_asScalarField.bind(spScope);

		if(m_asFlora.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector &location = m_asFlora.location(spRA, i);
				Real &radius = m_asFlora.radius(spRA, i);
				m_populationDensity->add(location, radius);
			}
		}
	}

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();


		m_asFlora.bind(spScope);
		m_asPlanet.bind(spScope);
		m_asScalarField.bind(spScope);

		if(m_asFlora.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector &location = m_asFlora.location(spRA, i);
				Real &radius = m_asFlora.radius(spRA, i);
				Real &maxradius = m_asFlora.maxRadius(spRA, i);
				Real &age = m_asFlora.age(spRA, i);
				Real &lifespan = m_asFlora.lifespan(spRA, i);
				Real &reprospan = m_asFlora.reprospan(spRA, i);
				Real &repronext = m_asFlora.repronext(spRA, i);
				Record r_planet = m_asFlora.home(spRA, i);
				Record r_field = m_asPlanet.organics(r_planet);
				sp<ScalarFieldI> spScalarField =
					m_asScalarField.field(r_field);

				//spScalarField->add(location,radius*0.005);
				//radius *= 0.999;
				age += 0.005;
				repronext -= 0.005;


				if(age > lifespan)
				{
					//spScalarField->add(location, radius/**0.999*/);
					Record r_kill = spRA->getRecord(i);
					killmap[r_kill.idr()] = r_kill;
				}
				else if(age > 0.1 && radius < 0.01)
				{
					Record r_kill = spRA->getRecord(i);
					killmap[r_kill.idr()] = r_kill;
				}
				else if(radius < maxradius)
				{
					Real density = spScalarField->sample(location) /
						spScalarField->volume();
					Real pop_density = m_populationDensity->sample(location) /
						m_populationDensity->volume();
					if(isZero(density) && radius > 0.01)
					{
						density = 0.00;
						if(radius > 0.02)
						{
							//spScalarField->add(location, radius - 0.01);
							radius = 0.02;
						}
						else
						{
							Record r_kill = spRA->getRecord(i);
							killmap[r_kill.idr()] = r_kill;
						}
						continue;
					}
					Real growth = 1.0 * density * radius * ((float)rand()/(float)RAND_MAX);
					if(growth > 0.2) { growth = 0.2; }
					if(pop_density > 0.01) { growth *= 0.01/(0.01+pop_density); }
					radius += growth;
					spScalarField->add(location, -growth);
				}
				else if (repronext < 0.0)
				{
					repronext = reprospan;
					Record r_flora = spScope->createRecord(spRA->layout());

					SpatialVector dx;
					dx[0] = -1.0f+2.0f*((float)rand()/(float)RAND_MAX);
					dx[1] = -1.0f+2.0f*((float)rand()/(float)RAND_MAX);
					dx[2] = -1.0f+2.0f*((float)rand()/(float)RAND_MAX);
					normalize(dx);
					dx *= ((float)rand()/(float)RAND_MAX)*10.0*radius;
					//dx *= 2.5;
					m_asFlora.location(r_flora) = location + dx;
					m_asFlora.radius(r_flora) = radius * 0.01;
					m_asFlora.age(r_flora) = 0.0;
					m_asFlora.reprospan(r_flora) = reprospan;
					m_asFlora.reprospan(r_flora) *= 1.0 + -0.1f+0.2f*((float)rand()/(float)RAND_MAX);
					m_asFlora.repronext(r_flora) = reprospan;
					m_asFlora.lifespan(r_flora) = lifespan;
					m_asFlora.lifespan(r_flora) *= 1.0 + -0.1f+0.2f*((float)rand()/(float)RAND_MAX);
					m_asFlora.maxRadius(r_flora) = maxradius * ( 1.0 +
						-0.1f+0.2f*((float)rand()/(float)RAND_MAX));
					if(m_asFlora.maxRadius(r_flora) < 0.01)
					{
						m_asFlora.maxRadius(r_flora) = 0.01;
					}
					radius *= 0.99;
					m_asFlora.home(r_flora) = m_asFlora.home(spRA, i);
					rg_new->add(r_flora);
				}

			}
		}
	}
	rg_input->add(rg_new);


	sp<RecordGroup> rg_collisions = cfg< sp<RecordGroup> >("collisions");
	for(RecordGroup::iterator i_rg = rg_collisions->begin();
		i_rg != rg_collisions->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();

		m_asProximity.bind(spScope);
		m_asFlora.bind(spScope);
		m_asPlanet.bind(spScope);
		m_asScalarField.bind(spScope);

		if(m_asProximity.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				WeakRecord &r_left = m_asProximity.left(spRA, i);
				WeakRecord &r_right = m_asProximity.right(spRA, i);

				if(!r_left.isValid()) { continue; }
				if(!r_right.isValid()) { continue; }
				if(!m_asFlora.check(r_left)) { continue; }
				if(!m_asFlora.check(r_right)) { continue; }


				if(m_asFlora.radius(r_right) > m_asFlora.radius(r_left))
				{
					killmap[r_left.idr()] = r_left;
				}
				else
				{
					killmap[r_right.idr()] = r_right;
				}
			}
		}
	}

	for(std::map<FE_UWORD, Record>::iterator i_kill = killmap.begin();
		i_kill != killmap.end(); i_kill++)
	{
		Record r_kill = i_kill->second;
		rg_kill->add(r_kill);
		SpatialVector &loc = m_asFlora.location(r_kill);
		Record r_planet = m_asFlora.home(r_kill);
		Record r_field = m_asPlanet.organics(r_planet);
		sp<ScalarFieldI> spScalarField =
			m_asScalarField.field(r_field);
		spScalarField->add(loc, m_asFlora.radius(r_kill)*0.99);
	}

	rg_input->remove(rg_kill);
}

} /* namespace ext */
} /* namespace fe */

