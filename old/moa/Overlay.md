# MOA Overlay Guide {#OverlayGuide}

The as simple as possible, but no simpler, solution to (much of) the
protoconfig requirement is simply the single function overlay().

## Datasets

A dataset is effectively a named RecordGroup, specified as a fe::ext::AsDataset.

## The overlay function

See fe::ext::overlay().

Most of overlaying is based on named Records, meaning Records with the
attribute "name".  See fe::ext::AsNamed

Roughly, the concept of an overlaying MOA datasets is simply to add one
RecordGroup atop another RecordGroup, replacing state that matches my name, and
adding state that has no match.

So one use case would be to have a simulation dataset all set up for test runs,
and then run many tests (changing settings, items in the test, etc).  By having
each of these changes as its own small RecordGroup, one would "overlay" these
RecordGroups atop the base dataset RecordGroup, and re-run the simulation for
each overlay.

xTireRig.exe does this to simulate various tires through various tests.
TireRigProducer through Producer, is used in this case.


## Producer

See fe::ext::Producer.

The gist of the Producer is to put together a configuration for an Orchestrator
to then do Orchestrator::perform().

The Producer::run() function does the following:

```
---------------------------------------------------------------------
| Prepare a baseline dataset -- derived Producer::prepare()
| Create case datasets based on DatasetManyToManys
| Create case datasets based on DatasetMixers
| for each active case dataset do:
|     Clone baseline dataset
|     Overlay case dataset atop clone dataset
|     Create Orchestrator -- derived Producer::createOrchestrator()
|     Perform a run for the case -- Orchestrator::perform()
---------------------------------------------------------------------
```

### Dataset Mixer

  * Record matching fe::ext::AsDatasetMixer
  * Induces Producer to overlay a given list of datasets (mixer:inputs)

### Dataset ManyToMany

  * Record matching fe::ext::AsDatasetManyToMany
  * Producer creates dataset for every combination datasets in "mixer:left" and "mixer:right"

### Dataset Control

  * Record matching fe::ext::AsDatasetControl
  * Used to turn cases on or off (a convienient control in input files)
