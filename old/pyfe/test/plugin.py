#!/usr/bin/python

import fe

# signal system serves as more thorough testbed for plugins
# this just creates a component

fe.manage("xPluginLibrary")
myComponent = fe.create("my.component")
myComponent = 0
fe.prune()


