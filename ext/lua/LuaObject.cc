/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

namespace fe
{
namespace ext
{

LuaRecordType::LuaRecordType(LuaContext *pContext,const String &mtn)
	: t_lua_record_t(pContext, mtn)
{
	setMethod("isValid", LuaRecordObject::isValid);
	setMethod("check", LuaRecordObject::check);
	setMethod("layout", LuaRecordObject::layout);
	setMethod("clone", LuaRecordObject::clone);
	//setMethod("deepclone", LuaRecordObject::deepclone);
}

void LuaRecordType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaRecordObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaRecordObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__newindex");
	lua_pushcfunction(pLuaState, LuaRecordObject::newindex);
	lua_settable(pLuaState, -3);
}

int LuaRecordObject::index(lua_State *pLuaState)
{
	String attributeName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);
	LuaRecordObject *pObj = (LuaRecordObject *)lua_touserdata(pLuaState, -1);

	attributeName=pObj->aliasedName(attributeName);

	Instance instance;
	if(pObj->data().isValid() &&
			pObj->data().extractInstance(instance, attributeName))
	{
		pObj->context()->pushInstance(instance);
	}
	else
	{
		if(!pObj->type()->pushMethod(attributeName))
		{
			String s;
			s.sPrintf("unknown record attribute \'%s\'", attributeName.c_str());
			LuaContext::error(pLuaState, s.c_str());
			return 0;
		}
	}
	return 1;
}

int LuaRecordObject::newindex(lua_State *pLuaState)
{
	LuaRecordObject *pObj = (LuaRecordObject *)lua_touserdata(pLuaState, 1);

	String attributeName = lua_tostring(pLuaState, -2);
	attributeName=pObj->aliasedName(attributeName);

	Instance instance;
	if(pObj->data().extractInstance(instance, attributeName))
	{
// what in the world is this
#if 0
		LuaVector4Object* pOther;
		if((pOther=reinterpret_cast<LuaVector4Object*>
				(luaL_checkudata(pLuaState,2,"mt_vector4"))))
		{
		}
#endif

		// value is still at top of stack
		pObj->context()->popInstance(instance);

		// pop other tw
		lua_pop(pLuaState, 2);

		// push the return value
		pObj->context()->pushInstance(instance);
	}
	else
	{
		lua_pop(pLuaState, 3);
		String s;
		s.sPrintf("unknown record attribute \'%s\'", attributeName.c_str());
		LuaContext::error(pLuaState, s.c_str());
		return 0;
	}
	return 1;
}

int LuaRecordObject::isValid(lua_State *pLuaState)
{
	LuaRecordObject *pObj = (LuaRecordObject *)selfIsFirst(pLuaState);

	lua_pushboolean(pLuaState, pObj && pObj->data().isValid());
	return 1;
}

int LuaRecordObject::check(lua_State *pLuaState)
{
	LuaRecordObject *pObj = (LuaRecordObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	String attrName = lua_tostring(pLuaState, -1);

	attrName=pObj->aliasedName(attrName);

	lua_pushboolean(pLuaState,
			(int)pObj->data().layout()->checkAttributeStr(attrName));

	return 1;
}

int LuaRecordObject::layout(lua_State *pLuaState)
{
	LuaRecordObject *pObj = (LuaRecordObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	t_lua_layout::push(pObj->context(), pObj->data().layout());

	return 1;
}

int LuaRecordObject::clone(lua_State *pLuaState)
{
	LuaRecordObject *pObj = (LuaRecordObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	t_lua_record::push(pObj->context(), pObj->data().clone());

	return 1;
}

#if 0
int LuaRecordObject::deepclone(lua_State *pLuaState)
{
	LuaRecordObject *pObj = (LuaRecordObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	t_lua_record::push(pObj->context(), pObj->data().deepclone());

	return 1;
}
#endif

LuaLayoutType::LuaLayoutType(LuaContext *pContext,const String &mtn)
	: t_lua_layout_t(pContext, mtn)
{
	setMethod("scope", LuaLayoutObject::scope);
	setMethod("populate", LuaLayoutObject::populate);
	setMethod("createRecord", LuaLayoutObject::createRecord);
	setMethod("name", LuaLayoutObject::name);
}

int LuaLayoutObject::index(lua_State *pLuaState)
{
	String attributeName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);
	LuaLayoutObject *pObj = (LuaLayoutObject *)selfIsFirst(pLuaState);

	attributeName=pObj->aliasedName(attributeName);

	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(!pObj->type()->pushMethod(attributeName))
	{
		lua_pushnil(pLuaState);
	}
	return 1;
}

int LuaLayoutObject::scope(lua_State *pLuaState)
{
	LuaLayoutObject *pObj = (LuaLayoutObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	t_lua_scope::push(pObj->context(), pObj->data()->scope());

	return 1;
}

int LuaLayoutObject::populate(lua_State *pLuaState)
{
	LuaLayoutObject *pObj = (LuaLayoutObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(lua_gettop(pLuaState) == 3)
	{
		String attrName = lua_tostring(pLuaState, -2);
		String typeName = lua_tostring(pLuaState, -1);

		attrName=pObj->aliasedName(attrName);

		pObj->data()->populate(attrName,typeName);
	}
	else if(lua_gettop(pLuaState) == 2)
	{
		LuaBaseObject *pB = (LuaBaseObject *)lua_touserdata(pLuaState, -1);
		if(pB && (pB->type() == pObj->type()->context()->lookup(LuaContext::e_layout)))
		{
			LuaLayoutObject *pL = (LuaLayoutObject *)pB;
			pObj->data()->populate(pL->data());
		}
		else
		{
			sp<PopulateI> spPopulateI = component<PopulateI>(pLuaState, -1);
			spPopulateI->populate(pObj->data());
		}
	}

	return 0;
}

int LuaLayoutObject::createRecord(lua_State *pLuaState)
{
	LuaLayoutObject *pObj = (LuaLayoutObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	t_lua_record::push(pObj->context(),
			pObj->data()->scope()->createRecord(pObj->data()));

	return 1;
}

int LuaLayoutObject::name(lua_State *pLuaState)
{
	LuaLayoutObject *pObj = (LuaLayoutObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	lua_pushstring(pLuaState, pObj->data()->name().c_str());

	return 1;
}

LuaScopeType::LuaScopeType(LuaContext *pContext,const String &mtn)
	: t_lua_scope_t(pContext, mtn)
{
	setMethod("declare", LuaScopeObject::declare);
	setMethod("createRecord", LuaScopeObject::createRecord);
}

int LuaScopeObject::index(lua_State *pLuaState)
{
	String attributeName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);
	LuaScopeObject *pObj = (LuaScopeObject *)selfIsFirst(pLuaState);

	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	attributeName=pObj->aliasedName(attributeName);

	if(!pObj->type()->pushMethod(attributeName))
	{
		lua_pushnil(pLuaState);
	}
	return 1;
}

int LuaScopeObject::declare(lua_State *pLuaState)
{
	LuaScopeObject *pObj = (LuaScopeObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	String layoutName = lua_tostring(pLuaState, -1);

	t_lua_layout::push(pObj->context(), pObj->data()->declare(layoutName));

	return 1;
}

int LuaScopeObject::createRecord(lua_State *pLuaState)
{
	LuaScopeObject *pObj = (LuaScopeObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	LuaBaseObject *pB = (LuaBaseObject *)lua_touserdata(pLuaState, -1);
	if(!pB)
	{
		LuaContext::error(pLuaState, "invalid layout");
		return 0;
	}
	if(pB->type() != pObj->type()->context()->lookup(LuaContext::e_layout))
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaLayoutObject *pL = (LuaLayoutObject *)pB;

	t_lua_record::push(pObj->context(),pObj->data()->createRecord(pL->data()));

	return 1;
}

LuaGroupType::LuaGroupType(LuaContext *pContext,const String &mtn)
	: t_lua_group_t(pContext, mtn)
{
	setMethod("length", LuaGroupObject::length);
	setMethod("add", LuaGroupObject::add);
	setMethod("remove", LuaGroupObject::remove);
	setMethod("lookup", LuaGroupObject::lookup);
	setMethod("setWeak", LuaGroupObject::setWeak);
	setMethod("clear", LuaGroupObject::clear);
	setMethod("array", LuaGroupObject::array);
}

int LuaGroupObject::length(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	lua_pushnumber(pLuaState, pObj->data()->count());

	return 1;
}

int LuaGroupObject::add(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	LuaBaseObject *pB = (LuaBaseObject *)lua_touserdata(pLuaState, -1);
	if(!pB)
	{
		LuaContext::error(pLuaState, "invalid record");
		return 0;
	}
	if(pB->type() != pObj->type()->context()->lookup(LuaContext::e_record))
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaRecordObject *pR = (LuaRecordObject *)pB;

	pObj->data()->add(pR->data());

	lua_pushnil(pLuaState);
	return 1;
}

int LuaGroupObject::remove(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	LuaBaseObject *pB = (LuaBaseObject *)lua_touserdata(pLuaState, -1);
	if(!pB)
	{
		LuaContext::error(pLuaState, "invalid record");
		return 0;
	}
	if(pB->type() != pObj->type()->context()->lookup(LuaContext::e_record))
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaRecordObject *pR = (LuaRecordObject *)pB;

	pObj->data()->remove(pR->data());

	lua_pushnil(pLuaState);
	return 1;
}

int LuaGroupObject::lookup(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	LuaScopeObject *pS = to<LuaScopeObject>(pLuaState, 2, LuaContext::e_scope);
	sp<Scope> spScope = pS->data();

	String attr_name = lua_tostring(pLuaState, 3);
	String attr_value = lua_tostring(pLuaState, 4);

	Accessor<String> aName(spScope, attr_name);

	Record r_found = pObj->data()->lookup<String>(aName, attr_value);

	t_lua_record::push(pObj->context(), r_found);
	return 1;
}

int LuaGroupObject::setWeak(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	int weak = (int)lua_tonumber(pLuaState, 2);

	pObj->data()->setWeak(weak!=0);

	lua_pushnil(pLuaState);
	return 1;
}

int LuaGroupObject::clear(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	pObj->data()->clear();

	lua_pushnil(pLuaState);
	return 1;
}

int LuaGroupObject::array(lua_State *pLuaState)
{
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	const String layoutName = lua_tostring(pLuaState, 2);

	sp<RecordArray> spRecordArray;
	if(layoutName.empty())
	{
		//* NOTE for empty string, return first RecordArray
		RecordGroup::iterator it=pObj->data()->begin();
		if(it!=pObj->data()->end())
		{
			spRecordArray= *it;
		}
	}
	else
	{
		spRecordArray=pObj->data()->getArray(layoutName);
	}

	if(spRecordArray.isNull())
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	t_lua_array::push(pObj->context(), spRecordArray);
	return 1;
}

int LuaGroupObject::index(lua_State *pLuaState)
{
	String attributeName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);
	LuaGroupObject *pObj = (LuaGroupObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	attributeName=pObj->aliasedName(attributeName);

	if(!pObj->type()->pushMethod(attributeName))
	{
		lua_pushnil(pLuaState);
	}
	return 1;
}

int LuaGroupObject::newiterator(lua_State *pLuaState)
{
	t_lua_group *pObj = (t_lua_group *)lua_touserdata(pLuaState, -1);
	if(!pObj)
	{
		LuaContext::error(pLuaState,
				"Did you forget to call a record group as a function"
				" to create an iterator?");
		return 0;
	}

	lua_pushcfunction(pLuaState, iterator);
	RecordGroup::iterator i_rg;

	if(!(pObj->data().isValid()) ||
			pObj->data()->begin() == pObj->data()->end())
	{
		return 1;
	}

	lua_pushnil(pLuaState);
	t_lua_groupit *pIt = t_lua_groupit::push(pObj->context(), i_rg);
	pIt->data() = pObj->data()->begin();

	return 3;
}

int LuaGroupObject::iterator(lua_State *pLuaState)
{
	t_lua_groupit *pIt = (t_lua_groupit *)lua_touserdata(pLuaState, -1);

	if(!pIt || !(pIt->data().group().isValid()) ||
			pIt->data() == pIt->data().group()->end())
	{
//		lua_pop(pLuaState, 1);
		return 0;
	}

	t_lua_array::push(pIt->context(), *(pIt->data()));
	pIt->data()++;

	return 2;
}

void LuaGroupType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaGroupObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaGroupObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__call");
	lua_pushcfunction(pLuaState, LuaGroupObject::newiterator);
	lua_settable(pLuaState, -3);
}


LuaGroupItType::LuaGroupItType(LuaContext *pContext,const String &mtn)
	: t_lua_groupit_t(pContext, mtn)
{
}

int LuaGroupItObject::index(lua_State *pLuaState)
{
	lua_pushnil(pLuaState);
	return 1;
}

LuaArrayType::LuaArrayType(LuaContext *pContext,const String &mtn)
	: t_lua_array_t(pContext, mtn)
{
	setMethod("check", LuaArrayObject::check);
	setMethod("length", LuaArrayObject::length);
	setMethod("add", LuaArrayObject::add);
	setMethod("remove", LuaArrayObject::remove);
}

int LuaArrayObject::index(lua_State *pLuaState)
{
	LuaArrayObject *pObj = (LuaArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(lua_isnumber(pLuaState, -1))
	{
		int index = (int)lua_tonumber(pLuaState, -1);

		lua_pop(pLuaState, 1);
		t_lua_record::push(pObj->context(), pObj->data()->getRecord(index));
	}
	else
	{
		String attributeName = lua_tostring(pLuaState, -1);
		attributeName=pObj->aliasedName(attributeName);

		lua_pop(pLuaState, 1);

		if(!pObj->type()->pushMethod(attributeName))
		{
			lua_pushnil(pLuaState);
		}
	}
	return 1;
}

int LuaArrayObject::length(lua_State *pLuaState)
{
	LuaArrayObject *pObj = (LuaArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	lua_pushnumber(pLuaState, pObj->data()->length());

	return 1;
}

int LuaArrayObject::check(lua_State *pLuaState)
{
	LuaArrayObject *pObj = (LuaArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	String attributeName = lua_tostring(pLuaState, -1);
	attributeName=pObj->aliasedName(attributeName);

	lua_pushboolean(pLuaState,
			(int)pObj->data()->layout()->checkAttributeStr(attributeName));

	return 1;
}

int LuaArrayObject::add(lua_State *pLuaState)
{
	LuaArrayObject *pObj = (LuaArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	LuaBaseObject *pB = (LuaBaseObject *)lua_touserdata(pLuaState, -1);
	if(!pB)
	{
		LuaContext::error(pLuaState, "invalid record");
		return 0;
	}
	if(pB->type() != pObj->type()->context()->lookup(LuaContext::e_record))
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaRecordObject *pR = (LuaRecordObject *)pB;

	pObj->data()->add(pR->data());

	lua_pushnil(pLuaState);
	return 1;
}

int LuaArrayObject::remove(lua_State *pLuaState)
{
	LuaArrayObject *pObj = (LuaArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	LuaBaseObject *pB = (LuaBaseObject *)lua_touserdata(pLuaState, -1);
	if(!pB)
	{
		LuaContext::error(pLuaState, "invalid record");
		return 0;
	}
	if(pB->type() != pObj->type()->context()->lookup(LuaContext::e_record))
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaRecordObject *pR = (LuaRecordObject *)pB;

	pObj->data()->remove(pR->data());

	lua_pushnil(pLuaState);
	return 1;
}

int LuaArrayObject::newiterator(lua_State *pLuaState)
{
	t_lua_array *pObj = (t_lua_array *)lua_touserdata(pLuaState, -1);
	if(!pObj)
	{
		LuaContext::error(pLuaState,
				"Did you forget to call a record array as a function"
				" to create an iterator?");
		return 0;
	}

	lua_pushcfunction(pLuaState, iterator);
	LuaArrayContext arrayContext;

	if(!(pObj->data().isValid()) || pObj->data()->length() == 0)
	{
		return 1;
	}

	lua_pushnil(pLuaState);
	t_lua_arrayit *pIt = t_lua_arrayit::push(pObj->context(), arrayContext);
	pIt->data().m_index = 0;
	pIt->data().m_spRecordArray=pObj->data();

	return 3;
}

int LuaArrayObject::iterator(lua_State *pLuaState)
{
	t_lua_arrayit *pIt = (t_lua_arrayit *)lua_touserdata(pLuaState, -1);

	if(!pIt || pIt->data().m_spRecordArray.isNull() ||
			pIt->data().m_index>=pIt->data().m_spRecordArray->length())
	{
//		lua_pop(pLuaState, 1);
		return 0;
	}

	t_lua_record::push(pIt->context(),
			pIt->data().m_spRecordArray->getRecord(pIt->data().m_index));

	pIt->data().m_index++;

	return 2;
}

void LuaArrayType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaArrayObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaArrayObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__call");
	lua_pushcfunction(pLuaState, LuaArrayObject::newiterator);
	lua_settable(pLuaState, -3);
}

LuaArrayItType::LuaArrayItType(LuaContext *pContext,const String &mtn)
	: t_lua_arrayit_t(pContext, mtn)
{
}

int LuaArrayItObject::index(lua_State *pLuaState)
{
	LuaArrayItObject *pIt = (LuaArrayItObject *)selfIsFirst(pLuaState);

	if(!pIt)
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	const String text = lua_tostring(pLuaState, -1);

	if(text=="index")
	{
		//* NOTE already incremented
		lua_pushnumber(pLuaState,pIt->data().m_index-1);
		return 1;
	}

	lua_pushnil(pLuaState);
	return 1;
}

LuaIntArrayType::LuaIntArrayType(LuaContext *pContext,const String &mtn)
	: t_lua_intarray_t(pContext, mtn)
{
	setMethod("length", LuaIntArrayObject::length);
	setMethod("add", LuaIntArrayObject::add);
	setMethod("resize", LuaIntArrayObject::resize);
}

int LuaIntArrayObject::index(lua_State *pLuaState)
{
	LuaIntArrayObject *pObj = (LuaIntArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(lua_isnumber(pLuaState, -1))
	{
		int index = (int)lua_tonumber(pLuaState, -1);
		lua_pop(pLuaState, 1);
		lua_pushnumber(pLuaState, (*pObj->data())[index]);
		return 1;
	}

	String methodName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);

	if(!pObj->type()->pushMethod(methodName))
	{
		return 0;
	}

	return 1;
}

int LuaIntArrayObject::length(lua_State *pLuaState)
{
	LuaIntArrayObject *pObj = (LuaIntArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	lua_pushnumber(pLuaState, (*pObj->data()).size());
	return 1;
}

int LuaIntArrayObject::add(lua_State *pLuaState)
{
	LuaIntArrayObject *pObj = (LuaIntArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(!lua_isnumber(pLuaState, -1))
	{
		LuaContext::error(pLuaState, "bad value type");
		return 0;
	}

	const int newValue = (int)lua_tonumber(pLuaState, -1);
	lua_pop(pLuaState, 1);

	const I32 newIndex=pObj->data()->size();
	pObj->data()->push_back(newValue);

	lua_pushnumber(pLuaState, newIndex);
	return 1;
}

int LuaIntArrayObject::resize(lua_State *pLuaState)
{
	LuaIntArrayObject *pObj = (LuaIntArrayObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(!lua_isnumber(pLuaState, -1))
	{
		LuaContext::error(pLuaState, "bad size type");
		return 0;
	}

	const int newSize = (int)lua_tonumber(pLuaState, -1);
	lua_pop(pLuaState, 1);

	pObj->data()->resize(newSize);

	lua_pushnumber(pLuaState, newSize);
	return 1;
}

int LuaIntArrayObject::newiterator(lua_State *pLuaState)
{
	t_lua_intarray *pObj = (t_lua_intarray *)lua_touserdata(pLuaState, -1);
	if(!pObj)
	{
		LuaContext::error(pLuaState,
				"Did you forget to call a int array as a function"
				" to create an iterator?");
		return 0;
	}

	lua_pushcfunction(pLuaState, iterator);
	LuaIntArrayContext arrayContext;

	if(pObj->data()->size() == 0)
	{
		return 1;
	}

	lua_pushnil(pLuaState);
	t_lua_intarrayit *pIt =
			t_lua_intarrayit::push(pObj->context(), arrayContext);
	pIt->data().m_pIntArray=pObj->data();
	pIt->data().m_index = 0;

	return 3;
}

int LuaIntArrayObject::iterator(lua_State *pLuaState)
{
	t_lua_intarrayit *pIt = (t_lua_intarrayit *)lua_touserdata(pLuaState, -1);

	if(!pIt || pIt->data().m_index>=I32(pIt->data().m_pIntArray->size()))
	{
//		lua_pop(pLuaState, 1);
		return 0;
	}

	t_lua_record::push(pIt->context(),
			(*pIt->data().m_pIntArray)[pIt->data().m_index]);
	pIt->data().m_index++;

	return 2;
}

void LuaIntArrayType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaIntArrayObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaIntArrayObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__call");
	lua_pushcfunction(pLuaState, LuaIntArrayObject::newiterator);
	lua_settable(pLuaState, -3);
}

LuaIntArrayItType::LuaIntArrayItType(LuaContext *pContext,const String &mtn)
	: t_lua_intarrayit_t(pContext, mtn)
{
}

int LuaIntArrayItObject::index(lua_State *pLuaState)
{
	lua_pushnil(pLuaState);
	return 1;
}

LuaVectorType::LuaVectorType(LuaContext *pContext,const String &mtn)
	: t_lua_vector_t(pContext, mtn)
{
}

int LuaVectorObject::index(lua_State *pLuaState)
{
	LuaVectorObject *pObj = (LuaVectorObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(lua_isnumber(pLuaState, -1))
	{
		int index = (int)lua_tonumber(pLuaState, -1);
		lua_pop(pLuaState, 1);
		lua_pushnumber(pLuaState, pObj->data()[index]);
	}
	else
	{
		String methodName = lua_tostring(pLuaState, -1);

		lua_pop(pLuaState, 1);

		if(!pObj->type()->pushMethod(methodName))
		{
			lua_pushnil(pLuaState);
		}
	}
	return 1;
}

int LuaVectorObject::newindex(lua_State *pLuaState)
{
	LuaVectorObject *pObj = (LuaVectorObject *)lua_touserdata(pLuaState, 1);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	if(lua_isnumber(pLuaState, -2))
	{
		int index = (int)lua_tonumber(pLuaState, -2);
		float value = (float)lua_tonumber(pLuaState, -1);

		pObj->data()[index] = value;

		lua_pushnumber(pLuaState, value);
	}
	else
	{
		lua_pushnil(pLuaState);
	}

	return 1;
}

void LuaVectorType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaVectorObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaVectorObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__newindex");
	lua_pushcfunction(pLuaState, LuaVectorObject::newindex);
	lua_settable(pLuaState, -3);
}

LuaStateCatalogType::LuaStateCatalogType(LuaContext *pContext,const String &mtn)
	: t_lua_statecatalog_t(pContext, mtn)
{
	setMethod("getState",		LuaStateCatalogObject::getState);
	setMethod("setState",		LuaStateCatalogObject::setState);
}

int LuaStateCatalogObject::index(lua_State *pLuaState)
{
	String attributeName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);
	LuaStateCatalogObject *pObj =
			(LuaStateCatalogObject *)selfIsFirst(pLuaState);

	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	attributeName=pObj->aliasedName(attributeName);

	if(!pObj->type()->pushMethod(attributeName))
	{
		lua_pushnil(pLuaState);
	}
	return 1;
}

int LuaStateCatalogObject::getState(lua_State *pLuaState)
{
	LuaStateCatalogObject *pObj =
			(LuaStateCatalogObject *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	if(lua_gettop(pLuaState)<2)
	{
		return 0;
	}

	sp<StateCatalog>& rspStateCatalog=pObj->data();

	const String keyName = lua_tostring(pLuaState, -1);
	lua_pop(pLuaState, 1);

	String propertyName = "value";

	if(lua_gettop(pLuaState) == 3)
	{
		propertyName = lua_tostring(pLuaState, -1);
		lua_pop(pLuaState, 1);
	}

//	const String typeName=rspStateCatalog->cataloged(keyName,propertyName)?
//			rspStateCatalog->catalogTypeName(keyName,propertyName): "";
	String typeName;
	rspStateCatalog->getTypeName(keyName,propertyName,typeName);

//	feLog("LuaStateCatalogObject::getState \"%s\" \"%s\" as \"%s\"\n",
//			keyName.c_str(),propertyName.c_str(),typeName.c_str());

	sp<TypeMaster> spTypeMaster=rspStateCatalog->typeMaster();
	sp<BaseType> spBaseType=spTypeMaster->lookupName(typeName);

	if(spBaseType==spTypeMaster->lookupType<bool>())
	{
		bool value(false);
		rspStateCatalog->getState<bool>(keyName,propertyName,value);
		lua_pushboolean(pLuaState,value);
	}
	else if(spBaseType==spTypeMaster->lookupType<U8>())
	{
		U8 value(0);
		rspStateCatalog->getState<U8>(keyName,propertyName,value);

		lua_pushnumber(pLuaState,value);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<U32>())
	{
		U32 value(0);
		rspStateCatalog->getState<U32>(keyName,propertyName,value);

		lua_pushnumber(pLuaState,value);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<I32>())
	{
		I32 value(0);
		rspStateCatalog->getState<I32>(keyName,propertyName,value);

		lua_pushnumber(pLuaState,value);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Real>())
	{
		Real value(0);
		rspStateCatalog->getState<Real>(keyName,propertyName,value);

		lua_pushnumber(pLuaState,value);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<F32>())
	{
		F32 value(0);
		rspStateCatalog->getState<F32>(keyName,propertyName,value);

		lua_pushnumber(pLuaState,value);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<F64>())
	{
		F64 value(0);
		rspStateCatalog->getState<F64>(keyName,propertyName,value);

		lua_pushnumber(pLuaState,value);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<String>())
	{
		String value;
		rspStateCatalog->getState<String>(keyName,propertyName,value);

		lua_pushstring(pLuaState,value.c_str());
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Vector2f>())
	{
		Vector2f value(0,0,0);
		rspStateCatalog->getState<Vector2f>(keyName,propertyName,value);

		Vector2f* pNew=new Vector2f(value);
		t_lua_vector2f::push(pObj->context(),pNew,TRUE);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Vector2d>())
	{
		Vector2d value(0,0,0);
		rspStateCatalog->getState<Vector2d>(keyName,propertyName,value);

		Vector2d* pNew=new Vector2d(value);
		t_lua_vector2d::push(pObj->context(),pNew,TRUE);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Vector3f>())
	{
		Vector3f value(0,0,0);
		rspStateCatalog->getState<Vector3f>(keyName,propertyName,value);

		Vector3f* pNew=new Vector3f(value);
		t_lua_vector3f::push(pObj->context(),pNew,TRUE);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Vector3d>())
	{
		Vector3d value(0,0,0);
		rspStateCatalog->getState<Vector3d>(keyName,propertyName,value);

		Vector3d* pNew=new Vector3d(value);
		t_lua_vector3d::push(pObj->context(),pNew,TRUE);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<SpatialEuler>())
	{
		SpatialEuler value(0,0,0);
		rspStateCatalog->getState<SpatialEuler>(keyName,propertyName,value);

		Vector3f* pNew=new Vector3f(value);
		t_lua_vector3f::push(pObj->context(),pNew);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Vector4f>())
	{
		Vector4f value(0,0,0);
		rspStateCatalog->getState<Vector4f>(keyName,propertyName,value);

		Vector4f* pNew=new Vector4f(value);
		t_lua_vector4f::push(pObj->context(),pNew,TRUE);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Vector4d>())
	{
		Vector4d value(0,0,0);
		rspStateCatalog->getState<Vector4d>(keyName,propertyName,value);

		Vector4d* pNew=new Vector4d(value);
		t_lua_vector4d::push(pObj->context(),pNew,TRUE);
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<Color>())
	{
		Color value(0,0,0,1);
		rspStateCatalog->getState<Color>(keyName,propertyName,value);

		Color* pNew=new Color(value);
#if FE_DOUBLE_REAL
		t_lua_vector4d::push(pObj->context(),pNew);
#else
		t_lua_vector4f::push(pObj->context(),pNew);
#endif
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<SpatialQuaternion>())
	{
		SpatialQuaternion value(0,0,0,1);
		rspStateCatalog->getState<SpatialQuaternion>(
				keyName,propertyName,value);

		Vector4* pNew=new Vector4(value[0],value[1],value[2],value[3]);
#if FE_DOUBLE_REAL
		t_lua_vector4d::push(pObj->context(),pNew,TRUE);
#else
		t_lua_vector4f::push(pObj->context(),pNew,TRUE);
#endif
		return 1;
	}
	else if(spBaseType==spTypeMaster->lookupType<SpatialTransform>())
	{
		SpatialTransform value;
		setIdentity(value);
		rspStateCatalog->getState<SpatialTransform>(
				keyName,propertyName,value);

		SpatialTransform* pNew=new SpatialTransform(value);
		t_lua_transform::push(pObj->context(),pNew);
		return 1;
	}

	return 0;
}

int LuaStateCatalogObject::setState(lua_State *pLuaState)
{
	LuaStateCatalogObject *pObj =
			(LuaStateCatalogObject *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaStateCatalogObject* pOther;
	if((pOther=reinterpret_cast<LuaStateCatalogObject*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		pObj->data()=pOther->data();
		t_lua_statecatalog::push(pObj->context(),pObj->data());
		return 1;
	}

	sp<StateCatalog>& rspStateCatalog=pObj->data();

	const String keyName = lua_tostring(pLuaState, -1);
	lua_pop(pLuaState, 1);

	String propertyName = "value";

//	const String typeName=rspStateCatalog->cataloged(keyName,propertyName)?
//			rspStateCatalog->catalogTypeName(keyName,propertyName): "";
	String typeName;
	rspStateCatalog->getTypeName(keyName,propertyName,typeName);

	sp<TypeMaster> spTypeMaster=rspStateCatalog->typeMaster();
	sp<BaseType> spBaseType=spTypeMaster->lookupName(typeName);

	if(lua_gettop(pLuaState) == 3)
	{
		propertyName = lua_tostring(pLuaState, -1);
		lua_pop(pLuaState, 1);
	}

	int t = lua_type(pLuaState, -1);
	if(t == LUA_TBOOLEAN)
	{
		rspStateCatalog->setState<bool>(keyName,propertyName,
				lua_toboolean(pLuaState, -1));
		lua_pop(pLuaState, 1);
	}
	else if(t == LUA_TNUMBER)
	{
		if(spBaseType==spTypeMaster->lookupType<U8>())
		{
			rspStateCatalog->setState<U8>(keyName,propertyName,
					lua_tonumber(pLuaState, -1));
			lua_pop(pLuaState, 1);
		}
		else if(spBaseType==spTypeMaster->lookupType<U32>())
		{
			rspStateCatalog->setState<U32>(keyName,propertyName,
					lua_tonumber(pLuaState, -1));
			lua_pop(pLuaState, 1);
		}
		else if(spBaseType==spTypeMaster->lookupType<I32>())
		{
			rspStateCatalog->setState<I32>(keyName,propertyName,
					lua_tonumber(pLuaState, -1));
			lua_pop(pLuaState, 1);
		}
		else if(spBaseType==spTypeMaster->lookupType<F64>())
		{
			rspStateCatalog->setState<F64>(keyName,propertyName,
					lua_tonumber(pLuaState, -1));
			lua_pop(pLuaState, 1);
		}
		else
		{
			rspStateCatalog->setState<Real>(keyName,propertyName,
					lua_tonumber(pLuaState, -1));
			lua_pop(pLuaState, 1);
		}
	}
	else if(t == LUA_TSTRING)
	{
		rspStateCatalog->setState<String>(keyName,propertyName,
				lua_tostring(pLuaState, -1));
		lua_pop(pLuaState, 1);
	}
	else if(t == LUA_TUSERDATA)
	{
		if(spBaseType==spTypeMaster->lookupType<Vector3>())
		{
			rspStateCatalog->setState<Vector3>(keyName,propertyName,
					Vector3(
					lua_tonumber(pLuaState, -3),
					lua_tonumber(pLuaState, -2),
					lua_tonumber(pLuaState, -1)));
			lua_pop(pLuaState, 3);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialEuler>())
		{
			rspStateCatalog->setState<SpatialEuler>(keyName,propertyName,
					SpatialEuler(
					lua_tonumber(pLuaState, -3),
					lua_tonumber(pLuaState, -2),
					lua_tonumber(pLuaState, -1)));
			lua_pop(pLuaState, 3);
		}
		else if(spBaseType==spTypeMaster->lookupType<Vector4>())
		{
			rspStateCatalog->setState<Vector4>(keyName,propertyName,
					Vector4(
					lua_tonumber(pLuaState, -4),
					lua_tonumber(pLuaState, -3),
					lua_tonumber(pLuaState, -2),
					lua_tonumber(pLuaState, -1)));
			lua_pop(pLuaState, 4);
		}
		else if(spBaseType==spTypeMaster->lookupType<Color>())
		{
			rspStateCatalog->setState<Color>(keyName,propertyName,
					Color(
					lua_tonumber(pLuaState, -4),
					lua_tonumber(pLuaState, -3),
					lua_tonumber(pLuaState, -2),
					lua_tonumber(pLuaState, -1)));
			lua_pop(pLuaState, 4);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialQuaternion>())
		{
			rspStateCatalog->setState<SpatialQuaternion>(keyName,propertyName,
					SpatialQuaternion(
					lua_tonumber(pLuaState, -4),
					lua_tonumber(pLuaState, -3),
					lua_tonumber(pLuaState, -2),
					lua_tonumber(pLuaState, -1)));
			lua_pop(pLuaState, 4);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialTransform>())
		{
			SpatialTransform transform;

			for(I32 m=0;m<4;m++)
			{
				transform.column(m)=Vector4(
						lua_tonumber(pLuaState, -3),
						lua_tonumber(pLuaState, -2),
						lua_tonumber(pLuaState, -1));
				lua_pop(pLuaState, 3);
			}

			rspStateCatalog->setState<SpatialTransform>(keyName,propertyName,
					transform);
		}
	}

	t_lua_statecatalog::push(pObj->context(),pObj->data());
	return 1;
}

// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------

LuaTransformType::LuaTransformType(LuaContext *pContext,const String &mtn)
	: t_lua_transform_t(pContext, mtn)
{
	//* unit normalize magnitude dot cross3 set
	setMethod("copy",		LuaTransformObject::copy);
	setMethod("set",		LuaTransformObject::set);
}

int LuaTransformObject::index(lua_State *pLuaState)
{
	LuaTransformObject *pObj = (LuaTransformObject *)selfIsFirst(pLuaState);
	if(!pObj)
	{
		return 0;
	}

	if(lua_isnumber(pLuaState, -1))
	{
		int index = (int)lua_tonumber(pLuaState, -1);
		lua_pop(pLuaState, 1);
		lua_pushnumber(pLuaState, (pObj->data()->column(index/3))[index%3]);
		return 1;
	}

	String methodName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);

	if(!pObj->type()->pushMethod(methodName))
	{
		return 0;
	}

	return 1;
}

int LuaTransformObject::newindex(lua_State *pLuaState)
{
	LuaTransformObject *pObj =
			(LuaTransformObject *)lua_touserdata(pLuaState, 1);
	if(!pObj|| !lua_isnumber(pLuaState, -2))
	{
		return 0;
	}

	int index = (int)lua_tonumber(pLuaState, -2);
	float value = (float)lua_tonumber(pLuaState, -1);

	setAt(pObj->data()->column(index/3),index%3,(Real)value);

	lua_pushnumber(pLuaState, value);
	return 1;
}

int LuaTransformObject::eq(lua_State *pLuaState)
{
	LuaTransformObject *pObj = (LuaTransformObject *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaTransformObject* pOther;
	if((pOther=reinterpret_cast<LuaTransformObject*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		lua_pushboolean(pLuaState,*pObj->data() == *pOther->data());
		return 1;
	}

	feLog("LuaTransformObject::eq incompatible argType \"%s\"\n",
			argType(pLuaState,2).c_str());
	return 0;
}

int LuaTransformObject::copy(lua_State *pLuaState)
{
	LuaTransformObject *pObj = (LuaTransformObject *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	SpatialTransform* pDuplicate=new SpatialTransform(*pObj->data());
	t_lua_transform::push(pObj->context(),pDuplicate);
	return 1;
}

int LuaTransformObject::set(lua_State *pLuaState)
{
	LuaTransformObject *pObj = (LuaTransformObject *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaTransformObject* pOther;
	if((pOther=reinterpret_cast<LuaTransformObject*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		*pObj->data()=*pOther->data();
		t_lua_transform::push(pObj->context(),pObj->data());
		return 1;
	}

	SpatialTransform& rTransform=*pObj->data();

	I32 argIndex=2;
	for(I32 m=0;m<4;m++)
	{
		Real vector[4]={ 0.0f,0.0f,0.0f };

		for(I32 n=0;n<3;n++)
		{
			if(lua_isnumber(pLuaState, argIndex))
			{
				vector[n]=(Real)lua_tonumber(pLuaState, argIndex);
			}
			argIndex++;
		}

		fe::set(rTransform.column(m),vector[0],vector[1],vector[2]);
	}

	t_lua_transform::push(pObj->context(),pObj->data());
	return 1;
}

void LuaTransformType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaTransformObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaTransformObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__newindex");
	lua_pushcfunction(pLuaState, LuaTransformObject::newindex);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__eq");
	lua_pushcfunction(pLuaState, LuaTransformObject::eq);
	lua_settable(pLuaState, -3);
}

} /* namespace ext */
} /* namespace fe */

