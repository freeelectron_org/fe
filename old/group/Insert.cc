/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "group.pmh"

namespace fe
{
namespace group
{

Insert::Insert(void)
{
	m_attributeNames.resize(3);
	m_attributeNames[0] = "input_group";
	m_attributeNames[1] = "output_group";
	m_attributeNames[2] = "_ID";
}

Insert::~Insert(void)
{
}

void Insert::handle(Record &signal)
{
	if(		m_aInput.check(signal)
		&&	m_aOutput.check(signal))
	{
		for(RecordGroup::iterator it = m_aInput(signal)->begin();
			it != m_aInput(signal)->end(); it++)
		{
			sp<RecordArray> spRA = *it;
			if(m_aID.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					Record r = m_aOutput(signal)->lookup(m_aID, m_aID(spRA, i));
					if(!r.isValid())
					{
						m_aOutput(signal)->add(spRA->getRecord(i));
					}
				}
			}
		}
	}
}

void Insert::bind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_spScope = spLayout->scope();
	doSetup();
}

void Insert::parse(std::vector<String> &attributeNames)
{
	if(attributeNames.size() != 3)
	{
		feX("Projector::setup",
			"expecting 3 attribute names");
	}
	m_attributeNames = attributeNames;
	doSetup();
}

void Insert::doSetup(void)
{
	if(!m_spScope.isValid())
	{
		return;
	}
	m_aInput.initialize(m_spScope,m_attributeNames[0]);
	m_aOutput.initialize(m_spScope,m_attributeNames[1]);
	m_aID.initialize(m_spScope,m_attributeNames[2]);
}



} /* namespace */
} /* namespace */

