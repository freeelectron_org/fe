#ifndef __mechanics_mechanics_h__
#define __mechanics_mechanics_h__

#include "mechanicsAS.h"
//#include "JointI.h"
//#include "BodyI.h"
//#include "MechanicsSimI.h"
#include "LinearSpringForce.h"
//#include "LinearSpringForce1D.h"
//#include "LinearSpringForce2D.h"
#include "LinearBendForce.h"
#include "LinearStrandForce.h"
//#include "LinearStrandForce2D.h"

namespace fe
{
Library* CreateMechanicsLibrary(sp<Master> spMaster);
}

#endif /* __mechanics_mechanics_h__ */

