#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"

#include "Animation/MorphTarget.h"
#include "Components/DynamicMeshComponent.h"
#include "DynamicMesh/MeshNormals.h"
#include "DynamicMesh/MeshAttributeUtil.h"
#include "Engine/SkeletalMesh.h"
#include "Materials/Material.h"
#include "Rendering/SkeletalMeshModel.h"
#include "Rendering/SkeletalMeshRenderData.h"
#include "Misc/Paths.h"

#include "Runtime/Launch/Resources/Version.h"
#define FE_UNREAL_5_1_PLUS (ENGINE_MAJOR_VERSION>=6 || (ENGINE_MAJOR_VERSION==5 && ENGINE_MINOR_VERSION>=1))
#define FE_UNREAL_5_2_PLUS (ENGINE_MAJOR_VERSION>=6 || (ENGINE_MAJOR_VERSION==5 && ENGINE_MINOR_VERSION>=2))

#if FE_UNREAL_5_2_PLUS
#include "MaterialDomain.h"
#endif

#include "IFEOps.h"

//* must be last include
#include "FEOperator.generated.h"

//UCLASS(hidecategories = (Object, LOD),
//	meta = (BlueprintSpawnableComponent),
//	ClassGroup = Rendering)
UCLASS(Blueprintable,BlueprintType)
class AFEOperator: public AActor
{
        GENERATED_UCLASS_BODY()

	protected:

virtual	void	OperatorCreate(void);
virtual	void	OperatorInit(void)											{}
virtual	void	OperatorUpdate(void)										{}

		UPROPERTY(VisibleAnywhere, Category="Operator",
				DisplayName="Dynamic Mesh")
		TObjectPtr<UDynamicMeshComponent> m_meshComponent;

		//* WARNING declaring BeginPlay here will make it not work in Blueprints
		//* probably true for other events
//virtual	void	BeginPlay(void) override								{}

virtual	void	PostActorCreated(void) override;
virtual	void	PostLoad(void) override;

virtual	bool	ShouldTickIfViewportsOnly() const override	{ return true; }
virtual	void	Tick(float DeltaTime) override;

virtual	void	UpdateMesh(void);

		fe::sp<fe::SingleMaster>			m_spSingleMaster;
		fe::ext::TerminalNode				m_terminalNode;

		fe::String							m_componentName;
		fe::String							m_nodeName;
		fe::Real							m_time;

		fe::sp<fe::ext::SurfaceAccessibleI>	m_spOutputAccessible;

											//* map UE vertex into surface
		fe::Array<I32>						m_vertexPoint;
		fe::Array<I32>						m_vertexPrimitive;
		fe::Array<I32>						m_vertexSub;
		I32									m_lastVertexCount;
		I32									m_lastTriangleCount;
};
