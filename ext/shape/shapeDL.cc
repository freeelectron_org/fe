/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <shape/shape.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

//	pLibrary->addSingleton<PickSelector>("SelectorI.PickSelector.fe");

	pLibrary->add<ShapeSelector>("SelectorI.ShapeSelector.fe");

	pLibrary->add<AsParticle>("PopulateI.AsParticle.fe");
	pLibrary->add<AsBounded>("PopulateI.AsBounded.fe");
	pLibrary->add<AsPoint>("PopulateI.AsPoint.fe");

	pLibrary->add<Sphere>("RecordFactoryI.Sphere.fe");
	pLibrary->add<Disk>("RecordFactoryI.Disk.fe");
	pLibrary->add<Cylinder>("RecordFactoryI.Cylinder.fe");

	sp<Scope> spScope=spMaster->catalog()->catalogComponent("Scope","SimScope");
	const String path=spMaster->catalog()->catalog<String>("path:media")+
			"/template/shape.rg";
	RecordView::loadRecordGroup(spScope,path);

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
