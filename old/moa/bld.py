import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "evaluate", "imgui" ]

def setup(module):
    lib_srcList = [ "moa.pmh",
                    "data",
                    "Function" ]

    lib = module.Lib( "fexMoa", lib_srcList )

    srcList = [ "moa.pmh",
                "SplatLineSystem",
                "Orchestrator",
                "EvaluateSystem",
                "MultiSweepSystem",
                "FrequencyResponseSystem",
                "SimulatorLoopSystem",
                "moaDL" ]

    dll = module.DLL( "fexMoaDL", srcList )

    # dll.linkmap = { "tbb": ' -ltbb' }

    deplibs = forge.corelibs + [
                "feMathLib",
                "fexEvaluateDLLib",
                "fexMoaLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "feDataDLLib",
                "fexMechanicsDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib" ]

    forge.deps( ["fexMoaDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexMoaDL",         None,   None) ]

    module.Module('test')
