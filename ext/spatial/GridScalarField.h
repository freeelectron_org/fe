/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_GridScalarField_h__
#define __spatial_GridScalarField_h__

#include "fe/plugin.h"
#include "spatial/ScalarFieldI.h"
#include "Space.h"

namespace fe
{
namespace ext
{


/** A ScalarFieldI implementation using a Continuum (grid of points) */
class FE_DL_EXPORT GridScalarField :
	virtual public ScalarFieldI,
	public Initialize<GridScalarField>
{
	public:
typedef Continuum<Real>	t_continuum;

					GridScalarField(void);
virtual				~GridScalarField(void);

		void		initialize(void);

					// AS ScalarFieldI
virtual	Real		sample(		const SpatialVector &a_location);
virtual	void		gradient(	SpatialVector &a_gradient,
								const SpatialVector &a_location);
virtual	void		radialSet(	const SpatialVector &a_location,
								const SpatialVector &a_radius,
								Real a_value);
virtual	Real		volume(		void);
virtual	void		set(		Real a_value);
virtual	void		set(		const SpatialVector &a_location,
								Real a_value);
virtual	void		add(		const SpatialVector &a_location,
								Real a_value);
virtual	sp<SpaceI>	space(		void);

		void		create(		const sp<SpaceI>	&a_space,
								const Vector3i		&a_count,
								Real				a_default = 0.0);

		t_continuum		&continuum(void);

	private:
		bool		getIndex(t_continuum::t_index &a_index,
						const SpatialVector &a_loc);


	private:
		t_continuum						m_continuum;
		Real							m_volume;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_GridScalarField_h__ */

