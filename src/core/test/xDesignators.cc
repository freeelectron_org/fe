/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"
#include "math.h"

using namespace fe;

template<typename T,T defaultValue>
class DefaultedValue
{
	public:
				DefaultedValue(T a_value=defaultValue): m_value(a_value)	{}

				operator T(void) const	{ return m_value; }
		bool	operator==(const T a_value) const
				{	return m_value==a_value; }
		bool	operator!=(const T a_value) const
				{	return m_value!=a_value; }
	private:
		T		m_value;
};

class Class0
{
	public:

		struct Method0_Args
		{
			DefaultedValue<int,7>	defaulted0;
			DefaultedValue<int,7>	defaulted1;
			String					string0;
			String					string1;
			char*					buffer0;
			double					double0;
			float					float0;
			float					float1;
			int						int0;
			bool					bool0;
		};

		int method0(const Method0_Args& args)
		{
			printf("defaulted0 %d\n",	int(args.defaulted0));
			printf("defaulted1 %d\n",	int(args.defaulted1));
			printf("string0 \"%s\"\n",	args.string0.c_str());
			printf("string1 \"%s\"\n",	args.string1.c_str());
			printf("buffer0 \"%s\"\n",	args.buffer0);
			printf("double0 %.3f\n",	args.double0);
			printf("float0 %.3f\n",		args.float0);
			printf("float1 %.3f\n",		args.float1);
			printf("int0 %d\n",			args.int0);
			printf("bool0 %d\n",		args.bool0);

			int problems(0);
			problems+=(args.defaulted0!=7);
			problems+=(args.defaulted1!=8);
			problems+=(args.string0!="");
			problems+=(args.string1!="abc");
			problems+=(args.buffer0!=nullptr);
			problems+=(args.double0!=0.0);
			problems+=(args.float0!=0.0);
			problems+=(fabs(args.float1-1.23)>1e-6);
			problems+=(args.int0!=3);
			problems+=(args.bool0!=false);
			return problems;
		}
};

int main()
{
	UNIT_START();

#if FE_COUNTED_TRACK
	Counted::startTracker();
#endif

	BWORD completed=FALSE;

	try
	{
		Class0 object0;

		//* sort-of 'Named Args'
		//* - must be in order
		//* - omission is ok, but leads to warnings
		//* - omitted args without constructor seem to init to zero
		const int problems=object0.method0(Class0::Method0_Args{
				.defaulted1=8,
				.string1="abc",
				.float1=1.23,
				.int0=3
				});

		UNIT_TEST(!problems);

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

#if FE_COUNTED_TRACK
	feLog("Counted %s\n",Counted::reportTracker().c_str());
	Counted::stopTracker();
#endif

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
