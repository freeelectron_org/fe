/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __sample_text_BruteScan_h__
#define __sample_text_BruteScan_h__

namespace fe
{

/**************************************************************************//**
    @brief Analyze text on the fly

	@ingroup sample_text

	Just answer queries without much regard for performance.
	Capital letters are not considered.
*//***************************************************************************/
class FE_DL_EXPORT BruteScan: virtual public StringScanI
{
	public:
				BruteScan(void)												{}
virtual			~BruteScan(void)											{}

				//* As StringScanI
virtual	void	analyze(String a_text);
virtual	U32		wordCount(void);
virtual	U32		charCount(void);
virtual	U32		letterCount(void);
virtual	U32		letterFrequency(char m_letter);

	private:

		void	populateHistogram(void);

		String				m_text;
		std::map<char,U32>	m_histogram;
};

} // namespace

#endif /* __sample_text_BruteScan_h__ */
