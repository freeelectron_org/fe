/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

using namespace fe;

int main(void)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		const String intName=fe_type_name<int>();
		feLogDirect("'int' is \"%s\"\n",intName.c_str());
		UNIT_TEST(intName=="int");

		const String constCharStarName=fe_type_name<const char*>();
		feLogDirect("'const char *' is \"%s\"\n",constCharStarName.c_str());
		UNIT_TEST(constCharStarName=="const char*");

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLogDirect("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(3);
	UNIT_RETURN();
}
