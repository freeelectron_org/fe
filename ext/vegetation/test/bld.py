import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs+ [
            "fexDataToolDLLib",
            "fexOperateDLLib",
            "fexSurfaceDLLib",
            "fexVegetationDLLib",
            "fexWindowDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]
        if 'viewer' in forge.modules_confirmed:
            deplibs += [    "fexViewerDLLib" ]

    tests = [   'xTree' ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    if forge.media:
        forge.tests += [
            ("xTree.exe",   "Aspen_Quaking 10",     None,   None),
            ("xTree.exe",   "Bamboo 10",            None,   None),
            ("xTree.exe",   "Elm_Rock 10",          None,   None),
            ("xTree.exe",   "Fir_Balsam 10",        None,   None),
            ("xTree.exe",   "Pine_Jack 10",         None,   None),
            ("xTree.exe",   "Pine_Longleaf 10",     None,   None),
            ("xTree.exe",   "Poplar_Lombardy 10",   None,   None),
            ("xTree.exe",   "Tupelo_Black 10",      None,   None),
            ("xTree.exe",   "crude 10",             None,   None),
            ("xTree.exe",   "death 10",             None,   None),
            ("xTree.exe",   "dense 10",             None,   None) ]

#           ("xTree.exe",   "feather 10",           None,   None),
#           ("xTree.exe",   "mop 10",               None,   None),
#           ("xTree.exe",   "nest 10",              None,   None),
#           ("xTree.exe",   "tower 10",             None,   None),
#           ("xTree.exe",   "umbrella 10",          None,   None),
#           ("xTree.exe",   "yarn 10",              None,   None),
#           ("xTree.exe",   "ieee 4",               None,   None),
#           ("xTree.exe",   "forest 4",             None,   None),
