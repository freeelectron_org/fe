#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

#if defined(_WIN32) || defined(_WIN64)
#pragma warning(push)
#pragma warning(disable:4002)
#pragma warning(disable:4003)
#endif

#include "unreal/unrealModule.pmh"

#if defined(_WIN32) || defined(_WIN64)
#pragma warning(pop)
#endif

inline void UnrealLogString(fe::String a_string)
{
	UE_LOG(LogTemp,Display,TEXT("%s"),*FString(a_string.c_str()));
}

inline void UnrealPrintFunction(const char* a_ascii)
{
	UnrealLogString(fe::String(a_ascii).chop("\n"));
}

class FE_DL_PUBLIC UnrealLog: public fe::Log
{
	public:
					UnrealLog(void)											{}
virtual				~UnrealLog(void)										{}

					using Log::log;

virtual		void	log(const std::string &a_message)
					{
						const fe::String text=fe::String(
								a_message.c_str()).chop("\n");
						const fe::String trim=text.prechop("\n");
						if(trim!=text)
						{
							UnrealLogString("");
						}
						UnrealLogString(m_preface+trim);
					}

			void	setPreface(fe::String a_preface)
					{	m_preface=a_preface; }

	protected:
			fe::String m_preface;
};

/**
	The public interface to this module.
	In most cases, this interface is only public to sibling modules
	within this plugin.
 */
class IFEOps : public IModuleInterface
{

public:

	/**
		Singleton-like access to this module's interface.
		This is just for convenience!
		Beware of calling this during the shutdown phase, though.
		Your module might have been unloaded already.

		@return Returns singleton instance,
		loading the module on demand if needed
	*/
	static inline IFEOps& Get()
	{
		return FModuleManager::LoadModuleChecked< IFEOps >("FEOps");
	}

	/**
		Checks to see if this module is loaded and ready.
		It is only valid to call Get() if IsAvailable() returns true.

		@return True if the module is loaded and ready to use
	*/
	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("FEOps");
	}
};

inline fe::sp<fe::SingleMaster> UnrealGetSingleMaster(void)
{
	fe::Logger* oldLogger=gs_feLogger;

	fe::sp<fe::SingleMaster> spSingleMaster=fe::SingleMaster::create();
	FEASSERT(spSingleMaster.isValid());

	if(!oldLogger)
	{
		//* NOTE initializations run only once

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		UnrealLogString("ResetFeLogger");

		//* for feLogDirect, including Exceptions
		fe::setPrintFunction(&UnrealPrintFunction);

		//* NOTE all common log mechanisms are useless on Windows Unreal
		feLogger()->clearAll();

		UnrealLog* pUnrealLogFe=new UnrealLog();
		pUnrealLogFe->setPreface("fe  ");
		feLogger()->setLog("unreal_fe", pUnrealLogFe);
		feLogger()->bind(".*", "unreal_fe");

		UnrealLog* pUnrealLogLua=new UnrealLog();
		pUnrealLogLua->setPreface("lua ");
		feLogger()->setLog("unreal_lua", pUnrealLogLua);
		feLogger()->bind("lua_script", "unreal_lua");
		feLogger()->antibind("lua_script", "unreal_fe");

		//* just to show that we can
		UnrealLog* pUnrealLogUnreal=new UnrealLog();
		pUnrealLogUnreal->setPreface("fe+ ");
		feLogger()->setLog("unreal", pUnrealLogUnreal);
		feLogger()->bind("unreal_plugins", "unreal");
		feLogger()->antibind("unreal_plugins", "unreal_fe");
#endif

#if FE_COMPILER==FE_GNU && !defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"
#endif
		const fe::String loadPath=fe::System::getLoadPath(
				(void*)IFEOps::IsAvailable,TRUE).pathname();
#if FE_COMPILER==FE_GNU && !defined(__clang__)
#pragma GCC diagnostic pop
#endif

		fe::DL_Loader::addPath(loadPath);

		feLog("(IFEOps) ::UnrealGetSingleMaster loadPath \"%s\"\n",
				loadPath.c_str());

		fe::sp<fe::Registry> spRegistry=spSingleMaster->master()->registry();

//		spRegistry->master()->catalog()->catalog<String>("path:media")=
//				"/home/symhost/sym/proj/fe/main/media";

		spRegistry->manage("fexOperatorDL");
		spRegistry->manage("fexVegetationDL");

//		spRegistry->manage("fexIronWorksDL");
//		spRegistry->manage("fexGrassDL");
	}

	return spSingleMaster;
}
