/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_h__
#define __thread_h__

#include "fe/plugin.h"
#include "math/math.h"

//#include "boost/thread.hpp"
//#include "boost/thread/thread.hpp"
//#include "boost/thread/tss.hpp"
//#include "boost/thread/condition.hpp"

#ifdef MODULE_thread
#define FE_THREAD_PORT FE_DL_EXPORT
#else
#define FE_THREAD_PORT FE_DL_IMPORT
#endif


#include "thread/JobQueue.h"
#include "thread/Gang.h"

#include "thread/SpannedRange.h"
#include "thread/WorkForceI.h"
#include "thread/WorkI.h"

#include "thread/CountedPool.h"

#endif // __thread_h__

