/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __guide_GuidePostI_h__
#define __guide_GuidePostI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Make Connections through Beacon

	@ingroup guide
*//***************************************************************************/
class FE_DL_EXPORT GuidePostI:
	virtual public beacon::BeaconClientI,
	public CastableAs<GuidePostI>
{
	public:

									/** @brief connect to the Beacon server

										The configuration string is currently
										just an IP address, but this may be
										extended in the future.

										An empty configuration string will use
										default settings.

										Returns FALSE if no connection
										could be made. */
virtual	BWORD						connect(String a_configuration)			=0;

									/** @brief get the known space names

										Populates the provided array
										with space names. */
virtual	void						listSpaces(
										Array<String>& a_rSpaceList)=0;

									/** @brief find and connect to space

										Find a match space in beacon nodes
										and connect.
										A StateCatalog of the appropriate
										implementation is returned.
										If no such space exists or
										the connection failed,
										an invalid reference is returned. */

virtual	sp<StateCatalog>			connectToSpace(String a_space)			=0;

									/** @brief invoke a new named space

										The newly created catalog is overlayed
										with values for the provided catalog. */
virtual	sp<StateCatalog>			createSpace(String a_space,
										sp<Catalog> a_spCatalog)=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __guide_GuidePostI_h__ */
