import sys
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "scene.pmh",
                "sceneDL" ]

    dll = module.DLL( "fexSceneDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    forge.deps( ["fexSceneDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexSceneDL",                       None,   None) ]
