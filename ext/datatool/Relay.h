/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_Relay_h__
#define __datatool_Relay_h__

#define	FE_RELAY_ASSERT			TRUE			//* for unmatched gets and sets
#define	FE_RELAY_SAFE			(FE_CODEGEN<=FE_DEBUG)	//* query records safely

#define	FE_RELAY_STORE_DEBUG	TRUE
#define	FE_RELAY_DEBUG			(FE_RELAY_STORE_DEBUG && FE_CODEGEN<FE_DEBUG)

#include <sstream>

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Default for BASE Relay template argument

	The use of this class as BASE in Relay indicates no inclusion.
*//***************************************************************************/
class FE_DL_EXPORT NoRelay
{
	public:
		enum
		{
			e_size=0
		};

static
const	char*	typeAt(U32 index)	{ return NULL; }
static
const	char*	nameAt(U32 index)	{ return NULL; }

		void	initializeRecursively(Record&)								{}
};

/**************************************************************************//**
	@brief Generic interface to the Relay template
*//***************************************************************************/
class FE_DL_EXPORT CoreRelay
{
	public:
virtual			~CoreRelay(void)											{}
virtual	Record	createRecord(void)											=0;
virtual	Record	createRecordAndBind(void)									=0;
virtual	void	bind(sp<Scope> spScope)										=0;
};

/**************************************************************************//**
	@brief Counted wrapper for a CoreRelay
*//***************************************************************************/
class FE_DL_EXPORT CountedRelay: public Counted
{
	public:
					CountedRelay(CoreRelay* pRelay):
#if FE_COUNTED_TRACK
							m_name("CountedRelay"),
#endif
							m_pRelay(pRelay)								{}

virtual				~CountedRelay(void)			{ delete m_pRelay; }

		CoreRelay*	relay(void)					{ return m_pRelay; }

#if FE_COUNTED_TRACK
const	String&		name(void) const			{ return m_name; }
	private:
		String		m_name;
#endif

	private:
		CoreRelay*	m_pRelay;
};


/**************************************************************************//**
	@brief Collection of accessors for a Record

	A Relay is somewhat like a C++ class with only virtual data members.

	A Relay can inherit attributes from one other Relay through the
	optional BASE template parameter.
	Any number of Relays can be chained together in this manner.

	Usage is usually done by deriving from Relay into a simple subclass:
	@code
	namespace
	{
	static const char* gs_myAttributes[]=
	{
		"F32",  "MyF32",
		"I32",  "MyI32"
	};
	}

	class MyRelay: public fe::Relay<2,MyRelay>
	{
		public:
			enum
			{
				e_myF32,
				e_myI32,
				e_size
			};
	static
	const   char*   layoutName(void)    { return "base"; }
	static
	const   char**  attributes(void)    { return gs_myAttributes; }
	};
	@endcode
	Because of the anonymous namespace, this can be defined as such in a
	shared header.

	The enumeration is purely for the convenience of users of your relay.
	The enumeration's entries should be aligned with the rows in the
	attribute array.
*//***************************************************************************/
template<int SIZE,typename TYPE,typename BASE=NoRelay>
class FE_DL_EXPORT Relay: public CoreRelay
{
	public:
		enum
		{
			e_totalSize=BASE::e_size+SIZE
		};

					Relay(void)	{ FEASSERT(TYPE::e_size==BASE::e_size+SIZE); }
virtual				~Relay(void)											{}

					/** @brief Bind all accessors to a given record

						A Relay can be rebound to different records,
						but caution is neccessary if you switch to a
						record in a different Scope. */
		WeakRecord	bind(WeakRecord record);

virtual	void		bind(sp<Scope> spScope);

					/// @brief Check if the bound Record supports the attribute
		BWORD		check(U32 index) const;

					/// @brief Verify that the attribute is the given type
					template<typename T>
		BWORD		checkType(U32 index) const;

					/// @brief Return a reference to the entry without checking
					template<typename T>
		T&			access(U32 index) const;

					/** @brief Return the value of an entry, if supported

						Since there is no function argument that uses the
						typename, you have to supply the template argument
						explicitly. */
					template<typename T>
		T			safeGet(U32 index) const;

					/** @brief Return the value of an entry, bypassing checks

						Since there is no function argument that uses the
						typename, you have to supply the template argument
						explicitly. */
					template<typename T>
		T			get(U32 index) const;

					/// @brief Set the value of an entry, if supported
					template<typename T>
		void		safeSet(U32 index,T value);

					/// @brief Set the value of an entry, bypassing checks
					template<typename T>
		void		set(U32 index,T value);

					/** @brief Returns a string representation of the
						entry's state */
		String		print(U32 index) const;

					/// @brief Logs all entries with state
		void		dump(void) const;

					/** @brief Create a new Record

						In addition to creating the record in the scope,
						this calls any Relay-based initialization. */
virtual	Record		createRecord(void);

					/// @brief (convenience) Create and bind a new Record
virtual	Record		createRecordAndBind(void)	{ return bind(createRecord()); }

					/// @brief Returns the currently bound scope
		sp<Scope>	scope(void)					{ return m_hpScope; }

					/** @brief Returns a Layout with the attributes of
						this relay */
		sp<Layout>	layout(void);

					/// @brief Returns the currently bound record
		WeakRecord&	record(void)	{ return m_record; }

					/** @brief Populate the given Layout with this Relay's
						attributes */
static	void		populate(sp<Layout>& rspLayout);

					/// @brief Returns the attribute's type
static
const	char*		typeAt(U32 index);

					/// @brief Returns the attribute's name
static
const	char*		nameAt(U32 index);

		void		initializeRecursively(Record& rRecord);

	protected:
					/** @brief Construct and insert a Component
						into the given slot

						The @em componentName is passed to Registry::create().
						*/
		void		createAndSetComponent(U32 index,String componentName);

					/** @brief Construct and insert a RecordGroup Component
						into the given slot */
		void		createAndSetRecordGroup(U32 index);

	private:
		void		prepare(void);

					/** @brief Create or extend a layout

						The layout is populated by attributes of this relay. */
		sp<Layout>	createLayout(void);

virtual	void		initialize(void)										{}

		WeakRecord		m_record;
mutable	Accessor<I32>	m_pAccessorArray[e_totalSize];
		hp<Scope>		m_hpScope;
		hp<Layout>		m_hpLayout;

#if	FE_RELAY_STORE_DEBUG
						// just to look at in the debugger
		String			m_debugCache[e_totalSize];
#endif
};

template<int SIZE,typename TYPE,typename BASE>
inline WeakRecord Relay<SIZE,TYPE,BASE>::bind(WeakRecord record)
{
	m_record=record;
#if	FE_RELAY_SAFE
	if(record.isValid())
	{
		sp<Scope> spScope=record.layout()->scope();
		if(m_hpScope!=spScope && spScope.isValid())
		{
			if(m_hpScope.isValid())
				feLog("Relay::bind Record changed scope\n");
			m_hpScope=spScope;
			prepare();
		}
	}
#else
	if(!m_hpScope.isValid())
	{
		m_hpScope=record.layout()->scope();
		prepare();
	}
#endif
	return m_record;
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::bind(sp<Scope> spScope)
{
	m_hpScope=spScope;
	layout();
}

template<int SIZE,typename TYPE,typename BASE>
inline BWORD Relay<SIZE,TYPE,BASE>::check(U32 index) const
{
	return m_pAccessorArray[index].check(m_record);
}

template<int SIZE,typename TYPE,typename BASE>
template<typename T>
inline BWORD Relay<SIZE,TYPE,BASE>::checkType(U32 index) const
{
	sp<Attribute> spAttribute=m_hpScope->findAttribute(nameAt(index));
	sp<BaseType> spBaseType=spAttribute->type();
	if(spBaseType->typeinfo().ref() != typeid(T))
	{
		feLog("Relay<>::checkType %s is %s not %s\n",
				nameAt(index),
				spBaseType->typeinfo().ref().name(),
				typeid(T).name());
#if	FE_RELAY_ASSERT
		FEASSERT(FALSE);
#endif
		return FALSE;
	}
	return TRUE;
}

template<int SIZE,typename TYPE,typename BASE>
template<typename T>
inline T& Relay<SIZE,TYPE,BASE>::access(U32 index) const
{
#if	FE_RELAY_SAFE
#if	FE_RELAY_DEBUG
	const_cast< Relay<SIZE,TYPE,BASE>* >(this)
			->m_debugCache[index]=print(index);
	checkType<T>(index);
#endif
	return *(((Accessor<T>*)&m_pAccessorArray[index])
			->queryAttribute(m_record));
#else
	return (*reinterpret_cast<Accessor<T>*>
			(&m_pAccessorArray[index]))(m_record);
#endif
}

template<int SIZE,typename TYPE,typename BASE>
template<typename T>
inline T Relay<SIZE,TYPE,BASE>::safeGet(U32 index) const
{
#if	FE_RELAY_SAFE && FE_RELAY_DEBUG
	checkType<T>(index);
#endif
	T* pValue=NULL;
	if(m_record.isValid() &&
			(pValue=(reinterpret_cast<Accessor<T>*>
			(&m_pAccessorArray[index]))->queryAttribute(m_record)))
	{
#if	FE_RELAY_DEBUG
		const_cast< Relay<SIZE,TYPE,BASE>* >(this)->m_debugCache[index]=
				print(index);
//		feLog("Relay<>::safeGet(%d) %d \"%s\"\n",index,*pValue,
//				m_debugCache[index].c_str());
//		dump();
#endif
		return *pValue;
	}

	return T(0);
}

template<int SIZE,typename TYPE,typename BASE>
template<typename T>
inline T Relay<SIZE,TYPE,BASE>::get(U32 index) const
{
#if	FE_RELAY_SAFE
#if	FE_RELAY_ASSERT
	if(!check(index))
	{
		feLog("Relay<>::get(%s) failed\n",nameAt(index));
		FEASSERT(FALSE);
	}
#endif

	return safeGet<T>(index);
#else
	return (*reinterpret_cast<Accessor<T>*>
			(&m_pAccessorArray[index]))(m_record);
#endif
}

template<int SIZE,typename TYPE,typename BASE>
template<typename T>
inline void Relay<SIZE,TYPE,BASE>::safeSet(U32 index,T value)
{
#if	FE_RELAY_SAFE && FE_RELAY_DEBUG
	checkType<T>(index);
#endif
	T* pValue=NULL;
	if(m_record.isValid() && (pValue=reinterpret_cast<T*>
			(m_pAccessorArray[index].queryAttribute(m_record))))
	{
		*pValue=value;
#if	FE_RELAY_DEBUG
		m_debugCache[index]=print(index);
//		feLog("Relay<>::safeSet(%d,%d) \"%s\"\n",index,value,
//				m_debugCache[index].c_str());
//		dump();
#endif
	}
}

template<int SIZE,typename TYPE,typename BASE>
template<typename T>
inline void Relay<SIZE,TYPE,BASE>::set(U32 index,T value)
{
#if	FE_RELAY_SAFE
#if	FE_RELAY_ASSERT
	if(!check(index))
	{
		feLog("Relay<>::set(%s) failed\n",nameAt(index));
		FEASSERT(FALSE);
	}
#endif
	safeSet(index,value);
#else
	(*reinterpret_cast<Accessor<T>*>
			(&m_pAccessorArray[index]))(m_record)=value;
#endif
}

template<int SIZE,typename TYPE,typename BASE>
inline String Relay<SIZE,TYPE,BASE>::print(U32 index) const
{
	if(!m_hpScope.isValid())
		return "<no scope>";
	if(!check(index))
		return "<absent>";
	void* pVoid=(void*)static_cast< Accessor<I32> >
			(m_pAccessorArray[index]).queryAttribute(m_record);
	sp<Attribute> spAttribute=m_hpScope->findAttribute(nameAt(index));
	sp<BaseType> spBaseType=spAttribute->type();
	sp<BaseType::Info> spInfo=spBaseType->getInfo();
	return spInfo->print(pVoid);
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::dump(void) const
{
	for(U32 index=0;index<e_totalSize;index++)
		feLog("%2d %-16s %-16s %s\n",index,typeAt(index),
				nameAt(index),print(index).c_str());
}

template<int SIZE,typename TYPE,typename BASE>
inline Record Relay<SIZE,TYPE,BASE>::createRecord(void)
{
	if(!m_hpScope.isValid())
		feX("Relay::createRecord","no scope bound (%s)\n",TYPE::layoutName());
	Record record=m_hpScope->createRecord(layout());

	TYPE relay;
	relay.initializeRecursively(record);
	return record;
}

template<int SIZE,typename TYPE,typename BASE>
inline sp<Layout> Relay<SIZE,TYPE,BASE>::layout(void)
{
	if(!m_hpScope.isValid())
		return sp<Layout>(NULL);
	if(!m_hpLayout.isValid())
		m_hpLayout=createLayout();
	return m_hpLayout;
}

template<int SIZE,typename TYPE,typename BASE>
inline const char* Relay<SIZE,TYPE,BASE>::typeAt(U32 index)
{
	if(index<BASE::e_size)
		return BASE::typeAt(index);
	else
		return TYPE::attributes()[(index-BASE::e_size)*2];
}

template<int SIZE,typename TYPE,typename BASE>
inline const char* Relay<SIZE,TYPE,BASE>::nameAt(U32 index)
{
	if(index<BASE::e_size)
		return BASE::nameAt(index);
	else
		return TYPE::attributes()[(index-BASE::e_size)*2+1];
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::initializeRecursively(Record& rRecord)
{
	BASE base;
	base.initializeRecursively(rRecord);
	bind(rRecord);
	initialize();
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::createAndSetComponent(U32 index,
		String componentName)
{
	sp<Component>& rspComponent=access< sp<Component> >(index);
	rspComponent=scope()->registry()->create(componentName);
	FEASSERT(rspComponent.isValid());
	rspComponent->setName(nameAt(index));
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::createAndSetRecordGroup(U32 index)
{
	sp<RecordGroup>& rspRecordGroup=access< sp<RecordGroup> >(index);
	rspRecordGroup=new RecordGroup();
	FEASSERT(rspRecordGroup.isValid());
//	rspRecordGroup->setName(nameAt(index));
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::prepare(void)
{
	if(!m_hpScope.isValid())
		return;
	for(U32 index=0;index<e_totalSize;index++)
		m_pAccessorArray[index].initialize(m_hpScope,nameAt(index));
}

template<int SIZE,typename TYPE,typename BASE>
inline void Relay<SIZE,TYPE,BASE>::populate(sp<Layout>& rspLayout)
{
	if(!rspLayout.isValid())
		feLog("Relay::populate layout not valid\n");

	sp<Scope> spScope=rspLayout->scope();
	for(U32 index=0;index<e_totalSize;index++)
	{
//		feLog("support %s %s\n",typeAt(index),nameAt(index));
		spScope->support(nameAt(index),typeAt(index));
	}

	//* TODO should we check for repeats (psuedo virtual base)
	for(U32 index=0;index<e_totalSize;index++)
		rspLayout->populate(nameAt(index));
}

template<int SIZE,typename TYPE,typename BASE>
inline sp<Layout> Relay<SIZE,TYPE,BASE>::createLayout(void)
{
	if(!m_hpScope.isValid())
		feLog("Relay::createLayout scope not valid\n");

	sp<Layout> spLayout=m_hpScope->declare(TYPE::layoutName());

	populate(spLayout);
	prepare();
	return spLayout;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_Relay_h__ */
