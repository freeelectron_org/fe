/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DataIOController.h"

namespace fe
{
namespace ext
{

DataIOController::DataIOController(void)
{
}

DataIOController::~DataIOController(void)
{
}

void DataIOController::initialize(void)
{
	maskDefault("fe_io_load",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)108 /* 'l' */,
							WindowEvent::e_statePress));

	maskDefault("fe_io_save",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)115 /* 's' */,
							WindowEvent::e_statePress));

	cfg<String>("file") = "data_io_controller_io.fea";
	cfg< sp<Component> >("scope");
}

void DataIOController::handle(Record &r_sig)
{
	WindowEvent::Mask maskLoad = maskGet("fe_io_load");
	WindowEvent::Mask maskSave = maskGet("fe_io_save");

	WindowEvent wev;
	wev.bind(r_sig);

	m_spScope = cfg< sp<Component> >("scope");

	String filename = cfg<String>("file");
	if(wev.is(maskSave))
	{
		sp<RecordGroup> rg_out = cfg<sp<RecordGroup> >("output");	// record group to save

		if(!rg_out.isValid())
		{
			feLog("DataIOController : output record group not valid");
			return;
		}

		if(!m_spScope.isValid())
		{
			feLog("DataIOController : i/o scope not set");
			return;
		}

		std::ofstream outfile(filename.c_str());
		sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));

		spStream->output(outfile, rg_out);
	}

	if(wev.is(maskLoad))
	{
		sp<RecordGroup> &rg_in = cfg<sp<RecordGroup> >("input");	// record group to load into

		if(!rg_in.isValid())
		{
			feLog("DataIOController : input record group not valid");
			return;
		}

		if(!m_spScope.isValid())
		{
			feLog("DataIOController : i/o scope not set");
			return;
		}

		std::ifstream infile(filename.c_str());
		sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));

		rg_in = spStream->input(infile);
	}
}

} /* namespace ext */
} /* namespace fe */



