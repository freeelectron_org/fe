/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

#define FE_OBLD_DEBUG			FALSE
#define FE_OBLD_VERBOSE			FALSE

using namespace fe;
using namespace fe::ext;

void BladeOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	//* NOTE inherits attributes from SurfaceBindOp

	catalogRemove("Metric");
	catalogRemove("Covert");
	catalogRemove("Conform");
	catalogRemove("LimitDistance");
	catalogRemove("MaxDistance");
	catalogRemove("PartitionDriver");
	catalogRemove("InputPartitionAttr");
	catalogRemove("DriverPartitionAttr");
	catalogRemove("DriverPartitionAttr");

	//* Page Config

	catalog<String>("Threads","page")="Config";

	catalog<String>("AutoThread","page")="Config";

	catalog<String>("Paging","page")="Config";

	catalog<String>("StayAlive","page")="Config";

	catalog<String>("TangentAttr")="tangent";
	catalog<String>("TangentAttr","label")="Tangent Attribute";
	catalog<String>("TangentAttr","page")="Config";
	catalog<String>("TangentAttr","hint")=
			"Name of vector primitive attribute on input curves"
			" describing the direction along which rotations can occur.";

	catalog<String>("FacingAttr")="N";
	catalog<String>("FacingAttr","label")="Facing Attribute";
	catalog<String>("FacingAttr","page")="Config";
	catalog<String>("FacingAttr","hint")=
			"Name of vector point attribute added to the input curves"
			" describing the facing side."
			"  These may get rotated in the output.";

	catalog<String>("ForceAttr")="force";
	catalog<String>("ForceAttr","label")="Force Attribute";
	catalog<String>("ForceAttr","page")="Config";
	catalog<String>("ForceAttr","hint")=
			"Name of vector point attribute on input curves"
			" describing an external force.";

	catalog<String>("VelocityAttr")="v";
	catalog<String>("VelocityAttr","label")="Velocity Attribute";
	catalog<String>("VelocityAttr","page")="Config";
	catalog<String>("VelocityAttr","hint")=
			"Name of vector point attribute on input curves"
			" describing relative air movement."
			"  Velocity values should produce a better wind response than"
			" an external force since the solver can account for facing area.";

	//* Page Physics

	catalog<Real>("Tension")=1e4;
	catalog<Real>("Tension","high")=1e5;
	catalog<Real>("Tension","max")=1e6;
	catalog<String>("Tension","label")="Spring Tension";
	catalog<String>("Tension","page")="Physics";
	catalog<bool>("Tension","joined")=true;
	catalog<String>("Tension","hint")="Angular springiness.";

	catalog<Real>("TensionRandom")=0.0;
	catalog<Real>("TensionRandom","high")=1e4;
	catalog<Real>("TensionRandom","max")=1e6;
	catalog<String>("TensionRandom","page")="Physics";
	catalog<String>("TensionRandom","label")="+-";

	catalog<Real>("Drag")=1e2;
	catalog<Real>("Drag","high")=1e3;
	catalog<Real>("Drag","max")=1e6;
	catalog<String>("Drag","label")="Angular Drag";
	catalog<String>("Drag","page")="Physics";
	catalog<bool>("Drag","joined")=true;
	catalog<String>("Drag","hint")="Angular sluggishness.";

	catalog<Real>("DragRandom")=0.0;
	catalog<Real>("DragRandom","high")=1e3;
	catalog<Real>("DragRandom","max")=1e6;
	catalog<String>("DragRandom","page")="Physics";
	catalog<String>("DragRandom","label")="+-";

	catalog<Real>("Mass")=1e0;
	catalog<Real>("Mass","high")=1e1;
	catalog<String>("Mass","page")="Physics";
	catalog<String>("Mass","hint")="Physical presence per vertex point.";

	catalog<Real>("Viscosity")=1e0;
	catalog<Real>("Viscosity","high")=1e1;
	catalog<String>("Viscosity","page")="Physics";
	catalog<String>("Viscosity","hint")="Effectiveness of air velocity.";

	catalog<Real>("Restoration")=0.0;
	catalog<Real>("Restoration","high")=1e5;
	catalog<String>("Restoration","page")="Physics";
	catalog<bool>("Restoration","joined")=true;

	catalog<Real>("RestorationRandom")=0.0;
	catalog<Real>("RestorationRandom","high")=1e4;
	catalog<String>("RestorationRandom","page")="Physics";
	catalog<String>("RestorationRandom","label")="+-";

	catalog<Real>("TimeStep")=1.0/60.0;
	catalog<String>("TimeStep","label")="Time Step";
	catalog<Real>("TimeStep","high")=1e-1;
	catalog<String>("TimeStep","page")="Physics";

	catalog<Real>("Wind")=0.0;
	catalog<String>("Wind","label")="Test Wind";
	catalog<Real>("Wind","high")=1e3;
	catalog<String>("Wind","page")="Physics";

	catalog<Real>("Turbulence")=0.0;
	catalog<String>("Turbulence","label")="Test Turbulence";
	catalog<Real>("Turbulence","high")=1e3;
	catalog<String>("Turbulence","page")="Physics";

	catalog<Real>("Bend")=0.0;
	catalog<String>("Bend","label")="Added Bend";
	catalog<Real>("Bend","high")=1e0;
	catalog<String>("Bend","page")="Physics";
	catalog<String>("Bend","hint")=
			"Flatness curvature.  This is uniformly added to all rest angles.";

#if FE_CODEGEN>FE_DEBUG
	catalog<bool>("Wind","hidden")=true;
	catalog<bool>("Turbulence","hidden")=true;
	catalog<bool>("Bend","hidden")=true;
#endif

	//* Page Driver

	catalog<bool>("DriverBound")=false;
	catalog<String>("DriverBound","label")="Driver Bound";
	catalog<String>("DriverBound","page")="Driver";
	catalog<String>("DriverBound","hint")=
			"This option requires the reference Driver Surface."
			"  An optional animated Deformed Surface should match"
			" the topology of the Driver Surface.";

	catalogMoveToEnd("DriverGroup");
	catalog<String>("DriverGroup","enabler")="DriverBound";
	catalog<String>("DriverGroup","suggest")="primitiveGroups";
	catalog<String>("DriverGroup","page")="Driver";

	catalogMoveToEnd("Bindings");
	catalog<I32>("Bindings","max")=1;
	catalog<String>("Bindings","enabler")="DriverBound";
	catalog<String>("Bindings","page")="Driver";
	catalog<bool>("Bindings","hidden")=true;

	catalogMoveToEnd("Bind Primitives");
	catalog<bool>("Bind Primitives")=true;
	catalog<String>("Bind Primitives","enabler")="DriverBound";
	catalog<String>("Bind Primitives","page")="Driver";
	catalog<bool>("Bind Primitives","hidden")=true;

	catalogMoveToEnd("Bind Fragments");
	catalog<bool>("Bind Fragments")=false;
	catalog<String>("Bind Fragments","enabler")="DriverBound";
	catalog<String>("Bind Fragments","page")="Driver";
	catalog<bool>("Bind Fragments","hidden")=true;

	catalogMoveToEnd("BindPrefix");
	catalog<String>("BindPrefix","enabler")="DriverBound";
	catalog<String>("BindPrefix","page")="Driver";
	catalog<bool>("BindPrefix","joined")=false;

	catalogMoveToEnd("PivotPoint");
	catalog<String>("PivotPoint","enabler")="DriverBound";
	catalog<String>("PivotPoint","page")="Driver";
	catalog<bool>("PivotPoint","hidden")=true;

	catalogMoveToEnd("FragmentAttr");
	catalog<String>("FragmentAttr","enabler")="DriverBound";
	catalog<String>("FragmentAttr","page")="Driver";
	catalog<bool>("FragmentAttr","hidden")=true;

	//* Page Collision

	catalog<String>("CollisionMethod")="RayCastHalvsies";
	catalog<String>("CollisionMethod","label")="Collision Method";
	catalog<String>("CollisionMethod","choice:0")="BoundingSphere";
	catalog<String>("CollisionMethod","label:0")=
			"Bounding Sphere (vertex collisions only)";
	catalog<String>("CollisionMethod","choice:1")="NearestPoint";
	catalog<String>("CollisionMethod","label:1")="Nearest Point";
	catalog<String>("CollisionMethod","choice:2")="RayCastOneStep";
	catalog<String>("CollisionMethod","label:2")="Ray Cast - One Step";
	catalog<String>("CollisionMethod","choice:3")="RayCastHalvsies";
	catalog<String>("CollisionMethod","label:3")="Ray Cast - Halvsies";
	catalog<String>("CollisionMethod","page")="Collision";

	catalog<I32>("HalfingSteps")=8;
	catalog<I32>("HalfingSteps","high")=32;
	catalog<String>("HalfingSteps","label")="Halfing Steps";
	catalog<String>("HalfingSteps","page")="Collision";
	catalog<String>("HalfingSteps","hint")=
			"Iterations in the Halvsies Collision Method.";

	catalog<Real>("Collision Gap")=0.0;
	catalog<String>("Collision Gap","page")="Collision";
	catalog<String>("Collision Gap","hint")=
			"Imposed extra distance from the collider.";

	catalog<I32>("Refinement")=0;
	catalog<I32>("Refinement","high")=4;
	catalog<I32>("Refinement","max")=8;
	catalog<String>("Refinement","page")="Collision";
	catalog<String>("Refinement","hint")=
			"Maximum level of collider subdivision."
			"  Refinement may be adaptive and non-uniform.";

	catalog<bool>("Grabbing")=false;
	catalog<String>("Grabbing","page")="Collision";
	catalog<bool>("Grabbing","joined")=true;

	catalog<Real>("GrabBias")=0.0;
	catalog<Real>("GrabBias","min")= -1.0;
	catalog<Real>("GrabBias","max")=1.0;
	catalog<String>("GrabBias","label")="Grab Bias";
	catalog<String>("GrabBias","enabler")="Grabbing";
	catalog<String>("GrabBias","page")="Collision";
	catalog<bool>("GrabBias","joined")=true;
	catalog<String>("GrabBias","hint")=
			"Tunes how easily the blade tips stick to the collider.";

	catalog<Real>("GrabBiasRandom")=0.0;
	catalog<Real>("GrabBiasRandom","high")=1.0;
	catalog<String>("GrabBiasRandom","enabler")="Grabbing";
	catalog<String>("GrabBiasRandom","page")="Collision";
	catalog<String>("GrabBiasRandom","label")="+-";

	catalog<bool>("DebugSurface")=false;
	catalog<String>("DebugSurface","label")="Debug Surface";
	catalog<String>("DebugSurface","page")="Collision";
	catalog<String>("DebugSurface","hint")=
			"Draw lines over the output showing the"
			" collider surface refinement."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<bool>("DebugCollisions")=false;
	catalog<String>("DebugCollisions","label")="Debug Collisions";
	catalog<String>("DebugCollisions","page")="Collision";
	catalog<String>("DebugCollisions","hint")=
			"Add lines to the output detailing the collisions."
			"Draw lines over the output detailing the collisions."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	//* Page Buckling

	catalog<bool>("Buckling")=false;
	catalog<String>("Buckling","page")="Buckling";

	catalog<Real>("BreakAngle")=1.0;
	catalog<Real>("BreakAngle","min")=0.0;
	catalog<Real>("BreakAngle","max")=2.0;
	catalog<String>("BreakAngle","label")="Break Angle";
	catalog<String>("BreakAngle","enabler")="Buckling";
	catalog<String>("BreakAngle","page")="Buckling";
	catalog<bool>("BreakAngle","joined")=true;
	catalog<String>("BreakAngle","hint")=
			"Threshold above which buckling will occur.";

	catalog<Real>("BreakAngleRandom")=0.0;
	catalog<Real>("BreakAngleRandom","high")=1.0;
	catalog<String>("BreakAngleRandom","enabler")="Buckling";
	catalog<String>("BreakAngleRandom","page")="Buckling";
	catalog<String>("BreakAngleRandom","label")="+-";

	catalog<Real>("ElasticAngle")=0.1;
	catalog<Real>("ElasticAngle","min")=0.0;
	catalog<Real>("ElasticAngle","max")=2.0;
	catalog<String>("ElasticAngle","label")="Elastic Angle";
	catalog<String>("ElasticAngle","enabler")="Buckling";
	catalog<String>("ElasticAngle","page")="Buckling";
	catalog<bool>("ElasticAngle","joined")=true;
	catalog<String>("ElasticAngle","hint")=
			"Subtracted from Break Angle when reseting dynamic rest angle.";

	catalog<Real>("ElasticAngleRandom")=0.0;
	catalog<Real>("ElasticAngleRandom","high")=1.0;
	catalog<String>("ElasticAngleRandom","enabler")="Buckling";
	catalog<String>("ElasticAngleRandom","page")="Buckling";
	catalog<String>("ElasticAngleRandom","label")="+-";

	catalog<Real>("PopAngle")=0.8;
	catalog<Real>("PopAngle","min")=0.0;
	catalog<Real>("PopAngle","max")=2.0;
	catalog<String>("PopAngle","label")="Pop Angle";
	catalog<String>("PopAngle","enabler")="Buckling";
	catalog<String>("PopAngle","page")="Buckling";
	catalog<bool>("PopAngle","joined")=true;
	catalog<String>("PopAngle","hint")=
			"Threshold below which buckling is fully recovered.";

	catalog<Real>("PopAngleRandom")=0.0;
	catalog<Real>("PopAngleRandom","high")=1.0;
	catalog<String>("PopAngleRandom","enabler")="Buckling";
	catalog<String>("PopAngleRandom","page")="Buckling";
	catalog<String>("PopAngleRandom","label")="+-";

	catalog<Real>("RecoveryAngle")=0.01;
	catalog<Real>("RecoveryAngle","min")=0.0;
	catalog<Real>("RecoveryAngle","max")=0.1;
	catalog<String>("RecoveryAngle","label")="Recovery Angle";
	catalog<String>("RecoveryAngle","enabler")="Buckling";
	catalog<String>("RecoveryAngle","page")="Buckling";
	catalog<bool>("RecoveryAngle","joined")=true;
	catalog<String>("RecoveryAngle","hint")=
			"Incremental movement of dynamic rest angle towards"
			" original rest angle.";

	catalog<Real>("RecoveryAngleRandom")=0.0;
	catalog<Real>("RecoveryAngleRandom","high")=0.1;
	catalog<String>("RecoveryAngleRandom","enabler")="Buckling";
	catalog<String>("RecoveryAngleRandom","page")="Buckling";
	catalog<String>("RecoveryAngleRandom","label")="+-";

	catalog<String>("Input Surface","label")="Input Curves";

	catalog< sp<Component> >("Collider Surface");
	catalog<bool>("Collider Surface","optional")=true;

	catalogMoveToEnd("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog< sp<Component> >("Deformed Surface");
	catalog<String>("Deformed Surface","label")="Deformed Driver Surface";
	catalog<bool>("Deformed Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","label")="Output Curves";
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;
	catalog<bool>("Output Surface","temporal")=true;

//	catalogDump();

	m_spDebugGroup=new DrawMode();
	m_spDebugGroup->setGroup("debug");
}

void BladeOp::handle(Record& a_rSignal)
{
	const I32 frame=currentFrame(a_rSignal);
	const BWORD replaced=catalog<bool>("Output Surface","replaced");

#if FE_OBLD_DEBUG
	feLog("BladeOp::handle frame %d replaced %d\n",frame,replaced);
#endif

	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	sp<SurfaceI> spCollider;
	access(spCollider,"Collider Surface",e_warning);

	sp<SurfaceI> spDriver;
	sp<SurfaceI> spDeformed;
	const BWORD driverBound=catalog<bool>("DriverBound");
	if(driverBound)
	{
		if(!access(spDriver,"Driver Surface")) return;

		spDriver->setRefinement(0);
		spDriver->center();

		if(access(spDeformed,"Deformed Surface",e_quiet))
		{
			spDeformed->setRefinement(0);
			spDeformed->center();
		}
		else
		{
			spDeformed=spDriver;
		}
	}
	else
	{
		spDriver=NULL;
		spDeformed=NULL;
	}

	const I32 threads=catalog<I32>("Threads");
	const Real collisionGap=catalog<Real>("Collision Gap");
	m_blade.setCollisionGap(collisionGap);

	if(spCollider.isValid())
	{
		const I32 refinement=catalog<I32>("Refinement");
		spCollider->setRefinement(refinement);

		if(threads>1 && refinement>0)
		{
			catalog<String>("warning")+=
					"multi-threaded adaptive refinement is poorly supported; ";
		}

		if(collisionGap<0.1)
		{
			catalog<String>("warning")+=
					"small collision gap may provoke jumpy collisions; ";
		}
	}

	const BWORD debugSurface=catalog<bool>("DebugSurface");
	const BWORD debugCollisions=catalog<bool>("DebugCollisions");
	const BWORD recycle=catalog<bool>("Output Surface","recycle");
	const BWORD wantDebug=
			((debugSurface || debugCollisions) && threads==1);

	sp<DrawI> spDrawDebug;
	if(wantDebug && !recycle && !accessGuide(spDrawDebug,a_rSignal)) return;

	if(recycle==wantDebug)
	{
		catalog<bool>("Output Surface","recycle")=(!wantDebug);
	}

	m_blade.setDrawDebug(debugCollisions? spDrawDebug: sp<DrawI>(NULL));

	m_blade.setCollider(spCollider);
	m_blade.setDriver(spDriver);
	m_blade.setDeformed(spDeformed);

	const String tangentName=catalog<String>("TangentAttr");
	const String facingName=catalog<String>("FacingAttr");
	const String forceName=catalog<String>("ForceAttr");
	const String velocityName=catalog<String>("VelocityAttr");

	m_blade.setTension(catalog<Real>("Tension"));
	m_blade.setTensionRandom(catalog<Real>("TensionRandom"));
	m_blade.setDrag(catalog<Real>("Drag"));
	m_blade.setDragRandom(catalog<Real>("DragRandom"));
	m_blade.setMass(catalog<Real>("Mass"));
	m_blade.setRestoration(catalog<Real>("Restoration"));
	m_blade.setRestorationRandom(catalog<Real>("RestorationRandom"));
	m_blade.setBend(catalog<Real>("Bend"));
	m_blade.setGrabbing(catalog<bool>("Grabbing"));
	m_blade.setGrabBias(catalog<Real>("GrabBias"));
	m_blade.setGrabBiasRandom(catalog<Real>("GrabBiasRandom"));
	m_blade.setWind(catalog<Real>("Wind"));
	m_blade.setTurbulence(catalog<Real>("Turbulence"));
	m_blade.setBuckling(catalog<bool>("Buckling"));
	m_blade.setBreakAngle(catalog<Real>("BreakAngle"));
	m_blade.setBreakAngleRandom(catalog<Real>("BreakAngleRandom"));
	m_blade.setElasticAngle(catalog<Real>("ElasticAngle"));
	m_blade.setElasticAngleRandom(catalog<Real>("ElasticAngleRandom"));
	m_blade.setPopAngle(catalog<Real>("PopAngle"));
	m_blade.setPopAngleRandom(catalog<Real>("PopAngleRandom"));
	m_blade.setRecoveryAngle(catalog<Real>("RecoveryAngle"));
	m_blade.setRecoveryAngleRandom(catalog<Real>("RecoveryAngleRandom"));
	m_blade.setHalfingSteps(catalog<I32>("HalfingSteps"));

	const String collisionString=catalog<String>("CollisionMethod");
	Blade::CollisionMethod collisionMethod=Blade::e_boundingSphere;
	if(collisionString=="NearestPoint")
	{
		collisionMethod=Blade::e_nearestPoint;
	}
	else if(collisionString=="RayCastOneStep")
	{
		collisionMethod=Blade::e_rayCastOneStep;
	}
	else if(collisionString=="RayCastHalvsies")
	{
		collisionMethod=Blade::e_rayCastHalvsies;
	}
	m_blade.setCollisionMethod(collisionMethod);

	const Real timeStep=catalog<Real>("TimeStep");
	m_blade.setTimeStep(timeStep);

	static Real t=0.0;	// HACK
	t+=timeStep;
	m_blade.setTime(t);

	m_blade.populate();

	sp<SurfaceAccessorI> spPrimitiveTangent;
	access(spPrimitiveTangent,m_spOutputAccessible,
			e_primitive,tangentName,e_quiet,e_refuseMissing);

	sp<SurfaceAccessorI> spPointFacing;
	access(spPointFacing,m_spOutputAccessible,
			e_point,facingName,e_quiet,e_refuseMissing);

	sp<SurfaceAccessorI> spPointForce;
	access(spPointForce,m_spOutputAccessible,
			e_point,forceName,e_quiet,e_refuseMissing);

	sp<SurfaceAccessorI> spPointVelocity;
	access(spPointVelocity,m_spOutputAccessible,
			e_point,velocityName,e_quiet,e_refuseMissing);

	sp<SurfaceAccessorI> spInputPosition;
	if(!access(spInputPosition,"Input Surface",
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spPrimitiveVertices;
	if(!access(spPrimitiveVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	const U32 pointCount=spInputPosition->count();
	const U32 primitiveCount=spPrimitiveVertices->count();
	const U32 subCount=primitiveCount? pointCount/primitiveCount: 0;

	if(pointCount!=primitiveCount*5)
	{
		catalog<String>("warning")+=
				"exactly five vertices per primitive expected; ";
	}

	Array<SpatialVector>& rTangent=m_blade.tangent();
	Array<SpatialVector>& rFacing=m_blade.facing();
	Array<SpatialVector>& rForceIn=m_blade.forceIn();
	Array<SpatialVector>& rVelocityIn=m_blade.velocityIn();
	Array<SpatialVector>& rLocationRef=m_blade.locationRef();
	Array<Real>& rVelocity=m_blade.velocity();
	Array<I32>& rBindFace=m_blade.bindFace();
	Array<SpatialBary>& rBindBary=m_blade.bindBary();

	//* inferred reset
	const BWORD reuseCache=(frame==m_frame+1 || (frame==m_frame && !replaced));

#if FE_OBLD_VERBOSE
	if(reuseCache)
	{
		if(frame==m_frame+1)
		{
			feLog("BladeOp::handle temporal step\n");
		}
		else
		{
			feLog("BladeOp::handle temporal repeat\n");
		}
	}
#endif

	m_frame=frame;

	if(!reuseCache)
	{
#if FE_OBLD_VERBOSE
		feLog("BladeOp::handle reset cache\n");
#endif
		rLocationRef.resize(pointCount);
		for(U32 m=0;m<pointCount;m++)
		{
			rLocationRef[m]=spInputPosition->spatialVector(m,0);
		}

		const U32 pointCountV=subCount? pointCount*(subCount-1)/subCount: 0;
		rVelocity.resize(pointCountV);
		for(U32 m=0;m<pointCountV;m++)
		{
			rVelocity[m]=0.0;
		}

		if(spPointFacing.isValid() &&
				spPointFacing->count() &&
				spPointFacing->subCount(0))
		{
			rFacing.resize(pointCount);
			for(U32 m=0;m<pointCount;m++)
			{
				rFacing[m]=spPointFacing->spatialVector(m,0);
			}
		}
		else
		{
			rFacing.resize(0);
			if(!facingName.empty())
			{
				catalog<String>("message")="facing attribute \"" + facingName +
						"\" is missing; ";
			}
		}

		const U32 pointCountT=subCount? pointCount/subCount: 0;
		rTangent.resize(pointCountT);
		if(spPrimitiveTangent.isValid() &&
				spPrimitiveTangent->count() && spPrimitiveTangent->subCount(0))
		{
			for(U32 m=0;m<pointCountT;m++)
			{
				rTangent[m]=spPrimitiveTangent->spatialVector(m,0);
			}
		}
		else
		{
			if(!tangentName.empty())
			{
				catalog<String>("warning")+="tangent attribute \"" +
						tangentName + "\" is missing; ";
			}

			//* HACK
			for(U32 m=0;m<pointCountT;m++)
			{
#if FALSE
				const Real angle=sin(0.7*m)+sin(0.3*m);

				set(rTangent[m],cos(angle),0.0,sin(angle));
#else
				//* NOTE presuming no driver
				SpatialVector ground=rLocationRef[m*5+4]-rLocationRef[m*5];
				ground[1]=0;
				rTangent[m]=unitSafe(ground);
#endif
			}
		}

		if(driverBound)
		{
			rBindFace.resize(pointCountT);
			rBindBary.resize(pointCountT);
			Array< sp<SurfaceAccessorI> > faces;
			Array< sp<SurfaceAccessorI> > barys;
			const I32 bindings=accessBindings(m_spOutputAccessible,
					faces,barys,1,"",FALSE,FALSE);
			if(bindings<1)
			{
				catalog<String>("warning")+="binding data is missing; ";
			}

			if(bindings>0 && faces[0].isValid() &&
					barys[0].isValid())
			{
				for(U32 m=0;m<pointCountT;m++)
				{
					rBindFace[m]=faces[0]->integer(m,0);
					rBindBary[m]=barys[0]->spatialVector(m,0);
				}
			}
			else
			{
				if(bindings)
				{
					catalog<String>("warning")+="binding data is incomplete; ";
				}

				for(U32 m=0;m<pointCountT;m++)
				{
					rBindFace[m]= -1;
					set(rBindBary[m]);
				}
			}
		}
		else
		{
			rBindFace.resize(0);
			rBindBary.resize(0);
		}

		for(U32 m=0;m<pointCountT;m++)
		{
			m_blade.storeAsRest(m);
		}
	}

	if(spPointForce.isValid() &&
			spPointForce->count() &&
			spPointForce->subCount(0))
	{
		rForceIn.resize(pointCount);
		for(U32 m=0;m<pointCount;m++)
		{
			rForceIn[m]=spPointForce->spatialVector(m,0);
		}
	}
	else
	{
		rForceIn.resize(0);
		if(!forceName.empty())
		{
			catalog<String>("message")+="force attribute \"" + forceName +
					"\" is missing; ";
		}
	}

	if(spPointVelocity.isValid() &&
			spPointVelocity->count() &&
			spPointVelocity->subCount(0))
	{
		rVelocityIn.resize(pointCount);
		const Real viscosity=catalog<Real>("Viscosity");
		for(U32 m=0;m<pointCount;m++)
		{
			rVelocityIn[m]=viscosity*spPointVelocity->spatialVector(m,0);
		}
	}
	else
	{
		rVelocityIn.resize(0);
		if(!velocityName.empty())
		{
			catalog<String>("message")+="velocity attribute \"" +
					velocityName + "\" is missing; ";
		}
	}

	if(spDrawDebug.isValid())
	{
		spDrawDebug->pushDrawMode(m_spDebugGroup);
	}

	setAtomicRange(m_spOutputAccessible,e_pointsOfPrimitives);
	OperatorThreaded::handle(a_rSignal);

	if(spDrawDebug.isValid())
	{
		if(debugSurface)
		{
			sp<DrawableI> spDrawable=spCollider;
			if(spDrawable.isValid())
			{
				spDrawable->draw(spDrawDebug,NULL);
			}
		}

		spDrawDebug->popDrawMode();
	}

#if FE_OBLD_DEBUG
	feLog("BladeOp::handle done\n");
#endif
}

void BladeOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
	const String facingName=catalog<String>("FacingAttr");

	if(m_spOutputAccessible.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spPointFacing;
	access(spPointFacing,m_spOutputAccessible,
			e_point,facingName,e_quiet,e_refuseMissing);

	sp<SurfaceAccessorI> spOutputPosition;
	if(!access(spOutputPosition,m_spOutputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spPrimitiveVertices;
	if(!access(spPrimitiveVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	const U32 pointCount=spOutputPosition->count();
	const U32 primitiveCount=spPrimitiveVertices->count();
	const U32 subCount=primitiveCount? pointCount/primitiveCount: 0;

	Array<SpatialVector>& rLocationDef=m_blade.locationDef();
	Array<SpatialVector>& rFacing=m_blade.facing();

#if FE_OBLD_VERBOSE
	feLog("BladeOp::run running thread %d %s\n",
			a_id,a_spRange->brief().c_str());
#endif

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 index=it.value();
		m_blade.step(index);

		const U32 startSub=index*subCount;
		const U32 endSub=startSub+subCount;
		for(U32 indexSub=startSub;indexSub<endSub;indexSub++)
		{
			spOutputPosition->set(indexSub,0,rLocationDef[indexSub]);
		}
		if(spPointFacing.isValid() &&
				spPointFacing->count() &&
				spPointFacing->subCount(0))
		{
			for(U32 indexSub=startSub;indexSub<endSub;indexSub++)
			{
				spPointFacing->set(indexSub,0,rFacing[indexSub]);
			}
		}
	}

#if FE_OBLD_VERBOSE
	feLog("BladeOp::run done running thread %d\n",a_id);
#endif
}
