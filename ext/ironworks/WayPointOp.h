/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ironworks_WayPointOp_h__
#define __ironworks_WayPointOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to append triangle locators

	@ingroup ironworks
*//***************************************************************************/
class FE_DL_EXPORT WayPointOp:
	public OperatorSurfaceCommon,
	public Initialize<WayPointOp>
{
	public:

					WayPointOp(void);
virtual				~WayPointOp(void);

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

		void		setAnchor(void);
		void		getAnchor(void);

		String		scanPeers(sp<DrawI>& a_rspDrawOverlay,
							const SpatialVector& a_cameraPos,
							const SpatialVector& a_cameraDir,
							BWORD a_draw,BWORD a_labels,
							I32 a_mouseX,I32 a_mouseY);

		sp<ManipulatorI>	m_spManipulator;
		sp<SurfaceI>		m_spDriver;

		WindowEvent			m_event;
		sp<DrawMode>		m_spSolid;
		sp<DrawMode>		m_spOverlay;

		SpatialTransform	m_locator;
		SpatialTransform	m_pivot;
		SpatialTransform	m_transform;

		Real				m_shareFrame;
		SpatialTransform	m_shareTransform;
		Real				m_shareBlend;

		Real				m_lastFrame;
		I32					m_picking;
		BWORD				m_dragging;
		BWORD				m_brushed;
		String				m_changing;

		I32					m_pickFace;
		SpatialBary			m_pickBary;
		String				m_highlightNode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ironworks_WayPointOp_h__ */

