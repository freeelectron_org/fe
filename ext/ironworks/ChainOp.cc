/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ironworks/ironworks.pmh>

#define	FE_CHAIN_DEBUG			FALSE
#define	FE_CHAIN_DEBUG_SCAN		FALSE

#define	FE_CHAIN_VERBOSE		FALSE

using namespace fe;
using namespace fe::ext;

void ChainOp::initialize(void)
{
	catalog<String>("label")="Iron ChainOp";

	//* Curve
	catalog<bool>("measureDistance")=false;
	catalog<String>("measureDistance","label")="Measure By Distance";
	catalog<String>("measureDistance","page")="Curve";
	catalog<String>("measureDistance","hint")=
			"Specify start and end points in world units"
			" instead of by parametric coordinate.";

	catalog<Real>("start")=0.0;
	catalog<Real>("start","low")= -1.0;
	catalog<Real>("start","high")=2.0;
	catalog<Real>("start","min")= -1e9;
	catalog<Real>("start","max")=1e9;
	catalog<String>("start","label")="Start At";
	catalog<String>("start","page")="Curve";
	catalog<bool>("start","joined")=true;

	catalog<bool>("tipStart")=false;
	catalog<String>("tipStart","label")="Tip Relative";
	catalog<String>("tipStart","page")="Curve";

	catalog<String>("wayStart")="";
	catalog<String>("wayStart","label")="Waypoint Start";
	catalog<String>("wayStart","page")="Curve";

	catalog<Real>("end")=1.0;
	catalog<Real>("end","low")= -1.0;
	catalog<Real>("end","high")=2.0;
	catalog<Real>("end","min")= -1e9;
	catalog<Real>("end","max")=1e9;
	catalog<String>("end","label")="End At";
	catalog<String>("end","page")="Curve";
	catalog<bool>("end","joined")=true;

	catalog<bool>("tipEnd")=false;
	catalog<String>("tipEnd","label")="Tip Relative";
	catalog<String>("tipEnd","page")="Curve";

	catalog<String>("wayEnd")="";
	catalog<String>("wayEnd","label")="Waypoint End";
	catalog<String>("wayEnd","page")="Curve";

	catalog<Real>("maxStepChange")=1.0;
	catalog<Real>("maxStepChange","high")=2.0;
	catalog<Real>("maxStepChange","max")=10.0;
	catalog<String>("maxStepChange","label")="Max Step Change";
	catalog<String>("maxStepChange","page")="Curve";

	catalog<bool>("wrench")=false;
	catalog<String>("wrench","label")="Wrench";
	catalog<String>("wrench","page")="Curve";
	catalog<bool>("wrench","joined")=true;
	catalog<String>("wrench","hint")=
			"Add explicit deviations with an attribute.";

	catalog<String>("wrenchAttr")="wrench";
	catalog<String>("wrenchAttr","label")="Attr";
	catalog<String>("wrenchAttr","page")="Curve";
	catalog<bool>("wrenchAttr","joined")=true;
	catalog<String>("wrenchAttr","hint")=
			"Name of point string attribute on curves describing"
			" a wrenching vector that can deviate from the normal.";

	catalog<Real>("wrenchScale")=1.0;
	catalog<String>("wrenchScale","label")="Wrench Scale";
	catalog<String>("wrenchScale","page")="Curve";
	catalog<String>("wrenchScale","hint")=
			"Scale wrench influence.";

	//* Link
	catalog<bool>("fragment")=false;
	catalog<String>("fragment","label")="Fragment";
	catalog<String>("fragment","page")="Link";
	catalog<bool>("fragment","joined")=true;
	catalog<String>("fragment","hint")=
			"Associate link primitives with a matching fragment attribute.";

	catalog<String>("FragmentAttr")="part";
	catalog<String>("FragmentAttr","label")="Attr";
	catalog<String>("FragmentAttr","page")="Link";
	catalog<String>("FragmentAttr","hint")=
			"Name of link string attribute describing membership"
			" in a collection of primitives.";

	catalog<SpatialVector>("linkScale")=SpatialVector(1.0,1.0,1.0);
	catalog<String>("linkScale","label")="Link Scale";
	catalog<String>("linkScale","page")="Link";

	catalog<I32>("maxCount")=100;
	catalog<I32>("maxCount","high")=1000;
	catalog<I32>("maxCount","max")=1000000;
	catalog<String>("maxCount","label")="Max Link Count";
	catalog<String>("maxCount","page")="Link";
	catalog<bool>("maxCount","joined")=true;
	catalog<String>("maxCount","hint")=
			"Stop processing a curve after reaching this number of links.";

	catalog<I32>("stopCount")=0;
	catalog<I32>("stopCount","high")=100;
	catalog<I32>("stopCount","max")=1000000;
	catalog<String>("stopCount","label")="Stop After";
	catalog<String>("stopCount","page")="Link";
	catalog<String>("stopCount","hint")=
			"Stop walking the curve after reaching this number of links."
			"  This limit is ignored if zero.";

	catalog<bool>("generateMax")=false;
	catalog<String>("generateMax","label")="Generate Max";
	catalog<String>("generateMax","page")="Link";
	catalog<String>("generateMax","hint")=
			"Output maximum count of links even if they"
			" have to be packed at the end.";

	catalog<I32>("linkOrder")=0;
	catalog<String>("linkOrder","label")="Link Order";
	catalog<String>("linkOrder","page")="Link";

	catalog<Real>("pivotBaseTweak")=0.0;
	catalog<Real>("pivotBaseTweak","min")= -1.0;
	catalog<String>("pivotBaseTweak","label")="Pivot Base Tweak";
	catalog<String>("pivotBaseTweak","page")="Link";
	catalog<bool>("pivotBaseTweak","joined")=true;

	catalog<Real>("pivotTipTweak")=0.0;
	catalog<Real>("pivotTipTweak","min")= -1.0;
	catalog<String>("pivotTipTweak","label")="Pivot Tip Tweak";
	catalog<String>("pivotTipTweak","page")="Link";

	catalog<Real>("pivotGapTweak")=0.0;
	catalog<Real>("pivotGapTweak","min")= -1.0;
	catalog<String>("pivotGapTweak","label")="Pivot Gap Tweak";
	catalog<String>("pivotGapTweak","page")="Link";
	catalog<bool>("pivotGapTweak","joined")=true;

	catalog<Real>("pivotYawTweak")=0.0;
	catalog<Real>("pivotYawTweak","min")= -1.0;
	catalog<String>("pivotYawTweak","label")="Pivot Yaw Tweak";
	catalog<String>("pivotYawTweak","page")="Link";

	catalog<String>("flipTwist")="";
	catalog<String>("flipTwist","label")="Flip Twist for";
	catalog<String>("flipTwist","page")="Link";
	catalog<String>("flipTwist","hint")=
			"Flip the twist of the output link for any link index matching"
			" this space-delimited set of regex patterns.";

	//* Attach
	catalog<String>("pointAttr")="part";
	catalog<String>("pointAttr","label")="Point Attr";
	catalog<String>("pointAttr","page")="Attach";
	catalog<bool>("pointAttr","joined")=true;
	catalog<String>("pointAttr","hint")=
			"Name of point string attribute to write indexed link name.";

	catalog<String>("primitiveAttr")="part";
	catalog<String>("primitiveAttr","label")="Primitive Attr";
	catalog<String>("primitiveAttr","page")="Attach";
	catalog<String>("primitiveAttr","hint")=
			"Name of primitive string attribute to write indexed link name.";

	catalog<String>("linkName")="linkINDEX";
	catalog<String>("linkName","label")="Link Name";
	catalog<String>("linkName","page")="Attach";
	catalog<bool>("linkName","joined")=true;
	catalog<String>("linkName","hint")=
			"Name of each link,"
			" presumably with an an index replacement string."
			"  This name will be stored in the Point Attr and Primitive Attr,"
			" if set.";

	catalog<String>("pattern")="INDEX";
	catalog<String>("pattern","label")="Replacing";
	catalog<String>("pattern","page")="Attach";
	catalog<bool>("pattern","joined")=true;
	catalog<String>("pattern","hint")=
			"Replace this string in the Link Name with"
			" an index unique to each link.";

	catalog<I32>("startIndex")=0;
	catalog<I32>("startIndex","high")=100;
	catalog<I32>("startIndex","max")=I32(1e6);
	catalog<String>("startIndex","label")="Starting With";
	catalog<String>("startIndex","page")="Attach";
	catalog<String>("startIndex","hint")=
			"Start the unique link counter from this number,"
			" probably 0 or 1.";

	catalog<Real>("spiralStep")=90.0;
	catalog<Real>("spiralStep","min")= -1e9;
	catalog<Real>("spiralStep","low")= -180.0;
	catalog<Real>("spiralStep","high")=180.0;
	catalog<Real>("spiralStep","max")=1e9;
	catalog<String>("spiralStep","label")="Spiral Step";
	catalog<String>("spiralStep","page")="Attach";
	catalog<bool>("spiralStep","joined")=true;

	catalog<Real>("spiralStart")=0.0;
	catalog<Real>("spiralStart","min")= -180.0;
	catalog<Real>("spiralStart","max")=180.0;
	catalog<String>("spiralStart","label")="Start";
	catalog<String>("spiralStart","page")="Attach";

	catalog<Real>("spiralVary")=0.0;
	catalog<Real>("spiralVary","max")=180.0;
	catalog<String>("spiralVary","label")="Spiral Vary";
	catalog<String>("spiralVary","page")="Attach";
	catalog<bool>("spiralVary","joined")=true;

	catalog<Real>("spiralFade")=0.0;
	catalog<String>("spiralFade","label")="Fade";
	catalog<String>("spiralFade","page")="Attach";

	catalog<Real>("yawVary")=0.0;
	catalog<Real>("yawVary","max")=180.0;
	catalog<String>("yawVary","label")="Yaw Vary";
	catalog<String>("yawVary","page")="Attach";
	catalog<bool>("yawVary","joined")=true;

	catalog<Real>("yawFade")=0.0;
	catalog<String>("yawFade","label")="Fade";
	catalog<String>("yawFade","page")="Attach";

	catalog<Real>("pitchVary")=0.0;
	catalog<Real>("pitchVary","max")=180.0;
	catalog<String>("pitchVary","label")="Pitch Vary";
	catalog<String>("pitchVary","page")="Attach";
	catalog<bool>("pitchVary","joined")=true;

	catalog<Real>("pitchFade")=0.0;
	catalog<String>("pitchFade","label")="Fade";
	catalog<String>("pitchFade","page")="Attach";

	catalog<Real>("upVary")=0.0;
	catalog<Real>("upVary","high")=1.0;
	catalog<Real>("upVary","max")=1e6;
	catalog<String>("upVary","label")="Up Vary";
	catalog<String>("upVary","page")="Attach";
	catalog<bool>("upVary","joined")=true;

	catalog<Real>("upFade")=0.0;
	catalog<String>("upFade","label")="Fade";
	catalog<String>("upFade","page")="Attach";

	catalog<Real>("sideVary")=0.0;
	catalog<Real>("sideVary","high")=1.0;
	catalog<Real>("sideVary","max")=1e6;
	catalog<String>("sideVary","label")="Side Vary";
	catalog<String>("sideVary","page")="Attach";
	catalog<bool>("sideVary","joined")=true;

	catalog<Real>("sideFade")=0.0;
	catalog<String>("sideFade","label")="Fade";
	catalog<String>("sideFade","page")="Attach";

	catalog<Real>("slackGap")=0.0;
	catalog<Real>("slackGap","min")= -1.0;
	catalog<String>("slackGap","label")="Slack Gap";
	catalog<String>("slackGap","page")="Attach";

	catalog<Real>("slackMin")=0.0;
	catalog<String>("slackMin","label")="Slack Min";
	catalog<String>("slackMin","page")="Attach";
	catalog<bool>("slackMin","joined")=true;

	catalog<Real>("slackMax")=0.0;
	catalog<String>("slackMax","label")="Max";
	catalog<String>("slackMax","page")="Attach";

	catalog<I32>("randomSeed")=0;
	catalog<String>("randomSeed","label")="Random Seed";
	catalog<String>("randomSeed","page")="Attach";
	catalog<bool>("randomSeed","joined")=true;

	catalog<Real>("randomness")=1.0;
	catalog<String>("randomness","label")="Randomness";
	catalog<String>("randomness","page")="Attach";

	catalog<Real>("rotationLimit")=180.0;
	catalog<Real>("rotationLimit","max")=180.0;
	catalog<String>("rotationLimit","label")="Rotation Limit";
	catalog<String>("rotationLimit","page")="Attach";

	catalog<bool>("stepProtect")=true;
	catalog<String>("stepProtect","label")="Short Circuit Protection";
	catalog<String>("stepProtect","page")="Attach";

	//* Constraint
	catalog<bool>("clampStart")=false;
	catalog<String>("clampStart","label")="Clamp Start";
	catalog<String>("clampStart","page")="Constraint";

	catalog<SpatialVector>("startClampN")=SpatialVector(0.0,1.0,0.0);
	catalog<String>("startClampN","label")="Start Normal";
	catalog<String>("startClampN","enabler")="clampStart";
	catalog<String>("startClampN","page")="Constraint";

	catalog<bool>("clampEnd")=false;
	catalog<String>("clampEnd","label")="Clamp End";
	catalog<String>("clampEnd","page")="Constraint";

	catalog<SpatialVector>("endClampN")=SpatialVector(0.0,1.0,0.0);
	catalog<String>("endClampN","label")="End Normal";
	catalog<String>("endClampN","enabler")="clampEnd";
	catalog<String>("endClampN","page")="Constraint";

	catalog<I32>("fittingPasses")=4;
	catalog<I32>("fittingPasses","min")=1;
	catalog<I32>("fittingPasses","high")=100;
	catalog<I32>("fittingPasses","max")=1000;
	catalog<String>("fittingPasses","label")="Fitting Passes";
	catalog<String>("fittingPasses","enabler")="clampEnd";
	catalog<String>("fittingPasses","page")="Constraint";
	catalog<bool>("fittingPasses","joined")=true;

	catalog<Real>("fittingRate")=1.0;
	catalog<Real>("fittingRate","high")=1.0;
	catalog<Real>("fittingRate","max")=10.0;
	catalog<String>("fittingRate","label")="Rate";
	catalog<String>("fittingRate","enabler")="clampEnd";
	catalog<String>("fittingRate","page")="Constraint";

	//* Temporal
	catalog<bool>("Temporal")=false;
	catalog<String>("Temporal","page")="Temporal";
	catalog<String>("Temporal","hint")=
			"Tries to retain state of the prior frame."
			"  If the prior frame was not the last evaluated,"
			" the node may choose not to use temporal data.";
	catalog<bool>("Temporal","joined")=true;

	catalog<Real>("slipResponse")=0.0;
	catalog<Real>("slipResponse","max")=1.0;
	catalog<String>("slipResponse","label")="Slip Response";
	catalog<String>("slipResponse","enabler")="Temporal";
	catalog<String>("slipResponse","page")="Temporal";
	catalog<String>("slipResponse","hint")=
			"When temporal, this defines how quickly the slip returns"
			" to zero.";

	catalog<Real>("slipLimit")=0.0;
	catalog<Real>("slipLimit","high")=60.0;
	catalog<Real>("slipLimit","max")=360.0;
	catalog<String>("slipLimit","label")="Slip Limit Twist";
	catalog<String>("slipLimit","enabler")="Temporal";
	catalog<String>("slipLimit","page")="Temporal";
	catalog<String>("slipLimit","hint")=
			"When temporal, allow spiral twist to defer this much change.";
	catalog<bool>("slipLimit","joined")=true;

	catalog<Real>("slipLimitYaw")=0.0;
	catalog<Real>("slipLimitYaw","high")=60.0;
	catalog<Real>("slipLimitYaw","max")=360.0;
	catalog<String>("slipLimitYaw","label")="Yaw";
	catalog<String>("slipLimitYaw","enabler")="Temporal";
	catalog<String>("slipLimitYaw","page")="Temporal";
	catalog<String>("slipLimitYaw","hint")=
			"When temporal, allow yaw to defer this much change.";
	catalog<bool>("slipLimitYaw","joined")=true;

	catalog<Real>("slipLimitPitch")=0.0;
	catalog<Real>("slipLimitPitch","high")=60.0;
	catalog<Real>("slipLimitPitch","max")=360.0;
	catalog<String>("slipLimitPitch","label")="Pitch";
	catalog<String>("slipLimitPitch","enabler")="Temporal";
	catalog<String>("slipLimitPitch","page")="Temporal";
	catalog<String>("slipLimitPitch","hint")=
			"When temporal, allow yaw to defer this much change.";

	//* Result

	//* NOTE internal caching for Maya, instead of recycling
	catalog<bool>("cacheResult")=FALSE;
	catalog<String>("cacheResult","label")="Cache Result";
	catalog<String>("cacheResult","page")="Result";
	catalog<bool>("cacheResult","hidden")=TRUE;

	catalog<bool>("drawGuides")=FALSE;
	catalog<String>("drawGuides","label")="Draw Guides";
	catalog<String>("drawGuides","page")="Result";

	catalog<bool>("outputTransforms")=FALSE;
	catalog<String>("outputTransforms","label")="Output Transform Triangles";
	catalog<String>("outputTransforms","page")="Result";

	catalog<I32>("linkCount")=0;
	catalog<I32>("linkCount","high")=100;
	catalog<I32>("linkCount","max")=1000000;
	catalog<String>("linkCount","label")="Links Along Curve";
	catalog<String>("linkCount","page")="Result";
	catalog<String>("linkCount","IO")="output";

	catalog<I32>("fileLinkCount")=0;
	catalog<I32>("fileLinkCount","high")=100;
	catalog<I32>("fileLinkCount","max")=1000000;
	catalog<String>("fileLinkCount","label")="Links in Output";
	catalog<String>("fileLinkCount","page")="Result";
	catalog<String>("fileLinkCount","IO")="output";

	catalog< sp<Component> >("Input Curves");

	catalog< sp<Component> >("Link Surfaces");

	catalog< sp<Component> >("Scan Surfaces");
	catalog<bool>("Scan Surfaces","optional")=true;

	//* no auto-copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<bool>("Output Surface","recycle")=true;
}

void ChainOp::handle(Record& a_rSignal)
{
#if FE_CHAIN_DEBUG
	feLog("ChainOp::handle\n");
#endif

	catalog<String>("summary")="";

	const String pointAttr=catalog<String>("pointAttr");
	const String primitiveAttr=catalog<String>("primitiveAttr");

	const BWORD fragment=catalog<bool>("fragment");
	const String fragmentAttr=catalog<String>("FragmentAttr");

	const String flipTwist=catalog<String>("flipTwist");

	const BWORD cacheResult=catalog<bool>("cacheResult");

	m_flipTwistPatterns.clear();
	if(!flipTwist.empty())
	{
		String buffer=flipTwist;
		while(!buffer.empty())
		{
			m_flipTwistPatterns.push_back(buffer.parse());
		}
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	if(cacheResult)
	{
#if FE_CHAIN_DEBUG
		feLog("ChainOp::handle cacheResult %d\n",
				m_spResultAccessible.isValid());
#endif

		if(m_spResultAccessible.isNull())
		{
			sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
					registry()->create("*.SurfaceAccessibleCatalog");
			if(spSurfaceAccessibleCatalog.isValid())
			{
				spSurfaceAccessibleCatalog->setKey(name()+":cache");
			}
			m_spResultAccessible=spSurfaceAccessibleCatalog;

#if FE_CHAIN_DEBUG
		feLog("ChainOp::handle result created %d\n",
				m_spResultAccessible.isValid());
#endif
		}
	}
	else
	{
		m_spResultAccessible=spOutputAccessible;
	}


	sp<SurfaceAccessibleI> spPathAccessible;
	if(!access(spPathAccessible,"Input Curves")) return;

	sp<SurfaceAccessorI> spPathVertices;
	if(!access(spPathVertices,spPathAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceI> spPath;
	if(!access(spPath,spPathAccessible)) return;

	const BWORD wrench=catalog<bool>("wrench");
	const String wrenchAttr=catalog<String>("wrenchAttr");
	const Real wrenchScale=catalog<Real>("wrenchScale");

	sp<SurfaceAccessorI> spPathNormal;
	sp<SurfaceAccessorI> spPathWrench;
	if(wrench && !wrenchAttr.empty())
	{
		if(access(spPathNormal,spPathAccessible,e_point,e_normal,e_warning) &&
				!access(spPathWrench,spPathAccessible,e_point,
				wrenchAttr,e_warning))
		{
			spPathNormal=NULL;
		}
	}

	if(!access(m_spLinkAccessible,"Link Surfaces")) return;

	if(!access(m_spLinkVertices,m_spLinkAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessibleI>	spScanAccessible;
	access(spScanAccessible,"Scan Surfaces",e_quiet);
	if(spScanAccessible.isValid())
	{
		if(!access(m_spScanVertices,spScanAccessible,
				e_primitive,e_vertices,e_warning))
		{
			spScanAccessible=NULL;
		}
	}
	if(spScanAccessible.isNull())
	{
		spScanAccessible=m_spLinkAccessible;
		m_spScanVertices=m_spLinkVertices;
	}

	sp<DrawI> spDrawGuide;
	if(catalog<bool>("drawGuides"))
	{
		accessGuide(spDrawGuide,a_rSignal);
	}

	//* TODO
	const String groupName;

	if(fragment)
	{
		m_spLinkVertices->fragmentWith(SurfaceAccessibleI::e_primitive,
				fragmentAttr,groupName);
		if(m_spScanVertices!=m_spLinkVertices)
		{
			m_spScanVertices->fragmentWith(SurfaceAccessibleI::e_primitive,
					fragmentAttr,groupName);

			if(m_spScanVertices->fragmentCount()!=
					m_spLinkVertices->fragmentCount())
			{
				catalog<String>("warning")+=
						"not using Scan Surface with a different number of"
						" link fragments than Link Surface;";

				spScanAccessible=m_spLinkAccessible;
				m_spScanVertices=m_spLinkVertices;
			}
		}
	}

	scanInputLinks();

	//* Curve
	const BWORD measureDistance=catalog<bool>("measureDistance");
	const BWORD tipStart=catalog<bool>("tipStart");
	const BWORD tipEnd=catalog<bool>("tipEnd");
	const String wayStart=catalog<String>("wayStart");
	const String wayEnd=catalog<String>("wayEnd");
	const Real start=catalog<Real>("start");
	const Real end=catalog<Real>("end");
	const Real maxStepChange=catalog<Real>("maxStepChange");

	sp<SurfaceAccessorI> spPathWaypoint;
	sp<SurfaceAccessorI> spPathWayPartial;
	if(!wayStart.empty() || !wayEnd.empty())
	{
		access(spPathWaypoint,spPathAccessible,
				e_point,"waypoint",e_warning);
		access(spPathWayPartial,spPathAccessible,
				e_point,"waypartial",e_warning);
	}

	//* Link
	const SpatialVector linkScale=catalog<SpatialVector>("linkScale");
	const BWORD generateMax=catalog<bool>("generateMax");
	const I32 stopCount=catalog<I32>("stopCount");
	const I32 maxCount=catalog<I32>("maxCount");
	const I32 linkOrder=catalog<I32>("linkOrder");
	const Real pivotBaseTweak=catalog<Real>("pivotBaseTweak");
	const Real pivotTipTweak=catalog<Real>("pivotTipTweak");
	const Real pivotGapTweak=catalog<Real>("pivotGapTweak");
	const Real pivotYawTweak=catalog<Real>("pivotYawTweak");

	//* Attach
	const Real yawVary=catalog<Real>("yawVary");
	const Real yawFade=catalog<Real>("yawFade");
	const Real pitchVary=catalog<Real>("pitchVary");
	const Real pitchFade=catalog<Real>("pitchFade");
	const Real upVary=catalog<Real>("upVary");
	const Real upFade=catalog<Real>("upFade");
	const Real sideVary=catalog<Real>("sideVary");
	const Real sideFade=catalog<Real>("sideFade");
	const Real slackGap=catalog<Real>("slackGap");
	const Real slackMin=catalog<Real>("slackMin");
	const Real slackMax=catalog<Real>("slackMax");
	const Real spiralStep=catalog<Real>("spiralStep");
	const Real spiralVary=catalog<Real>("spiralVary");
	const Real spiralStart=catalog<Real>("spiralStart");
	const Real spiralFade=catalog<Real>("spiralFade");
	const I32 randomSeed=catalog<I32>("randomSeed");
	const Real randomness=catalog<Real>("randomness");
	const Real rotationLimit=catalog<Real>("rotationLimit")*degToRad;
	const BWORD stepProtect=catalog<bool>("stepProtect");

	//* Constraint
	const BWORD clampStart=catalog<bool>("clampStart");
	const BWORD clampEnd=catalog<bool>("clampEnd");

	const SpatialVector startClampN=
			unitSafe(catalog<SpatialVector>("startClampN"));
	const SpatialVector endClampN=
			unitSafe(catalog<SpatialVector>("endClampN"));

	const SpatialVector zero(0.0,0.0,0.0);

	const I32 fittingPasses=catalog<I32>("fittingPasses");
	const Real fittingRate=catalog<Real>("fittingRate");

	//* Temporal
	const BWORD temporal=catalog<bool>("Temporal");
	const SpatialVector slipLimit=temporal? SpatialVector(
			catalog<Real>("slipLimitPitch")*degToRad,
			catalog<Real>("slipLimitYaw")*degToRad,
			catalog<Real>("slipLimit")*degToRad):
			SpatialVector(0.0,0.0,0.0);

	const BWORD doTemporal=(temporal && (clampEnd ||
			slipLimit[0]>0.0 || slipLimit[1]>0.0 || slipLimit[2]>0.0));

	const BWORD outputTransforms=catalog<bool>("outputTransforms");
	if(outputTransforms)
	{
		if(!accessDraw(m_spOutputDraw,a_rSignal)) return;
	}

	const BWORD verbose=FE_CHAIN_VERBOSE;

	const I32 refCount=m_linkRef.size();
	if(!refCount)
	{
		catalog<String>("error")+="no reference links found;";
		return;
	}

	const Real frame=currentFrame(a_rSignal);

	//* TODO can mixed links be reusable?
	if(m_clearNextOutput || refCount>1 || outputTransforms ||
			catalog<bool>("Link Surfaces","replaced"))
	{
//		feLog("ChainOp::handle clear result %d %d %d %d\n",
//				m_clearNextOutput,refCount>1,outputTransforms,
//				catalog<bool>("Link Surfaces","replaced"));

		m_spResultAccessible->clear();
	}

	m_clearNextOutput=outputTransforms;

	m_spPointLinkName=NULL;
	if(!pointAttr.empty())
	{
		access(m_spPointLinkName,m_spResultAccessible,
				e_point,pointAttr,e_warning,e_createMissing);
	}

	m_spPrimitiveLinkName=NULL;
	if(!primitiveAttr.empty())
	{
		access(m_spPrimitiveLinkName,m_spResultAccessible,
				e_primitive,primitiveAttr,e_warning,e_createMissing);
	}

#if FE_CHAIN_DEBUG
	feLog("ChainOp::handle frame %d->%d\n",m_frame,frame);
#endif

	BWORD doResponse=TRUE;

	//* presuming always replaced with frame change
	if(doTemporal && (frame==m_frame || frame==m_frame+1))
	{
#if FE_CHAIN_DEBUG
		if(frame==m_frame+1)
		{
			feLog("ChainOp::handle temporal step\n");
		}
		else
		{
			feLog("ChainOp::handle temporal repeat\n");
			doResponse=FALSE;
		}
#endif
	}
	else
	{
#if FE_CHAIN_DEBUG
		feLog("ChainOp::handle temporal reset\n");
#endif
		m_linkState.clear();
		m_gapCorrection.clear();
		m_spiralCorrection.clear();
	}

	const Real slipResponse=
			doResponse? catalog<Real>("slipResponse"): 0.0;

	m_frame=frame;

	//* TODO auto stop when a solution has converged
	const I32 passes=(clampEnd)?
			((fittingPasses > 0)? fittingPasses: 64): 1;

	const Real spiralStepRadians=spiralStep*degToRad;
	const Real spiralStartRadians=spiralStart*degToRad;
	const Real spiralVaryRadians=spiralVary*degToRad;
	const Real yawVaryRadians=yawVary*degToRad;
	const Real pitchVaryRadians=pitchVary*degToRad;

	SpatialTransform linkScaling;
	setIdentity(linkScaling);
	scale(linkScaling,linkScale);

	const SpatialVector unitX(1.0,0.0,0.0);
	const SpatialVector unitY(0.0,1.0,0.0);
	const SpatialVector unitZ(0.0,0.0,1.0);

	m_fileLinkCount=0;
	I32 linkCount=0;	//* doesn't include forced links past end
	m_pointStart=0;
	m_primitiveStart=0;

	const I32 stopAfter=(stopCount > 0)? stopCount: maxCount;
	const I32 linkLimit=
			(generateMax || maxCount<stopAfter)? maxCount: stopAfter;

	//* for each curve
	const I32 primitiveCount=spPathVertices->count();

#if FE_CHAIN_DEBUG
		feLog("ChainOp::handle path primitiveCount %d\n",primitiveCount);
#endif

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			break;
		}

		if(verbose)
		{
			feLog("path %d/%d\n",primitiveIndex,primitiveCount);
		}

		const Real fullLength=lengthAtFraction(spPathVertices,
						primitiveIndex,1.0);

		//* NOTE uv/cv conversions seem to presume even spacing

		Real uStart=tipStart?
				(measureDistance?
				fractionAtLength(spPathVertices,
						primitiveIndex,fullLength+start): 1.0+start):
				(measureDistance?
				fractionAtLength(spPathVertices,
						primitiveIndex,start): start);
		Real uEnd=tipEnd?
				(measureDistance?
				fractionAtLength(spPathVertices,
						primitiveIndex,fullLength+end): 1.0+end):
				(measureDistance?
				fractionAtLength(spPathVertices,
						primitiveIndex,end): end);

		const U32 subCount=
				spPathVertices->subCount(primitiveIndex);

		if(spPathWaypoint.isValid() && subCount>1)
		{
			Real subStart=uStart;
			Real subEnd=uEnd;

			BWORD replaceStart=FALSE;
			BWORD replaceEnd=FALSE;

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spPathVertices->integer(primitiveIndex,subIndex);
				const String waypoint=spPathWaypoint->string(pointIndex);
				if(waypoint.empty())
				{
					continue;
				}
				Real partial=0.0;
				if(spPathWayPartial.isValid())
				{
					partial=spPathWayPartial->real(pointIndex);
				}
				if(wayStart==waypoint)
				{
					subStart=(subIndex+partial)/(subCount-1.0);
					replaceStart=TRUE;
				}
				if(wayEnd==waypoint)
				{
					subEnd=(subIndex+partial)/(subCount-1.0);
					replaceEnd=TRUE;
				}
			}

			//* allow for start/end adjustment even in waypoint mode
			uStart=replaceStart? subStart+(subEnd-subStart)*uStart: uStart;
			uEnd=replaceEnd? subStart+(subEnd-subStart)*uEnd: uEnd;
		}

//		feLog("prim %d/%d fullLength %.6G uStart %.6G uEnd %.6G\n",
//				primitiveIndex,primitiveCount,fullLength,uStart,uEnd);

		Real uStepSum=0.0;
		I32 uStepCount=0;

		//*  if temporal, start with correction from last frame
		if(m_gapCorrection.size()<U32(primitiveIndex+1))
		{
			m_gapCorrection.resize(primitiveIndex+1,0.0);
		}
		if(m_spiralCorrection.size()<U32(primitiveIndex+1))
		{
			m_spiralCorrection.resize(primitiveIndex+1,0.0);
		}
		Real& rGapCorrection=m_gapCorrection[primitiveIndex];
		Real& rSpiralCorrection=m_spiralCorrection[primitiveIndex];

		for(I32 pass=0;pass<passes;pass++)
		{
			if(verbose)
			{
				feLog("pass %d/%d\n",pass,passes);
			}

			randomRealSeed(randomSeed+primitiveIndex);

			SpatialTransform lastConversion;
			setIdentity(lastConversion);
			SpatialVector lastTip(0.0,0.0,0.0);
			SpatialVector lastArmUnit=unitZ;
			SpatialVector lastCenter(0.0,0.0,0.0);
			Real lastWrenchAngle=0.0;
			Real lastFinishLength=0.0;
			Real u0=uStart;
			Real u1=0.0;
			I32 modelLinkCount=0;
			const Real inc=(uEnd > uStart)? 1.0: -1.0;
			BWORD on_curve=TRUE;

			I32 refIndex=0;
			I32 refIndexPrev=0;
			I32 refIndexNext=0;

			if(verbose)
			{
				feLog("link %d/%d\n",modelLinkCount,linkLimit);
			}

			const BWORD create=(pass == passes-1);
			while(modelLinkCount<linkLimit &&
					((on_curve=(inc*u0<inc*uEnd)) || generateMax))
			{
				// TODO pick from: first, in order, or random
				if(linkOrder == 0)
				{
					refIndex=linkCount % refCount;
					refIndexPrev=linkCount? (linkCount-1) % refCount: 0;
					refIndexNext=(linkCount+1) % refCount;
				}
				else
				{
					refIndexPrev=refIndex;
					refIndex=refIndexNext;
					refIndexNext=refCount*0.9999*randomReal(0.0,1.0);
				}
				// TODO else random

				LinkState* pLinkState=NULL;
				if(doTemporal)
				{
					if(m_linkState.size()<=U32(m_fileLinkCount))
					{
						m_linkState.resize(m_fileLinkCount+1);
					}
					pLinkState= &m_linkState[m_fileLinkCount];
				}

				const U32 axisForward=m_linkRef[refIndex].m_axisForward;

				const Real scaleForward=linkScale[axisForward];

				const Real pivotBase=m_linkRef[refIndex].m_pivotBase +
						pivotBaseTweak;
				const Real pivotTip=m_linkRef[refIndex].m_pivotTip +
						pivotTipTweak;
				const Real pivotGap=m_linkRef[refIndex].m_pivotGapTip +
						m_linkRef[refIndexNext].m_pivotGapBase +
						pivotGapTweak;
				const Real pivotYaw=m_linkRef[refIndexPrev].m_pivotYawTip +
						m_linkRef[refIndex].m_pivotYawBase +
						pivotYawTweak;

				const Real yawGap=pivotYaw-pivotBase;
				const SpatialVector vectorPivotBase(0.0,0.0,
						scaleForward*pivotBase);
				const SpatialVector vectorPivotTip(0.0,0.0,
						scaleForward*pivotTip);
				const SpatialVector vectorYawGap(0.0,0.0,scaleForward*yawGap);
				const SpatialVector minusPivotBase=-vectorPivotBase;
				const SpatialVector minusYawGap=-vectorYawGap;

				const SpatialVector refArm=vectorPivotTip-vectorPivotBase;
				const Real refArmLength=magnitude(refArm);
				const SpatialVector refArmUnit=refArm *
						(refArmLength > 0.0? 1.0/refArmLength: 1.0);

				if(verbose)
				{
					feLog("refArmUnit %s\n",c_print(refArmUnit));
				}

				SpatialTransform spiralStepRotation=
						SpatialQuaternion(spiralStepRadians,refArmUnit);

				SpatialTransform spiralStartRotation=
						SpatialQuaternion(spiralStartRadians,refArmUnit);

				//* create link
				sp<SurfaceAccessibleI> spLink;
				if(create)
				{
					if(outputTransforms)
					{
						spLink=NULL;
					}
					else if(fragment)
					{
						//* TODO cache each possible link

						sp<SurfaceAccessibleI::FilterI> spFilter;

						const String fragmentKey=
								m_spLinkVertices->fragment(refIndex);
						if(!m_spLinkVertices->filterWith(fragmentKey,spFilter)
								|| spFilter.isNull())
						{
							feLog("ChainOp::handle link filter failed\n");
						}

						spLink=registry()->create("*.SurfaceAccessibleCatalog");
						if(spLink.isValid())
						{
							spLink->copy(m_spLinkAccessible,spFilter);
						}
					}
					else
					{
						spLink=m_spLinkAccessible;
					}

//					char buffer[16];
//					sprintf(buffer,"m_chain%d",m_fileLinkCount+1);
					//* TODO set link name on primitives
				}

				if(!(clampEnd && generateMax) &&
						(!on_curve || modelLinkCount >= stopAfter))
				{
					modelLinkCount++;
					if(create)
					{
						addLink(spLink,lastConversion);

						m_fileLinkCount++;
						// not incrementing linkCount
					}
					continue;
				}

				//* locate desired pivots

				const Real baseLength=lengthAtFraction(spPathVertices,
						primitiveIndex,u0);

				//* NOTE true variable gap computed later
				const Real approximateGap=rGapCorrection+scaleForward*pivotGap;
				const Real step=inc*(refArmLength+approximateGap);

				const Real tipLength=baseLength+step;

				lastFinishLength=baseLength+inc*refArmLength;

				u1=fractionAtLength(spPathVertices,
						primitiveIndex,tipLength);

				const SpatialVector du=evaluateAtFraction(
						spPathVertices,primitiveIndex,u1,1);
				const SpatialVector du2=evaluateAtFraction(
						spPathVertices,primitiveIndex,u1,2);

				FEASSERT(u1>u0);

				const Real duLength=magnitude(du);
				const SpatialVector duUnit=
						du*(duLength > 0.0? 1.0/duLength: 1.0);

				const Real du2Length=magnitude(du2);
				const SpatialVector du2Unit=du2 *
						(du2Length > 0.0? 1.0/du2Length: 1.0);

				Real duDot=dot(du2Unit,duUnit);
				SpatialVector du2_perpendicular=du2*(1.0-fabs(duDot));

				const Real bend=magnitude(du2_perpendicular);
				Real slack=slackMax > 0.0?
						(bend-slackMin)/slackMax: 0.0;
				slack=slack > 0.0? (slack<1.0? slack: 1.0): 0.0;

				const Real gapUncorrected=scaleForward*(pivotGap+slackGap*slack);
				const Real gap=gapUncorrected+rGapCorrection;

				const SpatialVector nextPivotUncorrected=
						vectorPivotTip+gapUncorrected*refArmUnit;
				const SpatialVector nextPivot=
						vectorPivotTip+gap*refArmUnit;

				SpatialVector point0;
				SpatialVector point1;
				SpatialVector normal;
				SpatialBary bary;

				if(modelLinkCount)
				{
					point0=lastTip;
				}
				else
				{
					set(bary,u0,0.0);
					point0=spPath->sample(primitiveIndex,bary).translation();
				}
				set(bary,u1,0.0);
				point1=spPath->sample(primitiveIndex,bary).translation();

				if(verbose)
				{
					set(bary,u0,0.0);
					const SpatialVector point_u0=
							spPath->sample(primitiveIndex,bary).translation();

					feLog("\npoint_u0 %s\n",c_print(point_u0));
				}

				Real thisSpiral=0.0;
				Real thisYaw=0.0;
				Real thisPitch=0.0;
				Real thisUp=0.0;
				Real thisSide=0.0;

				if(modelLinkCount && modelLinkCount<linkLimit-1)
				{
					thisSpiral=randomness*spiralVaryRadians *
							(1.0-(1.0-slack)*spiralFade) *
							(1.0-2.0*randomReal(0.0,1.0));
					thisYaw=randomness*yawVaryRadians *
							(1.0-(1.0-slack)*yawFade) *
							(1.0-2.0*randomReal(0.0,1.0));
					thisPitch=randomness*pitchVaryRadians *
							(1.0-(1.0-slack)*pitchFade) *
							(1.0-2.0*randomReal(0.0,1.0));
					thisUp=randomness*upVary*
							(1.0-(1.0-slack)*upFade) *
							(1.0-2.0*randomReal(0.0,1.0));
					thisSide=randomness*sideVary*
							(1.0-(1.0-slack)*sideFade) *
							(1.0-2.0*randomReal(0.0,1.0));
				}

				const SpatialVector offset(thisUp,thisSide,0.0);

				SpatialVector worldOffset;
				rotateVector(lastConversion,offset,worldOffset);

				//* calculate transform from reference to curve
				const SpatialVector segArm=point1-point0-worldOffset;
				const Real segArmLength=magnitude(segArm);
				FEASSERT(segArmLength>1e-9);

				SpatialVector segArmUnit=segArm *
						(segArmLength > 0.0? 1.0/segArmLength: 1.0);

				//* pitch, yaw, spiral
				Real wrenchAngle=0.0;

				if(spPathWrench.isValid())
				{
					FEASSERT(spPathNormal.isValid());

					const Real cv=0.5*(u0+u1)*(subCount-1.0);
					const I32 pointIndex0=I32(cv)<I32(subCount)?
							I32(cv): subCount-1;
					const Real fraction=cv-Real(pointIndex0);

					const I32 pointIndex1=pointIndex0<I32(subCount-1)?
							pointIndex0+1: pointIndex0;
					const Real fraction1=Real(1)-fraction;

					const SpatialVector n0=
							spPathNormal->spatialVector(pointIndex0);
					const SpatialVector n1=
							spPathNormal->spatialVector(pointIndex1);
					const SpatialVector w0=
							spPathWrench->spatialVector(pointIndex0);
					const SpatialVector w1=
							spPathWrench->spatialVector(pointIndex1);

					const SpatialVector thisNorm=n0*fraction1+n1*fraction;
					const SpatialVector thisWrench=w0*fraction1+w1*fraction;

					const SpatialVector crossNorm=
							cross(thisNorm,segArmUnit);
					const SpatialVector lockNorm=unitSafe(
							cross(segArmUnit,crossNorm));

					const SpatialVector crossSpiral=
							cross(thisWrench,segArmUnit);
					const SpatialVector lockSpiral=unitSafe(
							cross(segArmUnit,crossSpiral));

					wrenchAngle=wrenchScale*
							asin(magnitude(cross(lockNorm,lockSpiral)));

					thisSpiral+=wrenchAngle-lastWrenchAngle;
				}

				lastWrenchAngle=wrenchAngle;

				const SpatialVector thisSlippage=
						(pLinkState && pLinkState->m_priorSet)?
						pLinkState->m_priorSlippage:
						SpatialVector(0.0,0.0,0.0);

				SpatialTransform thisSpiralRotation=SpatialQuaternion(
						thisSpiral+rSpiralCorrection+
						thisSlippage[2],refArmUnit);

				SpatialQuaternion rotation;
				set(rotation,lastArmUnit,segArmUnit);

				Real radians;
				SpatialVector axis;
				rotation.computeAngleAxis(radians,axis);
				if(modelLinkCount && radians>rotationLimit)
				{
					set(rotation,rotationLimit,axis);
					rotateVector(rotation,lastArmUnit,segArmUnit);
				}

				SpatialVector segCenter=
						point0+segArmUnit*magnitude(vectorPivotBase);

				SpatialTransform segChild=rotation;
				segChild.translation()=segCenter;
				translate(segChild,-lastCenter);

				SpatialTransform lastInverse;
				invert(lastInverse,lastConversion);

				const SpatialTransform segRotation=
						lastConversion*segChild*lastInverse;
				const SpatialTransform absolute0=segRotation*lastConversion;
				const SpatialTransform absolute=
						!modelLinkCount? spiralStartRotation*segRotation:
						thisSpiralRotation*spiralStepRotation*
						segRotation*lastConversion;
				const SpatialTransform relative=absolute*lastInverse;

				if(verbose)
				{
					feLog("\nChainOp::process %d"
							"  bend %.6G gap %.6G slack %.6G\n",
							modelLinkCount,bend,gap,slack);
					feLog("  length %.6G %.6G vs %.6G step %.6G\n",
							baseLength,tipLength,fullLength,step);
					feLog("u0 %.6G u1 %.6G\n",u0,u1);
					feLog("du %s\n",c_print(du));
					feLog("du2 %s\n",c_print(du2));
					feLog("du2_perpendicular %s\n",c_print(du2_perpendicular));
					feLog("vectorPivotBase %s\n",c_print(vectorPivotBase));
					feLog("vectorPivotTip %s\n",c_print(vectorPivotTip));
					feLog("nextPivot %s\n",c_print(nextPivot));
					feLog("point0 %s\n",c_print(point0));
					feLog("point1 %s\n",c_print(point1));
					feLog("point dist %.6G\n",magnitude(point1-point0));
					feLog("lastArmUnit %s\n",c_print(lastArmUnit));
					feLog("lastCenter %s\n",c_print(lastCenter));
					feLog("segArmUnit %s\n",c_print(segArmUnit));
					feLog("segCenter %s\n",c_print(segCenter));
					feLog("lastConversion\n%s\n",c_print(lastConversion));
					feLog("lastInverse\n%s\n",c_print(lastInverse));
					feLog("thisSpiralRotation\n%s\n",
							c_print(thisSpiralRotation));
					feLog("rotation %.6G deg about %s\n",
							radians/degToRad,c_print(axis));
					feLog("segChild\n%s\n",c_print(segChild));
					feLog("segRotation\n%s\n",c_print(segRotation));
					feLog("absolute0\n%s\n",c_print(absolute0));
					feLog("absolute\n%s\n",c_print(absolute));
					feLog("relative\n%s\n",c_print(relative));

					const SpatialQuaternion relativeQuat=relative;
					relativeQuat.computeAngleAxis(radians,axis);
					feLog("angle %.6G axis %s\n",radians,c_print(axis));
				}

				SpatialTransform conversion;

				if(modelLinkCount)
				{
					//* WARNING Euler starts to deviate at around 60 degrees

//					const SpatialTransform relative=absolute*lastInverse;

					SpatialTransform rePivot=relative;
					translate(rePivot,vectorPivotBase);

#if FALSE
					const SpatialEuler euler=relative;	//* ZXY
#else

					const SpatialTransform absoluteNoTwist=
							segRotation*lastConversion;
					const SpatialTransform relativeNoTwist=
							absoluteNoTwist*lastInverse;

					const SpatialQuaternion noTwistQuat=relativeNoTwist;
					Real noTwistRadians;
					SpatialVector noTwistAxis;
					noTwistQuat.computeAngleAxis(noTwistRadians,noTwistAxis);

					SpatialEuler euler;
					euler[0]=asin(relative(2,1));
					euler[1]=asin(-relative(2,0));
					euler[2]=0.0;

					const SpatialTransform relativeCheck=euler;
					const SpatialQuaternion checkQuat=relativeCheck;
					Real checkRadians;
					SpatialVector checkAxis;
					checkQuat.computeAngleAxis(checkRadians,checkAxis);

					if(checkRadians>1e-3)
					{
						euler*=noTwistRadians/checkRadians;
					}

					euler[2]=thisSpiral+rSpiralCorrection+
							spiralStepRadians;

					euler+=thisSlippage;

					if(verbose)
					{
						feLog("relativeNoTwist\n%s\n",c_print(relativeNoTwist));
						feLog("angle %.6G axis %s\n",
								noTwistRadians,c_print(noTwistAxis));

						feLog("relativeCheck\n%s\n",c_print(relativeCheck));
						feLog("angle %.6G axis %s\n",
								checkRadians,c_print(checkAxis));
					}
#endif

					SpatialVector eulerMod(euler);
					eulerMod[0] += thisPitch;
					eulerMod[1] += thisYaw;
					if(verbose)
					{
						feLog("rePivot\n%s\n",c_print(rePivot));
						feLog("euler %s\n",c_print(euler));
						feLog("eulerMod %s\n",c_print(eulerMod));
						feLog("deg %s\n",c_print(eulerMod/degToRad));
					}

					//* pivotal magic
					SpatialTransform relativeMod;
					setIdentity(relativeMod);
					relativeMod.translation()=rePivot.translation();
					translate(relativeMod,offset);
					rotate(relativeMod,eulerMod[2],e_zAxis);
					rotate(relativeMod,eulerMod[0],e_xAxis);
					translate(relativeMod,vectorYawGap);
					rotate(relativeMod,eulerMod[1],e_yAxis);
					translate(relativeMod,minusYawGap);
					translate(relativeMod,minusPivotBase);
					if(verbose)
					{
						feLog("relativeMod\n%s\n",c_print(relativeMod));
					}

					conversion=relativeMod*lastConversion;

					if(pLinkState && pLinkState->m_priorSet)
					{
						SpatialTransform invPriorConversion;
						invert(invPriorConversion,pLinkState->m_priorConversion);

						const SpatialTransform xformAdvance=
								conversion*invPriorConversion;
						const SpatialEuler eulerAdvance=xformAdvance;

/*
						feLog("\nprior\n%s\n",
								c_print(pLinkState->m_priorConversion));
						feLog("conversion\n%s\n",
								c_print(conversion));
						feLog("advance deg %s\n",
								c_print(eulerAdvance/degToRad));
*/

						const SpatialVector& priorSlippage=
								pLinkState->m_priorSlippage;

						//* NOTE consider more tuning
						const Real scaleOld=1.0-slipResponse;
						const Real scaleNew=scaleOld;

						SpatialVector slippage=scaleOld*priorSlippage-
								scaleNew*eulerAdvance;

						for(U32 m=0;m<3;m++)
						{
							if(slippage[m]>slipLimit[m])
							{
								slippage[m]=slipLimit[m];
							}
							else if(slippage[m]< -slipLimit[m])
							{
								slippage[m]= -slipLimit[m];
							}
						}

#if FALSE
						SpatialTransform xformSlip;
						setIdentity(xformSlip);
						rotate(xformSlip,slippage[2]-priorSlippage[2],e_zAxis);

						conversion=xformSlip*conversion;
#else
						const SpatialVector eulerSlip=
								eulerMod+slippage-priorSlippage;

						//* NOTE just like pivotal magic above
						setIdentity(relativeMod);
						relativeMod.translation()=rePivot.translation();
						rotate(relativeMod,eulerSlip[2],e_zAxis);
						translate(relativeMod,offset);
						rotate(relativeMod,eulerSlip[0],e_xAxis);
						translate(relativeMod,vectorYawGap);
						rotate(relativeMod,eulerSlip[1],e_yAxis);
						translate(relativeMod,minusYawGap);
						translate(relativeMod,minusPivotBase);

						conversion=relativeMod*lastConversion;
#endif

						pLinkState->m_currentSlippage=slippage;

/*
						feLog("prior %s advance %s\n",
								c_print(priorSlippage),
								c_print(eulerAdvance[2]));
						feLog("slippage %s limit %s\n",
								c_print(slippage),c_print(slipLimit));
						feLog("slip\n%s\n",c_print(xformSlip));
*/
					}
				}
				else
				{
					//* use basic method for first segment

					SpatialTransform relativeMod=relative;

					if(clampStart)
					{
#if FALSE
						SpatialTransform segInverse;
						invert(segInverse,segRotation);

						SpatialTransform clamp=startClamp;
						set(clamp.translation());

						const SpatialTransform delta=clamp*segInverse;
						const SpatialEuler euler=delta;	//* ZXY
						SpatialTransform spiralClampRotation=
								SpatialQuaternion(euler[0],refArmUnit);
#else
						const SpatialVector startTangent=
								inc*segRotation.column(2);
						SpatialVector startNormal=startClampN;
						SpatialVector startCross;
						cross(startCross,startTangent,startNormal);
						cross(startNormal,startCross,startTangent);
						normalizeSafe(startNormal);

						SpatialQuaternion delta;
						set(delta,segRotation.column(1),startNormal);

						SpatialTransform spiralClampRotation=delta;
						setTranslation(spiralClampRotation,segCenter);
						translate(spiralClampRotation,-segCenter);
#endif

						relativeMod=spiralStartRotation *
								segRotation*spiralClampRotation;
						if(verbose)
						{
//							feLog("euler %s\n",c_print(euler));
							feLog("startTangent %s\n",c_print(startTangent));
							feLog("startNormal %s\n",c_print(startNormal));
							feLog("startCross %s\n",c_print(startCross));
							feLog("delta %s\n",c_print(delta));
							feLog("refArmUnit %s\n",c_print(refArmUnit));
							feLog("spiralClampRotation\n%s\n",
									c_print(spiralClampRotation));
							feLog("spiralStartRotation\n%s\n",
									c_print(spiralStartRotation));
						}
					}

					SpatialTransform firstTransform;
					setIdentity(firstTransform);

					firstTransform.translation()=vectorPivotBase;
					rotate(firstTransform,thisPitch,e_xAxis);
					translate(firstTransform,vectorYawGap);
					rotate(firstTransform,thisYaw,e_yAxis);
					translate(firstTransform,minusYawGap);
					translate(firstTransform,minusPivotBase);

					conversion=relativeMod*firstTransform;

					if(verbose)
					{
						feLog("firstTransform\n%s\n",c_print(firstTransform));
						feLog("relativeMod\n%s\n",c_print(relativeMod));
					}
				}

				if(pLinkState)
				{
					pLinkState->m_currentConversion=conversion;
				}

				lastConversion=conversion;

				SpatialVector lastBase;
				transformVector(conversion,vectorPivotBase,lastBase);

				transformVector(conversion,nextPivot,lastTip);

				SpatialVector lastTipUncorrected;
				transformVector(conversion,
						nextPivotUncorrected,lastTipUncorrected);

				if(spDrawGuide.isValid())
				{
					const Color black(0.0,0.0,0.0);
					const Color red(1.0,0.0,0.0);
					const Color green(0.0,1.0,0.0);
					const Color blue(0.0,0.0,1.0);

					SpatialVector line[2];

					line[0]=point0;
					line[1]=point1;

					Color color[2];

					color[0]=red;
					color[1]=green;

					spDrawGuide->drawLines(line,NULL,2,
							DrawI::e_strip,true,color);

					line[0]=lastBase;
					line[1]=lastTip;

					color[0]=red;
					color[1]=blue;

					spDrawGuide->drawLines(line,NULL,2,
							DrawI::e_strip,true,color);

					if(clampStart && !modelLinkCount)
					{
						line[0]=point0;
						line[1]=point0+startClampN;

						color[0]=black;
						color[1]=red;

						spDrawGuide->drawLines(line,NULL,2,
								DrawI::e_strip,true,color);
					}
					if(clampEnd && modelLinkCount==linkLimit-1)
					{
						line[0]=point1+endClampN;
						line[1]=point1;

						color[0]=black;
						color[1]=red;

						spDrawGuide->drawLines(line,NULL,2,
								DrawI::e_strip,true,color);
					}
				}


				if(verbose)
				{
					feLog("conversion %s\n",c_print(conversion));
					feLog("lastBase %s\n",c_print(lastBase));
					feLog("lastTip %s\n",c_print(lastTip));
				}

				lastArmUnit=unit(lastTip-lastBase);
				lastCenter=lastBase+lastArmUnit*magnitude(vectorPivotBase);

				if(create)
				{
					const SpatialTransform xformRef=
							m_linkRef[refIndex].m_transform;
					const SpatialTransform xform=
							linkScaling*xformRef*conversion;
					addLink(spLink,xform);

					if(verbose)
					{
						feLog("linkScaling %s\n",c_print(linkScaling));
						feLog("xformRef %s\n",c_print(xformRef));
						feLog("xform %s\n",c_print(xform));
					}

					m_fileLinkCount++;
					linkCount++;
				}

				// WARNING could short circuit to another bend in the curve
				const Real cv=nearestPointAlongCurve(
						lastTip,spPathVertices,primitiveIndex);
				const Real cvUncorrected=nearestPointAlongCurve(
						lastTipUncorrected,spPathVertices,primitiveIndex);

				Vector2 uv;
				set(uv,subCount? cv/(subCount-1.0): 0.0,0.0);

				Vector2 uvUncorrected;
				set(uvUncorrected,
						subCount? cvUncorrected/(subCount-1.0): 0.0,0.0);

				//* NOTE force step in U when nearest point seems wrong
				const Real uStep=uStepCount? uStepSum/Real(uStepCount): 0.0;

				const Real uCorrection=uv[0]-uvUncorrected[0];

				if(verbose)
				{
					feLog("%d u0 %.6G u1 %.6G cv %.6G/%d"
							" uv %.6G %.6G uStep %.6G count %d"
							" uCorrection %.6G\n",
							modelLinkCount,u0,u1,cv,subCount-1,
							uv[0],uv[1],uStep,uStepCount,uCorrection);
				}

				//* try to get back to nearest cv
				const Real uStepRestore=0.05;	//* TODO param

				if(stepProtect &&
						(u1 > 1.0 || (uv[0]<=u0) == (inc>0.0) ||
						(uStepCount &&
						fabs(uv[0]-u0-uStep-uCorrection) >
						fabs(maxStepChange*uStep))))
				{
					if(verbose)
					{
						feLog("%d FORCE STEP %.6G vs %.6G\n",modelLinkCount,
								fabs(uv[0]-u0-uStep-uCorrection),
								fabs(maxStepChange*uStep));
					}

					if(!uStepCount)
					{
						u0=u1;
					}
					else
					{
						u0=(1.0-uStepRestore)*(u0+uStep+uCorrection)+
								uStepRestore*uv[0];
					}
				}
				else
				{
					uStepSum+=uv[0]-u0;
					uStepCount++;

					u0=uv[0];
				}

				modelLinkCount++;
			}

			if(clampEnd)
			{
				const Real uFinish=fractionAtLength(spPathVertices,
						primitiveIndex,lastFinishLength);

				SpatialTransform lastInverse;
				invert(lastInverse,lastConversion);
#if FALSE

				SpatialTransform clamp=endClamp;
				set(clamp.translation());

				const SpatialTransform delta=clamp*lastInverse;

				const SpatialEuler euler=delta;	//* ZXY
				rSpiralCorrection += (euler[0]-spiralStepRadians)/maxCount;
#else
				const SpatialVector endTangent=lastConversion.column(2);
				SpatialVector endNormal=endClampN;
				SpatialVector endCross;
				cross(endCross,endTangent,endNormal);
				cross(endNormal,endCross,endTangent);
				normalizeSafe(endNormal);

				SpatialVector localNormal;
				rotateVector(lastInverse,endNormal,localNormal);

				const Real spin=atan2(localNormal[0],localNormal[1]);

				rSpiralCorrection-=fittingRate*spin/maxCount;
#endif

				rGapCorrection+=
						inc*fittingRate*(uEnd-uFinish)*fullLength/maxCount;

				if(verbose)
				{
					if(!pass)
					{
						feLog("\n");
					}

					feLog("lastConversion\n%s\n",c_print(lastConversion));
					feLog("endClampN %s\n",c_print(endClampN));
					feLog("endNormal %s\n",c_print(endNormal));
					feLog("localNormal %s\n",c_print(localNormal));
					feLog("spin %s\n",c_print(spin));

					feLog("pass %d u1 %.6G uFinish %.6G"
							" fullLength %.6G gapCorrection %.6G"
							" spiralCorrection %.6G\n",
							pass,u1,uFinish,fullLength,
							rGapCorrection,rSpiralCorrection);
				}
			}
		}
	}

	if(doTemporal)
	{
		m_linkState.resize(m_fileLinkCount);
		for(I32 linkIndex=0;linkIndex<m_fileLinkCount;linkIndex++)
		{
			LinkState& rLinkState=m_linkState[linkIndex];
			rLinkState.m_priorSlippage=rLinkState.m_currentSlippage;
			rLinkState.m_priorConversion=rLinkState.m_currentConversion;
			rLinkState.m_priorSet=TRUE;
		}
	}
	else
	{
		m_linkState.clear();
	}

	catalog<I32>("linkCount")=linkCount;
	catalog<I32>("fileLinkCount")=m_fileLinkCount;

	String summary;
	summary.sPrintf("%d %s%s",linkCount,
			outputTransforms? "triangle": "link",linkCount==1? "": "s");
	catalog<String>("summary")=summary;

	m_spOutputDraw=NULL;

	//* delete any extra links from recycle
	if(!outputTransforms)
	{
		removeExcessLinks();
	}

	if(cacheResult)
	{
#if FE_CHAIN_DEBUG
		feLog("ChainOp::handle cacheResult copy\n");
#endif

		spOutputAccessible->copy(m_spResultAccessible);
	}
	else
	{
		m_spResultAccessible=NULL;
	}

#if FE_CHAIN_DEBUG
	feLog("ChainOp::handle output count %d file %d\n",
			linkCount,m_fileLinkCount);
#endif
}

void ChainOp::removeExcessLinks(void)
{
	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spResultAccessible,
			e_primitive,e_vertices)) return;

	const I32 primitiveCount=spOutputVertices->count();

//	feLog("removeExcessLinks primitives %d/%d\n",
//			m_primitiveStart,primitiveCount);

	for(I32 primitiveIndex=primitiveCount-1;primitiveIndex>=m_primitiveStart;
			primitiveIndex--)
	{
		if(interrupted())
		{
			break;
		}

		spOutputVertices->remove(primitiveIndex);
	}

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spResultAccessible,
			e_point,e_position)) return;

	const I32 pointCount=spOutputPoint->count();

//	feLog("removeExcessLinks points %d/%d\n",
//			m_pointStart,pointCount);

	for(I32 pointIndex=pointCount-1;pointIndex>=m_pointStart;pointIndex--)
	{
		if(interrupted())
		{
			break;
		}

		spOutputPoint->remove(pointIndex);
	}
}

void ChainOp::addLink(sp<SurfaceAccessibleI> a_spLink,
	const SpatialTransform& a_transform)
{
	const I32 priorPointStart=m_pointStart;
	const I32 priorPrimitiveStart=m_primitiveStart;

	const String linkName=catalog<String>("linkName");
	const String pattern=catalog<String>("pattern");
	const I32 startIndex=catalog<I32>("startIndex");

	String number;
	number.sPrintf("%d",startIndex+m_fileLinkCount);
	const String linkString=linkName.replace(pattern,number);

	SpatialTransform xformMod=a_transform;

	const U32 flipTwistCount=m_flipTwistPatterns.size();
	for(U32 flipTwistIndex=0;flipTwistIndex<flipTwistCount;flipTwistIndex++)
	{
		const String& pattern=m_flipTwistPatterns[flipTwistIndex];
		if(number.match(pattern))
		{
			rotate(xformMod,180.0*degToRad,e_zAxis);
			break;
		}
	}

	BWORD updateNames=TRUE;

	if(catalog<bool>("outputTransforms"))
	{
		SpatialVector vert[3];
		vert[0]=xformMod.translation();
		vert[1]=vert[0]+xformMod.column(1);
		vert[2]=vert[0]+xformMod.column(0);

		m_spOutputDraw->drawTriangles(vert,NULL,NULL,3,
				DrawI::e_discrete,false,NULL);

		m_primitiveStart++;
		m_pointStart+=3;
	}
	else
	{
		sp<SurfaceAccessorI> spOutputPoint;
		if(!access(spOutputPoint,m_spResultAccessible,
				e_point,e_position)) return;

		sp<SurfaceAccessorI> spOutputNormal;
		if(!access(spOutputNormal,m_spResultAccessible,
				e_point,e_normal)) return;

		const I32 pointCountOutput=spOutputPoint->count();

		sp<SurfaceAccessorI> spLinkVertices;
		if(!access(spLinkVertices,a_spLink,e_primitive,e_vertices)) return;

		sp<SurfaceAccessorI> spLinkPoint;
		if(!access(spLinkPoint,a_spLink,e_point,e_position)) return;

		sp<SurfaceAccessorI> spLinkNormal;
		access(spLinkNormal,a_spLink,e_point,e_normal,e_quiet);

		const I32 primitiveCountLink=spLinkVertices->count();
		const I32 pointCountLink=spLinkPoint->count();

		if(m_pointStart>=pointCountOutput)
		{
			m_spResultAccessible->append(a_spLink,&xformMod);

			m_primitiveStart+=primitiveCountLink;
			m_pointStart+=pointCountLink;
		}
		else
		{
			//* link already exists at index, just move points

			if(spLinkNormal.isValid())
			{
				for(I32 pointIndex=0;pointIndex<pointCountLink;pointIndex++)
				{
					const SpatialVector point=
							spLinkPoint->spatialVector(pointIndex);
					const SpatialVector norm=
							spLinkNormal->spatialVector(pointIndex);

					SpatialVector transformed;
					transformVector(xformMod,point,transformed);

					SpatialVector rotated;
					rotateVector(xformMod,norm,rotated);

					spOutputPoint->set(m_pointStart+pointIndex,transformed);
					spOutputNormal->set(m_pointStart+pointIndex,rotated);
				}
			}
			else
			{
				for(I32 pointIndex=0;pointIndex<pointCountLink;pointIndex++)
				{
					const SpatialVector point=
							spLinkPoint->spatialVector(pointIndex);

					SpatialVector transformed;
					transformVector(xformMod,point,transformed);

					spOutputPoint->set(m_pointStart+pointIndex,transformed);
				}
			}

			const String oldLinkString=
					m_spPrimitiveLinkName->string(m_primitiveStart);
			if(oldLinkString==linkString)
			{
				updateNames=FALSE;
			}

			m_primitiveStart+=primitiveCountLink;
			m_pointStart+=pointCountLink;
		}
	}

	if(updateNames)
	{
		if(m_spPointLinkName.isValid())
		{
			for(I32 pointIndex=priorPointStart;
					pointIndex<m_pointStart;pointIndex++)
			{
				m_spPointLinkName->set(pointIndex,linkString);
			}
		}

		if(m_spPrimitiveLinkName.isValid())
		{
			for(I32 primitiveIndex=priorPrimitiveStart;
					primitiveIndex<m_primitiveStart;primitiveIndex++)
			{
				m_spPrimitiveLinkName->set(primitiveIndex,linkString);
			}
		}
	}
}

void ChainOp::calcBounds(I32 a_fragmentIndex,SpatialVector& a_center,
		SpatialVector& a_innermin,SpatialVector& a_innermax,
		SpatialVector& a_outermin,SpatialVector& a_outermax)
{
	const BWORD fragment=catalog<bool>("fragment");

	const Real huge=1e9;
	const SpatialVector huge3(huge,huge,huge);
	a_outermin= -huge3;
	a_outermax=huge3;

	sp<SurfaceAccessibleI::FilterI> spFilter;

	if(fragment)
	{
		const String fragmentKey=m_spScanVertices->fragment(a_fragmentIndex);

//		feLog("ChainOp::calcBounds fragment %d key \"%s\"\n",
//				a_fragmentIndex,fragmentKey.c_str());

		if(!m_spScanVertices->filterWith(fragmentKey,spFilter) ||
				spFilter.isNull())
		{
			feLog("ChainOp::calcBounds filtering failed\n");
			return;
		}
	}

	BWORD started=FALSE;
	const U32 filterCount=fragment? spFilter->filterCount():
			m_spScanVertices->count();
	for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const U32 primitiveIndex=
				fragment? spFilter->filter(filterIndex): filterIndex;

		const U32 subCount=m_spScanVertices->subCount(primitiveIndex);

//		feLog("filter %d/%d prim %d verts %d\n",
//				filterIndex,filterCount,primitiveIndex,subCount);

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const SpatialVector point=m_spScanVertices->spatialVector(
					primitiveIndex,subIndex);

//			feLog("  vert %d/%d point %s\n",
//					subIndex,subCount,c_print(point));

			if(!started)
			{
				a_outermin=point;
				a_outermax=point;
				started=TRUE;
			}
			else
			{
				for(U32 m=0;m<3;m++)
				{
					if(a_outermin[m]>point[m])
					{
						a_outermin[m]=point[m];
					}
					if(a_outermax[m]<point[m])
					{
						a_outermax[m]=point[m];
					}
				}
			}
		}
	}

	a_center=0.5*(a_outermax+a_outermin);
	a_outermax-=a_center;
	a_outermin-=a_center;

	a_innermin= -huge3;
	a_innermax=huge3;

	for(U32 m=0;m<3;m++)
	{
		const U32 passes=20;
		U32 pass=0;
		for(;pass<passes;pass++)
		{
			const Real limit=fabs((0.01+0.02*pass)*a_outermax[m]);

			a_innermin[m]= -huge;
			a_innermax[m]=huge;

			for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				if(interrupted())
				{
					break;
				}

				const U32 primitiveIndex=
						fragment? spFilter->filter(filterIndex): filterIndex;

				const U32 subCount=m_spScanVertices->subCount(primitiveIndex);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=m_spScanVertices->spatialVector(
							primitiveIndex,subIndex)-a_center;

//					if(fabs(point[(m+1)%3])>limit[m] ||
//							fabs(point[(m+2)%3])>limit[m])
					if(fabs(point[(m+1)%3])+fabs(point[(m+2)%3])>limit)
					{
						continue;
					}

					if(point[m]>0.0)
					{
						if(a_innermax[m]>point[m])
						{
							a_innermax[m]=point[m];
						}
					}
					else
					{
						if(a_innermin[m]<point[m])
						{
							a_innermin[m]=point[m];
						}
					}
				}
			}

			if(a_innermin[m]>a_outermin[m] &&
				a_innermax[m]<a_outermax[m])
			{
				break;
			}
		}
	}

	if(((a_innermin[0]>a_outermin[0]) +
		(a_innermin[1]>a_outermin[1]) +
		(a_innermin[2]>a_outermin[2]) < 2) ||
		((a_innermax[0]<a_outermax[0]) +
		(a_innermax[1]<a_outermax[1]) +
		(a_innermax[2]<a_outermax[2]) < 2))
	{
		feLog("ChainOp::calcBounds link inner scan failed -> guessing\n");
		a_innermin=0.7*a_outermin;
		a_innermax=0.7*a_outermax;
	}
}

void ChainOp::scanInputLinks(void)
{
//	feLog("\nChainOp::scanInputLinks\n");

	const BWORD fragment=catalog<bool>("fragment");

	m_linkRef.clear();

	const U32 fragmentCount=fragment? m_spScanVertices->fragmentCount(): 1;
	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		if(interrupted())
		{
			break;
		}

		SpatialVector center;
		SpatialVector innermin;
		SpatialVector innermax;
		SpatialVector outermin;
		SpatialVector outermax;

		calcBounds(fragmentIndex,center,innermin,innermax,outermin,outermax);

#if FE_CHAIN_DEBUG_SCAN
		feLog("fragment %d/%d\n",fragmentIndex,fragmentCount);
		feLog("center %s\n",c_print(center));
		feLog("innermin %s\n",c_print(innermin));
		feLog("innermax %s\n",c_print(innermax));
		feLog("outermin %s\n",c_print(outermin));
		feLog("outermax %s\n",c_print(outermax));
#endif

		// count should equal primitiveIndex
		U32 count=m_linkRef.size();
		m_linkRef.resize(count+1);
		LinkRef& refLink=m_linkRef[count];

		setIdentity(refLink.m_transform);

		U32& rAxisForward=refLink.m_axisForward;
//		U32 axis_side=0;
		if(outermax[2]>outermax[0] && outermax[2]>outermax[1])
		{
			rAxisForward=2;
			if(outermax[1]>outermax[0])
			{
//				axis_side=1;
				rotate(refLink.m_transform,90.0*degToRad,e_zAxis);
			}
		}
		else if(outermax[1]>outermax[0])
		{
			rAxisForward=1;
			if(outermax[2]>outermax[0])
			{
//				axis_side=2;
				rotate(refLink.m_transform,90.0*degToRad,e_yAxis);
			}
			rotate(refLink.m_transform,90.0*degToRad,e_xAxis);
		}
		else
		{
			if(outermax[2]>outermax[1])
			{
//				axis_side=2;
			}
			else
			{
//				axis_side=1;
				rotate(refLink.m_transform,90.0*degToRad,e_xAxis);
			}
			rotate(refLink.m_transform,90.0*degToRad,e_yAxis);
		}

//		feLog("rAxisForward %d axis_side %d\n",rAxisForward,axis_side);

		Real diameter_base=innermin[rAxisForward]-outermin[rAxisForward];
		Real diameter_tip=outermax[rAxisForward]-innermax[rAxisForward];
		Real pivotBase=outermin[rAxisForward]+0.5*diameter_base;
		Real pivotTip=outermax[rAxisForward]-0.5*diameter_tip;
//		Real pivotGap=-diameter_tip;
//		Real pivotYaw=outermin[rAxisForward]+1.5*diameter_base;

#if FE_CHAIN_DEBUG_SCAN
		feLog("pivotBase %.6G pivotTip %.6G\n",
				pivotBase,pivotTip);
//		feLog(" pivotGap %.6G pivotYaw %.6G\n",
//				pivotGap,pivotYaw);
#endif

		refLink.m_pivotBase=pivotBase;
		refLink.m_pivotTip=pivotTip;

		// pivotGap=pivot_gap_tip (first)+pivot_gap_base (second)
		refLink.m_pivotGapBase=-0.5*diameter_base;
		refLink.m_pivotGapTip=-0.5*diameter_tip;

		// pivotYaw=pivot_yaw_tip (first)+pivot_yaw_base (second)
		refLink.m_pivotYawBase=outermin[rAxisForward]+diameter_base;
		refLink.m_pivotYawTip=0.5*diameter_base;

		translate(refLink.m_transform,-center);
	}
}

void ChainOp::LinkRef::log(char* a_label)
{
	feLog("ChainOp::LinkRef %s\n",a_label);
	feLog("m_pivotBase %.6G m_pivotTip %.6G\n",
			m_pivotBase,m_pivotTip);
	feLog("m_pivotGapBase %.6G m_pivotGapTip %.6G\n",
			m_pivotGapBase,m_pivotGapTip);
	feLog("m_pivotYawBase %.6G m_pivotYawTip %.6G\n",
			m_pivotYawBase,m_pivotYawTip);
	feLog("m_transform %s\n",c_print(m_transform));
}
