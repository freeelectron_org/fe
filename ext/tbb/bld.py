import sys
import os
import re

import utility
import common
forge = sys.modules["forge"]

def prerequisites():
    return ["thread"]

def setup(module):
    module.summary = []

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")

    versions = []

    tbb_sources = utility.safer_eval(os.environ["FE_TBB_SOURCES"])

    if tbb_sources == []:
        module.summary += [ "none" ]
        return

    for tbb_source in reversed(sorted(tbb_sources)):
        condition_variable_header = os.path.join(tbb_source[2],"tbb", "compat", "condition_variable")
        if not os.path.exists(tbb_source[2]) or not os.path.exists(tbb_source[3]) or not os.path.exists(condition_variable_header):
            module.summary += [ "-" + tbb_source[0] ]
            continue

        tbb_lib = common.find_tbb_lib(tbb_source[3])
        if tbb_lib == '':
            module.summary += [ "-" + tbb_source[0] ]
            continue

        if tbb_source[0] in versions:
            continue
        versions += [ tbb_source[0] ]

        if tbb_source[0] == "":
            module.summary += [ "/" ]
        else:
            module.summary += [ tbb_source[0] ]

#       module.summary += [ tbb_lib ]

        srcList = [ "WorkTbb",
                    "tbb.pmh",
                    "tbbDL" ]

        variant = tbb_source[0]
        if variant != "":
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink('.', variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        dllname = "fexTbb" + variant

        with open(manifest, 'a') as outfile:
            suffix = ""
            if variant != "":
                suffix = "." + variant
            outfile.write('\tspManifest->catalog<String>('+
                    '"WorkForceI.WorkTbb.fe'+suffix+'")='+
                    '"fexTbb'+variant+'";\n');

        dll = module.DLL( dllname, srcList )

        # HACK icc conflict with Houdini's boost
        alt_cxx = ""
        if forge.compiler_brand == "icc":
            alt_cxx = "g++"

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)
            srcTarget.includemap = { 'tbb' : tbb_source[2] }

            if alt_cxx != "":
                srcTarget.cxx = alt_cxx

#           srcTarget.cppmap = {}
#           if variant != "" and int(variant) < 9000:
#               srcTarget.cppmap['std'] = forge.use_std("c++98")
#           else:
#               srcTarget.cppmap['std'] = forge.use_std("c++11")

        dll.linkmap = { "tbb" :
                "-L" + tbb_source[3] + ' -l' + tbb_lib }

#       if tbb_source[0] == '':
        dll.linkmap["tbb"] += ' -Wl,-rpath=' + tbb_source[3]

        deplibs = forge.basiclibs + [ "fexThreadDLLib" ]

        forge.deps( [ dllname + "Lib" ], deplibs )
