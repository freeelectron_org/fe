/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <typeinfo>
#include "fe/plugin.h"
#include "xInterface.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();
	BWORD retain=(argc>1);

	BWORD completed=FALSE;

	try
	{
		feLog("Memory::newWorks() %s\n",Memory::newWorks()? "true": "false");

		//* hold onto a component after the Registry is gone
		sp<Component> spComponent;

		{
			sp<Registry> spRegistry(new Registry());
			spRegistry->initializeAll();

			Result result=spRegistry->manage("xPluginLibrary2");
			UNIT_TEST(successful(result));

			result=spRegistry->manage("xPluginLibrary");
			UNIT_TEST(successful(result));

			sp<MyComponentI> spMyComponentI(
					spRegistry->create("MyComponentI"));
			UNIT_TEST(spMyComponentI.isValid());
			if(spMyComponentI.isValid())
			{
				U32 square=spMyComponentI->test(7);
				UNIT_TEST(square==49);
				feLog("square=%d\n",square);
			}

			Regex regex1("MyC.*tI\\..*");
			sp<MyComponentI> spRegexComponent1(
					spRegistry->create(regex1));
			UNIT_TEST(spRegexComponent1.isValid());

			Regex regex2("MyC.*tI.\\..*");
			sp<MyComponentI> spRegexComponent2(
					spRegistry->create(regex2));
			UNIT_TEST(spRegexComponent2.isNull());

			sp<MyComponentI> spFail(spRegistry->create("non-existant"));
			UNIT_TEST(spFail.isNull());

			//* bad sp cast: should create and release
			spFail=spRegistry->create("MyComponent2I");
			UNIT_TEST(spFail.isNull());

			if(retain)
			{
				spComponent=spMyComponentI;
			}

			feLog("-- end of block --\n");
		}

		feLog("-- out of block --\n");

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	feLog("-- reporting --\n");
	UNIT_TEST(completed);
	UNIT_TEST(Counted::trackerCount()==(FE_COUNTED_TRACK && retain? 1: 0));
	UNIT_TRACK(10);
	UNIT_RETURN();
}
