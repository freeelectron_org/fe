/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>
#include "whereami.h"
#include "whereami.c"

#include <chrono>

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#include "dirent.h"
#else
#include <dirent.h>
#endif

#if FE_COMPILER==FE_GNU || FE_COMPILER==FE_INTEL
#include <cxxabi.h>
fe::String fe_demangle(const fe::String a_symbol,BWORD a_emptyDefault)
{
	char* buffer=NULL;
	FE_UWORD length=0;
	I32 status = 0;

	char* demangled=
			abi::__cxa_demangle(a_symbol.c_str(),buffer,&length,&status);

	fe::String result=demangled? demangled: (a_emptyDefault? "": a_symbol);

	free(demangled);

	return result;
}
#else
fe::String fe_demangle(const fe::String a_symbol,BWORD a_emptyDefault)
{
	return a_symbol;
}
#endif

namespace fe
{

String System::demangle(const String a_symbol,BWORD a_emptyDefault)
{
	return fe_demangle(a_symbol,a_emptyDefault);
}

void System::printBannerOnce(void)
{
	if(gs_bannerPrinted)
	{
		return;
	}

	const String verbose=System::getVerbose();

	fe_printf("Free Electron - %s\n",System::buildDate().c_str());

	if(verbose!="none")
	{
#if FE_CODEGEN==FE_VALIDATE
		fe_printf("%susing validate build\n",PrefixLog::prefix().c_str());
#elif FE_CODEGEN==FE_DEBUG
		fe_printf("%susing debug build\n",PrefixLog::prefix().c_str());
#elif FE_CODEGEN==FE_PROFILE
		fe_printf("%susing profile build\n",PrefixLog::prefix().c_str());
#endif
	}

	gs_bannerPrinted=TRUE;
}

bool System::getEnvironmentVariable(const String &name, String &value)
{
	bool rv = false;
#if FE_OS==FE_LINUX || FE_OS==FE_OSX
	char *cp;
	cp = getenv(name.c_str());
	if ((char *) 0 != cp)
	{
		value = cp;
		rv = true;
	}
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	unsigned long cp;
	const int size = 4096;
	char val[size];
	cp = GetEnvironmentVariableA(name.c_str(), val, size);
	if (0 != cp)
	{
		value = val;
		rv = true;
	}
#else
#error
#endif
	return rv;
}

bool System::getHomePath(String &value)
{
#if FE_OS==FE_LINUX || FE_OS==FE_OSX
	return getEnvironmentVariable("HOME",value);
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	String homeDrive;
	if(!getEnvironmentVariable("HomeDrive",homeDrive))
	{
		return false;
	}

	String homePath;
	if(!getEnvironmentVariable("HomePath",homePath))
	{
		return false;
	}

	value=homeDrive+homePath;
	return true;
#else
#error
#endif
	return false;
}

String System::getVerbose(void)
{
	String verbose;

#if FE_CODEGEN<=FE_DEBUG
	verbose="all";
#else
	System::getEnvironmentVariable("FE_VERBOSE", verbose);
	if(verbose.empty())
	{
		verbose="none";
	}
#endif

	return verbose;
}

//* static
String System::buildDate(void)
{
	return __DATE__;
}

//* static
String System::getExePath(BWORD a_prune)
{
	const I32 bufferLength=4096;
	char buffer[bufferLength];
	buffer[0]=0;

#if FE_OS==FE_LINUX || FE_OS==FE_OSX

	const ssize_t length=readlink("/proc/self/exe",buffer,bufferLength);
	if(length>bufferLength-1)
	{
		return "<PATH TOO LONG>";
	}

	String path=buffer;

	if(!a_prune)
	{
		return path;
	}

	// NOTE Boost regex can miss the second /../ of /../../
	// since the patterns overlap, so we repeat the operator

	String oldPath;
	while(path!=oldPath)
	{
		oldPath=path;
		path=path.replace("/\\.\\./","/@@/");
	}

	oldPath="";
	while(path!=oldPath)
	{
		oldPath=path;
		path=oldPath.replace("/[^/@]*/@@/","/");
	}

#if TRUE
	path=path.replace("/[^/]*$","");
	path=path.replace("/lib/.*$","");
	path=path.replace("/lib$","");
#else
	path=path.pathname().pathname().pathname();
#endif

	return path;
#else

	const I32 length=GetModuleFileNameA(NULL,buffer,bufferLength);
	if(length>bufferLength-1)
	{
		return "<PATH TOO LONG>";
	}

	return buffer;
#endif

	return "";
}

//* static
String System::getLoadPath(void* a_symbol,BWORD a_prune)
{
#if FE_OS==FE_LINUX || FE_OS==FE_OSX
	Dl_info dlInfo;
	if(!dladdr(a_symbol,&dlInfo))
	{
		fe_fprintf(stderr,"getLoadPath failed\n");
		return "";
	}

	String path=dlInfo.dli_fname;

	if(!a_prune)
	{
		return path;
	}

	// NOTE Boost regex can miss the second /../ of /../../
	// since the patterns overlap, so we repeat the operator

	String oldPath;
	while(path!=oldPath)
	{
		oldPath=path;
		path=path.replace("/\\.\\./","/@@/");
	}

	oldPath="";
	while(path!=oldPath)
	{
		oldPath=path;
		path=oldPath.replace("/[^/@]*/@@/","/");
	}

	return path;
#else
	char buffer[4096];
	HMODULE hmodule(NULL);

	if(!GetModuleHandleExA(
			GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
			GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
			(LPCSTR)a_symbol,&hmodule))
	{
		fe_fprintf(stderr,"GetModuleHandleEx failed\n");
		return "";
	}
	if(!GetModuleFileNameA(hmodule,buffer,sizeof(buffer)))
	{
		fe_fprintf(stderr,"GetModuleFileName failed\n");
		return "";
	}

	return String(buffer).substitute("\\","/");
#endif

	return "";
}

String System::getCurrentWorkingDirectory(void)
{
#if FE_OS==FE_LINUX || FE_OS==FE_OSX
	char* pBuffer=getcwd(NULL,0);
	String cwd(pBuffer);
	free(pBuffer);
	return cwd;
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	const I32 bufferLength=4096;
	char buffer[bufferLength];
	buffer[0]=0;

	GetCurrentDirectoryA(bufferLength,buffer);
	return buffer;
#else
#error
#endif
	return "";
}

bool System::setCurrentWorkingDirectory(const String &a_path)
{
	bool rv = false;
#if FE_OS==FE_LINUX || FE_OS==FE_OSX
	rv = (0 == chdir(a_path.c_str()));
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	rv = SetCurrentDirectoryA(a_path.c_str());
#else
#error
#endif
	return rv;
}

//* NOTE in C==17 we can just switch these over to use
//* std::filesystem::create_directory and std::filesystem::create_directories

bool System::createDirectory(const String &a_path)
{
	bool rv = false;
#if FE_OS==FE_LINUX || FE_OS==FE_OSX
	rv = !mkdir(a_path.c_str(),0777);
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	rv = !_mkdir(a_path.c_str());
#else
#error
#endif
	return rv;
}

bool System::createParentDirectories(const String &a_filename)
{
	String remainder=a_filename;
	String path=remainder.parse("","/");
	while(!remainder.empty())
	{
		if(!createDirectory(path) && errno!=EEXIST)
		{
			return false;
		}
		path+="/"+remainder.parse("","/");
	}

	return true;
}

bool System::listDirectory(String a_path,std::vector<String>& a_rEntries)
{
	DIR* pDIR=opendir(a_path.c_str());

	if(!pDIR)
	{
		return false;
	}

	struct dirent *pEntry;
	while( (pEntry=readdir(pDIR)) )
	{
		a_rEntries.push_back(pEntry->d_name);
	}
	closedir(pDIR);
	return true;
}

String System::getExecutablePath(void)
{
	char *path = NULL;
	int length, dirname_length;

	length = wai_getExecutablePath(NULL, 0, &dirname_length);
	if (length > 0)
	{
		path = (char *)malloc(length + 1);
		if (!path)
			abort();
		wai_getExecutablePath(path, length, &dirname_length);
		path[length] = '\0';
	}

	String exePath(path);

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	exePath = exePath.substitute("\\", "/");
#endif

	return exePath;
}

String System::getModulePath(void)
{
	char *path = NULL;
	int length, dirname_length;

	length = wai_getModulePath(NULL, 0, &dirname_length);
	if (length > 0)
	{
		path = (char *)malloc(length + 1);
		if (!path)
			abort();
		wai_getModulePath(path, length, &dirname_length);
		path[length] = '\0';
	}

	String modulePath(path);

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	modulePath = modulePath.substitute("\\", "/");
#endif

	return modulePath;
}

String System::getBinarySuffix(void)
{
	String binary_suffix;
	if (getEnvironmentVariable("FE_BINARY_SUFFIX", binary_suffix))
	{
		return binary_suffix;
	}
#ifdef FE_BINARY_SUFFIX
	else
	{
		return FE_STRING(FE_BINARY_SUFFIX);
	}
#endif

	return "";
}

String System::findFile(String a_filename,std::vector<String> a_pathArray)
{
	const I32 pathCount=a_pathArray.size();
	for(I32 pathIndex=0;pathIndex<pathCount;pathIndex++)
	{
		const String& path=a_pathArray[pathIndex];

		String fullname=path+"/"+a_filename;
		std::ifstream testFile;
		testFile.open(fullname.c_str());
		if(testFile)
		{
			testFile.close();
			return path;
		}
	}

	return "";
}

String System::findFile(String a_filename,String a_delimitedPaths,
	String a_delimiter)
{
	std::vector<String> pathArray;

	String path;
	while(!(path=a_delimitedPaths.parse("\"",a_delimiter)).empty())
	{
		pathArray.push_back(path);
	}

	return findFile(a_filename,pathArray);
}

String System::localTime(String a_format)
{
    std::chrono::system_clock::time_point tp=std::chrono::system_clock::now();
    time_t tt = std::chrono::system_clock::to_time_t(tp);

    tm now;
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
    localtime_s(&now, &tt);
#else
    localtime_r(&tt, &now);
#endif

    const std::chrono::duration<double> tse=tp.time_since_epoch();
    std::chrono::seconds::rep milliseconds=
			std::chrono::duration_cast<std::chrono::milliseconds>(tse).count()
			%1000;
	String ms;
	ms.sPrintf("%03d",milliseconds);

	const I32 bufferLength=a_format.length()+64;
	char* buffer=new char[bufferLength];
	strftime(buffer,bufferLength,a_format.c_str(),&now);

	String result(buffer);
	delete[] buffer;

	result=result.substitute("${MILLISECONDS}",ms);
	return result;
}

} /* namespace */

