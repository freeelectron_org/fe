/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MayaDraw_h__
#define __maya_MayaDraw_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw into Maya MFnMesh

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT MayaDraw:
	public DrawCommon,
	public CastableAs<MayaDraw>
{
	public:
					MayaDraw(void):
						m_pFnMesh(NULL)										{}
virtual				~MayaDraw(void)
					{
						if(m_pFnMesh)
						{
							delete m_pFnMesh;
						}
					}

					//* as DrawI

					using DrawCommon::drawPoints;

virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawLines;

virtual	void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

					//* Maya specific
		void		setMeshData(const MDataHandle a_meshData)
					{
						m_meshData=a_meshData;

						if(m_pFnMesh)
						{
							delete m_pFnMesh;
						}
						m_pFnMesh=new MFnMesh(m_meshData.asMesh());

					}
const	MDataHandle	meshData(void)
					{	return m_meshData; }

	private:
		MDataHandle	m_meshData;
		MFnMesh*	m_pFnMesh;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MayaDraw_h__ */
