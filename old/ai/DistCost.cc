/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DistCost.h"

namespace fe
{

DistCost::DistCost(void)
{
}

DistCost::~DistCost(void)
{
}

Real DistCost::cost(const SpatialVector &a_from, const SpatialVector &a_to)
{
	if(a_to[0] >= 3.5 && a_to[0] <= 4.0)
	{
		if(a_to[1] >= 2.0 && a_to[1] <= 8.0)
		{
			//if(a_to[2] >= 1.0 && a_to[2] <= 3.0)
			{
				return 1000.0;
			}
		}
	}
	return magnitude(a_from-a_to);
}

} /* namespace */

