/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "group.pmh"

namespace fe
{
namespace group
{

Projector::Projector(void)
{
	m_attributeNames.resize(3);
	m_attributeNames[0] = "input_group";
	m_attributeNames[1] = "output_group";
	m_attributeNames[2] = "project_layout";
}

Projector::~Projector(void)
{
}

void Projector::handle(Record &signal)
{
	if(		m_aInput.check(signal)
		&&	m_aOutput.check(signal)
		&&	m_aLayout.check(signal))
	{
		sp<Layout> spLayout = m_spScope->lookupLayout(m_aLayout(signal));
		if(!spLayout.isValid())
		{
			return;
		}

		// make sure layout is locked (we'll be using offsetTable())
		if(!spLayout->locked()) { spLayout->lock(); }

		for(RecordGroup::iterator it = m_aInput(signal)->begin();
			it != m_aInput(signal)->end(); it++)
		{
			sp<RecordArray> spRA = *it;
			sp<Layout> spL = spRA->layout();

			// make sure layout is locked (we'll be using offsetTable())
			if(!spL->locked()) { spL->lock(); }

			// check if layouts overlap properly
			bool overlap = true;
			for(UWORD t = 0; t < m_spScope->getAttributeCount(); t++)
			{
				if(		spLayout->offsetTable()[t] != offsetNone
					&&	spL->offsetTable()[t] == offsetNone)
				{
					overlap = false;
				}
			}

			// if overlap, create new records and add to output
			if(overlap)
			{
				sp<RecordArray> spNewRA = m_spScope->createRecordArray(
					spLayout, spRA->length());
				for(UWORD t = 0; t < m_spScope->getAttributeCount(); t++)
				{
					if(		spLayout->offsetTable()[t] != offsetNone
						&&	spL->offsetTable()[t] != offsetNone)
					{
						sp<Attribute> spAttribute = m_spScope->attribute(t);
						sp<BaseType> spBT = spAttribute->type();
						for(int i = 0; i < spRA->length(); i++)
						{
							spBT->assign(
								(void *)((char *)(spNewRA->data(i))
									+ spLayout->offsetTable()[t]),
								(void *)((char *)(spRA->data(i))
									+ spL->offsetTable()[t]));
						}
					}
				}
				for(int i = 0; i < spNewRA->length(); i++)
				{
					m_aOutput(signal)->add(spNewRA->getRecord(i));
				}
			}

		}
	}
}

void Projector::bind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_spScope = spLayout->scope();
	doSetup();
}

void Projector::parse(std::vector<String> &attributeNames)
{
	if(attributeNames.size() != 3)
	{
		feX("Projector::setup",
			"expecting 3 attribute names");
	}
	m_attributeNames = attributeNames;
	doSetup();
}

void Projector::doSetup(void)
{
	if(!m_spScope.isValid())
	{
		return;
	}
	m_aInput.initialize(m_spScope,m_attributeNames[0]);
	m_aOutput.initialize(m_spScope,m_attributeNames[1]);
	m_aLayout.initialize(m_spScope,m_attributeNames[2]);
}

} /* namespace */
} /* namespace */

