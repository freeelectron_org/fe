/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __field_DensitySink_h__
#define __field_DensitySink_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "math/math.h"
#include "shape/shape.h"
namespace fe
{
namespace ext
{

/**	Removes 'value' from a field at a rate dependent on local density.
	The removed value is accumulated.
	*/
class FE_DL_EXPORT DensitySink : public Initialize<DensitySink>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				DensitySink(void);
virtual			~DensitySink(void);
		void	initialize(void);

		// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsParticle					m_asParticle;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __field_DensitySink_h__ */
