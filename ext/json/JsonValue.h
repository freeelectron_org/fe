/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_JsonValue_h__
#define __json_JsonValue_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Json::Value wrapped for sp<>

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT JsonValue: public Component
{
	public:
		Json::Value&	value(void)		{ return m_jsonValue; }

	private:
		Json::Value		m_jsonValue;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __json_JsonValue_h__ */
