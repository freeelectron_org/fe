import sys
forge = sys.modules["forge"]

def setup(module):

    setupTestDL(module)

    exe = module.Exe("xOrbitTest", [
        "xOrbitTest",
    ])

    deplibs = forge.corelibs + [
        # imgui deps
        "fexImguiDLLib",
    ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexSignalLib",
                "fexGeometryDLLib",
                "fexMechanicsDLLib",
                "fexMoaDLLib",
                "fexOperateDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib",
                "fexSurfaceDLLib" ]

    forge.deps(["xOrbitTestExe"], deplibs)

#   forge.tests += [
#       ("xOrbitTest.exe",      "", "", None) ]

def setupTestDL(module):

    dll = module.DLL("moaTestDL", [
        "moaTestDL",
        "moa_test.pmh",
    ])

    deplibs = forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexSignalLib",
                "fexMechanicsDLLib",
                "fexMoaDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib" ]

    forge.deps(["moaTestDLLib"], deplibs)
