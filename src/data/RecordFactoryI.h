/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordFactoryI_h__
#define __data_RecordFactoryI_h__

namespace fe
{

/**************************************************************************//**
	@brief Component to create and initialize a Record

	@ingroup data
*//***************************************************************************/
class FE_DL_EXPORT RecordFactoryI:
	virtual public Component,
	public CastableAs<RecordFactoryI>
{
	public:
virtual	void		bind(sp<Scope>& rspScope)								=0;
virtual	sp<Layout>	layout(void)											=0;
virtual	void		produce(Record& rRecord)								=0;
virtual	void		finalize(Record& rRecord)								=0;
};

} /* namespace */

#endif /* __data_RecordFactoryI_h__ */

