/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleBase_h__
#define __surface_SurfaceAccessibleBase_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Common Functionality for Accessible Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleBase:
	public ObjectSafeShared<SurfaceAccessibleBase>,
	virtual public SurfaceAccessibleI,
	public CastableAs<SurfaceAccessibleBase>
{
	public:
								SurfaceAccessibleBase(void);
virtual							~SurfaceAccessibleBase(void);

								//* as SurfaceAccessibleI
virtual	BWORD					isBound(void)	{ return FALSE; }
virtual	void					bind(Instance a_instance)					{}
virtual	void					bind(sp<Scope> a_scope)						{}
virtual	void					bind(sp<SurfaceAccessibleI::ThreadingState>
										a_spThreadingState)
								{	m_spThreadingState=a_spThreadingState; }

virtual	BWORD					threadable(void)			{ return TRUE; }

virtual	void					lock(I64 a_id);
virtual	void					lock(void)		{ lock(0); }

virtual	void					unlock(I64 a_id);
virtual	void					unlock(void)	{ unlock(0); }

virtual	BWORD					load(String a_filename)
								{	return load(a_filename,sp<Catalog>(NULL)); }
virtual	BWORD					load(String a_filename,sp<Catalog> a_spSettings);
virtual	BWORD					save(String a_filename)
								{	return save(a_filename,sp<Catalog>(NULL)); }
virtual	BWORD					save(String a_filename,sp<Catalog> a_spSettings)
								{	return FALSE; }

virtual	void					groupNames(Array<String>& a_rNameArray,
										SurfaceAccessibleI::Element
										a_element) const
								{	a_rNameArray.clear(); }

								//* NOTE You must supply at least one of the
								//* attributeSpecs methods.
								//* These defaults will be be cross-dependent.
virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										SurfaceAccessibleI::Element
										a_element) const
								{	attributeSpecs(a_rSpecs,"",a_element); }
virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

virtual	void					clear(void)		{ reset(); }

virtual	I32						count(String a_node,
										SurfaceAccessibleI::Element
										a_element) const;
virtual	I32						count(SurfaceAccessibleI::Element
										a_element) const
								{	return count("",a_element); }

								//* NOTE Derived classes must supply
								//* the two 5-arg accessor methods.

								using SurfaceAccessibleI::accessor;
virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,
										Creation a_create,Writable a_writable)
								{
									return sp<SurfaceAccessorI>(NULL);
								}

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create,Writable a_writable)
								{
									return sp<SurfaceAccessorI>(NULL);
								}

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,
										Creation a_create)
								{	return accessor(a_node,a_element,a_name,
											a_create,e_readOnly); }

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create)
								{	return accessor(a_node,
											a_element,a_attribute,
											a_create,e_readOnly); }

virtual sp<SurfaceAccessorI>	accessor(Element a_element,String a_name,
										Creation a_create)
								{	return accessor("",a_element,a_name,
											a_create,e_readOnly); }

virtual sp<SurfaceAccessorI>	accessor(Element a_element,
										Attribute a_attribute,
										Creation a_create)
								{	return accessor("",a_element,a_attribute,
											a_create,e_readOnly); }

virtual sp<SurfaceAccessorI>	accessor(Element a_element,String a_name)
								{	return accessor(a_element,a_name,
											e_createMissing); }

virtual sp<SurfaceAccessorI>	accessor(Element a_element,
										Attribute a_attribute)
								{	return accessor(a_element,a_attribute,
											e_createMissing); }

virtual	I32						discardPattern(
										SurfaceAccessibleI::Element a_element,
										String a_pattern);
virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name);
virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute);

virtual	sp<SurfaceI>			surface(void)
								{	return surface(String(),
										SurfaceI::e_unrestricted); }
virtual	sp<SurfaceI>			surface(String a_group)
								{	return surface(a_group,
										SurfaceI::e_unrestricted); }
virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

virtual	sp<SurfaceI>			subSurface(U32 a_subIndex)
								{	return subSurface(a_subIndex,String(),
										SurfaceI::e_unrestricted); }
virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group)
								{	return subSurface(a_subIndex,a_subIndex,
										SurfaceI::e_unrestricted); }
virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group,
										SurfaceI::Restrictions a_restrictions);

virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible)
								{	reset();
									append(a_spSurfaceAccessible,NULL); }

virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										String a_nodeName)
								{	reset();
									Array<SpatialTransform> transformArray;
									append(a_spSurfaceAccessible,a_nodeName,
											transformArray,sp<FilterI>(NULL)); }

virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										sp<FilterI> a_spFilter)
								{	reset();
									append(a_spSurfaceAccessible,
											NULL,a_spFilter); }

virtual	void					instance(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										const Array<SpatialTransform>&
										a_rTransformArray);

virtual	void					append(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										const SpatialTransform*
										a_pTransform)
								{	append(a_spSurfaceAccessible,
											a_pTransform,sp<FilterI>(NULL)); }

		void					append(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										String a_nodeName,
										const Array<SpatialTransform>&
										a_rTransformArray,
										sp<FilterI> a_spFilter);

								//* promote?
		void					append(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										const SpatialTransform*
										a_pTransform,
										sp<FilterI> a_spFilter);

								//* default implementation is non-atomic
virtual	sp<SpannedRange>		atomize(AtomicChange a_atomicChange,
										String a_group,U32 a_desiredCount);

virtual	sp<SpannedRange>		atomizeConnectivity(AtomicChange a_atomicChange,
										String a_group,U32 a_desiredCount,
										BWORD a_checkPages);

virtual void					setPaging(BWORD a_paging)
								{	m_paging=a_paging; }
virtual BWORD					paging(void) const	{ return m_paging; }

virtual void					preparePaging(AtomicChange a_atomicChange,
										String a_group)						{}
virtual I32						pointPage(U32 a_pointIndex) const
								{	return 0; }
virtual I32						primitivePage(U32 a_primitiveIndex) const
								{	return 0; }

virtual BWORD					copyOutline(Array<String>&
										a_rStringArray) const;

static	String					elementLayout(
										SurfaceAccessibleI::Element a_element);
static	String					attributeString(SurfaceAccessibleI::Attribute
										a_attribute);

		String					findNameAttribute(void);

		void					setAttributeToDefault(Element a_element,
										I32 a_start,I32 a_count,
										const Spec& a_rSpec);

virtual	sp<Component>			payload(void)	{ return sp<Component>(NULL); }

static	BWORD					specsContain(const Array<Spec>& a_rSpecs,
										const Spec& a_rSpec);

	class FE_DL_EXPORT MultiGroup:
		public Counted,
		public CastableAs<MultiGroup>
	{
		public:
							MultiGroup(void):
								m_iterator(m_indices.begin()),
								m_currentPosition(0)						{}
	virtual					~MultiGroup(void)								{}

			BWORD			contains(I32 a_index) const
							{	return m_indices.find(a_index) !=
										m_indices.end(); }

			U32				count(void)	const	{ return m_indices.size(); }
			I32				at(I32 a_position);
			void			insert(I32 a_index)
							{
								m_indices.insert(a_index);
								m_iterator=m_indices.begin();
								m_currentPosition=0;
							}
			void			insert(I32 a_first,I32 a_last)
							{
								for(I32 index=a_first;index<=a_last;index++)
								{	m_indices.insert(index); }
								m_iterator=m_indices.begin();
								m_currentPosition=0;
							}
			void			insert(std::set<I32>& a_rIntSet)
							{
								for(I32 index: a_rIntSet)
								{	m_indices.insert(index); }
								m_iterator=m_indices.begin();
								m_currentPosition=0;
							}
			void			erase(I32 a_index)
							{
								m_indices.erase(a_index);
								m_iterator=m_indices.begin();
								m_currentPosition=0;
							}
			void			erase(I32 a_first,I32 a_last)
							{
								for(I32 index=a_first;index<=a_last;index++)
								{	m_indices.erase(index); }
								m_iterator=m_indices.begin();
								m_currentPosition=0;
							}
	virtual	void			add(I32 a_index)	{ insert(a_index); }
	virtual	void			remove(I32 a_index)	{ erase(a_index); }

		private:
			std::set<I32>				m_indices;
			std::set<I32>::iterator		m_iterator;
			I32							m_currentPosition;
			sp<SurfaceAccessibleBase>	m_spSurfaceAccessibleBase;
	};

virtual	I32						deleteElements(
										SurfaceAccessibleI::Element a_element,
										String a_groupString,
										BWORD a_retainGroups);

virtual	I32						deleteElements(
										SurfaceAccessibleI::Element a_element,
										std::set<I32>& a_rIntSet,
										BWORD a_retainGroups);

virtual	I32						deleteElements(
										SurfaceAccessibleI::Element a_element,
										sp<SurfaceAccessibleBase::MultiGroup>
										a_spMultiGroup,
										BWORD a_retainGroups);

								//* convert impl data in generic set of indices
virtual	sp<MultiGroup>			generateGroup(
										SurfaceAccessibleI::Element a_element,
										String a_groupString);

		BWORD					removeGroup(
										SurfaceAccessibleI::Element a_element,
										String a_groupString);
		BWORD					hasGroup(
										SurfaceAccessibleI::Element a_element,
										String a_groupString);
		sp<MultiGroup>			group(SurfaceAccessibleI::Element a_element,
										String a_groupString);

		std::map<String, sp<MultiGroup> >&	groupMap(
										SurfaceAccessibleI::Element a_element);

	protected:
virtual	void					reset(void);

static	String					commonName(String a_attrName);
static	String					attributeName(String a_specName);

		SurfaceAccessibleI::Threading	threading(void) const
								{
									if(m_spThreadingState.isNull())
									{
										return SurfaceAccessibleI::e_unknown;
									}
									return Threading(
											m_spThreadingState->threading());
								}

		void					outlineClear(void);
		void					outlineAppend(String a_line);
		void					outlineCreateDefault(void);

		sp<SurfaceAccessibleI::ThreadingState>	m_spThreadingState;

		BWORD					m_paging;
		I64						m_locker;
		Array<String>			m_outline;

		String					addGroupRanges(
										sp<SurfaceAccessibleBase::MultiGroup>,
										String a_groupString);

		std::map<String, sp<MultiGroup> >	m_pointGroupMap;
		std::map<String, sp<MultiGroup> >	m_primitiveGroupMap;
};

inline I32 SurfaceAccessibleBase::deleteElements(
	SurfaceAccessibleI::Element a_element,std::set<I32>& a_rIntSet,
	BWORD a_retainGroups)
{
//	feLog("SurfaceAccessibleBase::deleteElements '%s' count %d retain %d\n",
//			elementLayout(a_element).c_str(),a_rIntSet.count(),
//			a_retainGroups);

	if(!a_rIntSet.size())
	{
		return 0;
	}

	sp<MultiGroup> spMultiGroup=generateGroup(a_element,"");
	if(spMultiGroup.isNull())
	{
		feLog("SurfaceAccessibleBase::deleteElements MultiGroup NULL\n");
		return 0;
	}

	spMultiGroup->insert(a_rIntSet);

	return deleteElements(a_element,spMultiGroup,a_retainGroups);
}


} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleBase_h__ */
