/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

#define FE_TOP_DEBUG			FALSE
#define FE_TOP_SIGNAL_DEBUG		FALSE
#define FE_TOP_THREAD_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

TreeOp::TreeOp(void):
	m_lastFrame(1e6),
	m_lastPointCount(0),
	m_lastPrimitiveCount(0)
{
}

TreeOp::~TreeOp(void)
{
#if FE_TOP_SIGNAL_DEBUG
	Peeker peeker;
	if(m_spSignalerI.isValid())
	{
		m_spSignalerI->peek(peeker);
	}

	const String peek=peeker.str();
	if(!peek.empty())
	{
		feLog("\n~TreeOp\n%s\n",peek.c_str());
	}
#endif
}

void TreeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* page Config

	catalog<String>("Threads","page")="Config";
	catalog<String>("AutoThread","page")="Config";
	catalog<String>("Paging","page")="Config";
	catalog<String>("StayAlive","page")="Config";
	catalog<String>("Work","page")="Config";
	catalog<String>("Work")="Tbb";

	catalog<SpatialVector>("TreeLocation");
	catalog<String>("TreeLocation","label")="Tree Location";
	catalog<String>("TreeLocation","page")="Config";
	catalog<String>("TreeLocation","hint")=
			"Displacement of trunk base from origin.";

	catalog<SpatialVector>("gravity")=SpatialVector(0.0,-9.8,0.0);
	catalog<String>("gravity","page")="Config";
	catalog<String>("gravity","label")="Gravity";
	catalog<String>("gravity","hint")=
			"Acceleration caused by the weight of the world.";

	catalog<SpatialVector>("WindDirection");
	catalog<String>("WindDirection","page")="Config";
	catalog<String>("WindDirection","label")="Uniform Wind";
	catalog<String>("WindDirection","hint")=
			"Wind speed mulitplied by the unit direction."
			"  All parts of the plant receive the same velocity."
			"  For a tree given as an Input Surface,"
			" see Velocity Attribute for non-uniform values.";

	catalog<bool>("YUp")=true;
	catalog<String>("YUp","label")="Y Is Up";
	catalog<String>("YUp","page")="Config";
	catalog<String>("YUp","hint")=
			"Indicate that the Y axis points up instead of the Z axis.";

	catalog<bool>("payload")=false;
	catalog<String>("payload","label")="Output as Payload";
	catalog<String>("payload","page")="Config";

	catalog<bool>("writeRadius")=false;
	catalog<String>("writeRadius","label")="Write Radius";
	catalog<String>("writeRadius","page")="Config";
	catalog<String>("writeRadius","hint")=
			"Write the radius as a real point attribute"
			" on the Output Surface.";

	//* page Generate

	catalog<String>("TreeName")="Aspen_Quaking";
	catalog<String>("TreeName","label")="Tree Name";
	catalog<String>("TreeName","page")="Generate";
	catalog<String>("TreeName","hint")="Tree growth configuration"
			" (ignored when input geometry is provided).";

	catalog<String>("stickForm")="none";
	catalog<String>("stickForm","label")="Branch Form";
	catalog<String>("stickForm","choice:0")="none";
	catalog<String>("stickForm","label:0")="None";
	catalog<String>("stickForm","choice:1")="line";
	catalog<String>("stickForm","label:1")="Line";
	catalog<String>("stickForm","choice:2")="cylinder";
	catalog<String>("stickForm","label:2")="Cylinder";
	catalog<String>("stickForm","choice:3")="curve";
	catalog<String>("stickForm","label:3")="Curve";
	catalog<String>("stickForm","page")="Generate";
	catalog<String>("stickForm","hint")=
			"Geometry used to output branch segments.";

	catalog<String>("leafForm")="none";
	catalog<String>("leafForm","label")="Leaf Form";
	catalog<String>("leafForm","choice:0")="none";
	catalog<String>("leafForm","label:0")="None";
	catalog<String>("leafForm","choice:1")="point";
	catalog<String>("leafForm","label:1")="Point";
//	catalog<String>("leafForm","choice:2")="circle";
//	catalog<String>("leafForm","label:2")="circle";
	catalog<String>("leafForm","page")="Generate";
	catalog<String>("leafForm","hint")=
			"Geometry used to output each leaf.";

	//* page Input

	catalog<String>("radiusAttr")="radius";
	catalog<String>("radiusAttr","label")="Radius Attribute";
	catalog<String>("radiusAttr","page")="Input";
	catalog<String>("radiusAttr","hint")=
			"Name of real point attribute on the Input Surface"
			" describing the stem radius."
			"  If omitted, an algorithm will compute a radius"
			" based on the Area Ratio Power.";

	catalog<Real>("areaRatioPower")= -3.0;
	catalog<Real>("areaRatioPower","low")= -4.0;
	catalog<Real>("areaRatioPower","high")= -2.0;
	catalog<Real>("areaRatioPower","min")= -6.0;
	catalog<Real>("areaRatioPower","max")= -1.0;
	catalog<String>("areaRatioPower","label")="Area Ratio Power";
	catalog<String>("areaRatioPower","page")="Input";
	catalog<String>("areaRatioPower","hint")=
			"Logarithm to base 10 of the ratio of the base cross-sectional"
			" area of a stem to its length."
			"  Grass may be at -4 or -5, while trees are usually near -3."
			"  This is only used if no radius attribute is found.";

	catalog<String>("velocityAttr")="velocity";
	catalog<String>("velocityAttr","label")="Velocity Attribute";
	catalog<String>("velocityAttr","page")="Input";
	catalog<String>("velocityAttr","hint")=
			"Name of vector point attribute on the Input Surface"
			" describing the wind velocity."
			"  This value will be reread for each frame."
			"  If omitted, only the Uniform Wind will be used.";

	catalog<String>("rigidityAttr")="rigidity";
	catalog<String>("rigidityAttr","label")="Rigidity Attribute";
	catalog<String>("rigidityAttr","page")="Input";
	catalog<String>("rigidityAttr","hint")=
			"Name of real point attribute on the Input Surface"
			" scaling the structural stiffness."
			"  This value will only read on a rebuild."
			"  If omitted, only the uniform Rigidity will be used.";

	catalog<Real>("rigidity")=1.0;
	catalog<Real>("rigidity","high")=1.0;
	catalog<Real>("rigidity","max")=1e3;
	catalog<String>("rigidity","label")="Uniform Rigidity";
	catalog<String>("rigidity","page")="Input";
	catalog<bool>("rigidity","joined")=true;
	catalog<String>("rigidity","hint")=
			"Scale all springs in the plant."
			"  The input radius, by attribute or area ratio,"
			" is the primary influence of spring strengths."
			"  This value just softens or hardens all the springs uniformly."
			"  If the Rigidity Attribute is found, this value scales"
			" each provided rigidity value.";

	catalog<Real>("damping")=0.0;
	catalog<Real>("damping","high")=1.0;
	catalog<Real>("damping","max")=1e3;
	catalog<String>("damping","label")="Uniform Damping";
	catalog<String>("damping","page")="Input";
	catalog<String>("damping","hint")=
			"Add a sluggishness to all springs in the plant."
			"  This is not usually helpful.";

	catalog<String>("targetAttr")="target";
	catalog<String>("targetAttr","label")="Target Attribute";
	catalog<String>("targetAttr","page")="Input";
	catalog<String>("targetAttr","hint")=
			"Name of vector point attribute on the Input Surface"
			" describing the desired position of each point."
			"  This value will be reread for each frame.";

	catalog<String>("targetedAttr")="targeted";
	catalog<String>("targetedAttr","label")="Targeted Attribute";
	catalog<String>("targetedAttr","page")="Input";
	catalog<String>("targetedAttr","hint")=
			"Name of integer point attribute on the Input Surface"
			" describing whether to pull towards the target for each point."
			"  This value will be reread for each frame.";

	catalog<bool>("compensation")=true;
	catalog<String>("compensation","label")="Angle Compensation";
	catalog<String>("compensation","page")="Input";
	catalog<bool>("compensation","joined")=true;
	catalog<String>("compensation","hint")=
			"Compute an error due to angular recombination and then"
			" adjust the rest positions to help alleviate the issue.";

	catalog<bool>("correction")=true;
	catalog<String>("correction","label")="Correction";
	catalog<String>("correction","page")="Input";
	catalog<String>("correction","hint")=
			"Compute an error due to the solver space conversion and then"
			" apply an adjustment to every simulated result.";

	//* page Collision

	catalog<String>("collisionMethod")="boundingSphere";
	catalog<String>("collisionMethod","label")="Collision Method";
	catalog<String>("collisionMethod","choice:0")="boundingSphere";
	catalog<String>("collisionMethod","label:0")="Bounding Sphere";
	catalog<String>("collisionMethod","choice:1")="partSpheres";
	catalog<String>("collisionMethod","label:1")="Partition Spheres";
	catalog<String>("collisionMethod","choice:2")="impactRadial";
	catalog<String>("collisionMethod","label:2")="Impact Radial";
	catalog<String>("collisionMethod","choice:3")="impactNormal";
	catalog<String>("collisionMethod","label:3")="Impact Normal";
	catalog<String>("collisionMethod","page")="Collision";
	catalog<bool>("collisionMethod","joined")=true;
	catalog<String>("collisionMethod","hint")=
			"Determines how to interpret the collider surface."
			"  Bounding Sphere simplfies the collider to a single sphere."
			"  This bounding sphere is automatically computed after each change"
			" in the collider surface."
			"  Impact Radial checks for an intersection towards"
			" the collider center and reacts away from the center.";

	catalog<bool>("collideEnd")=true;
	catalog<String>("collideEnd","label")="Collide With Segment Ends";
	catalog<String>("collideEnd","page")="Collision";
	catalog<String>("collideEnd","hint")=
			"Collide with the end of each segment instead of computing"
			" a contact point along each segment."
			"  This may produce better results with relatively large colliders"
			" against smaller plants, such as with grass."
			"  Computing the contact point is probably better for"
			" relatively small colliders against larger plants,"
			" such as with trees.";

	catalog<String>("colliderPartitionAttr")="part";
	catalog<String>("colliderPartitionAttr","label")="Partition Attr";
	catalog<String>("colliderPartitionAttr","page")="Collision";
	catalog<String>("colliderPartitionAttr","hint")=
			"Primitive string attribute on collider to distinguish partitions"
			" using the Collision Method of Partition Spheres.";

	catalog<Real>("threshold")=0.1;
	catalog<Real>("threshold","high")=10.0;
	catalog<Real>("threshold","max")=1e6;
	catalog<String>("threshold","label")="Collision Threshold";
	catalog<String>("threshold","page")="Collision";
	catalog<bool>("threshold","joined")=true;
	catalog<String>("threshold","hint")=
			"Buffer distance added around collider.";

	catalog<Real>("reactivity")=0.1;
	catalog<Real>("reactivity","high")=1.0;
	catalog<Real>("reactivity","max")=10.0;
	catalog<String>("reactivity","label")="Reactivity";
	catalog<String>("reactivity","page")="Collision";
	catalog<String>("reactivity","hint")=
			"Controls how much response that collisions produce."
			"  Reduced reactivity can sometimes calm an overly excited plant.";

	catalog<Real>("depletion")=0.0;
	catalog<Real>("depletion","high")=1.0;
	catalog<String>("depletion","label")="Depletion";
	catalog<String>("depletion","page")="Collision";
	catalog<bool>("depletion","joined")=true;
	catalog<String>("depletion","hint")=
			"Loss of velocity near collider.";

	catalog<Real>("depletionFalloff")=1.0;
	catalog<Real>("depletionFalloff","high")=10.0;
	catalog<Real>("depletionFalloff","max")=1e6;
	catalog<String>("depletionFalloff","label")="Falloff";
	catalog<String>("depletionFalloff","page")="Collision";
	catalog<String>("depletionFalloff","hint")=
			"Distance of fading depletion effect beyond collider.";

	catalog<Real>("repulsion")=0.0;
	catalog<Real>("repulsion","high")=100.0;
	catalog<Real>("repulsion","max")=1e6;
	catalog<String>("repulsion","label")="Repulsion";
	catalog<String>("repulsion","page")="Collision";
	catalog<bool>("repulsion","joined")=true;
	catalog<String>("repulsion","hint")=
			"Peak force of repulsion around collider.";

	catalog<Real>("repulsionFalloff")=1.0;
	catalog<Real>("repulsionFalloff","high")=10.0;
	catalog<Real>("repulsionFalloff","max")=1e6;
	catalog<String>("repulsionFalloff","label")="Falloff";
	catalog<String>("repulsionFalloff","page")="Collision";
	catalog<String>("repulsionFalloff","hint")=
			"Distance of fading repulsion effect beyond collider.";

	catalog<Real>("windHampering")=1.0;
	catalog<Real>("windHampering","high")=1.0;
	catalog<String>("windHampering","label")="Wind Hampering";
	catalog<String>("windHampering","page")="Collision";
	catalog<bool>("windHampering","joined")=true;
	catalog<String>("windHampering","hint")=
			"Scale down wind response near collisions.";

	catalog<Real>("windFalloff")=1.0;
	catalog<Real>("windFalloff","high")=10.0;
	catalog<Real>("windFalloff","max")=1e6;
	catalog<String>("windFalloff","label")="Falloff";
	catalog<String>("windFalloff","page")="Collision";
	catalog<String>("windFalloff","hint")=
			"Ramp down hampering outward from collisions by this distance.";

	catalog<bool>("plastic")=false;
	catalog<String>("plastic","label")="Plastic Collisions";
	catalog<String>("plastic","page")="Collision";
	catalog<bool>("plastic","joined")=true;
	catalog<String>("plastic","hint")=
			"Retain some effect from collisions after the collider moves away.";

	catalog<Real>("plasticity")=1.0;
	catalog<Real>("plasticity","high")=1.0;
	catalog<String>("plasticity","label")="Plasticity";
	catalog<String>("plasticity","page")="Collision";
	catalog<String>("plasticity","hint")=
			"Fraction of collision effect to retain for each frame.";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Collider Surface");
	catalog<bool>("Collider Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<bool>("Output Surface","temporal")=true;
//	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;

	//* HACK TBB isn't good with free-form threading
//	Mutex::confirm("fexBoostThread1_56");
	Mutex::confirm();

	m_spSignalerI=registry()->create("SignalerI");
	if(m_spSignalerI.isNull())
	{
		feX("TreeOp::initialize", "can't create SignalerI");
	}

	sp<HandlerI> spSimPlant=registry()->create("*.SimPlant");
	if(spSimPlant.isNull())
	{
		feX("TreeOp::initialize", "can't create SimPlant");
	}

	sp<HandlerI> spDrawPlant=registry()->create("*.DrawPlant");
	if(spDrawPlant.isNull())
	{
		feX("TreeOp::initialize", "can't create DrawPlant");
	}

	m_modifierArray.push_back(spSimPlant);
	m_modifierArray.push_back(spDrawPlant);

	m_spScope=registry()->master()->catalog()->catalogComponent(
			"Scope","PlantScope");
	FEASSERT(m_spScope.isValid());

	m_forestRV.bind(m_spScope);
	m_forestRV.createRecord();
	set(m_forestRV.effectorVelocity());

	m_plantRV.bind(m_spScope);

	m_spSignalerI->insert(spSimPlant,m_forestRV.layout());
	m_spSignalerI->insert(spDrawPlant,m_forestRV.layout());
}

SpatialVector TreeOp::convertY(SpatialVector a_vector)
{
	return m_yUp?
			SpatialVector(a_vector[0],-a_vector[2],a_vector[1]): a_vector;
}

SpatialVector TreeOp::unconvertY(SpatialVector a_vector)
{
	return m_yUp?
			SpatialVector(a_vector[0],a_vector[2],-a_vector[1]): a_vector;
}

BWORD TreeOp::rebuild(void)
{
//	feLog("TreeOp::rebuild\n");

	setName("TreeOp (skeleton)");

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,e_primitive,e_vertices))
	{
		return FALSE;
	}

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,m_spInputAccessible,e_point,e_generic))
	{
		return FALSE;
	}

	const I32 pointCount=spInputPoints->count();
	const I32 primitiveCount=spInputVertices->count();

	m_forestRV.plants()->clear();
	I32 plantCount=0;

	Array<I32> plantOfPoint(pointCount,-1);

	m_primitivesOfPlant.resize(primitiveCount);

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spInputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		BWORD prepend=FALSE;
		I32 maxPlantIndex= -1;
		Array<U32> mergePrimitives;

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					spInputVertices->integer(primitiveIndex,subIndex);
			const I32 plantIndex=plantOfPoint[pointIndex];

			if(maxPlantIndex>=0 && plantIndex>=0 && maxPlantIndex!=plantIndex)
			{
				I32 lowIndex=maxPlantIndex;
				I32 highIndex=plantIndex;
				if(lowIndex>highIndex)
				{
					lowIndex=plantIndex;
					highIndex=maxPlantIndex;
					prepend=TRUE;
				}

				//* replace all highIndex with lowIndex; collapse into highIndex
				for(I32 pointIndex2=0;pointIndex2<pointCount;pointIndex2++)
				{
					I32& rIndex=plantOfPoint[pointIndex2];
					if(rIndex==highIndex)
					{
						rIndex=lowIndex;
					}
					else if(rIndex>highIndex)
					{
						rIndex--;
					}
				}

				maxPlantIndex=lowIndex;
				plantCount--;

				Array<U32>& rPrimitives=m_primitivesOfPlant[highIndex];
				for(Array<U32>::iterator it=rPrimitives.begin();
						it!=rPrimitives.end();it++)
				{
					const I32 primitiveIndex2= *it;
					mergePrimitives.push_back(primitiveIndex2);
				}

				for(I32 plantIndex2=highIndex;plantIndex2<plantCount;
						plantIndex2++)
				{
					m_primitivesOfPlant[plantIndex2]=
							m_primitivesOfPlant[plantIndex2+1];
				}
				m_primitivesOfPlant[plantCount].clear();
			}
			else if(maxPlantIndex<plantIndex)
			{
				maxPlantIndex=plantIndex;
				if(subIndex)
				{
					prepend=TRUE;
				}
			}
		}

		if(maxPlantIndex<0)
		{
			maxPlantIndex=plantCount++;
		}

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					spInputVertices->integer(primitiveIndex,subIndex);
			plantOfPoint[pointIndex]=maxPlantIndex;
		}

		if(prepend)
		{
			Array<U32>& rPrimitives=m_primitivesOfPlant[maxPlantIndex];
			const I32 count=rPrimitives.size()+1;
			rPrimitives.resize(count);
			for(I32 index=count-1;index>0;index--)
			{
				rPrimitives[index]=rPrimitives[index-1];
			}
			rPrimitives[0]=primitiveIndex;
		}
		else
		{
			m_primitivesOfPlant[maxPlantIndex].push_back(primitiveIndex);
		}

		for(Array<U32>::iterator it=mergePrimitives.begin();
				it!=mergePrimitives.end();it++)
		{
			const I32 primitiveIndex2= *it;
			m_primitivesOfPlant[maxPlantIndex].push_back(primitiveIndex2);
		}

//		feLog("prim %d plant %d\n",primitiveIndex,maxPlantIndex);
	}

	m_primitivesOfPlant.resize(plantCount);
	m_modelOfPlant.resize(plantCount);

	m_plantPointMap.resize(plantCount);
	m_plantOffset.resize(plantCount);

	const I32 threadCount=Thread::hardwareConcurrency();	//* TODO param

	const BWORD multiThread=(plantCount>1 && threadCount>1);

//	feLog("plantCount %d threadCount %d\n",plantCount,threadCount);

//~	if(multiThread)
//~	{
//~		if(m_spGang.isNull())
//~		{
//~			m_spGang=new Gang<TreeWorker,I32>();
//~		}
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::rebuild start\n");
//~#endif
//~
//~		m_spGang->start(sp<Counted>(this),threadCount,"rebuild");
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::rebuild started\n");
//~#endif
//~	}

	m_bundleSize=1;
//~	I32 bundleCount=1;

	if(multiThread)
	{
		m_bundleSize=0.51*plantCount/threadCount;
		if(m_bundleSize<1)
		{
			m_bundleSize=1;
		}
//~		bundleCount=(plantCount+m_bundleSize-1)/m_bundleSize;

//		feLog("bundleSize %d bundleCount %d\n",m_bundleSize,bundleCount);
	}

	//* NOTE avoid repeated factory searches
	std::map<String,Registry::FactoryLocation*>& rFactoryMap=
			registry()->factoryMap();
	Registry::FactoryLocation* pFactoryLocation=
			rFactoryMap["PlantModelI.Tree.fe"];

//~	I32 jobCount=0;

	for(I32 plantIndex=0;plantIndex<plantCount;plantIndex++)
	{
		Record plant=m_plantRV.createRecord();
		m_plantRV.plantName()="<skeleton>";
		m_plantRV.location()=convertY(catalog<SpatialVector>("TreeLocation"));

		sp<PlantModelI>& rspPlantModelI=m_modelOfPlant[plantIndex];

		if(rspPlantModelI.isNull())
		{
			if(pFactoryLocation)
			{
				rspPlantModelI=pFactoryLocation->m_spLibrary->create(
						pFactoryLocation->m_factoryIndex);
			}
			else
			{
				rspPlantModelI=registry()->create("PlantModelI.Tree");
			}
		}
		else
		{
			rspPlantModelI->reset();
		}
		if(rspPlantModelI.isNull())
		{
			feLog("TreeOp::rebuild could not create Tree\n");
			break;
		}

		m_plantRV.plantModel()=rspPlantModelI;

		m_forestRV.plants()->add(plant);

		m_plantRV.unbind();

//~		if(multiThread && jobCount<I32(plantIndex/m_bundleSize))
//~		{
//~			m_spGang->post(jobCount++);
//~		}
	}

//	feLog("posted %d/%d\n",jobCount,bundleCount);

	if(multiThread)
	{
		setNonAtomicRange(0,plantCount-1);
		runStage("rebuild");

//~		for(I32 bundleIndex=jobCount;bundleIndex<bundleCount;bundleIndex++)
//~		{
//~			m_spGang->post(bundleIndex);
//~			jobCount++;
//~		}
//~
//~		for(I32 threadIndex=0;threadIndex<threadCount;threadIndex++)
//~		{
//~			m_spGang->post(-1);
//~			jobCount++;
//~		}
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::rebuild posted\n");
//~#endif
//~
//~		I32 delivered;
//~		while(jobCount)
//~		{
//~//			feLog("TreeOp::rebuild jobCount %d\n",jobCount);
//~			while(m_spGang->acceptDelivery(delivered))
//~			{
//~				jobCount--;
//~			}
//~
//~			if(jobCount)
//~			{
//~				milliSleep(1);
//~			}
//~		}
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::rebuild finish\n");
//~#endif
//~
//~		m_spGang->finish();
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::rebuild finished\n");
//~#endif
	}
	else
	{
		rebuildPlants(0,plantCount);
	}

	m_primitivesOfPlant.clear();

//	feLog("TreeOp::rebuild done\n");

	return TRUE;
}

//~void TreeOp::runStage(String a_stage)
//~{
//~	const U32 plantCount=m_modelOfPlant.size();
//~	const I32 threadCount=Thread::hardwareConcurrency();
//~	const I32 bundleSize=0.51*plantCount/threadCount;
//~
//~//	feLog("TreeOp::runStage plantCount %d threadCount %d bundleSize %d\n",
//~//			plantCount,threadCount,bundleSize);
//~
//~	//* TODO selective
//~	const BWORD quiet=FALSE;
//~	sp<WorkForceI> spWorkForceI=
//~			registry()->create("WorkForceI.WorkTbb",quiet);
//~
//~	if(spWorkForceI.isValid())
//~	{
//~//		feLog("TreeOp::runStage using \"%s\"\n",
//~//				spWorkForceI->name().c_str());
//~
//~		sp<SpannedRange> spSpannedRange(new SpannedRange());
//~		spSpannedRange->nonAtomic().add(0,plantCount-1);
//~
//~		sp<SpannedRange> spFullRange=
//~				spSpannedRange->combineAtoms(bundleSize);
//~
//~		spWorkForceI->run(sp<WorkI>(this),threadCount,
//~				spFullRange,a_stage);
//~	}
//~}

void TreeOp::run(I32 a_id,sp<SpannedRange> a_spSpannedRange,String a_stage)
{
#if FE_TOP_THREAD_DEBUG
	feLog("TreeOp::run %d stage \"%s\"\n%s\n",
			a_id,a_stage.c_str(),c_print(a_spSpannedRange));
#endif

	const SpannedRange::MultiSpan& rMultiSpan=a_spSpannedRange->nonAtomic();
	const I32 spanCount=rMultiSpan.spanCount();
	for(I32 spanIndex=0;spanIndex<spanCount;spanIndex++)
	{
		const SpannedRange::Span& rSpan=rMultiSpan.span(spanIndex);

		const I32 plantIndex=rSpan[0];
		const I32 count=rSpan[1]-rSpan[0]+1;

//		if(count>1)
//		{
//			feLog("TreeOp::run %d count %d\n",plantIndex,count);
//		}

		if(a_stage=="rebuild")
		{
//			feLog("TreeOp::run %d rebuild %d\n",a_id,plantIndex);
			rebuildPlants(plantIndex,count);
		}
		else if(a_stage=="updateInput")
		{
//			feLog("TreeOp::run %d updateInput %d\n",a_id,plantIndex);
			updateInputPlants(plantIndex,count);
		}
		else if(a_stage=="replace")
		{
//			feLog("TreeOp::run %d replace %d\n",a_id,plantIndex);
			replacePlants(plantIndex,count);
		}
	}
}

//* TODO remove (using Work instead)
void TreeOp::TreeWorker::operate(void)
{
#if FE_TOP_THREAD_DEBUG
	feLog("TreeOp::TreeWorker::operate %d \"%s\"\n",m_id,m_stage.c_str());
#endif

	I32 job= -1;
	while(TRUE)
	{
		if(m_hpJobQueue->take(job))
		{
			if(job<0)
			{
				m_hpJobQueue->deliver(job);
				break;
			}

			const I32 bundleSize=m_hpTreeOp->m_bundleSize;

			I32 plantIndex=job*bundleSize;

#if FE_TOP_THREAD_DEBUG
			feLog("TreeOp::TreeWorker::operate %d job %d plant %d+\n",
					m_id,job,plantIndex);
#endif

			if(m_stage=="rebuild")
			{
				m_hpTreeOp->rebuildPlants(plantIndex,bundleSize);
			}
			else if(m_stage=="updateInput")
			{
				m_hpTreeOp->updateInputPlants(plantIndex,bundleSize);
			}
			else if(m_stage=="replace")
			{
				m_hpTreeOp->replacePlants(plantIndex,bundleSize);
			}

#if FE_TOP_THREAD_DEBUG
			feLog("TreeOp::TreeWorker::operate %d job %d finished\n",
					m_id,job);
#endif

			m_hpJobQueue->deliver(job);
		}
		else
		{
#if FE_TOP_THREAD_DEBUG
			feLog("TreeOp::TreeWorker::operate %d no jobs\n",m_id);
#endif
			milliSleep(1);
		}
	}

#if FE_TOP_THREAD_DEBUG
	feLog("TreeOp::TreeWorker::operate %d done\n",m_id);
#endif
}

void TreeOp::rebuildPlants(I32 a_start,I32 a_count)
{
	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,e_primitive,e_vertices))
	{
		return;
	}

	sp<SurfaceAccessorI> spInputRadius;
	const String radiusAttr=catalog<String>("radiusAttr");
	if(!radiusAttr.empty())
	{
		access(spInputRadius,m_spInputAccessible,e_point,radiusAttr,e_quiet);
	}

	sp<SurfaceAccessorI> spInputRigidity;
	const String rigidityAttr=catalog<String>("rigidityAttr");
	if(!rigidityAttr.empty())
	{
		access(spInputRigidity,m_spInputAccessible,
				e_point,rigidityAttr,e_quiet);
	}

	const Real rigidity=catalog<Real>("rigidity");
	const Real damping=catalog<Real>("damping");
	const Real areaRatioPower=catalog<Real>("areaRatioPower");
	const BWORD compensation=catalog<BWORD>("compensation");
	const BWORD correction=catalog<BWORD>("correction");

	const I32 maxPlantIndex=m_modelOfPlant.size()-1;

	for(I32 subIndex=0;subIndex<a_count;subIndex++)
	{
		const I32 plantIndex=a_start+subIndex;
		if(plantIndex>maxPlantIndex)
		{
			break;
		}

		sp<PlantModelI> spPlantModelI=m_modelOfPlant[plantIndex];
		if(spPlantModelI.isNull())
		{
			continue;
		}

		spPlantModelI->setUniformRigidity(rigidity);
		spPlantModelI->setDamping(damping);
		spPlantModelI->setCompensation(compensation);
		spPlantModelI->setCorrection(correction);

		std::map<U32,U32>& rPointMap=m_plantPointMap[plantIndex];

		std::set<U32> pointSet;

		I32 primitivesAdded=0;
		I32 primitivesSkipped=0;
		I32 nextPointIndex=0;

		Array<U32>& rPrimitives=m_primitivesOfPlant[plantIndex];
		for(Array<U32>::iterator it=rPrimitives.begin();
				it!=rPrimitives.end();it++)
		{
			const I32 primitiveIndex= *it;

			const U32 subCount=spInputVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				continue;
			}

			U32 fromIndex=spInputVertices->integer(primitiveIndex,0);
//			FEASSERT(fromIndex<pointCount);

			I32 plantFromIndex=nextPointIndex;

			if(pointSet.find(fromIndex)!=pointSet.end())
			{
				plantFromIndex=rPointMap[fromIndex];

				//* HACK only one root segment
				if(!plantFromIndex)
				{
					plantFromIndex=1;
				}
			}
			else
			{
				if(primitivesAdded)
				{
//						feLog("skipping new root prim %d from %d\n",
//								primitiveIndex,fromIndex);
					primitivesSkipped++;
					continue;
				}

				rPointMap[fromIndex]=nextPointIndex++;
				pointSet.insert(fromIndex);
			}

			SpatialVector fromPosition=convertY(
					spInputVertices->spatialVector(primitiveIndex,0));

			if(!primitivesAdded)
			{
				m_plantOffset[plantIndex]=fromPosition;
			}

			primitivesAdded++;

			Real curveLength(0);
			Real* segmentLength=new Real[subCount-1];
			SpatialVector basePosition=fromPosition;
			for(U32 subIndex=1;subIndex<subCount;subIndex++)
			{
				const SpatialVector tipPosition=convertY(
						spInputVertices->spatialVector(
						primitiveIndex,subIndex));

				Real& rSegmentLength=segmentLength[subIndex-1];
				rSegmentLength=magnitude(tipPosition-basePosition);
				curveLength+=rSegmentLength;

				basePosition=tipPosition;

//				feLog("  seg %d/%d length %.6G\n",
//						subIndex-1,subCount-1,rSegmentLength);
			}

//			feLog("curve %d from %d map %d curveLength %.6G\n",
//					primitiveIndex,fromIndex,plantFromIndex,
//					curveLength);

			Real remainingLength=curveLength;

			//* TODO param
			const Real areaRatio=pow(10.0,areaRatioPower);
			const Real taper=1.0;

			const Real radiusBase=spInputRadius.isValid()?
					spInputRadius->real(0):
					sqrtf(areaRatio/pi*curveLength);
			Real radius1=radiusBase;

			for(U32 subIndex=1;subIndex<subCount;subIndex++)
			{
				const U32 toIndex=
						spInputVertices->integer(primitiveIndex,subIndex);
//				FEASSERT(toIndex<pointCount);

				const SpatialVector toPosition=convertY(
						spInputVertices->spatialVector(
						primitiveIndex,subIndex));
				if(pointSet.find(toIndex)!=pointSet.end())
				{
					feLog("TreeOp::rebuild redundant segment"
							" from point %d to %d\n",
							fromIndex,toIndex);
					fromIndex=toIndex;
					plantFromIndex=rPointMap[toIndex];
					fromPosition=toPosition;
					continue;
				}

				const SpatialVector span=toPosition-fromPosition;
				const Real mag2=magnitudeSquared(span);
				if(mag2<1e-6)
				{
					feLog("TreeOp::rebuild short segment"
							" from point %d to %d length %.6G\n",
							fromIndex,toIndex,sqrt(mag2));

					//* TODO should move redundant point in replace()
					continue;
				}

				I32 plantToIndex=nextPointIndex;
				rPointMap[toIndex]=nextPointIndex++;

				const U32 level=(primitiveIndex!=0);

				Real rigidity=1.0;
				if(spInputRigidity.isValid())
				{
					rigidity=spInputRigidity->real(toIndex);
				}

				Real radius2=radius1;
				if(spInputRadius.isValid())
				{
					radius2=spInputRadius->real(toIndex);
				}
				else
				{
#if FALSE
					//* HACK radius by distance from root
					Real radiusFade=magnitude(toPosition)/30.0;
					if(radiusFade>1.0)
					{
						radiusFade=1.0;
					}
					const Real radius=0.10-0.09*radiusFade;
#else
					remainingLength-=segmentLength[subIndex-1];

					const Real along=Real(1)-remainingLength/curveLength;
					radius2=fe::maximum(Real(0),
							radiusBase*(Real(1)-taper*along));
#endif
				}

//					feLog("\nTreeOp::rebuild"
//							" seg %d/%d pt %d radius %.6G %.6G from %d to %d\n",
//							subIndex-1,subCount-1,toIndex,radius1,radius2,
//							plantFromIndex,plantToIndex);
//					feLog("    %s to %s span %s\n",c_print(fromPosition),
//							c_print(toPosition),c_print(span));

				spPlantModelI->addBranch(plantFromIndex,plantToIndex,
						level,radius1,radius2,span,rigidity);

				radius1=radius2;
				fromIndex=toIndex;
				plantFromIndex=plantToIndex;
				fromPosition=toPosition;
				pointSet.insert(toIndex);
			}
			delete[] segmentLength;
		}

		spPlantModelI->prepare();

//		feLog("primitivesAdded %d primitivesSkipped %d\n",
//				primitivesAdded,primitivesSkipped);

	}
}

BWORD TreeOp::updateInput(void)
{
	sp<SurfaceAccessorI> spInputVelocity;
	sp<SurfaceAccessorI> spInputTarget;
	sp<SurfaceAccessorI> spInputTargeted;

	const String velocityAttr=catalog<String>("velocityAttr");
	if(!velocityAttr.empty())
	{
		access(spInputVelocity,m_spInputAccessible,
				e_point,velocityAttr,e_quiet);
	}
	const String targetAttr=catalog<String>("targetAttr");
	if(!targetAttr.empty())
	{
		access(spInputTarget,m_spInputAccessible,
				e_point,targetAttr,e_quiet);
	}
	const String targetedAttr=catalog<String>("targetedAttr");
	if(!targetedAttr.empty())
	{
		access(spInputTargeted,m_spInputAccessible,
				e_point,targetedAttr,e_quiet);
	}

	if(spInputVelocity.isNull() &&
			(spInputTarget.isNull() || spInputTargeted.isNull()))
	{
		return FALSE;
	}

	const U32 plantCount=m_modelOfPlant.size();

	const I32 threadCount=Thread::hardwareConcurrency();	//* TODO param

	const BWORD multiThread=(plantCount>1 && threadCount>1);

	if(!multiThread)
	{
		updateInputPlants(0,plantCount);
		return TRUE;
	}

	setNonAtomicRange(0,plantCount-1);
	runStage("updateInput");

//~	if(m_spGang.isNull())
//~	{
//~		m_spGang=new Gang<TreeWorker,I32>();
//~	}
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::updateInput start\n");
//~#endif
//~
//~	m_spGang->start(sp<Counted>(this),threadCount,"updateInput");
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::updateInput started\n");
//~#endif
//~
//~	m_bundleSize=0.51*plantCount/threadCount;
//~	if(m_bundleSize<1)
//~	{
//~		m_bundleSize=1;
//~	}
//~
//~	const I32 bundleCount=(plantCount+m_bundleSize-1)/m_bundleSize;
//~
//~	I32 jobCount=0;
//~
//~	for(I32 bundleIndex=0;bundleIndex<bundleCount;bundleIndex++)
//~	{
//~		m_spGang->post(bundleIndex);
//~		jobCount++;
//~	}
//~
//~	for(I32 threadIndex=0;threadIndex<threadCount;threadIndex++)
//~	{
//~		m_spGang->post(-1);
//~		jobCount++;
//~	}
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::updateInput posted\n");
//~#endif
//~
//~	I32 delivered;
//~	while(jobCount)
//~	{
//~		while(m_spGang->acceptDelivery(delivered))
//~		{
//~			jobCount--;
//~		}
//~
//~		if(jobCount)
//~		{
//~			milliSleep(1);
//~		}
//~	}
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::updateInput finish\n");
//~#endif
//~
//~	m_spGang->finish();
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::updateInput finished\n");
//~#endif

	return TRUE;
}

void TreeOp::updateInputPlants(I32 a_start,I32 a_count)
{
	sp<SurfaceAccessorI> spInputVelocity;
	sp<SurfaceAccessorI> spInputTarget;
	sp<SurfaceAccessorI> spInputTargeted;

	const String velocityAttr=catalog<String>("velocityAttr");
	if(!velocityAttr.empty())
	{
		access(spInputVelocity,m_spInputAccessible,
				e_point,velocityAttr,e_quiet);
	}
	const String targetAttr=catalog<String>("targetAttr");
	if(!targetAttr.empty())
	{
		access(spInputTarget,m_spInputAccessible,
				e_point,targetAttr,e_quiet);
	}
	const String targetedAttr=catalog<String>("targetedAttr");
	if(!targetedAttr.empty())
	{
		access(spInputTargeted,m_spInputAccessible,
				e_point,targetedAttr,e_quiet);
	}

	const BWORD targeting=
			(spInputTarget.isValid() && spInputTargeted.isValid());

	if(spInputVelocity.isNull() && !targeting)
	{
		return;
	}

	const I32 maxPlantIndex=m_modelOfPlant.size()-1;

	for(I32 subIndex=0;subIndex<a_count;subIndex++)
	{
		const I32 plantIndex=a_start+subIndex;
		if(plantIndex>maxPlantIndex)
		{
			break;
		}

		const SpatialVector offset=plantIndex<I32(m_plantOffset.size())?
					m_plantOffset[plantIndex]: SpatialVector(0.0,0.0,0.0);

		sp<PlantModelI> spPlantModelI=m_modelOfPlant[plantIndex];
		if(spPlantModelI.isNull())
		{
			continue;
		}

		const U32 numSticks=spPlantModelI->numSticks();

		std::map<U32,U32>& rPointMap=m_plantPointMap[plantIndex];
		for(std::map<U32,U32>::iterator it=rPointMap.begin();
				it!=rPointMap.end();it++)
		{
			const U32 pointIndex=it->first;
			const U32 plantPointIndex=it->second;

			if(plantPointIndex<=numSticks)
			{
				PlantModelI::Stick* pStick=plantPointIndex?
						spPlantModelI->stick(plantPointIndex-1):
						spPlantModelI->stick(0);

				if(pStick)
				{
					if(spInputVelocity.isValid())
					{
						const SpatialVector velocity=convertY(
								spInputVelocity->spatialVector(pointIndex));

						pStick->setWindVelocity(velocity);
					}
					if(targeting)
					{
						const I32 targeted=
								spInputTargeted->integer(pointIndex);

						pStick->setTargeted(targeted);

						if(targeted)
						{
							const SpatialVector target=convertY(
									spInputTarget->spatialVector(pointIndex));

							pStick->setTarget(target-offset);
						}
					}
				}
			}

//			feLog("plant %d/%d map %d %d vel %s\n",
//					plantIndex,plantCount,pointIndex,plantPointIndex,
//					c_print(velocity));
		}
	}
}

void TreeOp::replace(void)
{
	const U32 plantCount=m_modelOfPlant.size();

	const I32 threadCount=Thread::hardwareConcurrency();	//* TODO param

	BWORD multiThread=(plantCount>1 && threadCount>1);

	//* TODO preallocate space for radius in payload SurfaceAccessorOpenCL
	if(m_spPayloadAccessible.isValid() && catalog<bool>("writeRadius"))
	{
		multiThread=FALSE;
	}

	if(!multiThread)
	{
		replacePlants(0,plantCount);
		return;
	}

	setNonAtomicRange(0,plantCount-1);
	runStage("replace");

//~	if(m_spGang.isNull())
//~	{
//~		m_spGang=new Gang<TreeWorker,I32>();
//~	}
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::replace start\n");
//~#endif
//~
//~	m_spGang->start(sp<Counted>(this),threadCount,"replace");
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::replace started\n");
//~#endif
//~
//~	m_bundleSize=0.51*plantCount/threadCount;
//~	if(m_bundleSize<1)
//~	{
//~		m_bundleSize=1;
//~	}
//~
//~	const I32 bundleCount=(plantCount+m_bundleSize-1)/m_bundleSize;
//~
//~	I32 jobCount=0;
//~
//~	for(I32 bundleIndex=0;bundleIndex<bundleCount;bundleIndex++)
//~	{
//~		m_spGang->post(bundleIndex);
//~		jobCount++;
//~	}
//~
//~	for(I32 threadIndex=0;threadIndex<threadCount;threadIndex++)
//~	{
//~		m_spGang->post(-1);
//~		jobCount++;
//~	}
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::replace posted\n");
//~#endif
//~
//~	I32 delivered;
//~	while(jobCount)
//~	{
//~		while(m_spGang->acceptDelivery(delivered))
//~		{
//~			jobCount--;
//~		}
//~
//~		if(jobCount)
//~		{
//~			milliSleep(1);
//~		}
//~	}
//~
//~#if FE_TOP_THREAD_DEBUG
//~	feLog("TreeOp::replace finish\n");
//~#endif
//~
//~	m_spGang->finish();
//~
//~#if FE_TOP_THREAD_DEBUG
//~		feLog("TreeOp::replace finished\n");
//~#endif
}

void TreeOp::replacePlants(I32 a_start,I32 a_count)
{
	const BWORD writeRadius=catalog<bool>("writeRadius");
	const String radiusAttr=catalog<String>("radiusAttr");

	sp<SurfaceAccessorI> spReplacePoint;
	if(!access(spReplacePoint,m_spReplaceAccessible,e_point,e_position))
	{
		return;
	}

	sp<SurfaceAccessorI> spReplaceRadius;
	if(writeRadius && !radiusAttr.empty())
	{
		access(spReplaceRadius,m_spReplaceAccessible,e_point,radiusAttr);
	}

	const I32 maxPlantIndex=m_modelOfPlant.size()-1;

	for(I32 subIndex=0;subIndex<a_count;subIndex++)
	{
		const I32 plantIndex=a_start+subIndex;
		if(plantIndex>maxPlantIndex)
		{
			break;
		}

		sp<PlantModelI> spPlantModelI=m_modelOfPlant[plantIndex];
		if(spPlantModelI.isNull())
		{
			continue;
		}

		const U32 numSticks=spPlantModelI->numSticks();

		const SpatialVector offset=m_plantOffset[plantIndex];

		std::map<U32,U32>& rPointMap=m_plantPointMap[plantIndex];

		for(std::map<U32,U32>::iterator it=rPointMap.begin();
				it!=rPointMap.end();it++)
		{
			const U32 plantPointIndex=it->second;

			if(plantPointIndex<=numSticks)
			{
				const PlantModelI::Stick* pStick=plantPointIndex?
						spPlantModelI->stick(plantPointIndex-1):
						spPlantModelI->stick(0);
				if(pStick)
				{
					const U32 pointIndex=it->first;
					const SpatialVector& base=pStick->base();
					const SpatialVector& span=pStick->span();
					const SpatialVector point=unconvertY(plantPointIndex?
							offset+base+span: offset+base);

#if FALSE
					feLog("plant %d/%d map %d %d point %s\n",
							plantIndex,maxPlantIndex,
							pointIndex,plantPointIndex,
							c_print(point));
#endif

					spReplacePoint->set(pointIndex,point);

					if(spReplaceRadius.isValid())
					{
						spReplaceRadius->set(pointIndex,pStick->radius1());
					}
				}
			}
		}
	}
}

void TreeOp::regenerate(void)
{
	feLog("TreeOp::regenerate \"%s\"\n",m_treeName.c_str());

	setName(m_treeName);

	m_forestRV.plants()->clear();

	const String path=
			registry()->master()->catalog()->catalog<String>("path:media")+
			"/vegetation/"+m_treeName+".rg";

	sp<RecordGroup>	spFileRG=RecordView::loadRecordGroup(m_spScope,path);
	if(spFileRG.isNull())
	{
		feLog("TreeOp::regenerate failed to load:\n  \"%s\"\n",path.c_str());
		return;
	}

	RecordGroup::iterator itFile=spFileRG->begin();
	sp<RecordArray>& rspRA= *itFile;
	m_plantRAV.bind(rspRA);
	if(m_plantRAV.recordView().plantName.check(rspRA))
	{
		m_forestRV.plants()->add(spFileRG);
	}
	else
	{
		Record plant=m_plantRV.createRecord();
		m_plantRV.plantName()=m_treeName;
		m_plantRV.location()=convertY(catalog<SpatialVector>("TreeLocation"));
		m_forestRV.plants()->add(plant);
		m_plantRV.unbind();
	}

	//* NOTE populate m_modelOfPlant (if ever needed)
#if FALSE
	sp<RecordGroup> spRG=m_forestRV.plants();
	RecordGroup::iterator it=spRG->begin();
	if(it==spRG->end())
	{
		return;
	}
	sp<RecordArray> spRA=*it;

	const U32 plantCount=spRA->length();
	if(!plantCount)
	{
		return;
	}

	m_modelOfPlant.resize(plantCount);

	for(U32 plantIndex=0;plantIndex<plantCount;plantIndex++)
	{
		Record plant=spRA->getRecord(plantIndex);
		if(!plant.isValid())
		{
			m_modelOfPlant[plantIndex]=NULL;
			continue;
		}
		m_plantRV.bind(plant);

		m_modelOfPlant[plantIndex]=m_plantRV.plantModel();
	}

	m_plantRV.unbind();
#endif
}

void TreeOp::handle(Record& a_rSignal)
{
	m_spThreadingState->set(SurfaceAccessibleI::e_multiThread);

	const Real frame=currentFrame(a_rSignal);
	const Real firstFrame=startFrame(a_rSignal);
	m_time=currentTime(a_rSignal);

	m_yUp=catalog<bool>("YUp");

//	const BWORD outputReplaced=catalog<bool>("Output Surface","replaced");

#if	FE_TOP_DEBUG
	feLog("TreeOp::handle start %.6G frame %.6G time %.6G\n",
			firstFrame,frame,m_time);
#endif

	access(m_spInputAccessible,"Input Surface",e_quiet);
	if(m_spInputAccessible.isValid())
	{
		m_treeName="";

		const I32 primitiveCount=
				m_spInputAccessible->count(SurfaceAccessibleI::e_primitive);
		const I32 pointCount=
				m_spInputAccessible->count(SurfaceAccessibleI::e_point);

		const BWORD inputChanged=
				(primitiveCount!=m_lastPrimitiveCount ||
				pointCount!=m_lastPointCount);
		const BWORD frameBack=(frame==firstFrame || frame<m_lastFrame);

#if	FE_TOP_DEBUG
		feLog("TreeOp::handle prims %d vs %d pts %d vs %d\n",
				primitiveCount,m_lastPrimitiveCount,
				pointCount,m_lastPointCount);
#endif
		if(inputChanged)
		{
			m_modelOfPlant.clear();
			m_plantPointMap.clear();
		}

		if(inputChanged || frameBack)
		{
#if	FE_TOP_DEBUG
			feLog("TreeOp::handle rebuild\n");
#endif
			if(!rebuild())
			{
				m_spInputAccessible=NULL;
				return;
			}
		}

#if	FE_TOP_DEBUG
		feLog("TreeOp::handle updateInput\n");
#endif

		updateInput();

		const U32 plantCount=m_modelOfPlant.size();
		catalog<String>("summary").sPrintf("%d %s using input",
				plantCount,plantCount==1? "plant": "plants");

		m_lastPrimitiveCount=primitiveCount;
		m_lastPointCount=pointCount;
	}
	else
	{
		m_modelOfPlant.clear();
		m_plantPointMap.clear();
		m_lastPrimitiveCount=0;
		m_lastPointCount=0;

		const String treeName=catalog<String>("TreeName");
		if(m_treeName!=treeName)
		{
			feLog("TreeOp::handle treeName changed \"%s\" -> \"%s\"\n",
					m_treeName.c_str(),treeName.c_str());
			m_treeName=treeName;
			regenerate();
		}

		catalog<String>("summary")=m_treeName;
	}

#if	FE_TOP_DEBUG
		feLog("TreeOp::handle update records\n");
#endif

	m_lastFrame=frame;

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal))
	{
		m_spInputAccessible=NULL;
		return;
	}

	m_forestRV.upAxis()=catalog<bool>("YUp")? 1: 2;

#if TRUE
	//* HACK always draw (for Arnold tests)
	m_forestRV.drawI()=spDrawI;
#else
	m_forestRV.drawI()=m_spInputAccessible.isValid()? sp<DrawI>(NULL): spDrawI;
#endif

	m_forestRV.stickForm()=catalog<String>("stickForm");
	m_forestRV.leafForm()=catalog<String>("leafForm");
	m_forestRV.deltaT()=(1.0f/24.0f);
	m_forestRV.gravity()=
			convertY(catalog<SpatialVector>("gravity"));
	m_forestRV.uniformVelocity()=
			convertY(catalog<SpatialVector>("WindDirection"));

	const Real reactivity=catalog<Real>("reactivity");
	const Real threshold=catalog<Real>("threshold");
	const String collisionMethod=catalog<String>("collisionMethod");
	const BWORD collideEnd=catalog<bool>("collideEnd");
	const Real repulsion=catalog<Real>("repulsion");
	const Real repulsionFalloff=catalog<Real>("repulsionFalloff");
	const Real depletion=catalog<Real>("depletion");
	const Real depletionFalloff=catalog<Real>("depletionFalloff");
	const Real windHampering=catalog<Real>("windHampering");
	const Real windFalloff=catalog<Real>("windFalloff");
	const Real plasticity=
			catalog<bool>("plastic")? catalog<Real>("plasticity"): 0.0;

	sp<SurfaceI> spCollider;
	access(spCollider,"Collider Surface",e_quiet);

#if FALSE
	if(spCollider.isValid())
	{
		sp<SurfaceSphere> spSphere(new SurfaceSphere());
		spSphere->setCenter(convertY(spCollider->center()));
		spSphere->setRadius(spCollider->radius());

		spCollider=spSphere;
	}
#endif

	if(spCollider.isValid() && collisionMethod=="partSpheres")
	{
		const String colliderPartitionAttr=
				catalog<String>("colliderPartitionAttr");

		if(!colliderPartitionAttr.empty())
		{
			spCollider->partitionWith(colliderPartitionAttr);
			spCollider->setPartitionFilter(".*");
		}
	}

	SpatialTransform colliderTransform;
	setIdentity(colliderTransform);

	if(catalog<bool>("YUp"))
	{
		colliderTransform.column(0)=SpatialVector(1.0, 0.0, 0.0);
		colliderTransform.column(1)=SpatialVector(0.0, 0.0, 1.0);
		colliderTransform.column(2)=SpatialVector(0.0,-1.0, 0.0);
		set(colliderTransform.translation());
	}

	m_forestRV.collider()=spCollider;
	m_forestRV.colliderTransform()=colliderTransform;

	sp<RecordGroup>& rspPlants=m_forestRV.plants();
	FEASSERT(rspPlants.isValid());

	//* update location, but only if it is an individual tree
	for(RecordGroup::iterator it=rspPlants->begin();it!=rspPlants->end();it++)
	{
		sp<RecordArray> spRA= *it;

		if(spRA->length()!=1)
		{
			continue;
		}

		if(!m_plantRV.plantModel.check(spRA))
		{
			continue;
		}

		m_plantRAV.bind(spRA);
		for(Plant& plantRV: m_plantRAV)
		{
			plantRV.location()=
					convertY(catalog<SpatialVector>("TreeLocation"));
		}
	}

	for(RecordGroup::iterator it=rspPlants->begin();it!=rspPlants->end();it++)
	{
		sp<RecordArray> spRA= *it;

		if(!m_plantRV.plantModel.check(spRA))
		{
			continue;
		}

		U32 plantIndex=0;

		m_plantRAV.bind(spRA);
		for(Plant& plantRV: m_plantRAV)
		{
			plantRV.threshold()=threshold;
			plantRV.reactivity()=reactivity;
			plantRV.collisionMethod()=collisionMethod;
			plantRV.collideEnd()=collideEnd;
			plantRV.repulsion()=repulsion;
			plantRV.repulsionFalloff()=repulsionFalloff;
			plantRV.depletion()=depletion;
			plantRV.depletionFalloff()=depletionFalloff;
			plantRV.windHampering()=windHampering;
			plantRV.windFalloff()=windFalloff;
			plantRV.plasticity()=plasticity;

			plantRV.offset()=plantIndex<m_plantOffset.size()?
					m_plantOffset[plantIndex]: SpatialVector(0.0,0.0,0.0);
			plantIndex++;
		}
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	accessOutput(spOutputAccessible,a_rSignal);

	if(m_spInputAccessible.isNull())
	{
		spOutputAccessible->clear();
	}

#if	FE_TOP_DEBUG
	feLog("TreeOp::handle signal\n");
#endif

	Record forest=m_forestRV.record();

	m_spSignalerI->signal(forest);

	const BWORD payload=catalog<bool>("payload");

	if(m_spInputAccessible.isValid())
	{
		if(payload)
		{
			if(m_spPayloadAccessible.isNull())
			{
				m_spPayloadAccessible=registry()->create(
						"*.SurfaceAccessibleOpenCL");
				if(m_spPayloadAccessible.isValid())
				{
//					feLog("TreeOp::handle payload copy input\n");
					m_spPayloadAccessible->copy(m_spInputAccessible);
				}
			}
			m_spReplaceAccessible=m_spPayloadAccessible;
		}
		else
		{
			m_spPayloadAccessible=NULL;
		}


		if(m_spPayloadAccessible.isNull())
		{
			spOutputAccessible->copy(m_spInputAccessible);
			m_spReplaceAccessible=spOutputAccessible;
		}
		else
		{
			spOutputAccessible->clear();
		}

		replace();

		m_spReplaceAccessible=NULL;
	}
	else
	{
		m_spPayloadAccessible=NULL;

		if(catalog<bool>("writeRadius"))
		{
			const String radiusAttr=catalog<String>("radiusAttr");

			sp<SurfaceAccessorI> spReplaceRadius;
			access(spReplaceRadius,spOutputAccessible,e_point,radiusAttr);
			if(spReplaceRadius.isValid())
			{
				I32 pointIndex=0;

				sp<RecordGroup> spRG=m_forestRV.plants();
				FEASSERT(spRG.isValid());

				for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
				{
					sp<RecordArray> spRA= *it;
					if(!m_plantRV.plantModel.check(spRA))
					{
						continue;
					}

					m_plantRAV.bind(spRA);
					for(Plant& plantRV: m_plantRAV)
					{
						sp<PlantModelI> spPlantModelI=plantRV.plantModel();
						if(spPlantModelI.isNull())
						{
							continue;
						}

						const U32 numSticks=spPlantModelI->numSticks();
						for(U32 index=0;index<numSticks;index++)
						{
							PlantModelI::Stick* pStick=
									spPlantModelI->stick(index);

							spReplaceRadius->set(pointIndex++,
									pStick->radius1());
							spReplaceRadius->set(pointIndex++,
									pStick->radius2());
						}
					}
				}
			}
		}
	}

	if(m_spPayloadAccessible.isValid())
	{
//		feLog("TreeOp::handle use payload\n");
		Array< sp<Component> >& rPayload=
				catalog< Array< sp<Component> > >("Output Surface","payload");
		rPayload.resize(1);
		rPayload[0]=m_spPayloadAccessible;
	}
	else
	{
		catalogRemove("Output Surface","payload");
	}

	m_spInputAccessible=NULL;

#if	FE_TOP_DEBUG
	feLog("TreeOp::handle done\n");
#endif
}
