/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Stream_h__
#define __data_Stream_h__
namespace fe
{
namespace data
{

class FE_DL_EXPORT StreamI:
	public Component,
	public CastableAs<StreamI>
{
	public:
virtual	void			bind(sp<Scope> spScope)								= 0;
virtual	void			output(std::ostream &ostm, sp<RecordGroup> spRG)	= 0;
virtual	sp<RecordGroup>	input(std::istream &istrm)							= 0;
};

class FE_DL_EXPORT FileStreamI:
	public Component,
	public CastableAs<FileStreamI>
{
	public:
virtual	void			bind(sp<Scope> spScope)								= 0;
virtual	void			output(const String &a_file, sp<RecordGroup> spRG)	= 0;
virtual	sp<RecordGroup>	input(const String &a_file)							= 0;
};

class FE_DL_EXPORT AsciiFileStream:
	virtual public FileStreamI,
	public CastableAs<AsciiFileStream>
{
	public:
						AsciiFileStream(void);
virtual					~AsciiFileStream(void);
virtual	void			bind(sp<Scope> spScope);
virtual	void			output(const String &a_file, sp<RecordGroup> spRG);
virtual	sp<RecordGroup>	input(const String &a_file);
const	String&			name(void) const	{ return m_name; }
	private:
		sp<Scope>		m_spScope;
		sp<StreamI>		m_spStream;
		String			m_name;
};

class FE_DL_EXPORT BinaryFileStream:
	virtual public FileStreamI,
	public CastableAs<BinaryFileStream>
{
	public:
						BinaryFileStream(void);
virtual					~BinaryFileStream(void);
virtual	void			bind(sp<Scope> spScope);
virtual	void			output(const String &a_file, sp<RecordGroup> spRG);
virtual	sp<RecordGroup>	input(const String &a_file);
const	String&			name(void) const	{ return m_name; }
	private:
		sp<Scope>		m_spScope;
		sp<StreamI>		m_spStream;
		String			m_name;
};

#define FE_SERIAL_VERSION	5

enum
{
	e_end			= 0,
	e_reset			= 1,
	e_info			= 2,
	e_attribute		= 3,
	e_layout		= 4,
	e_group			= 5,
	e_state			= 6
};

/**	Ascii IO
	*/
class FE_DL_EXPORT AsciiStream:
	virtual public StreamI,
	public CastableAs<AsciiStream>
{
	public:
							AsciiStream(void);
							AsciiStream(sp<Scope> spScope);
virtual						~AsciiStream(void);
virtual	void				bind(sp<Scope> spScope);
virtual	void				output(std::ostream &ostrm, sp<RecordGroup> spRG);
virtual	sp<RecordGroup>		input(std::istream &istrm);
const	String&				name(void) const	{ return m_name; }
	private:
		sp<Scope>			m_spScope;
		sp<Writer>			m_spWriter;
		sp<Reader>			m_spReader;
		String				m_name;
};

/**	Binary IO
	*/
class FE_DL_EXPORT BinaryStream:
	virtual public StreamI,
	public CastableAs<BinaryStream>
{
	public:
							BinaryStream(void);
							BinaryStream(sp<Scope> spScope);
virtual						~BinaryStream(void);
virtual	void				bind(sp<Scope> spScope);
virtual	void				output(std::ostream &ostrm, sp<RecordGroup> spRG);
virtual	sp<RecordGroup>		input(std::istream &istrm);
const	String&				name(void) const	{ return m_name; }
	private:
		sp<Scope>			m_spScope;
		sp<Writer>			m_spWriter;
		sp<Reader>			m_spReader;
		String				m_name;
};


} /* namespace */
} /* namespace */

#endif /* __data_Stream_h__ */

