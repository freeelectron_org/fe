/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer__WindowController_h__
#define __viewer__WindowController_h__

namespace fe
{
namespace ext
{

/**	Window controller

	@copydoc WindowController_info
	*/
class FE_DL_EXPORT WindowController :
	virtual public HandlerI,
	virtual public Mask,
	public Initialize<WindowController>
{
	public:
					WindowController(void);
virtual				~WindowController(void);

		void		initialize(void);

		// AS HandlerI
virtual void		handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void		handle(Record &record);
	private:
		AsSignal			m_asSignal;
		AsOrtho				m_asOrtho;
		AsSelection			m_asSel;

		bool				m_init;

		WindowEvent::Mask	m_orthoZoom;
		WindowEvent::Mask	m_orthoZoomUp;
		WindowEvent::Mask	m_orthoZoomDown;
		WindowEvent::Mask	m_orthoCenter;
		WindowEvent::Mask	m_orthoCenterUp;
		WindowEvent::Mask	m_orthoCenterDown;
		WindowEvent::Mask	m_orthoCenterLeft;
		WindowEvent::Mask	m_orthoCenterRight;
		WindowEvent::Mask	m_orthoRecenter;
		WindowEvent::Mask	m_quit;
		WindowEvent::Mask	m_pause;
};


} /* namespace ext */
} /* namespace fe */


#endif /* __viewer__WindowController_h__ */

