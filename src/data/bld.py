import sys
import time
import os
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "data.pmh",
                "Accessor",
                "PathAccessor",
                "Layout",
                "LayoutAV",
                "LayoutSB",
                "Record",
                "RecordAV",
                "RecordSB",
                "RecordArray",
                "RecordArraySB",
                "RecordArrayAV",
                "RecordCookbook",
                "RecordGroup",
                "Scope",
                "SegmentStore",
                "Stream",
                "Reader",
                "WeakRecordAV",
                "WeakRecordSB",
                "WeakRecord",
                "Scanner",
                "AsciiReader",
                "AsciiWriter",
                "BinaryWriter",
                "BinaryReader",
                "Writer",
                "Cloner",
                "Attribute",
                "dataLib"
                ]

    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    lib = module.DLL( "feData", srcList )

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "feImportMemoryLib" ]
        forge.deps( ["feDataLib"], deplibs )
    if forge.fe_os == 'FE_OSX':
        forge.deps( ["feDataLib"], deplibs )

    srcList += ["dataDL"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        dll = module.DLL( "feDataDL", ["data.pmh", "dataDL"] )
        deplibs += [ "feDataLib" ]  # else code might unload
    else:
        dll = module.DLL( "feDataDL", srcList )

    forge.deps( ["feDataDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "feDataDL",                     None,       None) ]

    module.Module('test')

    module.Image("data_design")

