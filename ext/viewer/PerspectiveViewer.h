/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer__PerspectiveViewer_h__
#define __viewer__PerspectiveViewer_h__

namespace fe
{
namespace ext
{

/**	Perspective viewer

	@copydoc PerspectiveViewer_info
	*/
class FE_DL_EXPORT PerspectiveViewer :
	public Initialize<PerspectiveViewer>,
	virtual public SignalerViewerI,
	virtual public HandlerI
{
	public:
				PerspectiveViewer(void);
virtual			~PerspectiveViewer(void);
		void	initialize(void);

				//* As ViewerI
				using ViewerI::bind;
virtual	void	bind(sp<WindowI> spWindowI);
virtual void	render(Real a_time,Real a_timestep)							{}
virtual Real	frameRate(void) const;

				//* As HandlerI
virtual	void	handle(Record& record);
virtual	void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> spLayout);

	private:
		sp<WindowI>						m_spWindowI;
		PathAccessor<SpatialTransform>	m_paPerspMatrix;
		AsSignal						m_asSignal;
		AsWindata						m_asWindata;
		sp<ViewI>						m_view;

		Real			m_frameMS;
		Real			m_frameRate;
		Real			m_frameTotal;
		U32				m_frameCount;
		SystemTicker	m_ticker;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer__PerspectiveViewer_h__ */

