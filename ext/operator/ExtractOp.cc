/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_ETO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void ExtractOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("nodeName")="";
	catalog<String>("nodeName","label")="Node Name";

	catalog<String>("format")="native";
	catalog<String>("format","label")="Format";
	catalog<String>("format","choice:0")="native";
	catalog<String>("format","label:0")="Native";
	catalog<String>("format","choice:1")="rg";
	catalog<String>("format","label:1")="RecordGroup";
	catalog<String>("format","choice:2")="opencl";
	catalog<String>("format","label:2")="OpenCL";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","temporal")=true;

	m_spScope=registry()->create("Scope");
}

void ExtractOp::handle(Record& a_rSignal)
{
#if FE_ETO_DEBUG
	feLog("ExtractOp::handle node \"%s\"\n",name().c_str());
#endif

	const String nodeName=catalog<String>("nodeName");
	const String format=catalog<String>("format");

	catalog<String>("summary")=nodeName.empty()? "<whole>": nodeName;

	const BWORD quiet=TRUE;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	if(format!="native")
	{
		sp<SurfaceAccessibleI> spAccessible=registry()->create(
				"SurfaceAccessibleI.*.*."+format,quiet);
		if(spAccessible.isNull())
		{
			spAccessible=registry()->create("SurfaceAccessibleI.*.*.rg");
		}
		if(spAccessible.isNull())
		{
			spAccessible=registry()->create("SurfaceAccessibleI");
		}
		if(spAccessible.isNull())
		{
			catalog<String>("error")+="no format drivers found";
			return;
		}

		catalog< sp<Component> >("Output Surface")=spAccessible;
		setSurfaceOutput(a_rSignal,spAccessible);

		spAccessible->bind(m_spScope);
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

#if FE_ETO_DEBUG
	feLog("ExtractOp::handle using \"%s\" for node \"%s\"\n",
			spOutputAccessible->name().c_str(),nodeName.c_str());
#endif

	spOutputAccessible->copy(spInputAccessible,nodeName);

#if FE_ETO_DEBUG
	feLog("ExtractOp::handle node \"%s\" done\n",name().c_str());
#endif
}
