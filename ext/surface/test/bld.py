import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    module.summary = []

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexGeometryDLLib",
                "fexDataToolDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib",
                        "fexThreadDLLib" ]

    tests = [   'xIntersect' ]

    forge.tests += [
        ("xIntersect.exe",  "",                                 None,None) ]

    if not 'ray' in forge.modules_found:
        module.summary += [ "-ray" ]

    if 'viewer' in forge.modules_confirmed:
        deplibs += [ "fexViewerDLLib" ]

        tests += [  'xSurface' ]

        if forge.media:
            forge.tests += [
                ("xSurface.exe",    "sphere 100",                       None,None),
                ("xSurface.exe",    "curves 100",                       None,None),
                ("xSurface.exe",    "disk 100",                         None,None),
                ("xSurface.exe",    "plane 100",                        None,None),
                ("xSurface.exe",    "cylinder 100",                     None,None),
                ("xSurface.exe",    "triangle 100",                     None,None),
                ("xSurface.exe",    "oval 100",                         None,None),
                ("xSurface.exe",    "teapot 10",                        None,None) ]

            if 'ray' in forge.modules_found:
                forge.tests += [
                    ("xSurface.exe",    "sphere     \"*.DrawRayTrace\" 10", None,None),
                    ("xSurface.exe",    "curves     \"*.DrawRayTrace\" 10", None,None),
                    ("xSurface.exe",    "disk       \"*.DrawRayTrace\" 10", None,None),
                    ("xSurface.exe",    "plane      \"*.DrawRayTrace\" 2",  None,None),
                    ("xSurface.exe",    "cylinder   \"*.DrawRayTrace\" 10", None,None),
                    ("xSurface.exe",    "triangle   \"*.DrawRayTrace\" 10", None,None),
                    ("xSurface.exe",    "oval       \"*.DrawRayTrace\" 10", None,None) ]
    else:
        module.summary += [ "-viewer" ]

    for t in tests:
        exe = module.Exe(t)

        if t == "xSurface":
            exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)
