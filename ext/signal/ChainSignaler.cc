/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

#define FE_CHAINSIGNALER_TRACE	FALSE

namespace fe
{
namespace ext
{

ChainSignaler::Entry::Entry(void)
{
}

ChainSignaler::Entry::~Entry(void)
{
#if FE_COUNTED_TRACK
	Counted::deregisterRegion(this);
#endif
}

ChainSignaler::ChainSignaler(void)
#ifdef FE_CHAINSIGNALER_PROFILING
	:m_pTicker(NULL)
#endif
{
	//m_entryMap = &(m_modeRegistry[""]);
	//m_default = m_entryMap;
#ifdef MODAL_SIGNALER
	m_modes[""] = 0;
	m_modeSwitch.push_back(true);
#endif
}

ChainSignaler::~ChainSignaler(void)
{
#ifdef FE_CHAINSIGNALER_PROFILING
	if(m_pTicker)
	{
		delete m_pTicker;
		m_pTicker=NULL;
	}

	Peeker peeker;
	peek(peeker);
	feLog(peeker.output().c_str());
#endif

#if 0
	for(std::map<String, t_entrymap>::iterator i_mode = m_modeRegistry.begin();
		i_mode != m_modeRegistry.end(); i_mode++)
	{
		for(std::map< sp<Layout>, List< Entry* > >::iterator
			it = i_mode->second.begin(); it != i_mode->second.end(); it++)
		{
			it->second.deleteAll();
		}
	}
#endif

	for(std::map< sp<Layout>, List< Entry* > >::iterator
		it = m_entryMap.begin(); it != m_entryMap.end(); it++)
	{
#if FE_COUNTED_TRACK
		Counted::deregisterRegion(&it->second);
#endif
		it->second.deleteAll();
	}
}

void ChainSignaler::signal(Record &signal)
{
//	feLog("ChainSignaler::signal \"%s\" \"%s\"\n",
//		name().c_str(),
//		signal.layout()->name().c_str());

	{
		SAFEGUARD;

#if FE_CHAINSIGNALER_TRACE
		SystemTicker ticker("ChainSignaler", "signal");
#endif

#if 1
		int *pTTL;
#if 1
		if(signal.layout()->scope() != m_aTTL.scope())
		{
			m_aTTL.setup(signal.layout()->scope(), FE_USE(":TTL"));
		}
#else
		m_aTTL.setup(signal.layout()->scope(), FE_USE(":TTL"));
#endif
		pTTL = m_aTTL.queryAttribute(signal);
		if(pTTL)
		{
			if(*pTTL > 0)
			{
//				feLog("ttl %d -> %d\n", *pTTL,  *pTTL - 1);
				(*pTTL)--;
			}
			else
			{
				return;
			}
		}
#endif
	}

#if FE_CHAINSIGNALER_TRACE
	ticker.log("pre signalEntries");
#endif

	signalEntries(signal, &m_entryMap);

#if FE_CHAINSIGNALER_TRACE
	ticker.log("signalEntries");
#endif
}

void ChainSignaler::signalEntries(Record &signal, t_entrymap *a_entryMap)
{
//	feLog("ChainSignaler::signalEntries \"%s\" \"%s\"\n",
//		name().c_str(),
//		signal.layout()->name().c_str());

	SAFEGUARD;

#if FE_CHAINSIGNALER_TRACE
	SystemTicker ticker("ChainSignaler", "signal");
#endif

	ListCore::Context context;
	Entry *pEntry;

	// NULL layout
	sp<Layout> spNullLayout;
	(*a_entryMap)[spNullLayout].toHead(context);
	while( (pEntry = (*a_entryMap)[spNullLayout].postIncrement(context)) )
	{
#ifdef MODAL_SIGNALER
		if(m_modeSwitch[pEntry->m_mode])
		{
#endif
			if(pEntry->m_component.isValid())
			{
				sp<HandlerI> spHandlerI= pEntry->m_component;
				SAFEUNLOCK;
#if FE_CHAINSIGNALER_TRACE
				fe_fprintf(stderr, "%s\n", spHandlerI->name().c_str());
				ticker.log("pre signalHandler");
#endif
				signalHandler(signal, spHandlerI);
#if FE_CHAINSIGNALER_TRACE
				ticker.log("signalHandler");
#endif
				SAFELOCK;
			}
			else
			{
#if FE_CHAINSIGNALER_TRACE
				fe_fprintf(stderr, "DELETE\n");
#endif
				(*a_entryMap)[spNullLayout].remove(pEntry, context);
				delete pEntry;
			}
#ifdef MODAL_SIGNALER
		}
#endif
	}

	//* NOTE don't create an entry
	if((*a_entryMap).find(signal.layout())==(*a_entryMap).end())
	{
		return;
	}

	// the signal layout
	(*a_entryMap)[signal.layout()].toHead(context);
	while( (pEntry = (*a_entryMap)[signal.layout()].postIncrement(context)) )
	{
#ifdef MODAL_SIGNALER
		if(m_modeSwitch[pEntry->m_mode])
		{
#endif
			if(pEntry->m_component.isValid())
			{
				sp<HandlerI> spHandlerI= pEntry->m_component;
				SAFEUNLOCK;
#if FE_CHAINSIGNALER_TRACE
				fe_fprintf(stderr, "%s\n", spHandlerI->name().c_str());
				ticker.log("pre signalHandler");
#endif
				signalHandler(signal, spHandlerI);
#if FE_CHAINSIGNALER_TRACE
				ticker.log("signalHandler");
#endif
				SAFELOCK;
			}
			else
			{
#if FE_CHAINSIGNALER_TRACE
				fe_fprintf(stderr, "DELETE\n");
#endif
				(*a_entryMap)[signal.layout()].remove(pEntry, context);
				delete pEntry;
			}
#ifdef MODAL_SIGNALER
		}
#endif
	}
}

void ChainSignaler::signalHandler(Record &signal,
	const sp<HandlerI> &spHandlerI)
{
//	feLog("ChainSignaler::signalHandler \"%s\" \"%s\" \"%s\"\n",
//		name().c_str(),
//		signal.layout()->name().c_str(),
//		spHandlerI->name().c_str());

#ifdef FE_CHAINSIGNALER_PROFILING
	if(!m_pTicker)
	{
		m_pTicker=new SystemTicker();
	}

	Profile &layoutProfile = m_layoutProfiles[signal.layout()];
	Profile &componentProfile = spHandlerI->name().empty()?
			m_componentProfiles[signal.layout()]["unnamed Component"]:
			m_componentProfiles[signal.layout()][spHandlerI->name()];
	FE_UWORD m0 = m_pTicker->timeMS();
	FE_UWORD t0 = systemMicroseconds();
#endif

	spHandlerI->handleSignal(signal, sp<SignalerI>(this));

#ifdef FE_CHAINSIGNALER_PROFILING
	FE_UWORD t1 = systemMicroseconds();
	FE_UWORD m1 = m_pTicker->timeMS();
	layoutProfile.m_count++;
	layoutProfile.m_ms += m1 - m0;
	layoutProfile.m_us += t1 - t0;
	componentProfile.m_count++;
	componentProfile.m_ms += m1 - m0;
	componentProfile.m_us += t1 - t0;
#endif
}

#ifdef MODAL_SIGNALER
void ChainSignaler::insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
		IWORD position)
{
	insert(spHandlerI, spLayout, "", position);
}

void ChainSignaler::insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
		const String &aMode, IWORD position)
{
	SAFEGUARD;

	// if(!spLayout->locked()) { spLayout->lock(); }  // why must we lock?
	Entry *pEntry = new Entry();
	pEntry->m_component = spHandlerI;

#if FE_COUNTED_TRACK
	if(m_entryMap.find(spLayout) == m_entryMap.end())
	{
		Counted::registerRegion(&m_entryMap[spLayout],sizeof(List<Entry>));
	}
	Counted::registerRegion(pEntry,sizeof(Entry),"ChainSignaler::Entry " +
			spHandlerI->name() + " on " + spLayout->name());
#endif

	std::map<String, unsigned int>::iterator i_mode = m_modes.find(aMode);
	if(i_mode == m_modes.end())
	{
		pEntry->m_mode = m_modeSwitch.size();
		m_modes[aMode] = pEntry->m_mode;
		m_modeSwitch.push_back(false);
	}
	else
	{
		pEntry->m_mode = i_mode->second;
	}
	if(position == -1)
	{
		m_entryMap[spLayout].append(pEntry);
	}
	else if(position == 0)
	{
		m_entryMap[spLayout].prepend(pEntry);
	}
	else if(position > 0)
	{
		ListCore::Context context;
		m_entryMap[spLayout].toHead(context);
		while(position)
		{
			position--;
			m_entryMap[spLayout].postIncrement(context);
		}
		m_entryMap[spLayout].insertBefore(context, pEntry);
	}
	else
	{
		ListCore::Context context;
		m_entryMap[spLayout].toTail(context);
		while(position < 0)
		{
			position++;
			m_entryMap[spLayout].postDecrement(context);
		}
		m_entryMap[spLayout].insertAfter(context, pEntry);
	}

	if(spHandlerI.isValid())
	{
		sp<SignalerI> spSelf(this);
		spHandlerI->handleBind(spSelf,spLayout);
	}
}
#else
void ChainSignaler::insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
		IWORD position)
{
	SAFEGUARD;

	// if(!spLayout->locked()) { spLayout->lock(); }  // why must we lock?
	Entry *pEntry = new Entry();
	pEntry->m_component = spHandlerI;

#if FE_COUNTED_TRACK
	if(m_entryMap.find(spLayout) == m_entryMap.end())
	{
		Counted::registerRegion(&m_entryMap[spLayout],sizeof(List<Entry>));
	}
	Counted::registerRegion(pEntry,sizeof(Entry),"ChainSignaler::Entry " +
			(spHandlerI.isValid()? spHandlerI->name():
			String("sp<HandlerI>(NULL)")) +
			" on " +
			(spLayout.isValid()? spLayout->name():
			String("sp<Layout>(NULL)")));
#endif

	if(position == -1)
	{
		m_entryMap[spLayout].append(pEntry);
	}
	else if(position == 0)
	{
		m_entryMap[spLayout].prepend(pEntry);
	}
	else if(position > 0)
	{
		ListCore::Context context;
		m_entryMap[spLayout].toHead(context);
		while(position)
		{
			position--;
			m_entryMap[spLayout].postIncrement(context);
		}
		m_entryMap[spLayout].insertBefore(context, pEntry);
	}
	else
	{
		ListCore::Context context;
		m_entryMap[spLayout].toTail(context);
		while(position < 0)
		{
			position++;
			m_entryMap[spLayout].postDecrement(context);
		}
		m_entryMap[spLayout].insertAfter(context, pEntry);
	}

	if(spHandlerI.isValid())
	{
		sp<SignalerI> spSelf(this);
		spHandlerI->handleBind(spSelf,spLayout);
	}
}
#endif

void ChainSignaler::remove(sp<HandlerI> spHandlerI, sp<Layout> spLayout)
{
	SAFEGUARD;
	ListCore::Context context;
	Entry *pEntry;
	List<Entry*> &list = m_entryMap[spLayout];
	list.toHead(context);
	while( (pEntry = list.postIncrement(context)) )
	{
		if(pEntry->m_component == spHandlerI)
		{
			list.remove(pEntry);
			delete pEntry;
		}
	}
}

bool ChainSignaler::contains(sp<HandlerI> spHandlerI, sp<Layout> spLayout)
{
	SAFEGUARD;
	ListCore::Context context;
	Entry *pEntry;
	List<Entry*> &list = m_entryMap[spLayout];
	list.toHead(context);
	while( (pEntry = list.postIncrement(context)) )
	{
		if(pEntry->m_component == spHandlerI)
		{
			return true;
		}
	}
	return false;
}

void ChainSignaler::peek(Peeker &peeker)
{
	String s;
	peeker.nl();
	peeker.cat("ChainSignaler Setup");
	peeker.in();

	peeker.nl();
	peeker.cyan();
	peeker.cat("LAYOUTS:");
	peeker.in();
	for(std::map< sp<Layout>, List< Entry* > >::iterator it =
			m_entryMap.begin(); it != m_entryMap.end(); it++)
	{
		if(!it->first.isValid()) { continue; }
		peeker.nl();
		s.sPrintf("%-30s",
			it->first->name().c_str());
		peeker.yellow();
		peeker.cat(s);

		peeker.in();
		peeker.green();
		ListCore::Context context;
		Entry* pEntry;
		it->second.toHead(context);
		int cnt = 0;
		while( (pEntry = it->second.postIncrement(context)) )
		{
			String componentName;
			sp<HandlerI> spHandlerI = pEntry->m_component;

			if(!spHandlerI.isValid())
			{
				componentName = "invalid Handle";
			}
			else if(spHandlerI->name().empty())
			{
				componentName = "unnamed Component";
			}
			else
			{
				componentName = spHandlerI->name();
			}
			s.sPrintf("%2d %s", cnt, componentName.c_str());
			peeker.nl();
			peeker.cat(s);
			cnt++;
		}
		peeker.base();
		peeker.out();
	}
	peeker.out();
	peeker.out();

#ifdef FE_CHAINSIGNALER_PROFILING
	peeker.nl();
	peeker.cat("ChainSignaler Profiling");
	peeker.nl();

	s.sPrintf("%6s %6s %7s %7s", "cnt", "ms","ms/cnt","us/cnt");
	peeker.cat(s);
	peeker.nl();

	peeker.cyan();
	peeker.cat("LAYOUTS:");
	for(std::map< sp<Layout>, Profile >::iterator it =
			m_layoutProfiles.begin(); it != m_layoutProfiles.end(); it++)
	{
		float us_per_count=
				(float)it->second.m_us/(float)(it->second.m_count);
		if(us_per_count>1e6)
		{
			us_per_count= -1.0f;
		}

		peeker.nl();
		String s;
		peeker.base();
		s.sPrintf("%6u %6u %7.2f %7.0f",
			it->second.m_count,
			it->second.m_ms,
			(float)it->second.m_ms/(float)it->second.m_count,
			us_per_count);
		peeker.cat(s);

		peeker.green();
		s.sPrintf(" %s",
			it->first->name().c_str());
		peeker.cat(s);
	}

	peeker.nl();
	peeker.cyan();
	peeker.cat("TOTAL:");
	for(std::map< sp<Layout>, Profile >::iterator it =
			m_layoutProfiles.begin(); it != m_layoutProfiles.end(); it++)
	{
		I32 components=m_componentProfiles[it->first].size();
		if(components<2)
		{
			continue;
		}

		I32 count=it->second.m_count/components;

		float us_per_count=
				(float)it->second.m_us/(float)(count);
		if(us_per_count>1e6)
		{
			us_per_count= -1.0f;
		}

		peeker.nl();
		String s;
		peeker.base();
		s.sPrintf("%6u %6u %7.2f %7.0f",
			count,
			it->second.m_ms,
			(float)it->second.m_ms/(float)count,
			us_per_count);
		peeker.cat(s);

		peeker.green();
		s.sPrintf(" %s",
			it->first->name().c_str());
		peeker.cat(s);
	}

	peeker.nl();
	peeker.cyan();
	peeker.cat("COMPONENTS:");
	for(std::map< sp<Layout>, std::map<String, Profile> >::iterator it =
			m_componentProfiles.begin(); it != m_componentProfiles.end(); it++)
	{
		for(std::map<String, Profile>::iterator it2 = it->second.begin();
				it2 != it->second.end(); it2++)
		{
			float us_per_count=
					(float)it2->second.m_us/(float)(it2->second.m_count);
#if 0
			if(us_per_count>1e6)
			{
				us_per_count= -1.0f;
			}
#endif
			us_per_count /= 1000.0;

			peeker.nl();
			String s;
			peeker.base();
			s.sPrintf("%6u %6u %7.2f %7.2f",
				it2->second.m_count,
				it2->second.m_ms,
				(float)it2->second.m_ms/(float)it2->second.m_count,
				us_per_count);
			peeker.cat(s);

			peeker.green();
			s.sPrintf(" %-16s %s",
					it->first->name().c_str(),
					it2->first.c_str());
			peeker.cat(s);
		}
	}

	peeker.base();
	peeker.nl();
#endif

}

#ifdef MODAL_SIGNALER
void ChainSignaler::setMode(const String &aMode,  bool aIsActive)
{
	SAFEGUARD;
	std::map<String, unsigned int>::iterator i_mode = m_modes.find(aMode);
	if(i_mode == m_modes.end())
	{
		m_modes[aMode] = m_modeSwitch.size();
		m_modeSwitch.push_back(aIsActive);
	}
	else
	{
		m_modeSwitch[m_modes[aMode]] = aIsActive;
	}
}

bool ChainSignaler::getMode(const String &aMode)
{
	std::map<String, unsigned int>::iterator i_mode = m_modes.find(aMode);
	if(i_mode != m_modes.end())
	{
		return m_modeSwitch[i_mode->second];
	}
	return false;
}
#endif

} /* namespace ext */
} /* namespace fe */

