/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_h__
#define __math_h__

#include "fe/core.h"
#include <limits>

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#define _USE_MATH_DEFINES
#endif

#include <math.h>	// SUPPRESS_SCAN

#ifdef MODULE_math
#define FE_MATH_PORT FE_DL_EXPORT
#else
#define FE_MATH_PORT FE_DL_IMPORT
#endif

#define	FE_EXPLOIT_MEMCPY			FALSE
#define	FE_BOUNDSCHECK				(FE_CODEGEN<=FE_DEBUG)
#define FE_VEC_CHECK_VALID			(FE_CODEGEN<=FE_DEBUG)

#define FE_INVALID_SCALAR(scalar)	(scalar != scalar)

namespace fe
{

const Real pi=Real(M_PI);
const Real degToRad=fe::pi/Real(180);
const Real radToDeg=Real(180)/fe::pi;

#if FE_DOUBLE_REAL
const Real tol=1e-8;
#else
const Real tol=1e-5f;
#endif

extern Real epsilon;
class EpsilonCalc
{
	public:
		EpsilonCalc(void)
		{
			epsilon = 1.0;
			do { epsilon /= 2.0f; }
			while ((Real)(1.0 + (epsilon/2.0)) != 1.0);
		}
};
extern const EpsilonCalc epscalc;

typedef enum
{
	e_xAxis=0x00,
	e_yAxis=0x01,
	e_zAxis=0x02
} Axis;

template<class T>	T minimum(T a,T b)	{ return a>b? b: a; }
template<class T>	T maximum(T a,T b)	{ return a>b? a: b; }
template<class T>	T clamp(T a, T lo, T hi)
{
	return (a <= lo) ? lo : ((a >= hi) ? hi : a);
}

template<class T>	T clampmax(T a, T hi)
{
	return (a >= hi) ? hi : a;
}

template<class T>	T clampmin(T a, T lo)
{
	return (a <= lo) ? lo : a;
}

template<class T>	T sign(T a)
{
	return (a < 0.0) ? -1.0 : 1.0;
}

template<class T>	class Matrix3x4;
}

#include "Vector.h"
#include "Vector2.h"
#include "Vector3.h"
//#include "Vector4.h"
//#include "Vector_gnu.h"
#include "Color.h"
#include "Matrix.h"
#include "Quaternion.h"
#include "DualQuaternion.h"
#include "Matrix3x4.h"
#include "OrthonormalBasis.h"
#include "Euler.h"
#include "Box.h"
#include "DAGNode.h"
#include "Barycenter.h"

namespace fe
{

typedef Vector<3,Real> SpatialVector;
typedef Matrix<3,3,Real> SpatialMatrix;

template<typename T,typename U>
inline bool isZero(T a_value,U a_tolerance)
{
	if(fabs(a_value) > a_tolerance)
	{
		return false;
	}
	return true;
}

template <typename T>
inline bool isZeroV3(const Vector<3,T> &a_vec,T a_tolerance)
{
	if(!isZero(a_vec[0],a_tolerance) || !isZero(a_vec[1],a_tolerance) ||
			!isZero(a_vec[2],a_tolerance))
	{
		return false;
	}
	return true;
}

template<typename T>
inline bool isZero(T a_value)
{
	return isZero(a_value,fe::tol);
}

inline bool isZero(const Vector<3,double> &a_vec)
{
	return isZeroV3<Real>(a_vec,fe::tol);
}

inline bool isZero(const Vector<3,float> &a_vec)
{
	return isZeroV3<float>(a_vec,fe::tol);
}

const SpatialVector zeroVector(Real(0),Real(0),Real(0));

}

#include "math/Mass.h"
#include "math/assertMath.h"
#include "math/algorithms.h"

#endif // __math_h__

