/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <image/image.pmh>

namespace fe
{
namespace ext
{

const U32 ImageRaw::m_bytes[4]={0,2,3,4};

ImageRaw::ImageRaw(void):
	m_selected(0)
{
}

ImageRaw::~ImageRaw(void)
{
}

I32 ImageRaw::createSelect(void)
{
	I32 id=0;
	I32 size=m_buffers.size();
	for(id=0;id<size;id++)
	{
		if(!m_buffers[id].m_created)
		{
			m_buffers[id].m_created=TRUE;
			m_selected=id;
			return id;
		}
	}

	id=size;
	m_buffers.resize(size+1);
	m_buffers[id].m_created=TRUE;
	m_selected=id;
	incrementSerial();
	return id;
}

void ImageRaw::select(I32 id)
{
	if(id<I32(m_buffers.size()))
	{
		m_selected=id;
	}
}

void ImageRaw::unload(I32 id)
{
	if(id<I32(m_buffers.size()))
	{
		m_buffers[id].reset();
		m_buffers[id].m_created=FALSE;
	}
}

void ImageRaw::setFormat(ImageI::Format format)
{
	if(m_selected<I32(m_buffers.size()))
	{
		m_buffers[m_selected].m_format=format;
	}
	incrementSerial();
}

void ImageRaw::resize(U32 width,U32 height,U32 depth)
{
	if(m_selected<I32(m_buffers.size()))
	{
		ImageRaw::Buffer& buffer=m_buffers[m_selected];
		buffer.m_width=width;
		buffer.m_height=height;
		buffer.m_depth=depth;
		buffer.m_pData=reallocate(buffer.m_pData,
				width*height*depth*m_bytes[buffer.m_format]);
	}
	incrementSerial();
}

void ImageRaw::replaceRegion(U32 x,U32 y,U32 z,
		U32 width,U32 height,U32 depth,void* data)
{
	if(m_selected<I32(m_buffers.size()))
	{
		ImageRaw::Buffer& buffer=m_buffers[m_selected];
		if(!x && !y && width==buffer.m_width && height==buffer.m_height &&
				depth==buffer.m_depth)
		{
			memcpy(buffer.m_pData,data,
					width*height*depth*m_bytes[buffer.m_format]);
		}
		else
		{
			// TODO
			feX("ImageRaw::replaceRegion",
					"partial replacement not implemented"
					" %d,%d,%d,%d within %d,%d",
					x,y,width,height,buffer.m_width,buffer.m_height);
		}
	}
	incrementSerial();
}

} /* namespace ext */
} /* namespace fe */
