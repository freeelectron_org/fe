/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "StringEntryController.h"

namespace fe
{
namespace ext
{

StringEntryController::StringEntryController(void)
{
}

StringEntryController::~StringEntryController(void)
{
}

void StringEntryController::initialize(void)
{
	// default event mapping
	maskDefault("fe_stringentry_start",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_statePress));

	maskDefault("fe_stringentry_end",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_stateRelease));

	maskDefault("fe_stringentry_drag",
		WindowEvent::Mask(	WindowEvent::e_sourceMousePosition,
							WindowEvent::e_itemDrag,
							WindowEvent::e_stateAny));
}

void StringEntryController::handleBind(sp<SignalerI> spSignalerI,
	sp<Layout> l_sig)
{
	m_asStringEntry.bind(l_sig->scope());
	m_asSignal.bind(l_sig->scope());
	m_asWindata.bind(l_sig->scope());
	m_asCallback.bind(l_sig->scope());
	m_asSel.bind(l_sig->scope());
	m_spSignaler = spSignalerI;
}

void StringEntryController::handle(Record &r_sig)
{
	Record r_event = r_sig;
	if(!m_asSignal.winData.check(r_event))
	{
		feX("StringEntryController");
	}
	Record r_windata = m_asSignal.winData(r_event);

	sp<RecordGroup> rg_input = m_asWindata.group(r_windata);
	if(!rg_input.isValid()) { return; }

	if(!m_asWindata.windowI(r_windata).isValid()) { return; }

	sp<WindowI> spWindow(m_asWindata.windowI(r_windata));
	Real h = height(spWindow->geometry());

	WindowEvent wev;
	wev.bind(r_event);

	m_start = maskGet("fe_stringentry_start");
	m_end = maskGet("fe_stringentry_end");
	m_drag = maskGet("fe_stringentry_drag");

	Vector4 v;

	bool request_off = false;
	bool request_on = false;

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);

		if(!m_asStringEntry.check(spRA)) { continue; }

		for(int i = 0; i < spRA->length(); i++)
		{
			bool in_button = false;
			Vector4 &v = m_asStringEntry.screen(spRA,i);
			if(		(wev.mouseX()		>=
					v[0])				&&
					(wev.mouseX()		<=
					v[2])				&&
					(h-wev.mouseY()		>=
					v[1])				&&
					(h-wev.mouseY()		<=
					v[3])

				)
			{
				in_button = true;
			}

			enum
			{
				e_n,
				e_si,
				e_so,
				e_ri,
				e_ro
			} transition;

			transition = e_n;

			if(wev.is(m_start))
			{
				if(in_button)
				{
					transition = e_si;
				}
				else
				{
					transition = e_so;
				}
			}
			if(wev.is(m_end))
			{
				if(in_button)
				{
					transition = e_ri;
				}
				else
				{
					transition = e_ro;
				}
			}

			if(in_button)
			{
				m_asStringEntry.state(spRA,i) |= e_in;
			}
			else
			{
				m_asStringEntry.state(spRA,i) &= ~e_in;
			}

			if((m_asStringEntry.state(spRA,i) & e_type) == e_basic)
			{
				if(m_asStringEntry.state(spRA,i) & e_set)
				{
					switch(transition)
					{
						case e_ri:
#if FE_POINTERMOTION==FALSE
							m_asStringEntry.state(spRA,i) &= ~e_in;
#endif
							break;
						case e_ro:
							m_asStringEntry.state(spRA,i) &= ~e_set;
							request_off = true;
							fireSignal(m_asStringEntry.signal(spRA,i));
							break;
						case e_si:
						case e_so:
						default:
							break;
					}
				}
				else
				{
					switch(transition)
					{
						case e_si:
							m_asStringEntry.state(spRA,i) |= e_sel;
							break;
						case e_so:
						case e_ro:
							m_asStringEntry.state(spRA,i) &= ~e_sel;
							break;
						case e_ri:
							if(m_asStringEntry.state(spRA,i) & e_sel)
							{
								m_asStringEntry.state(spRA,i) |= e_set;
								//m_asStringEntry.text(spRA,i) = "";
								m_asStringEntry.cursor(spRA,i) =
									m_asStringEntry.text(spRA,i).length();
								//fireSignal(m_asStringEntry.signal(spRA,i));
								request_on = true;
							}
							m_asStringEntry.state(spRA,i) &= ~e_sel;
#if FE_POINTERMOTION==FALSE
							m_asStringEntry.state(spRA,i) &= ~e_in;
#endif
							break;
						default:
							break;
					}
				}
			}

			if(m_asStringEntry.state(spRA,i) & e_set)
			{
				if(wev.is(WindowEvent::e_sourceKeyboard,WindowEvent::e_itemAny,
					WindowEvent::e_statePress))
				{
					std::string s(m_asStringEntry.text(spRA,i).c_str());
					switch(wev.item())
					{
						case WindowEvent::e_keyLineFeed:
							m_asStringEntry.state(spRA,i) &= ~e_set;
							m_asStringEntry.state(spRA,i) &= ~e_sel;
							request_off = true;
							fireSignal(m_asStringEntry.signal(spRA,i));
#if FE_POINTERMOTION==FALSE
							m_asStringEntry.state(spRA,i) &= ~e_in;
#endif
							break;
						case WindowEvent::e_keyBackspace:
							if(m_asStringEntry.cursor(spRA,i) > 0)
							{
								s.erase(m_asStringEntry.cursor(spRA,i)-1, 1);
								m_asStringEntry.text(spRA,i) = s.c_str();
								m_asStringEntry.cursor(spRA,i) -= 1;
							}
							break;
						case WindowEvent::e_keyDelete:
							if(m_asStringEntry.cursor(spRA,i) <
								int(s.length()))
							{
								s.erase(m_asStringEntry.cursor(spRA,i), 1);
								m_asStringEntry.text(spRA,i) = s.c_str();
							}
							break;
						case WindowEvent::e_keyCursorLeft:
							if(m_asStringEntry.cursor(spRA,i) > 0)
							{
								m_asStringEntry.cursor(spRA,i) -= 1;
							}
							break;
						case WindowEvent::e_keyCursorRight:
							if(m_asStringEntry.cursor(spRA,i) <
								int(m_asStringEntry.text(spRA,i).length()))
							{
								m_asStringEntry.cursor(spRA,i) += 1;
							}
							break;
						case WindowEvent::e_keyEscape:
							m_asStringEntry.text(spRA,i) = "";
							m_asStringEntry.cursor(spRA,i) = 0;
							break;
						default:
							s.insert(m_asStringEntry.cursor(spRA,i),
								1,(char)wev.item());
							m_asStringEntry.text(spRA,i) = s.c_str();
							m_asStringEntry.cursor(spRA,i) += 1;
							break;
					}
				}
			}
		}
	}
	if(request_on)
	{
		react("on_edit");
	}
	else if(request_off)
	{
		react("off_edit");
	}
}

void StringEntryController::fireSignal(Record &a_record)
{
	if(a_record.isValid())
	{
		if(m_asCallback.check(a_record))
		{
			if(m_asCallback.signal(a_record).isValid())
			{
				sp<SignalerI> spSignaler(m_asCallback.component(a_record));
				if(spSignaler.isValid())
				{
					spSignaler->signal(m_asCallback.signal(a_record));
				}
				else
				{
					sp<HandlerI> spHandler(m_asCallback.component(a_record));
					if(spHandler.isValid())
					{
						spHandler->handle(m_asCallback.signal(a_record));
					}
				}
			}
		}
		else
		{
			m_spSignaler->signal(a_record);
		}
	}
}


} /* namespace ext */
} /* namespace fe */

