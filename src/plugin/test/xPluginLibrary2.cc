/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

#include "platform/dlCore.cc"

#include "xInterface.h"

//* making sure that dependencies also get loaded (from xCatalogLibrary)
FE_DL_IMPORT void fe_catalog_library(I32 a_value);

class MyComponent2 : public MyComponent2I, public fe::CastableAs<MyComponent2>
{
	public:
virtual			~MyComponent2(void)											{}
virtual	int		test(int i)			{ return i*i*i; }
};

extern "C"
{

FE_DL_EXPORT fe::Library *CreateLibrary(fe::sp<fe::Master>)
{
	fe_catalog_library(7);

	fe::Library *pLibrary = fe::Memory::instantiate<fe::Library>();

	pLibrary->add<MyComponent2>("MyComponent2I.MyComponent2.test");

	return pLibrary;
}

}
