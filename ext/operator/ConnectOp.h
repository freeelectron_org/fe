/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_ConnectOp_h__
#define __operator_ConnectOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to connect points by UV values

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT ConnectOp:
	public OperatorSurfaceCommon,
	public Initialize<ConnectOp>
{
	public:

					ConnectOp(void)											{}
virtual				~ConnectOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_ConnectOp_h__ */
