import os
import sys
import string
forge = sys.modules["forge"]

def prerequisites():
    return [ "network" ]

def setup(module):
    srclist = [ "shm.pmh",
                "SharedMemCatalog",
                "shmDL",
                ]

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexNetworkDLLib" ]

    dll = module.DLL( "fexShmDL", srclist)

    dll.linkmap = {}

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["rt"] = "-lrt"

    forge.deps( ["fexShmDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexShmDL",                 None,       None) ]

def auto(module):
    if forge.fe_os == "FE_LINUX":

        forge.linkmap["rt"] = "-lrt"

    test_file = """
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

int main(void)
{
    const char* path="/path";
    int fd = shm_open(path, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
    shm_unlink(path);

    sem_t sem;
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME,&ts);
    sem_timedwait(&sem,&ts);
    return(0);
}
    \n"""

    result = forge.cctest(test_file)

    forge.linkmap.pop('rt', None)

    return result
