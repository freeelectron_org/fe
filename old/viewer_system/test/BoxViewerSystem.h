/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer_system/viewer_system.h"

namespace fe
{
namespace ext
{

/**
 * Test-only.
 *
 * Simple Box Viewer System used for unit testing.
 */
class BoxViewerSystem : virtual public ViewerSystem
{
public:
    void updateBoxes(const t_note_id &note_id)
    {
		boxAccessorSet.bind(m_dataset->scope());

        // Find box records.
		boxAccessorSet.filter(boxRecords, m_dataset);
    };

	void draw(Record viewportRecord) override
    {
		const Color green(0.0,1.0,0.0,1.0);

        // Get the pointer to the draw interface.
		sp<DrawI> draw = m_viewportAccessorSet.draw(viewportRecord);

        // Loop over each box.
        for (Record boxRecord : boxRecords)
        {
            // DrawI->drawTransformedBoxes() draws boxes from the corners.
            // We want to draw the box from the center so we just do a little
            // translation.
            SpatialTransform drawBoxCenter = boxAccessorSet.transform(boxRecord);
            translate(drawBoxCenter, -boxAccessorSet.size(boxRecord)/2);

            draw->drawTransformedBoxes(
                &drawBoxCenter,
                &boxAccessorSet.size(boxRecord),
                1,
                false,
                &green);
        }
    };

    void connectOrchestrator(sp<OrchestratorI> orchestrator) override
    {
        ViewerSystem::connectOrchestrator(orchestrator);

        orchestrator->connect(this, &BoxViewerSystem::updateBoxes, "boxes_added");
        orchestrator->connect(this, &BoxViewerSystem::updateBoxes, FE_NOTE_COMPILE);
    }

    AsBox boxAccessorSet;
    std::vector<Record> boxRecords;
};

} /* namespace ext */
} /* namespace fe */
