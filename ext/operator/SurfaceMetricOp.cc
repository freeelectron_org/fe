/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SMO_DEBUG	FALSE
#define FE_SMO_DUMP		FALSE

namespace fe
{
namespace ext
{

void SurfaceMetricOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	catalog<String>("metric")="sdf";
	catalog<String>("metric","label")="Metric";
	catalog<String>("metric","choice:0")="sdf";
	catalog<String>("metric","label:0")="Signed Distance Function";
	catalog<String>("metric","choice:1")="constant";
	catalog<String>("metric","label:1")="Constant (min or max)";
	catalog<bool>("metric","joined")=true;

	catalog<bool>("invert")=false;
	catalog<String>("invert","label")="Invert";

	catalog<String>("valueAttr")="value";
	catalog<String>("valueAttr","label")="Value Attribute";
	catalog<String>("valueAttr","hint")=
			"Name of point attribute to place results.";

	catalog<bool>("partitionInput")=false;
	catalog<String>("partitionInput","label")="Partition Input";
	catalog<bool>("partitionInput","joined")=true;
	catalog<String>("partitionInput","hint")=
			"Separate primitives into non-overlapping UV spaces.";

	catalog<String>("partitionAttr")="part";
	catalog<String>("partitionAttr","label")="Partition Attribute";
	catalog<String>("partitionAttr","enabler")="partitionInput";
	catalog<String>("partitionAttr","hint")=
			"Primitive string attribute on primary input surface"
			" describing membership in a non-overlapping UV space."
			"  This attribute will be transferred to the output points.";

	catalog<bool>("alignmentCheck")=false;
	catalog<String>("alignmentCheck","label")="Check Alignment";
	catalog<bool>("alignmentCheck","joined")=true;
	catalog<String>("alignmentCheck","hint")=
			"Disregard faces with divergent normals."
			"  This can be useful to ignore the inside faces of a"
			" two sided Contrasting Surface, such as belt against the skin.";

	catalog<Real>("alignmentDot")=0.0;
	catalog<Real>("alignmentDot","min")= -1.0;
	catalog<Real>("alignmentDot","max")=1.0;
	catalog<String>("alignmentDot","label")="Threshold";
	catalog<String>("alignmentDot","enabler")="alignmentCheck";
	catalog<String>("alignmentDot","hint")=
			"When checking alignment, the normal of each sample point"
			" on the Input Surface is compared to candidate contacts"
			" on the Contrasting Surface."
			"  If the dot product of the normals is less than"
			" this threshold, a candidate is dismissed.";

	catalog<Real>("minimum")= -1.0;
	catalog<Real>("minimum","low")= -10.0;
	catalog<Real>("minimum","high")=0.0;
	catalog<Real>("minimum","min")= -1e6;
	catalog<Real>("minimum","max")=1e6;
	catalog<String>("minimum","label")="Min";
	catalog<bool>("minimum","joined")=true;

	catalog<Real>("maximum")=1.0;
	catalog<Real>("maximum","low")=0.0;
	catalog<Real>("maximum","high")=10.0;
	catalog<Real>("maximum","min")= -1e6;
	catalog<Real>("maximum","max")=1e6;
	catalog<String>("maximum","label")="Max";

	catalog<I32>("width")=256;
	catalog<I32>("width","high")=1024;
	catalog<String>("width","label")="Width";
	catalog<bool>("width","joined")=true;

	catalog<I32>("height")=256;
	catalog<I32>("height","high")=1024;
	catalog<String>("height","label")="Height";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Contrasting Surface");
	catalog<bool>("Contrasting Surface","optional")=true;

	//* don't copy input to output
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void SurfaceMetricOp::handle(Record& a_rSignal)
{
	catalog<String>("summary")="";

	const String metric=catalog<String>("metric");
	const BWORD invert=catalog<bool>("invert");
	const String valueAttr=catalog<String>("valueAttr");
	const BWORD partitionInput=catalog<bool>("partitionInput");
	const String partitionAttr=
			partitionInput? catalog<String>("partitionAttr"): "";
	const Real minimum=catalog<Real>("minimum");
	const Real maximum=catalog<Real>("maximum");
	const I32 width=catalog<I32>("width");
	const I32 height=catalog<I32>("height");

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputUVs;
	if(!access(spOutputUVs,spOutputAccessible,e_point,e_uv)) return;

	sp<SurfaceAccessorI> spOutputWidth;
	if(!access(spOutputWidth,spOutputAccessible,
			e_detail,"raster_width")) return;

	sp<SurfaceAccessorI> spOutputHeight;
	if(!access(spOutputHeight,spOutputAccessible,
			e_detail,"raster_height")) return;

	sp<SurfaceAccessorI> spOutputMin;
	if(!access(spOutputMin,spOutputAccessible,
			e_detail,"raster_min")) return;

	sp<SurfaceAccessorI> spOutputMax;
	if(!access(spOutputMax,spOutputAccessible,
			e_detail,"raster_max")) return;

	sp<SurfaceAccessorI> spOutputPartAttr;
	if(!access(spOutputPartAttr,spOutputAccessible,
			e_detail,"raster_partition")) return;

	sp<SurfaceAccessorI> spOutputValue;
	if(!access(spOutputValue,spOutputAccessible,e_point,valueAttr)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceI> spContrast;
	if(!access(spContrast,"Contrasting Surface",e_quiet) && metric=="sdf")
	{
		catalog<String>("error")+=
				"Contrasting Surface required for SDF metric;";
		return;
	}

	sp<SurfaceI> spInput;
	if(!access(spInput,spInputAccessible)) return;

	spInput->setRefinement(0);

	sp<SurfaceAccessorI> spInputNormals;
	if(!access(spInputNormals,spInputAccessible,e_point,e_normal,e_quiet) &&
		!access(spInputNormals,spInputAccessible,e_vertex,e_normal,e_quiet))
	{
		catalog<String>("error")+="normals required on input surface;";
		return;
	}

	BWORD alignmentCheck=catalog<bool>("alignmentCheck");
	const Real alignmentDot=catalog<Real>("alignmentDot");
	if(alignmentCheck)
	{
		sp<SurfaceAccessorI> spContrastNormals;

		if(!access(spContrastNormals,"Contrasting Surface",
				e_point,e_normal,e_quiet))
		{
			catalog<String>("warning")+=
					"alignment check requires normals on contrasting surface;";
			alignmentCheck=FALSE;
		}
	}

	sp<SurfaceAccessorI> spOutputPartition;

	U32 mapCount=1;
	if(partitionInput)
	{
		spInput->partitionWith(partitionAttr);
		spInput->prepareForUVSearch();
		mapCount=spInput->partitionCount();

		if(!access(spOutputPartition,spOutputAccessible,
				e_point,partitionAttr)) return;
	}

#if FE_SMO_DUMP
		feLog("mapCount %d partition %d \"%s\"\n",
				mapCount,partitionInput,partitionAttr.c_str());
#endif

	spOutputWidth->set(0,width);
	spOutputHeight->set(0,height);
	spOutputMin->set(0,minimum);
	spOutputMax->set(0,maximum);
	spOutputPartAttr->set(0,partitionAttr);

	const Real maxDistance=maximum-minimum;

	U32 pointCount=0;

	//* TODO create each map in parallel

	for(U32 mapIndex=0;mapIndex<mapCount;mapIndex++)
	{
		String partition;
		if(partitionInput)
		{
			partition=spInput->partitionName(mapIndex);
			spInput->setPartitionFilter(partition);
		}

#if FE_SMO_DUMP
		feLog("map %d/%d partition \"%s\"\n",
				mapIndex,mapCount,partition.c_str());
#endif

		for(I32 vIndex=0;vIndex<height;vIndex++)
		{
			const Real vCoord=vIndex/(height-1.0);

			for(I32 uIndex=0;uIndex<width;uIndex++)
			{
				if(interrupted())
				{
					vIndex=height;
					mapIndex=mapCount;
					break;
				}

				const Real uCoord=uIndex/(width-1.0);

				const Vector2 uv(uCoord,vCoord);
				const SpatialTransform sample=spInput->sample(uv);
				const SpatialVector point=sample.translation();
				const SpatialVector normal=sample.column(1);

				Real value=invert? maximum: minimum;

#if	FE_SMO_DEBUG
				feLog("\n%d,%d %s\n",uIndex,vIndex,partition.c_str());
#endif

				if(metric=="sdf")
				{
					const SpatialVector origin=
							point+normal*(invert? minimum: -maximum);

					const U32 maxHits=alignmentCheck? 2: 1;
					Array< sp<SurfaceI::ImpactI> > impactArray=
							spContrast->rayImpacts(origin,normal,
							maxDistance,maxHits);
					const U32 hitCount=impactArray.size();
					for(U32 hitIndex=0;hitIndex<hitCount;hitIndex++)
					{
						sp<SurfaceI::ImpactI>& rspImpact=impactArray[hitIndex];

						if(alignmentCheck)
						{
							const SpatialVector hitNormal=rspImpact->normal();

#if	FE_SMO_DEBUG
							feLog("metric hit %d/%d normal %s vs %s\n",
									hitIndex,hitCount,
									c_print(hitNormal),c_print(normal));
#endif

							if(dot(hitNormal,normal)<alignmentDot)
							{
								continue;
							}
						}

						const Real distance=rspImpact->distance();
						value=invert? minimum+distance: maximum-distance;

#if	FE_SMO_DEBUG
						feLog("  dist %.6G value %.6G\n",distance,value);
#endif
						break;
					}
				}

				spDrawI->drawPoints(&point,NULL,1,false,NULL);

				spOutputUVs->set(pointCount,uv);
				spOutputValue->set(pointCount,value);

				if(spOutputPartition.isValid())
				{
					spOutputPartition->set(pointCount,partition);
				}

				pointCount++;

#if FE_SMO_DUMP
				feLog("%3d ",I32(value*100));
#endif
			}
#if FE_SMO_DUMP
			feLog("\n");
#endif
		}
	}

	catalog<String>("summary")=valueAttr;
}

} /* namespace ext */
} /* namespace fe */
