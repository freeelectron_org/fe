/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_h__
#define __solve_h__

#include "fe/data.h"
#include "math/math.h"
#include "thread/thread.h"

#ifdef MODULE_solve
#define FE_SOLVE_PORT FE_DL_EXPORT
#else
#define FE_SOLVE_PORT FE_DL_IMPORT
#endif

namespace fe
{

typedef double						t_solve_real;
typedef Vector<2, t_solve_real>		t_solve_v2;
typedef Vector<3, t_solve_real>		t_solve_v3;
typedef Matrix<3,3,t_solve_real>	t_solve_matrix;
typedef Matrix<2,2,t_solve_real>	t_solve_m2;
typedef Vector<2, t_solve_real>		t_solve_v2;
const t_solve_v3 solveZeroVector(0.0,0.0,0.0);

namespace ext
{


inline
void copy(t_solve_v3 &a_solve_v3, const SpatialVector &a_SpatialVector)
{
	a_solve_v3[0] = a_SpatialVector[0];
	a_solve_v3[1] = a_SpatialVector[1];
	a_solve_v3[2] = a_SpatialVector[2];
}
} /* namespace ext */
} /* namespace fe */

#include "solve/MassI.h"

#include "solve/DenseVector.h"
#include "solve/SparseArray.h"
#include "solve/SparseMatrix.h"
#include "solve/SparseMatrix2x2.h"
#include "solve/SparseMatrix3x3.h"
#include "solve/BlockPCG.h"
#include "solve/IncompleteSparse.h"
#include "solve/BiconjugateGradient.h"
#include "solve/BCGThreaded.h"
#include "solve/ConjugateGradient.h"
#include "solve/solveAS.h"

#include "solve/SemiImplicit2D.h"
#include "solve/SemiImplicit1D.h"
#include "solve/SemiImplicit.h"

#include "solve/Splat.h"

namespace fe
{
namespace ext
{

fe::Library* CreateSolveLibrary(fe::sp<fe::Master> spMaster);

} /* namespace ext */
} /* namespace fe */

#endif // __solve_h__
