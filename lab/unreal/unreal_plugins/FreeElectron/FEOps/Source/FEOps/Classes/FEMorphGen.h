#pragma once

#include "FEImportOp.h"

//* must be last include
#include "FEMorphGen.generated.h"

UCLASS(Blueprintable,BlueprintType)
class AFEMorphGen: public AFEImportOp
{
        GENERATED_UCLASS_BODY()

	protected:

virtual	bool	UpdateConfiguration(fe::String a_configuration) override;

        UPROPERTY(BlueprintReadWrite, Category="Operator",
				DisplayName="SkeletalMesh")
		USkeletalMesh*	m_pSkeletalMesh;

        UPROPERTY(EditAnywhere, Category="Operator",
				DisplayName="MorphTarget Name")
		FString		m_fsTargetName;

		UFUNCTION(BlueprintCallable, Category="Operator")
		bool	ExportVertexMap();

        UPROPERTY(EditAnywhere, Category="Operator",
				DisplayName="VertexMap Filename")
		FString		m_fsVertexMapfile;

};
