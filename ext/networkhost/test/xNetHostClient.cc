/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "networkhost/networkhost.h"
#include "math/math.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			milliSleep(100);

			sp<NetHost> spNetHost(NetHost::create());
			FEASSERT(spNetHost.isValid());

			sp<Master> spMaster=spNetHost->master();
			sp<Registry> spRegistry=spMaster->registry();

//~			assertMath(spMaster->typeMaster());

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			sp<StateCatalog> spStateCatalog=
					spNetHost->accessSpace("world",implementation);
			FEASSERT(spStateCatalog.isValid());

			if(implementation=="*.SharedMemCatalog")
			{
				result=spStateCatalog->configure("/xNetHostServer role=client");
			}
			else
			{
				result=spStateCatalog->configure("localhost:7890 role=client");
			}

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			feLog("StateCatalog started\n");

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				spStateCatalog->setState<Real>("nanoDelay",1);
				spStateCatalog->flush();

				String netID;
				spStateCatalog->getState<String>("net:id",netID);
				feLog("net:id \"%s\"\n",netID.c_str());

				SpatialTransform xform;
				setIdentity(xform);

				const I32 sampleStart=1000;
				const I32 sampleCount=100;
				I32 sampleIndex=0;
				U32 lastTimeMS=0;
				U32* timeFrame=new U32[sampleCount];
				U32* timeSample=new U32[sampleCount];

				SystemTicker ticker;

				const I32 maxPolls=5000;
				I32 polls=0;
				I32 flushCount=0;
				I32 frame= -1;
				I32 unique=0;
				bool running=true;
				I32 firstFrame= -1;
				while(running)
				{
					spStateCatalog->flush();

					const I32 lastFrame=frame;

#if FALSE
					{
						StateCatalog::Atomic atomic(spStateCatalog,
								flushCount);
						result=atomic.getState<bool>("running",running);
						result=atomic.getState<I32>("frame",frame);
						result=atomic.getState<SpatialTransform>(
								"instances[0].transform",xform);
					}
#else
					const I32 lastFlushCount=flushCount;
					sp<StateCatalog::Snapshot> spSnapshot;
					while(flushCount==lastFlushCount)
					{
						result=spStateCatalog->snapshot(spSnapshot);
						flushCount=spSnapshot->flushCount();
					}

//					feLog("Snapshot %d\n",spSnapshot->serial());
//					spSnapshot->dump();
//					spSnapshot->dumpUpdates();

#if FALSE
					Array<U8> byteBlock(5);
					spSnapshot->getState< Array<U8> >("byteBlock",byteBlock);
					const I32 byteCount=byteBlock.size();
					printf("byteBlock of %d:",byteCount);
					for(I32 byteIndex=0;byteIndex<byteCount;byteIndex++)
					{
						printf("  %d:%c",
								byteBlock[byteIndex],byteBlock[byteIndex]);
					}
					printf("\n");
#endif

					spSnapshot->getState<bool>("running",running);
					spSnapshot->getState<I32>("frame",frame);
					spSnapshot->getState<SpatialTransform>(
							"instances[0].transform",xform);
#endif

					if(firstFrame<0)
					{
						firstFrame=frame;

						spStateCatalog->setState<I32>(
								netID+":firstFrame",firstFrame);
						spStateCatalog->flush();
					}

					if(frame%100 == 50)
					{
						spStateCatalog->sendMessage<String>(
								"request","Hello server!");

						String message;
						message.sPrintf("client %s is on frame %d",
								netID.c_str(),frame);
						spStateCatalog->sendMessage<String>(
								"request",message);

						spStateCatalog->flush();
					}

//					feLog("running %d frame %d\n",running,frame);
//					feLog("transform\n%s\n",c_print(xform));

					const U32 timeMS=ticker.timeMS();
					const U32 deltaTimeMS=timeMS-lastTimeMS;
					lastTimeMS=timeMS;
					if(frame>=firstFrame+sampleStart &&
							sampleIndex<sampleCount)
					{
						timeFrame[sampleIndex]=frame;
						timeSample[sampleIndex]=deltaTimeMS;
						sampleIndex++;
					}

					if(successful(result))
					{
						const I32 tx=I32(xform.translation()[0]);
						if(frame!=lastFrame)
						{
							unique++;
						}
						if(frame!=tx)
						{
							feLog("frame %d %d\n",frame,tx);
						}
					}

					if(++polls > maxPolls)
					{
						feLog("reached max polls %d\n",maxPolls);
						break;
					}
				}

				feLog("firstFrame %d\n",firstFrame);
				feLog("final %d polls %d unique %d\n",frame,polls,unique);

				UNIT_TEST(!running);
				UNIT_TEST(frame==3999);
				UNIT_TEST(firstFrame+polls>3000 && firstFrame+polls<=4050);
				UNIT_TEST(firstFrame+unique>3000 && firstFrame+unique<=4000);

				setIdentity(xform);
				result=spStateCatalog->getState<SpatialTransform>(
						"instances[0].transform",xform);
				UNIT_TEST(successful(result));

				feLog("last transform:\n%s\n",c_print(xform));

				spStateCatalog->stop();

				for(I32 index=0;index<sampleIndex;index++)
				{
					if(index && timeFrame[index]!=timeFrame[index-1]+1)
					{
						feLog("--missing frames--\n");
					}
					feLog("sample %d frame %d %dms\n",
							index,timeFrame[index],timeSample[index]);
				}

				delete[] timeSample;
				delete[] timeFrame;

				feLog("End State:\n");
				spStateCatalog->catalogDump();
			}

			spNetHost=NULL;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(9);
	UNIT_RETURN();
}
