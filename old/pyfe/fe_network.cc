/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/network.h"
#include "fex/pyfe.h"
#include <boost/python.hpp>
using namespace boost::python;
#include "pyfe/fe_data.h"
#include "pyfe/fe_signal.h"
#include "pyfe/fe_network.h"

void networkExceptionTranslator(fe::Exception const& e)
{
	PyErr_SetString(PyExc_UserWarning, e.getMessage().c_str());
}

BOOST_PYTHON_MODULE(network)
{
	register_exception_translator<fe::Exception>(&networkExceptionTranslator);
	class_<ClientFS>("ClientFS", init<PyObject *>())
		.def("setScope",	&ClientFS::setScope)
		.def("addLayout",	&ClientFS::addLayout)
		.def("start",		&ClientFS::start)
		.def("stop",		&ClientFS::stop)
		.def("object",		&ClientFS::object)
	;
	class_<ServerFS>("ServerFS", init<PyObject *>())
		.def("addLayout",	&ServerFS::addLayout)
		.def("start",		&ServerFS::start)
		.def("stop",		&ServerFS::stop)
		.def("object",		&ServerFS::object)
	;
}

void ClientFS::setScope(const ScopePtr &scopePtr)
{
	ptr()->setScope(scopePtr.ptr());
}

void ClientFS::addLayout(const LayoutPtr &layoutPtr)
{
	ptr()->addLayout(layoutPtr.ptr());
}

void ClientFS::start(SignalerFS &signalerFS, const std::string &host, UWORD port, const LayoutPtr &layoutPtr)
{
	fe::String string_addr = host.c_str();
	fe::SockAddr sock_addr;
	sock_addr.setAddress(string_addr);
	sock_addr.setPort(port);
	ptr()->start(signalerFS.ptr(), sock_addr, layoutPtr.ptr());
}

void ClientFS::stop(void)
{
	ptr()->stop();
}

void ServerFS::addLayout(const LayoutPtr &layoutPtr)
{
	ptr()->addLayout(layoutPtr.ptr());
}

void ServerFS::start(SignalerFS &signalerFS, const std::string &host, UWORD port, const LayoutPtr &layoutPtr)
{
	fe::String string_addr = host.c_str();
	fe::SockAddr sock_addr;
	sock_addr.setAddress(string_addr);
	sock_addr.setPort(port);
	ptr()->start(signalerFS.ptr(), sock_addr, layoutPtr.ptr());
}

void ServerFS::stop(void)
{
	ptr()->stop();
}


