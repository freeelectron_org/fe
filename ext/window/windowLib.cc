/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <window/window.pmh>

namespace fe
{
namespace ext
{

Library* CreateWindowLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->setName("default_window");

	pLibrary->add<LogWindowHandler>("HandlerI.LogWindowHandler.fe");
	pLibrary->add<ConsoleKeys>("KeyI.ConsoleKeys.fe");

	pLibrary->add<AsSelect>("PopulateI.AsSelect.fe");
	pLibrary->add<AsSelection>("PopulateI.AsSelection.fe");
	pLibrary->add<AsPick>("PopulateI.AsPick.fe");
	pLibrary->add<AsColor>("PopulateI.AsColor.fe");
	pLibrary->add<AsSelectable>("PopulateI.AsSelectable.fe");
	pLibrary->add<AsOrtho>("PopulateI.AsOrtho.fe");
	pLibrary->add<AsPerspective>("PopulateI.AsPerspective.fe");
	pLibrary->add<AsCallback>("PopulateI.AsCallback.fe");
	pLibrary->add<AsSelScreenTriangle>("PopulateI.AsSelScreenTriangle.fe");
	pLibrary->add<AsSelWorldSphere>("PopulateI.AsSelWorldSphere.fe");
	pLibrary->add<AsSelWorldTriangle>("PopulateI.AsSelWorldTriangle.fe");

	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
