/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <sdl/sdl.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexWindowDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->addSingleton<ContextSDL>("Component.ContextSDL.fe");
	pLibrary->add<ImageSDL>("ImageI.ImageSDL.fe");

#if FE_OS!=FE_OSX
	pLibrary->add<JoySDL>("JoyI.JoySDL.fe");
#endif
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	spLibrary->registry()->prioritize("ImageI.ImageSDL.fe",-2);
}

}
