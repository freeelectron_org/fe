#pragma once

#include "FEOperator.h"

//* must be last include
#include "FETreeOp.generated.h"

USTRUCT()
struct FMyStruct
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY()
        FString TestString;
};

UCLASS(hidecategories = (Object, LOD),
	meta = (BlueprintSpawnableComponent),
	ClassGroup = Rendering)
class AFETreeOp: public AFEOperator
{
        GENERATED_UCLASS_BODY()

	protected:

virtual	void	OperatorInit(void);
virtual	void	OperatorUpdate(void);

        UPROPERTY(VisibleAnywhere, Category="Operator")
        FMyStruct MyStruct;
};
