/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_VLM_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void ValueManipulator::initialize(void)
{
}

void ValueManipulator::handle(Record& a_rSignal)
{
#if FE_VLM_DEBUG
	feLog("ValueManipulator::handle\n");
#endif

	//* HACK (should use signaler)
	sp<SignalerI> spSignaler;
	sp<Layout> spLayout=a_rSignal.layout();
	sp<Scope> spScope=spLayout->scope();
	handleBind(spSignaler,spLayout);

	ManipulatorCommon::handle(a_rSignal);
}

void ValueManipulator::updateGrips(void)
{
#if FE_VLM_DEBUG
	feLog("ValueManipulator::updateGrips\n");
#endif

	ManipulatorCommon::updateGrips();

	if(!catalogOrDefault<bool>("show",true))
	{
		m_gripArray.resize(0);
		return;
	}

	sp<ViewI> spView=m_spDrawCache->view();
	const Box2i viewport=spView->viewport();
//	const I32 width=viewport.size()[0];
	const I32 height=viewport.size()[1];

	const Real dark=1.0-dimming();

	const Color black(0.0,0.0,0.0);
	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color cyan(0.0,1.0,1.0);
	const Color magenta(1.0,0.0,1.0);
	const Color yellow(1.0,1.0,0.0);
	const Color white(1.0,1.0,1.0);
	const Color lightblue(0.5,0.5,1.0);

	const String selection=catalogOrDefault<String>("selection","");

	Array<String> keys;
	catalogKeys(keys);

	const U32 keyCount=keys.size();

	m_gripArray.resize(keyCount);

	U32 valueCount=0;
	for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String& key=keys[keyIndex];
		const String label=catalogOrDefault<String>(key,"label","");

		if(!label.empty())
		{
			valueCount++;
		}
	}

	const I32 valueHeight=32;
	const I32 top=(height+valueHeight*valueCount)/2;

	U32 gripCount=0;
	for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String& key=keys[keyIndex];
		const String label=catalogOrDefault<String>(key,"label","");

		if(label.empty())
		{
			continue;
		}

		Color rectColor=catalogOrDefault<SpatialVector>(key,"color",red);

		Color textColor=magenta;

		if(m_hotGrip!=I32(gripCount))
		{
			textColor*=dark;
			rectColor*=dark;
		}
		textColor[3]=1.0;
		rectColor[3]=1.0;

		SpatialVector cornerPoint(4.0,top-valueHeight*(gripCount+1));

		const Real value=catalog<Real>(key);
		const Real min=catalog<Real>(key,"min");
		const Real max=catalog<Real>(key,"max");
		const Real fraction=(value-min)/(max-min);

		const Real sensitivity=0.1*(max-min);

		String valueText;
		valueText.sPrintf("%.2f",value);

//		feLog("key %d/%d %dv%d %.3f \"%s\"\n",
//				keyIndex,keyCount,gripCount,m_hotGrip,value,key.c_str());

		SpatialVector rect[2];
		rect[0]=SpatialVector(cornerPoint[0],cornerPoint[1]);
		rect[1]=rect[0]+SpatialVector(64.0,8.0);

		Box2i backbox;
		set(backbox,rect[0][0]-2,rect[0][1]-2,
				rect[1][0]-rect[0][0]+4,rect[1][1]-rect[0][1]+4);

		const SpatialVector hotPoint(rect[0][0]+0.5*(rect[1][0]-rect[0][0]),
				rect[1][1]+7.0);

		const SpatialVector numPoint(rect[1][0]+4.0,rect[0][1]);

		rect[0][0]=rect[0][0]+fraction*(rect[1][0]-rect[0][0]);

		m_spOverlayCache->drawRectangles(rect,2,false,&black);

		rect[1][0]=rect[0][0];
		rect[0][0]=cornerPoint[0];

		m_spOverlayCache->drawRectangles(rect,2,false,&rectColor);
		m_spOverlayCache->drawBox(backbox,white);

		drawLabel(m_spOverlayCache,numPoint,valueText,FALSE,3,
				textColor,NULL,&black);

		m_gripArray[gripCount].m_hotSpot=hotPoint;
		m_gripArray[gripCount].m_alignment[0]=sensitivity*m_rightways;
		m_gripArray[gripCount].m_key[0]=key;
		m_gripArray[gripCount].m_label=label;
		m_gripArray[gripCount].m_message=selection;

		gripCount++;
	}

	m_gripArray.resize(gripCount);
}
