/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <openil/openil.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
//	list.append(new String("fexSignalDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<ImageIL>("ImageI.ImageIL.fe");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.bmp");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.exr");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.jpg");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.pgm");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.png");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.rgb");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.tga");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.tif");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.tiff");
	pLibrary->add<ImageIL>("ImageI.ImageIL.fe.xpm");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.bmp",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.exr",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.jpg",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.pgm",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.png",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.rgb",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.tga",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.tif",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.tiff",2);
	spLibrary->registry()->prioritize("ImageI.ImageIL.fe.xpm",2);
}

}
