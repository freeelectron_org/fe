/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __driveline_drivelineAS_h__
#define __driveline_drivelineAS_h__

namespace fe
{
namespace ext
{

/// @brief Driveline Model
class AsDrivelineModel
	: public AsNamed, public Initialize<AsDrivelineModel>
{
	public:
		AsConstruct(AsDrivelineModel);
		void initialize(void)
		{
			add(component,				FE_USE("driveline:component"));
		}
		Accessor<String>		component;
};

/// @brief Driveline Shaft
class AsShaft
	: public AccessorSet, public Initialize<AsShaft>
{
	public:
		AsConstruct(AsShaft);
		void initialize(void)
		{
			add(name,				FE_USE("name"));
			add(momentOfInertia,	FE_USE("driveline:momentOfInertia"));
		}
		Accessor< String >		name;
		Accessor< t_atomic_real >	momentOfInertia;

};

/// @brief
class AsExternalTorque
	: public AccessorSet, public Initialize<AsExternalTorque>
{
	public:
		AsConstruct(AsExternalTorque);
		void initialize(void)
		{
			add(externalTorque,		FE_USE("driveline:externalTorque"));
		}
		Accessor< t_atomic_real >	externalTorque;

};

/// @brief Run Time Driveline Shaft
class AsShaftLive
	: public AsShaft, public Initialize<AsShaftLive>
{
	public:
		AsConstruct(AsShaftLive);
		void initialize(void)
		{
			// These driveline:* versions may be ratio different than solver
			add(position,			FE_USE("driveline:position"));
			add(rps,				FE_USE("driveline:rps"));
			add(torque,				FE_USE("driveline:torque"));
		}
		Accessor< t_atomic_real >	position;
		Accessor< t_atomic_real >	rps;
		Accessor< t_atomic_real >	torque;

};

/// @brief Driveline
class AsDriveline
	: public AccessorSet, public Initialize<AsDriveline>
{
	public:
		AsConstruct(AsDriveline);
		void initialize(void)
		{
			add(model,				FE_USE("driveline:model"));
			add(contents,			FE_USE("driveline:contents"));
		}
		Accessor< String >					model;
		Accessor< sp<RecordGroup> >		contents;

};

/// @brief Run Time Driveline
class AsDrivelineLive
	: public AsDriveline, public Initialize<AsDrivelineLive>
{
	public:
		AsConstruct(AsDrivelineLive);
		void initialize(void)
		{
			add(spDrivelineI,			FE_USE("driveline:spDrivelineI"));
		}
		Accessor< sp<DrivelineI> >	spDrivelineI;
};

/// @brief Rotational Mechanical System
class AsRotationalMechanical
	: public AccessorSet, public Initialize<AsRotationalMechanical>
{
	public:
		AsConstruct(AsRotationalMechanical);
		void initialize(void)
		{
			add(model,				FE_USE("driveline:model"));
			add(contents,			FE_USE("driveline:contents"));
		}
		Accessor< String >				model;
		Accessor< sp<RecordGroup> >		contents;

};

/// @brief Run Time Rotational Mechanical System
class AsRotationalMechanicalLive
	: public AsRotationalMechanical,
	public Initialize<AsRotationalMechanicalLive>
{
	public:
		AsConstruct(AsRotationalMechanicalLive);
		void initialize(void)
		{
			add(spDrivelineI,			FE_USE("driveline:spDrivelineI"));
		}
		Accessor< sp<DrivelineI> >	spDrivelineI;
};

class AsGearTrain
	: public AccessorSet,
	public Initialize<AsGearTrain>
{
	public:
		AsConstruct(AsGearTrain);
		void initialize(void)
		{
			add(left_gear,			FE_USE("driveline:left_gear"));
			add(right_gear,			FE_USE("driveline:right_gear"));
		}
		Accessor<int>			left_gear;
		Accessor<int>			right_gear;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_drivelineAS_h__ */

