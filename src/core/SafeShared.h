/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_SafeShared_h__
#define __core_SafeShared_h__

// TODO prefix methods as safe*, like safeUnlock()

namespace fe
{

class SafeSharedCore
{
	public:
virtual			~SafeSharedCore(void)										{}

virtual	void	safeLock(void) const										=0;
virtual	void	safeUnlock(void) const										=0;
virtual	bool	safeTryLock(void) const										=0;

virtual	void	safeLockShared(void) const									=0;
virtual	void	safeUnlockShared(void) const								=0;
virtual	bool	safeTryLockShared(void) const								=0;

};

class SafeSharedBase: public SafeSharedCore
{
	public:
virtual			~SafeSharedBase(void)										{}
		// returns a pointer because NotSafeShared does not require a mutex
virtual	Mutex	*fe_thread_mutex(void) const								=0;
};

class ClassSafeSharedBase: public SafeSharedCore
{
	public:
virtual			~ClassSafeSharedBase(void)										{}
		// returns a pointer because NotSafeShared does not require a mutex
virtual	Mutex	*fe_class_thread_mutex(void) const							=0;
};

template <typename T>
class FE_DL_EXPORT trueClassSafeShared: public ClassSafeSharedBase
{
	public:

virtual	void	safeLock(void) const
				{	fe_class_thread_mutex()->lock(); }
virtual	void	safeUnlock(void) const
				{	fe_class_thread_mutex()->unlock(); }
virtual	bool	safeTryLock(void) const
				{	return fe_class_thread_mutex()->tryLock(); }

virtual	void	safeLockShared(void) const
				{	fe_class_thread_mutex()->lockReadOnly(); }
virtual	void	safeUnlockShared(void) const
				{	fe_class_thread_mutex()->unlockReadOnly(); }
virtual	bool	safeTryLockShared(void) const
				{	return fe_class_thread_mutex()->tryLockReadOnly(); }

#if FE_SAFE_COUNTED_MUTEX

				trueClassSafeShared(void)
				{
//					feLog("%p trueClassSafeShared<%s> %d %p\n",
//							this,FE_TYPESTRING(T).c_str(),
//							ms_mutexCount,&ms_pMutex);

//					if(ms_mutexCount++ == 0)
					if(!feAsmSwapIncr(&ms_mutexCount,1) && !ms_pMutex)
					{
						ms_pMutex=new Mutex;
						Memory::markForDeath(ms_pMutex,&ms_mutexCount);
					}
					FEASSERT(ms_pMutex);
				}

virtual			~trueClassSafeShared(void)
				{
//					feLog("%p ~trueClassSafeShared<%s> %d %p\n",
//							this,FE_TYPESTRING(T).c_str(),
//							ms_mutexCount,&ms_pMutex);

					FEASSERT(ms_pMutex);

//					if(--ms_mutexCount == 0)
					if(!(feAsmSwapIncr(&ms_mutexCount,-1)-1))
					{
						//* NOTE marked for death
//						delete ms_pMutex;
//						ms_pMutex=NULL;
					}
				}

virtual	Mutex* fe_class_thread_mutex(void) const
				{
//					feLog("%p fe_class_thread_mutex %d %p %p\n",
//							this,ms_mutexCount,&ms_pMutex,ms_pMutex);
					return ms_pMutex;
				}


	private:
static	FE_DL_PUBLIC	I32						ms_mutexCount;
static	FE_DL_PUBLIC	Mutex*					ms_pMutex;

#else

virtual	Mutex* fe_class_thread_mutex(void) const
				{
//					feLog("%p fe_class_thread_mutex %p\n",this,&ms_mutex);
					return &ms_mutex;
				}

	private:
static	FE_DL_PUBLIC	Mutex		ms_mutex;

#endif
};

template <typename T>
class FE_DL_EXPORT trueObjectSafeShared : public SafeSharedBase
{
	public:

				trueObjectSafeShared(void)
				{
					//* force initialization
					safeLock();
					safeUnlock();
				}
virtual			~trueObjectSafeShared(void)									{}

virtual	Mutex	*fe_thread_mutex(void) const
		{
			/*	cast away const here so that ObjectSafeShared can be
				used on const accessors that would otherwise be vulnerable
				to non-const mutators */
			return const_cast<Mutex*>(&m_fe_mutex);
		}

virtual	void	safeLock(void) const
				{	fe_thread_mutex()->lock(); }
virtual	void	safeUnlock(void) const
				{	fe_thread_mutex()->unlock(); }
virtual	bool	safeTryLock(void) const
				{	return fe_thread_mutex()->tryLock(); }

virtual	void	safeLockShared(void) const
				{	fe_thread_mutex()->lockReadOnly(); }
virtual	void	safeUnlockShared(void) const
				{	fe_thread_mutex()->unlockReadOnly(); }
virtual	bool	safeTryLockShared(void) const
				{	return fe_thread_mutex()->tryLockReadOnly(); }

	private:
		Mutex	m_fe_mutex;
};

template <typename T>
class FE_DL_EXPORT trueClassNotSafeShared : public ClassSafeSharedBase
{
	public:
				trueClassNotSafeShared(void)								{}
virtual			~trueClassNotSafeShared(void)								{}

virtual	Mutex* fe_class_thread_mutex(void) const			{ return NULL; }

virtual	void	safeLock(void) const										{}
virtual	void	safeUnlock(void) const										{}
virtual	bool	safeTryLock(void) const						{ return false; }

virtual	void	safeLockShared(void) const									{}
virtual	void	safeUnlockShared(void) const								{}
virtual	bool	safeTryLockShared(void) const				{ return false; }

};

template <typename T>
class FE_DL_EXPORT trueNotSafeShared : public SafeSharedBase
{
	public:
				trueNotSafeShared(void)										{}
virtual			~trueNotSafeShared(void)									{}

virtual	Mutex* fe_thread_mutex(void) const					{ return NULL; }

virtual	void	safeLock(void) const										{}
virtual	void	safeUnlock(void) const										{}
virtual	bool	safeTryLock(void) const						{ return false; }

virtual	void	safeLockShared(void) const									{}
virtual	void	safeUnlockShared(void) const								{}
virtual	bool	safeTryLockShared(void) const				{ return false; }

};

#if FE_SAFE_COUNTED_MUTEX
template<typename T>
FE_DL_PUBLIC Mutex* trueClassSafeShared<T>::ms_pMutex=NULL;
template<typename T>
FE_DL_PUBLIC I32 trueClassSafeShared<T>::ms_mutexCount=0;
#else
template<typename T>
FE_DL_PUBLIC Mutex trueClassSafeShared<T>::ms_mutex;
#endif

#ifndef FE_DISABLE_THREAD_SAFE
/**	Class level locking for thread safety
	*/
template<typename T>
class FE_DL_EXPORT ClassSafeShared:
	public trueClassSafeShared<T>		{};
///	Object level locking for thread safety
template<typename T>
class FE_DL_EXPORT ObjectSafeShared:
	public trueObjectSafeShared<T>		{};
///	No locking for thread safety
template<typename T>
class FE_DL_EXPORT NotSafeShared:
	public trueNotSafeShared<T>			{};
#else
///	Class level locking for thread safety
template<typename T>
class FE_DL_EXPORT ClassSafeShared:
	public trueNotClassSafeShared<T>	{};
///	Object level locking for thread safety
template<typename T>
class FE_DL_EXPORT ObjectSafeShared:
	public trueNotSafeShared<T>			{};
///	No locking for thread safety
template<typename T>
class FE_DL_EXPORT NotSafeShared:
	public trueNotSafeShared<T>			{};
#endif

} /* namespace */

#endif /* __core_SafeShared_h__ */
