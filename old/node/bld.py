import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ 
                "DrawNodes",
                "nodeDL" ]

    dll = module.DLL( "fexNodeDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexSpatialDLLib",
                "fexMiscDLLib",
                "fexWindowDLLib"
                ]

    forge.deps( ["fexNodeDLLib"], deplibs )

