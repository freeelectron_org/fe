/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ProxHash.h"

#if FE_PROXHASH_THREADED
#include "tbb/blocked_range.h"
#endif

namespace fe
{
namespace ext
{

ProxHash::ProxHash(void)
{
	m_poolCount = 0;
	m_hits.resize(1);
}

ProxHash::~ProxHash(void)
{
}

void ProxHash::initialize(void)
{
	cfg<int>("tableSize") = 20000;
	cfg<Real>("cellSize") = 2.0;
}

void ProxHash::setup(sp<Scope> spScope)
{
	m_spScope = spScope;
	m_asParticle.bind(m_spScope);
	m_asBounded.bind(m_spScope);
	m_asProximity.bind(m_spScope);

	m_filters.clear();
	m_filters.push_back(m_asBounded.radius);
	m_filters.push_back(m_asParticle.location);
}

#if FE_PROXHASH_THREADED
class ApplyCheck
{
	public:
		ApplyCheck(sp<ProxHash> a_prox)
		{
			m_prox = a_prox;
		}
		void operator()( const tbb::blocked_range<size_t>& r ) const
		{
			Array<unsigned int> to_check;
			std::vector<bool> mark(m_prox->sz());
			for(unsigned int i = 0; i < mark.size(); i++)
			{
				mark[i] = false;
			}
			Array< std::pair<unsigned int, unsigned int> > m_hits;

			//FE_UWORD poolIndex = m_prox->getNextPoolIndex();

			for(size_t i = r.begin(); i!=r.end(); ++i )
			{
				m_hits.clear();

				m_prox->checkSingle((unsigned int)i, to_check, mark, m_hits);
				//m_prox->transferHitsToPool(m_hits, poolIndex);
				m_prox->accumulateHits(m_hits);
			}
		}

		sp<ProxHash> m_prox;
};
#endif

void ProxHash::checkSingle(unsigned int i, Array<unsigned int> &to_check, std::vector<bool> &mark, Array< std::pair<unsigned int, unsigned int> > &a_hits)
{
	unsigned int collisions = 0;
	unsigned int i_self = i;
	to_check.clear();
	LocalRecord &lr = m_store[i];
	SpatialVector lo;
	SpatialVector hi;
	int ilo[3];
	int ihi[3];
	for(unsigned int d = 0; d < 3; d++)
	{
		lo[d] = lr.m_location[d] - lr.m_radius;
		hi[d] = lr.m_location[d] + lr.m_radius;
		ilo[d] = (int)(lo[d]/m_radius);
		ihi[d] = (int)(hi[d]/m_radius);
	}

	for(int i_x = ilo[0]; i_x <= ihi[0]; i_x++)
	{
		for(int i_y = ilo[1]; i_y <= ihi[1]; i_y++)
		{
			for(int i_z = ilo[2]; i_z <= ihi[2]; i_z++)
			{
				unsigned int h = hash(i_x, i_y, i_z);
				Array<unsigned int> &cells = m_cells[h];
				unsigned int k;
#if 1
				for(k = 0; k < cells.size(); k++)
				{
					if(i_self < cells[k]) { break; }
				}
#endif
				for(unsigned int j = k; j < cells.size(); j++)
				{
					unsigned int i_store = cells[j];
					//if(i_self > i_store)
					{
						//if(!m_store[i_store].m_mark)
						if(!mark[i_store])
						{
							mark[i_store] = true;
							//m_store[i_store].m_mark = true;
							to_check.push_back(i_store);
						}
					}
				}
			}
		}
	}
	for(unsigned int i_c = 0; i_c < to_check.size(); i_c++)
	{
		if(check(i_self, to_check[i_c], a_hits))
		//if(check(lr, m_store[to_check[i_c]], tid))
		{
			collisions++;
		}
		mark[to_check[i_c]] = false;
		//m_store[to_check[i_c]].m_mark = false;
	}
}

unsigned int ProxHash::detect(sp<Layout> l_pair, sp<RecordGroup> rg_in, sp<RecordGroup> rg_out)
{
	SystemTicker ticker("ProxHash");

	m_hpPairLayout = l_pair;

	unsigned int collisions = 0;

	m_poolCount = 0;
	for(unsigned int i = 0; i < m_poolIndex.size(); i++)
	{
		m_poolIndex[i] = 0;
	}

	getNextPoolIndex();

#if 0
	m_poolIndex = 0;
	if(!m_spPool.isValid())
	{
		//m_spPool = new RecordArray(l_pair);
		m_spPool = l_pair->scope()->createRecordArray(l_pair, 10000);
	}
#endif


	setup(l_pair->scope());

	t_lrecords lrecords;

	m_store.clear();

	m_tableSize = (unsigned int)cfg<int>("tableSize");
	m_radius = cfg<Real>("cellSize");

	m_cells.resize(m_tableSize);

	//Array< std::pair<LocalRecord *, LocalRecord *> > checklist;
	//checklist.reserve(10000);

	// insertion
	for(RecordGroup::iterator i_in = rg_in->begin(m_filters);
		i_in != rg_in->end(); i_in++)
	{
		sp<RecordArray> ra_in = *i_in;
		for(int i = 0; i < ra_in->length(); i++)
		{
			//to_check.clear();
			unsigned int i_self = m_store.size();
			m_store.resize(i_self + 1);
			LocalRecord &lr = m_store.back();
			lr.m_pRA = ra_in.raw();
			lr.m_index = i;
			lr.m_location = m_asParticle.location(ra_in,i);
			lr.m_radius = m_asBounded.radius(ra_in,i);
			lr.m_mark = true;
			SpatialVector lo;
			SpatialVector hi;
			int ilo[3];
			int ihi[3];
			for(unsigned int d = 0; d < 3; d++)
			{
				lo[d] = lr.m_location[d] - lr.m_radius;
				hi[d] = lr.m_location[d] + lr.m_radius;
				ilo[d] = (int)(lo[d]/m_radius);
				ihi[d] = (int)(hi[d]/m_radius);
			}

			for(int i_x = ilo[0]; i_x <= ihi[0]; i_x++)
			{
				for(int i_y = ilo[1]; i_y <= ihi[1]; i_y++)
				{
					for(int i_z = ilo[2]; i_z <= ihi[2]; i_z++)
					{
						unsigned int h = hash(i_x, i_y, i_z);
#if 0
						for(unsigned int j = 0; j < m_cells[h].size(); j++)
						{
							unsigned int i_store = m_cells[h][j];
							if(!m_store[i_store].m_mark)
							{
								m_store[i_store].m_mark = true;
								to_check.push_back((m_cells[h][j]));
							}
						}
#endif
						m_cells[h].push_back(i_self);
					}
				}
			}
#if 0
			lr.m_mark = false;
			for(unsigned int i_c = 0; i_c < to_check.size(); i_c++)
			{
#if 0
				m_checklist.push_back(std::pair<unsigned int, unsigned int>(
					i_self,
					to_check[i_c])
					);
#endif
#if 0
				if(check(lr, m_store[to_check[i_c]]))
				{
					collisions++;
				}
				m_store[to_check[i_c]].m_mark = false;
#endif
			}
#endif
		}
	}
	ticker.log("insertion");
#if FE_PROXHASH_THREADED
	//int max_threads = omp_get_max_threads();
#else
	int max_threads = 1;
	m_hits.resize(max_threads);
	for(unsigned int c = 0; c < m_hits.size(); c++)
	{
		m_hits[c].clear();
	}
#endif

#if 1
	for(unsigned int c = 0; c < m_cells.size(); c++)
	{
		std::sort(m_cells[c].begin(), m_cells[c].end());
	}

	ticker.log("sort");
#endif

#if 1
#if !FE_PROXHASH_THREADED
	//Array< std::vector<bool> > mark(m_poolCount);
	Array< std::vector<bool> > mark(max_threads);
	for(unsigned int i = 0; i < mark.size(); i++)
	{
		mark[i].resize(m_store.size());
		for(unsigned int j = 0; j < m_store.size(); j++)
		{
			mark[i][j] = false;
		}
	}
	Array< Array<unsigned int> > to_check(1);
#endif

	//Array< Array<unsigned int> > to_check(max_threads);

	m_chits.clear();
#if FE_PROXHASH_THREADED
#if 1
	tbb::parallel_for(tbb::blocked_range<size_t>(0,m_store.size(),100), ApplyCheck(sp<ProxHash>(this)));
#endif

#if 0
#pragma omp parallel
	{
		Array< std::pair<unsigned int, unsigned int> > t_hits;
		Array<unsigned int> t_to_check;
		std::vector<bool> t_mark(m_store.size());
		for(unsigned int j = 0; j < m_store.size(); j++)
		{
			t_mark[j] = false;
		}
#pragma omp for schedule(dynamic, 100)
		for(int i = 0; i < m_store.size(); i++)
		{
			//int id = omp_get_thread_num();
			//m_hits[id].clear();
			t_hits.clear();
			checkSingle(i, t_to_check, t_mark, t_hits);
			accumulateHits(t_hits);
		}
	}
	ticker.log("check0");

#if 0
	int chs = 0;
	for(int i = 0; i < max_threads; i++)
	{
		chs += m_hits[i].size();
	}
	m_chits.resize(chs);
	int k = 0;
	for(int i = 0; i < max_threads; i++)
	{
		for(int j = 0; j < m_hits[i].size(); j++)
		{
			m_chits[k] = m_hits[i][j];
			k++;
		}
	}
#endif

#endif

#else
	for(unsigned int i = 0; i < m_store.size(); i++)
	{
		m_hits[0].clear();
		checkSingle(i, to_check[0], mark[0], m_hits[0]);
		accumulateHits(m_hits[0]);
		//transferHitsToPool(m_hits[0]);
	}
#endif

#if 0
#if FE_PROXHASH_THREADED
	#pragma omp parallel for
#endif
	for(unsigned int i = 0; i < m_store.size(); i++)
	{
#if FE_PROXHASH_THREADED
		int id = omp_get_thread_num();
#else
		int id = 0;
#endif

		m_hits[id].clear();
		checkSingle(i, to_check[id], mark[id], m_hits[id]);
		accumulateHits(m_hits[id]);

#if 0
		unsigned int i_self = i;
		to_check[id].clear();
		LocalRecord &lr = m_store[i];
		SpatialVector lo;
		SpatialVector hi;
		int ilo[3];
		int ihi[3];
		for(unsigned int d = 0; d < 3; d++)
		{
			lo[d] = lr.m_location[d] - lr.m_radius;
			hi[d] = lr.m_location[d] + lr.m_radius;
			ilo[d] = (int)(lo[d]/m_radius);
			ihi[d] = (int)(hi[d]/m_radius);
		}

		for(int i_x = ilo[0]; i_x <= ihi[0]; i_x++)
		{
			for(int i_y = ilo[1]; i_y <= ihi[1]; i_y++)
			{
				for(int i_z = ilo[2]; i_z <= ihi[2]; i_z++)
				{
					unsigned int h = hash(i_x, i_y, i_z);
					Array<unsigned int> &cells = m_cells[h];
					unsigned int k;
#if 1
					for(k = 0; k < cells.size(); k++)
					{
						if(i_self < cells[k]) { break; }
					}
#endif
					for(unsigned int j = k; j < cells.size(); j++)
					{
						unsigned int i_store = cells[j];
						//if(i_self > i_store)
						{
							//if(!m_store[i_store].m_mark)
							if(!mark[id][i_store])
							{
								mark[id][i_store] = true;
								//m_store[i_store].m_mark = true;
								to_check[id].push_back(i_store);
							}
						}
					}
				}
			}
		}
		for(unsigned int i_c = 0; i_c < to_check[id].size(); i_c++)
		{
			if(check(lr, m_store[to_check[id][i_c]], id))
			{
				collisions++;
			}
			mark[id][to_check[id][i_c]] = false;
			//m_store[to_check[i_c]].m_mark = false;
		}
#endif
	}
#endif

	ticker.log("check");
#endif

#if 0
	for(unsigned int i = 0; i < m_checklist.size(); i++)
	{
		if(check(m_store[m_checklist[i].first], m_store[m_checklist[i].second]))
		{
			collisions++;
		}
	}
	ticker.log("checklist");
#endif

#if 0
	// check assembly
	for(unsigned int c = 0; c < m_cells.size(); c++)
	{
		Array<unsigned int> &cell = m_cells[c];

		unsigned int n = cell.size();

		for(unsigned int i = 0; i < n; i++)
		{
			unsigned int a = cell[i];
			for(unsigned int j = i+1; j < n; j++)
			{
				unsigned int b = cell[j];
				if(a<b)
				{
					m_store[a].m_candidates.insert((void *)&(m_store[b]));
				}
				else
				{
					m_store[b].m_candidates.insert((void *)&(m_store[a]));
				}
			}
		}
	}
	ticker.log("assembly");
#endif

	transferHitsToPool();
	ticker.log("transfer");

	if(rg_out.isValid())
	{
		sp<RecordArray> spPA = rg_out->getArray(l_pair);
		spPA->enableDuplicates(true);

		for(unsigned int i = 0; i < m_poolCount; i++)
		{
			spPA->add(m_spPool[i], 0, m_poolIndex[i]);
		}
	}

	ticker.log("finish");

	m_cells.clear();
	m_store.clear();
	m_checklist.clear();
	ticker.log("clear");
	return collisions;
}


unsigned int ProxHash::hash(int a_x, int a_y, int a_z)
{
	return ((a_x*73856093)^(a_y*19349663)^(a_z*83492791)) % m_tableSize;
}

#if 0
bool ProxHash::check(LocalRecord &r_a, LocalRecord &r_b)
{
	SpatialVector dist = r_a.m_location - r_b.m_location;
	Real dist_sq = dist[0]*dist[0] + dist[1]*dist[1] + dist[2]*dist[2];
	Real rad_sq = r_a.m_radius + r_b.m_radius;

	rad_sq *= rad_sq;
	if(dist_sq < rad_sq)
	{

		FEASSERT((int)m_poolIndex <= m_spPool->length());
		if((int)m_poolIndex == m_spPool->length())
		{
			m_spPool->add(m_spScope->createRecord(m_hpPairLayout));
		}

		r_a.m_pRA->set(m_asProximity.left(m_spPool, m_poolIndex), r_a.m_index);
		r_b.m_pRA->set(m_asProximity.right(m_spPool, m_poolIndex), r_b.m_index);

		m_poolIndex++;
		return true;
	}
	return false;
}
#endif

bool ProxHash::check(unsigned int a_a, unsigned int a_b, Array< std::pair<unsigned int, unsigned int> > &a_hits)
{
	LocalRecord &r_a = m_store[a_a];
	LocalRecord &r_b = m_store[a_b];

	SpatialVector dist = r_a.m_location - r_b.m_location;
	Real dist_sq = dist[0]*dist[0] + dist[1]*dist[1] + dist[2]*dist[2];
	Real rad_sq = r_a.m_radius + r_b.m_radius;

	rad_sq *= rad_sq;
	if(dist_sq < rad_sq)
	{
		a_hits.push_back(std::pair<unsigned int, unsigned int>(a_a, a_b));

		return true;
	}
	return false;
}

void ProxHash::transferHitsToPool()
{
	unsigned int i_pool = 0;
	sp<RecordArray> spPool = m_spPool[i_pool];
	FE_UWORD &poolIndex = m_poolIndex[i_pool];
	for(unsigned int j = 0; j < m_chits.size(); j++)
	{
		const std::pair<unsigned int, unsigned int> &chit = m_chits[j];
		LocalRecord &r_a = m_store[chit.first];
		LocalRecord &r_b = m_store[chit.second];

		if((int)poolIndex == spPool->length())
		{
			spPool->add(m_spScope->createRecord(m_hpPairLayout));
		}

		r_a.m_pRA->set(m_asProximity.left(spPool, poolIndex), r_a.m_index);
		r_b.m_pRA->set(m_asProximity.right(spPool, poolIndex), r_b.m_index);

		poolIndex++;
	}
}

FE_UWORD ProxHash::getNextPoolIndex(void)
{
	unsigned int i_pool = 0;
	{
#if FE_PROXHASH_THREADED
		tbb::spin_mutex::scoped_lock lock(m_mutex);
#endif

		i_pool = m_poolCount;
		m_poolCount++;

		if(i_pool == m_spPool.size())
		{
			m_spPool.resize(m_poolCount);
			m_poolIndex.resize(m_poolCount);
			m_spPool[i_pool] = m_hpPairLayout->scope()->createRecordArray(
					m_hpPairLayout, 10000);
			m_poolIndex[i_pool] = 0;
		}

	}

	return i_pool;
}

void ProxHash::transferHitsToPool(Array< std::pair<unsigned int,
	unsigned int> > &a_hits, FE_UWORD i_pool)
{
	for(unsigned int j = 0; j < a_hits.size(); j++)
	{
		LocalRecord &r_a = m_store[a_hits[j].first];
		LocalRecord &r_b = m_store[a_hits[j].second];

		if((int)m_poolIndex[i_pool] == m_spPool[i_pool]->length())
		{
			m_spPool[i_pool]->add(m_spScope->createRecord(m_hpPairLayout));
		}

		{
		//tbb::spin_mutex::scoped_lock lock(m_p_mutex[i_pool]);
		r_a.m_pRA->set(m_asProximity.left(m_spPool[i_pool],
				m_poolIndex[i_pool]), r_a.m_index);
		r_b.m_pRA->set(m_asProximity.right(m_spPool[i_pool],
				m_poolIndex[i_pool]), r_b.m_index);
		}

		m_poolIndex[i_pool]++;
	}
}

void ProxHash::accumulateHits(Array< std::pair<unsigned int,
	unsigned int> > &a_hits)
{
#if FE_PROXHASH_THREADED
	tbb::concurrent_vector< std::pair<unsigned int,
			unsigned int> >::iterator i_c =
			m_chits.grow_by((size_t)a_hits.size());

	// This is for old TBB, if you think you need this, you really need
	// to change to newer TBB.
	//size_t old_size = m_chits.grow_by((size_t)a_hits.size());
	//tbb::concurrent_vector< std::pair<unsigned int,
	//		unsigned int> >::iterator i_c = m_chits.begin();
	//i_c += old_size;

	for(unsigned int i = 0; i < a_hits.size(); i++)
	{
		*i_c = a_hits[i];
		i_c++;
	}
#else
	unsigned int base = m_chits.size();
	m_chits.resize(a_hits.size() + m_chits.size());
	for(unsigned int i = 0; i < a_hits.size(); i++)
	{
		m_chits[base+i] = a_hits[i];
	}
#endif
}

} /* namespace ext */
} /* namespace fe */

