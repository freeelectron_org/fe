/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "signal/signal.h"

namespace fe
{
namespace ext
{

Reactor::Reactor(void)
{
}

Reactor::~Reactor(void)
{
}

void Reactor::initialize(void)
{
}

void Reactor::add(const String &a_name, sp<SignalerI> a_signaler, Record r_sig)
{
	t_relay relay;
	relay.m_spSignaler = a_signaler;
	relay.m_signal = r_sig;
	m_relays[a_name].push_back(relay);
}

void Reactor::react(const String &a_name)
{
	Array<t_relay> &relays = m_relays[a_name];
	for(unsigned int i = 0; i < relays.size(); i++)
	{
		relays[i].m_spSignaler->signal(relays[i].m_signal);
	}
}


} /* namespace ext */
} /* namespace fe */

