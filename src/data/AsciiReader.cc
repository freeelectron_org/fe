/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#define FE_AR_HANDLE_DEBUG	FALSE
#define FE_AR_SOFTNESS 2

#include <sstream>

namespace fe
{
namespace data
{

AsciiReader::AsciiReader(sp<Scope> spScope)
{
	m_spScope = spScope;
	m_spRecordGroupType =
		m_spScope->typeMaster()->lookupType<sp<RecordGroup> >();
	m_spRecordArrayType =
		m_spScope->typeMaster()->lookupType<sp<RecordArray> >();
	m_spRecordType =
		m_spScope->typeMaster()->lookupType<Record>();
	m_spWeakRecordType =
		m_spScope->typeMaster()->lookupType<WeakRecord>();
	reset();

	m_next_subst_id = 0;
	m_subst_id.sPrintf("internal_subst_id:%u", m_next_subst_id);

#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"AsciiReader::AsciiReader");
#endif
}

AsciiReader::~AsciiReader(void)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
}

void AsciiReader::reset(void)
{
	m_rgs.clear();
	m_ras.clear();
	m_rs.clear();
	m_wiringList.clear();
	m_wkWiringList.clear();
	Record unset_record;
	m_rs["0"] = unset_record; // index 0 for unset record
	m_rgs["0"] = NULL; // index 0 for unset recordgroup
	m_ras["0"] = NULL; // index 0 for unset recordarray
	m_attrInfos.clear();
}

bool AsciiReader::readToken(std::istream &a_istrm, Token &a_token)
{
	bool in_word = false;
	bool in_quote = false;
	bool in_paren = false;
	bool in_comment = false;
	bool in_backslash = false;
	char c;
	std::string str;
	std::string args;
	while(a_istrm.get(c))
	{
		// strip windows CR
		if(c == '\r')
		{
			continue;
		}

		if(!in_word)
		{
			if(in_comment)
			{
				if(c == '\n')
				{
					in_comment = false;
				}
			}
			else
			{
				// leading ws
				if(c == ' ' || c == '\t' || c == '\n')
				{
					in_backslash = false;
					continue;
				}
				// start quoted token
				else if(c == '"')
				{
					in_word = true;
					in_quote = true;
				}
				else if(c == '#')
				{
					in_comment = true;
				}
				else if(c == FE_KW_STARTBINARY)
				{
					I32 n;
					a_istrm.read((char *)&n, sizeof(I32));
					n = ntohl(n);
					a_token.create(n);
					a_istrm.read((char *)a_token.m_pBlock, n);
					a_istrm.get(c); // FE_KW_ENDBINARY
					return true;
				}
				else
				{
					in_word = true;
					str.push_back(c);
				}
			}
		}
		else
		{
			if(in_quote)
			{
				if(c == '"' && !in_backslash)
				{
					break;
				}
				else
				{
					str.push_back(c);
				}
			}
			else if(in_paren)
			{
				if(c == ')' && !in_backslash)
				{
					in_paren = false;
				}
				else
				{

					args.push_back(c);
				}
			}
			else if(c == '(')
			{
				in_paren = true;
			}
			else if(c == ' ' || c == '\t' || c == '\n')
			{
				break;
			}
			else
			{
				str.push_back(c);
			}
		}

		in_backslash = (c == '\\');
	}

	a_token.m_string = str.c_str();
	a_token.m_args = args.c_str();

	return in_word;
}


void AsciiReader::dispatchBlock(BlockType a_type, const Token& token,
		Array<Token> &a_tokens)
{
	if(a_type == e_none)
	{
		return;
	}

	switch(a_type)
	{
		case e_record:
			handleRecord(a_tokens);
			break;
		case e_group:
		case e_defaultgroup:
			handleGroup(a_tokens,token);
			break;
		case e_template:
			handleTemplate(a_tokens);
			break;
		case e_attribute:
			handleAttribute(a_tokens);
			break;
		case e_layout:
			handleLayout(a_tokens);
			break;
		case e_info:
			handleInfo(a_tokens);
			break;
		case e_end:
			handleEnd(a_tokens);
			break;
		case e_comment:
			handleComment(a_tokens);
			break;
		default:
			feX("impossibility");
			break;
	}


	a_tokens.clear();
}

void AsciiReader::handleComment(Array<Token> &a_tokens)
{
	if(a_tokens.size() != 1)
	{
		feX(e_invalidFile,
			"AsciiReader::handleComment",
			"%s: requires one argument", FE_KW_COMMENT);
	}

	// ignore the token
}

void AsciiReader::handleInfo(Array<Token> &a_tokens)
{
	if(a_tokens.size() < 1)
	{
		feX(e_invalidFile,
			"AsciiReader::handleInfo",
			"%s: requires at least one argument", FE_KW_INFO);
	}

	U32 version = atoi(a_tokens[0].c_str());

	if(version != FE_SERIAL_VERSION)
	{
		feX(e_invalidFile,
			"AsciiReader::readInfo",
			"version mismatch");
	}
}

void AsciiReader::handleEnd(Array<Token> &a_tokens)
{
}

void AsciiReader::handleGroup(Array<Token> &a_tokens,const Token& token)
{
	if(a_tokens.size() < 1)
	{
		feX(e_invalidFile,
			"AsciiReader::handleGroup",
			"%s: requires at least one argument", FE_KW_RECORDGROUP);
	}

	String &rgid = a_tokens[0];
	if(rgid=="")
	{
		m_spDefaultRG=NULL;
		m_spDefaultRGs.clear();
		return;
	}

	sp<RecordGroup> spRG = m_rgs[rgid];
	if(!spRG.isValid())
	{
		spRG = new RecordGroup();
		m_rgs[rgid] = spRG;
	}
	if(token.m_string == FE_KW_DEFAULTGROUP)
	{
		m_spDefaultRGs.clear();
		m_spDefaultRGs.push_back(spRG);
		m_spDefaultRG=spRG;
		for(unsigned int i = 1; i < a_tokens.size(); i++)
		{
			sp<RecordGroup> spRGanother = m_rgs[a_tokens[i]];
			if(!spRGanother.isValid())
			{
				spRGanother = new RecordGroup();
				m_rgs[a_tokens[i]] = spRGanother;
			}
			m_spDefaultRGs.push_back(spRGanother);
		}



		return;
	}
	if(token.args() == "weak")
	{
		spRG->setWeak(TRUE);
	}

	for(unsigned int i = 1; i < a_tokens.size(); i++)
	{
		String &rid = a_tokens[i];
		t_str_r::iterator i_r = m_rs.find(rid);
		if(i_r == m_rs.end())
		{
			fail("unspecified record '"+rid+"'");
		}
		spRG->add(i_r->second);
	}
}

sp<Scope> AsciiReader::findScope(const String &a_name)
{
	if(!m_spScope->registry().isValid())
	{
		feX("AsciiReader::findScope",
			"cannot override Scope when default Scope is not component");
	}
	if(!m_spScope->registry()->master()->catalog()->cataloged(a_name))
	{
		m_spScope->registry()->create("Scope");
		m_spScope->setName(a_name);
	}
	sp<Scope> spScope =
			m_spScope->registry()->master()->catalog()->catalogComponent(
			"Scope", a_name);

	return spScope;
}

void AsciiReader::handleAttribute(Array<Token> &a_tokens)
{
	AttributeInfo info;
	if(a_tokens.size() < 2)
	{
		feX(e_invalidFile,
			"AsciiReader::handleAttribute",
			"%s: requires at least two arguments", FE_KW_ATTRIBUTE);
	}

	info.m_name = a_tokens[0];

	for(unsigned int i = 1; i < a_tokens.size(); i++)
	{
		info.m_typenames.push_back(a_tokens[i]);
	}

	sp<BaseType> spBT;
	String tstr;
	String matched_typename;
	for(std::list<String>::iterator it = info.m_typenames.begin();
		it != info.m_typenames.end(); it++)
	{
		// lookup type for attribute
		spBT = m_spScope->typeMaster()->lookupName(*it);
		tstr.cat(*it);
		tstr.cat(" ");
		if(spBT.isValid())
		{
			matched_typename = *it;
			break;
		}
	}
	sp<Scope> spScope = m_spScope;
	if(spBT.isValid())
	{
		// check for scope override
		if(spBT->getInfo().isValid())
		{
			for(std::list<String>::iterator it = info.m_typenames.begin();
				it != info.m_typenames.end(); it++)
			{
				std::string s = (*it).c_str();
				if(s.size() > 1 && s.substr(0,1) == "@")
				{
					s = s.substr(1);
					spScope = findScope(s.c_str());
				}
			}
		}

		sp<Attribute> spAttribute;
		// look for attribute
		spAttribute = spScope->findAttribute(info.m_name);
		if(spAttribute.isValid())
		{
			// type check existing attribute
			if(spAttribute->type() != spBT)
			{
				feX(e_typeMismatch,
					"AsciiReader::handleAttribute",
					"type mismatch for \'%s\'",
					info.m_name.c_str());
			}
		}
		else
		{
			// support new attribute
			spAttribute = spScope->support(info.m_name, matched_typename);
		}

		if(spBT->getInfo().isValid())
		{
			for(std::list<String>::iterator it = info.m_typenames.begin();
				it != info.m_typenames.end(); it++)
			{
				std::string s = (*it).c_str();
				// lookup defaults
				if(s.size() > 1 && s.substr(0,1) == "=")
				{
					s = s.substr(1);
					std::istringstream istrm(s);
					spAttribute->defaultInstance().create(spBT);
					spBT->getInfo()->input(istrm,
						spAttribute->defaultInstance().data(),
						BaseType::Info::e_ascii);
				}
			}
		}

		// check for local help
		if(spAttribute->type()->getInfo().isValid())
		{
			info.m_spAttribute = spAttribute;
		}
		else
		{
			feX(e_typeMismatch,
				"AsciiReader::handleAttribute",
				"type \'%s\' has no serialization helper",
				info.m_name.c_str());
		}
	}
	else
	{
		feX(e_cannotFind,
			"AsciiReader::readAttribute",
			"cannot find type %s", tstr.c_str());
	}

	m_attrInfos[info.m_name] = info;
}

void AsciiReader::handleLayout(Array<Token> &a_tokens)
{
	if(a_tokens.size() < 1)
	{
		feX(e_invalidFile,
			"AsciiReader::handleLayout",
			"%s: requires at least one argument", FE_KW_LAYOUT);
	}

	String layout_name;
	layout_name = a_tokens[0];
	LayoutInfo &layout_info = m_layoutInfos[layout_name];

	sp<Scope> spScope = m_spScope;
	for(unsigned int i = 1; i < a_tokens.size(); i++)
	{
		std::string s = a_tokens[i].c_str();
		if(s.size() > 1 && s.substr(0,1) == "@")
		{
			s = s.substr(1);
			spScope = findScope(s.c_str());
		}
		else
		{
			t_attrinfo::iterator i_info
				= m_attrInfos.find(a_tokens[i]);
			if(i_info == m_attrInfos.end())
			{
				feX(e_invalidFile,
					"AsciiReader::handleLayout",
					"unknown attribute %s", a_tokens[i].c_str());
			}
			layout_info.m_attributeInfos[a_tokens[i]] = i_info->second;
		}
	}

	sp<Layout> spL = spScope->lookupLayout(layout_name);

	if(!spL.isValid())
	{
		spL = spScope->declare(layout_name);
	}

	// if layout is not locked, populate
	//if(!spL->locked())
	{
		for(t_attrinfo::iterator i_info = layout_info.m_attributeInfos.begin();
			i_info != layout_info.m_attributeInfos.end(); i_info++)
		{
			// populate
			spL->populate(i_info->second.m_spAttribute);
		}
	}

	// set layout info for actual layout
	spL->initialize();
	//if(!spL->locked()){ spL->lock(); }

	for(t_attrinfo::iterator i_info = layout_info.m_attributeInfos.begin();
			i_info != layout_info.m_attributeInfos.end(); i_info++)
	{
		AttributeInfo &tinfo = i_info->second;
		sp<Attribute> spAttribute = tinfo.m_spAttribute;
		// look for attribute
		spAttribute = spScope->findAttribute(tinfo.m_name, tinfo.m_index);
		if(spAttribute.isValid())
		{
			// check if layout does not have has attribute
			if(!spL->checkAttribute(tinfo.m_index))
			{
				tinfo.m_spAttribute = NULL;
			}
		}
		else
		{
			feX(e_typeMismatch,
				"AsciiReader::handleLayout",
				"ascii format does not handle reading unknown attributes");
		}
	}
	layout_info.m_spLayout = spL;
}

void AsciiReader::handleTemplate(Array<Token> &a_tokens)
{
	if(a_tokens.size() < 2)
	{
		feX(e_invalidFile,
			"AsciiReader::handleTemplate",
			"%s: requires at least two arguments", FE_KW_TEMPLATE);
	}

	String &name = a_tokens[0];

#if FE_AR_HANDLE_DEBUG
	feLog("AsciiReader::handleTemplate \"%s\"\n",name.c_str());
#endif

	String &parents = a_tokens[1];
	Token parent;

	std::string s(parents.c_str());
	std::istringstream istrm(s);
	while(readToken(istrm,parent))
	{
		m_spScope->cookbook()->derive(name,parent);

#if FE_AR_HANDLE_DEBUG
		feLog("  inherit \"%s\"\n",parent.c_str());
#endif
	}

	for(unsigned int i = 2; i < a_tokens.size()-1; i+=2)
	{
		String &attr_name = a_tokens[i];
		String &value = a_tokens[i+1];

		m_spScope->cookbook()->add(name,attr_name,value);

#if FE_AR_HANDLE_DEBUG
		feLog("  attribute \"%s\" \"%s\"\n",attr_name.c_str(),value.c_str());
#endif
	}
}

void AsciiReader::handleRecord(Array<Token> &a_tokens)
{
	if(a_tokens.size() < 2)
	{
		feX(e_invalidFile,
			"AsciiReader::handleRecord",
			"%s: requires at least one argument", FE_KW_RECORD);
	}

	String *record_id;
	String *layout_name;
	if(a_tokens[0] == "*")
	{
		m_next_subst_id++;
		m_subst_id.sPrintf("internal_subst_id:%u", m_next_subst_id);
		record_id = &(m_subst_id);
		layout_name = &(a_tokens[1].str());
	}
	else
	{
		record_id = &(a_tokens[0].str());
		layout_name = &(a_tokens[1].str());
	}

	// RecordViews apparently don't need Layouts
	sp<Scope> spScope = m_spScope;
	t_id_layoutinfo::iterator i_linfo = m_layoutInfos.find(*layout_name);
	if(i_linfo != m_layoutInfos.end())
	{
		spScope = m_layoutInfos[*layout_name].m_spLayout->scope();
	}

#if FE_AR_HANDLE_DEBUG
	feLog("AsciiReader::handleRecord record \"%s\" layout \"%s\"\n",
			record_id->c_str(),layout_name->c_str());
#endif

	Record record=spScope->produceRecord(*layout_name);
	FEASSERT(record.isValid());

#if 0
	if(m_spDefaultRG.isValid())
	{
		m_spDefaultRG->add(record);
fe_fprintf(stderr, "A\n");
		for(unsigned int i = 0; i < m_spDefaultRGs.size(); i++)
		{
fe_fprintf(stderr, "B\n");
	}
	}
#else
	//for(sp<RecordGroup> spRG : m_spDefaultRG)
	for(unsigned int i = 0; i < m_spDefaultRGs.size(); i++)
	{
		sp<RecordGroup> spRG = m_spDefaultRGs[i];
		if(spRG.isValid())
		{
			spRG->add(record);
		}
	}
#endif

	m_rs[*record_id] = record;

	for(unsigned int i = 2; i < a_tokens.size()-1; i+=2)
	{
		String &attr_name = a_tokens[i];
#if FE_AR_HANDLE_DEBUG
		feLog("AsciiReader::handleRecord attr \"%s\"\n",attr_name.c_str());
#endif

		FE_UWORD index=0;
		sp<Attribute>& rspAttribute=spScope->findAttribute(attr_name,index);
		if(!rspAttribute.isValid())
		{
			feX("AsciiReader::handleRecord",
					"attribute \"%s\" not found in scope for \"%s\" in record \"%s\"\n",
					attr_name.c_str(),layout_name->c_str(),record_id->c_str());
		}


		if(!record.layout()->checkAttribute(index))
		{

#if FE_AR_SOFTNESS == 2
			feLog("adding attribute \"%s\" to \"%s\" due to record \"%s\" (layout did not have attribute)\n",
					attr_name.c_str(),layout_name->c_str(),record_id->c_str());
			record.layout()->populate(rspAttribute);
#elif FE_AR_SOFTNESS == 1
			feLog("skipping attribute \"%s\" for \"%s\" in record \"%s\" (layout does not have attribute)\n",
					attr_name.c_str(),layout_name->c_str(),record_id->c_str());
			continue;
#else
			feX("AsciiReader::handleRecord",
					"offsetTable[%d] for \"%s\" in \"%s\" invalid\n",
					index,attr_name.c_str(),layout_name->c_str());
#endif
		}

		void *instance = record.rawAttribute(index);
		sp<BaseType> spBT = rspAttribute->type();

		if(spBT == m_spRecordGroupType)
		{
			String &rgid = a_tokens[i+1];
			if(rgid != "0")
			{
				sp<RecordGroup> *pspRG;
				pspRG = reinterpret_cast<sp<RecordGroup> *>(instance);
				sp<RecordGroup> spRG = m_rgs[rgid];
				if(!spRG.isValid())
				{
					spRG = new RecordGroup();
#if FE_COUNTED_TRACK
					spRG->setName(attr_name);
#endif
					m_rgs[rgid] = spRG;
				}
				*pspRG = spRG;
			}
		}
		else if(spBT == m_spRecordArrayType)
		{
			String &raid = a_tokens[i+1];

			if(raid != "0")
			{
				sp<RecordArray> *pspRA;
				pspRA = reinterpret_cast<sp<RecordArray> *>(instance);

				sp<RecordGroup> spRG = m_rgs[raid];
				sp<RecordArray> spRA = m_ras[raid];

				if(!spRG.isValid())
				{
					spRG = new RecordGroup();
#if FE_COUNTED_TRACK
					spRG->setName(attr_name);
#endif
					m_rgs[raid] = spRG;
				}

				if(!spRA.isValid())
				{
					spRA = new RecordArray();
					m_ras[raid] = spRA;
				}

				*pspRA = spRA;
			}
		}
		else if(spBT == m_spRecordType)
		{
			String &rid = a_tokens[i+1];

			if(rid != "0")
			{
				RecordWiringInfo wiring;
				wiring.m_record = record;
				wiring.m_aRecord.setup(m_spScope, attr_name);
				wiring.m_id = rid;
				m_wiringList.push_back(wiring);
			}
		}
		else if(spBT == m_spWeakRecordType)
		{
			String &rid = a_tokens[i+1];

			if(rid != "0")
			{
				WeakRecordWiringInfo wiring;
				wiring.m_record = record;
				wiring.m_aRecord.setup(m_spScope, attr_name);
				wiring.m_id = rid;
				m_wkWiringList.push_back(wiring);
			}
		}
		else if(spBT->getInfo().isValid())
		{
			if(a_tokens[i+1].isString())
			{
				String &value = a_tokens[i+1];
				std::string s(value.c_str());
				std::istringstream istrm(s);
				spBT->getInfo()->input(istrm, instance,
					BaseType::Info::e_ascii);
			}
			else
			{
				void *value = a_tokens[i+1];
				std::string s((char *)value, a_tokens[i+1].m_size);
				std::istringstream istrm(s);
				spBT->getInfo()->input(istrm, instance,
					BaseType::Info::e_binary);
			}
		}
		else
		{
			fail("impossibility");
		}
	}
#if FE_AR_HANDLE_DEBUG
		feLog("AsciiReader::handleRecord finalize\n");
#endif
	spScope->finalize(record);
#if FE_AR_HANDLE_DEBUG
		feLog("AsciiReader::handleRecord done\n");
#endif
}

sp<RecordGroup> AsciiReader::input(std::istream &a_istrm)
{
	BlockType blocktype = e_end;
	Token token;
	Token lastToken;
	Array<Token> tokens;
	while(readToken(a_istrm, token))
	{
		if(token.m_string == FE_KW_RECORD)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_record;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_RECORDGROUP)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_group;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_DEFAULTGROUP)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_defaultgroup;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_TEMPLATE)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_template;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_ATTRIBUTE)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_attribute;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_LAYOUT)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_layout;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_INFO)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_info;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_COMMENT)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_comment;
			lastToken=token;
		}
		else if(token.m_string == FE_KW_END)
		{
			dispatchBlock(blocktype, lastToken, tokens);
			blocktype = e_end;
			lastToken=token;
			break;
		}
		else
		{
			tokens.push_back(token);
		}
		token.clear();
	}

	if(blocktype != e_end)
	{
		feX(e_invalidFile,
			"missing end token");
	}

	wireRecords();
	recordGroupsToArrays();

	sp<RecordGroup> spRG;
	if(m_rgs.size() >= 1)
	{
		spRG = m_rgs["1"];
	}

	if(!spRG.isValid())
	{
		spRG = new RecordGroup();
	}

	reset();
	return spRG;
}

void AsciiReader::fail(const String &a_message)
{
	feX("AsciiReader::fail", a_message.c_str());
}

void AsciiReader::wireRecords(void)
{
	for(t_wiring::iterator it = m_wiringList.begin();
		it != m_wiringList.end(); it++)
	{
		t_str_r::iterator i_r = m_rs.find(it->m_id);
		if(i_r == m_rs.end())
		{
			feX(e_cannotFind,
				"AsciiReader::wireRecords"
				"stream corrupt");
		}
		it->m_aRecord(it->m_record) = i_r->second;
	}

	for(t_wk_wiring::iterator it = m_wkWiringList.begin();
		it != m_wkWiringList.end(); it++)
	{
		t_str_r::iterator i_r = m_rs.find(it->m_id);
		if(i_r == m_rs.end())
		{
			feX(e_cannotFind,
				"AsciiReader::wireRecords"
				"stream corrupt");
		}
		it->m_aRecord(it->m_record) = i_r->second;
	}
}

void AsciiReader::recordGroupsToArrays(void)
{
	for(t_id_ra::iterator i_ra = m_ras.begin(); i_ra != m_ras.end(); i_ra++)
	{
		if(i_ra->first == "0") { continue; }
		sp<RecordArray> spRA = i_ra->second;
		sp<RecordGroup> spRG = m_rgs[i_ra->first];

		RecordGroup::iterator i_rg = spRG->begin();
		if(i_rg == spRG->end()) { return; }
		sp<RecordArray> spRGRA(*i_rg);
		for(int i = 0; i < spRGRA->length(); i++)
		{
			spRA->add(spRGRA->getRecord(i));
		}
	}
}

#if 0
void AsciiReader::tokenize(Array<String> &a_tokens, std::string &a_string)
{
	boost::char_separator<char> sep(" \t\n");
	typedef boost::tokenizer<boost::char_separator<char> > t_tokenizer;
	t_tokenizer tokens(a_string, sep);
	for(t_tokenizer::iterator i_t = tokens.begin(); i_t != tokens.end(); ++i_t)
	{
		String s = (*i_t).c_str();
		a_tokens.push_back(s);
	}
}

void AsciiReader::readAttribute(std::istream &a_istrm)
{
	AttributeInfo info;

	if(!readWord(a_istrm, info.m_name))
	{
		fail("missing attribute name");
	}
}

void AsciiReader::readInfo(std::istream &a_istrm)
{
	// INPUT: version
	U32 version;
	a_istrm >> version;
	if(version != FE_SERIAL_VERSION)
	{
		fail("invalid file version");
	}
}
#endif

} /* namespace */
} /* namespace */

