import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "DrawScalarField",
                "DensityField",
                "SourceSink",
                "SimpleDiffusion",
                "DensitySink",
                "DensitySource",
                "fieldDL" ]

    dll = module.DLL( "fexFieldDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexViewerLib",
                "fexWindowDLLib",
                "fexSpatialDLLib"
                ]

    forge.deps( ["fexFieldDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexFieldDL",                   None,       None) ]

