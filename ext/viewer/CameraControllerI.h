/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_CameraControllerI_h__
#define __viewer_CameraControllerI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Viewpoint controller for a Viewer

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT CameraControllerI:
	virtual public Component,
	public CastableAs<CameraControllerI>,
	public ObjectSafe<CameraControllerI>
{
	public:
						/** @brief Transform to center of interest

							A look-at controller might choose to spin
							around this point. */
virtual
const	SpatialTransform&	interestMatrix(void) const						=0;

						/** @brief Secondary point of significance

							A controller may wish this matrix to be
							represented in a viewer. */
virtual
const	SpatialTransform&	auxillaryMatrix(void) const						=0;

virtual	BWORD				closeEvent(void) const							=0;
virtual	U32					keyCount(U32 key) const							=0;
virtual	U32					setKeyCount(U32 key,U32 count)					=0;
virtual	void				setKeyMax(U32 key,U32 max)						=0;
virtual	Real				mouseDial(U32 mode) const						=0;

virtual	SpatialVector		lookPoint(void) const							=0;
virtual	Real				azimuth(void) const								=0;
virtual	Real				downangle(void) const							=0;
virtual	Real				distance(void) const							=0;

virtual	void				setVertical(SpatialVector a_vertical)			=0;
virtual	void				setLookPoint(SpatialVector a_point)				=0;
virtual	void				setLookAngle(Real azimuth,Real downangle,
									Real distance)							=0;

virtual	sp<CameraI>			camera(void) const								=0;
virtual	void				useCustomCamera(sp<CameraI> a_spCamera)			=0;

virtual	BWORD				rotationLock(void) const 						=0;
virtual	void				setRotationLock(BWORD a_rotationLock)			=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_CameraControllerI_h__ */
