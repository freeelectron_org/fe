import os
import sys
import string
forge = sys.modules["forge"]

def setup(module):
    srclist = [ "eigen.pmh",
                "eigenDL",
                ]

    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    dll = module.DLL( "fexEigenDL", srclist)

    forge.deps( ["fexEigenDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexEigenDL",                   None,       None) ]

    module.Module('test')

def auto(module):
    test_file = """
#include <iostream>
#include <eigen3/Eigen/Dense>

int main()
{
    Eigen::MatrixXd m(2,2);
    m(0,0) = 3;
    m(1,0) = 2.5;
    m(0,1) = -1;
    m(1,1) = m(1,0) + m(0,1);
//  std::cout << m << std::endl;
}
    \n"""

    result = forge.cctest(test_file)

    forge.includemap.pop('eigen', None)

    return result
