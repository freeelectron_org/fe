/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <flight/flight.pmh>

#define FE_SAD_DEBUG	TRUE

namespace fe
{
namespace ext
{

SurfaceAccessibleDrone::SurfaceAccessibleDrone(void)
{
#if FE_SAD_DEBUG
	feLog("SurfaceAccessibleDrone::SurfaceAccessibleDrone\n");
#endif
}

SurfaceAccessibleDrone::~SurfaceAccessibleDrone(void)
{
#if FE_SAD_DEBUG
	feLog("SurfaceAccessibleDrone::~SurfaceAccessibleDrone\n");
#endif
}

void SurfaceAccessibleDrone::reset(void)
{
	SurfaceAccessibleBase::reset();
}

sp<SurfaceAccessorI> SurfaceAccessibleDrone::accessor(
	String a_node,Element a_element,String a_name,
	Creation a_create,Writable a_writable)
{
	sp<SurfaceAccessorDrone> spAccessor(new SurfaceAccessorDrone);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurface(m_spSurfaceI);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleDrone::accessor(
	String a_node,Element a_element,Attribute a_attribute,
	Creation a_create,Writable a_writable)
{
	sp<SurfaceAccessorDrone> spAccessor(new SurfaceAccessorDrone);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurface(m_spSurfaceI);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

BWORD SurfaceAccessibleDrone::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAD_DEBUG
	feLog("SurfaceAccessibleDrone::load \"%s\"\n",a_filename.c_str());
#endif

	m_spSurfaceI=registry()->create("*.SurfaceDrone");

#if FE_SAD_DEBUG
	feLog("SurfaceAccessibleDrone::load valid %d\n",
			m_spSurfaceI.isValid());
#endif

	//* NOTE force cache
	m_spSurfaceI->prepareForSample();

	return TRUE;
}

sp<SurfaceI> SurfaceAccessibleDrone::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
#if FE_SAD_DEBUG
	feLog("SurfaceAccessibleDrone::surface group \"%s\"\n",a_group.c_str());
#endif

	if(!a_group.empty())
	{
		//* TODO
	}

	if(m_spSurfaceI.isValid())
	{
		m_spSurfaceI->setRestrictions(a_restrictions);
	}

	return m_spSurfaceI;
}

} /* namespace ext */
} /* namespace fe */
