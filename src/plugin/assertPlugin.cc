/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "plugin/plugin.pmh"

namespace fe
{

class InfoComponent : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					sp<Component>* pspC = (sp<Component>*)instance;
					return (*pspC).isValid()? (*pspC)->name(): "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					sp<Component>* pspC = (sp<Component>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					sp<Component>* pspC = (sp<Component>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)sp<Component>; }
virtual	void	destruct(void *instance)
				{	((sp<Component>*)instance)->~sp<Component>();}
};

class InfoComponentCOW : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					cp<Component>* pcpC = (cp<Component>*)instance;
					return (*pcpC).isValid()? (*pcpC)->name(): "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)cp<Component>; }
virtual	void	destruct(void *instance)
				{	((cp<Component>*)instance)->~cp<Component>();}
};

class InfoComponentHandle : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					hp<Component>* phpC = (hp<Component>*)instance;
					return (*phpC).isValid()? (*phpC)->name(): "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					hp<Component>* phpC = (hp<Component>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					hp<Component>* phpC = (hp<Component>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)hp<Component>; }
virtual	void	destruct(void *instance)
				{	((hp<Component>*)instance)->~hp<Component>();}
};

class FE_DL_EXPORT InfoComponentArray:
	public TypeInfoArray< Array< sp<Component> > >,
	public CastableAs<InfoComponentArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	String	print(void *instance)
				{
					return "";
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class FE_DL_EXPORT InfoComponentHandleArray:
	public TypeInfoArray< Array< hp<Component> > >,
	public CastableAs<InfoComponentHandleArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	String	print(void *instance)
				{
					return "";
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class FE_DL_EXPORT InfoStringHandleMap :
	public TypeInfoConstructable< std::map< String,hp<Component> > >
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	String	print(void *instance)
				{
					return "";
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class InfoCatalog : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					sp<Catalog>* pspC = (sp<Catalog>*)instance;
					return (*pspC).isValid()? (*pspC)->name(): "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					sp<Catalog>* pspC = (sp<Catalog>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					sp<Catalog>* pspC = (sp<Catalog>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)sp<Catalog>; }
virtual	void	destruct(void *instance)
				{	((sp<Catalog>*)instance)->~sp<Catalog>();}
};

class FE_DL_EXPORT InfoCatalogRemove :
	public TypeInfoConstructable<Catalog::Remove>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	String	print(void *instance)
				{
					return "";
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class InfoStateCatalog : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					sp<StateCatalog>* pspC = (sp<StateCatalog>*)instance;
					return (*pspC).isValid()? (*pspC)->name(): "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					sp<Catalog>* pspC = (sp<Catalog>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					sp<Catalog>* pspC = (sp<Catalog>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)sp<StateCatalog>; }
virtual	void	destruct(void *instance)
				{	((sp<StateCatalog>*)instance)->~sp<StateCatalog>();}
};

void assertPlugin(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spT;

	spT = spTypeMaster->assertType< sp<Component> >("Component");
	spT->setInfo(new InfoComponent());

	spT = spTypeMaster->assertType< cp<Component> >("COW");
	spT->setInfo(new InfoComponentCOW());

	spT = spTypeMaster->assertType< hp<Component> >("hpComponent");
	spT->setInfo(new InfoComponentHandle());

	spT = spTypeMaster->assertType< Array< sp<Component> > >("ComponentArray");
	spT->setInfo(new InfoComponentArray());

	spT = spTypeMaster->assertType< Array< hp<Component> > >(
			"ComponentHandleArray");
	spT->setInfo(new InfoComponentHandleArray());

	spT = spTypeMaster->assertType< std::map< String,hp<Component> > >(
			"StringHandleMap");
	spT->setInfo(new InfoStringHandleMap());

	spT = spTypeMaster->assertType< sp<Catalog> >("Catalog");
	spT->setInfo(new InfoCatalog());

	spT = spTypeMaster->assertType<Catalog::Remove>("Catalog::Remove");
	spT->setInfo(new InfoCatalogRemove());

	spT = spTypeMaster->assertType< sp<StateCatalog> >("StateCatalog");
	spT->setInfo(new InfoStateCatalog());

}

} /* namespace */
