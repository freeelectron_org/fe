/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terminal/terminal.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

static sp<SingleMaster> gs_spSingleMaster;
static sp<Terminal> gs_spTerminal;

extern "C"
{

FE_DL_EXPORT int fe_terminal_execute(
		int a_outputSize,char* a_output,const char* a_input)
{
//	feLogDirect("::fe_terminal_execute \"%s\"\n",a_input);

	if(gs_spSingleMaster.isNull())
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"
		const String loadPath=
				System::getLoadPath((void*)fe_terminal_execute,TRUE);
#pragma GCC diagnostic pop

		const String libPath=loadPath.pathname();
		DL_Loader::addPath(libPath);

		gs_spSingleMaster=SingleMaster::create();
		if(gs_spSingleMaster.isNull())
		{
			return 0;
		}

		sp<Registry> spRegistry=gs_spSingleMaster->master()->registry();
		if(spRegistry.isNull())
		{
			gs_spSingleMaster=NULL;
			return 0;
		}

		spRegistry->manage("fexTerminalDL");
	}
	if(gs_spSingleMaster.isValid() && gs_spTerminal.isNull())
	{
		gs_spTerminal=
				gs_spSingleMaster->master()->registry()->create("*.Terminal");
	}
	if(gs_spTerminal.isValid())
	{
		String result=gs_spTerminal->execute(a_input);
		strncpy(a_output,result.c_str(),a_outputSize);
		return 1;
	}

	return 0;
}

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexMetaDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<Terminal>("Component.Terminal.fe");

	pLibrary->add<TerminalDraw>("DrawI.TerminalDraw.fe");

	pLibrary->add<TerminalDispatch>("DispatchI.TerminalDispatch.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
