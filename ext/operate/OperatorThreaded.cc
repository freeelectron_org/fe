/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operate/operate.pmh>

#define FE_OTHR_DEBUG			FALSE
#define FE_OTHR_THREAD_VERBOSE	FALSE

#define FE_OTHR_ATOM_GRAIN		100		//* TODO param

using namespace fe;
using namespace fe::ext;

void OperatorThreaded::setFullRange(sp<SpannedRange> a_spRange)
{
//	feLog("fine atoms:\n%s\n",a_spRange->dump().c_str());
	m_spFullRange=a_spRange->combineAtoms(FE_OTHR_ATOM_GRAIN);
//	feLog("full atoms:\n%s\n",m_spFullRange->dump().c_str());
}

void OperatorThreaded::setNonAtomicRange(I32 a_start,I32 a_end)
{
	m_spFullRange=sp<SpannedRange>(new SpannedRange());
	m_spFullRange->nonAtomic().add(a_start,a_end);
}

void OperatorThreaded::setAtomicRange(
		sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
		OperatorSurfaceCommon::AtomicChange a_atomicChange,
		String a_group)
{
	sp<SpannedRange> spRange=a_spSurfaceAccessibleI->atomize(
			SurfaceAccessibleI::AtomicChange(a_atomicChange),a_group,
			8*catalog<I32>("Threads"));

//	feLog("OperatorThreaded::setAtomicRange\n%s\n",
//			spRange->dump().c_str());

	m_spFullRange=spRange->combineAtoms(FE_OTHR_ATOM_GRAIN);

//	feLog("  combined\n%s\n",
//			m_spFullRange->dump().c_str());
}

void OperatorThreaded::initialize(void)
{
	catalog<I32>("Threads")=1;
	catalog<I32>("Threads","min")=1;
	catalog<I32>("Threads","high")=32;
	catalog<I32>("Threads","max")=128;
	catalog<bool>("Threads","joined")=true;
	catalog<String>("Threads","IO")="input output";
	catalog<String>("Threads","hint")=
			"Number of workers to process the data in parallel."
			"  The number of jobs may not be the same."
			"  When the thread count is set to one,"
			" no threads are spawned and the entire"
			" processing job is done at once with the existing thread.";

	catalog<bool>("AutoThread")=true;
	catalog<String>("AutoThread","label")="Auto";
	catalog<bool>("AutoThread","joined")=true;
	catalog<String>("AutoThread","IO")="input output";
	catalog<String>("AutoThread","hint")=
			"Automatically set the number of threads.";

	catalog<bool>("Paging")=false;
	catalog<String>("Paging","label")="Page";
	catalog<bool>("Paging","joined")=true;
	catalog<String>("Paging","hint")=
			"Respect paging even when it can be avoided."
			"  This is relevant for Houdini 12.5+."
			"  Houdini 11 and Maya are never paged."
			"  Houdini 12.0 and 12.1 are always paged.";

	catalog<bool>("StayAlive")=false;
	catalog<String>("StayAlive","label")="Stay Alive";
	catalog<bool>("StayAlive","joined")=true;
	catalog<String>("StayAlive","hint")=
			"Keep the threads running and looking for jobs even"
			" when this node isn't being processed."
			"  This may speed up quick nodes where launching threads is"
			" a large portion of the processing time,"
			" but this technique should be considered tempermental"
			" (crashes may become more likely).";

#ifdef FE_OPENMP
	catalog<String>("Work")="Omp";
#else
	catalog<String>("Work")="Gang";
#endif
	catalog<String>("Work","label")="";
	catalog<String>("Work","choice:0")="Gang";
	catalog<String>("Work","choice:1")="Omp";
	catalog<String>("Work","label:1")="OpenMP";
	catalog<String>("Work","choice:2")="Tbb";
	catalog<String>("Work","label:2")="TBB";
}

BWORD OperatorThreaded::accessOutput(sp<SurfaceAccessibleI>& a_rspAccessible,
	Record& a_rSignal,Message a_message)
{
//	adjustThreads();

	const BWORD result=OperatorSurfaceCommon::accessOutput(
			a_rspAccessible,a_rSignal,a_message);

	if(result && a_rspAccessible.isValid())
	{
		a_rspAccessible->setPaging(catalog<bool>("Paging"));

		catalog<bool>("OutputThreadable")=a_rspAccessible->threadable();
	}

	return result;
}

void OperatorThreaded::adjustThreads(void)
{
	I32& rThreads=catalog<I32>("Threads");

	if(!catalogOrDefault<bool>("OutputThreadable",false))
	{
		rThreads=1;
	}
	else if(catalog<bool>("AutoThread"))
	{
		rThreads=Thread::hardwareConcurrency();
	}

	setThreading((rThreads==1)? e_singleThread: e_multiThread);
}

void OperatorThreaded::handle(Record& a_rSignal)
{
	runStage("");
}

void OperatorThreaded::runStage(String a_stage)
{
#if FE_OTHR_DEBUG
	feLog("OperatorThreaded::runStage \"%s\" support \"%s\"\n",
			name().c_str(),Thread::support().c_str());
#endif

	adjustThreads();

	I32& rThreads=catalog<I32>("Threads");
	BWORD runDirect=(rThreads==1);
	if(!runDirect && catalogOrDefault<I32>("Refinement",0))
	{
		catalog<String>("warning")+=
				"bypassing multi-threading while Refinement is on;";
		setThreading(e_singleThread);
		runDirect=TRUE;
	}
	if(runDirect)
	{
		m_spWorkForceI=NULL;
	}
	else
	{
		String work=catalog<String>("Work");

		BWORD workMessage=FALSE;

#ifdef FE_OPENMP
		if(work=="Gang" && Thread::support()=="omp")
		{
			work="Omp";
			catalog<String>("Work")=work;

			catalog<String>("warning")+=
					"Gang threading not currently functional using OpenMP;";
			workMessage=TRUE;
		}
#else
		if(work=="Omp")
		{
			work="Gang";
			catalog<String>("Work")=work;

			catalog<String>("warning")+=
					"OpenMP support was excluded from this build;";
			workMessage=TRUE;
		}
#endif

		const BWORD quiet=TRUE;
		m_spWorkForceI=registry()->create("*.Work"+work,quiet);
		if(m_spWorkForceI.isNull())
		{
			m_spWorkForceI=registry()->create("WorkForceI",quiet);

			if(m_spWorkForceI.isValid())
			{
				catalog<String>("warning")+=
						"threading component for \"*.Work"+
						work+"\" not available;";
				workMessage=TRUE;
			}
			else
			{
				catalog<String>("warning")+=
						"no threading components available;";
			}
		}
		if(m_spWorkForceI.isValid() && workMessage)
		{
			catalog<String>("warning")+=
					"threading with \""+m_spWorkForceI->name()+"\";";
		}
	}

	if(m_spWorkForceI.isValid())
	{
		//* TODO fix stayAlive logic
//		const BWORD stayAlive=(catalog<bool>("StayAlive") && rThreads>1);
		const BWORD stayAlive=FALSE;

		if(stayAlive)
		{
			catalog<String>("warning")+="using Stay Alive is dangerous;";
		}

		m_spWorkForceI->waitForJobs(stayAlive);
		const I32 threadsAlive=m_spWorkForceI->threadsAlive();

		if(threadsAlive &&
				(!stayAlive || (stayAlive && threadsAlive!=rThreads)))
		{
#if FE_OTHR_THREAD_VERBOSE
			feLog("Pre-finish stayAlive %d threadsAlive %d\n",
					stayAlive,threadsAlive);
#endif

			m_spWorkForceI->stop();
		}

#if FE_OTHR_THREAD_VERBOSE
		feLog("OperatorThreaded::runStage stage \"%s\" threads %d range %s\n",
				a_stage.c_str(),rThreads,m_spFullRange->brief().c_str());
#endif
		m_spWorkForceI->run(sp<WorkI>(this),rThreads,m_spFullRange,a_stage);
	}
	else
	{
		//* for a single thread, run directly
#if FE_OTHR_THREAD_VERBOSE
		feLog("OperatorThreaded::runStage stage \"%s\" direct range %s\n",
				a_stage.c_str(),m_spFullRange->brief().c_str());
#endif

		run(-1,m_spFullRange,a_stage);
	}

#if FE_OTHR_DEBUG
	feLog("OperatorThreaded::runStage done\n");
#endif
}
