global proc fe_add_operator(string $a_nodeType,int $optionBox,
	string $a_input0,string $a_required0,
	string $a_input1,string $a_required1,
	string $a_input2,string $a_required2,
	string $a_input3,string $a_required3)
{
	global int $fe_reveal_stages;
	global int $fe_use_deformers;
	int $renameShape = !$fe_reveal_stages && !$fe_use_deformers;

	string $inputName[4] =
			{ $a_input0, $a_input1, $a_input2, $a_input3 };
	string $inputRequired[4] =
			{ $a_required0, $a_required1, $a_required2, $a_required3 };

	$minInputs=0;
	$maxInputs=0;

	for($index=0;$index<4;$index++)
	{
		if($inputName[$index] != "")
		{
			$maxInputs++;
			if($inputRequired[$index]=="required")
			{
				$minInputs++;
			}
		}
	}

	string $sources[]=`ls -sl`;
	int $sourceCount=size($sources);
	if($sourceCount<$minInputs || $sourceCount>$maxInputs)
	{
		if($minInputs==$maxInputs)
		{
			error("// fe_add_operator() for " + $a_nodeType +
					" needs exactly " + $minInputs +
					" selection" + ($minInputs>1? "s": "") + "\n");
		}
		else
		{
			error("// fe_add_operator() for " + $a_nodeType +
					" needs between " + $minInputs +
					" and " + $maxInputs + " selections\n");
		}
		return;
	}

	int $use_deformers = $fe_use_deformers && $sourceCount;

	string $operator;
	if($use_deformers)
	{
		select $sources[0];
		$operator=fe_createDeformer($a_nodeType,"");

		string $operator_matrix=$operator+".geomMatrix[0]";
		string $source_matrix=$sources[0]+".worldMatrix";

		connectAttr $source_matrix $operator_matrix;
	}
	else
	{
		$operator=fe_createNode($a_nodeType,"","");
	}

	string $baseZero="";
	string $pathZero="";
	string $primarySource="";
	string $shadingGroup="initialShadingGroup";

	int $sourceIndex=0;
	for($index=0;$index<4;$index++)
	{
		if($inputName[$index] != "")
		{
			string $source;
			string $input_out;

			if($inputRequired[$index]=="required" || $sourceIndex<$sourceCount)
			{
				if($sourceIndex >= $sourceCount)
				{
					error("// fe_add_operator() for " + $a_nodeType +
							" logical error using up " + $sourceCount +
							" sources\n");
					return;
				}

				$source=$sources[$sourceIndex];

				print ("source: " + $source + "\n");

				if($sourceIndex == 0)
				{
					string $sourceChildren[]=
							`listRelatives -fullPath -children -shapes $source`;
					string $firstChild=$sourceChildren[0];
					if($firstChild!="" && `attributeExists "worldSpace" $firstChild`)
					{
						$renameShape=false;
					
					}

					if($renameShape)
					{
						$pathZero = firstParentOf($source);

						string $tokens[];
						$numTokens = `tokenize $source "|" $tokens`;
						$baseZero = $tokens[$numTokens-1];

						print ("baseZero: " + $baseZero + "\n");

						string $renamed=fe_renameNode($source,$baseZero+"Orig");

						string $parents[]=`listRelatives -parent $renamed`;
						if($parents[0] != "")
						{
							string $reparented[]=`parent -world $renamed`;
							$renamed=$reparented[0];
						}

						string $renamed_intermediate=$renamed + ".intermediateObject";
						setAttr $renamed_intermediate 1;

						$source=$renamed;

						hide $source;
					}

					$primarySource=$source;
				}

				string $sourceChildren[]=
						`listRelatives -fullPath -children -shapes $source`;
				string $firstChild=$sourceChildren[0];
				if($firstChild!="")
				{
					if(`attributeExists "worldMesh" $firstChild`)
					{
						$input_out=$firstChild+".worldMesh[0]";
					}
					else if(`attributeExists "worldSpace" $firstChild`)
					{
						select $firstChild;
						fe_add_op_CurveCombineOp(false);

						$selection=`ls -sl`;
						$selectionCount=size($selection);
						if($selectionCount<1)
						{
							error("// fe_add_operator() failed to convert input curve\n");
							return;
						}

						$input_out=$selection[0]+".worldMesh[0]";
						$source=$selection[0];

						print ("hide source: " + $source + "\n");

						hide $source;
					}
					else
					{
						error("// fe_add_operator() for " + $a_nodeType +
								" could not discern output attribute for " +
								$firstChild + "\n");
						return;
					}
				}

				if($sourceIndex == 0 && $sourceChildren[0] != "")
				{
					string $shadingList[]=
							`hyperShade -listDownstreamNodes $sourceChildren[0]`;

					if($shadingList[0] != "")
					{
						string $checkType = `nodeType $shadingList[0]`;
						if($checkType == "shadingEngine")
						{
							$shadingGroup=$shadingList[0];
						}
					}
				}

				$sourceIndex++;
			}
			else if($inputRequired[$index]=="freeze")
			{
				string $freeze_command="duplicate -name " + $primarySource + "Base -returnRootsOnly " + $primarySource;

				string $freeze[]=eval($freeze_command);

				delete -ch $freeze[0];
				hide $freeze[0];

				$source=$freeze[0];
				string $parents[]=`listRelatives -parent $source`;
				if($parents[0] != "")
				{
					string $reparented[]=`parent -world $source`;
					$source=$reparented[0];
				}

				if(`attributeExists "worldMesh" $source`)
				{
					$input_out=$source+".worldMesh[0]";
				}
				else if(`attributeExists "worldSpace" $source`)
				{
					select $source;
					fe_add_op_CurveCombineOp(false);

					$selection=`ls -sl`;
					$selectionCount=size($selection);
					if($selectionCount<1)
					{
						error("// fe_add_operator() failed to convert input curve\n");
						return;
					}

					$input_out=$selection[0]+".worldMesh[0]";

					$source=$selection[0];
				}
				else
				{
					error("// fe_add_operator() for " + $a_nodeType +
							" could not discern output attribute for " +
							$source + "\n");
					return;
				}
			}
			else
			{
				break;
			}

			if(!$use_deformers || $index)
			{
				string $input_mat=$source+".worldMatrix[0]";

				string $xform_input=fe_createNode("transformGeometry","","");

				string $xform_input_in=$xform_input+".inputGeometry";
				string $xform_input_mat=$xform_input+".transform";
				string $xform_input_out=$xform_input+".outputGeometry";

				string $operator_in=$operator+"."+$inputName[$index];

				connectAttr $input_out $xform_input_in;
				connectAttr $input_mat $xform_input_mat;

				connectAttr $xform_input_out $operator_in;
			}
		}
	}

	if(!$use_deformers)
	{
		string $resultShape=fe_createNode("mesh","poly"+$a_nodeType+"Shape","");
		string $resultShape_in=$resultShape+".inMesh";

		string $operator_out=$operator+".Output_Surface";
		connectAttr $operator_out $resultShape_in;

		string $resultGroup[]=`listRelatives -fullPath -parent $resultShape`;

		select -replace $resultGroup[0];

		hyperShade -assign $shadingGroup;

		if($renameShape && $baseZero!="")
		{
			if($pathZero == "")
			{
				rename $resultGroup[0] $baseZero;
			}
			else
			{
				string $parented[]=`parent $resultGroup[0] $pathZero`;
				string $resultMoved=$pathZero + "|" + $parented[0];

				rename $resultMoved $baseZero;
			}
		}
	}
}
