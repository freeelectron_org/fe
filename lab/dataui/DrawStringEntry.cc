/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawStringEntry.h"
#include "StringEntryController.h"
#include "RootLocate.h"

namespace fe
{
namespace ext
{

DrawStringEntry::DrawStringEntry(void)
{
	m_spNarrow=new DrawMode();
	m_spWide=new DrawMode();

	m_spNarrow->setLineWidth(0.5f);
	m_spWide->setLineWidth(2.0f);
}

DrawStringEntry::~DrawStringEntry(void)
{
}

void DrawStringEntry::initialize(void)
{
	m_color[0] =	Color(	1.0f,	0.5f,	0.5f	);
	m_color[1] =	Color(	1.0f,	1.0f,	1.0f	);
	m_color[2] =	Color(	1.0f,	1.0f,	0.0f	);
	m_color[3] =	Color(	0.0f,	1.0f,	0.0f	);
	m_color[4] =	Color(	1.0f,	0.5f,	0.5f	);
	m_color[5] =	Color(	1.0f,	0.0f,	0.0f	);
}

void DrawStringEntry::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_asStringEntry.bind(l_sig->scope());
}

void DrawStringEntry::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }

//	sp<EventContextI> spEC(m_drawview.windowI()->getEventContextI());

	for(RecordGroup::iterator it = m_drawview.group()->begin();
		it != m_drawview.group()->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		if(m_asStringEntry.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Vector4 &v = m_asStringEntry.screen(spRA,i);
				if(v[2] <= v[0] || v[3] <= v[1]) { continue; }
				drawBox(m_drawview.drawI(),	v[0], v[1], v[2], v[3],
								m_asStringEntry.depth(spRA,i),
								m_asStringEntry.state(spRA,i));

				std::string s = m_asStringEntry.text(spRA,i).c_str();
				int w = m_drawview.drawI()->font()->pixelWidth(
						m_asStringEntry.text(spRA,i));

				I32 a, d;
				m_drawview.drawI()->font()->fontHeight(&a, &d);

				int x_offset = int(a);
				int y_offset = int((v[3]-v[1])-(a+d)) / 2;
				Vector4 textLoc(v[0]+x_offset, v[1]+y_offset+d, 0.0f);

				int allowed_txt_w = v[2]-v[0]-x_offset;
				while(s.length() > 0 && w > allowed_txt_w)
				{
					s.erase(s.length()-1,1);
					w = m_drawview.drawI()->font()->pixelWidth(s.c_str());
				}

				if(m_asStringEntry.state(spRA,i) & StringEntryController::e_set)
				{
					w = m_drawview.drawI()->font()->pixelWidth(
						s.substr(0,m_asStringEntry.cursor(spRA,i)).c_str());
					SpatialVector line[2];
					set(line[0],
						(Real)(v[0] + x_offset + w),
						(Real)(v[1]),
						(Real)(m_asStringEntry.depth(spRA,i)));
					set(line[1],
						(Real)(v[0] + x_offset + w),
						(Real)(v[3]),
						(Real)(m_asStringEntry.depth(spRA,i)));
					m_drawview.drawI()->setDrawMode(m_spNarrow);
					m_drawview.drawI()->drawLines(line, NULL, 2,
						DrawI::e_discrete, false, &m_color[3]);
				}

				m_drawview.drawI()->drawAlignedText(
						textLoc,s.c_str(),m_color[0]);
			}
		}
	}
}

void DrawStringEntry::drawBox(sp<DrawI> spDraw, Real x1,Real y1,
		Real x2,Real y2,Real z,int state)
{
	Box3 box;
	set(box, x1, y1, z, x2-x1, y2-y1, z);
	Color c(m_color[5]);

	if(state & StringEntryController::e_sel)
	{
		if(state & StringEntryController::e_in) { c = m_color[3]; }
		else { c = m_color[4]; }
	}
	else
	{
		if(state & StringEntryController::e_in) { c = m_color[2]; }
		else { c = m_color[1]; }
	}

	spDraw->setDrawMode(
			(state & StringEntryController::e_set)? m_spWide: m_spNarrow);
	spDraw->drawBox(box, c);
}

} /* namespace ext */
} /* namespace fe */
