/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceSearchable_h__
#define __surface_SurfaceSearchable_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Tree-searchable Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceSearchable:
	public SurfaceSphere,
	public CastableAs<SurfaceSearchable>
{
	public:

		//* optional arrays
		typedef enum
		{
			e_arrayNull=		0x00,
			e_arrayColor=		0x01,
			e_arrayUV=			0x02,
			e_arrayRadius=		0x04
		} Arrays;

	class FE_DL_EXPORT Impact:
		public SurfaceSphere::Impact,
		public CastableAs<Impact>
	{
		public:
							Impact(void)
							{
#if FE_COUNTED_STORE_TRACKER
								setName("SurfaceSearchable::Impact");
#endif
							}
	virtual					~Impact(void)									{}

	virtual	I32				triangleIndex(void)
							{	return m_triangleIndex; }
			void			setTriangleIndex(I32 a_triangleIndex)
							{	m_triangleIndex=a_triangleIndex; }

	virtual	I32				primitiveIndex(void)
							{	return m_primitiveIndex; }
			void			setPrimitiveIndex(I32 a_primitiveIndex)
							{	m_primitiveIndex=a_primitiveIndex; }

	virtual	I32				nearestSubIndex(void)
							{	return m_nearestSubIndex; }
			void			setNearestSubIndex(I32 a_nearestSubIndex)
							{	m_nearestSubIndex=a_nearestSubIndex; }

	virtual	I32				partitionIndex(void)
							{	return m_partitionIndex; }
			void			setPartitionIndex(I32 a_partitionIndex)
							{	m_partitionIndex=a_partitionIndex; }

	virtual	Vector2			uv(void)
							{	return m_uv; }
			void			setUV(Vector2 a_uv)
							{	m_uv=a_uv; }

	virtual	SpatialVector	du(void)
							{	return m_du; }
			void			setDu(SpatialVector a_du)
							{	m_du=a_du; }

	virtual	SpatialVector	dv(void)
							{	return m_dv; }
			void			setDv(SpatialVector a_dv)
							{	m_dv=a_dv; }

	virtual	SpatialVector	tangent(void)
							{	return m_tangent; }
			void			setTangent(SpatialVector a_tangent)
							{	m_tangent=a_tangent; }

	virtual	SpatialBary		barycenter(void)
							{	return m_barycenter; }
			void			setBarycenter(SpatialBary a_barycenter)
							{	m_barycenter=a_barycenter; }

	virtual	I32				pointIndex0(void) const
							{	return m_pointIndex0; }
			void			setPointIndex0(I32 a_pointIndex0)
							{	m_pointIndex0=a_pointIndex0; }

	virtual	I32				pointIndex1(void) const
							{	return m_pointIndex1; }
			void			setPointIndex1(I32 a_pointIndex1)
							{	m_pointIndex1=a_pointIndex1; }

	virtual	SpatialVector	vertex0(void) const
							{	return m_vertex0; }
			void			setVertex0(SpatialVector a_vertex0)
							{	m_vertex0=a_vertex0; }

	virtual	SpatialVector	vertex1(void) const
							{	return m_vertex1; }
			void			setVertex1(SpatialVector a_vertex1)
							{	m_vertex1=a_vertex1; }

	virtual	SpatialVector	normal0(void) const
							{	return m_normal0; }
			void			setNormal0(SpatialVector a_normal0)
							{	m_normal0=a_normal0; }

	virtual	SpatialVector	normal1(void) const
							{	return m_normal1; }
			void			setNormal1(SpatialVector a_normal1)
							{	m_normal1=a_normal1; }

	virtual	void			copy(sp<SpatialTreeI::Hit>& a_rspHit);

							//* TODO point indexes

		protected:
			I32				m_triangleIndex;
			I32				m_primitiveIndex;
			I32				m_nearestSubIndex;
			I32				m_partitionIndex;
			Vector2			m_uv;
			SpatialVector	m_du;
			SpatialVector	m_dv;
			SpatialVector	m_tangent;
			SpatialBary		m_barycenter;
			I32				m_pointIndex0;
			I32				m_pointIndex1;
			SpatialVector	m_vertex0;
			SpatialVector	m_vertex1;
			SpatialVector	m_normal0;
			SpatialVector	m_normal1;
	};

	//* TODO move to SurfaceBase
	/// @brief match partition strings against patterns
	class FE_DL_EXPORT PartitionSearchable: virtual public PartitionI
	{
		public:
								PartitionSearchable(void):
									m_matchCount(0),
									m_tweaked(FALSE)						{}

	virtual	sp<PartitionI>		clone(void) const;

	virtual	U32					partitionCount(void) const
								{	return m_stringMap.size(); }

	virtual	String				partitionName(U32 a_index) const;

								//* NOTE lookup() and select() can
								//* occur in any order

	virtual	I32					lookup(String a_partitionString);
	virtual	I32					select(String a_filterString)
								{	return select(a_filterString,
											PartitionI::e_matchRegex); }
	virtual	I32					select(String a_filterString,
									PartitionI::FilterMethod a_filterMethod);
	virtual	BWORD				add(String a_string);
	virtual	BWORD				remove(String a_string);

	virtual	BWORD				match(I32 a_partitionIndex)
								{
									return a_partitionIndex>=0 &&
											a_partitionIndex<
											I32(m_filterArray.size()) &&
											m_filterArray[a_partitionIndex];
								}

	virtual	BWORD				matchable(void) const
								{	return m_matchCount>0; }

		private:

			std::map<String,I32>		m_stringMap;
			String						m_filterString;
			PartitionI::FilterMethod	m_filterMethod;
			Array<I32>					m_filterArray;
			I32							m_matchCount;
			BWORD						m_tweaked;
	};

							SurfaceSearchable(void);
virtual						~SurfaceSearchable(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* As SurfaceI
virtual	void				setRefinement(U32 a_refinement)
							{
								m_refinement=a_refinement;
								if(m_spSpatialTree.isValid())
								{
									m_spSpatialTree->setRefinement(
											m_refinement);
								}
							}

virtual	void				setAccuracy(SurfaceI::Accuracy a_accuracy)
							{
								m_accuracy=a_accuracy;
								if(m_spSpatialTree.isValid())
								{
									m_spSpatialTree->setAccuracy(
											m_accuracy);
								}
							}

virtual	void				setSearchable(BWORD a_searchable)
							{
								m_searchable=a_searchable;
							}

virtual	void				setSearch(String a_searchName)
							{
								m_searchName=a_searchName;
							}

virtual	I32					triangleCount(void)	{ return m_elements; }

							using SurfaceSphere::sample;

virtual SpatialTransform	sample(Vector2 a_uv) const;

virtual	void				prepareForSample(void);
virtual	void				prepareForSearch(void);
virtual	void				prepareForUVSearch(void);

							using SurfaceSphere::nearestPoints;
							using SurfaceSphere::nearestPoint;

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const Vector2& a_uv,
									Real a_maxDistance,U32 a_hitLimit) const;
virtual	sp<ImpactI>			nearestPoint(const Vector2& a_uv) const;

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit,
									sp<PartitionI> a_spPartition) const;
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance) const
							{	return nearestPoint(a_origin,
										a_maxDistance,FALSE); }
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance,BWORD a_anyHit) const;

							using SurfaceSphere::rayImpacts;

virtual	Array< sp<SurfaceI::ImpactI> >
							rayImpacts(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,U32 a_hitLimit) const;

							using SurfaceSphere::rayImpact;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const;

virtual	void				partitionWith(String a_attributeName);
virtual	U32					partitionCount(void) const
							{	return m_spPartition.isValid()?
										m_spPartition->partitionCount(): 0; }
virtual	String				partitionName(U32 a_index) const
							{	return m_spPartition.isValid()?
										m_spPartition->partitionName(a_index):
										""; }
virtual	Vector4				partitionSphere(U32 a_index) const;

							using SurfaceSphere::setPartitionFilter;

							//* WARNING not thread-safe
virtual	I32					setPartitionFilter(String a_filterString,
									PartitionI::FilterMethod a_filterMethod)
							{	checkCache();
								return m_spPartition.isValid()?
										m_spPartition->select(
										a_filterString,a_filterMethod): 0; }
virtual	sp<PartitionI>		createPartition(void);

							using SurfaceSphere::draw;

virtual	void				draw(sp<DrawI> a_spDrawI,
									const Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer) const
							{	drawInternal(FALSE,NULL,a_spDrawI,
										a_pColor,sp<DrawBufferI>(NULL),
										sp<PartitionI>(NULL)); }
virtual	void				draw(const SpatialTransform& a_rTransform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const
							{	drawInternal(TRUE,&a_rTransform,
										a_spDrawI,a_pColor,a_spDrawBuffer,
										a_spPartition); }

	protected:

virtual	void				drawInternal(BWORD a_transformed,
									const SpatialTransform* a_pTransform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const		{}

		I32					lookupPartition(String a_partitionString)
							{	return m_spPartition.isValid()?
										m_spPartition->lookup(
										a_partitionString): -1; }

virtual	void				cache(void);
		void				clear();

		void				filterUV(void);
		void				calcTangents(void);
		void				calcBoundingSphere(void);

		void				setOptionalArrays(Arrays a_arrays)
							{	m_optionalArrays=a_arrays; }
		void				resizeArrays(U32 a_elementCount,U32 a_verticeCount);
		void				resizeFor(U32 a_primitiveCount,U32 a_vertexCount,
									U32& a_rElementAllocated,
									U32& a_rVertexAllocated);

		U32						m_refinement;
		SurfaceI::Accuracy		m_accuracy;
		BWORD					m_searchable;
		String					m_searchName;

		U32						m_elements;
		U32						m_vertices;

		Arrays					m_optionalArrays;
		Vector3i*				m_pElementArray; //* <start, count, open>
		SpatialVector*			m_pVertexArray;
		SpatialVector*			m_pNormalArray;
		SpatialVector*			m_pTangentArray;
		Vector2*				m_pUVArray;
		SpatialVector*			m_pUV3Array;
		Color*					m_pColorArray;
		Real*					m_pRadiusArray;
		I32*					m_pTriangleIndexArray;
		I32*					m_pPartitionIndexArray;
		I32*					m_pPointIndexArray;
		I32*					m_pPrimitiveIndexArray;

		Array<I32>				m_vertexMap;
		Array<I32>				m_hullPointMap;
		Array<Vector4i>			m_hullFacePointArray;

		sp<Component>			m_spSearchComponent;
		sp<SpatialTreeI>		m_spSpatialTree;
		sp<SpatialTreeI>		m_spUVTree;

		String					m_partitionAttr;
		sp<PartitionI>			m_spPartition;
		Array<Vector4>			m_partitionSphere;

	protected:
mutable	CountedPoolCore*		m_pImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceSearchable_h__ */
