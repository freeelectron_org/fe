/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <qt/qt.pmh>

#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexDataToolDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

//	pLibrary->addSingleton<ContextOSG>("Component.ContextOSG.fe");
//	pLibrary->add<SceneNodeOSG>("SceneNodeI.SceneNodeOSG.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

