/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#include "fe/plugin.h"
#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOperatorDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<BallCollideCLOp>(
			"OperatorSurfaceI.BallCollideCLOp.fe");

	pLibrary->add<CacheCLOp>(
			"OperatorSurfaceI.CacheCLOp.fe");

	pLibrary->add<ContractCLOp>(
			"OperatorSurfaceI.ContractCLOp.fe");

	pLibrary->add<ClumpCLOp>(
			"OperatorSurfaceI.ClumpCLOp.fe");

	pLibrary->add<CurlCLOp>(
			"OperatorSurfaceI.CurlCLOp.fe");

	pLibrary->add<FlatnessCLOp>(
			"OperatorSurfaceI.FlatnessCLOp.fe");

	pLibrary->add<KinkCLOp>(
			"OperatorSurfaceI.KinkCLOp.fe");

	pLibrary->add<PostGuideCLOp>(
			"OperatorSurfaceI.PostGuideCLOp.fe");

	pLibrary->add<SurfaceWrapCLOp>(
			"OperatorSurfaceI.SurfaceWrapCLOp.fe");

	pLibrary->add<SwayCLOp>(
			"OperatorSurfaceI.SwayCLOp.fe");

	pLibrary->add<TransformCLOp>(
			"OperatorSurfaceI.TransformCLOp.fe");

	pLibrary->add<SurfaceAccessibleOpenCL>(
			"SurfaceAccessibleI.SurfaceAccessibleOpenCL.fe.opencl");

	pLibrary->add<SurfaceCurvesOpenCL>(
			"SurfaceI.SurfaceCurvesOpenCL.fe");

	pLibrary->add<SurfaceTrianglesOpenCL>(
			"SurfaceI.SurfaceTrianglesOpenCL.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
