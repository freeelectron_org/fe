/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

namespace fe
{
namespace ext
{

void MayaDraw::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	FEASSERT(m_pFnMesh);

	sp<SurfaceAccessorMaya> spFormatAccessor=
			Library::create<SurfaceAccessorMaya>("SurfaceAccessorMaya");
	if(!spFormatAccessor.isValid())
	{
		feLog("MayaDraw::drawPoints failed to create SurfaceAccessorMaya\n");
		return;
	}

	spFormatAccessor->setMaster(registry()->master());
	spFormatAccessor->setMeshData(m_meshData);
	spFormatAccessor->bind(SurfaceAccessibleI::e_primitive,"POLY_FORMAT");

	sp<SurfaceAccessorI> spAccessor=spFormatAccessor;
	if(!spAccessor.isValid())
	{
		feLog("MayaDraw::drawPoints failed to access SurfaceAccessorI\n");
		return;
	}

	MPointArray vertexArray;
	vertexArray.setLength(3);

	for(U32 subIndex=0;subIndex<vertices;subIndex++)
	{
		const SpatialVector& vert0=vertex[subIndex];

		vertexArray.set(0,vert0[0],vert0[1],vert0[2]);
		vertexArray.set(1,vert0[0]+0.01,vert0[1],vert0[2]);
		vertexArray.set(2,vert0[0],vert0[1],vert0[2]+0.01);
		m_pFnMesh->addPolygon(vertexArray);

		const U32 index=m_pFnMesh->numPolygons()-1;
		spAccessor->set(index,SurfaceAccessorMaya::e_freePoint);
	}
}

void MayaDraw::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	FEASSERT(m_pFnMesh);

	sp<SurfaceAccessorMaya> spFormatAccessor=
			Library::create<SurfaceAccessorMaya>("SurfaceAccessorMaya");
	if(!spFormatAccessor.isValid())
	{
		feLog("MayaDraw::drawPoints failed to create SurfaceAccessorMaya\n");
		return;
	}

	spFormatAccessor->setMaster(registry()->master());
	spFormatAccessor->setMeshData(m_meshData);
	spFormatAccessor->bind(SurfaceAccessibleI::e_primitive,"POLY_FORMAT");

	sp<SurfaceAccessorI> spAccessor=spFormatAccessor;
	if(!spAccessor.isValid())
	{
		feLog("MayaDraw::drawPoints failed to access SurfaceAccessorI\n");
		return;
	}

	MPointArray vertexArray;

	switch(strip)
	{
		case DrawI::e_discrete:
			vertexArray.setLength(4);

			for(U32 subIndex=0;subIndex<vertices;subIndex+=2)
			{
				const SpatialVector& vert0=vertex[subIndex];
				const SpatialVector& vert1=vertex[subIndex+1];

				vertexArray.set(0,vert0[0],vert0[1],vert0[2]);
				vertexArray.set(1,vert1[0],vert1[1],vert1[2]);
				vertexArray.set(2,vert1[0],vert1[1]+0.001,vert1[2]);
				vertexArray.set(3,vert0[0],vert0[1]+0.001,vert0[2]);
				m_pFnMesh->addPolygon(vertexArray);

				const U32 index=m_pFnMesh->numPolygons()-1;
				spAccessor->set(index,SurfaceAccessorMaya::e_openPoly);
			}
			break;
		case DrawI::e_strip:
		{
			vertexArray.setLength(vertices*2);
			for(U32 subIndex=0;subIndex<vertices;subIndex++)
			{
				const SpatialVector& vert=vertex[subIndex];
				const I32 twinIndex=vertices*2-1-subIndex;

				vertexArray.set(subIndex,vert[0],vert[1],vert[2]);
				vertexArray.set(twinIndex,vert[0],vert[1]+0.001,vert[2]);
			}
			m_pFnMesh->addPolygon(vertexArray);

			const U32 index=m_pFnMesh->numPolygons()-1;
			spAccessor->set(index,SurfaceAccessorMaya::e_openPoly);
		}
			break;
		case DrawI::e_fan:
		default:
			break;
	}
}

} /* namespace ext */
} /* namespace fe */
