/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __sdl_JoySDL_h__
#define __sdl_JoySDL_h__

#include "window/WindowEvent.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief WindowEvent handler that watches for a poll event and then
			feeds back any accumulated joystick events

	@ingroup sdl
*//***************************************************************************/
class FE_DL_EXPORT JoySDL:
	virtual public JoyI,
	public Initialize<JoySDL>
{
	public:
					JoySDL(void);
virtual				~JoySDL(void);

		void		initialize(void);

					//* As HandlerI
virtual	void		handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void		handle(Record &record);

virtual	ControllerType	getControllerType(I32 a_joystickIndex);

	private:


		void		poll(void);

		hp<SignalerI>				m_hpSignalerI;
		sp<Component>				m_spContextSDL;

		Array<SDL_Joystick*>		m_joysticks;
		Array<ControllerType>		m_controllerTypes;

#if FE_SDL>1
		Array<SDL_GameController*>	m_gameControllers;
#endif

		WindowEvent					m_event;
		WindowEvent					m_joyEvent;
		Record						m_joyRecord;

		I32							m_maxAxes;
		I32							m_maxButtons;

		Array< Array<I32> >			m_pAxisLastState;	//* for debounce
		Array< Array<I32> >			m_pAxisState;
		Array< Array<I32> >			m_pButtonState;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __sdl_JoySDL_h__ */
