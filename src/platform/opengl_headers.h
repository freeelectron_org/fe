/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opengl_headers_h__
#define __opengl_headers_h__

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#include "windows.h"
#include "winuser.h"
#include "gl/glew.h"
#include "gl/GL.h"
#include "gl/glu.h"

namespace fe
{
//* TODO
inline float compute_dpi(void)						{ return float(0); }
inline float determine_multiplication(Real a_dpi)	{ return float(1); }
} // namespace

#elif FE_OS==FE_LINUX
#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>

namespace fe
{
inline float compute_dpi(void)
{
	Display* pDisplayHandle=XOpenDisplay(NULL);
	if(!pDisplayHandle)
	{
		return 0;
	}

	Screen* pScreen=DefaultScreenOfDisplay(pDisplayHandle);
	if(!pScreen)
	{
		XCloseDisplay(pDisplayHandle);
		return 0;
	}

	const int screenWidth=XWidthOfScreen(pScreen);
	const int screenHeight=XHeightOfScreen(pScreen);

	const int screenWidthMM=XWidthMMOfScreen(pScreen);
	const int screenHeightMM=XHeightMMOfScreen(pScreen);

	XCloseDisplay(pDisplayHandle);

	const float screenWidthInches=screenWidthMM/float(25.4);
	const float screenHeightInches=screenHeightMM/float(25.4);

	const float dpiX=screenWidth/screenWidthInches;
	const float dpiY=screenHeight/screenHeightInches;

//	fe_printf("dpi_multiplication"
//			" screen %d %d size %.6G %.6G\n",
//			screenWidth,screenHeight,
//			screenWidthInches,screenHeightInches);
//	fe_printf("  dpi %.6G %.6G\n",dpiX,dpiY);

	return 0.5*(dpiX+dpiY);
}
inline float determine_multiplication(Real a_dpi)
{
	//* TODO tweak
	return fe::maximum(1,int(a_dpi/100));
}
} // namespace


#elif FE_OS==FE_OSX
//#include <AGL/agl.h>
#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>
//X #include <OpenGL/gl.h>
//X #include <OpenGL/glu.h>

namespace fe
{
//* TODO
inline float compute_dpi(void)						{ return float(0); }
inline float determine_multiplication(Real a_dpi)	{ return float(1); }
} // namespace

#else
#error "platform not recognized"
#endif

#endif /* __opengl_headers_h__ */
