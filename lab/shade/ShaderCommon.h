/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shade_ShaderCommon_h__
#define __shade_ShaderCommon_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Common Shader methods

	@ingroup shade
*//***************************************************************************/
class FE_DL_EXPORT ShaderCommon:
	public Catalog,
	virtual public ShaderI
{
	public:

				ShaderCommon(void)											{}
virtual			~ShaderCommon(void)											{}

				//* As ShaderI
virtual	void	update(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shade_ShaderCommon_h__ */
