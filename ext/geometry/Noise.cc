/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <geometry/geometry.pmh>

#include "FastNoise/FastNoise.h"
#include "FastNoise/FastNoise.cpp"

#define FE_PERLIN_SCALE (200.0/3.0)

namespace fe
{
namespace ext
{

Noise::Noise(void)
{
	m_pFastNoise=new FastNoise();
}

Noise::~Noise(void)
{
	delete (FastNoise*)m_pFastNoise;
}

void Noise::setSeed(I32 a_seed)
{
	FastNoise& rFastNoise=*(FastNoise*)m_pFastNoise;
	rFastNoise.SetSeed(a_seed);
}

Real Noise::perlin2d(const Vector2& a_rPosition)
{
	FastNoise& rFastNoise=*(FastNoise*)m_pFastNoise;
	return rFastNoise.GetPerlin(
			FE_PERLIN_SCALE*a_rPosition[0],
			FE_PERLIN_SCALE*a_rPosition[1]);
}

Real Noise::perlin3d(const Vector3& a_rPosition)
{
	FastNoise& rFastNoise=*(FastNoise*)m_pFastNoise;
	return rFastNoise.GetPerlin(
			FE_PERLIN_SCALE*a_rPosition[0],
			FE_PERLIN_SCALE*a_rPosition[1],
			FE_PERLIN_SCALE*a_rPosition[2]);
}

Real Noise::whiteNoise2d(const Vector2& a_rPosition)
{
	FastNoise& rFastNoise=*(FastNoise*)m_pFastNoise;
	return rFastNoise.GetWhiteNoise(
			a_rPosition[0],a_rPosition[1]);
}

Real Noise::whiteNoise3d(const Vector3& a_rPosition)
{
	FastNoise& rFastNoise=*(FastNoise*)m_pFastNoise;
	return rFastNoise.GetWhiteNoise(
			a_rPosition[0],a_rPosition[1],a_rPosition[2]);
}

} /* namespace ext */
} /* namespace fe */