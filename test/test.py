#!/usr/bin/python
###############################################################################
import os
import os.path
import time
import sys
import getopt
import string
import re
import thread

# add the path to WConio:
addpath = os.path.abspath('../bin')
sys.path.append(addpath)
addpath = os.path.abspath('./src')
sys.path.append(addpath)

import forge
import utility

# now get the test lists:
from xsetup import *

def cprint(color, bold, string):
    forge.color_on(bold,color)
    sys.stdout.write(string)
    forge.color_off()

utility.determine_api()

###############################################################################
# globals:
###############################################################################

python_exe = "python"
debugging=0
validating=0
profile=0
optimize=0
help=0
cmdtest=""
suppress_python=0
suppress_standalone=0

LOGFILE = "log.txt"

testcodes = ["PASS","MISS","FAIL","EXCP"]
testcolors = [forge.GREEN, forge.YELLOW, forge.RED, forge.WHITE]

# create a results list, with a counter for each category, and an extra
# counter for 'unknown':
faillist = []
testresults = []
for i in range(len(testcodes)):
    testresults.append(0)
testresults.append(0)

###############################################################################
#determine whether or not we're using the python debug binary:
###############################################################################

try:
    optlist, args = getopt.getopt(sys.argv[1:], "psdVfoht:", ['help','test='])
    # sys.argv[0] is the name of the running program ("test.py" presumably)

    for o,a in optlist:
        if o == "-d":
            debugging=1
            #if(forge.api == "x86_win32"):
            #   python_exe = "python_d"
        if o == "-V":
            debugging=1
            validating=1
        if o == "-f":
            profile=1
        if o == "-o":
            optimize=1
        if o == "-p":
            suppress_standalone=1
        if o == "-s":
            suppress_python=1
        if (o == "-h") or (o == "--help"):
            help=1
        if (o == "-t") or (o == "--test"):
            cmdtest=a

except Exception, e:
    print "\nException occurred based on the arguments!"
    help = 1

###############################################################################
# Display the help test if requested:
###############################################################################

if help == 1:
    print """\nUsage: test.py [option]... [ ARG1 ARG2...]
    -h, --help      display this help text
    -o,             optimize
    -f,             profile
    -d,             use debugging
    -V,             use validation debugging
    -p,             python tests only (by suppressing standalone tests)
    -s,             standalone tests only (by suppressing python tests)
    -t, --test <file>   execute only the specified test.
                <file> must have either a .py or .exe suffix

    """
    sys.exit()


###############################################################################
# If a test is provided as an argument, use only it:
###############################################################################

if cmdtest != "":

    pythontests = []
    standalonetests = []

    # note: each entry in pythontests and standalonetests are a tuple,
    # with each tuple containing 2 strings: the filename of the test,
    # the arguments used with the test

    # determine the type of the specified test:
    if (cmdtest[-3:] == ".py"):
        pythontests.append( (cmdtest, "") )
    elif (cmdtest[-4:] == ".exe"):
        standalonetests.append( (cmdtest, "", "", None) )

###############################################################################
# append the output dir to the $PATH:
###############################################################################

path=os.getenv("PATH")
if(forge.api == "x86_linux"):
    path+=':'
    if(validating == 1):
        addpath = os.path.abspath('../lib/x86_linux_validate')
    elif(debugging == 1):
        addpath = os.path.abspath('../lib/x86_linux_debug')
    elif(profile == 1):
        addpath = os.path.abspath('../lib/x86_linux_profile')
    elif(optimize == 1):
        addpath = os.path.abspath('../lib/x86_linux_optimize')
    else:
        addpath = os.path.abspath('../lib/x86_linux_default')
elif(forge.api == "x86_win32"):
    path+=';'
    if(validating == 1):
        addpath = os.path.abspath('..\\lib\\x86_win32_validate')
    elif(debugging == 1):
        addpath = os.path.abspath('..\\lib\\x86_win32_debug')
    elif(profile == 1):
        addpath = os.path.abspath('..\\lib\\x86_win32_profile')
    elif(optimize == 1):
        addpath = os.path.abspath('..\\lib\\x86_win32_optimize')
    else:
        addpath = os.path.abspath('..\\lib\\x86_win32_default')
path+=addpath
os.environ["PATH"]=path
os.putenv("PYTHONPATH",addpath)

###############################################################################
# prepare the log file:
###############################################################################

log = open(LOGFILE, "a")

log.write("\n==============================================================\n")
log.write("=   TEST RUN AT %s\n" % (time.ctime(time.time())))
log.write("==============================================================\n")

###############################################################################
# define the test function:
###############################################################################

re_exception_thrown = re.compile(r'^.*fatal(.*)$')


def runtest(testprogram,bCheckpath):
    test = Test(testprogram,testprogram,bCheckpath)
    test.start()
    test.wait()
    test.time_start()
    test.time_wait()
    test.valgrind_start()
    test.valgrind_wait()
    test.callgrind_start()
    test.callgrind_wait()
    return test.process()

def print_time(time):
    color = forge.GREEN
    if time<0.1:
        cprint(color, 0, "  .  ")
        return
    if time>5:
        color = forge.RED
    elif time>1:
        color = forge.YELLOW
    cprint(color, 1, " %4.1f" % time)

def print_kilo(color,count,digits):
    prefix = " kMGT"
    index = 0
    limit = pow(10, digits) - 1
    while count>limit:
        index = index + 1
        count *= 0.001
    val_fmt = "%0" + str(digits) + "d" + prefix[index]
    cprint(color, 1, val_fmt % count)

def print_percent(percent,low,medium,high):
    color = forge.GREEN
    if percent<low:
        cprint(color, 0, "  .  ")
        return
    if percent>high:
        color = forge.RED
    elif percent>medium:
        color = forge.YELLOW
    cprint(color, 1, " %3.1f" % percent)

class Test:
    def __init__(self, testprogram, valgrindprogram, bCheckpath):
        self.testprogram = testprogram
        self.bCheckpath = bCheckpath
        self.output = None
        self.valgrindprogram = valgrindprogram

    def start(self):
        self.output = os.popen(self.testprogram, "r", -1)
        return self.output

    def wait(self):
        # consume all of the test output:
        self.log_output = ""
        self.exception_thrown = 0
        self.err_msg = ''
        line = self.output.readline()
        while line:
            m = re_exception_thrown.match(line)
            if m != None:
                self.err_msg = string.strip(str(m.group(1)))
                self.exception_thrown = 1
            #log.write("%s" % (line))
            self.log_output += line
            try:
                line = self.output.readline()
            except Exception, e:
                print e

        # close the pipe
        # return value is the signal (lowbyte) + exit value (high byte)
        self.ec = self.output.close()

    def process(self):
        testprogram = self.testprogram
        bCheckpath = self.bCheckpath
        output = self.output
        log_output = self.log_output
        exception_thrown = self.exception_thrown
        err_msg = self.err_msg
        ec = self.ec
        log.write("\n--------- TEST OUTPUT FOR %s ---------\n" % (testprogram))
        log.write(log_output)
        signal=0
        exitval=0
        if ec != None:
            if forge.api == "x86_win32":
                exitval=ec
            else:
                signal= (int(str(ec))&255)
                exitval= ((int(str(ec))>>8)&255)
    #   sys.stdout.write(str(ec) + " " + str(signal) + " " + str(exitval) + "\n")

        retval = "UNSPECIFIED"

        fullpath = addpath + '/' + string.split(testprogram)[0]

        # print result based on exit status:
        if exception_thrown == 1:
                retval = "EXCP"
                color = forge.WHITE
                ec = 1
        elif ec == None:
                retval = "PASS"
                color = forge.GREEN
        elif bCheckpath and not os.path.exists(fullpath):
            retval = "MISS"
            color = forge.YELLOW
        else:
            retval = "FAIL"
            color = forge.RED

            # why does this corespond with exit return value?
    #       err_msg = os.strerror(ec) + ' (' + str(ec) + ')'

            if forge.api == "x86_linux":
                if signal == 6: # abort signal
                    err_msg = ' abort'
                if signal == 11:
                    err_msg = ' segmentation fault'

            faillist.append(testprogram)

        width = 31
        if len(testprogram) <= width:
            pad = ( width - (len(testprogram)) ) * '.'
            sys.stdout.write(testprogram + pad + '[')
        else:
            sys.stdout.write(testprogram + "\n" + width * '.' + '[')

        cprint(color, 1, retval)
        sys.stdout.write("]")
        sys.stdout.write(" " + ("%1d" % signal) + ' ' + ("%1d " % exitval))
        if err_msg != '':
            sys.stdout.write(err_msg)
        sys.stdout.flush()

        if self.ec == None and profile == 1 and forge.api == "x86_linux":
            print_time(self.realtime)
            print_time(self.usertime)
            print_time(self.systime)
            print_percent(self.I1,  0.1, 0.7, 1.0)
            print_percent(self.L2i, 0.1, 0.5, 1.0)
            print_percent(self.D1,  0.1, 5.5, 6.0)
            print_percent(self.L2d, 0.1, 2.0, 3.0)
            print_percent(self.L2,  0.1, 0.7, 1.4)

        if self.ec == None and debugging == 1 and forge.api == "x86_linux":
            val_rv = "UNSPECIFIED"

            fullpath = addpath + '/' + string.split(self.valgrind)[0]

            # print result based on exit status:
            if self.val_ec == None:
                if self.errors > 0:
                    val_rv = "FAIL"
                    color = forge.RED
                elif self.leak > 0 or self.indirect > 0 or self.possible > 0:
                    val_rv = "LEAK"
                    color = forge.YELLOW
                else:
                    val_rv = "PASS"
                    color = forge.GREEN
            else:
                val_rv = "FAIL"
                color = forge.RED

            sys.stdout.write('[')
            cprint(color, 1, val_rv)
            sys.stdout.write("] ")

            if self.errors > 0:
                color = forge.RED
            else:
                color = forge.GREEN

            cprint(color, 1, "%01d" % self.errors)
            sys.stdout.write(" ")

            if self.leak > 0:
                color = forge.RED
            else:
                color = forge.GREEN

            print_kilo(color,self.leak,4)
            sys.stdout.write(" ")

            if self.indirect > 0:
                color = forge.YELLOW
            else:
                color = forge.GREEN

            print_kilo(color,self.indirect,4)
            sys.stdout.write(" ")

            if self.possible > 0:
                color = forge.YELLOW
            else:
                color = forge.GREEN

            print_kilo(color,self.possible,4)
            sys.stdout.write(" ")

            if self.reachable > 1000:
                color = forge.MAGENTA
            elif self.reachable > 0:
                color = forge.BLUE
            else:
                color = forge.GREEN

            print_kilo(color,self.reachable,4)
            sys.stdout.write(" ")

            if self.suppressed > 9999:
                color = forge.MAGENTA
            elif self.suppressed > 0:
                color = forge.BLUE
            else:
                color = forge.GREEN

            print_kilo(color,self.suppressed,4)

            sys.stdout.write("\n")
            if test in notes:
                sys.stdout.write("                                             ");
                cprint(forge.BLUE, 0,notes[test])
                sys.stdout.write("\n")
        else:
            sys.stdout.write("\n")

        return retval

    def time_start(self):
        if self.ec == None and profile == 1 and forge.api == "x86_linux":
            self.timeprogram = '/usr/bin/time -f "TIME: %e %U %S" ' + self.testprogram
            (input,self.output) = os.popen4(self.timeprogram, "r", -1)
            return self.output

    def time_wait(self):
        if self.ec == None and profile == 1 and forge.api == "x86_linux":
            exit
            line = self.output.readline()
            time_expr = re.compile(r'^TIME: ([0-9\.]+) ([0-9\.]+) ([0-9\.]+)$')
            self.realtime = 0
            self.usertime = 0
            self.systime = 0
            while line:
                line = self.output.readline()

                m = time_expr.match(line)
                if m != None:
                    self.realtime = float(m.group(1))
                    self.usertime = float(m.group(2))
                    self.systime = float(m.group(3))

            self.time_ec = self.output.close()

    def valgrind_start(self):
        if self.ec == None and debugging == 1 and forge.api == "x86_linux":
            #self.valgrind = 'valgrind --trace-children=yes --suppressions=test.supp --leak-check=yes --logfile-fd=1 --logsocket=127.0.0.1:5555 ' + self.valgrindprogram
            self.valgrind = 'valgrind --trace-children=yes --suppressions=test.supp --leak-check=yes --logfile-fd=1 --num-callers=10 ' + self.valgrindprogram
            self.output = os.popen(self.valgrind, "r", -1)
            return self.output

    def valgrind_wait(self):
        if self.ec == None and debugging == 1 and forge.api == "x86_linux":
            line = self.output.readline()
            error_expr = re.compile(r'^.*ERROR SUMMARY:\s+(\d+).*$')
            lost_expr = re.compile(r'^.*definitely lost:\s+(\d+).*$')
            indirect_expr = re.compile(r'^.*indirectly lost:\s+(\d+).*$')
            possible_expr = re.compile(r'^.*possibly lost:\s+(\d+).*$')
            reachable_expr = re.compile(r'^.*still reachable:\s+(\d+).*$')
            supp_expr = re.compile(r'^.*suppressed:\s+(\d+).*$')
            self.errors = 0
            self.leak = 0
            self.indirect = 0
            self.possible = 0
            self.reachable = 0
            self.suppressed = 0
            while line:
                line = self.output.readline()
#               sys.stdout.write(line)

                m = error_expr.match(line)
                if m != None:
                    self.errors += int(m.group(1))

                m = lost_expr.match(line)
                if m != None:
                    self.leak += int(m.group(1))

                m = indirect_expr.match(line)
                if m != None:
                    self.indirect += int(m.group(1))

                m = possible_expr.match(line)
                if m != None:
                    self.possible += int(m.group(1))

                m = reachable_expr.match(line)
                if m != None:
                    self.reachable += int(m.group(1))

                m = supp_expr.match(line)
                if m != None:
                    self.suppressed += int(m.group(1))

            self.val_ec = self.output.close()

    def callgrind_start(self):
        if self.ec == None and profile == 1 and forge.api == "x86_linux":
            self.valgrind = 'callgrind --simulate-cache=yes ' + self.valgrindprogram + ' 2>&1'
            self.output = os.popen(self.valgrind, "r", -1)
            return self.output

    def callgrind_wait(self):
        if self.ec == None and profile == 1 and forge.api == "x86_linux":
            line = self.output.readline()
            I1_expr = re.compile(r'^.*I1  miss rate:\s+([0-9\.]+).*$')
            L2i_expr = re.compile(r'^.*L2i miss rate:\s+([0-9\.]+).*$')
            D1_expr = re.compile(r'^.*D1  miss rate:\s+([0-9\.]+).*$')
            L2d_expr = re.compile(r'^.*L2d miss rate:\s+([0-9\.]+).*$')
            L2_expr = re.compile(r'^.*L2 miss rate:\s+([0-9\.]+).*$')
            self.I1 = 0
            self.L2i = 0
            self.D1 = 0
            self.L2d = 0
            self.L2 = 0
            while line:
                line = self.output.readline()
#               sys.stdout.write(line)

                m = I1_expr.match(line)
                if m != None:
                    self.I1 += float(m.group(1))

                m = L2i_expr.match(line)
                if m != None:
                    self.L2i += float(m.group(1))

                m = D1_expr.match(line)
                if m != None:
                    self.D1 += float(m.group(1))

                m = L2d_expr.match(line)
                if m != None:
                    self.L2d += float(m.group(1))

                m = L2_expr.match(line)
                if m != None:
                    self.L2 += float(m.group(1))

            self.val_ec = self.output.close()


###############################################################################
# run all of the python tests:
###############################################################################

cprint(forge.CYAN, 1, "Python Tests:                  ")
cprint(forge.BLUE, 0,"norm  sg ex")
if forge.api == "x86_linux" and debugging == 1:
    cprint(forge.BLUE, 0," grnd er leak  indir poss  reach supp")
cprint(forge.BLUE, 0,"\n")

if not suppress_python:
    for (test, args) in pythontests:

        if args == '':
            pad = ''
        else:
            pad = ' '

        result = runtest(python_exe + ' ' + test + ' ' + args + pad, 0)

        for i in range(len(testcodes)):
            if result == testcodes[i]:
                testresults[i] = testresults[i] + 1
                break
        else:
            testresults[len(testcodes) + 1] = testresults[len(testcodes) + 1] + 1

###############################################################################
# run all of the discrete executable tests:
###############################################################################

cprint(forge.CYAN, 1,"Executable Tests:              ")
cprint(forge.BLUE, 0,"norm  sg ex")
if forge.api == "x86_linux" and profile == 1:
    cprint(forge.BLUE, 0," real user sys   I1  L2i D1  L2d L2")
if forge.api == "x86_linux" and debugging == 1:
    cprint(forge.BLUE, 0," grnd er leak  indir poss  reach supp")
cprint(forge.BLUE, 0,"\n")

if not suppress_standalone:
    for (test, args, valgrind_args, nest) in standalonetests:
        continue
        if args == '':
            pad = ''
        else:
            pad = ' '

        result = runtest( test + ' ' + args + pad, 1)

        for i in range(len(testcodes)):
            if result == testcodes[i]:
                testresults[i] = testresults[i] + 1
                break
        else:
            testresults[len(testcodes) + 1] = testresults[len(testcodes) + 1] + 1

def recursetest(test, args, valgrind_args, nest):
    if args == '':
        pad = ''
    else:
        pad = ' '
    if valgrind_args == None:
        valgrind_args = args
    t = Test(test + ' ' + args + pad, test + ' ' + valgrind_args + pad, 1)
    tests = [ t ]
    t.start()
    t.child = None
    if nest:
        (c, ts) = recursetest(nest[0], nest[1], nest[2], nest[3])
        t.child = c
        tests += ts
    t.wait()
    return (t, tests)

def time_pass(t):
    t.time_start()
    if t.child:
        time_pass(t.child)
    t.time_wait()

def valgrind_pass(t):
    t.valgrind_start()
    if t.child:
        valgrind_pass(t.child)
    t.valgrind_wait()

def callgrind_pass(t):
    t.callgrind_start()
    if t.child:
        callgrind_pass(t.child)
    t.callgrind_wait()

if not suppress_standalone:
    for (test, args, valgrind_args, nest) in standalonetests:
        (c, tests) = recursetest(test, args, valgrind_args, nest)
        time_pass(c)
        valgrind_pass(c)
        callgrind_pass(c)
        for t in tests:
            result = t.process()
            for i in range(len(testcodes)):
                if result == testcodes[i]:
                    testresults[i] = testresults[i] + 1
                    break
            else:
                testresults[len(testcodes) + 1] = testresults[len(testcodes) + 1] + 1

###############################################################################
# we're done:
###############################################################################

cprint(forge.CYAN, 1, "Summary:\n")

numtests= 0

for i in range(len(testresults)):
    numtests = numtests + testresults[i]

for i in range(len(testcodes)):

    if numtests != 0:
        percent = 100*testresults[i]/numtests
    else:
        percent = 0

    print("%s:   \t" % (testcodes[i])),

    if testresults[i] == 0:
        sys.stdout.write(str(testresults[i]))
    else:
        cprint(testcolors[i], 1, str(testresults[i]))

    print(" (%d%s)" % (percent, '%'))

cprint(forge.CYAN, 1, "Platform: ")
cprint(forge.CYAN, 0, forge.api)
if debugging:
    cprint(forge.CYAN, 0, " debug")
cprint(forge.CYAN, 0, "\n")

cprint(forge.CYAN, 0, "%s\n" % (time.ctime(time.time())))
cprint(forge.CYAN, 1, "checkin one liner:\n")
cprint(forge.WHITE, 0, forge.api)
for i in range(len(testcodes)):
    cprint(forge.WHITE, 0, " %s:%d" % (testcodes[i], testresults[i]))
cprint(forge.WHITE, 0, " failed:")
for f in faillist:
    f = string.split(f)[0]
    cprint(forge.WHITE, 0, " %s" % (f))

cprint(forge.WHITE, 0, "\n")

log.close()

###############################################################################
###############################################################################

