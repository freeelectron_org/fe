/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_HBO_OUTPUT_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void HobbleOp::initialize(void)
{
	//* Restraints
	catalog<Real>("Envelope")=1.0;
	catalog<Real>("Envelope","min")= -100.0;
	catalog<Real>("Envelope","low")=0.0;
	catalog<Real>("Envelope","high")=1.0;
	catalog<Real>("Envelope","max")=100.0;
	catalog<String>("Envelope","page")="Restraints";
	catalog<String>("Envelope","hint")="Scale the overall effect.";

	catalog<bool>("TranslateLock")=true;
	catalog<String>("TranslateLock","label")="Translation Lock";
	catalog<String>("TranslateLock","page")="Restraints";
	catalog<bool>("TranslateLock","joined")=true;
	catalog<String>("TranslateLock","hint")="Prevent input from shifting.";

	catalog<Real>("TranslateEnv")=1.0;
	catalog<String>("TranslateEnv","label")="Translate";
	catalog<Real>("TranslateEnv","min")= -100.0;
	catalog<Real>("TranslateEnv","low")=0.0;
	catalog<Real>("TranslateEnv","high")=1.0;
	catalog<Real>("TranslateEnv","max")=100.0;
	catalog<String>("TranslateEnv","enabler")="TranslateLock";
	catalog<String>("TranslateEnv","page")="Restraints";
	catalog<String>("TranslateEnv","hint")="Scale just the translation.";

	catalog<Real>("TranslateEnvX")=1.0;
	catalog<String>("TranslateEnvX","label")="in X";
	catalog<Real>("TranslateEnvX","min")= -100.0;
	catalog<Real>("TranslateEnvX","low")=0.0;
	catalog<Real>("TranslateEnvX","high")=1.0;
	catalog<Real>("TranslateEnvX","max")=100.0;
	catalog<String>("TranslateEnvX","enabler")="TranslateLock";
	catalog<String>("TranslateEnvX","page")="Restraints";
	catalog<String>("TranslateEnvX","hint")=
			"Additionally scale the X translation.";

	catalog<Real>("TranslateEnvY")=1.0;
	catalog<String>("TranslateEnvY","label")="in Y";
	catalog<Real>("TranslateEnvY","min")= -100.0;
	catalog<Real>("TranslateEnvY","low")=0.0;
	catalog<Real>("TranslateEnvY","high")=1.0;
	catalog<Real>("TranslateEnvY","max")=100.0;
	catalog<String>("TranslateEnvY","enabler")="TranslateLock";
	catalog<String>("TranslateEnvY","page")="Restraints";
	catalog<String>("TranslateEnvY","hint")=
			"Additionally scale the Y translation.";

	catalog<Real>("TranslateEnvZ")=1.0;
	catalog<String>("TranslateEnvZ","label")="in Z";
	catalog<Real>("TranslateEnvZ","min")= -100.0;
	catalog<Real>("TranslateEnvZ","low")=0.0;
	catalog<Real>("TranslateEnvZ","high")=1.0;
	catalog<Real>("TranslateEnvZ","max")=100.0;
	catalog<String>("TranslateEnvZ","enabler")="TranslateLock";
	catalog<String>("TranslateEnvZ","page")="Restraints";
	catalog<String>("TranslateEnvZ","hint")=
			"Additionally scale the Z translation.";

	catalog<bool>("RotateLock")=true;
	catalog<String>("RotateLock","label")="Rotation Lock";
	catalog<String>("RotateLock","page")="Restraints";
	catalog<bool>("RotateLock","joined")=true;
	catalog<String>("RotateLock","hint")="Prevent input from spinning.";

	catalog<Real>("RotateEnv")=1.0;
	catalog<String>("RotateEnv","label")="Rotate";
	catalog<Real>("RotateEnv","min")= -100.0;
	catalog<Real>("RotateEnv","low")=0.0;
	catalog<Real>("RotateEnv","high")=1.0;
	catalog<Real>("RotateEnv","max")=100.0;
	catalog<String>("RotateEnv","enabler")="RotateLock";
	catalog<String>("RotateEnv","page")="Restraints";
	catalog<String>("RotateEnv","hint")="Scale just the rotation.";

	//* Tracking
	catalog<bool>("GroupedX")=false;
	catalog<String>("GroupedX","label")="Use Forward Group";
	catalog<bool>("GroupedX","joined")=true;
	catalog<String>("GroupedX","page")="Tracking";
	catalog<String>("GroupedX","hint")=
			"Use points on reference surface to indicate forward tipping.";

	catalog<String>("GroupX");
	catalog<String>("GroupX","label")="Forward Point Group";
	catalog<String>("GroupX","suggest")="pointGroups";
	catalog<String>("GroupX","enabler")="GroupedX";
	catalog<String>("GroupX","page")="Tracking";
	catalog<String>("GroupX","hint")=
			"Points on reference surface that indicate forward tipping"
			" relative to the center.";

	catalog<bool>("GroupedZ")=false;
	catalog<String>("GroupedZ","label")="Use Side Group";
	catalog<bool>("GroupedZ","joined")=true;
	catalog<String>("GroupedZ","page")="Tracking";
	catalog<String>("GroupedZ","hint")=
			"Use points on reference surface to indicate sideward tipping.";

	catalog<String>("GroupZ");
	catalog<String>("GroupZ","label")="Side Point Group";
	catalog<String>("GroupZ","suggest")="pointGroups";
	catalog<String>("GroupZ","enabler")="GroupedZ";
	catalog<String>("GroupZ","page")="Tracking";
	catalog<String>("GroupZ","hint")=
			"Points on reference surface that indicate sideward tipping"
			" relative to the center.";

	catalog<bool>("GroupedT")=false;
	catalog<String>("GroupedT","label")="Use Center Group";
	catalog<bool>("GroupedT","joined")=true;
	catalog<String>("GroupedT","page")="Tracking";
	catalog<String>("GroupedT","hint")=
			"Use points on reference surface to calculate center.";

	catalog<String>("GroupT");
	catalog<String>("GroupT","label")="Center Point Group";
	catalog<String>("GroupT","suggest")="pointGroups";
	catalog<String>("GroupT","enabler")="GroupedT";
	catalog<String>("GroupT","page")="Tracking";
	catalog<String>("GroupT","hint")=
			"Points on reference surface whose average indicates"
			" the central pivot";

	catalog<bool>("ForceArms")=false;
	catalog<String>("ForceArms","label")="Force Indexes";
	catalog<String>("ForceArms","page")="Tracking";
	catalog<bool>("ForceArms","joined")=true;
	catalog<String>("ForceArms","hint")=
			"Explicitly choose rotation control points.";

	catalog<I32>("ArmIndexX")= -1;
	catalog<I32>("ArmIndexX","min")= -1;
	catalog<I32>("ArmIndexX","max")=INT_MAX;
	catalog<String>("ArmIndexX","label")="Index To Side";
	catalog<String>("ArmIndexX","page")="Tracking";
	catalog<bool>("ArmIndexX","joined")=true;
	catalog<String>("ArmIndexX","enabler")="ForceArms";
	catalog<String>("ArmIndexX","hint")=
		"Explicit point index towards one side of object,"
		" presumably well behaved (-1 to automatically choose).";

	catalog<I32>("ArmIndexZ")= -1;
	catalog<I32>("ArmIndexZ","min")= -1;
	catalog<I32>("ArmIndexZ","max")=INT_MAX;
	catalog<String>("ArmIndexZ","label")="Index To Front";
	catalog<String>("ArmIndexZ","page")="Tracking";
	catalog<String>("ArmIndexZ","enabler")="ForceArms";
	catalog<String>("ArmIndexZ","hint")=
		"Explicit point index towards front of object,"
		" presumably well behaved (-1 to automatically choose).";

	catalog<bool>("Force Source")=false;
	catalog<String>("Force Source","page")="Tracking";
	catalog<bool>("Force Source","joined")=true;
	catalog<String>("Force Source","hint")="Explicitly choose input center.";

	catalog<SpatialVector>("Source")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("Source","page")="Tracking";
	catalog<String>("Source","enabler")="Force Source";
	catalog<String>("Source","hint")="Explicit center of input.";

	catalog<bool>("Force Destination")=false;
	catalog<String>("Force Destination","page")="Tracking";
	catalog<bool>("Force Destination","joined")=true;
	catalog<String>("Force Destination","hint")="Explicitly place result.";

	catalog<SpatialVector>("Destination")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("Destination","page")="Tracking";
	catalog<String>("Destination","enabler")="Force Destination";
	catalog<String>("Destination","hint")="Explicit placement of result.";

	catalog<bool>("refJoint")=false;
	catalog<String>("refJoint","label")="Reference Joint";
	catalog<String>("refJoint","page")="Tracking";
	catalog<bool>("refJoint","joined")=true;

	catalog<String>("refJointName")="track";
	catalog<String>("refJointName","label")="Name";
	catalog<String>("refJointName","page")="Tracking";
	catalog<String>("refJointName","enabler")="refJoint";

	catalog<bool>("inputJoint")=false;
	catalog<String>("inputJoint","label")="Input Joint";
	catalog<String>("inputJoint","page")="Tracking";
	catalog<bool>("inputJoint","joined")=true;

	catalog<String>("inputJointName")="track";
	catalog<String>("inputJointName","label")="Name";
	catalog<String>("inputJointName","page")="Tracking";
	catalog<String>("inputJointName","enabler")="inputJoint";

	catalog<bool>("updateLocator")=false;
	catalog<String>("updateLocator","label")="Update Locator";
	catalog<String>("updateLocator","page")="Tracking";
	catalog<bool>("updateLocator","joined")=true;

	catalog<String>("locatorPreface")="loc";
	catalog<String>("locatorPreface","label")="Preface";
	catalog<String>("locatorPreface","page")="Tracking";
	catalog<String>("locatorPreface","enabler")="updateLocator";
	catalog<String>("locatorPreface","hint")=
			"Text at the beginning of locator attribute.";

	//* Output
	catalog<bool>("outputJoint")=false;
	catalog<String>("outputJoint","label")="Output Joint";
	catalog<String>("outputJoint","page")="Output";
	catalog<bool>("outputJoint","joined")=true;

	catalog<String>("outputJointName")="track";
	catalog<String>("outputJointName","label")="Name";
	catalog<String>("outputJointName","page")="Output";
	catalog<String>("outputJointName","enabler")="outputJoint";

	catalog<SpatialVector>("Input Center");
	catalog<String>("Input Center","IO")="output";
	catalog<String>("Input Center","page")="Output";
	catalog<String>("Input Center","hint")=
			"Approximate center of input surface.";

	catalog<SpatialVector>("InputE");
	catalog<String>("InputE","label")="Input Euler XYZ";
	catalog<String>("InputE","page")="Output";
	catalog<String>("InputE","IO")="output";

	catalog<SpatialVector>("Reference Center");
	catalog<String>("Reference Center","IO")="output";
	catalog<String>("Reference Center","page")="Output";
	catalog<String>("Reference Center","hint")=
			"Approximate center of reference surface.";

	catalog<SpatialVector>("ReferenceE");
	catalog<String>("ReferenceE","label")="Reference Euler XYZ";
	catalog<String>("ReferenceE","page")="Output";
	catalog<String>("ReferenceE","IO")="output";

	catalog<SpatialVector>("ConversionX");
	catalog<String>("ConversionX","label")="Rotation X";
	catalog<String>("ConversionX","page")="Output";
	catalog<String>("ConversionX","IO")="output";

	catalog<SpatialVector>("ConversionY");
	catalog<String>("ConversionY","label")="Rotation Y";
	catalog<String>("ConversionY","page")="Output";
	catalog<String>("ConversionY","IO")="output";

	catalog<SpatialVector>("ConversionZ");
	catalog<String>("ConversionZ","label")="Rotation Z";
	catalog<String>("ConversionZ","page")="Output";
	catalog<String>("ConversionZ","IO")="output";

	catalog<SpatialVector>("ConversionT");
	catalog<String>("ConversionT","label")="Translation";
	catalog<String>("ConversionT","page")="Output";
	catalog<String>("ConversionT","IO")="output";

	catalog<SpatialVector>("ConversionE");
	catalog<String>("ConversionE","label")="Euler XYZ";
	catalog<String>("ConversionE","page")="Output";
	catalog<String>("ConversionE","IO")="output";

	catalog<SpatialVector>("InverseX");
	catalog<String>("InverseX","label")="Inv Rotation X";
	catalog<String>("InverseX","page")="Output";
	catalog<String>("InverseX","IO")="output";

	catalog<SpatialVector>("InverseY");
	catalog<String>("InverseY","label")="Inv Rotation Y";
	catalog<String>("InverseY","page")="Output";
	catalog<String>("InverseY","IO")="output";

	catalog<SpatialVector>("InverseZ");
	catalog<String>("InverseZ","label")="Inv Rotation Z";
	catalog<String>("InverseZ","page")="Output";
	catalog<String>("InverseZ","IO")="output";

	catalog<SpatialVector>("InverseT");
	catalog<String>("InverseT","label")="Inv Translation";
	catalog<String>("InverseT","page")="Output";
	catalog<String>("InverseT","IO")="output";

	catalog<SpatialVector>("InverseE");
	catalog<String>("InverseE","label")="Inv Euler XYZ";
	catalog<String>("InverseE","page")="Output";
	catalog<String>("InverseE","IO")="output";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");
	catalog<bool>("Reference Surface","optional")=true;

	catalog< sp<Component> >("Input Joints");
	catalog<bool>("Input Joints","optional")=true;

	//* no auto-copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void HobbleOp::handle(Record& a_rSignal)
{
	String& rSummary=catalog<String>("summary");
	rSummary="OFF";

	const Real envelope=catalog<Real>("Envelope");
	if(envelope<=0.0)
	{
		return;
	}

	const String locatorPreface=catalog<bool>("updateLocator")?
			catalog<String>("locatorPreface"): "";

	SpatialVector& rInputCenter=catalog<SpatialVector>("Input Center");
	SpatialVector& rInputE=catalog<SpatialVector>("InputE");

	SpatialVector& rReferenceCenter=catalog<SpatialVector>("Reference Center");
	SpatialVector& rReferenceE=catalog<SpatialVector>("ReferenceE");

	SpatialVector& rConversionX=catalog<SpatialVector>("ConversionX");
	SpatialVector& rConversionY=catalog<SpatialVector>("ConversionY");
	SpatialVector& rConversionZ=catalog<SpatialVector>("ConversionZ");
	SpatialVector& rConversionT=catalog<SpatialVector>("ConversionT");
	SpatialVector& rConversionE=catalog<SpatialVector>("ConversionE");

	SpatialVector& rInverseX=catalog<SpatialVector>("InverseX");
	SpatialVector& rInverseY=catalog<SpatialVector>("InverseY");
	SpatialVector& rInverseZ=catalog<SpatialVector>("InverseZ");
	SpatialVector& rInverseT=catalog<SpatialVector>("InverseT");
	SpatialVector& rInverseE=catalog<SpatialVector>("InverseE");

	const String groupX=catalog<String>("GroupX");
	const String groupZ=catalog<String>("GroupZ");
	const String groupT=catalog<String>("GroupT");
	const BWORD groupedX=(catalog<bool>("GroupedX") && !groupX.empty());
	const BWORD groupedZ=(catalog<bool>("GroupedZ") && !groupZ.empty());
	const BWORD groupedT=(catalog<bool>("GroupedT") && !groupT.empty());

	const bool forceArms=catalog<bool>("ForceArms");
	const I32 armIndexX=catalog<I32>("ArmIndexX");
	const I32 armIndexZ=catalog<I32>("ArmIndexZ");
	const bool forceSource=catalog<bool>("Force Source");
	const bool forceDestination=catalog<bool>("Force Destination");
	const SpatialVector source=catalog<SpatialVector>("Source");
	const SpatialVector destination=catalog<SpatialVector>("Destination");
	const String refJointName=catalog<String>("refJointName");
	const BWORD refJoint=
			(catalog<bool>("refJoint") && !refJointName.empty());
	const String inputJointName=catalog<String>("inputJointName");
	const BWORD inputJoint=
			(catalog<bool>("inputJoint") && !inputJointName.empty());
	const String outputJointName=catalog<String>("outputJointName");
	const BWORD outputJoint=
			(catalog<bool>("outputJoint") && !outputJointName.empty());

	set(rInputCenter);
	set(rReferenceCenter);
	set(rConversionX,1.0,0.0,0.0);
	set(rConversionY,0.0,1.0,0.0);
	set(rConversionZ,0.0,0.0,1.0);
	set(rConversionT);

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	if(!outputJoint)
	{
		spOutputAccessible->copy(spInputAccessible);
	}

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	const I32 inputCount=spInputPoint->count();
	if(!inputCount)
	{
		catalog<String>("warning")="no points in input surface";
		return;
	}

	sp<SurfaceAccessibleI> spReferenceAccessible;
	access(spReferenceAccessible,"Reference Surface",e_quiet);

	sp<SurfaceAccessorI> spReferencePoint;
	if(spReferenceAccessible.isValid())
	{
		access(spReferencePoint,spReferenceAccessible,
				e_point,e_position,e_quiet);
	}

	I32 referenceCount=0;
	if(spReferencePoint.isValid())
	{
		referenceCount=spReferencePoint->count();
		if(!referenceCount)
		{
			catalog<String>("warning")+=
					"reference surface has no points; ";
			spReferencePoint=NULL;
		}
		else if(referenceCount!=inputCount)
		{
			catalog<String>("warning")+=
					"reference point count different than input point count; ";
		}
	}

	SpatialTransform referenceJointTransform;
	SpatialTransform inputJointTransform;

	setIdentity(referenceJointTransform);
	setIdentity(inputJointTransform);

	BWORD refJointSet=FALSE;
	BWORD inputJointSet=FALSE;

	if(inputJoint && refJoint)
	{
		sp<SurfaceAccessibleI> spJointAccessible;
		sp<SurfaceAccessorI> spJointName;

		if(access(spJointAccessible,"Input Joints") &&
				access(spJointName,spJointAccessible,e_primitive,"name"))
		{
			const U32 jointCount=spJointName->count();

			for(U32 pass=0;pass<2;pass++)
			{
				if(pass? refJoint: inputJoint)
				{
					const String pickName=pass? refJointName: inputJointName;

					U32 jointIndex;
					for(jointIndex=0;jointIndex<jointCount;jointIndex++)
					{
						if(spJointName->string(jointIndex)==pickName)
						{
							break;
						}
					}
					if(jointIndex==jointCount)
					{
						continue;
					}

					const String prefix=pass? "ref": "anim";
					SpatialTransform& rTransform=
							pass? referenceJointTransform: inputJointTransform;

					sp<SurfaceAccessorI> spInputX;
					sp<SurfaceAccessorI> spInputY;
					sp<SurfaceAccessorI> spInputZ;
					sp<SurfaceAccessorI> spInputT;
					sp<SurfaceAccessorI> spInputS;

					access(spInputX,spJointAccessible,e_primitive,prefix+"X");
					access(spInputY,spJointAccessible,e_primitive,prefix+"Y");
					access(spInputZ,spJointAccessible,e_primitive,prefix+"Z");
					access(spInputT,spJointAccessible,e_primitive,prefix+"T");

					access(spInputS,spJointAccessible,
							e_primitive,prefix+"S",e_quiet);

					if(spInputX.isValid() && spInputY.isValid() &&
							spInputZ.isValid() && spInputT.isValid())
					{
						rTransform.column(0)=
								spInputX->spatialVector(jointIndex);
						rTransform.column(1)=
								spInputY->spatialVector(jointIndex);
						rTransform.column(2)=
								spInputZ->spatialVector(jointIndex);
						rTransform.column(3)=
								spInputT->spatialVector(jointIndex);

						if(spInputS.isValid())
						{
							const SpatialVector localScale=
									spInputS->spatialVector(jointIndex);

							scale(rTransform,localScale);
						}

						BWORD& rJointSet=pass? refJointSet: inputJointSet;
						rJointSet=TRUE;

					}
				}
			}
		}
	}

	const BWORD translationLock=catalog<bool>("TranslateLock");
	Real translateEnv=translationLock? catalog<Real>("TranslateEnv"): Real(0);
	Real translateEnvX=translationLock? catalog<Real>("TranslateEnvX"): Real(0);
	Real translateEnvY=translationLock? catalog<Real>("TranslateEnvY"): Real(0);
	Real translateEnvZ=translationLock? catalog<Real>("TranslateEnvZ"): Real(0);

	const Real transMax=(translateEnvX>translateEnvY)?
			(translateEnvX>translateEnvZ? translateEnvX: translateEnvZ):
			(translateEnvY>translateEnvZ? translateEnvY: translateEnvZ);
	if(fabs(transMax-1.0)<1e-6)
	{
		translateEnv*=transMax;
		if(transMax>1e-6)
		{
			translateEnvX/=transMax;
			translateEnvY/=transMax;
			translateEnvZ/=transMax;
		}
	}

	const BWORD scaleAxes=(translateEnvX!=1.0 ||
			translateEnvY!=1.0 || translateEnvZ!=1.0);

	BWORD rotationLock=(spReferencePoint.isValid() || refJointSet) &&
			catalog<bool>("RotateLock");
	const Real rotateEnv=rotationLock? catalog<Real>("RotateEnv"): Real(0);

	if(!translationLock && !rotationLock)
	{
		return;
	}
	if(translateEnv==Real(0) && rotateEnv==Real(0))
	{
		return;
	}

	rSummary="";

	if(translateEnv!=Real(0))
	{
		rSummary+=String(rSummary.empty()? "": " ")+"move";
		if(scaleAxes)
		if(translateEnvX==Real(0) || translateEnvY==Real(0) ||
				translateEnvZ==Real(0))
		{
			rSummary+=" ";
			if(translateEnv*translateEnvX!=Real(0))
			{
				rSummary+="X";
			}
			if(translateEnv*translateEnvY!=Real(0))
			{
				rSummary+="Y";
			}
			if(translateEnv*translateEnvZ!=Real(0))
			{
				rSummary+="Z";
			}
		}
	}
	if(rotateEnv!=Real(0))
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"spin";
	}

	if(refJointSet || inputJointSet)
	{
		rSummary+=" using ";
		if(inputJointSet)
		{
			rSummary+=inputJointName;
			if(refJointSet && refJointName!=inputJointName)
			{
				rSummary+=" + "+refJointName;
			}
		}
		else
		{
			rSummary+=refJointName;
		}
	}

	I32 indexX=0;
	I32 indexZ=0;

	if(!forceArms || !forceSource || !forceDestination)
	{
		const U32 start=(forceArms && forceSource);
		const U32 passes=1+(spReferencePoint.isValid() &&
				(!forceArms || !forceDestination));
		for(U32 pass=start;pass<passes;pass++)
		{
			if((!pass && inputJointSet) || (pass && refJointSet))
			{
				continue;
			}

			sp<SurfaceAccessorI> spPoint=pass? spReferencePoint: spInputPoint;
			FEASSERT(spPoint.isValid());

			SpatialVector min=spPoint->spatialVector(0);
			SpatialVector max=min;
			SpatialVector sum=min;
			for(I32 index=1;index<inputCount;index++)
			{
				const SpatialVector point=spPoint->spatialVector(index);
				sum+=point;

				for(U32 m=0;m<3;m++)
				{
					const Real value=point[m];
					if(min[m]>value)
					{
						min[m]=value;
					}
					if(max[m]<value)
					{
						max[m]=value;
						if(pass==passes-1)
						{
							if(m==0)
							{
								indexX=index;
							}
							else if(m==2)
							{
								indexZ=index;
							}
						}
					}
				}
			}

			if(pass)
			{
//				rReferenceCenter=0.5*(min+max);
				rReferenceCenter=sum/Real(inputCount);
			}
			else
			{
//				rInputCenter=0.5*(min+max);
				rInputCenter=sum/Real(inputCount);
			}
		}
	}

	SpatialVector referenceTrue=rReferenceCenter;

	if(inputJointSet)
	{
		rInputCenter=inputJointTransform.translation();
	}
	else if(forceSource)
	{
		rInputCenter=source;
	}

	if(refJointSet)
	{
		rReferenceCenter=referenceJointTransform.translation();
	}
	else if(forceDestination)
	{
		rReferenceCenter=destination;
	}

	if(forceArms)
	{
		if(armIndexX>=0)
		{
			indexX=armIndexX;
		}
		if(armIndexZ>=0)
		{
			indexZ=armIndexZ;
		}
	}
	if(rotationLock)
	{
		if((!inputJointSet || !refJointSet) && indexX==indexZ)
		{
			catalog<String>("warning")+=
					"rotation control indexes can not match"
					" (rotation lock dismissed); ";
			rotationLock=FALSE;
		}
		if(!inputJointSet && (indexX>=inputCount || indexZ>=inputCount))
		{
			catalog<String>("warning")+=
					"a rotation control index exceeds input point count"
					" (rotation lock dismissed); ";
			rotationLock=FALSE;
		}
		if(!refJointSet && (indexX>=referenceCount || indexZ>=referenceCount))
		{
			catalog<String>("warning")+=
					"a rotation control index exceeds reference point count"
					" (rotation lock dismissed); ";
			rotationLock=FALSE;
		}
	}

	SpatialEuler eulerRef;
	SpatialEuler eulerDef;

	SpatialTransform xformRef;
	SpatialTransform xformDef;

	const SpatialVector zero(0.0,0.0,0.0);
	SpatialTransform conversion;
	if(rotationLock)
	{
		//* rotation (translation optional)

		for(U32 pass=0;pass<2;pass++)
		{
			if(pass && refJointSet)
			{
				xformRef=referenceJointTransform;
				continue;
			}
			if(!pass && inputJointSet)
			{
				xformDef=inputJointTransform;
				continue;
			}

			sp<SurfaceAccessorI> spPoint=pass? spReferencePoint: spInputPoint;
			sp<SurfaceAccessorI> spGroup;

			SpatialVector center=pass? referenceTrue: rInputCenter;

			U32 count=0;

			if(groupedT && (pass? !forceDestination: !forceSource) &&
					access(spGroup,spReferenceAccessible,e_pointGroup,groupT)
					&& (count=spGroup->count()))
			{
				set(center);
				for(U32 index=0;index<count;index++)
				{
					const I32 pointIndex=spGroup->integer(index);
					FEASSERT(pointIndex>=0);
					center+=spPoint->spatialVector(pointIndex);
				}
				center*=1.0/Real(count);

				if(pass)
				{
					rReferenceCenter=center;
					referenceTrue=center;
				}
				else
				{
					rInputCenter=center;
				}
			}

			SpatialVector side;
			if(groupedX &&
					access(spGroup,spReferenceAccessible,e_pointGroup,groupX)
					&& (count=spGroup->count()))
			{
				set(side);
				for(U32 index=0;index<count;index++)
				{
					const U32 pointIndex=spGroup->integer(index);
					side+=spPoint->spatialVector(pointIndex);
				}
				side*=1.0/Real(count);
			}
			else
			{
				side=spPoint->spatialVector(indexX);
			}
			side-=center;

			SpatialVector forward;
			if(groupedZ &&
					access(spGroup,spReferenceAccessible,e_pointGroup,groupZ)
					&& (count=spGroup->count()))
			{
				set(forward);
				for(U32 index=0;index<count;index++)
				{
					const U32 pointIndex=spGroup->integer(index);
					forward+=spPoint->spatialVector(pointIndex);
				}
				forward*=1.0/Real(count);
			}
			else
			{
				forward=spPoint->spatialVector(indexZ);
			}
			forward-=center;

#if FE_CODEGEN<=FE_DEBUG
			BWORD success=
#endif
					makeFrame(pass? xformRef: xformDef,
					(pass && translationLock)? rReferenceCenter: rInputCenter,
					unitSafe(side),forward);
			FEASSERT(success);
		}

		SpatialTransform invDef;
		invert(invDef,xformDef);
		conversion=invDef*xformRef;

		if(scaleAxes ||
				envelope*translateEnv!=1.0 || envelope*rotateEnv!=1.0)
		{
			Real power=1.0;
			Real envT=1.0;
			Real envR=1.0;

			if(fabs(rotateEnv)>fabs(translateEnv))
			{
				power=envelope*rotateEnv;
				envT=translateEnv/rotateEnv;
			}
			else
			{
				power=envelope*translateEnv;
				envR=rotateEnv/translateEnv;
			}

			if(scaleAxes || envT!=1.0 || envR!=1.0)
			{
				SpatialVector delta=
						envT*(xformRef.translation()-xformDef.translation());
				delta[0]*=translateEnvX;
				delta[1]*=translateEnvY;
				delta[2]*=translateEnvZ;

				const SpatialVector transMod=xformDef.translation()+delta;

				SpatialQuaternion quatRef(xformRef);
				SpatialQuaternion quatDef(xformDef);
				SpatialQuaternion quatMod;
				slerp(quatMod,quatDef,quatRef,envR);

				SpatialTransform xformMod(quatMod);

				setTranslation(xformMod,transMod);

				conversion=invDef*xformMod;
			}

			if(power!=1.0)
			{
				MatrixPower<SpatialTransform> matrixPower;
				matrixPower.solve(conversion,conversion,power);
			}
		}

		eulerRef=xformRef;
		eulerDef=xformDef;
	}
	else
	{
		//* translation only
		SpatialVector translation=
				envelope*translateEnv*(rReferenceCenter-rInputCenter);
		translation[0]*=translateEnvX;
		translation[1]*=translateEnvY;
		translation[2]*=translateEnvZ;

		setIdentity(conversion);
		translate(conversion,translation);

		set(eulerRef);
		set(eulerDef);

		setIdentity(xformRef);
		setIdentity(xformDef);

		translate(xformRef,rReferenceCenter);
		translate(xformDef,rInputCenter);
	}

	rConversionX=conversion.column(0);
	rConversionY=conversion.column(1);
	rConversionZ=conversion.column(2);
	rConversionT=conversion.column(3);

	//* NOTE Euler is XYZ order

	const SpatialEuler eulerConversion=conversion;
	rConversionE=eulerConversion/degToRad;
	rReferenceE=eulerRef/degToRad;
	rInputE=eulerDef/degToRad;

	SpatialTransform invConversion;
	invert(invConversion,conversion);

	rInverseX=invConversion.column(0);
	rInverseY=invConversion.column(1);
	rInverseZ=invConversion.column(2);
	rInverseT=invConversion.column(3);

	const SpatialEuler eulerInverse=invConversion;
	rInverseE=eulerInverse/degToRad;

#if FE_HBO_OUTPUT_DEBUG
	feLog("HobbleOp::handle\n")
	feLog("  InputCenter %s\n",c_print(rInputCenter));
	feLog("  InputEuler %s\n",c_print(rInputE));
	feLog("  ReferenceCenter %s\n",c_print(rReferenceCenter));
	feLog("  ReferenceEuler %s\n",c_print(rReferenceE));
	feLog("  ConversionX %s\n",c_print(rConversionX));
	feLog("  ConversionY %s\n",c_print(rConversionY));
	feLog("  ConversionZ %s\n",c_print(rConversionZ));
	feLog("  ConversionT %s\n",c_print(rConversionT));
	feLog("  ConversionE %s\n",c_print(rConversionE));
#endif

	if(!locatorPreface.empty())
	{
		sp<SurfaceAccessorI> spLocPositionIn;
		access(spLocPositionIn,spInputAccessible,
				e_detail,locatorPreface+"P",e_warning);

		sp<SurfaceAccessorI> spLocNormalIn;
		access(spLocNormalIn,spInputAccessible,
				e_detail,locatorPreface+"N",e_warning);

		sp<SurfaceAccessorI> spLocTangentIn;
		access(spLocTangentIn,spInputAccessible,
				e_detail,locatorPreface+"T",e_warning);

		sp<SurfaceAccessorI> spLocPosition;
		access(spLocPosition,spOutputAccessible,
				e_detail,locatorPreface+"P",e_warning);

		sp<SurfaceAccessorI> spLocNormal;
		access(spLocNormal,spOutputAccessible,
				e_detail,locatorPreface+"N",e_warning);

		sp<SurfaceAccessorI> spLocTangent;
		access(spLocTangent,spOutputAccessible,
				e_detail,locatorPreface+"T",e_warning);

		if(spLocPositionIn.isValid() &&
				spLocPosition.isValid())
		{
			const SpatialVector locPoint=spLocPositionIn->spatialVector(0);

			SpatialVector transformedPoint;

			transformVector(conversion,locPoint,transformedPoint);

			spLocPosition->set(0,transformedPoint);
		}
		if(spLocNormal.isValid() &&
				spLocNormalIn.isValid())
		{
			const SpatialVector locNormal=spLocNormalIn->spatialVector(0);

			SpatialVector transformedNormal;
			rotateVector(conversion,locNormal,transformedNormal);

			spLocNormal->set(0,transformedNormal);
		}
		if(spLocTangentIn.isValid() &&
				spLocTangent.isValid())
		{
			const SpatialVector locTangent=spLocTangentIn->spatialVector(0);

			SpatialVector transformedTangent;
			rotateVector(conversion,locTangent,transformedTangent);

			spLocTangent->set(0,transformedTangent);
		}
	}

	if(outputJoint)
	{
		sp<DrawI> spOutputDraw;
		if(!accessDraw(spOutputDraw,a_rSignal)) return;

		SpatialVector vert[3];
		vert[0]=xformDef.translation();
		vert[1]=vert[0]+xformDef.column(1);
		vert[2]=vert[0]+xformDef.column(0);

		spOutputDraw->drawTriangles(vert,NULL,NULL,3,
				DrawI::e_discrete,false,NULL);

		sp<SurfaceAccessorI> spJointName;
		if(access(spJointName,spOutputAccessible,e_primitive,"name"))
		{
			spJointName->set(0,outputJointName);
		}

		for(U32 pass=0;pass<2;pass++)
		{
			const String prefix=pass? "ref": "anim";
			SpatialTransform& rTransform=pass? xformRef: xformDef;

			sp<SurfaceAccessorI> spOutputX;
			sp<SurfaceAccessorI> spOutputY;
			sp<SurfaceAccessorI> spOutputZ;
			sp<SurfaceAccessorI> spOutputT;
			sp<SurfaceAccessorI> spOutputS;

			access(spOutputX,spOutputAccessible,e_primitive,prefix+"X");
			access(spOutputY,spOutputAccessible,e_primitive,prefix+"Y");
			access(spOutputZ,spOutputAccessible,e_primitive,prefix+"Z");
			access(spOutputT,spOutputAccessible,e_primitive,prefix+"T");

			if(spOutputX.isValid() && spOutputY.isValid() &&
					spOutputZ.isValid() && spOutputT.isValid())
			{
				spOutputX->set(0,rTransform.column(0));
				spOutputY->set(0,rTransform.column(1));
				spOutputZ->set(0,rTransform.column(2));
				spOutputT->set(0,rTransform.column(3));
			}
		}

		rSummary+=" as "+outputJointName;
	}
	else
	{
		for(I32 index=0;index<inputCount;index++)
		{
			const SpatialVector point=spInputPoint->spatialVector(index);
			SpatialVector transformed;
			transformVector(conversion,point,transformed);

			spOutputPoint->set(index,transformed);
		}
	}
}
