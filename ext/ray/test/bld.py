import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    # HACK temp
    deplibs +=  [ "fexRayDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib",
                        "fexViewerDLLib" ]

    tests = [   'xRay',
                'xRayBox' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xRay.exe",        "fexImageDL 30",            None,       None),
        ("xRayBox.exe",     "30",                       None,       None) ]
