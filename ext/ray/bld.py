import sys
forge = sys.modules["forge"]

import os.path

def prerequisites():
    return ["surface"]

def setup(module):
    module.summary = []

    srcList = [ "DrawRayTrace",
                "ray.pmh",
                "rayLib" ]

    srcList += [ "rayDL" ]

    dll = module.DLL( "fexRayDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexRayDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexRayDL",                         None,   None) ]

    if 'viewer' in forge.modules_confirmed:
        module.Module( 'test' )
    else:
        module.summary += [ "-viewer" ]
