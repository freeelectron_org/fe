/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_BladeOp_h__
#define __grass_BladeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Handler to bend and buckle curves based on a collider

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT BladeOp:
	public SurfaceBindOp,
	public Initialize<BladeOp>,
	public ObjectSafe<BladeOp>
{
	public:

					BladeOp(void):
						m_frame(-1)											{}
virtual				~BladeOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange);

	private:

		sp<SurfaceAccessibleI>	m_spOutputAccessible;
		sp<DrawMode>			m_spDebugGroup;

		I32						m_frame;

		Blade					m_blade;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_BladeOp_h__ */
