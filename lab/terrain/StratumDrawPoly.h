/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumDrawPoly_h__
#define __terrain_StratumDrawPoly_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw a terrain using a faceted surface

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumDrawPoly: virtual public StratumDrawI,
		public Initialize<StratumDrawPoly>
{
	public:
					StratumDrawPoly(void);
virtual				~StratumDrawPoly(void);
		void		initialize(void);

					//* As StratumDrawI
virtual	void		bind(sp<DrawI> spDrawI)				{ m_spDrawI=spDrawI; }
virtual	void		recenter(const SpatialVector& center)	{ m_center=center; }
virtual void		draw(const sp<StratumI>& rspStratumI);
virtual	void		assignVisibility(sp<RecordGroup>& rspRecordGroup);

	protected:
virtual	void		drawSurface(void);

		sp<DrawI>				m_spDrawI;
		sp<DrawMode>			m_spWireframe;

		SpatialVector			m_center;
		Real					m_size;
		U32						m_vertices;
		U32						m_probeStep;

		SpatialVector*			m_pFieldVert;
		SpatialVector*			m_pFieldNorm;
		Color*					m_pFieldColor;

		SpatialVector*			m_pVertex;
		SpatialVector*			m_pNormal;
		Color*					m_pColor;

		sp<SelectorI>			m_spSelector;
		sp<RecordGroup>			m_spProbeGroup;
		sp<RecordGroup>			m_spVisibilityGroup;
		sp<Scope>				m_spScope;

		Operator				m_selectOp;
		RecordArrayView<Sphere>	m_sphereRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumDrawPoly_h__ */
