/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#include "fe/data.h"
#include <math.h>

/**
@file
The purpose of src/data provide open run time extensible structured data.

Precedents range from Houdini and Maya data systems to game ECS systems.

Please see @ref data_primer for a full discussion.
___
## What

### Overview

Runtime Aggregate Typed Data System -- An aggregation of attributes defines
this notion of "type".  This manifests as both a form of implicit typing (akin
to python "duck" typing, where a thing is a thing because it has the attributes
of that thing), and an explicit form (where a thing is constructed as a thing
with a strict set of attributes).

This leads directly to analogy with other cases, such as Maya, Houdini,
USD, etc.  However, one of the closest patterns to compare is the
Entity Component System (ECS) pattern.

Distilled from https://en.wikipedia.org/wiki/Entity_component_system:
- Entity: The entity is a general purpose object.
  Usually, it only consists of a unique id.
- Component: The raw data for one aspect of the object, and how it
  interacts with the world.
- System: Each System runs continuouslyand performs global actions on every
  Entity that possesses a Component of the same aspect as that System.

src/data overlaps with the above definition.  src/data does not directly
facilitate the "System" part, but rather defers the functionality aspect to
src/plugin.  However, src/data extends the concept of Entity to a Typed Entity,
which improves on the ECS pattern's power and can be used for more optimized
implementation without losing usability.  Therefore, the src/data pattern could
be termed: Typed Entity Components for Systems (TEC-S)

A good reason to relate src/data to ECS is that ECS has a lot of stuff written
about it which applies to src/data, including the value of the run time part of
it, and the tradeoffs in such.

### Specifically

The Basic Summary (non circular)
- Scopes are namespaces, and everything else is within a Scope
- Attributes are named types.  Attribute instances are data.
- Layouts define a set of attributes, so they are "aggregate types"
- Records contain instances of attributes according to a Layout
- RecordGroups contain Records

The terms used in traditional ECS map roughly to these src/data terms.
- Entity Type: Layout
- Entity:      Record
- Component:   Attribute
- System:      src/plugin

### Deep

The way data is actually stored in src/data has some compile time options.
The original src/data implementation stored data in what were called
State Blocks (SB), which is an Array of Structures (AoS) approach.

In order to improve performance, a Structure of Arrays (SoA) approach
was added using what were called Attribute Vectors (AV).

Finally, to improve performance even more, for intense inner loop system
cases, more direct access to the Attribute Vectors in a heterogenously
usable way was added.  Here we call that AV'.

- SB: AoS (FE_DATA_STORE==FE_SB) -- 20200601: possibly deprecated
- AV: SoA (FE_DATA_STORE==FE_AV)
- AV': SoA with more direct access enabled (FE_AV_FASTITER_ENABLE)
___
## How

- error checking kept to a minimum for clarity of this guide
- good practice would be to check sp's for validity, etc

- Master
- Hello World
	+ Types
	+ Scope
	+ Attribute
	+ Layout
	+ Record
	+ Accessors
	+ RecordGroup
- Using Accessors
- Adding Data
- Using AccessorSets
- Data Access Methods
	+ Access Pattern: Basic
	+ Access Pattern: RecordArray
	+ Access Pattern: Layout
	+ Access Pattern: Compile
- Schemas
  + Writing Files
  + Reading Files

-------------------------------------------------------------------------- */

using namespace fe;

// make example terminal output a little nicer
void print_group(sp<RecordGroup> spRecordGroup);



sp<RecordGroup> hello_world(sp<Master> spMaster)
{
	/* TYPES ----------------------------------------------------------------
	New types can be added first order into the system, even from plugins.
	Further, the types themselves are also string named.  This means that it
	is possible for the same type to have mulitple names.  For example, it is
	common for I32 to have the names "I32", "integer", and "int".
	---------------------------------------------------------------------- */
	// Looking up a type by name, although actually doing this is uncommon
	sp<BaseType> spIntegerTypeThisPrimerDoesNotNeed =
		spMaster->typeMaster()->lookupName("integer");



	/* SCOPE ----------------------------------------------------------------
	Scope is namespace and starting point for src/data structures.

	In order to use src/data, one usually has to start with a Scope.
	---------------------------------------------------------------------- */
	sp<Scope> spScope(spMaster->registry()->create("Scope"));



	/* ATTRIBUTE ------------------------------------------------------------
	Attributes are named and typed.   The names are strings, and the
	namespace for the names is a Scope.

	The types are FE type system types.

	Accessors are the preferred way to both the define attributes, and to
	interact with instantiated Records.
	---------------------------------------------------------------------- */
	Accessor<String> hello_accessor(spScope, "hello");



	/* LAYOUT ---------------------------------------------------------------
	A Layout is the (type) of this aggregate type system. It is made
	up of an aggregation of Attributes.

	C++ type vs aggregate type (a Layout)

	A C++ type, such as unsigned int or class MyClass, is a compiler supported
	and compiled typed checked "type".

	A Layout is an aggregate of named "C++ types" that is assembled at
	runtime. An actual Layout at runtime may in fact have an aggregate of
	attributes that differ based on what is plugged in.
	---------------------------------------------------------------------- */
	sp<Layout> spLayout = spScope->declare("layout_name");

	// Layouts are named, and may be found by name from their Scope
	spLayout = spScope->lookupLayout("layout_name"); // redundant example

	// Add an attribute to a Layout
	spLayout->populate(hello_accessor);



	/* RECORD ---------------------------------------------------------------
	A Record is an instance of an aggregate type as defined by a Layout.

	Technically, the C++ class Record is a reference counted reference to
	a Record instance.

	Records are not named in the runtime, however, for referencing purposes
	in the file format, you may see them named in ascii files.
	---------------------------------------------------------------------- */
	Record record = spScope->createRecord(spLayout);



	/* ACCESSORS ------------------------------------------------------------
	Accessors are not only used to declare attributes (as above), but are
	also used to write and read attributes in Records.

	Accessors are the key to extending src/data from an ECS pattern to
	a TECS pattern without losing the power of the "E".  A heterogeneous
	base of data can still be operated on by a system based on Entity
	composition (per ECS), without needing any knowledge of Entity Type.
	---------------------------------------------------------------------- */
	hello_accessor(record) = "world";



	/* RECORDGROUP ----------------------------------------------------------
	A RecordGroup is a collection of Records.  Because Records are really
	references to instances, there is no problem with having Records
	within more than on RecordGroup.
	---------------------------------------------------------------------- */
	sp<RecordGroup> spRG(new RecordGroup());
	// Add a record to a group
	spRG->add(record);



	/* RESULTS --------------------------------------------------------------
	This example is not about reading or writing of src/data files. However,
	at this point if we do an Ascii file format output, it should look
	something like:

	INFO 5
	ATTRIBUTE hello string
	LAYOUT layout_name
			hello
	DEFAULTGROUP 1
	RECORD layout_name1 layout_name
			hello "world"
	RECORDGROUP 1
	END

	The following function should output the above (colorized on linux) to the
	terminal.
	---------------------------------------------------------------------- */
	print_group(spRG);


	return spRG;
}



void using_accessors(sp<Scope> spScope)
{
	/* Setup the Accessor ---------------------------------------------------
	An attribute is defined with a Scope, has a string name, and a C++
	type.   Therefore, we need to set all three, which can be done by
	constructing an Accessor.
	---------------------------------------------------------------------- */
	Accessor<String> hello_accessor(spScope, "hello");

	// Type Checking happens at setup/initialize of an Accessor
	Accessor<int> hello_wrong_type;
	try
	{
		hello_wrong_type.initialize(spScope, "hello");
	}
	catch(fe::Exception &e)
	{
		// NOOP
	}


	/* Populate a Layout ----------------------------------------------------
	Accessors can be used to add Attributes to layouts.  This is called
	populating a layout.
	---------------------------------------------------------------------- */
	// First, we need a layout to add to
	sp<Layout> spLayout = spScope->declare("layout_for_accessor_example");
	// Then we can populate with our attibute using our accessor
	spLayout->populate(hello_accessor);



	/* Writing to Attributes ------------------------------------------------
	The main syntax for using accessors to access the atribute data is with
	the () operator.  However, there are also a range of queryAttribute()
	methods available for more flexibility.
	---------------------------------------------------------------------- */
	// First, we need a Record, with our attribute, to write to
	Record record = spScope->createRecord(spLayout);
	// Then we can write to the attribute
	hello_accessor(record) = "WORLD";


	/* Reading from Attributes ----------------------------------------------
	Reading an attribute uses the same operator () syntax as writing.
	---------------------------------------------------------------------- */
	String string_value = hello_accessor(record);



	/* Check for Attributes -------------------------------------------------
	In a heterogeneous data environment, it is useful to be able to check
	if a Record or Layout has an attribute we are interested in.
	---------------------------------------------------------------------- */
	// Does the record have it?
	bool record_has_hello = hello_accessor.check(record);
	// Does the layout have it?
	bool layout_has_hello = hello_accessor.check(spLayout);
	// A queryAttribute() can be used to combine checking and accessing
	//   NULL is returned if the attribute wasn't there
	String *pointer_to_data= hello_accessor.queryAttribute(record);
}



void add_heterogenous_dataset(sp<RecordGroup> spRecordGroup)
{
	sp<Scope> spScope = spRecordGroup->scope();

	/* A Mix of Attributes and Layouts --------------------------------------
	We create a mix of attributes and layouts here ot play with in some
	further examples.

	There is nothing new here that has not been covered above.   There
	is just more of it.
	---------------------------------------------------------------------- */
	// repeat setup() calls to re-get an accessor are ok
	Accessor<String> hello_accessor;
	hello_accessor.setup(spScope, "hello");

	Accessor<Real> location_accessor;
	location_accessor.setup(spScope, "location");

	Accessor<Real> mass_accessor;
	mass_accessor.setup(spScope, "mass");

	Accessor<Real> force_accessor;
	force_accessor.setup(spScope, "force");

	Accessor<Real> velocity_accessor;
	velocity_accessor.setup(spScope, "velocity");

	sp<Layout> spParticleLayout = spScope->declare("particle");
	spParticleLayout->populate(mass_accessor);
	spParticleLayout->populate(location_accessor);
	spParticleLayout->populate(force_accessor);
	spParticleLayout->populate(velocity_accessor);

	sp<Layout> spParticle2Layout = spScope->declare("particle2");
	spParticle2Layout->populate(mass_accessor);
	spParticle2Layout->populate(location_accessor);
	spParticle2Layout->populate(force_accessor);
	spParticle2Layout->populate(velocity_accessor);

	sp<Layout> spPlaceLayout = spScope->declare("place");
	spPlaceLayout->populate(location_accessor);

	sp<Layout> spSignLayout = spScope->declare("sign");
	spSignLayout->populate(hello_accessor);
	spSignLayout->populate(location_accessor);

	/* Instantiate a mix of Records -----------------------------------------
	The extra outer loop here is to show the effect (or not) of creation
	order itself being heterogeneous.
	---------------------------------------------------------------------- */
	for(unsigned int r = 0; r < 2; r++)
	{
		for(unsigned int i = 0; i < 10; i++)
		{
			Record record = spScope->createRecord(spParticleLayout);
			location_accessor(record) = (Real)i;
			mass_accessor(record) = 1.1;
			spRecordGroup->add(record);
		}

		for(unsigned int i = 0; i < 5; i++)
		{
			Record record = spScope->createRecord(spParticle2Layout);
			location_accessor(record) = (Real)i + 0.5;
			mass_accessor(record) = 1.1;
			spRecordGroup->add(record);
		}

		for(unsigned int i = 0; i < 2; i++)
		{
			Record record = spScope->createRecord(spPlaceLayout);
			location_accessor(record) = (Real)i;
			spRecordGroup->add(record);
		}

		for(unsigned int i = 0; i < 1; i++)
		{
			Record record = spScope->createRecord(spSignLayout);
			location_accessor(record) = (Real)i;
			spRecordGroup->add(record);
		}
	}
}


class FE_DL_EXPORT AsMass:
public AccessorSet,
public Initialize<AsMass>
{
public:
	void initialize(void)
	{
		add(mass,		FE_USE("mass"));
		add(location,	FE_USE("location"));
	}
	Accessor<Real>		mass;
	Accessor<Real>		location;
};

void using_accessorsets(sp<RecordGroup> spRecordGroup)
{
	/* AccessorSets ---------------------------------------------------------
	As the name implies, an AccessorSet is a set of Accessors.

	AccessorSets can be used as a way to define an implicit or "duck" type.
	---------------------------------------------------------------------- */

	// Get the scope
	sp<Scope> spScope = spRecordGroup->scope();

	/* Manual and Transient Case --------------------------------------------
	Although an atypical usage, AccessorSets can be setup manually and
	used transiently
	---------------------------------------------------------------------- */
	// Make a couple accessors:
	Accessor<Real> mass_accessor;
	mass_accessor.setup(spScope, "mass");
	Accessor<Real> location_accessor;
	location_accessor.setup(spScope, "location");

	std::vector<Record> records;

	AccessorSet asMassManual(spScope);
	asMassManual.add(mass_accessor);
	asMassManual.add(location_accessor);

	// filter for the records that match
	asMassManual.filter(records, spRecordGroup);

	// clear the records for the next example
	records.clear();


	/* Define an AccessorSet class ------------------------------------------
	This is the more common way to use AccessorSets.

	Inherit from AccessorSet and Initialize to create a class that defines
	the implicit/duck type.
	---------------------------------------------------------------------- */

	AsMass asMass;
	// filter for the records that match
	asMass.filter(records, spRecordGroup);


	/* Populating a Layout with an AccessorSet -----------------------------
	---------------------------------------------------------------------- */
	sp<Layout> spLayout = spScope->declare("layout");
	asMass.populate(spLayout);

}


/* A SIMPLE USE CASE --------------------------------------------------------

Here is a very simple use case to compare 4 different patterns for
accessing src/data data.   The example is a 1D simple particle simulation.
The attributes for records created above were chosen with this case in
mind, with each particle having mass, velocity, force, and location.

4 Patterns are covered, with a very simple summary here, with the speed tests
being on a linux laptop on 20200529, optimized build.

            |  dataset size  |
Pattern     |  small | large | comment
--------------------------------------------------------------------------
Basic       |  1.13  | 14.7  | easy to use
RecordArray |  1.06  |  4.3  | general purpose, most common method
Layout      |  0.53  |  2.1  | requires FE_AV_FASTITER_ENABLE
Compile     |  0.10  |  1.2  | requires static topology

-------------------------------------------------------------------------- */

// for simplicity, just hard set the timestep and sim time
#define DT 1.0e-4
#define ST 0.1

// math functions implemented fully flat to avoid access assumptions
inline void gravity(
	Real &force_A, const Real &location_A, const Real & mass_A,
	Real &force_B, const Real &location_B, const Real & mass_B)
{
	Real f = 0.0;
	Real g = 0.01;
	Real d = location_B-location_A;
	Real r = fabs(d);
	if(r > 1.0e-1)
	{
		d = d / r;
		f = d * mass_A*mass_B / (r*r);

		force_A += f;
		force_B -= f;
	}
}

inline void viscosity(Real &force_A, const Real &velocity_A)
{
	Real Cd = 10.1;
	force_A -= Cd*velocity_A;
}

inline void integrate(Real &location, Real &velocity,
	const Real &force, const Real &mass, const Real &dt)
{
	velocity += dt*force/mass;
	location += dt*velocity;
}



// An AccessorSet for particles in our use case
class FE_DL_EXPORT AsParticle:
	public AccessorSet,
	public Initialize<AsParticle>
{
	public:
		void initialize(void)
		{
			add(mass,		FE_USE("mass"));
			add(location,	FE_USE("location"));
			add(force,		FE_USE("force"));
			add(velocity,	FE_USE("velocity"));
		}
		Accessor<Real>		mass;
		Accessor<Real>		location;
		Accessor<Real>		force;
		Accessor<Real>		velocity;
};


/* Access Pattern: Basic ----------------------------------------------------
This is the easiest method, and is essentially what was done above.

Pattern:
  * AccessorSet::filter() to get matching Records in RecordGroup
  * iterate through std::vector of Records
-------------------------------------------------------------------------- */
void access_pattern_basic(sp<RecordGroup> spRecordGroup)
{
	std::vector<Record> records;
	AsParticle asParticle;
	asParticle.filter(records, spRecordGroup);

	for(Real et = 0.0; et < ST; et += DT)
	{
		// clear force accumulator
		for(unsigned int i = 0; i < records.size(); i++)
		{
			Record r_a = records[i];
			asParticle.force(r_a) = 0.0;
		}

		// accumulate forces
		for(unsigned int i = 0; i < records.size(); i++)
		{
			Record r_a = records[i];
			for(unsigned int j = i+1; j < records.size(); j++)
			{
				Record r_b = records[j];
				gravity(
					asParticle.force(r_a),
					asParticle.location(r_a), asParticle.mass(r_a),
					asParticle.force(r_b),
					asParticle.location(r_b), asParticle.mass(r_b));
			}
			viscosity(asParticle.force(r_a), asParticle.velocity(r_a));
		}

		// integrate
		for(unsigned int i = 0; i < records.size(); i++)
		{
			Record r_a = records[i];
			integrate(asParticle.location(r_a), asParticle.velocity(r_a),
				asParticle.force(r_a), asParticle.mass(r_a), DT);
		}
	}
}


/* Access Pattern: RecordArray ----------------------------------------------
This method is usually faster than the Basic pattern, annd allows for
more flexibility and control while filtering, but is more involved, and
can get downright ugly when nesting.

== RecordArrays ==============

While not part of what you see in src/data files, the src/data runtime
automatically organizes the the Records with RecordGroups into RecordArrays.
There is a RecordArray for each Layout with Records in a RecordGroup.

Pattern:
  * iterate through RecordArrays in a RecordGroup
	* iterate through Records in a RecordArray for matching Layout types
-------------------------------------------------------------------------- */
void access_pattern_recordarray(sp<RecordGroup> spRecordGroup)
{
	AsParticle asParticle;
	asParticle.bind(spRecordGroup->scope());

	for(Real et = 0.0; et < ST; et += DT)
	{
		for(RecordGroup::iterator i_rg_A = spRecordGroup->begin();
			i_rg_A != spRecordGroup->end(); i_rg_A++)
		{
			sp<RecordArray> spRA_A = *i_rg_A;
			if(!asParticle.check(spRA_A)) { continue; }
			for(int i = 0; i < spRA_A->length(); i++)
			{
				asParticle.force(spRA_A,i) = 0.0;
			}
		}

		for(RecordGroup::iterator i_rg_A = spRecordGroup->begin();
			i_rg_A != spRecordGroup->end(); i_rg_A++)
		{
			sp<RecordArray> spRA_A = *i_rg_A;
			// for small data sets, this nested check() can cause be
			// slower than above, but RecordArray is better for large data
			if(!asParticle.check(spRA_A)) { continue; }
			for(int i = 0; i < spRA_A->length(); i++)
			{
				for(RecordGroup::iterator i_rg_B = i_rg_A;
					i_rg_B != spRecordGroup->end(); i_rg_B++)
				{
					sp<RecordArray> spRA_B = *i_rg_B;
					if(!asParticle.check(spRA_B)) { continue; }
					int first = 0;
					if(spRA_A == spRA_B)
					{
						first=i+1;
					}
					for(int j = first; j < spRA_B->length(); j++)
					{
						gravity(
							asParticle.force(spRA_A,i),
							asParticle.location(spRA_A,i),
							asParticle.mass(spRA_A,i),
							asParticle.force(spRA_B,j),
							asParticle.location(spRA_B,j),
							asParticle.mass(spRA_B,j));
					}
				}
				viscosity(asParticle.force(spRA_A,i),
					asParticle.velocity(spRA_A,i));
			}
		}

		for(RecordGroup::iterator i_rg_A = spRecordGroup->begin();
			i_rg_A != spRecordGroup->end(); i_rg_A++)
		{
			sp<RecordArray> spRA_A = *i_rg_A;
			if(!asParticle.check(spRA_A)) { continue; }
			for(int i = 0; i < spRA_A->length(); i++)
			{
				integrate(asParticle.location(spRA_A,i),
					asParticle.velocity(spRA_A,i),
					asParticle.force(spRA_A,i), asParticle.mass(spRA_A,i), DT);
			}
		}
	}
}

/* Access Pattern: Layout ---------------------------------------------------
This pattern only works with FE_AV_FASTITER_ENABLE compiled on.

The idea of this method is to iterate through data as direct as possible,
as cache friendly as possible.   To do this, not only do we need access to
the underlying arrays, but the managing of those arrays needs to be such
that one can iterate without overhead such as checking for holes.  However,
FE_AV_FASTITER_ENABLE, which does this, also makes (for now), some operations
no longer available.  In particular, adding new attributes to already
instantiated Records.

This patten also does not start with a RecordGroup, but is rather full scope
oriented, iterating through Layouts.

Pattern:
  * iterate through Layouts, finding matching ones
	* access data arrays directly

-------------------------------------------------------------------------- */
void access_pattern_layout(sp<Scope> spScope)
{
#ifdef FE_AV_FASTITER_ENABLE
	AsParticle asParticle;
	asParticle.bind(spScope);

	for(Real et = 0.0; et < ST; et += DT)
	{

		t_layouts &layouts = spScope->layouts();
		for(unsigned int i = 0; i < layouts.size(); i++)
		{
			sp<LayoutAV> l_av = layouts[i];
			if(l_av.isValid() && asParticle.check(l_av))
			{
				Real *av_force = asParticle.force(l_av);
				for(unsigned int i_av = 0; i_av < l_av->avSize(); i_av++)
				{
					av_force[i_av] = 0.0;
				}
			}
		}


		for(unsigned int i = 0; i < layouts.size(); i++)
		{
			sp<LayoutAV> l_av = layouts[i];
			if(l_av.isValid() && asParticle.check(l_av))
			{
				Real *av_location = asParticle.location(l_av);
				Real *av_velocity = asParticle.velocity(l_av);
				Real *av_force = asParticle.force(l_av);
				Real *av_mass = asParticle.mass(l_av);
				for(unsigned int i_av = 0; i_av < l_av->avSize(); i_av++)
				{
					for(unsigned int j_av =i_av+1; j_av<l_av->avSize(); j_av++)
					{
						gravity(
							av_force[i_av], av_location[i_av], av_mass[i_av],
							av_force[j_av], av_location[j_av], av_mass[j_av]);
					}
					for(unsigned int j = i+1; j < layouts.size(); j++)
					{
						sp<LayoutAV> l_av_j = layouts[j];
						if(l_av_j.isValid() && asParticle.check(l_av_j))
						{
							Real *av_location_j = asParticle.location(l_av_j);
							Real *av_velocity_j = asParticle.velocity(l_av_j);
							Real *av_force_j = asParticle.force(l_av_j);
							Real *av_mass_j = asParticle.mass(l_av_j);
							for(unsigned int j_av=0; j_av < l_av_j->avSize();
								j_av++)
							{
								gravity(
									av_force[i_av], av_location[i_av],
										av_mass[i_av],
									av_force_j[j_av], av_location_j[j_av],
										av_mass_j[j_av]);
							}
						}
					}
					viscosity(av_force[i_av], av_velocity[i_av]);
				}
			}
		}


		for(unsigned int i = 0; i < layouts.size(); i++)
		{
			sp<LayoutAV> l_av = layouts[i];
			if(l_av.isValid() && asParticle.check(l_av))
			{
				Real *av_location = asParticle.location(l_av);
				Real *av_velocity = asParticle.velocity(l_av);
				Real *av_force = asParticle.force(l_av);
				Real *av_mass = asParticle.mass(l_av);
				for(unsigned int i_av = 0; i_av < l_av->avSize(); i_av++)
				{
					integrate(av_location[i_av], av_velocity[i_av],
						av_force[i_av], av_mass[i_av], DT);
				}
			}
		}
	}
#endif
}

/* Access Pattern: Compile --------------------------------------------------
The idea of this pattern is to do as much processing overhead just once,
and run through repeated access faster.   The tradeoff here is that the
dataset topology itself cannot change.  So this pattern only applies to
static topologies.

Pattern:
  * iterate through data "compiling" a faster access structure
  * use fast structure

-------------------------------------------------------------------------- */
void access_pattern_compile(sp<RecordGroup> spRecordGroup)
{
	/* C++ structures pointing to the raw src/data --------------------------
	In this example we define two C++ structures, Node and Pair, which
	have data members that are pointers into the src/data locations.
	---------------------------------------------------------------------- */
	struct Node
	{
		Real *location;
		Real *velocity;
		Real *force;
		Real *mass;
	};

	struct Pair
	{
		Real *location[2];
		Real *force[2];
		Real *mass[2];
	};

	std::vector<Node> nodes;
	std::vector<Pair> pairs;


	/* Iterate through the src/data, in this case, using the Basic pattern */
	std::vector<Record> records;
	AsParticle asParticle;
	asParticle.filter(records, spRecordGroup);
	for(unsigned int i = 0; i < records.size(); i++)
	{
		Record r_a = records[i];
		Node node;
		node.location = &(asParticle.location(r_a));
		node.velocity = &(asParticle.velocity(r_a));
		node.force = &(asParticle.force(r_a));
		node.mass = &(asParticle.mass(r_a));
		nodes.push_back(node);
		for(unsigned int j = i+1; j < records.size(); j++)
		{
			Record r_b = records[j];
			Pair pair;
			pair.location[0] = &(asParticle.location(r_a));
			pair.force[0] = &(asParticle.force(r_a));
			pair.mass[0] = &(asParticle.mass(r_a));
			pair.location[1] = &(asParticle.location(r_b));
			pair.force[1] = &(asParticle.force(r_b));
			pair.mass[1] = &(asParticle.mass(r_b));
			pairs.push_back(pair);
		}
	}


	/* Use the new structure to do our simulation iterations ------------- */
	for(Real et = 0.0; et < ST; et += DT)
	{
		for(unsigned int i_node = 0; i_node < nodes.size(); i_node++)
		{
			*(nodes[i_node].force) = 0;
		}

		for(unsigned int i_pair = 0; i_pair < pairs.size(); i_pair++)
		{
			Pair &pair = pairs[i_pair];
			gravity(
				*(pair.force[0]), *(pair.location[0]), *(pair.mass[0]),
				*(pair.force[1]), *(pair.location[1]), *(pair.mass[1]));
		}

		for(unsigned int i_node = 0; i_node < nodes.size(); i_node++)
		{
			Node &node = nodes[i_node];
			viscosity(*(node.force), *(node.velocity));
		}

		for(unsigned int i_node = 0; i_node < nodes.size(); i_node++)
		{
			Node &node = nodes[i_node];
			integrate(*(node.location), *(node.velocity),
					*(node.force), *(node.mass), DT);
		}
	}
}

/* Splitting Schema from data ------------------------------------------------
Often .rg files carry their schema with them at the top of the file.
However it is possible to separate schema, either in separate file(s),
or even as C++ operations on the Scope before loading a data file.

The schema in the following "file" could just as well have been calls
as covered in earlier in this primer.
-------------------------------------------------------------------------- */
const char *schema_rg = "INFO 5			\
ATTRIBUTE location real F32 float		\
ATTRIBUTE mass real F32 float			\
ATTRIBUTE force real F32 float			\
ATTRIBUTE velocity real F32 float		\
LAYOUT particle							\
	location							\
	mass								\
	force								\
	velocity							\
LAYOUT place							\
	location							\
END";


/* Some data for the schema -------------------------------------------------

An example in json might look like:

"particle0" :
{
	"attributes" :
	{
		"location" : "0.1"
	}
},
"particle1" :
{
	"attributes" :
	{
		"location" : "0.3"
	}
}

For for this example we are using RG format which looks as follows.
-------------------------------------------------------------------------- */
const char *body_rg = "					\
RECORD 0 particle						\
	location 0.1						\
RECORD 1 particle						\
	location 0.3						\
";


void schema_example(void)
{
	/* Setup a Scope --------------------------------------------------------
	This is a from-cratch example, so we need to make a Scope.
	---------------------------------------------------------------------- */
	sp<Master> spMaster(new Master);
	spMaster->registry()->manage("feDataDL");
	sp<Scope> spScope(spMaster->registry()->create("Scope"));

	/* Load the Schema --------------------------------------------------- */
	sp<data::StreamI> spStream;
	std::istringstream schema_istrm(schema_rg);
	spStream=new data::AsciiStream(spScope);
	spStream->input(schema_istrm);

	/* Mix in some C++ Schema -----------------------------------------------
	To demostrate mixing in some C++ declared schema, we add a default
	value for "mass".

	TODO: Adding defaults in C++ could be streamlined.
	---------------------------------------------------------------------- */
	sp<Attribute> spAttribute = spScope->findAttribute("mass");
	sp<BaseType> spBT = spScope->typeMaster()->lookupType<Real>();
	spAttribute->defaultInstance().create(spBT);
	Real mass = 1.1;
	spBT->assign(spAttribute->defaultInstance().data(), &mass);


	// To streamline the data file even further, add header and footer here
	// TODO: This too could/should be streamlined in API
	const char *prefix = " INFO 5			\
	DEFAULTGROUP 1";
	const char *suffix = "RECORDGROUP 1		\
	END";

	/* Load the Data ----------------------------------------------------- */
	spStream=new data::AsciiStream(spScope);
	std::string body(prefix); body.append(body_rg); body.append(suffix);
	std::istringstream body_istrm(body);
	sp<RecordGroup> spRecordGroup = spStream->input(body_istrm);


	/* Display the whole RecordGroup ----------------------------------------
	Cut&paste from a run output follows.  Note the defaults are filled in.

	INFO 5
	ATTRIBUTE location real F32 float
	ATTRIBUTE mass real F32 float
	ATTRIBUTE force real F32 float
	ATTRIBUTE velocity real F32 float
	LAYOUT particle
			location
			mass
			force
			velocity
	DEFAULTGROUP 1
	RECORD particle1 particle
			location 0.1
			mass 1.1
			force 0
			velocity 0
	RECORD particle2 particle
			location 0.3
			mass 1.1
			force 0
			velocity 0
	RECORDGROUP 1
	END

	---------------------------------------------------------------------- */
	print_group(spRecordGroup);

}

void write_files(sp<RecordGroup> spRecordGroup)
{
	/* Writing RG Files -----------------------------------------------------
	RG (for RecordGroup) files can be either ascii or binary.

	The main way to serialize RG files if via a Stream component
	and std streams.
	---------------------------------------------------------------------- */
	sp<data::StreamI> spStream;

	// We use the Scope of the RecordGroup
	sp<Scope> spScope = spRecordGroup->scope();

	spStream=new data::AsciiStream(spScope);
	std::ofstream asciifile("ascii.rg");
	spStream->output(asciifile, spRecordGroup);
	asciifile.close();

	spStream=new data::BinaryStream(spScope);
	std::ofstream binaryfile("binary.rg");
	spStream->output(binaryfile, spRecordGroup);
	binaryfile.close();
}

void read_files(sp<Scope> spScope, sp<RecordGroup> &spRGFromAscii, sp<RecordGroup> &spRGFromBinary)
{
	/* Reading RG Files -----------------------------------------------------
	RG (for RecordGroup) files can be either ascii or binary.

	The main way to serialize RG files if via a Stream component
	and std streams.
	---------------------------------------------------------------------- */
	sp<data::StreamI> spStream;

	std::ifstream asciifile("ascii.rg");
	spStream=new data::AsciiStream(spScope);
	spRGFromAscii = spStream->input(asciifile);
	asciifile.close();

	std::ifstream binaryfile("binary.rg");
	spStream=new data::BinaryStream(spScope);
	spRGFromBinary = spStream->input(binaryfile);
	binaryfile.close();
}



String colorize_src_data(const std::string &ascii_text)
{
	String S = ascii_text.c_str();
#if FE_OS==FE_LINUX
	S = S.replace("ATTRIBUTE ", "[33m[1mATTRIBUTE[22m ");
	S = S.replace("RECORD ", "[31m[1mRECORD[22m ");
	S = S.replace("LAYOUT ", "[36m[1mLAYOUT[22m ");
	S = S.replace("RECORDGROUP ", "[32mRECORDGROUP ");
	S = S.replace("DEFAULTGROUP ", "[32mDEFAULTGROUP ");
	S = S.replace("END\n", "[35m[1mEND[0m\n");
	S = S.replace("INFO 5\n", "[35m[1mINFO 5[0m\n");
#endif
	return S;
}



void print_group(sp<RecordGroup> spRecordGroup)
{
	sp<data::StreamI> spStream;
	spStream=new data::AsciiStream(spRecordGroup->getFirstScope());
	std::ostringstream str_ostrm;
	spStream->output(str_ostrm, spRecordGroup);
	feLog(colorize_src_data(str_ostrm.str()).c_str());
}



int main(void)
{
	/* MASTER ---------------------------------------------------------------
	Scope is itself a Component, so ideally it is created as a Component,
	which means we start with a Master (and its Registry).

	src/data also uses the general FE runtime type system, .
	---------------------------------------------------------------------- */
	sp<Master> spMaster(new Master);
	spMaster->registry()->manage("feDataDL"); // plug in src/data lib itself

	sp<RecordGroup> spRecordGroup = hello_world(spMaster);

	using_accessors(spRecordGroup->scope());

	add_heterogenous_dataset(spRecordGroup);

	using_accessorsets(spRecordGroup);

	access_pattern_basic(spRecordGroup);
	access_pattern_recordarray(spRecordGroup);
	access_pattern_layout(spRecordGroup->scope());
	access_pattern_compile(spRecordGroup);

	schema_example();

	write_files(spRecordGroup);

	sp<RecordGroup> spRGfromAscii, spRGfromBinary;
	read_files(spRecordGroup->scope(), spRGfromAscii, spRGfromAscii);

	return 0;
}
