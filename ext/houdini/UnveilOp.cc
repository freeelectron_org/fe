/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

using namespace fe;
using namespace fe::ext;

void UnveilOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void UnveilOp::handle(Record& a_rSignal)
{
//	feLog("UnveilOp::handle\n");

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleHoudini> spAccessibleHoudini=spInputAccessible;
	if(spAccessibleHoudini.isNull())
	{
		catalog<String>("error")="failed to access input as a Houdini surface;";
		return;
	}

	const GU_Detail* pGdp=spAccessibleHoudini->gdp();

	GEO_PrimList primList=pGdp->primitives();
	const I32 primitiveCount=pGdp->getNumPrimitives();

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const GEO_Primitive* pPrimitive=
				pGdp->getGEOPrimitive(pGdp->primitiveOffset(primitiveIndex));

//		feLog("  prim %d/%d pPrimitive %p\n",
//				primitiveIndex,primitiveCount,pPrimitive);

		if(!pPrimitive)
		{
			continue;
		}

		const HoudiniPrimComponent* pPrimComponent=
				dynamic_cast<const HoudiniPrimComponent*>(pPrimitive);

//		feLog("  pPrimComponent %p\n",pPrimComponent);

		if(!pPrimComponent)
		{
			continue;
		}

		sp<Component> spPayload=
				const_cast<HoudiniPrimComponent*>(pPrimComponent)->payload();

//		feLog("  spPayload valid %d\n",spPayload.isValid());

		spOutputAccessible->copy(spPayload);

		//* TODO more than one
		break;
	}
}
