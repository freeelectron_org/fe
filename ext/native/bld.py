import os
import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool", "opengl" ]

def setup(module):
    module.summary = []

    if not "opengl" in forge.modules_confirmed:
        module.summary += [ "needs_opengl" ]
        return

    srcList = [ "native.pmh",
                "assertNative",
                "NativeEventContext",
                "NativeEventContextGDI",
                "NativeEventContextXWIN",
                "NativeEventContextCARBON",
                "NativeKeyboard",
                "NativeWindow",
                "NativeWindowGDI",
                "NativeWindowXWIN",
                "NativeWindowCARBON",
                "nativeDL",
                "weight" ]

    dll = module.DLL( "fexNativeWindowDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    was_deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexWindowDLLib" ]

    deplibs = forge.corelibs + [ "fexSignalLib" ]

    # win32
    now_deplibs = forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        now_deplibs += [
                "fexWindowDLLib" ]

    forge.deps( ["fexNativeWindowDLLib"], now_deplibs )

    forge.tests += [
        ("inspect.exe",     "fexNativeWindowDL",            None,       None) ]

    module.Module( 'test' )

def auto(module):
    forge.linkmap['gfxlibs'] = forge.gfxlibs

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        test_file = """
            #include "windows.h"
            int main(void)
            {
                HWND hwnd;
                return 0;
            }
            \n"""
    elif forge.fe_os == 'FE_OSX':
        test_file = """
            #include <Carbon/Carbon.h>
            int main(void)
            {
                WindowRef ref;
                return 0;
            }
            \n"""
    else:
        # NOTE XInput2 needed for NativeKeyboard
        # TODO should just skip NativeKeyboard if no XInput
        test_file = """
            #include <X11/Xlib.h>
            #include <X11/extensions/XInput.h>
            #include <X11/extensions/XInput2.h>

            int main(void)
            {
                return 0;
            }
            \n"""

    result = forge.cctest(test_file)

    forge.linkmap.pop('gfxlibs', None)

    return result
