/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_test_xRelay_h__
#define __datatool_test_xRelay_h__

namespace
{
static const char* gs_myAttributes[]=
{
	"F32",	"MyF32",
	"I32",	"MyI32"
};
}

class MyRelay: public fe::Relay<2,MyRelay>
{
	public:
		enum
		{
			e_myF32,
			e_myI32,
			e_size
		};
static
const	char*	layoutName(void)	{ return "base"; }
static
const	char**	attributes(void)	{ return gs_myAttributes; }
};

namespace
{
static const char* gs_moreAttributes[]=
{
	"F32",	"MoreF32"
};
}

class DerivedRelay: public fe::Relay<1,DerivedRelay,MyRelay>
{
	public:
		enum
		{
			e_moreF32=MyRelay::e_size,
			e_size
		};

static
const	char*	layoutName(void)	{ return "derived"; }
static
const	char**	attributes(void)	{ return gs_moreAttributes; }

virtual	void	initialize(void)	{ set(MyRelay::e_myF32,4.5f); }
};

#endif // __datatool_test_xRelay_h__
