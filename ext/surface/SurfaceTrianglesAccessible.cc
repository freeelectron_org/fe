/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define	FE_STA_DEBUG	FALSE
#define	FE_STA_PROFILE	FALSE

namespace fe
{
namespace ext
{

void SurfaceTrianglesAccessible::bind(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI)
{
	m_spSurfaceAccessibleI=a_spSurfaceAccessibleI;
}

void SurfaceTrianglesAccessible::cache(void)
{
	//* populate surface with generic accessors

#if FE_STA_DEBUG
	feLog("SurfaceTrianglesAccessible::cache valid %d nodeName \"%s\"\n",
			m_spSurfaceAccessibleI.isValid(),nodeName().c_str());
#endif

#if FE_STA_PROFILE
	sp<Profiler> spProfiler(new Profiler("SurfaceTrianglesAccessible"));
	sp<Profiler::Profile> spProfilePrep(
			new Profiler::Profile(spProfiler,"Prep"));
	sp<Profiler::Profile> spProfileLoop(
			new Profiler::Profile(spProfiler,"Loop"));
	sp<Profiler::Profile> spProfileResolve(
			new Profiler::Profile(spProfiler,"Resolve"));
	spProfiler->begin();
	spProfilePrep->start();
#endif

	FEASSERT(m_spSurfaceAccessibleI.isValid());

	sp<SurfaceAccessorI> spElementUV=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_vertex,SurfaceAccessibleI::e_uv,
			SurfaceAccessibleI::e_refuseMissing);
	const BWORD vertexUVs=spElementUV.isValid();
	if(!vertexUVs)
	{
#if FE_STA_DEBUG
		feLog("SurfaceTrianglesAccessible::cache no vertex uvs\n");
#endif
		spElementUV=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_uv,
				SurfaceAccessibleI::e_refuseMissing);
#if FE_STA_DEBUG
		if(spElementUV.isNull())
		{
			feLog("SurfaceTrianglesAccessible::cache no point uvs\n");
		}
#endif
	}

	sp<SurfaceAccessorI> spVertices=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointPosition=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointNormal=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spVertexNormal=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_vertex,SurfaceAccessibleI::e_normal,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPrimitiveNormal=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_normal,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointColor=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_color,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPrimitiveColor=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_color,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointAlpha=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,"Alpha",
			SurfaceAccessibleI::e_refuseMissing);
	if(spPointAlpha.isNull())
	{
		spPointAlpha=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_point,"alpha",
				SurfaceAccessibleI::e_refuseMissing);
	}

	sp<SurfaceAccessorI> spPrimitivePartition;
	if(!m_partitionAttr.empty())
	{
//		feLog("SurfaceTrianglesAccessible::cache partitionAttr \"%s\"\n",
//				m_partitionAttr.c_str());

		spPrimitivePartition=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_primitive,m_partitionAttr.c_str(),
				SurfaceAccessibleI::e_refuseMissing);
	}

	const U32 primitiveCount=spVertices.isValid()? spVertices->count(): 0;

#if FE_STA_DEBUG
		feLog("SurfaceTrianglesAccessible::cache primitiveCount %d\n",
				primitiveCount);
#endif

	m_elements=primitiveCount;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	U32 triangleIndex=0;
	U32 total=0;

	const SpatialVector zero(0.0,0.0,0.0);
	const Color white(1.0,1.0,1.0);

	setOptionalArrays(Arrays(e_arrayColor|e_arrayUV));

	//* NOTE allow for pentagons
//	const I32 quadDivisions=
//			(m_triangulation==SurfaceI::e_existingPoints)? 2: 4;
//	m_triangulation=SurfaceI::e_existingPoints;
	const I32 quadDivisions=
			(m_triangulation==SurfaceI::e_existingPoints)? 3: 5;

	//* pre-allocate estimate
	resizeFor(primitiveCount*(quadDivisions+1),primitiveCount*3*quadDivisions,
			elementAllocated,vertexAllocated);

	const I32 pointCount=spPointPosition.isValid()? spPointPosition->count(): 0;

	m_hullPointMap.resize(pointCount);
	m_hullFacePointArray.resize(primitiveCount);

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		m_hullPointMap[pointIndex]= -1;
	}

#if FE_STA_PROFILE
	spProfileLoop->replace(spProfilePrep);
#endif

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const U32 subCount=spVertices->subCount(primitiveIndex);

#if FE_STA_DEBUG
		feLog("SurfaceTrianglesAccessible::cache %d/%d pt %d/%d verts %d\n",
				primitiveIndex,primitiveCount,total,vertexAllocated,subCount);
#endif

		if(subCount<3)
		{
			continue;
		}

		if(subCount>5)
		{
			feLog("SurfaceTrianglesAccessible::cache prim %d/%d verts %d\n",
					primitiveIndex,primitiveCount,subCount);
		}

		if(subCount>4 && m_hullPointMap.size())
		{
			m_hullPointMap.clear();
			m_hullFacePointArray.clear();
		}

		if(primitiveIndex<m_hullFacePointArray.size() && subCount<4)
		{
			m_hullFacePointArray[primitiveIndex][3]= -1;
		}

		m_vertices+=3*((subCount>3)? quadDivisions: 1);
		if(subCount==5)
		{
			m_vertices++;
		}

		if(m_vertices>vertexAllocated)
		{
			resizeFor(primitiveCount*(quadDivisions+1),m_vertices,
					elementAllocated,vertexAllocated);
		}

		FEASSERT(triangleIndex<elementAllocated);
		FEASSERT(total<vertexAllocated);

		I32 partitionIndex= -1;
		if(spPrimitivePartition.isValid())
		{
			const String partition=
					spPrimitivePartition->string(primitiveIndex);
			partitionIndex=lookupPartition(partition);

//			feLog("SurfaceTrianglesAccessible::cache partition %d \"%s\"\n",
//					partitionIndex,partition.c_str());
		}

		SpatialVector norm;
		if(spPrimitiveNormal.isValid())
		{
			norm=spPrimitiveNormal->spatialVector(primitiveIndex);
		}
		else
		{
			const I32 pointIndex0=spVertices->integer(primitiveIndex,0);
			const I32 pointIndex1=spVertices->integer(primitiveIndex,1);
			const I32 pointIndex2=spVertices->integer(primitiveIndex,2);

			const SpatialVector point0=
					spPointPosition->spatialVector(pointIndex0);
			const SpatialVector point1=
					spPointPosition->spatialVector(pointIndex1);
			const SpatialVector point2=
					spPointPosition->spatialVector(pointIndex2);

			norm=unitSafe(cross(point2-point0,point1-point0));
		}

		const Color primitiveColor=spPrimitiveColor.isValid()?
				Color(spPrimitiveColor->spatialVector(primitiveIndex)): white;

		m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

		for(U32 subIndex=0;subIndex<subCount && subIndex<5;subIndex++)
		{
			const I32 pointIndex=
					spVertices->integer(primitiveIndex,subIndex);

			if(pointIndex<0 || pointIndex>=I32(vertexAllocated))
			{
				feLog("SurfaceTrianglesAccessible::cache"
						" corrupt point index\n");
				feLog("  primitiveIndex %d subIndex %d/%d pointIndex %d/%d\n",
						primitiveIndex,subIndex,subCount,
						pointIndex,vertexAllocated);
				continue;
			}

#if FALSE
			if(subCount==5)
			{
				feLog("  primitiveIndex %d subIndex %d/%d pointIndex %d/%d\n",
						primitiveIndex,subIndex,subCount,
						pointIndex,vertexAllocated);
				feLog("  total %d triangleIndex %d\n",
						total,triangleIndex);
			}
#endif

			FEASSERT(pointIndex>=0);
			FEASSERT(pointIndex<I32(vertexAllocated));

			if(pointIndex<I32(m_hullPointMap.size()))
			{
				FEASSERT(primitiveIndex<m_hullFacePointArray.size());

				m_hullFacePointArray[primitiveIndex][subIndex]=pointIndex;

				I32& rHullPointIndex=m_hullPointMap[pointIndex];
				if(rHullPointIndex<0)
				{
					rHullPointIndex=total;
				}
			}

			m_pVertexArray[total]=
					spPointPosition->spatialVector(pointIndex);
			m_pNormalArray[total]=(spVertexNormal.isValid())?
					spVertexNormal->spatialVector(primitiveIndex,subIndex):
					((spPointNormal.isValid())?
					spPointNormal->spatialVector(pointIndex): norm);

			if(spElementUV.isNull())
			{
				m_pUVArray[total]=zero;
			}
			else if(vertexUVs)
			{
				m_pUVArray[total]=
						spElementUV->spatialVector(primitiveIndex,subIndex);
			}
			else
			{
				m_pUVArray[total]=spElementUV->spatialVector(pointIndex);
			}

			m_pColorArray[total]=spPointColor.isValid()?
					Color(spPointColor->spatialVector(pointIndex)):
					primitiveColor;

			if(spPointAlpha.isValid())
			{
				m_pColorArray[total][3]=spPointAlpha->real(pointIndex);
			}

			m_pTriangleIndexArray[total]=triangleIndex+(subIndex==3);
			m_pPointIndexArray[total]=pointIndex;
			m_pPrimitiveIndexArray[total]=primitiveIndex;
			m_pPartitionIndexArray[total]=partitionIndex;

#if FE_STA_DEBUG
			feLog("  %d/%d v %s n %s uv%s %s tri %d pt %d pr %d part %d\n",
					subIndex,subCount,
					c_print(m_pVertexArray[total]),
					c_print(m_pNormalArray[total]),
					spElementUV.isNull()? "": (vertexUVs? "(vtx)": "(pt)"),
					c_print(m_pUVArray[total]),
					m_pTriangleIndexArray[total],
					m_pPointIndexArray[total],
					m_pPrimitiveIndexArray[total],
					m_pPartitionIndexArray[total]);
#endif

			total++;

			//* pentagon: just add a cap to the quad box
			if(subIndex==4)
			{
				triangleIndex++;

				FEASSERT(triangleIndex<elementAllocated);
				FEASSERT(total+1<vertexAllocated);

				m_pElementArray[triangleIndex]=
						Vector3i(total-1,3,FALSE);

				I32 diff=(m_triangulation==SurfaceI::e_existingPoints)? 4: 4;
				m_pVertexArray[total]=m_pVertexArray[total-diff];
				m_pNormalArray[total]=m_pNormalArray[total-diff];
				m_pColorArray[total]=m_pColorArray[total-diff];
				m_pUVArray[total]=m_pUVArray[total-diff];
				m_pTriangleIndexArray[total]=triangleIndex;
				m_pPointIndexArray[total]=m_pPointIndexArray[total-diff];
				m_pPrimitiveIndexArray[total]=primitiveIndex;
				m_pPartitionIndexArray[total]=partitionIndex;
				total++;

				diff=(m_triangulation==SurfaceI::e_existingPoints)? 4: 14;
				m_pVertexArray[total]=m_pVertexArray[total-diff];
				m_pNormalArray[total]=m_pNormalArray[total-diff];
				m_pColorArray[total]=m_pColorArray[total-diff];
				m_pUVArray[total]=m_pUVArray[total-diff];
				m_pTriangleIndexArray[total]=triangleIndex;
				m_pPointIndexArray[total]=m_pPointIndexArray[total-diff];
				m_pPrimitiveIndexArray[total]=primitiveIndex;
				m_pPartitionIndexArray[total]=partitionIndex;
				total++;

			}

			if(subIndex==3)
			{
				if(m_triangulation==SurfaceI::e_existingPoints)
				{
					triangleIndex++;

					FEASSERT(triangleIndex<elementAllocated);
					FEASSERT(total+1<vertexAllocated);

					m_pElementArray[triangleIndex]=
							Vector3i(total-1,3,FALSE);

					m_pVertexArray[total]=m_pVertexArray[total-4];
					m_pNormalArray[total]=m_pNormalArray[total-4];
					m_pColorArray[total]=m_pColorArray[total-4];
					m_pUVArray[total]=m_pUVArray[total-4];
					m_pTriangleIndexArray[total]=triangleIndex;
					m_pPointIndexArray[total]=m_pPointIndexArray[total-4];
					m_pPrimitiveIndexArray[total]=primitiveIndex;
					m_pPartitionIndexArray[total]=partitionIndex;
					total++;

					m_pVertexArray[total]=m_pVertexArray[total-3];
					m_pNormalArray[total]=m_pNormalArray[total-3];
					m_pColorArray[total]=m_pColorArray[total-3];
					m_pUVArray[total]=m_pUVArray[total-3];
					m_pTriangleIndexArray[total]=triangleIndex;
					m_pPointIndexArray[total]=m_pPointIndexArray[total-3];
					m_pPrimitiveIndexArray[total]=primitiveIndex;
					m_pPartitionIndexArray[total]=partitionIndex;
					total++;
				}
				else
				{
					//* quartering

					total-=4;

					//* 0:	0  1  2
					//* 1:	3  4  5
					//* 2:	6  7  8
					//* 3:	9 10 11

					//* prevent backwards UV interpolation
					for(U32 pass=0;pass<2;pass++)
					{
						if(m_pUVArray[total][pass]>0.7 ||
								m_pUVArray[total+1][pass]>0.7 ||
								m_pUVArray[total+2][pass]>0.7 ||
								m_pUVArray[total+3][pass]>0.7)
						{
							for(U32 n=0;n<4;n++)
							{
								Real& rValue=m_pUVArray[total+n][pass];
								if(rValue<0.3)
								{
									rValue+=1.0;
								}
							}
						}
					}

					FEASSERT(triangleIndex<elementAllocated-2);
					FEASSERT(total+11<vertexAllocated);

					m_pElementArray[triangleIndex+1]=
							Vector3i(total+3,3,FALSE);
					m_pElementArray[triangleIndex+2]=
							Vector3i(total+6,3,FALSE);
					m_pElementArray[triangleIndex+3]=
							Vector3i(total+9,3,FALSE);

					//* TODO use TrianglePN for midpoint
#if FALSE
					TrianglePN<Real> trianglePN;
					SpatialVector sumVertex;
					SpatialVector sumNormal;
					set(sumVertex);
					set(sumNormal);
					for(U32 pass=0;pass<4;pass++)
					{
						trianglePN.configure(
							m_pVertexArray[total+pass],
							m_pVertexArray[total+(pass+1)%4],
							m_pVertexArray[total+(pass+2)%4],
							m_pNormalArray[total+pass],
							m_pNormalArray[total+(pass+1)%4],
							m_pNormalArray[total+(pass+2)%4]);
						sumVertex+=trianglePN.midpoint(
								TrianglePN<Real>::e_v3v1);
						sumNormal+=trianglePN.midnormal(
								TrianglePN<Real>::e_v3v1);
					}
					const SpatialVector meanVertex=0.25*sumVertex;
					const SpatialVector meanNormal=unitSafe(sumNormal);
#else
					const SpatialVector meanVertex=0.25*
							(m_pVertexArray[total]+
							m_pVertexArray[total+1]+
							m_pVertexArray[total+2]+
							m_pVertexArray[total+3]);

					const SpatialVector meanNormal=unitSafe(
							m_pNormalArray[total]+
							m_pNormalArray[total+1]+
							m_pNormalArray[total+2]+
							m_pNormalArray[total+3]);
#endif

					const Color meanColor=0.25*
							(m_pColorArray[total]+
							m_pColorArray[total+1]+
							m_pColorArray[total+2]+
							m_pColorArray[total+3]);

					const SpatialVector meanUV=0.25*
							(m_pUVArray[total]+
							m_pUVArray[total+1]+
							m_pUVArray[total+2]+
							m_pUVArray[total+3]);

					//* triangle 2
					m_pVertexArray[total+6]=m_pVertexArray[total+2];
					m_pNormalArray[total+6]=m_pNormalArray[total+2];
					m_pColorArray[total+6]=m_pColorArray[total+2];
					m_pUVArray[total+6]=m_pUVArray[total+2];
					m_pTriangleIndexArray[total+6]=triangleIndex+2;
					m_pPointIndexArray[total+6]=m_pPointIndexArray[total+2];
					m_pPrimitiveIndexArray[total+6]=primitiveIndex;
					m_pPartitionIndexArray[total+6]=partitionIndex;

					m_pVertexArray[total+7]=m_pVertexArray[total+3];
					m_pNormalArray[total+7]=m_pNormalArray[total+3];
					m_pColorArray[total+7]=m_pColorArray[total+3];
					m_pUVArray[total+7]=m_pUVArray[total+3];
					m_pTriangleIndexArray[total+7]=triangleIndex+2;
					m_pPointIndexArray[total+7]=m_pPointIndexArray[total+3];
					m_pPrimitiveIndexArray[total+7]=primitiveIndex;
					m_pPartitionIndexArray[total+7]=partitionIndex;

					//* triangle 3
					m_pVertexArray[total+9]=m_pVertexArray[total+3];
					m_pNormalArray[total+9]=m_pNormalArray[total+3];
					m_pColorArray[total+9]=m_pColorArray[total+3];
					m_pUVArray[total+9]=m_pUVArray[total+3];
					m_pTriangleIndexArray[total+9]=triangleIndex+3;
					m_pPointIndexArray[total+9]=m_pPointIndexArray[total+3];
					m_pPrimitiveIndexArray[total+9]=primitiveIndex;
					m_pPartitionIndexArray[total+9]=partitionIndex;

					m_pVertexArray[total+10]=m_pVertexArray[total];
					m_pNormalArray[total+10]=m_pNormalArray[total];
					m_pColorArray[total+10]=m_pColorArray[total];
					m_pUVArray[total+10]=m_pUVArray[total];
					m_pTriangleIndexArray[total+10]=triangleIndex+3;
					m_pPointIndexArray[total+10]=m_pPointIndexArray[total];
					m_pPrimitiveIndexArray[total+10]=primitiveIndex;
					m_pPartitionIndexArray[total+10]=partitionIndex;

					//* triangle 1
					m_pVertexArray[total+3]=m_pVertexArray[total+1];
					m_pNormalArray[total+3]=m_pNormalArray[total+1];
					m_pColorArray[total+3]=m_pColorArray[total+1];
					m_pUVArray[total+3]=m_pUVArray[total+1];
					m_pTriangleIndexArray[total+3]=triangleIndex+1;
					m_pPointIndexArray[total+3]=m_pPointIndexArray[total+1];
					m_pPrimitiveIndexArray[total+3]=primitiveIndex;
					m_pPartitionIndexArray[total+3]=partitionIndex;

					m_pVertexArray[total+4]=m_pVertexArray[total+2];
					m_pNormalArray[total+4]=m_pNormalArray[total+2];
					m_pColorArray[total+4]=m_pColorArray[total+2];
					m_pUVArray[total+4]=m_pUVArray[total+2];
					m_pTriangleIndexArray[total+4]=triangleIndex+1;
					m_pPointIndexArray[total+4]=m_pPointIndexArray[total+2];
					m_pPrimitiveIndexArray[total+4]=primitiveIndex;
					m_pPartitionIndexArray[total+4]=partitionIndex;

					//* mean
					m_pVertexArray[total+2]=meanVertex;
					m_pNormalArray[total+2]=meanNormal;
					m_pColorArray[total+2]=meanColor;
					m_pUVArray[total+2]=meanUV;
					m_pTriangleIndexArray[total+2]=triangleIndex;
					m_pPointIndexArray[total+2]= -1;
					m_pPrimitiveIndexArray[total+2]=primitiveIndex;
					m_pPartitionIndexArray[total+2]=partitionIndex;

					m_pVertexArray[total+5]=meanVertex;
					m_pNormalArray[total+5]=meanNormal;
					m_pColorArray[total+5]=meanColor;
					m_pUVArray[total+5]=meanUV;
					m_pTriangleIndexArray[total+5]=triangleIndex+1;
					m_pPointIndexArray[total+5]= -1;
					m_pPrimitiveIndexArray[total+5]=primitiveIndex;
					m_pPartitionIndexArray[total+5]=partitionIndex;

					m_pVertexArray[total+8]=meanVertex;
					m_pNormalArray[total+8]=meanNormal;
					m_pColorArray[total+8]=meanColor;
					m_pUVArray[total+8]=meanUV;
					m_pTriangleIndexArray[total+8]=triangleIndex+2;
					m_pPointIndexArray[total+8]= -1;
					m_pPrimitiveIndexArray[total+8]=primitiveIndex;
					m_pPartitionIndexArray[total+8]=partitionIndex;

					m_pVertexArray[total+11]=meanVertex;
					m_pNormalArray[total+11]=meanNormal;
					m_pColorArray[total+11]=meanColor;
					m_pUVArray[total+11]=meanUV;
					m_pTriangleIndexArray[total+11]=triangleIndex+3;
					m_pPointIndexArray[total+11]= -1;
					m_pPrimitiveIndexArray[total+11]=primitiveIndex;
					m_pPartitionIndexArray[total+11]=partitionIndex;

					if(primitiveIndex<m_hullFacePointArray.size())
					{
//						m_hullPointMap[m_hullFacePointArray[primitiveIndex][0]]=
//								total+0;
						m_hullPointMap[m_hullFacePointArray[primitiveIndex][1]]=
								total+3;
						m_hullPointMap[m_hullFacePointArray[primitiveIndex][2]]=
								total+6;
						m_hullPointMap[m_hullFacePointArray[primitiveIndex][3]]=
								total+9;
					}

					triangleIndex+=3;
					total+=12;


				}
			}
		}

		triangleIndex++;
	}

#if FE_STA_PROFILE
	spProfileResolve->replace(spProfileLoop);
#endif

	m_elements=triangleIndex;
	m_vertices=total;

	if(elementAllocated!=triangleIndex || vertexAllocated!=m_vertices)
	{
		resizeArrays(triangleIndex,m_vertices);
	}

	if(spElementUV.isNull())
	{
		deallocate(m_pUVArray);
		m_pUVArray=NULL;
	}

	if(spPointColor.isNull() && spPointAlpha.isNull() && spPrimitiveColor.isNull())
	{
		deallocate(m_pColorArray);
		m_pColorArray=NULL;
	}

	calcBoundingSphere();

#if FE_STA_PROFILE
	spProfileResolve->finish();
	spProfiler->end();

	feLog("%s:\n%s\n",spProfiler->name().c_str(),
			spProfiler->report().c_str());
#endif
}

sp<SurfaceI::GaugeI> SurfaceTrianglesAccessible::gauge(void)
{
	if(m_spSurfaceAccessibleI.isNull())
	{
		feLog("SurfaceTrianglesAccessible::gauge"
				" not bound to a SurfaceAccessibleI\n");
		return sp<SurfaceI::GaugeI>(NULL);
	}

	if(m_triangulation==SurfaceI::e_balanced)
	{
		feLog("SurfaceTrianglesAccessible::gauge"
				" not implemented for balanced tringulation (quartering)\n");
		return sp<SurfaceI::GaugeI>(NULL);
	}

	sp<SurfaceAccessorI> spPointPosition=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spVertices=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);

	if(spPointPosition.isNull() || spVertices.isNull())
	{
		return sp<SurfaceI::GaugeI>(NULL);
	}

	sp<Gauge> spGauge(new Gauge(sp<SurfaceI>(this)));

	if(m_spMesh.isNull())
	{
		const I32 pointCount=spPointPosition->count();
		const I32 primitiveCount=spVertices->count();

		Array<F64> points(pointCount*3);
		Array<U32> faces(primitiveCount*6);

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spPointPosition->spatialVector(pointIndex);

			const I32 pointIndex3=pointIndex*3;
			points[pointIndex3]=point[0];
			points[pointIndex3+1]=point[1];
			points[pointIndex3+2]=point[2];
		}

		I32 total=0;

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			//* NOTE tris or quads

			const I32 subCount=spVertices->subCount(primitiveIndex);
			if(subCount<3 || subCount>4)
			{
				feLog("SurfaceTrianglesAccessible::gauge"
						" prim %d/%d subCount %d (not tri or quad)\n",
						primitiveIndex,primitiveCount,subCount);
				continue;
			}

			faces[total]=spVertices->integer(primitiveIndex,0);
			faces[total+1]=spVertices->integer(primitiveIndex,1);
			faces[total+2]=spVertices->integer(primitiveIndex,2);
			total+=3;

			if(subCount>3)
			{
				faces[total]=spVertices->integer(primitiveIndex,0);
				faces[total+1]=spVertices->integer(primitiveIndex,2);
				faces[total+2]=spVertices->integer(primitiveIndex,3);
				total+=3;
			}
		}

		faces.resize(total);

		m_spMesh=spGauge->createMesh(points,faces);
	}
	else
	{
		spGauge->setMesh(m_spMesh);
	}

	return spGauge;
}

sp<Geodesic::Mesh> SurfaceTrianglesAccessible::Gauge::createMesh(
	Array<F64>& a_rPoints,Array<U32>& a_rFaces)
{
	return m_geodesic.createMesh(a_rPoints,a_rFaces);
}

void SurfaceTrianglesAccessible::Gauge::setMesh(sp<Geodesic::Mesh> a_spMesh)
{
	m_geodesic.setMesh(a_spMesh);
}

sp<Geodesic::Mesh> SurfaceTrianglesAccessible::Gauge::mesh(void)
{
	return m_geodesic.mesh();
}

void SurfaceTrianglesAccessible::Gauge::pick(SpatialVector a_source,
	Real a_maxDistance)
{
	m_source=a_source;
	m_maxDistance=a_maxDistance;

	I32 faceIndex=0;

	sp<ImpactI> spImpact=m_spSurfaceI->nearestPoint(a_source,-1.0);
	if(spImpact.isValid())
	{
		//* TODO use lookup table to skip degenerate primitives
		faceIndex=spImpact->face();
	}

	m_geodesic.setSource(faceIndex,a_source);
	m_geodesic.setMaxDistance(a_maxDistance);
}

Real SurfaceTrianglesAccessible::Gauge::distanceTo(SpatialVector a_target)
{
	if(magnitudeSquared(a_target-m_source)>m_maxDistance*m_maxDistance)
	{
		return INFINITY;
	}

	I32 faceIndex=0;
	sp<ImpactI> spImpact=m_spSurfaceI->nearestPoint(a_target,-1.0);
	if(spImpact.isValid())
	{
		//* TODO use lookup table to skip degenerate primitives
		faceIndex=spImpact->face();
	}

	return m_geodesic.distanceTo(faceIndex,a_target);
}

void SurfaceTrianglesAccessible::resolveImpact(sp<ImpactI> a_spImpactI) const
{
	SurfaceTriangles::resolveImpact(a_spImpactI);

	sp<Impact> spImpact(a_spImpactI);

	if(spImpact.isNull())
	{
		return;
	}

	const Barycenter<Real> bary=spImpact->barycenter();
	const Real bary2=Real(1)-bary[0]-bary[1];

	const I32 nearestPointIndex=bary[0]>bary[1]?
		(bary[0]>bary2? spImpact->pointIndex0(): spImpact->pointIndex2()):
		(bary[1]>bary2? spImpact->pointIndex1(): spImpact->pointIndex2());

	if(nearestPointIndex<0)
	{
		return;
	}

	sp<SurfaceAccessorI> spVertices=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);

	if(spVertices.isNull())
	{
		return;
	}

	const I32 primitiveIndex=spImpact->primitiveIndex();
	const I32 subCount=spVertices->subCount(primitiveIndex);
	for(I32 subIndex=0;subIndex<subCount;subIndex++)
	{
		if(spVertices->integer(primitiveIndex,subIndex)==nearestPointIndex)
		{
			spImpact->setNearestSubIndex(subIndex);
			break;
		}
	}
}


} /* namespace ext */
} /* namespace fe */
