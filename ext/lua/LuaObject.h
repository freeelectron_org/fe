/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaObject_h__
#define __lua_LuaObject_h__

namespace fe
{
namespace ext
{

class LuaBaseObject
{
	public:
		LuaBaseObject(void) {}
virtual	~LuaBaseObject(void) {}
		sp<LuaBaseType >	&type(void) { return m_spType; }
		LuaContext			*context(void) { return m_spType->context(); }
	protected:
		sp<LuaBaseType >	m_spType;
};

template<class T, FE_UWORD tid>
class LuaObject : public LuaBaseObject
{
	private:
		LuaObject(void) { feX("LuaObject::LuaObject", "should never call"); }
	public:
		typedef LuaObject<T,tid> t_luott;
		LuaObject(LuaContext *pContext, const T &data)
		{
			m_spType = pContext->lookup(tid);
			m_data = data;
			m_spType->pushMetaTable();
			lua_setmetatable(m_spType->state(), -2);
		}
virtual	~LuaObject(void){}
		// factory that pushes onto stack
static	LuaObject<T,tid> *push(LuaContext *pContext, const T &data)
		{
			void *pBlock = lua_newuserdata(pContext->state(),
				sizeof(t_luott));
			t_luott *pObj = new(pBlock)t_luott(pContext, data);
			return pObj;
		}
		T	&data(void) { return m_data; }
static	int		destroy(lua_State *pLuaState)
		{
			void *pV = lua_touserdata(pLuaState, -1);
			t_luott *pW = (t_luott *)pV;
			pW->~t_luott();
			return 0;
		}
static	t_luott	*selfIsFirst(lua_State *pLuaState)
		{
			if(!lua_isuserdata(pLuaState, 1))
			{
				LuaContext::error(pLuaState, "self not first parameter");
				return NULL;
			}
			t_luott *pObj = (t_luott *)lua_touserdata(pLuaState, 1);
			if(pObj->type() != pObj->context()->lookup(tid))
			{
				LuaContext::error(pLuaState, "self not first parameter");
				return NULL;
			}
			return pObj;
		}

static	String	argType(lua_State *pLuaState,U32 index)
				{
					if (!lua_getmetatable(pLuaState, 2))
					{
						return "<no metatable>";
					}

					lua_rawget(pLuaState, LUA_REGISTRYINDEX);
					return lua_tostring(pLuaState, -1);
				}
	protected:
		String	aliasedName(String a_name)
				{
					LuaContext* pContext=context();
					return pContext? pContext->aliasedName(a_name): a_name;
				}

	private:
		T					m_data;
};

/* ----------------------------------------------------------------------- */
// RECORD
typedef LuaObject<Record, LuaContext::e_record> t_lua_record;
class LuaRecordObject : public t_lua_record
{
	public:
virtual				~LuaRecordObject(void) {}
static		int		index(lua_State *pLuaState);
static		int		newindex(lua_State *pLuaState);
static		int		isValid(lua_State *pLuaState);
static		int		check(lua_State *pLuaState);
static		int		layout(lua_State *pLuaState);
static		int		clone(lua_State *pLuaState);
//static		int		deepclone(lua_State *pLuaState);
};


typedef LuaType<Record, LuaRecordObject> t_lua_record_t;
class LuaRecordType : public t_lua_record_t
{
	public:
					LuaRecordType(LuaContext *pContext,const String &mtn);
virtual				~LuaRecordType(void){}
virtual		void	pushNewMetaTable(void);
};


/* ----------------------------------------------------------------------- */
// LAYOUT
typedef LuaObject<sp<Layout>, LuaContext::e_layout> t_lua_layout;
class LuaLayoutObject : public t_lua_layout
{
	public:
virtual				~LuaLayoutObject(void) {}
static		int		index(lua_State *pLuaState);
static		int		scope(lua_State *pLuaState);
static		int		populate(lua_State *pLuaState);
static		int		createRecord(lua_State *pLuaState);
static		int		name(lua_State *pLuaState);
};

typedef LuaType<sp<Layout>, LuaLayoutObject> t_lua_layout_t;
class LuaLayoutType : public t_lua_layout_t
{
	public:
					LuaLayoutType(LuaContext *pContext,const String &mtn);
virtual				~LuaLayoutType(void){}
};

/* ----------------------------------------------------------------------- */
// SCOPE
typedef LuaObject<sp<Scope>, LuaContext::e_scope> t_lua_scope;
class LuaScopeObject : public t_lua_scope
{
	public:
virtual				~LuaScopeObject(void) {}
static		int		index(lua_State *pLuaState);
static		int		declare(lua_State *pLuaState);
static		int		createRecord(lua_State *pLuaState);
};

typedef LuaType<sp<Scope>, LuaScopeObject> t_lua_scope_t;
class LuaScopeType : public t_lua_scope_t
{
	public:
					LuaScopeType(LuaContext *pContext,const String &mtn);
virtual				~LuaScopeType(void){}
};

/* ----------------------------------------------------------------------- */
// RECORD GROUP
typedef LuaObject<sp<RecordGroup>, LuaContext::e_group> t_lua_group;
class LuaGroupObject : public t_lua_group
{
	public:
virtual				~LuaGroupObject(void) {}
static		int		index(lua_State *pLuaState);
static		int		iterator(lua_State *pLuaState);
static		int		newiterator(lua_State *pLuaState);
static		int		length(lua_State *pLuaState);
static		int		add(lua_State *pLuaState);
static		int		remove(lua_State *pLuaState);
static		int		lookup(lua_State *pLuaState);
static		int		setWeak(lua_State *pLuaState);
static		int		clear(lua_State *pLuaState);
static		int		array(lua_State *pLuaState);
};

typedef LuaType<sp<RecordGroup>, LuaGroupObject> t_lua_group_t;
class LuaGroupType : public t_lua_group_t
{
	public:
					LuaGroupType(LuaContext *pContext,const String &mtn);
virtual				~LuaGroupType(void){}
virtual		void	pushNewMetaTable(void);
};

/* ----------------------------------------------------------------------- */
// RECORD GROUP ITERATOR
typedef LuaObject<RecordGroup::iterator, LuaContext::e_groupit> t_lua_groupit;
class LuaGroupItObject : public t_lua_groupit
{
	public:
virtual				~LuaGroupItObject(void) {}
static		int		index(lua_State *pLuaState);
};

typedef LuaType<RecordGroup::iterator, LuaGroupItObject> t_lua_groupit_t;
class LuaGroupItType : public t_lua_groupit_t
{
	public:
					LuaGroupItType(LuaContext *pContext,const String &mtn);
virtual				~LuaGroupItType(void){}
};

/* ----------------------------------------------------------------------- */
// RECORD Array
typedef LuaObject<sp<RecordArray>, LuaContext::e_array> t_lua_array;
class LuaArrayObject : public t_lua_array
{
	public:
virtual				~LuaArrayObject(void) {}
static		int		index(lua_State *pLuaState);
static		int		iterator(lua_State *pLuaState);
static		int		newiterator(lua_State *pLuaState);
static		int		length(lua_State *pLuaState);
static		int		check(lua_State *pLuaState);
static		int		add(lua_State *pLuaState);
static		int		remove(lua_State *pLuaState);
};

typedef LuaType<sp<RecordArray>, LuaArrayObject> t_lua_array_t;
class LuaArrayType : public t_lua_array_t
{
	public:
					LuaArrayType(LuaContext *pContext,const String &mtn);
virtual				~LuaArrayType(void){}
virtual		void	pushNewMetaTable(void);
};

class LuaArrayContext
{
	public:
			sp<RecordArray>	m_spRecordArray;
			I32				m_index;
};

typedef LuaObject<LuaArrayContext, LuaContext::e_arrayit> t_lua_arrayit;
class LuaArrayItObject : public t_lua_arrayit
{
	public:
virtual				~LuaArrayItObject(void) {}
static		int		index(lua_State *pLuaState);
};

typedef LuaType<LuaArrayContext, LuaArrayItObject> t_lua_arrayit_t;
class LuaArrayItType : public t_lua_arrayit_t
{
	public:
					LuaArrayItType(LuaContext *pContext,const String &mtn);
virtual				~LuaArrayItType(void){}
};

/* ----------------------------------------------------------------------- */
// integer array
typedef LuaObject< Array<I32>*, LuaContext::e_intarray> t_lua_intarray;
class LuaIntArrayObject : public t_lua_intarray
{
	public:
virtual				~LuaIntArrayObject(void)								{}
static		int		index(lua_State *pLuaState);
static		int		iterator(lua_State *pLuaState);
static		int		newiterator(lua_State *pLuaState);
static		int		length(lua_State *pLuaState);
static		int		add(lua_State *pLuaState);
static		int		resize(lua_State *pLuaState);
};

typedef LuaType< Array<I32>* , LuaIntArrayObject> t_lua_intarray_t;
class LuaIntArrayType : public t_lua_intarray_t
{
	public:
					LuaIntArrayType(LuaContext *pContext,const String &mtn);
virtual				~LuaIntArrayType(void)									{}
virtual		void	pushNewMetaTable(void);
};

class LuaIntArrayContext
{
	public:
			Array<I32>*	m_pIntArray;
			I32			m_index;
};

typedef LuaObject<LuaIntArrayContext,
	LuaContext::e_intarrayit> t_lua_intarrayit;
class LuaIntArrayItObject : public t_lua_intarrayit
{
	public:
virtual				~LuaIntArrayItObject(void)								{}
static		int		index(lua_State *pLuaState);
};

typedef LuaType<LuaIntArrayContext, LuaIntArrayItObject> t_lua_intarrayit_t;
class LuaIntArrayItType : public t_lua_intarrayit_t
{
	public:
					LuaIntArrayItType(LuaContext *pContext,const String &mtn);
virtual				~LuaIntArrayItType(void)								{}
};

/* ----------------------------------------------------------------------- */
// float vector
typedef LuaObject<float *, LuaContext::e_vector> t_lua_vector;
class LuaVectorObject : public t_lua_vector
{
	public:
virtual				~LuaVectorObject(void) {}
static		int		newindex(lua_State *pLuaState);
static		int		index(lua_State *pLuaState);
};

typedef LuaType<float *, LuaVectorObject> t_lua_vector_t;
class LuaVectorType : public t_lua_vector_t
{
	public:
					LuaVectorType(LuaContext *pContext,const String &mtn);
virtual				~LuaVectorType(void){}
virtual		void	pushNewMetaTable(void);
};

/* ----------------------------------------------------------------------- */
// StateCatalog specialization
typedef LuaObject< sp<StateCatalog>, LuaContext::e_statecatalog> t_lua_statecatalog;
class LuaStateCatalogObject : public t_lua_statecatalog
{
	public:
virtual				~LuaStateCatalogObject(void) {}
static		int		index(lua_State *pLuaState);
static		int		getState(lua_State *pLuaState);
static		int		setState(lua_State *pLuaState);
};

typedef LuaType< sp<StateCatalog>, LuaStateCatalogObject> t_lua_statecatalog_t;
class LuaStateCatalogType : public t_lua_statecatalog_t
{
	public:
					LuaStateCatalogType(LuaContext *pContext,
							const String &mtn);
virtual				~LuaStateCatalogType(void){}
};

template<class T, FE_UWORD tid>
class LuaDeletableObject : public LuaObject<T,tid>
{
	public:
		typedef LuaDeletableObject<T,tid> t_luott;

		LuaDeletableObject(LuaContext *pContext,
			const T &data, BWORD a_deletable):
			LuaObject<T,tid>(pContext,data),
			m_deletable(a_deletable)										{}

static	LuaObject<T,tid> *push(LuaContext *pContext, const T &data,
				BWORD a_deletable=FALSE)
		{
			void *pBlock = lua_newuserdata(pContext->state(),
				sizeof(t_luott));
			t_luott *pObj = new(pBlock)t_luott(pContext, data, a_deletable);
			return pObj;
		}

static	int		destroy(lua_State *pLuaState)
		{
			void *pV = lua_touserdata(pLuaState, -1);
			t_luott *pW = (t_luott *)pV;

			if(pW->m_deletable)
			{
				T pT=pW->data();
				delete pT;
			}

			pW->~t_luott();
			return 0;
		}
	BWORD	m_deletable;
};

/* ----------------------------------------------------------------------- */
// SpatialTransform specialization
typedef LuaDeletableObject<SpatialTransform*,
	LuaContext::e_transform> t_lua_transform;
class LuaTransformObject : public t_lua_transform
{
	public:
virtual				~LuaTransformObject(void) {}
static		int		newindex(lua_State *pLuaState);
static		int		index(lua_State *pLuaState);
static		int		eq(lua_State *pLuaState);
static		int		copy(lua_State *pLuaState);
static		int		set(lua_State *pLuaState);
};

typedef LuaType<SpatialTransform*, LuaTransformObject> t_lua_transform_t;
class LuaTransformType : public t_lua_transform_t
{
	public:
					LuaTransformType(LuaContext *pContext,
							const String &mtn);
virtual				~LuaTransformType(void){}
virtual		void	pushNewMetaTable(void);
};

/* ----------------------------------------------------------------------- */
// Vector<> specialization
template<int N, class T, FE_UWORD tid>
class LuaVectorNObject :
	public LuaDeletableObject<Vector<N,T>*, tid>
{
	public:
virtual				~LuaVectorNObject(void) {}
static		int		newindex(lua_State *pLuaState);
static		int		index(lua_State *pLuaState);
static		int		eq(lua_State *pLuaState);
static		int		unm(lua_State *pLuaState);
static		int		add(lua_State *pLuaState);
static		int		sub(lua_State *pLuaState);
static		int		mul(lua_State *pLuaState);
static		int		copy(lua_State *pLuaState);
static		int		unit(lua_State *pLuaState);
static		int		normalize(lua_State *pLuaState);
static		int		magnitude(lua_State *pLuaState);
static		int		dot(lua_State *pLuaState);
static		int		cross3(lua_State *pLuaState);
static		int		set(lua_State *pLuaState);
};

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::index(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj = (LuaVectorNObject<N,T,tid> *)
			LuaObject<T,tid>::selfIsFirst(pLuaState);
	if(!pObj)
	{
		return 0;
	}

	if(lua_isnumber(pLuaState, -1))
	{
		int index = (int)lua_tonumber(pLuaState, -1);
		lua_pop(pLuaState, 1);
		lua_pushnumber(pLuaState, (*pObj->data())[index]);
		return 1;
	}

	String methodName = lua_tostring(pLuaState, -1);

	lua_pop(pLuaState, 1);

	if(!pObj->type()->pushMethod(methodName))
	{
		return 0;
	}

	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::newindex(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj|| !lua_isnumber(pLuaState, -2))
	{
		return 0;
	}

	int index = (int)lua_tonumber(pLuaState, -2);
	float value = (float)lua_tonumber(pLuaState, -1);

	setAt(*pObj->data(),index,(Real)value);

	lua_pushnumber(pLuaState, value);
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::eq(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaVectorNObject<N,T,tid>* pOther;
	if((pOther=reinterpret_cast<LuaVectorNObject<N,T,tid>*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		lua_pushboolean(pLuaState,*pObj->data() == *pOther->data());
		return 1;
	}

	feLog("LuaVectorNObject::eq incompatible argType \"%s\"\n",
			LuaObject<T,tid>::argType(pLuaState,2).c_str());
	return 0;
}

//* unary negation
template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::unm(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	Vector<N,T>* pNegative=new Vector<N,T>(-(*pObj->data()));
	LuaVectorNObject<N,T,tid>::push(pObj->context(),pNegative,TRUE);
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::add(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaVectorNObject<N,T,tid>* pOther;
	if((pOther=reinterpret_cast<LuaVectorNObject<N,T,tid>*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		Vector<N,T>* pSum=new Vector<N,T>(*pObj->data() + *pOther->data());
		LuaVectorNObject<N,T,tid>::push(pObj->context(),pSum,TRUE);
		return 1;
	}

	feLog("LuaVectorNObject::add incompatible argType \"%s\"\n",
			LuaObject<T,tid>::argType(pLuaState,2).c_str());
	return 0;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::sub(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaVectorNObject<N,T,tid>* pOther;
	if((pOther=reinterpret_cast<LuaVectorNObject<N,T,tid>*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		Vector<N,T>* pDiff=new Vector<N,T>(*pObj->data() - *pOther->data());
		LuaVectorNObject<N,T,tid>::push(pObj->context(),pDiff,TRUE);
		return 1;
	}

	feLog("LuaVectorNObject::sub incompatible argType \"%s\"\n",
			LuaObject<T,tid>::argType(pLuaState,2).c_str());
	return 0;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::mul(lua_State *pLuaState)
{
	I32 scalarPos= -1;
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		scalarPos= -2;
		pObj = (LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 2);
	}
	if(!pObj)
	{
		return 0;
	}

	F32 scalar=0.0f;
	if(lua_isnumber(pLuaState,scalarPos))
	{
		scalar = (F32)lua_tonumber(pLuaState,scalarPos);
	}
	else
	{
		return 0;
	}

	Vector<N,T>* pSum=new Vector<N,T>(*pObj->data() * (Real)scalar);
	LuaVectorNObject<N,T,tid>::push(pObj->context(),pSum,TRUE);
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::copy(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	Vector<N,T>* pDuplicate=new Vector<N,T>(*pObj->data());
	LuaVectorNObject<N,T,tid>::push(pObj->context(),pDuplicate,TRUE);
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::unit(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	Vector<N,T>* pNormal=new Vector<N,T>(fe::unit(*pObj->data()));
	LuaVectorNObject<N,T,tid>::push(pObj->context(),pNormal,TRUE);
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::normalize(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	fe::normalize(*pObj->data());
	LuaVectorNObject<N,T,tid>::push(pObj->context(),pObj->data());
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::magnitude(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	F32 length=fe::magnitude(*pObj->data());
	lua_pushnumber(pLuaState, length);
	return 1;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::dot(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaVectorNObject<N,T,tid>* pOther;
	if((pOther=reinterpret_cast<LuaVectorNObject<N,T,tid>*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		F32 dot=fe::dot(*pObj->data(),*pOther->data());
		lua_pushnumber(pLuaState, dot);
		return 1;
	}

	feLog("LuaVectorNObject::dot incompatible argType \"%s\"\n",
			LuaObject<T,tid>::argType(pLuaState,2).c_str());
	return 0;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::cross3(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaVectorNObject<N,T,tid>* pOther;
	if((pOther=reinterpret_cast<LuaVectorNObject<N,T,tid>*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
//		Vector<N,T>* pCross=
//				new Vector<N,T>(fe::cross3(*pObj->data(),*pOther->data()));

		Vector<3,T> lhs(*pObj->data());
		Vector<3,T> rhs(*pOther->data());
		Vector<3,T> result=fe::cross3(lhs,rhs);
		Vector<N,T>* pCross=new Vector<N,T>(result);

		LuaVectorNObject<N,T,tid>::push(pObj->context(),pCross,TRUE);
		return 1;
	}

	feLog("LuaVectorNObject::cross3 incompatible argType \"%s\"\n",
			LuaObject<T,tid>::argType(pLuaState,2).c_str());
	return 0;
}

template<int N, class T, FE_UWORD tid>
int LuaVectorNObject<N,T,tid>::set(lua_State *pLuaState)
{
	LuaVectorNObject<N,T,tid> *pObj =
			(LuaVectorNObject<N,T,tid> *)lua_touserdata(pLuaState, 1);
	if(!pObj)
	{
		return 0;
	}

	LuaVectorNObject<N,T,tid>* pOther;
	if((pOther=reinterpret_cast<LuaVectorNObject<N,T,tid>*>
			(luaL_checkudata(pLuaState,2,pObj->type()->name().c_str()))))
	{
		*pObj->data()=*pOther->data();
		LuaVectorNObject<N,T,tid>::push(pObj->context(),pObj->data());
		return 1;
	}

	Vector<N,T>& rVector = *pObj->data();
	fe::set(rVector);

	if(lua_isnumber(pLuaState, 2))
	{
		rVector[0]=(Real)lua_tonumber(pLuaState, 2);
	}
	if(N>1 && lua_isnumber(pLuaState, 3))
	{
		rVector[1]=(Real)lua_tonumber(pLuaState, 3);
	}
	if(N>2 && lua_isnumber(pLuaState, 4))
	{
		rVector[2]=(Real)lua_tonumber(pLuaState, 4);
	}
	if(N>3 && lua_isnumber(pLuaState, 5))
	{
		rVector[3]=(Real)lua_tonumber(pLuaState, 5);
	}

	LuaVectorNObject<N,T,tid>::push(pObj->context(),pObj->data());
	return 1;
}

template<int N, class T, FE_UWORD tid>
class LuaVectorNType:
	public LuaType< Vector<N,T>*, LuaVectorNObject<N,T,tid> >
{
	public:
					LuaVectorNType(LuaContext *pContext,const String &mtn);
virtual				~LuaVectorNType(void){}
virtual		void	pushNewMetaTable(void);
};

template<int N, class T, FE_UWORD tid>
inline LuaVectorNType<N,T,tid>::LuaVectorNType(
	LuaContext *pContext,const String &mtn):
	LuaType< Vector<N,T>*, LuaVectorNObject<N,T,tid> >(pContext, mtn)
{
	//* unit normalize magnitude dot cross3 set
	LuaBaseType::setMethod("copy",		LuaVectorNObject<N,T,tid>::copy);
	LuaBaseType::setMethod("unit",		LuaVectorNObject<N,T,tid>::unit);
	LuaBaseType::setMethod("normalize",	LuaVectorNObject<N,T,tid>::normalize);
	LuaBaseType::setMethod("magnitude",	LuaVectorNObject<N,T,tid>::magnitude);
	LuaBaseType::setMethod("dot",		LuaVectorNObject<N,T,tid>::dot);
	LuaBaseType::setMethod("cross3",	LuaVectorNObject<N,T,tid>::cross3);
	LuaBaseType::setMethod("set",		LuaVectorNObject<N,T,tid>::set);
}

template<int N, class T, FE_UWORD tid>
void LuaVectorNType<N,T,tid>::pushNewMetaTable(void)
{
	lua_State *pLuaState = LuaVectorNType<N,T,tid>::m_pContext->state();

//	lua_newtable(pLuaState);
	luaL_newmetatable(pLuaState,LuaVectorNType<N,T,tid>::m_mtname.c_str());

	// call index newindex add sub mul div unm pow concat lt le eq

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::destroy));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::index));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__newindex");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::newindex));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__eq");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::eq));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__unm");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::unm));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__add");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::add));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__sub");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::sub));
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__mul");
	lua_pushcfunction(pLuaState, (LuaVectorNObject<N,T,tid>::mul));
	lua_settable(pLuaState, -3);
}

/* ----------------------------------------------------------------------- */
// Vector4d specialization
typedef LuaVectorNObject<4,double,LuaContext::e_vector4d> t_lua_vector4d;
class LuaVector4dObject : public t_lua_vector4d
{
	public:
virtual				~LuaVector4dObject(void) {}
};

typedef LuaVectorNType<4,double,LuaContext::e_vector4d> t_lua_vector4d_t;
class LuaVector4dType : public t_lua_vector4d_t
{
	public:
					LuaVector4dType(LuaContext *pContext,const String &mtn):
						LuaVectorNType<4,double,LuaContext::e_vector4d>(
						pContext,mtn)										{}
};

/* ----------------------------------------------------------------------- */
// Vector4f specialization
typedef LuaVectorNObject<4,float,LuaContext::e_vector4f> t_lua_vector4f;
class LuaVector4fObject : public t_lua_vector4f
{
	public:
virtual				~LuaVector4fObject(void) {}
};

typedef LuaVectorNType<4,float,LuaContext::e_vector4f> t_lua_vector4f_t;
class LuaVector4fType : public t_lua_vector4f_t
{
	public:
					LuaVector4fType(LuaContext *pContext,const String &mtn):
						LuaVectorNType<4,float,LuaContext::e_vector4f>(
						pContext,mtn)										{}
};

/* ----------------------------------------------------------------------- */
// Vector3d specialization
typedef LuaVectorNObject<3,double,LuaContext::e_vector3d> t_lua_vector3d;
class LuaVector3dObject : public t_lua_vector3d
{
	public:
virtual				~LuaVector3dObject(void) {}
};

typedef LuaVectorNType<3,double,LuaContext::e_vector3d> t_lua_vector3d_t;
class LuaVector3dType : public t_lua_vector3d_t
{
	public:
					LuaVector3dType(LuaContext *pContext,const String &mtn):
						LuaVectorNType<3,double,LuaContext::e_vector3d>(
						pContext,mtn)										{}
};

/* ----------------------------------------------------------------------- */
// Vector3f specialization
typedef LuaVectorNObject<3,float,LuaContext::e_vector3f> t_lua_vector3f;
class LuaVector3fObject : public t_lua_vector3f
{
	public:
virtual				~LuaVector3fObject(void) {}
};

typedef LuaVectorNType<3,float,LuaContext::e_vector3f> t_lua_vector3f_t;
class LuaVector3fType : public t_lua_vector3f_t
{
	public:
					LuaVector3fType(LuaContext *pContext,const String &mtn):
						LuaVectorNType<3,float,LuaContext::e_vector3f>(
						pContext,mtn)										{}
};

/* ----------------------------------------------------------------------- */
// Vector2d specialization
typedef LuaVectorNObject<2,double,LuaContext::e_vector2d> t_lua_vector2d;
class LuaVector2dObject : public t_lua_vector2d
{
	public:
virtual				~LuaVector2dObject(void) {}
};

typedef LuaVectorNType<2,double,LuaContext::e_vector2d> t_lua_vector2d_t;
class LuaVector2dType : public t_lua_vector2d_t
{
	public:
					LuaVector2dType(LuaContext *pContext,const String &mtn):
						LuaVectorNType<2,double,LuaContext::e_vector2d>(
						pContext,mtn)										{}
};

/* ----------------------------------------------------------------------- */
// Vector2f specialization
typedef LuaVectorNObject<2,float,LuaContext::e_vector2f> t_lua_vector2f;
class LuaVector2fObject : public t_lua_vector2f
{
	public:
virtual				~LuaVector2fObject(void) {}
};

typedef LuaVectorNType<2,float,LuaContext::e_vector2f> t_lua_vector2f_t;
class LuaVector2fType : public t_lua_vector2f_t
{
	public:
					LuaVector2fType(LuaContext *pContext,const String &mtn):
						LuaVectorNType<2,float,LuaContext::e_vector2f>(
						pContext,mtn)										{}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __lua_LuaObject_h__ */

