/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexArchitectureDL"));
	list.append(new String("fexDataToolDL"));
	list.append(new String("fexShapeDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	String path=spMaster->catalog()->catalog<String>(
			"path:media")+"/template/element.rg";

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<BruteCollide>("HandlerI.BruteCollide.fe");
	pLibrary->add<DrawRod>("HandlerI.DrawRod.fe");
	pLibrary->add<DrawTerrain>("HandlerI.DrawTerrain.fe");
	pLibrary->add<Gravity>("HandlerI.Gravity.fe");
	pLibrary->add<NewtonEuler>("HandlerI.NewtonEuler.fe");
	pLibrary->add<Provot>("HandlerI.Provot.fe");
	pLibrary->add<Swim>("HandlerI.Swim.fe");
	pLibrary->add<Terrain>("HandlerI.Terrain.fe");

	pLibrary->add<Particle>("RecordFactoryI.Particle.fe");
	pLibrary->add<Explosion>("RecordFactoryI.Explosion.fe");
	pLibrary->add<Rod>("RecordFactoryI.Rod.fe");

	sp<Scope> spScope=spMaster->catalog()->catalogComponent("Scope","SimScope");
	RecordView::loadRecordGroup(spScope,path);

	//* retain for reference
#if FALSE
	pLibrary->add< Modifier<Arena,Arena::e_group,
			Drag> >("HandlerI.DragModifier.fe");
#endif

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
