import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return [    "operate" ]

def setup(module):
    srcList = [ "operator.pmh",
                "BlendShapeOp",
                "BloatOp",
                "ClaspOp",
                "ConnectOp",
                "CurveCreateOp",
                "CurveFollowOp",
                "CurveSampleOp",
                "DeleteOp",
                "ExcarnateOp",
                "ExportOp",
                "ExtractOp",
                "FollicleOp",
                "FuseOp",
                "GroupDeleteOp",
                "GroupOp",
                "GroupPromoteOp",
                "HammerOp",
                "HingeOp",
                "HobbleOp",
                "InfluenceOp",
                "ImportOp",
                "BendOp",
                "LengthCorrectOp",
                "MapOp",
                "MatchSizeOp",
                "MergeOp",
                "MirrorOp",
                "NoiseOp",
                "NullOp",
                "OffsetOp",
                "OpenSubdivOp",
                "PartitionOp",
                "PoisonOp",
                "PuppetOp",
                "QuiltOp",
                "RasterOp",
                "RulerOp",
                "SmoothOp",
                "SpreadsheetOp",
                "StashOp",
                "SubdivideOp",
                "SurfaceAttrConformOp",
                "SurfaceAttrCopyOp",
                "SurfaceAttrCreateOp",
                "SurfaceAttrDeleteOp",
                "SurfaceAttrLabOp",
                "SurfaceAttrPromoteOp",
                "SurfaceAttrRampOp",
                "SurfaceBindOp",
                "SurfaceCopyOp",
                "SurfaceDeltaOp",
                "SurfaceMetricOp",
                "SurfaceNormalOp",
                "SurfaceProxyOp",
                "SurfaceSampleOp",
                "SurfaceSummaryOp",
                "SurfaceWalkOp",
                "SurfaceWrapOp",
                "TransformManipulator",
                "TransformOp",
                "TubeOp",
                "ValidateOp",
                "ValueManipulator",
                "operatorDL" ]

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")
    for src in srcList:
            if re.compile(r'(.*)Op$').match(src):
                with open(manifest, 'a') as outfile:
                    outfile.write('\tspManifest->catalog<String>(\n'+
                            '\t\t\t"OperatorSurfaceI.' + src + '.fe")='+
                            '"fexOperatorDL";\n');

    dll = module.DLL( "fexOperatorDL", srcList )

    deplibs = forge.corelibs+ [
                "fexGeometryDLLib",
                "fexOperateDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexOperatorDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOperatorDL",                    None,   None) ]

    module.Module('test')
