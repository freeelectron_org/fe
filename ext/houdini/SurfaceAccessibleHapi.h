/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_SurfaceAccessibleHapi_h__
#define __houdini_SurfaceAccessibleHapi_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Houdini Engine Surface Binding

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleHapi:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleHapi>
{
	public:
								SurfaceAccessibleHapi(void);
virtual							~SurfaceAccessibleHapi(void);

								using SurfaceAccessibleBase::load;

								//* as SurfaceAccessibleI
virtual	BWORD					isBound(void);

								using SurfaceAccessibleBase::attributeSpecs;
virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::accessor;
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

static	String					lastError(HAPI_Session* a_pHapiSession);

	private:
		sp<SurfaceAccessorHapi>	accessor(void);

		std::map<String,String>			m_optionMap;
		Real							m_frame;

		HAPI_Session					m_hapiSession;
		HAPI_NodeId						m_hapiNodeId;

		I32								m_curveParts;
		I32								m_meshParts;

		Array<SurfaceAccessibleI::Spec>	m_pointSpecs;
		Array<SurfaceAccessibleI::Spec>	m_vertexSpecs;
		Array<SurfaceAccessibleI::Spec>	m_primitiveSpecs;
		Array<SurfaceAccessibleI::Spec>	m_detailSpecs;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_SurfaceAccessibleHapi_h__ */
