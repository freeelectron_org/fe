/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa_test.pmh"

#include "platform/dlCore.cc"

using namespace fe;
using namespace ext;

extern "C"
{
    FE_DL_EXPORT void ListDependencies(List<String *> &list)
    {
        list.append(new String("feAutoLoadDL"));
    }

    FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
    {
		Library *pLibrary = Memory::instantiate<Library>();

        pLibrary->add<OrbiterSystem>("Stepper.OrbiterSystem.fe_test");

        return pLibrary;
    }

    FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
    {
    }
}
