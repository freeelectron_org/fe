/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTransform_h__
#define __surface_SurfaceTransform_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SurfaceTransform RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTransform:
	virtual public SurfacePoint,
	public CastableAs<SurfaceTransform>
{
	public:
		Functor<SpatialVector>		direction;
		Functor<SpatialVector>		left;

				SurfaceTransform(void)		{ setName("SurfaceTransform"); }
virtual	void	addFunctors(void)
				{
					SurfacePoint::addFunctors();

					add(direction,	FE_SPEC("spc:dir",
							"Orientation facing forward"));
					add(left,		FE_SPEC("spc:left",
							"Orientation to side from direction"));
				}
virtual	void	populate(sp<Layout> spLayout)
				{
					SurfacePoint::populate(spLayout);
				}
virtual	void	initializeRecord(void)
				{
					SurfacePoint::initializeRecord();

					set(direction(),1.0f,0.0f,0.0f);
					set(left(),0.0f,1.0f,0.0f);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTransform_h__ */



