/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawNodes.h"

namespace fe
{

DrawNodes::DrawNodes(void)
{
}

DrawNodes::~DrawNodes(void)
{
}

void DrawNodes::initialize(void)
{
}

void DrawNodes::setColors(sp<RecordGroup> rg_in)
{
	m_asColor.get(m_color[0],"foreground",	rg_in,Vector4f(1.0f,1.0f,1.0f));
	m_asColor.get(m_color[1],"background",	rg_in,Vector4f(0.0f,0.0f,0.0f));
}

void DrawNodes::bind(fe::sp<fe::SignalerI> spSignalerI, fe::sp<fe::Layout> l_sig)
{
	m_asColor.bind(l_sig->scope());
}

void DrawNodes::handle(fe::Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	setColors(m_drawview.group());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	Matrix3x4f transform;
	Vector4f scale(1.0f,1.0f,1.0f);

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asParticle.bind(spRA->layout()->scope());
		m_asNode.bind(spRA->layout()->scope());
		if(m_asNode.check(spRA) && m_asParticle.location.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				transform = Matrix3x4f::identity();
				translate(transform, m_asParticle.location(spRA, i));
				m_drawview.drawI()->drawCircle(transform,&scale,m_color[0]);
			}
		}
	}

}





} /* namespace */

