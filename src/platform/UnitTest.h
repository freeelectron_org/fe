/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define UNIT_START()			fe::UnitTest unitTest;
#define UNIT_TEST(expression)	unitTest(expression,#expression)
#define UNIT_TRACK(number)		unitTest.track(number);
#define UNIT_RETURN()			return unitTest.failures();

namespace fe
{

/**************************************************************************//**
	@brief Counts success and failures in a style similar to assertions

	@ingroup platform

	Instantiating the object starts unit testing for the scope.

	Upon destruction, a report is sent to the global logger.

	The block contains any number of tests.

	By convention, a stand-alone unit test executable should return the
	number of failures and the exit status.

	@code
	main()
	{
		fe::UnitTest unitTest;

		unitTest(TRUE,"TRUE is non-zero");

		unitTest.track(1);
		return unitTest.failures();
	}
	@endcode

	For convenience a set of macros is provided:
	@code
	main()
	{
		UNIT_START();

		UNIT_TEST(TRUE);

		UNIT_TRACK(1);
		UNIT_RETURN();
	}
	@endcode


*//***************************************************************************/
class FE_DL_PUBLIC UnitTest
{
	public:
				UnitTest(void):
					m_successes(0),
					m_failures(0),
					m_attempts(0)

				{
					//feRegisterSegvHandler();
				}
virtual			~UnitTest(void)
				{
					feLogDirect("[31m\nResults: %d tests ",m_attempts);
					if(m_failures > 0)
					{
						feLogDirect("%d FAILED",m_failures);
					}
					else
					{
						feLogDirect("ALL PASSED");
					}
					feLogDirect("[0m\n\n");
				}
				/// @brief Checks the "assertion" and log a message if it fails
virtual	void	operator()(bool success, const char *label="")
				{
					if(success)
					{
						m_successes++;
					}
					else
					{
						feLogDirect("[31mUnitTest(%s) failed[0m\n",label);
						m_failures++;
					}
					m_attempts++;
				}

				/// @brief Return the number of failures so far
		int		failures(void) { return m_failures; }

				/** @brief Compare the number of tests run to a reference

					Returns true if the numbers matched. */
		int		track(int expected)
				{
					BWORD equal=true;

					if(m_attempts!=expected)
					{
						feLogDirect("[31m\nWarning: Test count inconsistent!"
								"  Expected %d, but executed %d.",
								expected,m_attempts);
						equal=false ;
					}

					feLogDirect("[0m\n");

					return equal;
				}

	private:
		int		m_successes;
		int		m_failures;
		int		m_attempts;
};

} // namespace
