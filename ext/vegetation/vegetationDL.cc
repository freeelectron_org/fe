/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

#include "fe/plugin.h"
#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOperateDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	//* TODO check for redundancy
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<DrawPlant>("HandlerI.DrawPlant.fe");
	pLibrary->add<SimPlant>("HandlerI.SimPlant.fe");
	pLibrary->add<Tree>("PlantModelI.Tree.fe");
	pLibrary->add<PlantSketch>("SketchI.PlantSketch.fe");

	pLibrary->add<Plant>("RecordFactoryI.Plant.fe");
	pLibrary->add<TreeSeed>("RecordFactoryI.TreeSeed.fe");
	pLibrary->add<TreeLevel>("RecordFactoryI.TreeLevel.fe");
	pLibrary->add<StickLevel>("RecordFactoryI.StickLevel.fe");
	pLibrary->add<LeafLevel>("RecordFactoryI.LeafLevel.fe");

	pLibrary->add<LimberOp>("OperatorSurfaceI.LimberOp.fe");
	pLibrary->add<TreeOp>("OperatorSurfaceI.TreeOp.fe");

	sp<Scope> spScope=
			spMaster->catalog()->catalogComponent("Scope","PlantScope");
	const String path=spMaster->catalog()->catalog<String>("path:media")+
			"/template/vegetation.rg";
	RecordView::loadRecordGroup(spScope,path);

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
