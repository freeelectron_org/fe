/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_ConfigI_h__
#define __data_ConfigI_h__


namespace fe
{
namespace ext
{

/** ConfigI Interface
	See Config (implementation) for the interesting details
	*/
class FE_DL_EXPORT ConfigI:
	virtual public Component,
	public CastableAs<ConfigI>
{
	public:
virtual		void	configParent(sp<ConfigI> a_parent)						= 0;
virtual		bool	configLookup(const String &a_key, Instance &a_instance)	= 0;
virtual		void	configSet(const String &a_key, Instance &a_instance)	= 0;
virtual		void	configSet(const String &a_key, Record &a_record,
						const String &a_attr)								= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __data_ConfigI_h__ */

