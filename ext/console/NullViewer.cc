/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <console/console.pmh>

namespace fe
{
namespace ext
{

void NullViewer::initialize(void)
{
	m_spSignalerI=registry()->create("SignalerI");
	m_spDrawI=registry()->create(m_drawName);
	m_spScope=registry()->create("Scope");

	if(!m_spDrawI.isValid() || !m_spScope.isValid() || !m_spSignalerI.isValid())
	{
		feX("NullViewer::initialize","couldn't create components");
	}

	m_asViewer.bind(m_spScope);

	m_spBeatLayout=m_spScope->declare("beat");

	m_spUnderlayLayout=m_spScope->declare("underlay");
	m_asViewer.populate(m_spUnderlayLayout);

	m_spRenderLayout=m_spScope->declare("render");
	m_asViewer.populate(m_spRenderLayout);

	m_spOverlayLayout=m_spScope->declare("overlay");
	m_asViewer.populate(m_spOverlayLayout);
}

void NullViewer::insertDrawHandler(sp<HandlerI> spHandlerI)
{
	m_spSignalerI->insert(spHandlerI,m_spRenderLayout);
	m_spSignalerI->insert(spHandlerI,m_spOverlayLayout);
}

void NullViewer::run(U32 frames)
{
	Record beat=m_spScope->createRecord(m_spBeatLayout);
	Record underlay=m_spScope->createRecord(m_spUnderlayLayout);
	Record render=m_spScope->createRecord(m_spRenderLayout);
	Record overlay=m_spScope->createRecord(m_spOverlayLayout);

	m_asViewer.viewer_layer(underlay)=0;
	m_asViewer.viewer_spatial(underlay)=0;
	m_asViewer.viewer_layer(render)=1;
	m_asViewer.viewer_spatial(render)=1;
	m_asViewer.viewer_layer(overlay)=2;
	m_asViewer.viewer_spatial(overlay)=0;

	U32 count=0;
	while(++count!=frames)
	{
		m_spSignalerI->signal(beat);
		m_spSignalerI->signal(underlay);
		m_spSignalerI->signal(render);
		m_spSignalerI->signal(overlay);
		m_spDrawI->flush();
//		nanoSleep(0,1000000);
	}
}

} /* namespace ext */
} /* namespace fe */
