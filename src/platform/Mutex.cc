/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

namespace fe
{

Mutex::Mutex(BWORD a_recursive):
	m_recursive(a_recursive),
	m_initializing(FALSE),
	m_pRawMutex(NULL)
#if FE_MTX_COUNT
	,m_fakeCount(0),
	m_lockCount(0)
#endif
{
	//* potentially deferred, if no system available
	if(gs_fnMutexInit)
	{
		raw();
	}
}

Mutex::~Mutex(void)
{
#if FE_MTX_COUNT
	FEASSERT(m_fakeCount==0);
	FEASSERT(m_lockCount==0);
#endif

	if(m_pRawMutex)
	{
		if(gs_fnMutexFinish)
		{
			gs_fnMutexFinish(m_recursive,m_pRawMutex);
		}
		m_pRawMutex=NULL;
	}
}

void* Mutex::raw(void)
{
	if(!m_pRawMutex && !m_initializing)
	{
//		confirm();

		if(gs_fnMutexInit)
		{
			m_initializing=TRUE;
			m_pRawMutex=gs_fnMutexInit(m_recursive);
			m_initializing=FALSE;
		}
	}

#if FE_CODEGEN<=FE_DEBUG
	if(!m_pRawMutex)
	{
		fe_fprintf(stderr,"Mutex::raw NULL mutex\n");
	}
#endif

//	FEASSERT(m_pRawMutex);
	return m_pRawMutex;
}

BWORD Mutex::lock(void)
{
#if FE_MTX_COUNT
	FEASSERT(m_fakeCount>=0);
	FEASSERT(m_lockCount>=0);
#endif

	if(!gs_fnMutexLock || m_initializing)
	{
//		confirm();

		if(!gs_fnMutexLock || m_initializing)
		{
#if FE_MTX_COUNT
			m_fakeCount++;
//			fe_fprintf(stderr,"Mutex::lock   %p FAKE %d %d\n",
//					this,m_fakeCount,m_lockCount);
#endif
			return FALSE;
		}
	}

	const BWORD success=gs_fnMutexLock(m_recursive,raw(),false,false,false);

#if FE_MTX_COUNT
	if(success)
	{
		m_lockCount++;
//		fe_fprintf(stderr,"Mutex::lock   %p %p %d %d\n",
//				this,m_pRawMutex,m_fakeCount,m_lockCount);
	}
#endif

	return success;
}

BWORD Mutex::unlock(void)
{
	if(!gs_fnMutexLock || m_initializing)
	{
#if FE_MTX_COUNT
		FEASSERT(m_fakeCount>0);
		FEASSERT(m_lockCount==0);
		m_fakeCount--;
//		fe_fprintf(stderr,"Mutex::unlock %p FAKE %d %d\n",
//				this,m_fakeCount,m_lockCount);
#endif
		return FALSE;
	}

#if FE_MTX_COUNT
	FEASSERT(m_fakeCount==0);
	FEASSERT(m_lockCount>0);

	m_lockCount--;
//	fe_fprintf(stderr,"Mutex::unlock %p %p %d %d\n",
//			this,m_pRawMutex,m_fakeCount,m_lockCount);
#endif

	return gs_fnMutexLock(m_recursive,raw(),true,false,false);
}

BWORD Mutex::tryLock(void)
{
	if(!gs_fnMutexLock || m_initializing)
	{
#if FE_MTX_COUNT
		m_fakeCount++;
#endif
		return FALSE;
	}

	if(gs_fnMutexLock(m_recursive,raw(),false,true,false))
	{
#if FE_MTX_COUNT
		m_lockCount++;
//		fe_fprintf(stderr,"Mutex::tryLock %p %d %d\n",
//				this,m_fakeCount,m_lockCount);
#endif
		return TRUE;
	}

	return FALSE;
}

BWORD Mutex::lockReadOnly(void)
{
	if(!gs_fnMutexLock || m_initializing)
	{
#if FE_MTX_COUNT
		m_fakeCount++;
#endif
		return FALSE;
	}

#if FE_MTX_COUNT
	m_lockCount++;
//	fe_fprintf(stderr,"Mutex::lockReadOnly %p\n",this);
#endif

	return gs_fnMutexLock(m_recursive,raw(),false,false,true);
}

BWORD Mutex::unlockReadOnly(void)
{
	if(!gs_fnMutexLock || m_initializing)
	{
#if FE_MTX_COUNT
		m_fakeCount--;
#endif
		return FALSE;
	}

#if FE_MTX_COUNT
	m_lockCount--;
//	fe_fprintf(stderr,"Mutex::unlockReadOnly %p\n",this);
#endif
	return gs_fnMutexLock(m_recursive,raw(),true,false,true);
}

BWORD Mutex::tryLockReadOnly(void)
{
	if(!gs_fnMutexLock || m_initializing)
	{
#if FE_MTX_COUNT
		m_fakeCount++;
#endif
		return FALSE;
	}

	if(gs_fnMutexLock(m_recursive,raw(),false,true,true))
	{
#if FE_MTX_COUNT
		m_lockCount++;
//		fe_fprintf(stderr,"Mutex::tryLockReadOnly %p %d %d\n",
//				this,m_fakeCount,m_lockCount);
#endif
		return TRUE;
	}

	return FALSE;
}

Mutex::Guard::Guard(Mutex& a_rMutex,BWORD a_readOnly,BWORD a_truly):
	m_pMutex(&a_rMutex),
	m_pRawGuard(NULL),
	m_readOnly(a_readOnly)
{
//	confirm();

	if(gs_fnMutexGuardInit && a_truly)
	{
//		fe_fprintf(stderr,"Mutex::Guard::Guard %p\n",this);
		void* pRawMutex=m_pMutex->raw();
		if(pRawMutex)
		{
			m_pRawGuard=gs_fnMutexGuardInit(
					m_pMutex->m_recursive,pRawMutex,m_readOnly);
			FEASSERT(m_pRawGuard);
		}
	}
}

Mutex::Guard::~Guard(void)
{
	if(gs_fnMutexGuardFinish && m_pRawGuard)
	{
//		fe_fprintf(stderr,"Mutex::Guard::~Guard %p\n",this);
		gs_fnMutexGuardFinish(m_pMutex->m_recursive,m_pRawGuard,m_readOnly);
	}
}

BWORD Mutex::Guard::lock(void)
{
	if(gs_fnMutexGuardLock && m_pRawGuard)
	{
//		fe_fprintf(stderr,"Mutex::Guard::lock %p\n",this);
		return gs_fnMutexGuardLock(
				m_pMutex->m_recursive,m_pRawGuard,FALSE,FALSE,m_readOnly);
	}
	return FALSE;
}

BWORD Mutex::Guard::unlock(void)
{
	if(gs_fnMutexGuardLock && m_pRawGuard)
	{
//		fe_fprintf(stderr,"Mutex::Guard::unlock %p\n",this);
		return gs_fnMutexGuardLock(
				m_pMutex->m_recursive,m_pRawGuard,TRUE,FALSE,m_readOnly);
	}
	return FALSE;
}

BWORD Mutex::Guard::tryLock(void)
{
	if(gs_fnMutexGuardLock && m_pRawGuard)
	{
//		fe_fprintf(stderr,"Mutex::Guard::tryLock %p\n",this);
		return gs_fnMutexGuardLock(
				m_pMutex->m_recursive,m_pRawGuard,FALSE,TRUE,m_readOnly);
	}
	return FALSE;
}

Mutex::Condition::Condition(void):
	m_pRawCondition(NULL)
{
	//* defer init
}

Mutex::Condition::~Condition(void)
{
	if(m_pRawCondition)
	{
		if(gs_fnMutexConditionFinish)
		{
			gs_fnMutexConditionFinish(m_pRawCondition);
		}
		m_pRawCondition=NULL;
	}
}

void* Mutex::Condition::raw(void)
{
	if(!m_pRawCondition)
	{
//		confirm();

		if(gs_fnMutexConditionInit)
		{
			m_pRawCondition=gs_fnMutexConditionInit();
			FEASSERT(m_pRawCondition);
		}
	}

	return m_pRawCondition;
}


BWORD Mutex::Condition::wait(Guard& a_rGuard)
{
	if(gs_fnMutexConditionWait)
	{
		void* pRawGuard=a_rGuard.m_pRawGuard;
		if(pRawGuard)
		{
			return gs_fnMutexConditionWait(raw(),
					a_rGuard.m_pMutex->m_recursive,pRawGuard,
					a_rGuard.m_readOnly);
		}
	}
	return FALSE;
}

BWORD Mutex::Condition::notifyOne(void)
{
	if(gs_fnMutexConditionNotify)
	{
		return gs_fnMutexConditionNotify(raw(),FALSE);
	}
	return FALSE;
}

BWORD Mutex::Condition::notifyAll(void)
{
	if(gs_fnMutexConditionNotify)
	{
		return gs_fnMutexConditionNotify(raw(),TRUE);
	}
	return FALSE;
}

#ifdef _OPENMP
extern "C"
{
bool omp_mutex_init(void);
}
#endif

//* static
BWORD Mutex::confirm(String a_hint)
{
//	fe_fprintf(stderr,"Mutex::confirm \"%s\"\n",a_hint.c_str());

	if(gs_fnMutexInit)
	{
		return TRUE;
	}
	if(gs_mutexChecked)
	{
		return FALSE;
	}
	gs_mutexChecked=TRUE;

	const String verbose=System::getVerbose();

	System::printBannerOnce();

	gs_pMutexLoader=new DL_Loader();
	FEASSERT(gs_pMutexLoader);

	BWORD success=FALSE;
	const BWORD adaptname=TRUE;

	String nameOrder=FE_STRING(FE_MUTEX_ORDER);

	if(!a_hint.empty())
	{
		nameOrder=a_hint+":"+nameOrder;
	}

	String name;
	while(!nameOrder.empty())
	{
//		fe_fprintf(stderr,"  nameOrder \"%s\"\n",nameOrder.c_str());

		while(!success && !(name=nameOrder.parse("\"",":")).empty())
		{
//			fe_fprintf(stderr,"  name \"%s\"\n",name.c_str());
			if(name=="omp")
			{
#ifdef _OPENMP
				if(verbose=="all")
				{
					fe_printf("%susing OpenMP (for mutexes)\n",
							PrefixLog::prefix().c_str());
				}
				gs_mutexSupport=name;

				return omp_mutex_init();
#endif
			}

			success=gs_pMutexLoader->openDL(name,adaptname);
//			fe_fprintf(stderr,"  success %d\n",success);
		}

		if(!success)
		{
			if(verbose!="none")
			{
				fe_printf("%sno mutex support\n",
						PrefixLog::prefix().c_str());
			}
			delete gs_pMutexLoader;
			gs_pMutexLoader=NULL;
			return FALSE;
		}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

		bool (*mutex_init)(void);
		mutex_init=(bool (*)(void))gs_pMutexLoader->findDLFunction("mutex_init");

#pragma GCC diagnostic pop

		if(!mutex_init)
		{
			if(verbose!="none")
			{
				fe_fprintf(stderr,"%smutex_init not found for %s\n",
						PrefixLog::prefix().c_str(),name.c_str());
			}
		}
		else if(mutex_init())
		{
			if(verbose=="all")
			{
				fe_printf("%susing %s (for mutexes)\n",
						PrefixLog::prefix().c_str(),name.c_str());
			}

			gs_mutexSupport=name;
			return TRUE;
		}
		else
		{
#if FE_THREAD_DEBUG
			fe_fprintf(stderr,"%smutex_init failed for %s\n",
					PrefixLog::prefix().c_str(),name.c_str());
#endif
		}

		success=FALSE;
	}

	delete gs_pMutexLoader;
	gs_pMutexLoader=NULL;
	return FALSE;
}

//* static
BWORD Mutex::clearFunctionPointers(void)
{
	Mutex::replaceInitFunction(NULL);
	Mutex::replaceLockFunction(NULL);
	Mutex::replaceFinishFunction(NULL);

	Mutex::replaceGuardInitFunction(NULL);
	Mutex::replaceGuardLockFunction(NULL);
	Mutex::replaceGuardFinishFunction(NULL);

	Mutex::replaceConditionInitFunction(NULL);
	Mutex::replaceConditionWaitFunction(NULL);
	Mutex::replaceConditionNotifyFunction(NULL);
	Mutex::replaceConditionFinishFunction(NULL);

	return TRUE;
}

//* static
BWORD Mutex::dismiss(void)
{
	if(System::getVerbose()=="all")
	{
		fe_printf("%sdismissing mutex support\n",
				PrefixLog::prefix().c_str());
	}

	clearFunctionPointers();

	if(gs_pMutexLoader)
	{
		gs_pMutexLoader->closeDL();
	}
	gs_pMutexLoader=NULL;

	gs_mutexChecked=FALSE;

	return TRUE;
}

} /* namespace */

