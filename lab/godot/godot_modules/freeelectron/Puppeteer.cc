/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define PUP_NOTIFICATION_DEBUG	TRUE

#include <freeelectron.pmh>

Puppeteer::Puppeteer(void)
{
	feLog("Puppeteer::Puppeteer\n");

	set_process(true);
//	set_physics_process(true);

	m_skeletonPath = SceneStringNames::get_singleton()->path_pp;
}

Puppeteer::~Puppeteer(void)
{
	feLog("Puppeteer::~Puppeteer\n");
}

void Puppeteer::_notification(int p_notification)
{
#if PUP_NOTIFICATION_DEBUG
	feLog("Puppeteer::_notification %d\n",p_notification);
#endif

	if(p_notification == NOTIFICATION_POSTINITIALIZE)
	{
#if PUP_NOTIFICATION_DEBUG
		feLog("NOTIFICATION_POSTINITIALIZE\n");
#endif

		postinitialize();
	}
	if(p_notification == NOTIFICATION_PROCESS)
	{
#if PUP_NOTIFICATION_DEBUG
		feLog("NOTIFICATION_PROCESS %.6G\n",
				get_process_delta_time());
#endif

		process(get_process_delta_time());
	}
#if PUP_NOTIFICATION_DEBUG
	if(p_notification == NOTIFICATION_PHYSICS_PROCESS)
	{
		feLog("NOTIFICATION_PHYSICS_PROCESS %.6G\n",
				get_physics_process_delta_time());
	}
	if(p_notification == NOTIFICATION_READY)
	{
		feLog("NOTIFICATION_READY\n");
	}
	if(p_notification == NOTIFICATION_PAUSED)
	{
		feLog("NOTIFICATION_PAUSED\n");
	}
	if(p_notification == NOTIFICATION_UNPAUSED)
	{
		feLog("NOTIFICATION_UNPAUSED\n");
	}
#endif
}

void Puppeteer::postinitialize(void)
{
	feLog("Puppeteer::postinitialize\n");

	m_spSingleMaster=fe::SingleMaster::create();
	FEASSERT(m_spSingleMaster.isValid());

	fe::sp<fe::Registry> spRegistry=m_spSingleMaster->master()->registry();
	spRegistry->manage("feAutoLoadDL");
}

void Puppeteer::_bind_methods(void)
{
	feLog("Puppeteer::_bind_methods\n");

	ClassDB::bind_method(D_METHOD("set_skeleton", "path"),
			&Puppeteer::set_skeleton);
	ClassDB::bind_method(D_METHOD("get_skeleton"),
			&Puppeteer::get_skeleton);


	ADD_PROPERTY(PropertyInfo(Variant::NODE_PATH, "skeleton_node"),
			"set_skeleton", "get_skeleton");
}

void Puppeteer::set_skeleton(const NodePath &a_skeletonPath) {

	m_skeletonPath = a_skeletonPath;
}

NodePath Puppeteer::get_skeleton() const {

	return m_skeletonPath;
}

void Puppeteer::process(float delta)
{
	const BWORD inEditor=Engine::get_singleton()->is_editor_hint();

	feLog("Puppeteer::process delta=%.6G FPS=%.6G editor=%d paused=%d\n",
			delta,
			Engine::get_singleton()->get_frames_per_second(),
			inEditor,
			get_tree()->is_paused());

	if(!inEditor)
	{
		modify();
	}
}

void Puppeteer::modify(void)
{
	feLog("Puppeteer::modify\n");

	Node* pSkeletonNode = get_node(m_skeletonPath);
	if(!pSkeletonNode)
	{
		feLog("Puppeteer::modify selected node was not found\n");
		return;
	}

	Skeleton* pSkeleton = Object::cast_to<Skeleton>(pSkeletonNode);
	if(!pSkeleton)
	{
		feLog("Puppeteer::modify selected node is not a skeleton\n");
		return;
	}

	//* TODO
//	const int bone_idx = pSkeleton->find_bone("name");
	const int bone_idx = 1;

	//* radians
	const float max=1.0;
	static float inc=0.01;
	static float angle=0.5;

	const Vector3 axis(0.0, 1.0, 0.0);
	const Quat rotation(axis, angle);
	const Transform xform(rotation);

	pSkeleton->set_bone_pose(bone_idx, xform);

	angle+=inc;
	if(angle>=max)
	{
		inc= -fabs(inc);
	}
	if(angle<=0.0)
	{
		inc=fabs(inc);
	}
}
