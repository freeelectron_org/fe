/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void NewtonEuler::handle(Record& record)
{
	m_arenaRV.bind(record);
	const F32 deltaT=m_arenaRV.deltaT();

	sp<RecordGroup> spRG=m_arenaRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_particleRAV.bind(spRA);

		if(!m_particleRAV.recordView().mass.check(spRA))
		{
			continue;
		}

		for(Particle& particleRV: m_particleRAV)
		{
			SpatialVector& force=particleRV.force();
			SpatialVector& impulse=particleRV.impulse();

			particleRV.location()+=deltaT*
					(particleRV.velocity()+=
					((impulse+force*deltaT)/particleRV.mass()));

#if FE_CODEGEN <= FE_DEBUG
			if(particleRV.location()[2]< -1e1f)
			{
				feLog("NewtonEuler::handle particle very low %s\n"
						"\tpos %s vel %s\n"
						"\tforce %s imp %s\n",
						particleRV.record().layout()->name().c_str(),
						print(particleRV.location()).c_str(),
						print(particleRV.velocity()).c_str(),
						print(particleRV.force()).c_str(),
						print(particleRV.impulse()).c_str());

				if(particleRV.force()[1]<0.0f ||
						particleRV.impulse()[1]<0.0f)
				{
					feX("NewtonEuler::handle","low burrowing particle %s",
							particleRV.record().layout()->name().c_str());
				}
			}
			if(particleRV.location()[2]>1e4f)
			{
				feLog("NewtonEuler::handle particle very high %s\n"
						"\tpos %s vel %s\n"
						"\tforce %s imp %s\n",
						particleRV.record().layout()->name().c_str(),
						print(particleRV.location()).c_str(),
						print(particleRV.velocity()).c_str(),
						print(particleRV.force()).c_str(),
						print(particleRV.impulse()).c_str());

				if(particleRV.force()[2]>0.0f ||
						particleRV.impulse()[2]>0.0f)
				{
					feX("NewtonEuler::handle","high ballistic particle %s",
							particleRV.record().layout()->name().c_str());
				}
			}
			F32 mag=magnitude(particleRV.velocity());
			if(mag>1e2f)
			{
				feLog("NewtonEuler::handle particle very fast %s\n"
						"\tpos %s vel %s\n"
						"\tforce %s imp %s\n",
						particleRV.record().layout()->name().c_str(),
						print(particleRV.location()).c_str(),
						print(particleRV.velocity()).c_str(),
						print(particleRV.force()).c_str(),
						print(particleRV.impulse()).c_str());

				if(mag>1e3f)
				{
					feX("NewtonEuler::handle","particle too fast %s",
							particleRV.record().layout()->name().c_str());
				}
			}
			mag=magnitude(particleRV.force());
			if(mag>1e3f)
			{
				feLog("NewtonEuler::handle intense force on %s\n"
						"\tpos %s vel %s\n"
						"\tforce %s imp %s\n",
						particleRV.record().layout()->name().c_str(),
						print(particleRV.location()).c_str(),
						print(particleRV.velocity()).c_str(),
						print(particleRV.force()).c_str(),
						print(particleRV.impulse()).c_str());

				if(mag>1e5f)
				{
					feX("NewtonEuler::handle","force too high on %s",
							particleRV.record().layout()->name().c_str());
				}
			}
			mag=magnitude(particleRV.impulse());
			if(mag>1e1f)
			{
				feLog("NewtonEuler::handle intense impulse on %s\n"
						"\tpos %s vel %s\n"
						"\tforce %s imp %s\n",
						particleRV.record().layout()->name().c_str(),
						print(particleRV.location()).c_str(),
						print(particleRV.velocity()).c_str(),
						print(particleRV.force()).c_str(),
						print(particleRV.impulse()).c_str());

				if(mag>1e3f)
				{
					feX("NewtonEuler::handle","impulse too high on %s",
							particleRV.record().layout()->name().c_str());
				}
			}

			//* HACK draw force line
/*
			if(particleRV.velocity.check())
			{
				const Color blue(0.0f,0.0f,1.0f,1.0f);
				const SpatialVector& location=particleRV.location();
				const SpatialVector& force=particleRV.force();
				SpatialVector vertex[2];
				vertex[0]=location;
				vertex[1]=location+force;

				sp<DrawI> spDrawI=m_arenaRV.drawI();
				FEASSERT(spDrawI.isValid());

				spDrawI->drawLines(vertex,NULL,2,DrawI::e_strip,false,&blue);
			}
*/
#endif

			set(force);
			set(impulse);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
