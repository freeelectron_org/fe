import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    module.summary = []

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib" ]

    tests = [ ]

    if 'viewer' in forge.modules_confirmed:
        deplibs += [ "fexViewerDLLib" ]

        tests += [  'xImage' ]

        forge.tests += [
            ("xImage.exe",  "fexImageDL 10",                    None,   None) ]
    else:
        module.summary += [ "-viewer" ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)
