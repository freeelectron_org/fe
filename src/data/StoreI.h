/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_StoreI_h__
#define __data_StoreI_h__

#define	FE_SB_TO_HDR(s)	((SBHeader *)(((FE_UWORD)s)-sizeof(SBHeader)))
#define FE_HDR_TO_SB(a)	((void *)(((FE_UWORD)a)+sizeof(SBHeader)))

namespace fe
{

class StoreI;

struct SBHeader
{
	StoreI	*m_pStore;
};


/**	@brief Base class for state block memeory managers

	@ingroup data

	Reference counting is maintained on state blocks that have the attribute
	_C_.
	*/
class FE_DL_EXPORT StoreI : virtual public Component
{
	public:
virtual					~StoreI(void) {}
		/** Create a state block. */
virtual	void			*createSB(void)										= 0;
		/** Create an array of state blocks. */
virtual	void			*createSB(FE_UWORD count)								= 0;
		/** Get the layout for this StoreI. */
virtual	hp<LayoutSB>&	getLayout(void)										= 0;
		/** Set the layout for this StoreI. */
virtual	void			setLayout(sp<LayoutSB>& rspLayout)					= 0;
		/** Get the size of a state block created by this StoreI. */
virtual	FE_UWORD			skipSizeSB(void)									= 0;

	public:
		friend class LayoutSB;
		/** Acquire a state block. */
virtual	void			acquireSB(void *stateBlock)							= 0;
		/** Release a state block. */
virtual	void			releaseSB(const RecordSB &r_old)					= 0;

		/** Free the state block.
			Use only on non-reference counted data blocks.
			*/
virtual	void			freeSB(void *stateBlock)							= 0;
};

} /* namespace */

#endif /* __data_StoreI_h__ */

