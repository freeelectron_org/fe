/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexShapeDL"));
	list.append(new String("fexSurfaceDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	String path=spMaster->catalog()->catalog<String>(
			"path:media")+"/template/terrain.rg";

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<StratumBowl>("StratumI.StratumBowl.fe");
	pLibrary->add<StratumPlane>("StratumI.StratumPlane.fe");
	pLibrary->add<StratumRamp>("StratumI.StratumRamp.fe");
	pLibrary->add<StratumRaster>("StratumI.StratumRaster.fe");
	pLibrary->add<StratumWave>("StratumI.StratumWave.fe");
	pLibrary->add<StratumDrawPoly>("StratumDrawI.StratumDrawPoly.fe");
	pLibrary->add<StratumDrawWire>("StratumDrawI.StratumDrawWire.fe");
	pLibrary->add<Habitat>("StrataI.Habitat.fe");
	pLibrary->add<SurfaceStrata>("SurfaceI.SurfaceStrata.fe");

	pLibrary->add<Stratum>("RecordFactoryI.Stratum.fe");
	pLibrary->add<Strata>("RecordFactoryI.Strata.fe");

	sp<Scope> spScope=spMaster->catalog()->catalogComponent("Scope","SimScope");
	RecordView::loadRecordGroup(spScope,path);

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
