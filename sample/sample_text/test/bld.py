import sys
forge = sys.modules["forge"]

import os.path

def setup(module):

    tests = [   'xSampleText' ]

    forge.tests += [
        ("xSampleText.exe", "",             None,   None) ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], forge.corelibs)
