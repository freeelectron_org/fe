/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <openal/openal.pmh>

namespace fe
{
namespace ext
{

ListenerAL::ListenerAL(void)
{
}

void ListenerAL::initialize(void)
{
	//* provoke singleton
	registry()->create("*.AudioAL");

	SpatialVector zero(0.0f,0.0f,0.0f);
	SpatialVector posY(0.0f,1.0f,0.0f);
	SpatialVector negZ(0.0f,0.0f,-1.0f);
	setLocation(zero);
	setOrientation(posY,negZ);
	setVelocity(zero);
}

ListenerAL::~ListenerAL(void)
{
}

Result ListenerAL::setLocation(const SpatialVector& location)
{
	alListener3f(AL_POSITION,location[0],location[1],location[2]);
	return AudioAL::checkError("ListenerAL::setLocation");
}

Result ListenerAL::setOrientation(const SpatialVector& at,
	const SpatialVector& up)
{
	ALfloat orientation[6];
	orientation[0]=at[0];
	orientation[1]=at[1];
	orientation[2]=at[2];
	orientation[3]=up[0];
	orientation[4]=up[1];
	orientation[5]=up[2];

	alListenerfv(AL_ORIENTATION,orientation);
	return AudioAL::checkError("ListenerAL::setOrientation");
}

Result ListenerAL::setVelocity(const SpatialVector& velocity)
{
	alListener3f(AL_VELOCITY,velocity[0],velocity[1],velocity[2]);
	return AudioAL::checkError("ListenerAL::setVelocity");
}

} /* namespace ext */
} /* namespace fe */
