import sys
forge = sys.modules["forge"]

def prerequisites():
    return [    "surface",
                "window" ]

def setup(module):
    srcList = [ "operate.pmh",
                "ManipulatorCommon",
                "OperateCommon",
                "OperatorState",
                "OperatorSurfaceCommon",
                "OperatorThreaded",
                "operateDL" ]

    dll = module.DLL( "fexOperateDL", srcList )

    deplibs = forge.corelibs+ [
                "fexThreadDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib" ]

    forge.deps( ["fexOperateDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOperateDL",                 None,   None) ]
