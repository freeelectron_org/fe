/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define LISTENQ		5
#define SERV_PORT	7876

// Test code to see if a Sender can do the bind operation with UDP
//#define SENDER

void Receive();
void Send();
fe::sp<fe::ext::ChunkSpool> spBuffer;
fe::sp<fe::ext::ChunkSender> spSender;
fe::sp<fe::ext::ChunkReceiver> spReceiver;
fe::sp<fe::ext::Socket>		_socket;

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		fe::ext::Socket::startup();
		_socket = new fe::ext::Socket;
		fe::ext::SockAddr			server_addr;

		_socket->openStream(fe::ext::Socket::e_udp);		// Goes first.
		server_addr.setPort(SERV_PORT);
		_socket->connect(server_addr);
		_socket->bind(server_addr);
		spBuffer = new fe::ext::ChunkSpool();

#ifdef SENDER
		spSender = new fe::ext::ChunkSender();
		spSender->start(spBuffer, _socket);
		Send();
		spSender->stop();
#else
		spReceiver = new fe::ext::ChunkReceiver();
		spReceiver->start(spBuffer, _socket);
		Receive();
#endif
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}


//-----------------------------------------------------------------
// Receive()
//-----------------------------------------------------------------
void Receive()
{
		while(true)
		{
			fe::milliSleep(1);
			fe::sp<fe::ext::Chunk> spChunk;
			spChunk = spBuffer->wait();
			feLogGroup("netsignal","received chunk of size %u\n", spChunk->size());
		}
}


//-----------------------------------------------------------------
// Send()
//-----------------------------------------------------------------
void Send()
{
	for(int i = 1; i < 10; i++)
	{
		fe::milliSleep(10);
		fe::sp<fe::ext::Chunk> spChunk;
		spChunk = new fe::ext::Chunk(20);
		feLogGroup("netsignal","%d %d sz %d\n", spBuffer.isValid(), spChunk.isValid(), spChunk->size());
		spBuffer->put(spChunk.abandon());
	}
}
