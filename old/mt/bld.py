import sys
import os
import shutil
forge = sys.modules["forge"]

# TBB TEST FILE
test_file = """
#include <tbb/tbb_stddef.h>
int main(int argc, char *argv[])
{
    return 0;
}
\n"""

def setup(module):
    setup_tbb(module)

help_message = """
Thread Building Blocks (TBB) is required for some threading
support, but something is not configured properly to be used.

If a valid TBB is in your dep/tbb directory, the build system will find it.

If all else fails, you can simply disable performance threading altogether by
setting FE_MT to 0
"""

def mt_failed(message):
    forge.cprint(forge.RED,1,'')
    forge.cprint(forge.RED,1,'')
    forge.cprint(forge.RED,1,message)
    forge.cprint(forge.RED,0,help_message)


def setup_tbb(module):
    if sys.modules['root'].config['FE_MT'] == '0':
        return None

    if forge.fe_os == "FE_LINUX":
        forge.linkmap['tbb'] = ' -ltbb'
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        forge.linkmap['tbb'] = 'tbb.lib' # TODO verify
    elif forge.fe_os == 'FE_OSX':
        forge.linkmap['tbb'] = '-framework tbb' # TODO verify


