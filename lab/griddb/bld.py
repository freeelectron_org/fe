import sys
import os
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "griddb.pmh",
                "griddbDL" ]

    dll = module.DLL( "fexGriddbDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib"
                ]

    forge.deps( ["fexGriddbDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexGriddbDL",          None,       None) ]

    module.Module( 'test' )
