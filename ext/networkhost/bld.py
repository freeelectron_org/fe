import sys
import os
import shutil
import utility
forge = sys.modules["forge"]

def prerequisites():
    return [ "network" ]

def setup(module):
    srcList = [ "NetDispatch",
                "NetHost",
                "nethostExtern",
                "networkhost.pmh",
                "networkhostDL" ]

    dll = module.DLL( "fexNetworkHostDL", srcList )

    deplibs = forge.corelibs + [
                "fexSignalLib",
                "fexNetworkDLLib" ]

#   if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
#       deplibs += [    "fexDataToolLib" ]

    forge.deps( ["fexNetworkHostDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexNetworkHostDL",                 None,   None) ]

    module.Module('test')

    utility.copy_all(module.modPath + "/bin", forge.libPath)
