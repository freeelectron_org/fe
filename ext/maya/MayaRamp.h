/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MayaRamp_h__
#define __maya_MayaRamp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Ramp in Maya

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT MayaRamp:
	public MetaRamp,
	public CastableAs<MayaRamp>
{
	public:
						MayaRamp(void)										{}
virtual					~MayaRamp(void)										{}

						//* as RampI
virtual	U32				entryCount(void) const
						{
							MStatus status;
							const U32 count=const_cast<MRampAttribute&>(m_ramp)
									.getNumEntries(&status);
							FEASSERT(status);
							return count;
						}

virtual	Vector2			entry(U32 a_index) const
						{
							MIntArray indexes;
							MFloatArray positions;
							MFloatArray values;
							MIntArray interps;
							MStatus status;
							const_cast<MRampAttribute&>(m_ramp)
									.getEntries(indexes,positions,values,
									interps,&status);
							FEASSERT(status);
							return Vector2(positions[a_index],values[a_index]);
						}

virtual	Real			eval(Real a_u) const
						{
							float value;
							MStatus status;
							const_cast<MRampAttribute&>(m_ramp)
									.getValueAtPosition(a_u,value,&status);
							FEASSERT(status);
							return value;
						}

		void			setRamp(MRampAttribute& a_rRamp)
						{	m_ramp=a_rRamp; }

	private:

		MRampAttribute	m_ramp;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MayaRamp_h__ */
