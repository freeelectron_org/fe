/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"
#include "spatial/spatial.h"
#include "platform/opengl_headers.h"

#include "spatial/ProxMultiGrid.h"

using namespace fe;
using namespace fe::ext;

#if 0
template<unsigned int D>
void generateRGBA(void *a_buffer, const Continuum<Real,D> &a_continuum,
		unsigned int a_width, unsigned int a_height,
		const SpatialTransform &a_transform, unsigned int a_mode)
{
	U8 *pBuffer = static_cast<U8 *>(a_buffer);
	SpatialVector pre, post;
	pre[2] = 0.0;
	typename Continuum<Real,D>::t_span smpl_pnt;
	for(unsigned int i = 2; i < D; i++)
	{
		smpl_pnt[i] = 0.0;
	}
	for(FE_UWORD w = 0; w < a_width; w++)
	{
		pre[0] = static_cast<Real>(w);
		for(FE_UWORD h = 0; h < a_height; h++)
		{
			pre[1] = static_cast<Real>(h);
			transformVector(a_transform, pre, post);
			smpl_pnt[0] = post[0];
			smpl_pnt[1] = post[1];
			F32 sample;
			if(a_mode == 0) {sample = linearSample(a_continuum, smpl_pnt);}
			if(a_mode == 1) {sample = directSample(a_continuum, smpl_pnt);}
			if(a_mode == 2) {sample = closestSample(a_continuum, smpl_pnt);}
			if(a_mode == 3)
			{
				typename Continuum<Real,D>::t_index idx;
				a_continuum.closest(idx, smpl_pnt);
				typename Continuum<Real,D>::t_span grid_pnt;
				a_continuum.space(grid_pnt, idx);
				Real a = grid_pnt[0] - smpl_pnt[0];
				Real b = grid_pnt[1] - smpl_pnt[1];
				Real dist = sqrt(a*a + b*b);
				if(dist < 3.0)
				{
					sample = (1.0-dist/3.0)*closestSample(a_continuum,smpl_pnt)+
						(dist/3.0)*200;
				}
				else { sample = 200.0; }
			}
			if(a_mode == 4)
			{
				typename Continuum<Real,D>::t_index block;
				block[0] = 4;
				block[1] = 4;
				block[2] = 1;
				sample = polySample(a_continuum, smpl_pnt, block);
			}
			int k = (w+h*a_width)*4;
			//U8 isample = (U8)(256.0*((sample+100.0)/200.0));
			sample = sample/2.0 + 50.0;
#if 0
			if(sample > 0.0) sample = 200;
			if(sample == 0.0) sample = 100;
			if(sample < 0.0) sample = 0;
#endif
			U8 rsample = (U8)(sample);
			U8 gsample = (U8)(sample);
			U8 bsample = (U8)(sample);
			if(sample > 255.0)
			{
				rsample = 255;
				gsample = 255;
				bsample = 0;
			}
			pBuffer[k] = rsample;
			pBuffer[++k] = gsample;
			pBuffer[++k] = bsample;
			pBuffer[++k] = 150;
		}
	}
}
#endif

class MyDraw:
	virtual public HandlerI
{
	public:
				MyDraw(void *a_buffer, FE_UWORD a_width, FE_UWORD a_height,
						FE_UWORD a_x, FE_UWORD a_y)
				{
					m_pBuffer = a_buffer;
					m_width = a_width;
					m_height = a_height;
					m_x = a_x;
					m_y = a_y;
				}

virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
					sp<Scope> spScope=spLayout->scope();
					FEASSERT(spScope.isValid());
				}

virtual void	handle(Record& render)
				{
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					glRasterPos2i((GLint)m_x,(GLint)m_y);
					glPixelZoom(1.0f,1.0f);
					glDrawPixels(m_width, m_height,
						GL_RGBA, GL_UNSIGNED_BYTE, m_pBuffer);
				}

	private:
		Accessor<I32>	m_aLayer;
		void			*m_pBuffer;
		FE_UWORD			m_width;
		FE_UWORD			m_height;
		FE_UWORD			m_x;
		FE_UWORD			m_y;
};

#if 0
class DrawVecs : public HandlerI
{
	public:
				DrawVecs(Continuum<Vector3f,3> &vecField)
					: m_rVecField(vecField) { }
virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
					sp<Scope> spScope=spLayout->scope();
					FEASSERT(spScope.isValid());
				}
virtual void	handle(Record& r_sig)
				{
					glDisable(GL_LIGHTING);
					glColor3f(1.0,0.0,0.0);
					glBegin(GL_LINES);
					Continuum<Vector3f,3>::t_index idx;
					idx[2] = 0;
					for(idx[0]=0; idx[0] < m_rVecField.count()[0]; idx[0]++)
					{
						for(idx[1]=0; idx[1] < m_rVecField.count()[1]; idx[1]++)
						{
							Vector3f v;
							m_rVecField.space(v,idx);
							glVertex3fv(v.temp());
							v += m_rVecField[idx];
							glVertex3fv(v.temp());
						}
					}
					glEnd();
				}
	private:
		Continuum<Vector3f,3>	&m_rVecField;
};
#endif

#if 0
class DrawVecs2 : public HandlerI
{
	public:
				DrawVecs2(Continuum<Real,3> &field)
					: m_rField(field) { }
virtual void	bind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
					sp<Scope> spScope=spLayout->scope();
					FEASSERT(spScope.isValid());
				}
#define XS 5.0
#define YS 5.0
virtual void	handle(Record& r_sig)
				{
					glDisable(GL_LIGHTING);
					glColor3f(1.0,0.0,0.0);
					glBegin(GL_LINES);
					Continuum<Real,3>::t_span loc;
					loc[2] = 0.0;
					Continuum<Real,3>::t_span grad;
					for(Real x = 0.0; x <= fabs(m_rField.boundary()[0]);
						x += XS)
					{
						for(Real y = 0.0; y <= fabs(m_rField.boundary()[1]);
							y += YS)
						{
							loc[0] = x;
							loc[1] = y;
							gradient(m_rField, grad, loc);
							glVertex3fv(loc.temp());
							glVertex3fv((loc-grad).temp());
						}
					}
					glEnd();
				}
	private:
		Continuum<Real,3>	&m_rField;
};
#endif

#define WD 100
#define HT 100

#if 0
void setup_continuum(Continuum<Real,3> &nC)
{
	int cnt[3];
	cnt[0] = 20;
	cnt[1] = 20;
	cnt[2] = 1;
	Real bndry[3];
	bndry[0] = 100.0;
	bndry[1] = 100.0;
	bndry[2] = 0.0;
	nC.create(cnt,bndry,0.0);

	int idx[3];
	idx[0] = 1;
	idx[1] = 0;
	idx[2] = 0;

	Real loc[3];
	loc[0] = 93.0;
	loc[1] = 94.0;
	loc[2] = 0.0;

	Real radius[3];
	radius[0] = 20.0;
	radius[1] = 20.0;
	radius[2] = 10.0;

	radialSet<3>(nC, loc, radius, 255.0);

	loc[0] = 80.0;
	loc[1] = 20.0;
	radius[0] = 20.0;
	radius[1] = 10.0;
	radialSet<3>(nC, loc, radius, 100.0);


	idx[0] = 1;
	idx[1] = 1;
	idx[2] = 0;
	nC.set(idx, 255.0);

	idx[1] = 7;
	idx[2] = 0;

#define WALL 10000.0
	for(unsigned int x = 0; x < 7; x++)
	{
		idx[0] = x;
		nC.set(idx, WALL);
	}

	for(unsigned int y = 7; y < 17; y++)
	{
		idx[1] = y;
		nC.set(idx, WALL);
	}

	for(unsigned int x = 3; x < 7; x++)
	{
		idx[0] = x;
		nC.set(idx, WALL);
	}
	idx[0] = 3;

	for(unsigned int y = 10; y < 17; y++)
	{
		idx[1] = y;
		nC.set(idx, WALL);
	}

}
#endif

#if 0
void new_test(void *buffer, unsigned int width, unsigned int height,
		unsigned int mode)
{

	Continuum<Real,3> nC;

	setup_continuum(nC);


	SpatialTransform transform;
	transform = SpatialTransform::identity();
	scale(transform, 1.0);
	generateRGBA<3>((void *)buffer, nC, width, height, transform, mode);

}
#endif

void prox_test(sp<Registry> spRegistry)
{
	sp<Scope> spScope(spRegistry->create("Scope"));

	Peeker peek;
	peek(*spScope);
	peek(*(spRegistry->master()->typeMaster()));
	feLog(peek.output().c_str());
	feLog("\n");

	Vector<4,Real> scalefactor(3.5f, 3.5f, 1.0f);
	Vector<4,Real> translation(1.5f, 1.5f, 0.0f);
	SpatialTransform transform = SpatialTransform::identity();
	translate(transform, translation);
	scale(transform, scalefactor);

	sp<AffineSpace> spSpace(new AffineSpace());
	spSpace->setTransform(transform);

	sp<ProxMultiGrid> spProxMG(new ProxMultiGrid());
	spProxMG->addGrid(2.0f);
	//spProxMG->addGrid(1.0f);
	//spProxMG->addGrid(0.5f);
	//spProxMG->setSpace(spSpace);

	sp<RecordGroup> rg_in(new RecordGroup());
	sp<RecordGroup> rg_out(new RecordGroup());
	sp<Layout> l_pair = spScope->declare("l_pair");
	AsProximity asProximity;
	asProximity.bind(spScope);
	asProximity.populate(l_pair);

	sp<Layout> l_atom = spScope->declare("l_atom");
	AsParticle asParticle;
	asParticle.bind(spScope);
	asParticle.populate(l_atom);
	AsBounded asBounded;
	asBounded.bind(spScope);
	asBounded.populate(l_atom);

	Record r_atom;

#if 0
	r_atom = spScope->createRecord(l_atom);
	asParticle.location(r_atom)[0] = 0.1f;
	asParticle.location(r_atom)[1] = 3.0f;
	asBounded.radius(r_atom) = 1.0f;
	rg_in->add(r_atom);

	r_atom = spScope->createRecord(l_atom);
	asParticle.location(r_atom)[0] = 3.4f;
	asParticle.location(r_atom)[1] = 3.0f;
	asBounded.radius(r_atom) = 1.0f;
	rg_in->add(r_atom);
#endif

	r_atom = spScope->createRecord(l_atom);
	asParticle.location(r_atom)[0] = 1.6f;
	asParticle.location(r_atom)[1] = 1.6f;
	asBounded.radius(r_atom) = 0.2f;
	rg_in->add(r_atom);
	feLog("atom %u\n",r_atom.idr());

	r_atom = spScope->createRecord(l_atom);
	asParticle.location(r_atom)[0] = 1.6f;
	asParticle.location(r_atom)[1] = 1.7f;
	asBounded.radius(r_atom) = 0.2f;
	rg_in->add(r_atom);
	feLog("atom %u\n",r_atom.idr());

#if 0
	r_atom = spScope->createRecord(l_atom);
	asParticle.location(r_atom)[0] = 3.0f;
	asParticle.location(r_atom)[1] = 4.9f;
	asBounded.radius(r_atom) = 0.2f;
	rg_in->add(r_atom);

	r_atom = spScope->createRecord(l_atom);
	asParticle.location(r_atom)[0] = 3.2f;
	asParticle.location(r_atom)[1] = 4.9f;
	asBounded.radius(r_atom) = 0.2f;
	rg_in->add(r_atom);
#endif

	spProxMG->detect(l_pair, rg_in, rg_out);

	for(RecordGroup::iterator i_rg = rg_out->begin();
		i_rg != rg_out->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		for(int i = 0; i < spRA->length(); i++)
		{
			fprintf(stderr,"***** pair\n");
		}
	}
}

int main(int argc,char** argv)
{

	UnitTest unitTest;

	U32 frames=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
#if 0
		Continuum<Vector3f,3> vectorField;
		Continuum<Real,3> nC;
		setup_continuum(nC);

		derive<Vector3f, 3>(vectorField,nC);


		Continuum<Real,3>::t_index i_destination(5,5,0);
		Continuum<Real,3> nP;
		Continuum<Real,3> nS;
		nS = nC;
		//nS.copyTopology(nC, 0.0f);
		nP.copyTopology(nC, 0.0f);
		//wavefront<Real, Real, 3>(nP, nS, i_destination, -100.0f, 1e10f,
		//	potentialDifference<Real, Real>);

		Vector<3, Real> destination(50.0,10.0,0.0);
		//calc_path_field<Real, Real, 3>(nP, nS, destination);
#endif


		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();
		assertMath(spMaster->typeMaster());

		spRegistry->manage("feDataDL");
		spRegistry->manage("fexSpatialDL");
		spRegistry->manage("fexViewerDL");

		prox_test(spRegistry);

		sp<Scope> spScope(spRegistry->create("Scope"));
#if 0
		sp<ContinuumI> spContinuum(spRegistry->create("ContinuumI"));

		Accessor<Real> aScalarValue(spScope, "scalarValue");

		sp<Layout> l_continuum = spScope->declare("ContinuumLayout");
		sp<Layout> l_cell = spScope->declare("CellLayout");
		l_cell->populate(aScalarValue);

		Vector3i count(10,10,1);
		SpatialVector boundary(-100.0f,100.0f,1.0f);

		spContinuum->create(l_continuum, l_cell, count, boundary);

		sp<ScalarFieldI> spField = spContinuum->get(aScalarValue);

		SpatialVector location(0,0,0);
		setAt(location, 0, 32.0f);
		setAt(location, 1, 40.0f);

		spField->set((Real)42, location);

		setAt(location, 0, 15.0f);
		setAt(location, 1, 15.0f);

		spField->set((Real)-17, location);
		spField->add((Real)17.0, location);

		setAt(location, 0, 81.0f);
		setAt(location, 1, 22.0f);

		spField->set((Real)70, location);

		for(int i = 0; i < 100; i++)
		{
			setAt(location, 0, 100.0f*((float)rand()/(float)RAND_MAX));
			setAt(location, 1, 100.0f*((float)rand()/(float)RAND_MAX));
			spField->add((Real)(80.0f*((float)rand()/(float)RAND_MAX)-40.0f),
					location);
		}

		setAt(location, 0, 35.0f);
		setAt(location, 1, 45.0f);

		Real value = spField->sample(location);

		feLog("value %f\n", value);
#endif

		//spContinuum->dump(aScalarValue);

		FE_UWORD width = WD;
		FE_UWORD height = HT;
#if 0
		U8 buffer0[WD*HT*4];
		U8 buffer1[WD*HT*4];
		U8 buffer2[WD*HT*4];
		U8 buffer3[WD*HT*4];
		U8 buffer4[WD*HT*4];
#endif
		U8 buffer5[WD*HT*4];
		SpatialTransform transform;
		transform = SpatialTransform::identity();
		scaleAll(transform, 5.0);
		//rotate(transform, -0.1, e_yAxis);
		//rotate(transform, 0.1, e_zAxis);

		//spContinuum->GenerateRGB((void *)buffer,spField,width,height,transform);
#if 0
		new_test((void *)buffer0,width,height,0);
		new_test((void *)buffer1,width,height,1);
		new_test((void *)buffer2,width,height,2);
		new_test((void *)buffer3,width,height,3);
		new_test((void *)buffer4,width,height,4);
#endif

#if 0
		SpatialTransform x_form;
		x_form = SpatialTransform::identity();
		U8 buffer_s[WD*HT*4];
		generateRGBA<3>((void *)buffer_s, nS, width, height, x_form, 0);
		U8 buffer_p[WD*HT*4];
		generateRGBA<3>((void *)buffer_p, nP, width, height, x_form, 1);
#endif


		Peeker peek;
		peek(*spScope);
		feLog(peek.output().c_str());

		sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));

		//sp<DrawVecs> spDrawVecs(new DrawVecs(vectorField));
		//sp<DrawVecs2> spDrawVecs(new DrawVecs2(nP));
#if 0
		spQuickViewerI->insertDrawHandler(spDrawVecs);
#endif

#if 0
		sp<MyDraw> spMyDraw0(new MyDraw(buffer0,width,height,0,0));
		sp<MyDraw> spMyDraw1(new MyDraw(buffer1,width,height,100,0));
		sp<MyDraw> spMyDraw2(new MyDraw(buffer2,width,height,200,0));
		sp<MyDraw> spMyDraw3(new MyDraw(buffer3,width,height,300,0));
		sp<MyDraw> spMyDraw4(new MyDraw(buffer4,width,height,400,0));
		sp<MyDraw> spMyDraw6(new MyDraw(buffer_s,width,height,0,220));
		sp<MyDraw> spMyDraw7(new MyDraw(buffer_p,width,height,110,220));
#endif
		//spQuickViewerI->insertDrawHandler(spMyDraw0);
#if 0
		spQuickViewerI->insertDrawHandler(spMyDraw1);
		spQuickViewerI->insertDrawHandler(spMyDraw2);
		spQuickViewerI->insertDrawHandler(spMyDraw3);
		spQuickViewerI->insertDrawHandler(spMyDraw4);
		spQuickViewerI->insertDrawHandler(spMyDraw6);
		spQuickViewerI->insertDrawHandler(spMyDraw7);
#endif

		sp<GridScalarField> spGDF(new GridScalarField());
		sp<ScalarFieldI> spScalarField(spGDF);

		Vector3i cnt(40,40,10);
		sp<AffineSpace> spSpace(new AffineSpace());
		SpatialTransform space_transform;
		space_transform = SpatialTransform::identity();
		scaleAll(space_transform, 100.0f);
		spSpace->setTransform(space_transform);
		spGDF->create(spSpace, cnt);
		SpatialVector loc(50.0,50.0,0.0);
		SpatialVector rad(20.0,20.0,20.0);
		//radialSet(spGDF->continuum(), loc, rad, 100.0);
		spScalarField->radialSet(loc, rad, 100.0);
		//DrawScalarField dsf;
		SpatialTransform xform;
		xform = SpatialTransform::identity();
		Vector<4,Real> trans(0.0f,0.0f,15.0f,0.0f);
		//translate(xform,trans);
		scaleAll(xform,100.0);
		//rotate(xform, 0.5, e_yAxis);
		//rotate(xform, 0.4, e_zAxis);
		//dsf.generate(buffer5,width,height,xform,spScalarField);
		sp<MyDraw> spMyDraw5(new MyDraw(buffer5,width,height,0,100));
		spQuickViewerI->insertDrawHandler(spMyDraw5);


		spQuickViewerI->run(frames);


	}
	catch(Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

