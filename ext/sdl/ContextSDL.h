/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __sdl_ContextSDL_h__
#define __sdl_ContextSDL_h__

#include "window/WindowEvent.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Encapsulate start/shutdown of SDL

	@ingroup sdl
*//***************************************************************************/
class FE_DL_EXPORT ContextSDL: virtual public Component
{
	public:
					ContextSDL(void);
virtual				~ContextSDL(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __sdl_ContextSDL_h__ */
