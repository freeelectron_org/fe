#! /usr/bin/python3

import os
scriptDir = os.path.dirname(os.path.realpath(__file__))

import sys
sys.path.append(scriptDir)

print("append path '%s'" % scriptDir)

import pyfe.nethost

netHost = pyfe.nethost.NetHost()
space = netHost.accessSpace("world")

space.setState("net:role","client")
space.setState("net:address","127.0.0.1")
space.setState("net:port",7890)
space.start()
space.waitForConnection()

flushCount = -1
frame = -1
running = True

(success, flushCount, spins) = space.waitForUpdate(flushCount)

import ctypes

class Pod(ctypes.Structure):
    _fields_ = [("intValue", ctypes.c_int),
                ("floatValue", ctypes.c_float),
                ("stringValue", ctypes.c_char * 16)]

pod = Pod()
pod.intValue = 7
pod.floatValue = 3.14
pod.stringValue = b"hey"

byteBuffer = pyfe.nethost.Space.ByteBuffer(pod)
space.setState("structBlock2",byteBuffer)
space.flush()

(success, byteBuffer)=space.getState("structBlock",byteBuffer)
pod = byteBuffer.castTo(Pod)
print("pod: %d %.3f \"%s\"" % (pod.intValue, pod.floatValue, pod.stringValue))

while running:
    lastFrame = frame

    lastFlushCount = flushCount
    while flushCount == lastFlushCount:
        snapshot = space.snapshot()
        flushCount = snapshot.flushCount()

    (success, running)=snapshot.getState("running",running)
    (success, frame)=snapshot.getState("frame",frame)

    snapshot = None

space.stop()
