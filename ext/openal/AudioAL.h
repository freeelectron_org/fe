/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __openal_AudioAL_h__
#define __openal_AudioAL_h__

#define FE_RETURN_AL_ERROR(text)											\
	{	Result al_result=checkError(text);									\
		if(failure(al_result)) return al_result; }
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Audio handling using OpenAL

	@ingroup openal

	http://www.openal.org
*//***************************************************************************/
class AudioAL:
	virtual public AudioI,
	public CastableAs<AudioAL>
{
	public:
				AudioAL(void);
virtual			~AudioAL(void);

				//* As AudioI
virtual	Result	load(String name,String filename);
virtual	Result	unload(String name);
virtual	Result	flush(void);

		void	remember(sp<VoiceI> spVoiceI);
		void	forget(sp<VoiceI> spVoiceI);

		ALuint	lookup(String name) const;
static	Result	checkError(String text);

	private:
		void	forgetAll(void);

		typedef HashMap<String,ALuint,hash_string,eq_string> BufferMap;

		ALCdevice*			m_pDevice;
		ALCcontext*			m_pContext;

		BufferMap			m_bufferMap;

		List< hp<VoiceI> >	m_chorus;	// persistent voices
};

inline ALuint AudioAL::lookup(String name) const
{
	BufferMap::const_iterator it=m_bufferMap.find(name);
	if(it==m_bufferMap.end())
	{
		return ALuint(-1);
	}

	return it->second;
}

inline Result AudioAL::checkError(String text)
{
	ALuint error=alGetError();
	switch(error)
	{
		case AL_NO_ERROR:
			return e_ok;
		case AL_INVALID_NAME:
			feLog("AudioAL::checkError AL_INVALID_NAME (%s)\n",text.c_str());
			return e_invalidHandle;
		case AL_INVALID_ENUM:
			feLog("AudioAL::checkError AL_INVALID_ENUM (%s)\n",text.c_str());
			return e_unsupported;
		case AL_INVALID_VALUE:
			feLog("AudioAL::checkError AL_INVALID_VALUE (%s)\n",text.c_str());
			return e_invalidRange;
		case AL_INVALID_OPERATION:
			feLog("AudioAL::checkError AL_INVALID_OPERATION (%s)\n",
					text.c_str());
			return e_unsupported;
		case AL_OUT_OF_MEMORY:
			feLog("AudioAL::checkError AL_OUT_OF_MEMORY (%s)\n",text.c_str());
			return e_outOfMemory;
		default:
			feLog("AudioAL::checkError undefined error (%s)\n",text.c_str());
			return e_undefinedFailure;
	}
}

} /* namespace ext */
} /* namespace fe */

#endif /* __openal_AudioAL_h__ */
