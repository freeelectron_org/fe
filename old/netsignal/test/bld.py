import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
                "fexSignalLib",
                "fexNetworkDLLib",
                "fexNetSignalLib",
                "fexDataToolDLLib" ]

    tests = [   "xEchoClient",
                "xEchoServer",
                "xEchoUdpClient",
                "xEchoUdpServer",
                "xSignalSender",
                "xSignalReceiver",
                "xServer",
                "xClient",
                "xChunkSender",
                "xChunkReceiver",
                "xChunkReceiverUdp",
                "xChunkSenderUdp"
            ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + 'Exe'], deplibs)

    forge.tests += [
            ("xServer.exe",     "7876 tcp",     "",
            ("xClient.exe",     "7876 tcp",     "",     None)),
            ("xServer.exe",     "7876 udp",     "",
            ("xClient.exe",     "7876 udp",     "",     None)) ]
