/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Castable_h__
#define __core_Castable_h__

namespace fe
{

#define	FE_CASTABLE_DEBUG		FALSE
#define	FE_CASTABLE_MAP			FALSE

// NOTE Visual Studio doesn't appear to allow unspecialized export
// of template static members, so we have to compare on a string hash
// Unreal on Linux also seems to have problems with it.
#define	FE_CASTABLE_STATIC		FE_USE_TEMPLATE_STATIC
#define	FE_CASTABLE_STRING_KEY	FALSE	//* vs hash (if not FE_CASTABLE_STATIC)

#if !FE_RTTI_HOMEBREW

typedef std::type_info fe_type_info;

template<typename T>
inline const fe_type_info& getTypeId(void)
{
	return typeid(T);
}

#else

class fe_type_info: public String
{
	public:
		const char*	name(void) const	{ return c_str(); }

		bool		before(const fe_type_info& a_rOther) const
					{	return (strcmp(name(),a_rOther.name())<0); }
};

template<typename T>
class fe_type_info_of: public fe_type_info
{
	public:
					fe_type_info_of(void)
					{	String::operator=(FE_TYPESTRING(T)); }
};

template<typename T>
inline const fe_type_info& getTypeId(void)
{
	static fe_type_info_of<T> info;
	return info;
}

class fe_type_hash
{
	public:
		U64	value(void) const	{ return m_value; }

	protected:
		U64	m_value;
};

template<typename T>
class fe_type_hash_of: public fe_type_hash
{
	public:
					fe_type_hash_of(void)
					{
						const String typeString=FE_TYPESTRING(T);
						m_value=typeString.computeFowlerNollVo1();

//						feLog("fe_type_hash_of<%s> is %p\n",
//								typeString.c_str(),m_value);
					}
};

template<typename T>
inline U64 getTypeHash(void)
{
	static fe_type_hash_of<T> hash;
	return hash.value();
}

#endif

/**************************************************************************//**
	@brief Base participation non-RTTI fallback dynamic casting mechanism
*//***************************************************************************/
class FE_DL_PUBLIC FE_CORE_PORT Castable
{
	public:

#if FE_RTTI_HOMEBREW
#if !FE_CASTABLE_STATIC
#if FE_CASTABLE_STRING_KEY
	typedef		String	IdType;
#else
	typedef		U64	IdType;
#endif
#else
	typedef		void*	IdType;
#endif

#if FE_CASTABLE_MAP
	class CastMap: public std::map<IdType,void*>
	{
		public:
			void	set(const IdType a_classId,void* a_pPtr)
					{	operator[](a_classId)=a_pPtr; }
			void*	lookup(const IdType a_classId) const
					{
						CastMap<IdType,void*>::const_iterator it=
								find(a_classId);
						return (it==end()? NULL: it->second);
					}
	};
#else
	class Casting
	{
		public:
					Casting(const IdType a_classId,void* a_pPtr):
						m_classId(a_classId),
						m_pPtr(a_pPtr)										{}
			IdType	m_classId;
			void*	m_pPtr;
	};
	class CastMap: public std::vector<Casting>
	{
		public:
			void	set(const IdType a_classId,void* a_pPtr)
					{	push_back(Casting(a_classId,a_pPtr)); }
			void*	lookup(const IdType a_classId) const
					{
						const U32 count=size();
						for(U32 index=0;index<count;index++)
						{
							const Casting& rCasting=operator[](index);
							if(rCasting.m_classId == a_classId)
							{
								return rCasting.m_pPtr;
							}
						}
						return NULL;
					}
	};
#endif

					Castable(void)											{}

#if FE_CPLUSPLUS >= 201103L
					//* We have to specify these as default because deleting
					//* the implicit copy-assignment and move-assignment
					//* operator below also automatically deletes the implicit
					//* copy-constructor and move-constructor.
					Castable(Castable&&)							=default;
					Castable(const Castable&)						=default;

					//* otherwise gcc warns with '-Wvirtual-move-assign'
		Castable&	operator=(Castable&&)							=delete;
		Castable&	operator=(const Castable&)						=delete;
#endif

#if !FE_CASTABLE_STATIC
#if FE_CASTABLE_STRING_KEY
		void		addCast(const String& a_classId,void* a_pPtr)
#else
		void		addCast(const IdType a_classId,void* a_pPtr)
#endif
					{	m_castMap.set(a_classId,a_pPtr); }
#else
		void		addCast(IdType a_classId,void* a_pPtr)
					{	m_castMap.set(a_classId,a_pPtr); }
#endif

					template<class X>
		X*			castTo(void) const;

	private:

		CastMap		m_castMap;
#endif
};

/**************************************************************************//**
	@brief Per-class participation non-RTTI fallback dynamic casting mechanism
*//***************************************************************************/
template<class T>
class FE_DL_EXPORT CastableAs: virtual public Castable
{
#if FE_RTTI_HOMEBREW
	public:
#if !FE_CASTABLE_STATIC
				CastableAs(void):
					Castable()
				{
#if FE_CASTABLE_DEBUG
#if FE_CASTABLE_STRING_KEY
					feLog("CastableAs::CastableAs<%s> %p id \"%s\"\n",
							FE_TYPESTRING(T).c_str(),
							(T*)this,classId().c_str());
#else
					feLog("CastableAs::CastableAs<%s> %p id %p\n",
							FE_TYPESTRING(T).c_str(),(T*)this,classId());
#endif
#endif

					addCast(classId(),(T*)this);
				}

#if FE_CASTABLE_STRING_KEY
static	const String&	classId(void)	{ return getTypeId<T>(); }
#else
static	IdType		classId(void)	{ return getTypeHash<T>(); }
#endif
#else

				CastableAs(void)
				{
					IdType id=CastableAs<T>::classId();

#if FE_CASTABLE_DEBUG
					feLog("CastableAs::CastableAs<%s> %p id %p\n",
							FE_TYPESTRING(T).c_str(),(T*)this,id);
#endif

					addCast(id,(T*)this);
				}

static	IdType			classId(void)	{ return &ms_classId; }

	private:
static	FE_DL_PUBLIC IdType	ms_classId;
#endif

#endif
};

#if FE_RTTI_HOMEBREW

#if FE_CASTABLE_STATIC
template<class T> FE_DL_PUBLIC Castable::IdType CastableAs<T>::ms_classId=NULL;
#endif

template<class X>
inline X* Castable::castTo(void) const
{
#if FE_CASTABLE_DEBUG
#if !FE_CASTABLE_STATIC
#if FE_CASTABLE_STRING_KEY
	feLog("Castable::castTo<%s> id \"%s\"\n",
			FE_TYPESTRING(X).c_str(),CastableAs<X>::classId().c_str());
#else
	feLog("Castable::castTo<%s> id %p\n",
			FE_TYPESTRING(X).c_str(),CastableAs<X>::classId());
#endif

	for(CastMap::const_iterator it=m_castMap.begin();
			it!=m_castMap.end();it++)
	{
#if FE_CASTABLE_MAP
#if FE_CASTABLE_STRING_KEY
		feLog("  \"%s\" %p\n",it->first.c_str(),it->second);
#else
		feLog("  %p %p\n",it->first,it->second);
#endif
#else
		feLog("  %p %p\n",it->m_classId,it->m_pPtr);
#endif
	}
#else
	feLog("Castable::castTo<%s> id %p\n",
			FE_TYPESTRING(X).c_str(),CastableAs<X>::classId());
	for(CastMap::const_iterator it=m_castMap.begin();
			it!=m_castMap.end();it++)
	{
#if FE_CASTABLE_MAP
		feLog("  %p %p\n",it->first,it->second);
#else
		feLog("  %p %p\n",it->m_classId,it->m_pPtr);
#endif
	}
#endif
#endif

	return (X*)(m_castMap.lookup(CastableAs<X>::classId()));
}
#endif

} // namespace

#if FE_RTTI_HOMEBREW
template<class T,class U>
inline T* fe_cast(U* a_pU)
{	return a_pU? a_pU->template castTo<T>(): NULL; }
#else
template<class T,class U>
inline T* fe_cast(U* a_pU) { return dynamic_cast<T*>(a_pU); }
#endif

#endif // __core_Castable_h__
