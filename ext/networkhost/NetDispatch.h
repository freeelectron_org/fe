/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __networkhost_NetDispatch_h__
#define __networkhost_NetDispatch_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief DispatchI for NetHost

	@ingroup networkhost
*//***************************************************************************/
class FE_DL_PUBLIC FE_NETWORKHOST_PORT NetDispatch:
	public fe::ext::Dispatch,
	public fe::Initialize<NetDispatch>
{
	public:
				NetDispatch(void);
virtual			~NetDispatch(void);

		void	initialize(void);

				//* as Dispatch
virtual	bool	call(const fe::String &a_name,fe::Array<fe::Instance>& a_argv);
virtual	bool	call(const fe::String &a_name,fe::InstanceMap& a_argv);

	private:

		fe::sp<NetHost>				m_spNetHost;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __networkhost_NetDispatch_h__ */
