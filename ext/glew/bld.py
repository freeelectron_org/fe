import sys
import os
import re
import subprocess
import shutil
import glob
forge = sys.modules["forge"]

def get_platform():
    if forge.fe_os == 'FE_WIN64':
        return "x64"
    else:
        return "Win32"

def get_configuration():
    if forge.codegen == 'debug':
        return "Debug"
    else:
        return "Release"

def setup(module):
    return

def auto(module):

    plat = get_platform()
    configuration = get_configuration()

    confirmed_glew = os.path.join(module.modPath, "glew-2.1.0")
    confirmed_build = confirmed_glew
    soDir = confirmed_glew
    libDir = confirmed_glew

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        confirmed_build = os.path.join(confirmed_build, "build", "VC17")
        soDir = os.path.join(soDir, "bin", configuration, plat)
        libDir = os.path.join(libDir, "lib", configuration, plat)
        if configuration == "Debug":
            soName = "glew32d.dll"
            libName = "glew32d.lib"
        else:
            soName = "glew32.dll"
            libName = "glew32.lib"
    else:
        soDir = os.path.join(soDir, "lib")
        libDir = ""
        soName = "libGLEW.so"
        libName = ""

    soTargetBuild = os.path.join(soDir, soName)
    soTargetBuild2 = os.path.join(libDir, libName)
    soTargetLib = os.path.join(forge.libPath, soName)

    if (not "FE_BLIND_TEST" in os.environ or os.environ["FE_BLIND_TEST"] == "0") and (not os.path.exists(soTargetBuild) or (soTargetBuild2 != "" and not os.path.exists(soTargetBuild2)) or not os.path.exists(soTargetLib)):
        forge.cprint(forge.WHITE,0,'pre-building GLEW')

        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            drive = confirmed_build.split(":")[0]
            command = drive + ": && "
            command += "cd " + confirmed_build + " && "
        else:
            command = "cd " + confirmed_build + ";"

        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            command += "msbuild glew.sln -target:glew_shared /p:Configuration=" + configuration + " /p:Platform=" + plat
        else:
            command += "make"

        if forge.pretty < 2:
            forge.color_on(0, forge.BLUE)
            sys.stdout.write(command + "\n")
            forge.color_off()

#       result = os.system(command)
        popen = subprocess.Popen(command,shell=True,bufsize=-1,
                stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        (p_out, p_err)=popen.communicate()
        result=popen.returncode

        if result == 0:
            shutil.copy(soTargetBuild, soTargetLib)
        else:
            print("\nGLEW failed to build:\n%s\n" % p_out)
            return "prebuild"

    if not os.path.exists(soTargetLib):
        return "missing"

    return None
