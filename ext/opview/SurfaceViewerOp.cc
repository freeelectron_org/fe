/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opview/opview.pmh>

#define FE_SVO_DEBUG			FALSE
#define FE_SVO_THREAD_DEBUG		FALSE
#define FE_SVO_RECORDER			FALSE
#define FE_SVO_UNIMPLEMENTED	(FE_CODEGEN<=FE_DEBUG)
#define FE_SVO_VBO				TRUE
#define FE_SVO_WAIT_FOR_SERIAL	FALSE	//* sleep when nothing seems to change

#if FE_OS==FE_LINUX
#include <malloc.h>
#endif

#include <chrono>

using namespace fe;
using namespace fe::ext;

SurfaceViewerOp::SurfaceViewerOp(void):
	m_jobs(0),
	m_us(0),
	m_milliDelay(0),
	m_rebuild(FALSE),
	m_idleCount(0),
	m_freezeFrame(FALSE),
	m_lastFrame(-1.0),
	m_slack(0.0),
	m_slackInc(1.0),
	m_slackMin(0.0),
	m_targeting(e_reframeObject),
	m_uvSpace(FALSE),
	m_cameraView(TRUE),
	m_cameraIndex(0),
	m_help(FALSE),
	m_spiral(FALSE),
	m_tracking(FALSE)
{
#if FE_SVO_DEBUG
	feLog("SurfaceViewerOp::SurfaceViewerOp\n");
#endif
	set(m_scrubBox);
}

DrawMode::DrawStyle SurfaceViewerOp::drawStyle(void) const
{
	//* TODO vertices on top of others
//	const BWORD showVertices=m_spMenu->catalog<bool>("/sv");
	const BWORD showEdges=m_spMenu->catalog<bool>("/se");
	const BWORD showFaces=m_spMenu->catalog<bool>("/sf");

	if(showFaces)
	{
		return showEdges? DrawMode::e_edgedSolid: DrawMode::e_solid;
	}
	if(showEdges)
	{
		return backfacing()? DrawMode::e_wireframe: DrawMode::e_outline;
	}

	return DrawMode::e_pointCloud;
}

sp<DrawableI> SurfaceViewerOp::drawable(I32 a_surfaceIndex)
{
	return a_surfaceIndex<I32(m_drawableArray.size())?
			m_drawableArray[a_surfaceIndex]: sp<DrawableI>(NULL);
}

I32 SurfaceViewerOp::drawableCount(void)
{	return m_drawableArray.size(); }

Array< sp<DrawableI> >& SurfaceViewerOp::subDrawableArray(I32 a_surfaceIndex)
{
	FEASSERT(a_surfaceIndex<I32(m_drawableTable.size()));

	return m_drawableTable[a_surfaceIndex];
}

sp<CameraI> SurfaceViewerOp::camera(I32 a_cameraIndex)
{
	return a_cameraIndex<I32(m_cameraArray.size())?
			m_cameraArray[a_cameraIndex]: sp<CameraI>(NULL);
}

I32 SurfaceViewerOp::cameraCount(void)
{	return m_cameraArray.size(); }

BWORD SurfaceViewerOp::getRebuild(void)
{
	if(!m_rebuild)
	{
		return FALSE;
	}
	m_rebuild=FALSE;
	return TRUE;
}

I32 SurfaceViewerOp::idleCount(void)
{
	return m_idleCount;
}

void SurfaceViewerOp::clearIdle(void)
{
	m_idleCount=0;
}

void SurfaceViewerOp::incrementIdle(void)
{
	m_idleCount++;
}

void SurfaceViewerOp::target(Targeting a_targeting)
{	m_targeting=a_targeting; }

SurfaceViewerOp::Targeting SurfaceViewerOp::targeting(void) const
{	return m_targeting; }

String SurfaceViewerOp::orthoAxis(void) const
{	return m_orthoAxis; }

BWORD SurfaceViewerOp::uvSpace(void) const
{	return m_uvSpace; }

BWORD SurfaceViewerOp::cameraView(void) const
{	return m_cameraView; }

I32 SurfaceViewerOp::cameraIndex(void) const
{	return m_cameraIndex; }

BWORD SurfaceViewerOp::help(void) const
{	return m_help; }

BWORD SurfaceViewerOp::spiral(void) const
{	return m_spiral; }

BWORD SurfaceViewerOp::tracking(void) const
{	return m_tracking; }

BWORD SurfaceViewerOp::edging(void) const
{	return m_spMenu->catalog<bool>("/de"); }

BWORD SurfaceViewerOp::grid(void) const
{	return m_spMenu->catalog<bool>("/dg"); }

BWORD SurfaceViewerOp::outline(void) const
{	return m_spMenu->catalog<bool>("/do"); }

BWORD SurfaceViewerOp::backfacing(void) const
{	return m_spMenu->catalog<bool>("/sb"); }

BWORD SurfaceViewerOp::lighting(void) const
{	return m_spMenu->catalog<bool>("/sl"); }

BWORD SurfaceViewerOp::shadows(void) const
{	return m_spMenu->catalog<bool>("/ss"); }

Real SurfaceViewerOp::pointSize(void) const
{	return m_spMenu->catalog<Real>("/dp"); }

Real SurfaceViewerOp::lineWidth(void) const
{	return m_spMenu->catalog<Real>("/dl"); }

I32 SurfaceViewerOp::refinement(void) const
{	return m_spMenu->catalog<I32>("/dr"); }

Real SurfaceViewerOp::updateSlack(void)
{
	const Real current=m_slack;

	const Real slackGain=0.1;

	m_slack+=m_slackInc*slackGain;
	if(m_slack<m_slackMin)
	{
		m_slack=m_slackMin;
		m_slackInc=fabs(m_slackInc);
		m_slackMin=0.0;
	}
	else if(m_slack>1.0)
	{
		m_slack=1.0;
		m_slackInc=1.0;
		m_slackMin=0.0;
		target(e_reframeObject);	//* default
	}

	return current;
}

SurfaceViewerOp::~SurfaceViewerOp(void)
{
#if FE_SVO_DEBUG
	feLog("SurfaceViewerOp::~SurfaceViewerOp\n");
#endif

	post(-1);
}

void SurfaceViewerOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<SpatialVector>("vertical")=SpatialVector(0.0,1.0,0.0);

	catalog<bool>("live")=FALSE;

	catalog<Real>("startFrame")=0.0;
	catalog<String>("startFrame","IO")="output";

	catalog<Real>("endFrame")=0.0;
	catalog<String>("endFrame","IO")="output";

	catalog<bool>("finished")=FALSE;
	catalog<String>("finished","IO")="output";

	catalog<I32>("stepping")=1;
	catalog<String>("stepping","IO")="output";

	catalog<bool>("zigzag")=FALSE;
	catalog<String>("zigzag","IO")="output";

	catalog<String>("keys");
	catalog<String>("keys","IO")="output";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Input Surface 2");
	catalog<bool>("Input Surface 2","optional")=TRUE;

	catalog< sp<Component> >("Input Surface 3");
	catalog<bool>("Input Surface 3","optional")=TRUE;

	catalog< sp<Component> >("Input Surface 4");
	catalog<bool>("Input Surface 4","optional")=TRUE;

	//* no auto-copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<bool>("Output Surface","recycle")=true;

	catalog<String>("warning");
	catalog<String>("error");

	sp<Master> spMaster=registry()->master();

	m_spMenu=spMaster->createCatalog("Viewer Menu");
	m_spMenu->catalog<String>("root")="/";
	m_spMenu->catalog<String>("/","keys")="fpds";

	m_spMenu->catalog<String>("/f","label")="file";
	m_spMenu->catalog<String>("/f","keys")="fnq";
	m_spMenu->catalog<String>("/ff","label")="  find";
	m_spMenu->catalog<String>("/ff");
	m_spMenu->catalog<String>("/fn","label")="  next";
	m_spMenu->catalog<String>("/fn","suggest")="button";
	m_spMenu->catalog<bool>("/fn")=FALSE;
	m_spMenu->catalog<String>("/fq","label")="  quit";
	m_spMenu->catalog<String>("/fq","suggest")="button";
	m_spMenu->catalog<bool>("/fq")=FALSE;
#if FE_SVO_UNIMPLEMENTED
	m_spMenu->catalog<String>("/f","keys")="rfnq";
	m_spMenu->catalog<String>("/fr","label")="  reload";
	m_spMenu->catalog<String>("/fr","suggest")="button";
#endif

	m_spMenu->catalog<String>("/p","label")="play";
	m_spMenu->catalog<String>("/p","keys")="rsz";
	m_spMenu->catalog<String>("/pr","label")="  rate";
	m_spMenu->catalog<I32>("/pr")=60;
	m_spMenu->catalog<String>("/ps","label")="  stepping";
	m_spMenu->catalog<I32>("/ps")=1;
	m_spMenu->catalog<String>("/pz","label")="  zigzag";
	m_spMenu->catalog<bool>("/pz")=FALSE;

	m_spMenu->catalog<String>("/d","label")="display";
	m_spMenu->catalog<String>("/d","keys")="mbcedigoplrswh";
	m_spMenu->catalog<String>("/dm","label")="  multisample";
	m_spMenu->catalog<I32>("/dm")=8;
	m_spMenu->catalog<String>("/db","label")="  brightness";
	m_spMenu->catalog<Real>("/db")=Real(0.4);
	m_spMenu->catalog<String>("/dc","label")="  contrast";
	m_spMenu->catalog<Real>("/dc")=Real(0.6);
	m_spMenu->catalog<String>("/de","label")="  edging";
	m_spMenu->catalog<bool>("/de")=TRUE;
	m_spMenu->catalog<String>("/dd","label")="  hud designators";
	m_spMenu->catalog<bool>("/dd")=FALSE;
	m_spMenu->catalog<String>("/di","label")="  hud indicators";
	m_spMenu->catalog<bool>("/di")=FALSE;
	m_spMenu->catalog<String>("/dg","label")="  grid";
	m_spMenu->catalog<bool>("/dg")=FALSE;
	m_spMenu->catalog<String>("/do","label")="  outline";
	m_spMenu->catalog<bool>("/do")=FALSE;
	m_spMenu->catalog<String>("/dp","label")="  point size";
	m_spMenu->catalog<Real>("/dp")=Real(4);
	m_spMenu->catalog<String>("/dl","label")="  line width";
	m_spMenu->catalog<Real>("/dl")=Real(0.5);
	m_spMenu->catalog<String>("/dr","label")="  refinement";
	m_spMenu->catalog<I32>("/dr")=1;
	m_spMenu->catalog<String>("/ds","label")="  select RGB";
	m_spMenu->catalog<Color>("/ds")=Color(0.7,0.6,0.4);
	m_spMenu->catalog<String>("/dw","label")="  window RGB";
	m_spMenu->catalog<Color>("/dw")=Color(0.2,0.2,0.25);
	m_spMenu->catalog<String>("/dh","label")="  highlight part";
	m_spMenu->catalog<String>("/dh")="conditional";
	m_spMenu->catalog<String>("/dh","keys")="nca";
	m_spMenu->catalog<String>("/dhn","label")="    never";
	m_spMenu->catalog<String>("/dhc","label")="    conditional";
	m_spMenu->catalog<String>("/dha","label")="    always";

	m_spMenu->catalog<String>("/s","label")="show";
	m_spMenu->catalog<String>("/s","keys")="nefbilstc";
	m_spMenu->catalog<String>("/sn","label")="  normals";
	m_spMenu->catalog<bool>("/sn")=FALSE;
	m_spMenu->catalog<String>("/se","label")="  edges";
	m_spMenu->catalog<bool>("/se")=FALSE;
	m_spMenu->catalog<String>("/sf","label")="  faces";
	m_spMenu->catalog<bool>("/sf")=TRUE;
	m_spMenu->catalog<String>("/sb","label")="  backfacing";
	m_spMenu->catalog<bool>("/sb")=FALSE;
	m_spMenu->catalog<String>("/si","label")="  instances";
	m_spMenu->catalog<bool>("/si")=TRUE;
	m_spMenu->catalog<String>("/sl","label")="  lighting";
	m_spMenu->catalog<bool>("/sl")=TRUE;
	m_spMenu->catalog<String>("/ss","label")="  shadows";
	m_spMenu->catalog<bool>("/ss")=FALSE;
	m_spMenu->catalog<String>("/st","label")="  textures";
	m_spMenu->catalog<bool>("/st")=FALSE;
	m_spMenu->catalog<String>("/sc","label")="  coloring";
	m_spMenu->catalog<String>("/sc")="color";
	m_spMenu->catalog<String>("/sc","keys")="ocntupf";
	m_spMenu->catalog<String>("/sco","label")="    off";
	m_spMenu->catalog<String>("/scc","label")="    color";
	m_spMenu->catalog<String>("/scn","label")="    normal";
	m_spMenu->catalog<String>("/sct","label")="    tangent";
	m_spMenu->catalog<String>("/scu","label")="    uv";
	m_spMenu->catalog<String>("/scp","label")="    part";
	m_spMenu->catalog<String>("/scf","label")="    file";
//	m_spMenu->catalog<String>("/sci","label")="    index";
//	m_spMenu->catalog<String>("/scm","label")="    mesh";
//	m_spMenu->catalog<String>("/scs","label")="    set";
#if FE_SVO_UNIMPLEMENTED
	m_spMenu->catalog<String>("/s","keys")+="va";
	m_spMenu->catalog<String>("/sv","label")="  vertices";
	m_spMenu->catalog<String>("/sa","label")="  alpha";
#endif

	sp<Catalog> spMasterCatalog=spMaster->catalog();

	const String hintDraw=
			spMasterCatalog->catalogOrDefault<String>(
			"hint:DrawChain","*.DrawOpenGL");

	//* NOTE direct GPU transfers are much faster
	spMasterCatalog->catalog<bool>("hint:openclWaitForGLX")=TRUE;

	spMasterCatalog->catalog<bool>("hint:openclDoubleBuffer")=TRUE;

#if FE_SVO_RECORDER
	feLog("SurfaceViewerOp::initialize creating VideoRecorder\n");
	//* TODO param
	m_spVideoRecorder=registry()->create("*.VideoRecorder");
//	m_spVideoRecorder->configure("test/chainvideo",240);
	m_spVideoRecorder->configure("test/video",4);
	spMasterCatalog->catalog<String>("hint:DrawI")=hintDraw;
#else
	spMasterCatalog->catalog<String>("hint:DrawI")=
			"*.DrawCached";
#endif

	spMasterCatalog->catalog<I32>("hint:multisample")=
			m_spMenu->catalog<I32>("/dm");

	m_spDrawHandler=Library::create<DrawHandler>(
			"SurfaceViewerOp::DrawHandler",library().raw());

	m_spEventHandler=Library::create<EventHandler>(
			"SurfaceViewerOp::EventHandler",library().raw());

#ifdef FE_USE_OPENCL
	//* HACK else fexOpenGLDL can't load
	registry()->manage("fexOpenCLDL");
#endif

	m_spQuickViewerI=registry()->create("QuickViewerI");
	if(!m_spQuickViewerI.isValid())
	{
		catalog<String>("error")="couldn't create viewer";
		return;
	}

	sp<CameraControllerI> spCameraController=
			m_spQuickViewerI->getCameraControllerI();
	spCameraController->setLookPoint(SpatialVector(0.0,0.0,0.0));
	spCameraController->setLookAngle(-80.0,30.0,1.0f);

	if(m_spDrawHandler.isValid())
	{
		m_spDrawHandler->setSurfaceViewerOp(sp<SurfaceViewerOp>(this));
		m_spQuickViewerI->insertDrawHandler(m_spDrawHandler);
	}

	if(m_spEventHandler.isValid())
	{
		m_spEventHandler->setSurfaceViewerOp(sp<SurfaceViewerOp>(this));
		m_spQuickViewerI->insertEventHandler(m_spEventHandler);
	}

	if(m_spVideoRecorder.isValid())
	{
		feLog("SurfaceViewerOp::initialize adding VideoRecorder\n");

		m_spQuickViewerI->insertDrawHandler(m_spVideoRecorder);
	}

	const Real zoom(1.0);
	const Vector2 center(0.5,0.5);

	m_spOrthoCam=new CameraEditable();
	m_spOrthoCam->setName("CameraEditable ortho");
//X	m_spOrthoCam->setProjection(CameraI::e_ortho);
	m_spOrthoCam->setOrtho(zoom,center);

	Mutex::confirm();
}

void SurfaceViewerOp::post(Real a_frame)
{
#if	FE_SVO_THREAD_DEBUG
	feLog("SurfaceViewerOp::post %.6G jobs %d\n",a_frame,m_jobs);
#endif

	if(m_spGang.isNull())
	{
		feLog("SurfaceViewerOp::post Null Gang\n");
		return;
	}

	//* TODO figure out why any earlier millisleep gets interrupted
	static I32 countDown=2;

	m_spGang->post(a_frame);
	m_jobs++;

	Real completed=Real(0);
	while(m_jobs)
	{
		while(m_spGang->acceptDelivery(completed))
		{
#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::post accept %.6G jobs %d\n",
					completed,m_jobs);
#endif

			m_jobs--;
		}

		if(m_jobs)
		{
			if(!countDown)
			{
				nanoSpin(1e3);
			}
		}
	}

	if(countDown)
	{
		countDown--;
	}

#if	FE_SVO_THREAD_DEBUG
	feLog("SurfaceViewerOp::post done jobs %d\n",m_jobs);
#endif
}

void SurfaceViewerOp::Worker::operate(void)
{
#if	FE_SVO_THREAD_DEBUG
	feLog("SurfaceViewerOp::Worker::operate\n");
#endif

	BWORD bound=FALSE;
	BWORD firstFrame=TRUE;

	Real job=Real(0);
	while(TRUE)
	{
		//* lock down for each iteration
		if(!m_pSurfaceViewerOp)
		{
#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::Worker::operate no SurfaceViewerOp\n");
#endif
			break;
		}

		sp<QuickViewerI> spQuickViewer=m_pSurfaceViewerOp->quickViewer();
		if(spQuickViewer.isNull())
		{
#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::Worker::operate no QuickViewerI\n");
#endif
			milliSleep(10);
			continue;
		}

		if(!bound)
		{
			spQuickViewer->open();

			sp<EventContextI> spEventContextI=
					spQuickViewer->getWindowI()->getEventContextI();
			spEventContextI->setPointerMotion(TRUE);

			sp<DrawI> spDrawI=spQuickViewer->getDrawI();
			if(spDrawI.isNull())
			{
#if	FE_SVO_THREAD_DEBUG
				feLog("SurfaceViewerOp::Worker::operate no DrawI\n");
#endif
				continue;
			}

			m_pSurfaceViewerOp->drawHandler()->bind(spDrawI);

			sp<HandlerI> spHandlerI(spDrawI);
			if(spHandlerI.isValid())
			{
				spQuickViewer->insertEventHandler(spDrawI);
			}

			//* boost initial brightness
			spDrawI->setBrightness(0.4);
			spDrawI->setContrast(0.6);

			bound=TRUE;
		}

#if	FE_SVO_THREAD_DEBUG
		feLog("SurfaceViewerOp::Worker::operate check\n");
#endif

#if FE_SVO_RECORDER
		m_hpJobQueue->waitForJob(job);
#else
		if(m_hpJobQueue->take(job))
#endif
		{
			if(job<0)
			{
#if	FE_SVO_THREAD_DEBUG
				feLog("SurfaceViewerOp::Worker::operate quit %.6G\n",job);
#endif

				m_hpJobQueue->deliver(job);
				break;
			}

#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::Worker::operate do job %.6G\n",job);
#endif

			const U32 us=systemMicroseconds();
			const I32 delta=us-m_us;
			m_us=us;

#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::Worker::operate delta %d us\n",delta);
#endif

			sp<DrawHandler> spDrawHandler=m_pSurfaceViewerOp->drawHandler();

			const I32 inputCount=m_pSurfaceViewerOp->drawableCount();
			sp<DrawableI>* spDrawables=new sp<DrawableI>[inputCount];

			const BWORD rebuild=m_pSurfaceViewerOp->getRebuild();
			if(rebuild)
			{
				m_pSurfaceViewerOp->clearIdle();
			}

			//* TODO account for low frame rates
			//* TODO tweak
			if(m_pSurfaceViewerOp->idleCount()>200)
			{
				feLog("IDLE %d\n",m_pSurfaceViewerOp->idleCount());
				milliSleep(250);
			}

			I32 surfaceIndex=0;
			for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
			{
				sp<DrawableI>& rspDrawable=spDrawables[inputIndex];

				rspDrawable=m_pSurfaceViewerOp->drawable(inputIndex);

				if(!rebuild)
				{
					continue;
				}

//				feLog("SurfaceViewerOp::Worker::operate"
//						" input %d/%d Drawable %p \"%s\"\n",
//						inputIndex,inputCount,
//						rspDrawable.raw(),
//						rspDrawable.isValid()?
//						rspDrawable->name().c_str(): "<null>");

				Array< sp<DrawableI> >& rSubDrawableArray=
						m_pSurfaceViewerOp->subDrawableArray(inputIndex);

				const String inputName=
						m_pSurfaceViewerOp->inputNameByIndex(inputIndex);

				sp<SurfaceAccessibleI> spInputAccessible;
				m_pSurfaceViewerOp->access(spInputAccessible,inputName,
						OperatorSurfaceCommon::e_quiet);

				sp<SurfaceAccessorI> spInputType;
				if(spInputAccessible.isValid())
				{
					m_pSurfaceViewerOp->access(spInputType,spInputAccessible,
							OperatorSurfaceCommon::e_primitive,"type",
							OperatorSurfaceCommon::e_quiet,
							OperatorSurfaceCommon::e_refuseMissing);
				}

				sp<SurfaceAccessorI> spInputName;
				if(spInputType.isValid())
				{
					m_pSurfaceViewerOp->access(spInputName,spInputAccessible,
							OperatorSurfaceCommon::e_primitive,"name",
							OperatorSurfaceCommon::e_quiet,
							OperatorSurfaceCommon::e_refuseMissing);
				}

				sp<SurfaceAccessorI> spInputParent;
				if(spInputName.isValid())
				{
					m_pSurfaceViewerOp->access(spInputParent,spInputAccessible,
							OperatorSurfaceCommon::e_primitive,"parent",
							OperatorSurfaceCommon::e_quiet,
							OperatorSurfaceCommon::e_refuseMissing);
				}

				if(spInputName.isValid())
				{
//					feLog("SurfaceViewerOp::Worker::operate"
//							" rebuild node surfaces\n");

					I32 subSurfaceCount=0;

					const I32 primitiveCount=spInputName->count();
					rSubDrawableArray.resize(primitiveCount);

					for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
							primitiveIndex++)
					{
						const String nodeType=
								spInputType->string(primitiveIndex);

//						feLog("SurfaceViewerOp::Worker::operate"
//								" nodeType \"%s\"\n",
//								nodeType.c_str());

						if(nodeType!="Mesh" && nodeType!="BasisCurves")
						{
							continue;
						}

//						const String nodeParent=
//								spInputParent->string(primitiveIndex);
						const String nodeName=
								spInputName->string(primitiveIndex);
						sp<DrawableI> spSubDrawable=
								spInputAccessible->surface(nodeName);

						rSubDrawableArray[subSurfaceCount++]=spSubDrawable;

//						feLog("SurfaceViewerOp::Worker::operate"
//								" %d/%d %d/%d spSubDrawable %p \"%s\"\n",
//								inputIndex,inputCount,
//								primitiveIndex,primitiveCount,
//								spSubDrawable.raw(),nodeName.c_str());

						if(spSubDrawable.isValid())
						{
							spDrawHandler->bindDrawable(
									inputIndex,surfaceIndex++,
									spSubDrawable,nodeName);
						}
					}

					rSubDrawableArray.resize(subSurfaceCount);
				}
				else
				{
					rSubDrawableArray.resize(1);

					sp<DrawableI> spSubDrawable=rspDrawable;

					rSubDrawableArray[0]=spSubDrawable;

//					feLog("SurfaceViewerOp::Worker::operate"
//							" %d/%d spSubDrawable %p\n",
//							inputIndex,inputCount,
//							spSubDrawable.raw());

					if(spSubDrawable.isValid())
					{
						spDrawHandler->bindDrawable(
								inputIndex,surfaceIndex++,
								spSubDrawable,"");
					}
				}
			}

			spDrawHandler->setFrame(job);
			spDrawHandler->setMicroseconds(delta);

			//* TODO only when surfaces change
			spDrawHandler->reapply();

			sp<CameraI> spCamera;
			if(m_pSurfaceViewerOp->cameraView())
			{
				const I32 camCount=m_pSurfaceViewerOp->cameraCount();
				if(camCount)
				{
					spCamera=m_pSurfaceViewerOp->camera(
							m_pSurfaceViewerOp->cameraIndex()%camCount);
				}
			}
			else if(m_pSurfaceViewerOp->uvSpace() ||
					!m_pSurfaceViewerOp->orthoAxis().empty())
			{
				const String axis=m_pSurfaceViewerOp->orthoAxis();

				//* NOTE uv and z both use plain identity
				SpatialTransform cameraMatrix;
				setIdentity(cameraMatrix);

				const SpatialVector vertical=
						m_pSurfaceViewerOp->catalog<SpatialVector>("vertical");
				const BWORD zUp=(vertical[2]>0.9);

				if(zUp)
				{
					if(axis=="x")
					{
						rotate(cameraMatrix,-90.0*degToRad,e_xAxis);
						rotate(cameraMatrix,-90.0*degToRad,e_zAxis);
					}
					else if(axis=="-x")
					{
						rotate(cameraMatrix,-90.0*degToRad,e_xAxis);
						rotate(cameraMatrix,90.0*degToRad,e_zAxis);
					}
					else if(axis=="y")
					{
						rotate(cameraMatrix,-90.0*degToRad,e_xAxis);
						rotate(cameraMatrix,180.0*degToRad,e_zAxis);
					}
					else if(axis=="-y")
					{
						rotate(cameraMatrix,-90.0*degToRad,e_xAxis);
					}
					else if(axis=="-z")
					{
						rotate(cameraMatrix,180.0*degToRad,e_xAxis);
					}
				}
				else
				{
					if(axis=="x")
					{
						rotate(cameraMatrix,-90.0*degToRad,e_yAxis);
					}
					else if(axis=="-x")
					{
						rotate(cameraMatrix,90.0*degToRad,e_yAxis);
					}
					else if(axis=="y")
					{
						rotate(cameraMatrix,90.0*degToRad,e_xAxis);
					}
					else if(axis=="-y")
					{
						rotate(cameraMatrix,-90.0*degToRad,e_xAxis);
					}
					else if(axis=="-z")
					{
						rotate(cameraMatrix,180.0*degToRad,e_yAxis);
					}
				}

				m_pSurfaceViewerOp->orthoCam()->setCameraMatrix(cameraMatrix);

				spCamera=m_pSurfaceViewerOp->orthoCam();
			}

			sp<CameraControllerI> spCameraController=
					spQuickViewer->getCameraControllerI();
			spCameraController->setRotationLock(spCamera.isValid());
			spCameraController->useCustomCamera(spCamera);

			//* NOTE get current targeting before updating slack value
			const Targeting targeting=m_pSurfaceViewerOp->targeting();
			const Real slack=m_pSurfaceViewerOp->updateSlack();

			if(spDrawables[0].isValid() &&
					(firstFrame || slack<1.0 || m_pSurfaceViewerOp->tracking()))
			{
				SpatialVector center;
				Real radius(0);
				if(m_pSurfaceViewerOp->uvSpace())
				{
					//* TODO SurfaceI::uvCenter() and uvRadius()
					set(center,1.0,0.0);
					radius=1.0;
				}
				else if(!spDrawHandler->getSelectRegion(
						center,radius))
				{
					sp<SurfaceI> spSurface=spDrawHandler->impactSurface();
					if(spSurface.isValid())
					{
						center=spSurface->center();
						radius=spSurface->radius();
					}
					else
					{
						//* compute bounds of everything

						BWORD first=TRUE;

						I32 surfaceIndex=0;
						for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
						{
							Array< sp<DrawableI> >& rSubDrawableArray=
									m_pSurfaceViewerOp->subDrawableArray(
									inputIndex);
							const I32 arrayCount=rSubDrawableArray.size();
							const BWORD hasSub=(arrayCount!=0);
							const I32 subCount=hasSub? arrayCount: 1;

							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								spSurface=hasSub? rSubDrawableArray[subIndex]:
										spDrawables[surfaceIndex];
								if(spSurface.isNull())
								{
									continue;
								}

								if(first)
								{
									center=spSurface->center();
									radius=spSurface->radius();
									first=FALSE;
									continue;
								}

								const SpatialVector center2=spSurface->center();
								const Real radius2=spSurface->radius();

								//* combine two bounding spheres
								const SpatialVector unitBetween=
										unitSafe(center2-center);
								const SpatialVector p1=
										center-radius*unitBetween;
								const SpatialVector p2=
										center2+radius2*unitBetween;
								center=0.5*(p1+p2);
								radius=0.5*magnitude(p2-p1);
							}
						}
					}
				}

				SpatialVector impactCenter;
				Real impactRadius(0);
				const BWORD impacted=spDrawHandler->getImpactRegion(
						impactCenter,impactRadius);
				if(impacted)
				{
					center=impactCenter;
					if(impactRadius>0.0)
					{
						radius=impactRadius;
					}
					else
					{
						radius*=0.1;
					}
				}

				if(radius<=0.0)
				{
					radius=1.0;
				}

				//* NOTE tweak
				const Real radiusScale=1.0;
				const Real minDistance=radius*radiusScale*
						(firstFrame? 10.0: (4.0-3.5*pow(slack,8.0)));
				const Real maxDistance=radius*radiusScale*
						(firstFrame? 10.0: (10.0+90.0*slack));
				const Real maxOffset=radius*
						(firstFrame? 0.0: (0.1+0.9*slack));
				const Real response=
						(firstFrame? 1.0: (1.0-0.96*slack));

				SpatialVector lookPoint=spCameraController->lookPoint();
				const SpatialVector correction=
						targeting==e_centerOrigin? -lookPoint: center-lookPoint;
				const Real offset=magnitude(correction);
				const Real scalar=offset-maxOffset;
				if(scalar>0.0)
				{
					lookPoint+=correction*(response*scalar/offset);
					spCameraController->setLookPoint(lookPoint);
				}

				if(targeting==e_reframeObject)
				{
					BWORD changeDistance=FALSE;

					Real distance=spCameraController->distance();
					if(distance<minDistance)
					{
						distance=(1.0-response)*distance+response*minDistance;
						changeDistance=TRUE;
					}
					else if(distance>maxDistance)
					{
						distance=(1.0-response)*distance+response*maxDistance;
						changeDistance=TRUE;
					}

					if(changeDistance)
					{
						const Real azimuth=spCameraController->azimuth();
						const Real downangle=spCameraController->downangle();

						spCameraController->setLookAngle(
								azimuth,downangle,distance);

//						feLog("look at %s dist %.6G\n",
//								c_print(lookPoint),distance);
					}
				}

				firstFrame=FALSE;
			}
			if(m_pSurfaceViewerOp->spiral())
			{
				spCameraController->setLookAngle(
						spCameraController->azimuth()+0.02*cos(m_spiralAngle),
						spCameraController->downangle()+0.02*sin(m_spiralAngle),
						spCameraController->distance());

				const I32 microseconds=
						m_pSurfaceViewerOp->drawHandler()->microseconds();
				if(microseconds>0)
				{
					m_spiralStep=3e-6*microseconds;
				}
				m_spiralAngle+=m_spiralStep;
			}

			delete[] spDrawables;

#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::Worker::operate done job %.6G\n",job);
#endif

			sp<QuickViewerI> spQuickViewerI=m_pSurfaceViewerOp->quickViewer();
			if(spQuickViewerI.isValid())
			{
				try
				{
					spQuickViewerI->run(1);
				}
				catch(const Exception &e)
				{
					feLog("SurfaceViewerOp::Worker::operate"
							" spQuickViewerI run caught fe::Exception\n\t%s\n",
							c_print(e));
				}
				catch(const std::exception& std_e)
				{
					feLog("SurfaceViewerOp::Worker::operate"
							" spQuickViewerI run caught std::exception\n\t%s\n",
							std_e.what());
				}
				catch(...)
				{
					feLog("SurfaceViewerOp::Worker::operate"
							" spQuickViewerI run caught unknown exception\n");
				}
			}

			m_hpJobQueue->deliver(job);

#if	FE_SVO_THREAD_DEBUG
			feLog("SurfaceViewerOp::Worker::operate delivered job %.6G\n",job);
#endif
		}
#if	FE_SVO_THREAD_DEBUG
		else
		{
			feLog("SurfaceViewerOp::Worker::operate no jobs\n");
		}
#endif

		//* HACK
//X		if(m_pSurfaceViewerOp->menu()->catalog<bool>("/sf"))
//X		{
//X			feLog("\n**** hide faces ****\n\n");
//X			m_pSurfaceViewerOp->menu()->catalog<bool>("/sf")=FALSE;
//X			m_pSurfaceViewerOp->menu()->catalog<bool>("/se")=TRUE;
//X		}
//X		else
//X		{
//X			feLog("\n**** show faces ****\n\n");
//X			m_pSurfaceViewerOp->menu()->catalog<bool>("/sf")=TRUE;
//X			m_pSurfaceViewerOp->menu()->catalog<bool>("/se")=FALSE;
//X		}

		//* HACK
//X		if(m_pSurfaceViewerOp->menu()->catalog<String>("/sc")=="off")
//X		{
//X			feLog("\n**** color normal ****\n\n");
//X			m_pSurfaceViewerOp->menu()->catalog<String>("/sc")="normal";
//X		}
//X		else
//X		{
//X			feLog("\n**** color off ****\n\n");
//X			m_pSurfaceViewerOp->menu()->catalog<String>("/sc")="off";
//X		}
//X		feLog("\n**** clear buffers ****\n\n");
//X		m_pSurfaceViewerOp->drawHandler()->clearBuffers();
	}

#if	FE_SVO_THREAD_DEBUG
	feLog("SurfaceViewerOp::Worker::operate done\n");
#endif
}

void SurfaceViewerOp::handle(Record& a_rSignal)
{
#if FE_SVO_DEBUG
	feLog("SurfaceViewerOp::handle\n");
#endif

	m_spDrawHandler->incrementSerial();

	const U32 us=systemMicroseconds();
	I32 delta=us-m_us;
	if(delta>0)
	{
		const I32 rate=fe::maximum(I32(1),m_spMenu->catalog<I32>("/pr"));
		const I32 desired=1e6/Real(rate);
		const I32 change=0.001*(desired-delta);

		if(change>0)
		{
			m_milliDelay+=fe::maximum(I32(1),change/4);
		}
		else if(change<0)
		{
			m_milliDelay=fe::maximum(I32(0),
					m_milliDelay+fe::minimum(I32(-1),change/4));
		}

//		const I32 fps=1e6/delta;
//		feLog("us %d delta %d/%d fps %d/%d change %d delay %d\n",
//				us,delta,desired,fps,rate,change,m_milliDelay);
	}
	m_us=us;
	if(m_milliDelay)
	{
		milliSleep(m_milliDelay);
	}

	SpatialVector vertical=unitSafe(catalog<SpatialVector>("vertical"));
	if(magnitude(vertical)<0.999)
	{
		set(vertical,0.0,1.0,0.0);
	}

	if(m_spQuickViewerI.isValid())
	{
		sp<CameraControllerI> spCameraController=
				m_spQuickViewerI->getCameraControllerI();
		spCameraController->setVertical(vertical);
	}

	if(m_spGang.isNull())
	{
		m_spGang=new Gang<Worker,Real>();
		m_spGang->start(sp<Counted>(this),1);
	}

	//* see surface/test/xSurface.cc

	//* TODO flexible
	const I32 inputCount=4;

	m_accessibleArray.resize(inputCount);
	m_drawableArray.resize(inputCount);
	m_drawableTable.resize(inputCount);

	const Real frame=currentFrame(a_rSignal);
	const BWORD live=catalog<bool>("live");

	if(live || m_lastFrame!=frame)
	{
		m_lastFrame=frame;
		m_rebuild=TRUE;
		m_freezeFrame=FALSE;

		for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
		{
			m_accessibleArray[inputIndex]=NULL;
			m_drawableArray[inputIndex]=NULL;
//X			m_drawableTable[inputIndex].clear();
		}

		if(m_spDrawHandler.isValid())
		{
			m_spDrawHandler->clearGeodesics();
		}

		m_cameraArray.clear();
	}

	if(!m_cameraArray.size())
	{
		for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
		{
			sp<CameraI> spCameraI(m_drawableArray[inputIndex]);
			if(spCameraI.isValid())
			{
				m_cameraArray.push_back(spCameraI);
			}
		}
	}

	for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
	{
		sp<SurfaceAccessibleI>& rspAccessible=
				m_accessibleArray[inputIndex];
		sp<DrawableI>& rspSubDrawable=m_drawableArray[inputIndex];

		String inputName="Input Surface";
		if(inputIndex)
		{
			inputName.sPrintf("Input Surface %d",inputIndex+1);
		}

		sp<SurfaceAccessibleI> spSurfaceAccessible;
		access(spSurfaceAccessible,inputName,e_quiet);
		if(rspAccessible!=spSurfaceAccessible)
		{
			rspAccessible=spSurfaceAccessible;
			rspSubDrawable=NULL;
		}

		if(rspSubDrawable.isNull())
		{
			sp<SurfaceI> spInput;
			access(spInput,inputName,inputIndex? e_quiet: e_warning);

			if(!inputIndex && spInput.isNull())
			{
				return;
			}

			if(spInput.isValid() && spInput->radius()<1e-3)
			{
				catalog<String>("warning")+=
						inputName + " has no apparent content;";
			}

			if(spSurfaceAccessible.isValid() && spInput.isValid())
			{
				spInput->setName(spSurfaceAccessible->name());
//				spInput->setSearch("SpatialTreeI.MapTree");
			}

			rspSubDrawable=spInput;
		}
	}

	if(m_drawableArray[0].isNull())
	{
		catalog<String>("error")="input surface not drawable";
		return;
	}

	BWORD closed=m_spMenu->catalog<bool>("/fq");

	if(m_spQuickViewerI.isValid())
	{
		sp<CameraControllerI> spCameraController=
				m_spQuickViewerI->getCameraControllerI();
		if(spCameraController.isValid() && spCameraController->closeEvent())
		{
			closed=TRUE;
		}

		if(closed)
		{
			feLog("CLOSING\n");
		}

		if(m_spDrawHandler.isValid())
		{
			m_spQuickViewerI->getWindowI()->setBackground(
					m_spDrawHandler->background());
		}
	}

	post(closed? -1: frame);

	catalog<bool>("finished")=closed;

	if(m_spDrawHandler.isValid())
	{
		catalog<Real>("startFrame")=m_spDrawHandler->startFrame();
		catalog<Real>("endFrame")=m_spDrawHandler->endFrame();
	}

#if FE_SVO_DEBUG
	feLog("SurfaceViewerOp::handle done\n");
#endif
}

//* TODO use SurfaceAccessibleBase::findNameAttribute()
String SurfaceViewerOp::findPartitionAttribute(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible,String a_nodeName)
{
	String partAttr;

	Array<SurfaceAccessibleI::Spec> specs;

	a_spSurfaceAccessible->attributeSpecs(specs,a_nodeName,
			SurfaceAccessibleI::e_primitive);

	const I32 specCount=specs.size();
	for(I32 specIndex=0;specIndex<specCount;specIndex++)
	{
		const SurfaceAccessibleI::Spec& rSpec=specs[specIndex];

		const String attrName=rSpec.name();
		if(attrName!="name" && attrName!="part")
		{
			continue;
		}

		const String attrType=rSpec.typeName();
		if(attrType!="string")
		{
			continue;
		}

		String valueString;

		sp<SurfaceAccessorI> spSurfaceAccessorI;
		access(spSurfaceAccessorI,a_spSurfaceAccessible,
				OperatorSurfaceCommon::e_primitive,attrName,
				OperatorSurfaceCommon::e_warning,
				OperatorSurfaceCommon::e_refuseMissing);

		if(spSurfaceAccessorI.isValid())
		{
			partAttr=attrName;

			//* priority
			if(attrName=="name")
			{
				break;
			}
		}
	}

	return partAttr;
}

SurfaceViewerOp::DrawHandler::DrawHandler(void):
	m_serial(1),
	m_lastSerial(0),
	m_frame(0.0),
	m_newFrame(0.0),
	m_minFrame(1e9),
	m_maxFrame(-1e9),
	m_startFrame(1e9),
	m_endFrame(-1e9),
	m_us(0),
	m_reopenCountdown(0),
	m_selectRegion(FALSE),
	m_isolateSurfaceIndex(-1),
	m_outlineAt(0),
	m_multiplication(1.0),
	m_characterWidth(10),
	m_textHeight(10),
	m_paperMode(FALSE),
	m_keepEdges(FALSE),
	m_impactSurfaceIndex(-1)
{
	m_spDrawGhost=new DrawMode();
	m_spDrawGhost->setDrawStyle(DrawMode::e_ghost);
	m_spDrawGhost->setBackfaceCulling(FALSE);

	m_spDrawPoints=new DrawMode();
	m_spDrawPoints->setDrawStyle(DrawMode::e_pointCloud);
	m_spDrawPoints->setPointSize(2.0);
	m_spDrawPoints->setAntialias(TRUE);

	m_spDrawWire=new DrawMode();
	m_spDrawWire->setDrawStyle(DrawMode::e_wireframe);
	m_spDrawWire->setPointSize(6.0);
	m_spDrawWire->setLineWidth(4.0);
	m_spDrawWire->setAntialias(TRUE);
	m_spDrawWire->setBackfaceCulling(FALSE);

	m_spDrawImpact=new DrawMode();
	m_spDrawImpact->setDrawStyle(DrawMode::e_foreshadow);
	m_spDrawImpact->setPointSize(8.0);
	m_spDrawImpact->setLineWidth(4.0);
	m_spDrawImpact->setLit(FALSE);
	m_spDrawImpact->setAntialias(TRUE);

	m_spDrawEdging=new DrawMode();
	m_spDrawEdging->copy(m_spDrawImpact);
	m_spDrawEdging->setLineWidth(6.0);

	m_spDrawSketch=new DrawMode();
	m_spDrawSketch->copy(m_spDrawEdging);
	m_spDrawSketch->setDrawStyle(DrawMode::e_solid);
	m_spDrawSketch->setLineWidth(8.0);

	m_spDrawPicking=new DrawMode();
	m_spDrawPicking->setDrawStyle(DrawMode::e_solid);
	m_spDrawPicking->setLit(FALSE);
	m_spDrawPicking->setLineWidth(2.0);
	m_spDrawPicking->setZBuffering(FALSE);

	m_spDrawText=new DrawMode();
	m_spDrawText->setDrawStyle(DrawMode::e_solid);
	m_spDrawText->setLit(FALSE);
	m_spDrawText->setLineWidth(2.0);
	m_spDrawText->setZBuffering(FALSE);
}

BWORD SurfaceViewerOp::DrawHandler::hideAll(I32 a_surfaceIndex)
{
	if(a_surfaceIndex<0 || a_surfaceIndex>=I32(m_nodeArray.size()))
	{
		return FALSE;
	}

	return m_nodeArray[a_surfaceIndex].m_hideAll;
}

BWORD SurfaceViewerOp::DrawHandler::ghostAll(I32 a_surfaceIndex)
{
	if(a_surfaceIndex<0 || a_surfaceIndex>=I32(m_nodeArray.size()))
	{
		return FALSE;
	}

	return m_nodeArray[a_surfaceIndex].m_ghostAll;
}

void SurfaceViewerOp::DrawHandler::setSurfaceViewerOp(
		sp<SurfaceViewerOp> a_spSurfaceViewerOp)
{
	m_hpSurfaceViewerOp=a_spSurfaceViewerOp;

	m_spRulerOp=a_spSurfaceViewerOp->registry()->create(
			"*.RulerOp");
	if(m_spRulerOp.isValid())
	{
		m_spRulerOp->catalog<Real>("excess")=2.0;
		m_spRulerOp->catalog<bool>("cumulative")=true;

#if TRUE
		//* HACK jpweber ground
		m_spRulerOp->catalog<Real>("excess")=0.0;
		m_spRulerOp->catalog<bool>("cumulative")=false;
		m_spRulerOp->catalog<bool>("splatter")=false;
//		m_spRulerOp->catalog<String>("walls")="xy";
#endif
	}

	//* NOTE without loading an image, the default texture will be used
	if(m_spTextureImage.isNull())
	{
		m_spTextureImage=a_spSurfaceViewerOp->registry()->create("ImageI");
	}
}

void SurfaceViewerOp::DrawHandler::bindDrawable(I32 a_inputIndex,
	I32 a_surfaceIndex,sp<DrawableI> a_spDrawable,String a_nodeName)
{
	if(I32(m_nodeArray.size())<a_surfaceIndex+1)
	{
		m_nodeArray.resize(a_surfaceIndex+1);
	}

	SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];
	rSurfaceNode.m_inputIndex=a_inputIndex;
	rSurfaceNode.m_nodeName=a_nodeName;

	sp<DrawableI>& rspDrawable=rSurfaceNode.m_spDrawable;
	if(rspDrawable!=a_spDrawable)
	{
		rspDrawable=a_spDrawable;
		clearBuffer(a_surfaceIndex);
	}
}

void SurfaceViewerOp::DrawHandler::clearEmphasis(void)
{
	const I32 surfaceCount=m_nodeArray.size();
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		m_nodeArray[surfaceIndex].m_emphasis=0.0;
	}
}

void SurfaceViewerOp::DrawHandler::setEmphasis(
	I32 a_surfaceIndex,Real a_emphasis)
{
	if(a_surfaceIndex<0 || a_surfaceIndex>=I32(m_nodeArray.size()))
	{
		return;
	}

	SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];
	rSurfaceNode.m_emphasis=a_emphasis;
}

void SurfaceViewerOp::DrawHandler::clearGeodesics(void)
{
	const I32 surfaceCount=m_nodeArray.size();
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		m_nodeArray[surfaceIndex].m_spSurfaceGeodesic=NULL;
	}
}

void SurfaceViewerOp::DrawHandler::setFrame(Real a_frame)
{
	m_newFrame=a_frame;

	if(m_minFrame>m_newFrame)
	{
		if(m_startFrame==m_minFrame)
		{
			m_startFrame=m_newFrame;
		}
		m_minFrame=m_newFrame;
	}
	if(m_maxFrame<m_newFrame)
	{
		if(m_endFrame==m_maxFrame)
		{
			m_endFrame=m_newFrame;
		}
		m_maxFrame=m_newFrame;
	}
}

void SurfaceViewerOp::DrawHandler::updateFrame(void)
{
	if(m_frame!=m_newFrame)
	{
		m_frame=m_newFrame;
		m_keepEdges=FALSE;
		m_spSurfaceImpact=NULL;

		setPickingAccessible(sp<SurfaceAccessibleI>(NULL));

		if(m_impactSurfaceIndex<0)
		{
			m_spImpactSurface=NULL;
			m_spImpactAccessible=NULL;
			m_outline.clear();
			m_outlineAt=0;
		}
		else
		{
			m_hpSurfaceViewerOp->access(m_spImpactAccessible,
					m_impactInputName,e_quiet);

			if(m_impactSurfaceIndex>=0 &&
					m_impactSurfaceIndex<I32(m_nodeArray.size()))
			{
				m_spImpactSurface=
						m_nodeArray[m_impactSurfaceIndex].m_spDrawable;
			}
			else
			{
				m_spImpactSurface=NULL;
			}
		}
	}
}

BWORD SurfaceViewerOp::DrawHandler::getImpactRegion(
	SpatialVector& a_rCenter,Real& a_rRadius) const
{
	if(m_spSurfaceImpact.isNull())
	{
		return FALSE;
	}
	a_rCenter=m_spSurfaceImpact->intersection();
	a_rRadius=0.0; //* TODO
	return TRUE;
}

BWORD SurfaceViewerOp::DrawHandler::getSelectRegion(
	SpatialVector& a_rCenter,Real& a_rRadius) const
{
	if(!m_selectRegion)
	{
		return FALSE;
	}
	a_rCenter=m_selectCenter;
	a_rRadius=m_selectRadius;
	return TRUE;
}

void SurfaceViewerOp::DrawHandler::setImpactAccessible(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible)
{
	if(m_spImpactAccessible!=a_spSurfaceAccessible)
	{
		m_outlineAt=0;
	}

	m_spImpactAccessible=a_spSurfaceAccessible;

	if(m_spImpactAccessible.isNull() ||
			!m_spImpactAccessible->copyOutline(m_outline))
	{
		m_outline.clear();
	}
}

void SurfaceViewerOp::DrawHandler::pickIncrement(I32 a_increment)
{
	I32 surfaceIndex=impactSurfaceIndex()+a_increment;
	const I32 surfaceCount=m_nodeArray.size();

	if(surfaceIndex<0)
	{
		surfaceIndex=surfaceCount-1;
	}
	if(surfaceIndex>=surfaceCount)
	{
		surfaceIndex=0;
	}

	pick(surfaceIndex);
}

void SurfaceViewerOp::DrawHandler::pick(I32 a_surfaceIndex)
{
//	feLog("SurfaceViewerOp::DrawHandler::pick %d/%d\n",
//			a_surfaceIndex,m_nodeArray.size());

	setPickingAccessible(sp<SurfaceAccessibleI>(NULL));

	sp<SurfaceI> spSurface=surface(a_surfaceIndex);
	if(spSurface!=impactSurface())
	{
		unpick();
		unpick();
	}

	if(a_surfaceIndex>=0 && a_surfaceIndex<I32(m_nodeArray.size()))
	{
		sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;

		SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];
		const I32 inputIndex=rSurfaceNode.m_inputIndex;

		const String inputName=spSurfaceViewerOp->inputNameByIndex(inputIndex);

		spSurfaceViewerOp->access(m_spImpactAccessible,inputName,e_quiet);

		setSurfaceImpact(sp<SurfaceI::ImpactI>(NULL));
		setImpactSurface(spSurface);
		setImpactNodeName(rSurfaceNode.m_nodeName);
		setImpactInputName(inputName);
		setImpactSurfaceIndex(spSurface.isValid()? a_surfaceIndex: -1);
	}
}

void SurfaceViewerOp::DrawHandler::unpick(void)
{
	if(m_spSurfaceImpact.isNull() && m_selectName.empty())
	{
		m_spImpactAccessible=NULL;
		m_impactNodeName="";
		m_impactInputName="";
		m_impactSurfaceIndex= -1;
		m_spImpactSurface=NULL;
		m_outline.clear();
		m_outlineAt=0;

		m_isolateSurfaceIndex= -1;
		m_isolatePart="";
	}

	m_spSurfaceImpact=NULL;
	m_selectName="";
	m_keepEdges=FALSE;
}

void SurfaceViewerOp::DrawHandler::hideSelected(void)
{
	if(!m_isolatePart.empty())
	{
		return;
	}

	if(m_selectName.empty())
	{
		//* hide nothing means show all
		if(m_impactSurfaceIndex<0)
		{
			const I32 surfaceCount=m_nodeArray.size();
			for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
			{
				SurfaceNode& rSurfaceNode=m_nodeArray[surfaceIndex];

				rSurfaceNode.m_hideList.clear();
				rSurfaceNode.m_hideAll=FALSE;

				clearBuffer(surfaceIndex);
				applyHidden(surfaceIndex);
			}
			return;
		}

		SurfaceNode& rSurfaceNode=m_nodeArray[m_impactSurfaceIndex];

		rSurfaceNode.m_hideList.clear();
		rSurfaceNode.m_hideAll=TRUE;

		clearBuffer(m_impactSurfaceIndex);
		applyHidden(m_impactSurfaceIndex);
		unpick();
		unpick();
		return;
	}

	m_nodeArray[m_impactSurfaceIndex].m_hideList.push_back(m_selectName);

	clearBuffer(m_impactSurfaceIndex);
	applyHidden(m_impactSurfaceIndex);
	unpick();
}

void SurfaceViewerOp::DrawHandler::clearBuffer(I32 a_surfaceIndex)
{
	if(a_surfaceIndex>=0 && a_surfaceIndex<I32(m_nodeArray.size()))
	{
		SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];

		rSurfaceNode.m_spSolidBuffer=NULL;
		rSurfaceNode.m_spWireBuffer=NULL;
	}
}

void SurfaceViewerOp::DrawHandler::clearBuffers(void)
{
	const I32 surfaceCount=m_nodeArray.size();
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		clearBuffer(surfaceIndex);
	}
	m_spSurfaceImpact=NULL;
}

void SurfaceViewerOp::DrawHandler::applyHidden(I32 a_surfaceIndex)
{
	if(!m_isolatePart.empty())
	{
		return;
	}

	sp<SurfaceI> spInput=surface(a_surfaceIndex);
	if(spInput.isNull())
	{
		return;
	}

	SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];

	if(rSurfaceNode.m_ghostAll)
	{
		spInput->setPartitionFilter(".*");
		return;
	}
	if(rSurfaceNode.m_hideAll)
	{
		spInput->setPartitionFilter("");
		return;
	}

	const Array<String>& rHideList=rSurfaceNode.m_hideList;
	const I32 hideCount=rHideList.size();

	const Array<String>& rGhostList=rSurfaceNode.m_ghostList;
	const I32 ghostCount=rGhostList.size();

	if(!hideCount && !ghostCount)
	{
		spInput->setPartitionFilter(".*");
		return;
	}

#if FALSE
	//* not word "one" or "two": ((?!\b(one|two)\b).)*

	String filter="((?!\\b(";
	for(I32 hideIndex=0;hideIndex<hideCount;hideIndex++)
	{
		filter+=(hideIndex? "|": "")+rHideList[hideIndex].replace("/","\\\\/");
	}
	for(I32 ghostIndex=0;ghostIndex<ghostCount;ghostIndex++)
	{
		filter+=((hideCount || ghostIndex)? "|": "")+rGhostList[ghostIndex];
	}
	filter+=")\\b).)*";

	spInput->setPartitionFilter(filter);
#else
	String filter="^(";
	for(I32 hideIndex=0;hideIndex<hideCount;hideIndex++)
	{
		filter+=(hideIndex? "|": "")+
				rHideList[hideIndex].replace("/","\\\\/");
	}
	for(I32 ghostIndex=0;ghostIndex<ghostCount;ghostIndex++)
	{
		filter+=((hideCount || ghostIndex)? "|": "")+
				rGhostList[ghostIndex].replace("/","\\\\/");
	}
	filter+=")$";

	spInput->setPartitionFilter(filter,PartitionI::e_notMatchRegex);
#endif

//	feLog("  filter \"%s\"\n",filter.c_str());
}

void SurfaceViewerOp::DrawHandler::ghostSelected(void)
{
	if(!m_isolatePart.empty())
	{
		return;
	}

	if(m_selectName.empty())
	{
		//* ghost nothing means unghost all
		if(m_impactSurfaceIndex<0)
		{
			const I32 surfaceCount=m_nodeArray.size();
			for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
			{
				SurfaceNode& rSurfaceNode=m_nodeArray[surfaceIndex];

				rSurfaceNode.m_ghostList.clear();
				rSurfaceNode.m_spGhostPartition=NULL;

				rSurfaceNode.m_ghostAll=FALSE;

				clearBuffer(surfaceIndex);
				applyHidden(surfaceIndex);
			}
			return;
		}

		SurfaceNode& rSurfaceNode=m_nodeArray[m_impactSurfaceIndex];

		rSurfaceNode.m_ghostList.clear();
		rSurfaceNode.m_ghostAll=TRUE;

		clearBuffer(m_impactSurfaceIndex);
		applyGhosted(m_impactSurfaceIndex);
		applyHidden(m_impactSurfaceIndex);
		unpick();
		unpick();
		return;
	}

	Array<String>& rGhostList=m_nodeArray[m_impactSurfaceIndex].m_ghostList;

	rGhostList.push_back(m_selectName);

	clearBuffer(m_impactSurfaceIndex);
	applyGhosted(m_impactSurfaceIndex);
	applyHidden(m_impactSurfaceIndex);
	unpick();
}

void SurfaceViewerOp::DrawHandler::applyGhosted(I32 a_surfaceIndex)
{
	sp<SurfaceI> spInput=surface(a_surfaceIndex);
	if(spInput.isNull())
	{
		return;
	}

	SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];

	sp<PartitionI>& rspGhostPartition=rSurfaceNode.m_spGhostPartition;

	const Array<String>& rGhostList=rSurfaceNode.m_ghostList;
	const I32 ghostCount=rGhostList.size();

	if(!ghostCount && !rSurfaceNode.m_ghostAll)
	{
		rspGhostPartition=NULL;
		return;
	}

	if(rspGhostPartition.isNull())
	{
		rspGhostPartition=spInput->createPartition();

		if(rspGhostPartition.isNull())
		{
//			feLog("SurfaceViewerOp::DrawHandler::applyGhosted"
//					" could not create partition\n");
			return;
		}
	}

	if(rSurfaceNode.m_ghostAll)
	{
		rspGhostPartition->select(".*");
		return;
	}

	rspGhostPartition->select("");

	for(I32 ghostIndex=0;ghostIndex<ghostCount;ghostIndex++)
	{
		rspGhostPartition->add(rGhostList[ghostIndex]);
	}
}

void SurfaceViewerOp::DrawHandler::reapply(void)
{
	const I32 surfaceCount=m_nodeArray.size();
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		applyGhosted(surfaceIndex);
		applyHidden(surfaceIndex);
	}
}

void SurfaceViewerOp::DrawHandler::isolateToggle(void)
{
	//* clear existing isolate
	if(m_isolateSurfaceIndex>=0)
	{
		//* restore existing hide
		clearBuffer(m_isolateSurfaceIndex);
		applyHidden(m_isolateSurfaceIndex);

		m_isolateSurfaceIndex= -1;
		m_isolatePart="";

		return;
	}

	if(m_impactSurfaceIndex<0)
	{
		return;
	}

	m_isolateSurfaceIndex=m_impactSurfaceIndex;
	m_isolatePart=m_selectName;

	//* isolate on
	sp<SurfaceI> spInput=surface(m_isolateSurfaceIndex);
	if(spInput.isValid())
	{
		spInput->setPartitionFilter(m_selectName.empty()? String(".*"):
				m_isolatePart.replace("/","\\\\/"));
	}
}

void SurfaceViewerOp::DrawHandler::paperToggle(void)
{
	m_paperMode= !m_paperMode;
	m_spSurfaceImpact=NULL;
	m_selectName="";
}

void SurfaceViewerOp::DrawHandler::setPickingAccessible(
	sp<SurfaceAccessibleI> a_spPickingAccessible)
{
	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	if(spSurfaceViewerOp.isNull())
	{
		m_pickingSurfaceIndex= -1;
		m_pickingNodeName="";
		m_spPickingImpact=NULL;
		m_spPickingAccessible=NULL;
		m_spPickingGeodesic=NULL;
		return;
	}

	if(m_spPickingAccessible!=a_spPickingAccessible)
	{
		m_spPickingAccessible=a_spPickingAccessible;
		if(m_spPickingAccessible.isNull())
		{
			m_pickingNodeName="";
			m_spPickingImpact=NULL;
			m_spPickingGeodesic=NULL;
			return;
		}

		if(!m_spPickingAccessible->count(m_pickingNodeName,
				SurfaceAccessibleI::e_primitive))
		{
			m_spPickingGeodesic=NULL;
			return;
		}

		sp<SurfaceAccessorI> spPrimitiveProperties;
		spSurfaceViewerOp->access(spPrimitiveProperties,
				a_spPickingAccessible,
				OperatorSurfaceCommon::e_primitive,
				OperatorSurfaceCommon::e_properties,
				OperatorSurfaceCommon::e_warning,
				OperatorSurfaceCommon::e_refuseMissing);
		if(spPrimitiveProperties.isValid() && spPrimitiveProperties->count() &&
				spPrimitiveProperties->integer(0,e_openCurve))
		{
			m_spPickingGeodesic=NULL;
			return;
		}

		if(m_pickingSurfaceIndex<0 ||
				m_pickingSurfaceIndex>=I32(m_nodeArray.size()))
		{
			return;
		}

		//* NOTE cache the four geodesics, as made, clearing on frame change
		sp<SurfaceTrianglesAccessible>&	rspGeodesic=
				m_nodeArray[m_pickingSurfaceIndex].m_spSurfaceGeodesic;

		if(rspGeodesic.isNull())
		{
			rspGeodesic=spSurfaceViewerOp->registry()->create(
					"*.SurfaceTrianglesAccessible");
			if(rspGeodesic.isValid())
			{
				rspGeodesic->bind(m_spPickingAccessible);
			}
		}

		m_spPickingGeodesic=rspGeodesic;
	}
}

Color SurfaceViewerOp::DrawHandler::background(void)
{
	const Color white(1.0,1.0,1.0,0.0);
	const Color paperYellow(0.8,0.8,0.6);
//	const Color dark(0.20,0.20,0.25);

	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	sp<Catalog>& rspMenu=spSurfaceViewerOp->menu();
	const Color dark=rspMenu->catalog<Color>("/dw");

	const Real brightness=m_spDrawI.isValid()? m_spDrawI->brightness(): 0.0;

	return m_paperMode? paperYellow: Color(dark+0.1*(brightness-0.1)*white);
}

Real SurfaceViewerOp::DrawHandler::gaugeAlpha(
		sp<SurfaceI::GaugeI> a_spGauge,const SpatialVector& a_point)
{
	Real distance=Real(-1);

	if(a_spGauge.isValid())
	{
		//* NOTE radius check is built-in
		distance=a_spGauge->distanceTo(a_point);
		if(distance<0.0 || distance==INFINITY)
		{
			return 0.0;
		}
	}
	else
	{
		const Real pickRadius2=m_pickRadius*m_pickRadius;
		const Real mag2=magnitudeSquared(a_point-m_pickPoint);
		if(mag2>pickRadius2)
		{
			return 0.0;
		}
		distance=sqrt(mag2);
	}

	const Real proximity=Real(1)-distance/m_pickRadius;
	return proximity;
}
void SurfaceViewerOp::DrawHandler::handle(Record& a_render)
{
	if(!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(a_render.layout()->scope());
	}
	if(!m_asViewer.viewer_spatial.check(a_render) ||
			!m_asViewer.viewer_layer.check(a_render))
	{
		return;
	}

	const I32& rLayer=m_asViewer.viewer_layer(a_render);

#if FE_SVO_WAIT_FOR_SERIAL
	if(!rLayer)
	{
//		feLog("SurfaceViewerOp::DrawHandler::handle serial %d %d\n",
//				m_lastSerial,m_serial);

		I32 countdown=16;
		U32 serial=m_serial;
		while(serial==m_lastSerial && countdown)
		{
			milliSleep(4);
			serial=m_serial;
			countdown--;
		}
//		feLog("  countdown %d\n",countdown);
		m_lastSerial=serial;
	}
#endif

	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	const BWORD uvSpace=spSurfaceViewerOp->uvSpace();

	if(rLayer==0)
	{
		updateFrame();

		sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;

		sp<WindowI> spWindowI=spSurfaceViewerOp->quickViewer()->getWindowI();
		if(spWindowI.isValid())
		{
			sp<EventContextI> spEventContextI=
					spWindowI->getEventContextI();

			m_multiplication=fe::maximum(m_multiplication,
					spEventContextI->multiplication());

			I32 fontAscent=0;
			I32 fontDescent=0;
//X			spEventContextI->fontHeight(&fontAscent,&fontDescent);
			m_spDrawI->font()->fontHeight(&fontAscent,&fontDescent);
			m_textHeight=fe::maximum(m_textHeight,fontAscent+fontDescent);

//X			const I32 fontWidth=spEventContextI->pixelWidth("W");
			const I32 fontWidth=m_spDrawI->font()->pixelWidth("W");
			m_characterWidth=fe::maximum(m_characterWidth,fontWidth);

//			feLog("ascent %d descent %d width %d"
//					" characterWidth %d textHeight %d\n",
//					fontAscent,fontDescent,fontWidth,
//					m_characterWidth,m_textHeight);
		}

		sp<Catalog>& rspMenu=spSurfaceViewerOp->menu();

		if(m_reopenCountdown)
		{
			feLog("reopen %d\n",m_reopenCountdown);
			if(m_reopenCountdown==2)
			{
				spSurfaceViewerOp->quickViewer()->reopen();

				//* HACK reinitialize lights
				m_spDrawI->setBrightness(-1.0);
			}
			if(m_reopenCountdown==1)
			{
				//* HACK reinitialize lights
				m_spDrawI->setBrightness(rspMenu->catalog<Real>("/db"));
			}
			m_reopenCountdown--;
		}
		else
		{
			rspMenu->catalog<Real>("/db")=m_spDrawI->brightness();
			rspMenu->catalog<Real>("/dc")=m_spDrawI->contrast();
		}

		sp<DrawMode> spDrawMode=m_spDrawI->drawMode();
		FEASSERT(spDrawMode.isValid());

		spDrawMode->setDrawStyle(spSurfaceViewerOp->drawStyle());
		spDrawMode->setPointSize(spSurfaceViewerOp->pointSize());
		spDrawMode->setLineWidth(spSurfaceViewerOp->lineWidth());
		spDrawMode->setRefinement(spSurfaceViewerOp->refinement());
		spDrawMode->setFrontfaceCulling(FALSE);
		spDrawMode->setBackfaceCulling(spSurfaceViewerOp->backfacing());
		spDrawMode->setLit(spSurfaceViewerOp->lighting());
		spDrawMode->setShadows(spSurfaceViewerOp->shadows());
		spDrawMode->setUvSpace(uvSpace);
		spDrawMode->setColoring(
				DrawMode::stringToColoring(rspMenu->catalog<String>("/sc")));
		spDrawMode->setTextureImage(
				rspMenu->catalog<bool>("/st")?
				m_spTextureImage: sp<ImageI>(NULL));

		m_spDrawGhost->setRefinement(spSurfaceViewerOp->refinement());
	}

	m_spDrawWire->setUvSpace(uvSpace);
	m_spDrawImpact->setUvSpace(uvSpace);

	const BWORD isOrtho=(uvSpace || !spSurfaceViewerOp->orthoAxis().empty());

	if(rLayer==0 && isOrtho)
	{
		sp<ViewI> spView=m_spDrawI->view();
		const Box2i viewport=spView->viewport();
		const Vector2i size=viewport.size();

		const I32 edgeX=14*m_characterWidth;
		const I32 marginY=(m_maxFrame>m_minFrame)? 40: 0;
		const Box2i subPort(
				Vector2i(viewport[0]+edgeX,viewport[1]+marginY),
				Vector2i(size[0]-2*edgeX,size[1]-marginY-m_textHeight));

		sp<CameraI> spOrthoCam=spSurfaceViewerOp->orthoCam();
		const SpatialTransform cameraMatrix=spOrthoCam->cameraMatrix();

		//* NOTE zoom the the pixel count between unit values (1.0, 2.0, 3.0)
		Real zoom;
		Vector2 center;
		spOrthoCam->getOrtho(zoom,center);

		//* TODO adaptive scale
		if(zoom<16.0)
		{
			return;
		}

		//* NOTE negative zoom can assert in ViewCommon::computeOrtho()

		const Matrix<4,4,Real> cameraProjection=
				ViewCommon::computeOrtho(zoom,center,size);
//		feLog("size %s zoom %.6G\n",c_print(size),zoom);

		SpatialTransform invCameraMatrix;
		invert(invCameraMatrix,cameraMatrix);
		const SpatialVector target=invCameraMatrix.translation();
//		feLog("target %s\n",c_print(target));

		const Color green(0.0,1.0,0.0);
		const Color mediumGreen(0.0,0.5,0.0);
		const Color backColor=background();

		m_spDrawI->pushDrawMode(m_spDrawText);

		//* NOTE uv uses default setup on z
		const String orthoAxis=spSurfaceViewerOp->orthoAxis();

		for(I32 pass=0;pass<2;pass++)
		{
			I32 axis=pass;
			if(orthoAxis=="x" || orthoAxis=="-x")
			{
				axis=pass? 1: 2;
			}
			else if(orthoAxis=="y" || orthoAxis=="-y")
			{
				axis=pass? 2: 0;
			}

			const Real half=0.5*size[pass]/zoom;
			const Real min=target[axis]-half;
			const Real max=target[axis]+half;

			for(Real f=I32(min);f<max;f+=1.0)
			{
//				feLog("f %.6G\n",f);

				SpatialVector world(0.0,0.0,0.0);
				world[axis]=f;

				SpatialVector location;
				transformVector(cameraMatrix,world,location);

				Vector4 location4=location;
				Vector4 pixel;
				transformVector(cameraProjection,location4,pixel);

				const Vector2i raster(
						size[0]*(pixel[0]+1)/2,
						size[1]*(pixel[1]+1)/2);

				if(raster[pass]<subPort[pass] ||
						raster[pass]>subPort[pass]+subPort.size()[pass])
				{
					continue;
				}

				SpatialVector line[2];
				line[0]=subPort;
				line[1]=subPort+subPort.size();

				line[0][pass]=raster[pass];
				line[1][pass]=raster[pass];

				m_spDrawI->drawLines(line,NULL,2,
						DrawI::e_strip,false,&mediumGreen);

				String text;
				text.sPrintf("%.3G",f);
				drawLabel(m_spDrawI,
						pass? line[0]: line[1],
						text,FALSE,1,green,NULL,&backColor);
			}
		}

		m_spDrawI->popDrawMode();
	}

	if(rLayer==1)
	{
#if !FE_SVO_WAIT_FOR_SERIAL
		sp<Master> spMaster=spSurfaceViewerOp->registry()->master();
		sp<Catalog> spMasterCatalog=spMaster->catalog();
		const String hintDrawChain=
				spMasterCatalog->catalogOrDefault<String>("hint:DrawChain","");
		if(hintDrawChain=="*.DrawHydra")
		{
			//* HACK Hydra VBO updates can flood GPU, slowing OpenCL
			milliSleep(20);
		}
#endif

		const Color white(1.0,1.0,1.0);

		//* TODO more colors
		const I32 fileColorCount=4;
		const Color fileColor[fileColorCount]=
		{
			Color(0.5,1.0,1.0),
			Color(1.0,0.5,1.0),
			Color(1.0,1.0,0.5),
			Color(0.7,0.7,0.7)
		};

		m_selectRegion=FALSE;

		//* main render

		sp<ViewI> spView=m_spDrawI->view();
		const ViewI::Projection projection=isOrtho?
				ViewI::e_ortho: ViewI::e_perspective;
		spView->use(projection);

		sp<Catalog>& rspMenu=spSurfaceViewerOp->menu();
		const Color selectColor=rspMenu->catalog<Color>("/ds");

		SpatialTransform identity;
		setIdentity(identity);

		for(I32 ghostPass=0;ghostPass<2;ghostPass++)
		{
			const I32 surfaceCount=m_nodeArray.size();
			for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
			{
				SurfaceNode& rSurfaceNode=m_nodeArray[surfaceIndex];
				sp<DrawableI>& rspDrawable=rSurfaceNode.m_spDrawable;

				if(rspDrawable.isValid() && !rSurfaceNode.m_hideAll &&
						(m_isolateSurfaceIndex<0 ||
						m_isolateSurfaceIndex==surfaceIndex))
				{
					const Color* pColor=
							(rspDrawable==m_spImpactSurface && !m_paperMode)?
							&selectColor:
							(rspMenu->catalog<String>("/sc")=="off"?
							&white:
							(rspMenu->catalog<String>("/sc")=="file"?
							&fileColor[surfaceIndex%fileColorCount]: NULL));

					sp<DrawBufferI>& rspSolidBuffer=
							rSurfaceNode.m_spSolidBuffer;

#if FE_SVO_VBO
					if(!m_reopenCountdown && rspSolidBuffer.isNull())
					{
						rspSolidBuffer=m_spDrawI->createBuffer();
						if(rspSolidBuffer.isValid())
						{
							rspSolidBuffer->setName(rspDrawable->name());
						}
						else
						{
							feLog("SurfaceViewerOp::DrawHandler::handle"
									" failed to create draw buffer\n");
						}
					}
#endif

					if(ghostPass)
					{
						if(!m_isolatePart.empty())
						{
							continue;
						}
						if(rSurfaceNode.m_ghostAll)
						{
							m_spDrawI->pushDrawMode(m_spDrawGhost);
							m_spDrawI->draw(rspDrawable,pColor,rspSolidBuffer);
							m_spDrawI->popDrawMode();
						}
						else
						{
							if(rSurfaceNode.m_spGhostPartition.isValid())
							{
								m_spDrawI->pushDrawMode(m_spDrawGhost);
								rspDrawable->draw(identity,m_spDrawI,pColor,
										sp<DrawBufferI>(NULL),
										rSurfaceNode.m_spGhostPartition);
								m_spDrawI->popDrawMode();
							}
						}
					}
					else
					{
						if(!rSurfaceNode.m_ghostAll)
						{
							m_spDrawI->draw(rspDrawable,pColor,rspSolidBuffer);
						}
					}

					if(rspMenu->catalog<bool>("/sn"))
					{
						drawNormals(surfaceIndex);
					}

					if(rspMenu->catalog<bool>("/si"))
					{
						drawInstances(surfaceIndex,pColor);
					}
				}
			}
		}

		const I32 primitiveIndex=m_spSurfaceImpact.isValid()?
				m_spSurfaceImpact->primitiveIndex(): -1;

		const Color softGreen(0.0,1.0,0.0,0.2);
		const Color hardGreen(0.0,1.0,0.0,0.6);
		const Color hardYellow(1.0,1.0,0.0,0.8);

		if(spSurfaceViewerOp.isValid() && m_spImpactAccessible.isValid())
		{
			m_partAttr=spSurfaceViewerOp->findPartitionAttribute(
					m_spImpactAccessible,m_impactNodeName);

			String partName;
			if(primitiveIndex<0)
			{
				partName=m_selectName;
			}
			else
			{
				if(!m_partAttr.empty())
				{
					sp<SurfaceAccessorI> spSurfaceAccessorI;
					spSurfaceViewerOp->access(spSurfaceAccessorI,
							m_spImpactAccessible,m_impactNodeName,
							OperatorSurfaceCommon::e_primitive,
							m_partAttr,
							OperatorSurfaceCommon::e_warning,
							OperatorSurfaceCommon::e_refuseMissing);

					if(spSurfaceAccessorI.isValid())
					{
						partName=spSurfaceAccessorI->string(primitiveIndex);
					}
				}
			}

			sp<SurfaceAccessorI> spPrimitiveVertices;
			spSurfaceViewerOp->access(spPrimitiveVertices,
					m_spImpactAccessible,m_impactNodeName,
					OperatorSurfaceCommon::e_primitive,
					OperatorSurfaceCommon::e_vertices,
					OperatorSurfaceCommon::e_quiet,
					OperatorSurfaceCommon::e_refuseMissing);

			sp<SurfaceAccessorI> spPrimitiveProperties;
			spSurfaceViewerOp->access(spPrimitiveProperties,
					m_spImpactAccessible,m_impactNodeName,
					OperatorSurfaceCommon::e_primitive,
					OperatorSurfaceCommon::e_properties,
					OperatorSurfaceCommon::e_quiet,
					OperatorSurfaceCommon::e_refuseMissing);

			const I32 primCount=spPrimitiveVertices.isValid()?
					spPrimitiveVertices->count(): 0;

			I32 curveCount=0;

			if(!partName.empty() && !m_paperMode &&
					!spSurfaceViewerOp->uvSpace())
			{
				if(partName!=m_selectName)
				{
					spSurfaceViewerOp->setSlack(1.0,1.0,0.5);
					m_keepEdges=FALSE;
				}

				FEASSERT(!m_partAttr.empty());

				SpatialVector pickSum;
				SpatialVector pickMin;
				SpatialVector pickMax;
				I32 pickCount=0;

				m_spDrawI->pushDrawMode(m_spDrawWire);

				sp<SurfaceAccessorI> spPrimitivePart;
				spSurfaceViewerOp->access(spPrimitivePart,
						m_spImpactAccessible,m_impactNodeName,
						OperatorSurfaceCommon::e_primitive,
						m_partAttr,
						OperatorSurfaceCommon::e_warning,
						OperatorSurfaceCommon::e_refuseMissing);

				BWORD highlightPart=
						(rspMenu->catalog<String>("/dh")=="always");

				if(rspMenu->catalog<String>("/dh")=="conditional")
				{
					highlightPart=TRUE;

					const I32 matchLimit=10000;		//* TODO param
					I32 matchCount=0;

					for(I32 primIndex=0;primIndex<primCount;primIndex++)
					{
						const String onePart=spPrimitivePart->string(primIndex);
						if(onePart!=partName)
						{
							continue;
						}
						matchCount+=
								spPrimitiveVertices->subCount(primIndex);
						if(matchCount>matchLimit)
						{
							highlightPart=FALSE;
							break;
						}
					}

//					feLog("primCount %d matchCount %d\n",primCount,matchCount);
				}

				const I32 primStart=highlightPart? 0: primitiveIndex;
				const I32 primEnd=highlightPart? primCount: primitiveIndex+1;

				for(I32 primIndex=primStart;primIndex<primEnd;primIndex++)
				{
					const String onePart=spPrimitivePart->string(primIndex);
					if(onePart==partName)
					{
//						const BWORD wrappedU=
//								(spPrimitiveProperties.isValid() &&
//								spPrimitiveProperties->integer(
//								primIndex,e_wrappedU));
						const BWORD wrappedV=
								(spPrimitiveProperties.isValid() &&
								spPrimitiveProperties->integer(
								primIndex,e_wrappedV));

						const I32 countU=
								spPrimitiveProperties.isValid()?
								spPrimitiveProperties->integer(
								primIndex,e_countU): 0;
						const I32 countV=
								spPrimitiveProperties.isValid()?
								spPrimitiveProperties->integer(
								primIndex,e_countV): 0;

						const BWORD openCurve=
								(spPrimitiveProperties.isValid() &&
								spPrimitiveProperties->integer(
								primIndex,e_openCurve));
						curveCount+=openCurve;

						const I32 subCount=
								spPrimitiveVertices->subCount(primIndex);

						const I32 bandCount=(countV>1)? countU: 1;
						const I32 sectionCount=(bandCount>1)? countV: subCount;

						//* NOTE presumes only patches will report as wrapped
						const BWORD closed=
								(!openCurve && (wrappedV || countU<2));

						for(I32 bandIndex=0;bandIndex<bandCount;bandIndex++)
						{
							const I32 offset=bandIndex*sectionCount;

							const I32 maxLineCount=256;		//* tweak

							I32 lineCount=
									fe::minimum(maxLineCount,sectionCount-1);
							const BWORD drawClosed=
									(closed && lineCount<maxLineCount);
							if(drawClosed)
							{
								lineCount++;
							}

							SpatialVector* line=new SpatialVector[lineCount+1];

							I32 pickIndex=0;

							sp<SurfaceBase::Impact> spSurfaceBaseImpact=
									m_spSurfaceImpact;
							if(spSurfaceBaseImpact.isValid())
							{
								pickIndex=subCount*
										spSurfaceBaseImpact->barycenter()[0];
							}

							I32 startIndex=fe::maximum(0,
									pickIndex-(maxLineCount>>1));
							I32 endIndex=fe::minimum(sectionCount-1,
									startIndex+maxLineCount);
							startIndex=fe::maximum(0,
									endIndex-maxLineCount);

							I32 vertIndex=0;
							for(I32 sectionIndex=startIndex;
									sectionIndex<=endIndex;
									sectionIndex++)
							{
								const SpatialVector point=
										spPrimitiveVertices->spatialVector(
										primIndex,offset+sectionIndex);

								line[vertIndex++]=point;

								if(!pickCount)
								{
									pickSum=point;
									pickMin=point;
									pickMax=point;
								}
								else
								{
									pickSum+=point;
									for(I32 m=0;m<3;m++)
									{
										if(pickMin[m]>point[m])
										{
											pickMin[m]=point[m];
										}
										else if(pickMax[m]<point[m])
										{
											pickMax[m]=point[m];
										}
									}
								}
								pickCount++;
							}

							FEASSERT(fe::minimum(vertIndex+drawClosed-1,
									maxLineCount)==lineCount);

							if(drawClosed)
							{
								line[lineCount]=line[0];
							}

							//* TODO elements
#if 1==0
							drawLines(const SpatialVector *vertex,
									const SpatialVector *normal,U32 vertices,
									StripMode strip,BWORD multicolor,
									const Color *color,
									const Vector3i *element,U32 elementCount,
									sp<DrawBufferI> spDrawBuffer)
#endif

							m_spDrawI->drawLines(line,NULL,lineCount+1,
									DrawI::e_strip,false,
									primIndex==primitiveIndex?
									&hardGreen: &softGreen);

							if(primIndex==primitiveIndex)
							{
								m_spDrawI->drawPoints(line,NULL,lineCount+1,
										FALSE,&hardYellow);
							}

							delete[] line;
						}
					}
				}

				m_spDrawI->popDrawMode();

				if(pickCount)
				{
					m_selectRegion=TRUE;
					m_selectCenter=pickSum/Real(pickCount);
					m_selectRadius=magnitude(pickMin-m_selectCenter);
					const Real otherRadius=
							magnitude(pickMax-m_selectCenter);
					if(m_selectRadius<otherRadius)
					{
						m_selectRadius=otherRadius;
					}
				}
			}
			else
			{
				for(I32 primIndex=0;primIndex<primCount;primIndex++)
				{
					const BWORD openCurve=
							(spPrimitiveProperties.isValid() &&
							spPrimitiveProperties->integer(
							primIndex,e_openCurve));
					curveCount+=openCurve;
				}
			}

			if(!spSurfaceViewerOp->edging() || spSurfaceViewerOp->uvSpace() ||
					(m_selectName.empty() && primitiveIndex<0 && !m_paperMode))
			{
				m_keepEdges=FALSE;
			}
			else if(!curveCount)
			{
				if(!m_keepEdges)
				{
					createEdges(m_spImpactAccessible,
							m_paperMode? "": m_partAttr,m_edgeMap);
					m_keepEdges=TRUE;
				}

				sp<CameraI> spCameraI=spView->camera();
				SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
				SpatialTransform cameraTransform;
				invert(cameraTransform,cameraMatrix);

//				const SpatialVector cameraDir= -cameraTransform.column(2);
				const SpatialVector cameraPos=cameraTransform.column(3);

				const Color alphaBlack(0.0,0.0,0.0,0.3);
				const Color hardBlack(0.0,0.0,0.0,1.0);

				m_spDrawI->pushDrawMode(
							m_paperMode? m_spDrawSketch: m_spDrawEdging);

				drawOutline(m_spDrawI,m_edgeMap,
							m_paperMode? "": partName,cameraPos,
							m_paperMode? hardBlack: alphaBlack);

				m_spDrawI->popDrawMode();
			}

			m_selectName=partName;

			if(m_spRulerOp.isValid())
			{
				if(spSurfaceViewerOp->grid())
				{
					m_spRulerOp->drawOrthoGrid(m_spDrawI,sp<DrawI>(NULL),
							m_spImpactAccessible);
				}
				else
				{
					m_spRulerOp->catalog<I32>("clear")=1;
				}
			}
		}
		else
		{
			m_selectName="";
			m_keepEdges=FALSE;
		}

		if(!m_keepEdges)
		{
			m_edgeMap.clear();
		}

		sp<DrawableI> spDrawableImpacted=m_spImpactSurface;
		if(spDrawableImpacted.isValid() && !m_paperMode)
		{
			sp<SurfaceTriangles> spSurfaceTriangles=spDrawableImpacted;

			//* NOTE trianglated surfaces only (lots of hair can be slow)
			if(spSurfaceTriangles.isValid())
			{
				SurfaceNode& rSurfaceNode=m_nodeArray[m_impactSurfaceIndex];

				sp<DrawBufferI>& rspWireBuffer=
						rSurfaceNode.m_spWireBuffer;
#if FE_SVO_VBO
				if(!m_reopenCountdown && rspWireBuffer.isNull())
				{
					rspWireBuffer=m_spDrawI->createBuffer();
					rspWireBuffer->setName(spSurfaceTriangles->name()+".wire");
				}
#endif

				const Color blue(0.0,0.5,1.0,0.1);

				m_spDrawI->pushDrawMode(m_spDrawWire);
				m_spDrawI->draw(spDrawableImpacted,&blue,rspWireBuffer);
				m_spDrawI->popDrawMode();

				if(rSurfaceNode.m_spGhostPartition.isValid() &&
						(m_isolateSurfaceIndex<0 ||
						(m_isolateSurfaceIndex==m_impactSurfaceIndex &&
						m_isolatePart.empty())))
				{
					m_spDrawI->pushDrawMode(m_spDrawPoints);
					spDrawableImpacted->draw(identity,m_spDrawI,&blue,
							sp<DrawBufferI>(NULL),
							rSurfaceNode.m_spGhostPartition);
					m_spDrawI->popDrawMode();
				}
			}
		}

		sp<DrawableI> spDrawableImpact=m_spSurfaceImpact;
		if(spDrawableImpact.isValid())
		{
			const Color alphaYellow(1.0,1.0,0.0,0.3);

			m_spDrawI->pushDrawMode(m_spDrawImpact);
			m_spDrawI->draw(spDrawableImpact,&alphaYellow);
			m_spDrawI->popDrawMode();
		}

		return;
	}

	if(rLayer==2)
	{
		if(!spSurfaceViewerOp->menuString().empty())
		{
			runMenu();
			return;
		}

		if(m_paperMode)
		{
			return;
		}

		const Color lightRed(1.0,0.5,0.5);
		const Color lightBlue(0.5,0.5,1.0);
		const Color yellow(1.0,1.0,0.0);
		const Color brightGreen(0.0,1.0,0.0);
		const Color dimGreen(0.0,1.0,0.0,0.4);
		const Color grey(0.6,0.6,0.6);
		const Color blue(0.0,0.0,1.0);
		const Color backColor=background();

		sp<ViewI> spView=m_spDrawI->view();
		sp<CameraI> spCameraI=spView->camera();
		const I32 winx=width(spView->viewport());
		const I32 winy=height(spView->viewport());

		sp<Catalog>& rspMenu=spSurfaceViewerOp->menu();
		if(!isOrtho)
		{
			if(!isOrtho && rspMenu->catalog<bool>("/dd"))
			{
				m_spDrawI->pushDrawMode(m_spDrawText);

				const I32 surfaceCount=m_nodeArray.size();
				for(I32 surfaceIndex=surfaceCount-1;
						surfaceIndex>=0;surfaceIndex--)
				{
					drawDesignator(surfaceIndex);
				}

				m_spDrawI->popDrawMode();
			}
			if(rspMenu->catalog<bool>("/di"))
			{
				m_spDrawI->pushDrawMode(m_spDrawText);

				drawHorizon();

				m_spDrawI->popDrawMode();
			}
		}

		if(m_spImpactAccessible.isValid() &&
				m_spRulerOp.isValid()&& spSurfaceViewerOp->grid())
		{
			m_spRulerOp->drawOrthoGrid(sp<DrawI>(NULL),m_spDrawI,
					m_spImpactAccessible);
		}

		if(m_spPickingAccessible.isValid())
		{
			m_spDrawI->pushDrawMode(m_spDrawPicking);

			const I32 pickingPrimitive=m_spPickingImpact.isValid()?
					m_spPickingImpact->primitiveIndex(): -1;

			//* TODO also ortho
			if(!isOrtho)
			{
				const Real dotRadius=3.0;
				drawDot(spView,m_spDrawI,m_pickPoint,dotRadius,blue);
			}

			if(spSurfaceViewerOp.isValid())
			{
				sp<SurfaceI::GaugeI> spGauge;
				if(m_spPickingGeodesic.isValid())
				{
					spGauge=m_spPickingGeodesic->gauge();
					if(spGauge.isValid())
					{
						spGauge->pick(m_pickPoint,m_pickRadius);
					}
				}

				BWORD vertexUV=FALSE;

				sp<SurfaceAccessorI> spPickingPoint;
				if(spSurfaceViewerOp->uvSpace())
				{
					spSurfaceViewerOp->access(spPickingPoint,
							m_spPickingAccessible,m_pickingNodeName,
							OperatorSurfaceCommon::e_vertex,
							OperatorSurfaceCommon::e_uv,
							OperatorSurfaceCommon::e_quiet,
							OperatorSurfaceCommon::e_refuseMissing);
					if(spPickingPoint.isValid())
					{
						vertexUV=TRUE;
					}
					else
					{
						spSurfaceViewerOp->access(spPickingPoint,
								m_spPickingAccessible,m_pickingNodeName,
								OperatorSurfaceCommon::e_point,
								OperatorSurfaceCommon::e_uv,
								OperatorSurfaceCommon::e_quiet,
								OperatorSurfaceCommon::e_refuseMissing);
					}
				}
				else
				{
					spSurfaceViewerOp->access(spPickingPoint,
							m_spPickingAccessible,m_pickingNodeName,
							OperatorSurfaceCommon::e_point,
							OperatorSurfaceCommon::e_position,
							OperatorSurfaceCommon::e_warning,
							OperatorSurfaceCommon::e_refuseMissing);
				}

				if(spPickingPoint.isValid())
				{
					const I32 pointCount=vertexUV? 0: spPickingPoint->count();

					Array<Color> gaugeColor(pointCount);

					for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
					{
						const SpatialVector point=
								spPickingPoint->spatialVector(pointIndex);

						const Real alpha=gaugeAlpha(spGauge,point);

						set(gaugeColor[pointIndex],0.0,0.0,0.0,alpha);
					}

					sp<SurfaceAccessorI> spPickingVertices;
					spSurfaceViewerOp->access(spPickingVertices,
							m_spPickingAccessible,m_pickingNodeName,
							OperatorSurfaceCommon::e_primitive,
							OperatorSurfaceCommon::e_vertices,
							OperatorSurfaceCommon::e_quiet,
							OperatorSurfaceCommon::e_refuseMissing);

					sp<SurfaceAccessorI> spPickingProperties;
					spSurfaceViewerOp->access(spPickingProperties,
							m_spPickingAccessible,m_pickingNodeName,
							OperatorSurfaceCommon::e_primitive,
							OperatorSurfaceCommon::e_properties,
							OperatorSurfaceCommon::e_quiet,
							OperatorSurfaceCommon::e_refuseMissing);

					const ViewI::Projection projection=isOrtho?
							ViewI::e_ortho: ViewI::e_perspective;

					//* TODO use model ortho not raster match overlay ortho

					const I32 primCount=spPickingVertices.isValid()?
							spPickingVertices->count(): 0;
					for(I32 primitiveIndex=0;primitiveIndex<primCount;
							primitiveIndex++)
					{
						const I32 subCount=
								spPickingVertices->subCount(primitiveIndex);
						if(subCount<2)
						{
							continue;
						}

						const BWORD isPickedPrimitive=
								(primitiveIndex==pickingPrimitive);
						if(m_spPickingGeodesic.isNull() && !isPickedPrimitive)
						{
							continue;
						}

						const BWORD openCurve=
								(spPickingProperties.isValid() &&
								spPickingProperties->integer(
								primitiveIndex,e_openCurve));

						Color* vertexColor=new Color[subCount];

						//* TODO optimize

						BWORD drawPrimitive=FALSE;
						I32* indexArray=new I32[subCount];
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							I32& rPointIndex=indexArray[subIndex];
							rPointIndex=
									spPickingVertices->integer(
									primitiveIndex,subIndex);
							if(vertexUV)
							{
								const SpatialVector point=
										spPickingPoint->spatialVector(
										primitiveIndex,subIndex);

								const Real alpha=gaugeAlpha(spGauge,point);

								set(vertexColor[subIndex],0.0,0.0,0.0,alpha);
								if(alpha>0.0)
								{
									drawPrimitive=TRUE;
								}
							}
							else
							{
								vertexColor[subIndex]=gaugeColor[rPointIndex];
							}
							if(vertexColor[subIndex][3]>0.0)
							{
								drawPrimitive=TRUE;
							}
						}

						if(!drawPrimitive)
						{
							continue;
						}

						//* HACK switch projection
						if(isOrtho)
						{
							sp<CameraI> spOrthoCam=
									spSurfaceViewerOp->orthoCam();
							Real zoom;
							Vector2 center;
							spOrthoCam->getOrtho(zoom,center);

							sp<CameraI> spCamera=spView->camera();
							spCamera->setOrtho(zoom,center);

							spView->use(projection);
						}

						const I32 drawCount=subCount+(!openCurve);

						SpatialVector* line=new SpatialVector[drawCount];
						Color* color=new Color[drawCount];

						if(openCurve)
						{
							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								SpatialVector point;
								if(vertexUV)
								{
									point=spPickingPoint->spatialVector(
											primitiveIndex,subIndex);
								}
								else
								{
									const I32 pointIndex=indexArray[subIndex];
									point=spPickingPoint->spatialVector(
											pointIndex);
								}

								line[subIndex]=spView->project(
										point,projection);
								color[subIndex]=vertexColor[subIndex];
							}
						}
						else
						{
							SpatialVector sum(0.0,0.0,0.0);
							SpatialVector* point=new SpatialVector[subCount];

							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								SpatialVector& rPoint=point[subIndex];
								if(vertexUV)
								{
									rPoint=spPickingPoint->spatialVector(
											primitiveIndex,subIndex);
								}
								else
								{
									const I32 pointIndex=indexArray[subIndex];
									rPoint=spPickingPoint->spatialVector(
											pointIndex);
								}

								sum+=rPoint;
								line[subIndex]=spView->project(
										rPoint,projection);

								color[subIndex]=vertexColor[subIndex];
								if(!isPickedPrimitive)
								{
									color[subIndex][3]*=0.5;
								}
								else if(m_spPickingGeodesic.isValid())
								{
									color[subIndex][3]=1.0;
								}
							}

							const Real offset=0.4;
							const Real offset1=1.0-offset;
							const SpatialVector center=
									sum*(Real(1)/Real(subCount));
							const SpatialVector centerFraction=center*offset;

							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								line[subIndex]=spView->project(
										offset1*point[subIndex]+centerFraction,
										projection);
							}

							line[subCount]=line[0];
							color[subCount]=color[0];

							delete[] point;
						}

						delete[] indexArray;

						//* HACK restore projection
						if(isOrtho)
						{
							Real zoom=1.0;
							Vector2 center(0.0,0.0);

							sp<CameraI> spCamera=spView->camera();
							spCamera->setOrtho(zoom,center);

							spView->use(projection);
						}

						m_spDrawI->drawLines(line,NULL,drawCount,
								DrawI::e_strip,true,color);

						delete[] color;
						delete[] line;
						delete[] vertexColor;
					}
				}
			}

			m_spDrawI->popDrawMode();
		}

		m_spDrawI->pushDrawMode(m_spDrawText);

		SpatialVector location(m_characterWidth,19*m_textHeight,0.0);

		const BWORD help=(spSurfaceViewerOp.isValid() &&
				spSurfaceViewerOp->help());

		if(help)
		{
			drawLabel(m_spDrawI,location,"Alt camera",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"f   reframe",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"c   center",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"o   origin",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"Esc deselect",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"Sh  scan",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"Spc pause",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"->  forward",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"<-  back",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"C-h hide",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"A-h isolate",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"C-g ghost",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"Tab ortho",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,"/   menu",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;
		}

		set(location,winx-23*m_characterWidth,(help? 8: 1)*m_textHeight,0);

		const I32 outlineLimit=location[1]+m_textHeight;

		String text;

		if(help)
		{
			if(spSurfaceViewerOp.isValid())
			{
				text.sPrintf("      s Spiral     %s",
						spSurfaceViewerOp->spiral()? "ON": "OFF");
				drawLabel(m_spDrawI,location,text,
						FALSE,1,lightBlue,NULL,&backColor);
				location[1]-=m_textHeight;

				text.sPrintf("      t Tracking   %s",
						spSurfaceViewerOp->tracking()? "ON": "OFF");
				drawLabel(m_spDrawI,location,text,
						FALSE,1,lightBlue,NULL,&backColor);
				location[1]-=m_textHeight;

				text.sPrintf("      u UV Space   %s",
						spSurfaceViewerOp->uvSpace()? "ON": "OFF");
				drawLabel(m_spDrawI,location,text,
						FALSE,1,lightBlue,NULL,&backColor);
				location[1]-=m_textHeight;

				if(spSurfaceViewerOp->cameraView())
				{
					text.sPrintf("      v Cam View   %d",
							spSurfaceViewerOp->cameraIndex());
				}
				else
				{
					text.sPrintf("      v Cam View   OFF");
				}
				drawLabel(m_spDrawI,location,text,
						FALSE,1,lightBlue,NULL,&backColor);
				location[1]-=m_textHeight;

				const String ortho=spSurfaceViewerOp->orthoAxis();
				text.sPrintf("(S) xyz Ortho      %s",
						ortho.empty()? "OFF": ortho.c_str());
				drawLabel(m_spDrawI,location,text,
						FALSE,1,lightBlue,NULL,&backColor);
				location[1]-=m_textHeight;
			}

			text.sPrintf("    [ ] Brightness %.6G",m_spDrawI->brightness());
			drawLabel(m_spDrawI,location,text,
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			text.sPrintf("    { } Contrast   %.6G",m_spDrawI->contrast());
			drawLabel(m_spDrawI,location,text,
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;
		}

		text.sPrintf("      h Help       %s",help? "ON": "OFF");
		drawLabel(m_spDrawI,location,text,
				FALSE,1,lightBlue,NULL,&backColor);
		location[1]-=m_textHeight;

		if(help)
		{
			set(location,m_characterWidth,4*m_textHeight,0.0);

#if FE_OS==FE_LINUX
			struct mallinfo mi=mallinfo();
			const U32 megabytes=static_cast<U32>(mi.arena)>>20;

/*
			feLog("Total non-mmapped bytes (arena):       %d\n", mi.arena);
			feLog("# of free chunks (ordblks):            %d\n", mi.ordblks);
			feLog("# of free fastbin blocks (smblks):     %d\n", mi.smblks);
			feLog("# of mapped regions (hblks):           %d\n", mi.hblks);
			feLog("Bytes in mapped regions (hblkhd):      %d\n", mi.hblkhd);
			feLog("Max. total allocated space (usmblks):  %d\n", mi.usmblks);
			feLog("Free bytes held in fastbins (fsmblks): %d\n", mi.fsmblks);
			feLog("Total allocated space (uordblks):      %d\n", mi.uordblks);
			feLog("Total free space (fordblks):           %d\n", mi.fordblks);
			feLog("Topmost releasable block (keepcost):   %d\n", mi.keepcost);
*/

			text.sPrintf("%d Mb",megabytes);
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;
#endif

			if(m_us>0)
			{
				text.sPrintf("%.1f ms",0.001*m_us);
				drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			}
			location[1]-=m_textHeight;

			text.sPrintf("Frame %.6G",m_frame);
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
		}

		const I32 marginLeft=10*m_characterWidth;
		const I32 marginRight=24*m_characterWidth;
		const I32 timelineY=2*m_textHeight;
		if(m_maxFrame>m_minFrame && winx>marginLeft+marginRight+16)
		{
			const I32 glowFrame=spSurfaceViewerOp->glowFrame();
			const BWORD glowCurrent=(glowFrame==I32(m_frame+0.5));

			SpatialVector line[3];

			const I32 timelineWidth=winx-marginLeft-marginRight;

			set(spSurfaceViewerOp->m_scrubBox,
					marginLeft,0,timelineWidth,2*timelineY);

			Real width=4*m_multiplication;
			Real height=8*m_multiplication;

			//* NOTE precision rasterization
			set(line[0],I32(marginLeft+timelineWidth*(m_frame-m_minFrame)/
					(m_maxFrame-m_minFrame)+1.375)-0.375,timelineY-0.375);
			set(line[1],line[0][0]-width,line[0][1]+height,0);

			m_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,
					glowCurrent? &yellow: &brightGreen);

			//* NOTE sharp point
			line[0][0]-=1;
			set(line[1],line[0][0]+width,line[0][1]+height,0);
			m_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,
					glowCurrent? &yellow: &brightGreen);

			set(location,line[0][0],timelineY+1.5*m_textHeight);
			text.sPrintf("%.6G",m_frame);
			drawLabel(m_spDrawI,location,text,
					TRUE,1,brightGreen,NULL,&backColor);

			set(line[0],marginLeft,timelineY,0);
			set(line[1],winx-marginRight,timelineY,0);

			m_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,&dimGreen);

			set(location,line[0][0],timelineY-m_textHeight,0);
			text.sPrintf("%.6G",m_minFrame);
			drawLabel(m_spDrawI,location,text,
					TRUE,1,brightGreen,NULL,&backColor);

			set(location,line[1][0],timelineY-m_textHeight,0);
			text.sPrintf("%.6G",m_maxFrame);
			drawLabel(m_spDrawI,location,text,
					TRUE,1,brightGreen,NULL,&backColor);

			const I32 startX=line[0][0];
			const I32 endX=line[1][0];
			const BWORD subLabels=(timelineWidth>(m_maxFrame-m_minFrame)*3);

			const I32 startTick=I32(m_minFrame);
			const I32 endTick=I32(m_maxFrame)+1;
			for(I32 tick=startTick;tick<=endTick;tick++)
			{
				const Real realTick(tick);
				if(realTick>=m_minFrame && realTick<=m_maxFrame)
				{
					const BWORD leftWall=
							(fabs(realTick-m_startFrame)<0.5 &&
							fabs(m_minFrame-m_startFrame)>0.5);
					const BWORD rightWall=
							(fabs(realTick-m_endFrame)<0.5 &&
							fabs(m_maxFrame-m_endFrame)>0.5);
					const BWORD hardTick=(leftWall || rightWall);

					const BWORD glowTick=(tick==glowFrame && !glowCurrent);

					height=4;
					if(!(tick%10) || hardTick || glowTick)
					{
						height=12;
					}
					else if(!(tick%5))
					{
						height=8;
					}

					height*=m_multiplication;

					set(line[0],marginLeft+timelineWidth*(realTick-m_minFrame)/
							(m_maxFrame-m_minFrame),timelineY);
					set(line[1],line[0][0],timelineY+height,0);

					if(subLabels && !(tick%10) &&
							(line[1][0]-startX)>25 && (endX-line[1][0])>25)
					{
						set(location,line[1][0],timelineY-m_textHeight,0);
						text.sPrintf("%d",tick);
						drawLabel(m_spDrawI,location,text,
								TRUE,1,brightGreen,NULL,&backColor);
					}

					m_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,
							glowTick? &yellow:
							(hardTick? &brightGreen: &dimGreen));

					if(hardTick)
					{
						line[0][0]+=m_multiplication*(rightWall? 3: -3);
						line[1][0]=line[0][0];

						m_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,
								&brightGreen);

						set(location,line[0][0]+
								m_characterWidth*(rightWall? 1.2: -1.2),
								timelineY+0.5*m_textHeight,0);
						text.sPrintf("%d",tick);
						drawLabel(m_spDrawI,location,text,
								TRUE,1,brightGreen,NULL,&backColor);
					}
				}
			}
		}

		const I32 surfaceCount=m_nodeArray.size();

		I32 outlineY=winy-2*m_textHeight;

		if(m_isolateSurfaceIndex>=0)
		{
			String isolatedName=m_isolatePart;
			if(isolatedName.empty())
			{
				sp<SurfaceI> spSurface=surface(m_isolateSurfaceIndex);
				if(spSurface.isValid())
				{
					isolatedName=spSurface->name();
				}
			}

			const I32 charCount=
					fe::maximum(U32(7),U32(isolatedName.length()))+1;
			set(location,winx-charCount*m_characterWidth,
					winy-2*m_textHeight,0.0);

			drawLabel(m_spDrawI,location,"ISOLATE",
					FALSE,1,lightBlue,NULL,&backColor);
			location[1]-=m_textHeight;

			drawLabel(m_spDrawI,location,isolatedName,
					FALSE,1,lightRed,NULL,&backColor);
			location[1]-=m_textHeight;

			outlineY=location[1]-m_textHeight;
		}
		else
		{
			set(location,0.0,winy-2*m_textHeight,0.0);

			I32 charCount=5;

			BWORD anyAll[2]={FALSE,FALSE};
			BWORD anyList[2]={FALSE,FALSE};
			for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;
					surfaceIndex++)
			{
				SurfaceNode& rSurfaceNode=m_nodeArray[surfaceIndex];

				if(rSurfaceNode.m_hideAll)
				{
					anyAll[0]=TRUE;
				}
				if(rSurfaceNode.m_ghostAll)
				{
					anyAll[1]=TRUE;
				}
				if(rSurfaceNode.m_hideList.size())
				{
					anyList[0]=TRUE;
				}
				if(rSurfaceNode.m_ghostList.size())
				{
					anyList[1]=TRUE;
				}
			}

			//* hidden, then ghost
			for(I32 pass=0;pass<2;pass++)
			{
				if(anyAll[pass] || anyList[pass])
				{
					for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;
							surfaceIndex++)
					{
						SurfaceNode& rSurfaceNode=m_nodeArray[surfaceIndex];
						if(pass? rSurfaceNode.m_ghostAll:
								rSurfaceNode.m_hideAll)
						{
							sp<SurfaceI> spSurface=surface(surfaceIndex);
							if(spSurface.isNull())
							{
								continue;
							}

							const I32 thisCount=spSurface->name().length();
							if(charCount<thisCount)
							{
								charCount=thisCount;
							}
							continue;
						}

						const Array<String>& rList=pass?
								rSurfaceNode.m_ghostList:
								rSurfaceNode.m_hideList;
						const I32 listCount=rList.size();
						for(I32 listIndex=0;listIndex<listCount;listIndex++)
						{
							const I32 thisCount=rList[listIndex].length();
							if(charCount<thisCount)
							{
								charCount=thisCount;
							}
						}
					}
				}
			}

			charCount++;

			location[0]=winx-charCount*m_characterWidth;

			//* hidden, then ghost
			for(I32 pass=0;pass<2;pass++)
			{
				if(anyAll[pass] || anyList[pass])
				{
					drawLabel(m_spDrawI,location,pass? "GHOST": "HIDE",
							FALSE,1,lightBlue,NULL,&backColor);
					location[1]-=m_textHeight;

					U32 listTotal=0;
					for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;
							surfaceIndex++)
					{
						SurfaceNode& rSurfaceNode=m_nodeArray[surfaceIndex];
						if(pass? rSurfaceNode.m_ghostAll:
								rSurfaceNode.m_hideAll)
						{
							if(listTotal)
							{
								location[1]-=m_textHeight;
							}

							sp<SurfaceI> spSurface=surface(surfaceIndex);
							if(spSurface.isNull())
							{
								continue;
							}

							drawLabel(m_spDrawI,location,spSurface->name(),
									FALSE,1,lightRed,NULL,&backColor);
							location[1]-=m_textHeight;
							listTotal++;
							continue;
						}

						const Array<String>& rList=pass?
								rSurfaceNode.m_ghostList:
								rSurfaceNode.m_hideList;
						const I32 listCount=rList.size();
						if(listCount && listTotal)
						{
							location[1]-=m_textHeight;
						}
						for(I32 listIndex=0;listIndex<listCount;listIndex++)
						{
							drawLabel(m_spDrawI,location,rList[listIndex],
									FALSE,1,lightRed,NULL,&backColor);
							location[1]-=m_textHeight;
							listTotal++;
						}
					}

					location[1]-=m_textHeight;
				}
			}

			outlineY=location[1];
		}

		set(location,m_characterWidth,winy-2*m_textHeight,0.0);

		if(m_spImpactSurface.isValid() && m_spSurfaceImpact.isValid())
		{
			drawLabel(m_spDrawI,location,m_spImpactSurface->name(),
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			text.sPrintf("pick   %s",
					c_print(m_spSurfaceImpact->intersection()));
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			text.sPrintf("normal %s",c_print(m_spSurfaceImpact->normal()));
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			const Barycenter<Real> bary=m_spSurfaceImpact->barycenter();
			text.sPrintf("bary   %s face %d",c_print(bary),
					m_spSurfaceImpact->face());
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			text.sPrintf("uv     %s",c_print(m_spSurfaceImpact->uv()));
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			text.sPrintf("du     %s",c_print(m_spSurfaceImpact->du()));
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			text.sPrintf("dv     %s",c_print(m_spSurfaceImpact->dv()));
			drawLabel(m_spDrawI,location,text,
					FALSE,1,yellow,NULL,&backColor);
			location[1]-=m_textHeight;

			I32 pointIndex0= -1;
			I32 pointIndex1= -1;
			I32 pointIndex2= -1;
			I32 nearestPointIndex= -1;
			I32 nearestSubIndex= -1;

			sp<SurfaceSearchable::Impact> spSurfaceSearchableImpact=
					m_spSurfaceImpact;
			if(spSurfaceSearchableImpact.isValid())
			{
				pointIndex0=spSurfaceSearchableImpact->pointIndex0();
				pointIndex1=spSurfaceSearchableImpact->pointIndex1();

				nearestSubIndex=spSurfaceSearchableImpact->nearestSubIndex();

				sp<SurfaceTriangles::Impact> spSurfaceTrianglesImpact=
						m_spSurfaceImpact;
				if(spSurfaceTrianglesImpact.isValid())
				{
					pointIndex2=spSurfaceTrianglesImpact->pointIndex2();

					text.sPrintf("points %d %d %d",
							pointIndex0,pointIndex1,pointIndex2);
					drawLabel(m_spDrawI,location,text,
							FALSE,1,yellow,NULL,&backColor);
					location[1]-=m_textHeight;

					const Real bary2=Real(1)-bary[0]-bary[1];
					nearestPointIndex=bary[0]>bary[1]?
						(bary[0]>bary2? pointIndex0: pointIndex2):
						(bary[1]>bary2? pointIndex1: pointIndex2);
				}
				else
				{
					text.sPrintf("points %d %d",
							pointIndex0,pointIndex1);
					drawLabel(m_spDrawI,location,text,
							FALSE,1,yellow,NULL,&backColor);
					location[1]-=m_textHeight;

					nearestPointIndex=pointIndex0;

					if(m_spImpactAccessible.isValid())
					{
						sp<SurfaceAccessorI> spSurfaceAccessorI;
						spSurfaceViewerOp->access(spSurfaceAccessorI,
								m_spImpactAccessible,m_impactNodeName,
								OperatorSurfaceCommon::e_primitive,
								OperatorSurfaceCommon::e_vertices,
								OperatorSurfaceCommon::e_warning,
								OperatorSurfaceCommon::e_refuseMissing);
						if(spSurfaceAccessorI.isValid())
						{
							const I32 subCount=spSurfaceAccessorI->subCount(
									m_spSurfaceImpact->primitiveIndex());

							const Real realSubIndex=bary[0]*(subCount-1);
							const I32 intSubIndex=realSubIndex;

							const Real fraction=realSubIndex-intSubIndex;
							if(fraction>=0.5)
							{
								nearestPointIndex=pointIndex1;
							}
						}
					}
				}

#if TRUE
				const I32 partitionIndex=
						m_spSurfaceImpact->partitionIndex();

				sp<SurfaceSearchable> spSurfaceSearchable=m_spImpactSurface;
				const String partitionName=spSurfaceSearchable.isValid()?
						m_spImpactSurface->partitionName(partitionIndex): "";

				text.sPrintf("part   %d %s",
						partitionIndex,partitionName.c_str());
				drawLabel(m_spDrawI,location,text,
						FALSE,1,yellow,NULL,&backColor);
				location[1]-=m_textHeight;
#endif
			}
			else
			{
				sp<SurfaceBase::Impact> spSurfaceBaseImpact=m_spSurfaceImpact;
				if(spSurfaceBaseImpact.isValid())
				{
					pointIndex0=spSurfaceBaseImpact->pointIndex0();

					text.sPrintf("point  %d",pointIndex0);
					drawLabel(m_spDrawI,location,text,
							FALSE,1,yellow,NULL,&backColor);
					location[1]-=m_textHeight;

					nearestPointIndex=pointIndex0;
				}
			}

			const I32 primitiveIndex=m_spSurfaceImpact->primitiveIndex();
			const I32 triangleIndex=m_spSurfaceImpact->triangleIndex();

			const I32 primitiveCount=m_spImpactAccessible.isValid()?
					m_spImpactAccessible->count(m_impactNodeName,
					SurfaceAccessibleI::e_primitive): 0;

			sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;

			if(spSurfaceViewerOp.isValid() && m_spImpactAccessible.isValid())
			{
				Array<SurfaceAccessibleI::Spec> specs;

				location[1]-=m_textHeight;

				for(I32 pass=0;pass<4;pass++)
				{
					SurfaceAccessibleI::Element element;
					I32 elementIndex=0;
					I32 subIndex=0;

					if(pass==0)
					{
						if(nearestPointIndex<0)
						{
							continue;
						}

						elementIndex = nearestPointIndex;
						element = SurfaceAccessibleI::e_point;
					}
					else if(pass==1)
					{
						if(nearestSubIndex<0)
						{
							continue;
						}

						elementIndex = primitiveIndex;
						subIndex = nearestSubIndex;
						element = SurfaceAccessibleI::e_vertex;
					}
					else if(pass==2)
					{
						elementIndex = primitiveIndex;
						element = SurfaceAccessibleI::e_primitive;
					}
					else if(pass==3)
					{
						element = SurfaceAccessibleI::e_detail;
					}

					m_spImpactAccessible->attributeSpecs(specs,
							m_impactNodeName,element);

					const I32 specCount=specs.size();
					if(!specCount && pass==3)
					{
						continue;
					}

					if(pass==0)
					{
						const I32 pointCount=m_spImpactAccessible.isValid()?
								m_spImpactAccessible->count(m_impactNodeName,
								SurfaceAccessibleI::e_point): 0;

						text.sPrintf("point     %-14d",nearestPointIndex);
						if(pointCount>0)
						{
							text.sPrintf("%s of %d",text.c_str(),pointCount);
						}

						drawLabel(m_spDrawI,location,text,
								FALSE,1,yellow,NULL,&backColor);
						location[1]-=m_textHeight;
					}
					else if(pass==1)
					{
						text.sPrintf("vertex    %-14d",subIndex);
						drawLabel(m_spDrawI,location,text,
								FALSE,1,yellow,NULL,&backColor);
						location[1]-=m_textHeight;
					}
					else if(pass==2)
					{
						text.sPrintf("primitive %-14d",primitiveIndex);
						if(primitiveCount>0)
						{
							text.sPrintf("%s of %d",
									text.c_str(),primitiveCount);
						}
						if(primitiveIndex!=triangleIndex)
						{
							text.sPrintf("%s (triangle %d)",
									text.c_str(),triangleIndex);
						}
						drawLabel(m_spDrawI,location,text,
								FALSE,1,yellow,NULL,&backColor);
						location[1]-=m_textHeight;

						if(primitiveIndex<0)
						{
							location[1]-=m_textHeight;
							continue;
						}

						sp<SurfaceAccessorI> spSurfaceAccessorI;
						spSurfaceViewerOp->access(spSurfaceAccessorI,
								m_spImpactAccessible,m_impactNodeName,
								OperatorSurfaceCommon::e_primitive,
								OperatorSurfaceCommon::e_vertices,
								OperatorSurfaceCommon::e_warning,
								OperatorSurfaceCommon::e_refuseMissing);
						if(spSurfaceAccessorI.isValid())
						{
							const I32 subCount=spSurfaceAccessorI->subCount(
									primitiveIndex);

							text.sPrintf("%-9s %-14s %d",
									"integer","subCount",subCount);
							drawLabel(m_spDrawI,location,text,
									FALSE,1,yellow,NULL,&backColor);
							location[1]-=m_textHeight;
						}
					}
					else if(pass==3)
					{
						text.sPrintf("detail");
						drawLabel(m_spDrawI,location,text,
								FALSE,1,yellow,NULL,&backColor);
						location[1]-=m_textHeight;
					}

					if(elementIndex<0)
					{
						location[1]-=m_textHeight;
						continue;
					}

					for(I32 specIndex=0;specIndex<specCount;specIndex++)
					{
						const SurfaceAccessibleI::Spec& rSpec=specs[specIndex];
						const String attrName=rSpec.name();
						const String attrType=rSpec.typeName();

						if(attrName=="surf:points" || attrName=="surf:vertices")
						{
							continue;
						}

						String valueString;

						sp<SurfaceAccessorI> spSurfaceAccessorI;
						spSurfaceViewerOp->access(spSurfaceAccessorI,
								m_spImpactAccessible,m_impactNodeName,
								OperatorSurfaceCommon::Element(element),
								attrName,
								OperatorSurfaceCommon::e_warning,
								OperatorSurfaceCommon::e_refuseMissing);

						if(spSurfaceAccessorI.isValid())
						{
							if(attrType=="string")
							{
								valueString=spSurfaceAccessorI->string(
										elementIndex,subIndex);

								const I32 maxLen=36;	//* tweak
								const I32 len=valueString.length();
								if(len>maxLen)
								{
									String bracket;
									bracket.sPrintf("[%d chars]",len);
									valueString.sPrintf("%s %.*s...",
											bracket.c_str(),
											maxLen-bracket.length()-4,
											valueString.c_str());
								}
							}
							else if(attrType=="integer")
							{
								const I32 value=spSurfaceAccessorI->integer(
										elementIndex,subIndex);
								valueString.sPrintf("%d",value);
							}
							else if(attrType=="real")
							{
								const Real value=spSurfaceAccessorI->real(
										elementIndex,subIndex);
								valueString.sPrintf("%.6G",value);
							}
							else if(attrType=="vector3" || attrType=="color")
							{
								const SpatialVector value=
										spSurfaceAccessorI->spatialVector(
										elementIndex,subIndex);
								valueString.sPrintf("%s",c_print(value));
							}
						}

						text.sPrintf("%-9s %-14s %s",
								attrType.c_str(),attrName.c_str(),
								valueString.c_str());
						drawLabel(m_spDrawI,location,text,
								FALSE,1,yellow,NULL,&backColor);
						location[1]-=m_textHeight;
					}

					location[1]-=m_textHeight;
				}

				if(spSurfaceViewerOp->outline())
				{
					String scrollTo=m_selectName;
					if(scrollTo.empty())
					{
						scrollTo=m_impactNodeName;
					}
					if(scrollTo.empty() && m_spImpactSurface.isValid())
					{
						scrollTo=m_spImpactSurface->name();
					}

					const I32 outlineCount=m_outline.size();
					I32 outlineStart=0;
					for(;outlineStart<outlineCount;outlineStart++)
					{
						if(m_outline[outlineStart]==scrollTo)
						{
							break;
						}
					}

					//* HACK .faceset is Alembic-specific
					if(outlineStart==outlineCount)
					{
						outlineStart=0;
					}
					else if(outlineStart<outlineCount-1 &&
								m_outline[outlineStart+1].match(
								"  \\.faceset .*"))
					{
						const String parentName=
								scrollTo.replace("/(?=[^/]*$).*","");

						for(outlineStart=0;outlineStart<outlineCount;
								outlineStart++)
						{
							if(m_outline[outlineStart]==parentName)
							{
								break;
							}
						}
						if(outlineStart==outlineCount)
						{
							outlineStart=0;
						}
					}

					I32 outlineEnd=outlineStart+1;
					for(;outlineEnd<outlineCount;outlineEnd++)
					{
						const String& rString=m_outline[outlineEnd];
						if(!rString.empty() &&
								m_outline[outlineEnd].c_str()[0]!=' ')
						{
							break;
						}
					}

					I32 outlineTarget=outlineStart;
					const I32 maxLines=(outlineY-outlineLimit)/m_textHeight;
					const I32 emptyLines=maxLines-(outlineCount-outlineTarget);
					if(emptyLines>0)
					{
						outlineTarget-=emptyLines;
					}
					if(outlineTarget<0)
					{
						outlineTarget=0;
					}

					const I32 outlineSlow=4;
					I32 outlineDelta=outlineTarget-m_outlineAt;
					if(outlineDelta>0)
					{
						outlineDelta+=outlineSlow-1;
					}
					else if(outlineDelta<0)
					{
						outlineDelta-=outlineSlow-1;
					}
					m_outlineAt+=outlineDelta/outlineSlow;

					set(location,0.0,outlineY,0.0);

					I32 charCount=4;

					for(I32 outlineIndex=m_outlineAt;
							outlineIndex<outlineCount &&
							location[1]>outlineLimit;
							outlineIndex++)
					{
						const I32 oneCount=m_outline[outlineIndex].length();
						if(charCount<oneCount)
						{
							charCount=oneCount;
						}

						location[1]-=m_textHeight;
					}

					charCount++;

					set(location,winx-charCount*m_characterWidth,outlineY,0.0);

					for(I32 outlineIndex=m_outlineAt;
							outlineIndex<outlineCount &&
							location[1]>outlineLimit;
							outlineIndex++)
					{
						drawLabel(m_spDrawI,location,m_outline[outlineIndex],
								FALSE,1,
								(outlineIndex>=outlineStart &&
								outlineIndex<outlineEnd)? yellow: grey,
								NULL,&backColor);
						location[1]-=m_textHeight;
					}
				}
			}
		}
		else
		{
			I32 surfaceOffset=0;

			const BWORD help=spSurfaceViewerOp->help();
			const I32 surfaceLimit=m_textHeight*(help? 21: 2);

			String currentPath;

			if(m_impactSurfaceIndex>0)
			{
				const I32 maxLines=(winy-surfaceLimit)/m_textHeight;
				const I32 tryMax=maxLines/2;

				surfaceOffset=m_impactSurfaceIndex;
				I32 tryCount=0;

				while(surfaceOffset>=0)
				{
					sp<SurfaceI> spSurface=surface(surfaceOffset);
					if(spSurface.isNull())
					{
						continue;
					}

					tryCount++;

					const String surfaceName=spSurface->name();
					const String pathName=surfaceName.pathname();
					const String baseName=surfaceName.basename();

//					feLog("check %d %d/%d \"%s\"\n",
//							surfaceOffset,tryCount,tryMax,surfaceName.c_str());

					if(currentPath!=pathName)
					{
						currentPath=pathName;

						String tokens=pathName;
						String token=pathName;
						while(!(token=tokens.parse("\"","/")).empty())
						{
							tryCount++;
						}
					}

					if(tryCount>tryMax)
					{
						break;
					}

					surfaceOffset--;
				}
			}

			currentPath="";

			const SpatialVector tabVector(m_characterWidth,0.0);

			I32 indentCount=0;
			String text;
			for(I32 surfaceIndex=surfaceOffset;surfaceIndex<surfaceCount;
					surfaceIndex++)
			{
				sp<SurfaceI> spSurface=surface(surfaceIndex);
				if(spSurface.isNull())
				{
					continue;
				}

				const String surfaceName=spSurface->name();
				const String pathName=surfaceName.pathname();
				const String baseName=surfaceName.basename();

				if(currentPath!=pathName)
				{
					indentCount=0;

					String lastTokens=currentPath;
					String tokens=pathName;
					String token=pathName;
					while(!(token=tokens.parse("\"","/")).empty())
					{
						if(location[1]<surfaceLimit)
						{
							break;
						}

						String lastToken=lastTokens.parse("\"","/");
						if(token!=lastToken)
						{
							lastTokens="";
							drawLabel(m_spDrawI,location+indentCount*tabVector,
									token,FALSE,1,grey,NULL,&backColor);
							location[1]-=m_textHeight;
						}

						indentCount++;
					}

					currentPath=pathName;
				}

				if(location[1]<surfaceLimit)
				{
					break;
				}

				if(help)
				{
					text.sPrintf("%d %s",surfaceIndex+1,
							baseName.c_str());
				}
				else
				{
					text=baseName;
				}

				const Real emphasis=m_nodeArray[surfaceIndex].m_emphasis;

				Color tint;
				if(surfaceIndex==m_impactSurfaceIndex)
				{
					tint=yellow;
				}
				else if(help)
				{
					set(tint,
							0.5-0.5*emphasis,
							0.5+0.5*emphasis,
							1.0-1.0*emphasis);
				}
				else
				{
					set(tint,
							0.6-0.6*emphasis,
							0.6+0.4*emphasis,
							0.6-0.6*emphasis);
				}

				drawLabel(m_spDrawI,location+indentCount*tabVector,
						text,FALSE,1,tint,NULL,&backColor);
				location[1]-=m_textHeight;
			}
		}

		m_spDrawI->popDrawMode();

		return;
	}
}

void SurfaceViewerOp::DrawHandler::drawNormals(I32 a_surfaceIndex)
{
	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	const String inputName=spSurfaceViewerOp->inputNameByIndex(a_surfaceIndex);

	sp<SurfaceAccessibleI> spSurfaceAccessible;
	spSurfaceViewerOp->access(spSurfaceAccessible,inputName,e_quiet);
	if(spSurfaceAccessible.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spPointAccessor;
	spSurfaceViewerOp->access(spPointAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_point,
			OperatorSurfaceCommon::e_position,
			OperatorSurfaceCommon::e_warning,
			OperatorSurfaceCommon::e_refuseMissing);
	if(spPointAccessor.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spNormalAccessor;
	spSurfaceViewerOp->access(spNormalAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_point,
			OperatorSurfaceCommon::e_normal,
			OperatorSurfaceCommon::e_warning,
			OperatorSurfaceCommon::e_refuseMissing);
	if(spNormalAccessor.isNull())
	{
		return;
	}

	//* TODO param
	const Real normalScale=0.3;

	const I32 pointCount=spPointAccessor->count();

	SpatialVector* pLines=new SpatialVector[pointCount*2];
	I32 lineIndex=0;

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point=spPointAccessor->spatialVector(pointIndex);
		const SpatialVector norm=spNormalAccessor->spatialVector(pointIndex);

		pLines[lineIndex++]=point;
		pLines[lineIndex++]=point+normalScale*norm;
	}

	const Color mediumGreen(0.0,0.5,0.0);
	m_spDrawI->drawLines(pLines,NULL,lineIndex,
			DrawI::e_discrete,false,&mediumGreen);

	delete[] pLines;
}

void SurfaceViewerOp::DrawHandler::drawInstances(I32 a_surfaceIndex,
	const Color* a_pColor)
{
	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	const String inputName=spSurfaceViewerOp->inputNameByIndex(a_surfaceIndex);

	sp<SurfaceAccessibleI> spSurfaceAccessible;
	spSurfaceViewerOp->access(spSurfaceAccessible,inputName,e_quiet);
	if(spSurfaceAccessible.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spInstanceAccessor;
	spSurfaceViewerOp->access(spInstanceAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_primitive,"instance",
			e_quiet,e_refuseMissing);
	if(spInstanceAccessor.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spAnimXAccessor;
	spSurfaceViewerOp->access(spAnimXAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_primitive,"animX",
			e_quiet,e_refuseMissing);
	if(spAnimXAccessor.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spAnimYAccessor;
	spSurfaceViewerOp->access(spAnimYAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_primitive,"animY",
			e_quiet,e_refuseMissing);
	if(spAnimYAccessor.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spAnimZAccessor;
	spSurfaceViewerOp->access(spAnimZAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_primitive,"animZ",
			e_quiet,e_refuseMissing);
	if(spAnimZAccessor.isNull())
	{
		return;
	}

	sp<SurfaceAccessorI> spAnimTAccessor;
	spSurfaceViewerOp->access(spAnimTAccessor,spSurfaceAccessible,
			OperatorSurfaceCommon::e_primitive,"animT",
			e_quiet,e_refuseMissing);
	if(spAnimTAccessor.isNull())
	{
		return;
	}

	if(a_surfaceIndex>=I32(m_nodeArray.size()))
	{
		return;
	}

	SurfaceNode& rSurfaceNode=m_nodeArray[a_surfaceIndex];
	sp<DrawableI>& rspDrawable=rSurfaceNode.m_spDrawable;
	sp<DrawBufferI>& rspSolidBuffer=rSurfaceNode.m_spSolidBuffer;

	const I32 primitiveCount=spInstanceAccessor->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const String instance=spInstanceAccessor->string(primitiveIndex);
		if(instance.empty())
		{
			continue;
		}
		SpatialTransform transform;
		transform.column(0)=spAnimXAccessor->spatialVector(primitiveIndex);
		transform.column(1)=spAnimYAccessor->spatialVector(primitiveIndex);
		transform.column(2)=spAnimZAccessor->spatialVector(primitiveIndex);
		transform.column(3)=spAnimTAccessor->spatialVector(primitiveIndex);
		m_spDrawI->drawTransformed(transform,rspDrawable,
				a_pColor,rspSolidBuffer);
	}
}

void SurfaceViewerOp::DrawHandler::drawHorizon(void)
{
	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	const SpatialVector vertical=
			spSurfaceViewerOp->catalog<SpatialVector>("vertical");
	const BWORD zUp=(vertical[2]>0.9);
	const I32 upIndex=zUp? 2: 1;

	sp<ViewI> spView=m_spDrawI->view();
	sp<CameraI> spCameraI=spView->camera();
	const I32 winx=width(spView->viewport());
	const I32 winy=height(spView->viewport());

	SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
	SpatialTransform cameraTransform;
	invert(cameraTransform,cameraMatrix);

	const SpatialVector cameraSide=cameraTransform.column(0);
	const SpatialVector cameraDir= -cameraTransform.column(2);
	const SpatialVector cameraPos=cameraTransform.column(3);

	SpatialVector groundPos=cameraPos;
	SpatialVector groundSide=cameraSide;
	SpatialVector groundDir=cameraDir;

	groundPos[upIndex]=0.0;
	groundSide[upIndex]=0.0;
	groundDir[upIndex]=0.0;

	normalizeSafe(groundSide);
	normalizeSafe(groundDir);

	SpatialVector aheadCenter=groundPos+1e6*groundDir;
	SpatialVector aheadRight=aheadCenter+1e6*groundSide;
	SpatialVector aheadLeft=aheadCenter-1e6*groundSide;

	const SpatialVector winCenter=
			spView->project(aheadCenter,ViewI::e_perspective);
	const SpatialVector winRight=
			spView->project(aheadRight,ViewI::e_perspective);
	const SpatialVector winLeft=
			spView->project(aheadLeft,ViewI::e_perspective);

	const SpatialVector toRight=unitSafe(winRight-winCenter);
	const SpatialVector toLeft=unitSafe(winLeft-winCenter);
	const Real toShort=0.15*winx;
	const Real toLong=0.25*winx;

	const Color softGreen(0.0,1.0,0.0,0.3);
	SpatialVector line[4];

	line[0]=winCenter+toShort*toLeft;
	line[1]=winCenter+toLong*toLeft;
	line[2]=winCenter+toLong*toRight;
	line[3]=winCenter+toShort*toRight;

	m_spDrawI->drawLines(line,NULL,4,DrawI::e_discrete,FALSE,&softGreen);

	sp<QuickViewerI> spQuickViewer=spSurfaceViewerOp->quickViewer();
	const Real frameRate=spQuickViewer->frameRate();

	static Real fpsKeep=0.95;
	static Real fps=60.0;
	fps=fpsKeep*fps+(1.0-fpsKeep)*frameRate;
//	feLog("frameRate %.6G fps %.6G\n",frameRate,fps);

	static SpatialVector lastGroundPos(0,0,0);
	static Real lastAltitude(0);
	static Real lastForwardSpeed(0);
	static Real forwardAcceleration(0);
	static Real lateralSpeed(0);
	static Real verticalSpeed(0);

	const SpatialVector cameraStep(groundPos-lastGroundPos);
	const Real altitude(cameraPos[upIndex]);
	const Real north(zUp? cameraPos[1]: -cameraPos[2]);
	const Real east(zUp? cameraPos[0]: cameraPos[0]);
	const Real forwardSpeed=fps*dot(cameraDir,cameraStep);

	const Real accKeep=0.97;	//* NOTE tweak
	const Real accKeep1=1.0-accKeep;
	forwardAcceleration=accKeep*forwardAcceleration+
			accKeep1*(fps*(forwardSpeed-lastForwardSpeed));

	const Real keep=0.9;	//* NOTE tweak
	const Real keep1=1.0-keep;
	lateralSpeed=keep*lateralSpeed+
			keep1*(fps*dot(cameraSide,cameraStep));
	verticalSpeed=keep*verticalSpeed+
			keep1*(fps*(altitude-lastAltitude));

	lastGroundPos=groundPos;
	lastAltitude=altitude;
	lastForwardSpeed=forwardSpeed;

	const Color brightGreen(0.0,1.0,0.0);
	const Color dimGreen(0.0,1.0,0.0,0.4);
	const Color black(0.0,0.0,0.0);
	const Color backColor=background();

	Box2i box;

	set(box,0.12*winx,0.1*winy,0.15*winx,0.01*winy);
	drawIndicator(m_spDrawI,box,FALSE,TRUE,east,0.0,10.0,
			0,5,5,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	set(box,0.1*winx,0.12*winy,0.01*winx,0.15*winy);
	drawIndicator(m_spDrawI,box,TRUE,TRUE,north,0.0,10.0,
			0,5,5,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	set(box,0.8*winx,0.3*winy,0.01*winx,0.4*winy);
	drawIndicator(m_spDrawI,box,TRUE,FALSE,altitude,0.0,10.0,
			-1,6,6,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	box[0]=0.2*winx;
	drawIndicator(m_spDrawI,box,TRUE,TRUE,forwardSpeed,0.0,10.0,
			0,4,4,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	set(box,Vector2i(0.9*winx,0.4*winy),Vector2i(0.01*winx,0.2*winy));
	drawIndicator(m_spDrawI,box,TRUE,FALSE,verticalSpeed,0.0,1.0,
			-1,4,5,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	box[0]=0.1*winx;
	drawIndicator(m_spDrawI,box,TRUE,TRUE,forwardAcceleration,0.0,10.0,
			0,5,5,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	const Real heading=(upIndex==2)?
			180.0-fe::radToDeg*atan2(cameraDir[0],-cameraDir[1]):
			180.0+fe::radToDeg*atan2(cameraDir[0],-cameraDir[2]);

	set(box,0.3*winx,0.9*winy,0.4*winx,0.01*winy);
	drawIndicator(m_spDrawI,box,FALSE,FALSE,heading,360.0,10.0,
			0,3,3,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	set(box,0.4*winx,0.1*winy,0.2*winx,0.01*winy);
	drawIndicator(m_spDrawI,box,FALSE,TRUE,lateralSpeed,0.0,1.0,
			-1,5,5,m_multiplication,&brightGreen,&dimGreen,&backColor,&black);

	set(box,0.1*winx,0.1*winy,0.15*winx,0.15*winy);

	spView->setScissor(&box);

	SpatialTransform xform;
	setIdentity(xform);
	translate(xform,SpatialVector(0.195*winx,0.195*winy));
	rotate(xform,-heading*fe::degToRad,e_zAxis);

	SpatialTransform xform2=xform;
	translate(xform2,SpatialVector(0.0,-0.6*forwardSpeed));

	set(line[0],-0.5,-0.5);
	set(line[1],0.0,0.5);
	set(line[2],0.5,-0.5);

	SpatialVector scale(32.0,32.0,32.0);

	m_spDrawI->drawTransformedLines(xform2,&scale,line,NULL,3,
			DrawI::e_strip,false,&dimGreen);

	m_spDrawI->drawTransformedLines(xform,&scale,line,NULL,3,
			DrawI::e_strip,false,&brightGreen);

	spView->setScissor(NULL);
}

void SurfaceViewerOp::DrawHandler::drawDesignator(I32 a_surfaceIndex)
{
	if(m_impactSurfaceIndex>=0 && a_surfaceIndex!=m_impactSurfaceIndex)
	{
		return;
	}

	sp<SurfaceI> spSurface=surface(a_surfaceIndex);
	if(spSurface.isNull())
	{
		return;
	}

	//* skip active camera
	sp<CameraI> spCamera=spSurface;
	if(spCamera.isValid())
	{
		sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
		sp<QuickViewerI> spQuickViewer=spSurfaceViewerOp->quickViewer();
		sp<CameraControllerI> spCameraController=
				spQuickViewer->getCameraControllerI();
		sp<CameraI> spCustomCamera=spCameraController->camera();

		if(spCamera==spCustomCamera)
		{
			return;
		}
	}

	sp<ViewI> spView=m_spDrawI->view();
	sp<CameraI> spCameraI=spView->camera();

	SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
	SpatialTransform cameraTransform;
	invert(cameraTransform,cameraMatrix);

	const SpatialVector cameraDir= -cameraTransform.column(2);
	const SpatialVector cameraPos=cameraTransform.column(3);

	const I32 winx=width(spView->viewport());
	const I32 winy=height(spView->viewport());
	const I32 winMin=fe::maximum(8.0,fe::minimum(0.5*winx-64,0.5*winy-32));

	const SpatialVector center=spSurface->center();
	const Real radius=spSurface->radius();

	BWORD offScreen=FALSE;

	const Real along=dot(center-cameraPos,cameraDir);
	SpatialVector centerMod=center;
	if(along<0.0)
	{
		centerMod-=2.0*along*cameraDir;
		offScreen=TRUE;
	}

	SpatialVector target=
			spView->project(centerMod,ViewI::e_perspective);
	const Real pixels=spView->pixelSize(center,radius);

//	feLog("designator %d center %s radius %.6G\n",
//			a_surfaceIndex,c_print(center),radius);
//	feLog("  target %s pixels %.6G\n",c_print(target),pixels);
//	feLog("  along %.6G centerMod %s\n",along,c_print(centerMod));

	const I32 minHalfWidth=24;
	const I32 margin=12;
	const I32 edge=minHalfWidth+margin;

	if(target[0]<edge)
	{
		target[0]=edge;
		offScreen=TRUE;
	}
	else if(target[0]>winx-edge)
	{
		target[0]=winx-edge;
		offScreen=TRUE;
	}

	if(target[1]<edge)
	{
		target[1]=edge;
		offScreen=TRUE;
	}
	else if(target[1]>winy-edge)
	{
		target[1]=winy-edge;
		offScreen=TRUE;
	}

	I32 limit=winMin;

	limit=fe::minimum(limit,I32(target[0]-margin));
	limit=fe::minimum(limit,I32(target[1]-margin));
	limit=fe::minimum(limit,I32(winx-margin-target[0]));
	limit=fe::minimum(limit,I32(winy-margin-target[1]));

	const Real preferred=Real(0.5*pixels);
	const Real halfWidth=fe::maximum(Real(minHalfWidth),
			fe::minimum(Real(limit),preferred));
	const BWORD reduced=(halfWidth<preferred);
	const SpatialVector lip(reduced? 4.0: 8.0, 0.0, 0.0);

	const BWORD onlyThis=(a_surfaceIndex==m_impactSurfaceIndex);
	const BWORD onlyOne=(surface(1).isNull());

	const Real emphasis=(onlyThis || onlyOne)? 0.0:
			m_nodeArray[a_surfaceIndex].m_emphasis;
	const Real emphasisPow=pow(emphasis,0.3);

	const Color backColor=background();
	const Color backFade(backColor[0],backColor[1],backColor[2],
			onlyThis? Real(1): emphasisPow);
	const Color rampGreen(0.0,1.0,0.0,0.3+0.7*emphasis);
	const Color powerGreen(0.0,1.0,0.0,emphasisPow);
	const Color hardGreen(0.0,0.6,0.0);

	SpatialVector line[4];

	line[1]=target+SpatialVector(-halfWidth,-halfWidth);
	line[2]=target+SpatialVector(-halfWidth,halfWidth);
	line[0]=line[1]+lip;
	line[3]=line[2]+lip;

	m_spDrawI->drawLines(line,NULL,4,DrawI::e_strip,FALSE,&rampGreen);

	line[1]=target+SpatialVector(halfWidth,-halfWidth);
	line[2]=target+SpatialVector(halfWidth,halfWidth);
	line[0]=line[1]-lip;
	line[3]=line[2]-lip;

	m_spDrawI->drawLines(line,NULL,4,DrawI::e_strip,FALSE,&rampGreen);

	SpatialVector location=line[1]+SpatialVector(4,0);
	String text;
	const I32 border=2;
	const I32 textInc=m_textHeight+1;

	//* only if this surface is selected
	if(onlyThis)
	{
		//* NOTE bounding sphere, fixed background

		text.sPrintf("R%.1f",radius);
		drawLabel(m_spDrawI,location,text,FALSE,border,
				hardGreen,NULL,&backColor);
		location[1]+=textInc;

		text.sPrintf("Z%.1f",center[2]);
		drawLabel(m_spDrawI,location,text,FALSE,border,
				hardGreen,NULL,&backColor);
		location[1]+=textInc;

		text.sPrintf("Y%.1f",center[1]);
		drawLabel(m_spDrawI,location,text,FALSE,border,
				hardGreen,NULL,&backColor);
		location[1]+=textInc;

		text.sPrintf("X%.1f",center[0]);
		drawLabel(m_spDrawI,location,text,FALSE,border,
				hardGreen,NULL,&backColor);
		location[1]+=textInc;
	}

	//* only if multiple surfaces are shown, and none selected
	if(!onlyOne && !onlyThis)
	{
		//* NOTE surface name, no background

		location=target+SpatialVector(0.0,-halfWidth-5.0);

		drawLabel(m_spDrawI,location,spSurface->name().basename(),TRUE,0,
				powerGreen,NULL,NULL);
		location[1]+=textInc;
	}

	//* only if there are multiple surfaces
	if(!onlyOne)
	{
		//* NOTE surface number, dynamic background

		location=line[2]+SpatialVector(4,-6);

		text.sPrintf("%d",a_surfaceIndex+1);
		drawLabel(m_spDrawI,location,text,FALSE,border,
				rampGreen,NULL,&backFade);
		location[1]+=m_textHeight;
	}

	if(offScreen)
	{
		//* NOTE big X

		line[0]=target+0.7*SpatialVector(-halfWidth,-halfWidth);
		line[1]=target+0.7*SpatialVector(halfWidth,halfWidth);
		line[2]=target+0.7*SpatialVector(-halfWidth,halfWidth);
		line[3]=target+0.7*SpatialVector(halfWidth,-halfWidth);

		m_spDrawI->drawLines(line,NULL,4,DrawI::e_discrete,FALSE,&rampGreen);
	}
}

void SurfaceViewerOp::DrawHandler::runMenu(void)
{
//	feLog("\nSurfaceViewerOp::DrawHandler::runMenu\n");

	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	sp<Catalog>& rspMenu=spSurfaceViewerOp->menu();
	BWORD menuFound=FALSE;

	const Color backColor=background();
	const Color orange(1.0,0.8,0.0);
	const Color yellow(1.0,1.0,0.0);
	const Color grey(0.5,0.5,0.5);
	const Color lightRed(0.9,0.7,0.7);
	const Color lightGreen(0.7,0.9,0.7);
	const Color lightBlue(0.6,0.6,1.0);

	sp<ViewI> spView=m_spDrawI->view();
//	const I32 winx=width(spView->viewport());
	const I32 winy=height(spView->viewport());

	m_spDrawI->pushDrawMode(m_spDrawText);

	SpatialVector location(m_characterWidth,winy-2*m_textHeight,0);

	String buffer=spSurfaceViewerOp->menuString();
	String menuString="";
	while(!buffer.empty())
	{
		//* resolve backspaces
		String token=buffer.parse("\"","\b");
		if(buffer.empty())
		{
			menuString+=token;
			break;
		}
		menuString+=token.chop(1);
	}
	spSurfaceViewerOp->setMenuString(menuString);

	drawLabel(m_spDrawI,location,menuString,
			FALSE,1,orange,NULL,&backColor);
	location[1]-=20.0;

	const I32 columnX[4]={0,24,46,76};

	I32 column=0;
	const String root=rspMenu->catalog<String>("root");
	String atMenu=root;
	Array<String> atKeyStack(1);
	atKeyStack[0]="";
	while(!atMenu.empty())
	{
		if(atMenu==root)
		{
			set(location,m_characterWidth*(1+columnX[column++]),
					winy-4*m_textHeight,0);
		}

		const String keys=rspMenu->catalogOrDefault<String>(atMenu,"keys","");
		const I32 stackCount=atKeyStack.size();
		FEASSERT(stackCount>0);
		const String atKey=atKeyStack[stackCount-1];
//		feLog("at \"%s\" keys \"%s\" atKey \"%s\" stack %d\n",
//				atMenu.c_str(),keys.c_str(),atKey.c_str(),stackCount);

		if(atKey==keys)
		{
			if(stackCount<2)
			{
				break;
			}
//			feLog("pop\n");
			atMenu=atMenu.chop(1);
			atKeyStack.resize(stackCount-1);
			continue;
		}

		if(atMenu==menuString)
		{
			menuFound=TRUE;
		}

		const String remainder=keys.prechop(atKey);
		const FESTRING_U8 nextChar=remainder.c_str()[0];
		String nextString;
		nextString.sPrintf("%c",nextChar);

		atMenu+=nextString;

		atKeyStack.resize(stackCount+1);
		atKeyStack[stackCount-1]+=nextString;
		atKeyStack[stackCount]="";

		String text=rspMenu->catalogOrDefault<String>(atMenu,"label",atMenu);
		BWORD highlight=(atMenu==menuString);
		const BWORD keyed=
				!rspMenu->catalogOrDefault<String>(atMenu,"keys","").empty();
		BWORD active=keyed;
		Color textColor=lightBlue;

		Instance instance;

		const String parentMenu=atMenu.chop(1);
		if(rspMenu->catalogLookup(parentMenu,instance) &&
				instance.is<String>())
		{
			String& rParentValue=rspMenu->catalog<String>(parentMenu);

			String buffer=text;
			const String parsedKey=buffer.parse();

			active=TRUE;
			textColor=(rParentValue==parsedKey)? lightGreen: lightRed;

			if(highlight)
			{
				rParentValue=parsedKey;

				menuFound=TRUE;
				menuString=menuString.chop(1);
				spSurfaceViewerOp->setMenuString(menuString);

				if(parentMenu=="/sc")
				{
					if(parsedKey=="part")
					{
						const I32 inputCount=
								spSurfaceViewerOp->drawableCount();
						for(I32 inputIndex=0;inputIndex<inputCount;
								inputIndex++)
						{
							spSurfaceViewerOp->partitionSurface(inputIndex);
						}
					}

					clearBuffers();
				}
			}
		}

		if(rspMenu->catalogLookup(atMenu,instance) && !keyed)
		{
			if(instance.is<bool>())
			{
				active=TRUE;
				BWORD& rValue=rspMenu->catalog<bool>(atMenu);
				if(atMenu==menuString)
				{
					menuFound=TRUE;
					rValue=!rValue;
					menuString=menuString.chop(1);
					spSurfaceViewerOp->setMenuString(menuString);

					if(atMenu=="/fn")
					{
						rValue=FALSE;

						const String partName=rspMenu->catalog<String>("/ff");
						findPart(partName);
					}
					if(atMenu=="/se" || atMenu=="/sf")
					{
						clearBuffers();
					}
					if(atMenu=="/dd")
					{
						clearEmphasis();
					}
				}
				const BWORD button=(rspMenu->catalogOrDefault<String>(atMenu,
						"suggest","")=="button");

				text.sPrintf("%-13s%s",text.c_str(),
						button? "": (rValue? " ON": " OFF"));
				if(!button)
				{
					textColor=rValue? lightGreen: lightRed;
				}
			}
			else if(instance.is<I32>() || instance.is<Real>() ||
					instance.is<String>() || instance.is<Color>())
			{
				active=TRUE;
				String state;

				if(!strncmp(atMenu.c_str(),menuString.c_str(),atMenu.length()))
				{
					menuFound=TRUE;
					highlight=TRUE;

					String buffer=menuString.prechop(atMenu.length());
					state=instance.is<Color>()? buffer: buffer.parse();

//					feLog("atMenu \"%s\" menuString \"%s\"  state \"%s\"\n",
//							atMenu.c_str(),menuString.c_str(),state.c_str());

					if(menuString.search("\n"))
					{
						if(instance.is<I32>())
						{
							const Real value=atoi(state.c_str());
							if(rspMenu->catalog<I32>(atMenu)!=value)
							{
								rspMenu->catalog<I32>(atMenu)=value;

								if(atMenu=="/dm")
								{
									spSurfaceViewerOp->registry()->master()
											->catalog()->catalog<I32>(
											"hint:multisample")=value;
									m_reopenCountdown=2;
									clearBuffers();
								}

								if(atMenu=="/dr")
								{
									clearBuffers();
								}
							}
						}
						else if(instance.is<Real>())
						{
							const Real value=atof(state.c_str());
							rspMenu->catalog<Real>(atMenu)=value;

							if(atMenu=="/db")
							{
								m_spDrawI->setBrightness(value);
							}
							else if(atMenu=="/dc")
							{
								m_spDrawI->setContrast(value);
							}
						}
						else if(instance.is<String>())
						{
							rspMenu->catalog<String>(atMenu)=state;

							if(atMenu=="/ff")
							{
								findPart(state);
							}
						}
						else if(instance.is<Color>())
						{
							Real value[3];
							value[0]=atof(buffer.parse().c_str());
							value[1]=atof(buffer.parse().c_str());
							value[2]=atof(buffer.parse().c_str());

							rspMenu->catalog<Color>(atMenu)=
									Color(value[0],value[1],value[2]);
						}
						menuString=atMenu.chop(1);
						spSurfaceViewerOp->setMenuString(menuString);
					}
				}
				else
				{
					if(instance.is<I32>())
					{
						state=fe::print(rspMenu->catalog<I32>(atMenu));
					}
					else if(instance.is<Real>())
					{
						state=fe::print(rspMenu->catalog<Real>(atMenu));
					}
					else if(instance.is<String>())
					{
						state=rspMenu->catalog<String>(atMenu);
					}
					else if(instance.is<Color>())
					{
						const Color value=rspMenu->catalog<Color>(atMenu);
						state.sPrintf("%.6G %.6G %.6G",
								value[0],value[1],value[2]);
					}
				}

				if(atMenu!=menuString)
				{
					text.sPrintf("%-13s%s",text.c_str(),
							state.empty()? "": (" "+state).c_str());

				}
			}
		}

		drawLabel(m_spDrawI,location,text,FALSE,1,
				highlight? yellow: (active? textColor: grey),
				NULL,&backColor);
		location[1]-=m_textHeight;
	}

	m_spDrawI->popDrawMode();

	if(!menuFound)
	{
		const I32 len=menuString.length();
		if(len>1)
		{
			const char byte=menuString.c_str()[1];
			if(byte>='0' && byte<='9')
			{
				menuFound=TRUE;

				if(menuString.c_str()[len-1]=='\n')
				{
					const I32 value=atoi(menuString.c_str()+1);
					spSurfaceViewerOp->setMenuString("");

					pick(value-1);
				}
			}
		}
	}

	if(!menuFound && menuString.search("\n"))
	{
		//* unanticipated newline closes menu
		menuFound=TRUE;
		spSurfaceViewerOp->setMenuString("");
	}

	if(!menuFound)
	{
		spSurfaceViewerOp->setMenuString(menuString.chop(1));
	}
}

void SurfaceViewerOp::DrawHandler::findPart(String a_partname)
{
	if(m_impactSurfaceIndex>=0 && m_selectName!=a_partname)
	{
		//* TODO regex

		m_selectName=a_partname;

		sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
		spSurfaceViewerOp->partitionSurface(m_impactSurfaceIndex);

		//* HACK pretend a frame change
		const I32 frame=m_frame--;
		setFrame(frame);
	}
}

sp<SurfaceI> SurfaceViewerOp::DrawHandler::surface(I32 a_surfaceIndex) const
{
	if(a_surfaceIndex>=0 && a_surfaceIndex<I32(m_nodeArray.size()))
	{
		return m_nodeArray[a_surfaceIndex].m_spDrawable;
	}
	return sp<SurfaceI>(NULL);
}

String SurfaceViewerOp::DrawHandler::nodeName(I32 a_surfaceIndex) const
{
	if(a_surfaceIndex>=0 && a_surfaceIndex<I32(m_nodeArray.size()))
	{
		return m_nodeArray[a_surfaceIndex].m_nodeName;
	}
	return "";
}

void SurfaceViewerOp::EventHandler::handle(Record& a_rSignal)
{
	m_event.bind(a_rSignal);


	if(m_event.isPoll() || m_event.isIdleMouse())
	{
		if(m_event.isIdleMouse())
		{
			sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
			if(spSurfaceViewerOp.isValid())
			{
				spSurfaceViewerOp->incrementIdle();
			}
		}
		return;
	}

	sp<SurfaceViewerOp> spSurfaceViewerOp=m_hpSurfaceViewerOp;
	spSurfaceViewerOp->clearIdle();

	if(spSurfaceViewerOp.isValid())
	{
		spSurfaceViewerOp->react(m_event);
	}
}

void SurfaceViewerOp::react(WindowEvent& a_rEvent)
{
//	feLog("SurfaceViewerOp::react %s\n",fe::print(a_rEvent).c_str());

	//* excludes mouse move without scanning
	if(!a_rEvent.isMouseMotion() || (a_rEvent.item()&WindowEvent::e_keyShift))
	{
		m_spDrawHandler->incrementSerial();
	}

	if(a_rEvent.isMouseEnter())
	{
		return;
	}
	if(a_rEvent.isMouseExit())
	{
		m_glowFrame=m_spDrawHandler->startFrame()-1;
		return;
	}

	if(a_rEvent.isKeyPress(WindowEvent::e_keyEscape))
	{
		if(!m_menuString.empty())
		{
			m_menuString="";
			return;
		}

		m_spDrawHandler->unpick();
		m_spDrawHandler->setPickingAccessible(sp<SurfaceAccessibleI>(NULL));
		return;
	}

	sp<DrawI> spDrawI=m_spQuickViewerI->getDrawI();

	U32 item=a_rEvent.item();
	BWORD shifted=FALSE;
	if(item&WindowEvent::e_keyShift)
	{
		shifted=TRUE;
		item^=WindowEvent::e_keyShift;
	}
	BWORD ctrled=FALSE;
	if(item&WindowEvent::e_keyControl)
	{
		ctrled=TRUE;
		item^=WindowEvent::e_keyControl;
	}
	BWORD alted=FALSE;
	if(item&WindowEvent::e_keyAlt)
	{
		alted=TRUE;
		item^=WindowEvent::e_keyAlt;
	}

	char byte=0;
	String character;
	if(a_rEvent.isKeyPress(WindowEvent::e_itemAny) &&
		item<WindowEvent::e_keyAsciiMask)
	{
		byte=(shifted && item<94)? item+32: item;
		character.sPrintf("%c",byte);
	}

	//* NOTE defining cursor up/down/left/right as ascii DC1-DC4

	const BWORD plain=!(shifted || ctrled || alted);

	if(plain && !character.empty() &&
			(!m_menuString.empty() || character=="/"))
	{
		if(character=="/")
		{
			m_menuString="/";
		}
		else
		{
			m_menuString+=character;
		}
	}
	else if(plain && byte>='1' && byte<='9')
	{
		const I32 index=byte-'1';
		m_spDrawHandler->pick(index);
	}
	else if(plain && character=="f")
	{
		setSlack(1.0,-1.0,0.0);
	}
	else if(plain && character=="c")
	{
		setSlack(1.0,-1.0,0.0);
		target(e_centerObject);
	}
	else if(plain && character=="o")
	{
		setSlack(1.0,-1.0,0.0);
		target(e_centerOrigin);
	}
	else if(plain && character=="p")
	{
		m_spDrawHandler->paperToggle();
	}
	else if(plain && character=="h")
	{
		m_help= !m_help;
	}
	else if(plain && character=="s")
	{
		m_spiral= !m_spiral;
	}
	else if(plain && character=="t")
	{
		m_tracking= !m_tracking;
	}
	else if(plain && character=="u")
	{
		m_spDrawHandler->clearBuffers();

		m_uvSpace= !m_uvSpace;
		m_cameraView=FALSE;
		m_orthoAxis="";

		//* HACK frame object
		setSlack(1.0,-1.0,0.0);
	}
	else if((plain || shifted) && character=="v")
	{
		if(m_uvSpace)
		{
			m_spDrawHandler->clearBuffers();
			m_uvSpace=FALSE;

			//* HACK frame object
			setSlack(1.0,-1.0,0.0);
		}
		if(shifted)
		{
			const I32 camCount=m_cameraArray.size();
			m_cameraIndex=camCount? (m_cameraIndex+1)%camCount: 0;
		}
		else
		{
			m_cameraView= !m_cameraView;
		}
		m_orthoAxis="";
	}
	else if((plain || shifted) &&
			(character=="x" || character=="y" || character=="z"))
	{
		if(m_uvSpace)
		{
			m_spDrawHandler->clearBuffers();
			m_uvSpace=FALSE;

			//* HACK frame object
			setSlack(1.0,-1.0,0.0);
		}
		m_cameraView=FALSE;

		String axis=character;
		if(shifted)
		{
			axis="-"+axis;
		}

		m_orthoAxis=(m_orthoAxis==axis)? "": axis;
	}
	else if((plain || shifted) && a_rEvent.isKeyPress(WindowEvent::e_keyTab))
	{
		if(m_uvSpace)
		{
			m_spDrawHandler->clearBuffers();
			m_uvSpace=FALSE;

			//* HACK frame object
			setSlack(1.0,-1.0,0.0);
		}
		m_cameraView=FALSE;

		if(shifted)
		{
			if(!m_orthoAxis.empty())
			{
				m_orthoAxis=(m_orthoAxis.c_str()[0]=='-')?
						m_orthoAxis.prechop("-"):
						"-"+m_orthoAxis;
			}
			else if(m_spQuickViewerI.isValid())
			{
				sp<CameraControllerI> spCameraController=
						m_spQuickViewerI->getCameraControllerI();
				if(spCameraController.isValid())
				{
					const Real azimuth=spCameraController->azimuth();
					const Real downangle=spCameraController->downangle();
					const Real distance=spCameraController->distance();

					spCameraController->setLookAngle(azimuth+180.0,-downangle,
							distance);
				}
			}
		}
		else if(m_orthoAxis.empty())
		{
			sp<ViewI> spView=spDrawI->view();
			const SpatialTransform cameraMatrix=
					spView->camera()->cameraMatrix();

			const SpatialVector v(fabs(cameraMatrix(2,0)),
					fabs(cameraMatrix(2,1)),fabs(cameraMatrix(2,2)));
			const I32 index=v[0]>v[1]? (v[0]>v[2]? 0: 2): (v[1]>v[2]? 1: 2);

			m_orthoAxis.sPrintf("%c",'x'+index);
			if(cameraMatrix(2,index)<0.0)
			{
				m_orthoAxis="-"+m_orthoAxis;
			}
		}
		else
		{
			m_orthoAxis="";
		}
	}
	else if(plain && a_rEvent.isKeyPress(WindowEvent::e_keyCursorLeft))
	{
		catalog<String>("keys")+="\x13";	//* DC3
	}
	else if(plain && a_rEvent.isKeyPress(WindowEvent::e_keyCursorRight))
	{
		catalog<String>("keys")+="\x14";	//* DC4
	}
	else if((plain && !ctrled && !alted) &&
			a_rEvent.isKeyPress(WindowEvent::e_keyCursorUp))
	{
		m_spDrawHandler->pickIncrement(-1);
	}
	else if((plain && !ctrled && !alted) &&
			a_rEvent.isKeyPress(WindowEvent::e_keyCursorDown))
	{
		m_spDrawHandler->pickIncrement(1);
	}
	else if(plain && character=="]")
	{
		spDrawI->setBrightness(0.1*I32(10.0*(spDrawI->brightness()+0.11)));
	}
	else if(plain && character=="[")
	{
		spDrawI->setBrightness(0.1*I32(10.0*(spDrawI->brightness()-0.09)));
	}
	else if(shifted && character=="}")
	{
		spDrawI->setContrast(0.1*I32(10.0*(spDrawI->contrast()+0.11)));
	}
	else if(shifted && character=="{")
	{
		spDrawI->setContrast(0.1*I32(10.0*(spDrawI->contrast()-0.09)));
	}
	else if((ctrled || alted) && character=="h")
	{
		if(!ctrled && alted)
		{
			//* just alt
			m_spDrawHandler->isolateToggle();
		}
		else if(ctrled && !alted)
		{
			//* just ctrl
			m_spDrawHandler->hideSelected();
		}
	}
	else if(ctrled && character=="g")
	{
		m_spDrawHandler->ghostSelected();
	}
	else if(!character.empty())
	{
		character.sPrintf("%c",ctrled? item-64: item);

//		feLog("key %d '%s'\n",item,character.c_str());

		catalog<String>("keys")+=character;
	}

	catalog<I32>("stepping")=m_spMenu->catalog<I32>("/ps");
	catalog<bool>("zigzag")=m_spMenu->catalog<bool>("/pz");

	const BWORD mouseMotion=a_rEvent.isMouseMotion();
	const BWORD scanning=(shifted && mouseMotion);
	const BWORD leftPress=a_rEvent.isMousePress(WindowEvent::e_itemLeft);
	const BWORD middlePress=a_rEvent.isMousePress(WindowEvent::e_itemMiddle);
	const BWORD leftDrag=(a_rEvent.isMouseDrag() &&
			a_rEvent.mouseButtons()==WindowEvent::e_mbLeft);
	const BWORD middleDrag=(a_rEvent.isMouseDrag() &&
			a_rEvent.mouseButtons()==WindowEvent::e_mbMiddle);

	if(alted || ctrled || (!scanning && !leftPress && !middlePress &&
			!leftDrag && !middleDrag && !mouseMotion))
	{
		m_spDrawHandler->setPickingAccessible(sp<SurfaceAccessibleI>(NULL));
		return;
	}

	I32 mousex=0;
	I32 mousey=0;
	a_rEvent.getMousePosition(&mousex,&mousey);

	sp<ViewI> spView=spDrawI->view();
	const I32 winy=height(spView->viewport());

	const Vector2i location(mousex,winy-1-mousey);

	if(!scanning && intersecting(m_scrubBox,location))
	{
		const Real fraction=(mousex-m_scrubBox[0])/Real(width(m_scrubBox));
		const Real minFrame=m_spDrawHandler->minFrame();
		const Real maxFrame=m_spDrawHandler->maxFrame();
		const I32 intFrame=minFrame+(maxFrame-minFrame)*fraction+0.5;

//		feLog("range %.6G %.6G fraction %.6G to %d\n",
//				minFrame,maxFrame,fraction,intFrame);

		m_glowFrame=intFrame;

		if(leftPress || middlePress || leftDrag || middleDrag)
		{
			//* don't double up or swamp
			if(m_freezeFrame || !catalog<String>("keys").empty())
			{
				return;
			}

			const Real startFrame=m_spDrawHandler->startFrame();
			const Real endFrame=m_spDrawHandler->endFrame();

			if(middlePress || middleDrag)
			{
				if(fabs(startFrame-intFrame)<fabs(endFrame-intFrame))
				{
					m_spDrawHandler->setStartFrame(intFrame);
				}
				else
				{
					m_spDrawHandler->setEndFrame(intFrame);
				}
				return;
			}

			Real toFrame=intFrame;
			toFrame=fe::maximum(toFrame,startFrame);
			toFrame=fe::minimum(toFrame,endFrame);

			String addKeys;

			Real fromFrame=m_lastFrame;
			while(fromFrame<toFrame-0.5)
			{
				fromFrame+=Real(1);
				addKeys+="\x14";
			}
			while(fromFrame>toFrame+0.5)
			{
				fromFrame-=Real(1);
				addKeys+="\x13";
			}

			if(!addKeys.empty())
			{
				m_freezeFrame=TRUE;
				catalog<String>("keys")+=addKeys;
			}

			return;
		}

		return;
	}

	m_glowFrame=m_spDrawHandler->startFrame()-1;

	if((!scanning && leftDrag && !mouseMotion) || middlePress || middleDrag)
	{
		return;
	}

	I32 nearestInputIndex= -1;
	I32 nearestSurfaceIndex= -1;

	sp<SurfaceI> spNearestSurface;
	sp<SurfaceI::ImpactI> spNearestImpact;

	const SpatialVector rayOrigin=
			spView->unproject(mousex,location[1],0.0);
	const SpatialVector target=
			spView->unproject(mousex,location[1],1.0);
	const SpatialVector rayDirection=unitSafe(target-rayOrigin);

	const I32 isolatedIndex=m_spDrawHandler->isolatedIndex();

	if(!scanning && mouseMotion)
	{
		if(m_uvSpace || !m_spMenu->catalog<bool>("/dd"))
		{
			return;
		}

		const I32 inputCount=drawableCount();
		I32 surfaceIndex=0;
		for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
		{
			Array< sp<DrawableI> >& rSubDrawableArray=
					m_drawableTable[inputIndex];

			const I32 nodeCount=rSubDrawableArray.size();
			for(I32 nodeIndex=0;nodeIndex<nodeCount;nodeIndex++)
			{
				if(m_spDrawHandler->hideAll(surfaceIndex) ||
						m_spDrawHandler->ghostAll(surfaceIndex) ||
						(isolatedIndex>=0 && surfaceIndex!=isolatedIndex))
				{
					surfaceIndex++;
					continue;
				}

				sp<DrawableI> rspSubDrawable=rSubDrawableArray[nodeIndex];

				sp<SurfaceI> spSurface=rspSubDrawable;
				if(spSurface.isNull())
				{
					surfaceIndex++;
					continue;
				}

				const SpatialVector center=spSurface->center();
				const Real radius=spSurface->radius();

				const Real distance=RaySphereIntersect<Real>::solve(
						center,radius,rayOrigin,rayDirection);
				const BWORD hit=(distance>=0.0);

				Real closeness=0.0;

				if(hit)
				{
					const SpatialVector contact=rayOrigin+distance*rayDirection;
					const SpatialVector radial=contact-center;
					const Real along=dot(radial,rayDirection);
					const SpatialVector aside=radial-along*rayDirection;
					const Real sideDistance=magnitude(aside);

					closeness=Real(1)-sideDistance/radius;
				}

				m_spDrawHandler->setEmphasis(surfaceIndex,closeness);

				surfaceIndex++;
			}
		}
		return;
	}

//	feLog("pick origin %s dir %s\n",c_print(rayOrigin),c_print(rayDirection));

	String nodeName;

	I32 surfaceIndex=0;
	const I32 inputCount=drawableCount();
	for(I32 inputIndex=0;inputIndex<inputCount;inputIndex++)
	{
		Array< sp<DrawableI> >& rSubDrawableArray=m_drawableTable[inputIndex];

		const I32 nodeCount=rSubDrawableArray.size();
		for(I32 nodeIndex=0;nodeIndex<nodeCount;nodeIndex++)
		{
			if(m_spDrawHandler->hideAll(surfaceIndex) ||
					m_spDrawHandler->ghostAll(surfaceIndex) ||
					(isolatedIndex>=0 && surfaceIndex!=isolatedIndex))
			{
				surfaceIndex++;
				continue;
			}

			sp<DrawableI>& rspSubDrawable=rSubDrawableArray[nodeIndex];

			sp<SurfaceI> spSurface=rspSubDrawable;
			if(spSurface.isNull())
			{
				surfaceIndex++;
				continue;
			}

			//* TODO not inputIndex
			partitionSurface(inputIndex);

			try
			{
				const Real maxDistance=spNearestImpact.isValid()?
						spNearestImpact->distance(): Real(-1);
				sp<SurfaceI::ImpactI> spImpact;
				if(m_uvSpace)
				{
					const Vector2 uv(rayOrigin[0],rayOrigin[1]);
					spImpact=spSurface->nearestPoint(uv);
				}
				else
				{
					const Real distance=RaySphereIntersect<Real>::solve(
							spSurface->center(),spSurface->radius(),
							rayOrigin,rayDirection);

					//* NOTE don't build search tree until it might get hit
					//* TODO could sort the pre-checks nearest to furthest
					if(distance>=0.0 && (spNearestImpact.isNull() ||
							distance<maxDistance))
					{
						std::chrono::steady_clock::time_point tp0=
								std::chrono::steady_clock::now();

						spImpact=spSurface->rayImpact(
								rayOrigin,rayDirection,maxDistance);

						std::chrono::steady_clock::time_point tp1=
								std::chrono::steady_clock::now();
						const Real microseconds=1e6*
								std::chrono::duration<float>(tp1-tp0).count();
						feLog("rayImpact took %4.1f us\n",microseconds);
					}
				}
				if(spImpact.isValid())
				{
					if(spNearestImpact.isNull() ||
							spImpact->distance()<maxDistance)
					{
						nearestInputIndex=inputIndex;
						nearestSurfaceIndex=surfaceIndex;
						spNearestSurface=spSurface;
						spNearestImpact=spImpact;

//						nodeName=(nodeCount>1)? spSurface->name(): String();
//						nodeName=spSurface->name();
						nodeName=m_spDrawHandler->nodeName(surfaceIndex);
					}
				}
			}
			catch(...)
			{
			}

			surfaceIndex++;
		}
	}

	const String inputName=inputNameByIndex(nearestInputIndex);

	sp<SurfaceAccessibleI> spNearestAccessible;
	access(spNearestAccessible,inputName,e_quiet);

	if(scanning)
	{
		if(spNearestImpact.isValid())
		{
			const Real pickRadius=uvSpace()? 0.1:
					0.1*spNearestSurface->radius();

			m_spDrawHandler->setPickingImpact(spNearestImpact);
			m_spDrawHandler->setPickingSurfaceIndex(nearestSurfaceIndex);
			m_spDrawHandler->setPickingAccessible(spNearestAccessible);
			m_spDrawHandler->setPickingNodeName(nodeName);
			m_spDrawHandler->setPickPoint(
					spNearestImpact->intersection(),pickRadius);
		}
		else
		{
			m_spDrawHandler->setPickingAccessible(sp<SurfaceAccessibleI>(NULL));
		}
		return;
	}

	m_spDrawHandler->setPickingAccessible(sp<SurfaceAccessibleI>(NULL));

	if(spNearestSurface!=m_spDrawHandler->impactSurface())
	{
		m_spDrawHandler->unpick();
		m_spDrawHandler->setSurfaceImpact(sp<SurfaceI::ImpactI>(NULL));
	}
	else
	{
		m_spDrawHandler->setSurfaceImpact(spNearestImpact);
	}

	m_spDrawHandler->setImpactAccessible(spNearestAccessible);
	m_spDrawHandler->setImpactSurface(spNearestSurface);
	m_spDrawHandler->setImpactNodeName(nodeName);
	m_spDrawHandler->setImpactInputName(inputName);
	m_spDrawHandler->setImpactSurfaceIndex(nearestSurfaceIndex);
}

void SurfaceViewerOp::partitionSurface(I32 a_surfaceIndex)
{
	sp<SurfaceI> spSurface;
	if(a_surfaceIndex>=0)
	{
		spSurface=drawable(a_surfaceIndex);
	}
	if(spSurface.isNull())
	{
		return;
	}

	if(!spSurface->partitionCount())
	{
		const String inputName=inputNameByIndex(a_surfaceIndex);

		sp<SurfaceAccessibleI> spSurfaceAccessible;
		access(spSurfaceAccessible,inputName,e_quiet);

		const String partAttr=findPartitionAttribute(spSurfaceAccessible,"");
		if(!partAttr.empty())
		{
			spSurface->partitionWith(partAttr);
			spSurface->setPartitionFilter(".*");
		}
	}
}

String SurfaceViewerOp::inputNameByIndex(I32 a_inputIndex)
{
	String inputName;
	if(a_inputIndex==0)
	{
		inputName="Input Surface";
	}
	else if(a_inputIndex>0)
	{
		inputName.sPrintf("Input Surface %d",a_inputIndex+1);
	}

	return inputName;
}
