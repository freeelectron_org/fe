/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ironworks_ChainOp_h__
#define __ironworks_ChainOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Instantiate links along a curve

	@ingroup ironworks

	Based on the Siggraph 2015 Talk 'Rigid Link Chains in "Kung Fu Panda 3"'
	in the 20150810 3:45pm session "Link and Locks".
	Specifically reimplemented with permission.

	http://s2015.siggraph.org/attendees/talks/sessions/links-and-locks
*//***************************************************************************/
class FE_DL_EXPORT ChainOp:
	public OperatorSurfaceCommon,
	public Initialize<ChainOp>
{
	public:
				ChainOp(void):
					m_frame(-1),
					m_clearNextOutput(TRUE)									{}
virtual			~ChainOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		void	calcBounds(I32 a_fragmentIndex,SpatialVector& a_center,
					SpatialVector& a_innermin,SpatialVector& a_innermax,
					SpatialVector& a_outermin,SpatialVector& a_outermax);

		void	scanInputLinks(void);
		void	removeExcessLinks(void);
		void	addLink(sp<SurfaceAccessibleI> a_spLink,
					const SpatialTransform& a_transform);

		sp<SurfaceAccessibleI>	m_spResultAccessible;
		sp<DrawI>				m_spOutputDraw;

		sp<SurfaceAccessibleI>	m_spLinkAccessible;
		sp<SurfaceAccessorI>	m_spLinkVertices;
		sp<SurfaceAccessorI>	m_spScanVertices;

		sp<SurfaceAccessorI>	m_spPrimitiveLinkName;
		sp<SurfaceAccessorI>	m_spPointLinkName;

	class LinkRef {
		public:
			void				log(char* a_label);

			Real				m_pivotBase;
			Real				m_pivotTip;

			Real				m_pivotGapBase;
			Real				m_pivotGapTip;

			Real				m_pivotYawBase;
			Real				m_pivotYawTip;

			U32					m_axisForward;

			// transformation to forward=2 side=0
			SpatialTransform	m_transform;
	};

	class LinkState {
		public:
								LinkState(void):
									m_priorSet(FALSE),
									m_currentSlippage(0.0,0.0,0.0),
									m_priorSlippage(0.0,0.0,0.0)
								{
									setIdentity(m_priorConversion);
									setIdentity(m_currentConversion);
								}

			BWORD				m_priorSet;
			SpatialVector		m_currentSlippage;
			SpatialVector		m_priorSlippage;
			SpatialTransform	m_priorConversion;
			SpatialTransform	m_currentConversion;
	};

		I32						m_frame;
		BWORD					m_clearNextOutput;
		I32						m_pointStart;
		I32						m_primitiveStart;
		I32						m_fileLinkCount;

		Array<LinkRef>		m_linkRef;
		Array<LinkState>	m_linkState;
		Array<Real>			m_gapCorrection;
		Array<Real>			m_spiralCorrection;

		Array<String>		m_flipTwistPatterns;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ironworks_ChainOp_h__ */

