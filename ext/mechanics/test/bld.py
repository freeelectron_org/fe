import sys
import os
forge = sys.modules["forge"]

def setup(module):
    return
    deplibs = forge.corelibs + [
        'fexSignalLib',
        'fexMathLib',
        'fexSolveDLLib',
        'fexWindowLib',
        'fexViewerLib',
        #'fexMechanicsLib',
        'fexSpatialDLLib',
        'fexDataToolLib' ]

    # TEMP
    deplibs += [ 'fexRayDLLib', 'fexMechanicsDLLib' ]

    tests = [ ]
    tests = [ 'xMechanics' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    plugin_path = os.path.join(module.modPath, 'plugin')

    forge.tests += [
        ("xWorld.exe",  "count=300 dataset=%s architecture=%s ground=%s" % (
            os.path.join(module.modPath, 'spider.rg'),
            os.path.join(module.modPath, 'architecture.rg'),
            os.path.join(module.modPath, '..','..','..','..','media','terrain','cliff.rg')),
            None,   None) ]



