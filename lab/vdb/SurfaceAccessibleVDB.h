/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleVDB_h__
#define __surface_SurfaceAccessibleVDB_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief VDB Surface Binding

	@ingroup vdb
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleVDB:
	public SurfaceAccessibleBase
{
	public:
								SurfaceAccessibleVDB(void);
virtual							~SurfaceAccessibleVDB(void);

								//* as SurfaceAccessibleI
virtual	void					reset(void);

virtual	BWORD					isBound(void)
								{	return bool(m_gridPtr); }

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,
										Creation a_create,Writable a_writable);

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create,Writable a_writable);

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

	private:
		openvdb::GridBase::ConstPtr		m_gridPtr;
		sp<SurfaceVDB>					m_spSurfaceVDB;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleVDB_h__ */
