/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_AnnotateI_h__
#define __architecture_AnnotateI_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT AnnotateI : virtual public HandlerI
{
	public:
virtual	void	attach(const String &attrname, Record &r_data)				= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_AnnotateI_h__ */

