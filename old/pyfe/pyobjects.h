/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __python_pyobjects_h__
#define __python_pyobjects_h__

namespace pyfe
{

class Master
{
	public:
		PyObject_HEAD
		Master(void);

		void	Init(void);
		void	Fini(void);

	public:
		fe::Master				*m_pMaster;
		fe::sp<fe::TypeMaster>	*m_pspTypeMaster;
		std::map<fe::sp<fe::BaseType>, fe::sp<BaseConvertor> >
								*m_pConvertors;
};

class Record
{
	public:
		PyObject_HEAD

		void		Init(const fe::Record	&record);
		void		Init(void);
		void		Fini(void);

		fe::Record	*getRecord(void);
	public:
		fe::Record	*m_pRecord;
};

class RecordArray
{
	public:
		PyObject_HEAD

		void	Init(fe::sp<fe::RecordArray>	recordarray);
		void	Init(void);
		void	Fini(void);

		fe::sp<fe::RecordArray>	*getRecordArray(void);

	public:
		fe::sp<fe::RecordArray>	*m_pspRecordArray;
};

class Instance
{
	public:
		PyObject_HEAD

		void	Init(void);
		void	Init(fe::Instance instance);
		void	Fini(void);

		fe::Instance	*getInstance(void);

	private:
		fe::Instance		*m_pInstance;
};

class Component
{
	public:
		PyObject_HEAD

void	Init(fe::sp<fe::Component>	spComponent);
		void	Init(void);
		void	Fini(void);

		fe::sp<fe::Component>	*getComponent(void);

	public:
		fe::sp<fe::Component>	*m_pspComponent;
};

} /* namespace */


#endif /* __python_pyobjects_h__ */

