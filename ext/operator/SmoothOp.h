/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SmoothOp_h__
#define __operator_SmoothOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Smooth based on neighbors

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SmoothOp:
	public OperatorSurfaceCommon,
	public Initialize<SmoothOp>
{
	public:
				SmoothOp(void)												{}
virtual			~SmoothOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		sp<DrawMode>			m_spDots;

								//* feedback dots
		Array<SpatialVector>	m_locationArray;
		Array<Color>			m_colorArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SmoothOp_h__ */
