import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexWindowLib" ]

    deplibs2 = deplibs[:]

    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs2 += [
                "fexDrawDLLib",
                "fexViewerDLLib" ]

    tests = []

    if 'window' in forge.modules_confirmed:
        tests += [  'xJoy' ]

    if 'viewer' in forge.modules_confirmed:
        tests += [  'xJoyWindow' ]

        forge.tests += [
            ("xJoyWindow.exe",      "10",                   None,       None) ]

    for t in tests:
        exe = module.Exe(t)

        if t == "xJoyWindow":
            forge.deps([t + "Exe"], deplibs2)
        else:
            forge.deps([t + "Exe"], deplibs)

