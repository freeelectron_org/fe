/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "network/network.pmh"
#include "math/math.h"

#include <thread>
#include <chrono>

#define	TEST_LOCAL_LISTENER		FALSE

using namespace fe;
using namespace fe::ext;

#if TEST_LOCAL_LISTENER
class MyHandler:
	virtual public HandlerI,
	CastableAs<MyHandler>
{
	public:
				MyHandler(void)
				{	setName("MyHandler"); }
		void	bind(sp<StateCatalog> a_spStateCatalog)
				{	m_spStateCatalog=a_spStateCatalog; }
virtual	void	handle(Record &signal)
				{
					sp<Scope> spScope=signal.layout()->scope();
					if(m_aFrameIndex.scope()!=spScope)
					{
						m_aFrameIndex.initialize(spScope,"frameIndex");
						m_aPayload.initialize(spScope,"payload");
					}

					I32 frame(0);
					m_spStateCatalog->getState<I32>("frame",frame);

					feLog("Signal: Scope %p"
							" frameIndex %d payload \"%s\" on frame %d\n",
							spScope.raw(),m_aFrameIndex(signal),
							m_aPayload(signal).c_str(),frame);
				}
	private:
		Accessor<int>		m_aFrameIndex;
		Accessor<String>	m_aPayload;
		sp<StateCatalog>	m_spStateCatalog;
};
#endif

int main(int argc,char** argv)
{
	String envCiBuild(FALSE);
	System::getEnvironmentVariable("FE_CI_BUILD", envCiBuild);
	const I32 ciBuild=envCiBuild.integer();
	feLog("FE_CI_BUILD %d\n",ciBuild);

	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<SingleMaster> spSingleMaster=SingleMaster::create();
			sp<Master> spMaster=spSingleMaster->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			String transport="tcp";
			if(argc>2)
			{
				transport=argv[2];
			}
			feLog("transport \"%s\"\n",transport.c_str());

			U16 port=7890;
			if(argc>3)
			{
				port=atoi(argv[3]);
			}
			feLog("port %d\n",port);

			sp<StateCatalog> spStateCatalog=
					spMaster->registry()->create(implementation);
			UNIT_TEST(spStateCatalog.isValid());
			if(!spStateCatalog.isValid())
			{
				feX(argv[0],"spStateCatalog invalid");
			}

			String configuration;
			if(transport=="shm")
			{
				configuration.sPrintf("/xSignalServer");
			}
			else if(transport=="ipc")
			{
				configuration.sPrintf("/tmp/feeds/0");
			}
			else
			{
				configuration.sPrintf("*:%d",port);
			}
			configuration.catf(" role=server ioPriority=high transport=%s",
					transport.c_str());
			result=spStateCatalog->configure(configuration);

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			sp<Scope> spScope=spRegistry->create("Scope");
			sp<Layout> spLayout = spScope->declare("SIGNAL");

			Accessor<int> frameIndex(spScope,"frameIndex");
			Accessor<String> payload(spScope,"payload");
			spLayout->populate(frameIndex);
			spLayout->populate(payload);

			sp<HandlerI> spSignalMessenger=
					spRegistry->create("*.SignalMessenger");
			sp<StateBindI> spStateBindI(spSignalMessenger);
			if(spStateBindI.isValid())
			{
				spStateBindI->bind(spStateCatalog);
				spStateBindI->setKey("test_signal");
			}

			sp<SignalerI> spSignalerI=spRegistry->create("SignalerI");
			spSignalerI->insert(spSignalMessenger,spLayout);

			Record signal=spScope->createRecord(spLayout);

			sp<Layout> spTestLayout=spScope->declare("test layout");
			spScope->support("count","integer");
			spScope->populate("test layout","count");
			Accessor<I32> count;
			count.setup(spScope,"count","integer");

#if TEST_LOCAL_LISTENER
			sp<SignalerI> spMessageSignaler=
					spRegistry->create("*.MessageSignaler");
			spStateCatalog->addListener(spMessageSignaler, "test_signal");
			sp<MyHandler> spMyHandler(new MyHandler);
			spMyHandler->bind(spStateCatalog);
			spMessageSignaler->insert(spMyHandler, sp<Layout>(NULL));
#endif

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				const I32 frameCount=1000;

				spStateCatalog->setState<bool>("running",true);
				spStateCatalog->flush();

				spStateCatalog->setState<I32>("disposable0",7);
				spStateCatalog->setState<String>("disposable0","id","0");
				spStateCatalog->setState<I32>("disposable1",13);
				spStateCatalog->setState<String>("disposable1","id","1");
				spStateCatalog->setState<I32>("disposable2",17);
				spStateCatalog->setState<String>("disposable2","id","2");
				spStateCatalog->flush();

				for(I32 frame=0;frame<frameCount;frame++)
				{
					if(!(frame%100))
					{
						feLog("frame %d/%d\n",frame,frameCount);
					}

					frameIndex(signal)=frame;
					payload(signal)="\"TesT\"";
					spSignalerI->signal(signal);

					Record record;
					record = spScope->createRecord(spTestLayout);
					count(record) = frame;

					sp<RecordGroup> spRG(new RecordGroup());
					spRG->add(record);
					spStateCatalog->setState<sp<RecordGroup>>("rg", spRG);

					spStateCatalog->removeState("disposable0","value");
					spStateCatalog->removeState("disposable2");

					spStateCatalog->setState<I32>("frame",frame);
					spStateCatalog->flush();

					I32 messageIndex=0;
					while(TRUE)
					{
						String message;
						result=spStateCatalog->nextMessage("request",message);
						if(failure(result))
						{
							break;
						}
						feLog("message %d: \"%s\"\n",
								messageIndex++,message.c_str());
					}

					if(ciBuild)
					{
						std::this_thread::sleep_for(
								std::chrono::milliseconds(10));
					}
//					else if(transport=="shm")
//					{
//						std::this_thread::sleep_for(
//								std::chrono::microseconds(10));
//					}
					else
					{
						std::this_thread::sleep_for(
								std::chrono::milliseconds(1));
					}
				}

				spStateCatalog->setState<bool>("running",false);
				spStateCatalog->flush();

				spStateCatalog->stop();

				feLog("End State:\n");
				spStateCatalog->catalogDump();
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
