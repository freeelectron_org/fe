/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <world/world.pmh>

#include "platform/dlCore.cc"

#include "WorldPipeline.h"
#include "WorldWindow.h"
#include "SimpleGrid.h"
#include "DrawAtoms.h"
#include "DrawPairs.h"
#include "DrawTransform.h"
#include "FormDrag.h"
#include "Gravity.h"
#include "ClientPipeline.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
	list.append(new String("fexDataToolDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertData(spMaster->typeMaster());
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<WorldPipeline>("PipelineI.World.fe");
	pLibrary->add<WorldWindow>("ApplicationWindowI.World.fe");
	pLibrary->add<SimpleGrid>("HandlerI.SimpleGrid.fe");
	pLibrary->add<DrawAtoms>("HandlerI.DrawAtoms.fe");
	pLibrary->add<DrawTransform>("HandlerI.DrawTransform.fe");
	pLibrary->add<DrawPairs>("HandlerI.DrawPairs.fe");
	pLibrary->add<FormDrag>("HandlerI.FormDrag.fe");
	pLibrary->add<Gravity>("HandlerI.Gravity.fe");
	pLibrary->add<ClientPipeline>("PipelineI.Client.World.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
