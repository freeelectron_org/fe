import sys
import os
import re
import shutil
forge = sys.modules["forge"]

re_lib = re.compile(r'.*\.(dll|so|lib|a)')

def setup_fasp(module):
    if forge.fe_os == "FE_LINUX":
        forge.linkmap['fasp'] = ' -lfasp'
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        forge.linkmap['fasp'] = 'fasp.lib' # TODO verify

def setup(module):
    srcList =   [   "math.pmh",
                    "Box",
                    "DAGNode",
                    "Mass",
                    "Matrix3x4",
                    "Quaternion",
                    "algorithms",
                    "assertMath"
                ]

    deplibs = forge.basiclibs[:]

    # just laziness; Win32 could be DLL if you FE_MATH_PORT'd every function
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        lib = module.Lib("feMath",srcList)
        forge.deps( ["feMathLib"], deplibs )
    elif forge.fe_os == 'FE_OSX':
        lib = module.DLL("feMath",srcList)
        forge.deps( ["feMathLib"], deplibs )
    else:
        lib = module.DLL("feMath",srcList)

    srcList += [ "mathDL" ]
    deplibs += [ "fePluginLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "feImportMemoryLib" ]

    dll = module.DLL( "feMathDL", srcList)
    forge.deps( ["feMathDLLib"], deplibs )

    module.Module('test')

    #setup_fasp(module)
