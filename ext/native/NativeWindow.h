/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __native_NativeWindow_h__
#define __native_NativeWindow_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Implementation of a window using the raw native system

	@ingroup native
*//***************************************************************************/
class FE_DL_EXPORT NativeWindow:
		public ChainSignaler,
		public Initialize<NativeWindow>,
		virtual public WindowI,
		virtual public HandlerI
{
	public:
							NativeWindow(void);
virtual						~NativeWindow(void);
		void				initialize(void);
		void				startup(void);

							//* As WindowI
virtual
const	Box2i&				geometry(void)			{ return m_box; }
virtual	void				setPosition(I32 x,I32 y)
							{	set(m_box,x,y); }
virtual	void				setSize(U32 x,U32 y)
							{	resize(m_box,I32(x),I32(y)); }
virtual	void				setTitle(const String &title);

virtual	Result				open(const String title);
virtual	Result				close(void);

virtual void				makeCurrent(void);
virtual void				releaseCurrent(void);
virtual	void				setBackground(const Color& color);
virtual	const Color&		background(void) const;
virtual void				clear(void);
virtual void				swapBuffers(void);

virtual sp<Component>		getEventContextI(void)	{ return m_spEventContextI;}

							//* As HandlerI
virtual void				handleBind(sp<SignalerI> spSignalerI,
									sp<Layout> spLayout);
virtual	void				handle(Record &record);

							//* As NativeWindow

							/** @brief Get the native format window handle

								@internal */
		FE_WINDOW_HANDLE*	windowHandle(void)		{ return &m_windowHandle; }
							/** @brief Set the native format window handle

								@internal */
		void				setWindowHandle(const FE_WINDOW_HANDLE &set)
							{	memcpy(&m_windowHandle,&set,
										sizeof(FE_WINDOW_HANDLE));}

							/// @internal
		BWORD				created(void)			{ return m_created; }
							/// @internal
		BWORD				mapped(void)			{ return m_mapped; }

							/// @internal
		void				setPointerMotion(BWORD active);

	private:
							/// @internal
		void				setCreated(BWORD set)	{ m_created=set; }
							/// @internal
		void				setMapped(BWORD set)	{ m_mapped=set; }

		Result				nativeInitialize(void);
		Result				nativeStartup(void);
		Result				nativeShutdown(void);

		void				handleEvent(Record &record);

		Accessor<FE_WINDOW_HANDLE*>	m_aWindowHandle;

		sp<EventContextI>	m_spEventContextI;
		FE_WINDOW_HANDLE	m_windowHandle;
		Box2i				m_box;
		BWORD				m_started;
		BWORD				m_created;
		BWORD				m_mapped;
		BWORD				m_lockThreads;
		WindowEvent			m_event;
		Color				m_background;
		sp<Layout>			m_eventLayout;
#if FE_2DGL==FE_2D_X_GFX
		GLXContext			m_glxContext;
#elif FE_2DGL==FE_2D_GDI
		void				setDCPixelFormat(HDC hdc);
		HGLRC				m_hglrc;
#endif
};

} /* namespace ext */
} /* namespace fe */

#endif /* __native_NativeWindow_h__ */
