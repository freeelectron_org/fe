/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void DrawConstruct::handle(Record &record)
{
	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color darkgreen(0.0f,0.3f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,1.0f,1.0f);
	const Color orange(1.0f,0.5f,0.0f,1.0f);
	const Color blue(0.5f,0.5f,1.0f,1.0f);
	const Color purple(0.5f,0.0f,1.0f,1.0f);
	const Color darkpurple(0.3f,0.0f,0.6f,0.5f);
	const Color cyan(0.0f,1.0f,1.0f,0.4f);
	const Color magenta(1.0f,0.0f,1.0f,1.0f);

	m_theaterRV.bind(record);

	sp<DrawI> spDrawI=m_theaterRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_foundryRAV.bind(spRA);

		if(!m_foundryRAV.recordView().prototype.check(spRA))
		{
			continue;
		}

		for(Foundry& foundryRV: m_foundryRAV)
		{
			sp<RecordGroup> spPrototype=foundryRV.prototype();
			if(!spPrototype.isValid())
			{
				continue;
			}

			sp<RecordGroup> spSelection=foundryRV.selection();

			for(U32 pass=0;pass<2;pass++)
			{
				sp<RecordGroup> spGroup=pass? spPrototype: spSelection;
				const Color lineColor=pass? magenta: orange;
				for(RecordGroup::iterator it2=spGroup->begin();
						it2!=spGroup->end();it2++)
				{
					sp<RecordArray> spRA2= *it2;
					if(!foundryRV.pattern.check(spRA2))
					{
						continue;
					}

					m_matterRAV.bind(spRA2);
					for(Matter& matterRV: m_matterRAV)
					{
						if(pass)
						{
							Record pattern=matterRV.pattern();
							if(!pattern.isValid())
							{
								continue;
							}

							m_matterRV2.bind(pattern);

							if(!m_matterRV2.location.check())
							{
								continue;
							}
						}
						else
						{
							m_matterRV2.bind(matterRV.record());
						}

						SpatialVector vertex[2];
						vertex[0]=foundryRV.location()+
								SpatialVector(0.0f,0.1f,0.0f);
						vertex[1]=m_matterRV2.location()+
								SpatialVector(0.0f,0.1f,0.0f);

						//* draw line to product
						spDrawI->drawLines(vertex,NULL,2,DrawI::e_strip,
								false,&lineColor);

						F32 distance=magnitude(vertex[1]-vertex[0]);
						vertex[0]+=(vertex[1]-vertex[0])*
								((foundryRV.radius()+
								0.5f*(distance-m_matterRV2.radius()
								-foundryRV.radius()))/distance);

						char text[32];
						sprintf(text," %.2f %.2f",
								m_matterRV2.metal.safeGet(),
								m_matterRV2.gem.safeGet());
						spDrawI->drawAlignedText(vertex[0],text,yellow);
					}
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
