import sys
import os
forge = sys.modules["forge"]

def setup(module):


    srcList = [ "array.cpp",
                "box.cpp",
                "capsule.cpp",
                "collision_cylinder_box.cpp",
                "collision_cylinder_plane.cpp",
                "collision_cylinder_sphere.cpp",
                "collision_cylinder_trimesh.cpp",
                "collision_kernel.cpp",
                "collision_quadtreespace.cpp",
                "collision_space.cpp",
                "collision_transform.cpp",
                "collision_trimesh_box.cpp",
                "collision_trimesh_ccylinder.cpp",
                "collision_trimesh_distance.cpp",
                "collision_trimesh_gimpact.cpp",
                "collision_trimesh_opcode.cpp",
                "collision_trimesh_plane.cpp",
                "collision_trimesh_ray.cpp",
                "collision_trimesh_sphere.cpp",
                "collision_trimesh_trimesh.cpp",
                "collision_trimesh_trimesh_new.cpp",
                "collision_util.cpp",
                "convex.cpp",
                "cylinder.cpp",
                "error.cpp",
                "export-dif.cpp",
                "heightfield.cpp",
                "joint.cpp",
                "lcp.cpp",
                "mass.cpp",
                "mat.cpp",
                "matrix.cpp",
                "memory.cpp",
                "misc.cpp",
                "obstack.cpp",
                "ode.cpp",
                "odemath.cpp",
                "plane.cpp",
                "quickstep.cpp",
                "ray.cpp",
                "rotation.cpp",
                "sphere.cpp",
                "step.cpp",
                "stepfast.cpp",
                "testing.cpp",
                "timer.cpp",
                "util.cpp",
                "fastdot.c",
                "fastldlt.c",
                "fastlsolve.c",
                "fastltsolve.c" ]

    lib = module.Lib( "ode", srcList )

    module.includemap = { 'ode' : os.path.join(module.modPath,'..','..','include'),
                        'odeopcode' : os.path.join(module.modPath,'..','..','OPCODE') }

