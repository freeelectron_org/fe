/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

OrthoViewer::OrthoViewer(void)
{
}

OrthoViewer::~OrthoViewer(void)
{
}

void OrthoViewer::initialize(void)
{
	cfg<Real>("zoom") = 0.0;
	cfg<Vector2>("center")[0] = 0.0;
	cfg<Vector2>("center")[1] = 0.0;

	m_view = registry()->create("ViewI");
}

void OrthoViewer::bind(fe::sp<WindowI> spWindowI)
{
	m_spWindowI=spWindowI;
}

void OrthoViewer::handle(fe::Record& r_sig)
{
	if(!m_spWindowI.isValid())
	{
		feX("Perspective::handle",
			"not bound to a valid window");
	}
	Record r_windata = m_asSignal.winData(r_sig, "OrthoViewer");

	Real zoom = 1.0;
	Vector2 center;
	center = cfg<Vector2>("center");
	Record r_invalid;
	if(cfg<Real>("zoom") == 0.0)
	{
		if(m_asOrtho.zoom.check(r_windata))
		{
			zoom = m_asOrtho.zoom(r_windata);
		}
		if(m_asOrtho.center.check(r_windata))
		{
			center = m_asOrtho.center(r_windata);
		}
	}
	else
	{
		zoom = cfg<Real>("zoom");
	}

	fe::Box2i rect(Vector2i(0, 0));
	resize(rect, m_spWindowI->geometry().size());

	sp<DrawI> spDraw = m_asWindata.drawI(m_asSignal.winData(r_sig));

	m_view->setWindow(m_spWindowI);
	m_view->setViewport(rect);

	sp<CameraI> spCamera=m_view->camera();
	spCamera->setOrtho(zoom, center);
	m_view->use(ViewI::e_ortho);
	spDraw->setView(m_view);
}

void OrthoViewer::handleBind(fe::sp<SignalerI> spSignalerI,
		fe::sp<Layout> spLayout)
{
	sp<Scope> spScope = spLayout->scope();
	m_asOrtho.bind(spScope);
	m_asSignal.bind(spScope);
	m_asWindata.bind(spScope);
}


} /* namespace ext */
} /* namespace fe */

