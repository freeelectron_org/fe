/*	Copyright (C) 2003-2014 Free Electron Organization
	Any use of this software requires a license. */

/** @file */

#include "LinearBendForce.h"

namespace fe
{
namespace ext
{

LinearBendForce::LinearBendForce(sp<SemiImplicit> a_integrator)
{
	m_integrator = a_integrator;
}

LinearBendForce::~LinearBendForce(void)
{
}

void LinearBendForce::clear(void)
{
}

void LinearBendForce::compile(sp<RecordGroup> rg_input, std::vector<SemiImplicit::Particle> &a_particles, SemiImplicit::CompileMatrix &a_compileMatrix)
{
	AsBend				asLinearBend;
	AsForcePoint		asForcePoint;
	AsSolverParticle	asSolverParticle;

	m_elements.clear();
	unsigned int n = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearBend.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());

		if(asLinearBend.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				WeakRecord &r_A = asLinearBend.a(spRA,i);
				WeakRecord &r_B = asLinearBend.b(spRA,i);
				WeakRecord &r_C = asLinearBend.c(spRA,i);
				WeakRecord &r_D = asLinearBend.d(spRA,i);

				if(!asForcePoint.check(r_A)) { continue; }
				if(!asForcePoint.check(r_B)) { continue; }
				if(!asForcePoint.check(r_C)) { continue; }
				if(!asForcePoint.check(r_D)) { continue; }

				n += 1;
			}
		}
	}


	m_elements.resize(n);
	unsigned int ii = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearBend.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());
		asSolverParticle.bind(spRA->layout()->scope());

		if(asLinearBend.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Element &element = m_elements[ii];
				WeakRecord &r_A = asLinearBend.a(spRA,i);
				WeakRecord &r_B = asLinearBend.b(spRA,i);
				WeakRecord &r_C = asLinearBend.c(spRA,i);
				WeakRecord &r_D = asLinearBend.d(spRA,i);

				if(!asForcePoint.check(r_A)) { continue; }
				if(!asForcePoint.check(r_B)) { continue; }
				if(!asForcePoint.check(r_C)) { continue; }
				if(!asForcePoint.check(r_D)) { continue; }

				ii++;

				t_solve_real stiffness = asLinearBend.stiffness(spRA,i);
				t_solve_real damping = asLinearBend.damping(spRA,i);

				t_solve_v3 P_BA = asForcePoint.location(r_B)
					- asForcePoint.location(r_A);
				t_solve_v3 P_DC = asForcePoint.location(r_D)
					- asForcePoint.location(r_C);
				t_solve_v3 P_AC = asForcePoint.location(r_A)
					- asForcePoint.location(r_C);
				t_solve_v3 P_AD = asForcePoint.location(r_A)
					- asForcePoint.location(r_D);
				t_solve_v3 P_BC = asForcePoint.location(r_B)
					- asForcePoint.location(r_C);
				t_solve_v3 P_BD = asForcePoint.location(r_B)
					- asForcePoint.location(r_D);
				t_solve_v3 P_CB = asForcePoint.location(r_C)
					- asForcePoint.location(r_B);
				t_solve_v3 P_CA = asForcePoint.location(r_C)
					- asForcePoint.location(r_A);
				t_solve_v3 P_DB = asForcePoint.location(r_D)
					- asForcePoint.location(r_B);
				t_solve_v3 P_DA = asForcePoint.location(r_D)
					- asForcePoint.location(r_A);

				t_solve_v3 N_A = cross(P_AC, P_AD);
				t_solve_v3 N_B = cross(P_BD, P_BC);
				t_solve_v3 N_C = cross(P_CB, P_CA);
				t_solve_v3 N_D = cross(P_DA, P_DB);

				t_solve_real NNA = magnitude(N_A);
				t_solve_real NNB = magnitude(N_B);
				t_solve_real NNC = magnitude(N_C);
				t_solve_real NND = magnitude(N_D);

				bool degenerate_shut = false;
				if(isZero(NNC) && isZero(NND))
				{
					degenerate_shut = true;
				}

				element.m_alpha_A = NNB / (NNA + NNB);
				element.m_alpha_B = NNA / (NNA + NNB);
				if(!degenerate_shut)
				{
					element.m_alpha_C =-NND / (NNC + NND);
					element.m_alpha_D =-NNC / (NNC + NND);
				}

				t_solve_v3 Naxis = unit(P_DC);
				t_solve_real h_AB = magnitude(P_BA - (dot(P_BA,Naxis) * Naxis));
				t_solve_real h_A = element.m_alpha_B * h_AB;
				t_solve_real h_B = element.m_alpha_A * h_AB;
				t_solve_real l = magnitude(P_DC);


				element.m_lambda = (2.0 * (h_AB) * l * stiffness) /
					(3.0 * h_A * h_A * h_B * h_B);

				element.m_gamma = (2.0 * (h_AB) * l * damping) /
					(3.0 * h_A * h_A * h_B * h_B);

//fe_fprintf(stderr, "AJW m_lambda %10.10g h_AB %10.10g h_A %10.10g h_B %10.10g l %10.10g stiffness %10.10g\n", element.m_lambda, h_AB, h_A, h_B, l, stiffness);

				// calculate length (rest R)
				SpatialVector rest_R_s;
				if(!degenerate_shut)
				{
					rest_R_s =
							element.m_alpha_A * asForcePoint.location(r_A)
						+	element.m_alpha_B * asForcePoint.location(r_B)
						+	element.m_alpha_C * asForcePoint.location(r_C)
						+	element.m_alpha_D * asForcePoint.location(r_D);
					element.m_length = magnitude(rest_R_s);
				}

				t_solve_v3 rest_R;
				copy(rest_R, rest_R_s);

#if 0
				// find base frame
				t_solve_v3 direction = P_DC;
				t_solve_v3 up = P_BC;
				normalize(direction);
				normalize(up);
				t_solve_v3 side = cross(up, direction);
				normalize(side);
				up = cross(direction, side);
				normalize(up);
				SpatialTransform xform;
				xform.direction() = direction;
				xform.left() = side;
				xform.up() = up;

				// inverse frame
				SpatialTransform inv_xform;
				invert(inv_xform, xform);

				// project BC and AC into x==0 plane of frame
				t_solve_v3 BC_projected;
				t_solve_v3 NBC_projected;
				rotateVector(inv_xform, P_BC, BC_projected);
				t_solve_real B_len = BC_projected[0];
				BC_projected[0] = 0.0;
				t_solve_real BC_arm = magnitude(BC_projected);
				NBC_projected = BC_projected;
				normalize(NBC_projected);

				t_solve_v3 AC_projected;
				t_solve_v3 NAC_projected;
				rotateVector(inv_xform, P_AC, AC_projected);
				t_solve_real A_len = AC_projected[0];
				AC_projected[0] = 0.0;
				t_solve_real AC_arm = magnitude(AC_projected);
				NAC_projected = AC_projected;
				normalize(NAC_projected);

				t_solve_real LAB = (NNA+NNB)/l;
#endif

				t_solve_matrix common;
				setIdentity(common);
				common *= -element.m_lambda;

				element.m_dfdxAA = common * (element.m_alpha_A * element.m_alpha_A);
				element.m_dfdxAB = common * (element.m_alpha_A * element.m_alpha_B);
				element.m_dfdxAC = common * (element.m_alpha_A * element.m_alpha_C);
				element.m_dfdxAD = common * (element.m_alpha_A * element.m_alpha_D);
				element.m_dfdxBB = common * (element.m_alpha_B * element.m_alpha_B);
				element.m_dfdxBC = common * (element.m_alpha_B * element.m_alpha_C);
				element.m_dfdxBD = common * (element.m_alpha_B * element.m_alpha_D);
				element.m_dfdxCC = common * (element.m_alpha_C * element.m_alpha_C);
				element.m_dfdxCD = common * (element.m_alpha_C * element.m_alpha_D);
				element.m_dfdxDD = common * (element.m_alpha_D * element.m_alpha_D);

				setIdentity(common);
				common *= -element.m_gamma;

				element.m_dfdvAA = common * (element.m_alpha_A * element.m_alpha_A);
				element.m_dfdvAB = common * (element.m_alpha_A * element.m_alpha_B);
				element.m_dfdvAC = common * (element.m_alpha_A * element.m_alpha_C);
				element.m_dfdvAD = common * (element.m_alpha_A * element.m_alpha_D);
				element.m_dfdvBB = common * (element.m_alpha_B * element.m_alpha_B);
				element.m_dfdvBC = common * (element.m_alpha_B * element.m_alpha_C);
				element.m_dfdvBD = common * (element.m_alpha_B * element.m_alpha_D);
				element.m_dfdvCC = common * (element.m_alpha_C * element.m_alpha_C);
				element.m_dfdvCD = common * (element.m_alpha_C * element.m_alpha_D);
				element.m_dfdvDD = common * (element.m_alpha_D * element.m_alpha_D);


				int i_A = asSolverParticle.index(r_A);
				int i_B = asSolverParticle.index(r_B);
				int i_C = asSolverParticle.index(r_C);
				int i_D = asSolverParticle.index(r_D);

				element.m_location[0] = &(a_particles[i_A].m_location);
				element.m_location[1] = &(a_particles[i_B].m_location);
				element.m_location[2] = &(a_particles[i_C].m_location);
				element.m_location[3] = &(a_particles[i_D].m_location);

				element.m_velocity[0] = &(a_particles[i_A].m_velocity);
				element.m_velocity[1] = &(a_particles[i_B].m_velocity);
				element.m_velocity[2] = &(a_particles[i_C].m_velocity);
				element.m_velocity[3] = &(a_particles[i_D].m_velocity);

				element.m_force[0] = &(a_particles[i_A].m_force);
				element.m_force[1] = &(a_particles[i_B].m_force);
				element.m_force[2] = &(a_particles[i_C].m_force);
				element.m_force[3] = &(a_particles[i_D].m_force);

				a_compileMatrix.entry(i_A, i_A).first
					.push_back(&(element.m_p_dfdxAA));
				a_compileMatrix.entry(i_B, i_B).first
					.push_back(&(element.m_p_dfdxBB));
				a_compileMatrix.entry(i_C, i_C).first
					.push_back(&(element.m_p_dfdxCC));
				a_compileMatrix.entry(i_D, i_D).first
					.push_back(&(element.m_p_dfdxDD));

				if(i_A < i_B)
				{
					a_compileMatrix.entry(i_A, i_B).first
						.push_back(&(element.m_p_dfdxAB));
				}
				else
				{
					a_compileMatrix.entry(i_B, i_A).first
						.push_back(&(element.m_p_dfdxAB));
				}

				if(i_A < i_C)
				{
					a_compileMatrix.entry(i_A, i_C).first
						.push_back(&(element.m_p_dfdxAC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_A).first
						.push_back(&(element.m_p_dfdxAC));
				}

				if(i_A < i_D)
				{
					a_compileMatrix.entry(i_A, i_D).first
						.push_back(&(element.m_p_dfdxAD));
				}
				else
				{
					a_compileMatrix.entry(i_D, i_A).first
						.push_back(&(element.m_p_dfdxAD));
				}

				if(i_B < i_C)
				{
					a_compileMatrix.entry(i_B, i_C).first
						.push_back(&(element.m_p_dfdxBC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_B).first
						.push_back(&(element.m_p_dfdxBC));
				}

				if(i_B < i_D)
				{
					a_compileMatrix.entry(i_B, i_D).first
						.push_back(&(element.m_p_dfdxBD));
				}
				else
				{
					a_compileMatrix.entry(i_D, i_B).first
						.push_back(&(element.m_p_dfdxBD));
				}

				if(i_C < i_D)
				{
					a_compileMatrix.entry(i_C, i_D).first
						.push_back(&(element.m_p_dfdxCD));
				}
				else
				{
					a_compileMatrix.entry(i_D, i_C).first
						.push_back(&(element.m_p_dfdxCD));
				}


				a_compileMatrix.entry(i_A, i_A).second
					.push_back(&(element.m_p_dfdvAA));
				a_compileMatrix.entry(i_B, i_B).second
					.push_back(&(element.m_p_dfdvBB));
				a_compileMatrix.entry(i_C, i_C).second
					.push_back(&(element.m_p_dfdvCC));
				a_compileMatrix.entry(i_D, i_D).second
					.push_back(&(element.m_p_dfdvDD));

				if(i_A < i_B)
				{
					a_compileMatrix.entry(i_A, i_B).second
						.push_back(&(element.m_p_dfdvAB));
				}
				else
				{
					a_compileMatrix.entry(i_B, i_A).second
						.push_back(&(element.m_p_dfdvAB));
				}

				if(i_A < i_C)
				{
					a_compileMatrix.entry(i_A, i_C).second
						.push_back(&(element.m_p_dfdvAC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_A).second
						.push_back(&(element.m_p_dfdvAC));
				}

				if(i_A < i_D)
				{
					a_compileMatrix.entry(i_A, i_D).second
						.push_back(&(element.m_p_dfdvAD));
				}
				else
				{
					a_compileMatrix.entry(i_D, i_A).second
						.push_back(&(element.m_p_dfdvAD));
				}

				if(i_B < i_C)
				{
					a_compileMatrix.entry(i_B, i_C).second
						.push_back(&(element.m_p_dfdvBC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_B).second
						.push_back(&(element.m_p_dfdvBC));
				}

				if(i_B < i_D)
				{
					a_compileMatrix.entry(i_B, i_D).second
						.push_back(&(element.m_p_dfdvBD));
				}
				else
				{
					a_compileMatrix.entry(i_D, i_B).second
						.push_back(&(element.m_p_dfdvBD));
				}

				if(i_C < i_D)
				{
					a_compileMatrix.entry(i_C, i_D).second
						.push_back(&(element.m_p_dfdvCD));
				}
				else
				{
					a_compileMatrix.entry(i_D, i_C).second
						.push_back(&(element.m_p_dfdvCD));
				}

			}
		}
	}
}

bool LinearBendForce::validate(void)
{
	return true;
}

void LinearBendForce::accumulate(void)
{
	unsigned int n = (unsigned int)m_elements.size();
	for(unsigned int i = 0; i < n; i++)
	{
		Element &element = m_elements[i];

		// calculate R
		t_solve_v3 R =	element.m_alpha_A * *(element.m_location[0])
			+				element.m_alpha_B * *(element.m_location[1])
			+				element.m_alpha_C * *(element.m_location[2])
			+				element.m_alpha_D * *(element.m_location[3]);

		if(isZero(R))
		{
			continue;
		}

#if 0
		t_solve_v3 AD = *(element.m_location[0]) - *(element.m_location[3]);
		t_solve_v3 BD = *(element.m_location[1]) - *(element.m_location[3]);
		t_solve_v3 CD = *(element.m_location[2]) - *(element.m_location[3]);

		t_solve_v3 NA = cross(CD, AD);
		normalize(NA);
		t_solve_v3 NB = cross(BD, CD);
		normalize(NB);

#if 0
fe_fprintf(stderr, "NA %f NB %f\n", magnitude(NA), magnitude(NB));
fe_fprintf(stderr, "NA %f %f %f\n", NA[0], NA[1], NA[2]);
fe_fprintf(stderr, "NB %f %f %f\n", NB[0], NB[1], NB[2]);
#endif

		t_solve_v3 RP = NA + NB;
		normalize(RP);

		t_solve_real s = dot(R, RP);
		R = s * R;
#endif

		// adjust R by rest R
		t_solve_real r = magnitude(R);

		r -= element.m_length;

		if(isZero(R))
		{
			continue;
		}

		normalize(R);
		t_solve_v3 Rn = R;

//fe_fprintf(stderr, "l %g L %g Rn %g %g %g C %g D %g ang %f\n", element.m_length, element.m_lambda, Rn[0], Rn[1], Rn[2], element.m_alpha_C, element.m_alpha_D, 180.0/fe::pi*atan(Rn[0]/Rn[2]));
		R *= r;


		// calculate dx forces
		*(element.m_force[0]) -= element.m_lambda * element.m_alpha_A * R;
		*(element.m_force[1]) -= element.m_lambda * element.m_alpha_B * R;
		*(element.m_force[2]) -= element.m_lambda * element.m_alpha_C * R;
		*(element.m_force[3]) -= element.m_lambda * element.m_alpha_D * R;

		// calculate Rp
		t_solve_v3 Rp =	element.m_alpha_A * *(element.m_velocity[0])
			+				element.m_alpha_B * *(element.m_velocity[1])
			+				element.m_alpha_C * *(element.m_velocity[2])
			+				element.m_alpha_D * *(element.m_velocity[3]);
		Rp = dot(Rp, Rn) * Rn;

#if 1
		// calculate dv forces
		*(element.m_force[0]) -= element.m_gamma * element.m_alpha_A * Rp;
		*(element.m_force[1]) -= element.m_gamma * element.m_alpha_B * Rp;
		*(element.m_force[2]) -= element.m_gamma * element.m_alpha_C * Rp;
		*(element.m_force[3]) -= element.m_gamma * element.m_alpha_D * Rp;

		// the Jacobians
		add(*(element.m_p_dfdxAA), *(element.m_p_dfdxAA), element.m_dfdxAA);
		add(*(element.m_p_dfdxBB), *(element.m_p_dfdxBB), element.m_dfdxBB);
		add(*(element.m_p_dfdxCC), *(element.m_p_dfdxCC), element.m_dfdxCC);
		add(*(element.m_p_dfdxDD), *(element.m_p_dfdxDD), element.m_dfdxDD);
		add(*(element.m_p_dfdxAB), *(element.m_p_dfdxAB), element.m_dfdxAB);
		add(*(element.m_p_dfdxAC), *(element.m_p_dfdxAC), element.m_dfdxAC);
		add(*(element.m_p_dfdxAD), *(element.m_p_dfdxAD), element.m_dfdxAD);
		add(*(element.m_p_dfdxBC), *(element.m_p_dfdxBC), element.m_dfdxBC);
		add(*(element.m_p_dfdxBD), *(element.m_p_dfdxBD), element.m_dfdxBD);
		add(*(element.m_p_dfdxCD), *(element.m_p_dfdxCD), element.m_dfdxCD);

		add(*(element.m_p_dfdvAA), *(element.m_p_dfdvAA), element.m_dfdvAA);
		add(*(element.m_p_dfdvBB), *(element.m_p_dfdvBB), element.m_dfdvBB);
		add(*(element.m_p_dfdvCC), *(element.m_p_dfdvCC), element.m_dfdvCC);
		add(*(element.m_p_dfdvDD), *(element.m_p_dfdvDD), element.m_dfdvDD);
		add(*(element.m_p_dfdvAB), *(element.m_p_dfdvAB), element.m_dfdvAB);
		add(*(element.m_p_dfdvAC), *(element.m_p_dfdvAC), element.m_dfdvAC);
		add(*(element.m_p_dfdvAD), *(element.m_p_dfdvAD), element.m_dfdvAD);
		add(*(element.m_p_dfdvBC), *(element.m_p_dfdvBC), element.m_dfdvBC);
		add(*(element.m_p_dfdvBD), *(element.m_p_dfdvBD), element.m_dfdvBD);
		add(*(element.m_p_dfdvCD), *(element.m_p_dfdvCD), element.m_dfdvCD);
#endif
	}
}





} /* namespace */
} /* namespace */

