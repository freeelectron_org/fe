/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ironworks/ironworks.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOperateDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<ChainOp>(
			"OperatorSurfaceI.ChainOp.fe");
	pLibrary->add<WayPathOp>(
			"OperatorSurfaceI.WayPathOp.fe");
	pLibrary->add<WayPointOp>(
			"OperatorSurfaceI.WayPointOp.fe");
	pLibrary->add<WayRestraintOp>(
			"OperatorSurfaceI.WayRestraintOp.fe");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
