/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;
	const F32 margin=1e-3f;

	try
	{
		const Quaternionf q0(0.5f,e_yAxis);
		feLog("q0 %s\n",c_print(q0));
		UNIT_TEST(equivalent(q0,Quaternionf(0.0f,0.247f,0.0f,0.969f),margin));

		DualQuaternionf dq0(q0,Quaternionf(2.1,4.3,6.5,0));
		feLog("dq0 %s\n",c_print(dq0));

		Matrix3x4f m0(q0);
		setTranslation(m0,Vector3f(1.1,2.3,4.5));
		Quaternionf qm0(m0);
		DualQuaternionf dqm0(m0);
		feLog("m0\n%s\n",c_print(m0));
		feLog("as quaternion %s\n",c_print(qm0));
		feLog("as dual quat  %s\n",c_print(dqm0));

		const Vector4f* pVectorReal=
				reinterpret_cast< const Vector4f* >(&dqm0.real());
		const Vector4f* pVectorDual=
				reinterpret_cast< const Vector4f* >(&dqm0.dual());
		feLog("mag %.6G %.6G\n",magnitude(*pVectorReal),
				magnitude(*pVectorDual));
		UNIT_TEST(fabs(magnitude(*pVectorReal)-1.0)<margin);

		Matrix3x4f m1(dqm0);
		feLog("m1\n%s\n",c_print(m1));
		UNIT_TEST(equivalent(m0,m1,margin));

		const Vector3f vin(9,8,6);
		feLog("vin %s\n",c_print(vin));

		Vector3f vout0;
		transformVector(dqm0,vin,vout0);
		feLog("vout0 %s\n",c_print(vout0));

		Vector3f vout1;
		transformVector(m0,vin,vout1);
		feLog("vout1 %s\n",c_print(vout1));

		UNIT_TEST(magnitude(vout1-vout0)<margin);

		DualQuaternionf dqIdentity;
		DualQuaternionf dq2(dqIdentity*0.7+dqm0*0.3);

		pVectorReal=
				reinterpret_cast< const Vector4f* >(&dq2.real());
		pVectorDual=
				reinterpret_cast< const Vector4f* >(&dq2.dual());
		feLog("dq2  %s\n",c_print(dq2));
		feLog("mag %.8G %.6G\n",magnitude(*pVectorReal),
				magnitude(*pVectorDual));
		UNIT_TEST(fabs(magnitude(*pVectorReal)-1.0)<0.01);

		normalize(dq2);

		pVectorReal=
				reinterpret_cast< const Vector4f* >(&dq2.real());
		pVectorDual=
				reinterpret_cast< const Vector4f* >(&dq2.dual());
		feLog("dq2  %s\n",c_print(dq2));
		feLog("mag %.8G %.6G\n",magnitude(*pVectorReal),
				magnitude(*pVectorDual));
		UNIT_TEST(fabs(magnitude(*pVectorReal)-1.0)<margin);

		Matrix3x4f m2(dq2);
		feLog("m2\n%s\n",c_print(m2));

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(7);
	UNIT_RETURN();
}
