/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void PoisonOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("group")="poison";
	catalog<String>("group","label")="Poison Group";
	catalog<String>("group","suggest")="pointGroups";
	catalog<String>("group","hint")="Create group of poisoned points.";

	catalog<bool>("openEdges")=false;
	catalog<String>("openEdges","label")="Poison Open Edges";
	catalog<bool>("openEdges","joined")=true;
	catalog<String>("openEdges","hint")=
			"Poison the points of any polygons with too many open edges.";

	catalog<I32>("openEdgeLimit")=1;
	catalog<I32>("openEdgeLimit","high")=10;
	catalog<I32>("openEdgeLimit","max")=1e3;
	catalog<String>("openEdgeLimit","label")="Open Edge Limit";
	catalog<String>("openEdgeLimit","enabler")="openEdges";
	catalog<String>("openEdgeLimit","hint")=
			"Maximum number of open edges that a polygon can have"
			" without getting poisoned.";

	catalog<bool>("lowVertexCount")=false;
	catalog<String>("lowVertexCount","label")="Poison Low Vertex Count";
	catalog<bool>("lowVertexCount","joined")=true;
	catalog<String>("lowVertexCount","hint")=
			"Poison the points of any polygons with too few vertices.";

	catalog<I32>("minVertexCount")=2;
	catalog<I32>("minVertexCount","high")=10;
	catalog<I32>("minVertexCount","max")=1e3;
	catalog<String>("minVertexCount","label")="Min";
	catalog<String>("minVertexCount","enabler")="lowVertexCount";
	catalog<String>("minVertexCount","hint")=
			"Minimum number of vertices per primitive.";

	catalog<bool>("highVertexCount")=false;
	catalog<String>("highVertexCount","label")="Poison High Vertex Count";
	catalog<bool>("highVertexCount","joined")=true;
	catalog<String>("highVertexCount","hint")=
			"Poison the points of any polygons with too many vertices.";

	catalog<I32>("maxVertexCount")=4;
	catalog<I32>("maxVertexCount","high")=10;
	catalog<I32>("maxVertexCount","max")=1e6;
	catalog<String>("maxVertexCount","label")="Max";
	catalog<String>("maxVertexCount","enabler")="highVertexCount";
	catalog<String>("maxVertexCount","hint")=
			"Maximum number of vertices per primitive.";

	catalog<bool>("flood")=true;
	catalog<String>("flood","label")="Flood Fill";
	catalog<String>("flood","hint")=
			"Spread any poison throughout connectivity.";

	catalog< sp<Component> >("Input Surface");
}

void PoisonOp::handle(Record& a_rSignal)
{
	const String groupName=catalog<String>("group");
	const BWORD openEdges=catalog<bool>("openEdges");
	const I32 openEdgeLimit=catalog<I32>("openEdgeLimit");
	const BWORD lowVertexCount=catalog<bool>("lowVertexCount");
	const I32 minVertexCount=catalog<I32>("minVertexCount");
	const BWORD highVertexCount=catalog<bool>("highVertexCount");
	const I32 maxVertexCount=catalog<I32>("maxVertexCount");
	const BWORD flood=catalog<bool>("flood");

	String& rSummary=catalog<String>("summary");
	rSummary=groupName;

	if(groupName.empty())
	{
		catalog<String>("warning")="no group name specified;";
		return;
	}

	if(!openEdges && !lowVertexCount && !highVertexCount)
	{
		catalog<String>("warning")="no poison requested;";
		return;
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
				e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputPoisonGroup;
	if(!access(spOutputPoisonGroup,spOutputAccessible,
				e_pointGroup,groupName)) return;

	//* NOTE for now, initiate poison on faces with two open edges

	m_edgeMap.clear();

	const I32 pointCount=spOutputAccessible->count(SurfaceAccessibleI::e_point);
	Array<I32> pointPoison(pointCount,0);

	const I32 primitiveCount=spOutputVertices->count();

	if(lowVertexCount || highVertexCount)
	{
		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			if(interrupted())
			{
				break;
			}

			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if((!lowVertexCount || subCount>=minVertexCount) &&
					(!highVertexCount || subCount<=maxVertexCount))
			{
				continue;
			}

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spOutputVertices->integer(primitiveIndex,subIndex);

				pointPoison[pointIndex]=TRUE;
			}
		}
	}

	if(openEdges)
	{
		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			if(interrupted())
			{
				break;
			}

			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				continue;
			}

			I32 pointIndexA=spOutputVertices->integer(primitiveIndex,0);
			for(I32 subIndex=1;subIndex<=subCount;subIndex++)
			{
				const I32 pointIndexB=spOutputVertices->integer(
						primitiveIndex,subIndex%subCount);

				const U64 edgeKey=(pointIndexA>pointIndexB)?
						(U64(pointIndexA)<<32)+pointIndexB:
						(U64(pointIndexB)<<32)+pointIndexA;

				Edge& rEdge=m_edgeMap[edgeKey];

				rEdge.m_faceCount++;

				pointIndexA=pointIndexB;
			}
		}

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			if(interrupted())
			{
				break;
			}

			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				continue;
			}

			I32 neighborCount=0;

			I32 pointIndexA=spOutputVertices->integer(primitiveIndex,0);
			for(I32 subIndex=1;subIndex<=subCount;subIndex++)
			{
				const I32 pointIndexB=
						spOutputVertices->integer(primitiveIndex,subIndex%subCount);

				const U64 edgeKey=(pointIndexA>pointIndexB)?
						(U64(pointIndexA)<<32)+pointIndexB:
						(U64(pointIndexB)<<32)+pointIndexA;

				Edge& rEdge=m_edgeMap[edgeKey];

				if(rEdge.m_faceCount>1)
				{
					neighborCount++;
				}

				pointIndexA=pointIndexB;
			}
			if(neighborCount<subCount-openEdgeLimit)
			{
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=
							spOutputVertices->integer(primitiveIndex,subIndex);

					pointPoison[pointIndex]=TRUE;
				}
			}
		}
	}

	if(flood)
	{
		//* NOTE spread poison along neighbors

		BWORD changing=TRUE;
		while(changing)
		{
			changing=FALSE;

			for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				if(interrupted())
				{
					break;
				}

				const I32 subCount=spOutputVertices->subCount(primitiveIndex);
				if(subCount<2)
				{
					continue;
				}

				BWORD anyPoisoned=FALSE;
				BWORD anyUnpoisoned=FALSE;

				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=spOutputVertices->integer(
							primitiveIndex,subIndex);

					if(pointPoison[pointIndex])
					{
						anyPoisoned=TRUE;
					}
					else
					{
						anyUnpoisoned=TRUE;
					}
				}

				//* only if a mix (opportunity to spread)
				if(anyPoisoned && anyUnpoisoned)
				{
					for(I32 subIndex=0;subIndex<subCount;subIndex++)
					{
						const I32 pointIndex=spOutputVertices->integer(
								primitiveIndex,subIndex);

						if(!pointPoison[pointIndex])
						{
							changing=TRUE;
							pointPoison[pointIndex]++;
						}
					}
				}
			}
		}
	}

	I32 poisonCount=0;

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(pointPoison[pointIndex])
		{
			spOutputPoisonGroup->append(pointIndex);
			poisonCount++;
		}
	}

	rSummary.sPrintf("%d points in %s",poisonCount,groupName.c_str());
}

} /* namespace ext */
} /* namespace fe */
