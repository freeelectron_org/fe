/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

namespace fe
{
namespace ext
{

Library *CreateSignalLibrary(sp<Master>)
{
//	feLog("CreateSignalLibrary\n");

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->setName("default_signal");

	pLibrary->add<ChainSignaler>("SignalerI.ChainSignaler.fe");
	pLibrary->add<LogHandler>("HandlerI.LogHandler.fe");
	pLibrary->add<PushSequencer>("SequencerI.PushSequencer.fe");

	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
