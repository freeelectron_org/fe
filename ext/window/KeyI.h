/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_KeyI_h__
#define __window_KeyI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Keyboard event generator

	@ingroup window
*//***************************************************************************/
class FE_DL_EXPORT KeyI:
	virtual public HandlerI,
	public CastableAs<KeyI>
{
	public:

virtual	BWORD	generatesReleaseEvents(void)								=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __window_KeyI_h__ */
