/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ExistController.h"

namespace fe
{
namespace ext
{

ExistController::ExistController(void)
{
}

ExistController::~ExistController(void)
{
}

void ExistController::initialize(void)
{
	cfg<Record>("prototype");
	cfg<String>("prefix") = "fe_exist";
}

void ExistController::handle(Record &r_sig)
{
	m_asSignal.bind(r_sig.layout()->scope());
	if(!m_asSignal.winData.check(r_sig))
	{
		feX("ExistController");
	}
	Record r_windata = m_asSignal.winData(r_sig);

	String m_create_name = cfg<String>("prefix");
	m_create_name.cat("_create");
	String m_destroy_name = cfg<String>("prefix");
	m_destroy_name.cat("_destroy");
	Record m_prototype = cfg<Record>("prototype");

	WindowEvent::Mask m_create = maskGet(m_create_name);
	WindowEvent::Mask m_destroy = maskGet(m_destroy_name);

	WindowEvent wev;
	wev.bind(r_sig);

	if(!m_prototype.isValid())
	{
		feX("ExistController",
			"prototype not set");
	}
	if(wev.is(m_create))
	{
		sp<RecordGroup> rg_output = cfg< sp<RecordGroup> >("output");
		Record r_point = m_prototype.clone();
		m_asPoint.bind(m_prototype.layout()->scope());
		m_asPick.bind(r_sig.layout()->scope());
		m_asPoint.location(r_point) = m_asPick.focus(r_windata);
		rg_output->add(r_point);
	}
	if(wev.is(m_destroy))
	{
		sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");
		sp<RecordGroup> rg_output = cfg< sp<RecordGroup> >("output");
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			if(spRA->layout() == m_prototype.layout())
			{
				for(int i = spRA->length()-1; i >= 0; i--)
				{
					Record r_rm = spRA->getRecord(i);
					rg_output->remove(r_rm);
					rg_output->removeReferences(r_rm, true);
				}
				spRA->clear();
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

