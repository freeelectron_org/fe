// https://docs.unrealengine.com/5.1/en-US/integrating-third-party-libraries-into-unreal-engine/
// NOTE supposed to replace "Windows.h" with "Windows/WindowsHWrapper.h"
// TODO consider PRAGMA_PUSH_PLATFORM_DEFAULT_PACKING and POP, vs /Zp8
// TODO set ExternalDependencies on some FE files to assure rebuild

using System;
using System.IO;
using System.Collections.Generic;

namespace UnrealBuildTool.Rules
{
	public class FEOps : ModuleRules
	{
		public FEOps(ReadOnlyTargetRules Target) : base(Target)
		{
			bEnableExceptions = true;
//			bUseRTTI = true;
//			bUseAVX = true;

			// avoid .cpp concatenation
			MinSourceFilesForUnityBuildOverride = 1000;

			CppStandard = CppStandardVersion.Cpp17;

			bool isWindows = Target.IsInPlatformGroup(UnrealPlatformGroup.Windows);

			PublicIncludePaths.AddRange(
				new string[] {
					// ... add public include paths required here ...
				}
				);

			PrivateIncludePaths.AddRange(
				new string[] {
					// ... add other private include paths required here ...
				}
				);

			string feHeaders = PluginDirectory + "/Source/fe";
			string feLibs = PluginDirectory + "/Source/fe/lib/${FE_API_CODEGEN}";

			PublicSystemIncludePaths.AddRange(
				new string[] {
//					"/usr/include",		<- don't add this
					feHeaders + "/src",
					feHeaders + "/ext",
					feHeaders + "/lab",
					feLibs
				}
				);

			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"InputCore",
//~					"ProceduralMeshComponent",
					"GeometryCore",
					"GeometryFramework",
					"DynamicMesh",

//					"RenderCore",
//					"ShaderCore",
//					"RHI"

					// ... add other public dependencies that you statically link with here ...
				}
				);

			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					// ... add private dependencies that you statically link with here ...
				}
				);

			DynamicallyLoadedModuleNames.AddRange(
				new string[]
				{
					// ... add any modules that your module loads dynamically here ...
				}
				);

//			PublicAdditionalLibraries.Add("stdc++");

			List<string> libNames = new List<string>()
			{
				"fePlatform",
				"feMemory",
				"feImportMemory",
				"feCore",
				"feData",
				"fePlugin",
				"fexMetaDL",
				"fexSurfaceDL",
				"fexTerminalDL",
				"fexDrawDL"
			};

			if(isWindows)
			{
				libNames.AddRange(
					new List<string>()
					{
						"fexDataToolDL",
						"fexGeometryDL",
						"fexThreadDL"
					}
				);

				foreach(string libName in libNames)
				{
					PublicAdditionalLibraries.Add(
							feLibs + "/" + libName + ".lib");
				}

				Console.WriteLine("--------------------------------");
				Console.WriteLine("FEOps.Build.cs");
				Console.WriteLine(
						string.Format("editor {0}",Target.bBuildEditor));
				Console.WriteLine(
						string.Format("Engine {0}",EngineDirectory));
				Console.WriteLine(
						string.Format("Plugin {0}",PluginDirectory));
				Console.WriteLine(
						string.Format("Module {0}",ModuleDirectory));
				Console.WriteLine("--------------------------------");

				// doesn't seem to affect editor, but no reason to waste time
				if(!Target.bBuildEditor)
				{
					List<string> platformFiles = new List<string>();
					platformFiles.AddRange(
							Directory.GetFiles(feLibs,"*.dll"));
					platformFiles.AddRange(
							Directory.GetFiles(feLibs,"*.yaml"));
					foreach(string fullname in platformFiles)
					{
						string filename = Path.GetFileName(fullname);

						// NOTE while this convientiently copies to the
						// Binaries/Win64/ subdir for a package,
						// it also populates the one that the editor uses.
						// This can lead to crytic errors in Editor mode
						// if the plugin is updated on the engine,
						// until you repackage (or delete those dlls).

						// copy file to project and package
						// with Add(destPath, sourcePath)
//						RuntimeDependencies.Add(
//								"$(TargetOutputDir)/" + filename,fullname);
					}
				}

//				PublicRuntimeLibraryPaths.Add(
//						PluginDirectory + "/Binaries/Win64");
			}
			else
			{
				foreach(string libName in libNames)
				{
					if(libName =="feImportMemory")
						continue;
					PublicAdditionalLibraries.Add(
							feLibs + "/lib" + libName + ".so");
				}

				//* TODO proper packaging
				// should this be Public/PrivateRuntimeLibraryPaths?
				PublicSystemLibraryPaths.Add(feLibs + "");
			}
		}
	}
}
