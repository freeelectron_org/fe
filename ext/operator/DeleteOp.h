/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_DeleteOp_h__
#define __operator_DeleteOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Remove element from a Surface by group

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT DeleteOp:
	public OperatorSurfaceCommon,
	public Initialize<DeleteOp>
{
	public:
				DeleteOp(void)												{}
virtual			~DeleteOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_DeleteOp_h__ */
