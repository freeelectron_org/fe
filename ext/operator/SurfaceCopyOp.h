/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OperatorSurfaceCopy_h__
#define __operator_OperatorSurfaceCopy_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Copy first input surface onto points of second surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceCopyOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceCopyOp>
{
	public:
				SurfaceCopyOp(void)											{}
virtual			~SurfaceCopyOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

static	void	copyAttributes(String a_attrRegex,
					sp<SurfaceAccessibleI> a_spTemplateAccessible,
					sp<SurfaceAccessibleI> a_spOutputAccessible,
					I32 a_templateStart,I32 a_templateCount,
					I32 a_outputStart,I32 a_grain);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OperatorSurfaceCopy_h__ */
