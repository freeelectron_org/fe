/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_ExplicitInertial_h__
#define __solve_ExplicitInertial_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "shape/shape.h"

namespace fe
{
namespace ext
{

/**	Euler time integration

	@copydoc ExplicitInertial_info
	*/
class FE_DL_EXPORT ExplicitInertial:
	virtual public HandlerI,
	virtual public Config,
	public Initialize<ExplicitInertial>
{
	public:
				ExplicitInertial(void);
virtual			~ExplicitInertial(void);
virtual	void	initialize(void);

				// AS HandlerI
virtual	void	handleBind(sp<SignalerI> spSignaler,
					sp<Layout> l_sig);
virtual	void	handle(Record &r_sig);

	private:
		AsParticle			m_asParticle;
		AsForcePoint		m_asForcePoint;
		AsTemporal			m_asTemporal;
		AsRK2				m_asRK2;
		AsAccumulate		m_asAccumulate;
		AsClear				m_asClear;
		AsUpdate			m_asUpdate;
		hp<SignalerI>		m_hpSignaler;
		sp<Scope>			m_spScope;
		Record				r_clear;
		Record				r_accum;
		Record				r_update;
		//std::vector<SpatialVector>	m_forceTemp;
		//std::vector<SpatialVector>	m_velTemp;

		enum
		{
			e_foreback	= 0,
			e_rk2		= 1,
			e_euler		= 2,
			e_verlet	= 3
		};
		FE_UWORD				m_mode;

	void	accumulate(sp<RecordGroup> rg_input, Real a_timestep);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_ExplicitInertial_h__ */

