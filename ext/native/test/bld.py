import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexWindowLib" ]

    tests = [ 'xFont', 'xWindow' ]

    glxtests = []

    forge.tests += [
        ("xFont.exe",       "",                             None,       None),
        ("xWindow.exe",     "10",                           None,       None) ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib" ]
    else:
        tests += [ 'xRawKey' ]
        glxtests += [ 'xGlx' ]

        forge.tests += [
            ("xGlx.exe",    "",                             None,       None) ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    for t in glxtests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }
        forge.deps([t + "Exe"], deplibs)
