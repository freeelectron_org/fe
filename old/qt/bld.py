import sys
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "qt.pmh",
                "qtDL" ]

    dll = module.DLL( "fexQTDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    forge.includemap['qt'] = "/usr/include/qt4"
    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["guilibs"] = "-lQtGui"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            dll.linkmap["guilibs"] = "qtd.lib"
        else:
            dll.linkmap["guilibs"] = "qt.lib"

    forge.deps( ["fexQTDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexQTDL",          None,   None) ]

    module.Module('test')

def auto(module):
    test_file = """
#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
    return 0;
}
    \n"""

    qtmap = {}
    if forge.fe_os == "FE_LINUX":
        qtmap["quilibs"] = "-lQtGui"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            qtmap["quilibs"] = "qtd.llib"
        else:
            qtmap["quilibs"] = "qt.llib"

    forge.includemap['qt'] = "/usr/include/qt4"
    return forge.cctest(test_file, qtmap)
