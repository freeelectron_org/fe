import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "mechanics", "moa" ]

def setup(module):
    srcList = [ "driveline.pmh",
                "DrivelineISystem",
                "SemiImplicitDriveline",
                "TorsionSpring",
                "drivelineDL" ]

    dll = module.DLL( "fexDrivelineDL", srcList )

#   dll.linkmap = { "tbb": ' -ltbb' }

    deplibs = forge.corelibs + [
                "feMathLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "feDataDLLib",
                "fexEvaluateDLLib",
                "fexMechanicsDLLib",
                "fexMoaDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib" ]

    forge.deps( ["fexDrivelineDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "drivelineDL",                  None,   None) ]

    module.Module('test')
