/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordMap_h__
#define __data_RecordMap_h__

namespace fe
{


template <class T>
class RecordMap : public WatcherI
{
	public:
		typedef std::map<T, Record> t_T_record;

							RecordMap(const Accessor<T> aKey);
virtual						~RecordMap(void);

							//* as WatcherI
		void				add(const Record &record);
		void				remove(const Record &record);
		void				clear(void);

		Record				lookup(const T &t);

	public:
		std::map<T, Record>	m_table;
		Accessor<T>			m_aKey;
};

template <class T>
RecordMap<T>::RecordMap(const Accessor<T> aKey)
{
	m_aKey = aKey;
}

template <class T>
RecordMap<T>::~RecordMap(void)
{
}

template <class T>
void RecordMap<T>::add(const Record &record)
{
	if(m_aKey.check(record))
	{
		m_table[m_aKey(record)] = record;
	}
}

template <class T>
void RecordMap<T>::remove(const Record &record)
{
	if(m_aKey.check(record))
	{
		typename t_T_record::iterator it = m_table.find(m_aKey(record));
		if(it != m_table.end())
		{
			if(record == it->second)
			{
				m_table.erase(it);
			}
		}
	}
}

template <class T>
void RecordMap<T>::clear(void)
{
	m_table.clear();
}

template <class T>
Record RecordMap<T>::lookup(const T &t)
{
	typename t_T_record::iterator it = m_table.find(t);
	if(it == m_table.end())
	{
		Record invalid;
		return invalid;
	}
	else
	{
		return it->second;
	}
}

} /* namespace */

#endif /* __data_RecordMap_h__ */

