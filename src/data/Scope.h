/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Scope_h__
#define __data_Scope_h__

#define FE_SCOPE_SUPPORT_WATCHERS	FALSE
#define FE_SCOPE_GETRECORD			FALSE

namespace fe
{

class RecordFactoryI;
class WatcherI;
class RecordCookbook;
class AccessorSet;

typedef AutoHashMap< String, Array<String> > StringStringsMap;
typedef AutoHashMap< hp<Layout>, sp<StoreI> > layout_store_map;

/** Attribute collection */
typedef Array< sp<Attribute> >	t_attributes;
/** Depend collection */
typedef std::list< sp<Depend> >			t_depends;
/** Layout collection */
typedef Array< hp<Layout> >		t_layouts;
#if FE_SCOPE_SUPPORT_WATCHERS
/** Watcher collection */
typedef std::list<sp<WatcherI> >		t_watchers;
#endif
typedef	std::map<String, unsigned int>			t_indexmap;
typedef	std::map<Attribute *, unsigned int>		t_attrmap;

template <class T> class Accessor;

#if 0
class OffsetTable : public Counted
{
	public:
				OffsetTable(void);
				~OffsetTable(void);


		Offset	*table(void) { return m_table; }

		void	resize(unsigned int a_size);

		void	attach(sp<Layout> a_layout);
	private:
		Array< hp<Layout> >	m_layouts;
		Offset						*m_table;
		unsigned int				m_size;
};
#endif

/**	@brief Layout namespace

	@ingroup data

	Every Layout must be associated with a single
	Scope.  Every Record layout is defined by a single Layout, therefore
	every Record is associated with a single Scope.

	Scope is therefore the main class for state based activity.  Records
	are defined (via Layout), created, and destroyed using Scope.

	*/
class FE_DL_EXPORT Scope: public Component, public Initialize<Scope>,
		public ObjectSafe<Scope>
{
	public:
#if 1
		/** @brief  Create a scope from within the fe::Registry

			This function will assert if called outside the registry
			which supplies the fe::Master information.

			@internal */
								Scope(void);
#endif
		/** Construct a Scope bound to the the TypeMaster and Allocator
			within @em master. */
								Scope(Master &master);
								Scope(sp<Master> &spMaster);
		/** Construct a Scope bound to the given TypeMaster and Allocator. */
								Scope(sp<TypeMaster> spTypeMaster,
										sp<Allocator> allocator);
								Scope(const Scope &other);
virtual							~Scope(void);
		Scope					&operator=(const Scope &other);

virtual	void					setName(const String& name);

#if 1
		/// @brief Post construction step (last chance to find Master)
		void					initialize(void);
#endif

		/** Validate that there are no dependency conflicts and remove
			dependency duplicates.  This is mainly for internal use. */
		void					layoutValidate(void);

		/** Specify if select methods should be protected with a mutex. */
		void					setLocking(BWORD a_locking)
								{	m_locking=a_locking; }
		BWORD					isLocking(void)
								{	return m_locking; }

private:
		/** Release references to all Layout, Depend, and Store instances.
			This may be necessary to break dependency cycles.
			@todo create and reference dependency graph
			*/
		bool					shutdown(void);
public:

		/**	Declare a new layout

			This will create an empty and unlocked layout.

			An empty layout name is not permitted
			(an invalid sp<> would be returned).
		*/
		sp<Layout>				declare(const String &name);
		/** Lock a layout.  Finalize the structure that the layout represents.
			This must happen before records of this layout are created.
			If not done before a call to createRecord or createRecordArray
			then the layout is automatically locked upon these calls. */
		//void					lockLayout(sp<Layout> spLayout);
		/** Return the layout with the given name

			This first attempts to lookup an existing Layout.
			If that fails, it will then try to instantiate a layout from
			a RecordFactoryI registered under the same name.

			If no layout is found, an invalid sp<> is returned. */
		sp<Layout>				lookupLayout(const String &name);
#if 0
		/** Return the Store for @em spLayout in @em spStore.  Return true
			upon success, otherwise return false. */
		bool					lookupStore(sp<Layout> spLayout,
									sp<StoreI> &spStore);
#endif

		/**	Specify who can produce a named layout. */
		void					registerFactory(String name,
										sp<RecordFactoryI> spRecordFactoryI);
		///	Create and return a new Record from registered RecordFactoryI.
		Record					produceRecord(String name);
		///	Run post-initialization on Record using registered RecordFactoryI.
		void					finalize(Record& rRecord);

		/**	Create and return a new Record of layout @em spLayout. */
		Record					createRecord(sp<Layout> spLayout);
		Record					createRecord(const String layout);
		/**	Create and return a new RecordArray of layout @em spLayout. */
		sp<RecordArray>			createRecordArray(String layout,
									FE_UWORD count);
		sp<RecordArray>			createRecordArray(sp<Layout> spLayout,
									FE_UWORD count);
		/** Free the given record.  This will throw an Exception if
			@em record is reference counted.  All records with the
			Attribute FE_C are reference counted.  This is the default for
			all records (Scope has populate("", FE_C) in its constructor). */
		//void					freeRecord(Record record);

		/** Add a dependency.  This is mainly for internal use by support,
			enforce, populate, and share. */
		sp<Attribute>			addDepend(sp<Depend> &depend);
		/** Return the total number of Attributes in this Scope. */
		FE_UWORD				getAttributeCount(void) const;
		/** Return the Attribute with the given name.
			The index of the Attribute is returned in @em index. */
		sp<Attribute>&			findAttribute(const String &name, FE_UWORD &index);
		/** Return the Attribute with the given name.*/
		sp<Attribute>&			findAttribute(const String &name);
		/** Return the Attribute at the given index. */
		sp<Attribute>			attribute(FE_UWORD index);
		/** Return the index of the given Attribute. */
		FE_UWORD				attributeIndex(sp<Attribute> spAttribute);

		/** Add a support for a Attribute with name @mayHave and type of name
			@em ofType.   */
		sp<Attribute>			support(			const String &mayHave,
													const String &ofType);
		/** Enforce a Attribute dependency.  If a Layout has a Attribute named
			@em ifHas, it will also have a Attribute of name @em mustHave. */
		bool					enforce(			const String &ifHas,
													const String &mustHave);
		bool					enforce(	const Array<String> &ifHas,
													const String &mustHave);
		/** Add a Attribute to a Layout.  If a Layout is named @em ifIs, then
			it will have a Attribute of name @em mustHave. */
		bool					populate(			const String &ifIs,
													const String &mustHave);
		// seems too brittle while also being unnecessary
		// /** Clear all populate dependencies for Layout of name @em name. */
		//bool					clear(				const String &ifIs);
		/** Specify a Attribute share.
			This means that two Attributes will actually
			be the same instance (same piece of memory) for a particular
			Layout.  If a Layout is named @em ifIs, then treat Attributes
			of names @em share1 and @em share2 as the same. */
		bool					share(				const String &ifIs,
													const String &share1,
													const String &share2);

#ifdef DEPRECATE
		/* old example from SurfaceTransform to embed sub-vectors
			scope()->within(spLayout->name(), "spc:dir",
					"spc:transform", 0);
			scope()->within(spLayout->name(), "spc:left",
					"spc:transform", sizeof(SpatialVector));
			scope()->within(spLayout->name(), "spc:up",
					"spc:transform", 2*sizeof(SpatialVector));
			scope()->within(spLayout->name(), "spc:at",
					"spc:transform", 3*sizeof(SpatialVector));
		*/
		bool					within(				const String &ifIs,
													const String &child,
													const String &parent,
													const size_t &offset);
#endif

		void					clonePopulate(		const String &ifThisHas,
													const String &thenThisDoes);

		/** Return the associated TypeMaster. */
		sp<TypeMaster>			typeMaster(void);

		/** Return an Accessor for the Record reference count Attribute. */
		Accessor< std::atomic<int> >			&refCount(void);

		void					resizeLayoutLocators(void) const;

#if 0
		/**	Specify a store to use by name and registry for creating
			state blocks. */
		void					specifyStore(		sp<Registry> spRegistry,
													const String &storeName);
#endif

#if FE_SCOPE_SUPPORT_WATCHERS
		void					addWatcher(			sp<WatcherI> spWatch);
		bool					removeWatcher(		sp<WatcherI> spWatch);
#endif

		/** For debugging. */
		void					peek(Peeker &peeker);

		U32						serialIndex(void)	{ return m_serialIndex; }
		template <typename R>
		void					assignIDNumber(		const R &record);
		template <typename R>
		void					freeIDNumber(		const R &r_old);

		// This is not the IDs above.  This is for generated layout names.
		FE_UWORD					getNextUniqueLayoutID(void)
									{return m_nextUniqueLayoutID++;}

		FE_UWORD					getNextUniqueIDR(void)
									{return m_nextUniqueIDR++;}

#if FE_SCOPE_GETRECORD
		Record					getRecord(			IWORD id);
#endif

		void					addAccessorSet(AccessorSet *a_as);
		void					removeAccessorSet(AccessorSet *a_as);
	public:
		/** Direct access to the attribute table. Use with care. */
		t_attributes					&attributes(void);
		/** Direct access to the layout table. Use with care. */
		t_layouts						&layouts(void);
		/** Direct access to the depend list. Use with care. */
		t_depends						&depends(void);
		/** Direct access to the allocator. Use with care. */
		sp<Allocator>					allocator(void);
		sp<RecordCookbook>				cookbook(void);

	private:
		void					construct(sp<TypeMaster> spTypeMaster,
										sp<Allocator> allocator);
		void					prepareCookbook(sp<Master>& rspMaster);
		void					depAttributeCheck(sp<Depend> &depend);
		void					depRecordCheck(sp<Depend> &depend);
		sp<Attribute>			dynamicAddAttribute(const String &typeName,
										const String &attributeName);
		sp<Attribute>			addAttribute(const String &typeName,
										const String &attributeName);

		//sp<StoreI>				createStore(sp<Layout> spLayout) const;

		void					notifyLayoutsOfAttributeChange(
									sp<Depend> &depend);

#if FE_SCOPE_SUPPORT_WATCHERS
		void					watch(const Record &record);
		void					unwatch(const Record &record);
#endif

	private:
		sp<TypeMaster>					m_spTypeMaster;
		t_layouts						m_layouts;
		t_attributes					m_attributes;
		t_indexmap						m_indexmap;
		//t_attrmap						m_attrmap;
		t_depends						m_depend;
#if FE_SCOPE_SUPPORT_WATCHERS
		t_watchers						m_watchers;
#endif
		//layout_store_map				m_storeMap;

		sp<Allocator>					m_spAllocator;
		sp<RecordCookbook>				m_spRecordCookbook;

		std::set<AccessorSet *>			m_accessorSets;

		bool							m_looking;
		bool							m_locking;
		I32								m_serialCount;
		U32								m_serialIndex;

		Accessor< std::atomic<int> >	*m_paCount;
		Accessor< std::atomic<int> >	*m_paSerialNumber;
		hp<Registry>					m_hpRegistry;
		String							m_storeName;

#if FE_SCOPE_GETRECORD
		Accessor<int>					*m_paIDNumber;
		Array<WeakRecord>				m_idNumbered;
		Array<int>						m_freeIDNumbers;
#endif

		//Array< sp<OffsetTable> >	m_offsetTables;

typedef HashMap<	String,
					sp<RecordFactoryI>,
					hash_string,
					eq_string > StringRecordFactoryMap;

		StringRecordFactoryMap			m_factoryMap;
		StringStringsMap&				factoryMultiMap(void);

		sp<Attribute>					m_nullAttribute;
		FE_UWORD						m_nextUniqueLayoutID;
		FE_UWORD						m_nextUniqueIDR;
};

FE_DL_EXPORT void assertData(sp<TypeMaster> spTypeMaster);

inline sp<TypeMaster> Scope::typeMaster(void)
{
	return m_spTypeMaster;
}

inline t_attributes &Scope::attributes(void)
{
	return m_attributes;
}

inline t_layouts &Scope::layouts(void)
{
	return m_layouts;
}

inline t_depends &Scope::depends(void)
{
	return m_depend;
}

inline sp<Allocator> Scope::allocator(void)
{
	return m_spAllocator;
}



} /* namespace */

#endif /* __data_Scope_h__ */

