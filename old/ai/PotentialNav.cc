/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "PotentialNav.h"

namespace fe
{

PotentialNav::PotentialNav(void)
{
}

PotentialNav::~PotentialNav(void)
{
}

void PotentialNav::initialize(void)
{
}

void PotentialNav::handle(Record &r_sig)
{
	m_asMobile.bind(r_sig.layout()->scope());
	m_asParticle.bind(r_sig.layout()->scope());
	m_asNav.bind(r_sig.layout()->scope());
	m_asNavable.bind(r_sig.layout()->scope());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(	m_asParticle.check(spRA) &&
			m_asMobile.check(spRA) &&
			m_asNavable.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(m_asNavable.nav(spRA, i).isValid() &&
					m_asNav.check(m_asNavable.nav(spRA, i)))
				{
					SpatialVector location;
					location = m_asParticle.location(spRA, i);
					hp<VectorFieldI> spField(
						m_asNav.field(m_asNavable.nav(spRA, i)));
					SpatialVector dir;
					if(!spField.isValid())
					{
						feX("PotentialNav::handle",
							"field not valid");
					}
					if(spField->sample(dir, location))
					{
						m_asMobile.direction(spRA, i) = -dir;
					}
					else
					{
						m_asMobile.direction(spRA, i) =
							m_asNav.location(m_asNavable.nav(spRA, i)) - location;
					}
				}
			}
		}
	}
}



} /* namespace */

