/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#include <random>

#define FE_SWCL_DEBUG		FALSE

namespace fe
{
namespace ext
{

void SurfaceWrapCLOp::initialize(void)
{
	catalog< sp<Component> >("Reference Guide Curves");

	catalog< sp<Component> >("Guide Curves");

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:pt:bindFace0 "
			"2:pt:P "
			"2:pt:N "
			"3:vertStart "
			"3:vertCount "
			"3:pt:P "
			"3:pt:N "
			"param:bindT0";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "SurfaceWrapCLOp.cl.c"
			;
}

void SurfaceWrapCLOp::handle(Record& a_rSignal)
{
#if FE_SWCL_DEBUG
	feLog("SurfaceWrapCLOp::handle\n");
#endif

	//* TEMP
	catalog<String>("input:1")="Guide Curves";
	catalog<String>("input:2")="Reference Guide Curves";

	startup();
	if(!m_started)
	{
		return;
	}

	sp<Counted>& rspCounted=catalog< sp<Counted> >("bindT0");

	if(!paramContextIsCurrent("bindT0"))
	{
		rspCounted=NULL;
	}

	if(rspCounted.isNull())
	{
#if FE_SWCL_DEBUG
		feLog("SurfaceWrapCLOp::handle access ref guides\n");
#endif
		sp<SurfaceAccessibleI> spGuideAccessible=
				accessOpenCL("Reference Guide Curves");
		if(spGuideAccessible.isNull()) return;

#if FE_SWCL_DEBUG
		feLog("SurfaceWrapCLOp::handle access input\n");
#endif
		sp<SurfaceAccessibleI> spInputAccessible=accessOpenCL("Input Surface");
		if(spInputAccessible.isNull()) return;

#if FE_SWCL_DEBUG
		feLog("SurfaceWrapCLOp::handle access input positions\n");
#endif
		sp<SurfaceAccessorI> spInputPosition;
		if(!access(spInputPosition,spInputAccessible,
				e_point,e_position)) return;

#if FE_SWCL_DEBUG
		feLog("SurfaceWrapCLOp::handle access input positions\n");
#endif
		sp<SurfaceAccessorI> spInputBindBary0;
		if(!access(spInputBindBary0,spInputAccessible,
				e_point,"bindBary0")) return;

#if FE_SWCL_DEBUG
		feLog("SurfaceWrapCLOp::handle access guide surface\n");
#endif
		sp<SurfaceI> spGuides;
		if(!access(spGuides,spGuideAccessible)) return;

#if FE_SWCL_DEBUG
		feLog("SurfaceWrapCLOp::handle access guide vertices\n");
#endif
		const I32 pointCount=spInputPosition->count();

#if FE_SWCL_DEBUG
		sp<SurfaceAccessorI> spGuideVertices;
		if(!access(spGuideVertices,spGuideAccessible,
				e_primitive,e_vertices)) return;

		const I32 guideCount=spGuideVertices->count();

		feLog("SurfaceWrapCLOp::handle pointCount %d guideCount %d\n",
				pointCount,spGuideVertices->count());
#endif

		float* bindT0Array=new float[pointCount];

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			bindT0Array[pointIndex]=
					spInputBindBary0->spatialVector(pointIndex)[0];

//			feLog("point %d T0 %.6G\n",pointIndex,,bindT0Array[pointIndex]);
		}

		sp<SurfaceAccessibleOpenCL::ClMem> spClMem(
				new SurfaceAccessibleOpenCL::ClMem());
		rspCounted=spClMem;

		cl_int clError=0;

		spClMem->m_clMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
				pointCount*sizeof(float),bindT0Array,&clError);
		if(clError)
		{
			feLog("SurfaceWrapCLOp::handle clCreateBuffer clError %d\n",
					clError);
		}

		delete[] bindT0Array;
	}

#if FE_SWCL_DEBUG
	feLog("SurfaceWrapCLOp::handle as OpenCLOp\n");
#endif

	OpenCLOp::handle(a_rSignal);

#if FE_SWCL_DEBUG
	feLog("SurfaceWrapCLOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
