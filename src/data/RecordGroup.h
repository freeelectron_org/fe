/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordGroup_h__
#define __data_RecordGroup_h__

namespace fe
{

class RecordOperation;

//*	NOTE the watcher functionality should be moved to a Watchable class
//* in case it ever gets used for any purpose other that RecordMap

//typedef AutoHashMap< hp<Layout>, sp<RecordArray> >	t_layout_ra;
typedef std::map< hp<Layout>, sp<RecordArray> >	t_layout_ra;

typedef std::map<	BaseAccessor,
					sp<WatcherI> >			t_attribute_watcher;

typedef std::set<	sp<WatcherI> >			t_watchers;

/** @brief A heterogenous collection of records.

	@ingroup data

	From a membership management point of view this is a
	collection of Records.
	From an iteration point of view this is a collection of RecordArrays.
	Each of these RecordArrays contains Records of a single Layout.
	*/
class FE_DL_EXPORT RecordGroup :
	public Handled<RecordGroup>,
	public ClassSafe<RecordGroup>
{
	public:
		RecordGroup(void);
		RecordGroup(const RecordGroup &other);
virtual	~RecordGroup(void)													{}

virtual	RecordGroup		&operator=(const RecordGroup &other);

		/**	Add @em record to the collection.  */
virtual	void			add(const Record &record);
		/**	Add @em records to the collection.  */
virtual	void			add(sp<RecordArray> &records);
		/**	Add @em records to the collection.  */
virtual	void			add(sp<RecordGroup> &records);
		/** @brief Remove @em record from the collection

			Returns false if record was not present. */
virtual	bool			remove(const Record &record);
		/**	Remove @em records from the collection.  */
virtual	void			remove(sp<RecordGroup> &records);
		/** Clear the collection. */
virtual	void			clear(void);
		/** Return the number of RecordArray (Layouts). */
virtual	FE_UWORD			size(void);
		/** Return the the total count of records. */
virtual	FE_UWORD			count(void);

		/** Return the scope of the first RecordArray within. */
virtual	sp<Scope>		getFirstScope(void);
virtual	sp<Scope>		scope(void);

		class Info
		{
			public:
				std::set< sp<RecordGroup> >		m_rgs;
				std::set<Record>				m_rs;
		};

		/** Don't retain a mappping from Record to array index.
			RecordArray::remove is faster, while RecordArray::find is slower.
		*/
		void			enableDuplicates(bool a_bool);

		/** Try not to reorder entries in each array.

			Consider also using enableDuplicates(true).
			RecordArray::remove can be slower otherwise.
		*/
		void			maintainOrder(bool a_bool);

		/** Remove all references to the given record with contained records. */
virtual	void			removeReferences(const Record &record,
							bool recurse=false, Info *info=NULL);

		/** Perform the RecordOperation @em op on all records. */
virtual	void			all(RecordOperation &op);

		/**	Return true if @em record is in the record group.  Otherwise return
			false. */
virtual	bool			find(const Record &record);

virtual	sp<RecordArray>	getArray(sp<Layout> l_query);
virtual	sp<RecordArray>	getArray(const String layout);

		template <class T>
		sp< RecordMap<T> > lookupMap(Accessor<T> accessor);
		template <class T>
		Record			lookup(Accessor<T> accessor, const T &value);

						/** @brief Choose weak referencing

							Usually, this should only occur immediately
							after construction before any Records are added.

							Changing this value with existing records
							incurs the overhead of rebuilding all the
							underlying arrays. */
		void			setWeak(BWORD weak);

						/// @brief Return TRUE if using weak referencing
		BWORD			isWeak(void) const		{ return m_weak; }

						/// @brief Remove any invalid weak references
		void			prune(void);

		/** STL style iterator. */
		class FE_DL_EXPORT iterator
		{
			public:
				friend class RecordGroup;

								iterator(void)								{}
								iterator(const iterator& it);
				iterator&		operator=(const iterator& it);
				sp<RecordArray>	&operator*(void);
				sp<RecordArray>	*operator->(void);
				iterator		operator++();
				iterator		operator++(int);
				bool			operator==(const iterator &other);
				bool			operator!=(const iterator &other);
				sp<RecordGroup>	group(void);
				bool			check(void);
			private:
				t_layout_ra::iterator		m_raMapIt;
				sp<RecordGroup>				m_spRG; // be sure to keep RG alive
				Array<BaseAccessor>			m_filters;
				t_layout_ra::iterator		m_endIt;
		};

		iterator	begin(void);
		iterator	begin(Array<BaseAccessor> filters);
		iterator	end(void);

					/// @internal
		void		watcherAdd(const Record &record);
					/// @internal
		void		watcherRemove(const Record &record);

	private:
virtual	void		removeReferencesFromRecord(Record &from,
							const Record &record,
							bool recurse=false, Info *info=NULL);
		void		rebuildArrays(void);

		t_layout_ra									m_raMap;
		t_attribute_watcher							m_indices;
		t_watchers									m_watchers;
		BWORD										m_weak;
		BWORD										m_locking;
		bool										m_duplicates;
		bool										m_maintainOrder;

#if FE_COUNTED_TRACK
	public:
const	String	verboseName(void) const
				{	return "RecordGroup " +
							String(m_weak? "(weak) ": "") + name(); }
#endif
};

class FE_DL_EXPORT PtrRecordGroupInfo : public BaseType::Info
{
	public:
virtual	String	print(void *instance);
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	IWORD	iosize(void);
virtual	bool	getConstruct(void);
virtual	void	construct(void *instance);
virtual	void	destruct(void *instance);
};

template <class T>
sp< RecordMap<T> > RecordGroup::lookupMap(Accessor<T> accessor)
{
	sp< RecordMap<T> > spRI;

	t_attribute_watcher::iterator it = m_indices.find(accessor);
	if(it == m_indices.end())
	{
		spRI = new RecordMap<T>(accessor);
		m_indices[accessor] = spRI;
		m_watchers.insert(spRI);
		// scan existing records
		for(RecordGroup::iterator rg_it = this->begin();
			rg_it != this->end(); rg_it++)
		{
			sp<RecordArray> spRA = *rg_it;
			if(accessor.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					spRI->add(spRA->getRecord(i));
				}
			}
		}
	}
	else
	{
		spRI = it->second;
	}

	return spRI;
}

template <class T>
Record RecordGroup::lookup(Accessor<T> accessor, const T &value)
{
	sp< RecordMap<T> > spMap=lookupMap<T>(accessor);
	FEASSERT(spMap.isValid());

	return spMap.isValid()? spMap->lookup(value): Record();
}

inline RecordGroup::RecordGroup(void)
{
	m_weak = FALSE;
	m_locking = FALSE;
	m_duplicates = FALSE;
	m_maintainOrder = FALSE;
}

inline RecordGroup::RecordGroup(const RecordGroup &other):
	Castable(),
	Handled<RecordGroup>(),
	ClassSafe<RecordGroup>()
{
	m_raMap = other.m_raMap;
	m_weak = other.m_weak;
	m_locking = other.m_locking;
	m_duplicates = other.m_duplicates;
	m_maintainOrder = other.m_maintainOrder;
}

inline RecordGroup &RecordGroup::operator=(const RecordGroup &other)
{
	SAFEGUARDCLASS;
	feX("RecordGroup", "uncertain about desired assignment semantics");
	if(this != &other)
	{
		m_raMap = other.m_raMap;
		m_weak = other.m_weak;
		m_locking = other.m_locking;
		m_duplicates = other.m_duplicates;
		m_maintainOrder = other.m_maintainOrder;
	}
	return *this;
}

inline void RecordGroup::setWeak(BWORD weak)
{
	if(m_weak!=weak)
	{
		m_weak = weak;
		rebuildArrays();
	}
}

inline void RecordGroup::enableDuplicates(bool a_bool)
{
	if(m_duplicates!=a_bool)
	{
		m_duplicates = a_bool;
		rebuildArrays();
	}
}

inline void RecordGroup::maintainOrder(bool a_bool)
{
	if(m_maintainOrder!=a_bool)
	{
		m_maintainOrder = a_bool;
		rebuildArrays();
	}
}

inline FE_UWORD RecordGroup::size(void)
{
	SAFEGUARDCLASS_IF(m_locking);
	return m_raMap.size();
}

inline bool RecordGroup::find(const Record &record)
{
	SAFEGUARDCLASS_IF(m_locking);
	sp<RecordArray> spRA;
	t_layout_ra::iterator it = m_raMap.find(record.layout());
	if(it == m_raMap.end())
	{
		return false;
	}
	return it->second->find(record);
}

inline void RecordGroup::prune(void)
{
	SAFEGUARDCLASS_IF(m_locking);
	for(RecordGroup::iterator i_rg = begin(); i_rg != end(); i_rg++)
	{
		(*i_rg)->prune();
	}
}

inline void RecordGroup::clear(void)
{
	SAFEGUARDCLASS_IF(m_locking);
	m_raMap.clear();

	for(t_attribute_watcher::iterator tw_it = m_indices.begin();
		tw_it != m_indices.end(); tw_it++)
	{
		tw_it->second->clear();
	}
	for(t_watchers::iterator tw_it = m_watchers.begin();
		tw_it != m_watchers.end(); tw_it++)
	{
		(*tw_it)->clear();
	}
}

inline RecordGroup::iterator RecordGroup::begin()
{
	RecordGroup::iterator it;
	it.m_raMapIt = m_raMap.begin();
	it.m_endIt = m_raMap.end();
	it.m_spRG = this;
	return it;
}

inline RecordGroup::iterator RecordGroup::end()
{
	RecordGroup::iterator it;
	it.m_raMapIt = m_raMap.end();
	it.m_endIt = m_raMap.end();
	it.m_spRG = this;
	return it;
}

inline RecordGroup::iterator::iterator(const RecordGroup::iterator& it)
{
	operator=(it);
}

inline RecordGroup::iterator& RecordGroup::iterator::operator=(
	const RecordGroup::iterator& it)
{
	m_raMapIt=it.m_raMapIt;
	m_endIt=it.m_endIt;
	m_spRG=it.m_spRG;
	m_filters=it.m_filters;
	return *this;
}

inline sp<RecordArray> &RecordGroup::iterator::operator*(void)
{
	return m_raMapIt->second;
}

inline sp<RecordArray> *RecordGroup::iterator::operator->(void)
{
	return &(m_raMapIt->second);
}

inline bool RecordGroup::iterator::check(void)
{
	if(m_raMapIt == m_endIt) { return false; }
	for(unsigned int i = 0; i < m_filters.size(); i++)
	{
		if(!m_filters[i].check(m_raMapIt->second))
		{
			return false;
		}
	}
	return true;
}
inline RecordGroup::iterator RecordGroup::iterator::operator++(void)
{
	do
	{
		m_raMapIt++;
	}
	while(!check() && m_raMapIt != m_endIt);
	return *this;
}

inline RecordGroup::iterator RecordGroup::iterator::operator++(int dummy)
{
	RecordGroup::iterator it = *this;
	do
	{
		m_raMapIt++;
	}
	while(!check() && m_raMapIt != m_endIt);
	return it;
}

inline bool RecordGroup::iterator::operator==(
		const RecordGroup::iterator &other)
{
	return (m_raMapIt==other.m_raMapIt);
}

inline bool RecordGroup::iterator::operator!=(
		const RecordGroup::iterator &other)
{
	return (m_raMapIt!=other.m_raMapIt);
}

inline sp<RecordGroup> RecordGroup::iterator::group(void)
{
	return m_spRG;
}

} /* namespace */

#endif /* __data_RecordGroup_h__ */

