import sys
import os
forge = sys.modules["forge"]

def prerequisites():
    return [ "viewer" ]

def setup(module):

    srcList = [ "architecture.pmh",
                "Annotate",
                "AppDebugWindow",
                "Assign",
#               "ConfiguredApplication",
                "Executor",
                "Fork",
                "GroupExtract",
                "PaletteMap",
                "SimulationPipeline",
                "WindowPipeline",
                "PlayerPipeline",
                "architectureDL" ]

    dll = module.DLL( "fexArchitectureDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolLib",
                "fexViewerLib",
                ]

    forge.deps( ["fexArchitectureDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexArchitectureDL",    None,               None) ]

    module.Module( 'test' )
