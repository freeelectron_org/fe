import os
import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["terminal"]

def find_ptex_so():
    ptex_lib = os.environ["FE_PTEX_LIB"]

    libFiles = os.listdir(ptex_lib)
    for libFile in libFiles:
        if re.match(".*\.so", libFile):
            return libFile

    return "libptex.so.1.1"

def setup(module):
    module.summary = []

    ptex_include = os.environ["FE_PTEX_INCLUDE"]
    ptex_lib = os.environ["FE_PTEX_LIB"]

    srcList = [ "SurfaceAccessiblePtex",
                "ptex.pmh",
                "ptexDL" ]

    dll = module.DLL( "fexPtexDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib",
                "fexTerminalDLLib" ]

    forge.deps( ["fexPtexDLLib"], deplibs )

#   module.includemap = { 'ptex' : module.modPath }
    module.includemap = { 'ptex' : ptex_include }

    module.linkmap = {}

    soFile = find_ptex_so()

    module.linkmap['ptex'] = "-Wl,-rpath='" + ptex_lib + "'"
    module.linkmap['ptex'] += ' -L' + ptex_lib + ' -l:' + soFile

    forge.tests += [
        ("inspect.exe",     "fexPtexDL",                None,       None) ]

    for line in open(ptex_include + "/Ptexture.h").readlines():
        if re.match("#define PtexAPIVersion.*", line):
            version = line.split(' ')[2].rstrip()
            module.summary += [ version ]
            break

#   module.Module('ptex')

def auto(module):
    ptex_include = os.environ["FE_PTEX_INCLUDE"]
    ptex_lib = os.environ["FE_PTEX_LIB"]

    if ptex_include == '':
        return 'unset'

    if not os.path.exists(ptex_include):
        return 'include path'

    if not os.path.exists(ptex_lib):
        return 'lib path'

    test_file = """
#include "Ptexture.h"

int main(void)
{
    return(0);
}
    \n"""

    forge.includemap['ptex'] = ptex_include

    soFile = find_ptex_so()

    forge.linkmap['ptex'] = "-Wl,-rpath='" + ptex_lib + "'"
    forge.linkmap['ptex'] += ' -L' + ptex_lib + ' -l:' + soFile

    result = forge.cctest(test_file)

    forge.includemap.pop('ptex', None)
    forge.linkmap.pop('ptex', None)

    return result
