/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

class BaseHandled: public Handled<BaseHandled>, CastableAs<BaseHandled>
{
};

class MyHandled: public BaseHandled, CastableAs<MyHandled>
{
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		sp<BaseHandled> spBaseHandled(new MyHandled());
		UNIT_TEST(spBaseHandled.isValid());

		hp<BaseHandled> handle=spBaseHandled;
		UNIT_TEST(handle.isValid());
		UNIT_TEST(handle.raw()==spBaseHandled.raw());

		hp<BaseHandled> handle2=handle;
		UNIT_TEST(handle2.isValid());
		UNIT_TEST(handle2.raw()==spBaseHandled.raw());

		hp<MyHandled> handle3=handle;
		UNIT_TEST(handle3.isValid());

		handle3=handle2;
		UNIT_TEST(handle3.isValid());

		sp<MyHandled> spMyHandled=handle;
		UNIT_TEST(spMyHandled.isValid());
		UNIT_TEST(spBaseHandled==spMyHandled);

		spMyHandled=handle2;
		UNIT_TEST(spMyHandled.isValid());
		UNIT_TEST(spBaseHandled==spMyHandled);

		spBaseHandled=NULL;
		UNIT_TEST(handle.isValid());
		UNIT_TEST(handle2.isValid());
		UNIT_TEST(handle3.isValid());

		spMyHandled=NULL;
		UNIT_TEST(!handle.isValid());
		UNIT_TEST(!handle2.isValid());
		UNIT_TEST(!handle3.isValid());

		completed=TRUE;
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	UNIT_TEST(completed);
	UNIT_TRACK(18);
	UNIT_RETURN();
}
