import os
import sys
forge = sys.modules["forge"]

def setup(module):
    # the library
    srcList = [ "signal.pmh",
                "ChainSignaler",
                "PushSequencer",
                "Signature",
                "Dispatch",
                "LogHandler",
                "Reactor",
                "signalLib"
                ]

    lib = module.Lib("fexSignal",srcList)

    liblist = forge.corelibs + [ "fexSignalLib" ]

    # the plugin
    dll = module.DLL( "fexSignalDL", [ "signal.pmh", "signalDL" ] )
    forge.deps( ["fexSignalDLLib"], liblist )
#   dll.AddDep(lib)

    forge.tests += [
        ("inspect.exe",     "fexSignalDL",                  None,       None) ]

    module.Module('test')
