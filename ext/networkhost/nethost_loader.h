/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

#include <cstdio>
#include <stdlib.h>
#include <cstring>
#include <typeinfo>

#define FE_SPACE_VERBOSE	FALSE

typedef int (FnFeAccessSpace)
		(const char*,const char*);
typedef int (FnFeSpaceStart)
		(int);
typedef int (FnFeSpaceStop)
		(int);
typedef int (FnFeSpaceWaitForConnection)
		(int);
typedef int (FnFeSpaceFlush)
		(int);
typedef int (FnFeSpaceWaitForUpdate)
		(int,int*,int*);
typedef int (FnFeSpaceLockAfterUpdate)
		(int,int*,int*);
typedef int (FnFeSpaceLock)
		(int);
typedef int (FnFeSpaceUnlock)
		(int);
typedef int (FnFeSpaceSetStateBools)
		(int,const char*,const char*,const bool*,int);
typedef int (FnFeSpaceGetStateBools)
		(int,const char*,const char*,bool*,int,bool);
typedef int (FnFeSpaceSetStateInts)
		(int,const char*,const char*,const int*,int);
typedef int (FnFeSpaceGetStateInts)
		(int,const char*,const char*,int*,int,bool);
typedef int (FnFeSpaceSetStateFloats)
		(int,const char*,const char*,const float*,int);
typedef int (FnFeSpaceGetStateFloats)
		(int,const char*,const char*,float*,int,bool);
typedef int (FnFeSpaceSetStateString)
		(int,const char*,const char*,const char*);
typedef char* (FnFeSpaceGetStateString)
		(int,const char*,const char*,bool);
typedef int (FnFeSpaceSetStateBytes)
		(int,const char*,const char*,const unsigned char*,int);
typedef unsigned char* (FnFeSpaceGetStateBytes)
		(int,const char*,const char*,bool,int*);
typedef int (FnFeSpaceSendMessageString)
		(int,const char*,const char*);
typedef char* (FnFeSpaceNextMessageString)
		(int,const char*);
typedef int (FnFeFreeString)
		(const char*);

inline FnFeAccessSpace*& fpFeAccessSpace(void)
{
	static FnFeAccessSpace* s_fpFeAccessSpace=NULL;
	return s_fpFeAccessSpace;
}

inline FnFeSpaceStart*& fpFeSpaceStart(void)
{
	static FnFeSpaceStart* s_fpFeSpaceStart=NULL;
	return s_fpFeSpaceStart;
}

inline FnFeSpaceStop*& fpFeSpaceStop(void)
{
	static FnFeSpaceStop* s_fpFeSpaceStop=NULL;
	return s_fpFeSpaceStop;
}

inline FnFeSpaceWaitForConnection*& fpFeSpaceWaitForConnection(void)
{
	static FnFeSpaceWaitForConnection* s_fpFeSpaceWaitForConnection=NULL;
	return s_fpFeSpaceWaitForConnection;
}

inline FnFeSpaceFlush*& fpFeSpaceFlush(void)
{
	static FnFeSpaceFlush* s_fpFeSpaceFlush=NULL;
	return s_fpFeSpaceFlush;
}

inline FnFeSpaceWaitForUpdate*& fpFeSpaceWaitForUpdate(void)
{
	static FnFeSpaceWaitForUpdate* s_fpFeSpaceWaitForUpdate=NULL;
	return s_fpFeSpaceWaitForUpdate;
}

inline FnFeSpaceLockAfterUpdate*& fpFeSpaceLockAfterUpdate(void)
{
	static FnFeSpaceLockAfterUpdate* s_fpFeSpaceLockAfterUpdate=NULL;
	return s_fpFeSpaceLockAfterUpdate;
}

inline FnFeSpaceLock*& fpFeSpaceLock(void)
{
	static FnFeSpaceLock* s_fpFeSpaceLock=NULL;
	return s_fpFeSpaceLock;
}

inline FnFeSpaceUnlock*& fpFeSpaceUnlock(void)
{
	static FnFeSpaceUnlock* s_fpFeSpaceUnlock=NULL;
	return s_fpFeSpaceUnlock;
}

inline FnFeSpaceSetStateBools*& fpFeSpaceSetStateBools(void)
{
	static FnFeSpaceSetStateBools* s_fpFeSpaceSetStateBools=NULL;
	return s_fpFeSpaceSetStateBools;
}

inline FnFeSpaceGetStateBools*& fpFeSpaceGetStateBools(void)
{
	static FnFeSpaceGetStateBools* s_fpFeSpaceGetStateBool=NULL;
	return s_fpFeSpaceGetStateBool;
}

inline FnFeSpaceSetStateInts*& fpFeSpaceSetStateInts(void)
{
	static FnFeSpaceSetStateInts* s_fpFeSpaceSetStateInts=NULL;
	return s_fpFeSpaceSetStateInts;
}

inline FnFeSpaceGetStateInts*& fpFeSpaceGetStateInts(void)
{
	static FnFeSpaceGetStateInts* s_fpFeSpaceGetStateInts=NULL;
	return s_fpFeSpaceGetStateInts;
}

inline FnFeSpaceSetStateFloats*& fpFeSpaceSetStateFloats(void)
{
	static FnFeSpaceSetStateFloats* s_fpFeSpaceSetStateFloats=NULL;
	return s_fpFeSpaceSetStateFloats;
}

inline FnFeSpaceGetStateFloats*& fpFeSpaceGetStateFloats(void)
{
	static FnFeSpaceGetStateFloats* s_fpFeSpaceGetStateFloats=NULL;
	return s_fpFeSpaceGetStateFloats;
}

inline FnFeSpaceSetStateString*& fpFeSpaceSetStateString(void)
{
	static FnFeSpaceSetStateString* s_fpFeSpaceSetStateString=NULL;
	return s_fpFeSpaceSetStateString;
}

inline FnFeSpaceGetStateString*& fpFeSpaceGetStateString(void)
{
	static FnFeSpaceGetStateString* s_fpFeSpaceGetStateString=NULL;
	return s_fpFeSpaceGetStateString;
}

inline FnFeSpaceSetStateBytes*& fpFeSpaceSetStateBytes(void)
{
	static FnFeSpaceSetStateBytes* s_fpFeSpaceSetStateBytes=NULL;
	return s_fpFeSpaceSetStateBytes;
}

inline FnFeSpaceGetStateBytes*& fpFeSpaceGetStateBytes(void)
{
	static FnFeSpaceGetStateBytes* s_fpFeSpaceGetStateBytes=NULL;
	return s_fpFeSpaceGetStateBytes;
}

inline FnFeSpaceSendMessageString*& fpFeSpaceSendMessageString(void)
{
	static FnFeSpaceSendMessageString* s_fpFeSpaceSendMessageString=NULL;
	return s_fpFeSpaceSendMessageString;
}

inline FnFeSpaceNextMessageString*& fpFeSpaceNextMessageString(void)
{
	static FnFeSpaceNextMessageString* s_fpFeSpaceNextMessageString=NULL;
	return s_fpFeSpaceNextMessageString;
}

inline FnFeFreeString*& fpFeFreeString(void)
{
	static FnFeFreeString* s_fpFeFreeString=NULL;
	return s_fpFeFreeString;
}

#ifdef _WIN32
inline void* fe_nethost_dlsym(HINSTANCE a_handle,const char* a_symbol)
{
	return GetProcAddress(a_handle,a_symbol);
}
#else
inline void* fe_nethost_dlsym(void* a_handle,const char* a_symbol)
{
	return dlsym(a_handle,a_symbol);
}
#endif

inline int fe_nethost_nethost_loader_init(void)
{
	const int size(4096);
	char suffix[size];
	suffix[0]=0;

#ifdef FE_BINARY_SUFFIX
	#define AS_STRING2(x)	#x
	#define AS_STRING(x)	AS_STRING2(x)

	strncpy(suffix,AS_STRING(FE_BINARY_SUFFIX),size);
#endif

#ifdef _WIN32
	char val[size];
	const unsigned long cp=
			GetEnvironmentVariable("FE_BINARY_SUFFIX", val, size);
	if(cp)
	{
		strncpy(suffix,val,size);
	}

	char filename[size+100];
	snprintf(filename,size,"fexNetworkHostDL%s.dll",suffix);

	HINSTANCE handle=LoadLibrary(filename);

	if(!handle)
	{
		printf("fe_nethost_nethost_loader_init LoadLibrary \"%s\" failed\n",
				filename);
		exit(1);
	}
#else
	const char* cp=getenv("FE_BINARY_SUFFIX");
	if(cp)
	{
		strncpy(suffix,cp,size);
	}

	char filename[size+100];
	snprintf(filename,size,"libfexNetworkHostDL%s.so",suffix);

	dlerror();
	void* handle=dlopen(filename,RTLD_NOW);
	if(!handle)
	{
		printf("fe_nethost_nethost_loader_init dlopen \"%s\" failed\n  %s\n",
				filename,dlerror());
		exit(1);
	}
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

	fpFeAccessSpace()=(FnFeAccessSpace*)
			fe_nethost_dlsym(handle,"fe_nethost_access_space");
	fpFeSpaceStart()=(FnFeSpaceStart*)
			fe_nethost_dlsym(handle,"fe_nethost_space_start");
	fpFeSpaceStop()=(FnFeSpaceStop*)
			fe_nethost_dlsym(handle,"fe_nethost_space_stop");
	fpFeSpaceWaitForConnection()=(FnFeSpaceWaitForConnection*)
			fe_nethost_dlsym(handle,"fe_nethost_space_wait_for_connection");
	fpFeSpaceFlush()=(FnFeSpaceFlush*)
			fe_nethost_dlsym(handle,"fe_nethost_space_flush");
	fpFeSpaceWaitForUpdate()=(FnFeSpaceWaitForUpdate*)
			fe_nethost_dlsym(handle,"fe_nethost_space_wait_for_update");
	fpFeSpaceLockAfterUpdate()=(FnFeSpaceLockAfterUpdate*)
			fe_nethost_dlsym(handle,"fe_nethost_space_lock_after_update");
	fpFeSpaceLock()=(FnFeSpaceLock*)
			fe_nethost_dlsym(handle,"fe_nethost_space_lock");
	fpFeSpaceUnlock()=(FnFeSpaceUnlock*)
			fe_nethost_dlsym(handle,"fe_nethost_space_unlock");
	fpFeSpaceSetStateBools()=(FnFeSpaceSetStateBools*)
			fe_nethost_dlsym(handle,"fe_nethost_space_set_state_bools");
	fpFeSpaceGetStateBools()=(FnFeSpaceGetStateBools*)
			fe_nethost_dlsym(handle,"fe_nethost_space_get_state_bools");
	fpFeSpaceSetStateInts()=(FnFeSpaceSetStateInts*)
			fe_nethost_dlsym(handle,"fe_nethost_space_set_state_ints");
	fpFeSpaceGetStateInts()=(FnFeSpaceGetStateInts*)
			fe_nethost_dlsym(handle,"fe_nethost_space_get_state_ints");
	fpFeSpaceSetStateFloats()=(FnFeSpaceSetStateFloats*)
			fe_nethost_dlsym(handle,"fe_nethost_space_set_state_floats");
	fpFeSpaceGetStateFloats()=(FnFeSpaceGetStateFloats*)
			fe_nethost_dlsym(handle,"fe_nethost_space_get_state_floats");
	fpFeSpaceSetStateString()=(FnFeSpaceSetStateString*)
			fe_nethost_dlsym(handle,"fe_nethost_space_set_state_string");
	fpFeSpaceGetStateString()=(FnFeSpaceGetStateString*)
			fe_nethost_dlsym(handle,"fe_nethost_space_get_state_string");
	fpFeSpaceSetStateBytes()=(FnFeSpaceSetStateBytes*)
			fe_nethost_dlsym(handle,"fe_nethost_space_set_state_bytes");
	fpFeSpaceGetStateBytes()=(FnFeSpaceGetStateBytes*)
			fe_nethost_dlsym(handle,"fe_nethost_space_get_state_bytes");
	fpFeSpaceSendMessageString()=(FnFeSpaceSendMessageString*)
			fe_nethost_dlsym(handle,"fe_nethost_space_send_message_string");
	fpFeSpaceNextMessageString()=(FnFeSpaceNextMessageString*)
			fe_nethost_dlsym(handle,"fe_nethost_space_next_message_string");
	fpFeFreeString()=(FnFeFreeString*)
			fe_nethost_dlsym(handle,"fe_nethost_free_string");

#pragma GCC diagnostic pop

	return 0;
}

inline int fe_nethost_access_space(const char* a_name,const char* a_implementation)
{
	if(!fpFeAccessSpace())
	{
		fe_nethost_nethost_loader_init();
	}

	return fpFeAccessSpace() &&
			fpFeAccessSpace()(a_name,a_implementation);
}

inline int fe_nethost_access_space(const char* a_name)
{
	return fe_nethost_access_space(a_name,"*.NetworkCatalog");
}

inline int fe_nethost_space_start(int a_spaceIndex)
{
	return fpFeSpaceStart() && fpFeSpaceStart()(a_spaceIndex);
}

inline int fe_nethost_space_stop(int a_spaceIndex)
{
	return fpFeSpaceStop() && fpFeSpaceStop()(a_spaceIndex);
}

inline int fe_nethost_space_wait_for_connection(int a_spaceIndex)
{
	return fpFeSpaceWaitForConnection() &&
			fpFeSpaceWaitForConnection()(a_spaceIndex);
}

inline int fe_nethost_space_flush(int a_spaceIndex)
{
	return fpFeSpaceFlush() && fpFeSpaceFlush()(a_spaceIndex);
}

inline int fe_nethost_space_wait_for_update(int a_spaceIndex,
	int* a_pFlushCount,int* a_pSpins)
{
	return fpFeSpaceWaitForUpdate() &&
			fpFeSpaceWaitForUpdate()(a_spaceIndex,a_pFlushCount,a_pSpins);
}

inline int fe_nethost_space_lock_after_update(int a_spaceIndex,
	int* a_pFlushCount,int* a_pSpins)
{
	return fpFeSpaceLockAfterUpdate() &&
			fpFeSpaceLockAfterUpdate()(a_spaceIndex,
			a_pFlushCount,a_pSpins);
}

inline int fe_nethost_space_lock(int a_spaceIndex)
{
	return fpFeSpaceLock() && fpFeSpaceLock()(a_spaceIndex);
}

inline int fe_nethost_space_unlock(int a_spaceIndex)
{
	return fpFeSpaceUnlock() && fpFeSpaceUnlock()(a_spaceIndex);
}

template <class T>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,const T* a_values,int a_count)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<%s *> %d \"%s\" \"%s\" %d (NOP)\n",
			typeid(T).name(),a_spaceIndex,a_key,a_property,a_count);
#endif
	return 0;
}

template <class T>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,const T a_value)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<%s> %d \"%s\" \"%s\"\n",
			typeid(T).name(),a_spaceIndex,a_key,a_property);
#endif
	return fe_nethost_space_set_state(a_spaceIndex,a_key,a_property,&a_value,1);
}

template <class T>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const T a_value)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<%s> %d \"%s\"\n",
			typeid(T).name(),a_spaceIndex,a_key);
#endif
	return fe_nethost_space_set_state<T>(a_spaceIndex,a_key,"value",a_value);
}

template <class T>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,T* a_value)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<%s *> %d \"%s\"\n",
			typeid(T).name(),a_spaceIndex,a_key);
#endif
	return fe_nethost_space_set_state<const T*>(a_spaceIndex,a_key,"value",a_value);
}

template <class T>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const T* a_values,int a_count)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<%s *> %d \"%s\" %d\n",
			typeid(T).name(),a_spaceIndex,a_key,a_count);
#endif
	return fe_nethost_space_set_state(a_spaceIndex,a_key,"value",
			a_values,a_count);
}

template <class T>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,T a_value,int a_size,bool a_safe)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_get_state<%s> \"%s\" \"%s\" (NOP)\n",
			typeid(T).name(),a_key,a_property);
#endif
	return 0;
}

template <class T>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,T a_value,int a_size,bool a_safe)
{
	return fe_nethost_space_get_state(a_spaceIndex,a_key,"value",
			a_value,a_size,a_safe);
}

template <class T>
int fe_nethost_space_get_state_dynamic(int a_spaceIndex,
	const char* a_key,const char* a_property,T* a_pPointer,bool a_safe)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_get_state_dynamic<%s> \"%s\" \"%s\" (NOP)\n",
			typeid(T).name(),a_key,a_property);
#endif
	return 0;
}

template <class T>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	T* a_values,int a_count,bool a_safe)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_get_state<%s> \"%s\" \"%s\" (NOP)\n",
			typeid(T).name(),a_key,a_property);
#endif
	return 0;
}

template <class T>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,T* a_values,int a_count,bool a_safe)
{
	return fe_nethost_space_get_state(a_spaceIndex,a_key,"value",
			a_values,a_count,a_safe);
}

template <class T>
int fe_nethost_space_send_message(int a_spaceIndex,
	const char* a_key,const T a_value)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_send_message<%s> \"%s\" (NOP)\n",
			typeid(T).name(),a_key);
#endif
	return 0;
}

template <class T>
T* fe_nethost_space_next_message(int a_spaceIndex,const char* a_key)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_next_message<%s> \"%s\" (NOP)\n",
			typeid(T).name(),a_key);
#endif
	return 0;
}

//* bools
template <>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,const bool* a_values,int a_count)
{
	return fpFeSpaceSetStateBools() &&
			fpFeSpaceSetStateBools()(a_spaceIndex,a_key,a_property,
			a_values,a_count);
}

template <>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	bool* a_values,int a_count,bool a_safe)
{
	return fpFeSpaceGetStateBools() &&
			fpFeSpaceGetStateBools()(a_spaceIndex,a_key,a_property,
			a_values,a_count,a_safe);
}

//* ints
template <>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,const int* a_values,int a_count)
{
	return fpFeSpaceSetStateInts() &&
			fpFeSpaceSetStateInts()(a_spaceIndex,a_key,a_property,
			a_values,a_count);
}

template <>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	int* a_values,int a_count,bool a_safe)
{
	return fpFeSpaceGetStateInts() &&
			fpFeSpaceGetStateInts()(a_spaceIndex,a_key,a_property,
			a_values,a_count,a_safe);
}

//* floats
template <>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,const float* a_values,int a_count)
{
	return fpFeSpaceSetStateFloats() &&
			fpFeSpaceSetStateFloats()(a_spaceIndex,a_key,a_property,
			a_values,a_count);
}

template <>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	float* a_values,int a_count,bool a_safe)
{
	return fpFeSpaceGetStateFloats() &&
			fpFeSpaceGetStateFloats()(a_spaceIndex,a_key,a_property,
			a_values,a_count,a_safe);
}

//* string
template <>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,const char* a_value)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<const char*> %d \"%s\" \"%s\" \"%s\"\n",
			a_spaceIndex,a_key,a_property,a_value);
#endif
	return fpFeSpaceSetStateString() &&
			fpFeSpaceSetStateString()(a_spaceIndex,a_key,a_property,
			a_value);
}

template <>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	char* a_value,int a_size,bool a_safe)
{
	if(!fpFeSpaceGetStateString())
	{
		return 0;
	}

	char* buffer=
			fpFeSpaceGetStateString()(a_spaceIndex,a_key,a_property,a_safe);
	if(!buffer)
	{
		return 0;
	}

	strncpy(a_value,buffer,a_size-1);
	a_value[a_size-1]=0;

	free(buffer);
	return 1;
}

template <>
int fe_nethost_space_get_state_dynamic(int a_spaceIndex,
	const char* a_key,const char* a_property,
	char** a_pValue,bool a_safe)
{
	*a_pValue=NULL;

	if(!fpFeSpaceGetStateString())
	{
		return 0;
	}

	char* buffer=
			fpFeSpaceGetStateString()(a_spaceIndex,a_key,a_property,a_safe);
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_get_state_dynamic<char*> buffer \"%s\"\n",
			buffer);
#endif
	if(!buffer)
	{
		return 0;
	}

	*a_pValue=strdup(buffer);

	return strlen(*a_pValue);
}

//* bytes
template <>
int fe_nethost_space_set_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	const unsigned char* a_value,int a_size)
{
#if FE_SPACE_VERBOSE
	printf("fe_nethost_space_set_state<const uchar*> %d \"%s\" \"%s\"\n",
			a_spaceIndex,a_key,a_property);
#endif

	return fpFeSpaceSetStateBytes() &&
			fpFeSpaceSetStateBytes()(a_spaceIndex,a_key,a_property,
			a_value,a_size);
}

template <>
int fe_nethost_space_get_state(int a_spaceIndex,
	const char* a_key,const char* a_property,
	unsigned char* a_value,int a_size,bool a_safe)
{
	if(!fpFeSpaceGetStateBytes())
	{
		return 0;
	}

	int bytesRead(0);
	unsigned char* buffer=fpFeSpaceGetStateBytes()(
			a_spaceIndex,a_key,a_property,a_safe,&bytesRead);
	if(!buffer)
	{
		return 0;
	}

	int bytesCopied=a_size<bytesRead? a_size: bytesRead;
	memcpy(a_value,buffer,bytesCopied);

	free(buffer);
	return bytesCopied;
}

template <>
int fe_nethost_space_get_state_dynamic(int a_spaceIndex,
	const char* a_key,const char* a_property,
	unsigned char** a_pValue,bool a_safe)
{
	*a_pValue=NULL;

	if(!fpFeSpaceGetStateBytes())
	{
		return 0;
	}

	int bytesRead(0);
	unsigned char* buffer=fpFeSpaceGetStateBytes()(
			a_spaceIndex,a_key,a_property,a_safe,&bytesRead);
	if(!buffer)
	{
		return 0;
	}

	*a_pValue=(unsigned char*)malloc(bytesRead);
	memcpy(*a_pValue,buffer,bytesRead);

	return bytesRead;
}

//* message
template <>
int fe_nethost_space_send_message(int a_spaceIndex,
	const char* a_key,const char* a_value)
{
	return fpFeSpaceSendMessageString() &&
			fpFeSpaceSendMessageString()(a_spaceIndex,a_key,a_value);
}

template <>
char* fe_nethost_space_next_message(int a_spaceIndex,const char* a_key)
{
	if(!fpFeSpaceNextMessageString())
	{
		return NULL;
	}

	return fpFeSpaceNextMessageString()(a_spaceIndex,a_key);
}

namespace feSym
{

class Space
{
	public:

	template <class T>
	class ScopedBuffer
	{
		public:
				ScopedBuffer(T a_pBuffer,int a_size):
					m_pBuffer(a_pBuffer),
					m_size(a_size),
					m_pCount(&m_countBacking),
					m_countBacking(1)										{}

				ScopedBuffer(const ScopedBuffer<T>& a_rOther):
					m_pBuffer(a_rOther.m_pBuffer),
					m_size(a_rOther.m_size),
					m_pCount(a_rOther.m_pCount),
					m_countBacking(-1)
				{	(*m_pCount)++; }

				~ScopedBuffer(void)
				{
					if(!(--(*m_pCount))) free(m_pBuffer);
				}

	const	T	data(void) const	{ return m_pBuffer; }
			int	size(void) const	{ return m_size; }

		private:
			T		m_pBuffer;
			int		m_size;
			int*	m_pCount;
			int		m_countBacking;
	};

	class Atomic
	{
		public:
				Atomic(Space& a_rSpace):
					m_rSpace(a_rSpace)
				{
					if(!m_rSpace.lock())
					{
						printf("Space::Atomic::Atomic failed to lock\n");
					}
				}
				Atomic(Space& a_rSpace,int& a_rFlushCount):
					m_rSpace(a_rSpace)
				{
					if(!m_rSpace.lockAfterUpdate(a_rFlushCount,m_spinCount))
					{
						printf("Space::Atomic::Atomic failed to lock\n");
					}
				}
				~Atomic(void)
				{
					if(!m_rSpace.unlock())
					{
						printf("Space::Atomic::~Atomic failed to unlock\n");
					}
				}

				template <class T>
			int	getState(const char* a_key,const char* a_property,
						T a_value,int a_size) const
				{	return m_rSpace.getStateUnsafe(a_key,a_property,
							a_value,a_size); }

				template <class T>
			int	getState(const char* a_key,T a_value,int a_size) const
				{	return getState(a_key,"value",a_value,a_size); }

				template <class T>
			ScopedBuffer<T> getStateScoped(const char* a_key,
						const char* a_property) const
				{	return m_rSpace.getStateScopedUnsafe<T>(a_key,a_property); }

				template <class T>
			ScopedBuffer<T> getStateScoped(const char* a_key) const
				{	return getStateScoped<T>(a_key,"value"); }

			int	spinCount(void) const
				{	return m_spinCount; }

		private:
			Space&	m_rSpace;
			int		m_spinCount;
	};

			Space(int a_spaceIndex):
				m_spaceIndex(a_spaceIndex)									{}

		int start(void)	{ return fe_nethost_space_start(m_spaceIndex); }
		int stop(void)	{ return fe_nethost_space_stop(m_spaceIndex); }

		int waitForConnection(void)
			{	return fe_nethost_space_wait_for_connection(m_spaceIndex); }

		int flush(void)	{ return fe_nethost_space_flush(m_spaceIndex); }

		int lock(void)	{ return fe_nethost_space_lock(m_spaceIndex); }
		int unlock(void)	{ return fe_nethost_space_unlock(m_spaceIndex); }

		int waitForUpdate(int& a_rFlushCount,int& a_rSpins)
			{	return fe_nethost_space_wait_for_update(m_spaceIndex,
						&a_rFlushCount,&a_rSpins); }
		int waitForUpdate(int& a_rFlushCount)
			{	int spinCount(0);
				return fe_nethost_space_wait_for_update(m_spaceIndex,
						&a_rFlushCount,&spinCount); }

		int lockAfterUpdate(int& a_rFlushCount,int& a_rSpins)
			{	return fe_nethost_space_lock_after_update(m_spaceIndex,
						&a_rFlushCount,&a_rSpins); }
		int lockAfterUpdate(int& a_rFlushCount)
			{	int spinCount(0);
				return fe_nethost_space_lock_after_update(m_spaceIndex,
						&a_rFlushCount,&spinCount); }

			template <class T>
		int	setState(const char* a_key,const char* a_property,const T a_value)
			{	return fe_nethost_space_set_state(m_spaceIndex,a_key,a_property,
						a_value); }

			template <class T>
		int	setState(const char* a_key,const T a_value)
			{	return fe_nethost_space_set_state(m_spaceIndex,a_key,a_value); }

			template <class T>
		int	setState(const char* a_key,const char* a_property,
				const T a_value,int a_size)
			{	return fe_nethost_space_set_state(m_spaceIndex,a_key,a_property,
						a_value,a_size); }

			template <class T>
		int	setState(const char* a_key,const T a_value,int a_size)
			{	return fe_nethost_space_set_state(m_spaceIndex,a_key,
						a_value,a_size); }

			template <class T>
		int	getState(const char* a_key,const char* a_property,
					T a_value,int a_size) const
			{	return fe_nethost_space_get_state(m_spaceIndex,a_key,a_property,
						a_value,a_size,true); }

			template <class T>
		int	getState(const char* a_key,T a_value,int a_size) const
			{	return fe_nethost_space_get_state(m_spaceIndex,a_key,
						a_value,a_size,true); }

			template <class T>
		int	getStateUnsafe(const char* a_key,const char* a_property,
					T a_value,int a_size) const
			{	return fe_nethost_space_get_state(m_spaceIndex,
						a_key,a_property,a_value,a_size,false); }

			template <class T>
		ScopedBuffer<T>	getStateScoped(const char* a_key,
						const char* a_property) const
			{
				T pPointer(NULL);
				const int size=
						fe_nethost_space_get_state_dynamic<T>(m_spaceIndex,
						a_key,a_property,&pPointer,true);
				return ScopedBuffer<T>(pPointer,size);
			}

			template <class T>
		ScopedBuffer<T>	getStateScoped(const char* a_key) const
			{	return getStateScoped<T>(a_key,"value"); }

			template <class T>
		ScopedBuffer<T>	getStateScopedUnsafe(const char* a_key,
						const char* a_property) const
			{
				T pPointer(NULL);
				const int size=
						fe_nethost_space_get_state_dynamic<T>(m_spaceIndex,
						a_key,a_property,&pPointer,false);
				return ScopedBuffer<T>(pPointer,size);
			}

			template <class T>
		ScopedBuffer<T>	getStateScopedUnsafe(const char* a_key) const
			{	return getStateScopedUnsafe<T>(a_key,"value"); }

			template <class T>
		int	sendMessage(const char* a_key,const T* a_value)
			{	return fe_nethost_space_send_message(m_spaceIndex,
						a_key,a_value); }

	private:
		int	m_spaceIndex;
};

inline Space accessSpace(const char* a_name,
	const char* a_implementation="*.NetworkCatalog")
{
	return Space(fe_nethost_access_space(a_name,a_implementation));
}

}
