/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

//*	derived from Houndini sample: SOP_Surface

#ifndef __houdini_HoudiniSOP_h__
#define __houdini_HoudiniSOP_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini SOP node

	@ingroup houdini
*//***************************************************************************/
class HoudiniSOP:
	public SOP_Node,
	public MetaPlugin
{
	public:

	class OpOperator:
		public OP_Operator
	{
		public:
								OpOperator(const char *name,
										const char *english,
										OP_Constructor construct,
										PRM_Template *templates,
										unsigned min_sources,
										unsigned max_sources,
										CH_LocalVariable *variables,
										unsigned flags,
										const char **inputlabels,
										sp<Catalog> a_spCatalog);

								//* as OP_Operator
	virtual	bool				getOpHelpURL(UT_String& a_rUtString) override;

#if FE_HOUDINI_13_PLUS
    virtual bool				wantsInputEditor(void) const override
								{	return false; }
#endif

		private:
			String				m_helpUrl;
	};

	class UndoCallback:
		public UT_Undo
	{
		public:
									UndoCallback(HoudiniSOP* a_pHoudiniSOP,
											String a_change,
											sp<Catalog> a_spNewCatalog,
											sp<Counted> a_spOldCounted,
											sp<Counted> a_spNewCounted);

	virtual	bool					isValid(void) override;
	virtual	void					undo(void) override	{ undoOrRedo(FALSE); }
	virtual	void					redo(void) override	{ undoOrRedo(TRUE); }

		private:

	virtual	void					undoOrRedo(BWORD a_redo);

			HoudiniSOP*				m_pHoudiniSOP;
			String					m_change;
			sp<Catalog>				m_spNewCatalog;
			sp<Counted>				m_spOldCounted;
			sp<Counted>				m_spNewCounted;
	};

								HoudiniSOP(OP_Network *pNetwork,
										const char *name,OP_Operator *pOp);
virtual							~HoudiniSOP(void);

								//* as OperatorPlugin
virtual	void					dirty(BWORD a_aggressive) override;
virtual	void					select(void) override;

								//* as MetaPlugin
virtual	BWORD					chronicle(String a_change,
									sp<Counted> a_spOldCounted,
									sp<Counted> a_spNewCounted) override;
virtual	BWORD					interrupted(void) override;

								//* NOTE called by CacheOp
		BWORD					cookInputs(void);

static	int						discoverOperators(OP_OperatorTable *pTable);

								///	@internal
static	OP_Node*				create(OP_Network *pNetwork,const char *name,
										OP_Operator *pOp);

								///	@internal
static	int						reloadCallback(void *pData,int index,float time,
										const PRM_Template* pTemplate);

								///	@internal
		void					brush(GU_Detail* pGdp,WindowEvent& a_rEvent,
										SpatialVector& a_rRayOrigin,
										SpatialVector& a_rRayDirection);

		void					clearErrors(void);
		void					relayErrors(void);
const	String					prompt(void);
const	String					idleEvents(void);
const	String					brushVisible(void);
const	String					brushCook(void);

static	String					classOfImplementation(String a_implementation);

	protected:
								//* as OP_Node
virtual void					getDescriptiveName(
										UT_String& a_rString) const override;
virtual	OP_ERROR				bypassMe(OP_Context &context,
										int &copied_input) override;
virtual	void					opChanged(OP_EventType reason,
										void *data) override;
virtual	bool					load(UT_IStream& a_rStreamIn,
										const char* a_extension,
										const char* a_path) override;
virtual	OP_ERROR				save(std::ostream& a_rStreamOut,
										const OP_SaveFlags& a_rFlags,
										const char* a_pathPrefix,
										const UT_String& a_name_override)
										override;

								//* as SOP_Node
virtual OP_ERROR				cookMySop(OP_Context& a_rContext) override;
virtual	OP_ERROR				cookMyGuide1(OP_Context& a_rContext) override;

								//* as OP_Parameters
virtual	unsigned int			disableParms(void) override;

	protected:
virtual	bool					evalParamBoolean(String a_key,Real a_time);
virtual	I32						evalParamInteger(String a_key,Real a_time);
virtual	String					evalParamString(String a_key,Real a_time);

	private:
		void					createOperator(String a_operatorName,
										Real a_time);
		void					updateCatalog(Real time,BWORD a_doInput,
										BWORD a_force=FALSE);
		void					resignalBrush(void);

static	int						buttonCallback(void* a_pNode,int a_index,
										fpreal32 a_time,
										const PRM_Template* a_pPrmTemplate);

		HoudiniContext					m_houdiniContext;
		String							m_brushName;
		String							m_loadedState;
		double							m_frame;
		BWORD							m_cooking;
		BWORD							m_inputsLocked;
		BWORD							m_guideEmpty;
		BWORD							m_temporaryTemplate;

		OP_Operator*					m_pOpOperator;
		OP_Context*						m_pOpContext;
		UT_AutoInterrupt*				m_pInterrupt;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniSOP_h__ */
