/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <boostregex/boostregex.pmh>

#include "platform/dlCore.cc"

#define FE_BRE_DEBUG	FALSE

namespace fe
{
namespace ext
{

void* boostregex_init(const char* a_pattern)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_init \"%s\"\n",a_pattern);
#endif

	return new BOOST_NAMESPACE::regex(a_pattern);
}

void* boostregex_match(void* a_expression,const char* a_candidate)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_match \"%s\"\n",a_candidate);
#endif

	BOOST_NAMESPACE::regex& re=*(BOOST_NAMESPACE::regex*)a_expression;
	BOOST_NAMESPACE::match_results<std::string::const_iterator> what;
	std::string string=a_candidate;
	if(!regex_match(string, what, re))
	{
		return NULL;
	}
//	return new BOOST_NAMESPACE::match_results<std::string::const_iterator>(what);

	const U32 stringCount=what.length();

	Array<std::string>* pStringArray=new Array<std::string>(stringCount);

	for(U32 stringIndex=0;stringIndex<stringCount;stringIndex++)
	{
		(*pStringArray)[stringIndex]=what.str(stringIndex);
	}

	return pStringArray;
}

void* boostregex_search(void* a_expression,const char* a_candidate)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_search \"%s\"\n",a_candidate);
#endif

	BOOST_NAMESPACE::regex& re=*(BOOST_NAMESPACE::regex*)a_expression;
	BOOST_NAMESPACE::match_results<std::string::const_iterator> what;
	std::string string=a_candidate;
	if(!regex_search(string, what, re))
	{
		return NULL;
	}
//	return new BOOST_NAMESPACE::match_results<std::string::const_iterator>(what);

	const U32 stringCount=what.length();

	Array<std::string>* pStringArray=new Array<std::string>(stringCount);

	for(U32 stringIndex=0;stringIndex<stringCount;stringIndex++)
	{
		(*pStringArray)[stringIndex]=what.str(stringIndex);
	}

	return pStringArray;
}

void* boostregex_replace(void* a_expression,const char* a_candidate,
		const char* a_replacement)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_replace \"%s\"\n",a_candidate);
#endif

	BOOST_NAMESPACE::regex& re=*(BOOST_NAMESPACE::regex*)a_expression;
	std::string string=a_candidate;
	std::string replace=a_replacement;
	std::string result=regex_replace(string,re,replace).c_str();

	Array<std::string>* pStringArray=new Array<std::string>(1);
	(*pStringArray)[0]=result;

	return pStringArray;
}

const char* boostregex_result(void* a_result,U32 a_index)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_result %d\n",a_index);
#endif

//	BOOST_NAMESPACE::match_results<std::string::const_iterator>& what=
//			*(BOOST_NAMESPACE::match_results<std::string::const_iterator>*)a_result;
//	return what.str(a_index).c_str();

	Array<std::string>* pStringArray=(Array<std::string>*)a_result;
	return (*pStringArray)[a_index].c_str();
}

void boostregex_finish(void* a_expression)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_finish\n");
#endif

	BOOST_NAMESPACE::regex* expression=(BOOST_NAMESPACE::regex*)a_expression;
	delete expression;
}

void boostregex_release(void* a_result)
{
#if FE_BRE_DEBUG
	feLogError("boostregex_release\n");
#endif

//	BOOST_NAMESPACE::match_results<std::string::const_iterator>* pWhat=
//			(BOOST_NAMESPACE::match_results<std::string::const_iterator>*)a_result;
//	delete pWhat;

	Array<std::string>* pStringArray=(Array<std::string>*)a_result;
	delete pStringArray;
}

extern "C"
{

FE_DL_EXPORT bool regex_init(void)
{
#if FE_BRE_DEBUG
	feLog("BoostRegexDL regex_init()\n");
#endif

//	feLog("  using boost::regex\n");

	Regex::replaceInitFunction(boostregex_init);
	Regex::replaceMatchFunction(boostregex_match);
	Regex::replaceSearchFunction(boostregex_search);
	Regex::replaceReplaceFunction(boostregex_replace);
	Regex::replaceResultFunction(boostregex_result);
	Regex::replaceFinishFunction(boostregex_finish);
	Regex::replaceReleaseFunction(boostregex_release);
	return true;
}

}

} /* namespace ext */
} /* namespace fe */
