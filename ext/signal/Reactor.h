/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_Reactor_h__
#define __signal_Reactor_h__

#include "signal/ReactorI.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT Reactor :
	virtual public ReactorI,
	public Initialize<Reactor>
{
	public:
				Reactor(void);
virtual			~Reactor(void);

		void	initialize(void);

virtual	void	add(const String &a_name,
						sp<SignalerI> a_signaler, Record r_sig);
virtual	void	react(const String &a_name);
	private:
		class t_relay
		{
			public:
				sp<SignalerI>		m_spSignaler;
				Record				m_signal;
		};
		std::map<String, Array<t_relay> >	m_relays;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_Reactor_h__ */

