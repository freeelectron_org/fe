#!/usr/bin/python

import os
import sys
import time
import re
import string


# valgrind and python error codes both seem to have problems catching this
# backgrounded process
server_output = os.popen("xServer.exe", "r", -1)
time.sleep(1)
client_output = os.popen("xClient.exe", "r", -1)


line = client_output.readline()
while line:
    print "client: " + line
    line = client_output.readline()
c_ec = client_output.close()

line = server_output.readline()
while line:
    print "server: " + line
    line = server_output.readline()
s_ec = server_output.close()

exitval = 0
if s_ec != None:
    if sys.platform == "win32":
        exitval=s_ec
    else:
        signal= (int(str(s_ec))&255)
        exitval= ((int(str(s_ec))>>8)&255)
    sys.exit(exitval)

if c_ec != None:
    if sys.platform == "win32":
        exitval=c_ec
    else:
        signal= (int(str(c_ec))&255)
        exitval= ((int(str(c_ec))>>8)&255)
    sys.exit(exitval)

sys.exit(exitval)


