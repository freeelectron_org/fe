/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"
#include "math.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	const U32 testCount=32;
	BWORD completed=FALSE;

	try
	{
		String envCiBuild(FALSE);
		System::getEnvironmentVariable("FE_CI_BUILD", envCiBuild);
		const I32 ciBuild=envCiBuild.integer();
		feLog("FE_CI_BUILD %d\n",ciBuild);

		SystemTicker::calibrate();

		const double ticksPerMS=SystemTicker::ticksPerMS();
		feLog("ticksPerMS %.8G\n",ticksPerMS);

		U32* msPause=new U32[testCount];
		U32* msTick=new U32[testCount];
		U32* msTime=new U32[testCount];

		for(U32 testIndex=0;testIndex<testCount;testIndex++)
		{
			msPause[testIndex]=40.0+60.0*fabs(sin(7.0*testIndex));
		}

		milliSleep(100);

		SystemTicker ticker;

		U32 tick2=systemTick();
		U32 time2=ticker.timeMS();

		for(U32 testIndex=0;testIndex<testCount;testIndex++)
		{
			milliSleep(msPause[testIndex]);

			const U32 tick1=tick2;
			tick2=systemTick();

			const U32 time1=time2;
			time2=ticker.timeMS();

			msTick[testIndex]=(tick2-tick1)/ticksPerMS;
			msTime[testIndex]=time2-time1;
		}

		for(U32 testIndex=0;testIndex<testCount;testIndex++)
		{
			feLog("pause %3u msTick %3u msTime %3u\n",msPause[testIndex],
					msTick[testIndex],msTime[testIndex]);

			//* NOTE very loose tolerances

			if(ciBuild)
			{
				UNIT_TEST(abs(I32(msTick[testIndex]-msPause[testIndex]))<300);
				UNIT_TEST(abs(I32(msTime[testIndex]-msPause[testIndex]))<400);
			}
			else
			{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
				UNIT_TEST(abs(I32(msTick[testIndex]-msPause[testIndex]))<150);
				UNIT_TEST(abs(I32(msTime[testIndex]-msPause[testIndex]))<200);
#elif FE_OS==FE_OSX
				UNIT_TEST(abs(I32(msTick[testIndex]-msPause[testIndex]))<20);
				UNIT_TEST(abs(I32(msTime[testIndex]-msPause[testIndex]))<10);
#else
				UNIT_TEST(abs(I32(msTick[testIndex]-msPause[testIndex]))<60);
				UNIT_TEST(abs(I32(msTime[testIndex]-msPause[testIndex]))<4);
#endif
			}
		}

		delete[] msTime;
		delete[] msTick;
		delete[] msPause;

		completed=TRUE;
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	UNIT_TEST(completed);
	UNIT_TRACK(1+2*testCount);
	UNIT_RETURN();
}
