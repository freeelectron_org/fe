import os
import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):
    srcList = [ "shader.pmh",
                "ColorShade",
                "NoiseShade",
                "shaderDL" ]

    dll = module.DLL( "fexShaderDL", srcList )

    deplibs = forge.corelibs+ [
                "fexShadeDLLib",
                "fexSurfaceDLLib",
                "fexThreadDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexDataToolDLLib",
                "fexDrawDLLib" ]

    forge.deps( ["fexShaderDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexShaderDL",                  None,   None) ]

