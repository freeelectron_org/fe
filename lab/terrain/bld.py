import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "surface" ]

def setup(module):
    srcList = [ "terrain.pmh",
                "Habitat",
                "SurfaceStrata",
                "StratumBowl",
                "StratumDrawPoly",
                "StratumDrawWire",
                "StratumPlane",
                "StratumRaster",
                "StratumWave",
                "terrainDL" ]

    dll = module.DLL( "fexTerrainDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexSolveDLLib",
                "fexDataToolDLLib",
                "fexSurfaceDLLib" ]

    forge.deps( ["fexTerrainDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexTerrainDL",             None,       None) ]

    if 'viewer' in forge.modules_confirmed:
        module.Module('test')
