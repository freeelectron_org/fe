/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#include "platform/dlCore.cc"

extern "C"
{

FE_DL_EXPORT fe::Library* CreateLibrary(fe::sp<fe::Master> spMaster)
{
	return fe::CreateDataLibrary(spMaster);
}

}

