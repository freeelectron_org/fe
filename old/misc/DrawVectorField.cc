/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawVectorField.h"

namespace fe
{

DrawVectorField::DrawVectorField(void)
{
}

DrawVectorField::~DrawVectorField(void)
{
}

void DrawVectorField::initialize(void)
{
	m_mode = e_rod | e_cone;

	dispatch<String>("setPath");

	dispatch<String>("setMode");
}

void DrawVectorField::setColors(sp<RecordGroup> a_winGroup)
{
	m_color =	Color(	0.5f,	0.5f,	0.5f,	1.0f	);

	Record r_col;
	r_col = a_winGroup->lookup<String>(m_asColor.name, "foreground");
	if(r_col.isValid())
	{
		m_color = m_asColor.rgba(r_col);
	}
}

void DrawVectorField::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_asColor.bind(l_sig->scope());
}

void DrawVectorField::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	hp<DrawI> spDraw(m_drawview.drawI());

	sp<RecordGroup> spRG(m_drawview.group());

	setColors(spRG);

	if(m_aVectorField.path() != "")
	{
		sp<Component> *pspComponent = m_aVectorField(r_sig);
		if(pspComponent == NULL) { return; }
		sp<VectorFieldI> spField(*pspComponent);
		if(!spField.isValid()) { return; }

		draw(spDraw, spField);
	}

}

void DrawVectorField::draw(sp<DrawI> spDraw, sp<VectorFieldI> spField)
{
	SpatialVector location(0.0f, 0.0f, 0.0f);
	location[0] = -2.0f;
	for(int i = 0; location[0] < 12.0f; i++)
	{
		location[1] = -2.0f;
		for(int j = 0; location[1] < 12.0f; j++)
		{
			location[2] = -10.0f;
			location[2] = 2.0f;
			//for(int k = 0; k <= 2; k++)
			{
				SpatialVector direction;
				spField->sample(direction, location);
				if(m_mode & e_line)
				{
					SpatialVector line[2];
					line[0] = location;
					line[1] = location - direction;
					spDraw->drawLines(line, NULL, 2, DrawI::e_discrete,
						false, &m_color);
				}
				if(m_mode & e_sphere)
				{
					SpatialVector scale;
					set(scale,0.1f,0.1f,0.1f);
					Matrix3x4f transform;
					transform = Matrix3x4f::identity();
					SpatialVector loc = location;
					translate(transform, loc);
					spDraw->drawSphere(transform,&scale,m_color);
				}
				if(m_mode & e_rod)
				{
					Matrix3x4f transform;
					transform = Matrix3x4f::identity();
					Vector3f d;
					d = -direction;
					F32 r = magnitude(d);
					F32 rz = atanf(d[1]/d[0]);
					if(d[0] <= 0.0) { rz += pi; }
					F32 ry = acosf(d[2]/r);
					SpatialVector loc = location;
					translate(transform, loc);
					rotate(transform, rz, e_zAxis);
					rotate(transform, ry, e_yAxis);
					SpatialVector scale(0.02,0.02,0.5f);
					spDraw->drawCylinder(transform, &scale, 1.0f, m_color, 0);
				}
				if(m_mode & e_cone)
				{
					Matrix3x4f transform;
					transform = Matrix3x4f::identity();
					Vector3f d;
					d = -direction;
					F32 r = magnitude(d);
					if(r > 0.00001) { normalize(d); r = 1.0f; }
					F32 rz = atanf(d[1]/d[0]);
					if(d[0] <= 0.0) { rz += pi; }
					F32 ry = acosf(d[2]/r);
					SpatialVector loc = location;
					translate(transform, loc + d);
					rotate(transform, rz, e_zAxis);
					rotate(transform, pi + ry, e_yAxis);
					SpatialVector scale(0.06,0.06,0.5f);
					spDraw->drawCylinder(transform, &scale, 0.0f, m_color, 0);
				}
				location[2] += 10.0f;
			}
			location[1] += 1.0f;
		}
		location[0] += 1.0f;
	}

}

void DrawVectorField::setMode(unsigned int a_mode)
{
	m_mode = a_mode;
}

bool DrawVectorField::call(const String &a_name, std::vector<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("setPath", "[field path]"))
	{
		String path = a_argv[0].cast<String>();
		m_aVectorField.setup(path);
	}

	if(a_name == FE_DISPATCH("setMode", "[none|line|sphere|rod|cone]"))
	{
		String mode = a_argv[0].cast<String>();
		if(mode == "none")
		{
			m_mode = e_none;
		}
		if(mode == "line")
		{
			m_mode |= e_line;
		}
		if(mode == "sphere")
		{
			m_mode |= e_sphere;
		}
		if(mode == "rod")
		{
			m_mode |= e_rod;
		}
		if(mode == "cone")
		{
			m_mode |= e_cone;
		}
	}

	return true;
}


} /* namespace */
