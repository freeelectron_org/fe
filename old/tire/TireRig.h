/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_TireRig_h__
#define __tire_TireRig_h__

#include "solve/solve.h"
#include "evaluate/evaluate.h"
#include "evaluate/Function.h"

namespace fe
{
namespace ext
{

// Force for internal use
class TireRigForce : public SemiImplicit::Force,
	public CastableAs<TireRigForce>
{
	public:
		TireRigForce(sp<SemiImplicit> a_integrator) {}
virtual	~TireRigForce(void) {}

		void bind(SemiImplicit::Particle *a_particle)
		{
			m_particle = a_particle;
		}

virtual	void accumulate(void)
		{
			m_particle->m_force[0] += (t_solve_real)(m_force[0]);
			m_particle->m_force[1] += (t_solve_real)(m_force[1]);
			m_particle->m_force[2] += (t_solve_real)(m_force[2]);
		}

		void	set(const t_moa_v3 &a_force)
		{
			m_force = a_force;
		}
		void	add(const t_moa_v3 &a_force)
		{
			m_force += a_force;
		}
		t_solve_v3	location(void)
		{
			return m_particle->m_location;
		}
		t_solve_v3	velocity(void)
		{
			return m_particle->m_velocity;
		}

	private:
		SemiImplicit::Particle *m_particle;
		t_moa_v3					m_force;
};


/// @brief Tire Test Rig System
class RigSystem :
	virtual public Stepper,
	public Initialize<RigSystem>
{
	public:
		RigSystem(void)
		{
		}
		void	initialize(void) {}
virtual void compile(const t_note_id &a_note_id);
		void step(t_moa_real a_dt);

		struct Rig
		{
			WeakRecord					m_r_parent;
			sp<TireRigForce>			m_spTireForce;
			sp<SemiImplicit>	m_spSemiImplicit;
			SemiImplicit::Particle	*m_particle;
			void makeWishbone(RigSystem &a_system, t_moa_real a_stiffness,
				t_moa_real a_damping, t_moa_real a_load);

			std::vector< sp<TireRigForce> >	m_spTireForces;
			std::vector<Record>				m_forceParticles;
		};

		sp<Scope>					m_spScope;

		std::vector<Rig>			m_rigs;
		AsRigLive m_asRig;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_TireRig_h__ */
