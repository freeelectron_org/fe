/** @file */

#include <plugin/plugin.pmh>

#define	FE_SM_DEBUG	FALSE

using namespace fe;

namespace fe
{

FE_DL_PUBLIC FE_PLUGIN_PORT BWORD SingleMaster::ms_registered=FALSE;
FE_DL_PUBLIC FE_PLUGIN_PORT sp<SingleMaster> SingleMaster::ms_spSingleMaster;

SingleMaster::SingleMaster(void)
{
	usePrefix("fe  ");

#if FE_SM_DEBUG
	feLog("SingleMaster::SingleMaster %p\n",this);
#endif
}

SingleMaster::~SingleMaster(void)
{
#if FE_SM_DEBUG
	feLog("SingleMaster::~SingleMaster %p\n",this);
#endif
}

//* static
void SingleMaster::usePrefix(String a_prefix)
{
	PrefixLog* pPrefixLog=new PrefixLog();

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	pPrefixLog->setPrefix(a_prefix+"    ");
#else
	pPrefixLog->setPrefix("[34m"+a_prefix+"  [0m  ");
#endif

	feLogger()->clearAll();

	feLogger()->setLog("prefix",pPrefixLog);
	feLogger()->bind(".*", "prefix");

	feLogger()->setLog("win32", new Win32Log());
	feLogger()->bind(".*", "win32");

#if FALSE
	PrefaceLog* pPrefaceLog=new PrefaceLog();
	pPrefaceLog->setPreface("src     ");
	feLogger()->setLog("source", pPrefaceLog);
	feLogger()->bind("src\\..*", "source");

	PrefaceLog* pPrefaceLog2=new PrefaceLog();
	pPrefaceLog2->setPreface("lua ");
	feLogger()->setLog("lua_preface", pPrefaceLog2);
	feLogger()->bind("lua_script", "lua_preface");
	feLogger()->antibind("lua_script", "prefix");
	feLogger()->antibind("lua_script", "win32");

	feLogger()->dump();

	feLog("SingleMaster::usePrefix\n");
	feLogger()->dump();
#endif
}

//* static
sp<SingleMaster> SingleMaster::create(void)
{
	if(ms_spSingleMaster.isNull())
	{
		ms_spSingleMaster=new SingleMaster();
	}

#if FE_SM_DEBUG
	feLog("SingleMaster::create %p count %d\n",
			ms_spSingleMaster.raw(),ms_spSingleMaster->count());
#endif

	if(!ms_registered)
	{
		feRegisterSegvHandler();
		ms_registered=TRUE;
	}

	return ms_spSingleMaster;
}

void SingleMaster::release(void)
{
#if FE_SM_DEBUG
	feLog("SingleMaster::release %p count %d\n",this,count());
#endif

	//* NOTE count() can't be called after destruction
	const BWORD clearStatic=(count()==2 && this==ms_spSingleMaster.raw());

	Counted::release();

	if(clearStatic)
	{
		ms_spSingleMaster=NULL;
	}
}

sp<Master> SingleMaster::master(void)
{
	if(!m_spMaster.isValid())
	{
		m_spMaster=new Master();
	}

#if FE_SM_DEBUG
	feLog("SingleMaster::master %p\n",m_spMaster.raw())
#endif

	return m_spMaster;
}

} /* namespace fe */
