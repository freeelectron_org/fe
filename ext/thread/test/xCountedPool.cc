/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "thread/thread.h"

using namespace fe;
using namespace fe::ext;

class MyCounted: public Counted
{
	public:
		void	reset(void)		{ feLog("MyCounted::reset\n"); }
};

int main(int argc,char** argv,char **env)
{
	UNIT_START();
	BWORD complete=FALSE;
	try
	{
		CountedPool<MyCounted> myPool;
		sp<MyCounted> spMyCounted1=myPool.get();
		sp<MyCounted> spMyCounted2=myPool.get();
		sp<MyCounted> spMyCounted3=myPool.get();
		feLog("%p %p %p\n",
				spMyCounted1.raw(),spMyCounted2.raw(),spMyCounted3.raw());

		spMyCounted2=NULL;
		feLog("%p %p %p\n",
				spMyCounted1.raw(),spMyCounted2.raw(),spMyCounted3.raw());

		spMyCounted2=myPool.get();
		feLog("%p %p %p\n",
				spMyCounted1.raw(),spMyCounted2.raw(),spMyCounted3.raw());

		complete=TRUE;
	}
	catch(Exception &e)			{ e.log(); }
	catch(std::exception &e)	{ fprintf(stderr,"%s\n", e.what()); }
	catch(...)					{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(2);
	UNIT_RETURN();
}

