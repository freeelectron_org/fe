/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <xgen/xgen.pmh>

#define FE_SAXG_DEBUG		FALSE
#define FE_SAXG_VERBOSE		FALSE

//* filter XGen "errors"
FE_DL_EXPORT int xgmessage(const unsigned int descriptor,
	const char *srcFile,int lineNumber,const std::string &message)
{
	if(!message.empty() && message!=
			"User attribute xgen_densityFalloff has invalid size (0!=7).")
	{
		feLog("XGen: [33m%s[0m\n",message.c_str());
	}
	return 0;
}

//* HACK
#ifdef FE_XGEN_XPD
FileHeader::~FileHeader(void) {}
#endif

namespace fe
{
namespace ext
{

void SurfaceAccessibleXGen::reset(void)
{
	SurfaceAccessibleRecord::reset();
}

BWORD SurfaceAccessibleXGen::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAXG_DEBUG
	feLog("SurfaceAccessibleXGen::load \"%s\"\n",a_filename.c_str());
#endif

	String options;

	if(a_spSettings.isValid())
	{
		m_frame=a_spSettings->catalog<Real>("frame");
		options=a_spSettings->catalog<String>("options");
//		spFilter=a_spSettings->catalog< sp<Component> >("filter");
		m_spSkinContext=a_spSettings->catalogOrDefault< sp<Component> >(
				"context",sp<Component>(NULL));
	}
	else
	{
		m_frame=0.0;
		options="";
		m_spSkinContext=NULL;
	}

	m_optionMap.clear();
	m_patchMap.clear();

	BWORD baked=FALSE;
	String patchName;

#if FE_SAXG_DEBUG
	feLog("SurfaceAccessibleXGen::load context %d options \"%s\"\n",
			m_spSkinContext.isValid(),options.c_str());
#endif

	String buffer=options;
	String option;
	while(!(option=buffer.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

#if FE_SAXG_DEBUG
		feLog("SurfaceAccessibleXGen::load parse \"%s\" \"%s\"\n",
				property.c_str(),value.c_str());
#endif

		if(property=="baked")
		{
			baked=TRUE;
		}
		else
		{
			m_optionMap[property]=value;
		}
	}

	String& rResolution=m_optionMap["resolution"];
	if(rResolution.empty())
	{
		rResolution="0.01";		//* default of one percent
	}

	//* using Record writing code from SurfaceAccessibleLua

	m_spScope=registry()->create("Scope");
	bind(m_spScope);

	m_spScope->setLocking(FALSE);

	m_aPosition.setup(m_spScope,"spc:at");

	m_spLayoutPoint=m_spScope->declare("pt");
	m_spLayoutPoint->populate("spc:at","vector3");

	m_aVerts.setup(m_spScope,"surf:points");
	m_aProperties.setup(m_spScope,"properties");

	m_spLayoutPrimitive=m_spScope->declare("prim");
	m_spLayoutPrimitive->populate("surf:points","intarray");
	m_spLayoutPrimitive->populate("properties","int");

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(record());

	m_spPointRG=surfaceModelRV.surfacePointRG();
	m_spPrimRG=surfaceModelRV.surfacePrimitiveRG();

	m_pointCount=0;

	m_shutter_populated=FALSE;
	m_normal_populated=FALSE;
	m_uv_populated=FALSE;
	m_surfaceP_populated=FALSE;
	m_refSurfaceP_populated=FALSE;
	m_surfaceN_populated=FALSE;
	m_refSurfaceN_populated=FALSE;

	outlineClear();

	SAFEGUARDCLASS;

#if XGEN_MAJOR > 2015 && !XGEN_RENDER_API_PARALLEL
//	XgSeExpr::setMultithreading(false);

	XgTaskManager::setMultithreading(false);
	XgTaskManager::setPatchMultithreading(false);
	XgTaskManager::setCollisionMultithreading(false);
	XgTaskManager::setSeExprMultithreading(false);
#endif

	try
	{
#if FE_SAXG_DEBUG
		xgapi::setMessageLevel("debug",0);
		xgapi::setMessageLevel("warning",5);
		xgapi::setMessageLevel("stats",5);
#endif

		if(m_optionMap["palette"].empty() ||
				m_optionMap["descriptions"].empty() ||
				m_optionMap["patch"].empty())
		{
			//* TODO don't repeat probe for multiple frames
			probeOptions(a_filename);
		}

#if FE_SAXG_DEBUG
		xgapi::setMessageLevel("debug",0);
		xgapi::setMessageLevel("warning",0);
		xgapi::setMessageLevel("stats",0);
#endif
	}
	catch(std::exception const& e)
	{
		feLog("SurfaceAccessibleXGen::load probing caught \"%s\"\n",e.what());
	}
	catch(...)
	{
		feLog("SurfaceAccessibleXGen::load probing caught unknown exception\n");
	}

	const BWORD success=baked?
			importBakedCurves(a_filename):
			importProcedural(a_filename);

	m_spSkinContext=NULL;

	return success;
}

BWORD SurfaceAccessibleXGen::probeOptions(String a_filename)
{
#if FE_SAXG_DEBUG
	feLog("SurfaceAccessibleXGen::probeOptions \"%s\"\n",a_filename.c_str());
#endif

	String paletteName=m_optionMap["palette"];
	String descriptionNames=m_optionMap["descriptions"];
	String patchName=m_optionMap["patch"];

	String buffer=descriptionNames;
	Array<String> descriptionArray;
	while(!buffer.empty())
	{
		const String descriptionName=buffer.parse();
		if(!descriptionName.empty())
		{
			descriptionArray.push_back(descriptionName);
		}
	}

	std::vector<std::string> deltas;
	std::string response=xgapi::importPalette(a_filename.c_str(),deltas);

#if FE_SAXG_DEBUG
	feLog("SurfaceAccessibleXGen::probeOptions response \"%s\"\n",response.c_str());
#endif

//	if(response.empty())
//	{
//		return FALSE;
//	}

	const String projectPath=xgapi::getProjectPath().c_str();
	const String rootDir=xgapi::rootDir().c_str();

#if FE_SAXG_DEBUG
	feLog("projectPath \"%s\"\n",projectPath.c_str());
	feLog("rootDir \"%s\"\n",rootDir.c_str());
#endif

	String xpdPath;

	const BWORD findPalette=paletteName.empty();
	const BWORD findDescription=descriptionNames.empty();

	std::vector<std::string> paletteNames=xgapi::palettes();
	const I32 paletteCount=findPalette? paletteNames.size(): 1;

#if FE_SAXG_DEBUG
	feLog("paletteCount %d\n",paletteCount);
	for(I32 paletteIndex=0;paletteIndex<paletteCount;paletteIndex++)
	{
		feLog("  %d/%d \"%s\"\n",
				paletteIndex,paletteCount,paletteNames[paletteIndex].c_str())
	}
#endif

	BWORD found=FALSE;

	//* last first
	for(I32 paletteIndex=paletteCount-1;paletteIndex>=0;paletteIndex--)
	{
		if(findPalette)
		{
			paletteName=paletteNames[paletteIndex].c_str();
		}

		XgPalette* pPalette=XgPalette::palette(paletteName.c_str());

#if FE_SAXG_DEBUG
		feLog("palette %d/%d %p \"%s\"\n",
				paletteIndex,paletteCount,pPalette,paletteName.c_str())
#endif

		String dataPath;

		if(pPalette)
		{
#if FE_SAXG_DEBUG
			if(pPalette->namedAttr("xgProjectPath"))
			{
				const String project=pPalette->namedAttr("xgProjectPath")
						->expression().c_str();

				feLog("project \"%s\"\n",project.c_str());
			}
#endif

			dataPath=pPalette->xgDataPathStr().c_str();

#if FE_SAXG_DEBUG
			feLog("dataPath \"%s\"\n",dataPath.c_str());
#endif
		}

		std::vector<std::string> descriptionVector=
				xgapi::descriptions(paletteName.c_str());

		const I32 descriptionCount=findDescription?
				descriptionVector.size(): descriptionArray.size();
		for(I32 descriptionIndex=0;descriptionIndex<descriptionCount;
				descriptionIndex++)
		{
			const String descriptionName=findDescription?
					descriptionVector[descriptionIndex].c_str():
					descriptionArray[descriptionIndex].c_str();

#if FE_SAXG_DEBUG
			feLog("  description %d/%d \"%s\"\n",
					descriptionIndex,descriptionCount,descriptionName.c_str())
#endif

			XgDescription* pDescription=pPalette?
					pPalette->description(descriptionName.c_str()): NULL;

			if(pDescription)
			{
				const XgDescription::PatchMap& patches=pDescription->patches();
				for(XgDescription::PatchMap::const_iterator it=patches.begin();
						it!=patches.end();it++)
				{
					const String onePatchName=it->first;

#if FE_SAXG_DEBUG
					feLog("    patch \"%s\"\n",onePatchName.c_str());
#endif

					//* TODO iterate through all patches

					if(m_optionMap["patch"].empty())
					{
						patchName=onePatchName;
					}
				}
			}

			dataPath=xgutil::expandFilepath(
					dataPath.c_str(),descriptionName.c_str()).c_str();

#if FE_SAXG_DEBUG
			feLog("    expanded dataPath \"%s\"\n",dataPath.c_str());
#endif

			const String bakePath=dataPath+descriptionName.c_str()+"/Bake";
			xpdPath=bakePath+"/"+patchName+".xpd";

#if FE_SAXG_VERBOSE
			XgPrimitive* pPrimitive=
					pDescription? pDescription->activePrimitive(): NULL;

			feLog("    primitive %p\n",pPrimitive);

			std::vector<std::string> objectNames=
					xgapi::objects(paletteName.c_str(),descriptionName.c_str());

			const I32 objectCount=objectNames.size();
			for(I32 objectIndex=0;objectIndex<objectCount;
					objectIndex++)
			{
				std::string objectName=objectNames[objectIndex];
				feLog("    object %d/%d \"%s\"\n",
						objectIndex,objectCount,objectName.c_str())

				std::vector<std::string> attrNames=
						xgapi::attrs(paletteName.c_str(),
						descriptionName.c_str(),objectName);

				const I32 attrCount=attrNames.size();
#if TRUE
				for(I32 attrIndex=0;attrIndex<attrCount;
						attrIndex++)
				{
					std::string attrName=attrNames[attrIndex];
					feLog("      attr %d/%d \"%s\"\n",
							attrIndex,attrCount,attrName.c_str())
				}
#else
				feLog("      %d attributes\n",attrCount);
#endif
			}
#endif

			if(findDescription)
			{
				descriptionNames+=
						(descriptionNames.empty()? "": " ")+descriptionName;
			}

			//* TODO map xpd per description
			m_optionMap["xpdPath"]=xpdPath;

			m_patchMap[descriptionName]=patchName;
			found=TRUE;
		}

		m_optionMap["palette"]=paletteName;
		m_optionMap["descriptions"]=descriptionNames;

		if(found)
		{
			break;
		}
	}

#if FE_SAXG_DEBUG
	paletteNames=xgapi::palettes();
	const I32 paletteCount2=paletteNames.size();
	for(I32 paletteIndex=0;paletteIndex<paletteCount2;paletteIndex++)
	{
		feLog("  palette %d/%d \"%s\"\n",
				paletteIndex,paletteCount2,paletteNames[paletteIndex].c_str())
	}
#endif

#if FE_SAXG_DEBUG
	const BWORD deleted=
#endif
	xgapi::deletePalette(paletteName.c_str());

#if FE_SAXG_DEBUG
	feLog("SurfaceAccessibleXGen::probeOptions deletePalette \"%s\" %d\n",
			paletteName.c_str(),deleted);
#endif

	return found;
}

BWORD SurfaceAccessibleXGen::importBakedCurves(String a_filename)
{
#ifdef FE_XGEN_XPD
	const String xpdPath=m_optionMap["xpdPath"];

	m_aFaceUVW.setup(m_spScope,"face_uvw");
	m_aFace.setup(m_spScope,"face_index");
	m_aPrimitive.setup(m_spScope,"primitive_index");
	m_aId.setup(m_spScope,"primitive_id");
	m_aLength.setup(m_spScope,"length");
	m_aWidth.setup(m_spScope,"width");
	m_aTaper.setup(m_spScope,"taper");
	m_aTaperStart.setup(m_spScope,"taper_start");

	m_spLayoutPrimitive->populate("face_uvw","vector3");
	m_spLayoutPrimitive->populate("face_index","int");
	m_spLayoutPrimitive->populate("primitive_index","int");
	m_spLayoutPrimitive->populate("primitive_id","int");
	m_spLayoutPrimitive->populate("length","real");
	m_spLayoutPrimitive->populate("width","real");
	m_spLayoutPrimitive->populate("taper","real");
	m_spLayoutPrimitive->populate("taper_start","real");

#if FE_SAXG_DEBUG
	feLog("xpdPath \"%s\"\n",xpdPath.c_str());
#endif

	XpdFile xpd;
	if(!xpd.openRead(xpdPath.c_str()))
	{
		feLog("xpd open failed\n");
		return FALSE;
	}

	const I32 numCVs=xpd.numCVs();

#if FE_SAXG_DEBUG
	feLog("numCVs %d\n",numCVs);
#endif

	I32 faceIndex=0;
	while(xpd.nextFace())
	{
		int blockIndex = xpd.blockIndex("BakedGroom");
		if(!xpd.findBlock(blockIndex))
		{
			feLog("xpd has no BakedGroom\n");
			return FALSE;
		}

		I32 subPrimIndex=0;

		safevector<float> data;
		while(xpd.readPrim(data))
		{
			Record primitiveRecord=m_spScope->createRecord(m_spLayoutPrimitive);
			m_spPrimRG->add(primitiveRecord);

			m_aProperties(primitiveRecord)=TRUE;

			Array<I32>& rVertArray=m_aVerts(primitiveRecord);

			const U32 size=data.size();

			//*	Format:
			//*	primitive id (per face)
			//* uv[2] (within face)
			//*	position[3] x count
			//*	length width
			//*	taper taper_start
			//*	some_direction[3]?

			const float* pFloat=
					static_cast<float *>((void*)(&data[0]));
#if FALSE
			for(U32 index=0;index<size;index++)
			{
				feLog(" %.6G",pFloat[index]);
				if((index%3)==2)
				{
					feLog("\n");
				}
			}
			feLog("\n");
#endif

#if FE_SAXG_VERBOSE
			feLog("face %d prim %d size %d",
					faceIndex,subPrimIndex,size);
#endif

			if(size>7)
			{
#if FE_SAXG_VERBOSE
				feLog(" id %.6G uv %.6G %.6G"
						" length %.6G width %.6G"
						" taper %.6G start %.6G\n",
						pFloat[0],pFloat[1],pFloat[2],
						pFloat[size-7],pFloat[size-6],
						pFloat[size-5],pFloat[size-4]);
				feLog("  direction? %.6G %.6G %.6G (%.6f)",
						pFloat[size-3],pFloat[size-2],pFloat[size-1],
						magnitude(SpatialVector(pFloat[size-3],
						pFloat[size-2],pFloat[size-1])));
#endif

				m_aFaceUVW(primitiveRecord)=
						SpatialVector(pFloat[1],pFloat[2],0.0);
				m_aFace(primitiveRecord)=faceIndex;
				m_aPrimitive(primitiveRecord)=subPrimIndex;
				m_aId(primitiveRecord)=pFloat[0];
				m_aLength(primitiveRecord)=pFloat[size-7];
				m_aWidth(primitiveRecord)=pFloat[size-6];
				m_aTaper(primitiveRecord)=pFloat[size-5];
				m_aTaperStart(primitiveRecord)=pFloat[size-4];
			}

			feLog("\n");

			const I32 subCount=numCVs;
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const float* float3=
						static_cast<float *>((void*)(&data[3+3*subIndex]));
				const SpatialVector point(float3[0],float3[1],float3[2]);

#if FE_SAXG_VERBOSE
				feLog("  cv %d point %s\n",subIndex,c_print(point));
#endif

				Record pointRecord=m_spScope->createRecord(m_spLayoutPoint);
				m_spPointRG->add(pointRecord);

				m_aPosition(pointRecord)=point;

				rVertArray.push_back(m_pointCount++);
			}

			subPrimIndex++;
		}
		faceIndex++;
	}

	return TRUE;
#else
	return FALSE;
#endif
}

BWORD SurfaceAccessibleXGen::importProcedural(String a_filename)
{
#if FE_SAXG_DEBUG
	feLog("SurfaceAccessibleXGen::importProcedural \"%s\"\n",
			a_filename.c_str());
#endif

	//* NOTE only adding things that exist in the XGen data

	m_aRadius.setup(m_spScope,"radius");
	m_aColor.setup(m_spScope,"surf:color");
	m_aNormal.setup(m_spScope,"spc:up");

	m_spLayoutPoint->populate("radius","real");
	m_spLayoutPoint->populate("surf:color","color");

	m_aName.setup(m_spScope,"name");
	m_aSurfaceName.setup(m_spScope,"surface_name");
	m_aSurfacePart.setup(m_spScope,"surface_part");
	m_aUVW.setup(m_spScope,"surface_uvw");
	m_aSurfacePosition.setup(m_spScope,"surface_P");
	m_aRefSurfacePosition.setup(m_spScope,"ref_surface_P");
	m_aSurfaceNormal.setup(m_spScope,"surface_N");
	m_aRefSurfaceNormal.setup(m_spScope,"ref_surface_N");

	m_spLayoutPrimitive->populate("name","string");
	m_spLayoutPrimitive->populate("surface_name","string");
	m_spLayoutPrimitive->populate("surface_part","string");

	//* NOTE root is Procedural to supply ProceduralCallbacks to PatchRenderer
	Procedural* pProcedural=new Procedural();

	pProcedural->setSurfaceAccessible(sp<SurfaceAccessibleXGen>(this));
	pProcedural->setSkinContext(m_spSkinContext);
	pProcedural->setResolution(m_optionMap["resolution"]);

	const BWORD success=pProcedural->load(a_filename,m_frame);

	delete pProcedural;
	return success;
}

SurfaceAccessibleXGen::Procedural::Procedural(void):
	m_clearDescription(FALSE),
	m_resolution("1"),
	m_zero(0.0)
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::Procedural\n");
#endif

	m_falloffArray[0]=0.0;
	m_falloffArray[1]=0.0;
	m_falloffArray[2]=0.0;
	m_falloffArray[3]=0.0;
	m_falloffArray[4]=1.0;
	m_falloffArray[5]=0.0;
	m_falloffArray[6]=0.0;
}

SurfaceAccessibleXGen::Procedural::~Procedural(void)
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::~Procedural\n");
#endif
}

BWORD SurfaceAccessibleXGen::Procedural::load(String a_filename,Real a_frame)
{
	//* NOTE create geom abc using 'File -> Export Patches for Batch Render'

	// http://help.autodesk.com/view/MAYAUL/2015/ENU/?guid=GUID-3359F371-EAC1-4E56-BF7A-A544202CB41D

	//* arnold example:
	// data "-debug 1 -warning 1 -stats 1
	// -frame 1.000000 -shutter 0.0 -palette collection4
	// -file /Users/stephenblair/Documents/maya/projects/default/scenes/xgen_test5__collection4.xgen
	// -geom /Users/stephenblair/Documents/maya/projects/default/scenes/xgen_test5__collection4.caf
	// -patch pSphere1
	// -description description4
	// -world 1;0;0;0;0;1;0;0;0;0;1;0;0;0;0;1"

	//* also:
	//* -quitOnError
	//* -nameSpace <string>
	//* -delta <filenames>

	std::map<String,String>& rOptionMap=m_spSurfaceAccessibleXGen->m_optionMap;
	std::map<String,String>& rPatchMap=m_spSurfaceAccessibleXGen->m_patchMap;

	if(rOptionMap["geom"].empty())
	{
		rOptionMap["geom"]=a_filename.chop(".xgen")+".abc";
	}
	if(rOptionMap["skin"].empty())
	{
		rOptionMap["skin"]=rOptionMap["geom"];
	}

	sp<Registry> spRegistry=m_spSurfaceAccessibleXGen->registry();

	sp<SurfaceI> spSkinSurface;
	sp<SurfaceAccessibleI> spSkinAccessible;
	if(m_spSkinContext.isValid())
	{
#if FE_SAXG_DEBUG
		feLog("  SurfaceAccessibleXGen::Procedural::load using context\n");
#endif
		spSkinAccessible=m_spSkinContext;
	}
	else
	{
		const String skinfile=rOptionMap["skin"];
		const String format=String(strrchr(skinfile.c_str(),'.')).prechop(".");
		if(!format.empty())
		{
			spSkinAccessible=
					spRegistry->create("SurfaceAccessibleI.*.*."+format);
			if(spSkinAccessible.isValid())
			{
				try
				{
					sp<Scope> spScope=spRegistry->create("Scope");
					spSkinAccessible->bind(spScope);

					spScope->setLocking(FALSE);

					sp<Catalog> spSkinSettings=
							spRegistry->master()->createCatalog(
							"Load Settings");
					spSkinSettings->catalog<String>("options")="ref";

#if FE_SAXG_DEBUG
					const BWORD success=
#endif
							spSkinAccessible->load(skinfile,spSkinSettings);
#if FE_SAXG_DEBUG
					if(!success)
					{
						feLog("  SurfaceAccessibleXGen::Procedural::load"
								" failed to load skin surface\n");
					}
#endif
				}
				catch(...)
				{
#if FE_SAXG_DEBUG
					feLog("  SurfaceAccessibleXGen::Procedural::load"
							" exception while loading skin surface\n");
#endif
					spSkinAccessible=NULL;
				}
			}
		}
	}
	if(spSkinAccessible.isValid())
	{
		String partAttr;

		Array<SurfaceAccessibleI::Spec> specs;

		spSkinAccessible->attributeSpecs(specs,
				SurfaceAccessibleI::e_primitive);

		const I32 specCount=specs.size();
		for(I32 specIndex=0;specIndex<specCount;specIndex++)
		{
			const SurfaceAccessibleI::Spec& rSpec=specs[specIndex];

			const String attrType=rSpec.typeName();
			if(attrType!="string")
			{
				continue;
			}

			const String attrName=rSpec.name();
			if(attrName=="render_part")
			{
				partAttr=attrName;
				break;
			}
			else if(attrName=="part")
			{
				partAttr=attrName;
			}
			else if(attrName=="name" && partAttr!="part")
			{
				partAttr=attrName;
			}
			else if(partAttr.empty())
			{
				partAttr=attrName;
			}
		}

		try
		{
			spSkinSurface=spSkinAccessible->surface();
#if FE_SAXG_DEBUG
			feLog("  SurfaceAccessibleXGen::Procedural::load"
					" partition skin with \"%s\"\n",
					partAttr.c_str());
#endif
			spSkinSurface->partitionWith(partAttr);
			spSkinSurface->setPartitionFilter(".*");
#if FE_SAXG_DEBUG
			feLog("  SurfaceAccessibleXGen::Procedural::load"
					" prepare skin for search\n");
#endif
			spSkinSurface->prepareForSearch();
		}
		catch(...)
		{
#if FE_SAXG_DEBUG
			feLog("  SurfaceAccessibleXGen::Procedural::load"
					" skin partitioning failed\n");
#endif
			spSkinSurface=NULL;
			spSkinAccessible=NULL;
		}
	}

	String frameString;
	frameString.sPrintf("%.6G",a_frame);

	const String motionSamples=rOptionMap["motionSamples"];

#if FE_SAXG_DEBUG
	feLog("  SurfaceAccessibleXGen::Procedural::load descriptions\n  \"%s\"\n",
			rOptionMap["descriptions"].c_str());
#endif

	const String paletteName=rOptionMap["palette"];

	String line;
	line.sPrintf("pallete %s",paletteName.c_str());
	m_spSurfaceAccessibleXGen->outlineAppend(line);

	line.sPrintf("geom %s",rOptionMap["geom"].basename().c_str());
	m_spSurfaceAccessibleXGen->outlineAppend(line);

	String buffer=rOptionMap["descriptions"];
	String description;
	while(!(description=buffer.parse()).empty())
	{
		String patchName=rPatchMap[description];
		if(patchName.empty())
		{
			patchName=rOptionMap["patch"];
		}
		if(patchName.empty())
		{
			feLog("SurfaceAccessibleXGen::Procedural::load"
					" no patch for \"%s\"\n",description.c_str());
			continue;
		}

		String parameters=
				"-file "+			a_filename+
				" -palette "+		paletteName+
				" -description "+	description+
				" -geom "+			rOptionMap["geom"]+
				" -patch "+			patchName+
				" -frame "+			frameString;

		if(rOptionMap.find("delta")!=rOptionMap.end())
		{
			parameters+=" -delta "+rOptionMap["delta"];
		}

		//*  TODO what does this affect?
		parameters+=" -fps 24";

		if(!motionSamples.empty())
		{
			parameters+=
					" -motionSamplesLookup "+motionSamples+
					" -motionSamplesPlacement "+motionSamples;
		}

#if FE_SAXG_VERBOSE
		//* verbosity goes from 0 to 5
		parameters+=" -debug 1 -warning 1 -stats 1";
#endif

#if FE_SAXG_DEBUG
		feLog("  SurfaceAccessibleXGen::Procedural::load parameters\n  \"%s\"\n",
				parameters.c_str());
#endif

		XGenRenderAPI::PatchRenderer* pPatchRenderer=
				XGenRenderAPI::PatchRenderer::init(
				(ProceduralCallbacks*)this,parameters.c_str());

		if(pPatchRenderer==NULL)
		{
			feLog("  SurfaceAccessibleXGen::Procedural::load"
					" failed to initialize description \"%s\"\n",
					description.c_str());
			continue;
		}

		//* TODO consider new Procedural for every face (probably unneccessary)

		Procedural* pChild=new Procedural();
		pChild->setSurfaceAccessible(m_spSurfaceAccessibleXGen);
		pChild->setSkinSurface(spSkinSurface);
		pChild->setResolution(m_resolution);
		pChild->setDescription(description);

		Array< XGenRenderAPI::FaceRenderer* > faceArray;
		I32 faceCount=0;
		I32 faceSize=0;
		I32 faceIncrement=1000;

		XGenRenderAPI::bbox box;
		U32 face= -1;	//* Arnold example did assign negative to unsigned
		BWORD first=TRUE;

#if FE_SAXG_DEBUG
		feLog("  SurfaceAccessibleXGen::Procedural::load setup faces \"%s\"\n",
				description.c_str());
#endif

		//* NOTE this loop appears to include curves removed by density
		while(pPatchRenderer->nextFace(box,face))
		{
#if FE_SAXG_VERBOSE
			feLog("  min %.6G %.6G %.6G\n",box.xmin,box.ymin,box.zmin);
			feLog("  max %.6G %.6G %.6G\n",box.xmax,box.ymax,box.zmax);
#endif

			// Skip camera culled bounding boxes.
			if(XGenRenderAPI::Utils::isEmpty(box))
			{
#if FE_SAXG_VERBOSE
				feLog("  CULLED\n");
#endif
				continue;
			}

			faceCount++;
			if(faceCount>faceSize)
			{
				faceSize+=faceIncrement;
				faceArray.resize(faceSize);
			}

			faceArray[faceCount-1]=XGenRenderAPI::FaceRenderer::init(
					pPatchRenderer,face,pChild);
		}

#if FE_SAXG_DEBUG
		feLog("  SurfaceAccessibleXGen::Procedural::load render faces \"%s\"\n",
				description.c_str());
#endif

		m_spSurfaceAccessibleXGen->outlineAppend(description);

		line.sPrintf("  patch %s",rPatchMap[description].c_str());
		m_spSurfaceAccessibleXGen->outlineAppend(line);

		line.sPrintf("  faces %d",faceCount);
		m_spSurfaceAccessibleXGen->outlineAppend(line);

		//* NOTE run all face renderers after all have been created

#if XGEN_RENDER_API_PARALLEL
		XGenRenderAPI::ParallelRenderer* pParallelRenderer=
				XGenRenderAPI::ParallelRenderer::init(pPatchRenderer);

//		pParallelRenderer->setParallelFlush(true);
//		pParallelRenderer->setThreadCount(2);	//* default 0 = auto

		const BWORD parallel=pParallelRenderer->canRunInParallel();
		feLog("  SurfaceAccessibleXGen::Procedural::load parallel %d\n",
				parallel);

		if(!parallel)
		{
			pParallelRenderer->destroy();
			pParallelRenderer=NULL;
		}

		if(parallel)
		{
			for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
			{
				pChild->setClearDescription(first);
				first=FALSE;

				pParallelRenderer->enqueue(faceArray[faceIndex]);
			}

			pParallelRenderer->spawnAndWait();
			pParallelRenderer->destroy();
			pParallelRenderer=NULL;
		}
		else
#endif
		{
			for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
			{
				pChild->setClearDescription(first);
				first=FALSE;

				faceArray[faceIndex]->render();
			}
		}

#if FE_SAXG_DEBUG
		feLog("  SurfaceAccessibleXGen::Procedural::load delete faces \"%s\"\n",
				description.c_str());
#endif

#if !XGEN_RENDER_API_PARALLEL
		//* NOTE crashes newer XGen

		//* delete all face renderers after all have rendered
		for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
		{
			delete faceArray[faceIndex];
		}
#endif

		delete pChild;

#if !XGEN_RENDER_API_PARALLEL
		//* NOTE crashes newer XGen
		delete pPatchRenderer;
#endif
	}

#if FE_SAXG_DEBUG
	std::vector<std::string> paletteNames=xgapi::palettes();
	const I32 paletteCount=paletteNames.size();
	feLog("SurfaceAccessibleXGen::load paletteCount %d\n",paletteCount);
	for(I32 paletteIndex=0;paletteIndex<paletteCount;paletteIndex++)
	{
		feLog("  palette %d/%d \"%s\"\n",
				paletteIndex,paletteCount,paletteNames[paletteIndex].c_str());
	}
#endif

	//* NOTE deletePalette only effective if face renderers not deleted
#if FALSE
	const String renderPaletteName="XG_RENDER_:"+paletteName;
	const BWORD deleted=xgapi::deletePalette(renderPaletteName.c_str());

	feLog("SurfaceAccessibleXGen::load deletePalette \"%s\" %d\n",
			renderPaletteName.c_str(),deleted);
#endif

	return TRUE;
}

//* NOTE doesn't appear to get called
void SurfaceAccessibleXGen::Procedural::log(const char* a_str)
{
	feLog("SurfaceAccessibleXGen::Procedural::log \"%s\"\n",a_str);
}

void SurfaceAccessibleXGen::Procedural::flush(const char* a_geomName,
	XGenRenderAPI::PrimitiveCache* a_cache)
{
	const BWORD isSpline=a_cache->get(
			XGenRenderAPI::PrimitiveCache::PrimIsSpline);

#if FE_SAXG_VERBOSE
	const char* primType=a_cache->get(
			XGenRenderAPI::PrimitiveCache::PrimitiveType);

	feLog("SurfaceAccessibleXGen::Procedural::flush"
			" \"%s\" spline %d type \"%s\"\n",
			a_geomName,isSpline,primType);
#endif

	if(!isSpline)
	{
#if FE_SAXG_VERBOSE
		feLog("NOT A SPLINE\n");
#endif
		return;
	}

//	const BWORD isFacingCamera=a_cache->get(
//			XGenRenderAPI::PrimitiveCache::FaceCamera);
//	U32 shutterSize=a_cache->getSize(
//			XGenRenderAPI::PrimitiveCache::Shutter);
//	U32 cacheCount=a_cache->get(
//			XGenRenderAPI::PrimitiveCache::CacheCount);
//	U32 orientationsTotal=a_cache->getSize2(
//			XGenRenderAPI::PrimitiveCache::Norms,0);

	const I32 sampleCount=a_cache->get(
			XGenRenderAPI::PrimitiveCache::NumMotionSamples);
	const float* shutterArray=sampleCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::Shutter): NULL;

	const I32 primitiveCount=a_cache->getSize2(
			XGenRenderAPI::PrimitiveCache::NumVertices,0);

	const I32 widthCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::Widths);
	const float* widthArray=widthCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::Widths): NULL;

	const I32 uxsCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::U_XS);
	const float* uxsArray=uxsCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::U_XS): NULL;

	const I32 vxsCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::V_XS);
	const float* vxsArray=vxsCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::V_XS): NULL;

	const I32 pxsCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::P_XS);
	const XGenRenderAPI::vec3* pxsArray=pxsCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::P_XS): NULL;

	const I32 rpxsCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::Pref_XS);
	const XGenRenderAPI::vec3* rpxsArray=rpxsCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::Pref_XS): NULL;

	const I32 nxsCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::N_XS);
	const XGenRenderAPI::vec3* nxsArray=nxsCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::N_XS): NULL;

	const I32 rnxsCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::Nref_XS);
	const XGenRenderAPI::vec3* rnxsArray=rnxsCount?
			a_cache->get(XGenRenderAPI::PrimitiveCache::Nref_XS): NULL;

	Color* colorArray=new Color[primitiveCount];
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		set(colorArray[primitiveIndex],1.0,1.0,1.0);
	}

	const I32 customCount=a_cache->getSize(
			XGenRenderAPI::PrimitiveCache::CustomAttrNames);
	for(I32 customIndex=0;customIndex<customCount;customIndex++)
	{
		const String customName=a_cache->get(
				XGenRenderAPI::PrimitiveCache::CustomAttrNames,customIndex);
		if(!customName.match("uniform color .*"))
		{
			continue;
		}

		const I32 customValueCount=a_cache->getSize2(
				XGenRenderAPI::PrimitiveCache::CustomAttrValues,customIndex);

//		feLog("%d/%d attr \"%s\" %d %d\n",customIndex,customCount,
//				customName.c_str(),customValueCount,primitiveCount);

		const F32* customValueArray=a_cache->get(
				XGenRenderAPI::PrimitiveCache::CustomAttrValues,customIndex);
		for(I32 customValueIndex=0;customValueIndex<customValueCount;
				customValueIndex++)
		{
			const F32 customValue=customValueArray[customValueIndex];
			const I32 primitiveIndex=customValueIndex/3;
			const I32 componentIndex=customValueIndex%3;

//			if(customValueIndex<8)
//			{
//				feLog("  %d/%d %.6G\n",
//						customValueIndex,customValueCount,customValue);
//			}

			if(primitiveIndex<primitiveCount)
			{
				colorArray[primitiveIndex][componentIndex]=customValue;
			}
		}
	}

	const float constantRadius=
			0.5*a_cache->get(XGenRenderAPI::PrimitiveCache::ConstantWidth);

	if((uxsCount || vxsCount) && !m_spSurfaceAccessibleXGen->m_uv_populated)
	{
		m_spSurfaceAccessibleXGen->m_spLayoutPrimitive->populate(
				"surface_uvw","vector3");
		m_spSurfaceAccessibleXGen->m_uv_populated=TRUE;
	}

	if(pxsCount && !m_spSurfaceAccessibleXGen->m_surfaceP_populated)
	{
		m_spSurfaceAccessibleXGen->m_spLayoutPrimitive->populate(
				"surface_P","vector3");
		m_spSurfaceAccessibleXGen->m_surfaceP_populated=TRUE;
	}

	if(rpxsCount && !m_spSurfaceAccessibleXGen->m_refSurfaceP_populated)
	{
		m_spSurfaceAccessibleXGen->m_spLayoutPrimitive->populate(
				"ref_surface_P","vector3");
		m_spSurfaceAccessibleXGen->m_refSurfaceP_populated=TRUE;
	}

	if(nxsCount && !m_spSurfaceAccessibleXGen->m_surfaceN_populated)
	{
		m_spSurfaceAccessibleXGen->m_spLayoutPrimitive->populate(
				"surface_N","vector3");
		m_spSurfaceAccessibleXGen->m_surfaceN_populated=TRUE;
	}

	if(rnxsCount && !m_spSurfaceAccessibleXGen->m_refSurfaceN_populated)
	{
		m_spSurfaceAccessibleXGen->m_spLayoutPrimitive->populate(
				"ref_surface_N","vector3");
		m_spSurfaceAccessibleXGen->m_refSurfaceN_populated=TRUE;
	}

	I32 motionCount=0;
	if(sampleCount>1)
	{
		motionCount=sampleCount;
		for(I32 sampleIndex=0;sampleIndex<sampleCount;sampleIndex++)
		{
			const Real shutter=shutterArray[sampleIndex];
			if(shutter==0.0)
			{
				motionCount--;
			}
		}

		m_spSurfaceAccessibleXGen->m_aMotionArray.resize(motionCount);
	}

#if FE_SAXG_VERBOSE
	const I32 pointCount=a_cache->getSize2(
			XGenRenderAPI::PrimitiveCache::Points,0);

	feLog("  sampleCount %d pointCount %d primitiveCount %d\n"
			"  widthCount %d widthArray %p\n"
			"  uxsCount %d uxsArray %p\n"
			"  vxsCount %d vxsArray %p\n"
			"  pxsCount %d pxsArray %p\n"
			"  rpxsCount %d rpxsArray %p\n"
			"  nxsCount %d nxsArray %p\n"
			"  rnxsCount %d rnxsArray %p\n"
			"  constantRadius %.6G\n",
			sampleCount,pointCount,primitiveCount,
			widthCount,widthArray,
			uxsCount,uxsArray,
			vxsCount,vxsArray,
			pxsCount,pxsArray,
			rpxsCount,rpxsArray,
			nxsCount,nxsArray,
			rnxsCount,rnxsArray,
			constantRadius);
#endif

	Array<Record> pointRecordCache;

	I32 pointOffset=0;
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const Real uxs=
				(primitiveIndex<uxsCount)? uxsArray[primitiveIndex]: 0.0;
		const Real vxs=
				(primitiveIndex<vxsCount)? vxsArray[primitiveIndex]: 0.0;

		SpatialVector surface_uvw(uxs,vxs,0.0);

		SpatialVector surface_P(0.0,0.0,0.0);
		if(primitiveIndex<pxsCount)
		{
			const XGenRenderAPI::vec3& rVec3=pxsArray[primitiveIndex];
			set(surface_P,rVec3.x,rVec3.y,rVec3.z);
		}

		String surface_part="default";
		SpatialVector ref_surface_P(0.0,0.0,0.0);
		if(primitiveIndex<rpxsCount)
		{
			const XGenRenderAPI::vec3& rVec3=rpxsArray[primitiveIndex];
			set(ref_surface_P,rVec3.x,rVec3.y,rVec3.z);

			if(m_spSkinSurface.isValid())
			{
				sp<SurfaceI::ImpactI> spImpact=
						m_spSkinSurface->nearestPoint(ref_surface_P);
				if( spImpact.isValid())
				{
					surface_uvw=spImpact->uv();

					const I32 partitionIndex=spImpact->partitionIndex();

					sp<SurfaceSearchable> spSurfaceSearchable=m_spSkinSurface;
					if(spSurfaceSearchable.isValid())
					{
						String buffer=
								m_spSkinSurface->partitionName(partitionIndex);
						String token;
						while(!(token=buffer.parse("\"","/")).empty())
						{
							surface_part=token;
						}
						buffer=surface_part;
						while(!(token=buffer.parse("\"",":")).empty())
						{
							surface_part=token;
						}
					}
				}
				else
				{
					set(surface_uvw);
				}
			}
		}

		SpatialVector surface_N(0.0,0.0,0.0);
		if(primitiveIndex<nxsCount)
		{
			const XGenRenderAPI::vec3& rVec3=nxsArray[primitiveIndex];
			set(surface_N,rVec3.x,rVec3.y,rVec3.z);
		}

		SpatialVector ref_surface_N = surface_N;
		if(primitiveIndex<rnxsCount)
		{
			const XGenRenderAPI::vec3& rVec3=rnxsArray[primitiveIndex];
			set(ref_surface_N,rVec3.x,rVec3.y,rVec3.z);
		}

		Record primitiveRecord=
				m_spSurfaceAccessibleXGen->m_spScope->createRecord(
				m_spSurfaceAccessibleXGen->m_spLayoutPrimitive);

		m_spSurfaceAccessibleXGen->m_spPrimRG->add(primitiveRecord);

#if FE_SAXG_DEBUG
		const I32 recordCount=m_spSurfaceAccessibleXGen->m_spPrimRG->count();
		if(recordCount && !(recordCount%10000))
		{
			feLog("SurfaceAccessibleXGen::Procedural::flush curve %d\n",
					recordCount);
		}
#endif

		m_spSurfaceAccessibleXGen->m_aProperties(primitiveRecord)=TRUE;

		m_spSurfaceAccessibleXGen->m_aName(primitiveRecord)=m_description;

		m_spSurfaceAccessibleXGen->m_aSurfaceName(primitiveRecord)=
				a_geomName;

		m_spSurfaceAccessibleXGen->m_aSurfacePart(primitiveRecord)=
				surface_part;

		if(uxsCount)
		{
			m_spSurfaceAccessibleXGen->m_aUVW(primitiveRecord)=
					surface_uvw;
		}
		if(pxsCount)
		{
			m_spSurfaceAccessibleXGen->m_aSurfacePosition(primitiveRecord)=
					surface_P;
		}
		if(rpxsCount)
		{
			m_spSurfaceAccessibleXGen->m_aRefSurfacePosition(primitiveRecord)=
					ref_surface_P;
		}
		if(nxsCount)
		{
			m_spSurfaceAccessibleXGen->m_aSurfaceNormal(primitiveRecord)=
					surface_N;
		}
		if(rnxsCount)
		{
			m_spSurfaceAccessibleXGen->m_aRefSurfaceNormal(primitiveRecord)=
					ref_surface_N;
		}

		Array<I32>& rVertArray=
				m_spSurfaceAccessibleXGen->m_aVerts(primitiveRecord);

#if FE_SAXG_VERBOSE
		feLog("  primitive %d/%d surface_uvw %s\n",
				primitiveIndex,primitiveCount,c_print(surface_uvw));
		feLog("    surface_P %s ref_surface_P %s\n",
				c_print(surface_P),c_print(ref_surface_P));
		feLog("    surface_N %s ref_surface_N %s\n",
				c_print(surface_N),c_print(ref_surface_N));
#endif

		I32 motionIndex=0;
		for(I32 sampleIndex=0;sampleIndex<sampleCount;sampleIndex++)
		{
			const Real shutter=shutterArray[sampleIndex];

#if FE_SAXG_VERBOSE
				feLog("    sample %d/%d shutter %.6G\n",
						sampleIndex,sampleCount,shutter);
#endif

			Accessor<SpatialVector> aPoint;

			if(shutter==0.0)
			{
				aPoint=m_spSurfaceAccessibleXGen->m_aPosition;
			}
			else
			{
				aPoint=m_spSurfaceAccessibleXGen->m_aMotionArray[motionIndex];

				if(!m_spSurfaceAccessibleXGen->m_shutter_populated)
				{
					String attrName;
					attrName.sPrintf("P%s%.6G",shutter<0.0? "m": "",fabs(shutter));
					attrName=attrName.substitute("-","m").substitute(".","_");

					aPoint.setup(m_spSurfaceAccessibleXGen->m_spScope,attrName);
					m_spSurfaceAccessibleXGen->m_spLayoutPoint->populate(
							attrName,"vector3");
					m_spSurfaceAccessibleXGen->m_shutter_populated=TRUE;
				}
			}

			const I32* subCountArray=(int*)a_cache->get(
					XGenRenderAPI::PrimitiveCache::NumVertices,sampleIndex);
			if(!subCountArray)
			{
				feX(e_corrupt,"SurfaceAccessibleXGen::Procedural::flush",
						"XGenRenderAPI::PrimitiveCache::NumVertices is NULL");
			}

			const XGenRenderAPI::vec3* pointArray=a_cache->get(
					XGenRenderAPI::PrimitiveCache::Points,sampleIndex);
			if(!pointArray)
			{
				feX(e_corrupt,"SurfaceAccessibleXGen::Procedural::flush",
						"XGenRenderAPI::PrimitiveCache::Points is NULL");
			}

			const XGenRenderAPI::vec3* normArray=a_cache->get(
					XGenRenderAPI::PrimitiveCache::Norms,sampleIndex);

			const I32 subCount=subCountArray[primitiveIndex];

			//* HACK default normals
//			if(normArray)
			if(!m_spSurfaceAccessibleXGen->m_normal_populated)
			{
				m_spSurfaceAccessibleXGen->m_spLayoutPoint->populate(
						"spc:up","vector3");
				m_spSurfaceAccessibleXGen->m_normal_populated=TRUE;
			}

			const I32 widthOffset=pointOffset-1-2*primitiveIndex;

#if FE_SAXG_VERBOSE
			feLog("      pointArray %p normArray %p"
					" pointOffset %d widthOffset %d\n",
					pointArray,normArray,pointOffset,widthOffset);
#endif

			const I32 addCount=subCount-2;
			if(!sampleIndex)
			{
				if(I32(pointRecordCache.size())<addCount)
				{
					pointRecordCache.resize(addCount);
				}

#if FALSE
				sp<RecordArray> spRA=
						m_spSurfaceAccessibleXGen->m_spScope->createRecordArray(
						m_spSurfaceAccessibleXGen->m_spLayoutPoint,addCount);

				for(I32 addIndex=0;addIndex<addCount;addIndex++)
				{
					pointRecordCache[addIndex]=spRA->getRecord(addIndex);
				}

				m_spSurfaceAccessibleXGen->m_spPointRG->add(spRA);
#else
				for(I32 addIndex=0;addIndex<addCount;addIndex++)
				{
					Record pointRecord=
							m_spSurfaceAccessibleXGen->m_spScope->createRecord(
							m_spSurfaceAccessibleXGen->m_spLayoutPoint);

					m_spSurfaceAccessibleXGen->m_spPointRG->add(pointRecord);

					pointRecordCache[addIndex]=pointRecord;
				}
#endif
			}

			//* TODO check Arnold example about something tricky with normals

			//* NOTE extra point on each end
			for(I32 subIndex=1;subIndex<subCount-1;subIndex++)
			{
				const I32 offsetIndex=pointOffset+subIndex;

				const XGenRenderAPI::vec3& rVec3=pointArray[offsetIndex];
				const SpatialVector point(rVec3.x,rVec3.y,rVec3.z);

				const I32 widthIndex=widthOffset+subIndex;
				const Real radius=(widthIndex<widthCount)?
						0.5*widthArray[widthIndex]: constantRadius;

				Record& rPointRecord=pointRecordCache[subIndex-1];

				aPoint(rPointRecord)=point;
				m_spSurfaceAccessibleXGen->m_aRadius(rPointRecord)=radius;

#if TRUE
				m_spSurfaceAccessibleXGen->m_aColor(rPointRecord)=
						colorArray[primitiveIndex];
#else
				//* color by UV
				SpatialVector uvColor(0.125*surface_uvw[0],0.5*surface_uvw[1]);
				uvColor=0.5*uvColor+0.5*SpatialVector(1.0,1.0,1.0);

				m_spSurfaceAccessibleXGen->m_aColor(rPointRecord)=uvColor;
#endif

#if FE_SAXG_VERBOSE
				feLog("      pr %d/%d sub %d/%d point %d pos %s radius %.6G\n",
						primitiveIndex,primitiveCount,
						subIndex,subCount,
						m_spSurfaceAccessibleXGen->m_pointCount,
						c_print(point),radius);
#endif

				if(normArray)
				{
					const XGenRenderAPI::vec3& rNorm3=normArray[offsetIndex];
					const SpatialVector norm(rNorm3.x,rNorm3.y,rNorm3.z);

					m_spSurfaceAccessibleXGen->m_aNormal(rPointRecord)=norm;

#if FE_SAXG_VERBOSE
#endif
					feLog("      norm %s\n",c_print(norm));
				}
				else
				{
					//* NOTE default to upshaft normals

					const XGenRenderAPI::vec3& rPrior3=subIndex>1?
							pointArray[offsetIndex-1]:
							pointArray[offsetIndex+1];

					const SpatialVector prior(rPrior3.x,rPrior3.y,rPrior3.z);

					const SpatialVector norm=unitSafe(point-prior)*
						(subIndex>1? -1.0: 1.0);

					m_spSurfaceAccessibleXGen->m_aNormal(rPointRecord)=norm;
				}

				if(!sampleIndex)
				{
					rVertArray.push_back(
							m_spSurfaceAccessibleXGen->m_pointCount+subIndex-1);
				}
			}

			if(sampleIndex==sampleCount-1)
			{
				m_spSurfaceAccessibleXGen->m_pointCount+=addCount;
				pointOffset+=subCount;
			}

			motionIndex++;
		}
	}
	delete[] colorArray;
}

bool SurfaceAccessibleXGen::Procedural::get(EBoolAttribute a_attr) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::get(EBoolAttribute)\n");
#endif

	if( a_attr==ClearDescriptionCache )
	{
#if FE_SAXG_VERBOSE
		feLog("  ClearDescriptionCache %d\n",m_clearDescription);
#endif
		return m_clearDescription;
	}
#if XGEN_MAJOR > 2014
	else if( a_attr==DontUsePaletteRefCounting )
	{
#if FE_SAXG_VERBOSE
		feLog("  DontUsePaletteRefCounting\n");
#endif
	}
#endif

	return false;
}

const char* SurfaceAccessibleXGen::Procedural::get(
	EStringAttribute a_attr) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::get(EStringAttribute)\n");
#endif
	if(a_attr==BypassFXModulesAfterBGM)
	{
#if FE_SAXG_VERBOSE
		feLog("  BypassFXModulesAfterBGM\n");
#endif
	}
	else if(a_attr==CacheDir)
	{
#if FE_SAXG_VERBOSE
		feLog("  CacheDir\n");
#endif
	}
	else if(a_attr==Generator)
	{
#if FE_SAXG_VERBOSE
		feLog("  Generator\n");
#endif
	}
	else if(a_attr==Off)
	{
#if FE_SAXG_VERBOSE
		feLog("  Off\n");
#endif
	}
	else if(a_attr==Phase)
	{
#if FE_SAXG_VERBOSE
		feLog("  Phase\n");
#endif
	}
	else if(a_attr==RenderCam)
	{
#if FE_SAXG_VERBOSE
		feLog("  RenderCam\n");
#endif
		return "0";
	}
	else if(a_attr == RenderCamFOV)
	{
#if FE_SAXG_VERBOSE
		feLog("  RenderCamFOV\n");
#endif
		return "0";
	}
	else if(a_attr == RenderCamXform)
	{
#if FE_SAXG_VERBOSE
		feLog("  RenderCamXform\n");
#endif
		return "0";
	}
	else if(a_attr == RenderCamRatio)
	{
#if FE_SAXG_VERBOSE
		feLog("  RenderCamRatio\n");
#endif
		return "0";
	}
	else if(a_attr == RenderMethod)
	{
#if FE_SAXG_VERBOSE
		feLog("  RenderMethod\n");
#endif
	}

	return "";
}
float SurfaceAccessibleXGen::Procedural::get(EFloatAttribute a_attr) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::get(EFloatAttribute)\n");
#endif

	if( a_attr==ShadowMotionBlur )
	{
#if FE_SAXG_VERBOSE
		feLog("  ShadowMotionBlur\n");
#endif
	}
	else if( a_attr==ShutterOffset )
	{
#if FE_SAXG_VERBOSE
		feLog("  ShutterOffset\n");
#endif
	}

	return 1.0;
}

const float* SurfaceAccessibleXGen::Procedural::get(
	EFloatArrayAttribute a_attr) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::get(EFloatArrayAttribute)\n");
#endif

	if( a_attr==DensityFalloff )
	{
#if FE_SAXG_VERBOSE
		feLog("  DensityFalloff\n");
#endif

		//* source	range	falloff flatten	interpolation
		//* loc[3]	radA	radB	1-gain	enum:linear,smooth,gaussian,boxstep

		//* (1-((1-flatten) * <step>(dist,range,falloff))) * mask

		//* TODO
		return m_falloffArray;
	}
	else if( a_attr==LodHi )
	{
#if FE_SAXG_VERBOSE
		feLog("  LodHi\n");
#endif
	}
	else if( a_attr==LodLow )
	{
#if FE_SAXG_VERBOSE
		feLog("  LodLow\n");
#endif
	}
	else if( a_attr==LodMed )
	{
#if FE_SAXG_VERBOSE
		feLog("  LodMed\n");
#endif
	}
	else if( a_attr==Shutter )
	{
#if FE_SAXG_VERBOSE
		feLog("  Shutter\n");
#endif
	}

	return &m_zero;
}

unsigned int SurfaceAccessibleXGen::Procedural::getSize(
	EFloatArrayAttribute a_attr) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::getSize\n");
#endif

	if( a_attr==DensityFalloff )
	{
#if FE_SAXG_VERBOSE
		feLog("  DensityFalloff\n");
#endif
		return 0;	//* NOTE expects 7
	}
	else if( a_attr==LodHi )
	{
#if FE_SAXG_VERBOSE
		feLog("  LodHi\n");
#endif
		return 2;
	}
	else if( a_attr==LodLow )
	{
#if FE_SAXG_VERBOSE
		feLog("  LodLow\n");
#endif
		return 2;
	}
	else if( a_attr==LodMed )
	{
#if FE_SAXG_VERBOSE
		feLog("  LodMed\n");
#endif
		return 2;
	}
	else if( a_attr==Shutter )
	{
#if FE_SAXG_VERBOSE
		feLog("  Shutter\n");
#endif

		//* HACK test values for motion blur
//		return 3;
	}

	return 1;
}

/*
user:xgen_averageWidth
user:xgen_cullExpr
user:xgen_densityScale
user:xgen_depth
user:xgen_depthScale
user:xgen_inCameraMargin
user:xgen_inCameraOnly
user:xgen_length
user:xgen_lengthScale
user:xgen_lodFlag
user:xgen_mask
user:xgen_maskScale
user:xgen_maxWidthRatio
user:xgen_minDensity
user:xgen_offset
user:xgen_offsetScale
user:xgen_percent
user:xgen_percentScale
user:xgen_pixelCullSize
user:xgen_pixelFadeSize
user:xgen_taperScale
user:xgen_width
user:xgen_widthScale
*/
const char* SurfaceAccessibleXGen::Procedural::getOverride(
	const char* a_name) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::getOverride \"%s\"\n",a_name);
#endif

	const String property=a_name;

	if(property=="user:xgen_densityScale")
	{
#if FE_SAXG_DEBUG
		feLog("SurfaceAccessibleXGen::Procedural::getOverride"
				" user:xgen_densityScale \"%s\"\n",m_resolution.c_str());
#endif
		return m_resolution.c_str();
	}

	return "";
}
bool SurfaceAccessibleXGen::Procedural::getArchiveBoundingBox(
	const char* a_filename,XGenRenderAPI::bbox& out_bbox) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::getArchiveBoundingBox"
			" \"%s\"\n",a_filename);
#endif
	return false;
}

void SurfaceAccessibleXGen::Procedural::getTransform(float a_time,
	XGenRenderAPI::mat44& out_mat) const
{
#if FE_SAXG_VERBOSE
	feLog("SurfaceAccessibleXGen::Procedural::getTransform %.6G\n",a_time);
#endif

	XGenRenderAPI::Utils::identity(out_mat);
}

} /* namespace ext */
} /* namespace fe */
