import os
import sys
import string
forge = sys.modules["forge"]

def setup(module):
    # the library
    srclist = [ "message.pmh",
                "MessageUDP",
                "messageDL" ]

#   forge.objlists['message'] = module.SrcToObj(srclist);

    liblist = forge.corelibs + [
                "fePlatformLib",
                "feMemoryLib",
                "feCoreLib",
                "fePluginLib" ]

    dll = module.DLL( "fexMessageDL", srclist )

    forge.deps( ["fexMessageDLLib"], liblist )


    srcList = [ "MessageReliableUDP",
                "messageReliableUDP.pmh",
                "messageReliableUDPDL"              ]

    dll = module.DLL( "fexMessageReliableUDPDL", srcList )

    deplibs = forge.corelibs + [
                "fexMessageDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "fexDataToolLib" ]

    forge.deps( ["fexMessageReliableUDPDLLib"], deplibs )


    forge.tests += [
        ("inspect.exe", "fexMessageDL",                 None,   None) ]

    module.Module('test')
