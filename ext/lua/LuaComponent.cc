/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

namespace fe
{
namespace ext
{

LuaComponentType::LuaComponentType(LuaContext *pContext,const String &mtn)
	: t_lua_component_t(pContext, mtn)
{
	setMethod("name", LuaComponentObject::name);
	setMethod("setName", LuaComponentObject::setName);
	setMethod("adjoin", LuaComponentObject::adjoin);
}

void LuaComponentType::pushNewMetaTable(void)
{
	lua_State *pLuaState = m_pContext->state();

	luaL_newmetatable(pLuaState,m_mtname.c_str());

	lua_pushstring(pLuaState, "__gc");
	lua_pushcfunction(pLuaState, LuaComponentObject::destroy);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__index");
	lua_pushcfunction(pLuaState, LuaComponentObject::index);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__newindex");
	lua_pushcfunction(pLuaState, LuaComponentObject::newindex);
	lua_settable(pLuaState, -3);

	lua_pushstring(pLuaState, "__call");
	lua_pushcfunction(pLuaState, LuaComponentObject::call);
	lua_settable(pLuaState, -3);
}


int LuaComponentObject::index(lua_State *pLuaState)
{
	String attributeName = lua_tostring(pLuaState, -1);
	lua_pop(pLuaState, 1);

	LuaComponentObject *pObj = (LuaComponentObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	sp<ConfigI> spConfig(pObj->data());

	if(spConfig.isValid())
	{
		Instance instance;

		if(spConfig->configLookup(attributeName, instance))
		{
			pObj->context()->pushInstance(instance);

			return 1;
		}
	}

	if(!pObj->type()->pushMethod(attributeName))
	{
		if(!pObj->pushMethod(attributeName))
		{
			lua_pushnil(pLuaState);
		}
	}

	return 1;
}

int LuaComponentObject::newindex(lua_State *pLuaState)
{
	LuaComponentObject *pObj =
		(LuaComponentObject *)lua_touserdata(pLuaState, 1);
	String attributeName = lua_tostring(pLuaState, -2);

	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	sp<ConfigI> spConfig(pObj->data());

	if(spConfig.isValid())
	{
		Instance instance;

		spConfig->configLookup(attributeName, instance);

		pObj->context()->popInstance(instance, true);

		spConfig->configSet(attributeName, instance);

		return 0;
	}

	return 0;
}

int LuaComponentObject::call(lua_State *pLuaState)
{
	LuaComponentObject *pObj =
		(LuaComponentObject *)lua_touserdata(pLuaState, 1);

	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	sp<DispatchI> spDispatchI(pObj->data());
	if(spDispatchI.isNull())
	{
		return 0;
	}

	const sp<TypeMaster> &spTM = pObj->context()->typeMaster();

	const I32 argCount=lua_gettop(pLuaState) - 1;

	if(argCount==1 && lua_istable(pLuaState, -1))
	{
//		feLog(">> LuaComponentObject::call table\n");

		InstanceMap instanceMap(spTM);

		// https://stackoverflow.com/questions/6137684/iterate-through-lua-table

		//* stack: -1 table
		lua_pushnil(pLuaState);
		//* stack: -1 nil, -2 table

		while (lua_next(pLuaState, -2))
		{
			//* stack: -1 value, -2 key, -3 table

			//* duplicate the key
			lua_pushvalue(pLuaState, -2);
			//* stack: -1 key, -2 value, -3 key, -4 table

			const String key = lua_tostring(pLuaState, -1);

//			const char *value = lua_isuserdata(pLuaState, -2)?
//					"<userdata>": lua_tostring(pLuaState, -2);
//			feLog(">>   %s => %s\n", key.c_str(), value);

			//* pop duplicate
			lua_pop(pLuaState, 1);
			//* stack: -1 value, -2 key, -3 table

			Instance instance;

			if(lua_isnumber(pLuaState, -1))
			{
				instance.create<double>(spTM);
				pObj->context()->popInstance(instance,true);
			}
			else if(lua_isstring(pLuaState, -1))
			{
				instance.create<String>(spTM);
				pObj->context()->popInstance(instance,true);
			}
			else if(lua_isuserdata(pLuaState, -1))
			{
				//* TODO other custom types?

				instance.create<Record>(spTM);
				pObj->context()->popInstance(instance,true);
			}
			else
			{
				lua_pop(pLuaState, 1);
			}

			//* stack: -1 key, -2 table

			instanceMap[key]=instance;
		}
		//* stack: -1 table (final lua_next pops the key)

		lua_pop(pLuaState, 1);

		spDispatchI->call("FUNCTOR",instanceMap);

		const String outputKey="Output Surface";	//* TODO

		Instance& response=instanceMap[outputKey];

		if(!response.data())
		{
			feLog("LuaComponentObject::call no response\n");
			lua_pushnil(pLuaState);
			return 1;
		}

		pObj->context()->pushInstance(response);
		return 1;
	}

	Array<Instance> arglist(argCount+1);	//* NOTE first arg is result

	for(I32 index=argCount;index>0;index--)
	{
		Instance& rInstance=arglist[index];

		if(lua_isnil(pLuaState, -1))
		{
			lua_pop(pLuaState, 1);
		}
		else if(lua_isnumber(pLuaState, -1))
		{
			rInstance.create<double>(spTM);
			pObj->context()->popInstance(rInstance,true);
		}
		else if(lua_isstring(pLuaState, -1))
		{
			rInstance.create<String>(spTM);
			pObj->context()->popInstance(rInstance,true);
		}
		else if(lua_isuserdata(pLuaState, -1))
		{
			//* TODO other custom types?

			rInstance.create<Record>(spTM);
			pObj->context()->popInstance(rInstance,true);
		}
		else
		{
			feLog("LuaComponentObject::call unrecognized argument type\n");
			lua_pop(pLuaState, 1);
		}
	}

	spDispatchI->call("FUNCTOR",arglist);

	Instance& response=arglist[0];
	if(!response.data())
	{
		feLog("LuaComponentObject::call no response\n");
		lua_pushnil(pLuaState);
		return 1;
	}

	pObj->context()->pushInstance(response);
	return 1;
}

int LuaComponentObject::setName(lua_State *pLuaState)
{
	String name = lua_tostring(pLuaState, -1);
	lua_pop(pLuaState, 1);

	LuaComponentObject *pObj = (LuaComponentObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	pObj->data()->setName(name);

	lua_pushnil(pLuaState);
	return 1;
}

int LuaComponentObject::name(lua_State *pLuaState)
{
	LuaComponentObject *pObj = (LuaComponentObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	lua_pushstring(pLuaState, pObj->data()->name().c_str());

	return 1;
}

int LuaComponentObject::adjoin(lua_State *pLuaState)
{
	LuaComponentObject *pObj = (LuaComponentObject *)selfIsFirst(pLuaState);
	if(!pObj) { lua_pushnil(pLuaState); return 1; }

	sp<Component> spComponent = component<Component>(pLuaState, 2);

	pObj->data()->adjoin(spComponent);

	return 0;
}

Record record(lua_State *pLuaState, int index)
{
	LuaRecordObject *pR =
		to<LuaRecordObject>(pLuaState, index, LuaContext::e_record);
	return pR->data();
}

sp<Scope> scope(lua_State *pLuaState, int index)
{
	LuaScopeObject *pS =
		to<LuaScopeObject>(pLuaState, index, LuaContext::e_scope);
	return pS->data();
}

sp<RecordGroup> recordgroup(lua_State *pLuaState, int index)
{
	LuaGroupObject *pRG =
		to<LuaGroupObject>(pLuaState, index, LuaContext::e_group);
	return pRG->data();
}


sp<Layout> layout(lua_State *pLuaState, int index)
{
	return counted<LuaLayoutObject,Layout>(pLuaState, index,
			LuaContext::e_layout);
}


// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------


// SignalerI -----------------------------------------------------------------
int signal_insert(lua_State *pLuaState)
{
	sp<SignalerI> spSignaler = component<SignalerI>(pLuaState, 1);
	sp<HandlerI> spHandler = component<HandlerI>(pLuaState, 2);

	if(lua_gettop(pLuaState) >= 3)
	{
		sp<Layout> spLayout = layout(pLuaState, 3);

		if(lua_gettop(pLuaState) >= 4)
		{
			int position = (int)lua_tonumber(pLuaState, 4);
			spSignaler->insert(spHandler, spLayout, position);
		}
		else
		{
			spSignaler->insert(spHandler, spLayout);
		}
	}
	else
	{
		sp<Layout> spNil;
		spSignaler->insert(spHandler, spNil);
	}

	return 0;

}

int signal_signal(lua_State *pLuaState)
{
	sp<SignalerI> spSignaler = component<SignalerI>(pLuaState, 1);
	Record r_sig = record(pLuaState, 2);

	spSignaler->signal(r_sig);

	return 0;
}

// HandlerI ------------------------------------------------------------------
int handler_handle(lua_State *pLuaState)
{
	sp<HandlerI> spHandler = component<HandlerI>(pLuaState, 1);
	Record r_sig = record(pLuaState, 2);

	spHandler->handle(r_sig);

	return 0;
}

// WindowI -------------------------------------------------------------------
int window_getEventContext(lua_State *pLuaState)
{
	sp<WindowI> spWindow = component<WindowI>(pLuaState, 1);
	LuaComponentObject::pushComponent(pLuaState, spWindow->getEventContextI());

	return 1;
}

int window_open(lua_State *pLuaState)
{
	sp<WindowI> spWindow = component<WindowI>(pLuaState, 1);
	String windowName = lua_tostring(pLuaState, 2);
	spWindow->open(windowName);
	LuaComponentObject::pushComponent(pLuaState, spWindow->getEventContextI());

	return 0;
}

int window_settitle(lua_State *pLuaState)
{
	sp<WindowI> spWindow = component<WindowI>(pLuaState, 1);
	String windowName = lua_tostring(pLuaState, 2);
	spWindow->setTitle(windowName);
	LuaComponentObject::pushComponent(pLuaState, spWindow->getEventContextI());

	return 0;
}

int window_setsize(lua_State *pLuaState)
{
	sp<WindowI> spWindow = component<WindowI>(pLuaState, 1);
	int sx = (int)lua_tonumber(pLuaState, 2);
	int sy = (int)lua_tonumber(pLuaState, 3);
	spWindow->setSize(sx, sy);
	LuaComponentObject::pushComponent(pLuaState, spWindow->getEventContextI());

	return 0;
}

// ViewerI -------------------------------------------------------------------
int viewer_bind(lua_State *pLuaState)
{
	sp<ViewerI> spViewer = component<ViewerI>(pLuaState, 1);
	sp<WindowI> spWindow = component<WindowI>(pLuaState, 2);
	spViewer->bind(spWindow);

	return 0;
}

#ifdef FE_LUA_ARCHITECTURE
// AnnotateI -----------------------------------------------------------------
int annotate_attach(lua_State *pLuaState)
{
	sp<AnnotateI> spAnnotate = component<AnnotateI>(pLuaState, 1);
	String attrName = lua_tostring(pLuaState, 2);
	Record r_data = record(pLuaState, 3);

	spAnnotate->attach(attrName, r_data);

	return 0;
}
#endif

// ParserI ------------------------------------------------------------------
int parser_parse(lua_State *pLuaState)
{
	sp<ParserI> spParser = component<ParserI>(pLuaState, 1);

	int cnt = lua_gettop(pLuaState) - 1;
	Array<String> tokens;
	tokens.resize(cnt);
	for(int i = 0; i < cnt; i++)
	{
		tokens[i] = lua_tostring(pLuaState, i+2);
	}

	spParser->parse(tokens);

	return 0;
}

// LuaI ----------------------------------------------------------------------
int luai_loadfile(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);
	String filename = lua_tostring(pLuaState, 2);

	bool r = spLua->loadFile(filename);

	lua_pushboolean(pLuaState, r);

	return 1;
}

int luai_setrecord(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	Record r_value = record(pLuaState, 3);

	spLua->set(key, r_value);

	return 0;
}

int luai_setstring(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	String value = lua_tostring(pLuaState, 2);

	spLua->set(key, value);

	return 0;
}

int luai_setlayout(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	sp<Layout> spValue = layout(pLuaState, 3);

	spLua->set(key, spValue);

	return 0;
}

int luai_setscope(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	sp<Scope> spValue = scope(pLuaState, 3);

	spLua->set(key, spValue);

	return 0;
}

int luai_setcomponent(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	sp<Component> spValue = component<Component>(pLuaState, 3);

	spLua->set(key, spValue);

	return 0;
}

int luai_execute(lua_State *pLuaState)
{
	sp<LuaI> spLua = component<LuaI>(pLuaState, 1);

	bool r;
	int cnt = lua_gettop(pLuaState);
	if(cnt >= 2)
	{
		String name = lua_tostring(pLuaState, 2);
		r = spLua->execute(name);
	}
	else
	{
		r = spLua->execute();
	}


	lua_pushboolean(pLuaState, r);

	return 1;
}

// EventMapI -----------------------------------------------------------------
int eventmap_bind(lua_State *pLuaState)
{
	sp<EventMapI> spEM = component<EventMapI>(pLuaState, 1);
	WindowEvent::Source source =
		(WindowEvent::Source)(int)lua_tonumber(pLuaState, 2);
	WindowEvent::Item item =
		(WindowEvent::Item)(int)lua_tonumber(pLuaState, 3);
	WindowEvent::State state =
		(WindowEvent::State)(int)lua_tonumber(pLuaState, 4);
	sp<Layout> spValue = layout(pLuaState, 5);

	spEM->bind(source, item, state, spValue);

	return 0;
}

// MaskI --------------------------------------------------------------------
int mask_setmask(lua_State *pLuaState)
{
	sp<MaskI> spMask = component<MaskI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	WindowEvent::Source source =
		(WindowEvent::Source)(int)lua_tonumber(pLuaState, 3);
	WindowEvent::Item item =
		(WindowEvent::Item)(int)lua_tonumber(pLuaState, 4);
	WindowEvent::State state =
		(WindowEvent::State)(int)lua_tonumber(pLuaState, 5);

	spMask->maskSet(key, WindowEvent::Mask(source, item, state));

	return 0;
}

int mask_createmask(lua_State *pLuaState)
{
	sp<MaskI> spMask = component<MaskI>(pLuaState, 1);
	sp<Layout> spLayout = layout(pLuaState, 2);
	String key = lua_tostring(pLuaState, 3);

	Record r_mask = spMask->maskCreate(spLayout, key);

	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	t_lua_record::push(pContext, r_mask);

	return 1;
}

int mask_init(lua_State *pLuaState)
{
	sp<MaskI> spMask = component<MaskI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);

	spMask->maskInit(key);

	return 0;
}

int mask_enable(lua_State *pLuaState)
{
	sp<MaskI> spMask = component<MaskI>(pLuaState, 1);
	int i = (int)lua_tonumber(pLuaState, 2);

	spMask->maskEnable(i);

	return 0;
}

// EventContextI -------------------------------------------------------------
//~int eventcontext_fontbase(lua_State *pLuaState)
//~{
//~	sp<EventContextI> spEC = component<EventContextI>(pLuaState, 1);
//~
//~	lua_pushnumber(pLuaState, spEC->fontBase());
//~
//~	return 1;
//~}

// DrawI ---------------------------------------------------------------------
//~int draw_setfontbase(lua_State *pLuaState)
//~{
//~	sp<DrawI> spD = component<DrawI>(pLuaState, 1);
//~	U32 fontbase = (U32)lua_tonumber(pLuaState, 2);
//~
//~	spD->font()->setFontBase(fontbase);
//~
//~	return 0;
//~}



// ConfigI -------------------------------------------------------------------

int config_set(lua_State *pLuaState)
{
	sp<ConfigI> spC = component<ConfigI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	String path = lua_tostring(pLuaState, 3);

//~	spC->cfg<String>(key) = path;
	const sp<TypeMaster> &spTM = spC->registry()->master()->typeMaster();
	Instance instance;
	instance.set<String>(spTM,path);
	spC->configSet(key, instance);

	return 0;
}

int config_setParent(lua_State *pLuaState)
{
	sp<ConfigI> spC = component<ConfigI>(pLuaState, 1);
	sp<ConfigI> spParent = component<ConfigI>(pLuaState, 2);


	spC->configParent(spParent);

	return 0;
}


#ifdef FE_LUA_ARCHITECTURE
// PaletteI ------------------------------------------------------------------
int palette_set(lua_State *pLuaState)
{
	sp<PaletteI> spP = component<PaletteI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);
	sp<Component> spC = component<Component>(pLuaState, 3);

	spP->set(key, spC);

	return 0;
}

int palette_get(lua_State *pLuaState)
{
	sp<PaletteI> spP = component<PaletteI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);

	sp<Component> spC = spP->get(key);

	LuaComponentObject::pushComponent(pLuaState, spC);

	return 1;
}

int palette_remove(lua_State *pLuaState)
{
	sp<PaletteI> spP = component<PaletteI>(pLuaState, 1);
	String key = lua_tostring(pLuaState, 2);

	spP->remove(key);

	return 0;
}
#endif

// FileStreamI ---------------------------------------------------------------
int filestream_bind(lua_State *pLuaState)
{
	sp<data::FileStreamI> spFS = component<data::FileStreamI>(pLuaState, 1);
	LuaScopeObject *pS = to<LuaScopeObject>(pLuaState, 2, LuaContext::e_scope);
	sp<Scope> spScope = pS->data();
	spFS->bind(spScope);

	return 0;
}

int filestream_output(lua_State *pLuaState)
{
	sp<data::FileStreamI> spFS = component<data::FileStreamI>(pLuaState, 1);
	String filename = lua_tostring(pLuaState, 2);
	sp<RecordGroup> spRG = recordgroup(pLuaState, 3);

	spFS->output(filename, spRG);

	return 0;
}

int filestream_input(lua_State *pLuaState)
{
	sp<data::FileStreamI> spFS = component<data::FileStreamI>(pLuaState, 1);
	String filename = lua_tostring(pLuaState, 2);

	sp<RecordGroup> spRG;

	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));
	lua_pop(pLuaState, 1);

	spRG = spFS->input(filename);
	t_lua_group::push(pContext, spRG);

	return 1;
}

// DispatchI -----------------------------------------------------------------
int dispatch_call(lua_State *pLuaState)
{
	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));
	lua_pop(pLuaState, 1);

	String method_name = lua_tostring(pLuaState,lua_upvalueindex(1));

	sp<DispatchI> spD = component<DispatchI>(pLuaState, 1);

	SignatureMap &sigmap = spD->signatures();
	SignatureMap::iterator i_sig = sigmap.find(method_name);
	if(i_sig != sigmap.end())
	{
		Signature &sig = i_sig->second;

		const int luaargcnt = lua_gettop(pLuaState) - 1;
		const int specCount = sig.argspecs().size();
		const int returnCount = specCount - luaargcnt;

		Array<Instance> arglist(sig.argspecs().size());
		for(int i = specCount-1; i >= 0; i--)
		{
			arglist[i].create(sig.argspecs()[i].m_spType);

			if(i >= returnCount)
			{
				pContext->popInstance(arglist[i]);
			}
		}
		// dispatch the call
		if(!spD->checkedCall(method_name, arglist))
		{
			LuaContext::error(pLuaState, "dispatch call mismatch");
			return 0;
		}

		// convert array of Instances back
		for(unsigned int i = 0; i < arglist.size(); i++)
		{
			pContext->pushInstance(arglist[i]);
		}

		return arglist.size();
	}

	return 0;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

/** This is where you directly bind interfaces to lua. */
void LuaComponentObject::populateMethods(void)
{
	if(is<SignalerI>())
	{
		setMethod("insert", signal_insert);
		setMethod("signal", signal_signal);
	}
	if(is<HandlerI>())
	{
		setMethod("handle", handler_handle);
	}
	if(is<WindowI>())
	{
		setMethod("getEventContext", window_getEventContext);
		setMethod("open", window_open);
		setMethod("setTitle", window_settitle);
		setMethod("setSize", window_setsize);
	}
	if(is<ViewerI>())
	{
		setMethod("bind", viewer_bind);
	}
#ifdef FE_LUA_ARCHITECTURE
	if(is<AnnotateI>())
	{
		setMethod("attach", annotate_attach);
	}
	if(is<PaletteI>())
	{
		setMethod("set", palette_set);
		setMethod("get", palette_get);
		setMethod("remove", palette_remove);
	}
#endif
	if(is<LuaI>())
	{
		setMethod("loadFile", luai_loadfile);
		setMethod("setRecord", luai_setrecord);
		setMethod("setLayout", luai_setlayout);
		setMethod("setString", luai_setstring);
		setMethod("setComponent", luai_setcomponent);
		setMethod("setScope", luai_setscope);
		setMethod("execute", luai_execute);
	}
	if(is<EventMapI>())
	{
		setMethod("bind", eventmap_bind);
	}
	if(is<MaskI>())
	{
		setMethod("maskSet", mask_setmask);
		setMethod("maskCreate", mask_createmask);
		setMethod("maskInit", mask_init);
		setMethod("maskEnable", mask_enable);
	}
	if(is<ParserI>())
	{
		setMethod("parse", parser_parse);
	}
//~	if(is<EventContextI>())
//~	{
//~		setMethod("fontBase", eventcontext_fontbase);
//~	}
//~	if(is<DrawI>())
//~	{
//~		setMethod("setFontBase", draw_setfontbase);
//~	}
	if(is<ConfigI>())
	{
		setMethod("cfg", config_set);
		setMethod("cfgParent", config_setParent);
	}
	if(is<DispatchI>())
	{
		sp<DispatchI> spDispatchI(data());
		SignatureMap &sigmap = spDispatchI->signatures();
		for(SignatureMap::iterator i_sig = sigmap.begin();
			i_sig != sigmap.end(); i_sig++)
		{
			setMethod(i_sig->first, dispatch_call);
		}
	}
	if(is<data::FileStreamI>())
	{
		setMethod("bind", filestream_bind);
		setMethod("output", filestream_output);
		setMethod("input", filestream_input);
	}
}


} /* namespace ext */
} /* namespace fe */

