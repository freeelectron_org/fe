/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "beaconServer.pmh"

#define FE_BEACON_SERVER_STARTUP_VERBOSE FALSE
#define DEFAULT_STARTING_BEACON_PORT 5000
#define DEFAULT_STARTING_GD_PORT 5001

using namespace fe;
using namespace ext;

namespace beacon
{

bool BeaconServerStartup::startup(const char *fileName)
{
#if FE_BEACON_SERVER_STARTUP_VERBOSE
	feLog("BeaconServerStartup::startup file name = %s\n", fileName);
#endif

	sp<SingleMaster> spSingleMaster=SingleMaster::create();
	sp<Master> spMaster=spSingleMaster->master();
	sp<Registry> spRegistry=spMaster->registry();

	spRegistry->manage("feAutoLoadDL");

	sp<CatalogReaderI> spCatalogReader = spRegistry->create("CatalogReaderI.*.*.yaml");
    if(!spCatalogReader.isValid())
	{
		feLog("BeaconServerStartup::startup: spCatalogReader invalid\n");
        return false;
	}

	sp<Catalog> spCatalogIn = spMaster->createCatalog("input");
	if(!spCatalogIn.isValid())
	{
		feLog("BeaconServerStartup::startup: spCatalogIn invalid\n");
        return false;
	}

    // Load catalog
	if( spCatalogReader->load(fe::String(fileName),spCatalogIn) == 0 )
    {
        feLog("BeaconServerStartup::startup: Unable to load %s\n", fileName);
        return false;
    }

	// Setup default values
	uint16_t beaconRequestPort = DEFAULT_STARTING_BEACON_PORT;
	uint16_t gdRequestPort     = DEFAULT_STARTING_GD_PORT;

	// Read values from configuration file
    beaconRequestPort = spCatalogIn->catalogOrDefault<int>(
							"RequestPort",
							beaconRequestPort);
    gdRequestPort = spCatalogIn->catalogOrDefault<int>(
							"GDRequestPort",
							gdRequestPort);

#if FE_BEACON_SERVER_STARTUP_VERBOSE
	feLog("BeaconServerStartup::startup"
			" beacon request port: %u, Global Dictionary request port: %u"
			" Global Dictionary verification port: %u\n",
			beaconRequestPort, gdRequestPort);
#endif

	// Send any global dictionary values in configuration file to the Beacon
	Array<String> properties;
	spCatalogIn->catalogProperties("GlobalDictionary",properties);

	std::vector<std::pair<String,String>> setList;

	for(int i = 0; i < (int)properties.size(); i++)
	{
		String value = spCatalogIn->catalog<String>("GlobalDictionary",
				properties[i]);
		feLog("Property %d - %s, %s\n",
				i, properties[i].c_str(), value.c_str());
		setList.push_back(std::make_pair(properties[i],value));
	}

	m_globalDictionary.setInitialEntries(setList);

	return start(beaconRequestPort, gdRequestPort);
}

} // namespace beacon
