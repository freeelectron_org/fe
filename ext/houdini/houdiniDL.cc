/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define MAKING_DSO

//* present as a Houdini DSO
#if !defined(_MSC_VER) || FE_HOUDINI_ALLOWS_MSVS_2022
#include <UT/UT_DSOVersion.h>
#else

//* HACK reduced from UT_DSOVersion.h

#include <SYS/SYS_Types.h>
#include <SYS/SYS_Version.h>
#include <SYS/SYS_Visibility.h>

extern "C"
{
    class UT_DSOInfo
    {
        public:
        bool loadGlobal;
    };
}

extern "C" SYS_VISIBILITY_EXPORT void
HoudiniDSOVersion(const char **v)
{
//	feLogError("FE HoudiniDSOVersion is \"%s\"\n",SYS_VERSION_RELEASE);
	*v = SYS_VERSION_RELEASE;
}

extern "C" SYS_VISIBILITY_EXPORT unsigned
HoudiniCompilerVersion()
{
	feLogError("FE HoudiniCompilerVersion forcing %d -> 1920\n",_MSC_VER);
//	return (_MSC_VER);
	return (1920);	//* HACK masquerade as using older compiler
}

#endif

extern "C" FE_DL_EXPORT void HoudiniDSOInit(UT_DSOInfo& dsoinfo)
{
//	feLog("HoudiniDSOInit\n");

	//* force Houdini to load with RTLD_GLOBAL (needed for dynamic_cast<>)
	dsoinfo.loadGlobal=true;
}

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
//	feLog("HoudiniDL ListDependencies\n");
	list.append(new String("fexMetaDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> a_spMaster)
{
//	feLog("HoudiniDL CreateLibrary\n");

	assertHoudini(a_spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<SurfaceCurvesHoudini>(
			"SurfaceI.SurfaceCurvesHoudini.fe");
	pLibrary->add<SurfacePointsHoudini>(
			"SurfaceI.SurfacePointsHoudini.fe");
	pLibrary->add<SurfaceTrianglesHoudini>(
			"SurfaceI.SurfaceTrianglesHoudini.fe");
	pLibrary->add<SurfaceAccessibleHoudini>(
			"SurfaceAccessibleI.SurfaceAccessibleHoudini.fe");
	pLibrary->add<SurfaceAccessorHoudini>(
			"SurfaceAccessorI.SurfaceAccessorHoudini.fe");
	pLibrary->add<HoudiniDraw>(
			"DrawI.HoudiniDraw.fe");
	pLibrary->add<HoudiniRamp>(
			"RampI.HoudiniRamp.fe");
	pLibrary->add<FontHoudini>(
			"FontI.FontHoudini.fe");

	pLibrary->add<CacheOp>(
			"OperatorSurfaceI.CacheOp.fe");
	pLibrary->add<PortalOp>(
			"OperatorSurfaceI.PortalOp.fe.prototype");

#if FE_HOUDINI_15_PLUS
	pLibrary->add<UnveilOp>(
			"OperatorSurfaceI.UnveilOp.fe");
	pLibrary->add<VeilOp>(
			"OperatorSurfaceI.VeilOp.fe");
#endif

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
//	feLog("InitializeLibrary\n");

	spLibrary->registry()->prioritize("FontI.FontHoudini.fe",2);
}

}
