/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <meta/meta.pmh>

using namespace fe;
using namespace fe::ext;

void MetaGraph::clear(void)
{
	m_nodeMap.clear();
	m_voidMap.clear();
	m_nameMap.clear();
}

U32 MetaGraph::inputCount(String a_nodeName) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	return (it==m_voidMap.end())? 0: inputCount(it->second);
}

String MetaGraph::inputConnector(String a_nodeName,U32 a_index) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	return (it==m_voidMap.end())? "": inputConnector(it->second,a_index);
}

BWORD MetaGraph::hasInputConnector(String a_nodeName,
	String a_inputConnector) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	return (it!=m_voidMap.end()) &&
			hasInputConnector(it->second,a_inputConnector);
}

String MetaGraph::inputNode(String a_nodeName,String a_inputConnector) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	void* nativeNode=(it==m_voidMap.end())?
			NULL: inputNode(it->second,a_inputConnector);

	std::map<const void*,String>::const_iterator it2=m_nameMap.find(nativeNode);
	return (it2==m_nameMap.end())? "": it2->second;
}

String MetaGraph::inputNodeOutputConnector(String a_nodeName,
	String a_inputConnector) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	if(it==m_voidMap.end())
	{
		return "";
	}

	std::map< const void*, sp<MetaNode> >::const_iterator it2=
			m_nodeMap.find(const_cast<void*>(it->second));
	if(it2==m_nodeMap.end())
	{
		return "";
	}

	sp<DAGNode::Connection> spConnection=
			it2->second->parentConnection(a_inputConnector);
	if(spConnection.isNull())
	{
		return "";
	}

	return spConnection->parentConnector();
}

U32 MetaGraph::outputCount(String a_nodeName) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	return (it==m_voidMap.end())? 0: outputCount(it->second);
}

String MetaGraph::outputConnector(String a_nodeName,U32 a_index) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	return (it==m_voidMap.end())? "": outputConnector(it->second,a_index);
}

BWORD MetaGraph::hasOutputConnector(String a_nodeName,
	String a_outputConnector) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	return (it!=m_voidMap.end()) &&
			hasOutputConnector(it->second,a_outputConnector);
}

String MetaGraph::outputNode(String a_nodeName,String a_outputConnector) const
{
	std::map<String,const void*>::const_iterator it=m_voidMap.find(a_nodeName);
	void* nativeNode=(it==m_voidMap.end())?
			NULL: outputNode(it->second,a_outputConnector);

	std::map<const void*,String>::const_iterator it2=m_nameMap.find(nativeNode);
	return (it2==m_nameMap.end())? "": it2->second;
}

U32 MetaGraph::inputCount(const void* a_rNativeNode) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	return (it==m_nodeMap.end())? 0: it->second->parentConnectorCount();
}

String MetaGraph::inputConnector(
	const void* a_rNativeNode,U32 a_index) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	return (it==m_nodeMap.end())? "<none>":
			it->second->parentConnector(a_index);
}

BWORD MetaGraph::hasInputConnector(const void* a_rNativeNode,
	String a_inputConnector) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	if(it==m_nodeMap.end())
	{
		return NULL;
	}
	return (it!=m_nodeMap.end()) &&
			it->second->hasParentConnector(a_inputConnector);
}

void* MetaGraph::inputNode(
	const void* a_rNativeNode,String a_inputConnector) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	if(it==m_nodeMap.end())
	{
		return NULL;
	}
	sp<DAGNode::Connection> spConnection=
			it->second->parentConnection(a_inputConnector);
	if(spConnection.isNull())
	{
		return NULL;
	}
	sp<MetaNode> spMetaNode=spConnection->parent();
	return spMetaNode->m_nativeNode;
}

U32 MetaGraph::outputCount(const void* a_rNativeNode) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	return (it==m_nodeMap.end())? 0: it->second->childConnectorCount();
}

String MetaGraph::outputConnector(
	const void* a_rNativeNode,U32 a_index) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	return (it==m_nodeMap.end())? "<none>":
			it->second->childConnector(a_index);
}

BWORD MetaGraph::hasOutputConnector(const void* a_rNativeNode,
	String a_outputConnector) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	if(it==m_nodeMap.end())
	{
		return NULL;
	}
	return (it!=m_nodeMap.end()) &&
			it->second->hasChildConnector(a_outputConnector);
}

void* MetaGraph::outputNode(
	const void* a_rNativeNode,String a_outputConnector) const
{
	std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.find(const_cast<void*>(a_rNativeNode));
	if(it==m_nodeMap.end())
	{
		return NULL;
	}
	sp<DAGNode::Connection> spConnection=
			it->second->childConnection(a_outputConnector);
	if(spConnection.isNull())
	{
		return NULL;
	}
	hp<MetaNode> hpMetaNode=spConnection->child();
	return hpMetaNode->m_nativeNode;
}

void MetaGraph::add(void* a_nativeNode,String a_nodeName)
{
	sp<MetaNode>& rspMetaNode=m_nodeMap[a_nativeNode];
	if(rspMetaNode.isNull())
	{
		rspMetaNode=new MetaNode();
	}
	m_voidMap[a_nodeName]=a_nativeNode;
	m_nameMap[a_nativeNode]=a_nodeName;
	rspMetaNode->set(a_nativeNode,a_nodeName);
}

void MetaGraph::addInput(const void* a_nativeNode,String a_inputConnector)
{
	sp<MetaNode>& rspMetaNode=m_nodeMap[a_nativeNode];
	if(rspMetaNode.isNull())
	{
		rspMetaNode=new MetaNode();
	}
	rspMetaNode->addParentConnector(a_inputConnector);
}

void MetaGraph::addOutput(const void* a_nativeNode,String a_outputConnector)
{
	sp<MetaNode>& rspMetaNode=m_nodeMap[a_nativeNode];
	if(rspMetaNode.isNull())
	{
		rspMetaNode=new MetaNode();
	}
	rspMetaNode->addChildConnector(a_outputConnector);
}

BWORD MetaGraph::addConnection(String a_outputName,String a_outputConnector,
	String a_inputName,String a_inputConnector)
{
	std::map<String,const void*>::const_iterator it=
			m_voidMap.find(a_outputName);
	if(it==m_voidMap.end())
	{
		return FALSE;
	}
	void* a_outputNode=const_cast<void*>(it->second);

	it=m_voidMap.find(a_inputName);
	if(it==m_voidMap.end())
	{
		return FALSE;
	}
	void* a_inputNode=const_cast<void*>(it->second);

	return addConnection(a_outputNode,a_outputConnector,
			a_inputNode,a_inputConnector);
}

BWORD MetaGraph::removeConnection(String a_inputName,String a_inputConnector)
{
	std::map<String,const void*>::const_iterator it=
			m_voidMap.find(a_inputName);
	if(it==m_voidMap.end())
	{
		return FALSE;
	}
	void* a_inputNode=const_cast<void*>(it->second);

	return removeConnection(a_inputNode,a_inputConnector);
}

BWORD MetaGraph::addConnection(void* a_outputNode,String a_outputConnector,
						void* a_inputNode,String a_inputConnector)
{
	sp<MetaNode>& rspParent=m_nodeMap[a_outputNode];
	sp<MetaNode>& rspChild=m_nodeMap[a_inputNode];

	//* in case we just created either
	if(rspParent.isNull())
	{
		add(a_outputNode,"");
	}
	if(rspChild.isNull())
	{
		add(a_inputNode,"");
	}

	removeConnection(a_inputNode,a_inputConnector);
	return rspChild->attachTo(rspParent,a_inputConnector,a_outputConnector);
}

BWORD MetaGraph::removeConnection(void* a_inputNode,String a_inputConnector)
{
	sp<MetaNode>& rspChild=m_nodeMap[a_inputNode];

	if(rspChild.isNull())
	{
		return FALSE;
	}

	return rspChild->detach(a_inputConnector);
}

void MetaGraph::dump(void) const
{
	feLog("\nMetaGraph::dump\n");

	for(std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.begin();it!=m_nodeMap.end();it++)
	{
		const sp<MetaNode>& rspMetaNode=it->second;
		const String& rNodeName=rspMetaNode->m_nodeName;

		if(rNodeName.empty())
		{
			continue;
		}

		feLog("node \"%s\"\n",rNodeName.c_str());

		const void* rNativeNode=rspMetaNode->m_nativeNode;

		const U32 inputs=inputCount(rNativeNode);
		for(U32 inputIndex=0;inputIndex<inputs;inputIndex++)
		{
			const String& rInputConnector=
					inputConnector(rNativeNode,inputIndex);
			feLog("  input \"%s\"\n",rInputConnector.c_str());

			const void* rInputNode=inputNode(rNativeNode,rInputConnector);
			std::map< const void*, sp<MetaNode> >::const_iterator it=
					m_nodeMap.find(const_cast<void*>(rInputNode));
			if(it!=m_nodeMap.end())
			{
				const sp<MetaNode>& rspMetaNode=it->second;

				feLog("  from \"%s\"\n",rspMetaNode->m_nodeName.c_str());
			}
		}

		const U32 outputs=outputCount(rNativeNode);
		for(U32 outputIndex=0;outputIndex<outputs;outputIndex++)
		{
			const String& rOutputConnector=
					outputConnector(rNativeNode,outputIndex);
			feLog("  output \"%s\"\n",rOutputConnector.c_str());

			const void* rOutputNode=outputNode(rNativeNode,rOutputConnector);
			std::map< const void*, sp<MetaNode> >::const_iterator it=
					m_nodeMap.find(const_cast<void*>(rOutputNode));
			if(it!=m_nodeMap.end())
			{
				const sp<MetaNode>& rspMetaNode=it->second;

				feLog("  to \"%s\"\n",rspMetaNode->m_nodeName.c_str());
			}
		}
	}
}

String MetaGraph::generateDot(void) const
{
	String result=
			"digraph G {\n"
			"concentrate=false\n"
			"splines=true\n"
			"bgcolor=\"#ffffbb\"\n"
			"edge [fontsize=7 color=\"#aaaaaa\"]\n"
			"node [fontsize=8 margin=\"0.04,0.02\" height=0.1]\n"
			"node [fillcolor=\"#ddffdd\" style=filled shape=Mrecord]\n";
#if FALSE
	result+=
			"node [label=<\n"
			"	<table border=\"0\" cellborder=\"0\"\n"
			"		cellspacing=\"0\" cellpadding=\"1\">\n"
			"	<tr>\n"
			"		<td></td>\n"
			"		<td border=\"1\" port=\"in0\">0</td>\n"
			"		<td border=\"1\" port=\"in1\">1</td>\n"
			"		<td border=\"1\" port=\"in2\">2</td>\n"
			"		<td border=\"1\" port=\"in3\">3</td>\n"
			"		<td></td>\n"
			"	</tr>\n"
			"	<tr>\n"
			"		<td border=\"1\" bgcolor=\"yellow\">B</td>\n"
			"		<td border=\"1\" colspan=\"4\">\\N</td>\n"
			"		<td border=\"1\" bgcolor=\"blue\">R</td>\n"
			"	</tr>\n"
			"	<tr>\n"
			"		<td></td>\n"
			"		<td border=\"1\" bgcolor=\"cyan\">X</td>\n"
			"		<td border=\"1\" port=\"out0\" colspan=\"2\">out</td>\n"
			"		<td border=\"1\" bgcolor=\"magenta\">M</td>\n"
			"		<td></td>\n"
			"	</tr>\n"
			"	</table>\n"
			"	>]\n";
#else
	result+="node [label=\"|{{<in0> 0|<in1> 1|<in2> 2}|\\N|<out0> out}|\"]\n";
#endif

	for(std::map< const void*, sp<MetaNode> >::const_iterator it=
			m_nodeMap.begin();it!=m_nodeMap.end();it++)
	{
		const sp<MetaNode>& rspMetaNode=it->second;
		const String& rNodeName=rspMetaNode->m_nodeName;

		if(rNodeName.empty())
		{
			continue;
		}

		const void* rNativeNode=rspMetaNode->m_nativeNode;
		const U32 inputs=inputCount(rNativeNode);

		result+=rNodeName;
		result+=" [label=\"|{{";
		for(U32 inputIndex=0;inputIndex<inputs;inputIndex++)
		{
			const String& rInputConnector=
					inputConnector(rNativeNode,inputIndex);
			const I32 max=2;
			char buffer[max+1];
			strncpy(buffer,rInputConnector.c_str(),max);
			buffer[max]=0;

			result.catf("<in%d> %s%s",inputIndex,buffer,
					(inputIndex==inputs-1)? "": "|");
		}

//		result+="}|\\N|<out0> out}|\"]\n";
		result+="}|<out0> \\N}|\"]\n";

		for(U32 inputIndex=0;inputIndex<inputs;inputIndex++)
		{
			const String& rInputConnector=
					inputConnector(rNativeNode,inputIndex);

			void* rInputNode=inputNode(rNativeNode,rInputConnector);
			std::map< const void*, sp<MetaNode> >::const_iterator it=
					m_nodeMap.find(rInputNode);
			if(it!=m_nodeMap.end())
			{
				const sp<MetaNode>& rspMetaNode=it->second;

				result+=rspMetaNode->m_nodeName+":out0:s -> "+rNodeName;
				result.catf(":in%d:n",inputIndex);
				result+=" [label=\""+rInputConnector+"\"]\n";
			}
		}
	}

	result+="}\n";

	return result;
}

sp<ImageI> MetaGraph::render(void)
{
	if(m_spRenderer.isNull())
	{
		m_spRenderer=registry()->create("ImageI.GraphDot");
	}
	if(m_spRenderer.isNull())
	{
		return sp<ImageI>(NULL);
	}

	const String newSource=generateDot();
	if(m_source!=newSource)
	{
		m_source=newSource;
		m_spRenderer->interpretSelect(newSource);
	}
	return m_spRenderer;
}
