/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

namespace fe
{

//* static
void CatalogBuffer::resize(sp<Catalog> a_spCatalog,
	String a_name,String a_property,I32 a_size)
{
	Instance instance;
	if(!a_spCatalog->catalogLookup(a_name,a_property,instance))
	{
		feLog("CatalogBuffer::resize no property \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return;
	}

	const sp<BaseType> spBaseType=instance.type();
	if(spBaseType.isNull())
	{
		feLog("CatalogBuffer::resize bad type for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return;
	}

	sp<BaseTypeInfoArray> spBaseTypeInfoArray=spBaseType->getInfo();
	if(spBaseTypeInfoArray.isNull())
	{
		feLog("CatalogBuffer::resize no array for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return;
	}

	spBaseTypeInfoArray->resize(a_size,instance.data());
}

//* static
I32 CatalogBuffer::access(sp<Catalog> a_spCatalog,
	String a_name,String a_property,void*& a_ppBuffer)
{
	Instance instance;
	if(!a_spCatalog->catalogLookup(a_name,a_property,instance))
	{
		feLog("CatalogBuffer::access no property \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return -1;
	}

	const sp<BaseType> spBaseType=instance.type();
	if(spBaseType.isNull())
	{
		feLog("CatalogBuffer::access bad type for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return -1;
	}

	sp<BaseTypeInfoArray> spBaseTypeInfoArray=spBaseType->getInfo();
	if(spBaseTypeInfoArray.isNull())
	{
		feLog("CatalogBuffer::access no array for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return -1;
	}

	return spBaseTypeInfoArray->access(a_ppBuffer,instance.data());
}

} /* namespace */
