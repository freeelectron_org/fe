import sys
import os
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
        'odeLib',
        'fexSignalLib',
        'fexWindowLib',
        'fexViewerLib',
        'fexODEDLLib',
        'fexDataToolLib' ]

    #tests = [  'xOde' ]
    tests = []

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        #if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            #exe.linkmap['physics'] = "ode.lib"
        #else:
            #exe.linkmap['physics'] = "-lode"

        forge.deps([t + "Exe"], deplibs)

    plugin_path = os.path.join(module.modPath, 'plugin')
