import sys
import os
import shutil
import re
forge = sys.modules["forge"]

re_lib = re.compile(r'.*\.(dll|so|lib|a)')

def setup(module):

    setup_dep(module)

    srclist = [
        "platform.pmh",
        "backtrace",
        "debug",
        "DL_Loader",
        "DualString",
        "Exception",
        "Mutex",
        "Regex",
        "Result",
        "String",
        "System",
        "Thread"
        ]

    module.cppmap = {}

    # OpenMP
    use_omp = int(os.environ["FE_USE_OMP"])
    if use_omp:
        srclist += [ "ompthread" ]

        if forge.compiler_brand == 'clang':
            module.cppmap['omp'] = '-fopenmp=libomp'
        else:
            module.cppmap['omp'] = '-fopenmp'

        forge.cppmap['omp'] = '-DFE_OPENMP'

        module.linkmap = {}
        module.linkmap['omp'] = '-lgomp'

    if "FE_BINARY_SUFFIX" in os.environ:
        fe_binary_suffix = os.environ["FE_BINARY_SUFFIX"]
        if fe_binary_suffix != "":
            forge.cppmap['binary_suffix'] = '-DFE_BINARY_SUFFIX="' + fe_binary_suffix + '"'

    if use_omp and forge.compiler_brand == 'clang':
        forge.linkmap['omp'] = '-lgomp'

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        module.Lib("fePlatform",srclist)
    elif forge.fe_os == 'FE_OSX':
        lib = module.DLL("fePlatform",srclist)
        forge.deps("fePlatformLib", ["feMemoryLib"])
    else:
        dll = module.DLL("fePlatform",srclist)
        dll.linkmap = {}

    # TEMP off
    module.Module('test')

class CopyDepTarget(forge.Target):
    def __init__(self,str):
            forge.Target.__init__(self,str)
            self.help = """\
Copy external dependencies from 'dep' directory to 'lib' directory
for platform.
"""

    def Build(self, reference_time):
        deps_path = os.path.join(os.path.dirname(forge.rootPath), 'dep')
        if os.path.exists(deps_path):
            deps = os.listdir(deps_path)
            for dep in deps:
                lib_path = os.path.join(os.path.dirname(forge.rootPath), 'dep', dep, 'lib', forge.api)
                if os.path.exists(lib_path):
                    src = lib_path
                    dst = forge.libPath

                    for f in os.listdir(src):
                        if f[0] != '.':
                            if re_lib.match(f):
                                fn = os.path.join(src,f)
                                shutil.copy2(fn, dst)

                    src = os.path.join(os.path.dirname(forge.rootPath), 'dep', dep, 'include')
                    dst = os.path.join(forge.libPath, dep)
                    if not os.path.exists(dst):
                        os.makedirs(dst)

                    for f in os.listdir(src):
                        if f[0] != '.':
                            src_d = os.path.join(src, f)
                            dst_d = os.path.join(dst, f)
                            if os.path.isdir(src_d):
                                if os.path.exists(dst_d):
                                    shutil.rmtree(dst_d)
                                shutil.copytree(src_d, dst_d)
                            else:
                                shutil.copy2(src_d, dst_d)

copyTarget = CopyDepTarget("copydep")
forge.targetRegistry['copydep'] = copyTarget


def setup_dep(module):
    deps_path = os.path.join(os.path.dirname(forge.rootPath), 'dep')
    if os.path.exists(deps_path):
        deps = os.listdir(deps_path)
        for dep in deps:
            lib_path = os.path.join(os.path.dirname(forge.rootPath), 'dep', dep, 'lib', forge.api)
            if os.path.exists(lib_path):
                forge.includemap[dep] = os.path.join(os.path.join(forge.libPath, dep))

def setup_dep_orig(module):
    copy_dep = os.getenv('FE_COPY_DEP')
    do_copy = 0
    if copy_dep != None:
        do_copy = int(copy_dep)

    deps_path = os.path.join(os.path.dirname(forge.rootPath), 'dep')
    if os.path.exists(deps_path):
        deps = os.listdir(deps_path)
        for dep in deps:
            lib_path = os.path.join(os.path.dirname(forge.rootPath), 'dep', dep, 'lib', forge.api)
            if os.path.exists(lib_path):
                src = lib_path
                dst = forge.libPath

                for f in os.listdir(src):
                    if f[0] != '.':
                        if re_lib.match(f):
                            fn = os.path.join(src,f)
                            if do_copy:
                                shutil.copy2(fn, dst)

                src = os.path.join(os.path.dirname(forge.rootPath), 'dep', dep, 'include')
                dst = os.path.join(forge.libPath, dep)
                if not os.path.exists(dst):
                    os.makedirs(dst)

                for f in os.listdir(src):
                    if f[0] != '.':
                        src_d = os.path.join(src, f)
                        dst_d = os.path.join(dst, f)
                        if os.path.isdir(src_d):
                            if os.path.exists(dst_d):
                                if do_copy:
                                    shutil.rmtree(dst_d)
                            if do_copy:
                                shutil.copytree(src_d, dst_d)
                        else:
                            if do_copy:
                                shutil.copy2(src_d, dst_d)

                forge.includemap[dep] = os.path.join(dst)


