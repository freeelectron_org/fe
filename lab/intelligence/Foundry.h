/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Foundry_h__
#define __intelligence_Foundry_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Foundry RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Foundry: public Scanner
{
	public:
		Functor<String>				design;
		Functor<String>				nextDesign;
		Functor< sp<RecordGroup> >	prototype;

				Foundry(void)		{ setName("Foundry"); }
virtual	void	addFunctors(void)
				{
					Scanner::addFunctors();

					add(design,		FE_SPEC("ai:design",
							"Name of a RG file to construct"));
					add(nextDesign,	FE_SPEC("ai:nextDesign",
							"Next RG file to construct"));
					add(prototype,	FE_SPEC("ai:prototype",
							"RecordGroup to be duplicated"));
				}
virtual	void	initializeRecord(void)
				{
					Scanner::initializeRecord();

					identifier()="Foundry";

					Observation observationRV;
					observationRV.bind(scope());
					purview()=observationRV.createRecord();
					observationRV.observer()=record();

					Sphere sphereRV;
					sphereRV.bind(scope());
					observationRV.shape()=sphereRV.createRecord();

					sphereRV.radius()=2.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Foundry_h__ */
