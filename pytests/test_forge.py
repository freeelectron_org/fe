#! /usr/bin/env python

# -----------------------------------------------------------------------------
import importlib.util
import os
import sys
import subprocess
import unittest

# -----------------------------------------------------------------------------
# Common defines

# ROOT_PKG_NAME = 'forge'
UTIL_NAME = 'forge'
# UTIL_MODULE = f'{ROOT_PKG_NAME}.{UTIL_NAME}'
UTIL_MODULE = os.path.join('bin', f'{UTIL_NAME}.py')
# UTIL_CMD = ['python', '-m', f'{UTIL_MODULE}']
UTIL_CMD = ['python', f'{UTIL_MODULE}']
ENCODING = 'utf8'

# return codes
RETCODE_SUCCESS = 0
RETCODE_MISC = 1
RETCODE_MISUSE_MISSING_ARG = 2


# -----------------------------------------------------------------------------
def module_import(name, pkg=None):

    module = importlib.import_module(name, pkg)
    sys.modules[name] = module
    return module


# -----------------------------------------------------------------------------
class TestMainNoArgs(unittest.TestCase):

    def test_util_invoke(self):

        try:
            # Currently forge doesn't really do what it probably should, when
            # invoked with no args. So we'll noop this for now. Will removely
            # entirely shortly if there's no expectation to ever do this.
            proc = subprocess.run(
                ['ls', '-al', f'{UTIL_MODULE}'],
                # UTIL_CMD,
                capture_output=True,
                check=True,
            )
        except BaseException as err:
            self.fail("{} failed parameter-less invocation: ({}) {}".format(
                UTIL_MODULE, type(err).__name__, err
            ))

        else:
            self.assertEqual(proc.returncode, RETCODE_SUCCESS)


# -----------------------------------------------------------------------------
class TestMainHelpOnly(unittest.TestCase):

    def test_util_help(self):

        # mod = module_import(UTIL_MODULE)
        # version = mod.Version()

        # Currently forge doesn't offer a proper cli help arg. It will produce
        # similar output when an invalid command line is attempted, so we need
        # to refactor that behavior to also occur with a specific help param.
        # It does have a 'display manual' param, so we'll use that for now (but
        # that is actually a distinctly different output).

        try:
            proc = subprocess.run(
                UTIL_CMD[:] + ['-m'],
                capture_output=True,
                check=True,
            )
        except BaseException as err:
            self.fail("{} failed version invocation: ({}) {}".format(
                UTIL_MODULE, type(err).__name__, err
            ))

        else:
            self.assertEqual(proc.returncode, RETCODE_SUCCESS)

        # version_emitted = proc.stdout.decode(ENCODING).strip()
        # self.assertEqual(version, version_emitted)


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
