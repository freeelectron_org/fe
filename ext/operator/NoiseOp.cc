/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void NoiseOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* TODO noise seed

    catalog<String>("attribute")="value";
    catalog<String>("attribute","label")="Attribute";
    catalog<String>("attribute","hint")=
            "Point attribute to populate with noise samples.";

	catalog<Real>("frequency")=100.0;
	catalog<Real>("frequency","high")=1e4;
	catalog<Real>("frequency","max")=1e9;
    catalog<String>("frequency","label")="Frequency";
    catalog<String>("frequency","hint")=
            "Approximate count of high and low values per unit space.";

	catalog<Real>("toMin")=0.0;
	catalog<Real>("toMin","min")=-1e9;
	catalog<Real>("toMin","low")=0.0;
	catalog<Real>("toMin","high")=100.0;
	catalog<Real>("toMin","max")=1e9;
	catalog<String>("toMin","label")="Map To Min";
	catalog<bool>("toMin","joined")=true;

	catalog<Real>("toMax")=1.0;
	catalog<Real>("toMax","min")=-1e9;
	catalog<Real>("toMax","low")=0.0;
	catalog<Real>("toMax","high")=100.0;
	catalog<Real>("toMax","max")=1e9;
	catalog<String>("toMax","label")="Max";

	catalog< sp<Component> >("Input Surfaces");
}

void NoiseOp::handle(Record& a_rSignal)
{
    const String attribute=catalog<String>("attribute");
	const Real frequency=catalog<Real>("frequency");
	const Real toMin=catalog<Real>("toMin");
	const Real toMax=catalog<Real>("toMax");

	const Real toRange=toMax-toMin;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPosition;
	if(!access(spOutputPosition,spOutputAccessible,
			e_point,e_position)) return;

    sp<SurfaceAccessorI> spOutputValue;
    if(!access(spOutputValue,spOutputAccessible,
			e_point,attribute)) return;

	const I32 pointCount=spOutputPosition->count();
	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point=spOutputPosition->spatialVector(pointIndex);

		//* roughly -1 to 1
        const Real perlinValue=m_noise.perlin3d(frequency*point);

		//* NOTE not bounded 0 to 1
        const Real unitValue=0.5*(perlinValue+1.0);

		const Real outputValue=toMin+toRange*unitValue;

		spOutputValue->set(pointIndex,outputValue);
	}

	String summary;
	summary.sPrintf("%d point%s\n",pointCount,pointCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
