/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_SurfaceAccessorOpenCL_h__
#define __opencl_SurfaceAccessorOpenCL_h__

namespace fe
{
namespace ext
{

//* TODO coordinate counts between accessors

/**************************************************************************//**
    @brief Accessor backed with a OpenCL

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorOpenCL:
	virtual public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorOpenCL>
{
	public:
						SurfaceAccessorOpenCL(void);
virtual					~SurfaceAccessorOpenCL(void);

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string);
virtual	String			string(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer);
virtual	I32				integer(U32 a_index,U32 a_subIndex=0);

virtual	I32				append(I32 a_integer);
virtual	I32				append(void);
virtual	I32				append(SurfaceAccessibleI::Form a_form);
virtual	void			append(U32 a_index,I32 a_integer);
virtual	void			append(Array< Array<I32> >& a_rPrimVerts);

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real);
virtual	Real			real(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_spatialVector);
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0);

		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
									//* TODO
									name="_generic_";
									break;
								case SurfaceAccessibleI::e_position:
									name="P";
									break;
								case SurfaceAccessibleI::e_normal:
									name="N";
									break;
								case SurfaceAccessibleI::e_uv:
									name="uv";
									break;
								case SurfaceAccessibleI::e_color:
									name="Cd";
									break;
								case SurfaceAccessibleI::e_vertices:
									name="vertStart";
									break;
//									m_attrName="vertices";
//									FEASSERT(a_element==
//											SurfaceAccessibleI::e_primitive);
//									m_element=a_element;
//									return TRUE;
								case SurfaceAccessibleI::e_properties:
									name="properties";
									break;
//									m_attrName="properties";
//									FEASSERT(a_element==
//											SurfaceAccessibleI::e_primitive);
//									m_element=a_element;
//									return TRUE;
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							String name=a_name;
							m_attribute=SurfaceAccessibleI::e_generic;

							if(name=="P")
							{
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(name=="N")
							{
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(name=="uv")
							{
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(name=="Cd")
							{
								m_attribute=SurfaceAccessibleI::e_color;
							}

							return bindInternal(a_element,name);
						}

	private:

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		void			setAttrType(String a_attrType);

		void			ensureBufferSize(
								SurfaceAccessibleOpenCL::Bridge* a_pBridge,
								I32 a_minSize,I32 a_sizeof);

		I32				pointIndexForVertex(U32 a_index,U32 a_subIndex);

		String								m_bridgeName;

const	SurfaceAccessibleOpenCL::Bridge*	m_pBridgeConst;
const	SurfaceAccessibleOpenCL::Bridge*	m_pPositionBridgeConst;
const	SurfaceAccessibleOpenCL::Bridge*	m_pVertStartBridgeConst;
const	SurfaceAccessibleOpenCL::Bridge*	m_pVertCountBridgeConst;
const	SurfaceAccessibleOpenCL::Bridge*	m_pVertPointBridgeConst;

		SurfaceAccessibleOpenCL::Bridge*	m_pBridge;
		SurfaceAccessibleOpenCL::Bridge*	m_pPositionBridge;
		SurfaceAccessibleOpenCL::Bridge*	m_pVertStartBridge;
		SurfaceAccessibleOpenCL::Bridge*	m_pVertCountBridge;
		SurfaceAccessibleOpenCL::Bridge*	m_pVertPointBridge;

		sp<SurfaceAccessibleBase::MultiGroup>	m_spMultiGroup;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_SurfaceAccessorOpenCL_h__ */
