/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "spatial/spatial.h"
#include "platform/dlCore.cc"

#include "GotoOrigin.h"
#include "PotentialNav.h"
#include "GenerateNav.h"
#include "DistCost.h"
#include "aiAS.h"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
{
}

FE_DL_EXPORT fe::Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<GotoOrigin>("HandlerI.GotoOrigin.ai.fe");
	pLibrary->add<PotentialNav>("HandlerI.PotentialNav.ai.fe");
	pLibrary->add<GenerateNav>("HandlerI.GenerateNav.ai.fe");
	pLibrary->add<DistCost>("StepCostI.Dist.ai.fe");

	pLibrary->add<AsNav>("PopulateI.AsNav.ai.fe");
	pLibrary->add<AsNavDebug>("PopulateI.AsNavDebug.ai.fe");


	return pLibrary;
}

}
