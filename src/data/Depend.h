/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Depend_h__
#define __data_Depend_h__

namespace fe
{

/**	@brief Attribute dependency information

	@ingroup data

	Encapsulates Layout configuration (Attribute dependency) information.
	This is mainly for internal use, but may have applicability for
	advanced configuration setup.

	For typical configuration setup see:
	- Scope::support()
	- Scope::enforce()
	- Scope::populate()
	- Scope::clear()
	- Scope::share()
	*/
class FE_DL_EXPORT Depend : public Counted
{
	public:
		/** Dependency types */
		enum
		{
			e_null =		(FE_UWORD)(0),
			e_available =	(FE_UWORD)(1<<0),
			e_attribute =	(FE_UWORD)(1<<1),
			e_populate =	(FE_UWORD)(1<<2),
			e_share =		(FE_UWORD)(1<<3),
			e_within =		(FE_UWORD)(1<<4)
		};

		Depend(void):
				m_attributeName(""),
				m_attributeType(""),
				m_depName(""),
				m_shareName(""),
				m_depFlag(0)
		{	suppressReport(); }

		Depend(const String& attributeName):
				m_attributeName(attributeName),
				m_attributeType(""),
				m_depName(""),
				m_shareName(""),
				m_depFlag(0)
		{	suppressReport(); }

		Depend(const Depend &other):
				Castable(),
				Counted()
		{
			m_attributeName = other.m_attributeName;
			m_attributeType = other.m_attributeType;
			m_depName = other.m_depName;
			m_depFlag = other.m_depFlag;
			m_shareName = other.m_shareName;
			m_matchAttributes = other.m_matchAttributes;
			suppressReport();
		}

		~Depend(void)
		{
		}

		Depend &operator=(const Depend &other)
		{
			if(this != &other)
			{
				m_attributeName = other.m_attributeName;
				m_attributeType = other.m_attributeType;
				m_depName = other.m_depName;
				m_depFlag = other.m_depFlag;
				m_shareName = other.m_shareName;
				m_matchAttributes = other.m_matchAttributes;
			}
			return *this;
		}

		bool operator==(const Depend &other) const
		{
			if(m_attributeName != other.m_attributeName)	{ return false; }
			if(m_attributeType != other.m_attributeType)	{ return false; }
			if(m_depName != other.m_depName)		{ return false; }
			if(m_shareName != other.m_shareName)	{ return false; }
			if(m_depFlag != other.m_depFlag)		{ return false; }
			if(!(m_matchAttributes == other.m_matchAttributes))
			{
				if(m_matchAttributes.size() != other.m_matchAttributes.size())
				{
					return false;
				}
				std::set<String> a, b;
				for(unsigned int i = 0; i < m_matchAttributes.size(); i++)
				{
					a.insert(m_matchAttributes[i]);
				}
				for(unsigned int i = 0; i < other.m_matchAttributes.size(); i++)
				{
					b.insert(other.m_matchAttributes[i]);
				}
				if(!(a == b))
				{
					return false;
				}
			}
			return true;
		}

		void peek(Peeker &peeker);

		String	&attributeName(void)			{ return m_attributeName; }
		String	&attributeType(void)			{ return m_attributeType; }
		String	&dependName(void)				{ return m_depName; }
		String	&shareName(void)				{ return m_shareName; }
		size_t	&offset(void)					{ return m_offset; }
		FE_UWORD	&dependFlag(void)				{ return m_depFlag; }
		Array<String>
				&matchAttributes(void)			{ return m_matchAttributes; }

const	String	&name(void) const					{ return m_attributeName; }
const	String	verboseName(void) const
				{	return "Depend " + m_attributeName + " on " + m_depName; }

	private:
		String					m_attributeName;
		String					m_attributeType;
		String					m_depName;
		String					m_shareName;
		Array<String>			m_matchAttributes;
		size_t					m_offset;
		FE_UWORD				m_depFlag;
};

inline void Depend::peek(Peeker &peeker)
{
	peeker.str().catf("attribute:%s type:%s dep:%s share:%s flags:",
		m_attributeName.c_str(),
		m_attributeType.c_str(),
		m_depName.c_str(),
		m_shareName.c_str());
	if(m_depFlag & e_available)
	{
		peeker.cat(" available");
	}
	if(m_depFlag & e_attribute)
	{
		peeker.cat(" attribute");
	}
	if(m_depFlag & e_populate)
	{
		peeker.cat(" populate");
	}
	if(m_depFlag & e_share)
	{
		peeker.cat(" share");
	}
	if(m_depFlag & e_within)
	{
		peeker.cat(" within");
	}
	peeker.cat(" |");
	for(unsigned int i = 0; i < m_matchAttributes.size(); i++)
	{
		peeker.cat(" ");
		peeker.cat(m_matchAttributes[i]);
	}
}

} /* namespace */

#endif /* __data_Depend_h__ */

