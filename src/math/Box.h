/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Box_h__
#define __math_Box_h__

namespace fe
{

/**************************************************************************//**
	@brief N-dimensional axis-aligned bounding-box

	@ingroup math
*//***************************************************************************/
template<int N,class T> class Box: public Vector<N,T>
{
	public:
						Box(void)											{}

						Box(const Vector<N,T>& location):
								Vector<N,T>(location)						{}

						Box(const Vector<N,T>& location,
								const Vector<N,T>& size):
								Vector<N,T>(location), m_size(size)			{}

						Box(const Box<N,T>& rhs):
							Vector<N,T>(rhs)
						{	m_size=rhs.m_size; }

		Box<N,T>&		operator=(const Box<N,T>& rhs)
						{	Vector<N,T>::operator=(rhs);
							m_size=rhs.m_size;
							return *this; }

		Vector<N,T>&	size(void)			{ return m_size; }
const	Vector<N,T>&	size(void) const	{ return m_size; }

	private:
		Vector<N,T>		m_size;
};

/** Test equality
	@relates Box
	*/
template<int N,class T,class U>
inline bool operator==(const Box<N,T> &lhs, const Box<N,U> &rhs)
{
	return Vector<N,T>(lhs).operator==(rhs) && lhs.size()==rhs.size();
}

/** Test inequality
	@relates Box
	*/
template<int N,class T,class U>
inline bool operator!=(const Box<N,T> &lhs, const Box<N,U> &rhs)
{
	return Vector<N,T>(lhs).operator!=(rhs) || lhs.size()!=rhs.size();
}

/** Change the size
	@relates Box
	*/
template<int N,class T,class U>
inline Box<N,T>& resize(Box<N,T>& lhs,const Vector<N,U> &size)
{
	lhs.size()=size;
	return lhs;
}

/** Redefine the spatial domain
	@relates Box
	*/
template<int N,class T,class U,class V>
inline Box<N,T>& set(Box<N,T>& lhs,const Vector<N,U> &location,
		const Vector<N,V> &size)
{
	lhs=location;
	lhs.size()=size;
	return lhs;
}

/** Set the box to encapsulate the given sphere
	@relates Box
	*/
template<class T,class U,class V>
inline Box<3,T>& set(Box<3,T>& lhs,const Vector<3,U> &center,V radius)
{
	const Vector<3,U> corner(radius,radius,radius);
	const Vector<3,U> diff=center-corner;
	const Vector<3,U> corner2=2.0f*corner;
	lhs=diff;
	resize(lhs,corner2);
	return lhs;
}

/** Get the box width
	@relates Box
	*/
template<int N,class T>
inline T width(const Box<N,T>& lhs)
{
	FEASSERT(N>0);
	return lhs.size()[0];
}

/** Get the box height
	@relates Box
	*/
template<int N,class T>
inline T height(const Box<N,T>& lhs)
{
	FEASSERT(N>1);
	return lhs.size()[1];
}

/** Get the box depth
	@relates Box
	*/
template<int N,class T>
inline T depth(const Box<N,T>& lhs)
{
	FEASSERT(N>2);
	return lhs.size()[2];
}

/** Set by explicit elements (2D)
	@relates Box
	*/
template<class T,class U,class V,class W,class X>
inline Box<2,T>& set(Box<2,T>& lhs,U x,V y,W w,X h)
{
	set(lhs,x,y);
	set(lhs.size(),w,h);
	return lhs;
}

/** Set by explicit elements (3D)
	@relates Box
	*/
template<class T,class U,class V,class W,class X,class Y,class Z>
inline Box<3,T>& set(Box<3,T>& lhs,U x,V y,W z,X w,Y h,Z d)
{
	set(lhs,x,y,z);
	set(lhs.size(),w,h,d);
	return lhs;
}

/** Resize by explicit elements (2D)
	@relates Box
	*/
template<class T,class U,class V>
inline Box<2,T>& resize(Box<2,T>& lhs,U w,V h)
{
	set(lhs.size(),w,h);
	return lhs;
}

/** Resize by explicit elements (3D)
	@relates Box
	*/
template<class T,class U,class V,class W>
inline Box<3,T>& resize(Box<3,T>& lhs,U w,V h,W d)
{
	set(lhs.size(),w,h,d);
	return lhs;
}

/** Returns true if the @em box contains the @em point
	@relates Box
	*/
template<class T,class U>
inline bool intersecting(const Box<2,T>& box,const Vector<2,U>& point)
{
	return	point[0]>box[0] && point[0]<box[0]+box.size()[0] &&
			point[1]>box[1] && point[1]<box[1]+box.size()[1];
}

/** Returns true if the boxes overlap
	@relates Box
	*/
template<class T,class U>
inline bool intersecting(const Box<2,T>& box1,const Box<2,U>& box2)
{
	return	box1[0]+box1.size()[0]>box2[0] && box1[0]<box2[0]+box2.size()[0] &&
			box1[1]+box1.size()[1]>box2[1] && box1[1]<box2[1]+box2.size()[1];
}

/** Returns true if the @em box contains the @em point
	@relates Box
	*/
template<class T,class U>
inline bool intersecting(const Box<3,T>& box,const Vector<3,U>& point)
{
	return	point[0]>box[0] && point[0]<box[0]+box.size()[0] &&
			point[1]>box[1] && point[1]<box[1]+box.size()[1] &&
			point[2]>box[2] && point[2]<box[2]+box.size()[2];
}

/** Returns true if the boxes overlap
	@relates Box
	*/
template<class T,class U>
inline bool intersecting(const Box<3,T>& box1,const Box<3,U>& box2)
{
	return	box1[0]+box1.size()[0]>box2[0] && box1[0]<box2[0]+box2.size()[0] &&
			box1[1]+box1.size()[1]>box2[1] && box1[1]<box2[1]+box2.size()[1] &&
			box1[2]+box1.size()[2]>box2[2] && box1[2]<box2[2]+box2.size()[2];
}

typedef Box<2,I32>	Box2i;
typedef Box<2,F32>	Box2f;
typedef Box<2,F64>	Box2d;
typedef Box<2,Real>	Box2;

typedef Box<3,F32>	Box3f;
typedef Box<3,F64>	Box3d;
typedef Box<3,Real>	Box3;
typedef Box<3,Real>	SpatialBox;

/** Returns Box enveloping a tapered cylinder
	@relates Box
	*/
FE_DL_EXPORT const Box3 aabb(const Vector3f& location,const Vector3f& span,
		F32 baseRadius,F32 endRadius);

/** Returns Box enveloping a sphere
	@relates Box
	*/
FE_DL_EXPORT const Box3 aabb(const Vector3f& location,F32 radius);

/** Print to a string
	@relates Box
	*/
template<int N, class T>
inline String print(const Box<N,T> &box)
{
	return print((Vector<N,T>&)box) + " " + print(box.size());
}

} /* namespace */

#endif /* __math_Box_h__ */
