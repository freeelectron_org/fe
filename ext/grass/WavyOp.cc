/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

#define FE_WAVY_DEBUG			FALSE

namespace fe
{
namespace ext
{

void WavyOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("banking")=true;
	catalog<bool>("banking","joined")=true;
	catalog<String>("banking","label")="Banking";
	catalog<String>("banking","hint")=
			"Add a banking wave.";

	catalog<Real>("bankScale")=1.0;
	catalog<Real>("bankScale","high")=1.0;
	catalog<Real>("bankScale","max")=1e3;
	catalog<String>("bankScale","enabler")="banking";
	catalog<String>("bankScale","label")="Scale";
	catalog<String>("bankScale","hint")=
			"Linearly scale the overall banking wave.";

	catalog<Real>("bankFrequency")=9.0;
	catalog<Real>("bankFrequency","high")=100.0;
	catalog<Real>("bankFrequency","max")=1e3;
	catalog<String>("bankFrequency","enabler")="banking";
	catalog<String>("bankFrequency","label")="Bank Frequency";
	catalog<String>("bankFrequency","hint")=
			"Density of oscillations in bank.";

	catalog<bool>("pitching")=true;
	catalog<bool>("pitching","joined")=true;
	catalog<String>("pitching","label")="Pitching";
	catalog<String>("pitching","hint")=
			"Add a pitching wave.";

	catalog<Real>("pitchScale")=1.0;
	catalog<Real>("pitchScale","high")=1.0;
	catalog<Real>("pitchScale","max")=1e3;
	catalog<String>("pitchScale","enabler")="pitching";
	catalog<String>("pitchScale","label")="Scale";
	catalog<String>("pitchScale","hint")=
			"Linearly scale the overall pitching wave.";

	catalog<Real>("pitchFrequency")=24.0;
	catalog<Real>("pitchFrequency","high")=100.0;
	catalog<Real>("pitchFrequency","max")=1e3;
	catalog<String>("pitchFrequency","enabler")="pitching";
	catalog<String>("pitchFrequency","label")="Pitch Frequency";
	catalog<String>("pitchFrequency","hint")=
			"Density of oscillations in pitch.";

	catalog<bool>("targeting")=true;
	catalog<bool>("targeting","joined")=true;
	catalog<String>("targeting","label")="Targeting";
	catalog<String>("targeting","hint")=
			"Veer towards a point.";

	catalog<Real>("targetScale")=1.0;
	catalog<Real>("targetScale","high")=1.0;
	catalog<Real>("targetScale","max")=1e3;
	catalog<String>("targetScale","enabler")="targeting";
	catalog<String>("targetScale","label")="Scale";
	catalog<String>("targetScale","hint")=
			"Linearly scale the overall targeting effect.";

	//* TODO per point/primitive attribute
	catalog<SpatialVector>("target")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("target","enabler")="targeting";
	catalog<String>("target","label")="Target";
	catalog<String>("target","hint")=
			"Veer towards this point.";

	catalog<bool>("avoiding")=true;
	catalog<bool>("avoiding","joined")=true;
	catalog<String>("avoiding","label")="Avoiding";
	catalog<String>("avoiding","hint")=
			"Veer away from points from start to here.";

	catalog<Real>("avoidScale")=1.0;
	catalog<Real>("avoidScale","high")=1.0;
	catalog<Real>("avoidScale","max")=1e3;
	catalog<String>("avoidScale","enabler")="avoiding";
	catalog<bool>("avoidScale","joined")=true;
	catalog<String>("avoidScale","label")="Scale";
	catalog<String>("avoidScale","hint")=
			"Linearly scale the overall avoidance effect.";

	catalog<I32>("avoidMargin")=8;
	catalog<I32>("avoidMargin","high")=10;
	catalog<I32>("avoidMargin","max")=1e3;
	catalog<String>("avoidMargin","enabler")="avoiding";
	catalog<String>("avoidMargin","label")="Margin";
	catalog<String>("avoidMargin","hint")=
			"Ignore this number of vertices"
			" preceding the current being checked.";

	catalog<Real>("avoidRadius")=0.01;
	catalog<Real>("avoidRadius","high")=1.0;
	catalog<Real>("avoidRadius","max")=1e3;
	catalog<String>("avoidRadius","enabler")="avoiding";
	catalog<bool>("avoidRadius","joined")=true;
	catalog<String>("avoidRadius","label")="Avoidance Radius";
	catalog<String>("avoidRadius","hint")=
			"Radius around curve to avoid.";

	catalog<Real>("avoidFalloff")=0.3;
	catalog<Real>("avoidFalloff","high")=1.0;
	catalog<Real>("avoidFalloff","max")=1e3;
	catalog<String>("avoidFalloff","enabler")="avoiding";
	catalog<String>("avoidFalloff","label")="Falloff";
	catalog<String>("avoidFalloff","hint")=
			"Fade avoid effect over this distance past radius.";

	catalog<String>("facingAttr")="facing";
	catalog<String>("facingAttr","label")="Facing Attribute";
	catalog<String>("facingAttr","hint")=
			"Vector attribute on input curve points"
			" describing a direction roughly perpendicular to the curve.";

	catalog<bool>("debugAvoid")=false;
	catalog<String>("debugAvoid","label")="Debug Avoidance";
	catalog<String>("debugAvoid","hint")=
			"Draws lines over the output showing the avoidance.";

	catalog< sp<Component> >("Input Curves");
}

void WavyOp::handle(Record& a_rSignal)
{
	const bool banking=catalog<bool>("banking");
	const bool pitching=catalog<bool>("pitching");
	const bool targeting=catalog<bool>("targeting");

	const Real bankScale=catalog<Real>("bankScale");
	const Real pitchScale=catalog<Real>("pitchScale");
	const Real targetScale=catalog<Real>("targetScale");

	const Real bankFrequency=catalog<Real>("bankFrequency");
	const Real pitchFrequency=catalog<Real>("pitchFrequency");

	const bool avoiding=catalog<bool>("avoiding");
	const Real avoidScale=catalog<Real>("avoidScale");
	const I32 avoidMargin=catalog<I32>("avoidMargin");
	const Real avoidRadius=catalog<Real>("avoidRadius");
	const Real avoidFalloff=catalog<Real>("avoidFalloff");

	const SpatialVector target=catalog<SpatialVector>("target");

	const String facingAttr=catalog<String>("facingAttr");

	sp<DrawI> spDrawDebug;
	if(catalog<bool>("debugAvoid"))
	{
		accessGuide(spDrawDebug,a_rSignal);
	}

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Curves")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spPointFacing;
	access(spPointFacing,spOutputAccessible,
			e_point,facingAttr,e_warning,e_refuseMissing);

	Array<SpatialVector> curve;

	I32 curveCount=0;

	const I32 primitiveCount=spInputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const I32 subCount=spInputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		SpatialVector priorPoint =
				spInputVertices->spatialVector(primitiveIndex,0);
		const SpatialVector secondPoint =
				spInputVertices->spatialVector(primitiveIndex,1);

		const I32 rootIndex=spInputVertices->integer(primitiveIndex,0);
		const SpatialVector rootFacing = spPointFacing.isValid()?
				spPointFacing->spatialVector(rootIndex):
				SpatialVector(1.0,0.0,0.0);
		const SpatialVector rootTangent=unitSafe(secondPoint-priorPoint);

		SpatialTransform xformInput;
		makeFrameNormalY(xformInput,priorPoint,rootFacing,rootTangent);

		SpatialTransform xformOutput=xformInput;

		curve.resize(subCount);
		curve[0]=priorPoint;

#if FE_WAVY_DEBUG
		feLog("\nRoot:\n%s\n",c_print(xformOutput));
#endif

		for(I32 subIndex=1;subIndex<subCount;subIndex++)
		{
			const SpatialVector world =
					spInputVertices->spatialVector(primitiveIndex,subIndex);
			const I32 pointIndex =
					spInputVertices->integer(primitiveIndex,subIndex);
			const SpatialVector inputFacing = spPointFacing.isValid()?
					spPointFacing->spatialVector(pointIndex):
					SpatialVector(1.0,0.0,0.0);

			const SpatialVector tangent=unitSafe(world-priorPoint);

			SpatialTransform xformSelf;
			makeFrameNormalY(xformSelf,world,inputFacing,tangent);

			SpatialTransform invInput;
			invert(invInput,xformInput);

			SpatialTransform xformDelta=invInput*xformSelf;

			const Real bank=
					sin(bankFrequency*fe::pi*subIndex/Real(subCount-1));
			const Real pitch=
					sin(pitchFrequency*fe::pi*subIndex/Real(subCount-1));

			const Real rotY=banking? Real(bankScale*bank): Real(0);
			const Real rotZ=pitching? Real(pitchScale*pitch): Real(0);

			SpatialTransform xformExtra;
			setIdentity(xformExtra);
			rotate(xformExtra,rotY,e_yAxis);
			rotate(xformExtra,rotZ,e_zAxis);

			const SpatialTransform xformPrior=xformOutput;
			xformOutput=xformExtra*xformDelta*xformOutput;

			SpatialTransform invOutput;
			invert(invOutput,xformOutput);

			const SpatialVector yAxis(0.0,1.0,0.0);

			SpatialTransform xformTarget;
			setIdentity(xformTarget);

			if(targeting)
			{
				SpatialVector offset;
				transformVector(invOutput,target,offset);

				offset[2]=0.0;

				const Real deviation=magnitude(offset);
				const SpatialVector towards=deviation>1e-6?
						offset/deviation: SpatialVector(0.0,0.0,0.0);

				SpatialQuaternion rotation;
				set(rotation,yAxis,towards);
				rotation=angularlyScaled(rotation,targetScale*deviation);

				xformTarget=rotation;
				xformOutput=xformTarget*xformOutput;
			}

			if(avoiding && subIndex>avoidMargin)
			{
				const SpatialVector origin(xformOutput.translation());
				SpatialVector direction(0,0,0);
				SpatialVector intersection(0,0,0);
				Real along(0);
				const Real distance=PointCurveNearest<Real>::solve(
						curve.data(),subIndex-avoidMargin,avoidRadius,origin,
						direction,along,intersection);
				if(distance>=Real(0) && distance<avoidFalloff)
				{
					const Real avoidance=avoidScale*
							pow(Real(1)-(distance/avoidFalloff),0.1);

					SpatialTransform invPrior;
					invert(invPrior,xformPrior);

					SpatialVector offset;
					transformVector(invPrior,intersection,offset);

					offset[2]=0.0;

					const SpatialVector towards=unitSafe(offset);

					SpatialQuaternion rotation;
					set(rotation,yAxis,towards);

					Real radians;
					SpatialVector axis;
					rotation.computeAngleAxis(radians,axis);

					const SpatialVector oldY=xformOutput.column(1);

					const Real minAngle=atan(avoidFalloff/distance);

					const BWORD adjust=(radians<minAngle);
					if(adjust)
					{
						const Real radiansMod=
								-avoidance*(minAngle-radians);
						set(rotation,radiansMod,axis);

//						const SpatialTransform xformAvoid(rotation);

						SpatialTransform xformAvoid;
						setIdentity(xformAvoid);
						rotate(xformAvoid,radiansMod*axis[2],e_zAxis);

						xformOutput=xformTarget*xformExtra*xformDelta*
								xformAvoid*xformPrior;

#if FALSE
						feLog("\n%d/%d\norigin %s intersection %s\n",
								subIndex,subCount,
								c_print(origin),c_print(intersection));
						feLog("dist %.6G avoid %.6G minAngle %.6G  (%.6G)\n",
								distance,avoidance,
								minAngle,minAngle/fe::degToRad);
						feLog("radians %.6G (%.6G) -> %.6G (%.6G)"
								" axis %s\n",
								radians,radians/fe::degToRad,
								radiansMod,radiansMod/fe::degToRad,
								c_print(axis));
#endif
					}

					if(spDrawDebug.isValid())
					{
						const Color black(0.0,0.0,0.0,1.0);
						const Color red(1.0,0.0,0.0,1.0);
						const Color green(0.0,1.0,0.0,1.0);
						const Color cyan(0.0,1.0,1.0,1.0);

						Color colors[2];
						colors[0]=black;
						colors[1]=adjust? red: green;

						SpatialVector line[2];
						line[0]=origin;
						line[1]=intersection;

						spDrawDebug->drawLines(line,NULL,2,
								DrawI::e_strip,true,colors);

						if(adjust)
						{
							colors[1]=cyan;

							line[1]=origin+0.01*oldY;

							spDrawDebug->drawLines(line,NULL,2,
									DrawI::e_strip,true,colors);
						}
					}
				}
			}

			xformInput=xformSelf;
			priorPoint=world;
			curve[subIndex]=xformOutput.translation();

#if FE_WAVY_DEBUG
			feLog("\n%d/%d %d/%d\nDelta:\n%s\nExtra:\n%s\nOutput:\n%s\n",
					primitiveIndex,primitiveCount,
					subIndex,subCount,
					c_print(xformDelta),
					c_print(xformExtra),
					c_print(xformOutput));
#endif

			spOutputVertices->set(primitiveIndex,subIndex,
					xformOutput.translation());
			spPointFacing->set(pointIndex,xformOutput.column(0));
		}

		curveCount++;
	}

	String summary;
	summary.sPrintf("%d curve%s\n",curveCount,curveCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
