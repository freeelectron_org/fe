/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

Mask::Mask(void)
{
	m_enabled = true;
}

void Mask::maskDefault(const String &a_key, const WindowEvent::Mask &value)
{
	String key;
	cookKey(key, a_key);
	sp<MaskMapI> spMaskMap =
		registry()->master()->catalog()->catalogComponent("MaskMapI", "ViewerMaskMap");
	WindowEvent::Mask mask;
	if(!spMaskMap->get(mask, key))
	{
		spMaskMap->set(value, key);
	}
}

void Mask::maskSet(const String &a_key, const WindowEvent::Mask &value)
{
	String key;
	cookKey(key, a_key);
	sp<MaskMapI> spMaskMap =
		registry()->master()->catalog()->catalogComponent("MaskMapI", "ViewerMaskMap");
	spMaskMap->set(value, key);
}

WindowEvent::Mask Mask::maskGet(const String &a_key)
{
	String key;
	cookKey(key, a_key);
	sp<MaskMapI> spMaskMap =
		registry()->master()->catalog()->catalogComponent("MaskMapI", "ViewerMaskMap");
	WindowEvent::Mask mask;
	spMaskMap->get(mask, key);
	return mask;
}

Record Mask::maskCreate(const sp<Layout> l_mask, const String &a_key)
{
	String key;
	cookKey(key, a_key);
	sp<MaskMapI> spMaskMap =
		registry()->master()->catalog()->catalogComponent("MaskMapI", "ViewerMaskMap");
	WindowEvent::Mask mask;
	Record r_mask;
	if(spMaskMap->get(mask, key))
	{
		r_mask = l_mask->scope()->createRecord(l_mask);
		if(!r_mask.isValid())
		{
			feX("Mask::maskCreate",
				"failed to create record");
		}
		WindowEvent wev;
		wev.bind(r_mask);
		wev.setSIS(mask.source(), mask.item(), mask.state());
	}
	else
	{
		feX("Mask::maskCreate",
				"failed to get mask %s from mask map", key.c_str());
	}
	return r_mask;
}



void Mask::maskInit(const String &a_prefix)
{
	m_prefix = a_prefix;
	String enable_str;
	String disable_str;
	cookKey(enable_str, "_enable");
	cookKey(disable_str, "_disable");

	sp<MaskMapI> spMaskMap =
		registry()->master()->catalog()->catalogComponent("MaskMapI", "ViewerMaskMap");
	WindowEvent::Mask mask;
	if(spMaskMap->get(mask, enable_str))
	{
		return;
	}
	if(spMaskMap->get(mask, disable_str))
	{
		return;
	}

	int &i = registry()->master()->catalog()->catalog<int>("NextMaskMapUserItem", 1000);

	WindowEvent::Item item = (WindowEvent::Item)i;
	i++;

	maskSet(enable_str, WindowEvent::Mask(
		WindowEvent::e_sourceUser, item, WindowEvent::e_statePress));
	maskSet(disable_str, WindowEvent::Mask(
		WindowEvent::e_sourceUser, item, WindowEvent::e_stateRelease));
}

bool Mask::maskEnabled(const WindowEvent &a_wev)
{
	String enable_str;
	String disable_str;
	cookKey(enable_str, "_enable");
	cookKey(disable_str, "_disable");
	WindowEvent::Mask enable = maskGet(enable_str);
	WindowEvent::Mask disable = maskGet(disable_str);

	if(m_enabled)
	{
		if(a_wev.is(disable))
		{
			m_enabled = false;
		}
	}
	else
	{
		if(a_wev.is(enable))
		{
			m_enabled = true;
		}
	}

	return m_enabled;
}

void Mask::maskEnable(bool a_setting)
{
	m_enabled = a_setting;
}

void Mask::cookKey(String &a_cooked, const String &a_raw)
{
	if(a_raw.length() > 0 && a_raw.c_str()[0] == '_')
	{
		a_cooked.sPrintf("%s%s", m_prefix.c_str(), a_raw.c_str());
	}
	else
	{
		a_cooked = a_raw;
	}
}

} /* namespace ext */
} /* namespace fe */
