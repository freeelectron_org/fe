/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

FE_DL_EXPORT Library* CreateDataLibrary(sp<Master> spMaster)
{
	assertData(spMaster->typeMaster());

	if(!spMaster->catalog()->cataloged("path:media"))
	{
		fe::String mediaPath=fe::String("${FE_MEDIA}").substituteEnv();
		if(mediaPath.empty())
		{
			fe::String rootPath=fe::String("${FE_ROOT}").substituteEnv();

			//* HACK
			rootPath="";

			if(rootPath.empty())
			{
				rootPath=".";

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

				const String loadPath=System::getLoadPath(
						(void*)CreateDataLibrary,TRUE);

#pragma GCC diagnostic pop

				if(!loadPath.empty())
				{
					rootPath=loadPath;

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
					rootPath=rootPath.replace("\\\\[^\\\\]*$","");
					rootPath=rootPath.replace("\\\\lib\\\\.*$","");
					rootPath=rootPath.replace("\\\\lib$","");
#else
					rootPath=rootPath.replace("/[^/]*$","");
					rootPath=rootPath.replace("/lib/.*$","");
					rootPath=rootPath.replace("/lib$","");
#endif
				}
			}

			mediaPath=rootPath+"/../media";
		}
		spMaster->catalog()->catalog<fe::String>("path:media")=
				mediaPath.substituteEnv();

		const String verbose=System::getVerbose();
		if(verbose=="all")
		{
			feLog("setting 'path:media' to \"%s\"\n",mediaPath.c_str());
		}
	}

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->setName("default_data");
	pLibrary->add<fe::SegmentStore>("StoreI.SegmentStore.fe");
	pLibrary->add<fe::Scope>("Scope.Scope.fe");
	pLibrary->add<fe::data::AsciiFileStream>("FileStreamI.Ascii.fe");
	pLibrary->add<fe::data::BinaryFileStream>("FileStreamI.Binary.fe");
	pLibrary->add<fe::data::AsciiStream>("StreamI.AsciiStream.fe");
	pLibrary->add<fe::data::BinaryStream>("StreamI.BinaryStream.fe");

	pLibrary->addSingleton<fe::RecordCookbook>("Component.RecordCookbook.fe");
	return pLibrary;

}

}

