import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
                "fexThreadDLLib" ]

    tests = [   'xCountedPool',
                'xGang',
                'xMutexStressTest',
                'xThread',
                'xThreadDataStressTest' ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xCountedPool.exe",    "",                     None,   None),
        ("xGang.exe",           "13",                   None,   None),
        ("xMutexStressTest.exe", "",                    None,   None),
        ("xThread.exe",         "13",                   None,   None),
        ("xThreadDataStressTest.exe", "",               None,   None) ]
