/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_JCO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

//* static
void JointCombineOp::changeCB(MNodeMessage::AttributeMessage a_message,
	MPlug & a_plug,MPlug & a_otherPlug,void* a_pClientData)
{
	if(a_pClientData && a_message&MNodeMessage::kAttributeSet)
	{
		JointCombineOp* pOperator=(JointCombineOp*)a_pClientData;
		if(!pOperator->m_dirtied)
		{
			const BWORD aggressive=FALSE;
			pOperator->dirty(aggressive);
			pOperator->m_dirtied=TRUE;
		}
	}
}

//* static
void JointCombineOp::renameCB(MObject &a_node, const MString &a_string,
	void* a_pClientData)
{
	if(a_pClientData)
	{
		JointCombineOp* pOperator=(JointCombineOp*)a_pClientData;
		if(!pOperator->m_dirtied)
		{
			const BWORD aggressive=FALSE;
			pOperator->dirty(aggressive);
			pOperator->m_dirtied=TRUE;
		}
	}
}

JointCombineOp::~JointCombineOp(void)
{
	cancelCallbacks();
}

void JointCombineOp::cancelCallbacks(void)
{
	const U32 callbackCount=m_callbackIDs.size();
	for(U32 callbackIndex=0;callbackIndex<callbackCount;callbackIndex++)
	{
		MMessage::removeCallback(m_callbackIDs[callbackIndex]);
	}
	m_callbackIDs.clear();
}

void JointCombineOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("root")="joints";
	catalog<String>("root","label")="Root Node Name";
	catalog<String>("root","IO")="input output";

	//* not read; just for finding root
	catalog< sp<Component> >("Root Joint");

	catalog<bool>("Input Trigger");
	catalog<bool>("Input Trigger","hidden")=true;

	//* no copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";

	m_spScope=registry()->create("Scope");
}

void JointCombineOp::handle(Record& a_rSignal)
{
	m_dirtied=FALSE;

#if FE_JCO_DEBUG
	feLog("JointCombineOp::handle node \"%s\"\n",
				name().c_str());
#endif

	MStatus status;
	MObject rootObject;

	//* NOTE spRootAccessible->isBound() is probably FALSE
	sp<SurfaceAccessibleI> spRootAccessible=
			catalog< sp<Component> >("Root Joint");
	if(spRootAccessible.isValid())
	{
		sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=spRootAccessible;
		rootObject=spSurfaceAccessibleMaya->meshNode();

		MFnDagNode dagNode(rootObject);
		const String dagName=dagNode.fullPathName().asChar();

//		feLog("JointCombineOp::handle bound %d root \"%s\"\n",
//				spRootAccessible->isBound(),dagName.c_str());

		if(!dagName.empty())
		{
			catalog<String>("root")=dagName;
		}

		//* make sure trigger is connected
		const bool wantNetworkedPlug=true;
		MPlug plug=dagNode.findPlug("geometry",wantNetworkedPlug,&status);

		if(status)
		{
			MPlugArray connectionArray;
			plug.connectedTo(connectionArray,true,false,&status);

			const U32 connectionCount=connectionArray.length();

			for(U32 connectionIndex=0;connectionIndex<connectionCount;
					connectionIndex++)
			{
				MPlug connectionPlug=connectionArray[connectionIndex];
//				feLog("  %d/%d \"%s\"\n",
//						connectionIndex,connectionCount,
//						connectionPlug.name().asChar());

				MObject connectionObject=connectionPlug.node();
				MFnDependencyNode otherNode(connectionObject);
				const String otherName=otherNode.name().asChar();

				if(otherName.empty())
				{
					continue;
				}

				MPlug plugTriggerOut=otherNode.findPlug("Output_Trigger",
						wantNetworkedPlug,&status);
				if(!status)
				{
					continue;
				}

				String command;
				command.sPrintf("fe_connectAttr(\"%s.Output_Trigger\","
						"\"%s.Input_Trigger\")",
						otherName.c_str(),
						name().c_str());

//				feLog("  mel \"%s\"\n",command.c_str());

				const bool displayEnabled=false;
				MGlobal::executeCommandOnIdle(command.c_str(),displayEnabled);
				break;
			}
		}
	}

	const String rootName=catalog<String>("root");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	m_spJointAccessible=registry()->create("*.SurfaceAccessibleJoint");
	if(m_spJointAccessible.isNull())
	{
		return;
	}

	m_spJointAccessible->bind(m_spScope);
	m_spJointAccessible->startJoints();

	cancelCallbacks();

	MSelectionList originalList;
	MGlobal::getActiveSelectionList(originalList);

	if(rootObject.isNull())
	{
		//* TODO is there an easier way to get a node object by name?
		MGlobal::selectByName(rootName.c_str(),MGlobal::kReplaceList);

		MSelectionList selectionList;
		MGlobal::getActiveSelectionList(selectionList);

		if(selectionList.length())
		{
			selectionList.getDependNode(0,rootObject);
		}
		if(rootObject.isNull())
		{
			String message;
			message.sPrintf("root node not found: %s",
					status.errorString().asChar());
			catalog<String>("error")+=message;

			MGlobal::setActiveSelectionList(originalList);
			return;
		}
	}

	//* also assures UI refreshes
	MGlobal::setActiveSelectionList(originalList);

	m_animWorldMap.clear();
	setIdentity(m_animWorldMap[""]);

	m_refWorldMap.clear();
	setIdentity(m_refWorldMap[""]);

	scanDag(rootObject,"");

	spOutputAccessible->copy(m_spJointAccessible);

#if FE_JCO_DEBUG
	feLog("JointCombineOp::handle done\n");
#endif
}

void JointCombineOp::scanDag(MObject a_parent,String a_grandparent)
{
	const Color yellow(1.0,1.0,0.0);

	if(a_parent.isNull())
	{
		return;
	}

	MStatus status;

	//* watch for any change to each input joint
	void* pClientData=this;
	MCallbackId callbackID=MNodeMessage::addAttributeChangedCallback(
			a_parent,changeCB,pClientData,&status);
	m_callbackIDs.push_back(callbackID);
	callbackID=MNodeMessage::addNameChangedCallback(
			a_parent,renameCB,pClientData,&status);
	m_callbackIDs.push_back(callbackID);

	MFnDagNode dagParent(a_parent);
	const String jointNode=dagParent.name().asChar();

	const bool wantNetworkedPlug=true;
	MPlug plug=dagParent.findPlug("joint",wantNetworkedPlug,&status);
	const String plugString=plug.asString().asChar();
	const String jointName=plugString.empty()? jointNode: plugString;

	SpatialVector scaleLocal(1.0,1.0,1.0);
	plug=dagParent.findPlug("localScale",wantNetworkedPlug,&status);
	if(status)
	{
		set(scaleLocal,
				plug.child(0).asDouble(),
				plug.child(1).asDouble(),
				plug.child(2).asDouble());
	}

//	feLog("JointCombineOp::scanDag \"%s\" plug \"%s\" -> \"%s\"\n",
//			jointNode.c_str(),plugString.c_str(),jointName.c_str());
//	feLog("  scale %s\n",c_print(scaleLocal));

	SpatialTransform xformLocal;
	SpatialTransform xformLocalRef;

	MFnIkJoint fnJoint(a_parent,&status);
	if(status)
	{
		//* matrix = [S] * [RO] * [R] * [JO] * [IS] * [T]

		//* [S]  getScale()
		//* [RO] getScaleOrientation()
		//* [R]  getRotation()
		//* [JO] getOrientation()
		//* [IS] inverse of the getScale on the parent transformation matrix
		//* [T]  getTranslation()

		//* TODO scale

		const MVector trans=fnJoint.getTranslation(MSpace::kTransform);

		MQuaternion scaleOrientation;
		status=fnJoint.getScaleOrientation(scaleOrientation);

		MQuaternion rotation;
		status=fnJoint.getRotation(rotation,MSpace::kTransform);

		MQuaternion orientation;
		status=fnJoint.getOrientation(orientation);

		const SpatialVector relativeTranslation(trans[0],trans[1],trans[2]);
		const SpatialQuaternion relativeScaleOrientation(
				scaleOrientation[0],scaleOrientation[1],
				scaleOrientation[2],scaleOrientation[3]);
		const SpatialQuaternion relativeRotation(
				rotation[0],rotation[1],rotation[2],rotation[3]);
		const SpatialQuaternion relativeOrientation(
				orientation[0],orientation[1],
				orientation[2],orientation[3]);

		SpatialTransform relativeMove;
		setIdentity(relativeMove);
		translate(relativeMove,relativeTranslation);

		xformLocalRef=
				SpatialTransform(relativeScaleOrientation)*
				SpatialTransform(relativeOrientation)*
				relativeMove;

		xformLocal=
				SpatialTransform(relativeScaleOrientation)*
				SpatialTransform(relativeRotation)*
				SpatialTransform(relativeOrientation)*
				relativeMove;

#if FE_JCO_DEBUG
		if(jointName=="l_uparm")
		{
			const SpatialEuler euler=SpatialTransform(relativeRotation);

			feLog("for \"%s\" t %s r %s e %s\n",
					dagParent.name().asChar(),
					c_print(relativeTranslation),
					c_print(relativeRotation));
			feLog("  e %.6G %.6G %.6G ro %s o %s\n%s\n",
					euler[0]/degToRad,
					euler[1]/degToRad,
					euler[2]/degToRad,
					c_print(relativeScaleOrientation),
					c_print(relativeOrientation),
					c_print(xformLocal));
		}
#endif
	}
	else
	{
		setIdentity(xformLocal);
	}

	const SpatialTransform animWorld=xformLocal*m_animWorldMap[a_grandparent];

	m_animWorldMap[jointName]=animWorld;

	//* TODO ref is orientation only

	const SpatialTransform refWorld=xformLocalRef*m_refWorldMap[a_grandparent];

	m_refWorldMap[jointName]=refWorld;

	if(!a_grandparent.empty())
	{
		const Real length=1.0;	//* TODO
		m_spJointAccessible->addJoint(jointName,a_grandparent,
				xformLocal,animWorld,scaleLocal,refWorld,length,yellow);
	}

	const U32 childCount=dagParent.childCount(&status);
	for(U32 childIndex=0;childIndex<childCount;childIndex++)
	{
		MObject child=dagParent.child(childIndex,&status);
		MFnDagNode dagChild(child);
//		feLog("parent \"%s\" child \"%s\"\n",
//				dagParent.name().asChar(),dagChild.name().asChar());

		scanDag(child,jointName);
	}
}
