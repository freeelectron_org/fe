	spManifest->catalog<String>(
			"SurfaceAccessibleI.SurfaceAccessibleDrone.fe.drone")=
			"fexFlightDL";

	spManifest->catalog<String>("FlightMissionI.DroneMission.fe")="fexFlightDL";

	spManifest->catalog<String>("HandlerI.DroneControl.fe")="fexFlightDL";
	spManifest->catalog<String>("HandlerI.DroneDynamics.fe")="fexFlightDL";
	spManifest->catalog<String>("HandlerI.DroneInput.fe")="fexFlightDL";

	spManifest->catalog<String>("SurfaceI.SurfaceDrone.fe")="fexFlightDL";
