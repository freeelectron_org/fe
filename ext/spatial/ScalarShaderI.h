/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_ScalarShaderI_h__
#define __spatial_ScalarShaderI_h__

namespace fe
{
namespace ext
{

/**	Shader for DrawScalarField
	*/
class FE_DL_EXPORT ScalarShaderI : virtual public Component
{
	public:
virtual	void	shade(Real a_value, U8 *a_pixel)							= 0;
};

/**	A simple greyscale scalar field shader
	*/
class GreyShader : virtual public ScalarShaderI
{
	public:
				GreyShader(void) { }
virtual			~GreyShader(void) { }
virtual	void	shade(Real a_value, U8 *a_pixel)
		{
			a_pixel[0] = (U8)(a_value/2.0 + 50.0);
			a_pixel[1] = a_pixel[0];
			a_pixel[2] = a_pixel[0];
			a_pixel[3] = 255;
		}
};


} /* namespace ext */
} /* namespace fe */


#endif /* __spatial_ScalarShaderI_h__ */


