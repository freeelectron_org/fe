/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <solve/solve.pmh>

#include "ExplicitInertial.h"
#include "SemiImplicitInertial.h"

namespace fe
{
namespace ext
{

fe::Library* CreateSolveLibrary(fe::sp<fe::Master> spMaster)
{
	fe::assertData(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->setName("default_solve");
	pLibrary->add<ExplicitInertial>("HandlerI.ExplicitInertial.fe");
	pLibrary->add<SemiImplicitInertial>("HandlerI.SemiImplicitInertial.fe");
	return pLibrary;

}

} /* namespace ext */
} /* namespace fe */
