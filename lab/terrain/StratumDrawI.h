/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumDrawI_h__
#define __terrain_StratumDrawI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw a terrain

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumDrawI: virtual public Component
{
	public:
					//* As StratumDrawI

					/// @brief Choose a drawing implementation
virtual	void		bind(sp<DrawI> spDrawI)									=0;

					///	@brief Specify a center of interest
virtual	void		recenter(const SpatialVector& center)					=0;

					/// @brief Draw a specific terrain
virtual void		draw(const sp<StratumI>& rspStratumI)					=0;

					/// @brief Indicate zones in sight
virtual	void		assignVisibility(sp<RecordGroup>& rspRecordGroup)		=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumDrawI_h__ */
