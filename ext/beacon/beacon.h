/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __beacon_h__
#define __beacon_h__

#include "fe/plugin.h"

#include "MPP.h"
#include "BeaconServerI.h"
#include "BeaconServerStartupI.h"
#include "BeaconTimeSyncI.h"
#include "BeaconClientI.h"
#include "BeaconClientStartupI.h"

#endif // __beacon_h__
