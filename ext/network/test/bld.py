import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexNetworkDLLib" ]

    tests = [   'xMessageClient',
                'xMessageServer',
                'xSignalClient',
                'xSignalServer' ]

    for t in tests:
        exe = module.Exe(t)

        forge.deps([t + "Exe"], deplibs)

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        implEnetCat = "*.EnetCatalog"
        implShm = "*.SharedMemCatalog"
        implZero = "*.ZeroCatalog"
    else:
        implEnetCat = "'*.EnetCatalog'"
        implShm = "'*.SharedMemCatalog'"
        implZero = "'*.ZeroCatalog'"

    if 'zeromq' in forge.modules_confirmed:
        forge.tests += [
            ("xMessageServer.exe",  implZero,               "",
            ("xMessageClient.exe",  implZero,               "",
            ("xMessageClient.exe",  implZero + " nosend",   "", None))) ]

        forge.tests += [
            ("xSignalServer.exe",   implZero + " tcp 7891", "",
            ("xSignalClient.exe",   implZero + " tcp 7891", "", None)) ]

#       forge.tests += [
#           ("xSignalServer.exe",   implZero + " udp",  "",
#           ("xSignalClient.exe",   implZero + " udp",  "", None)) ]

#       if not "FE_CI_BUILD" in os.environ or os.environ["FE_CI_BUILD"] == "0":
#           forge.tests += [
#               ("xSignalServer.exe",   implZero + " ipc",  "",
#               ("xSignalClient.exe",   implZero + " ipc",  "", None)) ]

#   if 'enet' in forge.modules_confirmed:
#       forge.tests += [
#           ("xSignalServer.exe",   implEnetCat,        "",
#           ("xSignalClient.exe",   implEnetCat,        "", None)) ]

    if 'shm' in forge.modules_confirmed:
        forge.tests += [
            ("xSignalServer.exe",   implShm + " shm",       "",
            ("xSignalClient.exe",   implShm + " shm",       "", None)) ]

        forge.tests += [
            ("xSignalServer.exe",   implShm + " shm",       "",
            ("xSignalClient.exe",   implShm + " shm",       "",
            ("xSignalClient.exe",   implShm + " shm",       "", None))) ]
