#!/usr/bin/python

import sys
import string
import os
import re

re_flags = re.S|re.M
#re_flags = 0

re_srcfile = re.compile(r'(.*)\.(h|cc)$')
re_any = re.compile(r'((FE_COMP|FE_SLOT|FE_USE|FE_DISPATCH)\([^)]*\))', re_flags)
re_slot = re.compile(r'FE_SLOT\(\s*"([^"]*)"\s*\)', re_flags)
re_dispatch = re.compile(r'FE_DISPATCH\(\s*"([^"]*)"\s*,\s*"([^"]*)"\s*\)', re_flags)
re_use = re.compile(r'FE_USE\(\s*"([^"]*)"\s*\)', re_flags)
re_cfg = re.compile(r'(cfg<([\s\w<>]+)>\(([^\n"])*"([^"]*)"[^/\n]*(//([^\n]*))?\n)', re_flags)
re_cfgc = re.compile(r'(cfgComponent\(([^\n"])*"([^"]*)"[^/\n]*(//([^\n]*))?\n)', re_flags)

re_accessorset = re.compile(r'((As\w+)\s+m_as\w+\s*\;)', re_flags)

output = open(sys.argv[2], 'w')

components = {}

class Config:
    def __init__(self, match, component):
        if component:
            self.type = "sp<Component>"
            args = string.split(match.group(2), ',')
            self.name = string.strip(string.strip(args[0]), '"')
            self.record = string.strip(args[1])
            self.default = "invalid"
            self.doc = string.strip(match.group(3))
            if match.group(5):
                self.doc = string.strip(match.group(5))
            else:
                self.doc = ""
        else:
            self.type = match.group(2).strip()
            self.name = match.group(4).strip()
            if match.group(6):
                self.doc = string.strip(match.group(6))
            else:
                self.doc = ""

class Slot:
    def __init__(self, match):
        self.name = match.group(1)

class Dispatch:
    def __init__(self, match):
        self.name = match.group(1)
        self.doc = match.group(2)

class As:
    def __init__(self, match):
        self.name = match.group(2)

class Use:
    def __init__(self, match):
        self.name = match.group(1)

class Component:
    def __init__(self, name, group):
        self.group = group
        self.handler = 0
        self.name = name
        self.cfgs = []
        self.slots = []
        self.dispatches = []
        self.ases = []
        self.uses = []

def verify(component, filename):
    if component == None:
        basename = os.path.basename(filename)
        basename = re_srcfile.match(basename).group(1)
        (head, tail) = os.path.split(filename)
        component_group = os.path.basename(head)
        if component_group == None:
            component_group = "none"
        component_name = basename
        if components.has_key(component_name):
            component = components[component_name]
        else:
            component = Component(component_name, component_group)
            components[component_name] = component
    return component


def process_file(filename):
    global components
    component_name = "unknown"
    component = None
    input = open(filename, 'r')
    buffer = input.read()
    matches = re_any.findall(buffer)
    for match in matches:
        body = match[0]

        m = re_slot.match(body)
        if m:
            component = verify(component, filename)
            component.slots.append(Slot(m))

        m = re_dispatch.match(body)
        if m:
            component = verify(component, filename)
            component.dispatches.append(Dispatch(m))

        m = re_use.match(body)
        if m:
            component = verify(component, filename)
            component.uses.append(Use(m))

    matches = re_cfg.findall(buffer)
    for match in matches:
        body = match[0]

        m = re_cfg.match(body)
        if m:
            component = verify(component, filename)
            component.cfgs.append(Config(m,0))

    matches = re_cfgc.findall(buffer)
    for match in matches:
        body = match[0]

        m = re_cfgc.match(body)
        if m:
            component = verify(component, filename)
            component.cfgs.append(Config(m,1))

    matches = re_accessorset.findall(buffer)
    for match in matches:
        body = match[0]

        m = re_accessorset.match(body)
        if m:
            component = verify(component, filename)
            component.ases.append(As(m))

    basename = os.path.basename(filename)
    basename = re_srcfile.match(basename).group(1)
    re_handle = re.compile(r'.*(%s::handle\(([^\\)]*)\)).*' % basename, re_flags)
    match = re_handle.match(buffer)
    if match:
        component = verify(component, filename)
        component.handler = 1


def walker(arg, dirname, names):
    for name in names:
        if re_srcfile.match(name):
            process_file(os.path.join(dirname,name))

os.walk(sys.argv[1], walker, None)

content = """
"""

for component_name in components.keys():
    content += '/**\n@class fe::%s\n' % (component_name)
    content += '<table border="0" bgcolor="#DDDDDD">\n'
    if len(components[component_name].cfgs) > 0:
        content += '<tr><td colspan=3>ConfigI</td></tr>\n'
        content += '<tr> <td bgcolor="#EEEEEE">name</td>  <td bgcolor="#EEEEEE">type</td>  <td bgcolor="#EEEEEE">doc</td> </tr>\n'
        for config in components[component_name].cfgs:
            content += '<tr> <td bgcolor="white">%s</td> <td bgcolor="white">%s</td> <td bgcolor="white">%s</td> </tr>\n' % (config.name, config.type, config.doc)
    if len(components[component_name].slots) > 0:
        content += '<tr><td colspan=3>slots</td></tr>\n'
        for slot in components[component_name].slots:
            content += '<tr> <td colspan="3" bgcolor="white">%s</td> </tr>\n' % (slot.name)
    if len(components[component_name].dispatches) > 0:
        content += '<tr><td colspan=3>dispatches</td></tr>\n'
        content += '<tr> <td bgcolor="#EEEEEE">name</td>  <td colspan="2" bgcolor="#EEEEEE">doc</td> </tr>\n'
        for d in components[component_name].dispatches:
            content += '<tr> <td bgcolor="white">%s</td> <td colspan="2" bgcolor="white">%s</td> </tr>\n' % (d.name, d.doc)
    if len(components[component_name].uses) > 0:
        content += '<tr><td colspan=3>direct accessors</td></tr>\n'
        for u in components[component_name].uses:
            content += '<tr> <td colspan="4" bgcolor="white">%s</td> </tr>\n' % (u.name)
    if len(components[component_name].ases) > 0:
        content += '<tr><td colspan=3>accessor sets</td></tr>\n'
        for u in components[component_name].ases:
            content += '<tr> <td colspan="4" bgcolor="white">fe::%s</td> </tr>\n' % (u.name)
    content += '</table>\n*/\n\n\n'

groups = {}
for component_name in components.keys():
    if components[component_name].handler == 1:
        if not groups.has_key(components[component_name].group):
            groups[components[component_name].group] = []
        groups[components[component_name].group].append(components[component_name])

content += '/**\n@page handler_index Handler Reference\n'
for group in groups.keys():
    content += '<br><b>%s</b><ul>' % (group)
    for component in groups[group]:
        content += '<li> fe::%s\n' % (component.name)
    content += '</ul>\n'
content += '*/\n'

content += "\n"

output.write(content)
output.close()


