/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <arnold/arnold.pmh>

#include "platform/dlCore.cc"

#define FE_ARNOLD_DL_DEBUG			TRUE
#define FE_ARNOLD_DISCOVER_DEBUG	TRUE

static fe::ext::ArnoldContext feArnoldContext;

//* always called only once
static bool feArnoldInitPlugin(void** a_ppUserData)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldInitPlugin\n");
#endif

	feArnoldContext.master();
	return true;
}

//* always called, per instance, after InitPlugin
static bool feArnoldInitBounds(AtNode* a_pAtNodeProcedural,AtBBox* a_pAtBBox,
	void** a_ppUserData)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldInitBounds\n");
#endif

	fe::ext::ArnoldNode* pArnoldNode=new fe::ext::ArnoldNode();
	pArnoldNode->setAtNodeProcedural(a_pAtNodeProcedural);

	*a_ppUserData=pArnoldNode;

	pArnoldNode->initOperator();

	//* TODO
	a_pAtBBox->min[0]= -10.0;
	a_pAtBBox->min[1]= -10.0;
	a_pAtBBox->min[2]= -10.0;

	a_pAtBBox->max[0]= 10.0;
	a_pAtBBox->max[1]= 30.0;
	a_pAtBBox->max[2]= 10.0;

	return true;
}

//* called on first hit, per instance
static int feArnoldInit(AtNode* a_pAtNodeProcedural,void** a_ppUserData)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldInit\n");
#endif

	return TRUE;
}

//* called once if hit, after feArnoldInit
static int feArnoldNumNodes(void* a_pUserData)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldNumNodes\n");
#endif

	fe::ext::ArnoldNode* pArnoldNode=(fe::ext::ArnoldNode*)a_pUserData;

	if(pArnoldNode->atNode()==NULL)
	{
		pArnoldNode->runOperator();
	}

	return pArnoldNode->atNode()? 1: 0;
}

//* called for number of nodes, if hit, after feArnoldNumNodes
static AtNode* feArnoldGetNode(void* a_pUserData,int a_index)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldGetNode %d\n",a_index);
#endif

	fe::ext::ArnoldNode* pArnoldNode=(fe::ext::ArnoldNode*)a_pUserData;

	return pArnoldNode->atNode();
}

//* called if was hit, after GetNode calls completed
static int feArnoldCleanup(void* a_pUserData)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldCleanup\n");
#endif

	fe::ext::ArnoldNode* pArnoldNode=(fe::ext::ArnoldNode*)a_pUserData;
	delete pArnoldNode;

	return TRUE;
}

//* always called, only once, after render complete
static bool feArnoldCleanupPlugin(void* a_ppUserData)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldCleanupPlugin\n");
#endif

	return true;
}

AI_EXPORT_LIB int ProcLoader(AtProcVtable* a_pAtProcVtable)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> ProcLoader\n");
#endif

	a_pAtProcVtable->InitPlugin=feArnoldInitPlugin;
	a_pAtProcVtable->InitBounds=feArnoldInitBounds;
	a_pAtProcVtable->Init=feArnoldInit;
	a_pAtProcVtable->NumNodes=feArnoldNumNodes;
	a_pAtProcVtable->GetNode=feArnoldGetNode;
	a_pAtProcVtable->Cleanup=feArnoldCleanup;
	a_pAtProcVtable->CleanupPlugin=feArnoldCleanupPlugin;
	strcpy(a_pAtProcVtable->version,AI_VERSION);
	return TRUE;
}

static fe::String feArnoldShaderName;
static std::map< fe::String,std::map<fe::String,I32> > feArnoldParamMapMap;
static void feArnoldParameters(AtList* pAtList, AtMetaDataStore* mds)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldParameters \"%s\"\n",feArnoldShaderName.c_str());
#endif

	fe::sp<fe::Registry> spRegistry=feArnoldContext.master()->registry();
	fe::sp<fe::Catalog> spCatalog=
			spRegistry->create("ShaderI."+feArnoldShaderName);
	if(spCatalog.isValid())
	{
		std::map<fe::String,I32>& rParamMap=
				feArnoldParamMapMap[feArnoldShaderName];

		fe::Array<fe::String> keys;
		spCatalog->catalogKeys(keys);

		I32 paramIndex=1000;

		const U32 keyCount=keys.size();
		for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			const fe::String key=keys[keyIndex];
			if(!spCatalog->cataloged(key,"channel"))
			{
				continue;
			}

#if FE_ARNOLD_DL_DEBUG
			feLog("  channel \"%s\"\n",key.c_str());
#endif

			fe::Instance instance;
			if(!spCatalog->catalogLookup(key,instance))
			{
				const I32 offset=
						fe::ext::ShaderVariablesArnold::variableOffset(key);
				if(offset>=0)
				{
					rParamMap[key]=offset;
				}
				continue;
			}

			rParamMap[key]=paramIndex++;

			if(instance.is<fe::Color>())
			{
				const fe::Color& value=
						instance.cast<fe::Color>();

				if(spCatalog->catalogOrDefault<fe::String>(key,
						"suggest","")=="rgba")
				{
					AiNodeParamRGBA(pAtList,-1,key.c_str(),
							value[0],value[1],value[2],0.0);
				}
				else
				{
					AiNodeParamRGB(pAtList,-1,key.c_str(),
							value[0],value[1],value[2]);
				}
			}
			else if(instance.is<fe::SpatialVector>())
			{
				const fe::SpatialVector& value=
						instance.cast<fe::SpatialVector>();

				AiNodeParamVec(pAtList,-1,key.c_str(),
						value[0],value[1],value[2]);
			}
			else if(instance.is<fe::Real>())
			{
				const fe::Real value=instance.cast<fe::Real>();
				AiNodeParamFlt(pAtList,-1,key.c_str(),value);
			}
			else if(instance.is<I32>())
			{
				const I32 value=instance.cast<I32>();
				AiNodeParamInt(pAtList,-1,key.c_str(),value);
			}
			else if(instance.is<fe::String>())
			{
				const fe::String value=instance.cast<fe::String>();
				AiNodeParamStr(pAtList,-1,key.c_str(),value.c_str());
			}
		}
	}
}

static void feArnoldInitialize(AtNode* a_pAtNode, AtParamValue* pAtParamValue)
{
	const fe::String nodeName=AiNodeGetName(a_pAtNode);

	const AtNodeEntry* pAtNodeEntry=AiNodeGetNodeEntry(a_pAtNode);
	const fe::String entryName=AiNodeEntryGetName(pAtNodeEntry);

#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldInitialize \"%s\" \"%s\"\n",
			entryName.c_str(),nodeName.c_str());
#endif

	fe::sp<fe::Registry> spRegistry=feArnoldContext.master()->registry();
	fe::sp<fe::ext::ShaderI> spShaderI=spRegistry->create("ShaderI."+entryName);
	if(spShaderI.isValid())
	{
		fe::sp<fe::Catalog> spCatalog=spShaderI;
		if(spCatalog.isValid())
		{
			std::map<fe::String,I32>& rParamMap=
					feArnoldParamMapMap[entryName];
			for(std::map<fe::String,I32>::iterator it=rParamMap.begin();
					it!=rParamMap.end();it++)
			{
				spCatalog->catalog<I32>(it->first,"channel")=it->second;
			}
		}

		fe::ext::ArnoldShader* pArnoldShader=new fe::ext::ArnoldShader();
		if(pArnoldShader)
		{
			pArnoldShader->setShader(spShaderI);
			AiNodeSetLocalData(a_pAtNode,pArnoldShader);
		}
	}
}

static void feArnoldUpdate(AtNode* a_pAtNode, AtParamValue* pAtParamValue)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldUpdate\n");
#endif

	fe::ext::ArnoldShader* pArnoldShader=
			(fe::ext::ArnoldShader*)AiNodeGetLocalData(a_pAtNode);
	if(pArnoldShader)
	{
		fe::sp<fe::ext::ShaderI> spShaderI=pArnoldShader->shader();
		if(spShaderI.isValid())
		{
			spShaderI->update();
		}
	}
}

static void feArnoldEvaluate(AtNode* a_pAtNode,
	AtShaderGlobals* pAtShaderGlobals)
{
#if FE_ARNOLD_DL_DEBUG
//	feLog(">>>> feArnoldEvaluate\n");
#endif

	fe::ext::ArnoldShader* pArnoldShader=
			(fe::ext::ArnoldShader*)AiNodeGetLocalData(a_pAtNode);
	if(pArnoldShader)
	{
		fe::sp<fe::ext::ShaderI> spShaderI=pArnoldShader->shader();
		if(spShaderI.isValid())
		{
			//* TODO pool?
			fe::sp<fe::ext::ShaderVariablesArnold> spShaderVariablesArnold(
					new fe::ext::ShaderVariablesArnold());

			spShaderVariablesArnold->setAtNode(a_pAtNode);
			spShaderVariablesArnold->setAtShaderGlobals(pAtShaderGlobals);
			spShaderI->evaluate(spShaderVariablesArnold);
		}
	}
}

static void feArnoldFinish(AtNode* a_pAtNode)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> feArnoldFinish\n");
#endif

	fe::ext::ArnoldShader* pArnoldShader=
			(fe::ext::ArnoldShader*)AiNodeGetLocalData(a_pAtNode);
	delete pArnoldShader;
}

static AtCommonMethods feAtCommonMethods=
{
	feArnoldParameters,
	feArnoldInitialize,
	feArnoldUpdate,
	feArnoldFinish
};

static AtShaderNodeMethods feAtShaderNodeMethods=
{
	feArnoldEvaluate
};

static AtNodeMethods feAtNodeMethods=
{
	&feAtCommonMethods,
	&feAtShaderNodeMethods
};

bool feArnoldAddShader(int a_index,AtNodeLib* a_pAtNodeLib);

AI_EXPORT_LIB bool NodeLoader(int a_index,AtNodeLib* a_pAtNodeLib)
{
#if FE_ARNOLD_DL_DEBUG
	feLog(">>>> NodeLoader %d\n",a_index);
#endif

	strcpy(a_pAtNodeLib->version,AI_VERSION);

	return feArnoldAddShader(a_index,a_pAtNodeLib);
}

using namespace fe;
using namespace fe::ext;

bool feArnoldAddShader(int a_index,AtNodeLib* a_pAtNodeLib)
{
	sp<Registry> spRegistry=feArnoldContext.master()->registry();
	feArnoldContext.loadLibraries();

	spRegistry->manage("fexShaderDL");

	Array<String> available;
	spRegistry->listAvailable("ShaderI",available);
	U32 candidateCount=available.size();

	//* TODO check all nodes at once and store results

	I32 shaderIndex=0;

	for(U32 candidateIndex=0;candidateIndex<candidateCount;candidateIndex++)
	{
		const String implementation=available[candidateIndex];

#if FE_ARNOLD_DISCOVER_DEBUG
		feLog("feArnoldAddShader candidate %d/%d \"%s\"\n",
				candidateIndex,candidateCount,implementation.c_str());
#endif

		sp<ShaderI> spShaderI=
				spRegistry->create(implementation);
		if(spShaderI.isNull())
		{
			feLog("feArnoldAddShader Component is not a ShaderI \"%s\"\n",
					implementation.c_str());
			continue;
		}

		if(shaderIndex!=a_index)
		{
			shaderIndex++;
			continue;
		}

		AtByte outputType=AI_TYPE_NONE;

		sp<Catalog> spCatalog=spShaderI;
		if(spCatalog.isValid())
		{
			const String output=spCatalog->catalog<String>("output");
			if(output=="rgb")
			{
				outputType=AI_TYPE_RGB;
			}
			else if(output=="rgba")
			{
				outputType=AI_TYPE_RGBA;
			}
			else if(output=="vector")
			{
				outputType=AI_TYPE_VECTOR;
			}
			else if(output=="real")
			{
				outputType=AI_TYPE_FLOAT;
			}
			else if(output=="integer")
			{
				outputType=AI_TYPE_INT;
			}
			else if(output=="string")
			{
				outputType=AI_TYPE_STRING;
			}
		}

		if(outputType==AI_TYPE_NONE)
		{
			feLog("feArnoldAddShader \"%s\" has unsupported output\n",
					implementation.c_str());
			continue;
		}

		String feName=implementation;
		char* buffer1=strchr(const_cast<char*>(implementation.c_str()),'.');
		if(buffer1)
		{
			buffer1++;
			const char* buffer2=strchr(buffer1,'.');
			if(buffer2>buffer1)
			{
				feName.sPrintf("%.*s",int(buffer2-buffer1),buffer1);
			}
			else
			{
				feName=buffer1;
			}
		}

#if FE_ARNOLD_DISCOVER_DEBUG
		feLog("feArnoldAddShader feName \"%s\"\n",feName.c_str());
#endif

		a_pAtNodeLib->name=feName.c_str();
		a_pAtNodeLib->output_type=outputType;
		a_pAtNodeLib->node_type=AI_NODE_SHADER;
		a_pAtNodeLib->methods=&feAtNodeMethods;

		feArnoldShaderName=feName;

		return true;
	}

	return false;
}

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new fe::String("fexTerminalDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
