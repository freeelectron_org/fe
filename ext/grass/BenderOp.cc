/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

//* TODO cleanup brush update after view camera moves
//* TODO figure out why m_aFrame.check(spRA) sometimes fails (repeatedly)

#define FE_BDO_DEBUG		FALSE
#define FE_BDO_KEY_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void BenderOp::initParam(String a_prefix,String a_label,BWORD a_joined)
{
	const String key=a_prefix+a_label;
	catalog<Real>(key)=0.0;
	catalog<Real>(key,"low")= -2.0;
	catalog<Real>(key,"high")=2.0;
	catalog<String>(key,"IO")="input output";
	catalog<String>(key,"label")=a_label;
	catalog<String>(key,"page")="Edit";
	catalog<bool>(key,"joined")=a_joined;
}

void BenderOp::initialize(void)
{
	//* Page Data

	catalog<String>("ChangeGroup")="bent";
	catalog<String>("ChangeGroup","label")="Change Group";
	catalog<String>("ChangeGroup","page")="Data";
	catalog<String>("ChangeGroup","hint")=
			"Put all changed primitives in this new group.";

	catalog<bool>("fragment")=false;
	catalog<String>("fragment","label")="Fragment";
	catalog<String>("fragment","page")="Data";
	catalog<bool>("fragment","joined")=true;
	catalog<String>("fragment","hint")=
			"Associate collections of points which have"
			" matching a fragment attribute.";

	catalog<String>("fragmentAttr")="part";
	catalog<String>("fragmentAttr","label")="By Attr";
	catalog<String>("fragmentAttr","page")="Data";
	catalog<String>("fragmentAttr","enabler")="fragment";
	catalog<String>("fragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of points.";

	//* Page Edit

	catalog<Real>("Sensitivity")=1.0;
	catalog<Real>("Sensitivity","high")=1.0;
	catalog<Real>("Sensitivity","max")=100.0;
	catalog<String>("Sensitivity","page")="Edit";
	catalog<String>("Sensitivity","hint")=
			"Adjust magnitude of interactive changes.";

	catalog<bool>("ForceKey")=false;
	catalog<String>("ForceKey","label")="Alter Specific Key";
	catalog<String>("ForceKey","page")="Edit";
	catalog<bool>("ForceKey","joined")=true;
	catalog<String>("ForceKey","hint")=
			"Make interactive changes to the key at a specific frame"
			" (otherwise use current frame).";

	catalog<Real>("ForceFrame")=0.0;
	catalog<Real>("ForceFrame","high")=1e3;
	catalog<String>("ForceFrame","label")="At Frame";
	catalog<String>("ForceFrame","enabler")="ForceKey";
	catalog<String>("ForceFrame","page")="Edit";
	catalog<String>("ForceFrame","hint")=
			"Make interactive changes to the key at this frame.";

	for(U32 m=0;m<2;m++)
	{
		const String prefix=m? "after": "before";

		const String key=prefix+"Frame";
		const String label=String(m? "Following": "Leading")+" Frame";
		catalog<Real>(key)=0.0;
		catalog<Real>(key,"high")=1e3;
		catalog<Real>(key,"max")=1e6;
		catalog<String>(key,"IO")="output";
		catalog<String>(key,"label")=label;
		catalog<String>(key,"page")="Edit";

		initParam(prefix,"LeanX",TRUE);
		initParam(prefix,"ArcX",TRUE);
		initParam(prefix,"SpinY",FALSE);
		initParam(prefix,"LeanZ",TRUE);
		initParam(prefix,"ArcZ",TRUE);
		initParam(prefix,"TwistY",FALSE);
	}

	//* Page Behavior

	catalog<bool>("FastPower")=true;
	catalog<String>("FastPower","label")="Fast Power";
	catalog<String>("FastPower","page")="Behavior";

	catalog<Real>("Linearity")=0.45;
	catalog<Real>("Linearity","max")=1.0;
	catalog<String>("Linearity","enabler")="FastPower";
	catalog<String>("Linearity","page")="Behavior";

	//* Page Display

	catalog<bool>("DisplayOthers")=true;
	catalog<String>("DisplayOthers","label")="Keyed Primitives";
	catalog<String>("DisplayOthers","page")="Display";

	catalog<bool>("DisplayTracers")=true;
	catalog<String>("DisplayTracers","label")="Tracers";
	catalog<String>("DisplayTracers","page")="Display";

	catalog<bool>("DisplayEcho")=false;
	catalog<String>("DisplayEcho","label")="Temporal Echo (when isolated)";
	catalog<String>("DisplayEcho","page")="Display";

	catalog<bool>("DisplayTwist")=true;
	catalog<String>("DisplayTwist","label")="Twist";
	catalog<String>("DisplayTwist","page")="Display";

	catalog<bool>("DisplayAngle")=true;
	catalog<String>("DisplayAngle","label")="Angle";
	catalog<String>("DisplayAngle","page")="Display";

	catalog<bool>("DisplayText")=true;
	catalog<String>("DisplayText","label")="Overlay Text";
	catalog<String>("DisplayText","page")="Display";

	catalog<bool>("DisplayTimeline")=true;
	catalog<String>("DisplayTimeline","label")="Overlay Timeline";
	catalog<String>("DisplayTimeline","page")="Display";

	catalog<bool>("DisplayCurves")=false;
	catalog<String>("DisplayCurves","label")="Overlay Curves";
	catalog<String>("DisplayCurves","page")="Display";

	catalog<Real>("CurveSpace")=0.2;
	catalog<Real>("CurveSpace","max")=1.0;
	catalog<String>("CurveSpace","label")="Curve Space";
	catalog<String>("CurveSpace","page")="Display";
	catalog<String>("CurveSpace","enabler")="DisplayCurves";

	catalog<Real>("CurveScale")=1.0;
	catalog<Real>("CurveScale","high")=4.0;
	catalog<Real>("CurveScale","max")=100.0;
	catalog<String>("CurveScale","label")="Curve Scale";
	catalog<String>("CurveScale","page")="Display";
	catalog<String>("CurveScale","enabler")="DisplayCurves";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")=
			"LMB selects/leans a primitive."
			"  ctrl-LMB bends it."
			"  MMB spins it."
			"  ctrl-MMB twists it."
			"  Enter isolates it."
			"  DEL removes a key (shift for all its keys)."
			"  ESC deselects.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog<bool>("Output Surface","recycle")=true;

	m_spChart=new DrawMode();
	m_spChart->setDrawStyle(DrawMode::e_wireframe);
	m_spChart->setPointSize(6.0);
	m_spChart->setLineWidth(1.0);

	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setLineWidth(2.0);
//	m_spWireframe->setAntialias(TRUE);

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_solid);
}

void BenderOp::handle(Record& a_rSignal)
{
	m_fastPower=catalog<bool>("FastPower");
	m_linearity=catalog<Real>("Linearity");

	const Real sensitivity=catalog<Real>("Sensitivity");

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

#if FE_BDO_DEBUG
	feLog("BenderOp::handle brush %d brushed %d frame %.6G keyframe %.6G\n",
			spDrawBrush.isValid(),m_brushed,m_frame,m_keyframe);
#endif

	I32 pickIndex=m_fragmented? m_pickId: lookupIndex(m_pickId);
	if(pickIndex<0)
	{
		m_pickId= -1;
	}

	if(spDrawBrush.isValid())
	{
		spDrawBrush->pushDrawMode(m_spWireframe);

		m_brushed=TRUE;

		const Color black(0.0,0.0,0.0,1.0);
		const Color white(1.0,1.0,0.8,1.0);
		const Color grey(0.8,0.8,1.0,1.0);
		const Color darkgrey(0.5,0.5,0.5,1.0);
		const Color red(0.8,0.0,0.0,1.0);
		const Color green(0.0,0.5,0.0,1.0);
		const Color blue(0.0,0.0,1.0,1.0);
		const Color cyan(0.0,0.7,0.9,1.0);
		const Color magenta(0.8,0.0,0.8,1.0);
		const Color pink(0.8,0.4,0.4,1.0);
		const Color yellow(0.7,0.7,0.0,1.0);

		const SpatialVector up(0.0,1.0,0.0);

		const Real circleRadius=0.1;
		const SpatialVector circleScale(circleRadius,circleRadius,circleRadius);

		const Real sphereRadius=0.01;
		const SpatialVector sphereScale(sphereRadius,sphereRadius,sphereRadius);

		sp<ViewI> spView=spDrawBrush->view();
		const Box2i viewport=spView->viewport();

		const I32 xWidth=spDrawBrush->font()->pixelWidth("X");
		const I32 xStart=20*xWidth;
		const I32 xEnd=viewport.size()[0]-xWidth;

		const Real minFrame=m_startFrame;
		const Real maxFrame=m_endFrame;

		I32 highlightIndex= -1;
		I32 highlightId= -1;

		if(m_spOutput.isValid())
		{
			BWORD cook=FALSE;

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			m_event.bind(windowEvent(a_rSignal));

#if FE_BDO_DEBUG
			feLog("%s\n",c_print(m_event));
			feLog("ray %s  %s\n",c_print(rRayOrigin),c_print(rRayDirection));
#endif

			U32 item=m_event.item();
			BWORD shifted=FALSE;
			if(item&WindowEvent::e_keyShift)
			{
				shifted=TRUE;
				item^=WindowEvent::e_keyShift;
			}
			BWORD ctrled=FALSE;
			if(item&WindowEvent::e_keyControl)
			{
				ctrled=TRUE;
				item^=WindowEvent::e_keyControl;
			}

			SpatialTransform pickedTip;

			if(m_event.isKeyPress(WindowEvent::e_itemAny))
			{
				if(item==WindowEvent::e_keyEscape)
				{
					m_pickId= -1;
					m_key=Record();
					pickIndex= -1;
				}
				if(item==WindowEvent::e_keyCarriageReturn)
				{
					m_isolate=!m_isolate;
				}
				if(item==WindowEvent::e_keyDelete)
				{
					if(m_pickId>=0)
					{
						anticipate("BenderOp delete");

						if(shifted)
						{
							removeChannel(m_pickId);
							if(m_spChangeGroup.isValid())
							{
								m_spChangeGroup->remove(pickIndex);
							}
						}
						else
						{
							removeKey(m_pickId,m_keyframe);
						}
						m_key=Record();

						resolve("BenderOp delete");
					}
				}
				cook=TRUE;
			}

			const WindowEvent::MouseButtons buttons=m_event.mouseButtons();

			m_transient=!!buttons;

			//* start dragging
			if(!m_dragging && m_event.isMouseDrag())
			{
				m_changing="";

				if(buttons==WindowEvent::e_mbLeft)
				{
					m_changing=ctrled? "BenderOp bend": "BenderOp lean";
				}
				if(buttons==WindowEvent::e_mbMiddle)
				{
					m_changing=ctrled? "BenderOp twist": "BenderOp spin";
				}
				if(!m_changing.empty())
				{
					anticipate(m_changing);
				}
			}
			if(m_dragging && m_event.isMouseRelease())
			{
				resolve(m_changing);
			}

			//* left release (no drag): select
			//* left drag XY: lean - rotate base and tip the same
			//* control-left drag XY: rotate base more
			//* shift-left drag Y: bias?

			//* middle drag X: spin base and tip together
			//* control-middle drag X: twist tip, added to spin

			//* enter: isolate selection (shrink others)
			//* esc: deselect

			if(!m_dragging)
			{
				const Real maxDistance= -1.0;
				sp<SurfaceI::ImpactI> spImpact=m_spOutput->rayImpact(
						rRayOrigin,rRayDirection,maxDistance);

				if(spImpact.isValid())
				{
					sp<SurfaceSearchable::Impact> spSearchableImpact=spImpact;
					if(spSearchableImpact.isValid())
					{
						highlightIndex=spSearchableImpact->primitiveIndex();

						sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
#if FE_BDO_DEBUG
						I32 face= -1;
						if(spTriImpact.isValid())
						{
							face=spTriImpact->face();
						}

						feLog("BenderOp::handle face %d pick %d\n",
								face,highlightIndex);
#endif
						if(!m_fragmented)
						{
							highlightId=lookupId(highlightIndex);
						}
						else if(spTriImpact.isValid() &&
								m_spInputFragment.isValid())
						{
							const I32 pointIndex0=
									spTriImpact->pointIndex0();
							const String fragmentName=
									m_spInputFragment->string(pointIndex0);
							highlightIndex=m_indexOfFragment[fragmentName];

							highlightId=highlightIndex;
						}
					}
				}

				if(m_event.isMouseRelease(WindowEvent::e_itemLeft))
				{
					pickIndex=highlightIndex;
					m_pickId=highlightId;
					m_key=Record();
					m_transient=TRUE;
				}
			}

			if(m_event.isMouseRelease(WindowEvent::e_itemAny))
			{
				cook=TRUE;
			}

			if(buttons && m_pickId>=0)
			{
				const I32 mouseX=m_event.mouseX();
				const I32 mouseY=m_event.mouseY();

				const I32 moveX=mouseX-m_lastX;
				const I32 moveY=mouseY-m_lastY;

				const Real scaledX=0.01*sensitivity*moveX;
				const Real scaledY=0.01*sensitivity*moveY;

				SpatialVector groundRay=rRayDirection;
				groundRay[1]=0.0;
				normalize(groundRay);
				const Real deltaX=groundRay[2]*scaledY+groundRay[0]*scaledX;
				const Real deltaZ=groundRay[0]*scaledY-groundRay[2]*scaledX;

				if(buttons&WindowEvent::e_mbLeft)
				{
					if(m_event.isMouseDrag())
					{
						if(!m_key.isValid())
						{
							m_key=keyAt(m_pickId,m_keyframe);
						}
						if(m_key.isValid())
						{
							if(ctrled)
							{
								m_aArcX(m_key)+=deltaX;
								m_aArcZ(m_key)+=deltaZ;
							}
							else
							{
								m_aLeanX(m_key)+=deltaX;
								m_aLeanZ(m_key)+=deltaZ;
							}
						}
					}
				}

				if(buttons&WindowEvent::e_mbMiddle)
				{
					if(m_event.isMouseDrag())
					{
						if(!m_key.isValid())
						{
							m_key=keyAt(m_pickId,m_keyframe);
						}
						if(m_key.isValid())
						{
							if(ctrled)
							{
								m_aTwistY(m_key)+=scaledX;
							}
							else
							{
								m_aSpinY(m_key)+=scaledX;
							}
						}
					}
				}

				m_lastX=mouseX;
				m_lastY=mouseY;
				cook=TRUE;
			}

			m_dragging=m_event.isMouseDrag();

			BWORD drewPicked=FALSE;
			const I32 objectCount=m_fragmented?
					m_spInputPoint->fragmentCount():
					m_spOutputVertices->count();

			sp<SurfaceAccessibleI::FilterI> spFilter;

			sp<RecordArray> spRA=m_spState->getArray("Channel");
			if(spRA.isValid())
			{
				for(int i=0;i<spRA->length();i++)
				{
					const I32 primId=m_aIndex(spRA,i);

					const I32 objectIndex=
							m_fragmented? primId: lookupIndex(primId);

					const BWORD picked=(primId==m_pickId);
					const BWORD highlighted=(primId==highlightId);
					if(m_isolate && !picked)
					{
						continue;
					}

					if(objectIndex<0 || objectIndex>=objectCount)
					{
						feLog("BenderOp::handle"
								" bad objectIndex %d/%d id %d\n",
								objectIndex,objectCount,primId);
						if(primId<0)
						{
							removeChannel(primId);
							i--;
						}
						continue;
					}

					Record channel=spRA->getRecord(i);
					Record key=evalAt(channel,m_keyframe);

					I32 elementCount=0;

					if(m_fragmented)
					{
						const String fragmentName=
								m_spInputPoint->fragment(objectIndex);

						if(!m_spInputPoint->filterWith(fragmentName,spFilter)
								|| spFilter.isNull())
						{
							continue;
						}

						elementCount=spFilter->filterCount();
					}
					else
					{
						elementCount=m_spOutputVertices->subCount(objectIndex);
					}

					if(!elementCount)
					{
						continue;
					}

					SpatialTransform parent;
					SpatialTransform root;
					SpatialTransform end;
#if FE_CODEGEN<=FE_DEBUG
					BWORD success=
#endif
							calculateExtent(TRUE,objectIndex,parent,root,end);
					FEASSERT(success);

					SpatialTransform base;
					SpatialTransform diff;
					calculateTransforms(key,root,end,base,diff);

					SpatialVector line[4];

					if(picked)
					{
						if(catalog<bool>("DisplayAngle"))
						{
							line[0]=base.translation();
							line[1]=(base*diff).translation();
							line[2]=line[1];
							line[2][1]=line[0][1];

							transformVector(parent,line[0],line[0]);
							transformVector(parent,line[1],line[1]);
							transformVector(parent,line[2],line[2]);

							line[3]=line[0];
							spDrawBrush->drawLines(line,NULL,4,
									DrawI::e_strip,false,&magenta);
						}
//~						pickedTip=base*diff;
						drewPicked=TRUE;
					}

					set(line[0]);
					line[1]=SpatialVector(2.0,0.0,0.0);

					const U32 rings=picked? 9: 2;
					const Color* pRingColor=
							picked? &blue: (highlighted? &cyan: &grey);
					for(U32 m=0;m<rings;m++)
					{
						SpatialTransform between;
						interpolate(between,base,diff,m/Real(rings-1));

						between=between*parent;

						if(picked && catalog<bool>("DisplayTwist"))
						{
							spDrawBrush->drawTransformedLines(
									between,&circleScale,
									line,NULL,2,
									DrawI::e_discrete,false,&red);
						}

						if(picked || catalog<bool>("DisplayOthers"))
						{
							rotate(between,-90.0*degToRad,e_xAxis);
							spDrawBrush->drawCircle(between,&circleScale,
									*pRingColor);
						}
					}
				}
			}
			if(m_pickId>=0)
			{
				SpatialTransform parent;
				SpatialTransform root;
#if FE_CODEGEN<=FE_DEBUG
				BWORD success=
#endif
						calculateExtent(TRUE,pickIndex,parent,root,pickedTip);
				FEASSERT(success);

				//* account for immediate changes
				Record key=evalAt(m_pickId,m_frame);
				if(key.isValid())
				{
					SpatialTransform base;
					SpatialTransform diff;
					calculateTransforms(key,root,pickedTip,base,diff);
					pickedTip=base*diff;
				}

				if(!drewPicked)
				{

					rotate(root,-90.0*degToRad,e_xAxis);
					spDrawBrush->drawCircle(root,&circleScale,yellow);
				}
				else
				{
					const I32 maxSteps=512;
					const I32 frameCount=maxFrame-minFrame;
					const I32 frameStep=frameCount/maxSteps+1;
					const I32 lineCount=
							fe::maximum(I32(1),frameCount/frameStep+1);

					SpatialVector* line=new SpatialVector[lineCount];
					SpatialVector* midline=new SpatialVector[lineCount];

					const I32 maxEchoes=32;
					const I32 echoStep=frameCount/maxEchoes+1;
					const I32 echoSync=echoStep/frameStep;
					const I32 echoCount=frameCount/echoStep+1;

					SpatialVector* echo=new SpatialVector[echoCount*4];

					I32 lineIndex=0;
					I32 echoIndex=0;
					for(I32 frameIndex=0;frameIndex<frameCount;
							frameIndex+=frameStep)
					{
						Real frame=minFrame+frameIndex;
						Record inputKey=evalAt(m_pickId,frame);

						SpatialTransform inputParent;
						SpatialTransform inputRoot;
						SpatialTransform inputTip;
#if FE_CODEGEN<=FE_DEBUG
						BWORD success=
#endif
								calculateExtent(TRUE,pickIndex,
								inputParent,inputRoot,inputTip);
						FEASSERT(success);

						SpatialTransform inputBase;
						SpatialTransform inputDiff;
						calculateTransforms(inputKey,inputRoot,inputTip,
								inputBase,inputDiff);
						SpatialTransform tip=inputBase*inputDiff;

						transformVector(parent,
								tip.translation(),line[lineIndex]);

						SpatialTransform half;
						interpolate(half,inputBase,inputDiff,0.5);

						transformVector(parent,
								half.translation(),midline[lineIndex++]);

						if(m_isolate && !(frameIndex%echoSync))
						{
							transformVector(parent,
									inputBase.translation(),echo[echoIndex++]);
							transformVector(parent,
									half.translation(),echo[echoIndex++]);
							transformVector(parent,
									half.translation(),echo[echoIndex++]);
							transformVector(parent,
									tip.translation(),echo[echoIndex++]);
						}
					}

					if(catalog<bool>("DisplayTracers") &&
							lineIndex>1 && lineIndex<=maxSteps)
					{
						spDrawBrush->drawLines(line,NULL,lineIndex,
								DrawI::e_strip,FALSE,&white);
						spDrawBrush->drawLines(midline,NULL,lineIndex,
								DrawI::e_strip,FALSE,&grey);
					}

					if(catalog<bool>("DisplayEcho") && echoIndex>1)
					{
						spDrawBrush->drawLines(echo,NULL,echoIndex,
								DrawI::e_discrete,FALSE,&darkgrey);
					}

					delete[] echo;
					delete[] midline;
					delete[] line;
				}

				spDrawBrush->pushDrawMode(m_spSolid);

				spDrawBrush->drawSphere(pickedTip*parent,&sphereScale,yellow);

				spDrawBrush->popDrawMode();
			}
			if(highlightId>=0 && highlightId!=m_pickId)
			{
				SpatialTransform parent;
				SpatialTransform root;
#if FE_CODEGEN<=FE_DEBUG
				BWORD success=
#endif
						calculateExtent(FALSE,highlightIndex,
						parent,root,pickedTip);
				FEASSERT(success);

				spDrawBrush->pushDrawMode(m_spSolid);

				spDrawBrush->drawSphere(pickedTip*parent,&sphereScale,blue);

				spDrawBrush->popDrawMode();
			}

			if(cook)
			{
				catalog<String>("Brush","cook")="once";
			}
		}

		if(spDrawOverlay.isValid())
		{
			const Real multiplication=spDrawOverlay->multiplication();

			spDrawOverlay->pushDrawMode(m_spChart);

			const BWORD displayCurves=catalog<bool>("DisplayCurves");
			const BWORD displayTimeline=catalog<bool>("DisplayTimeline");
			const Real curveSpace=catalog<Real>("CurveSpace");
			const Real curveScale=catalog<Real>("CurveScale");

			SpatialVector line[2];

			if(catalog<bool>("DisplayText") && pickIndex>=0)
			{
				set(line[0],4*multiplication,48*multiplication,1.0);

				String text;
				if(m_fragmented)
				{
					const String fragmentName=
							m_spInputPoint->fragment(pickIndex);
					text.sPrintf("Fragment %s",fragmentName.c_str());
				}
				else
				{
					text.sPrintf("Primitive %d Id %d",pickIndex,m_pickId);
				}
				spDrawOverlay->drawAlignedText(line[0],text,blue);
			}

			const I32 yCurveBase=(displayTimeline? 40: 20)*multiplication;

			if(displayTimeline)
			{
				const I32 yTimeline=yCurveBase-20*multiplication;
				const I32 yKey=yTimeline+5*multiplication;

				set(line[0],xStart,yTimeline,1.0);
				set(line[1],xEnd,yTimeline,1.0);
				spDrawOverlay->drawLines(line,NULL,2,
						DrawI::e_discrete,FALSE,&yellow);

				BWORD hitKey=FALSE;
				BWORD hitFrame=FALSE;
				if(m_pickId>=0)
				{
					const I32 keys=keyCount(m_pickId);
					for(I32 keyIndex=0;keyIndex<keys;keyIndex++)
					{
						const Record key=indexedKey(m_pickId,keyIndex);
						const Real frame=m_aFrame(key);

						const BWORD onKey=(fabs(frame-m_keyframe)<1e-3);
						if(onKey)
						{
							hitKey=TRUE;
						}

						const BWORD onFrame=(fabs(frame-m_frame)<1e-3);
						if(onFrame)
						{
							hitFrame=TRUE;
						}

						const I32 x0=xStart+(xEnd-xStart)*
								((frame-minFrame)/(maxFrame-minFrame));

						drawOverlayKey(spDrawOverlay,x0,yKey,frame,
								onKey? red: (onFrame? green: blue));
					}
				}

				if(!hitKey)
				{
					const I32 x0=xStart+(xEnd-xStart)*
							((m_keyframe-minFrame)/(maxFrame-minFrame));

					drawOverlayKey(spDrawOverlay,x0,yKey,m_keyframe,
							(m_frame==m_keyframe)? magenta: cyan);
				}

				if(m_frame!=m_keyframe && !hitFrame)
				{
					const I32 x0=xStart+(xEnd-xStart)*
							((m_frame-minFrame)/(maxFrame-minFrame));

					drawOverlayKey(spDrawOverlay,x0,yKey,m_frame,yellow);
				}
			}

			if(m_pickId>=0 && displayCurves)
			{
				Color trackColor[6];
				set(trackColor[0],0.8,0.4,0.0,1.0);
				set(trackColor[1],0.8,0.0,0.4,1.0);
				set(trackColor[2],0.4,0.8,0.0,1.0);
				set(trackColor[3],0.0,0.8,0.4,1.0);
				set(trackColor[4],0.4,0.0,0.8,1.0);
				set(trackColor[5],0.0,0.4,0.8,1.0);

				const I32 halfSpace=0.5*curveSpace*viewport.size()[1];
				const I32 yScale=curveScale*halfSpace;
				const I32 yCenter=yCurveBase+halfSpace;

				const U32 tracks=6;
				const I32 maxSteps=128;
				const I32 frameCount=maxFrame-minFrame;
				const I32 frameStep=frameCount/maxSteps+1;
				const I32 lineCount=
						fe::maximum(I32(1),frameCount/frameStep+1);

				SpatialVector* trackLine[tracks];
				for(U32 track=0;track<tracks;track++)
				{
					trackLine[track]=new SpatialVector[lineCount];
				}

				I32 frameIndex=0;
				for(I32 lineIndex=0;lineIndex<lineCount;lineIndex++)
				{
					Real frame=minFrame+frameIndex;
					Record key=evalAt(m_pickId,frame);
					if(!key.isValid())
					{
						break;
					}

					for(U32 track=0;track<tracks;track++)
					{
						Real value=0.0;
						switch(track)
						{
							case 0:
								value=m_aLeanX(key);
								break;
							case 1:
								value=m_aLeanZ(key);
								break;
							case 2:
								value=m_aArcX(key);
								break;
							case 3:
								value=m_aArcZ(key);
								break;
							case 4:
								value=m_aSpinY(key);
								break;
							case 5:
								value=m_aTwistY(key);
								break;
							default:
								break;
						}
						const I32 x0=xStart+(xEnd-xStart)*
								((frame-minFrame)/(maxFrame-minFrame));
						const I32 y2=yCenter+yScale*value;
						set(trackLine[track][lineIndex],x0,y2,1.0);
					}

					frameIndex+=frameStep;
				}

				if(lineCount>0)
				{
					for(U32 track=0;track<tracks;track++)
					{
						spDrawOverlay->drawLines(trackLine[track],NULL,
								lineCount,DrawI::e_strip,
								FALSE,&trackColor[track]);
					}
				}

				for(U32 track=0;track<tracks;track++)
				{
					delete[] trackLine[track];
				}

				const Real incY=spDrawOverlay->font()->fontHeight(NULL,NULL);
				const Real legendX=spDrawOverlay->font()->pixelWidth("X");
				Real legendY=yCenter+incY*2;

				spDrawOverlay->drawAlignedText(
						SpatialVector(legendX,legendY,1.0),
						"LeanX",trackColor[0]);
				legendY-=incY;
				spDrawOverlay->drawAlignedText(
						SpatialVector(legendX,legendY,1.0),
						"LeanZ",trackColor[1]);
				legendY-=incY;
				spDrawOverlay->drawAlignedText(
						SpatialVector(legendX,legendY,1.0),
						"ArcX",trackColor[2]);
				legendY-=incY;
				spDrawOverlay->drawAlignedText(
						SpatialVector(legendX,legendY,1.0),
						"ArcZ",trackColor[3]);
				legendY-=incY;
				spDrawOverlay->drawAlignedText(
						SpatialVector(legendX,legendY,1.0),
						"SpinY",trackColor[4]);
				legendY-=incY;
				spDrawOverlay->drawAlignedText(
						SpatialVector(legendX,legendY,1.0),
						"TwistY",trackColor[5]);
				legendY-=incY;
			}

			spDrawOverlay->popDrawMode();
		}

		spDrawBrush->popDrawMode();

		return;
	}

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	if(!access(m_spOutput,spOutputAccessible)) return;

	if(!access(m_spInputPoint,spInputAccessible,
			e_point,e_position)) return;

	if(!access(m_spOutputPoint,spOutputAccessible,
			e_point,e_position)) return;

	if(!access(m_spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	if(!access(m_spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	m_spInputFragment=NULL;

	I32 fragmentCount=1;

	m_indexOfFragment.clear();

	const String fragmentAttr=catalog<String>("fragmentAttr");

	m_fragmented=(!fragmentAttr.empty() && catalog<bool>("fragment"));
	if(m_fragmented)
	{
		access(m_spInputFragment,spInputAccessible,e_point,fragmentAttr);

		const String inputGroup="";	//* TODO

		m_spInputPoint->fragmentWith(SurfaceAccessibleI::e_point,
				fragmentAttr,inputGroup);
		fragmentCount=m_spInputPoint->fragmentCount();
		if(!fragmentCount)
		{
			fragmentCount=1;
			m_fragmented=FALSE;
		}

		for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
		{
			const String fragmentName=m_spInputPoint->fragment(fragmentIndex);
			m_indexOfFragment[fragmentName]=fragmentIndex;
		}
	}

	catalog<String>("summary")=
			m_fragmented? fragmentAttr: catalog<String>("IdAttr");

	if(!m_synced)
	{
		syncIds();
		m_synced=TRUE;
	}

	Real inputRadius=0.0;
	try
	{
		inputRadius=m_spOutput->radius();
	}
	catch(const Exception& e)
	{
//		catalog<String>("warning")+="\nFE EXCEPTION: "+print(e)+";";
	}
	if(inputRadius<1e-6)
	{
		catalog<String>("warning")+=
				"Input Surface is not pickable in current form"
				" (existing keys will still be applied);";
	}

	access(m_spDriver,"Driver Surface",e_quiet);

	const String changeGroup=catalog<String>("ChangeGroup");
	const BWORD hadChangeGroup=m_spChangeGroup.isValid();
	if(!changeGroup.empty())
	{
		if(!accessOutput(m_spChangeGroup,a_rSignal,
				e_primitiveGroup,changeGroup)) return;
	}
	else
	{
		m_spChangeGroup=NULL;
	}
	if(m_spChangeGroup.isValid() && !hadChangeGroup)
	{
		//* TODO clear all entries in change group
	}

	m_frame=currentFrame(a_rSignal);
	m_startFrame=startFrame(a_rSignal);
	m_endFrame=endFrame(a_rSignal);

	const Real wasKeyFrame=m_keyframe;
	m_keyframe=m_frame;
	if(catalog<bool>("ForceKey"))
	{
		m_keyframe=catalog<Real>("ForceFrame");
		if(m_keyframe<m_startFrame)
		{
			m_keyframe=m_startFrame;
		}
		if(m_keyframe>m_endFrame)
		{
			m_keyframe=m_endFrame;
		}
	}
	if(wasKeyFrame!=m_keyframe)
	{
		m_key=Record();
	}

	const BWORD frameChanged=(m_frame!= m_lastFrame);
	const BWORD replaced=(catalog<bool>("Output Surface","replaced"));
	const BWORD paramChanged=(!m_brushed && !replaced && !frameChanged);
	m_brushed=FALSE;

	const BWORD rebuild=(paramChanged || frameChanged || replaced);
	if(rebuild)
	{
		m_isolating= -1;
		m_key=Record();
	}

#if FE_BDO_DEBUG
	feLog("BenderOp::handle frame %.6G->%.6G paramChanged %d"
			" frameChanged %d replaced %d\n",
			m_lastFrame,m_frame,paramChanged,frameChanged,replaced);
#endif

	m_lastFrame=m_frame;

	updateIds();

	const I32 objectCount=m_fragmented?
			m_spInputPoint->fragmentCount():
			m_spOutputVertices->count();

	const I32 toIsolate=m_isolate? pickIndex: -1;
	const BWORD refreshIsolation=(m_isolating!=toIsolate);

	if(refreshIsolation)
	{
		const Real isolateScale=(m_isolate && pickIndex>=0)? 0.2: 1.0;

		sp<SurfaceAccessibleI::FilterI> spFilter;

		for(I32 objectIndex=0;objectIndex<objectCount;objectIndex++)
		{
			if(objectIndex==pickIndex)
			{
				continue;
			}

			if(m_fragmented)
			{
				const String fragmentName=
						m_spInputPoint->fragment(objectIndex);

				if(!m_spInputPoint->filterWith(fragmentName,spFilter)
						|| spFilter.isNull())
				{
					continue;
				}

				const I32 filterCount=spFilter->filterCount();
				if(!filterCount)
				{
					continue;
				}

				const I32 rootIndex=spFilter->filter(0);
				const SpatialVector rootPoint=
						m_spInputPoint->spatialVector(rootIndex);

				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 reIndex=spFilter->filter(filterIndex);

					SpatialVector point=
							m_spInputPoint->spatialVector(reIndex);

					point=rootPoint+isolateScale*(point-rootPoint);

					m_spOutputPoint->set(reIndex,point);
				}

				continue;
			}

			const U32 subCount=m_spInputVertices->subCount(objectIndex);
			if(subCount<2)
			{
				continue;
			}

			const SpatialVector rootPoint=
					m_spInputVertices->spatialVector(objectIndex,0);

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				SpatialVector point=m_spInputVertices->spatialVector(
						objectIndex,subIndex);

				point=rootPoint+isolateScale*(point-rootPoint);

				m_spOutputVertices->set(objectIndex,subIndex,point);
			}
		}
		m_isolating=toIsolate;
	}

	const BWORD hasPicked=(pickIndex>=0 && pickIndex<objectCount);
	BWORD refreshPicked=hasPicked;

	Record before;
	Record after;
	if(hasPicked)
	{
		findKeys(m_pickId,m_frame,before,after);

		if(paramChanged)
		{
			if(before.isValid())
			{
				m_aFrame(before)=catalog<Real>("beforeFrame");
				m_aLeanX(before)=catalog<Real>("beforeLeanX");
				m_aLeanZ(before)=catalog<Real>("beforeLeanZ");
				m_aArcX(before)=catalog<Real>("beforeArcX");
				m_aArcZ(before)=catalog<Real>("beforeArcZ");
				m_aSpinY(before)=catalog<Real>("beforeSpinY");
				m_aTwistY(before)=catalog<Real>("beforeTwistY");
			}
			if(after.isValid())
			{
				m_aFrame(after)=catalog<Real>("afterFrame");
				m_aLeanX(after)=catalog<Real>("afterLeanX");
				m_aLeanZ(after)=catalog<Real>("afterLeanZ");
				m_aArcX(after)=catalog<Real>("afterArcX");
				m_aArcZ(after)=catalog<Real>("afterArcZ");
				m_aSpinY(after)=catalog<Real>("afterSpinY");
				m_aTwistY(after)=catalog<Real>("afterTwistY");
			}
		}
		else
		{
			if(before.isValid())
			{
				catalog<Real>("beforeFrame")=m_aFrame(before);
				catalog<Real>("beforeLeanX")=m_aLeanX(before);
				catalog<Real>("beforeLeanZ")=m_aLeanZ(before);
				catalog<Real>("beforeArcX")=m_aArcX(before);
				catalog<Real>("beforeArcZ")=m_aArcZ(before);
				catalog<Real>("beforeSpinY")=m_aSpinY(before);
				catalog<Real>("beforeTwistY")=m_aTwistY(before);
			}
			if(after.isValid())
			{
				catalog<Real>("afterFrame")=m_aFrame(after);
				catalog<Real>("afterLeanX")=m_aLeanX(after);
				catalog<Real>("afterLeanZ")=m_aLeanZ(after);
				catalog<Real>("afterArcX")=m_aArcX(after);
				catalog<Real>("afterArcZ")=m_aArcZ(after);
				catalog<Real>("afterSpinY")=m_aSpinY(after);
				catalog<Real>("afterTwistY")=m_aTwistY(after);
			}
		}
	}

	if(!before.isValid())
	{
		catalog<Real>("beforeFrame")=0.0;
		catalog<Real>("beforeLeanX")=0.0;
		catalog<Real>("beforeLeanZ")=0.0;
		catalog<Real>("beforeArcX")=0.0;
		catalog<Real>("beforeArcZ")=0.0;
		catalog<Real>("beforeSpinY")=0.0;
		catalog<Real>("beforeTwistY")=0.0;
	}
	if(!after.isValid())
	{
		catalog<Real>("afterFrame")=0.0;
		catalog<Real>("afterLeanX")=0.0;
		catalog<Real>("afterLeanZ")=0.0;
		catalog<Real>("afterArcX")=0.0;
		catalog<Real>("afterArcZ")=0.0;
		catalog<Real>("afterSpinY")=0.0;
		catalog<Real>("afterTwistY")=0.0;
	}

	sp<RecordArray> spRA=m_spState->getArray("Channel");
	if(spRA.isValid())
	{
		sp<SurfaceAccessibleI::FilterI> spFilter;

		for(int i=0;i<spRA->length();i++)
		{
			const I32 primId=m_aIndex(spRA,i);
			if(primId==m_pickId)
			{
				refreshPicked=FALSE;
			}
			else if(m_isolate || (!rebuild && !refreshIsolation))
			{
				//* recycled data should be valid
				continue;
			}

			const I32 objectIndex=
					m_fragmented? primId: lookupIndex(primId);

			if(objectIndex<0 || objectIndex>=objectCount)
			{
				feLog("BenderOp::handle bad objectIndex %d/%d id %d\n",
						objectIndex,objectCount,primId);
				if(primId<0)
				{
					removeChannel(primId);
					i--;
				}
				continue;
			}

			if(m_spChangeGroup.isValid())
			{
				m_spChangeGroup->append(objectIndex);
			}

			Record channel=spRA->getRecord(i);
			Record key=evalAt(channel,m_frame);

			if(!spRA->length())
			{
				//* primitive exists, but no keys
				removeChannel(primId);
				i--;
				continue;
			}

			I32 elementCount=0;

			if(m_fragmented)
			{
				const String fragmentName=
						m_spInputPoint->fragment(objectIndex);

				if(!m_spInputPoint->filterWith(fragmentName,spFilter)
						|| spFilter.isNull())
				{
					continue;
				}

				elementCount=spFilter->filterCount();
			}
			else
			{
				elementCount=m_spInputVertices->subCount(objectIndex);
			}

			if(!elementCount)
			{
				continue;
			}

			SpatialTransform parent;
			SpatialTransform root;
			SpatialTransform end;
#if FE_CODEGEN<=FE_DEBUG
			BWORD success=
#endif
					calculateExtent(TRUE,objectIndex,parent,root,end);
			FEASSERT(success);

			SpatialTransform base;
			SpatialTransform diff;
			calculateTransforms(key,root,end,base,diff);

			const Real length=
					magnitude(end.translation()-root.translation());

			SpatialTransform invParent;
			invert(invParent,parent);

			SpatialTransform invRoot;
			invert(invRoot,root);

			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				const I32 reIndex=m_fragmented? spFilter->filter(elementIndex):
						m_spInputVertices->integer(objectIndex,elementIndex);

				const SpatialVector point=
						m_spInputPoint->spatialVector(reIndex);

				SpatialVector parented;
				transformVector(invParent,point,parented);

				SpatialVector local;
				transformVector(invRoot,parented,local);

				SpatialTransform between;
				interpolate(between,base,diff,local[1]/(length+1e-6));
				local[1]=0.0;

				SpatialVector transformed;
				transformVector(between,local,transformed);

				SpatialVector unparented;
				transformVector(parent,transformed,unparented);

				m_spOutputPoint->set(reIndex,unparented);
			}
		}
	}

	if(refreshPicked)
	{
		if(m_fragmented)
		{
			sp<SurfaceAccessibleI::FilterI> spFilter;

			const String fragmentName=
					m_spInputPoint->fragment(pickIndex);

			if(m_spInputPoint->filterWith(fragmentName,spFilter)
					&& spFilter.isValid())
			{
				const I32 filterCount=spFilter->filterCount();
				for(I32 filterindex=0;filterindex<filterCount;filterindex++)
				{
					const I32 reIndex=spFilter->filter(filterindex);

					const SpatialVector point=
							m_spInputPoint->spatialVector(reIndex);

					m_spOutputPoint->set(reIndex,point);
				}
			}
		}
		else
		{
			const U32 subCount=m_spInputVertices->subCount(pickIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const SpatialVector point=m_spInputVertices->spatialVector(
						pickIndex,subIndex);

				m_spOutputVertices->set(pickIndex,subIndex,point);
			}
		}
	}

	sp<SurfaceAccessorI> spOutputTransient;
	if(access(spOutputTransient,spOutputAccessible,
			e_detail,"transient",e_warning,e_createMissing))
	{
		BWORD alreadyTransient=FALSE;

		sp<SurfaceAccessorI> spInputTransient;
		if(access(spInputTransient,spInputAccessible,
				e_detail,"transient",e_quiet))
		{
			alreadyTransient=spInputTransient->integer(0);
		}

		if(!alreadyTransient)
		{
			spOutputTransient->set(0,I32(m_transient));
		}
	}

	return;
}

void BenderOp::drawOverlayKey(sp<DrawI>& a_rspDrawI,
	I32 a_px,I32 a_py,Real a_frame,const Color& a_rColor)
{
	SpatialVector point(a_px,a_py,1.0);
	a_rspDrawI->drawPoints(&point,NULL,1,FALSE,&a_rColor);

	String text;
	text.sPrintf("%.6G",a_frame);

	const I32 px1=a_px-(a_frame>=10.0? (a_frame>=100.0? 8: 5): 3);

	set(point,px1,a_py+5,1.0);
	a_rspDrawI->drawAlignedText(point,text,a_rColor);
}

BWORD BenderOp::calculateExtent(BWORD a_input,
	U32 a_objectIndex,SpatialTransform& a_rParent,
	SpatialTransform& a_rRoot,SpatialTransform& a_rEnd)
{
	SpatialVector rootPoint;
	SpatialVector endPoint;

	if(m_fragmented)
	{
		sp<SurfaceAccessorI>& rspPoint=
				a_input? m_spInputPoint: m_spOutputPoint;

		const String fragmentName=
				m_spInputPoint->fragment(a_objectIndex);

		sp<SurfaceAccessibleI::FilterI> spFilter;
		if(!m_spInputPoint->filterWith(fragmentName,spFilter)
				|| spFilter.isNull())
		{
			return FALSE;
		}

		const I32 filterCount=spFilter->filterCount();
		if(!filterCount)
		{
			return FALSE;
		}

		const I32 firstIndex=spFilter->filter(0);

		rootPoint=rspPoint->spatialVector(firstIndex);

		Real maxDist2=0.0;
		endPoint=rootPoint;

		for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 reIndex=spFilter->filter(filterIndex);

			const SpatialVector point=rspPoint->spatialVector(reIndex);

			const Real dist2=magnitudeSquared(point-rootPoint);
			if(maxDist2<dist2)
			{
				maxDist2=dist2;
				endPoint=point;
			}
		}
	}
	else
	{
		sp<SurfaceAccessorI>& rspVertices=
				a_input? m_spInputVertices: m_spOutputVertices;

		const U32 subCount=rspVertices->subCount(a_objectIndex);
		if(!subCount)
		{
			return FALSE;
		}

		rootPoint=rspVertices->spatialVector(a_objectIndex,0);

		Real maxDist2=0.0;
		endPoint=rootPoint;

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const SpatialVector point=rspVertices->spatialVector(
					a_objectIndex,subIndex);

			const Real dist2=magnitudeSquared(point-rootPoint);
			if(maxDist2<dist2)
			{
				maxDist2=dist2;
				endPoint=point;
			}
		}
	}

	if(m_spDriver.isValid())
	{
		sp<SurfaceI::ImpactI> spImpact=m_spDriver->nearestPoint(rootPoint);
		if(spImpact.isValid())
		{
			const I32 triangleIndex=spImpact->triangleIndex();
			const SpatialBary bary=spImpact->barycenter();

			a_rParent=m_spDriver->sample(triangleIndex,bary);

			SpatialTransform invParent;
			invert(invParent,a_rParent);

			transformVector(invParent,rootPoint,rootPoint);
			transformVector(invParent,endPoint,endPoint);
		}
		else
		{
			setIdentity(a_rParent);
		}
	}
	else
	{
		setIdentity(a_rParent);
	}

	const SpatialVector from(0.0,1.0,0.0);
	const SpatialVector to=unitSafe(endPoint-rootPoint);

	set(a_rRoot,from,to);
	a_rEnd=a_rRoot;

	setTranslation(a_rRoot,rootPoint);
	setTranslation(a_rEnd,endPoint);

	return TRUE;
}

void BenderOp::calculateTransforms(Record key,
	const SpatialTransform& a_rRoot,
	const SpatialTransform& a_rEnd,
	SpatialTransform& a_rBase,SpatialTransform& a_rDiff)
{
	if(!key.isValid())
	{
		feLog("BenderOp::calculateTransforms invalid key\n");
		setIdentity(a_rBase);
		setIdentity(a_rDiff);
		return;
	}

	const SpatialVector zero(0.0,0.0,0.0);
	const SpatialVector xAxis(1.0,0.0,0.0);
	const SpatialVector yAxis(0.0,1.0,0.0);

	Real& rLeanX=m_aLeanX(key);
	Real& rLeanZ=m_aLeanZ(key);
	Real& rArcX=m_aArcX(key);
	Real& rArcZ=m_aArcZ(key);
	Real& rSpinY=m_aSpinY(key);
	Real& rTwistY=m_aTwistY(key);

	const Real arc=fe::minimum(Real(0.48*fe::pi),
			Real(sqrt(rArcX*rArcX+rArcZ*rArcZ)));
	const SpatialVector arcAxis=unitSafe(SpatialVector(rArcX,0.0,-rArcZ));
	SpatialQuaternion arcRot(arc,arcAxis);

	const Real maxAngle=0.98*fe::pi-arc;
	Real lean=sqrt(rLeanX*rLeanX+rLeanZ*rLeanZ);
//	feLog("arc %.6G (%.6G) lean %.6G/%.6G (%.6G)\n",
//			arc,arc/degToRad,lean,maxAngle,lean/degToRad);
	if(lean>maxAngle)
	{
		const Real rescale=maxAngle/lean;
		rLeanX*=rescale;
		rLeanZ*=rescale;
		lean=maxAngle;
	}

	const SpatialVector leanAxis=unitSafe(SpatialVector(rLeanX,0.0,-rLeanZ));
	SpatialQuaternion leanRot(lean,leanAxis);

	const Real length=magnitude(a_rEnd.translation()-a_rRoot.translation());

	SpatialVector target;
	rotateVector(leanRot,length*yAxis,target);

	SpatialQuaternion spinRot(-rSpinY,yAxis);
	SpatialQuaternion twistRot(-rTwistY,yAxis);

	SpatialQuaternion baseBend=leanRot*arcRot*spinRot;
	SpatialVector baseTan;
	rotateVector(baseBend,xAxis,baseTan);
	SpatialVector baseDir;
	rotateVector(baseBend,yAxis,baseDir);

	SpatialQuaternion tipBend=leanRot*inverse(arcRot)*spinRot*twistRot;
	SpatialVector tipTan;
	rotateVector(tipBend,xAxis,tipTan);
	SpatialVector tipDir;
	rotateVector(tipBend,yAxis,tipDir);

	makeFrameNormalY(a_rBase,zero,baseTan,baseDir);

	SpatialTransform tip;
	makeFrameNormalY(tip,target,tipTan,tipDir);

	a_rBase=a_rBase*a_rRoot;
	tip=tip*a_rRoot;

	SpatialTransform invBase;
	invert(invBase,a_rBase);

	const SpatialTransform diff=invBase*tip;

#if FALSE
	//* HACK shrink with curvature
	if(!m_fastPower)
	{
		m_matrixPower.solve(a_rDiff,diff,cos(0.5*lean));
	}
	else
	{
		SpatialTransform partial;
		interpolate(partial,a_rBase,diff,cos(0.5*lean));
		a_rDiff=invBase*partial;
	}
#else
	a_rDiff=diff;
#endif

//	feLog("base\n%s\n",c_print(a_rBase));
//	feLog("tip\n%s\n",c_print(tip));
//	feLog("diff\n%s\n",c_print(diff));
}

void BenderOp::interpolate(SpatialTransform& a_rPartial,
	const SpatialTransform& a_rBase,
	const SpatialTransform& a_rDiff, Real a_power)
{
	if(!m_fastPower)
	{
		SpatialTransform partialDiff;
		m_matrixPower.solve(partialDiff,a_rDiff,a_power);
		a_rPartial=a_rBase*partialDiff;
		return;
	}

	//* NOTE order of matrix operator may be backwards
	//* TODO compare to CurveCreateOp::interpolateTransform

	const SpatialVector rootPoint=a_rBase.translation();
	const SpatialTransform tip=a_rBase*a_rDiff;

	SpatialTransform localBase=a_rBase;
	SpatialTransform localTip=tip;
	localBase.translation()-=rootPoint;
	localTip.translation()-=rootPoint;

	const Real power1=1.0-a_power;
	const Real distance=magnitude(localTip.translation());

	const Real arm=0.01;

	const Real yStart=distance*a_power;
	const SpatialVector locStart(0.0,yStart,0.0);
	const SpatialVector xArmStart(arm,yStart,0.0);
	const SpatialVector yArmStart(0.0,yStart+arm,0.0);

	const Real yFull= -distance*power1;
	const SpatialVector locFull(0.0,yFull,0.0);
	const SpatialVector xArmFull(arm,yFull,0.0);
	const SpatialVector yArmFull(0.0,yFull+arm,0.0);

	SpatialVector locBase;
	SpatialVector xArmBase;
	SpatialVector yArmBase;

	transformVector(localBase,locStart,locBase);
	transformVector(localBase,xArmStart,xArmBase);
	transformVector(localBase,yArmStart,yArmBase);

	SpatialVector locTip;
	SpatialVector xArmTip;
	SpatialVector yArmTip;

	transformVector(localTip,locFull,locTip);
	transformVector(localTip,xArmFull,xArmTip);
	transformVector(localTip,yArmFull,yArmTip);

	const SpatialVector locBlend=locBase*power1+locTip*a_power;
	const SpatialVector xArmBlend=xArmBase*power1+xArmTip*a_power;
	const SpatialVector yArmBlend=yArmBase*power1+yArmTip*a_power;

	const SpatialVector locLinear=
			localBase.translation()*power1+localTip.translation()*a_power;
	const SpatialVector locMix=
			locLinear*m_linearity+locBlend*(1.0-m_linearity);

	makeFrameNormalY(a_rPartial,locMix,
			unitSafe(xArmBlend-locBlend),unitSafe(yArmBlend-locBlend));

	a_rPartial.translation()+=rootPoint;
}

void BenderOp::setupState(void)
{
	m_aFrame.populate(m_spScope,"Key","Frame");
	m_aLeanX.populate(m_spScope,"Key","LeanX");
	m_aLeanZ.populate(m_spScope,"Key","LeanZ");
	m_aArcX.populate(m_spScope,"Key","ArcX");
	m_aArcZ.populate(m_spScope,"Key","ArcZ");
	m_aSpinY.populate(m_spScope,"Key","SpinY");
	m_aTwistY.populate(m_spScope,"Key","TwistY");

	m_aIndex.populate(m_spScope,"Channel","Index");
	m_aName.populate(m_spScope,"Channel","Name");
	m_aKeys.populate(m_spScope,"Channel","Keys");
}

BWORD BenderOp::loadState(const String& rBuffer)
{
	if(OperatorState::loadState(rBuffer))
	{
		m_spChannelMap=m_spState->lookupMap(m_aIndex);

		return TRUE;
	}

	return FALSE;
}

void BenderOp::syncIds(void)
{
	if(!m_fragmented)
	{
		return;
	}

	sp<RecordArray> spRA=m_spState->getArray("Channel");
	if(spRA.isNull())
	{
		return;
	}

	sp<SurfaceAccessibleI::FilterI> spFilter;

	for(int i=0;i<spRA->length();i++)
	{
		const String fragmentName=m_aName(spRA,i);
		if(fragmentName.empty())
		{
			const I32 fragmentIndex=m_aIndex(spRA,i);
			m_aName(spRA,i)=m_spInputPoint->fragment(fragmentIndex);

//			feLog("sync %d -> \"%s\"\n",
//					fragmentIndex,m_aName(spRA,i).c_str());
		}
		else
		{
//			feLog("sync \"%s\" %d -> %d\n",
//					fragmentName.c_str(),
//					m_aIndex(spRA,i),
//					m_indexOfFragment[fragmentName]);

			m_aIndex(spRA,i)=m_indexOfFragment[fragmentName];
		}
	}
}

BWORD BenderOp::undo(String a_label,sp<Counted> a_spCounted)
{
	if(!OperatorState::undo(a_label,a_spCounted))
	{
		return FALSE;
	}

	m_key=Record();
	m_spChannelMap=m_spState->lookupMap(m_aIndex);

	dirty(FALSE);
	return TRUE;
}

BWORD BenderOp::redo(String a_label,sp<Counted> a_spCounted)
{
	if(!OperatorState::redo(a_label,a_spCounted))
	{
		return FALSE;
	}

	m_key=Record();
	m_spChannelMap=m_spState->lookupMap(m_aIndex);

	dirty(FALSE);
	return TRUE;
}

Record BenderOp::removeChannel(I32 a_channelIndex)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	return removeChannel(channel);
}

Record BenderOp::removeChannel(Record a_channel)
{
	if(a_channel.isValid())
	{
#if FE_BDO_KEY_DEBUG
		feLog("remove channel %d\n",m_aIndex(a_channel));
#endif

		m_spState->remove(a_channel);
		return a_channel;
	}

	return Record();
}

I32 BenderOp::keyCount(I32 a_channelIndex)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
		return 0;
	}

	return keyCount(channel);
}

I32 BenderOp::keyCount(Record a_channel)
{
	sp<RecordArray> spRA=m_aKeys(a_channel);
	return spRA.isValid()? spRA->length(): 0;
}

Record BenderOp::indexedKey(I32 a_channelIndex,I32 a_keyIndex)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
		return Record();
	}

	return indexedKey(channel,a_keyIndex);
}

Record BenderOp::indexedKey(Record a_channel,I32 a_keyIndex)
{
	sp<RecordArray> spRA=m_aKeys(a_channel);
	return spRA.isValid()? spRA->getRecord(a_keyIndex): Record();
}

Record BenderOp::keyAt(I32 a_channelIndex,Real a_frame)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
#if FE_BDO_KEY_DEBUG
		feLog("create channel %d\n",a_channelIndex);
#endif

		channel=m_spScope->createRecord("Channel");
		m_aIndex(channel)=a_channelIndex;

		if(m_fragmented)
		{
			m_aName(channel)=m_spInputPoint->fragment(a_channelIndex);
		}

		m_spState->add(channel);
	}

	return keyAt(channel,a_frame);
}

Record BenderOp::keyAt(Record a_channel,Real a_frame)
{
	const Real threshold=1e-3;

	Record key;

	sp<RecordArray> spRA=m_aKeys(a_channel);
	if(!spRA.isValid())
	{
		spRA=m_spScope->createRecordArray("Key",0);
		m_aKeys(a_channel)=spRA;
	}

	if(!m_aFrame.check(spRA))
	{
		feLog("BenderOp::keyAt channel's RecordArray has no frame data\n");
		return Record();
	}

	for(int i=0;i<spRA->length();i++)
	{
		if(fabs(m_aFrame(spRA,i)-a_frame)<threshold)
		{
			key=spRA->getRecord(i);
			break;
		}
	}

	if(!key.isValid())
	{
		key=evalAt(a_channel,a_frame);

		if(!key.isValid())
		{
#if FE_BDO_KEY_DEBUG
			feLog("create key %d %.6G\n",m_aIndex(a_channel),a_frame);
#endif

			key=m_spScope->createRecord("Key");
			m_aFrame(key)=a_frame;
			m_aLeanX(key)=0.0;
			m_aLeanZ(key)=0.0;
			m_aArcX(key)=0.0;
			m_aArcZ(key)=0.0;
			m_aSpinY(key)=0.0;
			m_aTwistY(key)=0.0;
		}

#if FE_BDO_KEY_DEBUG
		feLog("add key %d %.6G\n",m_aIndex(a_channel),a_frame);
#endif

		spRA->add(key);
	}

	return key;
}

Record BenderOp::removeKey(I32 a_channelIndex,Real a_frame)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
		return Record();
	}

	return removeKey(channel,a_frame);
}

Record BenderOp::removeKey(Record a_channel,Real a_frame)
{
	const Real threshold=1e-3;

	sp<RecordArray> spRA=m_aKeys(a_channel);
	if(!spRA.isValid())
	{
		return Record();
	}

	if(!m_aFrame.check(spRA))
	{
		feLog("BenderOp::removeKey channel's RecordArray has no frame data\n");
		return Record();
	}

	Record key;

	for(int i=0;i<spRA->length();i++)
	{
		if(fabs(m_aFrame(spRA,i)-a_frame)<threshold)
		{
#if FE_BDO_KEY_DEBUG
			feLog("remove key %d %.6G\n",m_aIndex(a_channel),a_frame);
#endif

			key=spRA->getRecord(i);
			spRA->remove(key);
			break;
		}
	}

	if(!spRA->length())
	{
		removeChannel(a_channel);
	}

	return key;
}

void BenderOp::findKeys(I32 a_channelIndex,Real a_frame,
	Record& a_rBefore,Record& a_rAfter)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
		return;
	}

	findKeys(channel,a_frame,a_rBefore,a_rAfter);
}

void BenderOp::findKeys(Record a_channel,Real a_frame,
	Record& a_rBefore,Record& a_rAfter)
{
	sp<RecordArray> spRA=m_aKeys(a_channel);
	if(!spRA.isValid())
	{
		feLog("BenderOp::findKeys channel has no records\n");
		return;
	}

	if(!m_aFrame.check(spRA))
	{
		feLog("BenderOp::findKeys channel's RecordArray has no frame data\n");
		return;
	}

	Real closestBefore= -1.0;
	Real closestAfter= -1.0;

	for(int i=0;i<spRA->length();i++)
	{
		const Real otherFrame=m_aFrame(spRA,i);
		if(otherFrame<=a_frame)
		{
			if(closestBefore<0.0 || closestBefore<otherFrame)
			{
				closestBefore=otherFrame;
				a_rBefore=spRA->getRecord(i);
			}
		}
		else
		{
			if(closestAfter<0.0 || closestAfter>otherFrame)
			{
				closestAfter=otherFrame;
				a_rAfter=spRA->getRecord(i);
			}
		}
	}
}

void BenderOp::findKeys(I32 a_channelIndex,Real a_frame,
	Record& a_rBefore,Record& a_rAfter,
	Record& a_rTwoBefore,Record& a_rTwoAfter)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
		return;
	}

	findKeys(channel,a_frame,a_rBefore,a_rAfter,a_rTwoBefore,a_rTwoAfter);
}

void BenderOp::findKeys(Record a_channel,Real a_frame,
	Record& a_rBefore,Record& a_rAfter,
	Record& a_rTwoBefore,Record& a_rTwoAfter)
{
	sp<RecordArray> spRA=m_aKeys(a_channel);
	if(!spRA.isValid())
	{
		return;
	}

	if(!m_aFrame.check(spRA))
	{
		feLog("BenderOp::findKeys channel's RecordArray has no frame data\n");
		return;
	}

	findKeys(a_channel,a_frame,a_rBefore,a_rAfter);

	const Real beforeFrame=a_rBefore.isValid()? m_aFrame(a_rBefore): -1.0;
	const Real afterFrame=a_rAfter.isValid()? m_aFrame(a_rAfter): -1.0;

	Real closestTwoBefore= -1.0;
	Real closestTwoAfter= -1.0;

	for(int i=0;i<spRA->length();i++)
	{
		const Real otherFrame=m_aFrame(spRA,i);
		if(otherFrame<beforeFrame)
		{
			if(closestTwoBefore<0.0 || closestTwoBefore<otherFrame)
			{
				closestTwoBefore=otherFrame;
				a_rTwoBefore=spRA->getRecord(i);
			}
		}
		else if(otherFrame>afterFrame)
		{
			if(closestTwoAfter<0.0 || closestTwoAfter>otherFrame)
			{
				closestTwoAfter=otherFrame;
				a_rTwoAfter=spRA->getRecord(i);
			}
		}
	}
}

Record BenderOp::evalAt(I32 a_channelIndex,Real a_frame)
{
	Record channel=m_spChannelMap->lookup(a_channelIndex);

	if(!channel.isValid())
	{
		return Record();
	}

	return evalAt(channel,a_frame);
}

Record BenderOp::evalAt(Record a_channel,Real a_frame)
{
	Record before;
	Record after;
	Record twoBefore;
	Record twoAfter;
	findKeys(a_channel,a_frame,before,after,twoBefore,twoAfter);

	const Real closestBefore=before.isValid()? m_aFrame(before): -1.0;
	const Real closestAfter=after.isValid()? m_aFrame(after): -1.0;

	if(!twoBefore.isValid())
	{
		twoBefore=before;
	}
	if(!twoAfter.isValid())
	{
		twoAfter=after;
	}

	if(!before.isValid())
	{
		if(!after.isValid())
		{
#if FE_BDO_KEY_DEBUG
			feLog("no keys %d\n",m_aIndex(a_channel));
#endif
			return Record();
		}
#if FE_BDO_KEY_DEBUG
		feLog("after key %d %.6G\n",m_aIndex(a_channel),closestAfter);
#endif
		Record key=after.clone();
		m_aFrame(key)=a_frame;
		return key;
	}
	if(!after.isValid())
	{
#if FE_BDO_KEY_DEBUG
		feLog("before key %d %.6G\n",m_aIndex(a_channel),closestBefore);
#endif
		Record key=before.clone();
		m_aFrame(key)=a_frame;
		return key;
	}

	const Real ratio=(a_frame-closestBefore)/(closestAfter-closestBefore);

#if FE_BDO_KEY_DEBUG
	feLog("temp key %d %.6G between %.6G %.6G ratio %.6G\n",
			m_aIndex(a_channel),a_frame,closestBefore,closestAfter,ratio);
#endif

	//* temporary tween key
	Record key=before.clone();
	m_aFrame(key)=a_frame;

#if FALSE
	//* linear
	const Real ratio1=1.0-ratio;
	m_aLeanX(key)=m_aLeanX(before)*ratio1+m_aLeanX(after)*ratio;
	m_aLeanZ(key)=m_aLeanZ(before)*ratio1+m_aLeanZ(after)*ratio;
	m_aArcX(key)=m_aArcX(before)*ratio1+m_aArcX(after)*ratio;
	m_aArcZ(key)=m_aArcZ(before)*ratio1+m_aArcZ(after)*ratio;
	m_aSpinY(key)=m_aSpinY(before)*ratio1+m_aSpinY(after)*ratio;
	m_aTwistY(key)=m_aTwistY(before)*ratio1+m_aTwistY(after)*ratio;
#else
	//* spline
	Vector<6,Real> state0(m_aLeanX(twoBefore),m_aLeanZ(twoBefore),
			m_aArcX(twoBefore),m_aArcZ(twoBefore),
			m_aSpinY(twoBefore),m_aTwistY(twoBefore));
	Vector<6,Real> state1(m_aLeanX(before),m_aLeanZ(before),
			m_aArcX(before),m_aArcZ(before),
			m_aSpinY(before),m_aTwistY(before));
	Vector<6,Real> state2(m_aLeanX(after),m_aLeanZ(after),
			m_aArcX(after),m_aArcZ(after),
			m_aSpinY(after),m_aTwistY(after));
	Vector<6,Real> state3(m_aLeanX(twoAfter),m_aLeanZ(twoAfter),
			m_aArcX(twoAfter),m_aArcZ(twoAfter),
			m_aSpinY(twoAfter),m_aTwistY(twoAfter));

	Vector<6,Real> between=Spline< Vector<6,Real>, Real >::Cardinal2D(ratio,
			state0,state1,state2,state3);

	m_aLeanX(key)=between[0];
	m_aLeanZ(key)=between[1];
	m_aArcX(key)=between[2];
	m_aArcZ(key)=between[3];
	m_aSpinY(key)=between[4];
	m_aTwistY(key)=between[5];

//	feLog("ratio %.6G\n",ratio);
//	feLog("state0 %s\n",c_print(state0));
//	feLog("state1 %s\n",c_print(state1));
//	feLog("state2 %s\n",c_print(state2));
//	feLog("state3 %s\n",c_print(state3));
//	feLog("between %s\n",c_print(between));
#endif

	return key;
}
