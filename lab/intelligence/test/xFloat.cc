/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "datatool/datatool.h"
#include "intelligence/intelligence.h"
#include "terrain/terrain.h"
#include "viewer/viewer.h"

#define XFLOAT_VIDEO	1	//* 1=graphics 2=console 3=null
#define XFLOAT_RAY		0	//* 1=use ray tracer

#define XFLOAT_RECORDER	(FE_CODEGEN<=FE_DEBUG)
#define XFLOAT_DEBUGGER	(FE_CODEGEN<=FE_DEBUG && XFLOAT_VIDEO!=3)

using namespace fe;
using namespace fe::ext;

class MyDraw: public HandlerI
{
	public:
				MyDraw(void)												{}
virtual			~MyDraw(void);

virtual void	handle(Record &record);

		void	bind(sp<QuickViewerI> spQuickViewerI)
				{	m_spQuickViewerI=spQuickViewerI; }
		void	save(String filename);
	private:
		void	setup(sp<Scope> spViewerScope);

		std::vector< sp<HandlerI> >		m_modifierArray;
		sp<SignalerI>					m_spModifierChain;
		sp<RecordGroup>					m_spSurfaceGroup;
		sp<StrataI>						m_spStrataI;
		sp<StratumDrawI>				m_spStratumDrawI;
		sp<QuickViewerI>				m_spQuickViewerI;
		AsViewer						m_asViewer;
		Theater							m_theaterRV;
		ControlCenter					m_controlCenterRV;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result;
		result=spRegistry->manage("fexTerrainDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage("fexIntelligenceDL");
		UNIT_TEST(successful(result));

#if FALSE
		String& draw_hint=spMaster->catalog()->catalog<String>("hint:DrawI");
		draw_hint="*.DrawOpenGL";
#endif

#if XFLOAT_RAY
		String& draw_hint=spMaster->catalog()->catalog<String>("hint:DrawI");
		draw_hint="*.DrawRayTrace";
		result=spRegistry->manage("fexRayDL");
		UNIT_TEST(successful(result));
#endif

#if XFLOAT_VIDEO==1
		if(XFLOAT_DEBUGGER)
		{
			result=spRegistry->manage("fexDatauiDL");
		}
		else
		{
			result=spRegistry->manage("fexViewerDL");
		}
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexSDLDL");
#else
		result=spRegistry->manage("fexConsoleDL");
		UNIT_TEST(successful(result));
#endif

#if XFLOAT_VIDEO==3
		sp<QuickViewerI> spQuickViewerI(spRegistry->create("*.NullViewer"));
#else
		sp<QuickViewerI> spQuickViewerI(spRegistry->create("QuickViewerI"));
#endif

#if XFLOAT_DEBUGGER
		sp<HandlerI> spDebugWindow(spRegistry->create("*.DebugWindow"));
		spQuickViewerI->insertBeatHandler(spDebugWindow);
#endif

		feLog("Using %s\n",spQuickViewerI->name().c_str());

		if(!spQuickViewerI.isValid())
		{
			feX(e_cannotCreate,argv[0], "couldn't create components");
		}

		sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
		if(!spDrawI.isValid())
		{
			feX(e_notInitialized,argv[0], "viewer has NULL spDrawI");
		}
		//* change default line width
		sp<DrawMode> spDrawMode=spDrawI->drawMode();
		if(spDrawMode.isValid())
		{
			spDrawI->drawMode()->setLineWidth(1.5f);
		}
		if(spDrawI->isDirect())
		{
			//* Local audio only
			spRegistry->manage("fexOpenALDL");
		}

		sp<MyDraw> spMyDraw(Library::create<MyDraw>("MyDraw"));
		spMyDraw->bind(spQuickViewerI);

		sp<CameraControllerI> spCameraControllerI=
				spQuickViewerI->getCameraControllerI();
		if(spCameraControllerI.isValid())
		{
			spCameraControllerI->setKeyMax(WindowEvent::e_keyCursorLeft,10);
			spCameraControllerI->setKeyMax(WindowEvent::e_keyCursorRight,10);
//			spCameraControllerI->setKeyCount('p',1);
		}

		spQuickViewerI->insertDrawHandler(spMyDraw);
		spQuickViewerI->run(frames);

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5 + (XFLOAT_RAY==1));
	UNIT_RETURN();
}

void MyDraw::save(String filename)
{
	//* dump to file
	std::ofstream outfile(filename.c_str());
	if(!outfile)
	{
		feLog("MyDraw::bind could not open test output file\n");
	}
	else
	{
		sp<RecordGroup> spOutputGroup(new RecordGroup());
		spOutputGroup->add(m_theaterRV.record());

		sp<data::StreamI> spStream(new data::AsciiStream(m_theaterRV.scope()));
		spStream->output(outfile, spOutputGroup);
		outfile.close();
	}
}

void MyDraw::setup(sp<Scope> spViewerScope)
{
	if(m_spModifierChain.isValid())
		return;

	setName("MyDraw");

	FEASSERT(spViewerScope.isValid());
	m_asViewer.bind(spViewerScope);

	sp<Registry> spRegistry=spViewerScope->registry();
	sp<Scope> spSimScope=spRegistry->master()->catalog()->catalogComponent(
			"Scope","SimScope");

#if TRUE
	Peeker peeker;
	spSimScope->peek(peeker);
	feLog("\nSim Scope (before use):%s\nDone\n\n",peeker.output().c_str());
#endif

	m_spModifierChain=spRegistry->create("SignalerI");

#if TRUE
	feLog("lake load\n");
	String mediaPath=
			spRegistry->master()->catalog()->catalog<String>("path:media");
	m_spSurfaceGroup=RecordView::loadRecordGroup(spSimScope,
			mediaPath+"/terrain/lake.rg");
	feLog("lake done\n");
	FEASSERT(m_spSurfaceGroup.isValid());

	RecordArrayView<Strata> strataRAV;
	strataRAV.bind(spSimScope);

	for(RecordGroup::iterator it=m_spSurfaceGroup->begin();
			it!=m_spSurfaceGroup->end() && !m_spStrataI.isValid();it++)
	{
		sp<RecordArray> spRA= *it;
		strataRAV.bind(spRA);

		if(!strataRAV.recordView().strataComponent.check(spRA))
		{
			continue;
		}

		for(Strata& strataRV: strataRAV)
		{
			m_spStrataI=strataRV.strataComponent();

			// HACK update cache
			Recordable recordableRV;
			recordableRV.bind(strataRV.record());
			cp<Component> cpComponent=recordableRV.component();
			cpComponent.protect();
			cpComponent.writable();
			break;
		}
	}
	m_spStratumDrawI=spRegistry->create("StratumDrawI");
#else
	m_spStrataI=spRegistry->create("*.Habitat");
	m_spStratumDrawI=spRegistry->create("StratumDrawI");

	sp<StratumI> spGround=spRegistry->create("*.StratumBowl");
	sp<StratumI> spWater=spRegistry->create("*.StratumWave");
	sp<StratumI> spAir=spRegistry->create("*.StratumPlane");

	spGround->setPhase(StratumI::e_solid);
	spGround->setDensity(1000.0f);
	spGround->setOffset(-2.0f);
	spGround->setScale(0.02f);

	spWater->setPhase(StratumI::e_liquid);
	spWater->setDensity(10.0f);
	spWater->setScale(0.5f);

	spAir->setPhase(StratumI::e_gas);
	spAir->setOffset(15.0f);

	m_spStrataI->addStratum(spGround);
	m_spStrataI->addStratum(spWater);
	m_spStrataI->addStratum(spAir);
#endif

	sp<DrawI> spDrawI=m_spQuickViewerI->getDrawI();
	m_spStratumDrawI->bind(spDrawI);

	m_theaterRV.bind(spSimScope);
	m_theaterRV.createRecord();

	std::vector<String> modifierNames;
#if XFLOAT_RECORDER
	modifierNames.push_back("*.RecordPlayer");
#endif
	modifierNames.push_back("*.Tick");
	modifierNames.push_back("*.Scan");
	modifierNames.push_back("*.Attract");
	modifierNames.push_back("*.Construct");
	modifierNames.push_back("*.Dominate");
	modifierNames.push_back("*.Communicate");
	modifierNames.push_back("*.Collect");
	modifierNames.push_back("*.Control");
	modifierNames.push_back("*.Shoot");
	modifierNames.push_back("*.Detonate");
	modifierNames.push_back("*.BruteCollide");
	modifierNames.push_back("*.Swim");
	modifierNames.push_back("*.Gravity");
	modifierNames.push_back("*.Thrust");
	modifierNames.push_back("*.NewtonEuler");
	modifierNames.push_back("*.Provot");
	modifierNames.push_back("*.Terrain");
	modifierNames.push_back("*.Sing");
	modifierNames.push_back("*.Discard");
	modifierNames.push_back("*.DrawConstruct");
	modifierNames.push_back("*.DrawParticle");
	modifierNames.push_back("*.DrawRadio");
	modifierNames.push_back("*.DrawScan");
	modifierNames.push_back("*.DrawTargeting");
	modifierNames.push_back("*.DrawThrust");
	modifierNames.push_back("*.DrawRod");
	modifierNames.push_back("*.DrawTerrain");
#if XFLOAT_RECORDER
	modifierNames.push_back("*.RecordRecorder");
#endif

	U32 size=modifierNames.size();
	for(U32 m=0;m<size;m++)
	{
		m_modifierArray.push_back(spRegistry->create(modifierNames[m]));
	}
	size=m_modifierArray.size();
	for(U32 m=0;m<size;m++)
	{
		if(m_modifierArray[m].isValid())
		{
			m_spModifierChain->insert(m_modifierArray[m],
					m_theaterRV.layout());
		}
	}

	sp<RecordGroup>& rspParticles=m_theaterRV.particles();

	m_theaterRV.strataI()=m_spStrataI;
	m_theaterRV.drawI()=spDrawI;
	m_theaterRV.stratumDrawI()=m_spStratumDrawI;

	sp<DirectorI> spDirectorI=m_theaterRV.directorI();

	Matter matterRV;
	matterRV.bind(spSimScope);

	//* assorted junk
	for(U32 m=0;m<9;m++)
	{
		matterRV.createRecord();
		matterRV.radius()=0.2f;
		matterRV.mass()=0.5f;
		matterRV.metal()=1.0f*(m%2);
		matterRV.gem()=1.0f*((m+1)%2);
		matterRV.identifier()="junk";
		set(matterRV.location(),F32(m/3),F32(m%3),10.0f);
		rspParticles->add(matterRV.record());
	}

	//* seeker
	sp<RecordGroup> spSeekerGroup=RecordView::loadRecordGroup(spSimScope,
			mediaPath+"/craft/seeker.rg");
	spDirectorI->moveRecordGroup(spSeekerGroup,SpatialVector(0.0f,0.0f,1.0f));
	spDirectorI->adoptRecordGroup(m_theaterRV.record(),spSeekerGroup);
	Record particle=
			spDirectorI->searchRecordGroup(spSeekerGroup,"ControlCenter");
	spDirectorI->addBehavior(particle,mediaPath+"/lua/seek.lua");

	//* balloon
	sp<RecordGroup> spBalloonGroup=RecordView::loadRecordGroup(spSimScope,
			"../media/craft/balloon.rg");
	spDirectorI->moveRecordGroup(spBalloonGroup,
			SpatialVector(5.0f,-6.0f,3.0f));
	spDirectorI->adoptRecordGroup(m_theaterRV.record(),spBalloonGroup);
	particle=spDirectorI->searchRecordGroup(spBalloonGroup,"ControlCenter");
	spDirectorI->addBehavior(particle,mediaPath+"/lua/noop.lua");

	//* vacuum
	sp<RecordGroup> spVacuumGroup=RecordView::loadRecordGroup(spSimScope,
			mediaPath+"/craft/vacuum.rg");
	spDirectorI->moveRecordGroup(spVacuumGroup,
			SpatialVector(4.0f,2.0f,3.0f));
	spDirectorI->adoptRecordGroup(m_theaterRV.record(),spVacuumGroup);
	particle=spDirectorI->searchRecordGroup(spVacuumGroup,"ControlCenter");
	spDirectorI->addBehavior(particle,mediaPath+"/lua/track.lua");

	//* shooter
	sp<RecordGroup> spShooterGroup=RecordView::loadRecordGroup(spSimScope,
			"../media/craft/shooter.rg");
	spDirectorI->moveRecordGroup(spShooterGroup,
			SpatialVector(3.0f,-4.0f,3.0f));
	spDirectorI->adoptRecordGroup(m_theaterRV.record(),spShooterGroup);
	particle=spDirectorI->searchRecordGroup(spShooterGroup,"ControlCenter");
	spDirectorI->addBehavior(particle,mediaPath+"/lua/target.lua");

	//* spider
#if FALSE
	sp<RecordGroup> spSpiderGroup=RecordView::loadRecordGroup(spSimScope,
			"../media/craft/spider.rg");
	spDirectorI->moveRecordGroup(spSpiderGroup,
			SpatialVector(9.0f,1.0f,8.0f));
	spDirectorI->adoptRecordGroup(m_theaterRV.record(),spSpiderGroup);
#endif

	if(spSeekerGroup.isValid())
	{
		//* dump to file
		std::ofstream outCraftFile("test/seeker_out.rg");
		if(!outCraftFile)
		{
			feLog("MyDraw::bind could not open craft output file");
		}
		else
		{
			sp<data::StreamI> spStream(new data::AsciiStream(spSimScope));
			spStream->output(outCraftFile, spSeekerGroup);
			outCraftFile.close();
		}
	}

#if FALSE
	Peeker peeker;
	spViewerScope->peek(peeker);
	feLog("\nViewer Scope:%s\n",peeker.output().c_str());
	peeker.clear();
	spSimScope->peek(peeker);
	feLog("\nSim Scope:%s\n",peeker.output().c_str());

	feLog("Theater:\n");
	m_theaterRV.dump();
#endif

#if TRUE
	save("test/xFloat_out.rg");
#endif
}

void MyDraw::handle(Record &render)
{
	if(!m_theaterRV.scope().isValid())
	{
		setup(render.layout()->scope());
	}

	if(m_asViewer.viewer_layer.check(render) &&
			m_asViewer.viewer_spatial.check(render))
	{
		const Color yellow(1.0f,1.0f,0.0f);
		const Color pink(1.0f,0.5f,0.5f);

		const SpatialVector origin(0.0f,0.0f,0.0f);
		const SpatialVector corner(10.0f,10.0f,0.0f);
		const SpatialVector posX(1.0f,0.0f,0.0f);
		const SpatialVector posZ(0.0f,0.0f,1.0f);

		sp<DrawI> spDrawI=m_spQuickViewerI->getDrawI();

		const I32& rPerspective=m_asViewer.viewer_spatial(render);
		switch(rPerspective)
		{
			case 0:
#if 1//FEX_BEH_DRAW_TEXT
				{
					const I32& rLayer=m_asViewer.viewer_layer(render);
					if(rLayer>0)
					{
						sp<ViewerI> spViewerI=m_spQuickViewerI;
						if(spViewerI.isValid())
						{
							String text;
							text.sPrintf("%.0f FPS %.3f %+d ms",
									spViewerI->frameRate(),
									m_theaterRV.time(),
									U32(m_theaterRV.deltaT()*1e3f));
							spDrawI->drawAlignedText(corner,text,pink);
						}
					}
				}
#endif
				break;

			case 1:
				{
					static F32 maxTime=0.0f;

					const F32 step=(1.0f/60.0f);

					m_spStratumDrawI->recenter(
							m_spQuickViewerI->interestPoint());

					sp<CameraControllerI> spCameraControllerI=
							m_spQuickViewerI->getCameraControllerI();
					if(spCameraControllerI.isValid())
					{
						//* report camera position
						const SpatialTransform& cameraMatrix=
								spCameraControllerI->camera()->cameraMatrix();
						SpatialTransform position;
						invert(position,cameraMatrix);
						m_theaterRV.cameraLoc()=position.translation();
						rotateVector(position,posX,m_theaterRV.cameraAt());
						rotateVector(position,posZ,m_theaterRV.cameraUp());

						//* control time
						m_theaterRV.deltaT()=
								(spCameraControllerI->keyCount('p'))?
								0.0f: step;
						I32 count;
						if((count=spCameraControllerI->keyCount(
								WindowEvent::e_keyCursorLeft))>0)
						{
							m_theaterRV.time()-=step*count;
							spCameraControllerI->setKeyCount(
									WindowEvent::e_keyCursorLeft,0);
						}
						if((count=spCameraControllerI->keyCount(
								WindowEvent::e_keyCursorRight))>0)
						{
							if(m_theaterRV.time()+step*count>maxTime)
							{
								m_theaterRV.time()=maxTime;
								m_theaterRV.deltaT()=step;
							}
							else
							{
								m_theaterRV.time()+=step*count;
							}
							spCameraControllerI->setKeyCount(
									WindowEvent::e_keyCursorRight,0);
						}
					}

#if TRUE
					//* blind rewind test
					static BWORD rewound=FALSE;
					if(!rewound && maxTime>0.1)
					{
						feLog("REWIND\n");
						rewound=TRUE;
						m_theaterRV.time()-=step;
						m_theaterRV.deltaT()=0.0f;
					}
#endif
					I32 frame=I32(m_theaterRV.time()/step);
					if(!(frame%1000))
					{
						feLog("FRAME %d\n",frame);
					}

					if(m_theaterRV.deltaT()>0.001f)
					{
						m_theaterRV.time()+=m_theaterRV.deltaT();
						maxTime=m_theaterRV.time();
					}

					//* update what is visible to us
					sp<DirectorI> spDirectorI=m_theaterRV.directorI();
					Record particle=spDirectorI->searchRecordGroup(
							m_theaterRV.particles(),"ControlCenter");
					m_controlCenterRV.bind(particle);
					m_theaterRV.viewpoint()=m_controlCenterRV.viewpoint();

					Record record=m_theaterRV.record();

					try
					{
						m_spModifierChain->signal(record);
					}
					catch(Exception &e)
					{
						//* if disaster strikes, try pausing if running
						if(m_theaterRV.deltaT()>0.001f)
						{
							e.log();
							feLog("Auto-pausing\n");
							m_theaterRV.time()-=step;
							m_theaterRV.deltaT()=0.0f;
							if(spCameraControllerI.isValid())
							{
								spCameraControllerI->setKeyCount('p',1);
							}
						}
						else
						{
							throw;
						}
					}

					spDrawI->drawAxes(1.0f);
#if FEX_BEH_DRAW_TEXT
					spDrawI->drawAlignedText(origin,"origin",yellow);
#endif
				}
				break;
		}
	}
}

MyDraw::~MyDraw(void)
{
#if FE_CODEGEN<FE_OPTIMIZE
	Peeker peeker;
	m_spModifierChain->peek(peeker);

	feLog("~MyDraw\n%s\n",peeker.str().c_str());
#endif
}
