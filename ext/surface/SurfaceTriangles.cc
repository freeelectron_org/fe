/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define	FE_ST_DRAW_UV			FALSE

#define	FE_ST_TREE_DRAW_DEBUG	FALSE
#define	FE_ST_TREE_DRAW_NORMALS	FALSE

namespace fe
{
namespace ext
{

SurfaceTriangles::SurfaceTriangles(void):
	m_triangulation(SurfaceI::e_balanced)
{
	m_pImpactPool=&m_triangleImpactPool;
}

SurfaceTriangles::~SurfaceTriangles(void)
{
}

sp<SurfaceI::ImpactI> SurfaceTriangles::sampleImpact(I32 a_triangleIndex,
	SpatialBary a_barycenter,SpatialVector a_tangent) const
{
	const SpatialTransform xform=
			sample(a_triangleIndex,a_barycenter,a_tangent);

	U32 face=a_triangleIndex;
	U32 vertexIndex0=a_triangleIndex*3;
	U32 vertexIndex1=vertexIndex0+1;
	U32 vertexIndex2=vertexIndex0+2;

	if(m_vertexMap.size())
	{
		const I32 tri3=a_triangleIndex*3;
		vertexIndex0=m_vertexMap[tri3];
		vertexIndex1=m_vertexMap[tri3+1];
		vertexIndex2=m_vertexMap[tri3+2];
	}
	else
	{
		//* TODO clean up duplicate code with sample()
		const U32 faceCount=m_vertices/3;
		if(face*3+3>m_vertices ||
				m_pTriangleIndexArray[face*3]!=a_triangleIndex)
		{
			for(face=0;face<faceCount;face++)
			{
				if(m_pTriangleIndexArray[face*3]==a_triangleIndex)
				{
					break;
				}
			}
		}

		const U32 face3=face*3;
		if(face3+3>m_vertices)
		{
			return sp<Impact>(NULL);
		}

		vertexIndex0=face3;
		vertexIndex1=face3+1;
		vertexIndex2=face3+2;
	}

	const SpatialVector& rVertex0=m_pVertexArray[vertexIndex0];
	const SpatialVector& rVertex1=m_pVertexArray[vertexIndex1];
	const SpatialVector& rVertex2=m_pVertexArray[vertexIndex2];

	Vector2 uv(0,0);
	SpatialVector du(0,0,0);
	SpatialVector dv(0,0,0);
	if(m_pUVArray)
	{
		const Vector2& rUV0=m_pUVArray[vertexIndex0];
		const Vector2& rUV1=m_pUVArray[vertexIndex1];
		const Vector2& rUV2=m_pUVArray[vertexIndex2];

		uv=location(a_barycenter,rUV0,rUV1,rUV2);

		triangleDuDv(du,dv,rVertex0,rVertex1,rVertex2,rUV0,rUV1,rUV2);
	}

	SpatialVector tangent(0,0,0);
	if(m_pTangentArray)
	{
		const SpatialVector& rTangent0=m_pTangentArray[vertexIndex0];
		const SpatialVector& rTangent1=m_pTangentArray[vertexIndex1];
		const SpatialVector& rTangent2=m_pTangentArray[vertexIndex2];

		tangent=location(a_barycenter,rTangent0,rTangent1,rTangent2);
	}
	else if(m_pUVArray)
	{
		const SpatialVector& rNorm0=m_pNormalArray[vertexIndex0];
		const SpatialVector& rNorm1=m_pNormalArray[vertexIndex1];
		const SpatialVector& rNorm2=m_pNormalArray[vertexIndex2];

		const SpatialVector norm=location(a_barycenter,rNorm0,rNorm1,rNorm2);
		tangentFromDuDvN(tangent,du,dv,norm);
	}

	sp<Impact> spImpact=m_pImpactPool->get();

	spImpact->setSurface(this);

	spImpact->setTriangleIndex(a_triangleIndex);
	spImpact->setBarycenter(a_barycenter);
	spImpact->setUV(uv);
	spImpact->setDu(du);
	spImpact->setDv(dv);
	spImpact->setTangent(tangent);

	spImpact->setPointIndex0(m_pPointIndexArray[vertexIndex0]);
	spImpact->setPointIndex1(m_pPointIndexArray[vertexIndex1]);
	spImpact->setPointIndex2(m_pPointIndexArray[vertexIndex2]);

	spImpact->setVertex0(rVertex0);
	spImpact->setVertex1(rVertex1);
	spImpact->setVertex2(rVertex2);

	spImpact->setNormal0(m_pNormalArray[vertexIndex0]);
	spImpact->setNormal1(m_pNormalArray[vertexIndex1]);
	spImpact->setNormal2(m_pNormalArray[vertexIndex2]);

	const SpatialVector intersection=xform.translation();
	const Real distance=magnitude(intersection);
	spImpact->setOrigin(SpatialVector(0.0,0.0,0.0));
	spImpact->setDistance(distance);
	spImpact->setDirection(intersection*(distance>0.0? 1.0/distance: 1.0));

	return spImpact;
}

SpatialTransform SurfaceTriangles::sample(I32 a_triangleIndex,
	SpatialBary a_barycenter,SpatialVector a_tangent) const
{
	FEASSERT(a_triangleIndex>=0);

	U32 vertexIndex0=a_triangleIndex*3;
	U32 vertexIndex1=vertexIndex0+1;
	U32 vertexIndex2=vertexIndex0+2;

	if(m_vertexMap.size())
	{
		const I32 tri3=a_triangleIndex*3;
		vertexIndex0=m_vertexMap[tri3];
		vertexIndex1=m_vertexMap[tri3+1];
		vertexIndex2=m_vertexMap[tri3+2];
	}
	else
	{
		const U32 faceCount=m_vertices/3;

		U32 face=a_triangleIndex;
		if(face*3+3>m_vertices ||
				m_pTriangleIndexArray[face*3]!=a_triangleIndex)
		{
			//* TODO map
			for(face=0;face<faceCount;face++)
			{
				if(m_pTriangleIndexArray[face*3]==a_triangleIndex)
				{
					break;
				}
			}
		}

		const U32 face3=face*3;
		if(face3+3>m_vertices)
		{
#if FALSE
			//* WARN multi-threaded logging can segfault
			feLog("SurfaceTriangles::sample"
					" excess face %d (%d>%d) face %d/%d\n",
					a_triangleIndex,face3+3,m_vertices,face,faceCount);
#endif
			return SpatialTransform::identity();
		}

		vertexIndex0=face3;
		vertexIndex1=face3+1;
		vertexIndex2=face3+2;
	}

	const SpatialVector& rVertex0=m_pVertexArray[vertexIndex0];
	const SpatialVector& rVertex1=m_pVertexArray[vertexIndex1];
	const SpatialVector& rVertex2=m_pVertexArray[vertexIndex2];
	const SpatialVector& rNormal0=m_pNormalArray[vertexIndex0];
	const SpatialVector& rNormal1=m_pNormalArray[vertexIndex1];
	const SpatialVector& rNormal2=m_pNormalArray[vertexIndex2];

	SpatialVector point;
	SpatialVector norm;
	if(m_sampleMethod==e_pointNormal)
	{
		//* TODO can we cache the configure?

		TrianglePN<Real> trianglePN;
		trianglePN.configure(rVertex0,rVertex1,rVertex2,
				rNormal0,rNormal1,rNormal2);
		trianglePN.solve(a_barycenter,point,norm);
	}
	else if(m_sampleMethod==e_linear)
	{
		point=location(a_barycenter,rVertex0,rVertex1,rVertex2);
		norm=location(a_barycenter,rNormal0,rNormal1,rNormal2);
	}
	else //* flat
	{
		point=location(a_barycenter,rVertex0,rVertex1,rVertex2);
		norm=rNormal0+rNormal1+rNormal2;
	}

	const Real mag=magnitude(norm);

	SpatialVector tangent;
	SpatialVector du;
	SpatialVector dv;

	SpatialTransform result;
	if(mag>0.0)
	{
		norm*=Real(1)/mag;

		if(m_pTangentArray)
		{
			tangent=unitSafe(location(a_barycenter,
					m_pTangentArray[vertexIndex0],
					m_pTangentArray[vertexIndex1],
					m_pTangentArray[vertexIndex2]));
		}
		else if(m_pUVArray)
		{
			triangleDuDv(du,dv,rVertex0,rVertex1,rVertex2,
					m_pUVArray[vertexIndex0],
					m_pUVArray[vertexIndex1],
					m_pUVArray[vertexIndex2]);

			tangentFromDuDvN(tangent,du,dv,norm);
		}
		else
		{
			tangent=a_tangent;
		}
		if(isZero(tangent))
		{
			tangent=unitSafe(unitSafe(rVertex1-rVertex0)+
					unitSafe(rVertex2-rVertex0));
		}

		makeFrameNormalY(result,point,tangent,norm);

//		feLog("a_tangent %s tangent %s\n",c_print(a_tangent),c_print(tangent));
	}
	else
	{
		setIdentity(result);
		setTranslation(result,point);
	}

#if FALSE
	feLog("tri %d bary %s\n",
			a_triangleIndex,c_print(a_barycenter));
	feLog("rVertex0 %s\n",c_print(rVertex0));
	feLog("rVertex1 %s\n",c_print(rVertex1));
	feLog("rVertex2 %s\n",c_print(rVertex2));
	feLog("rNormal0 %s\n",c_print(rNormal0));
	feLog("rNormal1 %s\n",c_print(rNormal1));
	feLog("rNormal2 %s\n",c_print(rNormal2));
	feLog("du %s\n",c_print(du));
	feLog("dv %s\n",c_print(dv));
	if(m_pUVArray)
	{
		feLog("uv0 %s\n",c_print(m_pUVArray[vertexIndex0]));
		feLog("uv1 %s\n",c_print(m_pUVArray[vertexIndex1]));
		feLog("uv2 %s\n",c_print(m_pUVArray[vertexIndex2]));
	}
	feLog("tangent %s\n",c_print(tangent));
	feLog("point %s\n",c_print(point));
	feLog("norm %s\n",c_print(norm));
	feLog("result %s\n",c_print(result));
#endif

	return result;
}

void SurfaceTriangles::drawInternal(BWORD a_transformed,
	const SpatialTransform* a_pTransform,sp<DrawI> a_spDrawI,
	const fe::Color* a_pColor,sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
	if(!m_vertices)
	{
		return;
	}

#if FE_ST_TREE_DRAW_DEBUG
	sp<DrawMode> spWireframe(new DrawMode);
	spWireframe->copy(a_spDrawI->drawMode());
	spWireframe->setDrawStyle(DrawMode::e_outline);
	a_spDrawI->pushDrawMode(spWireframe);
#endif

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);

	BWORD multicolor=(!a_pColor && m_pColorArray);
	const Color* pColor=multicolor? m_pColorArray: a_pColor;

	sp<PartitionI> spPartition=
			a_spPartition.isValid()? a_spPartition: m_spPartition;

	BWORD drawPartitioned=FALSE;
	const I32 partCount=
			spPartition.isValid()?  spPartition->partitionCount(): 0;
	if(partCount)
	{
		for(I32 partIndex=0;partIndex<partCount;partIndex++)
		{
			if(!spPartition->match(partIndex))
			{
				drawPartitioned=TRUE;
				break;
			}
		}
	}

	const DrawMode::Coloring coloring=a_spDrawI->drawMode()->coloring();

	//* TODO cache temp color
	Color* pTempColor=NULL;

	if(partCount && coloring==DrawMode::e_partition)
	{
		pTempColor=new Color[m_vertices];

		for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
		{
			const I32 partitionIndex=m_pPartitionIndexArray[vertexIndex];
			const Real r=0.4+0.2*(partitionIndex%4);
			const Real g=0.4+0.2*((partitionIndex>>2)%4);
			const Real b=0.4+0.2*((partitionIndex>>4)%4);
			pTempColor[vertexIndex]=Color(r,g,b);
		}

		multicolor=TRUE;
		pColor=pTempColor;
	}

	if(m_pNormalArray && coloring==DrawMode::e_normal)
	{
		pTempColor=new Color[m_vertices];

		for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
		{
			const SpatialVector& norm=m_pNormalArray[vertexIndex];
			pTempColor[vertexIndex]=Color(0.5,0.5,0.5)+0.5*norm;
		}

		multicolor=TRUE;
		pColor=pTempColor;
	}

	if(m_pTangentArray && coloring==DrawMode::e_tangent)
	{
		pTempColor=new Color[m_vertices];

		for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
		{
			const SpatialVector& tangent=m_pTangentArray[vertexIndex];
			pTempColor[vertexIndex]=Color(0.5,0.5,0.5)+0.5*tangent;
		}

		multicolor=TRUE;
		pColor=pTempColor;
	}

	if(m_pUVArray && coloring==DrawMode::e_uv)
	{
		pTempColor=new Color[m_vertices];

		for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
		{
			const Vector2& uv=m_pUVArray[vertexIndex];
			pTempColor[vertexIndex]=Color(uv[0],uv[1],0.0);
		}

		multicolor=TRUE;
		pColor=pTempColor;
	}

	const Array<I32>* pVertexMap=
			m_vertexMap.size()? &m_vertexMap: NULL;
	const Array<I32>* pHullPointMap=
			m_hullPointMap.size()? &m_hullPointMap: NULL;
	const Array<Vector4i>* pHullFacePointArray=
			m_hullFacePointArray.size()? &m_hullFacePointArray: NULL;

	if(drawPartitioned)
	{
//		I32 onePartitionIndex= -1;

		I32 fromPointIndex0=0;
		for(U32 elementIndex=0;elementIndex<m_elements;elementIndex++)
		{
			const I32 pointIndex0=elementIndex*3;

			//* NOTE using partition of first vertex
			const I32 partitionIndex=m_pPartitionIndexArray[pointIndex0];
			const BWORD show=spPartition->match(partitionIndex);

//			if(show && onePartitionIndex!=partitionIndex)
//			{
//				onePartitionIndex=partitionIndex;
//
//				feLog("incl %d \"%s\"\n",partitionIndex,
//						spPartition->partitionName(partitionIndex).c_str());
//			}

			const BWORD lastElement=(elementIndex==m_elements-1);
			if(!lastElement)
			{
				if(show)
				{
					continue;
				}
				if(fromPointIndex0==pointIndex0)
				{
					fromPointIndex0+=3;
					continue;
				}
			}

			const U32 drawCount=pointIndex0-fromPointIndex0+
					(show && lastElement? 3: 0);

			if(!drawCount)
			{
				continue;
			}

			//* catch up

//			feLog("SurfaceTriangles::drawInternal"
//					" catch up %d (%d to %d) to \"%s\"\n",
//					drawCount,fromPointIndex0,pointIndex0,
//					a_spDrawI->name().c_str());

			//* TODO FE_ST_DRAW_UV

			//* TODO multiple arrays per DrawBuffer

			//* TODO rebuild subset hull

			if(a_transformed)
			{
				a_spDrawI->drawTransformedTriangles(*a_pTransform,NULL,
						&m_pVertexArray[fromPointIndex0],
						&m_pNormalArray[fromPointIndex0],
						m_pUVArray,drawCount,DrawI::e_discrete,multicolor,
						pColor? (multicolor? &pColor[fromPointIndex0]: pColor):
						&white,NULL,NULL,NULL,sp<DrawBufferI>(NULL));
			}
			else
			{
				a_spDrawI->drawTriangles(
						&m_pVertexArray[fromPointIndex0],
						&m_pNormalArray[fromPointIndex0],
						m_pUVArray,drawCount,DrawI::e_discrete,multicolor,
						pColor? (multicolor? &pColor[fromPointIndex0]: pColor):
						&white,NULL,NULL,NULL,sp<DrawBufferI>(NULL));
			}
			fromPointIndex0=pointIndex0+3;
		}
	}
	else
	{
		SpatialVector* pUVW=NULL;

#if FE_ST_DRAW_UV
		if(m_pUVArray)
		{
			pUVW=new SpatialVector[m_vertices];
			for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
			{
				Vector2& rUV=m_pUVArray[vertexIndex];
				set(pUVW[vertexIndex],rUV[0],rUV[1],1.0);

				pUVW[vertexIndex]*=10.0;
			}
		}
#endif

		if(a_transformed)
		{
			a_spDrawI->drawTransformedTriangles(*a_pTransform,NULL,
					m_pVertexArray,m_pNormalArray,m_pUVArray,m_vertices,
					DrawI::e_discrete,multicolor,pColor? pColor: &white,
					pVertexMap,pHullPointMap,pHullFacePointArray,
					a_spDrawBuffer);

			if(pUVW)
			{
				a_spDrawI->drawTransformedTriangles(*a_pTransform,NULL,
						pUVW,m_pNormalArray,m_pUVArray,m_vertices,
						DrawI::e_discrete,multicolor,pColor? pColor: &yellow,
						pVertexMap,pHullPointMap,pHullFacePointArray,
						a_spDrawBuffer);
			}
		}
		else
		{
			a_spDrawI->drawTriangles(m_pVertexArray,m_pNormalArray,m_pUVArray,
					m_vertices,DrawI::e_discrete,multicolor,
					pColor? pColor: &white,
					pVertexMap,pHullPointMap,pHullFacePointArray,
					a_spDrawBuffer);

			if(pUVW)
			{
				a_spDrawI->drawTriangles(pUVW,m_pNormalArray,m_pUVArray,
						m_vertices,DrawI::e_discrete,multicolor,
						pColor? pColor: &yellow,
						pVertexMap,pHullPointMap,pHullFacePointArray,
						a_spDrawBuffer);
			}
		}

		delete pUVW;

#if FE_ST_TREE_DRAW_NORMALS
		const Color red(1.0f,0.0f,0.0f,1.0f);
		const Real normalScale=1.0;
		SpatialVector pLines[2];
		for(U32 m=0;m<m_vertices;m++)
		{
			pLines[0]=m_pVertexArray[m];
			pLines[1]=pLines[0]+m_pNormalArray[m]*normalScale;

			const SpatialVector* pNormals=NULL;
			const BWORD multicolor=FALSE;
			if(a_transformed)
			{
				a_spDrawI->drawTransformedLines(*a_pTransform,NULL,pLines,
						pNormals,2,DrawI::e_discrete,multicolor,&red);
			}
			else
			{
				a_spDrawI->drawLines(pLines,pNormals,2,DrawI::e_discrete,
						multicolor,&red);
			}
		}
#endif
	}

#if FE_ST_TREE_DRAW_DEBUG
	a_spDrawI->popDrawMode();
#endif

#if FE_ST_TREE_DRAW_DEBUG
	if(m_spSpatialTree.isValid())
	{
		m_spSpatialTree->draw(a_spDrawI,NULL);
	}
#endif

	if(pTempColor)
	{
		delete[] pTempColor;
	}
}

void SurfaceTriangles::Impact::copy(sp<SpatialTreeI::Hit>& a_rspHit)
{
	SurfaceSearchable::Impact::copy(a_rspHit);
	setFace(a_rspHit->face());

	const I32* pPointIndex=a_rspHit->pointIndex();
	setPointIndex2(pPointIndex[2]);

	const SpatialVector* pVertex=a_rspHit->vertex();
	setVertex2(pVertex[2]);

	const SpatialVector* pNormal=a_rspHit->normal();
	setNormal2(pNormal[2]);
}

void SurfaceTriangles::Impact::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_pColor,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
	//* NOTE UV impacts already have UV vertices
	const BWORD uvSpace=a_spDrawI->drawMode()->uvSpace();
	if(uvSpace)
	{
		a_spDrawI->drawMode()->setUvSpace(FALSE);
	}

	const Real inset=0.1;	//* TODO DrawMode param
	const Real scale=1.0-2.0*inset;
	const Real normalLength=0.05*
			(magnitude(m_vertex1-m_vertex0)+
			magnitude(m_vertex2-m_vertex1)+
			magnitude(m_vertex0-m_vertex2));

	SpatialVector line[4];
	line[0]=scale*m_vertex0+inset*m_vertex1+inset*m_vertex2;
	line[1]=inset*m_vertex0+scale*m_vertex1+inset*m_vertex2;
	line[2]=inset*m_vertex0+inset*m_vertex1+scale*m_vertex2;
	line[3]=line[0];

	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,4,DrawI::e_strip,false,a_pColor);

	if(uvSpace)
	{
		a_spDrawI->drawMode()->setUvSpace(TRUE);
		return;
	}

#if TRUE
	line[0]=(1.0/3.0)*(m_vertex0+m_vertex1+m_vertex2);

	const Color normalColor(0.0,1.0,0.0,a_pColor[0][3]);
//	Color normalColor=Color(1.0,1.0,1.0)-a_pColor[0];
//	normalColor[3]=a_pColor[0][3];
	line[1]=line[0]+normalLength*m_normal;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&normalColor);

	const Color duColor(1.0,0.0,0.0,a_pColor[0][3]);
//	Color duColor=Color(normalColor[1],normalColor[2],
//			normalColor[0],normalColor[3]);
	line[1]=line[0]+normalLength*m_du;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&duColor);

	const Color dvColor(0.0,0.0,1.0,a_pColor[0][3]);
//	Color dvColor=Color(normalColor[2],normalColor[0],
//			normalColor[1],normalColor[3]);
	line[1]=line[0]+normalLength*m_dv;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&dvColor);

	const Color tangentColor(1.0,0.0,1.0,a_pColor[0][3]);
	line[1]=line[0]+normalLength*m_tangent;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&tangentColor);

	Color black(0.0,0.0,0.0);
	black[3]=a_pColor[0][3]*0.3;
	line[1]=m_intersection;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&black);

	Color white(1.0,1.0,1.0);
	white[3]=a_pColor[0][3]*0.3;
	line[0]=m_intersection+normalLength*m_normal;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&white);

	line[0]=m_vertex0;
	line[1]=m_vertex0+normalLength*m_normal0;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&normalColor);

	line[0]=m_vertex1;
	line[1]=m_vertex1+normalLength*m_normal1;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&normalColor);

	line[0]=m_vertex2;
	line[1]=m_vertex2+normalLength*m_normal2;
	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			line,NULL,2,DrawI::e_strip,false,&normalColor);
#endif
}

void SurfaceTriangles::resolveImpact(sp<ImpactI> a_spImpactI) const
{
	sp<Impact> spImpact(a_spImpactI);

	// TEMP if no nearest
	if(!spImpact.isValid() || spImpact->distance()<Real(0))
	{
		return;
	}

	SpatialVector intersection;
	SpatialVector normal;
	RayTriangleIntersect<Real>::resolveContact(
			spImpact->vertex0(),
			spImpact->vertex1(),
			spImpact->vertex2(),
			spImpact->normal0(),
			spImpact->normal1(),
			spImpact->normal2(),
			spImpact->origin(),
			spImpact->direction(),
			spImpact->barycenter(),
			spImpact->distance(),
			intersection,normal);
	spImpact->setIntersectionLocal(intersection);
	spImpact->setNormalLocal(normal);
}

} /* namespace ext */
} /* namespace fe */
