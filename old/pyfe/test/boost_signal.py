#!/usr/bin/python

import sys

import fe
import fe.data
import fe.signal

# a python implemented signal handler
class MyHandler(fe.signal.HandlerFS):
    def handle(self,record):
        print record
        print record.attr
    def bind(self, signaler, layout):
        print "bind"
        print signaler
        print layout

# create a scope 
scope = fe.data.Scope()

# create a layout
layout = scope.declare("test")
layout.populate("attr", "int");
layout.populate("name", "string");
layout.populate("handler", "HandlerI");

# create record as the signal
signal = scope.createRecord(layout)
signal.attr = 42

# plug in signal system components
fe.manage("fexSignalDL")
# create a signaler from the plugin
signaler = fe.signal.SignalerFS(fe.create("SignalerI"))

# plugged in handler
logger = fe.signal.HandlerFS(fe.create("HandlerI.Log"))
signaler.insert(logger, layout)

# python derived handler
handler = MyHandler()
signaler.insert(handler,layout)

# fire off a signal
signaler.signal(signal)

# can set a record attribute that is a handler
signal.handler = handler

# it remains valid
signaler.signal(signal)

# can use the record attribute handler
signaler.insert(signal.handler, layout)
signaler.signal(signal)

scope.dump()
