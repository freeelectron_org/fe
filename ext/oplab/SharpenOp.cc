/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

namespace fe
{
namespace ext
{

//* try to coordinate terms with similar SmoothOp

void SharpenOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("primitiveGroup");
	catalog<String>("primitiveGroup","label")="Primitive Group";
	catalog<String>("primitiveGroup","suggest")="primitiveGroups";
	catalog<String>("primitiveGroup","hint")=
			"Group in Input Surface that limits which"
			" primitives are affected.";

	catalog<Real>("sharpening")=0.5;
	catalog<Real>("sharpening","high")=1.0;
	catalog<Real>("sharpening","max")=10.0;
	catalog<String>("sharpening","label")="Sharpening";
	catalog<String>("sharpening","hint")=
			"Amount of sharpening to apply per iteration.";

	catalog<I32>("iterations")=1;
	catalog<I32>("iterations","high")=50;
	catalog<I32>("iterations","max")=1e3;
	catalog<String>("iterations","label")="Iterations";
	catalog<String>("iterations","hint")=
			"Number of times to apply sharpening.";

	catalog<bool>("lockBoundaries")=false;
	catalog<String>("lockBoundaries","label")="Lock Boundaries";
	catalog<String>("lockBoundaries","hint")=
			"Do not affect points on an outside edge.";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<String>("Output Surface","iterate")="Output Surfaces";

	m_spDots=new DrawMode();
	m_spDots->setDrawStyle(DrawMode::e_solid);
	m_spDots->setPointSize(8.0);
}

void SharpenOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		const I32 dotCount=m_locationArray.size();

		spDrawBrush->pushDrawMode(m_spDots);

		//* TODO pull towards camera a little, out of surface
		spDrawBrush->drawPoints(m_locationArray.data(),NULL,
				dotCount,TRUE,m_colorArray.data());

#if FALSE
		const Color black(0.0,0.0,0.0,1.0);
		const Color white(1.0,1.0,1.0,1.0);
		const U32 margin(1);

		sp<ViewI> spView=spDrawOverlay->view();

		for(I32 dotIndex=0;dotIndex<dotCount;dotIndex++)
		{
			Vector2i projected=spView->project(m_locationArray[dotIndex],
					ViewI::e_perspective)+SpatialVector(4,4,0);

			String text;
			text.sPrintf("%.3f",m_heightArray[dotIndex]);

			drawLabel(spDrawOverlay,projected,text,
					FALSE,margin,white,NULL,&black);
		}
#endif

		spDrawBrush->popDrawMode();

		return;
	}

	const String primitiveGroup=catalog<String>("primitiveGroup");
	const BWORD primitiveGrouped=(!primitiveGroup.empty());
	const Real sharpening=catalog<Real>("sharpening");
	const I32 iterations=catalog<I32>("iterations");
	const BWORD lockBoundaries=catalog<bool>("lockBoundaries");

	String& rSummary=catalog<String>("summary");
	rSummary="";

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spInputProperties;
	if(!access(spInputProperties,spInputAccessible,
			e_primitive,e_properties)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputGroup;
	if(primitiveGrouped)
	{
		if(!access(spInputGroup,spInputAccessible,
				e_primitiveGroup,primitiveGroup)) return;
	}

	const U32 pointCount=spOutputPoint->count();
	const U32 primitiveCount=spInputVertices->count();
	const U32 memberCount=primitiveGrouped?
			(spInputGroup.isValid()? spInputGroup->count(): 0):
			primitiveCount;

	if(!memberCount)
	{
		feLog("SharpenOp::handle primitive count is zero\n");

		if(primitiveGrouped)
		{
			catalog<String>("warning")="no primitives in input group;";

			rSummary="EMPTY group "+primitiveGroup;
		}
		else
		{
			catalog<String>("warning")="no primitives in input surface;";

			rSummary="NO PRIMITIVES";
		}
		return;
	}

	//* NOTE polygon surfaces only
	//* TODO curves and meshes

	Array< std::set<I32> > pointFaces;
	Array< std::set<I32> > pointNeighbors;
	determineNeighborhood(spOutputAccessible,pointFaces,pointNeighbors);

	std::set<I32> inGroup;

	SpatialVector* pointBuffer[2];
	pointBuffer[0]=new SpatialVector[pointCount];
	pointBuffer[1]=new SpatialVector[pointCount];

	SpatialVector* normalBuffer=new SpatialVector[pointCount];

	I32 readIndex=0;
	SpatialVector* readBuffer=pointBuffer[0];
	SpatialVector* writeBuffer=pointBuffer[1];

	Real* heightBuffer=new Real[pointCount];

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		readBuffer[pointIndex]=spOutputPoint->spatialVector(pointIndex);
		heightBuffer[pointIndex]=Real(0);
	}

	if(spInputGroup.isValid())
	{
		for(U32 memberIndex=0;memberIndex<memberCount;memberIndex++)
		{
			const U32 subCount=spInputGroup->subCount(memberIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 primitiveIndex=spInputGroup->integer(memberIndex);
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);
				inGroup.insert(pointIndex);
			}
		}
	}
	else
	{
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const U32 subCount=spInputVertices->subCount(primitiveIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);
				inGroup.insert(pointIndex);
			}
		}
	}

	Real maxHeight(0);

	for(I32 iteration=0;iteration<iterations;iteration++)
	{
		maxHeight=0;

		memcpy((void*)writeBuffer,(void*)readBuffer,
				pointCount*sizeof(SpatialVector));

		//* calc height
		for(std::set<I32>::iterator it=inGroup.begin();it!=inGroup.end();it++)
		{
			const I32 pointIndex= *it;

			std::set<I32>& rNeighbors=pointNeighbors[pointIndex];
			const I32 neighborCount=rNeighbors.size();
			if(!neighborCount)
			{
				continue;
			}

			//* TODO cache face normals for other vertices of that face

			std::set<I32>& rFaces=pointFaces[pointIndex];
			const I32 faceCount=rFaces.size();
			Array<SpatialVector> faceCenter(faceCount);
			Array<SpatialVector> faceNormal(faceCount);

			SpatialVector faceSum(0,0,0);

			I32 facesUsed(0);
			for(std::set<I32>::iterator it=rFaces.begin();it!=rFaces.end();it++)
			{
				const I32 primitiveIndex= *it;

				const I32 subCount=spInputVertices->subCount(primitiveIndex);
				if(subCount<3)
				{
					continue;
				}

				const BWORD clockwise(TRUE);

				const I32 i0=spInputVertices->integer(primitiveIndex,0);
				const I32 i1=spInputVertices->integer(primitiveIndex,1);
				const I32 i2=spInputVertices->integer(primitiveIndex,2);

				const SpatialVector p0=readBuffer[i0];
				const SpatialVector p1=readBuffer[i1];
				const SpatialVector p2=readBuffer[i2];

				if(subCount==3)
				{
					faceCenter[facesUsed]=(1.0/3.0)*(p0+p1+p2);
					faceNormal[facesUsed]=unitSafe(cross(p1-p0,p2-p0))*
							(clockwise? Real(-1): Real(1));
				}
				else
				{
					//* TODO beware of repeats like SurfaceNormalOp

					//* NOTE presuming quad

					const I32 i3=spInputVertices->integer(primitiveIndex,3);
					const SpatialVector p3=readBuffer[i3];

					const SpatialVector mid=0.25*(p0+p1+p2+p3);

					const SpatialVector n01=unitSafe(cross(p0-mid,p1-mid));
					const SpatialVector n12=unitSafe(cross(p1-mid,p2-mid));
					const SpatialVector n23=unitSafe(cross(p2-mid,p3-mid));
					const SpatialVector n30=unitSafe(cross(p3-mid,p0-mid));

					faceCenter[facesUsed]=mid;
					faceNormal[facesUsed]=unitSafe(n01+n12+n23+n30)*
							(clockwise? Real(-1): Real(1));
				}

				faceSum+=faceNormal[facesUsed++];
			}
			if(!facesUsed)
			{
				continue;
			}

			const SpatialVector point=readBuffer[pointIndex];
			const SpatialVector pointNormal=unitSafe(faceSum);

			normalBuffer[pointIndex]=pointNormal;

			Real alongSum(0);
			for(std::set<I32>::iterator it=rNeighbors.begin();
					it!=rNeighbors.end();it++)
			{
				const I32 otherIndex= *it;

				const SpatialVector other=readBuffer[otherIndex];

				const SpatialVector delta=other-point;
				const Real otherAlong=dot(delta,pointNormal);

				alongSum+=otherAlong;
			}

			heightBuffer[pointIndex]= -alongSum/Real(neighborCount);
			if(maxHeight<fabs(heightBuffer[pointIndex]))
			{
				maxHeight=fabs(heightBuffer[pointIndex]);
			}
		}

		for(std::set<I32>::iterator it=inGroup.begin();it!=inGroup.end();it++)
		{
			const I32 pointIndex= *it;

			std::set<I32>& rNeighbors=pointNeighbors[pointIndex];
			const I32 neighborCount=rNeighbors.size();
			if(!neighborCount)
			{
				continue;
			}

			const SpatialVector point=readBuffer[pointIndex];
			const SpatialVector pointNormal=normalBuffer[pointIndex];
//			const Real pointHeight=heightBuffer[pointIndex];

			for(std::set<I32>::iterator it=rNeighbors.begin();
					it!=rNeighbors.end();it++)
			{
				const I32 otherIndex= *it;

				const BWORD isBoundary=(pointNeighbors[otherIndex].size() >
						pointFaces[otherIndex].size());
				if(lockBoundaries && isBoundary)
				{
					continue;
				}

				const SpatialVector other=readBuffer[otherIndex];

//				const Real otherHeight=heightBuffer[otherIndex];
//				if((pointHeight>0.0 && otherHeight>=pointHeight) ||
//						(pointHeight<0.0 && otherHeight<=pointHeight))
//				{
//					continue;
//				}

				const SpatialVector delta=other-point;
//				const Real distance=magnitude(delta);
				const SpatialVector toOther=unitSafe(delta);

				//* stabilize at 90 degrees
				const Real dotOther=dot(pointNormal,toOther);

				writeBuffer[otherIndex]-=delta*fabs(dotOther)*sharpening;
			}
		}

		readIndex=!readIndex;
		readBuffer=pointBuffer[readIndex];
		writeBuffer=pointBuffer[!readIndex];
	}

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		spOutputPoint->set(pointIndex,readBuffer[pointIndex]);
	}

	m_locationArray.resize(inGroup.size());
	m_colorArray.resize(inGroup.size());
	m_heightArray.resize(inGroup.size());
	I32 dotIndex(0);
	for(std::set<I32>::iterator it=inGroup.begin();it!=inGroup.end();it++)
	{
		const I32 pointIndex= *it;

		const Real heightScale=heightBuffer[pointIndex]/maxHeight;

		m_locationArray[dotIndex]=readBuffer[pointIndex];
		set(m_colorArray[dotIndex],
			heightScale>0.0? heightScale: 0.0,
			heightScale<0.0? -heightScale: 0.0,
			0.0,1.0);
		m_heightArray[dotIndex]=heightBuffer[pointIndex];

		dotIndex++;
	}

	feLog("maxHeight %.6G\n",maxHeight);

	delete[] heightBuffer;
	delete[] normalBuffer;
	delete[] pointBuffer[1];
	delete[] pointBuffer[0];

	rSummary.sPrintf("%d primitives",memberCount);

	if(primitiveGrouped)
	{
		String text;
		text.sPrintf(" in group %s",primitiveGroup.c_str());
		rSummary+=text;
	}

}

} /* namespace ext */
} /* namespace fe */
