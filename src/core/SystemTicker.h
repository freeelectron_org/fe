/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_SystemTicker_h__
#define __platform_SystemTicker_h__

#if FE_OS==FE_LINUX
//#define FE_HAS_TIMES
//#include <sys/times.h>
#endif

#if FE_OS==FE_OSX
#define FE_HAS_TIMES
#include <sys/times.h>
#endif

namespace fe
{

/**************************************************************************//**
	@brief Sleep the current process for the given number of milliseconds

	@ingroup platform

	This can sleep all the threads for the process.
*//***************************************************************************/
void FE_DL_EXPORT milliSleep(long milliseconds);

/**************************************************************************//**
	@brief Sleep the current process for the given number of microseconds

	@ingroup platform

	This can sleep all the threads for the process.
*//***************************************************************************/
void FE_DL_EXPORT microSleep(long microseconds);

/**************************************************************************//**
	@brief Sleep the current process for the given number of microseconds
			or the minimum that the OS can resolve, whichever is larger

	@ingroup platform

	This can sleep all the threads for the process.
*//***************************************************************************/
void FE_DL_EXPORT minimumSleep(long microseconds=1);

/**************************************************************************//**
	@brief Delay the current thread for the given number of nanoseconds

	@ingroup platform

	This function may involve an active polling of clock ticks.

	This should not sleep other threads.
*//***************************************************************************/
void FE_DL_EXPORT nanoSpin(long nanoseconds);

/**************************************************************************//**
	@brief Sleep the current thread for the given number of
	seconds and nanoseconds

	@ingroup platform

	This should not sleep other threads.
*//***************************************************************************/
#if 0
inline void nanoSleep(long seconds,long nanoseconds)
{
	FE_SLEEP(seconds, nanoseconds);
}
#endif

/**************************************************************************//**
	@brief Yield the current thread

	@ingroup platform

	This should not yield other threads.
*//***************************************************************************/
#if 0
inline void yield(void)
{
	FE_YIELD();
}
#endif

/**************************************************************************//**
	@fn unsigned long systemTick(void)

	@brief Get the high precision tick count from the processor

	@ingroup platform

	See fe::SystemTicker for utilities and interpreted results,
	including calibration.
*//***************************************************************************/

#if FE_HW==FE_X86 ||  FE_HW==FE_ARM
//* no return value and we know it (asm sets the return value)
#if FE_COMPILER==FE_MICROSOFT || FE_COMPILER==FE_INTEL
#if FE_COMPILER==FE_MICROSOFT
#if FE_OS==FE_WIN64
inline unsigned long systemTick(void)
{
	LARGE_INTEGER tick;
	BOOL success=QueryPerformanceCounter(&tick);
	return success? tick.LowPart: 0;
}
#else
#pragma warning( push )
#pragma warning( disable : 4035 )
inline unsigned long systemTick(void) { __asm { rdtsc } };
#pragma warning( pop )
#endif // FE_OS
#else // FE_COMPILER

#ifdef FE_HAS_TIMES
inline unsigned long systemTick(void)
{
	tms s_tms;
	times(s_tms);
	return s_tms.tms_cutime/* + s_tms.tms_cstime*/;
}
#elif FE_OS==FE_LINUX && FE_HW==FE_X86
inline unsigned long systemTick(void)
{
	unsigned long low;
	unsigned long high;
	__asm__ __volatile__ ( "rdtsc" : "=a" (low) , "=d" (high) );
	return low;
};
#else
inline unsigned long systemTick(void) { __asm { rdtsc }; return 0; };
#endif /* FE_OS */
#endif /* FE_COMPILER */
#elif FE_COMPILER==FE_GNU && FE_OS!=FE_OSX
#ifndef FE_SUPPRESS_GetSystemTick
#define GetSystemTick2(low,high) __asm__ __volatile__ ( "rdtsc" : "=a" (low) , "=d" (high) )
inline unsigned long systemTick(void)
{
	unsigned long low;
	unsigned long high;
	__asm__ __volatile__ ( "rdtsc" : "=a" (low) , "=d" (high) );
	return low;
};
#else
inline unsigned long systemTick(void) { return 0; }
#endif /* FE_SUPPRESS_GetSystemTick */
#elif defined(FE_HAS_TIMES)
inline unsigned long systemTick(void)
{
	// NOTE for OSX times(), tms elements all come back zero,
	// but return value seems to work

	tms s_tms;
	clock_t wall_ticks=times(&s_tms);
//	feLog("systemTick %lu %d %lu %lu %lu\n",
//			wall_ticks,
//			s_tms.tms_utime,
//			s_tms.tms_stime,
//			s_tms.tms_cutime,
//			s_tms.tms_cstime);
	return wall_ticks;
}
#else
inline unsigned long systemTick(void) { return 0; }
#endif /* FE_COMPILER */
#else
inline unsigned long systemTick(void) { return 0; }
#endif /* FE_HW */


/**************************************************************************//**
	@brief High precision timer

	@ingroup platform
*//***************************************************************************/
class FE_DL_PUBLIC FE_CORE_PORT SystemTicker
{
	public:
						SystemTicker(void);
						SystemTicker(const char *name);
						SystemTicker(const char *group, const char *name);
virtual					~SystemTicker(void)								{ }

						/** @brief Returns a time in milliseconds from an
							arbitrary fixed reference point

							This number can loop. */
		unsigned long	timeMS(void);

						/** @brief Perform a self-contained recalibration

							Must be performed at least once before TicksPerMS
							can be considered useful. */
static	void FE_CDECL	calibrate(void) { staticInit(); }

						/** @brief Returns the approximate number of processor
							clock ticks that occur every millisecond. */
static	double			ticksPerMS(void)			{ return ms_ticksPerMS; }

						/** @brief Return the reltive tick change between
							two absolute tick values

							Rollover is corrected.
						*/
static	unsigned long	tickDifference(unsigned long start,
								unsigned long finish)
						{
							if(start<=finish)
							{
								return finish - start;
							}
							return finish + (UINT_MAX-start);
						}

						/** @brief Returns the fractional number of microseonds
							for each processor clock tick */
static	float			microsecondsPerTick()		{ return ms_usPerTick; }

		void			log(const char *fmt, ...);
		void			log(void);

	private:
static	void			staticInit(void);

static	double			ms_ticksPerMS;
static	float			ms_usPerTick;

		unsigned long	m_lastincl;
		unsigned long	m_lastexcl;
		String			m_name;
		String			m_group;
		unsigned long	m_lasttick;
		struct timeval	m_timer;
};

/**************************************************************************//**
	@fn unsigned long systemMicroseconds(void)

	@brief Get the high precision microsecond timer.

	@ingroup platform

	This is a convenience function utilizing fe::systemTick().
	You must call fe::SystemTicker::calibrate() once prior to use.
*//***************************************************************************/
inline unsigned long systemMicroseconds(void)
{
	return (unsigned long)(systemTick()*SystemTicker::microsecondsPerTick()); }

} /* namespace */

#endif /* __platform_SystemTicker_h__ */
