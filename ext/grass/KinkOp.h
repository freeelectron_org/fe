/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_KinkOp_h__
#define __grass_KinkOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief displace points on curves using a Perlin field

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT KinkOp:
	public OperatorSurfaceCommon,
	public Initialize<KinkOp>
{
	public:
				KinkOp(void)												{}
virtual			~KinkOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		Noise	m_noise;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_KinkOp_h__ */
