/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceAttrDeleteOp_h__
#define __operator_SurfaceAttrDeleteOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Remove attributes from a Surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAttrDeleteOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceAttrDeleteOp>
{
	public:
				SurfaceAttrDeleteOp(void)									{}
virtual			~SurfaceAttrDeleteOp(void)									{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceAttrDeleteOp_h__ */
