/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperatorSurfaceI_h__
#define __operate_OperatorSurfaceI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief General Surface Manipulation

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperatorSurfaceI:
	virtual public HandlerI,
	public CastableAs<OperatorSurfaceI>
{
	public:

virtual	BWORD				saveState(String& rBuffer)						=0;
virtual	BWORD				loadState(const String& rBuffer)				=0;

virtual	BWORD				undo(String a_label,sp<Counted> a_spCounted)	=0;
virtual	BWORD				redo(String a_label,sp<Counted> a_spCounted)	=0;

virtual	void				setPlugin(OperatorPlugin* a_pPlugin)			=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperatorSurfaceI_h__ */
