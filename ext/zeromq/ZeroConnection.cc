/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "zeromq/zeromq.pmh"

#define FE_ZCN_VERBOSE		FALSE

namespace fe
{
namespace ext
{

ZeroConnection::ZeroConnection(void):
	m_isServer(FALSE),
	m_connected(FALSE),
	m_pContext(NULL),
	m_pSocket(NULL)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::ZeroConnection\n");
#endif
	memset(m_identity,0,5);
}

ZeroConnection::~ZeroConnection(void)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::~ZeroConnection\n");
#endif

	disconnect();

	if(m_pSocket)
	{
		const size_t bufferSize=128;
		char buffer[bufferSize];
		int rc=zmq_getsockopt(m_pSocket,ZMQ_LAST_ENDPOINT,
				buffer,(size_t *)&bufferSize);
		if(rc<0)
		{
			feLog("ZeroConnection::~ZeroConnection"
					" failed to read back endpoint\n");
		}
		else
		{
			m_endpoint=buffer;
		}

		if(m_isServer)
		{
			rc=zmq_unbind(m_pSocket,m_endpoint.c_str());
			if(rc<0)
			{
				feLog("ZeroConnection::~ZeroConnection"
						" could not unbind \"%s\"\n",
						m_endpoint.c_str());
			}
		}

		rc=zmq_close(m_pSocket);
		if(rc<0)
		{
			feLog("ZeroConnection::~ZeroConnection could not close \"%s\"\n",
					m_endpoint.c_str());
		}
		m_pSocket=NULL;

		rc=zmq_ctx_term(m_pContext);
		if(rc<0)
		{
			feLog("ZeroConnection::~ZeroConnection could not terminate context\n");
		}
		m_pContext=NULL;
	}
}

void ZeroConnection::initialize(void)
{
	m_pContext=zmq_ctx_new();
	if(!m_pContext)
	{
		feLog("ZeroConnection::initialize failed to create ZeroMQ context\n");
	}
}

Result ZeroConnection::connect(String a_address,short a_port)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::connect \"%s\" port %d\n",
			a_address.c_str(),a_port);
#endif

	m_isServer=a_address.empty();

	if(m_isServer)
	{
		m_endpoint.sPrintf("tcp://*:%d",a_port);

#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::connect server \"%s\"\n",m_endpoint.c_str());
#endif

		m_pSocket=zmq_socket(m_pContext,ZMQ_ROUTER);
		const int rc=zmq_bind(m_pSocket,m_endpoint.c_str());
		if(rc<0)
		{
			feLog("ZeroConnection::connect port %d failed binding\n",a_port);
			return e_refused;
		}

		String message;
		Result result=receive(message);
		if(failure(result))
		{
			return result;
		}

#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::connect server token message \"%s\"\n",
				message.c_str());
#endif
	}
	else
	{
		m_endpoint.sPrintf("tcp://%s:%d",a_address.c_str(),a_port);

#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::connect client \"%s\"\n",m_endpoint.c_str());
#endif

		m_pSocket=zmq_socket(m_pContext,ZMQ_DEALER);
		const int rc=zmq_connect(m_pSocket,m_endpoint.c_str());
		if(rc<0)
		{
			feLog("ZeroConnection::connect \"%s\" port %d failed connection\n",
					a_address.c_str(),a_port);
			return e_refused;
		}

		Result result=send("token");
		if(failure(result))
		{
			return result;
		}
	}

	m_connected=TRUE;
	return e_ok;
}

Result ZeroConnection::disconnect(void)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::disconnect\n");
#endif

	if(!m_connected)
	{
		return e_notInitialized;
	}

	m_connected=FALSE;

    zmq_disconnect(m_pSocket, m_endpoint.c_str());

	return e_ok;
}

static void freeFunction(void* a_pData,void* pHint)
{
	free(a_pData);
}

Result ZeroConnection::send(String a_message)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::send \"%s\"\n",a_message.c_str());
#endif

	if(m_isServer)
	{
		void *data=malloc(5);
		memcpy(data,m_identity,5);

		zmq_msg_t msg;
		const I32 rc=zmq_msg_init_data(&msg,data,5,freeFunction,NULL);
		if(rc<0)
		{
			feLog("ZeroConnection::sendBytes failed to init identity msg\n");
			return e_writeFailed;
		}

		const I32 bytesSent=zmq_msg_send(&msg,m_pSocket,ZMQ_SNDMORE);
		if(bytesSent!=5)
		{
			feLog("ZeroConnection::sendBytes only sent %d bytes of identity\n",
					bytesSent);
		}
	}

	const I32 byteCount=a_message.length()+1;
	void *data=malloc(byteCount);
	memcpy(data,a_message.c_str(),byteCount);

	zmq_msg_t msg;
	const I32 rc=zmq_msg_init_data(&msg,data,byteCount,freeFunction,NULL);
	if(rc<0)
	{
		feLog("ZeroConnection::sendBytes failed to init msg\n");
		return e_writeFailed;
	}

	const I32 bytesSent=zmq_msg_send(&msg,m_pSocket,0);
	if(bytesSent!=byteCount)
	{
		feLog("ZeroConnection::sendBytes only sent %d bytes of msg\n",
				bytesSent);
	}

	return e_ok;
}

Result ZeroConnection::receive(String& a_rMessage)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::receive\n");
#endif

	if(m_isServer)
	{
		zmq_msg_t msg;
		I32 rc=zmq_msg_init(&msg);
		if(rc<0)
		{
			feLog("ZeroConnection::receive"
					" could not init identity message \n");
			return e_readFailed;
		}

		I32 bytesRead=zmq_msg_recv(&msg,m_pSocket,0);
#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::receive identity bytesRead %d\n",
				bytesRead);
#endif

		if(bytesRead<0)
		{
			return e_readFailed;
		}

		void* pBuffer=zmq_msg_data(&msg);

		if(bytesRead==5)
		{
			memcpy(m_identity,(U8*)pBuffer,bytesRead);
		}
		else
		{
			memset(m_identity,0,5);
		}

		zmq_msg_close(&msg);
	}

	zmq_msg_t msg;
	I32 rc=zmq_msg_init(&msg);
	if(rc<0)
	{
		feLog("ZeroConnection::receive"
				" could not init message \n");
		return e_readFailed;
	}

	I32 bytesRead=zmq_msg_recv(&msg,m_pSocket,0);
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::receive bytesRead %d\n",
			bytesRead);
#endif

	if(bytesRead<0)
	{
		zmq_msg_close(&msg);
		return e_readFailed;
	}

	void* pBuffer=zmq_msg_data(&msg);

	if(bytesRead<0)
	{
		a_rMessage="";
	}
	else
	{
		a_rMessage=(char*)pBuffer;
	}

	zmq_msg_close(&msg);

#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::receive read \"%s\"\n",a_rMessage.c_str());
#endif

	return e_ok;
}

} /* namespace ext */
} /* namespace fe */

