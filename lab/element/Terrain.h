/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Terrain_h__
#define __element_Terrain_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Contribute impact response from terrain

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Terrain: virtual public HandlerI
{
	public:
				Terrain(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Arena						m_arenaRV;
		RecordArrayView<Particle>	m_particleRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Terrain_h__ */
