/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_LinearTire_h__
#define __tire_LinearTire_h__

namespace fe
{
namespace ext
{

/// @brief Simple Linear Tire Model
class AsLinearTireModel
	: public AsTireModel, public Initialize<AsLinearTireModel>
{
	public:
		AsConstruct(AsLinearTireModel);
		void initialize(void)
		{
			add(stiffness,			FE_USE("tire:z:stiffness"));
			add(damping,			FE_USE("tire:z:damping"));
			add(xy_stiffness,		FE_USE("tire:xy:stiffness"));
			add(friction,			FE_USE("tire:friction"));
			add(do_rollover,		FE_USE("tire:do_rollover"));
			add(pneumatic_trail,	FE_USE("tire:pneumatic_trail"));
			add(Fmax,				FE_USE("tire:Fmax"));
		}
		Accessor<Real>			stiffness;
		Accessor<Real>			damping;
		Accessor<Real>			xy_stiffness;
		Accessor<Real>			friction;
		Accessor<Real>			pneumatic_trail;
		Accessor<Real>			Fmax;
		Accessor<bool>				do_rollover;
};


/// @brief Linear Tire Model
class LinearTire
	: virtual public TireI, virtual public StreamableI,
	public CastableAs<LinearTire>
{
	public:
					LinearTire(void);
virtual				~LinearTire(void);

					/** Integrate a time step. */
		void		step(t_moa_real a_dt);

					/** Compile internal structure.   This should be done
						before any time stepping. */
		bool		compile(Record r_tire, Record r_config,
						sp<RecordGroup> a_rg_dataset);

		/// @name	Dynamic State Accessors
		/// @{
		void		setVelocity(		const t_moa_v3		&a_velocity);
		void		setAngularVelocity(	const t_moa_v3		&a_ang_velocity);
		void		setContact(			const t_moa_real	a_radius,
										const t_moa_real	a_inclination);
		const t_moa_v3		&getForce(void);
		const t_moa_v3		&getMoment(void);
		const t_moa_v3		&getVelocity(void);
		const t_moa_v3		&getAngularVelocity(void);
		const t_moa_real	&getRadius(void) { return m_radius; }

		t_moa_real	&contactRadius(void) { return m_contact_radius; }
		t_moa_real &inclination(void) { return m_inclination; }

virtual	void	output(std::ostream &a_ostrm)
		{
			a_ostrm << m_force[0] << " " << m_force[1] << " " << m_force[2];
		}
virtual	void	input(std::istream &a_istrm)
		{
			a_istrm >> m_force[0] >> m_force[1] >> m_force[2];
		}

	private:
		void			initialize(void);
		// dynamic state
		t_moa_v3			m_force;
		t_moa_v3			m_moment;
		t_moa_v3			m_velocity;
		t_moa_v3			m_angular_velocity;
		t_moa_real			m_contact_radius;
		t_moa_real			m_inclination;

		// geometry
		t_moa_real			m_radius;
		t_moa_real			m_width;
		t_moa_real			m_pneumatic_trail;

		bool				m_do_rollover;

		t_moa_real			m_xy_stiffness;
		t_moa_real			m_z_stiffness;
		t_moa_real			m_z_damping;
		t_moa_real			m_Cf;
		t_moa_real			m_Fmax;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_LinearTire_h__ */

