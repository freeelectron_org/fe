/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <field/field.pmh>

#include "DensitySource.h"

namespace fe
{
namespace ext
{

DensitySource::DensitySource(void)
{
}

DensitySource::~DensitySource(void)
{
}


void DensitySource::handle(Record &r_sig)
{
	Real timestep = cfg<Real>("timestep");

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	sp<DensityField> spField = cfg< sp<Component> >("field"); // target field
	if(!spField.isValid()) { return; }

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		hp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		ScopedPathAccessor<Real> aRate(spScope,
			cfg<String>("rate"));
		ScopedPathAccessor<Real> aAccum(spScope,
			cfg<String>("accumulator"));
		if(	m_asParticle.location.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record record = spRA->getRecord(i);
				SpatialVector &location = m_asParticle.location(record);
				Real *pAccum = aAccum(record);
				if(!pAccum) { continue; }

				Real *pRate = aRate(record);
				Real rate = 1.0;
				if(pRate) { rate = *pRate; }

				Real amount = rate * *pAccum * timestep;

				spField->inject(amount, location);

				*pAccum -= amount;
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

