/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_TransformCLOp_h__
#define __opencl_TransformCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to transform points

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT TransformCLOp:
	public OpenCLOp,
	public Initialize<TransformCLOp>
{
	public:
				TransformCLOp(void)											{}
virtual			~TransformCLOp(void)										{}

		void	initialize(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_TransformCLOp_h__ */
