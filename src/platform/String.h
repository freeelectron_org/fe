/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_string_h__
#define __platform_string_h__

namespace fe
{

#define FESTRING_DEBUG			FALSE
#define	FESTRING_LOCALSIZE		16		//* unshared for small strings

#ifdef HAS_MEMRCHR
#define FEMEMRCHR(s,c,n)		memrchr(s,c,n)
#else
inline void *fe_memrchr(const void *s, int c, size_t n)
{
	char* r=(char*)s+n;
	while(r>=s && *r!=c) { r--; }
	return r<s? NULL: r;
}
#define FEMEMRCHR(s,c,n)		fe_memrchr(s,c,n)
#endif

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
//#define FESTRLEN_MB(x,y)		_mbstrlen((char *)x)
#define FESTRCMP_MB(x,y)		_mbscmp(x,y)
#define FESTRNCMP_MB(x,y,z)		_mbsncmp(x,y,z)
#define FESTR_MB2UPPER(x)		_mbsupr((unsigned char*)x)
#define FESTR_MB2LOWER(x)		_mbslwr((unsigned char*)x)
#define FEVSNPRINTF(x,y,z,a)	_vsnprintf(x,y,z,a)
#define FESTRCPY(dest,size,src)	strcpy_s((char *)dest,size,(char *)src)
#else
#define FEVSNPRINTF(x,y,z,a)	vsnprintf(x,y,z,a)
#define FESTRCPY(dest,size,src)	strcpy((char *)dest,(char *)src)
#endif

#if defined(__linux__) || FE_OS==FE_OSX
//#define FESTRLEN_MB(x,y)		mblen((char *)x,(FE_UWORD)y)
#define FESTRCMP_MB(x,y)		strcmp((char *)x,(char *)y)
#define FESTRNCMP_MB(x,y,z)		strncmp((char *)x,(char *)y,z)
#endif

#if defined(FE_OS) && FE_OS==FE_FREEBSD
#define FESTRCMP_MB(x,y)		strcmp((char *)x,(char *)y)
#define FESTRNCMP_MB(x,y,z)		strncmp((char *)x,(char *)y,z)
#endif

#define FESTRLEN(x)				strlen((char *)x)

#define FESTRING_I8	char			// presumably 7-bit ASCII
#define FESTRING_U8	unsigned char	// presumably multibyte

#if FE_SSE>=1 && FE_COMPILER==FE_GNU
//#define FE_SSE_STRING_GNU
typedef int v4si __attribute__ ((vector_size (16)));
#endif

//* convert macro to String
#define FE_STRING2(x)	#x
#define FE_STRING(x)	FE_STRING2(x)

//* TODO consider deriving from std::string now that it has
//* short string optimization

//* WARNING short strings are thread-safe, but rep strings are not
//* TODO atomic reference counting on Rep for thread-safe sharing

/**************************************************************************//**
	@brief Automatically reference-counted string container.

	@ingroup platform

	Designed to behave as a native type, like float or int.
	All allocation worries are completely hidden.

	To assign a string just use simple assignments.
	@code
		String name("me");
		name="you";
		String id=name;
	@endcode

	You can compare strings in a boolean or lexical fashion.
	@code
		if(name!="Bob" && name.compare("M")<0)
			...
	@endcode

	To print a string in printf fashion, use c_str().
	@code
		printf("%s\n",name.c_str());
	@endcode

	Although the String encoding may start like a (char*) byte-wise,
	you should always use the c_str() method in conjunction
	with C-style string operations,
	especially variable argument functions like *printf.

	A String object is 32 bytes.
	Strings shorter than FESTRING_LOCALSIZE (currently 16),
	including terminator,
	are stored in a local buffer and compared quickly as two 64-bit words
	or as a single 128-bit word, depending on the compiler.
	Longer strings are allocated and can be shared in assignment.

	We would prefer to derive fe::String from std::string,
	just adding fe methods to the std::string buffer handling.
	But, as of Feb 2021, on gcc 8.3.0, same size small string operator==()
	is 3 times faster with fe::String than std::string.
	Also, std::string is forbidden from being ref counted,
	so passing std::string as an argument is a deep copy every time.
	You don't don't need a full mutex to protect the ref count,
	but a swap increment appears to take about 4ns (in 2021).
	The alternative of a deep copy on every string copy can be
	a little less, or a much much more, depending on the string length.
	But, for the sake of conformance, this task will likely happen eventually.

	As a tight core class, String doesn't carry virtual functions.

	@note Based on Coral GPL_String
	@note Reference: Stroustrup C++ Ref Man, 2nd ed., pg. 248
	@note operator cast to (char*) intentionally omitted
*//***************************************************************************/
class FE_DL_PUBLIC String
	{
	private:

	class FE_DL_PUBLIC Rep
		{
		public:
			void					newBuffer(U32 size);
			void					deleteBuffer(void);

			I32						m_references;
			FESTRING_U8*			m_pBuffer;
		};

	public:
						/// Construct an empty string.
						String(void);

						/// Copy constructor.
						String(const String &operand);

						/// Construct using a signed byte buffer.
						String(const FESTRING_I8* operand);

						/// Construct using a unsigned byte buffer.
						String(const FESTRING_U8* operand);

						/// allow init as 0 in templates
						String(int operand);

						~String(void);

#if FALSE				//* Meyers More Effective C++ #5: No!
						//* http://ptgmedia.pearsoncmg.com/imprint_downloads/informit/aw/meyerscddemo/demo/mec/m.htm#5970
						/** Cast the contents of the 8-bit buffer as signed
							bytes. */
						operator const FESTRING_I8 *(void) const
						{	return c_str(); }

						/** Cast the contents of the 8-bit buffer as
							unsigned bytes. */
						operator const FESTRING_U8 *(void) const
						{	return rawU8(); }
#endif

						/// Compare to another String.
		String&			operator=(const String& operand);

						/// Compare to a signed byte buffer (using 8-bit).
		String&			operator=(const FESTRING_I8* operand)
						{ return operator=((const FESTRING_U8 *)operand); };

						/// Compare to an unsigned byte buffer (using 8-bit).
		String&			operator=(const FESTRING_U8* operand);

						/// checks if the string is equivalent
		BWORD			equals(const String &operand) const;

						/** Standard string compare: returns -1, 0, or 1 if the
							string is alphabetically less than, equal,
							or greater than the operand */
		I32				compare(const String &operand) const;

		BWORD			operator<(const String &operand) const
						{	return compare(operand) < 0; }

						/** Return the substring preceding the last '/'

							If there is no '/', no text is returned. */
		String			pathname(void) const;

						/** Return the substring following the last '/'

							If there is no '/', the entire text is returned. */
		String			basename(void) const;

						/** Return the substring after the prefix

							If the string doesn't begin with the prefix,
							the original text is returned. */
		String			prechop(const String& prefix) const;

						/** Return the substring without trailing characters

							This version does not care what the
							characters are */
		String			prechop(I32 count) const;

						/** Return the substring before the suffix

							If the string doesn't end with the suffix,
							the original text is returned. */
		String			chop(const String& suffix) const;

						/** Return the substring without trailing characters

							This version does not care what the
							characters are */
		String			chop(I32 count) const;

						/// Returns TRUE if string contains the substring
		BWORD			contains(String find) const;

						/// Return a string with replaced substrings
		String			substitute(String find,String replace) const;

						/** Return a string with enviroment substitution

							Replace substrings like $VARIABLE and ${VARIABLE}
							with environment variables and or a map.

							Without arguments, only environment variables
							are used for substitution.

							A map can be provided for non-environment variables.
							In that case, the environment variable can
							optionally also be checked when a variable is
							not in the map.

							If set to throw missing, a exception is thrown
							is a variable is found that can't be resolved.
						*/
		String			substituteEnv(
								std::map<String,String>& a_rMap,
								BWORD a_alsoUseEnv=FALSE,
								BWORD a_throwMissing=FALSE) const;
		String			substituteEnv(void)
						{
							std::map<String,String> map;
							return substituteEnv(map,TRUE,FALSE);
						}

						/** Return a string with sections removed

							Each substring beginning with the given start
							up to the given end is removed.

							If either of the keep options is on,
							the associated delimiting string is retained.
						*/
		String			stripComments(String a_start="/*",String a_end="*/",
								BWORD a_keepStart=FALSE,BWORD a_keepEnd=FALSE);

						/** Compares to another string assuming the given
							string is a dot-delimited ordered list containing
							optional substrings.
							Returns true if strings are exactly the same, or if
							they match only up to the length of the operand
							and the operand's terminating zero matches a
							period. */
		BWORD			dotMatch(const String &operand) const;

						/// Force all applicable characters to upper case.
		void			forceUppercase(void)	{ forceCase(true); };
						/// Force all applicable characters to lower case.
		void			forceLowercase(void)	{ forceCase(false); };

						/** Return the number of represented characters,
							but not necessarily the size of any buffer. */
		U32				length(void) const;

						/// Returns true if the contents have zero length
		BWORD			empty(void) const;

						/// Populate the string in the manner of sprintf().
		String&			sPrintf(const char* fmt, ...);

						/** Populate the string as with sPrintf(),
							but by concatenating the results to the
							existing string. */
		String&			catf(const char* fmt, ...);

						/// Append the current String with the given text
		String&			cat(const char* operand);

						/// Append the current String with the given String
		String&			cat(const String& operand);

						/** Concatenate the strings in a list. */
		String&			cat(std::list<String> &strList, String sep = "");

						/** Add or remove characters to set the string length

							If expanded, the given character is used
							to pad the string. */
		void			resize(U32 size,char c=' ');

						/** Return the string, adding quotes if it contains
							a likely delimiter */
		String			maybeQuote(String beginQuote,String endQuote) const;
		String			maybeQuote(String quote="\"") const
						{	return maybeQuote(quote,quote); }

						/** Return the string,
							removing outer quotes if they exist */
		String			maybeUnQuote(String beginQuote,String endQuote) const;
		String			maybeUnQuote(String quote="\"") const
						{	return maybeUnQuote(quote,quote); }

		String			parse(String quote="\"", String delimiters=" \t\n",
							BWORD eachDelimiter=FALSE);

		I32				lines(void) const;
		String			line(I32 a_lineNumber) const;

static	String			join(const String &a_sep,
								std::list<String> &a_list);

static	String			join(const String &a_sep,
								const String &a_1,
								const String &a_2);

static	String			join(const String &a_sep,
								const String &a_1,
								const String &a_2,
								const String &a_3);

						/**	@brief Populate using variable arg format

							@internal */
		String&			vsPrintf(const char* fmt,va_list ap,int& size);

						/** Return the contents of the 8-bit buffer cast as
							signed bytes. */
const	FESTRING_I8*	c_str(void) const	{ return (FESTRING_I8*)rawU8(); };
						/** Return the contents of the 8-bit buffer cast as
							unsigned bytes. */
const	FESTRING_U8*	rawU8(void) const
						{	FEASSERT(!m_pRep || m_pRep->m_pBuffer);
							return m_pRep? m_pRep->m_pBuffer: m_pLocal; }

						/** @todo: string serialization currently hacked in.
							only works for 8bit and does too much copying */
		void			output(std::ostream &ostrm) const;
		void			input(std::istream &istrm);

		String&			operator+=(const String& operand)
						{	return cat(operand); }

		int				integer(void) const;
		Real			real(void) const;
		bool			match(const String a_pattern) const;
		bool			search(const String a_pattern) const;
		String			replace(const String a_pattern,
								const String a_replacement) const;
		String			convertGlobToRegex(void) const;

						/// Compute Fowler-Noll-Vo hash function (64-bit FNV-1)
		U64				computeFowlerNollVo1(void) const;

						/** Sort an Array of Strings

							Array<String> keys=getSomeUnorderedKeys();

							std::sort(keys.begin(),keys.end(),String::Sort());
						*/
	struct Sort
	{
		bool operator()(const String& rString1,const String& rString2)
		{
			return (rString1.compare(rString2)<0);
		}
	};

	private:
		FESTRING_U8*	buffer(void)
						{	FEASSERT(!m_pRep || m_pRep->m_pBuffer);
							return m_pRep? m_pRep->m_pBuffer: m_pLocal; }

		void			create(const FESTRING_I8* operand);

		void			init(void);

		void			forceCase(BWORD upper);

		Rep*			newRep(void);
		void			deleteRep(void);

#ifdef FE_SSE_STRING_GNU
	union
	{
		v4si			m_simd;
		FESTRING_U8		m_pLocal[FESTRING_LOCALSIZE];
	};
#elif FE_CPLUSPLUS >= 201103L
		alignas(16)
		FESTRING_U8		m_pLocal[FESTRING_LOCALSIZE];
#else
		FESTRING_U8		m_pLocal[FESTRING_LOCALSIZE];
#endif
		I32				m_length;
		Rep*			m_pRep;
	};

inline String::String(void)
{
	init();
}

inline void String::init(void)
{
	m_length=0;
	m_pRep=NULL;
	memset(m_pLocal,0,FESTRING_LOCALSIZE);
}

inline String::String(const String& operand)
{
	init();
	operator=(operand);
}

inline void String::create(const FESTRING_I8* operand)
{
	m_length=FESTRLEN(operand);
	U32 bytes=m_length+1;
	if(bytes<=FESTRING_LOCALSIZE)
	{
		m_pRep=NULL;
		FESTRCPY(m_pLocal,FESTRING_LOCALSIZE,operand);
		if(bytes<FESTRING_LOCALSIZE)
		{
			memset(m_pLocal+bytes,0,FESTRING_LOCALSIZE-bytes);
		}
	}
	else
	{
		m_pRep=newRep();
		m_pRep->newBuffer(bytes);
		FEASSERT(m_pRep->m_pBuffer);
		FESTRCPY(m_pRep->m_pBuffer,bytes,operand);
		memset(m_pLocal,0,FESTRING_LOCALSIZE);
	}
}

inline String::String(const FESTRING_I8* operand)
{
	if(!operand)
	{
		init();
		return;
	}
	create(operand);
#if FESTRING_DEBUG
	if(m_pRep)
	{
		fe_fprintf(stderr,
				"String::String(FESTRING_I8*) create 0x%p 0x%p \"%s\"\n",
				this,m_pRep,m_pRep->m_pBuffer);
	}
#endif
}

inline String::String(const FESTRING_U8* operand)
{
	if(!operand)
	{
		init();
		return;
	}
	create((const FESTRING_I8*)operand);
#if FESTRING_DEBUG
	if(m_pRep)
	{
		fe_fprintf(stderr,
				"String::String(FESTRING_U8*) create 0x%p 0x%p \"%s\"\n",
				this,m_pRep,m_pRep->m_pBuffer);
	}
#endif
}

inline String::String(int operand)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	sprintf_s((FESTRING_I8*)m_pLocal,16,"%-15d",operand);
#else
	sprintf((FESTRING_I8*)m_pLocal,"%-15d",operand);
#endif
	m_length=FESTRLEN(m_pLocal);
	m_pRep=NULL;
}

inline String::~String(void)
{
	if(m_pRep)
	{
		FEASSERT(m_pRep->m_references>0);

#ifdef FE_COUNT_ASM_IMPL
		if(!(feAsmSwapIncr((int*)(&m_pRep->m_references),-1)-1))
#else
		if(--m_pRep->m_references == 0)
#endif
		{
#if FESTRING_DEBUG
			fe_fprintf(stderr,"~String destroy 0x%p 0x%p \"%s\"\n",
					this,m_pRep,m_pRep->m_pBuffer);
#endif
			deleteRep();
		}
	}
}

inline String &String::operator=(const FESTRING_U8* operand)
{
	if(m_pRep)
	{
		FEASSERT(m_pRep->m_references>0);

#ifdef FE_COUNT_ASM_IMPL
		if(!(feAsmSwapIncr((int*)(&m_pRep->m_references),-1)-1))
#else
		if(--m_pRep->m_references == 0)
#endif
		{
#if FESTRING_DEBUG
			fe_fprintf(stderr,"String::operator=(FESTRING_U8*)"
					" destroy 0x%p 0x%p \"%s\"\n",
					this,m_pRep,m_pRep->m_pBuffer);
#endif
			deleteRep();
		}

		m_pRep=NULL;

/*
		// still in use elsewhere
		if(m_pRep->m_references>1)
		{
			// abandon old m_pRep (other ref will clean)
			m_pRep->m_references--;
			m_pRep=NULL;
		}
		else
		{
#if FESTRING_DEBUG
			fe_fprintf(stderr,"String::operator=(FESTRING_U8*)"
					" destroy 0x%p 0x%p \"%s\"\n",
					this,m_pRep,m_pRep->m_pBuffer);
#endif
			deleteRep();
		}
*/
	}

	create((const FESTRING_I8*)operand);
#if FESTRING_DEBUG
	if(m_pRep)
	{
		fe_fprintf(stderr,"String::operator=(FESTRING_U8*) create 0x%p 0x%p %s\n",
				this,m_pRep,m_pRep->m_pBuffer);
	}
#endif
	return *this;
}

inline String &String::operator=(const String &operand)
{
	// protect against string=string
	if(operand.m_pRep)
	{
		FEASSERT(operand.m_pRep->m_references>=0);

#ifdef FE_COUNT_ASM_IMPL
		feAsmIncr((int*)(&operand.m_pRep->m_references),1);
#else
		operand.m_pRep->m_references++;
#endif
	}

	if(m_pRep)
	{
		FEASSERT(m_pRep->m_references>0);

#ifdef FE_COUNT_ASM_IMPL
		if(!(feAsmSwapIncr((int*)(&m_pRep->m_references),-1)-1))
#else
		if(--m_pRep->m_references == 0)
#endif
		{
#if FESTRING_DEBUG
			fe_fprintf(stderr,
					"String::operator=(String) destroy 0x%p 0x%p \"%s\"\n",
					this,m_pRep,m_pRep->m_pBuffer);
#endif
			deleteRep();
		}
	}

	m_length=operand.m_length;

	if(&m_pLocal[0]==&operand.m_pLocal[0])
	{
#if FESTRING_DEBUG
		fe_fprintf(stderr,"String::operator=(FESTRING_U8*)"
				" string=string (local) \"%s\"\n",m_pLocal);
#endif
		return *this;
	}

	FEASSERT(m_pLocal>operand.m_pLocal+FESTRING_LOCALSIZE ||
			operand.m_pLocal>m_pLocal+FESTRING_LOCALSIZE);

//	memcpy(m_pLocal,operand.m_pLocal,FESTRING_LOCALSIZE);

#if FE_CPLUSPLUS >= 201103L
		U64* p1=reinterpret_cast<U64*>(m_pLocal);
		const U64* p2=reinterpret_cast<const U64*>(operand.m_pLocal);
		p1[0]=p2[0];
		p1[1]=p2[1];
#else
		U32* p1=reinterpret_cast<U32*>(m_pLocal);
		const U32* p2=reinterpret_cast<const U32*>(operand.m_pLocal);
		p1[0]=p2[0];
		p1[1]=p2[1];
		p1[2]=p2[2];
		p1[3]=p2[3];
#endif

	m_pRep=operand.m_pRep;

#if FESTRING_DEBUG
	if(m_pRep)
	{
		fe_fprintf(stderr,
				"String::operator=(FESTRING_U8*)"
				" copy 0x%p 0x%p \"%s\"\n",
				this,m_pRep,m_pRep->m_pBuffer);
	}
#endif
	return *this;
}

//* TODO check that this works for multibyte
inline U32 String::length(void) const
{
	return m_length;
}

inline BWORD String::empty(void) const
{
	return !m_pLocal[0] && !m_pRep;
}

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif

#if defined(__GNUC__) && ( __GNUC__==4 && __GNUC_MINOR__>=2 && __GNUC_MINOR__<5)
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif
inline BWORD String::equals(const String &operand) const
{
	if(m_length!=operand.m_length)
	{
		return FALSE;
	}

#if FESTRING_LOCALSIZE==16
	if(!m_pRep && !operand.m_pRep)
	{
#ifdef FE_SSE_STRING_GNU
		//* 4xI32 compare -> for each 32 bits, all set if equal, all zero if not
		v4si comp=__builtin_ia32_pcmpeqd128(
				*(v4si*)m_pLocal,
				*(v4si*)operand.m_pLocal);

		U32* pComp4=reinterpret_cast<U32*>(&comp);

#if FALSE
		fe_printf("pComp4 0x%x 0x%x 0x%x 0x%x\n",
				pComp4[0],pComp4[1],pComp4[2],pComp4[3]);

		long unsigned int* pComp2=reinterpret_cast<long unsigned int*>(&comp);
		fe_printf("\n\"%s\" \"%s\" %d\n",m_pLocal,operand.m_pLocal,
				!~(pComp2[0]&pComp2[1]));
#endif

		//* NOTE 64-bit int version does not optimize correctly
//		return ((pComp2[0]&pComp2[1])==0xFFFFFFFFFFFFFFFF);
//		return (!~(pComp2[0]&pComp2[1]));

//		return ((pComp4[0]&pComp4[1]&pComp4[2]&pComp4[3])==0xFFFFFFFF);
		return (!~(pComp4[0]&pComp4[1]&pComp4[2]&pComp4[3]));
#elif FE_CPLUSPLUS >= 201103L
		const U64* p1=reinterpret_cast<const U64*>(m_pLocal);
		const U64* p2=reinterpret_cast<const U64*>(operand.m_pLocal);
		return (p1[0]==p2[0] && p1[1]==p2[1]);
#else

		const U32* p1=reinterpret_cast<const U32*>(m_pLocal);
		const U32* p2=reinterpret_cast<const U32*>(operand.m_pLocal);
		return (p1[0]==p2[0] && p1[1]==p2[1] &&
				p1[2]==p2[2] && p1[3]==p2[3]);
#endif
	}
#endif

	return !FESTRCMP_MB(rawU8(),operand.rawU8());
}

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

inline I32 String::compare(const String &operand) const
{
	return FESTRCMP_MB(rawU8(),operand.rawU8());
}

inline String& String::cat(const String& operand)
{
	return cat(operand.c_str());
}

/// Compare two String's
inline BWORD operator == (const String& s1, const String& s2)
{
	return s1.equals(s2);
}

/// Compare a byte buffer to an String
inline BWORD operator == (const char* s1, const String& s2)
{
	return ((const String)s1).equals(s2);
}

/// Compare an String to a byte buffer
inline BWORD operator == (const String& s1, const char* s2)
{
	return s1.equals((const String)s2);
}

/// Compare two String's (reverse logic)
inline BWORD operator != (const String& s1, const String& s2)
{
	return !s1.equals(s2);
}

/// Compare a byte buffer to an String (reverse logic)
inline BWORD operator != (const char* s1, const String& s2)
{
	return !((const String)s1).equals(s2);
}

/// Compare an String to a byte buffer (reverse logic)
inline BWORD operator != (const String& s1, const char* s2)
{
	return !s1.equals((const String)s2);
}

inline String operator+(const String& s1, const String& s2)
{
	String result=s1;
	return result.cat(s2);
}

inline String operator+(const String& s1, const int& i)
{
	String result;
	return result.sPrintf("%s%d",s1.c_str(),i);
}

inline String operator+(const int& i, const String& s1)
{
	String result;
	return result.sPrintf("%d%s",i,s1.c_str());
}

inline String operator+(const String& s1, const float& f)
{
	String result;
	return result.sPrintf("%s%.6G",s1.c_str(),f);
}

inline String operator+(const float& f, const String& s1)
{
	String result;
	return result.sPrintf("%.6G%s",f,s1.c_str());
}

/**************************************************************************//**
	@brief Return a string for an FE_ERRNO code.

	@ingroup platform
*//***************************************************************************/
FE_DL_PUBLIC String errorString(int errnum);

inline String print(const String& a_rString)
{
	return a_rString;
}

inline String print(char* a_buffer)
{
	return String(a_buffer);
}

inline String print(U32 a_integer)
{
	String text;
	text.sPrintf("%d",a_integer);
	return text;
}

inline String print(I32 a_integer)
{
	String text;
	text.sPrintf("%d",a_integer);
	return text;
}

#if FALSE
inline String print(int a_integer)
{
	String text;
	text.sPrintf("%d",a_integer);
	return text;
}
#endif

inline String print(F32 a_float)
{
	String text;
	text.sPrintf("%.6G",a_float);
	return text;
}

inline String print(F64 a_double)
{
	String text;
	text.sPrintf("%.6G",a_double);
	return text;
}

/** @fn const FESTRING_I8* c_print(const T& t)

	@brief Converts any object to a char* buffer

	The argument type must support:

	@code
	String print(const T&);
	@endcode

	The returned buffer is not persistent and can be invalid
	after the statement.
	It should be only used immediately such as within a printf() statement.

	This function may be implemented with a macro.
*/
#define c_print(x)	fe::print(x).c_str()

} /* namespace */

#endif /* __platform_string_h__ */
