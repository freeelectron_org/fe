import os
import sys
forge = sys.modules["forge"]

def setup(module):
    module.summary = []

    pcre_include = os.environ["FE_PCRE_INCLUDE"]
    pcre_lib = os.environ["FE_PCRE_LIB"]

    if pcre_include == "":
        module.summary += [ "-" + str(pcre_version) ]

    pcre_config = ""
    if pcre_lib == 'auto':
        pcre_config = "pcre-config"
    else:
        pcre_config = pcre_lib + "/pcre-config"
        if not os.path.exists(pcre_config):
            pcre_config = pcre_lib + "/../bin/pcre-config"
            if not os.path.exists(pcre_config):
                pcre_config = ""

    pcre_version = ""
    if pcre_config != "":
        pcre_check = os.popen(pcre_config + " --version",'r')
        pcre_version = pcre_check.read()
        pcre_check.close()

    if pcre_version != "":
        pcre_version = float(pcre_version)
        module.summary += [ str(pcre_version) ]

    srcList = [ "pcre.pmh",
                "pcreDL" ]

    module.includemap = {}

    if pcre_lib != 'auto':
        module.includemap['pcre'] = pcre_include

    dll = module.DLL( "fexPcre", srcList )

    deplibs = forge.basiclibs[:]

    dll.linkmap = {}

    if pcre_lib != 'auto':
        dll.linkmap['pcre'] = "-Wl,-rpath='" + pcre_lib + "'"
        dll.linkmap['pcre'] += ' -L' + pcre_lib

    dll.linkmap["pcrecpp"] = "-lpcrecpp"

    forge.deps( ["fexPcreLib"], deplibs )

def auto(module):
    pcre_include = os.environ["FE_PCRE_INCLUDE"]
    pcre_lib = os.environ["FE_PCRE_LIB"]

    if pcre_include == '':
        return 'unset'

    test_file = """
#include <pcrecpp.h>

int main(void)
{
    return pcrecpp::RE("a?c").FullMatch("abc");
}
    \n"""

    scenemap = { }
    if forge.fe_os == "FE_LINUX":
        scenemap["scenelibs"] = "-lpcrecpp"

    if pcre_include != 'auto':
        forge.includemap['pcre'] = pcre_include

        forge.linkmap['pcre'] = "-Wl,-rpath='" + pcre_lib + "'"
        forge.linkmap['pcre'] += ' -L' + pcre_lib + ' -lpcrecpp'

    result = forge.cctest(test_file, scenemap)

    if pcre_include != 'auto':
        forge.includemap.pop('pcre', None)
        forge.linkmap.pop('pcre', None)

    return result
