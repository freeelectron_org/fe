#! /usr/bin/python3

import ctypes
import ctypes.util

def fe_execute(command):
    size = 256
    result = ctypes.create_string_buffer(size)

    print "\n[36m" + command + "[0m"
    fe_terminal_execute(size,result,command.encode('utf-8'))
    print "[34m" + result.value.decode('utf-8') + "[0m"

print "finding"

found = ctypes.util.find_library("fexTerminalDL")

print found

print "loading"

fe_dl = ctypes.CDLL("libfexTerminalDL.so", mode=ctypes.RTLD_GLOBAL)

print "loaded"

print fe_dl

fe_terminal_execute = fe_dl.fe_terminal_execute

print fe_terminal_execute

fe_commands = open('ext/terminal/test/wrap_test.fesh','r')

fe_command = fe_commands.readline()
while fe_command:
    if len(fe_command) > 1 and fe_command[0] != '#':
        fe_execute(fe_command.strip())
    fe_command = fe_commands.readline()
