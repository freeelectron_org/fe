INFO 5
ATTRIBUTE name string "=unset"
ATTRIBUTE tire:model string
ATTRIBUTE tire:model:upper string
ATTRIBUTE tire:model:lower string
ATTRIBUTE tire:slow_blend vector4 "=60.0 10.0 100.0 1.0"
ATTRIBUTE tire:radius real "=0.22"
ATTRIBUTE tire:width real "=0.3"

ATTRIBUTE spc:transform transform "=0 0 0 0 0 0 0 0 0 0 0 0"
ATTRIBUTE moa:contact spatial_vector "=0 0 -10000"
ATTRIBUTE spc:normal spatial_vector

ATTRIBUTE tire:z:stiffness				real		=200000
ATTRIBUTE tire:z:damping				real		=1000
ATTRIBUTE tire:component				string
ATTRIBUTE tire:friction					real		=0.43
ATTRIBUTE tire:xy:stiffness				real		=10.1

ATTRIBUTE tire:stick:bias	real		=0.5
ATTRIBUTE tire:stick:stiffness	real	=1.0e2

LAYOUT tire
	name
	tire:radius
	tire:width
	tire:model

	spc:transform
	moa:contact
	spc:normal

LAYOUT stick_tire_model
	name
	tire:component
	tire:z:stiffness
	tire:z:damping
	tire:friction
	tire:stick:bias
	tire:stick:stiffness
	tire:xy:stiffness
	
LAYOUT blend_tire_model
	name
	tire:component
	tire:model:upper
	tire:model:lower
	tire:slow_blend

DEFAULTGROUP 1

RECORD * stick_tire_model
	name "Stick Default"
	tire:component "TireI.Stick.fe"
	tire:friction			1.5
	tire:stick:bias			0.5
	tire:stick:stiffness	1.0e5
	tire:xy:stiffness		1.0e2

RECORD * blend_tire_model
	name "Blend Default"
	tire:component "TireI.Blend.fe"
	tire:model:lower "Stick Default"
	tire:model:upper "Linear Default"
	tire:slow_blend "60.0 10.0 100.0 1.0"

DEFAULTGROUP 2


#RECORD PT_tire tire
#	tire:xxx "TireI.Pacejka.fe '951.964 -1296.21 1048.86'"
#	name "was PT tire"
#	tire:radius 0.22
#	tire:model "Pacejka Default"


END
