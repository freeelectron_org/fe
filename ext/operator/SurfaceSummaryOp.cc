/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SSP_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceSummaryOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("probeAttribute")="P";
	catalog<String>("probeAttribute","label")="Probe Attribute";

	catalog<String>("probeClass")="Point";
	catalog<String>("probeClass","choice:0")="Point";
	catalog<String>("probeClass","choice:1")="Primitive";
	catalog<String>("probeClass","label")="Probe Class";

	catalog<I32>("probeIndex")=0;
	catalog<I32>("probeIndex","high")=100;
	catalog<I32>("probeIndex","max")=INT_MAX;
	catalog<String>("probeIndex","label")="Probe Index";

	catalog<String>("probeString");
	catalog<String>("probeString","IO")="output";
	catalog<String>("probeString","label")="String";

	catalog<I32>("probeInteger")=0;
	catalog<I32>("probeInteger","low")=0;
	catalog<I32>("probeInteger","high")=10;
	catalog<I32>("probeInteger","min")=INT_MIN;
	catalog<I32>("probeInteger","max")=INT_MAX;
	catalog<String>("probeInteger","IO")="output";
	catalog<String>("probeInteger","label")="Integer";

	catalog<Real>("probeReal")=0.0;
	catalog<Real>("probeReal","low")=0.0;
	catalog<Real>("probeReal","high")=1.0;
	catalog<Real>("probeReal","min")=-1e38;
	catalog<Real>("probeReal","max")=1e38;
	catalog<String>("probeReal","IO")="output";
	catalog<String>("probeReal","label")="Real";

	catalog<SpatialVector>("probeVector");
	catalog<String>("probeVector","IO")="output";
	catalog<String>("probeVector","label")="Vector";

	catalog<I32>("pointCount");
	catalog<I32>("pointCount","max")=INT_MAX;
	catalog<String>("pointCount","IO")="output";
	catalog<String>("pointCount","label")="Point Count";

	catalog<I32>("primitiveCount");
	catalog<I32>("primitiveCount","max")=INT_MAX;
	catalog<String>("primitiveCount","IO")="output";
	catalog<String>("primitiveCount","label")="Primitive Count";

	catalog< sp<Component> >("Input Surface");
}

void SurfaceSummaryOp::handle(Record& a_rSignal)
{
#if FE_SSP_DEBUG
	feLog("SurfaceSummaryOp::handle\n");
#endif

	I32& pointCount=catalog<I32>("pointCount");
	I32& primitiveCount=catalog<I32>("primitiveCount");

	const String probeClass=catalog<String>("probeClass");
	const String probeAttribute=catalog<String>("probeAttribute");
	const I32 probeIndex=catalog<I32>("probeIndex");

	String& probeString=catalog<String>("probeString");
	I32& probeInteger=catalog<I32>("probeInteger");
	Real& probeReal=catalog<Real>("probeReal");
	SpatialVector& probeVector=catalog<SpatialVector>("probeVector");

	sp<SurfaceAccessibleI> spInputSurface;
	if(!access(spInputSurface,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputElement;
	access(spInputElement,spInputSurface,e_point,e_generic,e_warning);

	pointCount=(spInputElement.isValid())? spInputElement->count(): -1;

#if FE_SSP_DEBUG
	feLog("SurfaceSummaryOp::handle pointCount=%d\n",pointCount);
#endif

	access(spInputElement,spInputSurface,e_primitive,e_vertices,e_warning);

	primitiveCount=(spInputElement.isValid())? spInputElement->count(): -1;

#if FE_SSP_DEBUG
	feLog("SurfaceSummaryOp::handle primitiveCount=%d\n",primitiveCount);
#endif

	bool probePoint=(probeClass=="Point");
	I32 probeMax=probePoint? pointCount: primitiveCount;

	probeString="inaccessible";
	probeReal=Real(0);
	set(probeVector);
	probeInteger=0;

	if(probeIndex<0 || probeIndex>=probeMax)
	{
		probeString="bad index";
	}
	else
	{
		access(spInputElement,spInputSurface,
				probePoint? e_point: e_primitive,probeAttribute,e_warning);

		if(spInputElement.isValid())
		{
			const String probeType=spInputElement->type();
#if FE_SSP_DEBUG
			feLog("SurfaceSummaryOp::handle probeType \"%s\"\n",
					probeType.c_str());
#endif

			probeString="bad type";

			if(probeType=="string")
			{
				probeString=spInputElement->string(probeIndex,0);
				probeReal=probeString.real();
				probeVector[0]=probeReal;
				probeInteger=probeString.integer();
			}
			else if(probeType=="integer")
			{
				const I32 integer=spInputElement->integer(probeIndex,0);

				probeString=c_print(integer);
				probeReal=Real(integer);
				probeVector[0]=probeReal;
				probeInteger=integer;
			}
			else if(probeType=="real")
			{
				const Real value=spInputElement->real(probeIndex,0);

				probeString=c_print(value);
				probeReal=value;
				probeVector[0]=value;
				probeInteger=I32(value);
			}
			else if(probeType.match("vector."))
			{
				const SpatialVector vector=
						spInputElement->spatialVector(probeIndex,0);

				probeString=c_print(vector);
				probeReal=magnitude(vector);
				probeVector=vector;
				probeInteger=probeReal;
			}
#if FE_SSP_DEBUG
			feLog("SurfaceSummaryOp::handle probeString \"%s\"\n",
					probeString.c_str());
#endif
		}
	}

#if FE_SSP_DEBUG
	feLog("SurfaceSummaryOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
