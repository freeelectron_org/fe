/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("xCatalogLibrary");
		UNIT_TEST(successful(result));

		sp<Catalog> spCatalog(spRegistry->create("Dictionary"));
		UNIT_TEST(spCatalog.isValid());
		if(!spCatalog.isValid())
		{
			feX(argv[0], "NULL Catalog");
		}

		spCatalog->catalog<String>("a string")="some words";

/*
		spCatalog->catalogSet("a real", (Arg[]){
				{1.2f,"min"},
				3.4f,
				{5.6f,"max"},
				{"text","label"},
				(void*)NULL });
*/

		spCatalog->catalog<I32>("integer")= -7;
		spCatalog->catalog<String>("integer","label")="An Integer";
		spCatalog->catalog<I32>("integer","min")=2;
		spCatalog->catalog<I32>("integer","max")=8;

		I32 value=spCatalog->catalog<I32>("integer");
		feLog("value %d\n", value);
		UNIT_TEST(value== -7);

		feLog("\n");
		spCatalog->catalogDump();

		spCatalog->catalog<Real>("float")=1.23;
		spCatalog->catalog<String>("float","label")="A Float";

		spCatalog->catalogDump();

		value=spCatalog->catalogOrDefault<I32>("integer",13);
		feLog("value %d\n", value);
		UNIT_TEST(value== -7);

		feLog("\n");
		spCatalog->catalogDump();

		spCatalog->catalogRemove("integer");
		UNIT_TEST(!spCatalog->cataloged("integer"));

		spCatalog->catalogDump();

		value=spCatalog->catalogOrDefault<I32>("integer",13);
		feLog("value %d\n", value);
		UNIT_TEST(value==13);
		UNIT_TEST(!spCatalog->cataloged("integer"));

		feLog("\n");
		spCatalog->catalogDump();

		spCatalog->catalogSet("ints","value","intarray","");
		CatalogBuffer::resize(spCatalog,"ints","value",8);
		I32* pIntBuffer(NULL);
		const I32 intCount=CatalogBuffer::access(spCatalog,
				"ints","value",(void*&)pIntBuffer);
		UNIT_TEST(intCount==8);
		UNIT_TEST(pIntBuffer);
		for(I32 intIndex=0;intIndex<intCount;intIndex++)
		{
			pIntBuffer[intIndex]=1000+intIndex;
		}

		feLog("\n");
		spCatalog->catalogDump();

		sp<Catalog> spCatalogCopy=spCatalog->catalogDeepCopy();
		spCatalogCopy->catalog<String>("a string")="other words";
		feLog("Original\n");
		spCatalog->catalogDump();
		feLog("Deep Copy\n");
		spCatalogCopy->catalogDump();

		UNIT_TEST(spCatalog->catalog<String>("a string")=="some words");
		UNIT_TEST(spCatalogCopy->catalog<String>("a string")=="other words");

		UNIT_TEST(spMaster->catalog().isValid());

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(14);
	UNIT_RETURN();
}
