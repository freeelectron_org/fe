#pragma once

namespace fe
{
namespace ext
{

class FE_DL_EXPORT ConnectionI:
	virtual public fe::Component,
	public fe::CastableAs<ConnectionI>
{
	public:
virtual	bool	connect(const bool asServer, const std::string &address,
					const short port)										=0;

virtual	bool	sendReliable(const std::vector<char> &message)				=0;
virtual	bool	sendReliable(const char *message, const int size)			=0;

virtual	bool	sendUnreliable(const std::vector<char> &message)			=0;
virtual	bool	sendUnreliable(const char *message, const int size)			=0;

virtual	bool	receiveBlocking(std::vector<char> &message)					=0;
virtual	bool	receiveNonblocking(std::vector<char> &message,
					const std::uint_fast32_t msWait)						=0;

virtual	bool	connected(void)												=0;
virtual	bool	disconnect(void)											=0;
};

} /* namespace ext */
} /* namespace fe */
