/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_PickPoint_h__
#define __viewer_PickPoint_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Interactive specification of a 3D point

	@ingroup viewer

	PickPoint expects WindowEvent, but it checks safely.

	The fourth element is non-zero when the point is valid.
*//***************************************************************************/
class FE_DL_EXPORT PickPoint: virtual public HandlerI,
		virtual public PointI
{
	public:
						PickPoint(void);
virtual					~PickPoint(void)									{}

						//* As HandlerI
virtual void			handle(Record& record);

						//* As PointI
virtual SpatialVector&	point(const WindowEvent::Mask& mask);
virtual
const	SpatialVector&	point(const WindowEvent::Mask& mask) const;

virtual	U32				flags(void) const				{ return m_flags; }

virtual	void			setView(sp<ViewI> spViewI)		{ m_spViewI=spViewI; }
virtual	void			drawFeedback(sp<DrawI> spDrawI) const;

	private:
		U32				itemIndex(const WindowEvent::Mask& mask) const;

		WindowEvent		m_event;
		sp<ViewI>		m_spViewI;
		I32				m_mouseState[2];
		U32				m_flags;
		Real			m_zValue;
		SpatialVector	m_feedback;
		SpatialVector	m_point[3][2];
		sp<DrawMode>	m_spHalo;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_PickPoint_h__ */
