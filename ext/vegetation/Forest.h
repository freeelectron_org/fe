/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_Forest_h__
#define __vegetation_Forest_h__

FE_ATTRIBUTE("sim:collider",	"Spherical obstacle (x,y,z,r)");
FE_ATTRIBUTE("sim:effectForce",	"Force on effector");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Forest RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT Forest: public RecordView
{
	public:
		Functor< sp<Component> >	collider;
		Functor<SpatialTransform>	colliderTransform;
		Functor<SpatialVector>		effectorVelocity;
		Functor<SpatialVector>		effectForce;
		Functor<SpatialVector>		gravity;
		Functor<SpatialVector>		uniformVelocity;
		Functor< sp<RecordGroup> >	plants;
		Functor< sp<Component> >	pointI;
		Functor< sp<Component> >	drawI;
		Functor< sp<Component> >	cameraControllerI;
		Functor<String>				stickForm;
		Functor<String>				leafForm;
		Functor<I32>				serial;
		Functor<I32>				upAxis;
		Functor<Real>				time;
		Functor<Real>				deltaT;

				Forest(void)		{ setName("Forest"); }
virtual	void	addFunctors(void)
				{
					add(collider,			FE_USE("sim:collider"));
					add(colliderTransform,	FE_USE("sim:colliderTransform"));
					add(effectorVelocity,	FE_USE("sim:effectorVel"));
					add(effectForce,		FE_USE("sim:effectForce"));
					add(gravity,			FE_USE("sim:gravity"));
					add(uniformVelocity,	FE_USE("sim:uniformVel"));

					add(plants,				FE_SPEC("veg:plants",
							"RecordGroup of vegetation"));

					add(pointI,				FE_USE("ren:PointI"));
					add(drawI,				FE_USE("ren:DrawI"));
					add(cameraControllerI,	FE_USE("ren:CameraControllerI"));
					add(stickForm,			FE_USE("ren:stickForm"));
					add(leafForm,			FE_USE("ren:leafForm"));

					add(serial,				FE_USE(":SN"));
					add(upAxis,				FE_USE("sim:upAxis"));
					add(time,				FE_USE("sim:time"));
					add(deltaT,				FE_USE("sim:timestep"));
				}
virtual	void	initializeRecord(void)
				{
					plants.createAndSetRecordGroup();

					setIdentity(colliderTransform());
					set(effectorVelocity());
					set(effectForce());
					set(gravity());
					set(uniformVelocity());

					stickForm()="line";
					leafForm()="point";
					upAxis()=2;
					time()=0.0f;
					deltaT()=(1.0f/60.0f);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_Forest_h__ */
