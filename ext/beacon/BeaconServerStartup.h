/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconServerStartup_h__
#define __BeaconServerStartup_h__

namespace beacon
{

/***************************************************************************//**
	@brief Node registion and simulation time sycronization

	@ingroup beacon
*//****************************************************************************/
class FE_DL_EXPORT BeaconServerStartup : virtual public BeaconServer
{
public:
	virtual bool startup(const char *fileName) override;
};

}  // namespace beacon

#endif // __BeaconServerStartup_h__
