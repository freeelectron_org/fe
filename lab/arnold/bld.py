import os
import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["terminal"]

def setup(module):
    module.summary = []

    arnoldArch = ""
    arnoldMajor = ""
    arnoldMinor = ""
    arnoldFix = ""

    arnold_include = os.environ["FE_ARNOLD_INCLUDE"]
    arnold_lib = os.environ["FE_ARNOLD_LIB"]

    for line in open(arnold_include + "/ai_version.h").readlines():
        if re.match("#define AI_VERSION_ARCH_NUM.*", line):
            arnoldArch = line.split()[2].rstrip()
        if re.match("#define AI_VERSION_MAJOR_NUM.*", line):
            arnoldMajor = line.split()[2].rstrip()
        if re.match("#define AI_VERSION_MINOR_NUM.*", line):
            arnoldMinor = line.split()[2].rstrip()
        if re.match("#define AI_VERSION_FIX.*", line):
            arnoldFix = line.split()[2].rstrip()[1:-1]

    version = arnoldArch + "." + arnoldMajor + "." + arnoldMinor + "." + arnoldFix
    if version != "...":
        module.summary += [ version ]

    srcList = [ "ArnoldNode",
                "ArnoldShader",
                "ShaderVariablesArnold",
                "arnold.pmh",
                "arnoldDL" ]

    dll = module.DLL( "fexArnoldDL", srcList )

    module.includemap = {}
    module.includemap['arnold'] = arnold_include

    dll.linkmap = {}
    dll.linkmap['arnold'] = "-Wl,-rpath='" + arnold_lib + "'"
    dll.linkmap['arnold'] = '-L' + arnold_lib + ' -lai'

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib",
                "fexTerminalDLLib"  ]

    forge.deps( ["fexArnoldDLLib"], deplibs )

    arnoldPath = os.path.join(forge.libPath, 'arnold')
    if os.path.isdir(arnoldPath) == 0:
        os.makedirs(arnoldPath, 0o755)

    dest = os.path.join(arnoldPath, 'libfexArnoldDL.so')
    if os.path.lexists(dest) == 0:
        os.symlink("../libfexArnoldDL.so", dest)


def auto(module):
    arnold_include = os.environ["FE_ARNOLD_INCLUDE"]
    arnold_lib = os.environ["FE_ARNOLD_LIB"]

    if arnold_include == '' or arnold_lib == '':
        return 'unset'

    test_file = """
#include "ai.h"

int main(void)
{
    return(0);
}
    \n"""

    forge.includemap['arnold'] = arnold_include

#   forge.linkmap['arnold'] = "-Wl,-rpath='" + arnold_lib + "'"
#   forge.linkmap['arnold'] = '-L' + arnold_lib + ' -lai'

    result = forge.cctest(test_file)

    forge.includemap.pop('arnold', None)
    forge.linkmap.pop('arnold', None)

    return result
