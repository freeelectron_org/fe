/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <usd/usd.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSurfaceDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<SurfaceAccessibleUsd>(
			"SurfaceAccessibleI.SurfaceAccessibleUsd.fe.usd");
	pLibrary->add<SurfaceAccessibleUsd>(
			"SurfaceAccessibleI.SurfaceAccessibleUsd.fe.usda");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
