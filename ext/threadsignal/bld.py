import os
import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "signal", "thread" ]

def setup(module):
    srcList = [ "threadsignal.pmh",
                "ThreadSignaler",
                "threadsignalDL"
                ]

    dll = module.DLL( "fexThreadSignalDL", srcList )

    deplibs = forge.corelibs+ [
            "fexSignalLib",
            "fexThreadDLLib" ]

    forge.deps( ["fexThreadSignalDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexThreadSignalDL",            None,   None) ]

    module.Module('test')
