/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "xCatalog.h"
#include "platform/dlCore.cc"

using namespace fe;

FE_DL_EXPORT void fe_catalog_library(I32 a_value)
{
	feLog("fe_catalog_library %d\n",a_value);
}

extern "C"
{

FE_DL_EXPORT Library *CreateLibrary(sp<Master>)
{
	Library *pLibrary = new Library();
	pLibrary->add<Dictionary>("Dictionary.Dictionary.test");
	return pLibrary;
}

}
