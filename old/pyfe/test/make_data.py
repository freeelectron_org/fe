#!/usr/bin/python

import fe
import fe.data
import fe.signal

def make(scope):

    # create a particle layout
    particle_layout = scope.declare("particle")
    particle_layout.populate("x", "real");
    particle_layout.populate("y", "real");
    particle_layout.populate("z", "real");

    # create a record group
    rg = fe.data.RecordGroup();
    rg2 = fe.data.RecordGroup();

    # populate the record group
    for i in range(0,2):
        p = scope.createRecord(particle_layout)
        rg2.add(p)
    for i in range(0,5):
        p = scope.createRecord(particle_layout)
        p.x = float(i)
        p.y = float(i)
        p.z = 0.0
        rg.add(p)
    rg2 = 0
    for i in range(0,5):
        p = scope.createRecord(particle_layout)
        p.x = float(i)
        p.y = float(i)
        p.z = 0.0
        rg.add(p)

    return rg

def print_record_group(rg):
    pass

