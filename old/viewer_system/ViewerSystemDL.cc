/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer_system.pmh"

#include "platform/dlCore.cc"

using namespace fe;
using namespace ext;

extern "C"
{
    FE_DL_EXPORT void ListDependencies(List<String *> &list)
    {
        list.append(new String("feAutoLoadDL"));
        list.append(new String("fexNativeWindowDL"));
        list.append(new String("fexOpenGLDL"));
    }

    FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
    {
        sp<BaseType> spT;

        spT = spMaster->typeMaster()->assertType<std::thread::id>("threadId");
		spT->setInfo(new InfoVoid());

		spT = spMaster->typeMaster()->assertType<fe::hp<fe::ext::DrawI>>("draw");
		spT->setInfo(new InfoVoid());

		Library *pLibrary = Memory::instantiate<Library>();

        pLibrary->add<MOAViewer>("ThreadedViewerI.MOAViewer.fe");

        pLibrary->add<OriginViewerSystem>("ViewerSystem.OriginViewerSystem.fe");

        return pLibrary;
    }

    FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
    {
    }
}
