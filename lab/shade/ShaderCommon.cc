/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <shade/shade.pmh>

using namespace fe;
using namespace fe::ext;

void ShaderCommon::update(void)
{
	feLog("ShaderCommon::update\n");
}
