import os
import sys
import string
import glob
forge = sys.modules["forge"]

# zmq on Windows:
# install vcpkg in your FE_WINDOWS_LOCAL
# see https://github.com/Microsoft/vcpkg/
# vcpkg.exe install zmq:x86-windows-static-md
# vcpkg.exe install zmq:x64-windows-static-md

def prerequisites():
    return [ "network" ]

def build_lines() -> (str, str):
    zmq_include = os.environ["FE_ZMQ_INCLUDE"]
    zmq_lib = os.environ["FE_ZMQ_LIB"]

    include_line = ""
    link_line = ""

    if forge.fe_os == "FE_LINUX":
        if zmq_include != "" and zmq_lib != "":
            include_line += zmq_include

            link_line = " -Wl,-rpath='" + zmq_lib + "'"
            link_line += ' -L' + zmq_lib

        link_line += " -lzmq"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            pattern = "libzmq-*-sgd-*.lib"
        else:
            pattern = "libzmq-*-s-*.lib"

        if zmq_include != "" and zmq_lib != "":
            full_pattern = os.path.join(zmq_lib, pattern)
        else:
            full_pattern = os.path.join(forge.vcpkg_lib, pattern)

        link_line = ' '.join(glob.glob(full_pattern))
        link_line += " Iphlpapi.lib Rpcrt4.lib"

    return include_line, link_line

def setup(module):

    srclist = [ "zeromq.pmh",
                "ZeroCatalog",
                "ZeroConnection",
                "zeromqDL",
                ]

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexNetworkDLLib" ]

    dll = module.DLL( "fexZeroMQDL", srclist)

    module.includemap = {}
    dll.linkmap = {}

    module.includemap['zmq'], dll.linkmap['zmq'] = build_lines()

    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs += [ "feDataLib" ]

    forge.deps( ["fexZeroMQDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexZeroMQDL",                  None,       None) ]

    module.properties = { "zmq_libs": dll.linkmap['zmq'] }

    module.Module('test')

def auto(module):
    zmq_include = os.environ["FE_ZMQ_INCLUDE"]
    zmq_lib = os.environ["FE_ZMQ_LIB"]

    forge.includemap['zmq'], forge.linkmap['zmq'] = build_lines()

    test_file = """
#define ZMQ_STATIC
#include <zmq.h>

int main(void)
{
    zmq_ctx_new();
    return(0);
}
    \n"""

    result = forge.cctest(test_file)

    forge.includemap.pop('zmq', None)
    forge.linkmap.pop('zmq', None)
    forge.linkmap.pop('zmq_libs', None)

    return result
