/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

Library* CreateViewerLibrary(sp<Master> spMaster)
{
	fe::assertData(spMaster->typeMaster());
	fe::assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->setName("default_viewer");
	pLibrary->add<InspectController>("CameraControllerI.InspectController.fe");
	pLibrary->add<ObjectViewer>("ViewerI.ObjectViewer.fe");
	pLibrary->add<OrthoViewer>("ViewerI.OrthoViewer.fe");
	pLibrary->add<PerspectiveViewer>("ViewerI.PerspectiveViewer.fe");
	pLibrary->add<DrawSelection>("HandlerI.DrawSelection.fe");
	pLibrary->add<QuickViewer>("QuickViewerI.QuickViewer.fe");
	pLibrary->add<SelectController>("HandlerI.SelectController.fe");
	pLibrary->add<DebugController>("HandlerI.DebugController.fe");
	pLibrary->add<WindowController>("HandlerI.WindowController.fe");
	pLibrary->add<ChannelController>("HandlerI.ChannelController.fe");
	pLibrary->add<ChannelFilter>("HandlerI.ChannelFilter.fe");
	pLibrary->add<ChannelBinder>("HandlerI.ChannelBinder.fe");
	pLibrary->add<EventMap>("HandlerI.EventMap.fe");
	pLibrary->add<MaskMap>("MaskMapI.MaskMap.fe");
	pLibrary->add<PickPoint>("HandlerI.PickPoint.fe");
	pLibrary->add<ProjectPoint>("HandlerI.ProjectPoint.fe");
	pLibrary->add<IntersectRaySphere>("IntersectRayI.Sphere.fe");
	pLibrary->add<IntersectPointTriangle>("IntersectPointI.Triangle.fe");
	pLibrary->add<IntersectRectPoint>("IntersectRectI.Point.fe");
	pLibrary->add<VideoRecorder>("RecorderI.VideoRecorder.fe");
	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
