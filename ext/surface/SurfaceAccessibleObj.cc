/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SAO_DEBUG		FALSE
#define	FE_SAO_Z_IS_UP		FALSE

namespace fe
{
namespace ext
{

//* TODO allow 5+ verts

BWORD SurfaceAccessibleObj::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAO_DEBUG
	feLog("SurfaceAccessibleObj::load \"%s\"\n",a_filename.c_str());
#endif

	ensureBinding();

	std::ifstream stream;

	stream.open(a_filename.c_str(),std::fstream::in);
	if(!stream.good() && a_filename.c_str()[0]!='/')
	{
		const String path=
				registry()->master()->catalog()->catalog<String>("path:media")+
				"/"+a_filename;
		stream.open(path.c_str(),std::fstream::in);
	}

	if(!stream.good())
	{
		feLog("SurfaceAccessibleObj::load failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	BWORD splitIndices=FALSE;
	BWORD hasColor=FALSE;
	I32 vertexCount=0;
	I32 normalCount=0;
	I32 uvCount=0;
	I32 faceCount=0;
	char line[256];
	char type[256];
	char face0[256];
	char face1[256];
	char face2[256];
	char face3[256];
	char face4[256];

	//~ first pass: just count
	while(stream.good())
	{
		type[0]=0;
		stream.getline(line,256);
		sscanf(line,"%s",type);

		if(!strcmp(type,"g"))
		{
			char objname[256];

			objname[0]=0;
			sscanf(line,"%8s%64s",type,objname);

			setName(objname);
		}
		if(!strcmp(type,"v"))
		{
			vertexCount++;

			if(!hasColor)
			{
				float x(0);
				float y(0);
				float z(0);
				int n(0);
				sscanf(line,"%8s%f%f%f%n",type,&x,&y,&z,&n);
				const char* post=line+n;
				hasColor=(strlen(post)>1);
			}
		}
		if(!strcmp(type,"vn"))
		{
			normalCount++;
		}
		if(!strcmp(type,"vt"))
		{
			uvCount++;
		}
		if(!strcmp(type,"f"))
		{
			if(!splitIndices)
			{
				Vector4i id;
				sscanf(line,"%8s%d/%d/%d/%d",type,&id[0],&id[1],&id[2],&id[3]);

				if(id[1]>0 && id[2]>0)
				{
					splitIndices=TRUE;
				}
			}

			faceCount++;
		}
	}

#if FE_SAO_DEBUG
	feLog("SurfaceAccessibleObj::load"
			" vertices=%d normals=%d uvs=%d faces=%d split=%d\n",
			vertexCount,normalCount,uvCount,faceCount,splitIndices);
#endif

	//* back to beginning
	stream.clear();
	stream.seekg(0,std::ios::beg);

	I32 vertexIndex=0;
	I32 normalIndex=0;
	I32 uvIndex=0;
	I32 faceIndex=0;

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_createMissing;

	sp<SurfaceAccessorI> spOutputPoint=accessor(e_point,e_position,creation);
	sp<SurfaceAccessorI> spOutputVertices=
			accessor(e_primitive,e_vertices,creation);

	sp<SurfaceAccessorI> spOutputColor;
	sp<SurfaceAccessorI> spOutputNormal;
	sp<SurfaceAccessorI> spOutputUV;
	sp<SurfaceAccessorI> spOutputNormalId;
	sp<SurfaceAccessorI> spOutputUVId;

//	sp<SurfaceAccessorI> spOutputProperties=
//			accessor(e_primitive,e_properties,creation);

	if(hasColor)
	{
		spOutputColor=accessor(e_point,e_color,creation);
	}

	SpatialVector* pNormals(NULL);
	SpatialVector* pUVs(NULL);
	if(splitIndices)
	{
		pNormals=new SpatialVector[normalCount];
		pUVs=new SpatialVector[uvCount];

		if(normalCount)
		{
			spOutputNormal=accessor(e_vertex,e_normal,creation);
			spOutputNormalId=accessor(e_vertex,"nid",creation);
		}

		if(uvCount)
		{
			spOutputUV=accessor(e_vertex,e_uv,creation);
			spOutputUVId=accessor(e_vertex,"tid",creation);
		}
	}
	else
	{
		if(normalCount)
		{
			spOutputNormal=accessor(e_point,e_normal,creation);
		}
		if(uvCount)
		{
			spOutputUV=accessor(e_point,e_uv,creation);
		}
	}

	if(pNormals)
	{
		for(I32 index=0;index<normalCount;index++)
		{
			set(pNormals[index]);
		}
	}
	if(pUVs)
	{
		for(I32 index=0;index<uvCount;index++)
		{
			set(pUVs[index]);
		}
	}

	//~ second pass: convert
	while(!stream.eof())
	{
		stream.getline(line,256);

		type[0]=0;
		sscanf(line,"%8s",type);

		if(!strcmp(type,"g"))
		{
			char objname[256];

			objname[0]=0;
			sscanf(line,"%8s%64s",type,objname);

			setName(objname);
		}

		if(!strcmp(type,"v"))
		{
			float x(0);
			float y(0);
			float z(0);
			int n(0);
			sscanf(line,"%8s%f%f%f%n",type,&x,&y,&z,&n);


			SpatialVector fileVertex;
#if FE_SAO_Z_IS_UP
			set(fileVertex,x,-z,y);
#else
			set(fileVertex,x,y,z);
#endif

#if FE_SAO_DEBUG
			feLog("vertex %d/%d %s\n",vertexIndex,vertexCount,
					c_print(fileVertex));
#endif

			if(I32(spOutputPoint->count())<=vertexIndex)
			{
				spOutputPoint->append(1);
			}
			spOutputPoint->set(vertexIndex,fileVertex);

			if(spOutputColor.isValid())
			{
				//* optional RGB color with three additional floats
				const char* post=line+n;
				float r(1);
				float g(1);
				float b(1);
				if(strlen(post)>1)
				{
					sscanf(post,"%f%f%f",&r,&g,&b);
				}

				spOutputColor->set(vertexIndex,Color(r,g,b));
			}

			vertexIndex++;
		}
		if(!strcmp(type,"vn"))
		{
			float x(0);
			float y(0);
			float z(0);
			sscanf(line,"%8s%f%f%f",type,&x,&y,&z);

			SpatialVector fileNormal;
#if FE_SAO_Z_IS_UP
			set(fileNormal,x,-z,y);
#else
			set(fileNormal,x,y,z);
#endif

#if FE_SAO_DEBUG
			feLog("normal %d/%d %s\n",normalIndex,normalCount,
					c_print(fileNormal));
#endif

			if(pNormals)
			{
				if(normalIndex<normalCount)
				{
					pNormals[normalIndex++]=fileNormal;
				}
			}
			else if(normalIndex<vertexIndex)
			{
				spOutputNormal->set(normalIndex++,fileNormal);
			}
		}
		if(!strcmp(type,"vt"))
		{
			float u(0);
			float v(0);
			float w(0);
			sscanf(line,"%8s%f%f%f",type,&u,&v,&w);

			SpatialVector fileUV(u,v,w);

#if FE_SAO_DEBUG
			feLog("uv %d/%d %s\n",uvIndex,uvCount,c_print(fileUV));
#endif

			if(pUVs)
			{
				if(uvIndex<uvCount)
				{
					pUVs[uvIndex++]=fileUV;
				}
			}
			else if(uvIndex<vertexIndex)
			{
				spOutputUV->set(uvIndex++,fileUV);
			}
		}
		if(!strcmp(type,"f"))
		{
			face0[0]=0;
			face1[0]=0;
			face2[0]=0;
			face3[0]=0;
			face4[0]=0;

			sscanf(line,"%8s%s%s%s%s%s",type,face0,face1,face2,face3,face4);

			Vector<5,I32> vid;
			Vector<5,I32> tid;
			Vector<5,I32> nid;

			set(vid);
			set(tid);
			set(nid);

			sscanf(face0,"%d/%d/%d",&vid[0],&tid[0],&nid[0]);
			sscanf(face1,"%d/%d/%d",&vid[1],&tid[1],&nid[1]);
			sscanf(face2,"%d/%d/%d",&vid[2],&tid[2],&nid[2]);

			const I32 subCount=face4[0]? 5: (face3[0]? 4: 3);

			if(subCount>3)
			{
				sscanf(face3,"%d/%d/%d",&vid[3],&tid[3],&nid[3]);
			}

			if(subCount>4)
			{
				sscanf(face4,"%d/%d/%d",&vid[4],&tid[4],&nid[4]);
			}

			if(!vid[0] || !vid[1] || !vid[2] ||
					vid[0]==vid[1] || vid[0]==vid[2] || vid[1]==vid[2] ||
					vid[0]==vid[3] || vid[1]==vid[3] || vid[2]==vid[3] ||
					vid[0]==vid[4] || vid[1]==vid[4] || vid[2]==vid[4])
			{
				feLog("SurfaceAccessibleObj::load bad face %d %d %d %d %d\n",
						vid[0],vid[1],vid[2],vid[3],vid[4]);
				continue;
			}

			if(tid[0]<0 || tid[1]<0 || tid[2]<0 || tid[3]<0 || tid[4]<0)
			{
				feLog("SurfaceAccessibleObj::load bad face %d %d %d %d %d\n",
						vid[0],vid[1],vid[2],vid[3],vid[4]);
				feLog("  tid %d %d %d %d %d\n",
						tid[0],tid[1],tid[2],tid[3],tid[4]);
				continue;
			}

			if(nid[0]<0 || nid[1]<0 || nid[2]<0 || nid[3]<0 || nid[4]<0)
			{
				feLog("SurfaceAccessibleObj::load bad face %d %d %d %d %d\n",
						vid[0],vid[1],vid[2],vid[3],vid[4]);
				feLog("  nid %d %d %d %d %d\n",
						nid[0],nid[1],nid[2],nid[3],nid[4]);
				continue;
			}

			const I32 primitiveIndex=spOutputVertices->append();

#if FE_SAO_DEBUG
			feLog("face %d: %d %d %d %d %d (%d)\n",primitiveIndex,
					vid[0],vid[1],vid[2],vid[3],vid[4],subCount);
			feLog("  tid %d %d %d %d %d of %d\n",
					tid[0],tid[1],tid[2],tid[3],tid[4],uvCount);
			feLog("  nid %d %d %d %d %d of %d\n",
					nid[0],nid[1],nid[2],nid[3],nid[4],normalCount);
#endif

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				spOutputVertices->append(primitiveIndex,I32(vid[subIndex]-1));

				if(splitIndices)
				{
					if(nid[subIndex]<=normalCount)
					{
						spOutputNormal->set(primitiveIndex,subIndex,
								pNormals[nid[subIndex]-1]);
						spOutputNormalId->set(primitiveIndex,subIndex,
								nid[subIndex]);
					}

					if(tid[subIndex]<=uvCount)
					{
						spOutputUV->set(primitiveIndex,subIndex,
								pUVs[tid[subIndex]-1]);
						spOutputUVId->set(primitiveIndex,subIndex,
								tid[subIndex]);
					}
				}
			}

			faceIndex++;
		}
	}

	if(!splitIndices)
	{
		if(spOutputNormal.isValid() && normalCount<vertexCount)
		{
			feLog("SurfaceAccessibleObj::load"
					" missing normals will be set to unit Z\n");

			for(I32 file_normal=normalCount;file_normal<I32(vertexCount);
					file_normal++)
			{
				spOutputNormal->set(file_normal,SpatialVector(0,0,1));
			}
		}
		if(spOutputUV.isValid() && uvCount<vertexCount)
		{
			feLog("SurfaceAccessibleObj::load"
					" missing uvs will be zeroed\n");

			for(I32 file_uv=uvCount;file_uv<I32(vertexCount);
					file_uv++)
			{
				spOutputUV->set(file_uv,SpatialVector(0,0,0));
			}
		}
	}

	if(splitIndices)
	{
		delete[] pUVs;
		delete[] pNormals;
	}

	stream.close();

	feLog("SurfaceAccessibleObj::load name \"%s\" points %d prims %d\n",
			verboseName().c_str(),spOutputPoint->count(),
			spOutputVertices->count());

#if FALSE
	save("output/test.obj",sp<Catalog>(NULL));
	SurfaceAccessibleRecord::save("output/test.rg",sp<Catalog>(NULL));
#endif

	return TRUE;
}

BWORD SurfaceAccessibleObj::save(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAO_DEBUG
	feLog("SurfaceAccessibleObj::save \"%s\"\n",a_filename.c_str());
#endif

	if(!m_weakRecord.isValid())
	{
		feX(e_notInitialized,"no data");
	}

	std::ofstream stream(a_filename.c_str());
	if(!stream.is_open())
	{
		feLog("SurfaceAccessibleObj::save failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	stream<<"# OBJ file created by fe::SurfaceAccessibleObj\n";
	stream<<"#\n";
	stream<<"g Object001\n";

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_refuseMissing;

	sp<SurfaceAccessorI> spOutputPoint=accessor(e_point,e_position,creation);
	sp<SurfaceAccessorI> spOutputColor=accessor(e_point,e_color,creation);
	sp<SurfaceAccessorI> spOutputVertices=
			accessor(e_primitive,e_vertices,creation);

	sp<SurfaceAccessorI> spOutputNormal=accessor(e_point,e_normal,creation);
	sp<SurfaceAccessorI> spOutputUV=accessor(e_point,e_uv,creation);
	sp<SurfaceAccessorI> spOutputNormalId;
	sp<SurfaceAccessorI> spOutputUVId;
	BWORD splitIndices(FALSE);

	if(spOutputNormal.isNull() && spOutputUV.isNull())
	{
		spOutputNormal=accessor(e_vertex,e_normal,creation);
		spOutputNormalId=accessor(e_vertex,"nid",creation);

		spOutputUV=accessor(e_vertex,e_uv,creation);
		spOutputUVId=accessor(e_vertex,"tid",creation);

		if(spOutputNormal.isValid() || spOutputUV.isValid())
		{
			splitIndices=TRUE;
		}
	}

#if FE_SAO_DEBUG
	if(splitIndices)
	{
		feLog("SurfaceAccessibleObj::save splitting indices\n");
	}
#endif

	if(spOutputPoint.isNull())
	{
		feLog("SurfaceAccessibleObj::save no points in surface\n");
		return FALSE;
	}

	stream<<"\n";

	String line;

	const I32 pointCount=spOutputPoint->count();
	if(spOutputColor.isValid())
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			line.sPrintf("v %s %s\n",
					c_print(spOutputPoint->spatialVector(pointIndex)),
					c_print(spOutputColor->spatialVector(pointIndex)));
			stream<<line.c_str();
		}
	}
	else
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			line.sPrintf("v %s\n",
					c_print(spOutputPoint->spatialVector(pointIndex)));
			stream<<line.c_str();
		}
	}

#if FE_SAO_DEBUG
	feLog("SurfaceAccessibleObj::save pointCount %d\n",pointCount);
#endif

	const I32 primCount=spOutputVertices->count();

	if(splitIndices)
	{
		if(spOutputVertices.isNull())
		{
			feLog("SurfaceAccessibleObj::save split indices without faces\n");
			return FALSE;
		}

		const I32 maxCount=primCount*4;
		SpatialVector* pNormals=new SpatialVector[maxCount];
		SpatialVector* pUVs=new SpatialVector[maxCount];

		for(I32 index=0;index<maxCount;index++)
		{
			set(pNormals[index]);
			set(pUVs[index]);
		}

		I32 normalIndex(0);
		I32 uvIndex(0);

		I32 normalMax(-1);
		I32 uvMax(-1);

		for(I32 primIndex=0;primIndex<primCount;primIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primIndex);

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				if(spOutputNormal.isValid())
				{
					const SpatialVector norm=
							spOutputNormal->spatialVector(primIndex,subIndex);
					I32 nid(normalIndex++);

					if(spOutputNormalId.isValid())
					{
						nid=spOutputNormalId->integer(primIndex,subIndex);
					}

					if(normalMax<nid)
					{
						normalMax=nid;
					}

//					feLog("pr %d/%d sub %d/%d normalIndex %d nid %d\n",
//							primIndex,primCount,subIndex,subCount,
//							normalIndex,nid);

					pNormals[nid]=norm;
				}

				if(spOutputUV.isValid())
				{
					const SpatialVector uv=
							spOutputUV->spatialVector(primIndex,subIndex);
					I32 nid(uvIndex++);

					if(spOutputUVId.isValid())
					{
						nid=spOutputUVId->integer(primIndex,subIndex);
					}

					if(uvMax<nid)
					{
						uvMax=nid;
					}

					pUVs[nid]=uv;
				}
			}
		}

#if FE_SAO_DEBUG
		feLog("SurfaceAccessibleObj::save normalMax %d uvMax %d\n",
				normalMax,uvMax);
#endif

		for(I32 normalIndex=0;normalIndex<=normalMax;normalIndex++)
		{
			line.sPrintf("vn %s\n",c_print(pNormals[normalIndex]));
			stream<<line.c_str();
		}

		for(I32 uvIndex=0;uvIndex<=uvMax;uvIndex++)
		{
			line.sPrintf("vt %s\n",c_print(pUVs[uvIndex]));
			stream<<line.c_str();
		}

#if FE_SAO_DEBUG
	feLog("SurfaceAccessibleObj::save primCount %d\n",primCount);
#endif

		normalIndex=0;
		uvIndex=0;

		for(I32 primIndex=0;primIndex<primCount;primIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primIndex);

			if(subCount>3)
			{
				line.sPrintf("f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d\n",
						spOutputVertices->integer(primIndex,0)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,0): uvIndex+1,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,0): normalIndex+1,

						spOutputVertices->integer(primIndex,1)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,1): uvIndex+2,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,1): normalIndex+2,

						spOutputVertices->integer(primIndex,2)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,2): uvIndex+3,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,2): normalIndex+3,

						spOutputVertices->integer(primIndex,3)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,3): uvIndex+4,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,3): normalIndex+4);
				stream<<line.c_str();
			}
			else if(subCount>2)
			{
				line.sPrintf("f %d/%d/%d %d/%d/%d %d/%d/%d\n",
						spOutputVertices->integer(primIndex,0)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,0): uvIndex+1,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,0): normalIndex+1,

						spOutputVertices->integer(primIndex,1)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,1): uvIndex+2,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,1): normalIndex+2,

						spOutputVertices->integer(primIndex,2)+1,
						spOutputUVId.isValid()?
						spOutputUVId->integer(primIndex,2): uvIndex+3,
						spOutputNormalId.isValid()?
						spOutputNormalId->integer(primIndex,2): normalIndex+3);
				stream<<line.c_str();
			}
			else
			{
				feLog("SurfaceAccessibleObj::save"
						" prim %d/%d subCount %d\n",
						primIndex,primCount,subCount);
			}
			normalIndex+=subCount;
			uvIndex+=subCount;
		}

		delete[] pUVs;
		delete[] pNormals;
	}
	else
	{
		if(spOutputNormal.isValid())
		{
			stream<<"\n";

			const I32 normalCount=spOutputNormal->count();
			for(I32 normalIndex=0;normalIndex<normalCount;normalIndex++)
			{
				line.sPrintf("vn %s\n",
						c_print(spOutputNormal->spatialVector(normalIndex)));
			stream<<line.c_str();
			}
		}

		if(spOutputUV.isValid())
		{
			stream<<"\n";

			const I32 uvCount=spOutputUV->count();
			for(I32 uvIndex=0;uvIndex<uvCount;uvIndex++)
			{
				line.sPrintf("vt %s\n",
						c_print(spOutputUV->spatialVector(uvIndex)));
			stream<<line.c_str();
			}
		}

		if(spOutputVertices.isValid())
		{
			stream<<"\n";

#if FE_SAO_DEBUG
			feLog("SurfaceAccessibleObj::save primCount %d\n",primCount);
#endif

			for(I32 primIndex=0;primIndex<primCount;primIndex++)
			{
				const I32 subCount=spOutputVertices->subCount(primIndex);

				if(subCount>3)
				{
					line.sPrintf("f %d %d %d %d\n",
							spOutputVertices->integer(primIndex,0)+1,
							spOutputVertices->integer(primIndex,1)+1,
							spOutputVertices->integer(primIndex,2)+1,
							spOutputVertices->integer(primIndex,3)+1);
					stream<<line.c_str();
				}
				else if(subCount>2)
				{
					line.sPrintf("f %d %d %d\n",
							spOutputVertices->integer(primIndex,0)+1,
							spOutputVertices->integer(primIndex,1)+1,
							spOutputVertices->integer(primIndex,2)+1);
					stream<<line.c_str();
				}
				else
				{
					feLog("SurfaceAccessibleObj::save"
							" prim %d/%d subCount %d\n",
							primIndex,primCount,subCount);
				}
			}
		}
	}

#if FE_SAO_DEBUG
	feLog("SurfaceAccessibleObj::save complete\n");
#endif

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
