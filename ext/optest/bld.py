import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["operator"]

def setup(module):
    srcList = [ "optest.pmh",
                "AttachOp",
                "BrushTestOp",
                "DrawTestOp",
                "IdleSpinOp",
                "JunkOp",
                "MimicOp",
                "SurfaceDrawOp",
                "SurfaceWaveOp",
                "optestDL" ]

    dll = module.DLL( "fexOpTestDL", srcList )

    deplibs = forge.corelibs+ [
                "fexOperateDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexOpTestDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOpTestDL",                  None,   None) ]
