require("util")

function window_populate()
	l_win_data:populate(create("PopulateI.AsOrtho"))
	l_win_data:populate(create("PopulateI.AsSelection"))
end

function window_create()

	manage("fexDatauiDL")

	c_window:setTitle("xArchitecture Test App -- Debug Window")

	c_ortho = install(c_window, "ViewerI.OrthoViewer",
		{l_draw_hb})
	c_ortho:bind(c_window)
	r_win_data['ortho:zoom'] = 1.0

	c_scopeDebug = install(c_window, "HandlerI.ScopeDebug",
		{l_draw_hb,l_event})
	c_scopeDebug['scope'] = componentScope(l_draw_hb:scope())
	c_scopeDebug['draw'] = l_draw_hb


	c_catalogDebug = install(c_window, "HandlerI.CatalogDebug",
		{l_draw_hb,l_event})
	c_catalogDebug['draw'] = l_draw_hb
	c_catalogDebug['offset'][0] = 300
	c_catalogDebug['offset'][1] = 200
	c_catalogDebug["margin"] = 20
	c_catalogDebug["label"] = "CATALOG"

	rg_test = createRecordGroup()
	rg_test:add(r_win_data)
	c_rgDebug = install(c_window, "HandlerI.RecordGroupDebug",
		{l_draw_hb,l_event})
	c_rgDebug['draw'] = l_draw_hb
	c_rgDebug['group'] = rg_test
	c_rgDebug['offset'][0] = 300
	c_rgDebug['offset'][1] = 170

	c_maskMapDebug = install(c_window, "HandlerI.MaskMapDebug",
		{l_draw_hb,l_event})
	c_maskMapDebug['draw'] = l_draw_hb
	c_maskMapDebug['offset'][0] = 0
	c_maskMapDebug['offset'][1] = 0
	c_maskMapDebug['label'] = "mask map"
	c_maskMapDebug:maskEnable(0)
	c_maskMapDebug:maskInit("popup")
	c_maskMapDebug:maskSet("popup_enable",
		WindowEvent.sourceKeyboard,
		string.byte('\t'),
		WindowEvent.statePress)
	c_maskMapDebug:maskSet("popup_disable",
		WindowEvent.sourceKeyboard,
		string.byte('\t'),
		WindowEvent.stateRelease)

	c_windowController = install(c_window, "HandlerI.WindowController",
		{l_event})


end
