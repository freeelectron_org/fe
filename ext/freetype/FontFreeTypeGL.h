
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opengl_FontFreeTypeGL_h__
#define __opengl_FontFreeTypeGL_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief OpenGL-specific implementations for FontI

	@ingroup freetype
*//***************************************************************************/
class FE_DL_EXPORT FontFreeTypeGL: public FontI,
		public Initialize<FontFreeTypeGL>
{
	public:
				FontFreeTypeGL(void);
virtual			~FontFreeTypeGL(void);

		void	initialize(void);

virtual	I32		fontHeight(I32* a_pAscent,I32* a_pDescent)
				{
					if(a_pAscent)
					{
						*a_pAscent=m_fontAscent;
					}
					if(a_pDescent)
					{
						*a_pDescent=m_fontDescent;
					}

					return m_fontAscent+m_fontDescent;
				}
virtual	I32		pixelWidth(String a_string);

virtual	void	drawAlignedText(sp<Component> a_spDrawComponent,
					const SpatialVector& a_location,
					const String a_text,const Color &a_color);

	private:
		void	add_text(const SpatialVector& a_location,
					const String a_text,const Color &a_color);

		String				m_freetypePath;
		String				m_fontName;
		Real				m_fontSize;
		BWORD				m_fixedWidth;
		I32					m_maxWidth;

		I32					m_fontAscent;
		I32					m_fontDescent;

		Real				m_multiplication;

		texture_atlas_t*	m_pAtlas;
		texture_font_t*		m_pFont;
		vertex_buffer_t*	m_pBuffer;
		GLuint				m_shader;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opengl_FontFreeTypeGL_h__ */
