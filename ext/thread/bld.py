import sys
import os
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "thread.pmh",
                "SpannedRange",
                "WorkGang",
                "threadDL" ]

    # OpenMP
    if int(os.environ["FE_USE_OMP"]):
        srcList += [ "WorkOmp" ]

        module.cppmap = {}
        module.cppmap['omp'] = '-fopenmp'

        module.linkmap = {}
        module.linkmap['omp'] = '-lgomp'

    dll = module.DLL( "fexThreadDL", srcList )

    deplibs = forge.corelibs[:]

    forge.deps( ["fexThreadDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexThreadDL",                  None,   None) ]

    module.Module('test')
