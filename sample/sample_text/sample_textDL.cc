/** @file */

#include <sample_text/sample_text.pmh>

#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<BruteScan>("StringScanI.BruteScan.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

