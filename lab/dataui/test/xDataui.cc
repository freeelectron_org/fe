/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"
#include "window/WindowEvent.h"
#include "architecture/architecture.h"

using namespace fe;
using namespace fe::ext;

class Flush : public HandlerI
{
	public:
				Flush(void) { }
virtual			~Flush(void) { }

virtual void	handle(Record &r_sig)
		{
			m_asSignal.bind(r_sig.layout()->scope());
			m_asWindata.bind(r_sig.layout()->scope());
			if(!m_asSignal.winData.check(r_sig)) { return; }
			Record r_windata = m_asSignal.winData(r_sig);

			sp<WindowI> spWindow = m_asWindata.windowI(r_windata);
			if(spWindow.isValid())
			{
				sp<DrawI> spDrawI(m_asWindata.drawI(r_windata));
				if(spDrawI.isValid())
				{
					if(spDrawI->view().isValid())
					{
						spDrawI->view()->setWindow(spWindow);
						spDrawI->flush();
					}
				}
			}
		}
	private:
		AsWindata						m_asWindata;
		AsSignal						m_asSignal;
};


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	I32 frames=(argc>1)? atoi(argv[1])+1: -1;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		assertMath(spMaster->typeMaster());

		spRegistry->manage("fexViewerDL");
		spRegistry->manage("fexDatauiDL");
		spRegistry->manage("fexNativeWindowDL");
		spRegistry->manage("fexArchitectureDL");
		spRegistry->manage("fexOpenGLDL");

		// application level scope
		sp<Scope> spAppScope(spRegistry->create("Scope"));

		// application level signaler
		sp<SignalerI> spAppSignaler = spRegistry->create("SignalerI");

		// create sequencer
		sp<SequencerI>	spAppSequencer(spRegistry->create("SequencerI"));

		// heartbeat signal to push sequencer
		sp<Layout> l_seq_hb = spAppScope->declare("l_seq_hb");
		spAppSignaler->insert(spAppSequencer, l_seq_hb);


		//-----------------------------------------------
		//-----------------------------------------------
		//-----------------------------------------------

		sp<Scope> spWinScope(spRegistry->create("Scope"));

		// window event heartbeat
		sp<Layout> l_winev_hb = spWinScope->declare("l_winev_hb");
#ifdef FE_LOCK_SUPPRESSION
		l_winev_hb->suppressLock(true);
#endif

		// draw heartbeat
		sp<Layout> l_draw_hb = spWinScope->declare("l_draw_hb");
#ifdef FE_LOCK_SUPPRESSION
		l_draw_hb->suppressLock(true);
#endif

		// window data layout
		sp<Layout> l_win_data = spWinScope->declare("l_win_data");
		AsWindata asWindata;
		asWindata.populate(l_win_data);
		AsSelection asSelection;
		asSelection.populate(l_win_data);
		l_draw_hb->populate(FE_USE("win:data"), "record");

		///////////////

		// WindowEvent
		sp<Layout> l_event = spWinScope->declare("WindowEvent");
		l_event->populate(FE_USE("win:data"), "record");

		//////////////
		Record r_win_data;
		sp<WindowI>	spWindow(spRegistry->create("WindowI"));
		sp<EventContextI> spEC = spWindow->getEventContextI();
		spAppSignaler->insert(spEC, l_winev_hb);
		spAppSignaler->insert(spWindow, l_draw_hb);

		// annotate window data to draw and event signals
		sp<AnnotateI> spAnnotateWinData(
			spRegistry->create("HandlerI.Annotate"));
		spAnnotateWinData->adjoin(spWindow);
		sp<SignalerI> spSignaler(spWindow);
		spSignaler->insert(spAnnotateWinData, l_draw_hb);
		spSignaler->insert(spAnnotateWinData, l_event);

		// set the background color
		sp<HandlerI> spFlush(new Flush());
		spFlush->adjoin(spSignaler);
		spSignaler->insert(spFlush, l_draw_hb);

		// create the window data record itself and set the attributes
		r_win_data = l_draw_hb->scope()->createRecord(l_win_data);
		spAnnotateWinData->attach(FE_USE("win:data"), r_win_data);
		asWindata.bind(l_draw_hb->scope());
#if FE_OS == FE_OSX
		sp<DrawI> spDraw(spRegistry->create("DrawI"));
#else
		sp<DrawI> spDraw(spRegistry->create("*.DrawThreaded"));
#endif
		asWindata.drawI(r_win_data) = spDraw;
		asWindata.windowI(r_win_data) = spWindow;
		asWindata.group(r_win_data) = new RecordGroup();

		// =============================================================
		// START Example User Window Pipeline
		// =============================================================

#if 1
		// Orthographic for UI elements
		sp<Component> spOrtho(spRegistry->create("ViewerI.OrthoViewer"));
		spOrtho->adjoin(spSignaler);
		spSignaler->insert(spOrtho, l_draw_hb);
		sp<ViewerI>(spOrtho)->bind(spWindow);
#endif

#if 0
		// add a window controller
		sp<Component> spWindowController(
				spRegistry->create("HandlerI.WindowController"));
		spWindowController->adjoin(spSignaler);
		spSignaler->insert(spWindowController, l_event);

		// add a selection controller
		sp<Component> spSelectController(
				spRegistry->create("HandlerI.SelectController"));
		spSelectController->adjoin(spSignaler);
		spSignaler->insert(spSelectController, l_event);
#endif

#if 1
		// add a ScopeDebug component to the window pipeline
		sp<Component> spScopeDB(spRegistry->create("HandlerI.ScopeDebug"));
		spScopeDB->adjoin(spSignaler);
		spSignaler->insert(spScopeDB, l_draw_hb);
		spSignaler->insert(spScopeDB, l_event);
		sp<Config> spConfig(spScopeDB);
		spConfig->cfg<String>("dummy key") = "dummy value";
		spConfig->cfg< sp<Layout> >("draw") = l_draw_hb;
		spConfig->cfg< sp<Component> >("scope") = sp<Component>(spWinScope);
		spConfig->cfg< Vector<2,Real> >("offset") = Vector<2,Real>(50,200);
		spConfig->cfg<String>("label") = String("This Window Scope");
		spConfig->cfg<Real>("margin") = 10;
#endif

#if 0
		// add a CatalogDebug component to the window pipeline
		sp<Component> spCatalogDB(spRegistry->create("HandlerI.CatalogDebug"));
		spCatalogDB->adjoin(spSignaler);
		spSignaler->insert(spCatalogDB, l_draw_hb);
		spSignaler->insert(spCatalogDB, l_event);
		sp<DspatchI>(spCatalogDB)->call("setDraw", l_draw_hb);
		sp<DspatchI>(spCatalogDB)->call("setOffset", (Real)0, (Real)0);
		sp<DspatchI>(spCatalogDB)->call("setLabel", String("Catalog"));

		// add a MaskMapDebug component to the window pipeline
		sp<Component> spMaskMapDB(spRegistry->create("HandlerI.MaskMapDebug"));
		spMaskMapDB->adjoin(spSignaler);
		spSignaler->insert(spMaskMapDB, l_draw_hb);
		spSignaler->insert(spMaskMapDB, l_event);
		sp<DspatchI>(spMaskMapDB)->call("setDraw", l_draw_hb);
		sp<DspatchI>(spMaskMapDB)->call("setOffset", (Real)200, (Real)0);
		sp<DspatchI>(spMaskMapDB)->call("setLabel", String("MaskMap"));

		// add a selection draw
		sp<Component> spSelectDraw(spRegistry->create("HandlerI.DrawSelection"));
		spSelectDraw->adjoin(spSignaler);
		spSignaler->insert(spSelectDraw, l_draw_hb);
		spSignaler->insert(spSelectDraw, l_event);

		// add stuff to catalog for the fun of it
		spMaster->catalog(spScopeDB, "Scope Debug Component");
		spMaster->catalog(spCatalogDB, "Catalog Debug Component");
		spMaster->catalog(spMaskMapDB, "MaskMap Debug Component");
#endif

		// =============================================================
		// END Example User Window Pipeline
		// =============================================================

#if 0
		// =============================================================
		// START mask wiring, which is best to happen after pipeline
		// since component creation is what sets up masks to be wired
		// =============================================================

		// turn selection drawing on and off based on selection
		// actually happening
		sp<ReactorI>(spSelectController)->add("on_selecting", spSignaler,
				sp<MaskI>(spSelectController)->maskCreate(l_event,
				"fe_drawselection_enable"));
		sp<ReactorI>(spSelectController)->add("off_selecting", spSignaler,
				sp<MaskI>(spSelectController)->maskCreate(l_event,
				"fe_drawselection_disable"));
#endif

		// =============================================================
		// END mask wiring
		// =============================================================

		// open the window
		spWindow->open("xDataui");

		// window event heartbeat
		AsSequenceSignal asSequenceSignal;
		asSequenceSignal.bind(spWinScope);
#ifdef FE_LOCK_SUPPRESSION
		l_winev_hb->suppressLock(false);
#endif
		Record r_winev_hb = spAppSequencer->add(l_winev_hb,
			spAppSequencer->getCurrentTime(), 50);
		asSequenceSignal.count(r_winev_hb) = 0;
		asSequenceSignal.periodic(r_winev_hb) = 0;

		// draw heartbeat
#ifdef FE_LOCK_SUPPRESSION
		l_draw_hb->suppressLock(false);
#endif
		Record r_draw_hb = spAppSequencer->add(l_draw_hb,
			spAppSequencer->getCurrentTime(), 50);
		asSequenceSignal.count(r_draw_hb) = 0;
		asSequenceSignal.periodic(r_draw_hb) = 0;


#if 0
		// application control
		// 0 == quit, 1 == run, 2 == pause
		sp<IntegerI> spRunLevel = spMaster->catalog("IntegerI", "run:level");
		spRunLevel->integer() = 1;
#endif

		//-----------------------------------------------
		//-----------------------------------------------
		//-----------------------------------------------

		// sequencer heartbeat
		Record r_seq_hb = spAppScope->createRecord(l_seq_hb);

#if 0
		while(frames != 0 && spRunLevel->integer() != 0)
		{
			spAppSignaler->signal(r_seq_hb);
			frames--;
		}
#else
		while(frames != 0)
		{
			spAppSignaler->signal(r_seq_hb);
			frames--;
		}
#endif

		// de-cycle references
		spOrtho->disjoin();
		spScopeDB->disjoin();
		asWindata.drawI(r_win_data) = NULL;
		asWindata.windowI(r_win_data) = NULL;
		asWindata.group(r_win_data) = NULL;

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(2);
	UNIT_RETURN();
}

