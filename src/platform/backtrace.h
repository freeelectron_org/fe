/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_backtrace_h__
#define __platform_backtrace_h__

/**************************************************************************//**
	@brief Get the program stack

	The default @em skip of 2 will hide the calls to
	feSegvHandler and feLogBacktrace.
*//***************************************************************************/
FE_DL_PUBLIC fe::String feGenerateBacktrace(U32 skip=2);

/**************************************************************************//**
	@brief Dump the program stack
*//***************************************************************************/
FE_DL_PUBLIC void feLogBacktrace(U32 skip=2);

/**************************************************************************//**
	@brief Start debugger if env FE_SEGV_ATTACH is 1
*//***************************************************************************/
FE_DL_PUBLIC void feAttachDebugger(void);

/**************************************************************************//**
	@brief Register a built-in handler for SIGSEGV
*//***************************************************************************/
FE_DL_PUBLIC void feRegisterSegvHandler(void);

#endif /* __platform_backtrace_h__ */
