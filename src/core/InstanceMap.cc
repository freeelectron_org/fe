/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

void InstanceMap::deepCopy(const InstanceMap& r_other)
{
	for(std::map< String, Instance >::const_iterator it=r_other.begin();
			it!=r_other.end();it++)
	{
		Instance& rInstance=(*this)[it->first];
		rInstance.create(it->second.type());
		rInstance.assign(it->second);
	}
}

} /* namespace */
