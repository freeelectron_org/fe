import os
import sys
forge = sys.modules["forge"]

class DocTarget(forge.Target):
    def __init__(self,str,doxyfile):
            forge.Target.__init__(self,str)
            self.doxyfile = doxyfile
            self.help = """\
Generate documentation
"""
    def Build(self, reference_time, for_name):
        forge.Target.Build(self, reference_time, for_name)
        cwd = os.getcwd()
        os.chdir(os.path.join(forge.rootPath,'doc'))
        attrgen = os.path.join(forge.rootPath,'bin','attrdox.py')
        os.system(attrgen + ' ' + forge.rootPath + ' ' + 'attributes.dox')
        attrgen = os.path.join(forge.rootPath,'bin','catalogdox.py')
        os.system(attrgen + ' ' + forge.rootPath + ' ' + 'catalog.dox')
        compgen = os.path.join(forge.rootPath,'bin','compdox.py')
        os.system(compgen + ' ' + forge.rootPath + ' ' + 'component.dox')
        os.system('doxygen ' + self.doxyfile)
        os.chdir(cwd)

def setup(module):
    dummy = forge.Target("dummy_doc_target")
    forge.targetRegistry['documentation'] = dummy

    docTarget = DocTarget("document_gen", "Doxyfile")
    forge.targetRegistry['dox'] = docTarget
    docTarget.AddDep(dummy)

    docTarget = DocTarget("ref_doc_gen", "Doxyfile.ref")
    forge.targetRegistry['ref'] = docTarget
    docTarget.AddDep(dummy)


