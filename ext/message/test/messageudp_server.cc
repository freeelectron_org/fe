/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <string>
#include "message/message.pmh"
#include "message/test/test.h"

using namespace fe;
using namespace ext;

int main(int argc, char **argv)
{
	sp<Master> master(new Master);
	sp<Registry> registry = master->registry();

	registry->manage("fexMessageDL");

	sp<MessageI> messenger(registry->create("MessageI.MessageUDP"));

	TestRequest testRequest;
	memset(&testRequest, 0, sizeof(testRequest));

	testRequest.initPosition.x = 1.0;
	testRequest.initPosition.y = 2.0;
	testRequest.initPosition.z = 3.0;
	testRequest.zTrackGripRR = 99.96001;

	// import your connection info from somewhere ...
	std::string host;
	host = "127.0.0.1";
	uint16_t port = 5000;

	messenger->postInit();

	messenger->bind(host.c_str(), port);

	while (true)
	{
		Messagegram mg;
		memset(&mg, 0, sizeof(mg));
		printf("server waiting for message [%s:%d]\n", host.c_str(), port);

		messenger->recvFrom(&mg);

		if (mg.dataLen != sizeof(TestRequest))
		{
			printf("bad request, ignored\n");
			continue;
			//throw std::system_error(0, std::system_category(), "unexpected sensorgram payload");
		}

		// copy payload to response
		TestRequest testRequest;
		memset(&testRequest, 0, sizeof(testRequest));
#ifdef _WIN32
		memcpy_s(static_cast<void *>(&testRequest), sizeof(testRequest),
			static_cast<const void *>(mg.data), mg.dataLen);
#else
		memcpy(&testRequest, mg.data, mg.dataLen);
#endif

		printf("received TestRequest data:\n");
		printf("=========================================\n");
		printf("initialPosition.x=%2.1lf\n", testRequest.initPosition.x);
		printf("initialPosition.y=%2.1lf\n", testRequest.initPosition.y);
		printf("initialPosition.z=%2.1lf\n", testRequest.initPosition.z);
		printf("zTrackGripRR=%3.5lf\n\n", testRequest.zTrackGripRR);

		// copy Messagegram payload to response
		TestResponse testResponse;
		memset(&testResponse, 0, sizeof(testResponse));

		// compose testResponse
		testResponse.position.x = 4.0;
		testResponse.position.y = 5.0;
		testResponse.position.z = 6.0;
		testResponse.nWheelRR = 29.12340;

		printf("sending TestResponse data:\n");
		printf("=========================================\n");
		printf("position.x=%2.1lf\n", testResponse.position.x);
		printf("position.y=%2.1lf\n", testResponse.position.y);
		printf("position.z=%2.1lf\n", testResponse.position.z);
		printf("nWheelRR=%3.5lf\n\n", testResponse.nWheelRR);

		// compose sensorgram response
		memset(&mg, 0, sizeof(mg));
		mg.dataLen = sizeof(testResponse);
#ifdef _WIN32
		memcpy_s(static_cast<void *>(mg.data), sizeof(mg.data),
			static_cast<const void *>(&testResponse), sizeof(testResponse));
#else
		memcpy(mg.data, &testResponse, sizeof(testResponse));
#endif
		// send the response
		messenger->replyLast(mg);
	}

	return 0;
}

