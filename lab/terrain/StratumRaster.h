/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumRaster_h__
#define __terrain_StratumRaster_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Elevation of a height map

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumRaster: public StratumPlane
{
	public:
						StratumRaster(void);
virtual					~StratumRaster(void);

						//* As StratumI
virtual Real			height(Real x,Real z) const;
virtual SpatialVector	normal(Real x,Real z) const;

						//* As RecordableI

						using StratumPlane::bind;

virtual	void			bind(Record& a_rRecord);

	private:
		void			rebind(void);

		Raster			m_rasterRV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumRaster_h__ */

