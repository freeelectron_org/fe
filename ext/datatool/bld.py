import sys
forge = sys.modules["forge"]

def prerequisites():
    return [    "signal" ]

def setup(module):

    # the library
    srcList = [ "StringAccessor",
                "Config"
                ]

    lib = module.Lib("fexDataTool",srcList)

    # the plugin
    srcList += ["datatool.pmh",
                "assertDataTool",
                "RecordPlayer",
                "RecordRecorder",
                "datatoolLib"
                ]

    srcList += ["datatoolDL"]

    dll = module.DLL( "fexDataToolDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib" ]

#   if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
#       deplibs += [ "fexDataToolLib" ]

    forge.deps( ["fexDataToolDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexDataToolDL",                None,       None) ]

    module.Module( 'test' )
