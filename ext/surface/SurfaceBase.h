/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceBase_h__
#define __surface_SurfaceBase_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Trivial Point Origin Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceBase: virtual public RecordableI,
	virtual public SurfaceI,
	virtual public DrawableI,
	public CastableAs<SurfaceBase>
{
	public:

	class Impact:
		virtual public ImpactI,
		virtual public DrawableI,
		public CastableAs<Impact>
	{
		public:
								Impact(void)
								{
#if FE_COUNTED_STORE_TRACKER
									setName("SurfaceBase::Impact");
#endif
									setIdentity(m_transform);
								}
	virtual						~Impact(void)								{}

								//* As DrawableI
	virtual	void				draw(sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor)
								{
									const_cast<const SurfaceBase::Impact*>(this)
											->draw(a_spDrawI,a_pColor);
								}
	virtual	void				draw(sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor) const
								{
									SpatialTransform identity;
									setIdentity(identity);
									draw(identity,a_spDrawI,a_pColor);
								}
	virtual	void				draw(const SpatialTransform& a_transform,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor)
								{
									const_cast<const SurfaceBase::Impact*>(this)
											->draw(a_transform,a_spDrawI,
											a_pColor);
								}
	virtual	void				draw(const SpatialTransform& a_transform,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor) const
								{
									draw(a_transform,a_spDrawI,
											a_pColor,sp<DrawBufferI>(NULL),
											sp<PartitionI>(NULL));
								}
	virtual	void				draw(const SpatialTransform& a_transform,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor,
										sp<DrawBufferI> a_spDrawBuffer,
										sp<PartitionI> a_spPartition)
								{
									const_cast<const SurfaceBase::Impact*>(this)
											->draw(a_transform,a_spDrawI,
											a_pColor,a_spDrawBuffer,
											a_spPartition);
								}
	virtual	void				draw(const SpatialTransform& a_transform,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor,
										sp<DrawBufferI> a_spDrawBuffer,
										sp<PartitionI> a_spPartition) const	{}

			void				setSurface(const SurfaceI* a_pSurface)
								{	m_hpSurface=
											hp<SurfaceI>(const_cast<SurfaceI*>(
											a_pSurface)); }

			void				setTransform(SpatialTransform a_transform)
								{	m_transform=a_transform; }

	virtual	SpatialVector		location(void) const
								{	return rotateVector(m_transform,
											m_location); }
			SpatialVector		locationLocal(void) const
								{	return m_location; }
			void				setLocationLocal(SpatialVector a_location)
								{	m_location=a_location; }

	virtual	Color				diffuse(void) const
								{	return m_diffuse; }
	virtual	void				setDiffuse(Color a_diffuse)
								{	m_diffuse=a_diffuse; }

	virtual	Real				radius(void) const
								{	return m_radius; }
			void				setRadius(Real a_radius)
								{	m_radius=a_radius; }

	virtual	SpatialVector		origin(void) const
								{	return m_origin; }
			void				setOrigin(SpatialVector a_origin)
								{	m_origin=a_origin; }

	virtual	SpatialVector		direction(void) const
								{	return m_direction; }
			void				setDirection(SpatialVector a_direction)
								{	m_direction=a_direction; }

	virtual	Real				distance(void) const
								{	return m_distance; }
			void				setDistance(Real a_distance)
								{	m_distance=a_distance; }

	virtual	I32					pointIndex0(void) const	{ return -1; }
	virtual	I32					face(void)				{ return -1; }
	virtual	I32					triangleIndex(void)		{ return -1; }
	virtual	I32					primitiveIndex(void)	{ return -1; }
	virtual	I32					partitionIndex(void)	{ return -1; }
	virtual	SpatialBary			barycenter(void)
								{	return SpatialBary(0.0,0.0); }

	virtual Vector2				uv(void)
								{	return Vector2(0.0,0.0); }
	virtual SpatialVector		du(void)
								{	return SpatialVector(0.0,0.0,0.0); }
	virtual SpatialVector		dv(void)
								{	return SpatialVector(0.0,0.0,0.0); }

		protected:

			hp<SurfaceI>			m_hpSurface;
			SpatialTransform		m_transform;
			Color					m_diffuse;
			SpatialVector			m_location;
			Real					m_radius;
			SpatialVector			m_origin;
			SpatialVector			m_direction;
			Real					m_distance;
	};

							SurfaceBase(void):
								m_cached(FALSE),
								m_sampleMethod(e_linear),
								m_restrictions(e_unrestricted)				{}
virtual						~SurfaceBase(void)								{}

							//* As Protectable
virtual	void				protect(void)
							{
//								feLog("SurfaceBase::protect checkCache\n");
								checkCache();
//								feLog("SurfaceBase::protect"
//										" Protectable::protect\n");
								Protectable::protect();
//								feLog("SurfaceBase::protect done\n");
							}

							//* As SurfaceI
virtual	void				bind(Instance a_instance)						{}

virtual SpatialVector		center(void)		{ return SpatialVector(0,0,0); }
virtual SpatialVector		center(void) const	{ return SpatialVector(0,0,0); }

virtual Real				radius(void)		{ return Real(0); }
virtual Real				radius(void) const	{ return Real(0); }

virtual Color				diffuse(void) const
							{	return Color(1,1,1,1); }

virtual	void				setSampleMethod(SampleMethod a_sampleMethod)
							{	m_sampleMethod=a_sampleMethod; }

virtual	void				setNodeName(String a_nodeName)
							{	m_nodeName=a_nodeName; }
virtual	String				nodeName(void) const
							{	return m_nodeName; }

virtual	void				setRestrictions(Restrictions a_restrictions)
							{	m_restrictions=a_restrictions; }
virtual	Restrictions		restrictions(void) const
							{	return m_restrictions; }

virtual	void				setRefinement(U32 a_refinement)					{}

virtual	void				setAccuracy(SurfaceI::Accuracy a_accuracy)		{}

virtual	void				setSearchable(BWORD a_searchable)				{}

virtual	void				setSearch(String a_searchName)					{}

virtual	void				setTriangulation(Triangulation a_triangulation)	{}

virtual	I32					triangleCount(void)	{ return -1; }

virtual SpatialTransform	sample(Vector2 a_uv)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										sample(a_uv);
							}
virtual SpatialTransform	sample(Vector2 a_uv) const
							{	return SpatialTransform::identity(); }

virtual SpatialTransform	sample(I32 a_triangleIndex,SpatialBary a_barycenter)
							{
								checkCache();
								return const_cast<const SurfaceBase*>(this)->
										sample(a_triangleIndex,a_barycenter);
							}
virtual SpatialTransform	sample(I32 a_triangleIndex,
									SpatialBary a_barycenter) const
							{	return sample(a_triangleIndex,a_barycenter,
										SpatialVector(0.0,0.0,0.0)); }
virtual SpatialTransform	sample(I32 a_triangleIndex,SpatialBary a_barycenter,
									SpatialVector a_tangent) const
							{	return SpatialTransform::identity(); }

virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter)
							{
								checkCache();
								return const_cast<const SurfaceBase*>(this)->
										sampleImpact(a_triangleIndex,
										a_barycenter);
							}
virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter) const
							{	return sampleImpact(a_triangleIndex,
										a_barycenter,
										SpatialVector(0.0,0.0,0.0)); }
virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter,
									SpatialVector a_tangent) const
							{	return sp<ImpactI>(NULL); }

virtual SpatialVector		samplePoint(const SpatialTransform& a_transform,
									Vector2 a_uv)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										samplePoint(a_transform,a_uv);
							}
virtual SpatialVector		samplePoint(Vector2 a_uv)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										samplePoint(a_uv);
							}

virtual SpatialVector		samplePoint(const SpatialTransform& a_transform,
									Vector2 a_uv) const
							{	return transformVector(a_transform,
										sample(a_uv).translation()); }
virtual SpatialVector		samplePoint(Vector2 a_uv) const
							{	return sample(a_uv).translation(); }

virtual SpatialVector		sampleNormal(const SpatialTransform& a_transform,
									Vector2 a_uv)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										sampleNormal(a_transform,a_uv);
							}
virtual SpatialVector		sampleNormal(Vector2 a_uv)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										sampleNormal(a_uv);
							}

virtual SpatialVector		sampleNormal(const SpatialTransform& a_transform,
									Vector2 a_uv) const
							{	return rotateVector(a_transform,
										sample(a_uv).up()); }
virtual SpatialVector		sampleNormal(Vector2 a_uv) const
							{	return sample(a_uv).up(); }

virtual	void				prepareForSample(void)			{ checkCache(); }
virtual	void				prepareForSearch(void)							{}
virtual	void				prepareForUVSearch(void)						{}

virtual Containment			containment(const SpatialVector& a_origin)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										containment(a_origin);
							}
virtual Containment			containment(const SpatialVector& a_origin) const;

virtual	sp<ImpactI>			nearestPoint(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									Real a_maxDistance)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										nearestPoint(a_transform,a_origin,
										a_maxDistance);
							}
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance,BWORD a_anyHit)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										nearestPoint(a_origin,a_maxDistance,
										a_anyHit);
							}
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										nearestPoint(a_origin,a_maxDistance);
							}
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin)
							{
								const Real maxDistance= -1.0;
								return nearestPoint(a_origin,maxDistance);
							}
virtual	sp<ImpactI>			nearestPoint(const Vector2& a_uv)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										nearestPoint(a_uv);
							}

virtual	sp<ImpactI>			nearestPoint(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									Real a_maxDistance) const
							{
								const SpatialVector invOrigin=
										inverseTransformVector(
										a_transform,a_origin);
								sp<Impact> spImpact=nearestPoint(invOrigin,
										a_maxDistance);
								if(spImpact.isValid())
								{
									spImpact->setTransform(a_transform);
								}
								return spImpact;
							}
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance,BWORD a_anyHit) const
							{	return nearestPoint(a_origin,a_maxDistance); }
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance) const
							{	return sp<ImpactI>(NULL); }
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin) const
							{	return nearestPoint(a_origin,-1.0,FALSE); }

virtual	sp<ImpactI>			nearestPoint(const Vector2& a_uv) const
							{	return sp<ImpactI>(NULL); }

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const Vector2& a_uv,
									Real a_maxDistance,U32 a_hitLimit)
							{
								checkCache();
								prepareForUVSearch();
								return const_cast<const SurfaceBase*>(this)->
										nearestPoints(a_uv,a_maxDistance,
										a_hitLimit);
							}
virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const Vector2& a_uv,
									Real a_maxDistance,U32 a_hitLimit) const
							{
								//* default implementation is single point
								Array< sp<SurfaceI::ImpactI> >
										impactArray;
								sp<SurfaceI::ImpactI> spImpact=
										nearestPoint(a_uv,a_maxDistance);
								if(spImpact.isValid())
								{
									impactArray.resize(1);
									impactArray[0]=spImpact;
								}
								return impactArray;
							}

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit)
							{
								return nearestPoints(a_origin,
										a_maxDistance,a_hitLimit,
										sp<PartitionI>(NULL));
							}
virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit) const
							{
								return nearestPoints(a_origin,
										a_maxDistance,a_hitLimit,
										sp<PartitionI>(NULL));
							}

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit,
									sp<PartitionI> a_spPartition)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										nearestPoints(a_origin,a_maxDistance,
										a_hitLimit,a_spPartition);
							}
virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit,
									sp<PartitionI> a_spPartition) const
							{
								//* default implementation is single point
								Array< sp<SurfaceI::ImpactI> >
										impactArray;
								sp<SurfaceI::ImpactI> spImpact=
										nearestPoint(a_origin,a_maxDistance);
								if(spImpact.isValid())
								{
									impactArray.resize(1);
									impactArray[0]=spImpact;
								}
								return impactArray;
							}

virtual	sp<ImpactI>			rayImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										rayImpact(a_transform,
										a_origin,a_direction,
										a_maxDistance,a_anyHit);
							}

virtual	sp<ImpactI>			rayImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const
							{
								const SpatialVector invOrigin=
										inverseTransformVector(
										a_transform,a_origin);
								const SpatialVector invDirection=
										inverseRotateVector(
										a_transform,a_direction);
								sp<Impact> spImpact=rayImpact(
										invOrigin,invDirection,
										a_maxDistance,a_anyHit);
								if(spImpact.isValid())
								{
									spImpact->setTransform(a_transform);
								}
								return spImpact;
							}

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										rayImpact(a_origin,a_direction,
										a_maxDistance,a_anyHit);
							}

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const
							{	return sp<ImpactI>(NULL); }

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										rayImpact(a_origin,a_direction,
										a_maxDistance,FALSE);
							}

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance) const
							{	return rayImpact(a_origin,a_direction,
										a_maxDistance,FALSE); }

virtual	Array< sp<SurfaceI::ImpactI> >
							rayImpacts(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,U32 a_hitLimit)
							{
								checkCache();
								prepareForSearch();
								return const_cast<const SurfaceBase*>(this)->
										rayImpacts(a_origin,a_direction,
										a_maxDistance,a_hitLimit);
							}
virtual	Array< sp<SurfaceI::ImpactI> >
							rayImpacts(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,U32 a_hitLimit) const
							{
								//* default implementation is single point
								Array< sp<SurfaceI::ImpactI> >
										impactArray;
								sp<SurfaceI::ImpactI> spImpact=rayImpact(
										a_origin,a_direction,
										a_maxDistance,FALSE);
								if(spImpact.isValid())
								{
									impactArray.resize(1);
									impactArray[0]=spImpact;
								}
								return impactArray;
							}

virtual	sp<ImpactI>			coneImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_coneAngle,
									sp<DrawI> a_spDrawI);
virtual	sp<ImpactI>			coneImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_coneAngle,
									sp<DrawI> a_spDrawI);

virtual	sp<ImpactI>			capsuleImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_radius,
									BWORD a_anyHit,sp<DrawI> a_spDrawI,
									sp<ImpactI> a_spLastImpact);
virtual	sp<ImpactI>			capsuleImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_radius,
									BWORD a_anyHit,sp<DrawI> a_spDrawI,
									sp<ImpactI> a_spLastImpact);

virtual	sp<GaugeI>			gauge(void)			{ return sp<GaugeI>(NULL); }

virtual	void				partitionWith(String a_attributeName)			{}
virtual	U32					partitionCount(void) const			{ return 0; }
virtual	String				partitionName(U32 a_index) const	{ return ""; }
virtual	Vector4				partitionSphere(U32 a_index) const
							{	Vector4 sphere=center();
								sphere[3]=radius();
								return sphere; }
virtual	I32					setPartitionFilter(String a_filterString)
							{	return setPartitionFilter(a_filterString,
										PartitionI::e_matchRegex); }
virtual	I32					setPartitionFilter(String a_filterString,
									PartitionI::FilterMethod a_filterMethod)
							{	return 0; }
virtual	sp<PartitionI>		createPartition(void)
							{	return sp<PartitionI>(NULL); }

							//* As DrawableI
virtual	void				draw(sp<DrawI> a_spDrawI,const fe::Color* a_pColor)
							{
								checkCache();
								const_cast<const SurfaceBase*>(this)->draw(
										a_spDrawI,a_pColor);
							}
virtual	void				draw(sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor) const
							{
								SpatialTransform identity;
								setIdentity(identity);
								draw(identity,a_spDrawI,a_pColor);
							}
virtual	void				draw(const SpatialTransform& a_transform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor)
							{
								checkCache();
								const_cast<const SurfaceBase*>(this)->
										draw(a_transform,a_spDrawI,a_pColor);
							}
virtual	void				draw(const SpatialTransform& a_transform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor) const
							{
								draw(a_transform,a_spDrawI,
										a_pColor,sp<DrawBufferI>(NULL),
										sp<PartitionI>(NULL));
							}
virtual	void				draw(const SpatialTransform& a_transform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition)
							{
								checkCache();
								const_cast<const SurfaceBase*>(this)->
										draw(a_transform,a_spDrawI,
										a_pColor,a_spDrawBuffer,a_spPartition);
							}
virtual	void				draw(const SpatialTransform& a_transform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const		{}

							//* as RecordableI
virtual	void				bind(Record& rRecord)							{}

	protected:

		sp<ImpactI>			shootRay(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit,
									sp<ImpactI> a_spLastImpact);
		sp<ImpactI>			tubeImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,
									Real a_radius0,Real a_radius1,
									BWORD a_anyHit,
									sp<DrawI> a_spDrawI,
									sp<ImpactI> a_spLastImpact);

virtual	void				cache(void)										{}
virtual	void				checkCache(void)
							{
								if(!m_cached)
								{
									m_cached=TRUE;
									cache();
								}
							}

static	void				drawHit(sp<DrawI> a_spDrawI,SpatialVector a_center,
									Real a_radius,Color a_color);

		BWORD			m_cached;
		SampleMethod	m_sampleMethod;
		String			m_nodeName;
		Restrictions	m_restrictions;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceBase_h__ */

