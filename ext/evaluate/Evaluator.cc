/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "evaluate.pmh"

namespace fe
{
namespace ext
{

Node::Node(void)
{
	m_leaf = -1;
	m_t_eval = e_eval_none;
	m_is_define = false;
	m_is_jump = false;
}

Node::~Node(void)
{
}

void Node::leafMake(Evaluator *a_evaluator, sp<Node> a_node)
{
	a_node->m_aggregate = a_evaluator->literalAggregate();
	a_node->m_leaf = a_evaluator->index(a_node->m_token);
	if(!a_evaluator->variable(a_node->m_leaf).isValid())
	{
		a_evaluator->makeVariable(a_node.raw());
	}
}

void Node::compile(t_stdlist<t_token> &a_tokens, Evaluator *a_evaluator, Node *a_parent, sp<Debug> a_debug)
{
	t_token token = a_tokens.front();
	m_token = token;
	a_debug->log("info", "[%s]", token.c_str());
	a_tokens.pop_front();
	if (token.m_str == ")")
	{
		a_debug->log("error", "dangling ) line %d", m_token.m_line);
	}
	else if (token.m_str == "(")
	{
		while(!a_tokens.empty() && a_tokens.front().m_str != ")")
		{
			sp<Node> spNode(new Node());
			m_children.push_back(spNode);
			a_debug->push();
			spNode->compile(a_tokens, a_evaluator, this, a_debug);
			a_debug->pop();
			if(a_tokens.empty())
			{
				a_debug->log("error", "missing ) for line %d", token.m_line);
			}
		}
		a_debug->log("info", "[%s]", a_tokens.front().c_str());

		if(m_children.size() > 0)
		{
			if(m_is_define)
			{
				a_evaluator->m_jump_locations[m_children[1]->m_token.c_str()].m_node = this;

			}
			else if(m_is_jump)
			{
			}
			else if(m_function.isValid())
			{
				// node is a function
				m_argv.resize(m_children.size()-1);
				for(unsigned int i = 0; i < m_argv.size(); i++)
				{
					unsigned int i_child = i+1;
					// if child 1 doesn't exist yet, make it a leaf
					if(!m_children[i_child]->returnType().isValid())
					{
						leafMake(a_evaluator, m_children[i_child]);
						a_evaluator->asVariable()->value(a_evaluator->variable(m_children[i_child]->m_leaf)) = 0.0;

						m_children[i_child]->m_value = a_evaluator->variable(m_children[i_child]->m_leaf);
					}

					m_argv[i] = m_children[i+1]->value();
				}
				t_stdstring msg;
				if(!m_function->compile(a_evaluator->recordGroup(), m_value, m_argv, msg))
				{
					a_debug->log("error", "%s | line %d", msg.c_str(), m_token.m_line);
				}
				m_aggregate = m_function->returnType();
			}
			else if(m_children.size() == 2)
			{
				// if child 1 doesn't exist yet, make it a leaf
				if(!m_children[1]->returnType().isValid())
				{
					leafMake(a_evaluator, m_children[1]);
					a_evaluator->asVariable()->value(a_evaluator->variable(m_children[1]->m_leaf)) = 0.0;

					m_children[1]->m_value = a_evaluator->variable(m_children[1]->m_leaf);
				}

				// check for assignment to a number
				Regex re_real("[-+]?[0-9]*\\.?[0-9]+");
				if(re_real.match(m_children[0]->m_token.c_str()))
				{
					a_debug->log("warning", "assignment to [%s] | which is a number | line %d", m_children[0]->m_token.c_str(), m_token.m_line);
				}

				// if child 0 doesn't exist yet, match it for assignment from child 1
				if(!m_children[0]->returnType().isValid())
				{
					m_children[0]->m_aggregate = m_children[1]->m_aggregate;
					m_children[0]->m_leaf = a_evaluator->index(m_children[0]->m_token);
					if(!a_evaluator->variable(m_children[0]->m_leaf).isValid())
					{
						a_evaluator->makeVariable(m_children[0].raw());
						a_debug->log("info", "assignment to [%s] | make new variable", m_children[0]->m_token.c_str());
					}
					else
					{
						a_debug->log("info", "assignment to [%s] | not yet return type", m_children[0]->m_token.c_str());
					}
				}
				else
				{
					a_debug->log("info", "assignment to [%s]", m_children[0]->m_token.c_str());
				}
				if(m_leaf != (unsigned int)-1)
				{
					m_children[0]->m_value = a_evaluator->variable(m_children[0]->m_leaf);
					m_aggregate = m_children[0]->returnType();
					m_value = a_evaluator->createRecord(m_aggregate->m_spLayout);
				}
			}
			else if(m_children.size() == 1)
			{
				// if child 1 doesn't exist yet, make it a leaf
				if(!m_children[0]->returnType().isValid())
				{
					leafMake(a_evaluator, m_children[0]);
					a_evaluator->asVariable()->value(a_evaluator->variable(m_children[0]->m_leaf)) = 0.0;
					m_children[0]->m_value = a_evaluator->variable(m_children[0]->m_leaf);
					a_debug->log("info", "single child [%s] | make leaf", m_children[0]->m_token.c_str());
				}
				else
				{
					a_debug->log("info", "single child [%s]", m_children[0]->m_token.c_str());
				}

				m_aggregate = m_children[0]->returnType();
				m_value = a_evaluator->createRecord(m_aggregate->m_spLayout);
			}
			else
			{
				a_debug->log("warning", "assign with extra children [%s] | children: %d | line %d", m_children[0]->m_token.c_str(), m_children.size(), m_token.m_line);
				for(unsigned int i_child = 1; i_child < m_children.size(); i_child++)
				{
					// if child 1 doesn't exist yet, make it a leaf
					if(!m_children[i_child]->returnType().isValid())
					{
						if(i_child == 1)
						{
							leafMake(a_evaluator, m_children[i_child]);
							a_evaluator->asVariable()->value(a_evaluator->variable(m_children[i_child]->m_leaf)) = 0.0;

							m_children[1]->m_value = a_evaluator->variable(m_children[i_child]->m_leaf);
							a_debug->log("info", "assignment from [%s] | make new variable", m_children[1]->m_token.c_str());
						}
						else
						{
							a_debug->log("info", "extra child [%s] | skipping", m_children[1]->m_token.c_str());
						}
					}
				}

				// check for assignment to a number
				Regex re_real("[-+]?[0-9]*\\.?[0-9]+");
				if(re_real.match(m_children[0]->m_token.c_str()))
				{
					a_debug->log("warning", "assignment to [%s] | which is a number | line %d", m_children[0]->m_token.c_str(), m_token.m_line);
				}

				// if child 0 doesn't exist yet, match it for assignment from child 1
				if(!m_children[0]->returnType().isValid())
				{
					m_children[0]->m_aggregate = m_children[1]->m_aggregate;
					m_children[0]->m_leaf = a_evaluator->index(m_children[0]->m_token);
					if(!a_evaluator->variable(m_children[0]->m_leaf).isValid())
					{
						a_debug->log("info", "assignment to [%s] | make new variable", m_children[0]->m_token.c_str());
						a_evaluator->makeVariable(m_children[0].raw());
					}
					else
					{
						a_debug->log("info", "assignment to [%s] | not yet return type", m_children[0]->m_token.c_str());
					}
				}
				else
				{
					a_debug->log("info", "assignment to [%s]", m_children[0]->m_token.c_str());
				}
				m_children[0]->m_value = a_evaluator->variable(m_children[0]->m_leaf);

				m_aggregate = m_children[0]->returnType();
				m_value = a_evaluator->createRecord(m_aggregate->m_spLayout);
			}
		}
		else
		{
			m_aggregate = a_evaluator->literalAggregate();
			m_value = a_evaluator->createRecord(m_aggregate->m_spLayout);
			a_evaluator->asVariable()->value(m_value) = 0.0;
			a_debug->log("warning", "childless leaf | line %d", m_token.m_line);
		}
		if(!a_tokens.empty()) { a_tokens.pop_front(); }
	}
	else
	{
		if(token.m_str == "define")
		{
			a_parent->m_is_define = true;
			a_debug->log("info", "token [%s] function define", m_token.c_str());
		}
		else
		{
			t_stdmap<t_stdstring, t_jump_location>::iterator i_jump = a_evaluator->m_jump_locations.find(m_token.c_str());
			if(i_jump != a_evaluator->m_jump_locations.end())
			{
				a_parent->m_is_jump = true;
				a_parent->m_jump_node = i_jump->second.m_node;
				a_debug->log("info", "token [%s] jump", m_token.c_str());
			}
			else
			{
				sp<FunctionI> spFunction = a_evaluator->createFunction(token);
				if(spFunction.isValid())
				{
					a_parent->m_function = spFunction;
					a_debug->log("info", "token [%s] function", m_token.c_str());
				}
				else if(a_parent->m_is_define)
				{
					// should be a define name token
					a_debug->log("info", "define name token [%s]", m_token.c_str());
				}
				else
				{
					// what amounts to a constant/literal, although it looks up in env
					m_leaf = a_evaluator->index(token);
					Regex re_real("[-+]?[0-9]*\\.?[0-9]+");
					Regex re_string_double("\"([^\"]*)\"");
					Regex re_string_single("\'([^\']*)\'");
					if(re_real.match(token.c_str()))
					{
						m_aggregate = a_evaluator->literalAggregate();

						if(!a_evaluator->variable(m_leaf).isValid())
						{
							a_evaluator->makeVariable(this);
						}
						a_evaluator->asVariable()->value(a_evaluator->variable(m_leaf)) = atof(token.c_str());
						m_value = a_evaluator->variable(m_leaf);
						a_debug->log("info", "token [%s] | number", m_token.c_str());
					}
					else if(re_string_double.match(token.c_str()))
					{
						m_aggregate = a_evaluator->stringAggregate();

						if(!a_evaluator->variable(m_leaf).isValid())
						{
							a_evaluator->makeVariable(this);
						}
						t_stdstring s = token;
						s.erase( std::remove(s.begin(), s.end(), '\"'), s.end()); // remove quotes
						a_evaluator->asString()->value(a_evaluator->variable(m_leaf)) = s.c_str();
						m_value = a_evaluator->variable(m_leaf);
						a_debug->log("info", "token [%s] | string", m_token.c_str());
					}
					else if(re_string_single.match(token.c_str()))
					{
						m_aggregate = a_evaluator->stringAggregate();

						if(!a_evaluator->variable(m_leaf).isValid())
						{
							a_evaluator->makeVariable(this);
						}
						t_stdstring s = token;
						s.erase( std::remove(s.begin(), s.end(), '\''), s.end()); // remove quotes
						a_evaluator->asString()->value(a_evaluator->variable(m_leaf)) = s.c_str();
						m_value = a_evaluator->variable(m_leaf);
						a_debug->log("info", "token [%s] | string", m_token.c_str());
					}
					else if(a_evaluator->variable(m_leaf).isValid())
					{
						m_value = a_evaluator->variable(m_leaf);
						m_aggregate = a_evaluator->aggregate(m_leaf);
						a_debug->log("info", "token [%s] | aggregate [%s]", m_token.c_str(), m_value.layout()->name().c_str());
					}
					else
					{
						a_debug->log("info", "token [%s] | defer", m_token.c_str());
					}
				}
			}
		}
	}

	preEvaluate(a_evaluator, a_debug);
}

void Node::preEvaluate(Evaluator *a_evaluator, sp<Debug> a_debug)
{
	t_variables &a_variables = a_evaluator->variables();
	if(m_children.size() > 0)
	{
		if(m_is_define)
		{
			m_t_eval = e_eval_childless;
		}
		else if(m_is_jump)
		{
			m_t_eval = e_eval_jump;
		}
		else if(m_function.isValid())
		{
			/// Case: Function Evaluate
			m_t_eval = e_eval_function;
		}
		else if(m_children.size() >= 2)
		{
			if(m_children[0]->m_leaf < a_variables.size())
			{
				if(a_variables[m_children[0]->m_leaf].layout() == m_children[1]->value().layout())
				{
					/// Case: variable assignment
					m_t_eval = e_eval_assign_matched;
					a_debug->log("info", "assign [%s] LINE: %d", m_children[0]->m_token.c_str(), m_token.m_line);
				}
				else
				{
					/// Case: type mismatch
					// need a new aggregate (so we don't screw with current possibly shared one)
					m_aggregate = new Aggregate();
					m_aggregate->m_spLayout = a_variables[m_children[0]->m_leaf].layout();
					m_aggregate->asUnion(m_children[1]->m_aggregate);
					m_t_eval = e_eval_assign_mismatch;
					a_debug->log("info", "assign (w/ union) [%s] LINE: %d", m_children[0]->m_token.c_str(), m_token.m_line);
				}
			}
			else
			{
				/// Case: first child (of more than one) is neither function nor variable
				a_debug->log("error", "invalid first child [%s] LINE: %d", m_children[0]->m_token.c_str(), m_token.m_line);
				m_t_eval = e_eval_impossible;
			}
		}
		else // one child
		{
			/// Case: one child -- basically unnecessary parens: ( node )
			m_t_eval = e_eval_single_child;
		}
	}
	else
	{
		/// Case: no children
		if(m_leaf < a_variables.size())
		{
			a_debug->log("info", "childless [%s] LINE: %d", m_token.c_str(), m_token.m_line);
			m_t_eval = e_eval_childless;
		}
		else
		{
			// function nodes themselves (not the parent w/ m_function) come here
			a_debug->log("info", "childless fn node [%s] LINE: %d", m_token.c_str(), m_token.m_line);
			m_t_eval = e_eval_childless;
		}
	}
}


void Node::evaluate(sp<t_variables> &a_variables)
{
	switch(m_t_eval)
	{
		case e_eval_function:
			for(unsigned int i = 1; i < m_children.size(); i++)
			{
				m_children[i]->evaluate(a_variables);
			}
			m_function->eval(m_value, m_argv);
			break;
		case e_eval_jump:
			for(unsigned int i_child = 1; i_child < m_children.size(); i_child++)
			{
				m_children[i_child]->evaluate(a_variables);
			}
			m_jump_node->m_children[2]->evaluate(a_variables);
			m_aggregate->assign(m_value, m_jump_node->m_children[2]->value());
			break;
		case e_eval_assign_matched:
			m_children[1]->evaluate(a_variables);
			m_children[1]->m_aggregate->assign((*a_variables)[m_children[0]->m_leaf], m_children[1]->value());
			break;
		case e_eval_assign_mismatch:
			m_children[1]->evaluate(a_variables);
			m_aggregate->assign((*a_variables)[m_children[0]->m_leaf], m_children[1]->value());
			break;
		case e_eval_single_child:
			m_children[0]->evaluate(a_variables);
			m_aggregate->assign(m_value, m_children[0]->value());
			break;
		case e_eval_childless:
			/* NO OP NECESSARY */
			break;
		case e_eval_impossible:
			break;
		default:
			fe_fprintf(stderr, "UNHANDLED CASE\n");
			break;
	}
}

Evaluator::Evaluator(void)
{
}

Evaluator::~Evaluator(void)
{
}

void Evaluator::initialize(void)
{
}

void Evaluator::bind(sp<RecordGroup> a_rg)
{
	m_rg = a_rg;
	m_scope = a_rg->scope();

	m_literalAggregate = new Aggregate();
	m_asVariable = new AsVariable();
	m_literalAggregate->bakeTypes(m_asVariable, m_scope->declare("l_eval_real"));

	m_stringAggregate = new Aggregate();
	m_asString = new AsString();
	m_stringAggregate->bakeTypes(m_asString, m_scope->declare("l_eval_string"));

	m_variables = new t_variables();
}

void Evaluator::dump(void)
{
	fe_fprintf(stderr, "m_variables size %d\n", (int)m_variables->size());
	for(t_stdmap<t_stdstring, unsigned int>::iterator i_names = m_var_names.begin(); i_names != m_var_names.end(); i_names++)
	{
		fe_fprintf(stderr, "[%s] %d", i_names->first.c_str(), i_names->second );
		if((*m_variables)[i_names->second].isValid())
		{
			fe_fprintf(stderr, " | %s", (*m_variables)[i_names->second].layout()->name().c_str());
			Record r_var = (*m_variables)[i_names->second];
			if(m_asVariable->check(r_var))
			{
				fe_fprintf(stderr, " <%g>", m_asVariable->value(r_var));
			}
			if(m_asString->check(r_var))
			{
				fe_fprintf(stderr, " <%s>", m_asString->value(r_var).c_str());
			}
		}
		fe_fprintf(stderr, "\n");
	}
}

void Evaluator::evaluate(void)
{
	for(unsigned int i = 0; i < m_nodes.size(); i++)
	{
		m_nodes[i]->evaluate(m_variables);
	}
}


bool is_notoken(const char &s)
{
	if(s == ' ' || s == '\n' || s == '\r' || s == '\t' || s == '#')
	{
		return true;
	}
	return false;
}

bool is_ws(const char &s)
{
	if(s == ' ' || s == '\t')
	{
		return true;
	}
	return false;
}

bool is_eol(const char &s)
{
	if(s == '\n' || s == '\r' || s == '#')
	{
		return true;
	}
	return false;
}

void Evaluator::tokenize(t_stdlist<t_token> &a_tokens, const t_stdstring &a_buffer)
{
	unsigned int line = 1;
	const char * s = a_buffer.c_str();
	bool in_comment = false;
	while(*s)
	{
		while (is_notoken(*s))
		{
			if(*s == '#')
			{
				in_comment = true;
			}
			if(*s == '\n')
			{
				in_comment = false;
			}
			if(*s == '\n') { line++; }
			s++;
		}
		if(!in_comment)
		{
			if(*s == '(')
			{
				a_tokens.push_back(Token("(", line));
				if(*s == '\n') { line++; }
				s++;
			}
			else if(*s == ')')
			{
				a_tokens.push_back(Token(")", line));
				if(*s == '\n') { line++; }
				s++;
			}
			else
			{
				const char * t = s;
				unsigned int l = 0;
				while (*t && !is_notoken(*t) && *t != '(' && *t != ')')
				{
					if(*t == '\n') { line++; }
					if(*t == '\'')
					{
						t++;
						l++;
						// very simple quote/string handling
						while( *t && *t != '\'' )
						{
							t++;
							l++;
						}
						if(*t == '\'')
						{
							t++;
							l++;
							break; // end quote is end of a token, is_notoken or not
						}
					}
					else
					{
						t++;
						l++;
					}
				}
				if(l>0)
				{
					a_tokens.push_back(Token(t_stdstring(s, l), line));
				}
				s = t;
			}
		}
		else
		{
			const char * t = s;
			unsigned int l = 0;
			while (*t && !is_notoken(*t))
			{
				if(*t == '\n') { line++; }
				t++;
				l++;
			}
			t_stdstring key(s, l);
			s = t;
			while (is_ws(*s))
			{
				if(*s == '\n') { line++; }
				s++;
			}
			t = s;
			l = 0;
			while (*t && !is_eol(*t))
			{
				if(*t == '\n') { line++; }
				t++;
				l++;
			}
			t_stdstring value(s, l);
			s = t;
		}
	}
}


void Evaluator::compile(const t_stdstring &a_buffer)
{
	t_stdlist<t_token> tokens;
	tokenize(tokens, a_buffer);
	sp<Debug> spDebug( new Debug() );
	spDebug->m_depth = 0;

	// make sure regex is good enough
	{
		Regex re_real("[-+]?[0-9]*\\.?[0-9]+");
		if(!re_real.match("1.23"))
		{
			spDebug->log("error", "REGEX library is not sufficient");
			return;
		}
	}


	while(tokens.size() > 0)
	{
		sp<Node> spNode;
		spNode = new Node();
		spNode->compile(tokens, this, NULL, spDebug);
		m_nodes.push_back(spNode);
	}
}


unsigned int Evaluator::index(const t_stdstring &a_name)
{
	t_stdmap<t_stdstring, unsigned int>::iterator i_name = m_var_names.find(a_name);
	unsigned int i_var = 0;
	if(i_name == m_var_names.end())
	{
		i_var = m_variables->size();
		m_var_names[a_name] = i_var;
		m_variables->resize(i_var+1);
		m_aggregates.resize(i_var+1);
	}
	else
	{
		i_var = i_name->second;
	}
	return i_var;
}

Record Evaluator::create(const t_stdstring &a_name, sp< AccessorSet > a_spSA)
{
	t_stdmap< sp< AccessorSet >, sp< Layout > >::iterator i_layout =
		m_layouts.find(a_spSA);
	sp<Layout> spLayout;
	if(i_layout == m_layouts.end())
	{
		spLayout = m_scope->declare(a_name.c_str());
		m_layouts[a_spSA] = spLayout;
	}
	else
	{
		spLayout = m_layouts[a_spSA];
	}

	sp<Aggregate> spAggregate( new Aggregate() );
	spAggregate->bakeTypes(a_spSA, spLayout);


	unsigned int i_instance = index(a_name);
	Record r_instance;
	if(!variable(i_instance).isValid())
	{
		r_instance = createRecord(spAggregate->m_spLayout);
		assignVariable(i_instance, r_instance, spAggregate);
	}
	return variable(i_instance);
}


void Evaluator::addFunction(const t_stdstring &a_function, const t_stdstring &a_component)
{
	Registry::FactoryLocation *pLocation = NULL;
	sp<Registry> spRegistry = m_scope->registry();
	t_stdmap<String,Registry::FactoryLocation*>::const_iterator it=spRegistry->factoryMap().begin();
	while(it!=spRegistry->factoryMap().end())
	{
		const String &factoryName= it->first;
		if(factoryName.dotMatch(a_component.c_str()))
		{
			pLocation=it->second;
			break;
		}
		it++;
	}

	m_factory_locations[a_function.c_str()] = pLocation;
}

sp<FunctionI> Evaluator::createFunction(const t_stdstring &a_function)
{
	Registry::FactoryLocation *pLocation = NULL;
	t_stdmap<t_stdstring, t_factory_location>::iterator i_function;
	i_function = m_factory_locations.find(a_function.c_str());
	if(i_function != m_factory_locations.end())
	{
		pLocation = i_function->second;
	}

	sp<FunctionI> spFunction;

	if(pLocation != NULL)
	{
		spFunction = pLocation->m_spLibrary->create(pLocation->m_factoryIndex);
	}

	return spFunction;
}

void Evaluator::makeVariable(Node *a_node)
{
	(*m_variables)[a_node->m_leaf] = createRecord(a_node->m_aggregate->m_spLayout);
	m_aggregates[a_node->m_leaf] = a_node->m_aggregate;
}

Record Evaluator::createRecord(sp<Layout> a_layout)
{
	Record r_e = m_scope->createRecord(a_layout);
	m_rg->add(r_e);
	return r_e;
}


Aggregate::Aggregate(void)
{
}

Aggregate::~Aggregate(void)
{
}

bool RealFunction::compile(sp<RecordGroup> a_rg, Record &a_return, t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_aggregate = new Aggregate();
	m_asVariable = new AsVariable();
	m_aggregate->bakeTypes(m_asVariable, a_rg->scope()->declare("l_eval_real"));
	a_return = a_rg->scope()->createRecord(m_aggregate->m_spLayout);

	m_valid = true;

	if( !m_asVariable->check(a_return) )
	{
		m_valid = false;
	}

	for(unsigned int i = 0; i < a_argv.size(); i++)
	{
		if( !m_asVariable->check(a_argv[i]) )
		{
			a_msg = "RealFunction argument not a real";
			m_valid = false;
			break;
		}
	}

	return m_valid;
}


void RealFunction::initialize(void)
{
}


void EvaluatorLog::log(const std::string &a_message,
	std::map<std::string,std::string> &a_attributes)
{
	if(m_newline)
	{
#if FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64
		if(a_attributes["group"] == "error")
		{
			std::cout << "[31m[1m" ;
		}
		else if(a_attributes["group"] == "warning")
		{
			std::cout << "[33m[1m" ;
		}
		else
		{
			std::cout << "[34m" ;
		}
#endif

		output(std::cout,a_attributes["group"].c_str(),7,false);

#if FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64
		std::cout << "[0m" ;
#endif
		std::cout << " ";
	}

	std::cout << a_message.c_str();
	std::cout.flush();

	const char* message=a_message.c_str();
	const int len=(int)(strlen(message));
	m_newline=(message[len-1]=='\n');
}



} /* namespace ext */
} /* namespace fe */

