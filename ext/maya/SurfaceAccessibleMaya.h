/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_SurfaceAccessibleMaya_h__
#define __maya_SurfaceAccessibleMaya_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Maya Surface Binding

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleMaya:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleMaya>
{
	public:
								SurfaceAccessibleMaya(void):
									m_pMeshArrayData(NULL),
									m_pFnMesh(NULL),
									m_pGroupIt(NULL),
									m_useTransform(FALSE)					{}
virtual							~SurfaceAccessibleMaya(void)
								{
									if(m_pFnMesh)
									{
										delete m_pFnMesh;
									}
									if(m_pGroupIt)
									{
										delete m_pGroupIt;
									}
								}

								//* as SurfaceAccessibleI

								using SurfaceAccessibleBase::bind;

virtual	void					bind(Instance a_instance)
								{	setMeshData(
											a_instance.cast<MDataHandle>()); }
virtual	BWORD					isBound(void)
								{	return (m_pMeshArrayData ||
											!m_meshData.asMesh().isNull()); }

virtual	void					groupNames(Array<String>& a_rGroupNames,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);

								using SurfaceAccessibleBase::discard;

virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name);

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

								//* TODO
/*
								using SurfaceAccessibleBase::subSurface;

virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group,
										SurfaceI::Restrictions a_restrictions);
*/

virtual	void					append_not(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										const SpatialTransform* a_pTransform);

virtual	void					instance(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										const Array<SpatialTransform>&
										a_rTransformArray);

								//* Maya specific
		void					setMeshNode(const MObject a_meshNode)
								{
									m_meshNode=a_meshNode;
								}
const	MObject					meshNode(void)
								{	return m_meshNode; }
		void					setMeshData(const MDataHandle a_meshData)
								{
									m_meshData=a_meshData;
									m_spSurfaceI=NULL;

									if(m_pMeshArrayData)
									{
										delete m_pMeshArrayData;
										m_pMeshArrayData=NULL;
									}

									if(m_pFnMesh)
									{
										delete m_pFnMesh;
									}

									//* not thread safe
									m_pFnMesh=new MFnMesh(m_meshData.asMesh());
								}
const	MDataHandle				meshData(void)
								{	return m_meshData; }

		void					setMeshArrayData(
										const MArrayDataHandle& a_meshArrayData)
								{
									m_pMeshArrayData=new MArrayDataHandle(
											a_meshArrayData);
									m_spSurfaceI=NULL;

									m_meshData=MDataHandle();

									if(m_pFnMesh)
									{
										delete m_pFnMesh;
									}

									m_pFnMesh=NULL;
								}
		MArrayDataHandle&		meshArrayData(void)
								{	return *m_pMeshArrayData; }
		void					setGroupIt(MItGeometry* a_pGroupIt)
								{
									if(m_pGroupIt)
									{
										delete m_pGroupIt;
									}
									m_pGroupIt=a_pGroupIt;
								}

		void					clearTransform(void)
								{
									m_useTransform=FALSE;
								}
		void					setTransform(SpatialTransform a_transform)
								{
									m_useTransform=TRUE;
									m_transform=a_transform;
								}

	protected:

virtual	void					reset(void);

		sp<SurfaceAccessorMaya>	createAccessor(void)
								{
									sp<SurfaceAccessorMaya> spAccessor(
											new SurfaceAccessorMaya);
									spAccessor->setMaster(registry()->master());
									spAccessor->setMeshNode(m_meshNode);
									spAccessor->setMeshData(m_meshData);
									spAccessor->setGroupIt(m_pGroupIt);
									if(m_useTransform)
									{
										spAccessor->setTransform(m_transform);
									}
									else
									{
										spAccessor->clearTransform();
									}
									return spAccessor;
								}

	private:
		sp<SurfaceI>		m_spSurfaceI;

		MObject				m_meshNode;
		MDataHandle			m_meshData;
		MArrayDataHandle*	m_pMeshArrayData;
		MFnMesh*			m_pFnMesh;
		MItGeometry*		m_pGroupIt;

		BWORD				m_useTransform;
		SpatialTransform	m_transform;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_SurfaceAccessibleMaya_h__ */
