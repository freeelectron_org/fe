/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_IMO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void ImportOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	//* page Config
	catalog<String>("format")="auto";
	catalog<String>("format","label")="Format";
	catalog<String>("format","choice:0")="auto";
	catalog<String>("format","label:0")="Auto";
	catalog<String>("format","choice:1")="abc";
	catalog<String>("format","label:1")="Alembic";
	catalog<String>("format","choice:2")="fbx";
	catalog<String>("format","label:2")="Filmbox";
	catalog<String>("format","choice:3")="geo";
	catalog<String>("format","label:3")="Geo";
	catalog<String>("format","choice:4")="json";
	catalog<String>("format","label:4")="JSON";
	catalog<String>("format","choice:5")="lua";
	catalog<String>("format","label:5")="Lua";
	catalog<String>("format","choice:6")="ptx";
	catalog<String>("format","label:6")="Ptex";
	catalog<String>("format","choice:7")="rg";
	catalog<String>("format","label:7")="RecordGroup";
	catalog<String>("format","choice:8")="usd";
	catalog<String>("format","label:8")="USD";
	catalog<String>("format","choice:9")="xgen";
	catalog<String>("format","label:9")="XGen";
	catalog<String>("format","choice:10")="xml";
	catalog<String>("format","label:10")="XML";
	catalog<String>("format","choice:11")="yaml";
	catalog<String>("format","label:11")="YAML";
	catalog<String>("format","page")="Config";

	catalog<String>("loadfile")="";
	catalog<String>("loadfile","label")="Load File";
	catalog<String>("loadfile","suggest")="filename";
	catalog<String>("loadfile","page")="Config";
	catalog<String>("loadfile","hint")="Full pathname of file to import.";

	catalog<String>("searchPaths")="";
	catalog<String>("searchPaths","label")="Search Paths";
	catalog<String>("searchPaths","page")="Config";
	catalog<String>("searchPaths","hint")=
			"Delimited string of potential load paths."
			"  Each path can be absolute or relative."
			"  Relative paths are used in whatever sense the process"
			" thinks the present working directory is."
			"  If any paths are supplied,"
			" the file must be explicitly found in one of those paths."
			"  Otherwise, the loadfile string is used"
			" without first checking if anything exists.";
	catalog<bool>("searchPaths","joined")=true;

	catalog<String>("searchPathsDelimiter")=":";
	catalog<String>("searchPathsDelimiter","label")="Delimiter";
	catalog<String>("searchPathsDelimiter","page")="Config";
	catalog<String>("searchPathsDelimiter","hint")=
			"Characters used to separate paths in the string."
			"  Look carefully as a simple space is a common delimiter.";

	catalog<Real>("frame")=0.0;
	catalog<Real>("frame","high")=1e3;
	catalog<Real>("frame","max")=1e6;
	catalog<String>("frame","label")="Frame";
	catalog<String>("frame","page")="Config";
	catalog<bool>("frame","joined")=true;

	catalog<String>("fallbackFrame")="100";
	catalog<String>("fallbackFrame","label")="Fallback Frame";
	catalog<String>("fallbackFrame","page")="Config";
	catalog<bool>("fallbackFrame","joined")=true;

	catalog<bool>("cacheFrames")=true;
	catalog<String>("cacheFrames","label")="Cache Frames";
	catalog<String>("cacheFrames","page")="Config";
	catalog<String>("cacheFrames","hint")=
			"Store a copy of each frame as it is loaded.";

	catalog<String>("options")="";
	catalog<String>("options","label")="Options";
	catalog<String>("options","page")="Config";
	catalog<String>("options","hint")=
			"Generalized option string passed to the import driver."
			"  Each option is separated by whitespace and follows the form"
			" 'option=\"value1 value2\"' (without the single quotes)."
			"  The 'equals' predicate is not needed if there are no values"
			" and the quotes are not needed if there is only one value.";

	catalog<bool>("filterJoints")=true;
	catalog<String>("filterJoints","label")="Filter Joints";
	catalog<String>("filterJoints","page")="Config";
	catalog<bool>("filterJoints","joined")=true;

	catalog<String>("filterFile")="";
	catalog<String>("filterFile","label")="Filter File";
	catalog<String>("filterFile","suggest")="filename";
	catalog<String>("filterFile","enabler")="filterJoints";
	catalog<String>("filterFile","page")="Config";
	catalog<bool>("filterFile","joined")=true;

	catalog<String>("filterSpec")="";
	catalog<String>("filterSpec","label")="Spec";
	catalog<String>("filterSpec","enabler")="filterJoints";
	catalog<String>("filterSpec","page")="Config";

	catalog<String>("extractNodes")="";
	catalog<String>("extractNodes","label")="Extract Sub Nodes";
	catalog<String>("extractNodes","page")="Config";
	catalog<bool>("extractNodes","joined")=true;
	catalog<String>("extractNodes","hint")=
			"Append these named subnodes into the output surface."
			"  Each space-delimited substring is an individual pattern."
			"  An attribute qualifies if it matches"
			" any of the substring patterns."
			"  Wildcard matching uses globbing unless Regex is turned on.";

	catalog<bool>("extractRegex")=false;
	catalog<String>("extractRegex","label")="Regex";
	catalog<String>("extractRegex","page")="Config";
	catalog<String>("extractRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("configuration")="";
	catalog<String>("configuration","label")="Configuration";
	catalog<String>("configuration","suggest")="multiline";
	catalog<String>("configuration","page")="Config";
	catalog<String>("configuration","hint")=
			"Free form data passed to the import driver."
			"  The purpose is similar to Options,"
			" but no formatting is imposed at this level.";

	//* page Direct
	catalog<String>("inputString")="";
	catalog<String>("inputString","label")="Direct Input";
	catalog<String>("inputString","suggest")="multiline";
	catalog<String>("inputString","page")="Direct";

	catalog<bool>("writeOutput")=TRUE;
	catalog<bool>("writeOutput","hidden")=TRUE;

	catalog<bool>("replaceOutput")=FALSE;
	catalog<bool>("replaceOutput","hidden")=TRUE;

	catalog< sp<Catalog> >("settings");

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=TRUE;

	catalog< sp<Component> >("Second Surface");
	catalog<bool>("Second Surface","optional")=TRUE;

	catalog< sp<Component> >("Third Surface");
	catalog<bool>("Third Surface","optional")=TRUE;

	catalog< sp<Component> >("Fourth Surface");
	catalog<bool>("Fourth Surface","optional")=TRUE;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<bool>("Output Surface","temporal")=true;
}

void ImportOp::handle(Record& a_rSignal)
{
#if FE_IMO_DEBUG
	feLog("ImportOp::handle node \"%s\"\n",name().c_str());
#endif

	const String loadfile=catalog<String>("loadfile");
	const String searchPaths=catalog<String>("searchPaths");
	const String searchPathsDelimiter=catalog<String>("searchPathsDelimiter");
	const String inputString=catalog<String>("inputString");
	const String options=catalog<String>("options");
	const String configuration=catalog<String>("configuration");
	const BWORD filterJoints=catalog<bool>("filterJoints");
	const String filterFile=filterJoints? catalog<String>("filterFile"): "";
	const String filterSpec=catalog<String>("filterSpec");
	const BWORD cacheFrames=catalog<bool>("cacheFrames");
	const BWORD writeOutput=catalog<bool>("writeOutput");
	const BWORD replaceOutput=catalog<bool>("replaceOutput");

	catalog<String>("summary")="";

	if(loadfile.empty() && inputString.empty())
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle loadfile and inputString empty\n");
#endif
		catalog<String>("summary")="NO FILE OR STRING";
		return;
	}

	catalog<String>("summary")="FAILED";

	String format=catalog<String>("format");

#if FE_IMO_DEBUG
	feLog("ImportOp::handle load \"%s\" as \"%s\" from \"%s\" by \"%s\"\n",
			loadfile.c_str(),format.c_str(),
			searchPaths.c_str(),searchPathsDelimiter.c_str());
#endif

	if(format=="auto")
	{
		format=String(strrchr(loadfile.c_str(),'.')).prechop(".");
#if FE_IMO_DEBUG
		feLog("ImportOp::handle auto format as \"%s\"\n",format.c_str());
#endif
	}

	if(m_loadedFormat!=format)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle format \"%s\" -> \"%s\"\n",
				m_loadedFormat.c_str(),format.c_str());
#endif

		m_loadedFormat=format;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedFile!=loadfile)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle file \"%s\" -> \"%s\"\n",
				m_loadedFile.c_str(),loadfile.c_str());
#endif

		m_loadedFile=loadfile;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedString!=inputString)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle imput string changed\n");
#endif

		m_loadedString=inputString;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedPaths!=searchPaths)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle paths \"%s\" -> \"%s\"\n",
				m_loadedPaths.c_str(),searchPaths.c_str());
#endif

		m_loadedPaths=searchPaths;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedPathsDelimiter!=searchPathsDelimiter)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle delimiter \"%s\" -> \"%s\"\n",
				m_loadedPathsDelimiter.c_str(),searchPathsDelimiter.c_str());
#endif

		m_loadedPathsDelimiter=searchPathsDelimiter;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedFilter!=filterFile)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle filter \"%s\" -> \"%s\"\n",
				m_loadedFilter.c_str(),filterFile.c_str());
#endif

		m_loadedFilter=filterFile;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedSpec!=filterSpec)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle spec \"%s\" -> \"%s\"\n",
				m_loadedSpec.c_str(),filterSpec.c_str());
#endif

		m_loadedSpec=filterSpec;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedOptions!=options)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle options \"%s\" -> \"%s\"\n",
				m_loadedOptions.c_str(),options.c_str());
#endif

		m_loadedOptions=options;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}
	if(m_loadedConfiguration!=configuration)
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle configuration changed\n");
#endif

		m_loadedConfiguration=configuration;

		m_spLoadAccessible=NULL;
		m_frameCache.clear();
	}

	sp<SurfaceAccessibleI> spInputAccessible;
	if(access(spInputAccessible,"Input Surface",e_quiet))
	{
		if(catalogOrDefault<bool>("Input Surface","replaced",true))
		{
#if FE_IMO_DEBUG
			feLog("ImportOp::handle Input Surface replaced\n");
#endif
			m_frameCache.clear();
		}
	}

	const BWORD checkPrimitives=(format=="abc" || format=="xml");

	const BWORD quiet=TRUE;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(writeOutput && !accessOutput(spOutputAccessible,a_rSignal)) return;

	if(m_spLoadAccessible.isNull())
	{
		m_spLoadAccessible=registry()->create(
				"SurfaceAccessibleI.*.*."+format,quiet);
	}
	if(m_spLoadAccessible.isNull())
	{
		feLog("ImportOp::handle failed to invoke handler for '%s'\n",
				format.c_str())

		catalog<String>("warning")+=
				"no usable handler for "+format+";";

		if(format != "abc")
		{
			feLog("ImportOp::handle trying abc\n");

			m_spLoadAccessible=registry()->create("SurfaceAccessibleI.*.*.abc");

			catalog<String>("warning")+=m_spLoadAccessible.isValid()?
					"used abc;": "tried abc;";
		}
	}
	if(m_spLoadAccessible.isNull())
	{
		m_spLoadAccessible=registry()->create("SurfaceAccessibleI");

		if(m_spLoadAccessible.isValid())
		{
			catalog<String>("warning")+=" used arbitrary handler " +
					m_spLoadAccessible->name() + ";";
		}
		else
		{
			catalog<String>("warning")+=" tried to create any handler;";
		}
	}
	if(m_spLoadAccessible.isNull())
	{
		catalog<String>("error")+="no format drivers found";

		catalog<String>("summary")="NO DRIVER";
		return;
	}

#if FE_IMO_DEBUG
	feLog("ImportOp::handle using \"%s\"\n",
			m_spLoadAccessible->name().c_str());
#endif

	sp<StringFilterI> spStringFilter;
	if(!filterFile.empty())
	{
		const String filterFormat=
			String(strrchr(filterFile.c_str(),'.')).prechop(".");

		spStringFilter=registry()->create("StringFilterI.*.*."+filterFormat);
		if(spStringFilter.isValid())
		{
			spStringFilter->configure(filterSpec);
			if(!spStringFilter->load(filterFile))
			{
				spStringFilter=NULL;
			}
		}

		if(!spStringFilter.isValid())
		{
			catalog<String>("warning")+=
					"failed to access filter file \""+filterFile+"\"";
		}
	}

	Real frame=catalog<Real>("frame");

//	feLog("ImportOp::handle loadfile \"%s\"\n",loadfile.c_str());
//	feLog("  expression \"%s\"\n",
//			catalog<String>("loadfile","expression").c_str());
//	feLog("frame %.6G\n",frame);
//	feLog("  expression \"%s\"\n",
//			catalog<String>("frame","expression").c_str());

	if(frame<startFrame(a_rSignal) || frame>endFrame(a_rSignal))
	{
		frame=currentFrame(a_rSignal);
	}

	const I32 cacheIndex=frame*1000;	//* TODO precision param
	if(cacheFrames)
	{
		sp<SurfaceAccessibleI> spCacheFrame=m_frameCache[cacheIndex];
		if(spCacheFrame.isValid())
		{
#if FE_IMO_DEBUG
			feLog("ImportOp::handle use cache %d\n",cacheIndex);
#endif

			if(replaceOutput)
			{
				spOutputAccessible=spCacheFrame;
				catalog< sp<Component> >("Output Surface")=spOutputAccessible;
			}
			else if(spOutputAccessible.isValid())
			{
				spOutputAccessible->copy(spCacheFrame);
			}

			catalog<String>("summary")=loadfile.basename()+" CACHED";
			return;
		}
	}
	else
	{
		m_frameCache.clear();
	}

	const BWORD substitute=loadfile.match(".*[$]F.*");

	const I32 intFrame=frame;
	const String intText=fe::print(intFrame);
	const String realText=fe::print(frame);
	String modfile=loadfile.substitute("$F",fe::print(intFrame))
			.substitute("$FF",fe::print(frame));

#if FE_IMO_DEBUG
	feLog("ImportOp::handle"
			" start %.6G end %.6G current %.6G frame %.6G -> %.6G\n",
			startFrame(a_rSignal),endFrame(a_rSignal),currentFrame(a_rSignal),
			catalog<Real>("frame"),frame);
	feLog("ImportOp::handle sub %d load \"%s\"\n",substitute,modfile.c_str());
#endif

	sp<Catalog> spSettings=catalog< sp<Catalog> >("settings");
	if(spSettings.isNull())
	{
		spSettings=registry()->master()->createCatalog("ImportOp Settings");
		catalog< sp<Catalog> >("settings")=spSettings;
	}
	if(spSettings.isValid())
	{
		spSettings->catalog<Real>("frame")=frame;
		spSettings->catalog<String>("options")=options;
		spSettings->catalog<String>("configuration")=configuration;

		if(!inputString.empty())
		{
			spSettings->catalog<String>("inputString")=inputString;
		}

		if(spStringFilter.isValid())
		{
			spSettings->catalog< sp<Component> >("filter")=spStringFilter;
		}

		if(spInputAccessible.isValid() &&
				spInputAccessible->count(SurfaceAccessibleI::e_primitive))
		{
			spSettings->catalog< sp<Component> >("context")=spInputAccessible;
			spSettings->catalog< sp<Component> >("input0")=spInputAccessible;
		}

		sp<SurfaceAccessibleI> spSecondAccessible;
		if(access(spSecondAccessible,"Second Surface",e_quiet) &&
				spSecondAccessible->count(SurfaceAccessibleI::e_primitive))
		{
			spSettings->catalog< sp<Component> >("input1")=spSecondAccessible;
		}

		sp<SurfaceAccessibleI> spThirdAccessible;
		if(access(spThirdAccessible,"Third Surface",e_quiet) &&
				spThirdAccessible->count(SurfaceAccessibleI::e_primitive))
		{
			spSettings->catalog< sp<Component> >("input2")=spThirdAccessible;
		}

		sp<SurfaceAccessibleI> spFourthAccessible;
		if(access(spFourthAccessible,"Fourth Surface",e_quiet) &&
				spFourthAccessible->count(SurfaceAccessibleI::e_primitive))
		{
			spSettings->catalog< sp<Component> >("input3")=spFourthAccessible;
		}
	}

	U32 primitiveCount=0;
	String fullname=find(modfile);
	if(fullname.empty() && inputString.empty())
	{
		feLog("ImportOp::handle"
				" node \"%s\" failed to find:\n"
				"  \"%s\"\n",name().c_str(),modfile.c_str());

		m_spLoadAccessible=NULL;

		if(!checkPrimitives)
		{
			catalog<String>("error")+="file not found;";
		}
	}
	else if(!m_spLoadAccessible->load(fullname,spSettings))
	{
		feLog("ImportOp::handle"
				" node \"%s\" failed to load:\n"
				"  \"%s\"\n"
				"  with filter \"%s\"\n",
				name().c_str(),modfile.c_str(),filterFile.c_str());

		m_spLoadAccessible=NULL;

		if(!checkPrimitives)
		{
			catalog<String>("error")+="load failed;";
		}
	}
	else if(checkPrimitives)
	{
		primitiveCount=m_spLoadAccessible->count(
				SurfaceAccessibleI::e_primitive);
		if(!primitiveCount)
		{
			feLog("ImportOp::handle"
					" node \"%s\" parsed zero primitives:\n"
					"  \"%s\"\n"
					"  with filter \"%s\"\n",
					name().c_str(),modfile.c_str(),filterFile.c_str());
		}
	}

#if FE_IMO_DEBUG
	if(m_spLoadAccessible.isValid())
	{
		feLog("ImportOp::handle points %d primitives %d\n",
				m_spLoadAccessible->count(SurfaceAccessibleI::e_point),
				m_spLoadAccessible->count(SurfaceAccessibleI::e_primitive));
	}
#endif

	if(!primitiveCount && checkPrimitives)
	{
		if(!substitute)
		{
			catalog<String>("error")+="load failed (no substitution);";
			m_spLoadAccessible=NULL;
		}
		else
		{
			const String fallbackFrame=catalog<String>("fallbackFrame");

			modfile=loadfile.substitute("$F",fallbackFrame)
					.substitute("$FF",fallbackFrame);

			fullname=find(modfile);
			if(!fullname.empty() &&
					m_spLoadAccessible->load(modfile,spSettings) &&
					m_spLoadAccessible->count(SurfaceAccessibleI::e_primitive))
			{
				feLog("  -> using fallback frame \"%s\"\n",
						fallbackFrame.c_str());

				catalog<String>("warning")+=
						"substituting fallback frame \""+fallbackFrame+"\"";
			}
			else
			{
				feLog("  -> also failed using fallback frame \"%s\"\n",
						fallbackFrame.c_str());

				const Real current=currentFrame(a_rSignal);
				const String currentText=fe::print(current);

				modfile=loadfile.substitute("$F",fe::print(I32(current)))
						.substitute("$FF",currentText);

				fullname=find(modfile);
				if(!fullname.empty() &&
						m_spLoadAccessible->load(modfile,spSettings) &&
						m_spLoadAccessible->count(
						SurfaceAccessibleI::e_primitive))
				{
					feLog("  -> using current frame \"%s\"\n",
							currentText.c_str());

					catalog<String>("warning")+=
							"substituting current frame \""+currentText+"\"";
				}
				else
				{
					feLog("  -> also failed using current frame \"%s\"\n",
							currentText.c_str());

					catalog<String>("error")+=
							"load failed (with substitution);";
					m_spLoadAccessible=NULL;
				}
			}
		}
	}

	if(m_spLoadAccessible.isValid())
	{
#if FE_IMO_DEBUG
		feLog("ImportOp::handle copy to output\n");
#endif

		if(spOutputAccessible.isValid())
		{
			String extractNodes=catalog<String>("extractNodes");
			if(extractNodes.empty())
			{
				if(replaceOutput)
				{
					spOutputAccessible=m_spLoadAccessible;
					catalog< sp<Component> >("Output Surface")=
							spOutputAccessible;
				}
				else
				{
					spOutputAccessible->copy(m_spLoadAccessible);
				}
			}
			else
			{
				sp<SurfaceAccessorI> spLoadName;
				if(!access(spLoadName,m_spLoadAccessible,
						e_primitive,"name")) return;
				const I32 nameCount=spLoadName.isValid()?
						spLoadName->count(): 0;

				if(!catalog<bool>("extractRegex"))
				{
					extractNodes=extractNodes.convertGlobToRegex();
				}

				Array<String> copyPatternArray;
				String buffer=extractNodes;
				String pattern;
				while(!buffer.empty())
				{
					copyPatternArray.push_back(buffer.parse());
				}

				I32 appendCount=0;

				for(I32 nameIndex=0;nameIndex<nameCount;nameIndex++)
				{
					const String nodeName=spLoadName->string(nameIndex);

					BWORD match=copyPatternArray.size()? FALSE: TRUE;
					if(!match)
					{
						const U32 copyPatternCount=copyPatternArray.size();
						for(U32 patternIndex=0;patternIndex<copyPatternCount;
								patternIndex++)
						{
							const String pattern=
									copyPatternArray[patternIndex];

							if(nodeName.match(pattern))
							{
								match=TRUE;
								break;
							}
						}
					}
					if(match)
					{
#if FE_IMO_DEBUG
						feLog("ImportOp::handle extract %d/%d \"%s\"\n",
								nameIndex,nameCount,nodeName.c_str());
#endif

						Array<SpatialTransform> transformArray;
						spOutputAccessible->append(m_spLoadAccessible,nodeName,
								transformArray,sp<FilterI>(NULL));
						appendCount++;
					}
				}
			}
		}

		String elementString;
		elementString.sPrintf("%d pts %d prims",
				m_spLoadAccessible->count(SurfaceAccessibleI::e_point),
				m_spLoadAccessible->count(SurfaceAccessibleI::e_primitive));

		if(cacheFrames)
		{
#if FE_IMO_DEBUG
			feLog("ImportOp::handle store cache %d\n",cacheIndex);
#endif
			m_frameCache[cacheIndex]=m_spLoadAccessible;
			m_spLoadAccessible=NULL;
		}

		catalog<String>("summary")=format;

		const String basename=loadfile.basename();
		if(!basename.empty())
		{
			catalog<String>("summary")+=" "+basename;
		}
		else
		{
			catalog<String>("summary")+=" DIRECT";
		}

		catalog<String>("summary")+=" "+elementString;
	}

#if FE_IMO_DEBUG
	feLog("ImportOp::handle node \"%s\" done\n",name().c_str());
#endif
}

String ImportOp::find(String a_filename)
{
	const String searchPaths=catalog<String>("searchPaths");
	if(searchPaths.empty())
	{
		return a_filename;
	}

	const String searchPathsDelimiter=catalog<String>("searchPathsDelimiter");
	const String path=System::findFile(a_filename,
			searchPaths,searchPathsDelimiter);
	if(path.empty())
	{
		return "";
	}

	return path+"/"+a_filename;
}
