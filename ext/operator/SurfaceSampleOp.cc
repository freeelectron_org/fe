/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SSO_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceSampleOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("attribute");
	catalog<String>("attribute","label")="Attribute";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Source Surface");
}

void SurfaceSampleOp::handle(Record& a_rSignal)
{
#if FE_SSO_DEBUG
	feLog("SurfaceSampleOp::handle\n");
#endif

	catalog<String>("summary")="";

	const String attribute=catalog<String>("attribute");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputAttribute;
	if(!access(spOutputAttribute,spOutputAccessible,e_point,attribute)) return;

	sp<SurfaceAccessibleI> spSourceAccessible;
	if(!access(spSourceAccessible,"Source Surface")) return;

	sp<SurfaceI> spSource;
	if(!access(spSource,spSourceAccessible)) return;
	spSource->setRefinement(0);

	sp<SurfaceAccessorI> spSourcePosition;
	if(!access(spSourcePosition,spSourceAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spSourceVertices;
	if(!access(spSourceVertices,spSourceAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spSourceAttribute;
	if(!access(spSourceAttribute,spSourceAccessible,
			e_point,attribute)) return;

	sp<SurfaceAccessorI> spSourceProperties;
	if(!access(spSourceProperties,spSourceAccessible,
			e_primitive,e_properties)) return;

	const String sourceType=spSourceAttribute->type();

	const I32 pointCountSource=spSourcePosition->count();
	const I32 primitiveCountSource=spSourceVertices->count();

	const U32 pointCount=spOutputPoint->count();
	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const SpatialVector point=spOutputPoint->spatialVector(pointIndex);
		sp<SurfaceI::ImpactI> spImpact=spSource->nearestPoint(point);

		if(spImpact.isValid())
		{
			const I32 primitiveIndex=spImpact->primitiveIndex();
			const SpatialBary bary=spImpact->barycenter();

			if(!primitiveCountSource && primitiveIndex<pointCountSource)
			{
				//* NOTE presuming point cloud

				if(sourceType=="string")
				{
					const String sampleReal=
							spSourceAttribute->string(primitiveIndex);
					spOutputAttribute->set(pointIndex,sampleReal);
				}
				else if(sourceType=="integer")
				{
					const I32 sampleInteger=
							spSourceAttribute->integer(primitiveIndex);
					spOutputAttribute->set(pointIndex,sampleInteger);
				}
				else if(sourceType=="real")
				{
					const Real sampleReal=
							spSourceAttribute->real(primitiveIndex);
					spOutputAttribute->set(pointIndex,sampleReal);
				}
				else if(sourceType.match("vector."))
				{
					const SpatialVector sampleVector=
							spSourceAttribute->spatialVector(primitiveIndex);
					spOutputAttribute->set(pointIndex,sampleVector);

//					feLog("point %d/%d at %s hit %d sampleVector %s\n",
//							pointIndex,pointCount,c_print(point),
//							primitiveIndex,c_print(sampleVector));
				}
			}
			else if(primitiveIndex<primitiveCountSource)
			{
				BWORD openCurve=FALSE;
				if(spSourceProperties.isValid())
				{
					openCurve=spSourceProperties->integer(
							primitiveIndex,e_openCurve);
				}

				if(openCurve)
				{
					const I32 subCount=
							spSourceVertices->subCount(primitiveIndex);

					const Real realIndex=bary[0]*(subCount-1);
					const I32 subIndex=I32(realIndex>0.0?
							(realIndex<(subCount-1.0)? realIndex: subCount-2.0):
							0.0);
					const Real weight1=realIndex-subIndex;
					const Real weight0=1.0-weight1;
					const I32 p0=spSourceVertices->integer(
							primitiveIndex,subIndex);
					const I32 p1=(subIndex<subCount-1)?
							spSourceVertices->integer(
							primitiveIndex,subIndex+1): p0;

					if(sourceType=="string")
					{
						String sampleString;

						if(weight0>=0.5)
						{
							sampleString=spSourceAttribute->string(p0);
						}
						else
						{
							sampleString=spSourceAttribute->string(p1);
						}

						spOutputAttribute->set(pointIndex,sampleString);
					}
					else if(sourceType=="integer")
					{
						const I32 value0=spSourceAttribute->integer(p0);
						const I32 value1=spSourceAttribute->integer(p1);

						const I32 sampleInteger=
								value0*weight0+value1*weight1+0.5;

						spOutputAttribute->set(pointIndex,sampleInteger);
					}
					else if(sourceType=="real")
					{
						const Real value0=spSourceAttribute->real(p0);
						const Real value1=spSourceAttribute->real(p1);

						const Real sampleReal=value0*weight0+value1*weight1;

						spOutputAttribute->set(pointIndex,sampleReal);
					}
					else if(sourceType.match("vector."))
					{
						const SpatialVector value0=
								spSourceAttribute->spatialVector(p0);
						const SpatialVector value1=
								spSourceAttribute->spatialVector(p1);

						const SpatialVector sampleVector=
								value0*weight0+value1*weight1;

						spOutputAttribute->set(pointIndex,sampleVector);
					}
					continue;
				}

				//* TODO quads

				//* HACK presumes triangles, and in same vertex order
				const I32 p0=spSourceVertices->integer(primitiveIndex,0);
				const I32 p1=spSourceVertices->integer(primitiveIndex,1);
				const I32 p2=spSourceVertices->integer(primitiveIndex,2);

				if(sourceType=="string")
				{
					String sampleString;

					if(bary[0]>bary[1] && bary[0]>bary[2])
					{
						sampleString=spSourceAttribute->string(p0);
					}
					else if(bary[1]>bary[2])
					{
						sampleString=spSourceAttribute->string(p1);
					}
					else
					{
						sampleString=spSourceAttribute->string(p2);
					}

					spOutputAttribute->set(pointIndex,sampleString);
				}
				else if(sourceType=="integer")
				{
					const I32 value0=spSourceAttribute->integer(p0);
					const I32 value1=spSourceAttribute->integer(p1);
					const I32 value2=spSourceAttribute->integer(p2);

					const I32 sampleInteger=
							blend(bary,value0,value1,value2);

					spOutputAttribute->set(pointIndex,sampleInteger);
				}
				else if(sourceType=="real")
				{
					const Real value0=spSourceAttribute->real(p0);
					const Real value1=spSourceAttribute->real(p1);
					const Real value2=spSourceAttribute->real(p2);

					const Real sampleReal=
							blend(bary,value0,value1,value2);

					spOutputAttribute->set(pointIndex,sampleReal);
				}
				else if(sourceType.match("vector."))
				{
					const SpatialVector value0=
							spSourceAttribute->spatialVector(p0);
					const SpatialVector value1=
							spSourceAttribute->spatialVector(p1);
					const SpatialVector value2=
							spSourceAttribute->spatialVector(p2);

					const SpatialVector sampleVector=
							location(bary,value0,value1,value2);

					spOutputAttribute->set(pointIndex,sampleVector);
				}
			}

//			feLog("%d/%d point %s prim %d bary %s",
//					pointIndex,pointCount,c_print(point),
//					primitiveIndex,c_print(bary));
//			feLog(" p %d %d %d val %.6G %.6G %.6G -> %.6G\n",
//					p0,p1,p2,value0,value1,value2,sampleReal);
		}
	}

	catalog<String>("summary")=attribute;

#if FE_SSO_DEBUG
	feLog("SurfaceSampleOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
