/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOperateDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<AttachOp>(
			"OperatorSurfaceI.AttachOp.fe.prototype");
	pLibrary->add<BrushTestOp>(
			"OperatorSurfaceI.BrushTestOp.fe.prototype");
	pLibrary->add<DrawTestOp>(
			"OperatorSurfaceI.DrawTestOp.fe.prototype");
	pLibrary->add<IdleSpinOp>(
			"OperatorSurfaceI.IdleSpinOp.fe.prototype");
	pLibrary->add<JunkOp>(
			"OperatorSurfaceI.JunkOp.fe.prototype");
	pLibrary->add<MimicOp>(
			"OperatorSurfaceI.MimicOp.fe.prototype");
	pLibrary->add<SurfaceDrawOp>(
			"OperatorSurfaceI.SurfaceDrawOp.fe.prototype");
	pLibrary->add<SurfaceWaveOp>(
			"OperatorSurfaceI.SurfaceWaveOp.fe.prototype");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
