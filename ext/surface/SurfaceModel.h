/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_Surface_h__
#define __surface_Surface_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SurfaceModel RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceModel:
	virtual public Recordable,
	public CastableAs<SurfaceModel>
{
	public:
		Functor<String>				searchName;
		Functor< sp<Component> >	searchComponent;
		Functor< sp<RecordGroup> >	surfacePointRG;
		Functor< sp<RecordGroup> >	surfacePointSetRG;
		Functor< sp<RecordGroup> >	surfaceVertexRG;
		Functor< sp<RecordGroup> >	surfacePrimitiveRG;
		Functor< sp<RecordGroup> >	surfacePrimitiveSetRG;
		Functor< sp<RecordGroup> >	surfaceDetailRG;

				SurfaceModel(void)			{ setName("SurfaceModel"); }
virtual	void	addFunctors(void)
				{
					Recordable::addFunctors();

					add(searchName,				FE_SPEC("surf:searchName",
							"name of SpatialTreeI Component"));
					add(searchComponent,		FE_SPEC("surf:search",
							"Instance of SpatialTreeI Component"));
					add(surfacePointRG,			FE_SPEC("surf:pointgroup",
							"RecordGroup of surface points"));
					add(surfacePointSetRG,		FE_SPEC("surf:ptsetgroup",
							"RecordGroup of surface point sets"));
					add(surfaceVertexRG,		FE_SPEC("surf:vertgroup",
							"RecordGroup of surface vertices"));
					add(surfacePrimitiveRG,		FE_SPEC("surf:primgroup",
							"RecordGroup of surface primitives"));
					add(surfacePrimitiveSetRG,	FE_SPEC("surf:prsetgroup",
							"RecordGroup of surface primitive sets"));
					add(surfaceDetailRG,		FE_SPEC("surf:detgroup",
							"RecordGroup of surface details"));
				}
virtual	void	initializeRecord(void)
				{
					Recordable::initializeRecord();

					surfacePointRG.createAndSetRecordGroup();
					surfacePointSetRG.createAndSetRecordGroup();
					surfaceVertexRG.createAndSetRecordGroup();
					surfacePrimitiveRG.createAndSetRecordGroup();
					surfacePrimitiveSetRG.createAndSetRecordGroup();
					surfaceDetailRG.createAndSetRecordGroup();

					searchComponent.attribute()->setSerialize(FALSE);
				}
virtual	void	finalizeRecord(void)
				{
					Recordable::finalizeRecord();

					//* TODO may need to be more generalized than SpatialTreeI
					if(!searchName().empty())
					{
						searchComponent.createAndSetComponent(searchName());
					}
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_Surface_h__ */


