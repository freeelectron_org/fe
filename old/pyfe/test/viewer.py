#!/usr/bin/python

import sys
import fe
import fe.data
import fe.signal
import fe.window
import time

class MyDraw(fe.signal.HandlerFS):
    def run(self,record):
        print "draw here with PyOpenGL"
    def bind(self, signaler, layout):
        pass

fe.manage("fexNativeWindowDL")
fe.manage("fexDrawOpenGLDL")
fe.manage("fexViewerDL")

window = fe.window.WindowFS(fe.create("WindowI"))
signaler = fe.signal.SignalerFS(fe.create("SignalerI"))
eventcontext = fe.signal.HandlerFS(window.getEventContextI())
viewer = fe.window.ViewerFS(fe.create("ViewerI"))

scope = fe.data.Scope()
renderLayout = scope.declare("render")
renderLayout.populate("viewer_layer","I32")
signaler.insert(eventcontext,renderLayout)

render = scope.createRecord(renderLayout)

viewer.bind(window.object())

viewsignaler = fe.signal.SignalerFS(viewer.object())
drawer = MyDraw()
viewsignaler.insert(drawer,renderLayout)

window.open("test")

if len(sys.argv) > 1:
    viewtime = float(sys.argv[1])
else:
    viewtime = 0.0

starttime = time.time()
while not viewtime or time.time() - starttime < viewtime:
    signaler.signal(render)
    time.sleep(0.2)

