/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

ListCore::ListCore(void)
{
	m_pHead=NULL;
	m_pTail=NULL;
	m_length=0;
	m_autodestruct=false;
}

ListCore::~ListCore(void)
{
}

void ListCore::removeAll(void)
{
#if FE_CODEGEN<=FE_DEBUG
	BWORD result;

	while( m_pHead != NULL)
	{
		result=coreRemoveNode(m_pHead);
		FEASSERT(result);
	}
#else
	while( m_pHead != NULL)
	{
		coreRemoveNode(m_pHead);
	}
#endif
}

void ListCore::internalToHead(Context& context) const
{
#if FE_LIST_CHECKPOINTER
	context.setCorePointer(this);
#endif
	context.setCurrent(m_pHead);
}

void ListCore::internalPostIncrement(Context& context) const
{
#if FE_LIST_CHECKPOINTER
	context.checkCorePointer(this);
#endif
	NodeCore* pNodeCore=context.current();
	if(pNodeCore)
		context.setCurrent(pNodeCore->next());
	else if(!(context.isAtTailNull()))
		internalToHead(context);
	if(m_length && !context.current())
		context.setAtTailNull(true);
}

ListCore::NodeCore* ListCore::coreGetElement(I32 index) const
{
	Context context;
	internalToHead(context);

	I32 m;
	for(m=0;m<index;m++)
		internalPostIncrement(context);

	return context.current();
}

ListCore::NodeCore* ListCore::coreInsert(BWORD before,Context& context,
		NodeCore* pExistingNodeCore)
{
#if FE_LIST_CHECKPOINTER
	context.checkCorePointer(this);
#endif

	NodeCore* pNodeCore=pExistingNodeCore;
	if(!pNodeCore)
	{
		pNodeCore=newNode();
		if(!pNodeCore)
			return NULL;
	}

	NodeCore* pCurrent=context.current();
	NodeCore* pLeft;
	NodeCore* pRight;

	if(!m_length)
	{
		pLeft=NULL;
		pRight=NULL;
		m_pHead=pNodeCore;
		m_pTail=pNodeCore;
	}
	else if(pCurrent)
	{
		if(before)
		{
			pLeft=pCurrent->previous();
			pRight=pCurrent;
		}
		else
		{
			pLeft=pCurrent;
			pRight=pCurrent->next();
		}
	}
	else if(context.isAtTailNull())
	{
		// NULL after tail
		pLeft=m_pTail;
		pRight=NULL;
	}
	else
	{
		// NULL before head
		pLeft=NULL;
		pRight=m_pHead;
	}

	pNodeCore->setPrevious(pLeft);
	pNodeCore->setNext(pRight);

	if(pLeft)
		pLeft->setNext(pNodeCore);
	else
		m_pHead=pNodeCore;

	if(pRight)
		pRight->setPrevious(pNodeCore);
	else
		m_pTail=pNodeCore;

	m_length++;
	return pNodeCore;
}

BWORD ListCore::coreRemoveNode(NodeCore* pNodeCore)
{
	if(!pNodeCore)
		return false;

	internalDetachNode(pNodeCore);

	//* let node delete itself later
	pNodeCore->abandon();
	return true;
}

void ListCore::internalDetachNode(NodeCore* pNodeCore)
{
	NodeCore* pLeft;
	NodeCore* pRight;
	pLeft=pNodeCore->previous();
	pRight=pNodeCore->next();

	if(pLeft)
		pLeft->setNext(pRight);
	else
		m_pHead=pRight;

	if(pRight)
		pRight->setPrevious(pLeft);
	else
		m_pTail=pLeft;

	m_length--;
}

BWORD ListCore::coreMoveNode(BWORD before,Context& from,Context& to)
{
	NodeCore* pNodeCore=from.current();
	internalDetachNode(pNodeCore);
	return (coreInsert(before,to,pNodeCore)!=NULL);
}

void ListCore::NodeCore::decRef(void)
{
	const int references=releaseInternal();
#if FE_LIST_REF_DEBUG
	feLog("%p ListCore::NodeCore::decRef() %d->%d\n",
			this,references+1,references);
#endif
	if(!references)
	{
#if FE_LIST_REF_DEBUG
		feLog("%p ListCore::NodeCore release (heir=%p)\n",this,m_pHeir);
#endif

		if(m_pHeir)
			m_pHeir->decRef();

#if FE_LIST_REF_DEBUG
		fe_printf("%p ListCore::NodeCore delete\n",(U32)this);
#endif

		// TODO use an allocator policy
		delete this;
	}
}

} // namespace
