import sys
import os
import re
forge = sys.modules["forge"]

def setup(module):
    module.summary = []

    # TODO find
    incPath = os.environ["FE_OIIO_INCLUDE"]
    libPath = os.environ["FE_OIIO_LIB"]

    oiioversion_h = incPath + "/OpenImageIO/oiioversion.h"
#   if not os.path.exists(oiioversion_h):
#       module.summary += [ "no_header" ]
#       return

    srcList = [ "oiio.pmh",
                "ImageOIIO",
                "oiioDL" ]

    dll = module.DLL( "fexOiioDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexImageDLLib" ]

    oiioMajor = ""
    oiioMinor = ""
    oiioPatch = ""
    if os.path.exists(oiioversion_h):
        for line in open(oiioversion_h).readlines():
            if re.match("#define OIIO_VERSION_MAJOR.*", line):
                oiioMajor = line.split()[2].rstrip()
            if re.match("#define OIIO_VERSION_MINOR.*", line):
                oiioMinor = line.split()[2].rstrip()
            if re.match("#define OIIO_VERSION_PATCH.*", line):
                oiioPatch = line.split()[2].rstrip()

    version = oiioMajor + "." + oiioMinor + "." + oiioPatch
    if version != "..":
        module.summary += [ version ]

    module.includemap = { 'oiio' : incPath }

    module.cppmap= { 'std': forge.use_std("c++17") }

#   dll.linkmap = { "gfxlibs": forge.gfxlibs }
    dll.linkmap = {}

    if forge.fe_os == "FE_LINUX":
        dll.linkmap['oiio'] = "-Wl,-rpath='" + libPath + "'"
        dll.linkmap["oiio"] += " -L" + libPath + " -lOpenImageIO"
#       dll.linkmap["oiio"] += " -lOpenColorIO"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == "debug":
            dll.linkmap["oiio"] = "OpenImageIO_d.lib OpenImageIO_Util_d.lib"
            dll.linkmap["oiio"] += " boost_filesystem-vc140-mt-gd.lib"
            dll.linkmap["oiio"] += " boost_thread-vc140-mt-gd.lib"
            dll.linkmap["oiio"] += " Iex-3_2_d.lib"
            dll.linkmap["oiio"] += " IlmThread-3_2_d.lib"
            dll.linkmap["oiio"] += " Imath-3_1_d.lib"
            dll.linkmap["oiio"] += " libpng16d.lib"
            dll.linkmap["oiio"] += " OpenEXRCore-3_2_d.lib"
            dll.linkmap["oiio"] += " OpenEXR-3_2_d.lib"
            dll.linkmap["oiio"] += " tiffd.lib"
            dll.linkmap["oiio"] += " zlibd.lib"
        else:
            dll.linkmap["oiio"] = "OpenImageIO.lib OpenImageIO_Util.lib"
            dll.linkmap["oiio"] += " boost_filesystem-vc140-mt.lib"
            dll.linkmap["oiio"] += " boost_thread-vc140-mt.lib"
            dll.linkmap["oiio"] += " Iex-3_2.lib"
            dll.linkmap["oiio"] += " IlmThread-3_2.lib"
            dll.linkmap["oiio"] += " Imath-3_1.lib"
            dll.linkmap["oiio"] += " libpng16.lib"
            dll.linkmap["oiio"] += " OpenEXRCore-3_2.lib"
            dll.linkmap["oiio"] += " OpenEXR-3_2.lib"
            dll.linkmap["oiio"] += " tiff.lib"
            dll.linkmap["oiio"] += " zlib.lib"
        dll.linkmap["oiio"] += " deflatestatic.lib"
        dll.linkmap["oiio"] += " jpeg.lib"
        dll.linkmap["oiio"] += " lzma.lib"

    forge.deps( ["fexOiioDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOiioDL",                        None,   None) ]

    if not 'ray' in forge.modules_found:
        module.summary += [ "-ray" ]

    if 'ext.viewer' in forge.modules_confirmed:
        forge.tests += [
            ("xImage.exe",  "fexOiioDL 10",                     None,   None) ]

        if forge.media:
            forge.tests += [ ("xImage.exe",
                "fexOiioDL " + forge.media + "/image/FreeElectron64.png 10",
                                                                None,   None) ]

        if 'ray' in forge.modules_found:
            forge.tests += [
                ("xRay.exe",    "fexOiioDL 30",                 None,   None) ]
    else:
        module.summary += [ "-viewer" ]

#   module.Module('test')

def auto(module):
    incPath = os.environ["FE_OIIO_INCLUDE"]
    libPath = os.environ["FE_OIIO_LIB"]

    if incPath == '':
        return 'unset'

    test_file = """
#include <OpenImageIO/imagebuf.h>

int main(void)
{
    return(0);
}
    \n"""

    std_orig = forge.cppmap.get('std', "")
    forge.cppmap['std'] = forge.use_std("c++17")

    forge.includemap['oiio'] = incPath
#   forge.linkmap['oiio'] = '-L' + libPath + ' -lOpenImageIO'

    result = forge.cctest(test_file)

    forge.includemap.pop('oiio', None)
    forge.linkmap.pop('oiio', None)

    forge.cppmap['std'] = std_orig

    return result
