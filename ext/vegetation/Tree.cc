/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

#define FE_TREE_MT_SOLVE			FALSE	// should be TRUE
#define FE_TREE_MT_KINE				FALSE	// should be TRUE
#define FE_TREE_MT_DEBUG			FALSE

#define FE_TREE_HIT_DEBUG			FALSE

#define FE_TREE_PROFILE				FALSE
#define FE_TREE_TRACE				FALSE	// verbose kinematics
#define FE_TREE_EXPLICIT			FALSE	// zero the Jacobians
#define FE_TREE_PRECONDITION		TRUE	// precondition here vs solver
#define FE_TREE_PREMULTIPLY_MASS	TRUE	// force symmetry
#define FE_TREE_SOLVE_OFF_MAIN		FALSE	// use 2 other threads for solvers
#define FE_TREE_VISIBLE_MIN_RADIUS	0.001f

#define	FE_TREE_SOLVER_THRESHOLD	1e-4
#define	FE_TREE_COLLIDE_SPHERE		FALSE	// always treat collider as sphere
#define	FE_TREE_DAMP_REGION			1.2f	// vel drain, >=1 up to perhaps 2

#define	FE_TREE_MT_ACTION_QUIT		0x00010000
#define	FE_TREE_MT_ACTION_SOLVE		0x00020000
#define	FE_TREE_MT_ACTION_KINE		0x00040000
#define	FE_TREE_MT_ACTION_MASK		0x7fff0000
#define	FE_TREE_MT_INDEX_MASK		0x0000ffff

#define FE_TREE_MT					(FE_TREE_MT_SOLVE||FE_TREE_MT_KINE)
#define FE_TREE_MT_THREADS			0		// FE_TREE_SOLVE_OFF_MAIN? 2: 1;

namespace fe
{
namespace ext
{

void Tree::initialize(void)
{
	start();

	m_solver[0].setThreshold(FE_TREE_SOLVER_THRESHOLD);
	m_solver[1].setThreshold(FE_TREE_SOLVER_THRESHOLD);
//	m_solver[0].setPreconditioning(
//			ConjugateGradient<MatrixN,VectorN>::e_diagonal_A);
//	m_solver[1].setPreconditioning(
//			ConjugateGradient<MatrixN,VectorN>::e_diagonal_A);

	m_bundles=FE_TREE_MT_THREADS+1;
	m_stickTransformBundles.resize(m_bundles);
	m_stickScaleBundles.resize(m_bundles);
	m_stickBaseScaleBundles.resize(m_bundles);
	m_stickSliceBundles.resize(m_bundles);
	m_bundleSize.resize(m_bundles);

	for(U32 m=0;m<m_bundles;m++)
	{
		m_stickTransformBundles[m]=NULL;
		m_stickScaleBundles[m]=NULL;
		m_stickBaseScaleBundles[m]=NULL;
		m_stickSliceBundles[m]=NULL;
		m_bundleSize[m]=0;
	}
}

Tree::~Tree(void)
{
	stop();

	for(U32 m=0;m<m_bundles;m++)
	{
		delete[] m_stickSliceBundles[m];
		delete[] m_stickBaseScaleBundles[m];
		delete[] m_stickScaleBundles[m];
		delete[] m_stickTransformBundles[m];
	}
}

void Tree::reset(void)
{
	stop();

	const I32 stickCount=m_stickArray.size();
	for(I32 stickIndex=0;stickIndex<stickCount;stickIndex++)
	{
		const Stick* pStick=m_stickArray[stickIndex];
		if(pStick!=&m_root)
		{
			delete pStick;
		}
	}
	m_stickArray.clear();

	const I32 leafCount=m_leafArray.size();
	for(I32 leafIndex=0;leafIndex<leafCount;leafIndex++)
	{
		delete m_leafArray[leafIndex];
	}
	m_leafArray.clear();

	m_kineArray.clear();
	m_stickMap.clear();

	m_root.reset();

	start();
}

void Tree::start(void)
{
	m_boundLevel= -1;
	m_sticks=1;
	m_lastDeltaT=0.0f;
	m_pLeafPoints=NULL;
	m_pLeafNormals=NULL;

	set(m_location);
	set(m_rotation);
	set(m_effectorForce);

	setStick(0,&m_root);

#if FE_TREE_MT
	feLog("Tree::initialize start Gang\n");
	//* start helper threads
	m_spGang=new Gang<SolverWorker,I32>();
	m_spGang->start(sp<Counted>(this),FE_TREE_MT_THREADS);
#endif

	m_effectorForceArray.resize(FE_TREE_MT_THREADS+1,SpatialVector());

#if FE_TREE_PROFILE
	//* initialize profiling
	m_spProfiler=new Profiler("Tree Simulation");
	m_spProfileReset=new Profiler::Profile(m_spProfiler,"Reset");
	m_spProfilePopulate=new Profiler::Profile(m_spProfiler,"Populate");
	m_spProfileSolve=new Profiler::Profile(m_spProfiler,"Solve");
	m_spProfileSolveWait=new Profiler::Profile(m_spProfiler,"Solve Wait");
	for(U32 m=0;m<2;m++)
	{
		String name;
		name.sPrintf("%d Setup",m);
		m_spProfileSetup[m]=new Profiler::Profile(m_spProfiler,name);
		name.sPrintf("%d Solver",m);
		m_spProfileSolver[m]=new Profiler::Profile(m_spProfiler,name);
	}
	m_spProfileThreadWait=new Profiler::Profile(m_spProfiler,"Thread Wait");
	m_spProfileKine=new Profiler::Profile(m_spProfiler,"Kine");
	m_spProfileKineWait=new Profiler::Profile(m_spProfiler,"Kine Wait");
#endif
}

void Tree::stop(void)
{
#if FE_TREE_PROFILE
	feLog("%s:\n%s\n",m_spProfiler->name().c_str(),
			m_spProfiler->report().c_str());
#endif

#if FE_TREE_MT
	//* finish both threads
	for(U32 m=0;m<FE_TREE_MT_THREADS;m++)
	{
		m_spGang->post(FE_TREE_MT_ACTION_QUIT);
	}
	U32 count=FE_TREE_MT_THREADS;
	while(count)
	{
		I32 job;
		m_spGang->waitForDelivery(job);
		if(job==FE_TREE_MT_ACTION_QUIT)
		{
			count--;
		}
	}
#endif

	delete[] m_pLeafNormals;
	delete[] m_pLeafPoints;
}

void Tree::Stick::grow(sp<Tree>& rspTree,U32& stick,
		const U32 segment,const U32 level,
		const F32 a_fullLength,const F32 along,
		const F32 baseRotate,const F32 rotate)
{
//	feLog("a_fullLength %.6G\n",a_fullLength);
	rspTree->bindLevel(level);
	const TreeSeed& seedRV=rspTree->m_seedRV;
	const StickLevel& levelRV=rspTree->m_levelRV;

	const F32 stiffPower=2.5f;

	const F32 stiffness=seedRV.stiffness();
	const F32 stillness=seedRV.stillness();
	const F32 density=seedRV.density();
	const U32 levels=seedRV.levels();

	const U32 segments=levelRV.segments();
	const U32 sideshoots=levelRV.sideshoots();
	const F32 bare=levelRV.bare();
	const F32 valleybase=levelRV.valleybase();
	const F32 valleytip=levelRV.valleytip();
	const F32 peakalong=levelRV.peakalong();
	const F32 powerbase=levelRV.powerbase();
	const F32 powertip=levelRV.powertip();
	const F32 forkrotate=levelRV.forkrotate()*degToRad;
	const F32 forktrench=levelRV.forktrench()*degToRad;
	const I32 forks=levelRV.forks();
	const F32 shootdown=levelRV.shootdown()*degToRad;
	const F32 shootramp=levelRV.shootramp()*degToRad;
	const F32 shootreact=levelRV.shootreact();
	const F32 shootdrain=levelRV.shootdrain();
	const F32 centerdeath=levelRV.centerdeath();
	const F32 sidedeath=levelRV.sidedeath();
	const F32 curvedown=levelRV.curvedown()*degToRad;
	const F32 areaRatio=levelRV.areaRatio();
	const F32 taper=levelRV.taper();
	const F32 fullLength=(a_fullLength>0.0)?
			a_fullLength: (1.0f-along)*levelRV.length();

	if(m_length<=0.0f)
	{
		m_length=fullLength/F32(segments);
		m_radius1=sqrtf(areaRatio/pi*fullLength);
/*
		m_radius1=sqrtf(areaRatio/pi*fullLength)*
				(1.0f-taper*segment/F32(segments));
*/
	}
	m_level=level;
	m_resolution=(level>0)? 3: 6;
	m_radius2=m_radius1*(1.0f-taper*m_length/fullLength);
	m_spring=stiffness*powf(m_radius1,stiffPower);
	m_drag=stillness*powf(m_radius1,stiffPower);

	//* V=1/3*pi*(r1^2+r1*r2+r2^2)*h
//	const F32 volume=m_length*pi*m_radius1*m_radius1;
	const F32 volume=0.333*m_length*pi*
			(m_radius1*m_radius1+m_radius1*m_radius2+m_radius2*m_radius2);

	m_mass=density*volume;
	BWORD fork=(forks==1 && !segment);
	if(segments<3)
	{
		fork=(I32(segment)<forks);
	}
	else if(forks>1)
	{
		const U32 remainder=segment*(forks-1)%(segments-2);
		fork=!remainder || remainder < (segment-1)*(forks-1)%(segments-2);
	}

#if FALSE
	feLog("%2d lv %d/%d sg %d/%d sp %.3G st %.3G"
			" rest=%.3G,%.3G l=%.3G r=%.3G m=%.3G f=%d rot=%.3G,%.3G\n",
			stick,level,levels,segment,segments,m_spring,m_drag,
			m_rest[0],m_rest[1],m_length,m_radius1,m_mass,fork,
			baseRotate,rotate);
#endif

	if(FE_INVALID_SCALAR(m_radius1))
	{
		feX("Tree::Stick::grow","invalid radius");
	}

	if(segment>=segments || level>=levels)
	{
		return;
	}

	const BWORD no_center=(segment+1==segments) ||
			(randomReal(0.0f,1.0f)<centerdeath);
	I32 sides=0;
	if(level+1<levels && fork && segment/F32(segments)>=bare)
	{
		for(U32 m=0;m<sideshoots;m++)
		{
			if(randomReal(0.0f,1.0f)>=sidedeath)
			{
				sides++;
			}
		}
	}

	const F32 childAlong=segment/F32(segments);
	const F32 shapeScale=shapeFunction(
			(childAlong-bare)/(1.0f-bare),valleybase,
			valleytip,peakalong,powerbase,powertip);
//	feLog("lv %d sh %.6G al %.6G vb %.6G vt %.6G pe %.6G pb %.6G pt %.6G\n",
//			level,shapeScale,childAlong,valleybase,
//			valleytip,peakalong,powerbase,powertip);

	const F32 totalArea=pi*m_radius2*m_radius2;
	F32 remainingArea=totalArea;
	for(I32 m=sides;m>=no_center;m--)
	{
		const U32 nextSegment=m? 0: segment+1;
		const F32 nextAlong=m? childAlong: along;
		const F32 sideRotate=m? pi+2.0f*pi*(m-1)/F32(sides): 0.0f;
		const F32 nextTrench=m? (forktrench*((segment%2)*2-1.0f)): 0.0;
		const F32 nextRotate=rotate+(fork? forkrotate: 0.0f)+
				sideRotate+nextTrench;
		const F32 nextBaseRotate=m? nextRotate: baseRotate;

		const I32 nextLevel=level+(m!=0);
		rspTree->bindLevel(nextLevel);

		const F32 nextMaxLength=fullLength*(m? levelRV.length(): 1.0);

		if(levelRV.form()=="Leaf")
		{
			U32 leafIndex=rspTree->newLeaf();
			attachLeaf(leafIndex);
			Tree::Leaf* pLeaf=
					fe_cast<Tree::Leaf>(rspTree->leaf(leafIndex));

			const F32 alongY=m/F32(sides);
			const F32 radial=m_radius1+alongY*(m_radius2-m_radius1)+
					nextMaxLength*levelRV.bare();

			set(pLeaf->m_normal,sinf(nextRotate),0.5f,cosf(nextRotate));
			//* HACK should we normalize it?

			pLeaf->m_center=radial*pLeaf->m_normal;
			pLeaf->m_center[1]=m_length*alongY;

			continue;
		}

		const F32 nextFullLength=(m)? nextMaxLength*shapeScale: fullLength;

		Stick* pStick=new Stick();
		pStick->m_index=stick++;
		rspTree->setStick(pStick->m_index,pStick);
		attach(pStick);

		if(level==0 && nextLevel==1)
		{
			rspTree->addKine(pStick);
		}

		const U32 nextSegments=levelRV.segments();
		const F32 nextAreaRatio=levelRV.areaRatio();
		F32 nextArea=remainingArea;

		pStick->m_length=nextFullLength/F32(nextSegments);
		if(m>no_center)
		{
			pStick->m_radius1=sqrtf(nextAreaRatio/pi*nextFullLength);
			const F32 maxRadius=m_radius2*0.4f;
			if(pStick->m_radius1>maxRadius)
			{
				pStick->m_radius1=maxRadius;
			}
			nextArea=pi*pStick->m_radius1*pStick->m_radius1;
			remainingArea-=nextArea*(no_center? 1.0f: shootdrain);
		}
		else
		{
			pStick->m_radius1=sqrtf(remainingArea/pi);
			m_radius2=pStick->m_radius1;
		}

		if(fork)
		{
			pStick->m_rest= -(shootdown+nextAlong*shootramp)*
					(m? 1.0: shootreact)*
					(1.0f-nextArea/totalArea)*
					Vector2(sinf(nextRotate),cosf(nextRotate));
		}
		else
		{
			set(pStick->m_rest);
		}
		if(segments>1)
		{
			pStick->m_rest-=curvedown/F32(segments-1)*
					Vector2(sinf(nextBaseRotate),cosf(nextBaseRotate));
		}

		pStick->grow(rspTree,stick,nextSegment,nextLevel,
				nextFullLength,nextAlong,nextBaseRotate,nextRotate);
	}
}

F32 Tree::shapeFunction(const F32 along,const F32 valleybase,
		const F32 valleytip,const F32 peakalong,
		const F32 powerbase,const F32 powertip)
{
	if(along>=peakalong)
	{
		return valleytip+(1.0f-valleytip)*
				powf((1.0-along)/(1.0f-peakalong),powertip);
	}
	return valleybase+(1.0f-valleybase)*
			powf(along/peakalong,powerbase);
}

void Tree::bindLevel(const U32 level)
{
	if(m_boundLevel==I32(level))
	{
		return;
	}
	m_boundLevel=I32(level);

	sp<RecordGroup> spLevelGroup=m_seedRV.levelgroup();
	sp<RecordArray> spRA;

	RecordArrayView<StickLevel>	levelRAV;

	for(RecordGroup::iterator it=spLevelGroup->begin();
			it!=spLevelGroup->end();it++)
	{
		spRA= *it;
		levelRAV.bind(spRA);
		for(StickLevel& levelRV: levelRAV)
		{
			if(levelRV.level() == I32(level))
			{
				m_levelRV.bind(levelRV.record());
				return;
			}
		}
	}

	// TODO some intentional default vs simply last one read
}

void Tree::generate(const Record seed)
{
	//* first random very near zero
	randomReal(0.0f,1.0f);

	m_seedRV.bind(seed);
	feLog("Tree::generate levels=%d stiffness=%.6G density=%.6G\n",
			m_seedRV.levels(),m_seedRV.stiffness(),m_seedRV.density());

	sp<Tree> spTree(this);
	m_root.grow(spTree,m_sticks,0,0,-1.0f,0.0f,90.0f*degToRad,0.0f*degToRad);

	feLog("Tree::generate sticks %d leaves %d\n", numSticks(), numLeaves());

	m_useCorrection=0;
	prepare();
}

void Tree::addBranch(U32 a_fromIndex,U32 a_toIndex,U32 a_level,
	Real a_radius1,Real a_radius2,const SpatialVector& a_span,Real a_rigidity)
{
	Stick* pParent=NULL;
	Stick* pStick;

	//* TODO allow no parent when not root (disconnected)

	if(a_fromIndex>numSticks())
	{
		feLog("Tree::addBranch no parent from %d to %d\n",
				a_fromIndex,a_toIndex);
		return;
	}

	const SpatialVector zAxis(0.0,0.0,1.0);

	if(a_fromIndex)
	{
		pParent=m_stickArray[a_fromIndex-1];
		if(!pParent)
		{
			feLog("Tree::addBranch discard disconnected from %d to %d\n",
					a_fromIndex,a_toIndex);
			return;
		}

		pStick=new Stick();
		pParent->attach(pStick);

		if(pParent->m_level==0 && a_level==1)
		{
			addKine(pStick);
		}
	}
	else
	{
		pStick=&m_root;

		set(m_rotation,zAxis,unitSafe(a_span));
	}

	//* NOTE store for correction
	pStick->m_span=a_span;

	SpatialVector offset;
	rotateVector(inverse(m_rotation),a_span,offset);

	pStick->m_index=a_toIndex-1;
	setStick(pStick->m_index,pStick);

	if(m_sticks<pStick->m_index+1)
	{
		m_sticks=pStick->m_index+1;
	}

	const Real density=500.0;
	const Real length=magnitude(offset);
	const Real rigidity=m_uniformRigidity*a_rigidity;

	//* NOTE round solid section: I=pi*r^4/4
	//* k=F/y=3EI/L^3  (y is deflection distance aside)
	//* k=F/a=2EI/L^2  (a is deflection angle)
	//* E ~= 1e3-1e6

	//* NOTE power 4 seems to be correct, but behaves poorly

	//* tweak
#if FALSE
	const Real radius4=powf(a_radius1,4.0);
	pStick->m_spring=1e11*rigidity*radius4;
	pStick->m_drag=1e11*m_damping*radius4;
#else
	const Real radiusPow=powf(a_radius1,2.5);
	const Real lengthPow=powf(length,1.0);
	pStick->m_spring=1e8*rigidity*radiusPow/lengthPow;
	pStick->m_drag=1e8*m_damping*radiusPow;
#endif

	pStick->m_length=length;
	pStick->m_radius1=a_radius1;
	pStick->m_radius2=a_radius2;
	pStick->m_level=a_level;
	pStick->m_resolution=(a_level>0)? 3: 6;

	//* V=1/3*pi*(r1^2+r1*r2+r2^2)*h
//	const F32 volume=pStick->m_length*pi*radius1*radius1;
	const F32 volume=0.333*pStick->m_length*pi*
			(a_radius1*a_radius1+a_radius1*a_radius2+a_radius2*a_radius2);

	pStick->m_mass=density*volume;

	pStick->m_absRest=Vector2f(
			atan2(offset[0],offset[2]),
			atan2(offset[1],offset[2]));

	pStick->m_rest=pStick->m_absRest;
	if(pParent)
	{
		pStick->m_rest-=pParent->m_absRest;
		pStick->m_position=pParent->m_rest+pParent->m_position;
	}

	for(I32 m=0;m<2;m++)
	{
		if(pStick->m_rest[m]>pi)
		{
			pStick->m_rest[m]-=pi;
		}
		else if(pStick->m_rest[m]<-pi)
		{
			pStick->m_rest[m]+=pi;
		}
	}

//	feLog("Tree::addBranch from %d to %d level %d offset %s\n"
//			"  absRest %s rest %s\n",
//			a_fromIndex,a_toIndex,a_level,
//			c_print(offset),
//			c_print(pStick->m_absRest),
//			c_print(pStick->m_rest));
//	feLog("  length %.6G radius %.6G %.6G mass %.6G (1/%.6G) spring %.6G\n",
//			length,a_radius1,a_radius2,pStick->m_mass,1.0/pStick->m_mass,
//			pStick->m_spring);
//	feLog("  rotation %s\n",c_print(m_rotation));

	if(m_compensation)
	{
		//* angle compensation
		const Real phi=magnitude(pStick->m_rest);

		pStick->m_dirRest=unitSafe(offset);
		SpatialQuaternion rotRest;
		if(pParent)
		{
			set(rotRest,pParent->m_dirRest,pStick->m_dirRest);
		}
		else
		{
			set(rotRest,zAxis,pStick->m_dirRest);
		}

		Real radians;
		SpatialVector axis;
		rotRest.computeAngleAxis(radians,axis);

		if(phi>1e-3)
		{
			pStick->m_rest*=radians/phi;
		}

//		feLog("  dirRest %s\n",c_print(pStick->m_dirRest));
//		feLog("  rotRest %s (%.6G ^ %s)\n",
//				c_print(rotRest),radians*radToDeg,c_print(axis));
//		feLog("  want %.6G got %.6G mod rest %s\n",
//				radians,phi,c_print(pStick->m_rest));
	}

	m_useCorrection=m_correction;
}

void Tree::prepare(void)
{
	//* compact
	m_stickMap.resize(m_sticks);
	for(U32 index=0;index<m_sticks;index++)
	{
		m_stickMap[index]= -1;
	}

	U32 checkIndex=0;
	U32 getIndex=1;
	while(checkIndex<m_sticks)
	{
		if(!m_stickArray[checkIndex])
		{
			while(getIndex<m_sticks && !m_stickArray[getIndex])
			{
				getIndex++;
			}
			if(getIndex>=m_sticks)
			{
				break;
			}
			m_stickMap[getIndex]=checkIndex;
			m_stickArray[checkIndex]=m_stickArray[getIndex];
			m_stickArray[checkIndex]->m_index=checkIndex;

			m_stickArray[getIndex]=NULL;
		}
		else
		{
			m_stickMap[checkIndex]=checkIndex;
		}
		checkIndex++;
		getIndex++;
	}
	m_sticks=checkIndex;
	m_stickArray.resize(m_sticks);

	const U32 leaves=m_leafArray.size();

	delete[] m_pLeafNormals;
	delete[] m_pLeafPoints;
	m_pLeafPoints=new SpatialVector[leaves];
	m_pLeafNormals=new SpatialVector[leaves];

	for(U32 m=0;m<m_bundles;m++)
	{
		delete[] m_stickSliceBundles[m];
		delete[] m_stickBaseScaleBundles[m];
		delete[] m_stickScaleBundles[m];
		delete[] m_stickTransformBundles[m];
		m_stickTransformBundles[m]=new SpatialTransform[m_sticks];
		m_stickScaleBundles[m]=new SpatialVector[m_sticks];
		m_stickBaseScaleBundles[m]=new Real[m_sticks];
		m_stickSliceBundles[m]=new U32[m_sticks];
		m_bundleSize[m]=0;
	}

#if FE_TREE_PROFILE
	m_spProfileKineLimbArray.resize(m_kineArray.size());
	for(U32 m=0;m<m_spProfileKineLimbArray.size();m++)
	{
		String name;
		name.sPrintf("%d Kine Limb",m);
		m_spProfileKineLimbArray[m]=new Profiler::Profile(m_spProfiler,name);
	}
#endif

	m_im.reset(m_sticks);
	m_mass.reset(m_sticks);
	m_him.reset(m_sticks);

	m_identity.reset(m_sticks,m_sticks);
	setIdentity(m_identity);

	for(U32 m=0;m<2;m++)
	{
		m_tempVector1[m].reset(m_sticks);
		m_tempVector2[m].reset(m_sticks);
		m_b[m].reset(m_sticks);
		m_deltaV[m].reset(m_sticks);

//		m_A2[m].reset(m_sticks,m_sticks);
		m_tempMatrix[m].reset(m_sticks,m_sticks);
		m_A[m].reset(m_sticks,m_sticks);
		m_dfdx[m].reset(m_sticks,m_sticks);
//		m_dfdx2[m].reset(m_sticks,m_sticks);
//		m_hdfdx[m].reset(m_sticks,m_sticks);
		m_imdfdx[m].reset(m_sticks,m_sticks);
		m_imdfdv[m].reset(m_sticks,m_sticks);
	}

	sp<Tree> spTree(this);

	VectorN zero[2];
	zero[0].reset(m_sticks);
	zero[1].reset(m_sticks);
	set(zero[0]);
	set(zero[1]);
	m_root.forward_kine_recursive(0,spTree,0.0f,zero,zero,zero,
			m_effectorForce,FALSE);

//	FEASSERT(I32(m_sticks)==(1<<levels)-1);

	set(m_im);

	MatrixN dfdv[2];
	for(U32 m=0;m<2;m++)
	{
		dfdv[m].reset(m_sticks,m_sticks);
		set(dfdv[m]);

		set(m_dfdx[m]);
	}

	m_root.populate_static(spTree,m_dfdx,dfdv,m_im);
#if FE_TREE_EXPLICIT
	m_dfdx[0].clear();
	m_dfdx[1].clear();
	dfdv[0].clear();
	dfdv[1].clear();
#endif

	for(U32 m=0;m<2;m++)
	{
		premultiplyDiagonal(m_imdfdx[m],m_im,m_dfdx[m]);
		premultiplyDiagonal(m_imdfdv[m],m_im,dfdv[m]);
	}

	if(m_useCorrection)
	{
		m_useCorrection=2;
	}
}

void Tree::update(const Real deltaT)
{
#if FE_TREE_PROFILE
	m_spProfiler->begin();
#endif

	// HACK
	F32 new_deltaT=deltaT;//0.001f;

	if(new_deltaT<=0.0f)
	{
		return;
	}
	else if(m_lastDeltaT!=new_deltaT)
	{
//		feLog("Tree::update dt changed %.6G -> %.6G (%.6G FPS)\n",
//				m_lastDeltaT,new_deltaT,1.0f/new_deltaT);

		m_lastDeltaT=new_deltaT;

		const F32& h=m_lastDeltaT;

		m_him=h*m_im;
		for(U32 m=0;m<m_im.size();m++)
		{
			m_mass[m]=1.0f/m_im[m];
		}

		for(U32 m=0;m<2;m++)
		{
#if FE_TREE_PREMULTIPLY_MASS
			m_tempMatrix[m]=m_identity-h*m_imdfdv[m]-(h*h)*m_imdfdx[m];
			premultiplyInverseDiagonal(m_A[m],m_im,m_tempMatrix[m]);

//			feLog("%d premultiply mass on A from\n%s\n",
//					m,c_print(m_tempMatrix[m]));
//			feLog("to\n%s\n",c_print(m_A[m]));
//			feLog("im\n%s\n",c_print(m_im));
#else
			m_A[m]=m_identity-h*m_imdfdv[m]-(h*h)*m_imdfdx[m];
#endif

#if FE_TREE_PRECONDITION
			m_invSqrtA[m].reset(width(m_A[m]));
			for(U32 k=0;k<m_A[m].rows();k++)
			{
				m_invSqrtA[m][k]=1.0f/sqrt(m_A[m](k,k));
			}

			premultiplyDiagonal(m_tempMatrix[m],m_invSqrtA[m],m_A[m]);
			postmultiplyDiagonal(m_A[m],m_tempMatrix[m],m_invSqrtA[m]);

//			feLog("invSqrtA\n%s\n",c_print(m_invSqrtA[m]));
//			feLog("mid\n%s\n",c_print(m_tempMatrix[m]));
//			feLog("A'\n%s\n",c_print(m_A[m]));
#endif
//			m_hdfdx[m]=h*m_dfdx[m];
		}
	}

#if FE_TREE_PROFILE
	m_spProfileReset->start();
#endif

	for(U32 m=0;m<2;m++)
	{
		m_force[m].reset(m_sticks);
		m_velocity[m].reset(m_sticks);
		m_addPosition[m].reset(m_sticks);
		m_addVelocity[m].reset(m_sticks);
//		m_dfdx2[m].clear();
	}

#if FE_TREE_PROFILE
	m_spProfilePopulate->replace(m_spProfileReset);
#endif

	sp<Tree> spTree(this);
	m_root.populate_dynamic(spTree,m_force,m_velocity,
			m_addPosition,m_addVelocity,FALSE);

#if FE_TREE_EXPLICIT
	m_dfdx2[0].clear();
	m_dfdx2[1].clear();
#endif

#if FE_TREE_PROFILE
	m_spProfileSolve->replace(m_spProfilePopulate);
#endif

#if FE_TREE_MT_SOLVE
	runSolvers();
#else
	for(U32 m=0;m<2;m++)
	{
		solve(m);
	}
#endif

#if FE_TREE_PROFILE
	m_spProfileKine->replace(m_spProfileSolve);
#endif

	set(m_effectorForce);
	for(U32 m=0;m<m_effectorForceArray.size();m++)
	{
		set(m_effectorForceArray[m]);
	}

	for(U32 m=0;m<m_bundles;m++)
	{
		m_bundleSize[m]=0;
	}

#if FE_TREE_MT_KINE
	runKine();
#else
	m_root.forward_kine_recursive(0,spTree,m_lastDeltaT,m_deltaV,
			m_addPosition,m_addVelocity,m_effectorForce,FALSE);
#endif

	for(U32 m=0;m<m_effectorForceArray.size();m++)
	{
		m_effectorForce+=m_effectorForceArray[m];
	}

#if FE_TREE_PROFILE
	m_spProfileKine->finish();
	m_spProfiler->end();
#endif
}

void Tree::solve(U32 m)
{
#if FE_TREE_PROFILE
	m_spProfileSetup[m]->start();
#endif

/*
	m_A2=m_A[m]-h*premultiplyDiagonal(m_tempMatrix,m_him,m_dfdx2[m]);
	VectorN b=m_him*(m_force[m]+(m_hdfdx[m]+h*m_dfdx2[m])*m_velocity[m]+
			(m_dfdx[m]+m_dfdx2[m])*m_addPosition[m]);
*/

/*
	const F32& h=m_lastDeltaT;
	premultiplyDiagonal(m_A2[m],m_him,m_dfdx2[m]);
	m_A2[m]*= -h;
	m_A2[m]+=m_A[m];
	m_tempMatrix[m]=m_dfdx2[m];
	m_tempMatrix[m]*=h;
	m_tempMatrix[m]+=m_hdfdx[m];
	m_tempMatrix[m].transform(m_velocity[m],m_tempVector1[m]);
	m_tempVector1[m]+=m_force[m];
	m_tempMatrix[m]=m_dfdx[m];
	m_tempMatrix[m]+=m_dfdx2[m];
	m_tempMatrix[m].transform(m_addPosition[m],m_tempVector2[m]);
	m_tempVector1[m]+=m_tempVector2[m];
	m_him.transform(m_tempVector1[m],m_b[m]);
*/

//	m_A2[m]=m_A[m];

//	m_hdfdx[m].transform(m_velocity[m],m_tempVector1[m]);
	m_dfdx[m].transform(m_velocity[m],m_tempVector1[m]);
	m_tempVector1[m]*=m_lastDeltaT;

	m_tempVector1[m]+=m_force[m];
	m_dfdx[m].transform(m_addPosition[m],m_tempVector2[m]);
	m_tempVector1[m]+=m_tempVector2[m];

#if FE_TREE_PREMULTIPLY_MASS
//	m_him.transform(m_tempVector1[m],m_tempVector2[m]);
	componentMultiply(m_tempVector2[m],m_him,m_tempVector1[m]);

//	premultiplyInverseDiagonal(m_b[m],m_im,m_tempVector2[m]);
	componentMultiply(m_b[m],m_mass,m_tempVector2[m]);

//	feLog("%d premultiply M on b from\n%s\n",m,c_print(m_tempVector2[m]));
//	feLog("to\n%s\n",c_print(m_b[m]));
#else
//	m_him.transform(m_tempVector1[m],m_b[m]);
	componentMultiply(m_b[m],m_him,m_tempVector1[m]);
#endif

#if FE_TREE_PRECONDITION
//	m_tempVector1[m]=m_b[m];
//	premultiplyDiagonal(m_b[m],m_invSqrtA[m],m_tempVector1[m]);
	componentMultiply(m_b[m],m_invSqrtA[m],m_b[m]);

//	feLog("b=<%s>\n",c_print(m_tempVector1[m]));
//	feLog("b`=<%s>\n",c_print(m_b[m]));
#endif

#if FE_TREE_PROFILE
	m_spProfileSolver[m]->replace(m_spProfileSetup[m]);
#endif

	//* A2*dV=b solve for dV
//	m_solver[m].solve(m_deltaV[m],m_A2[m],m_b[m]);
	m_solver[m].solve(m_deltaV[m],m_A[m],m_b[m]);

#if FE_TREE_PROFILE
	m_spProfileSolver[m]->finish();
#endif

#if FE_TREE_PRECONDITION
//	m_tempVector1[m]=m_deltaV[m];
//	premultiplyDiagonal(m_deltaV[m],m_invSqrtA[m],m_tempVector1[m]);
	componentMultiply(m_deltaV[m],m_invSqrtA[m],m_deltaV[m]);

//	feLog("dV'=<%s>\n",c_print(m_tempVector1[m]));
//	feLog("dV=<%s>\n",c_print(m_deltaV[m]));
#endif
}

void Tree::Stick::populate_static(sp<Tree>& rspTree,
		MatrixN* dfdx,MatrixN* dfdv,VectorN& invMass)
{
	if(m_pParent)
	{
		U32 parent=m_pParent->m_index;

		dfdx[0](parent,parent)-=m_spring;
		dfdx[1](parent,parent)-=m_spring;

		dfdx[0](m_index,parent)+=m_spring;
		dfdx[1](m_index,parent)+=m_spring;

		dfdx[0](parent,m_index)+=m_spring;
		dfdx[1](parent,m_index)+=m_spring;

		if(m_drag != 0.0f)
		{
			dfdv[0](parent,parent)-=m_drag;
			dfdv[1](parent,parent)-=m_drag;

			dfdv[0](m_index,parent)+=m_drag;
			dfdv[1](m_index,parent)+=m_drag;

			dfdv[0](parent,m_index)+=m_drag;
			dfdv[1](parent,m_index)+=m_drag;
		}
	}

	dfdx[0](m_index,m_index)-=m_spring;
	dfdx[1](m_index,m_index)-=m_spring;

	if(m_drag != 0.0f)
	{
		dfdv[0](m_index,m_index)-=m_drag;
		dfdv[1](m_index,m_index)-=m_drag;
	}

	invMass[m_index]=1.0f/m_mass;

	Stick* pStick;
	List<Stick*>::Iterator iterator(m_children);
	while((pStick= *iterator++)!=NULL)
	{
		pStick->populate_static(rspTree,dfdx,dfdv,invMass);
	}
}

Real Tree::Stick::effectOfEffector(sp<Tree>& rspTree,
		const SpatialVector& a_effector,SpatialVector& a_effect)
{
	const BWORD endCollision=rspTree->m_collideEnd;

	const SpatialVector toEffect=a_effector-m_base;
	F32 along=dot(toEffect,m_span)/(m_length*m_length);
	if(endCollision || along>1.0f)
	{
		along=1.0f;
		m_contact=m_base+m_span;
		a_effect=m_base+m_span-a_effector;
	}
	else if(along<0.0f)
	{
		along=0.0f;
		m_contact=m_base;
		a_effect=m_base-a_effector;
	}
	else
	{
		SpatialVector projection=m_base+along*m_span;
		m_contact=projection;
		a_effect=projection-a_effector;
	}
	return along;
}

void Tree::Stick::populate_dynamic(sp<Tree>& rspTree,VectorN* force,
		VectorN* velocity,VectorN* addPosition,VectorN* addVelocity,
		bool rekine)
{
	//* in case parent just moved with rekine
	if(rekine && m_pParent)
	{
		m_base=m_pParent->m_base+m_pParent->m_span;
	}

#if FE_TREE_HIT_DEBUG
	const Vector2 angle=m_rest+m_position;

	feLog("\n");
	for(U32 m=0;m<2;m++)
	{
		feLog("%d %d %.6G+%.6G=%.6G (%.6G)\n",
				m_index,m,m_rest[m],m_position[m],angle[m],angle[m]/degToRad);
	}
#endif

	const F32 radius_exaggerate(1.2);	//* tweak

	BWORD collideBoundSphere=rspTree->m_collisionMethod=="boundingSphere";
	BWORD collidePartSpheres=rspTree->m_collisionMethod=="partSpheres";

	sp<SurfaceI> spCollider=rspTree->collider();
	SpatialTransform colliderTransform=rspTree->colliderTransform();
	SpatialVector colliderCenter;
	if(spCollider.isValid())
	{
		colliderTransform.translation()-=rspTree->m_offset;
		transformVector(colliderTransform,spCollider->center(),colliderCenter);
//		colliderCenter-=rspTree->m_offset;

//		rotateVector(-rspTree->m_rotation,colliderCenter,colliderCenter);

		//* HACK name is not a reliable distinction
		//* WARNING compiling expressions is not threadsafe (pcre or boost)
//		const BWORD colliderCurves=spCollider->name().search("Curves");
		const BWORD colliderCurves=!strncmp(spCollider->name().c_str(),
				"SurfaceI.SurfaceCurves",22);

//		feLog("collider \"%s\" curves %d\n",
//				spCollider->name().c_str(),colliderCurves);

		if(colliderCurves ||
			(collidePartSpheres && spCollider->partitionCount()<2))
		{
			collideBoundSphere=TRUE;
			collidePartSpheres=FALSE;
		}
	}
	else
	{
		set(colliderCenter);
	}

	//* TODO true surface versus just bounding sphere
	SpatialVector effector=colliderCenter;
	F32 radiusMod=spCollider.isValid()?
			radius_exaggerate*spCollider->radius(): 0.0;

	const F32 reactivity=rspTree->m_reactivity;
	const F32 threshold=rspTree->m_threshold;
	const F32 repulsion=rspTree->m_repulsion;
	const F32 repulsionFalloff=rspTree->m_repulsionFalloff;
	const F32 depletion=rspTree->m_depletion;
	const F32 depletionFalloff=rspTree->m_depletionFalloff;

	//* m_contact and m_contact2 are just for debug drawing

	//* point line-segment distance
	SpatialVector effect;
	F32 along=effectOfEffector(rspTree,effector,effect);

	F32 radius_along=m_radius1+(m_radius2-m_radius1)*along;
	F32 radius_sum=radiusMod+threshold+radius_along;
	F32 distance2=magnitudeSquared(effect);

#if FE_TREE_HIT_DEBUG
	feLog("center %s radiusMod %.6G sphere %d\n",
			c_print(colliderCenter),radiusMod,collideBoundSphere);
	feLog("base %s distance %.6G\n",
			c_print(m_base),sqrtf(distance2));
#endif

	BWORD hit=FALSE;
	SpatialVector correctiveEffect(0.0,0.0,0.0);
	SpatialVector externalForce(0.0,0.0,0.0);

	if(m_targeted)
	{
		along=1.0f;
		hit=TRUE;
		correctiveEffect=reactivity*(m_target-m_base);

//		feLog("target %s -> %s\n",c_print(m_base),c_print(m_target));
	}
	else if(radiusMod>0.0f && distance2>0.0f &&
			distance2<(radius_sum*radius_sum))
	{
#if FE_TREE_HIT_DEBUG
		feLog("checking %d along %.6G spherical %d\n",
				m_index,along,collideBoundSphere);
#endif
		if(collideBoundSphere)
		{
			hit=TRUE;
#if FALSE
			const F32 distance=sqrtf(distance2)-radius_along-threshold;
			const Real correction=distance>0.0f? 1.0f-distance/radiusMod: 1.0f;
			correctiveEffect=reactivity*correction*unit(effect);
#else
			const F32 distance=sqrtf(distance2);
			const Real correction=radius_sum-distance;
			correctiveEffect=reactivity*correction*unit(effect);
#endif

#if FE_TREE_HIT_DEBUG
			feLog("HIT %d distance %.2f correction %.6G effect %s\n",
					m_index,distance,correction,
					c_print(correctiveEffect));
#endif
		}
		else if(collidePartSpheres)
		{
			I32 hits=0;
			set(correctiveEffect);

			const I32 partCount=spCollider->partitionCount();
			for(I32 partIndex=0;partIndex<partCount;partIndex++)
			{
				const Vector4 sphere=spCollider->partitionSphere(partIndex);

				SpatialVector partCenter;
				transformVector(colliderTransform,
						SpatialVector(sphere),partCenter);

				const F32 radiusMod=radius_exaggerate*sphere[3];

				SpatialVector part_effect;
				F32 part_along=effectOfEffector(rspTree,partCenter,part_effect);

				const F32 part_radius_along=
						m_radius1+(m_radius2-m_radius1)*part_along;
				const F32 part_radius_sum=
						radiusMod+threshold+part_radius_along;
				const F32 part_distance2=magnitudeSquared(part_effect);

				if(radiusMod>0.0f && part_distance2>0.0f &&
						part_distance2<(part_radius_sum*part_radius_sum))
				{
					const F32 distance=sqrtf(part_distance2);
					const Real correction=part_radius_sum-distance;

					correctiveEffect+=correction*unit(part_effect);

					hits++;
				}
			}

			if(hits)
			{
				hit=TRUE;
				correctiveEffect*=Real(1)/Real(hits);

				//* TODO something better
				const SpatialVector approxCenter=m_base-correctiveEffect;
				radiusMod=radius_exaggerate*2.0*magnitude(correctiveEffect);

				//* NOTE adjust original values with combined collisions
				effector=approxCenter;
				along=effectOfEffector(rspTree,effector,effect);
				radius_along=m_radius1+(m_radius2-m_radius1)*along;
				radius_sum=radiusMod+threshold+radius_along;
				distance2=magnitudeSquared(effect);

				correctiveEffect*=reactivity;
			}
		}
		else
		{
			SpatialTransform invCollider;
			invert(invCollider,colliderTransform);

			const BWORD impactNormal=
					(rspTree->m_collisionMethod=="impactNormal");

			sp<SurfaceI::ImpactI> spImpact;
			if(impactNormal)
			{
				const SpatialVector colliderContact=
						transformVector(invCollider,m_contact);
				spImpact=spCollider->nearestPoint(colliderContact);
			}
			else
			{
				const SpatialVector rayDir=unitSafe(effector-m_contact);
				const SpatialVector rayOrigin=effector-radiusMod*rayDir;
				const SpatialVector colliderRayOrigin=
						transformVector(invCollider,rayOrigin);
				const SpatialVector colliderRayDir=
						rotateVector(invCollider,rayDir);
				spImpact=spCollider->rayImpact(colliderRayOrigin,
						colliderRayDir,radiusMod);
			}

			if(spImpact.isValid())
			{
				const SpatialVector intersection=
						transformVector(colliderTransform,
						spImpact->intersection());
				const SpatialVector norm=impactNormal?
						rotateVector(colliderTransform,spImpact->normal()):
						unitSafe(intersection-effector);

#if TRUE
				const SpatialVector push=intersection-m_contact;
				const Real outward=dot(push,norm)+
						radius_along+threshold;

#if FE_TREE_HIT_DEBUG
				feLog("HIT %d along %.2f outward %.6G norm %s\n",
						m_index,along,outward,c_print(norm));
#endif

				if(outward>0.0)
				{
					correctiveEffect=reactivity*outward*norm;
					hit=TRUE;
				}
#else
				const F32 radiusScale=magnitude(intersection-effector)*
						radius_exaggerate/radiusMod;
				const F32 shortRadius=radiusScale*radiusMod;
				const F32 distance=sqrtf(distance2)-radius_along;
				const Real correction=
						distance>0.0f? 1.0f-distance/shortRadius: 1.0f;

#if FE_TREE_HIT_DEBUG
				feLog("radiusScale %.6G correction %.6G\n",
						radiusScale,correction);
#endif
				if(correction>0.0)
				{
					correctiveEffect=correction*unit(effect);

					const Real hitDot=dot(norm,unitSafe(correctiveEffect));

					if(hitDot>0.0)
					{
						hit=TRUE;
					}
				}
#endif
			}
		}
	}

	if(repulsion>Real(0) && spCollider.isValid())
	{
		SpatialVector acceleration(0.0,0.0,0.0);

		if(collidePartSpheres)
		{
			Real maxAccel=Real(0);
			const I32 partCount=spCollider->partitionCount();
			for(I32 partIndex=0;partIndex<partCount;partIndex++)
			{
				const Vector4 sphere=spCollider->partitionSphere(partIndex);

				SpatialVector partCenter;
				transformVector(colliderTransform,
						SpatialVector(sphere),partCenter);

				const F32 radiusMod=radius_exaggerate*sphere[3];

				SpatialVector part_effect;
				F32 part_along=effectOfEffector(rspTree,partCenter,part_effect);

				const F32 part_radius_along=
						m_radius1+(m_radius2-m_radius1)*part_along;
				const F32 part_radius_sum=
						radiusMod+threshold+part_radius_along;
				const F32 part_radius_outer=part_radius_sum+repulsionFalloff;
				const F32 part_distance2=magnitudeSquared(part_effect);

				if(radiusMod>0.0f && part_distance2>0.0f &&
						part_distance2<(part_radius_outer*part_radius_outer))
				{
					const F32 distance=sqrtf(part_distance2);

					const Real proximity=
							fe::minimum(Real(1),fe::maximum(Real(0),
							Real(1)-
							(distance-part_radius_sum)/repulsionFalloff));

					const Real oneAccel=repulsion*proximity;
					acceleration+=oneAccel*unit(part_effect);

					if(maxAccel<oneAccel)
					{
						maxAccel=oneAccel;
					}
				}
#if TRUE
				const SpatialVector unitPush=unitSafe(acceleration);
				acceleration=maxAccel*unitPush;
#endif
			}
		}
		else
		{
			const F32 radius_outer=radius_sum+repulsionFalloff;

			if(radiusMod>0.0f && distance2>0.0f &&
					distance2<(radius_outer*radius_outer))
			{
				const F32 distance=sqrtf(distance2);

				const Real proximity=
						fe::minimum(Real(1),fe::maximum(Real(0),
						Real(1)-
						(distance-radius_sum)/repulsionFalloff));

				acceleration=repulsion*proximity*unit(effect);
			}
		}

		externalForce+=acceleration*m_mass;
	}

	Vector2 parent_intensity;
	set(parent_intensity);
	if(m_pParent)
	{
		parent_intensity=m_pParent->m_intensity;
	}

	//* TODO tweak
	const BWORD plasticReversal=FALSE;
	const Real maxIntensity=1.4;
	const Real popIntensity=0.01;

	const Vector2 lastIntensity=m_intensity;
	set(m_intensity);

	const Real plasticity=rspTree->m_plasticity;
	const Real minIntensity=plasticity*magnitude(lastIntensity);

//	feLog("%d %s last %s corr %s\n",m_index,
//			hit? "**** HIT ": "---- miss",
//			c_print(lastIntensity),c_print(correctiveEffect));

	if(hit || minIntensity>popIntensity)
	{
//~		Real magIntensity(0);

		if(hit)
		{
			m_contact2=effector;

			Real invMag2[2];
			invMag2[0]=Real(1)/magnitudeSquared(m_dir[0]);
			invMag2[1]=Real(1)/magnitudeSquared(m_dir[1]);

			Vector2 low;
			Vector2 high;
			for(U32 m=0;m<2;m++)
			{
				low[m]=fe::minimum(Real(0),parent_intensity[m])-maxIntensity;
				high[m]=fe::maximum(Real(0),parent_intensity[m])+maxIntensity;
			}

			//* TODO tweak
			const U32 passes=4;

			for(U32 pass=0;pass<passes;pass++)
			{
				for(U32 m=0;m<2;m++)
				{
					SpatialVector residual=correctiveEffect-
							m_intensity[!m]*m_dir[!m];
					m_intensity[m]=
							fe::maximum(low[m],
							fe::minimum(high[m],
							dot(residual,m_dir[m])*invMag2[m]));
//~					m_intensity[m]=dot(residual,m_dir[m])*invMag2[m];
				}

//~				if(pass==passes-1)
//~				{
//~					m_intensity[0]=fe::maximum(-maxIntensity,
//~							fe::minimum(maxIntensity,m_intensity[0]));
//~					m_intensity[1]=fe::maximum(-maxIntensity,
//~							fe::minimum(maxIntensity,m_intensity[1]));
//~				}
			}

//~			magIntensity=magnitude(m_intensity);

#if FE_TREE_HIT_DEBUG
			feLog("%.6G*(%s) + %.6G*(%s)\n  = %s\n",
					m_intensity[0],c_print(m_dir[0]),
					m_intensity[1],c_print(m_dir[1]),
					c_print(m_intensity[0]*m_dir[0]+m_intensity[1]*m_dir[1]));
#endif
		}

		if(plasticReversal)
		{
			if(plasticity>Real(0))
			{
				for(U32 m=0;m<2;m++)
				{
					const Real limit=plasticity*lastIntensity[m];

//					if((m_intensity[m]>Real(0)) != (limit>Real(0)))
//					{
//						feLog(">>>> FLIP\n");
//					}

					if(m_intensity[m]==Real(0) ||
							(m_intensity[m]<Real(0) && m_intensity[m]>limit) ||
							(m_intensity[m]>Real(0) && m_intensity[m]<limit))
					{
//						feLog(">> intensity[%d] %.6G -> %.6G\n",
//								m,m_intensity[m],limit);

						m_intensity[m]=limit;
					}
				}
			}
		}
		else
		{
//~			if(magIntensity<1e-6)
//~			{
//~				m_intensity=plasticity*lastIntensity;
//~			}
//~			else if(magIntensity<minIntensity)
			{
//~				m_intensity*=minIntensity/magIntensity;

//~				const SpatialVector oldUnit=unitSafe(lastIntensity);
//~				const SpatialVector newUnit=unitSafe(m_intensity);
//~				const SpatialVector between=unitSafe(0.9*oldUnit+0.1*newUnit);
//~				m_intensity=minIntensity*between;

				for(U32 m=0;m<2;m++)
				{
					const Real limit=plasticity*lastIntensity[m];

					if(limit<Real(0) && m_intensity[m]>limit)
					{
//						feLog(">> intensity[%d] %.6G -> %.6G\n",
//								m,m_intensity[m],limit);

						m_intensity[m]=limit;
					}
					if(limit>Real(0) && m_intensity[m]<limit)
					{
//						feLog(">> intensity[%d] %.6G -> %.6G\n",
//								m,m_intensity[m],limit);
						m_intensity[m]=limit;
					}
				}
			}
		}

		for(U32 m=0;m<2;m++)
		{
#if FALSE
			//* raw penalty force
			const F32 push=0.0f;
			force[m][m_index]+=push*m_intensity[m];
#endif

			//* for Baraff's Position Alteration
			//* TODO non-arbitrary scale
#if FALSE
			const F32 restitution=0.0f;
			addVelocity[m][m_index]= -(1.0+restitution)*m_velocity[m];
#else

			addPosition[m][m_index]=along*m_intensity[m];
#endif

#if TRUE
			// deplete kinetic energy
			m_velocity[m]*=Real(0);
//			m_velocity[m]*=Real(1)-depletion;
			addVelocity[m][m_index]=0.0f;
#else
			const Vector2 response=unit(m_intensity);

			if((m_velocity[m]>0.0f) != (response[m]>0.0f))
			{
				addVelocity[m][m_index]= -m_velocity[m]*fabs(response[m]);
			}
			else
			{
				addVelocity[m][m_index]=0.0f;
			}
#endif

#if FALSE
			m_lastAddPosition[m]*=0.9f;
			if(m_lastAddPosition[m]<0.0f &&
					addPosition[m][m_index]>m_lastAddPosition[m])
			{
				addPosition[m][m_index]=m_lastAddPosition[m];
			}
			if(m_lastAddPosition[m]>0.0f &&
					addPosition[m][m_index]<m_lastAddPosition[m])
			{
				addPosition[m][m_index]=m_lastAddPosition[m];
			}
			m_lastAddPosition[m]=addPosition[m][m_index];
#endif
		}
		rekine=TRUE;
	}
	else
	{
//		const F32 deplete_radius=FE_TREE_DAMP_REGION*radius_sum;
		const F32 deplete_radius=radius_sum+depletionFalloff;

		if(distance2>0.0f && distance2<(deplete_radius*deplete_radius))
		{
			// deplete kinetic energy
			const Real distance=sqrtf(distance2)-radius_along;
			const Real proximity=Real(1)-(distance-radiusMod)/depletionFalloff;

			m_velocity*=fe::maximum(Real(0),Real(1)-depletion*proximity);
		}

		set(m_contact);
		set(m_contact2);

#if FALSE
		m_lastAddPosition*=0.9f;
		for(U32 m=0;m<2;m++)
		{
			addPosition[m][m_index]=m_lastAddPosition[m];
		}
#endif
	}

	Vector2 modPosition=m_position;
#if TRUE
	//* anticipate addPosition in springs
	modPosition[0] += addPosition[0][m_index];
	modPosition[1] += addPosition[1][m_index];

	//* NOTE parent position already rekined by its addPosition
#endif

	Vector2 parent_position;
	Vector2 parent_velocity;
	set(parent_position);
	set(parent_velocity);
	if(m_pParent)
	{
		const U32 parent=m_pParent->m_index;

		parent_position=m_pParent->m_rest+m_pParent->m_position;
		parent_velocity=m_pParent->m_velocity;

		force[0][parent]+=m_spring*(modPosition[0]-parent_position[0]);
		force[1][parent]+=m_spring*(modPosition[1]-parent_position[1]);

//		feLog("parent force %.6G %.6G\n",force[0][parent],force[1][parent]);

		if(m_drag != 0.0f)
		{
			force[0][parent]+=m_drag*(m_velocity[0]-parent_velocity[0]);
			force[1][parent]+=m_drag*(m_velocity[1]-parent_velocity[1]);
		}
	}

	Real windScale=1.0;

	const Real hampering=rspTree->m_windHampering;
	if(hampering>Real(0) && spCollider.isValid())
	{
		const Real windFalloff=rspTree->m_windFalloff;

		//* TODO hamper for plastic changes

		const Real separation=sqrt(distance2)-radius_sum;
		const Real t=fe::maximum(Real(0),fe::minimum(Real(3),
				Real(3*separation/windFalloff)));

		//* soft step function from <0,0> to <3,1>
		const F64 knots[4]={0.0,1.0,2.0,3.0};
		const F64 controls[4]={0.0,0.0,1.0,1.0};
		const F64 step=Spline<F64,F64>::Basis2D(t,4,knots,controls);

		const Real oldFreedom=m_freedom;
		const Real maxFreedom=1.0-plasticity*(1.0-oldFreedom);
		m_freedom=fe::minimum(maxFreedom,Real(step));

		windScale=1.0-(1.0-hampering*m_freedom);
	}
	else
	{
		m_freedom=1.0;
	}

	const SpatialVector windVector=
			rspTree->m_uniformVelocity+windScale*m_windVelocity;

	SpatialVector windForce=1e3f*windVector;	//* tweak

	const F32 wind2=magnitudeSquared(windVector);
	if(wind2>Real(0))
	{
		//* NOTE facing area
		const Real radiusMid=0.5*(m_radius1+m_radius2);

		windForce*=radiusMid*m_length*
				(1-fabs(dot(m_span/m_length,windVector/sqrtf(wind2))));
	}

	externalForce+=windForce;

	const SpatialVector gravity=rspTree->m_gravity;
	externalForce+=gravity*m_mass;

//	feLog("hampering %-6.4f t %-6.4f step %-6.4f windScale %-6.4f\n",
//			hampering,t,step,windScale);

//	feLog(" correction %-6.4f externalForce %-6.4f intensity %s\n",
//			t,windScale,
//			magnitude(correctiveEffect),
//			magnitude(externalForce),
//			c_print(m_intensity));

//	rotateVector(-rspTree->m_rotation,externalForce,externalForce);

	//* NOTE gravity from local node only
	//* TODO accumulate axial child gravity during kinematics
	Vector2 addForce;
#if FALSE
//	const SpatialVector downforce(0.0f,-gm*(1.0f-fabs(unit(m_span)[1])),0.0f);
	const SpatialVector downforce(0.0f,-gm,0.0f);
	SpatialVector unitdir[2];
	unitdir[0]=unit(m_dir[0]);
	unitdir[1]=unit(m_dir[1]);

	set(addForce);
	const U32 passes=4;
	for(U32 pass=0;pass<passes;pass++)
	{
		for(U32 m=0;m<2;m++)
		{
			SpatialVector residual=downforce-addForce[!m]*unitdir[!m];
			addForce[m]=dot(residual,unitdir[m]);
		}
	}
#else
	SpatialVector dir=unit(m_dir[0]);
	addForce[0]=dot(dir,externalForce);
	dir=unit(m_dir[1]);
	addForce[1]=dot(dir,externalForce);

//	feLog("mass %.6G externalForce %s addForce %s\n",
//			m_mass,c_print(externalForce),c_print(addForce));

	//* shouldn't this max when across span, not with it?
//	const F32 mag2=magnitudeSquared(addForce);
//	const SpatialVector unitSpan=m_span/m_length;
//	const F32 max=fabs(dot(externalForce,unitSpan));
//	if(mag2>max)
//	{
//		addForce*=max/sqrtf(mag2);
//	}
#endif

	force[0][m_index]+=addForce[0];
	force[1][m_index]+=addForce[1];

#if FALSE
	// NOTE currently messes up ball
	m_intensity[0]= 0.001f*addForce[0];
	m_intensity[1]= 0.001f*addForce[1];
#endif

	force[0][m_index]-=m_spring*(modPosition[0]-parent_position[0]);
	force[1][m_index]-=m_spring*(modPosition[1]-parent_position[1]);

//	feLog("spring %.6G pos %s par %s force %.6G %.6G\n",
//			m_spring,c_print(m_position),c_print(parent_position),
//			force[0][m_index],force[1][m_index]);

	if(m_drag != 0.0f)
	{
		force[0][m_index]-=m_drag*(m_velocity[0]-parent_velocity[0]);
		force[1][m_index]-=m_drag*(m_velocity[1]-parent_velocity[1]);
	}

	velocity[0][m_index]=m_velocity[0];
	velocity[1][m_index]=m_velocity[1];

#if FE_TREE_HIT_DEBUG
	feLog("addPosition %.6G %.6G addVelocity %.6G %.6G\n",
			addPosition[0][m_index],addPosition[1][m_index],
			addVelocity[0][m_index],addVelocity[1][m_index]);
#endif

	if(rekine)
	{
		forward_kine(0,rspTree,0.0f,NULL,addPosition,addVelocity,
				rspTree->m_effectorForce,FALSE);
	}

	Stick* pStick;
	List<Stick*>::Iterator iterator(m_children);
	while((pStick= *iterator++)!=NULL)
	{
		pStick->populate_dynamic(rspTree,force,velocity,
				addPosition,addVelocity,rekine);
	}
}

void Tree::Stick::forward_kine(U32 thread,sp<Tree>& rspTree,
		const F32 deltaT,const VectorN* deltaV,
		const VectorN* addPosition,const VectorN* addVelocity,
		SpatialVector& rEffectorForce,BWORD rebundle)
{
	const BWORD transform_leaves=(deltaV!=NULL);
	Vector2 parentAngle;
	SpatialQuaternion parentRotation;
	if(m_pParent)
	{
		m_base=m_pParent->m_base+m_pParent->m_span;
		parentAngle=m_pParent->m_rest+m_pParent->m_position;
		parentRotation=m_pParent->m_rotation;
	}
	else
	{
		m_base=rspTree->m_location;
		parentRotation=rspTree->m_rotation;
		set(parentAngle);
	}

#if FE_TREE_TRACE
	feLog("\n%d lev %d p %s v %s ap %.6G %.6G av %.6G %.6G\n",
			m_index,m_level,c_print(m_position),c_print(m_velocity),
			addPosition[0][m_index],addPosition[1][m_index],
			addVelocity[0][m_index],addVelocity[1][m_index]);
	feLog(" base %s span %s\n",c_print(m_base),c_print(m_span));
	feLog(" parentAngle %s\n",c_print(parentAngle));
#endif

	if(deltaV)
	{
		m_velocity[0]+=deltaV[0][m_index];
		m_velocity[1]+=deltaV[1][m_index];

#if FE_TREE_TRACE
		feLog(" dv %.6G %.6G\n",
				deltaV[0][m_index],deltaV[1][m_index]);
#endif
	}

	m_velocity[0]+=addVelocity[0][m_index];
	m_velocity[1]+=addVelocity[1][m_index];

	if(FE_INVALID_SCALAR(m_velocity[0]) || FE_INVALID_SCALAR(m_velocity[1]))
	{
		feLog("deltaV %.6G %.6G\n",
				deltaV[0][m_index],deltaV[1][m_index]);
		feLog("addVelocity %.6G %.6G\n",
				addVelocity[0][m_index],addVelocity[1][m_index]);
		feLog("m_velocity %s\n",c_print(m_velocity));

		feX("Tree::Stick::forward_kine","invalid m_velocity");
	}

	m_position[0]+=deltaT*m_velocity[0]+addPosition[0][m_index];
	m_position[1]+=deltaT*m_velocity[1]+addPosition[1][m_index];

	Vector2 angle=m_rest+m_position-parentAngle;

#if FE_TREE_TRACE
		feLog(" rest %s position %s angle %s\n",
				c_print(m_rest),c_print(m_position),c_print(angle));
#endif

#if FALSE
	F32 mag_angle=magnitude(angle);
	if(mag_angle>pi)
	{
		const Vector2 unitangle=unit(angle);
		mag_angle=2*pi-mag_angle;
		angle= -mag_angle*unitangle;
		m_position=angle-m_rest;
	}
#endif

//	SpatialVector yVector(0.0f,m_length);

	SpatialVector span;
	for(U32 pass=0;pass<3;pass++)
	{
		const F32 delta=0.01f;
		const F32 invDelta=(1.0f/delta);

		const F32 angle0=angle[0]+(pass==1? delta: 0.0f);
		const F32 angle1=angle[1]+(pass==2? delta: 0.0f);

#if FALSE
		//* reverse projection - intersection of planes
		const SpatialVector normalx(cosf(angle0),-sinf(angle0),0.0f);
		const SpatialVector normalz(0.0f,-sinf(angle1),cosf(angle1));
		const SpatialVector direction=unit(cross(normalz,normalx));
		SpatialQuaternion local_rotation;
		// TODO special set
		set(local_rotation,Vector3f(0.0f,1.0f,0.0f),direction);
//		feLog("\n%x pass %d angle %.6G %.6G\n",this,pass,angle0,angle1);
//		feLog("normalx %s\n",c_print(normalx));
//		feLog("normalz %s\n",c_print(normalz));
//		feLog("direction %s\n",c_print(direction));
//		feLog("local_rotation %s\n",c_print(local_rotation));
		const SpatialQuaternion rotation=parentRotation*local_rotation;
//		feLog("rotation %s\n",c_print(rotation));
#endif

#if TRUE
		//* hypotenuse angle with proportional axis
		const F32 phi=sqrtf(angle0*angle0+angle1*angle1);
		SpatialVector axis=SpatialVector(-angle1,angle0,0.0f);
		if(phi>1e-6f)
		{
			axis*=1.0f/phi;
		}
//		F32 phi2=0.5f*(fabs(angle0)+fabs(angle1));
		SpatialQuaternion rotation=parentRotation*SpatialQuaternion(phi,axis);

//		feLog("%d %.6G %.6G %.6G  %s  %s  %s\n",
//				pass,angle0,angle1,phi,c_print(axis),
//				c_print(rotation),c_print(span));
#endif

#if FALSE
		//* universal joint (like a drive shaft)
		const SpatialQuaternion rotationx=SpatialQuaternion(angle0,e_xAxis);
		const SpatialQuaternion rotationz=SpatialQuaternion(angle1,e_zAxis);
		const SpatialQuaternion rotation=parentRotation*rotationx*rotationz;
#endif

		if(!pass && rspTree->m_useCorrection==1)
		{
			//* without correction
			rotateZVector(rotation,m_length,span);

			const SpatialVector zAxis(0.0,0.0,1.0);
			SpatialQuaternion expected;
			set(expected,zAxis,unitSafe(m_span));

			m_correction=inverse(rotation)*expected;
			normalize(m_correction);

//			feLog("%s\nspan %s vs %s\n"
//					"parent %s local %s rotation %s\n"
//					"expected %s correction %s makes %s\n",
//					(m_correction[3]>1.001)? "PROBLEM": "",
//					c_print(span),c_print(m_span),
//					c_print(parentRotation),
//					c_print(SpatialQuaternion(phi,axis)),c_print(rotation),
//					c_print(expected),c_print(m_correction),
//					c_print(rotation*m_correction));
		}

		if(rspTree->m_useCorrection)
		{
			rotation=rotation*m_correction;
		}

		//* potentially with correction
		rotateZVector(rotation,m_length,span);

		if(!pass)
		{
			m_span=span;
			m_rotation=rotation;
			m_transform=m_rotation;
			setTranslation(m_transform,m_base);
		}
		else
		{
			m_dir[pass-1]=(span-m_span)*invDelta;
		}
	}

	// push back on effector
	const F32 tweak=8.0f;
	rEffectorForce -= tweak*(m_dir[0]*m_intensity[0]+
			m_dir[1]*m_intensity[1]);

	if(transform_leaves)
	{
		const U32 leafIndexCount=m_leafIndexArray.size();
		for(U32 leafLookup=0;leafLookup<leafIndexCount;leafLookup++)
		{
			const U32 leafIndex=m_leafIndexArray[leafLookup];
			if(leafIndex>=rspTree->m_leafArray.size())
			{
				feLog("Tree::Stick::forward_kine leafIndex %d/%d\n",
						leafIndex,rspTree->m_leafArray.size());
				continue;
			}
			FEASSERT(leafIndex<rspTree->m_leafArray.size());
			const Tree::Leaf& rLeaf=*rspTree->m_leafArray[leafIndex];
			transformVector(m_transform,rLeaf.center(),
					rspTree->m_pLeafPoints[leafIndex]);
			rotateVector(m_transform,rLeaf.normal(),
					rspTree->m_pLeafNormals[leafIndex]);
		}
	}

	// NOTE skipped during populate_dynamic
	if(rebundle && m_radius1>FE_TREE_VISIBLE_MIN_RADIUS)
	{
		U32& index=rspTree->m_bundleSize[thread];
		FEASSERT(index<rspTree->numSticks());

		//* TODO consider, maybe cylinders should point in Y

#if FALSE
		//* rotate object's Z axis to Y axis
		const SpatialTransform& transform=m_transform;
		SpatialTransform& cylTransform=
				rspTree->m_stickTransformBundles[thread][index];
		cylTransform=transform;
//		cylTransform.up()= -transform.direction();
//		cylTransform.direction()= transform.up();
#else
		rspTree->m_stickTransformBundles[thread][index]=m_transform;
#endif

		set(rspTree->m_stickScaleBundles[thread][index],
				m_radius2,m_radius2,m_length);
		rspTree->m_stickBaseScaleBundles[thread][index]=m_radius1/m_radius2;
		rspTree->m_stickSliceBundles[thread][index]=m_resolution;

		index++;
	}
}

void Tree::Stick::forward_kine_recursive(U32 thread,sp<Tree>& rspTree,
		const F32 deltaT,const VectorN* deltaV,
		const VectorN* addPosition,const VectorN* addVelocity,
		SpatialVector& rEffectorForce,BWORD only_zero)
{
	forward_kine(thread,rspTree,deltaT,deltaV,addPosition,addVelocity,
			rEffectorForce,TRUE);

	Stick* pStick;
	List<Stick*>::Iterator iterator(m_children);
	while((pStick= *iterator++)!=NULL)
	{
		if(!only_zero || pStick->m_level==0)
		{
			pStick->forward_kine_recursive(thread,rspTree,deltaT,deltaV,
					addPosition,addVelocity,rEffectorForce,
					only_zero);
		}
	}
}

void Tree::forward_kine_thread(U32 thread,I32 index)
{
#if FE_TREE_MT_DEBUG
	feLog("[33mTree::forward_kine_thread %d start %p %p[0m\n",index,
			m_kineArray[index],m_spProfileKineLimbArray[index].raw());
#endif
	m_spProfileKineLimbArray[index]->start();
	sp<Tree> spTree(this);
	m_kineArray[index]->forward_kine_recursive(thread,spTree,
			m_lastDeltaT,m_deltaV,
			m_addPosition,m_addVelocity,m_effectorForceArray[thread],
			FALSE);
	m_spProfileKineLimbArray[index]->finish();
}

void Tree::runSolvers(void)
{
#if FE_TREE_MT_DEBUG
	feLog("[33mTree::runSolvers start[0m\n");
#endif

#if FE_TREE_SOLVE_OFF_MAIN
	m_spGang->post(FE_TREE_MT_ACTION_SOLVE|0,FE_TREE_MT_ACTION_SOLVE|1);
#else
	m_spGang->post(FE_TREE_MT_ACTION_SOLVE|1);
	solve(0);
#endif

#if FE_TREE_MT_DEBUG
	feLog("[33mTree::runSolvers posted[0m\n");
#endif

	U32 jobs=FE_TREE_SOLVE_OFF_MAIN? 2: 1;
	while(jobs)
	{
		I32 job=0;

#if FE_TREE_PROFILE
		m_spProfileSolveWait->start();
#endif

		m_spGang->waitForDelivery(job);

#if FE_TREE_PROFILE
		m_spProfileSolveWait->finish();
#endif

		jobs--;

#if FE_TREE_MT_DEBUG
		feLog("[33mTree::runSolvers accepted %10p[0m\n",job);
#endif
	}

#if FE_TREE_MT_DEBUG
	feLog("[33mTree::runSolvers done[0m\n");
#endif
}

void Tree::runKine(void)
{
#if FE_TREE_MT_DEBUG
	feLog("[33mTree::runKine start[0m\n");
#endif

	sp<Tree> spTree(this);
	m_root.forward_kine_recursive(0,spTree,m_lastDeltaT,m_deltaV,
			m_addPosition,m_addVelocity,m_effectorForceArray[0],TRUE);

	U32 jobs=m_kineArray.size();
	m_spGang->post(FE_TREE_MT_ACTION_KINE|0,FE_TREE_MT_ACTION_KINE|(jobs-1));

	I32 job=0;
	while(m_spGang->take(job))
	{
#if FE_TREE_MT_DEBUG
		feLog("[33mTree::runKine take     %10p[0m\n",job);
#endif
		forward_kine_thread(0,job&FE_TREE_MT_INDEX_MASK);
#if FE_TREE_MT_DEBUG
		feLog("[33mTree::runKine deliver  %10p[0m\n",job);
#endif
		m_spGang->deliver(job);
	}
#if FE_TREE_MT_DEBUG
		feLog("[33mTree::runKine all taken[0m\n");
#endif
	while(jobs)
	{
#if FE_TREE_PROFILE
		m_spProfileKineWait->start();
#endif

		m_spGang->waitForDelivery(job);

#if FE_TREE_PROFILE
		m_spProfileKineWait->finish();
#endif

		jobs--;

#if FE_TREE_MT_DEBUG
		feLog("[33mTree::runKine accepted %10p[0m\n",job);
#endif
	}

#if FE_TREE_MT_DEBUG
	feLog("[33mTree::runKine done[0m\n");
#endif
}

void Tree::SolverWorker::operate(void)
{
	const U32 thread=m_id+1;

#if FE_TREE_MT_DEBUG
	feLog("[32mTree::SolverWorker::operate"
			" %p Thread %d starting[0m\n",this,thread);

	BWORD wait_message=TRUE;
#endif

#if FE_TREE_PROFILE
	BWORD started=FALSE;
#endif

	I32 job=0;
	I32 index=0;
	while(TRUE)
	{
#if FE_TREE_PROFILE
		if(started)
		{
			try
			{
				m_hpTree->m_spProfileThreadWait->start();
			}
			catch(Exception &e)
			{
				if(e.getResult()!=e_invalidHandle) { throw e; }
				feLog("[33mTree::SolverWorker::operate"
						" m_hpTree invalid[0m\n\n");
			}
		}
#endif

		m_hpJobQueue->waitForJob(job);
		if(job==FE_TREE_MT_ACTION_QUIT)
		{
#if FE_TREE_MT_DEBUG
			feLog("[32mTree::SolverWorker::operate"
					" %p Thread %d Job %10p break[0m\n",this,thread,job);
#endif
			m_hpJobQueue->deliver(job);
			break;
		}

#if FE_TREE_PROFILE
		if(started)
		{
			m_hpTree->m_spProfileThreadWait->finish();
		}
#endif

#if FE_TREE_MT_DEBUG
		feLog("[32mTree::SolverWorker::operate"
				" %p Thread %d Job %10p[0m\n",
				this,thread,job);
#endif

#if FE_TREE_PROFILE
		started=TRUE;
#endif

		index=job&FE_TREE_MT_INDEX_MASK;

		if(job&FE_TREE_MT_ACTION_SOLVE)
		{
			m_hpTree->solve(index);
		}
		else if(job&FE_TREE_MT_ACTION_KINE)
		{
			m_hpTree->forward_kine_thread(thread,index);
		}

#if FE_TREE_MT_DEBUG
		feLog("[32mTree::SolverWorker::operate"
				" %p Thread %d Job %10p finished[0m\n",
				this,thread,job);
		wait_message=TRUE;
#endif

		m_hpJobQueue->deliver(job);
	}
#if FE_TREE_MT_DEBUG
	feLog("[32mTree::SolverWorker::operate"
			" %p Thread %d leaving[0m\n",this,thread);
#endif
}

} /* namespace ext */
} /* namespace fe */
