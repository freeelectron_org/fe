/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#undef MODULE_plugin
#include "fe/plugin.h"

using namespace fe;

int main(int argc, char *argv[])
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		feRegisterSegvHandler();

		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		for(int i = 1; i < argc; i++)
		{
			feLog("Manage \"%s\"\n",argv[i]);
			Result result=spRegistry->manage(argv[i], true);
			UNIT_TEST(successful(result));
		}

		std::map<String,Registry::FactoryLocation*> &factories =
				spRegistry->factoryMap();

		std::map<String,Registry::FactoryLocation*>::const_iterator it =
				factories.begin();
		while(it!=factories.end())
		{
			sp<Component> spComponent =
					it->second->m_spLibrary->create(it->second->m_factoryIndex);

			feLog("%-32s %-17s ",
					it->first.c_str(),
					it->second->m_spLibrary->name().c_str());

			if(!spComponent.isValid())
			{
				feLog("CANNOT CREATE\n");
			}
			else
			{
				feLog("(%s)\n",
						spComponent->name().c_str());
				UNIT_TEST(spComponent->name() != "");
			}

			it++;
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_RETURN();
}
