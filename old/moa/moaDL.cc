/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

#include "platform/dlCore.cc"

using namespace fe;
using namespace ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("feDataDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	sp<BaseType> spT;
	spT = spMaster->typeMaster()->assertType<t_accum_v3>("accum3");
	spT->setInfo(new InfoVectorAccum());

	spT = spMaster->typeMaster()->assertType<t_atomic_real>("atomic");
	spT->setInfo(new InfoAtomicReal());

	spT = spMaster->typeMaster()->assertType<t_note_perform_tmp>("perform");
	spT->setInfo(new InfoNotePerform());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<GetRealFromRecord>(		"FunctionI.GetRealFromRecord.fe");
	pLibrary->add<CanonicalRealGet>(		"FunctionI.CanonicalRealGet.fe");
	pLibrary->add<CanonicalRealSet>(		"FunctionI.CanonicalRealSet.fe");
	pLibrary->add<Concatenate>(				"FunctionI.Concatenate.fe");
	pLibrary->add<GetGlobalVar>(			"FunctionI.GetGlobal.fe");
	pLibrary->add<Orchestrator>(			"OrchestratorI.Orchestrator.fe");

	pLibrary->add<EvaluatorStd>(			"EvaluatorI.EvaluatorStd.fe");

	pLibrary->add<SplatLineSystem>(			"SystemI.SplatLineSystem.fe");
	pLibrary->add<NyquistLineSystem>(		"SystemI.NyquistLineSystem.fe");
	pLibrary->add<EvaluateSystem>(			"SystemI.EvaluateSystem.fe");

	pLibrary->add<MultiSweepSystem>(		"SystemI.MultiSweep.fe");
	pLibrary->add<FrequencyResponseSystem>(	"SystemI.FrequencyResponse.fe");
	pLibrary->add<SimulatorLoopSystem>(		"SystemI.SimulatorLoop.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
