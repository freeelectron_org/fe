/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <usd/usd.pmh>

#define FE_SAU_DEBUG		FALSE
#define FE_SAU_LOAD_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAccessibleUsd::reset(void)
{
	SurfaceAccessibleBase::reset();

	m_usdStage=NULL;
	m_filename="";
	m_isolateNode="";

	m_nodeArray.clear();
	m_nodeMap.clear();
}

sp<SurfaceAccessorI> SurfaceAccessibleUsd::accessor(
	String a_node,Element a_element,String a_name,
	Creation a_create,Writable a_writable)
{
#if FE_SAU_DEBUG
	if(a_name!="instance")
	{
		feLog("SurfaceAccessibleUsd::accessor"
				" node \"%s\" elem '%s' attr \"%s\" create %d\n",
				a_node.c_str(),
				SurfaceAccessibleBase::elementLayout(a_element).c_str(),
				a_name.c_str(),
				a_create==SurfaceAccessibleI::e_createMissing);
	}
#endif

	if(a_node.empty())
	{
		a_node=m_isolateNode;
	}

	if(a_node.empty())
	{
		sp<SurfaceAccessorUsdGraph> spAccessor(new SurfaceAccessorUsdGraph);

		if(spAccessor.isValid())
		{
			spAccessor->setWritable(a_writable);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			if(spAccessor->bind(a_element,a_name) ||
					a_create==SurfaceAccessibleI::e_createMissing)
			{
				return spAccessor;
			}
		}
	}
	else
	{
		sp<SurfaceAccessorUsd> spAccessor(new SurfaceAccessorUsd);

		if(spAccessor.isValid())
		{
			spAccessor->setYToZ(m_ytoz);

			UsdPrim usdPrim;

			sp<PrimNode> spPrimNode=m_nodeMap[a_node];
			if(spPrimNode.isValid())
			{
				usdPrim=spPrimNode->usdPrim();
			}

			if(!usdPrim)
			{
				return sp<SurfaceAccessorI>(NULL);
			}

			spAccessor->setWritable(a_writable);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			spAccessor->setPrimNode(spPrimNode);
			spAccessor->setFrame(m_frame);
			if(spAccessor->bind(a_element,a_name))
			{
				return spAccessor;
			}

			if(a_create==SurfaceAccessibleI::e_createMissing)
			{
				const String primType=usdPrim.GetTypeName().GetText();
				if(primType!="Skeleton")
				{
					return spAccessor;
				}
			}
		}
	}

#if FE_SAU_DEBUG
	if(a_name!="instance")
	{
		feLog("SurfaceAccessibleUsd::accessor fail\n");
	}
#endif

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleUsd::accessor(
	String a_node,Element a_element,Attribute a_attribute,
	Creation a_create,Writable a_writable)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessibleUsd::accessor"
			" node \"%s\" elem '%s' attr '%s' create %d\n",
			a_node.c_str(),
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			SurfaceAccessibleBase::attributeString(a_attribute).c_str(),
			a_create==SurfaceAccessibleI::e_createMissing);
#endif

	if(a_node.empty())
	{
		a_node=m_isolateNode;
	}

	if(a_node.empty())
	{
		sp<SurfaceAccessorUsdGraph> spAccessor(new SurfaceAccessorUsdGraph);

		if(spAccessor.isValid())
		{
			spAccessor->setWritable(a_writable);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			if(spAccessor->bind(a_element,a_attribute) ||
					a_create==SurfaceAccessibleI::e_createMissing)
			{
				return spAccessor;
			}
		}
	}
	else
	{
		sp<SurfaceAccessorUsd> spAccessor(new SurfaceAccessorUsd);

		if(spAccessor.isValid())
		{
			spAccessor->setYToZ(m_ytoz);

			UsdPrim usdPrim;

			sp<PrimNode> spPrimNode=m_nodeMap[a_node];
			if(spPrimNode.isValid())
			{
				usdPrim=spPrimNode->usdPrim();
			}

			if(!usdPrim)
			{
				return sp<SurfaceAccessorI>(NULL);
			}

			spAccessor->setWritable(a_writable);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			spAccessor->setPrimNode(spPrimNode);
			spAccessor->setFrame(m_frame);
			if(spAccessor->bind(a_element,a_attribute))
			{
				return spAccessor;
			}

			if(a_create==SurfaceAccessibleI::e_createMissing)
			{
				const String primType=usdPrim.GetTypeName().GetText();
				if(primType!="Skeleton")
				{
					return spAccessor;
				}
			}
		}
	}

#if FE_SAU_DEBUG
	feLog("SurfaceAccessibleUsd::accessor fail\n");
#endif

	return sp<SurfaceAccessorI>(NULL);
}

BWORD SurfaceAccessibleUsd::discard(
	SurfaceAccessibleI::Element a_element,String a_name)
{
	if(m_isolateNode.empty())
	{
		feLog("SurfaceAccessibleUsd::discard"
				" not permitted without isolating a node\n");
		return FALSE;
	}

	sp<PrimNode> spPrimNode=m_nodeMap[m_isolateNode];
	if(spPrimNode.isNull())
	{
		feLog("SurfaceAccessibleUsd::discard"
				" no mapping for node \"%s\"\n",m_isolateNode.c_str());
		return FALSE;
	}

	TfToken nameToken(a_name.c_str());

	UsdPrim usdPrim=spPrimNode->usdPrim();
	if(!usdPrim.HasProperty(nameToken))
	{
		feLog("SurfaceAccessibleUsd::discard"
				" no property \"%s\" in node \"%s\"\n",
				a_name.c_str(),m_isolateNode.c_str());
		return FALSE;
	}

	const BWORD removed=usdPrim.RemoveProperty(nameToken);
	if(!removed)
	{
		feLog("SurfaceAccessibleUsd::discard"
				" failed to remove \"%s\" from node \"%s\"\n",
				a_name.c_str(),m_isolateNode.c_str());
	}

	return removed;
}

BWORD SurfaceAccessibleUsd::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAU_LOAD_DEBUG
	feLog("SurfaceAccessibleUsd::load \"%s\"\n",a_filename.c_str());
#endif

	String options;

	if(a_spSettings.isValid())
	{
		//* NOTE cross stdlib use is unsafe in a mixed libstdc++/libc++ build
//		m_frame=a_spSettings->catalog<Real>("frame");
//		options=a_spSettings->catalog<String>("options");
		m_frame=a_spSettings->catalogValue("frame").real();
		options=a_spSettings->catalogValue("options");

//		spFilter=a_spSettings->catalog< sp<Component> >("filter");
	}
	else
	{
		m_frame=0.0;
		options="";
	}

	std::map<String,String> optionMap;

	String buffer=options;
	String option;
	while(!(option=buffer.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

#if FE_SAU_LOAD_DEBUG
		feLog("SurfaceAccessibleUsd::load parse \"%s\" \"%s\"\n",
				property.c_str(),value.c_str());
#endif

		optionMap[property]=value;
	}

	m_ytoz=FALSE;
	if(optionMap.find("ytoz")!=optionMap.end())
	{
		m_ytoz=TRUE;
	}

	if(optionMap.find("node")!=optionMap.end())
	{
		m_isolateNode=optionMap["node"];
	}

	m_usdStage=UsdStage::Open(a_filename.c_str());
	if(!m_usdStage)
	{
		feLog("SurfaceAccessibleUsd::load failed\n");
		return FALSE;
	}

	m_filename=a_filename;

	return interpret();
}

BWORD SurfaceAccessibleUsd::interpret(void)
{
#if FE_SAU_LOAD_DEBUG
	feLog("SurfaceAccessibleUsd::interpret\n");
#endif

	m_nodeArray.clear();
	m_nodeMap.clear();

	outlineClear();

	UsdPrim rootPrim=m_usdStage->GetPseudoRoot();

#if FE_SAU_LOAD_DEBUG
	const TfToken& rootName=rootPrim.GetName();
	const TfToken& rootType=rootPrim.GetTypeName();
	feLog("root \"%s\" type \"%s\"\n",rootName.GetText(),rootType.GetText());

	UsdPrim defPrim=m_usdStage->GetDefaultPrim();
	if(defPrim.IsValid())
	{
		const TfToken& defName=defPrim.GetName();
		const TfToken& defType=defPrim.GetTypeName();
		feLog("def \"%s\" type \"%s\"\n",defName.GetText(),defType.GetText());
	}
#endif

//	for(UsdTreeIterator it(rootPrim); it ; ++it)

	UsdPrim::SubtreeRange subtreeRange=rootPrim.GetAllDescendants();
	for(UsdPrim::SubtreeRange::iterator it=subtreeRange.begin();
			it!=subtreeRange.end();it++)
	{
		const UsdPrim& usdPrim= *it;

		if(!usdPrim.IsValid())
		{
			continue;
		}

		const UsdPrim primParent=usdPrim.GetParent();

		const String primType=usdPrim.GetTypeName().GetText();
		const String primName=usdPrim.GetName().GetText();

#if FE_SAU_LOAD_DEBUG
		feLog("\nprim \"%s\" type \"%s\" parent \"%s\"\n",
				primName.c_str(),primType.c_str(),
				primParent.GetName().GetText());
#endif

		//* TODO get full path for key
		String primPath="/"+primName;
		String parentPath;
		UsdPrim ancestor=primParent;
		while(ancestor)
		{
			const String ancestorName=ancestor.GetName().GetText();
			if(ancestorName=="/")
			{
				break;
			}

			primPath="/"+ancestorName+primPath;
			parentPath="/"+ancestorName+parentPath;

			ancestor=ancestor.GetParent();
		}

#if FE_SAU_LOAD_DEBUG
		feLog("  path \"%s\"\n",primPath.c_str());
#endif

		sp<PrimNode> spPrimNode(new PrimNode());
		m_nodeArray.push_back(spPrimNode);
		m_nodeMap[primPath]=spPrimNode;

		spPrimNode->setUsdPrim(usdPrim);
		spPrimNode->setUsdType(primType);
		spPrimNode->setUsdPath(primPath);
		spPrimNode->setUsdParent(parentPath);

//		const BWORD isXform=(primType=="Xform");
		const BWORD isMesh=(primType=="Mesh");
		const BWORD isCurves=(primType=="BasisCurves");
		const BWORD isSkeleton=(primType=="Skeleton");
		const BWORD addSpecs=(isMesh || isCurves || isSkeleton);
		const BWORD outline=(isMesh || isCurves || isSkeleton);

		if(outline)
		{
			outlineAppend(primPath);
			outlineAppend("  ("+primType+")");
		}

		Array<SurfaceAccessibleI::Spec>& rPointSpecs=
				spPrimNode->pointSpecs();
		Array<SurfaceAccessibleI::Spec>& rVertexSpecs=
				spPrimNode->vertexSpecs();
		Array<SurfaceAccessibleI::Spec>& rPrimitiveSpecs=
				spPrimNode->primitiveSpecs();
		Array<SurfaceAccessibleI::Spec>& rDetailSpecs=
				spPrimNode->detailSpecs();

#if FE_SAU_LOAD_DEBUG
		UsdMetadataValueMap metaMap=usdPrim.GetAllMetadata();
		for(UsdMetadataValueMap::const_iterator it=metaMap.begin();
				it!=metaMap.end(); it++)
		{
			const TfToken& rToken=it->first;
			const VtValue& rValue=it->second;
			feLog("  META \"%s\" type \"%s\"\n",rToken.GetText(),
					rValue.GetTypeName().c_str());

			if(rValue.IsHolding<double>())
			{
				feLog("    %.6G\n",rValue.Get<double>());
			}
			else if(rValue.IsHolding<float>())
			{
				feLog("    %.6G\n",rValue.Get<float>());
			}
			else if(rValue.IsHolding<int>())
			{
				feLog("    %d\n",rValue.Get<int>());
			}
			else if(rValue.IsHolding<std::string>())
			{
				feLog("    '%s'\n",rValue.Get<std::string>().c_str());
			}
			else if(rValue.IsHolding<TfToken>())
			{
				feLog("    '%s'\n",rValue.Get<TfToken>().GetText());
			}
			else if(rValue.IsHolding<SdfSpecifier>())
			{
				std::ostringstream ostrstrm;
				ostrstrm<<rValue.Get<SdfSpecifier>();
				feLog("    '%s'\n",
						String(ostrstrm.str().c_str()).chop("\n").c_str());
			}
			else if(rValue.IsHolding< SdfListOp<TfToken> >())
			{
				//* TODO for "apiSchemas", apparently
			}
		}
#endif

		I32 pointCount=0;
		I32 vertexCount=0;
		I32 primitiveCount=0;

		std::vector<UsdProperty> propArray=usdPrim.GetProperties();
		const I32 propCount=propArray.size();

		//* pass 0: only get pointCount, vertexCount, primitiveCount
		for(I32 pass=0;pass<2;pass++)
		{
			for(I32 propIndex=0;propIndex<propCount;propIndex++)
			{
				UsdProperty& rProperty=propArray[propIndex];
				const String propName=rProperty.GetName().GetText();

				if(!pass && propName!="points" &&
						propName!="faceVertexIndices" &&
						propName!="faceVertexCounts" &&
						propName!="curveVertexCounts")
				{
					continue;
				}

				String line="  "+propName;

#if FE_SAU_LOAD_DEBUG
				String propString;
				propString.sPrintf("prop %d/%d \"%s\"",
						propIndex,propCount,propName.c_str());
#endif

				const BWORD isPrimVar=!strncmp(propName.c_str(),"primvars:",9);

				if(UsdAttribute attr=rProperty.As<UsdAttribute>())
				{
					const String roleName=attr.GetRoleName().GetText();
					const String typeName=
							attr.GetTypeName().GetType().GetTypeName().c_str();

					if(!roleName.empty())
					{
						line.catf(" (%s)",roleName.c_str());
					}
					if(!typeName.empty())
					{
						line.catf(" %s",typeName.c_str());
					}

					//* TODO rate?

#if FE_SAU_LOAD_DEBUG
					feLog("  %s type \"%s\" role \"%s\"\n",
							propString.c_str(),typeName.c_str(),
							roleName.c_str());

					feLog("      HasValue %d\n",attr.HasValue());
					feLog("      HasAuthoredValueOpinion %d\n",
							attr.HasAuthoredValueOpinion());
					feLog("      HasAuthoredValue %d\n",
							attr.HasAuthoredValue());
					feLog("      HasFallbackValue %d\n",
							attr.HasFallbackValue());
					feLog("      ValueMightBeTimeVarying %d\n",
							attr.ValueMightBeTimeVarying());
					feLog("      HasAuthoredConnections %d\n",
							attr.HasAuthoredConnections());
					feLog("      HasColorSpace %d\n",
							attr.HasColorSpace());
#endif

					std::vector<double> timeArray;
					attr.GetTimeSamples(&timeArray);

					const I32 timeCount=timeArray.size();
#if FE_SAU_LOAD_DEBUG
					if(timeCount>2)
					{
						feLog("      time %.6G..%.6G\n",
								timeArray[0],timeArray[timeCount-1]);
					}
					else if(timeCount)
					{
						feLog("      time %.6G\n",timeArray[0]);
					}
#endif

					const UsdTimeCode timeCode=timeCount?
							UsdTimeCode(timeArray[0]): UsdTimeCode::Default();

					SurfaceAccessibleI::Spec spec;
					I32 valueCount(0);

					if(typeName=="TfToken")
					{
						TfToken value;
						if(attr.Get(&value,timeCode))
						{
#if FE_SAU_LOAD_DEBUG
							feLog("      value \"%s\"\n",value.GetText());
#endif

							spec.set(propName,"string");
						}
					}
					else if(typeName=="bool")
					{
						bool value;
						if(attr.Get(&value,timeCode))
						{
#if FE_SAU_LOAD_DEBUG
							feLog("      value %d\n",value);
#endif

							spec.set(propName,"integer");
						}
					}
					else if(typeName=="double")
					{
						double value;
						if(attr.Get(&value,timeCode))
						{
#if FE_SAU_LOAD_DEBUG
							feLog("      value %.6G\n",value);
#endif

							spec.set(propName,"real");
						}
					}
					else if(typeName=="VtArray<TfToken>")
					{
						VtArray<TfToken> array;
						if(attr.Get(&array,timeCode))
						{
							valueCount=array.size();
#if FE_SAU_LOAD_DEBUG
							feLog("      size %d \n",valueCount);
							for(I32 index=0;index<valueCount && index<16;
									index++)
							{
								const TfToken value=array[index];
								feLog("        %d/%d \"%s\"\n",
										index,valueCount,value.GetText());
							}
#endif

							if(propName=="joints")
							{
								spec.set("joint","string");
							}
							else
							{
								spec.set(propName,"string");
							}
						}
					}
					else if(typeName=="VtArray<int>")
					{
						VtArray<int> array;
						if(attr.Get(&array,timeCode))
						{
							valueCount=array.size();
#if FE_SAU_LOAD_DEBUG
							feLog("      size %d \n",valueCount);
							for(I32 index=0;index<valueCount && index<16;
									index++)
							{
								const int value=array[index];
								feLog("        %d/%d %d\n",
										index,valueCount,value);
							}
#endif

#if FALSE
							//* check for non-triangles
							if(propName=="faceVertexCounts")
							{
								for(I32 index=0;index<valueCount;index++)
								{
									const int value=array[index];
									if(value!=3)
									{
										feLog("        %d/%d %d NON-TRIANGLE\n",
												index,valueCount,value);
									}
								}
							}
#endif

							if(propName=="faceVertexIndices")
							{
								vertexCount=valueCount;
							}
							else if(propName=="faceVertexCounts" ||
									propName=="curveVertexCounts")
							{
								primitiveCount=valueCount;
							}
							else
							{
								spec.set(propName,"integer");
							}
						}
					}
					else if(typeName=="VtArray<float>")
					{
						VtArray<float> array;
						if(attr.Get(&array,timeCode))
						{
							valueCount=array.size();
#if FE_SAU_LOAD_DEBUG
							feLog("      size %d \n",valueCount);
							for(I32 index=0;index<valueCount && index<16;
									index++)
							{
								const float value=array[index];
								feLog("        %d/%d %.6G\n",
										index,valueCount,value);
							}
#endif
							spec.set(propName,"real");
						}
					}
					else if(typeName=="VtArray<GfVec3f>")
					{
						VtArray<GfVec3f> array;
						if(attr.Get(&array,timeCode))
						{
							valueCount=array.size();
#if FE_SAU_LOAD_DEBUG
							feLog("      size %d \n",valueCount);
							for(I32 index=0;index<valueCount && index<16;
									index++)
							{
								const GfVec3f& rVec=array[index];
								feLog("        %d/%d %.6G %.6G %.6G\n",
										index,valueCount,
										rVec[0],rVec[1],rVec[2]);
							}
#endif

							if(propName=="points")
							{
								pointCount=valueCount;
							}
							else if(valueCount==2 && propName=="extent")
							{
								if(m_ytoz)
								{
									for(I32 valueIndex=0;valueIndex<valueCount;
											valueIndex++)
									{
										GfVec3f& rValue=array[valueIndex];
										rValue=GfVec3f(
												-rValue[0],rValue[2],rValue[1]);
									}
								}

								const GfVec3f& rVecMin=array[0];
								const GfVec3f& rVecMax=array[1];

								const SpatialVector extentMin(
										rVecMin[0],rVecMin[1],rVecMin[2]);
								const SpatialVector extentMax(
										rVecMax[0],rVecMax[1],rVecMax[2]);

								spPrimNode->setExtentMin(extentMin);
								spPrimNode->setExtentMax(extentMax);
							}

							if(propName=="points")
							{
								spec.set("P","vector3");
							}
							else if(propName=="normals")
							{
								spec.set("N","vector3");
							}
							else
							{
								spec.set(propName,"vector3");
							}
						}
					}
					else if(typeName=="VtArray<GfVec2f>")
					{
						VtArray<GfVec2f> array;
						if(attr.Get(&array,timeCode))
						{
							valueCount=array.size();
#if FE_SAU_LOAD_DEBUG
							feLog("      size %d \n",valueCount);
							for(I32 index=0;index<valueCount && index<16;
									index++)
							{
								const GfVec2f& rVec=array[index];
								feLog("        %d/%d %.6G %.6G\n",
										index,valueCount,rVec[0],rVec[1]);
							}
#endif


							spec.set(propName,"vector3");
						}
					}
					else if(typeName=="GfMatrix4d")
					{
						GfMatrix4d matrix;
						if(attr.Get(&matrix,timeCode))
						{
#if FE_SAU_LOAD_DEBUG
							Matrix<4,4,double> xform;
							set(xform,matrix.GetArray());

							feLog(	"          %s\n"
									"          %s\n"
									"          %s\n"
									"          %s\n",
									c_print(xform.column(0)),
									c_print(xform.column(1)),
									c_print(xform.column(2)),
									c_print(xform.column(3)));
#endif


							//* SurfaceAccessorI would need matrix attr support
//							spec.set(propName,"matrix4x4d");
						}
					}
					else if(typeName=="VtArray<GfMatrix4d>")
					{
						VtArray<GfMatrix4d> array;
						if(attr.Get(&array,timeCode))
						{
							valueCount=array.size();
#if FE_SAU_LOAD_DEBUG
							feLog("      size %d \n",valueCount);
							for(I32 index=0;index<valueCount && index<8;index++)
							{
								const GfMatrix4d& rMat=array[index];

								Matrix<4,4,double> xform;
								set(xform,rMat.GetArray());

								feLog(	"        %d/%d\n"
										"          %s\n"
										"          %s\n"
										"          %s\n"
										"          %s\n",
										index,valueCount,
										c_print(xform.column(0)),
										c_print(xform.column(1)),
										c_print(xform.column(2)),
										c_print(xform.column(3)));
							}
#endif

							//* SurfaceAccessorI would need matrix attr support
//							spec.set(propName,"matrix4x4d");

							if(isSkeleton && propName=="bindTransforms")
							{
								primitiveCount=valueCount;

								spec.set("P","vector3");
							}
						}
					}

					if(!pass)
					{
						continue;
					}

					if(addSpecs && !spec.name().empty())
					{
						if(isSkeleton && propName=="bindTransforms")
						{
							spec.set("P","vector3");
							rPointSpecs.push_back(spec);

							spec.set("Cd","color");
							rPointSpecs.push_back(spec);

							spec.set("refX","vector3");
							rPrimitiveSpecs.push_back(spec);

							spec.set("refY","vector3");
							rPrimitiveSpecs.push_back(spec);

							spec.set("refZ","vector3");
							rPrimitiveSpecs.push_back(spec);

							spec.set("refT","vector3");
							rPrimitiveSpecs.push_back(spec);

							spec.set("joint","string");
							rPrimitiveSpecs.push_back(spec);

							spec.set("name","string");
							rPrimitiveSpecs.push_back(spec);
						}
						else if(!valueCount)
						{
							rDetailSpecs.push_back(spec);

							if(propName=="orientation")
							{
								spec.set("rightHandedWindingOrder","integer");
								rDetailSpecs.push_back(spec);
							}
						}
						else if(isPrimVar && (valueCount==1 ||
								(primitiveCount &&
								valueCount==primitiveCount)))
						{
							rPrimitiveSpecs.push_back(spec);

							if(propName=="primvars:displayColor")
							{
								spec.set("Cd","color");
								rPrimitiveSpecs.push_back(spec);
							}
						}
						else if(pointCount && valueCount==pointCount)
						{
							rPointSpecs.push_back(spec);
						}
						else if(vertexCount && valueCount==vertexCount)
						{
							rVertexSpecs.push_back(spec);
						}

						if(propName=="primvars:st")
						{
							spec.set("uv","vector3");
							rVertexSpecs.push_back(spec);
						}
					}

					if(valueCount)
					{
						line.catf(" [%d]",valueCount);
					}

					if(timeCount>1)
					{
						line.catf(" x%d",timeCount);
					}
				}
				else if(UsdRelationship rel=rProperty.As<UsdRelationship>())
				{
#if FE_SAU_LOAD_DEBUG
					feLog("  %s relationship\n",propString.c_str());
#endif
					line+=" relationship";

					SdfPathVector targets;
					if(rel.GetTargets(&targets))
					{
						const I32 targetCount=targets.size();
						for(I32 targetIndex=0;targetIndex<targetCount;
								targetIndex++)
						{
							SdfPath& rTarget=targets[targetIndex];
							const String targetPath=
									rTarget.GetAsString().c_str();

#if FE_SAU_LOAD_DEBUG
							feLog("    path \"%s\"\n",targetPath.c_str());
#endif
							//* TODO or too much data?
//							line+=" path...";
						}
					}
				}
				else
				{
#if FE_SAU_LOAD_DEBUG
					feLog("  %s unreadable\n",propString.c_str());
#endif
					line+=" UNREADABLE";
				}

				if(outline)
				{
					outlineAppend(line);
				}
			}
		}

		spPrimNode->setPointCount(pointCount);
		spPrimNode->setVertexCount(vertexCount);
		spPrimNode->setPrimitiveCount(primitiveCount);
	}

	//* scan loaded layers
#if FALSE
	SdfLayerHandleSet sdfLayerHandleSet=SdfLayer::GetLoadedLayers();
	for(SdfLayerHandleSet::iterator it=sdfLayerHandleSet.begin();
			it!=sdfLayerHandleSet.end();it++)
	{
		const SdfLayerHandle& sdfLayerHandle= *it;
		const std::string& identifier=sdfLayerHandle->GetIdentifier();
		feLog("layer \"%s\"\n",identifier.c_str());

		const SdfSchemaBase& sdfSchemaBase=sdfLayerHandle->GetSchema();

		std::vector<SdfValueTypeName> allTypes=
				sdfSchemaBase.GetAllTypes();
		const I32 typeCount=allTypes.size();
		for(I32 typeIndex=0;typeIndex<typeCount;typeIndex++)
		{
			feLog("%d/%d \"%s\"\n",typeIndex,typeCount,
					allTypes[typeIndex].GetType().GetTypeName().c_str());
		}
	}
#endif

#if FE_SAU_LOAD_DEBUG
	feLog("complete\n");
#endif

	return TRUE;
}

BWORD SurfaceAccessibleUsd::save(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessibleUsd::save \"%s\"\n",a_filename.c_str());
#endif

	if(!m_usdStage)
	{
		feLog("SurfaceAccessibleUsd::save \"%s\" UsdStage is NULL\n",
				a_filename.c_str());
		return FALSE;
	}

	//* NOTE Save() doesn't use filename; selectively saves in place
	//* TODO maybe do some duplication operation if new name is introduced
	if(a_filename.empty() || a_filename==m_filename)
	{
		m_usdStage->Save();
#if FE_SAU_DEBUG
		feLog("SurfaceAccessibleUsd::save \"%s\" saved %d\n",
				a_filename.c_str());
#endif
		return TRUE;
	}

	//* Export() will write a single flattened layer to one file
	const std::string filename(a_filename.c_str());
	const BWORD success=m_usdStage->Export(filename);

	if(!success)
	{
#if FE_SAU_DEBUG
		feLog("SurfaceAccessibleUsd::save \"%s\" export failed\n",
				a_filename.c_str());
#endif
		return FALSE;
	}

#if FE_SAU_DEBUG
	feLog("SurfaceAccessibleUsd::save \"%s\" exported\n",
			a_filename.c_str());
#endif

	return success;
}

Protectable* SurfaceAccessibleUsd::clone(Protectable* pInstance)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessibleUsd::clone\n");
#endif

	SurfaceAccessibleUsd* pSurfaceAccessibleUsd= pInstance?
			fe_cast<SurfaceAccessibleUsd>(pInstance):
			new SurfaceAccessibleUsd();

	pSurfaceAccessibleUsd->setLibrary(library());
	pSurfaceAccessibleUsd->setFactoryIndex(factoryIndex());

#if TRUE
	//* duplicate layers in UsdStage

	pSurfaceAccessibleUsd->m_isolateNode=m_isolateNode;
	pSurfaceAccessibleUsd->m_ytoz=m_ytoz;
	pSurfaceAccessibleUsd->m_frame=m_frame;

	//* generate unique identifier
	String text=m_filename.basename();
	const String token=text.parse("",".");
	pSurfaceAccessibleUsd->m_filename.sPrintf("%s.%p.usd",
			token.c_str(),pSurfaceAccessibleUsd);
	const std::string identifier(pSurfaceAccessibleUsd->m_filename.c_str());

	pSurfaceAccessibleUsd->m_usdStage=
			UsdStage::CreateInMemory(identifier,TfNullPtr,UsdStage::LoadNone);
	SdfLayerHandle rootLayer=
			pSurfaceAccessibleUsd->m_usdStage->GetRootLayer();

	SdfLayerHandleVector sdfLayerHandleVector=
			m_usdStage->GetUsedLayers();
//			m_usdStage->GetLayerStack();

	//* TODO each source layer should be duplicated as its own clone layer
	const I32 layerCount=1;//sdfLayerHandleVector.size();

	for(I32 layerIndex=0;layerIndex<layerCount;layerIndex++)
	{
		SdfLayerHandle sdfLayerHandle=sdfLayerHandleVector[layerIndex];
		const std::string& sourceIdentifier=sdfLayerHandle->GetIdentifier();

		feLog("SurfaceAccessibleUsd::clone transfer layer %d/%d \"%s\"\n",
				layerIndex,sdfLayerHandleVector.size(),
				sourceIdentifier.c_str());

		rootLayer->TransferContent(sdfLayerHandle);

//		std::string serialization;
//		if(sdfLayerHandle->ExportToString(&serialization))
//		{
//			feLog("EXPORTED %d\n",serialization.length());
//			const bool success=rootLayer->ImportFromString(serialization);
//			feLog("IMPORTED %d\n",success);
//		}
	}

	if(!pSurfaceAccessibleUsd->interpret())
	{
		feLog("SurfaceAccessibleUsd::clone failed to transfer USD layers\n");

		delete pSurfaceAccessibleUsd;
		pSurfaceAccessibleUsd=NULL;
	}
#else
	//* HACK Export() to a file and re-Open()
	const std::string filename("/tmp/clone.usd");
	feLog("SurfaceAccessibleUsd::clone export \"%s\"\n",filename.c_str());
	if(m_usdStage->Export(filename))
	{
		feLog("SurfaceAccessibleUsd::clone load \"%s\"\n",filename.c_str());

		sp<Catalog> spSettings=
				registry()->master()->createCatalog("Load Settings");
		if(m_frame!=Real(0))
		{
//			spSettings->catalog<Real>("frame")=m_frame;
			String value;
			value.sPrintf("%.6G",m_frame);
			spSettings->catalogSet("frame","value","real",value);
		}
		String options;
		if(!m_isolateNode.empty())
		{
			options="node="+m_isolateNode;
		}
		if(m_ytoz)
		{
			options+=" ytoz";
		}
		if(!options.empty())
		{
//			spSettings->catalog<String>("options")=options;
			spSettings->catalogSet("options","value","string",options);
		}
		pSurfaceAccessibleUsd->load(filename.c_str(),spSettings);
	}
	else
	{
		feLog("SurfaceAccessibleUsd::clone failed to export tmp file\n");

		delete pSurfaceAccessibleUsd;
		pSurfaceAccessibleUsd=NULL;
	}
#endif

	return pSurfaceAccessibleUsd;
}

void SurfaceAccessibleUsd::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_nodeName,
	SurfaceAccessibleI::Element a_element) const
{
	if(a_nodeName.empty())
	{
		a_nodeName=m_isolateNode;
	}

	a_rSpecs.clear();

	SurfaceAccessibleI::Spec spec;

	if(a_nodeName.empty())
	{
		switch(a_element)
		{
			case SurfaceAccessibleI::e_point:
				spec.set("P","vector3");
				a_rSpecs.push_back(spec);
				break;
			case SurfaceAccessibleI::e_vertex:
				break;
			case SurfaceAccessibleI::e_primitive:
			{
				spec.set("type","string");
				a_rSpecs.push_back(spec);

				spec.set("name","string");
				a_rSpecs.push_back(spec);

				spec.set("parent","string");
				a_rSpecs.push_back(spec);

				spec.set("extentMin","vector3");
				a_rSpecs.push_back(spec);

				spec.set("extentMax","vector3");
				a_rSpecs.push_back(spec);
			}
				break;
			case SurfaceAccessibleI::e_detail:
				break;
			default:
				;
		}
		return;
	}

	try
	{
		const sp<PrimNode> spPrimNode=primNodeAt(a_nodeName);
		if(spPrimNode.isNull())
		{
			return;
		}

		switch(a_element)
		{
			case SurfaceAccessibleI::e_point:
				a_rSpecs=spPrimNode->pointSpecs();
				break;
			case SurfaceAccessibleI::e_vertex:
				a_rSpecs=spPrimNode->vertexSpecs();
				break;
			case SurfaceAccessibleI::e_primitive:
				a_rSpecs=spPrimNode->primitiveSpecs();
				break;
			case SurfaceAccessibleI::e_detail:
				a_rSpecs=spPrimNode->detailSpecs();
				break;
			default:
				a_rSpecs.clear();
				;
		}
	}
	catch(...)
	{
		feLog("SurfaceAccessibleUsd::attributeSpecs"
				" failed to get PrimNode for \"%s\"\n",a_nodeName.c_str());
		a_rSpecs.clear();
	}
}

I32 SurfaceAccessibleUsd::count(String a_nodeName,
	SurfaceAccessibleI::Element a_element) const
{
//	feLog("SurfaceAccessibleUsd::count element %d\n",a_element);

	if(a_nodeName.empty())
	{
		a_nodeName=m_isolateNode;
	}

	if(a_nodeName.empty())
	{
		//* NOTE matches SurfaceAccessorUsdGraph::elementCount()
		switch(a_element)
		{
			case SurfaceAccessibleI::e_point:
				return 8*m_nodeArray.size();
				break;
			case SurfaceAccessibleI::e_vertex:
				break;
			case SurfaceAccessibleI::e_primitive:
				return m_nodeArray.size();
				break;
			case SurfaceAccessibleI::e_detail:
				return 1;
				break;
			default:
				;
		}
		return 0;
	}

	try
	{
		const sp<PrimNode> spPrimNode=primNodeAt(a_nodeName);
		if(spPrimNode.isNull())
		{
			return 0;
		}

		const UsdPrim usdPrim=spPrimNode->usdPrim();
		const String primType=usdPrim.GetTypeName().GetText();
		const BWORD isSkeleton=(primType=="Skeleton");

		if(isSkeleton)
		{
			I32 jointCount(0);

			TfToken propName("bindTransforms");
			const UsdProperty& rProperty=usdPrim.GetProperty(propName);
			if(UsdAttribute attr=rProperty.As<UsdAttribute>())
			{
				//* TODO get count without getting all the data

				std::vector<double> timeArray;
				attr.GetTimeSamples(&timeArray);
				const I32 timeCount=timeArray.size();

				const UsdTimeCode timeCode=timeCount?
						UsdTimeCode(timeArray[0]): UsdTimeCode::Default();

				VtArray<GfMatrix4d> array;
				if(attr.Get(&array,timeCode))
				{
					jointCount=array.size();
				}
			}
			if(a_element==SurfaceAccessibleI::e_point)
			{
				return 4*jointCount;
			}
			if(a_element==SurfaceAccessibleI::e_primitive)
			{
				return jointCount;
			}
		}

		switch(a_element)
		{
			case SurfaceAccessibleI::e_point:
				return spPrimNode->pointCount();
			case SurfaceAccessibleI::e_vertex:
//~				return spPrimNode->vertexCount();
				return spPrimNode->primitiveCount();
			case SurfaceAccessibleI::e_primitive:
				return spPrimNode->primitiveCount();
			case SurfaceAccessibleI::e_detail:
				return 1;
				break;
			default:
				;
		}
	}
	catch(...)
	{
	}

	return 0;
}

sp<SurfaceI> SurfaceAccessibleUsd::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
#if FALSE
	//* TEMP just a sphere
	sp<SurfaceSphere> spSurfaceSphere=registry()->create("*.SurfaceSphere");
	if(spSurfaceSphere.isValid())
	{
		spSurfaceSphere->setCenter(SpatialVector(0.0,0.0,0.0));
		spSurfaceSphere->setRadius(1.0);
	}

	return spSurfaceSphere;
#endif

	if(a_group.empty())
	{
		a_group=m_isolateNode;
	}

	if(a_group.empty())
	{
		return SurfaceAccessibleBase::surface(a_group,a_restrictions);
	}

	//* NOTE copied from SurfaceAccessibleBase::surface()

	//* TODO select form
	sp<SurfaceI> spSurface;

	FEASSERT(registry().isValid());

	if(registry().isValid())
	{
		const String nodeName=a_group;

		sp<SurfaceAccessorI> spSurfacePoints=
				accessor(nodeName,e_point,e_position,e_refuseMissing);
		const I32 pointCount=spSurfacePoints.isValid()?
				spSurfacePoints->count(): 0;

		sp<SurfaceAccessorI> spSurfaceVertices=
				accessor(nodeName,e_primitive,e_vertices,e_refuseMissing);
		const I32 primitiveCount=spSurfaceVertices.isValid()?
				spSurfaceVertices->count(): 0;

		const BWORD pointCloud=(pointCount && !primitiveCount);

		sp<SurfaceAccessorI> spSurfaceProperties=
				accessor(nodeName,e_primitive,e_properties,e_refuseMissing);

		const BWORD openCurve=(spSurfaceProperties.isValid() &&
				spSurfaceProperties->count() &&
				spSurfaceProperties->integer(0,
				SurfaceAccessibleI::e_openCurve));

		//* TODO be better about being points, not curves,
		//* if both e_excludeCurves and e_excludePolygons are set

		if(pointCloud ||
				(openCurve && !(a_restrictions&SurfaceI::e_excludeCurves)) ||
				(a_restrictions&SurfaceI::e_excludePolygons))
		{
			spSurface=registry()->create(
					"SurfaceI.*.*.SurfaceCurvesAccessible");

			sp<SurfaceCurvesAccessible> spSurfaceCurvesAccessible=
					spSurface;
			if(spSurfaceCurvesAccessible.isValid())
			{
				spSurfaceCurvesAccessible->bind(
						sp<SurfaceAccessibleI>(this));
			}
		}
		else
		{
			spSurface=registry()->create(
					"SurfaceI.*.*.SurfaceTrianglesAccessible");

			sp<SurfaceTrianglesAccessible> spSurfaceTrianglesAccessible=
					spSurface;
			if(spSurfaceTrianglesAccessible.isValid())
			{
				spSurfaceTrianglesAccessible->bind(
						sp<SurfaceAccessibleI>(this));
			}
		}
	}
	else
	{
		feLog("SurfaceAccessibleUsd::surface registry is NULL\n");
	}

	if(spSurface.isNull())
	{
		feLog("SurfaceAccessibleUsd::surface failed to access surface\n");
	}

	if(spSurface.isValid())
	{
		spSurface->setNodeName(a_group);
		spSurface->setName(a_group);
		spSurface->setRestrictions(a_restrictions);
	}

	return spSurface;
}

I32 SurfaceAccessibleUsd::PrimNode::count(
	SurfaceAccessibleI::Element a_element) const
{
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			return pointCount();
		case SurfaceAccessibleI::e_vertex:
			return vertexCount();
		case SurfaceAccessibleI::e_primitive:
			return primitiveCount();
		case SurfaceAccessibleI::e_detail:
			break;
		default:
			;
	}

	return 0;
}

sp<SurfaceAccessibleUsd::PrimNode> SurfaceAccessibleUsd::primNodeAt(
	String a_nodeName) const
{
	std::map< String,sp<PrimNode> >::const_iterator it=
			m_nodeMap.find(a_nodeName);
	if(it==m_nodeMap.end())
	{
		feLog("SurfaceAccessibleUsd::primNodeAt"
				" no PrimNode for \"%s\"\n",a_nodeName.c_str());
		return sp<PrimNode>(NULL);
	}

	const sp<PrimNode> spPrimNode=it->second;

	if(spPrimNode.isNull())
	{
		feLog("SurfaceAccessibleUsd::primNodeAt"
				" NULL PrimNode for \"%s\"\n",a_nodeName.c_str());
	}

	return spPrimNode;
}

} /* namespace ext */
} /* namespace fe */
