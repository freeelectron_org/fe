/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_CatalogWriterJson_h__
#define __json_CatalogWriterJson_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief save catalog to json file

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT CatalogWriterJson:
	virtual public CatalogWriterI
{
	public:

virtual	BWORD	save(String a_filename, sp<Catalog> spCatalog);

	private:

		sp<JsonValue>		m_spJsonRoot;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __json_CatalogWriterJson_h__ */
