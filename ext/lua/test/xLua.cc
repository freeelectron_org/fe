/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "signal/signal.h"
#include "lua/lua.h"
#include "math/math.h"

using namespace fe;
using namespace fe::ext;


int main(int argc,char** argv)
{
	UNIT_START();

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();
		sp<Scope>		spScope(new Scope(*spMaster.raw()));

		assertMath(spMaster->typeMaster());

		spRegistry->manage("fexSignalDL");
		spRegistry->manage("fexLuaDL");
		spRegistry->manage("fexLuaTestDL");

#if 0
		String fname("my_function");
		Record r_rec;
		int a = 117;

		sp<TypeMaster> spTM = spRegistry->master()->typeMaster();
		sp<DispatchI> spMine = spRegistry->create("DispatchI.TestDispatch");

		Peeker peek;
		peek(*spTM);
		feLog(peek.output().c_str());

		Array<Instance> argv(3);
		argv[0].set(spTM, fname);
		argv[1].set(spTM, r_rec);
		argv[2].set(spTM, a);

		spMine->call(argv);

		fprintf(stderr, "rv %d\n", a);
#endif

#if 0
		sp<HandlerI>	spLuaBind(spRegistry->create("HandlerI.LuaBind"));
#endif

		sp<Layout> l_mine = spScope->declare("myLayout");
		sp<Layout> l_sig = spScope->declare("signal");
		l_sig->populate("a", "integer");
		l_sig->populate("r", "record");
		l_sig->populate("rg", "RecordGroup");
		Record r_sig = spScope->createRecord(l_sig);

		sp<Layout> l_other = spScope->declare("other");
		l_other->populate("a", "integer");

		sp<RecordGroup> spRG(new RecordGroup());
		spRG->add(spScope->createRecord(l_sig));
		spRG->add(spScope->createRecord(l_sig));
		spRG->add(spScope->createRecord(l_sig));
		spRG->add(spScope->createRecord(l_other));
		spRG->add(spScope->createRecord(l_mine));

		Accessor<sp<RecordGroup> > aRG;
		aRG.setup(spScope, "rg");
		aRG(r_sig) = spRG;

		sp<SignalerI> spSignaler(spRegistry->create("SignalerI"));

		//spLuaBind->bind(spSignaler, l_sig);
		//spLuaBind->handle(r_sig);

		SystemTicker ticker("xLua");

		feLog("======================================================\n");

		sp<LuaI> spLua(spRegistry->create("LuaI"));

		const String filename("test/test.lua");

		feLog("loading lua file \"%s\"\n",filename.c_str());

		const Result loadResult=spLua->loadFile(filename);
		UNIT_TEST(successful(loadResult));

		if(loadResult==e_invalidFile)
		{
			feLog("file open error\n");
		}
		else if(loadResult==e_readFailed)
		{
			feLog("file read error\n");
		}
		else if(loadResult==e_unsolvable)
		{
			feLog("lua compile error\n");
		}

		if(failure(loadResult))
		{
			feX(argv[0],"load failure");
		}

		spLua->set("sig", r_sig);
		ticker.log("pre");

		feLog("execute lua script\n");
		const BWORD executed=spLua->execute();
		UNIT_TEST(executed);

		ticker.log("first");
		feLog("======================================================\n");
		spLua->set("sig", r_sig);
#if 0
		for(int i = 0; i < 1000; i++)
		{
			spLua->execute();
		}
		ticker.log("loop");
#endif
	}
	catch(Exception &e)
	{
		e.log();
		return 1;
	}
	catch(...)
	{
		feLog("uncaught exception\n");
		return 2;
	}

	UNIT_TRACK(2);
	UNIT_RETURN();
}
