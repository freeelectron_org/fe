/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		SpatialTransform I;
		fe::setIdentity(I);
		SpatialTransform M;
		fe::setIdentity(M);
		translate(M,SpatialVector(1.0f,2.0f));
		rotate(M,30.0f*degToRad,e_zAxis);
		SpatialTransform Minv;
		invert(Minv,M);
		SpatialTransform MM=M;
		MM*=M;
		SpatialTransform Mm1;
		interpolate(Mm1,I,M,-1.0);
		SpatialTransform M2;
		interpolate(M2,I,M,2.0);

		feLog("M\n%s\n",c_print(M));
		feLog("Minv\n%s\n",c_print(Minv));
		feLog("MM\n%s\n",c_print(MM));
		feLog("Mm1\n%s\n",c_print(Mm1));
		feLog("M2\n%s\n",c_print(M2));
		feLog("---\n");

		SpatialTransform A;
		fe::setIdentity(A);
		SpatialTransform B;
		fe::setIdentity(B);

		SpatialTransform C;

		C = A * B;

		scaleAll(A,2.0f);
		UNIT_TEST(A(0,0)==2);
		UNIT_TEST(A(1,1)==2);
		UNIT_TEST(A(2,2)==2);

		Vector2f partial_scale(1.2f,3.4f);
		scale(B,partial_scale);
		UNIT_TEST(B(0,0)==1.2f);
		UNIT_TEST(B(1,1)==3.4f);
		UNIT_TEST(B(2,2)==1.0f);

		Matrix<4,4,double> osg, jad, brk, src;
#if 1
		src[ 0] = 4.36;
		src[ 1] = 0.00;
		src[ 2] = 0.00;
		src[ 3] = 0.00;

		src[ 4] = 0.00;
		src[ 5] = 3.73;
		src[ 6] = 0.00;
		src[ 7] = 0.00;

		src[ 8] = 0.00;
		src[ 9] = 0.00;
		src[10] =-1.00;
		src[11] =-1.00;

		src[12] = 0.00;
		src[13] = 0.00;
		src[14] =-0.20;
		src[15] = 0.00;
#endif

		feLog("%s\n-----------------\n",c_print(src));

		osg_invert(osg, src);
		jad_invert(jad, src);
		brk_invert(brk, src);

		feLog("%s\n-----------------\n",c_print(osg));
		feLog("%s\n-----------------\n",c_print(jad));
		feLog("%s\n-----------------\n",c_print(brk));
		feLog("%s\n-----------------\n",c_print(src));

		Matrix<3,3,double> m0;
		m0[ 0] = 1.3;
		m0[ 1] =-4.1;
		m0[ 2] = 2.4;
		m0[ 3] = 5.8;
		m0[ 4] =-8.1;
		m0[ 5] = 3.5;
		m0[ 6] = 7.6;
		m0[ 7] = 9.7;
		m0[ 8] =-6.2;

		Matrix<3,3,double> m1, m2;

		invert(m1, m0);
		m2 = m1;
		invert(m1, m2);

		for(unsigned int i = 0; i < 9; i++)
		{
			feLog("%.6G %.6G %.6G\n", m0[i], m1[i], m0[i]-m1[i]);
			UNIT_TEST(isZero(m0[i]-m1[i]));
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(16);
	UNIT_RETURN();
}
