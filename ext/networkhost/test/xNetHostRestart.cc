/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "networkhost/networkhost.h"

using namespace fe;
using namespace fe::ext;

void initNet(BWORD a_incrementPort)
{
	sp<NetHost> spNetHost = NetHost::create();
	sp<Master> m_spMaster = spNetHost->master();
	sp<Registry> m_spRegistry = m_spMaster->registry();

	m_spRegistry->manage("feAutoLoadDL");

	static I32 port(9002);

	String confLine;
	confLine.sPrintf("localhost:%d role=server"
			" ioPriority=high transport=tcp",port);

	if(a_incrementPort)
	{
		port++;
	}

	feLog("configuration: \"%s\"\n", confLine.c_str());

	sp<StateCatalog> m_spStateCatalog =
			spNetHost->accessSpace("world", "*.ZeroCatalog");
	FEASSERT(m_spStateCatalog.isValid());

	m_spStateCatalog->configure(confLine);
	m_spStateCatalog->start();
	m_spStateCatalog->stop();
}

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD incrementPort(FALSE);
	BWORD keepHost(FALSE);

	for(I32 index=1;index<argc;index++)
	{
		if(argv[index]==String("inc"))
		{
			incrementPort=TRUE;
		}
		else if(argv[index]==String("keep"))
		{
			keepHost=TRUE;
		}
	}

	BWORD complete(FALSE);

	try
	{
		{
			sp<NetHost> spNetHost;
			if(keepHost)
			{
				spNetHost=NetHost::create();
			}

			const I32 instanceCount(4);
			for(I32 instanceIndex=0;instanceIndex<instanceCount;instanceIndex++)
			{
				feLog("\n>>>> instance %d/%d\n",instanceIndex,instanceCount);
				initNet(incrementPort);
			}

			if(keepHost)
			{
				feLog("\nrelease NetHost\n");
				spNetHost=NULL;
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(2);
	UNIT_RETURN();
}
