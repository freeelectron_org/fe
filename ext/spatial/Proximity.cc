/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Proximity.h"
#include "ProxBrute.h"

namespace fe
{
namespace ext
{


Proximity::Proximity(void)
{
}

Proximity::~Proximity(void)
{
}

void Proximity::initialize(void)
{
}

void Proximity::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_spScope = l_sig->scope();

}

void Proximity::handle(Record &r_sig)
{
	SystemTicker ticker("Proximity");

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	sp<RecordGroup> rg_output =
		cfg< sp<RecordGroup> >("output"); // output group

	m_spProx = cfg< sp<Component> >("prox");

	if(!m_spProx.isValid())
	{
		m_spProx = new ProxBrute();
		m_spProx->setName("default brute force ProxI");
	}

	cfg<String>("ProxI") = m_spProx->name();

	sp<Layout> l_pair = cfg< sp<Layout> >("proximitylayout");

	feAssert(l_pair.isValid(), "Proximity needs pair Layout specified");

	ticker.log();
	unsigned int collisions = m_spProx->detect(l_pair, rg_input, rg_output);
	ticker.log("detect");

	feLog("detected pairs: %d\n", collisions);

#define PAIR_DEBUG
#ifdef PAIR_DEBUG
	unsigned int output_pairs = 0;
	for(RecordGroup::iterator i_rg = rg_output->begin();
		i_rg != rg_output->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		for(int i = 0; i < spRA->length(); i++)
		{
			//fe_fprintf(stderr,"***** pair\n");
			output_pairs++;
		}
	}
	feLog("output pairs: %d\n", output_pairs);
#endif
}


} /* namespace ext */
} /* namespace fe */

