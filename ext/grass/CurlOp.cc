/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

namespace fe
{
namespace ext
{

//* TODO ramp in effect (versus harsh offset at root)

void CurlOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("amplitude")=1.0;
	catalog<Real>("amplitude","high")=1.0;
	catalog<Real>("amplitude","max")=1e6;
	catalog<String>("amplitude","label")="Amplitude";

	catalog<Real>("spin")=360.0;
	catalog<Real>("spin","high")=360.0;
	catalog<Real>("spin","max")=1e6;
	catalog<String>("spin","label")="Spin";

	catalog< sp<Component> >("Input Curves");
}

void CurlOp::handle(Record& a_rSignal)
{
	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Curves")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

//	spOutputAccessible->copy(spInputAccessible);

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	//* TODO maps
	const Real amplitude=catalog<Real>("amplitude");
	const Real fullSpin=catalog<Real>("spin");

	I32 curveCount=0;

	const I32 primitiveCount=spInputVertices->count();

//	feLog("CurlOp::handle input points %d curves %d\n",
//			spInputAccessible->count(SurfaceAccessibleI::e_point),
//			primitiveCount);

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spInputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		Real lastSpin=0.0;

		SpatialVector prior = spInputVertices->spatialVector(primitiveIndex,0);

		for(I32 subIndex=1;subIndex<subCount;subIndex++)
		{
			const Real curlSpin=lastSpin + fullSpin / Real(subCount);
			lastSpin=curlSpin;

			const SpatialVector world =
					spInputVertices->spatialVector(primitiveIndex,subIndex);
			const SpatialVector tangent=unitSafe(world-prior);

			prior = world;

			const SpatialVector facing(1.0,0.0,0.0);

			SpatialTransform xformSelf;
			makeFrameNormalY(xformSelf,world,facing,tangent);

			SpatialTransform invSelf;
			invert(invSelf,xformSelf);

			SpatialVector local;
			transformVector(invSelf,world,local);

			const Real angle=2*fe::pi*curlSpin;

			local[0]+=amplitude*sin(angle);
			local[2]+=amplitude*cos(angle);

			SpatialVector curled;
			transformVector(xformSelf,local,curled);

			spOutputVertices->set(primitiveIndex,subIndex,curled);
		}

		curveCount++;
	}

	String summary;
	summary.sPrintf("%d curve%s\n",curveCount,curveCount==1? "": "s");
	catalog<String>("summary")=summary;

//	feLog("CurlOp::handle curled %d curves\n",curveCount);
}

} /* namespace ext */
} /* namespace fe */
