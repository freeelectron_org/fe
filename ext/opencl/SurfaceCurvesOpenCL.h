/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_SurfaceCurvesOpenCL_h__
#define __opencl_SurfaceCurvesOpenCL_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief OpenCL augmentation of SurfaceCurvesAccessible

	@ingroup opencl

*//***************************************************************************/
class FE_DL_EXPORT SurfaceCurvesOpenCL:
	public SurfaceCurvesAccessible,
	public CastableAs<SurfaceCurvesOpenCL>
{
	public:

					SurfaceCurvesOpenCL(void)								{}
virtual				~SurfaceCurvesOpenCL(void)								{}

	protected:

virtual	void		drawInternal(BWORD a_transformed,
							const SpatialTransform* a_pTransform,
							sp<DrawI> a_spDrawI,
							const fe::Color* a_pColor,
							sp<DrawBufferI> a_spDrawBuffer,
							sp<PartitionI> a_spPartition) const;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_SurfaceCurvesOpenCL_h__ */


