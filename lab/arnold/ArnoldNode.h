/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __arnold_ArnoldNode_h__
#define __arnold_ArnoldNode_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Arnold node

	@ingroup arnold
*//***************************************************************************/
class ArnoldNode:
	public TerminalNode
{
	public:

								ArnoldNode(void);
virtual							~ArnoldNode(void);

		void					setAtNodeProcedural(AtNode* a_pAtNodeProcedural)
								{	m_pAtNodeProcedural=a_pAtNodeProcedural; }
		AtNode*					atNodeProcedural(void)
								{	return m_pAtNodeProcedural; }

		void					setAtNode(AtNode* a_pAtNode)
								{	m_pAtNode=a_pAtNode; }
		AtNode*					atNode(void)
								{	return m_pAtNode; }

		void					initOperator(void);
		void					runOperator(void);

	private:
		void					declareParameters(void);

		void					readParameters(void);
		void					convertOutput(void);

		ArnoldContext			m_arnoldContext;
		sp<SurfaceAccessibleI>	m_spOutputAccessible;

		AtNode*					m_pAtNodeProcedural;
		AtNode*					m_pAtNode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __arnold_ArnoldNode_h__ */
