/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessorCached_h__
#define __surface_SurfaceAccessorCached_h__

#define FE_SACH_DEBUG			FALSE
#define FE_SACH_USE_INSTANCE	FALSE

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Accessor with Deferred Writes

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorCached:
	virtual public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorCached>
{
	public:
						SurfaceAccessorCached(void);
virtual					~SurfaceAccessorCached(void);

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
virtual	String			type(void) const
						{	return m_spAccessorChain->type(); }

virtual	U32				count(void) const
						{	return m_spAccessorChain->count(); }
virtual	U32				subCount(U32 a_index) const
						{	return m_spAccessorChain->subCount(a_index); }

virtual	void			set(U32 a_index,U32 subIndex,String a_string)
						{
							if(!m_changed[a_index])
							{
#if FE_SACH_USE_INSTANCE
								m_value[a_index].create<String>(typeMaster());
#endif
								m_changed[a_index]=TRUE;
								if(m_attrType==e_none)
								{
									m_attrType=e_string;
#if !FE_SACH_USE_INSTANCE
									m_string.resize(m_size);
#endif
								}
								FEASSERT(m_attrType==e_string);
							}
#if FE_SACH_USE_INSTANCE
							m_value[a_index].cast<String>()=a_string;
#else
							m_string[a_index]=a_string;
#endif
						}
virtual	String			string(U32 a_index,U32 a_subIndex=0)
						{
							if(m_changed[a_index])
							{
								FEASSERT(m_attrType==e_string);
#if FE_SACH_USE_INSTANCE
								return m_value[a_index].cast<String>();
#else
								return m_string[a_index];
#endif
							}
							return m_spAccessorChain->string(
									a_index,a_subIndex);
						}

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)
						{
							if(!m_changed[a_index])
							{
#if FE_SACH_USE_INSTANCE
								m_value[a_index].create<I32>(typeMaster());
#endif
								m_changed[a_index]=TRUE;
								if(m_attrType==e_none)
								{
									m_attrType=e_integer;
#if !FE_SACH_USE_INSTANCE
									m_integer.resize(m_size);
#endif
								}
								FEASSERT(m_attrType==e_integer);
							}
#if FE_SACH_USE_INSTANCE
							m_value[a_index].cast<I32>()=a_integer;
#else
							m_integer[a_index]=a_integer;
#endif
						}
virtual	I32				integer(U32 a_index,U32 a_subIndex=0)
						{
							//* TODO should be able to change vertex indexes

							if(m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									m_changed[a_index])
							{
								FEASSERT(m_attrType==e_integer);
#if FE_SACH_USE_INSTANCE
								return m_value[a_index].cast<I32>();
#else
								return m_integer[a_index];
#endif
							}
							return m_spAccessorChain->integer(
									a_index,a_subIndex);
						}
						//* DANGER
virtual	I32				duplicate(U32 a_index,U32 a_subIndex=0)
						{	return m_spAccessorChain->duplicate(
									a_index,a_subIndex); }
						//* DANGER
virtual	void			append(U32 a_index,I32 a_integer)
						{	m_spAccessorChain->append(a_index,a_integer); }
						//* DANGER
virtual	void			remove(U32 a_index,I32 a_integer)
						{	m_spAccessorChain->remove(a_index,a_integer); }

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)
						{
							if(!m_changed[a_index])
							{
#if FE_SACH_USE_INSTANCE
								m_value[a_index].create<Real>(typeMaster());
#endif
								m_changed[a_index]=TRUE;
								if(m_attrType==e_none)
								{
									m_attrType=e_real;
#if !FE_SACH_USE_INSTANCE
									m_real.resize(m_size);
#endif
								}
								FEASSERT(m_attrType==e_real);
							}
#if FE_SACH_USE_INSTANCE
							m_value[a_index].cast<Real>()=a_real;
#else
							m_real[a_index]=a_real;
#endif
						}
virtual	Real			real(U32 a_index,U32 a_subIndex=0)
						{
							if(m_changed[a_index])
							{
								FEASSERT(m_attrType==e_real);
#if FE_SACH_USE_INSTANCE
								return m_value[a_index].cast<Real>();
#else
								return m_real[a_index];
#endif
							}
							return m_spAccessorChain->real(a_index,a_subIndex);
						}

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_spatialVector)
						{
							const U32 index=trueIndex(a_index,a_subIndex);
#if FE_SACH_DEBUG
							feLog("SurfaceAccessorCached::set"
									" \"%s\" %d %d (%d) ch %d %s\n",
									m_attrName.c_str(),
									a_index,a_subIndex,index,
									I32(m_changed[index]),
									c_print(a_spatialVector));
#endif
							if(!m_changed[index])
							{
#if FE_SACH_DEBUG
								feLog("  create %d\n",index);
#endif
								m_changed[index]=TRUE;
								if(m_attrType==e_none)
								{
									m_attrType=e_spatialVector;
#if !FE_SACH_USE_INSTANCE
									m_spatialVector.resize(m_size);
#endif
									if(m_element==SurfaceAccessibleI::
											e_primitiveGroup ||
											m_attribute==
											SurfaceAccessibleI::e_vertices ||
											m_attribute==
											SurfaceAccessibleI::e_properties)
									{
										m_integer.resize(m_size);
										m_subIndex.resize(m_size);
									}
								}
								FEASSERT(m_attrType==e_spatialVector);
#if FE_SACH_USE_INSTANCE
								m_value[index].create<SpatialVector>(
										typeMaster());
#endif
								if(m_element==SurfaceAccessibleI::
										e_primitiveGroup ||
										m_attribute==
										SurfaceAccessibleI::e_vertices ||
										m_attribute==
										SurfaceAccessibleI::e_properties)
								{
									m_integer[index]=a_index;
									m_subIndex[index]=a_subIndex;
								}
							}
#if FE_SACH_USE_INSTANCE
							m_value[index].cast<SpatialVector>()=
									a_spatialVector;
#else
							m_spatialVector[index]=a_spatialVector;
#endif
						}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{
							const U32 index=trueIndex(a_index,a_subIndex);
#if FE_SACH_DEBUG
							feLog("SurfaceAccessorCached::spatialVector"
									" \"%s\" %d %d (%d) ch %d\n",
									m_attrName.c_str(),
									a_index,a_subIndex,index,
									I32(m_changed[index]));
#endif
							if(m_changed[index])
							{
								FEASSERT(m_attrType==e_spatialVector);
#if FE_SACH_DEBUG
								feLog("  override\n");
#endif
#if FE_SACH_USE_INSTANCE
								return m_value[index].cast<SpatialVector>();
#else
								return m_spatialVector[index];
#endif
							}
							return m_spAccessorChain->spatialVector(
									a_index,a_subIndex);
						}

		void			setAccessorChain(sp<SurfaceAccessorI> a_spAccessorI)
						{
							m_spAccessorChain=a_spAccessorI;
							m_element=m_spAccessorChain->element();
							m_attribute=m_spAccessorChain->attribute();
							m_attrName=m_spAccessorChain->attributeName();
							m_count=m_spAccessorChain->count();
							m_size=m_count;
							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices
									|| m_attribute==
									SurfaceAccessibleI::e_properties)
							{
								m_size=0;
								for(U32 index=0;index<m_count;index++)
								{
									m_size+=m_spAccessorChain->subCount(index);
								}
								m_count=0;
							}
							m_changed.resize(m_size);
							for(U32 m=0;m<m_size;m++)
							{
								m_changed[m]=FALSE;
							}
#if FE_SACH_USE_INSTANCE
							m_value.resize(m_size);
#endif
						}

	private:
		U32				trueIndex(U32 a_index,U32 a_subIndex)
						{
							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								const U32 index=m_spAccessorChain->integer(
										a_index,a_subIndex);
								if(index>=m_count)
								{
									m_count=index+1;

									FEASSERT(index<m_size);
#if FALSE
									if(index>=m_size)
									{
										//* tweak
										const U32 newSize=index*1.1+10;

										m_value.resize(newSize);
										m_changed.resize(newSize);
										for(;m_size<newSize;m_size++)
										{
											m_changed[m_size]=FALSE;
										}

										feLog("SurfaceAccessorCached::trueIndex"
												" \"%s\" resized %d\n",
												m_attrName.c_str(),m_size);
									}
#endif
								}
								return index;
							}
							return a_index;
						}

		enum	AttrType
				{
					e_none,
					e_string,
					e_integer,
					e_real,
					e_spatialVector
				};
		sp<TypeMaster>	typeMaster(void)
						{	return registry()->master()->typeMaster(); }

		sp<SurfaceAccessorI>			m_spAccessorChain;

		AttrType						m_attrType;
		U32								m_count;
		U32								m_size;
		Array<I32>						m_changed;

#if FE_SACH_USE_INSTANCE
		Array<Instance>					m_value;
#else
		Array<String>					m_string;
		Array<Real>						m_real;
		Array<SpatialVector>			m_spatialVector;
#endif
		Array<I32>						m_integer;	//* also primitive index
		Array<I32>						m_subIndex;	//* for primitive index
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessorCached_h__ */
