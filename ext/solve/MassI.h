/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_MassI_h__
#define __solve_MassI_h__

namespace fe
{
namespace ext
{

/** 
	*/
class FE_DL_EXPORT MassI
	: virtual public Component
{
	public:
virtual	void	setMass(const Mass &a_mass)									= 0;
virtual	void	getMass(Mass &a_mass)										= 0;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_MassI_h__ */

