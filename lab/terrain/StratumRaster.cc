/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

#define FE_MAX(x,max)		(x>max? max: x)
#define FE_CAP(x,min,max)	(x<min? min: (x>max? max: x))

StratumRaster::StratumRaster(void)
{
}

StratumRaster::~StratumRaster(void)
{
}

void StratumRaster::bind(Record& a_rRecord)
{
	feLog("StratumRaster::bind %s\n",name().c_str());

	RecordableI::bind(a_rRecord);

	if(!m_rasterRV.scope().isValid())
	{
		sp<Scope> spScope=record().layout()->scope();
		m_rasterRV.bind(spScope);
	}

	rebind();
}

void StratumRaster::rebind(void)
{
	feLog("StratumRaster::rebind\n");

	sp<RecordGroup>& rspRG=dataRG();
	FEASSERT(rspRG.isValid());

	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray>& rspRA= *it;
		if(!m_rasterRV.span.check(rspRA))
		{
			continue;
		}

//~		m_rasterRV.bind(rspRA);
//~		while(m_rasterRV.step())
//~		{
//~			feLog("StratumRaster::rebind step\n");
//~		}

		const I32 rasterCount=rspRA->length();
		if(rasterCount>0)
		{
			m_rasterRV.bind(rspRA->getRecord(rasterCount-1));
		}
		else
		{
			m_rasterRV.unbind();
		}
	}

	if(m_rasterRV.record().isValid())
	{
		const U32 required=m_rasterRV.span()[0]*m_rasterRV.span()[1];
		std::vector<Real>& data=m_rasterRV.data();
		U32 size=data.size();

		data.resize(required);
		for(U32 m=size;m<required;m++)
		{
			data[m]=Real(0);
		}
	}
}

Real StratumRaster::height(Real x,Real y) const
{
	if(!m_rasterRV.record().isValid())
	{
		const_cast<StratumRaster*>(this)->rebind();
		if(!m_rasterRV.record().isValid())
		{
			return Real(0);
		}
	}

	Real minX=m_rasterRV.minimum()[0];
	Real minY=m_rasterRV.minimum()[1];
	Real maxX=m_rasterRV.maximum()[0];
	Real maxY=m_rasterRV.maximum()[1];
	U32 verticesX=m_rasterRV.span()[0];
	U32 verticesY=m_rasterRV.span()[1];
	const std::vector<Real>& data=m_rasterRV.data();

	const Real currentOffset=offset();
	const Real currentScale=scale();

	const Real rx=(x-minX)/(maxX-minX)*verticesX-0.5;
	const Real ry=(y-minY)/(maxY-minY)*verticesY-0.5;

	I32 ix=I32(rx);
	I32 iy=I32(ry);

	Real tx=rx-Real(ix);
	Real ty=ry-Real(iy);
	if(tx<Real(0))
	{
		ix--;
		tx+=Real(1);
	}
	if(ty<Real(0))
	{
		iy--;
		ty+=Real(1);
	}

#if FALSE
	feLog("StratumRaster::height(%.6G,%.6G) min %.6G,%.6G max %.6G,%.6G\n",
			x,y,minX,minY,maxX,maxY);
	feLog("  size %d,%d off %.6G scale %.6G r %.6G,%.6G i %d,%d t %.6G,%.6G\n",
			verticesX,verticesY,currentOffset,currentScale,rx,ry,ix,iy,tx,ty);
#endif

	const I32 x0=FE_CAP(ix-1,0,I32(verticesX)-1);
	const I32 x1=FE_CAP(ix,0,I32(verticesX)-1);
	const I32 x2=FE_CAP(ix+1,0,I32(verticesX)-1);
	const I32 x3=FE_CAP(ix+2,0,I32(verticesX)-1);

	const I32 y0=FE_MAX(verticesY-iy,verticesY-1);
	const I32 y1=FE_MAX(verticesY-iy-1,verticesY-1);
	const I32 y2=FE_MAX(verticesY-iy-2,verticesY-1);
	const I32 y3=FE_MAX(verticesY-iy-3,verticesY-1);

	const Real a0=data[y0*verticesX+x0];
	const Real a1=data[y1*verticesX+x0];
	const Real a2=data[y2*verticesX+x0];
	const Real a3=data[y3*verticesX+x0];
	const Real a=Spline<Real>::Cardinal2D(ty,a0,a1,a2,a3);

	const Real b0=data[y0*verticesX+x1];
	const Real b1=data[y1*verticesX+x1];
	const Real b2=data[y2*verticesX+x1];
	const Real b3=data[y3*verticesX+x1];
	const Real b=Spline<Real>::Cardinal2D(ty,b0,b1,b2,b3);

	const Real c0=data[y0*verticesX+x2];
	const Real c1=data[y1*verticesX+x2];
	const Real c2=data[y2*verticesX+x2];
	const Real c3=data[y3*verticesX+x2];
	const Real c=Spline<Real>::Cardinal2D(ty,c0,c1,c2,c3);

	const Real d0=data[y0*verticesX+x3];
	const Real d1=data[y1*verticesX+x3];
	const Real d2=data[y2*verticesX+x3];
	const Real d3=data[y3*verticesX+x3];
	const Real d=Spline<Real>::Cardinal2D(ty,d0,d1,d2,d3);

	const Real height=Spline<Real>::Cardinal2D(tx,a,b,c,d);

	return height*currentScale+currentOffset;
}

SpatialVector StratumRaster::normal(Real x,Real y) const
{
	if(!m_rasterRV.record().isValid())
	{
		const_cast<StratumRaster*>(this)->rebind();
		if(!m_rasterRV.record().isValid())
		{
			return SpatialVector(0.0,0.0,0.0);
		}
	}

	Real minX=m_rasterRV.minimum()[0];
	Real minY=m_rasterRV.minimum()[1];
	Real maxX=m_rasterRV.maximum()[0];
	Real maxY=m_rasterRV.maximum()[1];
	U32 verticesX=m_rasterRV.span()[0];
	U32 verticesY=m_rasterRV.span()[1];
	const std::vector<Real>& data=m_rasterRV.data();

	const Real currentOffset=offset();
	const Real currentScale=scale();

	const Real stepx=(maxX-minX)/verticesX;
	const Real stepy=(maxY-minY)/verticesY;

	const Real rx=(x-minX)/(maxX-minX)*verticesX-0.5;
	const Real ry=(y-minY)/(maxY-minY)*verticesY-0.5;

	I32 ix=I32(rx);
	I32 iy=I32(ry);

	Real tx=rx-Real(ix);
	Real ty=ry-Real(iy);
	if(tx<Real(0))
	{
		ix--;
		tx+=Real(1);
	}
	if(ty<Real(0))
	{
		iy--;
		ty+=Real(1);
	}

	const I32 x0=FE_CAP(ix-1,0,I32(verticesX)-1);
	const I32 x1=FE_CAP(ix,0,I32(verticesX)-1);
	const I32 x2=FE_CAP(ix+1,0,I32(verticesX)-1);
	const I32 x3=FE_CAP(ix+2,0,I32(verticesX)-1);

	const I32 y0=FE_MAX(verticesY-iy,verticesY-1);
	const I32 y1=FE_MAX(verticesY-iy-1,verticesY-1);
	const I32 y2=FE_MAX(verticesY-iy-2,verticesY-1);
	const I32 y3=FE_MAX(verticesY-iy-3,verticesY-1);

	const Real a0=data[y0*verticesX+x0];
	const Real a1=data[y1*verticesX+x0];
	const Real a2=data[y2*verticesX+x0];
	const Real a3=data[y3*verticesX+x0];
	const Real a=Spline<Real>::Cardinal2D(ty,a0,a1,a2,a3)*currentScale+
			currentOffset;

	const Real b0=data[y0*verticesX+x1];
	const Real b1=data[y1*verticesX+x1];
	const Real b2=data[y2*verticesX+x1];
	const Real b3=data[y3*verticesX+x1];
	const Real b=Spline<Real>::Cardinal2D(ty,b0,b1,b2,b3)*currentScale+
			currentOffset;

	const Real c0=data[y0*verticesX+x2];
	const Real c1=data[y1*verticesX+x2];
	const Real c2=data[y2*verticesX+x2];
	const Real c3=data[y3*verticesX+x2];
	const Real c=Spline<Real>::Cardinal2D(ty,c0,c1,c2,c3)*currentScale+
			currentOffset;

	const Real d0=data[y0*verticesX+x3];
	const Real d1=data[y1*verticesX+x3];
	const Real d2=data[y2*verticesX+x3];
	const Real d3=data[y3*verticesX+x3];
	const Real d=Spline<Real>::Cardinal2D(ty,d0,d1,d2,d3)*currentScale+
			currentOffset;

	const Real dx=Spline<Real>::Cardinal2D_d1(tx,a,b,c,d)/stepx;

	const Real e0=data[y0*verticesX+x0];
	const Real e1=data[y0*verticesX+x1];
	const Real e2=data[y0*verticesX+x2];
	const Real e3=data[y0*verticesX+x3];
	const Real e=Spline<Real>::Cardinal2D(tx,e0,e1,e2,e3)*currentScale+
			currentOffset;

	const Real f0=data[y1*verticesX+x0];
	const Real f1=data[y1*verticesX+x1];
	const Real f2=data[y1*verticesX+x2];
	const Real f3=data[y1*verticesX+x3];
	const Real f=Spline<Real>::Cardinal2D(tx,f0,f1,f2,f3)*currentScale+
			currentOffset;

	const Real g0=data[y2*verticesX+x0];
	const Real g1=data[y2*verticesX+x1];
	const Real g2=data[y2*verticesX+x2];
	const Real g3=data[y2*verticesX+x3];
	const Real g=Spline<Real>::Cardinal2D(tx,g0,g1,g2,g3)*currentScale+
			currentOffset;

	const Real h0=data[y3*verticesX+x0];
	const Real h1=data[y3*verticesX+x1];
	const Real h2=data[y3*verticesX+x2];
	const Real h3=data[y3*verticesX+x3];
	const Real h=Spline<Real>::Cardinal2D(tx,h0,h1,h2,h3)*currentScale+
			currentOffset;

	const Real dy=Spline<Real>::Cardinal2D_d1(ty,e,f,g,h)/stepy;

#if FALSE
	feLog("StratumRaster::normal"
			" %.3f %.3f  %d %d t %.3f %.3f s %.3f %.3f d %.3f %.3f\n",
			x,y,ix,iy,tx,ty,stepx,stepy,dx,dy);
	feLog("  %s\n",c_print(unit(SpatialVector(-dx,-dy,1.0f))));
	feLog("  %.3f %.3f %.3f %.3f  %.3f %.3f %.3f %.3f\n",
			a,b,c,d,e,f,g,h);
#endif

	// HACK
	return unit(SpatialVector(-dx,-dy,1.0f));
}

} /* namespace ext */
} /* namespace fe */
