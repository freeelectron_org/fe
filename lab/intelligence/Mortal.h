/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Mortal_h__
#define __intelligence_Mortal_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Mortal RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Mortal: public Audible
{
	public:
		Functor<String>	faction;
		Functor<String>	deathSong;
		Functor<String>	deathSpawn;

				Mortal(void)				{ setName("Mortal"); }
virtual	void	addFunctors(void)
				{
					Audible::addFunctors();

					add(faction,	FE_USE("ai:faction"));
					add(deathSong,	FE_SPEC("ai:deathSong",
							"Sound started on termination"));
					add(deathSpawn,	FE_SPEC("ai:deathSpawn",
							"Layout of object generated on termination"));
				}
virtual	void	initializeRecord(void)
				{
					Audible::initializeRecord();

					identifier()="Mortal";
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Mortal_h__ */
