/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaIterate_h__
#define __lua_LuaIterate_h__

namespace fe
{
namespace ext
{

/** lua execution component.  Can also operate as a handler.

	@copydoc LuaIterate_info
	*/
class LuaIterate : public Initialize<LuaIterate>,
	virtual public Dispatch,
	virtual public HandlerI,
	virtual public LuaI,
	virtual public Config
{
	public:
		LuaIterate(void);
virtual	~LuaIterate(void);

		void	initialize(void);

		// AS LuaI
virtual	bool			preloadString(const String &text)	{ return false; }
virtual	Result			loadFile(const String &filename);
virtual	Result			loadString(const String &text);

virtual	void			set(const String &key, Record r_value);
virtual	void			set(const String &key, sp<RecordArray> ra_value);
virtual	void			set(const String &key, sp<RecordGroup> rg_value);
virtual	void			set(const String &key, const String &value);
virtual	void			set(const String &key, const Real &value)			{}
virtual	void			set(const String &key, Vector2f &value)				{}
virtual	void			set(const String &key, Vector2d &value)				{}
virtual	void			set(const String &key, Vector3f &value)				{}
virtual	void			set(const String &key, Vector3d &value)				{}
virtual	void			set(const String &key, Vector4f &value)				{}
virtual	void			set(const String &key, Vector4d &value)				{}
virtual	void			set(const String &key, sp<Layout> l_value);
virtual	void			set(const String &key, sp<Component> spValue);
virtual	void			set(const String &key, sp<Scope> spValue);
virtual	void			set(const String &key, sp<StateCatalog> spValue)	{}

						//*TODO
virtual	Record			getRecord(const String &key)	{ return Record(); }
virtual	String			getString(const String &key)	{ return String(); }
virtual	Real			getReal(const String &key)		{ return 0.0; }
virtual	Vector2f		getVector2f(const String &key)
						{	return Vector2f(0,0); }
virtual	Vector2d		getVector2d(const String &key)
						{	return Vector2d(0,0); }
virtual	Vector3f		getVector3f(const String &key)
						{	return Vector3d(0,0,0); }
virtual	Vector3d		getVector3d(const String &key)
						{	return Vector3f(0,0,0); }
virtual	Vector4f		getVector4f(const String &key)
						{	return Vector4f(0,0,0,0); }
virtual	Vector4d		getVector4d(const String &key)
						{	return Vector4d(0,0,0,0); }

virtual	bool			execute(void);
virtual	bool			execute(const String &a_fnName) { return false; };
virtual	void			flush(void);
virtual	void			alias(String a_aliasName,String a_trueName)			{}

		// AS HandlerI
virtual	void			handle(Record& r_sig);

		using Dispatch::call;

		// AS DispatchI
virtual	bool			call(const String &a_name, Array<Instance>& a_argv);

	private:
		Result				compile(void);
		lua_State			*m_pLuaState;
		String				m_chunk;
		sp<LuaContext>		m_spContext;
		String				m_name;
#if FE_COUNTED_TRACK
		Array<void*>		m_trackArray;
#endif
};

} /* namespace ext */
} /* namespace fe */


#endif /* __lua_LuaIterate_h__ */

