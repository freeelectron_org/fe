/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_SurfaceStrata_h__
#define __terrain_SurfaceStrata_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Surface Layer

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT SurfaceStrata: public SurfaceTriangles,
		public Initialize<SurfaceStrata>
{
	public:
							SurfaceStrata(void);
virtual						~SurfaceStrata(void)							{}

void						initialize(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* As SurfaceI

							using SurfaceTriangles::sample;

virtual SpatialTransform	sample(Vector2 a_uv) const;

							//* As DrawableI

							using SurfaceTriangles::draw;

virtual	void				draw(sp<DrawI> a_spDrawI,const Color* color) const;

	private:

virtual	void				cache(void);

		sp<StratumDrawI>	m_spStratumDrawI;
		sp<StrataI>			m_spStrataI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_SurfaceStrata_h__ */
