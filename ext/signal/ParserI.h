/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_ParserI_h__
#define __signal_ParserI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Interface to handle a sequence (vector) of tokens/words/strings.

	@ingroup signal

	see @ref component_binding
*//***************************************************************************/
class ParserI:
	virtual public Component,
	public CastableAs<ParserI>
{
	public:
virtual	void	parse(Array<String> &tokens)							= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_ParserI_h__ */

