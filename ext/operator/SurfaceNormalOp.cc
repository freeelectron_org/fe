/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define	FE_NOP_DEBUG	FALSE

namespace fe
{
namespace ext
{

//* TODO make sure driver has UV data

void SurfaceNormalOp::initialize(void)
{
	catalog<String>("Group");
	catalog<String>("Group","label")="Point Group";
	catalog<String>("Group","suggest")="pointGroups";

	catalog<bool>("PointCustom")=false;
	catalog<String>("PointCustom","label")="Custom Point Normal";
	catalog<bool>("PointCustom","joined")=true;

	catalog<String>("PointName")="N";
	catalog<String>("PointName","label")="Attribute";
	catalog<String>("PointName","enabler")="PointCustom";

	catalog<bool>("PrimitiveCustom")=false;
	catalog<String>("PrimitiveCustom","label")="Custom Primitive Normal";
	catalog<bool>("PrimitiveCustom","joined")=true;

	catalog<String>("PrimitiveName")="N";
	catalog<String>("PrimitiveName","label")="Attribute";
	catalog<String>("PrimitiveName","enabler")="PrimitiveCustom";

	catalog<bool>("PointTangentCustom")=false;
	catalog<String>("PointTangentCustom","label")="Custom Point Tangent";
	catalog<bool>("PointTangentCustom","joined")=true;

	catalog<String>("PointTangentName")="tangent";
	catalog<String>("PointTangentName","label")="Attribute";
	catalog<String>("PointTangentName","enabler")="PointTangentCustom";

	catalog<SpatialVector>("DefaultNormal")=SpatialVector(0.0,0.0,1.0);
	catalog<String>("DefaultNormal","label")="Default Normal";
	catalog<String>("DefaultNormal","hint")=
			"On curves without a driver surface or usable bend estimation,"
			"start each curve with this normal.";

	catalog<bool>("PointNormals")=true;
	catalog<String>("PointNormals","label")="Add Point Normals";

	catalog<bool>("PointTangents")=false;
	catalog<String>("PointTangents","label")="Add Point Tangents";

	catalog<bool>("PrimitiveNormals")=false;
	catalog<String>("PrimitiveNormals","label")="Add Primitive Normals";

	catalog<bool>("Clockwise")=true;
	catalog<String>("Clockwise","label")="Clockwise Vertices";
	catalog<String>("Clockwise","hint")=
			"On meshes, expect faces to have vertices in clockwise order."
			"  Toggling this option can flip normal directions.";

	catalog<bool>("FollowBend")=false;
	catalog<String>("FollowBend","label")="Follow Bend";
	catalog<String>("FollowBend","enabler")="PointNormals";
	catalog<String>("FollowBend","hint")=
			"On curves without a driver surface,"
			" try to generate outward point normals"
			" in the same plane as the overall bend of the curve.";

	catalog<bool>("orthogonalize")=true;
	catalog<String>("orthogonalize","label")="Ortho";
	catalog<String>("orthogonalize","enabler")="PointNormals || PointTangents";
	catalog<String>("orthogonalize","hint")=
			"On curves, realign normals to ensure that each point's normal"
			" is perpendicular to either the segment connecting"
			" the adjacent points, or when at the root or tip,"
			" the first or last segment."
			"  On polygons, realign each tangent to be perpendicular"
			" to that point's normal.";

	catalog<bool>("randomSpin")=false;
	catalog<String>("randomSpin","label")="Random Spin";
	catalog<String>("randomSpin","hint")=
			"On curves, start with a random spin around the first segment.";

	catalog<bool>("lockRoots")=false;
	catalog<String>("lockRoots","label")="Lock Roots";
	catalog<String>("lockRoots","hint")=
			"On curves, start with an existing normal for first point"
			" of each curve, if normals already exist.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog< sp<Component> >("Input Reference");
	catalog<bool>("Input Reference","optional")=true;

	catalog< sp<Component> >("Driver Reference");
	catalog<bool>("Driver Reference","optional")=true;
}

void SurfaceNormalOp::handle(Record& a_rSignal)
{
	const BWORD pointNormals=catalog<bool>("PointNormals");
	const BWORD primitiveNormals=catalog<bool>("PrimitiveNormals");
	const BWORD pointTangents=catalog<bool>("PointTangents");
	const BWORD followBend=catalog<bool>("FollowBend");
	const BWORD orthogonalize=catalog<bool>("orthogonalize");
	const BWORD randomSpin=catalog<bool>("randomSpin");

	const BWORD pointCustom=catalog<bool>("PointCustom");
	const String pointName=
			pointCustom? catalog<String>("PointName"): "";

	const BWORD primitiveCustom=catalog<bool>("PrimitiveCustom");
	const String primitiveName=
			primitiveCustom? catalog<String>("PrimitiveName"): "";

	const BWORD pointTangentCustom=catalog<bool>("PointTangentCustom");
	const String pointTangentName=
			pointTangentCustom? catalog<String>("PointTangentName"): "tangent";

	const BWORD clockwise=catalog<bool>("Clockwise");

	const SpatialVector defaultNormal=catalog<SpatialVector>("DefaultNormal");

	String& rSummary=catalog<String>("summary");
	rSummary="";

	if(!pointNormals && !primitiveNormals && !pointTangents)
	{
		rSummary="OFF";
		return;
	}

	const String group=catalog<String>("Group");
	const BWORD grouped=((pointNormals || pointTangents) && !group.empty());

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputProperties;
	if(!access(spInputProperties,spInputAccessible,
			e_primitive,e_properties)) return;

	sp<SurfaceI> spDriver;
	access(spDriver,"Driver Surface",e_quiet);
	if(spDriver.isValid())
	{
		spDriver->setRefinement(0);
	}

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputGroup;
	if(grouped)
	{
		if(!access(spInputGroup,spInputAccessible,
				e_pointGroup,group)) return;
	}
	else
	{
		spInputGroup=spOutputPoint;
	}

	const U32 primitiveCount=spInputVertices->count();
	U32 referenceCount=0;

	sp<SurfaceAccessorI> spReferenceVertices;
	sp<SurfaceI> spDriverReference;
	if(access(spReferenceVertices,"Input Reference",
			e_primitive,e_vertices,e_quiet))
	{
		referenceCount=spReferenceVertices->count();
		if(referenceCount!=primitiveCount)
		{
			String message;
			message.sPrintf("Ignoring input reference with different"
					" number of primitives (%d vs %d);",
					referenceCount,primitiveCount);
			catalog<String>("warning")=message;

			spReferenceVertices=NULL;
		}
		else
		{
			access(spDriverReference,"Driver Reference",e_quiet);
			if(spDriverReference.isValid())
			{
				spDriverReference->setRefinement(0);
			}
			else
			{
				spReferenceVertices=NULL;
			}
		}
	}

	sp<SurfaceAccessorI> spInputUVs;
	SurfaceAccessibleI::Element uvElement=SurfaceAccessibleI::e_vertex;
	if(pointTangents)
	{
		if(!access(spInputUVs,spInputAccessible,
				e_vertex,e_uv,e_quiet))
		{
			uvElement=SurfaceAccessibleI::e_point;
			if(!access(spInputUVs,spInputAccessible,
					e_point,e_uv,e_quiet))
			{
				feLog("SurfaceNormalOp::handle no UVs for tangent\n");
				catalog<String>("warning")="no UVs for tangent;";
			}
		}
	}

	const U32 pointCount=spOutputPoint->count();

	const U32 count=spInputGroup->count();
	if(!count)
	{
		feLog("SurfaceNormalOp::handle point count is zero\n");
		catalog<String>("warning")="no points in input surface;";
		return;
	}

#if	FE_NOP_DEBUG
	feLog("SurfaceNormalOp::handle count %d\n",count);
#endif

	//* TODO option to create/replace vertex normals/tangents instead
	if(pointNormals)
	{
		discard(spOutputAccessible,e_vertex,e_normal);
	}
	if(pointTangents)
	{
		discard(spOutputAccessible,e_vertex,pointTangentName);
	}

	if(pointNormals || pointTangents)
	{
		String text;
		text.sPrintf("%d %spoints",pointCount,
				String(grouped? group+" ":"").c_str());
		rSummary+=String(rSummary.empty()? "": " ")+text;
	}
	if(primitiveNormals)
	{
		String text;
		text.sPrintf("%d primitives",primitiveCount);
		rSummary+=String(rSummary.empty()? "": " + ")+text;
	}

	sp<SurfaceAccessorI> spPrimitiveNormal;
	if(primitiveNormals)
	{
		if(primitiveCustom)
		{
			if(!access(spPrimitiveNormal,spOutputAccessible,
					e_primitive,primitiveName,e_quiet,e_createMissing)) return;
		}
		else
		{
			if(!access(spPrimitiveNormal,spOutputAccessible,
					e_primitive,e_normal,e_quiet,e_createMissing)) return;
		}
	}

	BWORD normalsExisted=FALSE;

	sp<SurfaceAccessorI> spPointNormal;
	sp<SurfaceAccessorI> spPointTangent;

	BWORD* inGroup=new BWORD[pointCount];
	BWORD* normalSet=new BWORD[pointCount];
	BWORD* postOrtho=new BWORD[pointCount];

	SpatialVector* pointNormal=NULL;
	SpatialVector* pointTangent=NULL;

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		normalSet[pointIndex]=FALSE;
		postOrtho[pointIndex]=FALSE;
	}

	if(pointNormals || pointTangents)
	{
		BWORD clearOthers=FALSE;

		if(pointNormals)
		{
			if(pointCustom)
			{
				normalsExisted=access(spPointNormal,spOutputAccessible,
						e_point,pointName,e_quiet,e_refuseMissing);
				if(!grouped || !normalsExisted)
				{
					clearOthers=grouped;
					if(!access(spPointNormal,spOutputAccessible,
							e_point,pointName,
							e_warning,e_createMissing))
					{
						catalog<String>("warning")=
								"failed to create custom point normal attribute";
						return;
					}
				}
			}
			else
			{
				normalsExisted=access(spPointNormal,spOutputAccessible,
						e_point,e_normal,e_quiet,e_refuseMissing);
				if(!grouped || !normalsExisted)
				{
					clearOthers=grouped;
					if(!access(spPointNormal,spOutputAccessible,
							e_point,e_normal,
							e_warning,e_createMissing))
					{
						catalog<String>("warning")=
								"failed to create point normal attribute";
						return;
					}
				}
			}
		}

		if(pointTangents)
		{
			if(!grouped || !access(spPointTangent,spOutputAccessible,
					e_point,pointTangentName,e_quiet,e_refuseMissing))
			{
				clearOthers=grouped;
				if(!access(spPointTangent,spOutputAccessible,
						e_point,pointTangentName,
						e_warning,e_createMissing))
				{
					catalog<String>("warning")=
							"failed to create custom point normal attribute";
					return;
				}
			}
		}

		pointNormal=new SpatialVector[pointCount];
		pointTangent=new SpatialVector[pointCount];

		const SpatialVector zero(0.0,0.0,0.0);

		if(grouped)
		{
			for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				inGroup[pointIndex]=FALSE;
			}
		}

		//* clear normals
		if(clearOthers)
		{
			for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				set(pointNormal[pointIndex]);
				set(pointTangent[pointIndex]);
			}
			for(U32 index=0;index<count;index++)
			{
				const I32 pointIndex=spInputGroup->integer(index);

#if	FE_NOP_DEBUG
				feLog("  %d/%d affects point %d\n",index,count,pointIndex);
#endif

				inGroup[pointIndex]=TRUE;
			}
		}
		else
		{
			for(U32 index=0;index<count;index++)
			{
				const I32 pointIndex=
						grouped? spInputGroup->integer(index): index;

#if	FE_NOP_DEBUG
				feLog("  %d/%d affects point %d\n",index,count,pointIndex);
#endif

				inGroup[pointIndex]=TRUE;

				set(pointNormal[pointIndex]);
				set(pointTangent[pointIndex]);
			}
		}
	}

	const BWORD lockRoots=normalsExisted && catalog<bool>("lockRoots");

	Noise* pNoise=(pointNormals && randomSpin)? new Noise: NULL;

	//* accumulate face normals to points
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const U32 subCount=spInputVertices->subCount(primitiveIndex);
		if(!subCount)
		{
			continue;
		}

		BWORD openCurve=FALSE;
		U32 countU=FALSE;
		U32 countV=FALSE;
		BWORD wrappedU=FALSE;
		BWORD wrappedV=FALSE;

		if(spInputProperties.isValid())
		{
			openCurve=spInputProperties->integer(primitiveIndex,e_openCurve);
			countU=spInputProperties->integer(primitiveIndex,e_countU);
			countV=spInputProperties->integer(primitiveIndex,e_countV);
			wrappedU=spInputProperties->integer(primitiveIndex,e_wrappedU);
			wrappedV=spInputProperties->integer(primitiveIndex,e_wrappedV);
		}

#if	FE_NOP_DEBUG
		feLog("SurfaceNormalOp::handle primitive %d/%d subCount %d\n",
				primitiveIndex,primitiveCount,subCount);
		feLog("  curve %d mesh %d %d wrapped %d %d\n",
				openCurve,countU,countV,wrappedU,wrappedV);
#endif

		if(openCurve)
		{
			//* open curves

			const SpatialVector rootPoint=
					spInputVertices->spatialVector(primitiveIndex,0);

			BWORD useSideVector=FALSE;
			SpatialVector sideVector(0.0,1.0,0.0);

			SpatialVector normal=defaultNormal;

			SpatialVector tangent;
			tangent[0]=defaultNormal[1];
			tangent[1]=defaultNormal[2];
			tangent[2]=defaultNormal[0];

			if(spDriver.isValid())
			{
				if(spDriverReference.isValid() && spReferenceVertices.isValid())
				{
					const SpatialVector rootRef=
							spReferenceVertices->spatialVector(
							primitiveIndex,0);

					sp<SurfaceI::ImpactI> spImpactRef=
							spDriverReference->nearestPoint(rootRef);
					if(spImpactRef.isValid())
					{
						const I32 triangleIndex=spImpactRef->triangleIndex();
						const SpatialBary bary=spImpactRef->barycenter();

						sp<SurfaceI::ImpactI> spImpact=
								spDriver->sampleImpact(triangleIndex,bary);
						if(spImpact.isValid())
						{
							normal=spImpact->du();
						}
					}
				}
				else
				{
					sp<SurfaceI::ImpactI> spImpact=
							spDriver->nearestPoint(rootPoint);
					if(spImpact.isValid())
					{
						normal=spImpact->du();
					}
				}
			}

			if(subCount>1)
			{
				const SpatialVector secondPoint=
						spInputVertices->spatialVector(primitiveIndex,1);
				tangent=secondPoint-rootPoint;

				if(followBend && spDriver.isNull())
				{
					const SpatialVector lastPoint=
							spInputVertices->spatialVector(
							primitiveIndex,subCount-1);
					const SpatialVector reach=lastPoint-rootPoint;
					sideVector=cross(reach,tangent);

					if(magnitudeSquared(sideVector)>1e-6)
					{
						normal=cross(sideVector,tangent);
						useSideVector=TRUE;
					}
				}

				if(orthogonalize)
				{
					//* first segment
					const SpatialVector side=cross(tangent,normal);
					normal=cross(side,tangent);
				}
			}

			I32 pointIndex=spInputVertices->integer(primitiveIndex,0);

			if(spDriver.isNull())
			{
				if(normalSet[pointIndex])
				{
					normal=pointNormal[pointIndex];
					useSideVector=FALSE;
				}
				else if(lockRoots)
				{
					normal=spPointNormal->spatialVector(pointIndex);
				}
			}

			if(primitiveNormals)
			{
				spPrimitiveNormal->set(primitiveIndex,unitSafe(normal));
			}
			if(!pointNormals && !pointTangents)
			{
				continue;
			}

			if(pNoise)
			{
				const Vector2 location(10*primitiveIndex,0);
				const Real sample=pNoise->whiteNoise2d(location);
				const Real spin=fe::pi*sample;

//				feLog("location %s sample %.6G spin %.6G\n",
//						c_print(location),sample,spin);

				const SpatialQuaternion rotation(spin,unitSafe(tangent));

				rotateVector(rotation,normal,normal);
			}

			const I32 rootIndex=
					spInputVertices->integer(primitiveIndex,0);
			if(!grouped || inGroup[rootIndex])
			{
//~				spPointNormal->set(rootIndex,unitSafe(normal));
				pointNormal[rootIndex]=unitSafe(normal);
				pointTangent[rootIndex]=unitSafe(tangent);
				normalSet[rootIndex]=TRUE;
			}

			if(subCount<2)
			{
				continue;
			}

			SpatialVector lastPoint=rootPoint;
			SpatialVector lastNormal=normal;
			SpatialVector lastSegment;
			Real lastLength=0.0;

			SpatialVector thisPoint=
					spInputVertices->spatialVector(primitiveIndex,1);

			for(U32 subIndex=1;subIndex<subCount;subIndex++)
			{
				pointIndex=spInputVertices->integer(primitiveIndex,subIndex);
				const BWORD writeNormal=(!grouped || inGroup[pointIndex]);

				if(subIndex==subCount-1)
				{
					if(writeNormal)
					{
						normal=lastNormal;
						if(orthogonalize && subCount>2)
						{
							//* last segment
							const SpatialVector side=cross(lastSegment,normal);
							normal=cross(side,lastSegment);
						}
//~						spPointNormal->set(pointIndex,unitSafe(normal));
						pointNormal[pointIndex]=unitSafe(normal);
						pointTangent[pointIndex]=unitSafe(lastSegment);
						normalSet[pointIndex]=TRUE;
					}
					continue;
				}

				const SpatialVector nextPoint=spInputVertices->spatialVector(
						primitiveIndex,subIndex+1);

				//* TODO groups

				SpatialVector nextSegment=nextPoint-thisPoint;
				Real nextLength=magnitude(nextSegment);
				nextSegment*=(nextLength>0.0)? 1.0/nextLength: 1.0;

				if(subIndex==1)
				{
					lastSegment=thisPoint-lastPoint;
					lastLength=magnitude(lastSegment);
					lastSegment*=(lastLength>0.0)? 1.0/lastLength: 1.0;
				}

				if(nextLength<1e-6)
				{
					const I32 nextPointIndex=spInputVertices->integer(
							primitiveIndex,subIndex+1);

					String message;
					message.sPrintf("primitive %d uses sequential coincident"
							" points %d and %d;",
							primitiveIndex,pointIndex,nextPointIndex);
					catalog<String>("warning")=message;
				}

				SpatialVector nextNormal=lastNormal;
				if(useSideVector)
				{
					nextNormal=unitSafe(cross(sideVector,nextSegment));
				}
				else
				{
					if(!isZero(lastSegment) && !isZero(nextSegment))
					{
						SpatialTransform rotation;
						set(rotation,lastSegment,nextSegment);

						rotateVector(rotation,lastNormal,nextNormal);
					}
				}

				normal=unitSafe(lastLength*lastNormal+nextLength*nextNormal);

				if(writeNormal)
				{
					pointTangent[pointIndex]=unitSafe(nextPoint-lastPoint);

					if(orthogonalize)
					{
						//* not first or last segment
						const SpatialVector side=
								cross(pointTangent[pointIndex],normal);
						normal=unitSafe(cross(side,pointTangent[pointIndex]));
					}

//~					spPointNormal->set(pointIndex,normal);
					pointNormal[pointIndex]=normal;
					normalSet[pointIndex]=TRUE;
				}

				if(!isZero(nextSegment))
				{
					lastPoint=thisPoint;
//~					lastNormal=nextNormal;
					lastNormal=normal;
					lastSegment=nextSegment;
					lastLength=nextLength;
				}

				thisPoint=nextPoint;
			}

			continue;
		}

		//* mesh
		if(countV>1)
		{
			const U32 maxU=wrappedU? countU: countU-1;
			const U32 maxV=wrappedV? countV: countV-1;

			for(U32 v=0;v<maxV;v++)
			{
				const U32 vv=v*countU;
				for(U32 u=0;u<maxU;u++)
				{
					U32 p[4];
					p[0]=vv+u;
					p[1]=(u==countU-1)? vv: p[0]+1;
					p[2]=(v==countV-1)? u: p[0]+countU;
					p[3]=(u==countU-1)? p[2]-(countU-1): p[2]+1;

					SpatialVector sv[4];
					sv[0]=spInputVertices->spatialVector(primitiveIndex,p[0]);
					sv[1]=spInputVertices->spatialVector(primitiveIndex,p[1]);
					sv[2]=spInputVertices->spatialVector(primitiveIndex,p[2]);
					sv[3]=spInputVertices->spatialVector(primitiveIndex,p[3]);

					SpatialVector faceNormal[2];
					faceNormal[0]= -unitSafe(cross(sv[1]-sv[0],sv[2]-sv[0]));
					faceNormal[1]= -unitSafe(cross(sv[1]-sv[2],sv[3]-sv[2]));

					if(pointNormals)
					{
						for(U32 half=0;half<2;half++)
						{
							for(U32 m=0;m<3;m++)
							{
								const U32 mm=half+m;
								const I32 pointIndex=spInputVertices->integer(
										primitiveIndex,p[mm]);
								if(!grouped || inGroup[pointIndex])
								{
//~									const SpatialVector normal=
//~											spPointNormal->spatialVector(
//~											pointIndex);
//~									spPointNormal->set(pointIndex,
//~											normal+faceNormal[half]);
									pointNormal[pointIndex]+=faceNormal[half];
#if	FE_NOP_DEBUG
									feLog("    uv %d/%d %d/%d"
											" half %d n %s\n",
											u,countU,v,countV,half,
											c_print(pointNormal[pointIndex]));
#endif
								}
							}
						}
					}

					if(primitiveNormals)
					{
						const SpatialVector normal=
								spPrimitiveNormal->spatialVector(
								primitiveIndex);
						spPrimitiveNormal->set(primitiveIndex,
								normal+faceNormal[0]+faceNormal[1]);
					}
				}
			}
			if(primitiveNormals)
			{
				const SpatialVector normal=
						spPrimitiveNormal->spatialVector(primitiveIndex);
				spPrimitiveNormal->set(primitiveIndex,unitSafe(normal));
			}
			continue;
		}

		if(subCount<3)
		{
			continue;
		}

		//* otherwise, presume triangular

		const I32 s0(0);
		I32 s1;
		I32 s2;

		if(subCount==3)
		{
			s1=1;
			s2=2;
		}
		else
		{
			//* NOTE presuming quads
			const I32 pointIndex0=
					spInputVertices->integer(primitiveIndex,0);
			I32 pointIndex1=spInputVertices->integer(primitiveIndex,1);
			if(pointIndex1==pointIndex0)
			{
				s1=2;
				s2=3;
			}
			else
			{
				I32 pointIndex2=spInputVertices->integer(primitiveIndex,2);
				if(pointIndex2==pointIndex0 || pointIndex2==pointIndex1)
				{
					s1=1;
					s2=3;
				}
				else
				{
					s1=1;
					s2=2;
				}
			}
		}

		const SpatialVector p0=
				spInputVertices->spatialVector(primitiveIndex,s0);
		const SpatialVector p1=
				spInputVertices->spatialVector(primitiveIndex,s1);
		const SpatialVector p2=
				spInputVertices->spatialVector(primitiveIndex,s2);

		const SpatialVector faceNormal=unitSafe(cross(p1-p0,p2-p0))*
				(clockwise? Real(-1): Real(1));

#if	FE_NOP_DEBUG
		const I32 v0=spInputVertices->integer(primitiveIndex,s0);
		const I32 v1=spInputVertices->integer(primitiveIndex,s1);
		const I32 v2=spInputVertices->integer(primitiveIndex,s2);

		feLog("  prim %d/%d v0 %d v1 %d v2 %d grouped %d %d %d\n",
					primitiveIndex,primitiveCount,v0,v1,v2,
					inGroup[v0],inGroup[v1],inGroup[v2]);
		feLog("  p0 %s p1 %s p2 %s n %s\n",
					c_print(p0),c_print(p1),c_print(p2),c_print(faceNormal));
#endif

		if(pointNormals || pointTangents)
		{
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);

				if(!grouped || inGroup[pointIndex])
				{
//~					const SpatialVector normal=
//~							spPointNormal->spatialVector(pointIndex);
//~					spPointNormal->set(pointIndex,normal+faceNormal);
#if	FE_NOP_DEBUG
					feLog("    vert %d/%d pt %d n %s + %s\n",
							subIndex,subCount,pointIndex,
							c_print(pointNormal[pointIndex]),
							c_print(faceNormal));
#endif
					pointNormal[pointIndex]+=faceNormal;

					if(orthogonalize)
					{
						postOrtho[pointIndex]=TRUE;
					}
				}
			}
		}

		if(primitiveNormals)
		{
			spPrimitiveNormal->set(primitiveIndex,faceNormal);
		}

		if(spInputUVs.isValid())
		{
			const I32 v0=spInputVertices->integer(primitiveIndex,s0);
			const I32 v1=spInputVertices->integer(primitiveIndex,s1);
			const I32 v2=spInputVertices->integer(primitiveIndex,s2);

			SpatialVector uv0;
			SpatialVector uv1;
			SpatialVector uv2;
			if(uvElement==SurfaceAccessibleI::e_point)
			{
				uv0=spInputUVs->spatialVector(v0);
				uv1=spInputUVs->spatialVector(v1);
				uv2=spInputUVs->spatialVector(v2);
			}
			else
			{
				uv0=spInputUVs->spatialVector(primitiveIndex,s0);
				uv1=spInputUVs->spatialVector(primitiveIndex,s1);
				uv2=spInputUVs->spatialVector(primitiveIndex,s2);
			}
			SpatialVector du(0,0,0);
			SpatialVector dv(0,0,0);
			SpatialVector faceTangent(0,0,0);

			if(!triangleDuDv(du,dv,p0,p1,p2,uv0,uv1,uv2))
			{
				feLog("SurfaceNormalOp::handle"
						"prim %d/%d weak dudv from verts %d %d %d\n",
						primitiveIndex,primitiveCount,v0,v1,v2);

				feLog("  uv %s  %s  %s\n",
						c_print(uv0),c_print(uv1),c_print(uv2));
				feLog("  du %s dv %s\n",c_print(du),c_print(du));
				feLog("  dp1 %s dp2 %s\n",
						c_print(p1-p0),
						c_print(p2-p0));
				feLog("  ndp1 %s ndp2 %s\n",
						c_print(unitSafe(p1-p0)),
						c_print(unitSafe(p2-p0)));
			}

			tangentFromDuDvN(faceTangent,du,dv,faceNormal);

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);

				if(!grouped || inGroup[pointIndex])
				{
					pointTangent[pointIndex]+=faceTangent;
				}
			}
		}
	}

	//* normalize accumulated normals
	if(pointNormals)
	{
		for(U32 index=0;index<count;index++)
		{
			const I32 pointIndex=grouped? spInputGroup->integer(index): index;

			spPointNormal->set(pointIndex,unitSafe(pointNormal[pointIndex]));
		}
	}

	//* normalize accumulated tangents
	if(pointTangents)
	{
		for(U32 index=0;index<count;index++)
		{
			const I32 pointIndex=grouped? spInputGroup->integer(index): index;

			SpatialVector tangent=unitSafe(pointTangent[pointIndex]);
			if(postOrtho[pointIndex])
			{
				const SpatialVector normal=unitSafe(pointNormal[pointIndex]);
				const SpatialVector sideVector=cross(tangent,normal);

				if(magnitudeSquared(sideVector)>1e-6)
				{
					tangent=cross(normal,sideVector);
				}
			}

			spPointTangent->set(pointIndex,tangent);
		}
	}

	delete[] postOrtho;
	delete[] normalSet;
	delete[] inGroup;
	delete[] pointTangent;
	delete[] pointNormal;
}

} /* namespace ext */
} /* namespace fe */
