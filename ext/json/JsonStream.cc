/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

namespace fe
{
namespace ext
{
namespace data
{

JsonStream::JsonStream(void):
		m_name("JsonStream")
{
}

JsonStream::~JsonStream(void)
{
}

void JsonStream::bind(sp<Scope> spScope)
{
	m_spScope = spScope;
}

void JsonStream::output(std::ostream &ostrm, sp<RecordGroup> spRG)
{
	if(m_spWriter.isNull())
	{
		m_spWriter = new JsonWriter(m_spScope);
	}
	m_spWriter->output(ostrm, spRG);
}

sp<RecordGroup> JsonStream::input(std::istream &istrm)
{
	if(m_spReader.isNull())
	{
		m_spReader = new JsonReader(m_spScope);
	}
	return m_spReader->input(istrm);
}

} /* namespace ext */
} /* namespace fe */
} /* namespace */


