/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __planet_FloraHandler_h__
#define __planet_FloraHandler_h__

#include "field/fieldAS.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT FloraHandler:
	virtual public HandlerI,
	virtual public Config,
	public Initialize<FloraHandler>
{
	public:
				FloraHandler(void);
virtual			~FloraHandler(void);
		void	initialize(void);


				// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig);
virtual void	handle(Record &r_sig);

	private:

		AsFlora			m_asFlora;
		AsPlanet		m_asPlanet;
		AsTemporal		m_asTemporal;
		AsProximity		m_asProximity;
		AsScalarField	m_asScalarField;

		sp<ScalarFieldI>	m_populationDensity;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __planet_FloraHandler_h__ */

