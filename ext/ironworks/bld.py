import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["operator"]

def setup(module):
    srcList = [ "ironworks.pmh",
                "ChainOp",
                "WayPathOp",
                "WayPointOp",
                "WayRestraintOp",
                "ironworksDL" ]

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")
    for src in srcList:
            if re.compile(r'(.*)Op$').match(src):
                with open(manifest, 'a') as outfile:
                    outfile.write('\tspManifest->catalog<String>(\n'+
                            '\t\t\t"OperatorSurfaceI.' + src + '.fe")='+
                            '"fexIronWorksDL";\n');

    dll = module.DLL( "fexIronWorksDL", srcList )

    deplibs = forge.corelibs + [
                "fexGeometryDLLib",
                "fexOperateDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexOperateDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexIronWorksDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexIronWorksDL",                   None,   None) ]
