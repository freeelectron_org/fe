/** @file */

#include <plugin/plugin.pmh>

#define	FE_STC_DEBUG			FALSE
#define	FE_STC_DEEP_SNAPSHOT	FALSE

using namespace fe;

namespace fe
{

Result StateCatalog::getTypeName(String a_name,String a_property,
	String& a_rTypeName) const
{
	safeLockShared();
	try
	{
		Result result=preGet(a_name,a_property);
		if(failure(result))
		{
			safeUnlockShared();
			return result;
		}

		a_rTypeName=catalogTypeName(a_name,a_property);

		result=postGet(a_name,a_property);
		if(failure(result))
		{
			safeUnlockShared();
			return result;
		}

		safeUnlockShared();
		return e_ok;
	}
	catch(const Exception& e)
	{
		safeUnlockShared();
		return e_cannotFind;
	}
	catch(...)
	{
		safeUnlockShared();
		throw;
	}
}

Result StateCatalog::getTypeNameUnsafe(String a_name,String a_property,
	String& a_rTypeName) const
{
	try
	{
		Result result=preGet(a_name,a_property);
		if(failure(result))
		{
			return result;
		}

		a_rTypeName=catalogTypeName(a_name,a_property);

		result=postGet(a_name,a_property);
		if(failure(result))
		{
			return result;
		}

		return e_ok;
	}
	catch(const Exception& e)
	{
		return e_cannotFind;
	}
}

void StateCatalog::getStateKeys(Array<String> &a_keys,
	String a_pattern) const
{
	safeLockShared();
	try
	{
		catalogKeys(a_keys,a_pattern);

		safeUnlockShared();
		return;
	}
	catch(...)
	{
		safeUnlockShared();
		throw;
	}
}

void StateCatalog::getStateProperties(String a_name,
	Array<String> &a_properties) const
{
	safeLockShared();
	try
	{
		catalogProperties(a_name,a_properties);

		safeUnlockShared();
		return;
	}
	catch(...)
	{
		safeUnlockShared();
		throw;
	}
}

Result StateCatalog::addListener(sp<ListenerI> a_spListener,String a_name)
{
	if(a_spListener.isNull())
	{
		return e_invalidHandle;
	}

	Mutex::Guard guard(m_notificationMutex);

	Array< sp<ListenerI> >& rListenerArray=m_listenerMap[a_name];
	rListenerArray.push_back(a_spListener);
	a_spListener->bind(sp<StateCatalog>(this),a_name);
	return e_ok;
}

#ifdef FE_STATECATALOG_CALLBACK
Result StateCatalog::addCallback(Callback& a_lambda,String a_name)
{
	if(!a_lambda)
	{
		return e_invalidHandle;
	}

	Mutex::Guard guard(m_notificationMutex);

	Array<Callback>& rCallbackArray=m_callbackMap[a_name];
	rCallbackArray.push_back(a_lambda);
	return e_ok;
}
#endif

Result StateCatalog::addNotification(String a_name,String a_property)
{
//	feLog("StateCatalog::addNotification \"%s\" \"%s\"\n",
//			a_name.c_str(),a_property.c_str());

	Mutex::Guard guard(m_notificationMutex);

	m_notificationArray.push_back(Notification(a_name,a_property));
	return e_ok;
}

Result StateCatalog::sendNotifications(void)
{
	while(TRUE)
	{
		String name;
		String property;

		{
			Mutex::Guard guard(m_notificationMutex);

			const I32 notificationCount=m_notificationArray.size();
			if(!notificationCount)
			{
				break;
			}

			Notification& rNotification=
					m_notificationArray[notificationCount-1];
			name=rNotification.m_name;
			property=rNotification.m_property;

			m_notificationArray.pop_back();
		}

		notify(name,property);
	}

	return e_ok;
}

Result StateCatalog::notify(String a_name,String a_property)
{
//	feLog("StateCatalog::notify \"%s\" \"%s\"\n",
//			a_name.c_str(), a_property.c_str());

	AutoHashMap<String, Array< sp<ListenerI> > >::iterator listenerIt=
			m_listenerMap.find(a_name);
	if(listenerIt!=m_listenerMap.end())
	{
		Array< sp<ListenerI> >& rListenerArray=listenerIt->second;
		for(sp<ListenerI>& rspListener: rListenerArray)
		{
			FEASSERT(rspListener.isValid());
			rspListener->notify(a_name,a_property);
		}
	}

#ifdef FE_STATECATALOG_CALLBACK
	AutoHashMap<String, Array<Callback> >::iterator callbackIt=
			m_callbackMap.find(a_name);
	if(callbackIt!=m_callbackMap.end())
	{
		Array<Callback>& rCallbackArray=callbackIt->second;
		for(Callback& rCallback: rCallbackArray)
		{
			FEASSERT(rCallback);
			rCallback(a_name,a_property);
		}
	}
#endif

	return e_ok;
}

Result StateCatalog::updateState(String a_name,String a_property,
	String a_typeName,String a_value)
{
//	feLog("StateCatalog::updateState \"%s\" \"%s\" \"%s\" \"%s\"\n",
//			a_name.c_str(), a_property.c_str(),
//			a_typeName.c_str(),a_value.c_str());

	if(a_property==FE_STATECATALOG_MESSAGE_PROPERTY)
	{
		if(a_typeName=="string")
		{
			std::deque<String>& rStringQueue=
						catalog< std::deque<String> >(a_name);
			rStringQueue.push_back(a_value);
		}
	}
	else
	{
		Instance* pInstance=NULL;
		if(!catalogSet(a_name,a_property,a_typeName,a_value,&pInstance) ||
				!pInstance)
		{
			return e_cannotChange;
		}

		cacheInstanceUpdates(a_name,a_property,*pInstance);
	}

	return addNotification(a_name,a_property);
}

Result StateCatalog::updateState(String a_name,String a_property,
	String a_typeName,const U8* a_pRawBytes,I32 a_byteCount)
{
	Instance* pInstance=NULL;
	if(!catalogSet(a_name,a_property,a_typeName,
			a_pRawBytes,a_byteCount,&pInstance) || !pInstance)
	{
		return e_cannotChange;
	}

	cacheInstanceUpdates(a_name,a_property,*pInstance);

	return addNotification(a_name,a_property);
}

Result StateCatalog::clearState(String a_name,String a_property)
{
#if FE_STC_DEBUG
	feLog("StateCatalog::clearState \"%s\" \"%s\"\n",
			a_name.c_str(),a_property.c_str());
#endif

	catalogRemove(a_name,a_property);

	//* NOTE similar to cacheInstanceUpdates
	for(AutoHashMap<I32, sp<Snapshot> >::iterator it=m_snapshotMap.begin();
			it!=m_snapshotMap.end();it++)
	{
//		feLog("  CR %d %p\n",it->first,it->second.raw());
		sp<Snapshot>& rspSnapshot=it->second;
		if(rspSnapshot.isValid())
		{
			//* replace, if exists (new type)
			rspSnapshot->m_spUpdateCatalog->catalogRemove(a_name,a_property);

			//* indicate removal with special type
			rspSnapshot->m_spUpdateCatalog->catalog<Catalog::Remove>(
					a_name,a_property);
		}
	}
	return e_ok;
}

Result StateCatalog::getState(String a_name,String a_property,
	String& a_rTypeName,String& a_rValue) const
{
	safeLockShared();
	try
	{
		Result result=preGet(a_name,a_property);
		if(failure(result))
		{
			safeUnlockShared();
			return result;
		}

		a_rValue=catalogValue(a_name,a_property);
		a_rTypeName=catalogTypeName(a_name,a_property);

		result=postGet(a_name,a_property);
		if(failure(result))
		{
			safeUnlockShared();
			return result;
		}

		safeUnlockShared();
		return e_ok;
	}
	catch(const Exception& e)
	{
		safeUnlockShared();
		return e_cannotFind;
	}
	catch(...)
	{
		safeUnlockShared();
		throw;
	}
}

Result StateCatalog::setState(String a_name,String a_property,
	String a_typeName,String a_value)
{
	safeLock();

	Result result=preSet(a_name,a_property);
	if(failure(result))
	{
		safeUnlock();
		return result;
	}

	updateState(a_name,a_property,a_typeName,a_value);

	result=postSet(a_name,a_property);
	if(failure(result))
	{
		safeUnlock();
		return result;
	}

	safeUnlock();

	sendNotifications();

	return e_ok;
}

Result StateCatalog::removeState(String a_name,String a_property)
{
	safeLock();

	Result result=preSet(a_name,a_property);
	if(failure(result))
	{
		safeUnlock();
		return result;
	}

	catalogRemove(a_name,a_property);

	result=postSet(a_name,a_property);
	if(failure(result))
	{
		safeUnlock();
		return result;
	}

	safeUnlock();
	return e_ok;
}

Result StateCatalog::removeState(String a_name)
{
	safeLock();

	//* TODO regex remove over connection to get rid of them all at once

	Result overallResult=e_ok;

	Array<String> properties;
	catalogProperties(a_name,properties);
	const I32 propertyCount=properties.size();

	//* remove each known property, one at a time
	for(I32 propertyIndex=0;propertyIndex<propertyCount;propertyIndex++)
	{
		const String& property=properties[propertyIndex];

		Result result=preSet(a_name,property);
		if(failure(result))
		{
			overallResult=e_undefinedFailure;
			continue;
		}

		catalogRemove(a_name,property);

		result=postSet(a_name,property);
		if(failure(result))
		{
			overallResult=e_undefinedFailure;
			continue;
		}
	}

	safeUnlock();
	return overallResult;
}

Result StateCatalog::overlayState(sp<Catalog> a_spOtherCatalog)
{
	safeLock();

	Array<String> keys;
	a_spOtherCatalog->catalogKeys(keys);
	const I32 keyCount=keys.size();

	for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String& key=keys[keyIndex];

		Array<String> properties;
		a_spOtherCatalog->catalogProperties(key,properties);
		const I32 propertyCount=properties.size();

		for(I32 propertyIndex=0;propertyIndex<propertyCount;propertyIndex++)
		{
			const String& property=properties[propertyIndex];

			Result result=preSet(key,property);
			if(failure(result))
			{
				safeUnlock();
				return result;
			}

			const Instance& instance=
					a_spOtherCatalog->catalogInstance(key,property);

			//* make an independent instance
			catalogInstance(key,property).create(instance.type());
			catalogInstance(key,property).assign(instance);

			result=postSet(key,property);
			if(failure(result))
			{
				safeUnlock();
				return result;
			}
		}
	}

	safeUnlock();
	return e_ok;
}

void StateCatalog::cacheInstanceUpdates(String a_name,String a_property,
	Instance& a_rInstance)
{
#if FE_STC_DEBUG
	feLog("StateCatalog::cacheInstanceUpdates \"%s\" \"%s\"\n",
			a_name.c_str(),a_property.c_str());
#endif

	//* cache update for every snapshot
	for(AutoHashMap<I32, sp<Snapshot> >::iterator it=m_snapshotMap.begin();
			it!=m_snapshotMap.end();it++)
	{
//		feLog("  CI %d %p\n",it->first,it->second.raw());
		sp<Snapshot>& rspSnapshot=it->second;
		if(rspSnapshot.isValid())
		{
			//* NOTE reference copy
			rspSnapshot->m_spUpdateCatalog->catalogInstance(a_name,a_property)=
					a_rInstance;
		}
	}
}

Result StateCatalog::snapshot(sp<Snapshot>& a_rspSnapshot)
{
	//* NOTE another thread can change these members while we are reading them
	const I32 current=m_serial;
	const I32 latest=m_latestSerial;
	a_rspSnapshot=m_latestSnapshot;

	if(current==latest && current==a_rspSnapshot->serial())
	{
#if FE_STC_DEBUG
		feLog("StateCatalog::snapshot using current serial %d\n",current);
#endif
		return e_ok;
	}

	//* NOTE release early so we can reuse in pool
	a_rspSnapshot=NULL;

	safeLock();

#if FE_STC_DEBUG
	feLog("StateCatalog::snapshot current %d latest %d\n",current,latest);
#endif

	AutoHashMap<I32, sp<Snapshot> >::iterator it=m_snapshotMap.find(current);
	if(it!=m_snapshotMap.end())
	{
		feLog("StateCatalog::snapshot"
				" warning found current %d with latest %d\n",
				current,latest);
	}

	sp<Snapshot> spSnapshot;

	for(AutoHashMap<I32, sp<Snapshot> >::iterator it=m_snapshotMap.begin();
			it!=m_snapshotMap.end();it++)
	{
//		feLog("  SN %d %p\n",it->first,it->second.raw());
		sp<Snapshot>& rspExistingSnapshot=it->second;
		if(rspExistingSnapshot.isNull())
		{
			feLog("StateCatalog::snapshot invalid snapshot for key %d\n",
					it->first);
			continue;
		}

		const I32 externalRefs=rspExistingSnapshot->count()-1;

#if FE_STC_DEBUG
		feLog("StateCatalog::snapshot existing key %d serial %d refs %d\n",
				it->first,rspExistingSnapshot->serial(),externalRefs);
		if(it->first!=rspExistingSnapshot->serial())
		{
			feLog("  MISMATCH\n");
		}
#endif

		if(externalRefs<1 && spSnapshot.isNull())
		{
#if FE_STC_DEBUG
			feLog("StateCatalog::snapshot serial %d reusing %d\n",
					current,rspExistingSnapshot->serial());
#endif

			spSnapshot=rspExistingSnapshot;
		}
	}

	if(spSnapshot.isValid())
	{
#if FE_STC_DEEP_SNAPSHOT
		const BWORD shallow=FALSE;
#else
		const BWORD shallow=TRUE;
#endif

//		feLog("Old State\n");
//		spSnapshot->dump();
//		feLog("Old Updates\n");
//		spSnapshot->dumpUpdates();

		spSnapshot->m_spSnapshotCatalog->catalogOverlay(
				spSnapshot->m_spUpdateCatalog,NULL,shallow);
		spSnapshot->m_spUpdateCatalog->catalogClear();
	}
	else
	{
#if FE_STC_DEBUG
		feLog("StateCatalog::snapshot serial %d NEW SNAPSHOT\n",current);
#endif

		spSnapshot=new Snapshot();
		if(spSnapshot.isNull())
		{
			feLog("StateCatalog::snapshot could not create Snapshot\n");
			safeUnlock();
			return e_cannotCreate;
		}

#if FE_STC_DEEP_SNAPSHOT
		spSnapshot->m_spSnapshotCatalog=catalogDeepCopy();
#else
		spSnapshot->m_spSnapshotCatalog=catalogShallowCopy();
#endif
		if(spSnapshot->m_spSnapshotCatalog.isNull())
		{
			feLog("StateCatalog::snapshot could not clone state\n");
			safeUnlock();
			return e_cannotCreate;
		}

		spSnapshot->m_spUpdateCatalog=
				registry()->master()->createCatalog("StateCatalog update");
		if(spSnapshot->m_spUpdateCatalog.isNull())
		{
			feLog("StateCatalog::snapshot"
					" could not create update catalog\n");
			safeUnlock();
			return e_cannotCreate;
		}
	}

	//* remove from previous key
	const I32 oldSerial=spSnapshot->m_serial;
	if(oldSerial>=0)
	{
		m_snapshotMap.erase(oldSerial);
	}

	spSnapshot->m_serial=current;
	spSnapshot->m_flushCount=flushCount();

	m_latestSerial=current;
	m_latestSnapshot=spSnapshot;

	m_snapshotMap[m_latestSerial]=spSnapshot;
	a_rspSnapshot=spSnapshot;

	safeUnlock();

#if FE_STC_DEBUG
	feLog("StateCatalog::snapshot new current serial %d\n",current);
#endif
	return e_ok;
}

Result StateCatalog::Snapshot::getTypeName(String a_name,String a_property,
	String& a_rTypeName) const
{
	FEASSERT(m_spSnapshotCatalog.isValid());

	if(!m_spSnapshotCatalog->cataloged(a_name,a_property))
	{
		return e_cannotFind;
	}

	try
	{
		a_rTypeName=m_spSnapshotCatalog->catalogTypeName(a_name,a_property);
		return e_ok;
	}
	catch(const Exception& e)
	{
		return e_typeMismatch;
	}
}

} /* namespace fe */
