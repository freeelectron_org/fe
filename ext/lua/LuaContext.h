/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaContext_h__
#define __lua_LuaContext_h__

namespace fe
{
namespace ext
{

void *lua_alloc(void *ud, void *ptr, size_t osize, size_t nsize);

class LuaBaseType;

class LuaContext : public Counted
{
	public:
						LuaContext(sp<Master> spMaster, lua_State *pLuaState);
virtual					~LuaContext(void);

		lua_State		*state(void);

		void			pushInstance(Instance &instance);
		Instance		popCreateInstance(void);
		void			popInstance(Instance &instance, bool create=false);

		sp<LuaBaseType>	lookup(FE_UWORD tid);
		sp<TypeMaster>	typeMaster(void) {return m_hpMaster->typeMaster();}
		sp<Master>		master(void) {return m_hpMaster;}

		void			setWindowEventEnums(void);
		void			set(const char *tbl, const char *name, double value);

						//* offset past these lines in debugging
		void			setPreamble(I32 a_lineCount)
						{	m_preamble=a_lineCount; }

		enum
		{
			e_record = 0,
			e_array,
			e_arrayit,
			e_group,
			e_groupit,
			e_layout,
			e_scope,
			e_component,
			e_intarray,
			e_intarrayit,
			e_vector,
			e_vector2f,
			e_vector2d,
			e_vector3f,
			e_vector3d,
			e_vector4f,
			e_vector4d,
			e_transform,
			e_statecatalog,
			e_size
		};

static	int		debug(lua_State *pLuaState);
static	void	error(lua_State *pLuaState, const char *message);
static	int		manage(lua_State *pLuaState);
static	int		catalog(lua_State *pLuaState);
static	int		create(lua_State *pLuaState);
static	int		createRecordArray(lua_State *pLuaState);
static	int		createScope(lua_State *pLuaState);
static	int		createScopeComponent(lua_State *pLuaState);
static	int		createRecordGroup(lua_State *pLuaState);
static	int		createRecord(lua_State *pLuaState);
static	int		createVector3(lua_State *pLuaState);
static	int		b_or(lua_State *pLuaState);
static	int		b_and(lua_State *pLuaState);

const	String&	name(void) const				{ return m_name; }

		void	alias(String a_aliasName,String a_trueName)
				{	m_aliasMap[a_aliasName]=a_trueName; }
		String	aliasedName(String a_name)
				{
					if(m_aliasMap.find(a_name)==m_aliasMap.end())
					{
						return a_name;
					}
					return m_aliasMap[a_name];
				}

	private:
		lua_State								*m_pLuaState;
		sp<BaseType>							m_spBooleanType;
		sp<BaseType>							m_spIntegerType;
		sp<BaseType>							m_spFloatType;
		sp<BaseType>							m_spDoubleType;
		sp<BaseType>							m_spStringType;
		sp<BaseType>							m_spRecordType;
		sp<BaseType>							m_spGroupType;
		sp<BaseType>							m_spArrayType;
		sp<BaseType>							m_spComponentType;
		sp<BaseType>							m_spIntArrayType;
		sp<BaseType>							m_spVector2fType;
		sp<BaseType>							m_spVector2dType;
		sp<BaseType>							m_spVector3fType;
		sp<BaseType>							m_spVector3dType;
		sp<BaseType>							m_spVector4fType;
		sp<BaseType>							m_spVector4dType;
		sp<BaseType>							m_spTransformType;
		sp<BaseType>							m_spStateCatalogType;
		sp<BaseType>							m_spScopeType;
		sp<BaseType>							m_spLayoutType;

		hp<Master>								m_hpMaster;

		Array<sp<LuaBaseType> >					m_luaTypes;

		String									m_name;
		std::map<String,String>					m_aliasMap;
#if FE_COUNTED_TRACK
		Array<void*>							m_trackArray;
#endif
		I32										m_preamble;
};


} /* namespace ext */
} /* namespace fe */


#endif /* __lua_LuaContext_h__ */

