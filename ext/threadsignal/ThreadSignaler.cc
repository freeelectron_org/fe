/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <threadsignal/threadsignal.pmh>

#define FE_TSG_DEBUG	FALSE

namespace fe
{
namespace ext
{

void ThreadSignaler::SignalWorker::operate(void)
{
#if FE_TSG_DEBUG
	feLog("ThreadSignaler::SignalWorker::operate starting thread %d\n",m_id);
#endif

	sp<HandlerI> job(NULL);
	while(m_hpJobQueue->take(job))
	{
#if FE_TSG_DEBUG
		feLog("ThreadSignaler::SignalWorker::operate thread %d Job %p\n",
				m_id,job.raw());
#endif

		if(job.isNull())
		{
			break;
		}

//		m_hpThreadSignaler->signalHandler(m_hpThreadSignaler->m_signal, job);
		job->handleSignal(m_hpThreadSignaler->m_signal, m_hpThreadSignaler);
	}

#if FE_TSG_DEBUG
	feLog("ThreadSignaler::SignalWorker::operate sync thread %d\n",m_id);
#endif

	U32 leader=m_hpJobQueue->synchronize();
	FE_MAYBE_UNUSED(leader);

#if FE_TSG_DEBUG
	feLog("ThreadSignaler::SignalWorker::operate sync'd thread %d leader=%d\n",
			m_id,leader);
#endif
}

ThreadSignaler::ThreadSignaler(void)
{
}

void ThreadSignaler::initialize(void)
{
	Mutex::confirm();
}

ThreadSignaler::~ThreadSignaler(void)
{
}

void ThreadSignaler::signalEntries(Record &signal, t_entrymap *a_entryMap)
{
#if FE_TSG_DEBUG
	feLog("ThreadSignaler::signalEntries \"%s\" \"%s\"\n",
		name().c_str(),
		signal.layout()->name().c_str());
#endif

	sp< Gang< SignalWorker, sp<HandlerI> > > spGang(
			new Gang< SignalWorker, sp<HandlerI> >());

	SAFEGUARD;

	ListCore::Context context;
	Entry *pEntry;

	I32 handlerCount(0);

	// NULL layout
	sp<Layout> spNullLayout;
	(*a_entryMap)[spNullLayout].toHead(context);
	while( (pEntry = (*a_entryMap)[spNullLayout].postIncrement(context)) )
	{
#ifdef MODAL_SIGNALER
		if(m_modeSwitch[pEntry->m_mode])
		{
#endif
			if(pEntry->m_component.isValid())
			{
				sp<HandlerI> spHandlerI=pEntry->m_component;
				if(spHandlerI.isValid())
				{
					spGang->post(spHandlerI);
					handlerCount++;
				}
			}
			else
			{
				(*a_entryMap)[spNullLayout].remove(pEntry, context);
				delete pEntry;
			}
#ifdef MODAL_SIGNALER
		}
#endif
	}

	// the signal's layout
	(*a_entryMap)[signal.layout()].toHead(context);
	while( (pEntry = (*a_entryMap)[signal.layout()].postIncrement(context)) )
	{
#ifdef MODAL_SIGNALER
		if(m_modeSwitch[pEntry->m_mode])
		{
#endif
			if(pEntry->m_component.isValid())
			{
				sp<HandlerI> spHandlerI= pEntry->m_component;
				if(spHandlerI.isValid())
				{
					spGang->post(spHandlerI);
					handlerCount++;
				}
			}
			else
			{
				(*a_entryMap)[signal.layout()].remove(pEntry, context);
				delete pEntry;
			}
#ifdef MODAL_SIGNALER
		}
#endif
	}

	const I32 threads=fe::minimum(handlerCount,Thread::hardwareConcurrency());

#if FE_TSG_DEBUG
	feLog("ThreadSignaler::signalEntries handlers %d threads %d\n",
			handlerCount,threads);
#endif

	for(I32 m=0;m<threads;m++)
	{
		spGang->post(sp<HandlerI>(NULL));
	}

	m_signal=signal;

	SAFEUNLOCK;

//	const BWORD success=
			spGang->start(sp<Counted>(this),threads);

	spGang->finish();
}

} /* namespace ext */
} /* namespace fe */
