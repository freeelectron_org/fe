/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <geometry/geometry.pmh>

namespace fe
{
namespace ext
{

Real ConvergentSpline::configure(
	I32 a_count,SpatialVector* a_pPoint,SpatialVector* a_pNormal)
{
	reset();

	m_count=a_count;
	if(!m_count)
	{
		return Real(0);
	}

	m_pKnots=new F64[m_count];
	m_pControl[0]=new F64[m_count];
	m_pControl[1]=new F64[m_count];
	m_pControl[2]=new F64[m_count];

	Real totalDistance=Real(0);

	SpatialVector lastPoint=a_pPoint[0];
	for(I32 pointIndex=0;pointIndex<m_count;pointIndex++)
	{
		const SpatialVector& rPoint=a_pPoint[pointIndex];

		m_pControl[0][pointIndex]=rPoint[0];
		m_pControl[1][pointIndex]=rPoint[1];
		m_pControl[2][pointIndex]=rPoint[2];

		if(pointIndex)
		{
			totalDistance+=magnitude(rPoint-lastPoint);
		}

		m_pKnots[pointIndex]=totalDistance;

		lastPoint=rPoint;
	}

	SpatialVector* correction=new SpatialVector[m_count];

	for(I32 iteration=0;iteration<m_iterations;iteration++)
	{
		BWORD converging=FALSE;

		for(I32 pointIndex=0;pointIndex<m_count;pointIndex++)
		{
			SpatialVector eval=solve(m_pKnots[pointIndex]);
			correction[pointIndex]=a_pPoint[pointIndex]-eval;

			const SpatialVector rCorrection=correction[pointIndex];
			if(fabs(rCorrection[0])+fabs(rCorrection[1])+
					fabs(rCorrection[2])>m_threshold)
			{
//				feLog("it %d/%d pt %d/%d cor %s\n",iteration,m_iterations,
//						pointIndex,m_count,c_print(rCorrection));
				converging=TRUE;
			}
		}
		if(!converging)
		{
			break;
		}
		for(I32 pointIndex=0;pointIndex<m_count;pointIndex++)
		{
			m_pControl[0][pointIndex]+=correction[pointIndex][0];
			m_pControl[1][pointIndex]+=correction[pointIndex][1];
			m_pControl[2][pointIndex]+=correction[pointIndex][2];
		}
	}

	delete[] correction;

	return totalDistance;
}

SpatialVector ConvergentSpline::solve(Real a_t)
{
	SpatialVector result(
			Spline<F64,F64>::Basis2D(a_t,m_count,m_pKnots,m_pControl[0]),
			Spline<F64,F64>::Basis2D(a_t,m_count,m_pKnots,m_pControl[1]),
			Spline<F64,F64>::Basis2D(a_t,m_count,m_pKnots,m_pControl[2]));

	FEASSERT(checkValid(result));
	return result;
}

} /* namespace ext */
} /* namespace fe */