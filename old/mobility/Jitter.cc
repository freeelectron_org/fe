/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Jitter.h"

namespace fe
{

Jitter::Jitter(void)
{
}

Jitter::~Jitter(void)
{
}

void Jitter::initialize(void)
{
}

void Jitter::handle(Record &r_sig)
{
	m_asMobile.bind(r_sig.layout()->scope());
	m_asParticle.bind(r_sig.layout()->scope());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	SpatialVector zero(0.0, 0.0, 0.0);

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA) && m_asMobile.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector force(0.0, 0.0, 0.0);
				force[0] = -0.1f+0.2f*((float)rand()/(float)RAND_MAX);
				force[1] = -0.1f+0.2f*((float)rand()/(float)RAND_MAX);
				force[2] = -0.1f+0.2f*((float)rand()/(float)RAND_MAX);
				if(!(force == zero))
				{
					m_asParticle.force(spRA, i) += force;
				}
			}
		}
	}
}

} /* namespace */

