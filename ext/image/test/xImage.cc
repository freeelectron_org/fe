/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "image/image.h"
#include "viewer/viewer.h"

using namespace fe;
using namespace fe::ext;

class MyDraw: public HandlerI
{
	public:
				MyDraw(sp<DrawI> spDrawI,sp<ImageI> spImageI,char* filename)
				{
					m_spDrawI=spDrawI;
					m_spImageI=spImageI;
					if(filename)
					{
						if(!m_spImageI->loadSelect(filename))
						{
							feX(e_invalidFile,
									"MyDraw::MyDraw","failed to load %s",
									filename);
						}
					}
					else
					{
						const U32 lenx=128;
						const U32 leny=64;
						U8 data[leny][lenx][3];
						for(U32 y=0;y<leny;y++)
						{
							for(U32 x=0;x<lenx;x++)
							{
								U8* element=data[y][x];
								element[0]=255*((x%2)==(y%2));
								element[1]=255*(((x/2)%2)==((y/2)%2));
								element[2]=255*(((x/4)%2)==((y/4)%2));
							}
						}
						m_spImageI->createSelect();
						m_spImageI->setFormat(ImageI::e_rgb);
						m_spImageI->resize(lenx,leny,1);
						m_spImageI->replaceRegion(0,0,0,lenx,leny,1,data);
					}
				}

virtual void	handle(Record& render)
				{
					if(!m_asViewer.scope().isValid())
					{
						m_asViewer.bind(render.layout()->scope());
					}

					const I32& rLayer=m_asViewer.viewer_layer(render);
					U32 height=(rLayer==2)? m_spImageI->height(): 0;
					SpatialVector location(5.0f,10.0f+height,0.0f);
					m_spDrawI->drawRaster(m_spImageI,location);
				}

	private:
		sp<DrawI>		m_spDrawI;
		sp<ImageI>		m_spImageI;
		AsViewer		m_asViewer;
};

int main(int argc,char** argv)
{
	if(argc<2)
	{
		feLog("%s: No lib or image specified\nTry %s <DL> [image] [frames]\n",
				argv[0],argv[0]);
		return 1;
	}

	UNIT_START();

	BWORD complete=FALSE;
	char* filename=NULL;
	U32 frames=0;
	if(argc>3)
	{
		filename=argv[2];
		frames=atoi(argv[3])+1;
	}
	else if(argc>2)
	{
		frames=atoi(argv[2])+1;
		if(frames<2)
		{
			frames=0;
			filename=argv[2];
		}
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage(argv[1]);
		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			sp<ImageI> spImageI(spRegistry->create("ImageI"));
			if(!spQuickViewerI.isValid() || !spImageI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<MyDraw> spMyDraw(new MyDraw(spDrawI,spImageI,filename));

			spQuickViewerI->insertDrawHandler(spMyDraw);
			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
