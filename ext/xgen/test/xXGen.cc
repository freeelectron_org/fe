/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "surface/surface.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [xgen file]\n",argv[0]);
		return -1;
	}
	const String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		//* TODO figure out latest available version
		Result result=spRegistry->manage("fexXGenDL2016");
		UNIT_TEST(successful(result));

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				spRegistry->create("SurfaceAccessibleI.*.*.xgen");
		UNIT_TEST(spSurfaceAccessibleI.isValid());

		if(spSurfaceAccessibleI.isValid())
		{
			sp<Catalog> spSettings=spMaster->createCatalog("Load Settings");
			spSettings->catalog<String>("options")="xpd=pPlane1";

			const BWORD loaded=spSurfaceAccessibleI->load(filename,spSettings);
			UNIT_TEST(loaded);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
