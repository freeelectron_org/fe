echo off
setlocal

set SIDEFX=c:\Program Files\Side Effects Software

:: auto-find the highest numbered version
for /f "usebackq delims=|" %%f in (`dir /b "%SIDEFX%\Houdini *" ^| findstr "[0-9]*\.[0-9]*\.[0-9]*"`) do set HOUDINI_SUBDIR=%%f
set HOUDINI_VERSION=%HOUDINI_SUBDIR:~8%
echo found Houdini version: %HOUDINI_VERSION%

set HOUDINI_BIN=%SIDEFX%\Houdini %HOUDINI_VERSION%\bin
if exist "%HOUDINI_BIN%" goto found_houdini
    echo could not verify install for Houdini
	echo %HOUDINI_BIN%
    pause
    exit /b
:found_houdini

set FE_ROOT=%cd%

if exist "%FE_ROOT%\lib\x86_win64_optimize\houdini%HOUDINI_VERSION%\dso" goto picked_root
	set FE_ROOT=%USERPROFILE%\scoop\apps\freeelectron\current\fe
if exist "%FE_ROOT%\lib\x86_win64_optimize\houdini%HOUDINI_VERSION%\dso" goto picked_root
    echo could not find valid install for freeelectron
    pause
    exit /b
:picked_root

echo using FE_ROOT as %FE_ROOT%

set HOUDINI_DSO_ERROR=1
set HOUDINI_PATH=^&;%FE_ROOT%\lib\x86_win64_optimize\houdini%HOUDINI_VERSION%
set path=%PATH%;%SIDEFX%\Houdini %HOUDINI_VERSION%\bin
set path=%PATH%;%FE_ROOT%\lib\x86_win64_optimize

houdini -foreground

endlocal
