/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void DrawParticle::handle(Record &record)
{
	if(!m_surveillanceRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_surveillanceRV.bind(spScope);

		m_spSelector=registry()->create("SelectorI");
		FEASSERT(m_spSelector.isValid());

		m_spLine=new DrawMode;
		m_spLine->setLayer(1);

		m_selectOp.bind(spScope);
		m_selectOp.createRecord();
	}

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color darkgreen(0.0f,0.3f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,1.0f,1.0f);
	const Color orange(1.0f,0.5f,0.0f,1.0f);
	const Color blue(0.5f,0.5f,1.0f,1.0f);
	const Color purple(0.5f,0.0f,1.0f,1.0f);
	const Color darkpurple(0.3f,0.0f,0.6f,0.5f);
	const Color cyan(0.0f,1.0f,1.0f,0.4f);

	m_theaterRV.bind(record);

	sp<DrawI> spDrawI=m_theaterRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	//* mark who is visible
	Record surveillance=m_theaterRV.viewpoint();
	if(surveillance.isValid())
	{
		sp<RecordGroup> spObserveRG=
				m_surveillanceRV.observations(surveillance);
		if(spObserveRG.isValid())
		{
			const BWORD cumulative=TRUE;
			const sp<FilterI> spFilter;
			m_spSelector->configure(spObserveRG,spFilter,cumulative);

			m_selectOp.input()=spRG;
			sp<HandlerI> spHandlerI=m_spSelector;
			FEASSERT(spHandlerI.isValid());

			Record record=m_selectOp.record();
			spHandlerI->handle(record);
		}
	}

	//* draw everybody
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_particleRAV.bind(spRA);

		if(!m_particleRAV.recordView().radius.check(spRA))
		{
			continue;
		}

		const Color& basecolor=
				m_particleRAV.recordView().mass.check(spRA)? pink: red;

		for(Particle& particleRV: m_particleRAV)
		{
			Color color=basecolor*(particleRV.picked()? 1.0f: 0.5f);
			color[3]=1.0f;

			SpatialTransform transform;
			setIdentity(transform);
			const SpatialVector& location=particleRV.location();
			translate(transform,location);
			const F32 radius=particleRV.radius();
			SpatialVector scale(radius,radius,radius);
			spDrawI->drawSphere(transform,&scale,color);

			//* draw velocity line
#if FALSE
			if(particleRV.velocity.check())
			{
				spDrawI->pushDrawMode(m_spLine);
				const SpatialVector& velocity=particleRV.velocity();
				SpatialVector vertex[2];
				vertex[0]=location;
				vertex[1]=location-velocity;
				spDrawI->drawLines(vertex,NULL,2,DrawI::e_strip,false,&white);
				spDrawI->popDrawMode();
			}
#endif

#if 0//FEX_BEH_DRAW_TEXT
			ControlCenter controlCenter;
			controlCenter.bind(spRA->getWeakRecord(particleRV.index()));

			char text[32];
			sprintf(text," %.4G %d %d %s %s",
					particleRV.mass.safeGet(),
					controlCenter.dominance.safeGet(),
					controlCenter.subservience.safeGet(),
					controlCenter.faction.check()?
					controlCenter.faction.safeGet().c_str(): "",
					controlCenter.name.check()?
					controlCenter.name.safeGet().c_str(): "");
			spDrawI->drawAlignedText(location+SpatialVector(0.0f,0.5f),
					text,yellow);
#endif
		}
	}
}

} /* namespace ext */
} /* namespace fe */
