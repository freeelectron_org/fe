/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <imgui/imgui.pmh>

#define FE_OSC_SCOPE_BG						TRUE
#define FE_OSC_VISUALIZATION_BG				FALSE
#define FE_OSC_VISUALIZATION_SQUARE_ASPECT	TRUE

#define FE_OSC_HISTORY						1000
#define FE_OSC_HISTORY_SHORT				200

namespace fe
{
namespace ext
{

Oscilloscope::Oscilloscope(void):
	m_indicatorSpace(100),
	m_historyPage(0),
	m_historyIndex(0)
{
	set(m_historyScale,1.0,1.0,1.0);

	m_history[0].resize(FE_OSC_HISTORY);
	m_history[1].resize(FE_OSC_HISTORY);
	m_history[2].resize(FE_OSC_HISTORY);
	m_history[3].resize(FE_OSC_HISTORY);

	m_spDrawText=new DrawMode();
	m_spDrawText->setDrawStyle(DrawMode::e_solid);
	m_spDrawText->setLit(FALSE);
	m_spDrawText->setZBuffering(FALSE);
	m_spDrawText->setAntialias(TRUE);
	m_spDrawText->setPointSize(4.0);
}

Oscilloscope::~Oscilloscope(void)
{
}

void Oscilloscope::bind(sp<DrawI> a_spDrawI)
{
	m_spDrawI=a_spDrawI;

	const Real multiplication=m_spDrawI->multiplication();
	m_spDrawText->setPointSize(4.0*multiplication);
	m_spDrawText->setLineWidth(multiplication);
}

void Oscilloscope::clear(void)
{
	const I32 historyCount=m_history[m_historyPage].size();
	for(I32 index=0;index<historyCount;index++)
	{
		set(m_history[m_historyPage][index]);
	}
	m_warmup=FE_OSC_HISTORY_SHORT;
	m_doClear=TRUE;
}

void Oscilloscope::step(void)
{
	m_historyIndex++;

	const I32 historyCount=m_history[m_historyPage].size();
	if(m_historyIndex>=historyCount)
	{
		m_historyIndex-=historyCount;
	}

	if(m_warmup>0)
	{
		m_warmup--;
	}
	m_doClear=FALSE;
}

void Oscilloscope::setPane(I32 a_offsetX,I32 a_offsetY,I32 a_sizeX,I32 a_sizeY)
{
	m_offsetX=a_offsetX;
	m_offsetY=a_offsetY;
	m_sizeX=a_sizeX;
	m_sizeY=a_sizeY;

	m_pageWidth=FE_OSC_HISTORY_SHORT+m_indicatorSpace;
}

void Oscilloscope::stepValue(void)
{
	setHistory(0.0);

	step();
}

void Oscilloscope::stepValue(F64 a_value)
{
	setHistory(a_value);
	scopeValue(a_value);

	step();
}

void Oscilloscope::stepValue(Vector3f a_value)
{
	setHistory(a_value);
	scopeValue(SpatialVector(a_value));

	setPage(1);
	setHistory(a_value);
	visualizePlanes();

	setPage(0);

	step();
}

void Oscilloscope::stepValue(Vector3d a_value)
{
	setHistory(a_value);
	scopeValue(SpatialVector(a_value));

	setPage(1);
	setHistory(a_value);
	visualizePlanes();

	setPage(0);

	step();
}

void Oscilloscope::stepValue(SpatialEuler a_value)
{
	setHistory(a_value);
	scopeValue(SpatialVector(a_value));

	step();
}

void Oscilloscope::stepValue(Color a_value)
{
	setHistory(a_value);
	scopeValue(a_value);

	step();
}

void Oscilloscope::stepValue(SpatialQuaternion a_value)
{
	const Vector4d value4(a_value[0],a_value[1],a_value[2],a_value[3]);
	setHistory(value4);
	scopeValue(value4);

	setPage(1);

	const SpatialVector unitX(1.0,0.0,0.0);
	SpatialVector spinner;
	rotateVector(a_value,unitX,spinner);

	setHistory(spinner);
	visualizePlanes();

	setPage(0);

	step();
}

void Oscilloscope::stepValue(SpatialTransform a_value)
{
	const Vector4d value0(a_value(0,0),a_value(0,1),a_value(0,2),a_value(0,3));
	setHistory(value0);
	scopeValue(value0);

	setPage(1);
	const Vector4d value1(a_value(1,0),a_value(1,1),a_value(1,2),a_value(1,3));
	setHistory(value1);
	scopeValue(value1);

	setPage(2);
	const Vector4d value2(a_value(2,0),a_value(2,1),a_value(2,2),a_value(2,3));
	setHistory(value2);
	scopeValue(value2);

	setPage(3);
	setHistory(a_value.translation());
	visualizePlanes();

	setPage(0);

	step();
}

void Oscilloscope::setHistory(Vector4d a_value)
{
	if(m_historyIndex>=I32(m_history[m_historyPage].size()))
	{
		return;
	}

	m_history[m_historyPage][m_historyIndex]=a_value;

	if(m_doClear)
	{
		const I32 historyCount=m_history[m_historyPage].size();
		for(I32 index=0;index<historyCount;index++)
		{
			m_history[m_historyPage][index]=
					m_history[m_historyPage][m_historyIndex];
		}

		m_lastValueMin[m_historyPage]=a_value;
		m_lastValueMax[m_historyPage]=a_value;
	}
}

void Oscilloscope::scopeValue(F64 a_value)
{
	scopeValue(m_offsetX,m_offsetY+0.05*m_sizeY,
			m_pageWidth,0.9*m_sizeY,a_value,0);
}

void Oscilloscope::scopeValue(SpatialVector a_value)
{
	const Real subHeight(0.29*m_sizeY);

	scopeValue(m_offsetX,m_offsetY+0.67*m_sizeY,
			m_pageWidth,subHeight,a_value[0],0);
	scopeValue(m_offsetX,m_offsetY+0.35*m_sizeY,
			m_pageWidth,subHeight,a_value[1],1);
	scopeValue(m_offsetX,m_offsetY+0.03*m_sizeY,
			m_pageWidth,subHeight,a_value[2],2);
}

void Oscilloscope::visualizePlanes(void)
{
	const Real subHeight(0.29*m_sizeY);
	const Real subWidth(subHeight);

	const I32 pageOffsetX=m_offsetX+m_historyPage*(m_pageWidth+100);

	if(pageOffsetX>m_offsetX+m_sizeX)
	{
		return;
	}

	const Box2i scissor(Vector2i(m_offsetX,m_offsetY),
			Vector2i(m_sizeX,m_sizeY));
	sp<ViewI> spView=m_spDrawI->view();
	spView->setScissor(&scissor);

	visualizePlane(pageOffsetX,m_offsetY+0.67*m_sizeY,
			subWidth,subHeight,1,2);
	visualizePlane(pageOffsetX,m_offsetY+0.35*m_sizeY,
			subWidth,subHeight,0,2);
	visualizePlane(pageOffsetX,m_offsetY+0.03*m_sizeY,
			subWidth,subHeight,0,1);

	spView->setScissor(NULL);
}

void Oscilloscope::scopeValue(Vector4d a_value)
{
	const Real subHeight(0.20*m_sizeY);

	const I32 pageOffsetX=m_offsetX+m_historyPage*(m_pageWidth+100);

	scopeValue(pageOffsetX,m_offsetY+0.76*m_sizeY,
			m_pageWidth,subHeight,a_value[0],0);
	scopeValue(pageOffsetX,m_offsetY+0.52*m_sizeY,
			m_pageWidth,subHeight,a_value[1],1);
	scopeValue(pageOffsetX,m_offsetY+0.28*m_sizeY,
			m_pageWidth,subHeight,a_value[2],2);
	scopeValue(pageOffsetX,m_offsetY+0.04*m_sizeY,
			m_pageWidth,subHeight,a_value[3],3);
}

void Oscilloscope::scopeValue(Color a_value)
{
	const Real subHeight(0.15*m_sizeY);

	scopeValue(m_offsetX,m_offsetY+0.80*m_sizeY,
			m_pageWidth,subHeight,a_value[0],0);
	scopeValue(m_offsetX,m_offsetY+0.61*m_sizeY,
			m_pageWidth,subHeight,a_value[1],1);
	scopeValue(m_offsetX,m_offsetY+0.42*m_sizeY,
			m_pageWidth,subHeight,a_value[2],2);
	scopeValue(m_offsetX,m_offsetY+0.23*m_sizeY,
			m_pageWidth,subHeight,a_value[3],2);
	scopeValue(m_offsetX,m_offsetY+0.04*m_sizeY,
			m_pageWidth,subHeight,a_value);
}

void Oscilloscope::visualizePlane(Real a_offsetX,Real a_offsetY,
	Real a_width,Real a_height,I32 a_channel0,I32 a_channel1)
{
	const I32 historyCount=m_history[m_historyPage].size();
	if(!historyCount)
	{
		return;
	}

	const Real centerX(a_offsetX+0.5*a_width);
	const Real centerY(a_offsetY+0.5*a_height);

	Vector2f valueMin(0,0);
	Vector2f valueMax(0,0);

	for(I32 channelIndex=0;channelIndex<2;channelIndex++)
	{
		const I32 channel=channelIndex? a_channel1: a_channel0;

		valueMin[channelIndex]=m_history[m_historyPage][0][channel];
		valueMax[channelIndex]=m_history[m_historyPage][0][channel];

		for(I32 index=0;index<historyCount;index++)
		{
			const F64 historyValue=m_history[m_historyPage][index][channel];
			if(valueMin[channelIndex]>historyValue)
			{
				valueMin[channelIndex]=historyValue;
			}
			if(valueMax[channelIndex]<historyValue)
			{
				valueMax[channelIndex]=historyValue;
			}
		}
	}

	SpatialVector* point=new SpatialVector[historyCount];

	Vector2f rangeValue(valueMax-valueMin);
#if FE_OSC_VISUALIZATION_SQUARE_ASPECT
	if(rangeValue[0]<rangeValue[1])
	{
		rangeValue[0]=rangeValue[1];
	}
	else
	{
		rangeValue[1]=rangeValue[0];
	}
#endif

	for(I32 channelIndex=0;channelIndex<2;channelIndex++)
	{
		const I32 channel=channelIndex? a_channel1: a_channel0;
		const Real span=channelIndex? a_height: a_width;
		const Real center=channelIndex? centerY: centerX;

		const F64 centerValue=
				0.5*(valueMin[channelIndex]+valueMax[channelIndex]);
		const F64 scale=(rangeValue[channelIndex]>0.0)?
				span/rangeValue[channelIndex]: 0.0;

		for(I32 index=0;index<historyCount;index++)
		{
			const I32 reIndex=
					(-index+m_historyIndex+2*historyCount)%historyCount;
			const F64 historyValue=
					m_history[m_historyPage][reIndex][channel];

			point[index][channelIndex]=center+scale*(historyValue-centerValue);
		}
	}

	Color* colors=new Color[historyCount];

	for(I32 index=0;index<historyCount;index++)
	{
		set(colors[index],1.0-0.8*index/Real(historyCount-1),0.0,0.0);
	}

	m_spDrawI->pushDrawMode(m_spDrawText);

#if FE_OSC_VISUALIZATION_BG
	const Color dark(0.1,0.1,0.1);
	const Color clearColor=dark;

	SpatialVector rect[2];
	set(rect[0],a_offsetX,a_offsetY);
	set(rect[1],a_offsetX+a_width,a_offsetY+a_height);
	m_spDrawI->drawRectangles(rect,2,false,&clearColor);
#endif

	m_spDrawI->drawPoints(point,NULL,historyCount,true,colors);

	m_spDrawI->popDrawMode();

	delete[] colors;
	delete[] point;
}

void Oscilloscope::scopeValue(Real a_offsetX,Real a_offsetY,
	Real a_width,Real a_height,F64 a_value,I32 a_channel)
{
	const I32 indicatorWidth(20);

	const I32 maxDecimal(6);
	const F64 expGrain=pow(0.1,maxDecimal);

	const I32 rightEdge=m_offsetX+m_sizeX;
	if(a_offsetX+m_indicatorSpace>=rightEdge)
	{
		return;
	}

	const I32 fontHeight=m_spDrawI->font()->fontHeight(NULL,NULL);
	const I32 maxLines=1.0*a_height/fontHeight;		//* tweak

	const Real multiplication=m_spDrawI->multiplication();

	const Box2i scissor(Vector2i(m_offsetX,m_offsetY),
			Vector2i(m_sizeX,m_sizeY));
	sp<ViewI> spView=m_spDrawI->view();
	spView->setScissor(&scissor);

	const Color brightGreen(0.0,1.0,0.0);
	const Color dimGreen(0.0,1.0,0.0,0.6);
	const Color red(1.0,0.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color dark(0.1,0.1,0.1);
	const Color black(0.0,0.0,0.0);
	const Color clearColor=dark;
	const Color backColor=black;

	m_spDrawI->pushDrawMode(m_spDrawText);

#if FE_OSC_SCOPE_BG
	SpatialVector rect[2];
	set(rect[0],a_offsetX+m_indicatorSpace,a_offsetY);
	set(rect[1],a_offsetX+a_width,a_offsetY+a_height);
	m_spDrawI->drawRectangles(rect,2,false,&clearColor);
#endif

	const I32 centerY(a_offsetY+0.5*a_height);

	F64 grain(1);
	F64 digits=fe::maximum(F64(0),F64(log10(a_value)));
	I32 decimals(1);

	const I32 historyCount=m_history[m_historyPage].size();
	const I32 historyShort=FE_OSC_HISTORY_SHORT;

	if(historyCount)
	{
		F64 valueMin=m_history[m_historyPage][0][a_channel];
		F64 valueMax=m_history[m_historyPage][0][a_channel];

		for(I32 index=0;index<historyShort;index++)
		{
			const I32 reIndex=(m_historyIndex-index+
					2*historyCount)%historyCount;
			const F64 historyValue=
					m_history[m_historyPage][reIndex][a_channel];

			if(valueMin>historyValue)
			{
				valueMin=historyValue;
			}
			if(valueMax<historyValue)
			{
				valueMax=historyValue;
			}
		}

		const F64 grainBias(-2.0);		//* tweak
		const F64 flatLineSpan(3.0);	//* tweak

		if(valueMax<=valueMin)
		{
			valueMin-=0.5*flatLineSpan;
			valueMax+=0.5*flatLineSpan;
		}

		const F64 gainControl=m_warmup? 0.0: 0.997;	//* TODO tweak
		const F64 gainControl1=1.0-gainControl;
		F64& rLastValueMin=m_lastValueMin[m_historyPage][a_channel];
		F64& rLastValueMax=m_lastValueMax[m_historyPage][a_channel];
		if(valueMin>rLastValueMin)
		{
			valueMin=gainControl1*valueMin+gainControl*rLastValueMin;
		}
		if(valueMax<rLastValueMax)
		{
			valueMax=gainControl1*valueMax+gainControl*rLastValueMax;
		}

		rLastValueMin=valueMin;
		rLastValueMax=valueMax;

		const F64 valueLimit=fe::maximum(fabs(valueMin),fabs(valueMax));
		const F64 valueSpan=valueMax-valueMin;
		const F64 valueCenter=valueMin+0.5*valueSpan;
		const F64 graphScale=valueSpan>0.0? a_height/valueSpan: 0.0;

		digits=fe::maximum(F64(0),F64(log10(valueLimit)));

		grain=pow(10.0,I32(log10(valueSpan)+grainBias));
		I32 grainSteps=0;
		if(grain>0.0)
		{
			while(valueSpan/grain>maxLines)
			{
				grain*=((grainSteps++)%2)? 2.0: 5.0;
			}
		}

		decimals=(grain>0.0)? fe::maximum(F64(0),F64(0.999-log10(grain))): 0;

		if(a_offsetX<rightEdge)
		{
			SpatialVector gradLine[4];
			gradLine[0][0]=a_offsetX+m_indicatorSpace;
			gradLine[1][0]=gradLine[0][0]+FE_OSC_HISTORY_SHORT;
			gradLine[2][0]=gradLine[0][0];
			gradLine[3][0]=gradLine[1][0];

			if(valueSpan==0.0 || grain==0.0)
			{
				gradLine[0][1]=centerY;
				gradLine[1][1]=gradLine[0][1];

				m_spDrawI->drawLines(gradLine,NULL,2,
						DrawI::e_discrete,false,&red);

				String text;
				if(grain<expGrain)
				{
					text.sPrintf("%.*E",maxDecimal-3,valueMin);
				}
				else
				{
					text.sPrintf("%.*f",decimals,valueMin);
				}
				OperateCommon::drawLabel(m_spDrawI,
						gradLine[1]+SpatialVector(10,-0.5*fontHeight),
						text,FALSE,1,dimGreen,NULL,NULL);
			}
			else
			{
				const I32 lineLimit(1000);
				I32 lineCount(0);

				F64 graduationValue(I32((valueMin-grain)/grain)*grain);
				while(graduationValue<valueMax)
				{
					//* no negative zero
					if(graduationValue<0.0 && graduationValue>-0.1*grain)
					{
						graduationValue=0.0;
					}

					const F64 yValue=
							centerY+(graduationValue-valueCenter)*graphScale;

					if(yValue>=a_offsetY && yValue<a_offsetY+a_height)
					{
						gradLine[0][1]=yValue;
						gradLine[1][1]=yValue;

						m_spDrawI->drawLines(gradLine,NULL,2,
								DrawI::e_discrete,false,
								fabs(graduationValue)<0.1*grain?
								&dimGreen: &blue);

						String text;
						if(grain<=expGrain)
						{
							text.sPrintf("%.*E",maxDecimal-3,graduationValue);
						}
						else
						{
							text.sPrintf("%.*f",decimals,graduationValue);
						}
						OperateCommon::drawLabel(m_spDrawI,
								gradLine[1]+SpatialVector(10,-0.5*fontHeight),
								text,FALSE,1,dimGreen,NULL,NULL);
					}

					graduationValue+=grain;

					lineCount++;
					if(lineCount>=lineLimit)
					{
						feLog("Oscilloscope::scopeValue too many lines\n");
						break;
					}
				}
			}

			SpatialVector* line=new SpatialVector[historyShort];
			for(I32 index=0;index<historyShort;index++)
			{
//~				const I32 reIndex=
//~						(-index+m_historyIndex+2*historyCount)%historyCount;
				const I32 reIndex=(m_historyIndex-index+
						2*historyCount)%historyCount;
				const F64 historyValue=
						m_history[m_historyPage][reIndex][a_channel];
				line[index]=SpatialVector(
						a_offsetX+m_indicatorSpace+historyShort-1-index,
						centerY+(historyValue-valueCenter)*graphScale);
			}

			m_spDrawI->drawLines(line,NULL,historyShort,
					DrawI::e_strip,false,&red);

			delete[] line;
		}
	}

	spView->setScissor(NULL);


	const I32 chars=digits+(decimals? decimals+3: 2);

	Box2i box;
	set(box,a_offsetX,a_offsetY,indicatorWidth,a_height);
	OperateCommon::drawIndicator(m_spDrawI,box,TRUE,FALSE,a_value,
			0.0,grain,-decimals,chars,chars,multiplication,
			&brightGreen,&dimGreen,&backColor,&black);

	m_spDrawI->popDrawMode();
}


void Oscilloscope::scopeValue(Real a_offsetX,Real a_offsetY,
	Real a_width,Real a_height,Color a_value)
{
	const I32 rightEdge=m_offsetX+m_sizeX;
	if(a_offsetX+m_indicatorSpace>=rightEdge)
	{
		return;
	}

	const I32 historyCount=m_history[m_historyPage].size();
	if(!historyCount)
	{
		return;
	}

	const I32 historyShort=FE_OSC_HISTORY_SHORT;
	SpatialVector* line=new SpatialVector[historyShort*2];
	Color* colors=new Color[historyShort*2];
	for(I32 index=0;index<historyShort;index++)
	{
		const I32 reIndex=(historyShort-index+m_historyIndex+
				historyCount)%historyCount;
		const Color historyValue=m_history[m_historyPage][reIndex];

		colors[index*2]=historyValue;
		colors[index*2+1]=historyValue;

		line[index*2]=SpatialVector(a_offsetX+m_indicatorSpace+index,a_offsetY);
		line[index*2+1]=line[index*2]+SpatialVector(0,a_height);
	}

	m_spDrawI->drawLines(line,NULL,historyShort*2,
			DrawI::e_discrete,true,colors);

	delete[] colors;
	delete[] line;
}

} /* namespace ext */
} /* namespace fe */
