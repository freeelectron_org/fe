/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <draw/draw.pmh>

#define	FE_DTH_CACHE_DEBUG	FALSE

namespace fe
{
namespace ext
{

DrawThreaded::DrawThreaded(void):
	m_jobs(0),
	m_released(FALSE)
{
}

DrawThreaded::~DrawThreaded(void)
{
	if(m_spGang.isValid())
	{
#if FE_DTH_CACHE_DEBUG
		feLog("~DrawThreaded posting -1\n");
#endif

		m_spGang->post(-1);
		I32 job=0;
		while(job>=0)
		{
			m_spGang->waitForDelivery(job);

#if FE_DTH_CACHE_DEBUG
			feLog("~DrawThreaded job %d\n",job);
#endif
		}
	}
}

void DrawThreaded::initialize(void)
{
#if FE_OS == FE_OSX
	feX(e_cannotCreate,"DrawThreaded::initialize",
			"OSX threaded draw is broken");
#endif
	m_spGang=new Gang<RenderWorker,I32>();
	if(!m_spGang->start(sp<Counted>(this),1))
	{
		m_spGang=NULL;
		feX(e_cannotCreate,"DrawThreaded::initialize",
				"Gang failed to create thread");
	}
}

void DrawThreaded::flush(void)
{
	if(!m_released)
	{
		sp<WindowI> spWindowI=view()->window();
		spWindowI->releaseCurrent();
		m_released=TRUE;
	}

#if FE_DTH_CACHE_DEBUG
	feLog("[33mDrawThreaded::flush queue %d[0m\n",m_inputQueue);
#endif

	clearCameraMap();

	//* NOTE This presumes that jobs are rendered in order, so we don't need
	//* to track individual jobs, just the total currently running.

	m_spGang->post(m_inputQueue);
	m_jobs++;

	m_inputQueue=(m_inputQueue+1)%m_queues;

	I32 rendered=0;
	while(m_spGang->acceptDelivery(rendered))
	{
		m_jobs--;
	}

#if FE_DTH_CACHE_DEBUG
	feLog("[33mDrawThreaded::flush new queue %d jobs=%d[0m\n",
			m_inputQueue,m_jobs);

	BWORD first=TRUE;
#endif
	while(m_jobs==m_queues)
	{
#if FE_DTH_CACHE_DEBUG
		if(first)
		{
			feLog("[33mDrawThreaded::flush waiting on queue %d[0m\n",
					m_inputQueue);
			first=FALSE;
		}
#endif

		while(m_spGang->acceptDelivery(rendered))
		{
			m_jobs--;
		}

		//* TODO need formal thread wait
//		nanoSleep(0,100000);
	}
#if FE_DTH_CACHE_DEBUG
	if(!first)
	{
		feLog("[33mDrawThreaded::flush done waiting on queue %d[0m\n",
				m_inputQueue);
	}
#endif
}

void DrawThreaded::RenderWorker::operate(void)
{
#if FE_DTH_CACHE_DEBUG
	feLog("[32mDrawThreaded::RenderWorker::operator()"
			" %p Thread %d starting[0m\n",this,m_id);

	BWORD wait_message=TRUE;
#endif

	I32 job=0;
	while(TRUE)
	{
		FEASSERT(m_hpJobQueue.isValid());
		if(m_hpJobQueue->take(job))
		{
			if(job<0)
			{
#if FE_DTH_CACHE_DEBUG
				feLog("[32mDrawThreaded::RenderWorker::operator()"
						" %p Thread %d break[0m\n",this,m_id);
#endif
				m_hpJobQueue->deliver(job);
				break;
			}

#if FE_DTH_CACHE_DEBUG
			feLog("[32mDrawThreaded::RenderWorker::operator()"
					" %p Thread %d Job %d[0m\n",
					this,m_id,job);
#endif

			try
			{
				m_hpDrawThreaded->drawOutput(job,TRUE);
			}
			catch(Exception &e)
			{
				if(e.getResult()!=e_invalidHandle) { throw e; }
				feLog("[33mDrawThreaded::RenderWorker::operator()"
						" m_hpDrawThreaded invalid[0m\n\n");
			}

#if FE_DTH_CACHE_DEBUG
			feLog("[32mDrawThreaded::RenderWorker::operator()"
					" %p Thread %d Job %d finished[0m\n",
					this,m_id,job);
			wait_message=TRUE;
#endif

			m_hpJobQueue->deliver(job);
		}
		else
		{
#if FE_DTH_CACHE_DEBUG
			if(wait_message)
			{
				feLog("[32mDrawThreaded::RenderWorker::operator()"
						" %p Thread %d waiting[0m\n",this,m_id);
				wait_message=FALSE;
			}
#endif

			//* TODO need formal thread wait
//			nanoSleep(0,100000);
		}
	}
#if FE_DTH_CACHE_DEBUG
	feLog("[32mDrawThreaded::RenderWorker::operator()"
			" %p Thread %d leaving[0m\n",this,m_id);
#endif
}

} /* namespace ext */
} /* namespace fe */
