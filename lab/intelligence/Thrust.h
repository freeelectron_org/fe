/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Thrust_h__
#define __intelligence_Thrust_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operate thrusting abilities

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Thrust: virtual public HandlerI
{
	public:
				Thrust(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater						m_theaterRV;
		RecordArrayView<Thruster>	m_thrusterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Thrust_h__ */
