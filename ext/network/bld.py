import os
import sys
import string
forge = sys.modules["forge"]

def prerequisites():
    return [    "signal" ]

def setup(module):
    srclist = [ "network.pmh",
                "ConnectedCatalog",
                "MessageSignaler",
                "SignalMessenger",
                "networkDL" ]

    dll = module.DLL( "fexNetworkDL", srclist)

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "feDataLib",
                "fexSignalLib" ]

    forge.deps( ["fexNetworkDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexNetworkDL",             None,       None) ]

    module.Module('test')
