/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_RampI_h__
#define __shape_RampI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief A function giving a Real result for a Real input

	Convention suggests a fairly strict domain of 0 to 1
	and a usual range of 0 to 1.
	An implementation is not required to enforce such limits.

	@ingroup shape
*//***************************************************************************/
class FE_DL_EXPORT RampI:
	virtual public Component,
	public CastableAs<RampI>
{
	public:
virtual	U32		entryCount(void) const										=0;
virtual	Vector2	entry(U32 a_index) const									=0;

virtual	Real	eval(Real a_u) const										=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_RampI_h__ */
