/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawAtoms.h"

namespace fe
{
namespace ext
{

DrawAtoms::DrawAtoms(void)
{
}

DrawAtoms::~DrawAtoms(void)
{
}

void DrawAtoms::initialize(void)
{
	m_mode = e_sphere;

	cfg<String>("mode") = "sphere";
}

void DrawAtoms::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group
	sp<RecordGroup> rg_selected =
		cfg< sp<RecordGroup> >("selected"); // selection group
	sp<RecordGroup> rg_marked =
		cfg< sp<RecordGroup> >("marked"); // marked group

	feAssert(rg_input.isValid(), "DrawAtoms requires valid input group");

	String mode = cfg<String>("mode");
	if(mode == "point")
	{
		m_mode = e_point;
	}
	else if(mode == "circle")
	{
		m_mode = e_circle;
	}
	else if(mode == "sphere")
	{
		m_mode = e_sphere;
	}
	else
	{
		m_mode = e_none;
	}

	const Color color0 = m_drawview.getColor("theme0",
		Color(0.0f,0.0f,1.0f,1.0f));
	const Color color1 = m_drawview.getColor("theme1",
		Color(1.0f,0.0f,0.0f,1.0f));
	const Color color2 = m_drawview.getColor("theme2",
		Color(1.0f,1.0f,0.0f,1.0f));

	SpatialTransform transform;

	std::vector<SpatialVector> points;
	std::vector<SpatialVector> sels;

	hp<DrawI> spDraw(m_drawview.drawI());

	sp<DrawMode> outline(new DrawMode());
	outline->setDrawStyle(DrawMode::e_wireframe);
	outline->setAntialias(true);
	outline->setLineWidth(1.5);
	outline->setBackfaceCulling(false);
	outline->setTwoSidedLighting(true);
	outline->setLit(false);

	// screen-align transformation (billboard)
	SpatialVector zero(0.0,0.0,0.0);
#if 0
	SpatialVector v[3];
	v[0] = spDraw->view()->unproject(0.0, 0.0, 0.0);
	v[1] = spDraw->view()->unproject(1.0, 0.0, 0.0);
	v[2] = spDraw->view()->unproject(0.0, 1.0, 0.0);
	v[1] -= v[0];
	v[2] -= v[0];
	cross3(v[0], v[2], v[1]);
	cross3(v[1], v[2], v[0]);
	if(v[0] == zero || v[1] == zero || v[2] == zero)
	{
		v[1] = SpatialVector(1.0,0.0,0.0);
		v[2] = SpatialVector(0.0,1.0,0.0);
		v[0] = SpatialVector(0.0,0.0,1.0);
	}
	else
	{
		normalize(v[0]);
		normalize(v[1]);
		normalize(v[2]);
	}
	SpatialTransform rotation;
	setColumn(0, rotation, v[1]);
	setColumn(1, rotation, v[2]);
	setColumn(2, rotation, v[0]);
#endif

	SpatialTransform rotation;
	spDraw->view()->screenAlignment(rotation);

	spDraw->pushDrawMode(outline);
	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asParticle.bind(spRA->layout()->scope());
		m_asLabeled.bind(spRA->layout()->scope());
		m_asBounded.bind(spRA->layout()->scope());
		m_asSelectable.bind(spRA->layout()->scope());
		m_asColored.bind(spRA->layout()->scope());
		if(m_asParticle.location.check(spRA) && m_asBounded.radius.check(spRA))
		{
			bool colored = false;
			if(m_asColored.check(spRA))
			{
				colored = true;
			}
			Real r;
			SpatialVector scale;
			if(m_asSelectable.is.check(spRA) && rg_selected.isValid()
				&& rg_marked.isValid())
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					Record r_item = spRA->getRecord(i);
					SpatialVector location = m_asParticle.location(spRA,i);
					switch(m_mode)
					{
						case e_circle:
							r = m_asBounded.radius(spRA,i);
							set(scale,r,r,r);
							transform = SpatialTransform::identity();
							transform = rotation;
							setTranslation(transform, location);

							if(rg_selected->find(r_item))
							{
								spDraw->drawCircle(transform,&scale,color1);
							}
							else if(rg_marked->find(r_item))
							{
								spDraw->drawCircle(transform,&scale,color2);
							}
							else
							{
								spDraw->drawCircle(transform,&scale,color0);
							}
							break;
						case e_sphere:
							r = m_asBounded.radius(spRA,i);
							set(scale,r,r,r);
							transform = SpatialTransform::identity();
							translate(transform, location);
							if(rg_selected->find(r_item))
							{
								spDraw->drawSphere(transform,&scale,color1);
							}
							else if(rg_marked->find(r_item))
							{
								spDraw->drawSphere(transform,&scale,color2);
							}
							else
							{
								if(colored)
								{
									spDraw->drawSphere(transform,&scale,
										m_asColored.color(spRA,i));
								}
								else
								{
									spDraw->drawSphere(transform,&scale,color0);
								}
							}
							break;
						case e_point:
							if(rg_selected->find(r_item))
							{
								sels.push_back(location);
							}
							else
							{
								points.push_back(location);
							}
							break;
						default:
							break;
					}
				}
			}
			else
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					SpatialVector location = m_asParticle.location(spRA,i);
					switch(m_mode)
					{
						case e_circle:
							r = m_asBounded.radius(spRA,i);
							set(scale,r,r,r);
							transform = rotation;
							setTranslation(transform, location);
							spDraw->drawCircle(transform,&scale,color0);
							break;
						case e_sphere:
							r = m_asBounded.radius(spRA,i);
							set(scale,r,r,r);
							transform = SpatialTransform::identity();
							translate(transform, location);
							if(colored)
							{
								spDraw->drawSphere(transform,&scale,
									m_asColored.color(spRA,i));
							}
							else
							{
								spDraw->drawSphere(transform,&scale,color0);
							}
							break;
						case e_point:
							points.push_back(location);
							break;
						default:
							break;
					}
				}
			}
			if(m_asLabeled.check(spRA))
			{
				SpatialVector offset(0.0,0.0,0.05);
				for(int i = 0; i < spRA->length(); i++)
				{
					spDraw->drawAlignedText(m_asParticle.location(spRA,i)+offset,
						m_asLabeled.label(spRA,i).c_str(), color0);
				}
			}
		}
	}
	spDraw->popDrawMode();
	if(points.size() > 0)
	{
		// assumes contiguous std::vector layout.  Not sure that's always valid
		if(points.size())
		{
			spDraw->drawPoints(&points.front(), NULL, points.size(),
					false, &color0);
		}
		if(sels.size())
		{
			spDraw->drawPoints(&sels.front(), NULL, sels.size(), false, &color1);
		}
	}

}

void DrawAtoms::setMode(t_mode a_mode)
{
	m_mode = a_mode;
}

bool DrawAtoms::call(const String &a_name, std::vector<Instance> a_argv)
{
	return true;
}


} /* namespace ext */
} /* namespace fe */


