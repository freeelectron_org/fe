/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __image_h__
#define __image_h__

#include "fe/plugin.h"
#include "math/math.h"

#include "image/ImageI.h"

#endif /* __image_h__ */
