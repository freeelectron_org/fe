/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Accessor_h__
#define __data_Accessor_h__

#if FE_CODEGEN<FE_DEBUG
#define FE_ATTRIBUTE_DEBUG
#define FE_ACCESSOR_DEBUG
#endif

namespace fe
{

/** @brief Type inspecific Accessor.

	An accessor is a type of functor that is bound to a particular attribute.
	It is useful for attribute specific operations, most notably accessing a
	attribute within a record.  An accessor can also be used to check if a
	attribute exists in a Record, RecordArray, Bag, or Layout.  An accessor is
	also a convienient way to setup attributes in a Scope.
	*/
class FE_DL_EXPORT BaseAccessor
{
	public:
		class FE_DL_PUBLIC Exception : public ::fe::Exception
		{
			public:
				Exception(const char *a_message) : ::fe::Exception(a_message) {}
		};

		BaseAccessor(void);
		~BaseAccessor(void);
		/**	initialize functions setup the accessor but do not setup anything
			with the Scope. */
		void			initialize(	sp<Scope> scope,
									const String &attribute);
		/**	initialize functions setup the accessor but do not setup anything
			with the Scope. */
		void			initialize(	Scope *pScope,
									const String &attribute);
		/**	initialize functions setup the accessor but do not setup anything
			with the Scope. */
		void			initialize(		sp<Scope> scope,
										sp<Attribute> spAttribute);
		/** setup functions setup the accessor and also setup the Scope. */
		void			setup(		sp<Scope> spScope,
									const String &attribute);
		/** setup functions setup the accessor and also setup the Scope. */
		void			setup(		sp<Scope> scope,
									const String &attribute,
									const String &attributetype);
		/** setup the accessor and populate the named Layout,
			creating the Layout if neccessary */
		void			populate(		sp<Scope> spScope,
									const String &layout,
									const String &attribute);

		sp<Scope>		scope(void) const;

		bool			operator==(const BaseAccessor &other) const;
		bool			operator<(const BaseAccessor &other) const;

		BaseAccessor	&operator=(const BaseAccessor &other);
						operator FE_UWORD() const;
		FE_UWORD			index(void) const;


		/** Return true if @em record has attribute */
		bool			check(const RecordSB& r) const;
		/** Return true if @em record has attribute */
		bool			check(const WeakRecordSB& r) const;
		/** Return true if @em record has attribute */
		bool			check(RecordSB *pR) const;
		/** Return true if @em spRA has attribute */
		bool			check(sp<RecordArraySB>& rspRA) const;
		/** Return true if @em rRA has attribute */
		bool			check(RecordArraySB &rRA) const;
		/** Return true if @em spL has attribute */
		bool			check(sp<LayoutSB>& rspL) const;
		/** Return true if @em L has attribute */
		bool			check(LayoutSB &rL) const;

		/** Return true if @em record has attribute */
		bool			check(const RecordAV& r) const;
		/** Return true if @em record has attribute */
		bool			check(const WeakRecordAV& r) const;
		/** Return true if @em record has attribute */
		bool			check(RecordAV *pR) const;
		/** Return true if @em spRA has attribute */
		bool			check(sp<RecordArrayAV>& rspRA) const;
		/** Return true if @em rRA has attribute */
		bool			check(RecordArrayAV &rRA) const;
		/** Return true if @em spL has attribute */
		bool			check(sp<LayoutAV>& rspL) const;
		/** Return true if @em L has attribute */
		bool			check(LayoutAV &rL) const;

		bool			check(sp<Layout>& rspL) const;
		bool			check(Layout &rL) const;


		/** Return the attribute this accessor is for */
		sp<Attribute>	attribute(void) const;
		const String	&name(void) const;

		/** optionally bind to a record */
		void			bind(const WeakRecord &a_record) { m_optional_record = a_record; }

		void			typeCheck(const String &attribute);

	protected:
		FE_UWORD			m_index;
		hp<Scope>			m_hpScope;
		TypeInfo			m_typeInfo;
		WeakRecord			m_optional_record;
};


/** @brief The main data access class for the data system.

	@ingroup data
	*/
template <class T>
class FE_DL_EXPORT Accessor : public BaseAccessor
{
	public:
		Accessor(void);
		Accessor(sp<Scope> scope, const String &attribute);
		~Accessor(void);

		Accessor(const Accessor<T> &other);

		Accessor<T>	&operator=(const Accessor<T> &other);


		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(const RecordSB& r);
		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const RecordSB& r) const;
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(const WeakRecordSB &r);
		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const WeakRecordSB &r) const;
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(RecordSB *record);
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(sp<RecordArraySB>& rspRA, FE_UWORD index);
		/** Return the attribute.  Checking for existence is not done */
const	T			&operator()(sp<RecordArraySB>& rspRA, FE_UWORD index) const;
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(hp<RecordArraySB>& rhpRA, FE_UWORD index);

		/** Return the attribute.  Checking for existence is done.
			If the attribute does not exist then an Exception is thrown */
		T			&operator()(const RecordSB& r, const char *message);

		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const RecordSB& r, T *&data);
		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const WeakRecordSB r, T *&data);
		T			*queryAttribute(const RecordSB& r);
		/** Return a pointer to the attribute if it exists in @em record.
			Otherwise return NULL */
		T			*queryAttribute(const WeakRecordSB r);
		/** Return a pointer to the attribute if it exists in @em record.
			Otherwise return NULL */
		T			*queryAttribute(RecordSB *pR);
		/** Return a pointer to the attribute if it exists in @em record
			indexed into the RecordArraySB.
			Otherwise return NULL */
		T			*queryAttribute(sp<RecordArraySB>& rspRA, FE_UWORD index);

		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(const RecordAV& r);
		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const RecordAV& r) const;
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(const WeakRecordAV &r);
		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const WeakRecordAV &r) const;
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(RecordAV *record);
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(sp<RecordArrayAV>& rspRA, FE_UWORD index);
		/** Return the attribute.  Checking for existence is not done */
const	T			&operator()(sp<RecordArrayAV>& rspRA, FE_UWORD index) const;
		/** Return the attribute.  Checking for existence is not done */
		T			&operator()(hp<RecordArrayAV>& rhpRA, FE_UWORD index);

		/** Return the attribute.  Checking for existence is done.
			If the attribute does not exist then an Exception is thrown */
		T			&operator()(const RecordAV& r, const char *message);

		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const RecordAV& r, T *&data);
		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const WeakRecordAV r, T *&data);
		T			*queryAttribute(const RecordAV& r);
		/** Return a pointer to the attribute if it exists in @em record.
			Otherwise return NULL */
		T			*queryAttribute(const WeakRecordAV r);
		/** Return a pointer to the attribute if it exists in @em record.
			Otherwise return NULL */
		T			*queryAttribute(RecordAV *pR);
		/** Return a pointer to the attribute if it exists in @em record
			indexed into the RecordArrayAV.
			Otherwise return NULL */
		T			*queryAttribute(sp<RecordArrayAV>& rspRA, FE_UWORD index);

		bool		operator()(const RecordAV& record, T &data);
		bool		operator()(const WeakRecordAV& record, T &data);
		bool		operator()(const RecordSB& record, T &data);
		bool		operator()(const WeakRecordSB& record, T &data);

		void		registerDefault(T *pT);

		T			*operator()(const sp<LayoutAV> &spLayout);

		Accessor<T> &operator=(const T &a_value)
		{
			(*this)(BaseAccessor::m_optional_record) = a_value;
			return *this;
		}
		T	*operator*(void)
		{
			return &((*this)(BaseAccessor::m_optional_record));
		}
		operator T&()
		{
			return (*this)(BaseAccessor::m_optional_record);
		}
		T &c_val(void)
		{
			return (*this)(BaseAccessor::m_optional_record);
		}

	private:
		T			*m_pT;
};

template <>
class FE_DL_EXPORT Accessor<void> : public BaseAccessor
{
	public:
		Accessor(void)
		{
			m_typeInfo = TypeInfo(getTypeId<void>());
		}
		Accessor(sp<Scope> scope, const String &attribute)
		{
			m_typeInfo = TypeInfo(getTypeId<void>());
			setup(scope, attribute);
		}
		~Accessor(void)
		{
		}
};


/*****************************************************************************
IMPLEMENTATION
*****************************************************************************/


inline bool BaseAccessor::operator==(const BaseAccessor &other) const
{
	if(m_index != other.m_index) { return false; }
	if(!(m_hpScope == other.m_hpScope)) { return false; }
	return true;
}

inline bool BaseAccessor::operator<(const BaseAccessor &other) const
{
	return (m_index < other.m_index);
}


inline BaseAccessor::operator FE_UWORD() const
{
	return m_index;
}

inline FE_UWORD BaseAccessor::index(void) const
{
	return m_index;
}

#ifdef FE_ATTRIBUTE_DEBUG
#define FE_SCOPE_CHECK(s)													\
		if(!m_hpScope.isValid())											\
		{																	\
			feX("FE_SCOPE_CHECK",											\
			"Accessor for \'%s\' is not initialized",						\
					s->attribute(m_index)->name().c_str());					\
		}																	\
		if(s != m_hpScope)													\
		{																	\
			feX("FE_SCOPE_CHECK",											\
			"Accessor for \'%s\' is not bound to the same scope as Record",	\
					m_hpScope->attribute(m_index)->name().c_str());			\
		}
#else
#define FE_SCOPE_CHECK(s) /* NOOP */
#endif

inline bool BaseAccessor::check(const RecordSB& record) const
{
	FE_SCOPE_CHECK(record.layout()->scope());
	return record.layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(const WeakRecordSB& record) const
{
	FE_SCOPE_CHECK(record.layout()->scope());
	return record.layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(RecordSB *record) const
{
	FE_SCOPE_CHECK(record->layout()->scope());
	return record->layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(sp<RecordArraySB>& rspRA) const
{
	FE_SCOPE_CHECK(rspRA->layout()->scope());
	return rspRA->layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(RecordArraySB &rRA) const

{
	FE_SCOPE_CHECK(rRA.layout()->scope());
	return rRA.layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(sp<LayoutSB>& rspL) const
{
	FE_SCOPE_CHECK(rspL->scope());
	return rspL->checkAttribute(m_index);
}

inline bool BaseAccessor::check(LayoutSB &rL) const
{
	FE_SCOPE_CHECK(rL.scope());
	return rL.checkAttribute(m_index);
}

inline bool BaseAccessor::check(sp<LayoutAV>& rspL) const
{
	FE_SCOPE_CHECK(rspL->scope());
	return rspL->checkAttribute(m_index);
}

inline bool BaseAccessor::check(LayoutAV &rL) const
{
	FE_SCOPE_CHECK(rL.scope());
	return rL.checkAttribute(m_index);
}

inline bool BaseAccessor::check(sp<Layout>& rspL) const
{
	FE_SCOPE_CHECK(rspL->scope());
	return rspL->checkAttribute(m_index);
}

inline bool BaseAccessor::check(Layout &rL) const
{
	FE_SCOPE_CHECK(rL.scope());
	return rL.checkAttribute(m_index);
}

inline bool BaseAccessor::check(const RecordAV& record) const
{
	FE_SCOPE_CHECK(record.layout()->scope());
	return record.layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(const WeakRecordAV& record) const
{
	FE_SCOPE_CHECK(record.layout()->scope());
	return record.layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(RecordAV *record) const
{
	FE_SCOPE_CHECK(record->layout()->scope());
	return record->layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(sp<RecordArrayAV>& rspRA) const
{
	FE_SCOPE_CHECK(rspRA->layout()->scope());
	return rspRA->layout()->checkAttribute(m_index);
}

inline bool BaseAccessor::check(RecordArrayAV &rRA) const

{
	FE_SCOPE_CHECK(rRA.layout()->scope());
	return rRA.layout()->checkAttribute(m_index);
}


template <class T>
inline Accessor<T>::Accessor(void)
{
	m_pT = NULL;
	m_typeInfo = TypeInfo(getTypeId<T>());
}

template <class T>
inline Accessor<T>::Accessor(sp<Scope> scope, const String &attribute)
{
	m_pT = NULL;
	m_typeInfo = TypeInfo(getTypeId<T>());
	setup(scope, attribute);
}

template <class T>
inline Accessor<T>::~Accessor(void)
{
}

#ifdef FE_ATTRIBUTE_DEBUG
#define FE_ATTRIBUTE_CHECK(r)												\
		if(!check(r))														\
		{																	\
			feX("FE_ATTRIBUTE_CHECK",										\
					"invalid access: \'%s\' is not in \'%s\'",				\
					m_hpScope->attribute(m_index)->name().c_str(),			\
					r.layout()->name().c_str());							\
		}
#else
#define FE_ATTRIBUTE_CHECK(r) /* NOOP */
#endif

#ifdef FE_ATTRIBUTE_DEBUG
#define FE_ATTRIBUTE_TYPECHECK(r)											\
	sp<TypeMaster> spTM = m_hpScope->typeMaster();							\
	sp<BaseType> spType = spTM->lookupType(TypeInfo(getTypeId<T>()));		\
	if(spType != m_hpScope->attribute(m_index)->type())						\
	{																		\
		feX("FE_ATTRIBUTE_TYPECHECK",										\
			"accessor on \'%s\' type mismatch \'%s\' is not \'%s\'\n",		\
			m_hpScope->attribute(m_index)->name().c_str(),					\
			spTM->reverseLookup(spType).c_str(),							\
			spTM->reverseLookup(m_hpScope->attribute(m_index)->type()).		\
			c_str());														\
	}
#else
#define FE_ATTRIBUTE_TYPECHECK(r) /* NOOP */
#endif

template <class T>
inline Accessor<T>::Accessor(const Accessor<T> &other)
{
	if(&other != this)
	{
		m_index = other.m_index;
		m_hpScope = other.m_hpScope;
		m_typeInfo = other.m_typeInfo;
		m_pT = other.m_pT;
	}
}

template <class T>
inline Accessor<T> &Accessor<T>::operator=(const Accessor<T> &other)
{
	if(&other != this)
	{
		m_index = other.m_index;
		m_hpScope = other.m_hpScope;
		m_typeInfo = other.m_typeInfo;
		m_pT = other.m_pT;
	}
	return *this;
}

template <class T>
inline T &Accessor<T>::operator()(const RecordSB& record)
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);

	return record.accessAttribute<T>(m_index);
}

template <class T>
inline const T Accessor<T>::operator()(const RecordSB& record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline T &Accessor<T>::operator()(const WeakRecordSB &record)
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline const T Accessor<T>::operator()(const WeakRecordSB &record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline T &Accessor<T>::operator()(RecordSB *record)
{
	FE_ATTRIBUTE_CHECK((*record));
	FE_ATTRIBUTE_TYPECHECK(*record);
	return record->accessAttribute<T>(m_index);
}


template <class T>
inline T &Accessor<T>::operator()(const RecordSB& record, const char *message)
{
	if(!record.isValid())
	{
		feX(e_invalidPointer,
			(message!=NULL)?message:"",
			"invalid record in access of %s",
			m_hpScope->attribute(m_index)->name().c_str());
	}
	if(!check(record))
	{
		feX(e_cannotFind,
			(message!=NULL)?message:"",
			"%s not found in layout %s",
			m_hpScope->attribute(m_index)->name().c_str(),
			record.layout()->name().c_str());
	}
	return record.accessAttribute<T>(m_index);
}


template <class T>
inline const T &Accessor<T>::operator()(sp<RecordArraySB>& rspRA, FE_UWORD index) const
{
	FE_ATTRIBUTE_CHECK((*rspRA));
	return rspRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline T &Accessor<T>::operator()(sp<RecordArraySB>& rspRA, FE_UWORD index)
{
	FE_ATTRIBUTE_CHECK((*rspRA));
	return rspRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline T &Accessor<T>::operator()(hp<RecordArraySB>& rhpRA, FE_UWORD index)
{
	FE_ATTRIBUTE_CHECK((*rhpRA));
	return rhpRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline bool Accessor<T>::queryAttribute(const RecordSB& record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline bool Accessor<T>::queryAttribute(const WeakRecordSB record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline T *Accessor<T>::queryAttribute(const RecordSB& record)
{
	if(!check(record))
	{
		return m_pT;
	}
	return &(operator()(record));
}

template <class T>
inline T *Accessor<T>::queryAttribute(const WeakRecordSB record)
{
	if(!check(record))
	{
		return m_pT;
	}
	return &(operator()(record));
}

template <class T>
inline T *Accessor<T>::queryAttribute(sp<RecordArraySB>& rspRA, FE_UWORD index)
{
	if(!check(rspRA))
	{
		return m_pT;
	}
	return &(operator()(rspRA,index));
}

template <class T>
inline T *Accessor<T>::queryAttribute(RecordSB *record)
{
	if(!check(record))
	{
		return m_pT;
	}
	return &(operator()(record));
}

#if 1
template <class T>
inline	T			*Accessor<T>::operator()(const sp<LayoutAV> &spLayout)
{
	return (T *)(spLayout->baseOfAV(m_index));
}


template <class T>
inline T &Accessor<T>::operator()(const RecordAV& record)
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);

	return record.accessAttribute<T>(m_index);
}

template <class T>
inline const T Accessor<T>::operator()(const RecordAV& record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline T &Accessor<T>::operator()(const WeakRecordAV &record)
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline const T Accessor<T>::operator()(const WeakRecordAV &record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline T &Accessor<T>::operator()(RecordAV *record)
{
	FE_ATTRIBUTE_CHECK((*record));
	FE_ATTRIBUTE_TYPECHECK(*record);
	return record->accessAttribute<T>(m_index);
}

template <class T>
inline bool Accessor<T>::operator()(const RecordSB& record, T &data)
{
	if(!check(record))
	{
		return false;
	}
	data = (operator()(record));
	return true;
}

template <class T>
inline bool Accessor<T>::operator()(const WeakRecordSB& record, T &data)
{
	if(!check(record))
	{
		return false;
	}
	data = (operator()(record));
	return true;
}

template <class T>
inline T &Accessor<T>::operator()(const RecordAV& record, const char *message)
{
	if(!record.isValid())
	{
		feX(e_invalidPointer,
			(message!=NULL)?message:"",
			"invalid record in access of %s",
			m_hpScope->attribute(m_index)->name().c_str());
	}
	if(!check(record))
	{
		feX(e_cannotFind,
			(message!=NULL)?message:"",
			"%s not found in layout %s",
			m_hpScope->attribute(m_index)->name().c_str(),
			record.layout()->name().c_str());
	}
	return record.accessAttribute<T>(m_index);
}


template <class T>
inline const T &Accessor<T>::operator()(sp<RecordArrayAV>& rspRA, FE_UWORD index) const
{
	FE_ATTRIBUTE_CHECK((*rspRA));
	return rspRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline T &Accessor<T>::operator()(sp<RecordArrayAV>& rspRA, FE_UWORD index)
{
	FE_ATTRIBUTE_CHECK((*rspRA));
	return rspRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline T &Accessor<T>::operator()(hp<RecordArrayAV>& rhpRA, FE_UWORD index)
{
	FE_ATTRIBUTE_CHECK((*rhpRA));
	return rhpRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline bool Accessor<T>::queryAttribute(const RecordAV& record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline bool Accessor<T>::queryAttribute(const WeakRecordAV record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline T *Accessor<T>::queryAttribute(const RecordAV& record)
{
	if(!check(record))
	{
		return m_pT;
	}
	return &(operator()(record));
}

template <class T>
inline T *Accessor<T>::queryAttribute(const WeakRecordAV record)
{
	if(!check(record))
	{
		return m_pT;
	}
	return &(operator()(record));
}

#if 1
template <class T>
inline T *Accessor<T>::queryAttribute(sp<RecordArrayAV>& rspRA, FE_UWORD index)
{
	if(!check(rspRA))
	{
		return m_pT;
	}
	return &(operator()(rspRA,index));
}
#endif

template <class T>
inline T *Accessor<T>::queryAttribute(RecordAV *record)
{
	if(!check(record))
	{
		return m_pT;
	}
	return &(operator()(record));
}
#endif

template <class T>
inline void Accessor<T>::registerDefault(T *pT)
{
	m_pT = pT;
}

template <class T>
class FE_DL_EXPORT AccessorReadOnly : public BaseAccessor
{
	public:
		AccessorReadOnly(void);
		AccessorReadOnly(sp<Scope> scope, const String &attribute);
		~AccessorReadOnly(void);

		AccessorReadOnly(const Accessor<T> &other);

		AccessorReadOnly<T>	&operator=(const AccessorReadOnly<T> &other);


		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const RecordSB& r) const;
		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const WeakRecordSB &r) const;
		/** Return the attribute.  Checking for existence is not done */
const	T			&operator()(sp<RecordArraySB>& rspRA, FE_UWORD index) const;

		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const RecordSB& r, T *&data);
		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const WeakRecordSB r, T *&data);

		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const RecordAV& r) const;
		/** Return the attribute.  Checking for existence is not done */
const	T			operator()(const WeakRecordAV &r) const;
		/** Return the attribute.  Checking for existence is not done */
const	T			&operator()(sp<RecordArrayAV>& rspRA, FE_UWORD index) const;

		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const RecordAV& r, T *&data);
		/** Return true if the attribute exists in @em record.  Return the
			attribute data in @data */
		bool		queryAttribute(const WeakRecordAV r, T *&data);

		bool		operator()(const RecordAV& record, T &data);
		bool		operator()(const WeakRecordAV& record, T &data);
		bool		operator()(const RecordSB& record, T &data);
		bool		operator()(const WeakRecordSB& record, T &data);

		void		registerDefault(T *pT);

	private:
		T			*m_pT;
};

template <class T>
inline AccessorReadOnly<T>::AccessorReadOnly(void)
{
	m_pT = NULL;
	m_typeInfo = TypeInfo(getTypeId<T>());
}

template <class T>
inline AccessorReadOnly<T>::AccessorReadOnly(sp<Scope> scope, const String &attribute)
{
	m_pT = NULL;
	m_typeInfo = TypeInfo(getTypeId<T>());
	setup(scope, attribute);
}

template <class T>
inline AccessorReadOnly<T>::~AccessorReadOnly(void)
{
}

template <class T>
inline AccessorReadOnly<T>::AccessorReadOnly(const Accessor<T> &other)
{
	if(&other != this)
	{
		m_index = other.m_index;
		m_hpScope = other.m_hpScope;
		m_typeInfo = other.m_typeInfo;
		m_pT = other.m_pT;
	}
}

template <class T>
inline AccessorReadOnly<T>& AccessorReadOnly<T>::operator=(
	const AccessorReadOnly<T>& other)
{
	if(&other != this)
	{
		m_index = other.m_index;
		m_hpScope = other.m_hpScope;
		m_typeInfo = other.m_typeInfo;
		m_pT = other.m_pT;
	}
	return *this;
}

template <class T>
inline const T AccessorReadOnly<T>::operator()(const RecordSB& record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline const T AccessorReadOnly<T>::operator()(const WeakRecordSB &record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline const T &AccessorReadOnly<T>::operator()(sp<RecordArraySB>& rspRA, FE_UWORD index) const
{
	FE_ATTRIBUTE_CHECK((*rspRA));
	return rspRA->accessAttribute<T>(m_index, index);
}

template <class T>
inline bool AccessorReadOnly<T>::queryAttribute(const RecordSB& record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline bool AccessorReadOnly<T>::queryAttribute(const WeakRecordSB record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline const T AccessorReadOnly<T>::operator()(const RecordAV& record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}


template <class T>
inline const T AccessorReadOnly<T>::operator()(const WeakRecordAV &record) const
{
	FE_ATTRIBUTE_CHECK(record);
	FE_ATTRIBUTE_TYPECHECK(record);
	return record.accessAttribute<T>(m_index);
}

template <class T>
inline bool AccessorReadOnly<T>::operator()(const RecordSB& record, T &data)
{
	if(!check(record))
	{
		return false;
	}
	data = (operator()(record));
	return true;
}

template <class T>
inline bool AccessorReadOnly<T>::operator()(const WeakRecordSB& record, T &data)
{
	if(!check(record))
	{
		return false;
	}
	data = (operator()(record));
	return true;
}


template <class T>
inline const T &AccessorReadOnly<T>::operator()(sp<RecordArrayAV>& rspRA, FE_UWORD index) const
{
	FE_ATTRIBUTE_CHECK((*rspRA));
	return rspRA->accessAttribute<T>(m_index, index);
}


template <class T>
inline bool AccessorReadOnly<T>::queryAttribute(const RecordAV& record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}

template <class T>
inline bool AccessorReadOnly<T>::queryAttribute(const WeakRecordAV record, T *&data)
{
	if(!check(record))
	{
		return false;
	}
	data = &(operator()(record));
	return true;
}


template <class T>
inline void AccessorReadOnly<T>::registerDefault(T *pT)
{
	m_pT = pT;
}

template <class T>
class FE_DL_EXPORT AccessorW : public Accessor<T> { };

template <class T>
class FE_DL_EXPORT AccessorR : public AccessorReadOnly<T> { };

} /* namespace */

#endif /* __data_Accessor_h__ */

