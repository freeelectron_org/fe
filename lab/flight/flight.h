/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_flight_h__
#define __flight_flight_h__

#include "fe/plugin.h"

#include "flight/FlightMissionI.h"

#endif /* __flight_flight_h__ */

