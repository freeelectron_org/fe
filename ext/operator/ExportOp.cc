/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_EXO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void ExportOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("format")="auto";
	catalog<String>("format","label")="Format";
	catalog<String>("format","choice:0")="auto";
	catalog<String>("format","label:0")="Auto";
	catalog<String>("format","choice:1")="geo";
	catalog<String>("format","label:1")="Geo";
	catalog<String>("format","choice:2")="json";
	catalog<String>("format","label:2")="JSON";
	catalog<String>("format","choice:3")="rg";
	catalog<String>("format","label:3")="RecordGroup";
	catalog<String>("format","choice:4")="yaml";
	catalog<String>("format","label:4")="YAML";

	catalog<String>("savefile")="";
	catalog<String>("savefile","label")="Save File";
	catalog<String>("savefile","suggest")="filename";

	catalog<Real>("frame")=0.0;
	catalog<Real>("frame","high")=1e3;
	catalog<Real>("frame","max")=1e6;
	catalog<String>("frame","label")="Frame";

	catalog<String>("options")="";
	catalog<String>("options","label")="Options";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","temporal")=true;
}

void ExportOp::handle(Record& a_rSignal)
{
#if FE_EXO_DEBUG
	feLog("ExportOp::handle node \"%s\"\n",name().c_str());
#endif

	const String savefile=catalog<String>("savefile");
	const String options=catalog<String>("options");

	if(savefile.empty())
	{
#if FE_EXO_DEBUG
		feLog("ExportOp::handle savefile empty\n");
#endif

		catalog<String>("summary")="<no filename>";

		return;
	}

	catalog<String>("summary")="<failed>";

	String format=catalog<String>("format");
	if(format=="auto")
	{
		format=String(strrchr(savefile.c_str(),'.')).prechop(".");
	}

	const BWORD quiet=TRUE;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spSaveAccessible=registry()->create(
			"SurfaceAccessibleI.*.*."+format,quiet);
	if(spSaveAccessible.isNull())
	{
		spSaveAccessible=registry()->create("SurfaceAccessibleI.*.*.rg");
	}
	if(spSaveAccessible.isNull())
	{
		spSaveAccessible=registry()->create("SurfaceAccessibleI");
	}
	if(spSaveAccessible.isNull())
	{
		catalog<String>("error")+="no format drivers found";
		return;
	}

	sp<Scope> spScope=registry()->create("Scope");

	spSaveAccessible->bind(spScope);
	spSaveAccessible->copy(spInputAccessible);

#if FE_EXO_DEBUG
	feLog("ExportOp::handle using \"%s\"\n",
			spSaveAccessible->name().c_str());
#endif

	Real frame=catalog<Real>("frame");

	if(frame<startFrame(a_rSignal) || frame>endFrame(a_rSignal))
	{
		frame=currentFrame(a_rSignal);
	}

	const I32 intFrame=frame;
	const String intText=fe::print(intFrame);
	const String realText=fe::print(frame);
	String modfile=savefile.substitute("$F",fe::print(intFrame))
			.substitute("$FF",fe::print(frame));

#if FE_EXO_DEBUG
	const BWORD substitute=savefile.match(".*[$]F.*");

	feLog("ExportOp::handle start %.6G end %.6G frame %.6G -> %.6G\n",
			startFrame(a_rSignal),endFrame(a_rSignal),
			catalog<Real>("frame"),frame);
	feLog("ExportOp::handle sub %d save \"%s\"\n",substitute,modfile.c_str());
#endif

	sp<Catalog> spSettings=
			registry()->master()->createCatalog("Export Settings");
	if(spSettings.isValid())
	{
		spSettings->catalog<Real>("frame")=frame;
		spSettings->catalog<String>("options")=options;
	}

	if(!spSaveAccessible->save(modfile,spSettings))
	{
		feLog("ExportOp::handle"
				" node \"%s\" failed to save:\n  \"%s\"\n",
				name().c_str(),modfile.c_str());

		catalog<String>("warning")+="save failed;";
	}

	catalog<String>("summary")=savefile;

#if FE_EXO_DEBUG
	feLog("ExportOp::handle node \"%s\" done\n",name().c_str());
#endif
}
