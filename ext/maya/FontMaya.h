
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_FontMaya_h__
#define __maya_FontMaya_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Maya-specific implementations for FontI

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT FontMaya: public FontI,
	public CastableAs<FontMaya>
{
	public:
				FontMaya(void);
virtual			~FontMaya(void);

virtual	I32		fontHeight(I32* a_pAscent,I32* a_pDescent)
				{
					if(a_pAscent)
					{
						*a_pAscent=m_fontAscent;
					}
					if(a_pDescent)
					{
						*a_pDescent=m_fontDescent;
					}

					return m_fontAscent+m_fontDescent;
				}
virtual	I32		pixelWidth(String a_string)
				{	return m_charWidth*a_string.length(); }

virtual	void	drawAlignedText(sp<Component> a_spDrawComponent,
					const SpatialVector& a_location,
					const String a_text,const Color &a_color);

	private:

		I32				m_fontAscent;
		I32				m_fontDescent;
		I32				m_charWidth;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_FontMaya_h__ */
