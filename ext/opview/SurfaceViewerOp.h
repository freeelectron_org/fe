/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opview_SurfaceViewerOp_h__
#define __opview_SurfaceViewerOp_h__

#include "operator/RulerOp.h"

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to look at surfaces in an independent window

	@ingroup opview
*//***************************************************************************/
class FE_DL_EXPORT SurfaceViewerOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceViewerOp>
{
	public:
		typedef enum
		{
			e_reframeObject=	0x00,
			e_centerObject=		0x01,
			e_centerOrigin=		0x02
		} Targeting;

							SurfaceViewerOp(void);
virtual						~SurfaceViewerOp(void);

		void				initialize(void);

							//* As HandlerI
virtual	void				handle(Record& a_rSignal);

		sp<QuickViewerI>	quickViewer(void)		{ return m_spQuickViewerI; }

		sp<DrawableI>		drawable(I32 a_surfaceIndex);
		I32					drawableCount(void);

		Array< sp<DrawableI> >&	subDrawableArray(I32 a_surfaceIndex);

		sp<CameraI>			camera(I32 a_cameraIndex);
		I32					cameraCount(void);

		BWORD				getRebuild(void);

		I32					idleCount(void);
		void				clearIdle(void);
		void				incrementIdle(void);

		void				target(Targeting a_targeting);
		Targeting			targeting(void) const;

		DrawMode::DrawStyle	drawStyle(void) const;
		String				orthoAxis(void) const;
		BWORD				uvSpace(void) const;
		BWORD				cameraView(void) const;
		I32					cameraIndex(void) const;
		BWORD				help(void) const;
		BWORD				spiral(void) const;
		BWORD				tracking(void) const;
		BWORD				edging(void) const;
		BWORD				grid(void) const;
		BWORD				outline(void) const;
		BWORD				backfacing(void) const;
		BWORD				lighting(void) const;
		BWORD				shadows(void) const;
		Real				pointSize(void) const;
		Real				lineWidth(void) const;
		I32					refinement(void) const;

		I32					glowFrame(void) const { return m_glowFrame; }

		Real				updateSlack(void);
		void				setSlack(Real a_slack,Real a_slackInc,
								Real a_slackMin)
							{	m_slack=a_slack;
								m_slackInc=a_slackInc;
								m_slackMin=a_slackMin; }

		String				menuString(void) const	{ return m_menuString; }
		void				setMenuString(String a_string)
							{	m_menuString=a_string; }
		sp<Catalog>&		menu(void)				{ return m_spMenu; }

		String				findPartitionAttribute(sp<SurfaceAccessibleI>
									a_spSurfaceAccessible,String a_nodeName);

		sp<CameraEditable>	orthoCam(void)			{ return m_spOrthoCam; }
	private:

		void				post(Real a_frame);
		void				react(WindowEvent& a_rEvent);
		void				partitionSurface(I32 a_surfaceIndex);
		String				inputNameByIndex(I32 a_inputIndex);

	class DrawHandler:
		virtual public HandlerI,
		public CastableAs<DrawHandler>
	{
		public:
						DrawHandler(void);

	virtual void		handle(Record& a_render);

			void		bind(sp<DrawI> a_spDrawI)
						{	m_spDrawI=a_spDrawI; }
			void		bindDrawable(I32 a_inputIndex,I32 a_surfaceIndex,
							sp<DrawableI> a_spDrawable,String a_nodeName);

			void		incrementSerial(void)		{ m_serial++; }
			void		setFrame(Real a_frame);
			void		updateFrame(void);
			Real		minFrame(void) const		{ return m_minFrame; }
			Real		maxFrame(void) const		{ return m_maxFrame; }
			void		setStartFrame(Real a_frame)	{ m_startFrame=a_frame; }
			Real		startFrame(void) const		{ return m_startFrame; }
			void		setEndFrame(Real a_frame)	{ m_endFrame=a_frame; }
			Real		endFrame(void) const		{ return m_endFrame; }

			void		setMicroseconds(I32 a_us)	{ m_us=a_us; }
			I32			microseconds(void)			{ return m_us; }

			sp<SurfaceI> impactSurface(void) const
						{	return m_spImpactSurface; }
			sp<SurfaceI> surface(I32 a_surfaceIndex) const;
			String		nodeName(I32 a_surfaceIndex) const;

			I32			isolatedIndex(void)
						{	return m_isolateSurfaceIndex; }

			BWORD		hideAll(I32 a_surfaceIndex);
			BWORD		ghostAll(I32 a_surfaceIndex);

			void		setSurfaceViewerOp(
								sp<SurfaceViewerOp> a_spSurfaceViewerOp);
			void		setImpactAccessible(
							sp<SurfaceAccessibleI> a_spSurfaceAccessible);
			void		setImpactSurface(sp<SurfaceI> a_spSurface)
						{	m_spImpactSurface=a_spSurface; }
			void		setSurfaceImpact(
								sp<SurfaceI::ImpactI> a_spSurfaceImpact)
						{	if(!m_paperMode)
								m_spSurfaceImpact=a_spSurfaceImpact; }
			void		setImpactNodeName(String a_nodeName)
						{	m_impactNodeName=a_nodeName; }
			void		setImpactInputName(String a_inputName)
						{	m_impactInputName=a_inputName; }

			I32			impactSurfaceIndex(void)
						{	return m_impactSurfaceIndex; }
			void		setImpactSurfaceIndex(I32 a_surfaceIndex)
						{	m_impactSurfaceIndex=a_surfaceIndex; }

			void		pickIncrement(I32 a_increment);
			void		pick(I32 a_surfaceIndex);
			void		unpick(void);
			void		hideSelected(void);
			void		ghostSelected(void);
			void		isolateToggle(void);
			void		paperToggle(void);

			BWORD		getImpactRegion(SpatialVector& a_rCenter,
							Real& a_rRadius) const ;
			BWORD		getSelectRegion(SpatialVector& a_rCenter,
							Real& a_rRadius) const ;

			void		setPickingSurfaceIndex(I32 a_surfaceIndex)
						{	m_pickingSurfaceIndex=a_surfaceIndex; }
			void		setPickingAccessible(
							sp<SurfaceAccessibleI> a_spPickingAccessible);
			void		setPickingNodeName(String a_nodeName)
						{	m_pickingNodeName=a_nodeName; }
			void		setPickingImpact(
							sp<SurfaceI::ImpactI> a_spPickingImpact)
						{	m_spPickingImpact=a_spPickingImpact; }
			void		setPickPoint(SpatialVector a_point,Real a_radius)
						{	m_pickPoint=a_point;
							m_pickRadius=a_radius; }

			void		clearEmphasis(void);
			void		setEmphasis(I32 a_surfaceIndex,Real a_emphasis);

			void		clearGeodesics(void);

			void		drawNormals(I32 a_surfaceIndex);
			void		drawInstances(I32 a_surfaceIndex,
							const Color* a_pColor);
			void		drawHorizon(void);
			void		drawDesignator(I32 a_surfaceIndex);
			void		runMenu(void);
			void		findPart(String a_partname);
			void		clearBuffer(I32 a_surfaceIndex);
			void		clearBuffers(void);
			void		applyHidden(I32 a_surfaceIndex);
			void		applyGhosted(I32 a_surfaceIndex);
			void		reapply(void);

			Color		background(void);
			Real		gaugeAlpha(sp<SurfaceI::GaugeI> a_spGauge,
							const SpatialVector& a_point);

		private:

	class SurfaceNode
	{
		public:
												SurfaceNode(void):
													m_hideAll(FALSE),
													m_ghostAll(FALSE),
													m_inputIndex(-1),
													m_emphasis(0)			{}

				sp<DrawableI>					m_spDrawable;
				sp<DrawBufferI>					m_spSolidBuffer;
				sp<DrawBufferI>					m_spWireBuffer;
				sp<SurfaceTrianglesAccessible>	m_spSurfaceGeodesic;
				sp<PartitionI>					m_spGhostPartition;
				Array<String>					m_hideList;
				Array<String>					m_ghostList;
				BWORD							m_hideAll;
				BWORD							m_ghostAll;
				I32								m_inputIndex;
				Real							m_emphasis;
				String							m_nodeName;

	};
			Array<SurfaceNode>				m_nodeArray;

			AsViewer						m_asViewer;
			sp<DrawI>						m_spDrawI;
			U32								m_serial;
			U32								m_lastSerial;
			Real							m_frame;
			Real							m_newFrame;
			Real							m_minFrame;
			Real							m_maxFrame;
			Real							m_startFrame;
			Real							m_endFrame;
			I32								m_us;
			I32								m_reopenCountdown;

			I32								m_pickingSurfaceIndex;
			String							m_pickingNodeName;
			sp<SurfaceI::ImpactI>			m_spPickingImpact;
			sp<SurfaceAccessibleI>			m_spPickingAccessible;
			sp<SurfaceTrianglesAccessible>	m_spPickingGeodesic;
			SpatialVector					m_pickPoint;
			Real							m_pickRadius;
			BWORD							m_selectRegion;
			SpatialVector					m_selectCenter;
			Real							m_selectRadius;
			String							m_selectName;
			I32								m_isolateSurfaceIndex;
			String							m_isolatePart;
			I32								m_outlineAt;
			Array<String>					m_outline;

			Real							m_multiplication;
			I32								m_characterWidth;
			I32								m_textHeight;

			BWORD							m_paperMode;
			BWORD							m_keepEdges;
			std::map<U64,Edge>				m_edgeMap;

			hp<SurfaceViewerOp>				m_hpSurfaceViewerOp;
			sp<RulerOp>						m_spRulerOp;
			sp<SurfaceAccessibleI>			m_spImpactAccessible;
			sp<SurfaceI>					m_spImpactSurface;
			sp<SurfaceI::ImpactI>			m_spSurfaceImpact;
			String							m_impactNodeName;
			String							m_impactInputName;
			I32								m_impactSurfaceIndex;
			String							m_partAttr;

			sp<DrawMode>					m_spDrawGhost;
			sp<DrawMode>					m_spDrawPoints;
			sp<DrawMode>					m_spDrawWire;
			sp<DrawMode>					m_spDrawImpact;
			sp<DrawMode>					m_spDrawEdging;
			sp<DrawMode>					m_spDrawSketch;
			sp<DrawMode>					m_spDrawPicking;
			sp<DrawMode>					m_spDrawText;

			sp<ImageI>						m_spTextureImage;
	};

	class EventHandler:
		virtual public HandlerI,
		public CastableAs<EventHandler>
	{
		public:
					EventHandler(void)										{}

	virtual void	handle(Record& a_render);

			void	setSurfaceViewerOp(sp<SurfaceViewerOp> a_spSurfaceViewerOp)
					{	m_hpSurfaceViewerOp=a_spSurfaceViewerOp; }

		private:
			WindowEvent			m_event;
			hp<SurfaceViewerOp>	m_hpSurfaceViewerOp;
	};

	class Worker: public Thread::Functor
	{
		public:
							Worker(sp< JobQueue<Real> > a_spJobQueue,
								U32 a_id,String a_stage):
								m_hpJobQueue(a_spJobQueue),
								m_id(a_id),
								m_us(0),
								m_spiralAngle(0.0),
								m_spiralStep(0.1)							{}

	virtual					~Worker(void)									{}

	virtual	void			operate(void);

			void			setObject(sp<Counted> a_spObject)
							{	m_pSurfaceViewerOp=
										sp<SurfaceViewerOp>(a_spObject).raw(); }

		private:
			hp< JobQueue<Real> >	m_hpJobQueue;
			U32						m_id;
			U32						m_us;
			Real					m_spiralAngle;
			Real					m_spiralStep;

									//* may use during ~SurfaceViewerOp
			SurfaceViewerOp*		m_pSurfaceViewerOp;
	};

		U32							m_jobs;
		U32							m_us;
		I32							m_milliDelay;
		sp< JobQueue<Real> >		m_spJobQueue;
		sp< Gang<Worker,Real> >		m_spGang;
		sp<QuickViewerI>			m_spQuickViewerI;
		sp<EventHandler>			m_spEventHandler;
		sp<DrawHandler>				m_spDrawHandler;
		sp<RecorderI>				m_spVideoRecorder;
		sp<CameraEditable>			m_spOrthoCam;

		Array< sp<SurfaceAccessibleI> >	m_accessibleArray;
		Array< sp<DrawableI> >			m_drawableArray;
		Array< sp<CameraI> >			m_cameraArray;

		Array< Array< sp<DrawableI> > >		m_drawableTable;

		sp<Catalog>					m_spMenu;
		String						m_menuString;

		BWORD						m_rebuild;
		I32							m_idleCount;
		BWORD						m_freezeFrame;
		Real						m_lastFrame;
		I32							m_glowFrame;
		Real						m_slack;
		Real						m_slackInc;
		Real						m_slackMin;
		Targeting					m_targeting;
		String						m_orthoAxis;
		BWORD						m_uvSpace;
		BWORD						m_cameraView;
		I32							m_cameraIndex;
		BWORD						m_help;
		BWORD						m_spiral;
		BWORD						m_tracking;
		Box2i						m_scrubBox;

	public:

		sp<DrawHandler>		drawHandler(void)	{ return m_spDrawHandler; }
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opview_SurfaceViewerOp_h__ */
