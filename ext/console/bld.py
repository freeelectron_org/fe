import sys
import os
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool" ]

def setup(module):

    srcList = [ "console.pmh",
                "DrawConsole",
                "NullViewer",
                "consoleDL" ]

    dll = module.DLL( "fexConsoleDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib",
#               "fexViewerLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexConsoleDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexConsoleDL",                 None,       None) ]
