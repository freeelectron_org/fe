/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceAccessorRecord::SurfaceAccessorRecord(void)
{
	setName("SurfaceAccessorRecord");

#if FE_COUNTED_STORE_TRACKER
	registerRegion(this,sizeof(SurfaceAccessorRecord));
#endif
}

SurfaceAccessorRecord::~SurfaceAccessorRecord(void)
{
}

void SurfaceAccessorRecord::setWritable(BWORD a_writable)
{
//	feLog("SurfaceAccessorRecord::setWritable %d\n",a_writable);

	SurfaceAccessorBase::setWritable(a_writable);
}

sp<RecordArray> SurfaceAccessorRecord::recordArray(void) const
{
	if(m_spRecordArray.isValid())
	{
		return m_spRecordArray;
	}

	RecordGroup::iterator it=m_spRG->begin();
	if(it!=m_spRG->end())
	{
		sp<RecordArray> spRecordArray= *it;

		if(!m_surfaceElementRAV.isBound())
		{
			const_cast< RecordArrayView<SurfacePoint>* >(&m_surfaceElementRAV)
					->bind(spRecordArray);
		}

		return spRecordArray;
	}
	return sp<RecordArray>(NULL);
}

U32 SurfaceAccessorRecord::count(void) const
{
	if(m_element==SurfaceAccessibleI::e_detail)
	{
		return 1;
	}

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		if(m_groupSetRecord.isValid())
		{
			return m_groupSetAccessor(m_groupSetRecord).size();
		}
		if(m_spMultiGroup.isValid())
		{
			return m_spMultiGroup->count();
		}
		return 0;
	}

	sp<RecordArray> spRecordArray=recordArray();

	return spRecordArray.isValid()? spRecordArray->length(): 0;
}

U32 SurfaceAccessorRecord::subCount(U32 a_index) const
{
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(m_spPrimitiveArray.isNull())
		{
			feLog("SurfaceAccessorRecord::subCount no primitive array\n");
			return 0;
		}

		Record primitiveRecord=m_spPrimitiveArray->getRecord(a_index);
		FEASSERT(primitiveRecord.isValid());

		const Array<I32>& rVertexArray=m_vertexIndexAccessor(primitiveRecord);

		return rVertexArray.size();
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		sp<RecordArray> spRecordArray=recordArray();
		if(spRecordArray.isValid())
		{
			Record record=spRecordArray->getRecord(a_index);
			const Array<I32>& rVertexArray=m_vertexIndexAccessor(record);
			return rVertexArray.size();
		}
	}

	return 0;
}

String SurfaceAccessorRecord::type(void) const
{
	if(!m_attrType.empty())
	{
		return m_attrType;
	}

	sp<RecordArray> spRecordArray=recordArray();
	sp<Layout> spLayout=spRecordArray->layout();
	sp<Scope> spScope=spLayout->scope();
	sp<Attribute> spAttribute=spScope->findAttribute(m_attrName);
	const TypeInfo& rTypeInfo=spAttribute->type()->typeinfo();

	if(rTypeInfo==getTypeId<String>())
	{
		return "string";
	}
	else if(rTypeInfo==getTypeId<bool>())
	{
		return "bool";
	}
	else if(rTypeInfo==getTypeId<I32>())
	{
		return "integer";
	}
	else if(rTypeInfo==getTypeId<Real>())
	{
		return "real";
	}
	else if(rTypeInfo==getTypeId<SpatialVector>())
	{
		return "vector3";
	}
	else if(rTypeInfo==getTypeId< sp<Component> >())
	{
		return "component";
	}

	return String();
}

void SurfaceAccessorRecord::ensureType(String a_attrType)
{
//	feLog("SurfaceAccessorRecord::ensureType \"%s\" vs \"%s\" for \"%s\"\n",
//			a_attrType.c_str(),m_attrType.c_str(),m_attrName.c_str());

	if(a_attrType==m_attrType)
	{
		return;
	}
	if(m_attrType.empty())
	{
		sp<RecordArray> spRecordArray=recordArray();
		if(spRecordArray.isNull() && m_element==SurfaceAccessibleI::e_detail)
		{
			append(SurfaceAccessibleI::e_closed);
			spRecordArray=recordArray();
		}
		if(spRecordArray.isNull())
		{
			feLog("SurfaceAccessorRecord::ensureType(\"%s\")"
					" no records for \"%s\"\n",
					a_attrType.c_str(),m_attrName.c_str());
			return;
		}

		m_attrType=a_attrType;

		sp<Layout> spLayout=spRecordArray->layout();
		const String layoutName=spLayout->name();

		const BWORD populated=spLayout->checkAttributeStr(m_attrName);

//		feLog("SurfaceAccessorRecord::ensureType"
//				" \"%s\" for \"%s\" in \"%s\" populated %d\n",
//				a_attrType.c_str(),m_attrName.c_str(),layoutName.c_str(),
//				populated);

		sp<Scope> spScope=spLayout->scope();

//		m_spSurfaceAccessibleI->lock();

		spScope->support(m_attrName,m_attrType);

		if(!populated)
		{
			spScope->populate(layoutName,m_attrName);
		}

		if(m_attrType=="string")
		{
			m_accessorString.setup(spScope,m_attrName,m_attrType);
		}
		else if(m_attrType=="integer")
		{
			m_accessorInteger.setup(spScope,m_attrName,m_attrType);
		}
		else if(m_attrType=="real")
		{
			m_accessorReal.setup(spScope,m_attrName,m_attrType);
		}
		else if(m_attrType=="vector3")
		{
			m_accessorSpatialVector.setup(spScope,m_attrName,m_attrType);
		}
		else if(m_attrType=="color")
		{
			m_accessorColor.setup(spScope,m_attrName,m_attrType);
		}

//		m_spSurfaceAccessibleI->unlock();

		return;
	}

#if FE_CODEGEN<=FE_DEBUG
	feLog("SurfaceAccessorRecord::ensureType(\"%s\")"
			" already \"%s\" for \"%s\"\n",
			a_attrType.c_str(),m_attrType.c_str(),m_attrName.c_str());
#endif
}

BWORD SurfaceAccessorRecord::ensurePrimitiveArray(void)
{
	if(m_spPrimitiveArray.isValid())
	{
		return TRUE;
	}

	RecordGroup::iterator it=m_spPrimitiveRG->begin();
	if(it!=m_spPrimitiveRG->end())
	{
		m_spPrimitiveArray= *it;
	}

	FEASSERT(m_spPrimitiveArray.isValid());

	if(m_spPrimitiveArray.isNull())
	{
		feLog("SurfaceAccessorRecord::ensurePrimitiveArray"
				" no primitive array\n");
		return FALSE;
	}

	sp<Layout> spPrimitiveLayout=m_spPrimitiveArray->layout();
	sp<Scope> spScope=spPrimitiveLayout->scope();

	const String attrName="surf:vertices";
	const String attrType="intarray";

	spScope->support(attrName,attrType);
	spScope->populate(spPrimitiveLayout->name(),attrName);

	m_vertexIndexAccessor.setup(spScope,attrName,attrType);

	return TRUE;
}

void SurfaceAccessorRecord::ensureGroupSetRecord(BWORD a_create)
{
	if(m_groupSetRecord.isValid() || m_spMultiGroup.isValid())
	{
		return;
	}

	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup)
	{
		feLog("SurfaceAccessorRecord::ensureGroupSetRecord"
				" not group element\n");
		return;
	}

	if(m_attrName.empty())
	{
		feLog("SurfaceAccessorRecord::ensureGroupSetRecord"
				" no group string\n");
		return;
	}

	sp<Scope> spScope=m_spLayout.isValid()?
			m_spLayout->scope():
			m_weakRecord.layout()->scope();

	if(!m_spRG->size())
	{
		const String layoutName=
				SurfaceAccessibleBase::elementLayout(m_element);

		spScope->populate(layoutName,"surf:groupName");
		spScope->populate(layoutName,"surf:indexSet");
	}

	if(!m_groupSetRecord.isValid() && m_spRG->size())
	{
		//* find existing group with matching name

		RecordGroup::iterator it=m_spRG->begin();
		if(it!=m_spRG->end())
		{
			sp<RecordArray> spRA= *it;

			const I32 length=spRA->length();
			for(I32 index=0;index<length;index++)
			{
				Record record=spRA->getRecord(index);
				if(m_groupNameAccessor(record)==m_attrName)
				{
//					feLog("FOUND GROUP \"%s\"\n",m_attrName.c_str());
					m_groupSetRecord=record;

					sp<SurfaceAccessibleBase> spAccessible=
							m_spSurfaceAccessibleI;
					if(spAccessible.isNull())
					{
						break;
					}

					if(spAccessible->hasGroup(m_element,m_attrName))
					{
						m_spMultiGroup=
								spAccessible->group(m_element,m_attrName);
						break;
					}

					m_spMultiGroup=sp<SurfaceAccessibleBase::MultiGroup>(
							new SurfaceAccessibleBase::MultiGroup());
					m_spMultiGroup->insert(m_groupSetAccessor(record));

					std::map<String,sp<SurfaceAccessibleBase::MultiGroup> >&
							rGroupMap=spAccessible->groupMap(m_element);
					rGroupMap[m_attrName]=m_spMultiGroup;
					break;
				}
			}
		}
	}

	FEASSERT(!m_attrName.empty());
	const char firstChar=m_attrName.c_str()[0];
	if((firstChar>=0 && firstChar<=9) || m_attrName.contains(" "))
	{
//		feLog("DERIVED GROUP \"%s\"\n",m_attrName.c_str());

		//* TODO numerical and multi groups
		sp<SurfaceAccessibleBase> spAccessible=m_spSurfaceAccessibleI;
		if(spAccessible.isValid())
		{
			m_spMultiGroup=spAccessible->group(m_element,m_attrName);
		}

		return;
	}

	//* create new group
	if(a_create && !m_groupSetRecord.isValid())
	{
//		feLog("NEW GROUP \"%s\"\n",m_attrName.c_str());

		m_groupSetRecord=spScope->createRecord(m_spLayout);
		m_spRG->add(m_groupSetRecord);
		m_groupNameAccessor(m_groupSetRecord)=m_attrName;

		sp<SurfaceAccessibleBase> spAccessible=m_spSurfaceAccessibleI;
		if(spAccessible.isValid())
		{
			m_spMultiGroup=sp<SurfaceAccessibleBase::MultiGroup>(
					new SurfaceAccessibleBase::MultiGroup());

			std::map<String, sp<SurfaceAccessibleBase::MultiGroup> >&
					rGroupMap=spAccessible->groupMap(m_element);
			rGroupMap[m_attrName]=m_spMultiGroup;
		}
	}
}

I32 SurfaceAccessorRecord::ensureVertex(U32 a_index,U32 a_subIndex)
{
	if(!ensurePrimitiveArray())
	{
		feLog("SurfaceAccessorRecord::ensureVertex no primitive array\n");
		return -1;
	}

	Record primitiveRecord=m_spPrimitiveArray->getRecord(a_index);
	FEASSERT(primitiveRecord.isValid());

	Array<I32>& rVertexArray=m_vertexIndexAccessor(primitiveRecord);

	const U32 existingCount=rVertexArray.size();
	if(existingCount<a_subIndex+1)
	{
		rVertexArray.resize(a_subIndex+1);

		for(U32 subIndex=existingCount;subIndex<=a_subIndex;subIndex++)
		{
			rVertexArray[subIndex]= -1;
		}
	}

	I32& rIndex=rVertexArray[a_subIndex];
	if(rIndex<0)
	{
		const String layoutName=
				SurfaceAccessibleBase::elementLayout(m_element);
		sp<Scope> spScope=m_weakRecord.layout()->scope();
		m_spLayout=spScope->declare(layoutName);

		rIndex=m_spRG->count();

		Record record=spScope->createRecord(m_spLayout);
		m_spRG->add(record);
	}

	return rIndex;
}

void SurfaceAccessorRecord::set(U32 a_index,U32 a_subIndex,String a_string)
{
	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType("string");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return;
	}

	m_spRecordArray=spRecordArray;

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		m_accessorString(record)=a_string;
	}
}

String SurfaceAccessorRecord::string(U32 a_index,U32 a_subIndex)
{
	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType("string");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return "";
	}

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		return m_accessorString(record);
	}

	return "";
}

void SurfaceAccessorRecord::set(U32 a_index,U32 a_subIndex,I32 a_integer)
{
	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		//* HACK only e_openCurve
		if(a_subIndex!=SurfaceAccessibleI::e_openCurve)
		{
			return;
		}
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		FEASSERT(m_element==SurfaceAccessibleI::e_primitive);

		ensureType("intarray");

		sp<RecordArray> spRecordArray=recordArray();
		if(spRecordArray.isNull() || a_index>=spRecordArray->length())
		{
			return;
		}

		Record record=spRecordArray->getRecord(a_index);
		Array<I32>& rVertexArray=m_vertexIndexAccessor(record);

		if(a_subIndex<rVertexArray.size())
		{
			rVertexArray[a_subIndex]=a_integer;
		}

		return;
	}

	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType("integer");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return;
	}

	m_spRecordArray=spRecordArray;

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		m_accessorInteger(record)=a_integer;
	}
}

I32 SurfaceAccessorRecord::integer(U32 a_index,U32 a_subIndex)
{
	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		//* HACK only e_openCurve
		if(a_subIndex!=SurfaceAccessibleI::e_openCurve)
		{
			return 0;
		}
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		FEASSERT(m_element==SurfaceAccessibleI::e_primitive);

		ensureType("intarray");

		sp<RecordArray> spRecordArray=recordArray();
		if(spRecordArray.isNull() || a_index>=spRecordArray->length())
		{
			return 0;
		}

		Record record=spRecordArray->getRecord(a_index);
		const Array<I32>& rVertexArray=m_vertexIndexAccessor(record);

		if(a_subIndex>=rVertexArray.size())
		{
			return 0;
		}

		return rVertexArray[a_subIndex];
	}

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		ensureGroupSetRecord(FALSE);

		if(m_spMultiGroup.isValid())
		{
			return (a_index<m_spMultiGroup->count())?
					m_spMultiGroup->at(a_index): -1;
		}

		feLog("SurfaceAccessorRecord::integer no MultiGroup for \"%s\"\n",
				m_attrName.c_str());

		if(m_groupSetRecord.isValid())
		{
			const std::set<I32>& rGroupSet=
					m_groupSetAccessor(m_groupSetRecord);

			//* NOTE very inefficient (m_spMultiGroup should be valid)
			std::set<I32>::iterator iterator=rGroupSet.begin();
			I32 steps=a_index;
			while(steps>0)
			{
				iterator++;
				steps--;
			}
			return (iterator!=rGroupSet.end())? *iterator: -1;
		}

		return -1;
	}

	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType("integer");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return 0;
	}

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		return m_accessorInteger(record);
	}

	return 0;
}

void SurfaceAccessorRecord::append(U32 a_index,I32 a_integer)
{
	ensureType("intarray");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull())
	{
		feLog("SurfaceAccessorRecord::append(U32,I32) null record array\n");
		return;
	}

	m_spRecordArray=spRecordArray;

	Record record=spRecordArray->getRecord(a_index);
	m_vertexIndexAccessor(record).push_back(a_integer);
}

//* see SurfaceAccessorBase::append
void SurfaceAccessorRecord::append(Array< Array<I32> >& a_rPrimVerts)
{
	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		return;
	}

	const I32 appendCount=a_rPrimVerts.size();

	m_attribute=SurfaceAccessibleI::e_generic;

	//* add primitives
	I32 primitiveIndex=append(appendCount,SurfaceAccessibleI::e_closed);

	m_attribute=SurfaceAccessibleI::e_vertices;

	ensureType("intarray");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull())
	{
		feLog("SurfaceAccessorRecord::append(Array< Array<I32> >&)"
				" null record array\n");
		return;
	}

	m_spRecordArray=spRecordArray;

	for(I32 appendIndex=0;appendIndex<appendCount;appendIndex++)
	{
		Record record=spRecordArray->getRecord(primitiveIndex++);
		Array<I32>& rVertexIndexArray=m_vertexIndexAccessor(record);

		const Array<I32>& rVertList=a_rPrimVerts[appendIndex];

		const U32 subCount=rVertList.size();
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=rVertList[subIndex];
			rVertexIndexArray.push_back(pointIndex);
		}
	}
}

I32 SurfaceAccessorRecord::append(SurfaceAccessibleI::Form a_form)
{
	if(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute==SurfaceAccessibleI::e_vertices)
	{
		//* just append one primitive, no vertices

		m_attribute=SurfaceAccessibleI::e_generic;

		const I32 primitiveIndex=append(1,a_form);

		m_attribute=SurfaceAccessibleI::e_vertices;

		return primitiveIndex;
	}

	return append(1,a_form);
}

I32 SurfaceAccessorRecord::append(I32 a_integer,SurfaceAccessibleI::Form a_form)
{
	if(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute==SurfaceAccessibleI::e_vertices)
	{
		return SurfaceAccessorBase::append(a_integer,a_form);
	}

	if(a_integer<1)
	{
		return -1;
	}

	//* append multiple points or primitives

	const String layoutName=SurfaceAccessibleBase::elementLayout(m_element);

	sp<Scope> spScope;

	if(m_spLayout.isValid())
	{
		spScope=m_spLayout->scope();
	}
	else
	{
		spScope=m_weakRecord.layout()->scope();
		m_spLayout=spScope->declare(layoutName);
	}

	if(m_spLayout.isNull())
	{
		feLog("SurfaceAccessorRecord::append"
				" no such layout \"%s\" for \"%s\" type \"%s\"\n",
				layoutName.c_str(),m_attrName.c_str(),m_attrType.c_str());
		FEASSERT(FALSE);

		return 0;
	}

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		ensureGroupSetRecord(TRUE);

		m_groupSetAccessor(m_groupSetRecord).insert(a_integer);
		if(m_spMultiGroup.isValid())
		{
			m_spMultiGroup->insert(a_integer);
		}
	}
	else
	{
		for(I32 addIndex=0;addIndex<a_integer;addIndex++)
		{
			Record record=spScope->createRecord(m_spLayout);
			m_spRG->add(record);
		}
	}

	if(m_spRecordArray.isValid())
	{
		return m_spRecordArray->length()-a_integer;
	}

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isValid())
	{
		if(!m_surfaceElementRAV.isBound())
		{
			m_surfaceElementRAV.bind(spRecordArray);
		}

		m_spRecordArray=spRecordArray;

		return spRecordArray->length()-a_integer;
	}

	return 0;
}

void SurfaceAccessorRecord::remove(U32 a_index,I32 a_integer)					{
	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || a_index>=spRecordArray->length())
	{
		return;
	}

	spRecordArray->remove(a_index);

	sp<SurfaceAccessibleRecord> spSurfaceAccessibleRecord=
			m_spSurfaceAccessibleI;
	if(spSurfaceAccessibleRecord.isValid())
	{
		spSurfaceAccessibleRecord->setDiscontiguous(m_element);
	}
}

void SurfaceAccessorRecord::set(U32 a_index,U32 a_subIndex,Real a_real)
{
	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType("real");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return;
	}

	m_spRecordArray=spRecordArray;

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		m_accessorReal(record)=a_real;
	}
}

Real SurfaceAccessorRecord::real(U32 a_index,U32 a_subIndex)
{
	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType("real");

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return Real(0);
	}

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		return m_accessorReal(record);
	}

	return Real(0);
}

void SurfaceAccessorRecord::set(U32 a_index,U32 a_subIndex,
	const SpatialVector& a_vector)
{
	if(m_attribute==SurfaceAccessibleI::e_vertices && ensurePointRV())
	{
		const I32 pointIndex=integer(a_index,a_subIndex);

		m_surfacePointRAV[pointIndex].at()=a_vector;
		return;
	}

	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	ensureType(m_attribute==SurfaceAccessibleI::e_color? "color": "vector3");

	switch(m_attribute)
	{
		case SurfaceAccessibleI::e_generic:
			break;
		case SurfaceAccessibleI::e_position:
			m_surfaceElementRAV[elementIndex].at()=a_vector;
			return;
		case SurfaceAccessibleI::e_normal:
			m_surfaceElementRAV[elementIndex].up()=a_vector;
			return;
		case SurfaceAccessibleI::e_uv:
			m_surfaceElementRAV[elementIndex].uvw()=a_vector;
			return;
		case SurfaceAccessibleI::e_color:
			m_surfaceElementRAV[elementIndex].color()=a_vector;
			return;
		default:
			return;
	}

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return;
	}

	m_spRecordArray=spRecordArray;

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		if(m_attribute==SurfaceAccessibleI::e_color)
		{
			m_accessorColor(record)=a_vector;
		}
		else
		{
			m_accessorSpatialVector(record)=a_vector;
		}
	}
}

SpatialVector SurfaceAccessorRecord::spatialVector(U32 a_index,U32 a_subIndex)
{
	FEASSERT(m_spSurfaceAccessibleI.isValid());

	if(m_attribute==SurfaceAccessibleI::e_vertices && ensurePointRV())
	{
		const I32 pointIndex=integer(a_index,a_subIndex);

		return m_surfacePointRAV[pointIndex].at();
	}

#if FALSE
	ensureType("vector3");
#else
	if(m_attrType.empty())
	{

		sp<RecordArray> spRecordArray=recordArray();
		sp<Layout> spLayout=spRecordArray->layout();
		sp<Scope> spScope=spLayout->scope();

		if(m_attribute==SurfaceAccessibleI::e_color)
		{
			m_attrType="color";
			m_accessorColor.setup(spScope,m_attrName,m_attrType);
		}
		else
		{
			m_attrType="vector3";
			m_accessorSpatialVector.setup(spScope,m_attrName,m_attrType);
		}
	}
#endif

	I32 elementIndex=a_index;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		elementIndex=ensureVertex(a_index,a_subIndex);
	}

	switch(m_attribute)
	{
		case SurfaceAccessibleI::e_generic:
			break;
		case SurfaceAccessibleI::e_position:
			return m_surfaceElementRAV[elementIndex].at();
		case SurfaceAccessibleI::e_normal:
			return m_surfaceElementRAV[elementIndex].up();
		case SurfaceAccessibleI::e_uv:
			return m_surfaceElementRAV[elementIndex].uvw();
		case SurfaceAccessibleI::e_color:
			return m_surfaceElementRAV[elementIndex].color();
		default:
			return SpatialVector(Real(0),Real(0),Real(0));
	}

	sp<RecordArray> spRecordArray=recordArray();
	if(spRecordArray.isNull() || elementIndex>=spRecordArray->length())
	{
		return SpatialVector(Real(0),Real(0),Real(0));
	}

	Record record=spRecordArray->getRecord(elementIndex);
	if(record.isValid())
	{
		if(m_attribute==SurfaceAccessibleI::e_color)
		{
			return m_accessorColor(record);
		}

		return m_accessorSpatialVector(record);
	}

	return SpatialVector(Real(0),Real(0),Real(0));
}

BWORD SurfaceAccessorRecord::spatialVector(SpatialVector* a_pVectorArray,
		const Vector2i* a_pIndexArray,I32 a_arrayCount)
{
	for(I32 arrayIndex=0;arrayIndex<a_arrayCount;arrayIndex++)
	{
		const Vector2i& rIndexPair=a_pIndexArray[arrayIndex];

		a_pVectorArray[arrayIndex]=spatialVector(rIndexPair[0],rIndexPair[1]);
	}

	return TRUE;
}

BWORD SurfaceAccessorRecord::ensurePointRV(void)
{
	if(m_surfacePointRAV.isBound())
	{
		return TRUE;
	}

	sp<RecordGroup> spRG=m_spRG;
	sp<RecordArray> spRA=m_spRecordArray;

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_weakRecord);

	FEASSERT(m_weakRecord.isValid());
	m_spRG=surfaceModelRV.surfacePointRG();
	m_spRecordArray=NULL;

	sp<RecordArray> spRecordArrayPoint=recordArray();
	if(spRecordArrayPoint.isValid())
	{
		m_surfacePointRAV.bind(spRecordArrayPoint);
	}

	m_spRG=spRG;
	m_spRecordArray=spRA;

	return m_surfacePointRAV.isBound();
}

BWORD SurfaceAccessorRecord::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
//	feLog("SurfaceAccessorRecord::bindInternal \"%s\" \"%s\"\n",
//			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
//			a_name.c_str());

	m_element=a_element;
	m_attrName=a_name;
	m_spPrimitiveArray = NULL;

	if(m_element==SurfaceAccessibleI::e_pointGroup)
	{
		if(a_name.empty())
		{
			m_element=SurfaceAccessibleI::e_point;
			m_attribute=SurfaceAccessibleI::e_position;
			m_attrName="spc:at";
		}
	}

	if(m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		if(a_name.empty())
		{
			m_element=SurfaceAccessibleI::e_primitive;
			m_attribute=SurfaceAccessibleI::e_vertices;
			m_attrName="surf:points";
		}
	}

	FEASSERT(m_weakRecord.isValid());

	if(!m_weakRecord.isValid())
	{
		feLog("SurfaceAccessorRecord::bindInternal invalid Record\n");
		return FALSE;
	}

	//* RecordView bind to Record is not weak
	//* Record ref counting is not thread-safe, apparently
//	m_spSurfaceAccessibleI->lock();

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_weakRecord);

	FEASSERT(m_weakRecord.isValid());

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		ensurePointRV();
	}

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			m_spRG=surfaceModelRV.surfacePointRG();
			break;
		case SurfaceAccessibleI::e_pointGroup:
			m_spRG=surfaceModelRV.surfacePointSetRG();
			break;
		case SurfaceAccessibleI::e_vertex:
			m_spRG=surfaceModelRV.surfaceVertexRG();
			break;
		case SurfaceAccessibleI::e_primitive:
			m_spRG=surfaceModelRV.surfacePrimitiveRG();
			break;
		case SurfaceAccessibleI::e_primitiveGroup:
			m_spRG=surfaceModelRV.surfacePrimitiveSetRG();
			break;
		case SurfaceAccessibleI::e_detail:
			m_spRG=surfaceModelRV.surfaceDetailRG();
			break;
		default:
			feLog("SurfaceAccessorRecord::bindInternal invalid element\n");
			return FALSE;
	}

	m_spPrimitiveRG=surfaceModelRV.surfacePrimitiveRG();

	surfaceModelRV.unbind();

//	m_spSurfaceAccessibleI->unlock();

	FEASSERT(m_weakRecord.isValid());

	sp<RecordArray> spRecordArray=recordArray();

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		sp<Scope> spScope=m_weakRecord.layout()->scope();
		m_groupNameAccessor.setup(spScope,"surf:groupName","string");
		m_groupSetAccessor.setup(spScope,"surf:indexSet","intset");

		ensureGroupSetRecord(FALSE);
		if(!m_groupSetRecord.isValid() && !m_spMultiGroup.isValid())
		{
			//* group does not exist and can not be generated
//			feLog("SurfaceAccessorRecord::bindInternal"
//					" group \"%s\" does not exist\n",a_name.c_str());
			return FALSE;
		}
		return TRUE;
	}
	else if(spRecordArray.isValid() && spRecordArray->length()>0)
	{
		m_surfaceElementRAV.bind(spRecordArray);

		//* TODO check other attributes
		if(m_attribute==SurfaceAccessibleI::e_position &&
				!m_surfaceElementRAV.recordView().at.check(spRecordArray))
		{
//			feLog("SurfaceAccessorRecord::bindInternal no points\n");
			return FALSE;
		}
		if(m_attribute==SurfaceAccessibleI::e_normal &&
				!m_surfaceElementRAV.recordView().up.check(spRecordArray))
		{
//			feLog("SurfaceAccessorRecord::bindInternal no normals\n");
			return FALSE;
		}
		if(m_attribute==SurfaceAccessibleI::e_uv &&
				!m_surfaceElementRAV.recordView().uvw.check(spRecordArray))
		{
//			feLog("SurfaceAccessorRecord::bindInternal no uvs\n");
			return FALSE;
		}
		if(m_attribute==SurfaceAccessibleI::e_color &&
				!m_surfaceElementRAV.recordView().color.check(spRecordArray))
		{
//			feLog("SurfaceAccessorRecord::bindInternal no colors\n");
			return FALSE;
		}

		if(!spRecordArray->layout()->checkAttributeStr(m_attrName))
		{
//			feLog("SurfaceAccessorRecord::bindInternal"
//					" no attribute named \"%s\"\n",m_attrName.c_str());
			return FALSE;
		}
	}

	m_spRecordArray=spRecordArray;

	FEASSERT(m_weakRecord.isValid());

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		sp<Scope> spScope=m_weakRecord.layout()->scope();
		m_vertexIndexAccessor.setup(spScope,"surf:points","intarray");
	}
	else if(m_spRecordArray.isNull())
	{
//		feLog("SurfaceAccessorRecord::bindInternal record array null\n");
		return FALSE;
	}

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		ensurePrimitiveArray();
	}

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
