/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

using namespace fe;
using namespace fe::ext;

void AttachOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");

	catalog< sp<Component> >("Deformed Surface");
}

void AttachOp::handle(Record& a_rSignal)
{
	sp<SurfaceAccessorI> spOutputVertices;
	if(!accessOutput(spOutputVertices,a_rSignal,e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,"Input Surface",e_primitive,e_vertices)) return;

	sp<SurfaceI> spDriver;
	if(!access(spDriver,"Driver Surface")) return;

	sp<SurfaceI> spDeformed;
	if(!access(spDeformed,"Deformed Surface")) return;

	const U32 primitiveCount=spOutputVertices->count();
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const U32 vertCount=spOutputVertices->subCount(primitiveIndex);
		if(!vertCount)
		{
			continue;
		}
		const SpatialVector root=
				spInputVertices->spatialVector(primitiveIndex,0);
		sp<SurfaceI::ImpactI> spImpact=spDriver->nearestPoint(root);
		if(!spImpact.isValid())
		{
			continue;
		}
		const I32 face=spImpact->face();
		const Barycenter<Real> barycenter=spImpact->barycenter();
		const SpatialTransform xformDriver=spDriver->sample(face,barycenter);
		const SpatialTransform xformDeform=spDeformed->sample(face,barycenter);
		SpatialTransform invDriver;
		invert(invDriver,xformDriver);
		const SpatialTransform conversion=invDriver*xformDeform;

		for(U32 vertIndex=0;vertIndex<vertCount;vertIndex++)
		{
			const SpatialVector inputPoint=
					spInputVertices->spatialVector(primitiveIndex,vertIndex);

			SpatialVector transformed;
			transformVector(conversion,inputPoint,transformed);

			spOutputVertices->set(primitiveIndex,vertIndex,transformed);
		}
	}
}
