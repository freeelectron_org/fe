/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SDO_DEBUG		FALSE
#define	FE_SDO_VERBOSE		FALSE
#define	FE_SDO_EXPAND_DEBUG	FALSE

#define FE_SDO_THREAD		FALSE
#define	FE_SDO_PROFILE		(FE_CODEGEN==FE_PROFILE)

#define	FE_SDO_UV_LO(U)		(U<bridgeLo)
#define	FE_SDO_UV_HI(U)		(U>bridgeHi)

#define	FE_SDO_WRAP_UV(WRAP,UV)					\
				if(WRAP){						\
				if(UV[0]>1.0) UV[0]-=1.0;		\
				if(UV[1]>1.0) UV[1]-=1.0; }

namespace fe
{
namespace ext
{

void SubdivideOp::initialize(void)
{
#if !FE_SDO_THREAD
	catalog<bool>("Threads","hidden")=true;
	catalog<bool>("AutoThread","hidden")=true;
	catalog<bool>("Paging","hidden")=true;
	catalog<bool>("StayAlive","hidden")=true;
	catalog<bool>("Work","hidden")=true;
#endif

	catalog<String>("icon")="FE_alpha";

	catalog<String>("partitionAttr")="hull_primitive";
	catalog<String>("partitionAttr","label")="Partition Attr";
	catalog<String>("partitionAttr","hint")=
			"Primitive string attribute created on output"
			" distinguishing the original primitives.";

	catalog<String>("subuvAttr")="subuv";
	catalog<String>("subuvAttr","label")="Subdiv UV Attr";
	catalog<String>("subuvAttr","hint")=
			"Vertex vector attribute created on output"
			" describing coordinates within the original hull face.";

	catalog<I32>("depth")=1;
	catalog<I32>("depth","high")=8;
	catalog<I32>("depth","max")=32;
	catalog<String>("depth","label")="Depth";
	catalog<String>("depth","hint")=
			"Number of subdivision iterations.";

	catalog<I32>("flatDepth")=0;
	catalog<I32>("flatDepth","high")=8;
	catalog<I32>("flatDepth","max")=32;
	catalog<String>("flatDepth","label")="Flat Depth";
	catalog<String>("flatDepth","hint")=
			"Number of iterations to linearly interpolate (at finest end).";

	catalog<bool>("recomputeNormals")=true;
	catalog<bool>("recomputeNormals","joined")=true;
	catalog<String>("recomputeNormals","label")="Recompute Normals";

	catalog<bool>("wrapUVs")=false;
	catalog<String>("wrapUVs","label")="Wrap UVs";

	catalog<bool>("bridgeUVs")=false;
	catalog<bool>("bridgeUVs","joined")=true;
	catalog<String>("bridgeUVs","label")="Bridge UVs";

	catalog<Real>("bridgeLo")=0.2;
	catalog<bool>("bridgeLo","joined")=true;
	catalog<String>("bridgeLo","label")="Low Margin";
	catalog<String>("bridgeLo","enabler")="bridgeUVs";

	catalog<Real>("bridgeHi")=0.8;
	catalog<String>("bridgeHi","label")="High Margin";
	catalog<String>("bridgeHi","enabler")="bridgeUVs";

	catalog<bool>("expand")=false;
	catalog<String>("expand","label")="Expand";
	catalog<bool>("expand","joined")=true;
	catalog<String>("expand","hint")=
			"Move points along normals towards the original input points.";

	catalog<Real>("expansion")=1.0;
	catalog<Real>("expansion","low")=0.0;
	catalog<Real>("expansion","high")=1.0;
	catalog<Real>("expansion","min")= -10.0;
	catalog<Real>("expansion","max")=10.0;
	catalog<String>("expansion","label")="Expansion";
	catalog<String>("expansion","enabler")="expand";
	catalog<String>("expand","hint")=
			"Full expansion tries to move the otherwise incrementally"
			" inscribed result to a more circumscribed result.";

	catalog<bool>("writeHull")=false;
	catalog<String>("writeHull","label")="Write Hull Indices";
	catalog<bool>("writeHull","joined")=true;
	catalog<String>("writeHull","hint")=
			"Store contributing hull point indices on an"
			" output string point attribute."
			"  Each index instance counts equally,"
			" but indexes can be repeated.";

	catalog<String>("hullAttr")="hullVerts";
	catalog<String>("hullAttr","label")="Hull Attr";
	catalog<String>("hullAttr","hint")=
			"Write hull verts to this string point attribute.";

	catalog< sp<Component> >("Input Surface");

	//* don't copy input to output
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";

	m_spSurfaceNormalOp=registry()->create("OperatorSurfaceI.SurfaceNormalOp");
}

void SubdivideOp::handleBind(sp<SignalerI> a_spSignalerI,sp<Layout> a_spLayout)
{
	OperatorSurfaceCommon::handleBind(a_spSignalerI,a_spLayout);

	sp<OperatorSurfaceCommon> spCommon=m_spSurfaceNormalOp;
	if(spCommon.isValid())
	{
		spCommon->handleBind(a_spSignalerI,a_spLayout);
	}
}

void SubdivideOp::clearData(void)
{
	m_facePointIndex.clear();
	m_facePointSum.clear();
	m_faceCount.clear();
	m_midPointSum.clear();
	m_midpointCount.clear();
	m_boundaryPointSum.clear();
	m_boundaryCount.clear();

	m_expand.clear();

	m_edgeMap.clear();
}

void SubdivideOp::handle(Record& a_rSignal)
{
#if FE_SDO_DEBUG
	feLog("SubdivideOp::handle\n");
#endif

#if !FE_SDO_THREAD
	catalog<bool>("AutoThread")=false;
	catalog<I32>("Threads")=1;
#endif

#if FE_SDO_PROFILE
	m_spProfiler=new Profiler("SubdivideOp");
	m_spProfileAccess=new Profiler::Profile(m_spProfiler,"Access");
	m_spProfileResize=new Profiler::Profile(m_spProfiler,"Resize");
	m_spProfileSlot=new Profiler::Profile(m_spProfiler,"Slot");
	m_spProfileFace=new Profiler::Profile(m_spProfiler,"Face");
	m_spProfileFacePoint=new Profiler::Profile(m_spProfiler,"FacePoint");
	m_spProfileVertSize=new Profiler::Profile(m_spProfiler,"VertSize");
	m_spProfileEdgeMap=new Profiler::Profile(m_spProfiler,"EdgeMap");
	m_spProfileEdgePoint=new Profiler::Profile(m_spProfiler,"EdgePoint");
	m_spProfileVertex=new Profiler::Profile(m_spProfiler,"Vertex");
	m_spProfileAppend=new Profiler::Profile(m_spProfiler,"Append");
	m_spProfileEdge=new Profiler::Profile(m_spProfiler,"Edge");
	m_spProfilePoint=new Profiler::Profile(m_spProfiler,"Point");
	m_spProfileExpand=new Profiler::Profile(m_spProfiler,"Expand");
	m_spProfileNormal=new Profiler::Profile(m_spProfiler,"Normal");

	m_spProfiler->begin();
	m_spProfileAccess->start();
#endif

	const String partitionAttr=catalog<String>("partitionAttr");
	const String subuvAttr=catalog<String>("subuvAttr");
	const BWORD recomputeNormals=catalog<bool>("recomputeNormals");
	const BWORD wrapUVs=catalog<bool>("wrapUVs");
	const BWORD bridgeUVs=catalog<bool>("bridgeUVs");
	const Real bridgeLo=catalog<Real>("bridgeLo");
	const Real bridgeHi=catalog<Real>("bridgeHi");
	const I32 depth=catalog<I32>("depth");
	const I32 flatDepth=catalog<I32>("flatDepth");
	const String hullAttr=catalog<String>("hullAttr");
	const BWORD writeHull=(!hullAttr.empty() && catalog<bool>("writeHull"));
	const BWORD expand=catalog<bool>("expand");
	const BWORD generateHull=(writeHull || expand);

	sp<SurfaceAccessibleI> spFinalOutputAccessible;
	if(!accessOutput(spFinalOutputAccessible,a_rSignal)) return;

	if(!access(m_spInputAccessible,"Input Surface")) return;

	if(depth<1)
	{
		spFinalOutputAccessible->copy(m_spInputAccessible);
	}

	catalog<String>("summary")=depth? String("depth ")+depth: String("");

	for(I32 iteration=0;iteration<depth;iteration++)
	{
		m_flat=(iteration>=depth-flatDepth);

		//* shuffle buffers
		if(iteration)
		{
			m_spInputAccessible=m_spOutputAccessible;
		}

		if(iteration==depth-1)
		{
			m_spOutputAccessible=spFinalOutputAccessible;
		}
		else
		{
#if FALSE
			//* Record-based
			m_spOutputAccessible=registry()->create("SurfaceAccessibleI");
			sp<Scope> spScope=registry()->create("Scope");
			m_spOutputAccessible->bind(spScope);
#else
			m_spOutputAccessible=
					registry()->create("*.SurfaceAccessibleCatalog");
#endif
		}

		setOutput(m_spOutputAccessible);

		sp<SurfaceAccessorI> spOutputVertices;
		if(!access(spOutputVertices,m_spOutputAccessible,
				e_primitive,e_vertices)) return;

		sp<SurfaceAccessorI> spOutputPoint;
		if(!access(spOutputPoint,m_spOutputAccessible,
				e_point,e_position)) return;

		sp<SurfaceAccessorI> spOutputPartition;
		if(!partitionAttr.empty() &&
				!access(spOutputPartition,m_spOutputAccessible,
				e_primitive,partitionAttr)) return;

		sp<SurfaceAccessorI> spOutputSubuv;
		if(!subuvAttr.empty() &&
				!access(spOutputSubuv,m_spOutputAccessible,
				e_vertex,subuvAttr)) return;

		sp<SurfaceAccessorI> spInputVertices;
		if(!access(spInputVertices,m_spInputAccessible,
				e_primitive,e_vertices)) return;

		sp<SurfaceAccessorI> spInputPoint;
		if(!access(spInputPoint,m_spInputAccessible,
				e_point,e_position)) return;

		//* TODO catalog needs notion of read only
		//* (instead of needing explicit e_refuseMissing)

		sp<SurfaceAccessorI> spInputPartition;
		if(iteration && !partitionAttr.empty() &&
				spOutputPartition.isValid() &&
				!access(spInputPartition,m_spInputAccessible,
				e_primitive,partitionAttr)) return;

		sp<SurfaceAccessorI> spInputSubuv;
		if(iteration && !subuvAttr.empty() &&
				spOutputSubuv.isValid() &&
				!access(spInputSubuv,m_spInputAccessible,
				e_vertex,subuvAttr)) return;

		sp<SurfaceAccessorI> spInputNormal;
		if(!recomputeNormals || expand)
		{
			if(!access(spInputNormal,m_spInputAccessible,
					e_point,e_normal,e_quiet))
			{
				if(expand)
				{
					catalog<String>("error")+="expansion requires normals;";
				}
				else
				{
					catalog<String>("message")+="no normals found;";
				}
			}
		}

		sp<SurfaceAccessorI> spOutputNormal;
		if(spInputNormal.isValid())
		{
			if(!access(spOutputNormal,m_spOutputAccessible,
					e_point,e_normal)) return;

		}

		m_uvRate=e_primitive;
		sp<SurfaceAccessorI> spInputUV;
		if(!access(spInputUV,m_spInputAccessible,m_uvRate,e_uv,
				e_quiet,e_refuseMissing))
		{
			m_uvRate=e_point;

			access(spInputUV,m_spInputAccessible,m_uvRate,e_uv,
					e_quiet,e_refuseMissing);
		}

		sp<SurfaceAccessorI> spOutputUV;
		if(spInputUV.isValid())
		{
			if(!access(spOutputUV,m_spOutputAccessible,m_uvRate,e_uv)) return;
		}
		else
		{
			catalog<String>("message")+="no UVs found;";
		}

		sp<SurfaceAccessorI> spInputHullVerts;
		sp<SurfaceAccessorI> spOutputHullVerts;
		if(generateHull)
		{
			access(spInputHullVerts,m_spInputAccessible,
					e_point,hullAttr,e_quiet);

			if(!access(spOutputHullVerts,m_spOutputAccessible,
					e_point,hullAttr)) return;
		}

		//* Catmull-Clark

		const U32 primitiveCountIn=spInputVertices->count();
		const U32 pointCountIn=spInputPoint->count();

#if FE_SDO_DEBUG
		feLog("SubdivideOp::handle input points %d primitives %d\n",
				pointCountIn,primitiveCountIn);
#endif

#if FE_SDO_PROFILE
		m_spProfileResize->replace(m_spProfileAccess);
#endif

		clearData();

		m_facePointIndex.resize(primitiveCountIn);

		m_facePointSum.resize(pointCountIn,SpatialVector(0.0,0.0,0.0));

		m_faceCount.resize(pointCountIn,0);

		m_midPointSum.resize(pointCountIn,SpatialVector(0.0,0.0,0.0));

		m_midpointCount.resize(pointCountIn,0);

		m_boundaryPointSum.resize(pointCountIn,SpatialVector(0.0,0.0,0.0));

		m_boundaryCount.resize(pointCountIn,0);

		m_expand.resize(pointCountIn,0.0);

#if FE_SDO_PROFILE
		m_spProfileSlot->replace(m_spProfileResize);
#endif
		const BWORD haveNormals=(spInputNormal.isValid());

		SpatialVector* pointsIn=new SpatialVector[pointCountIn];
		SpatialVector* normalsIn=
				new SpatialVector[haveNormals? pointCountIn: 1];

		m_pointsIn=pointsIn;
		m_normalsIn=haveNormals? normalsIn: NULL;

		//* WARNING does nothing for Maya mesh
		spOutputPoint->append(pointCountIn+primitiveCountIn);

		//* make slots for original points (with adjustments)
		for(U32 pointIndexIn=0;pointIndexIn<pointCountIn;pointIndexIn++)
		{
			m_pointsIn[pointIndexIn]=spInputPoint->spatialVector(pointIndexIn);

			if(haveNormals)
			{
				SpatialVector& rNormal=m_normalsIn[pointIndexIn];
				rNormal=spInputNormal->spatialVector(pointIndexIn);

				//* WARNING broken for Maya mesh
				spOutputNormal->set(pointIndexIn,rNormal);
			}

			if(generateHull)
			{
				String hullVerts;
				if(spInputHullVerts.isValid())
				{
					hullVerts=spInputHullVerts->string(pointIndexIn);
				}
				if(hullVerts.empty())
				{
					hullVerts.sPrintf("%d",pointIndexIn);
				}
				spOutputHullVerts->set(pointIndexIn,hullVerts);
			}
		}

		U32 primVertInc=4096;
		U32 primVertSize=primitiveCountIn*5;	//* probably overshoot

		Array<I32> primOrig(primVertSize);
		Array< Array<I32> > primVerts(primVertSize);
		Array< Array<SpatialVector> > primUVs(
				(m_uvRate==e_primitive)? primVertSize: 0);
		Array< Array<SpatialVector> > primSubuvs(primVertSize);

		U32 primVertCount=0;

#if FE_SDO_PROFILE
		m_spProfileFace->replace(m_spProfileSlot);
#endif

		for(U32 primitiveIndexIn=0;primitiveIndexIn<primitiveCountIn;
				primitiveIndexIn++)
		{
			SpatialVector pointSum(0.0,0.0,0.0);
			SpatialVector normalSum(0.0,0.0,0.0);
			SpatialVector subuvSum(0.0,0.0,0.0);
			SpatialVector uvSum(0.0,0.0,0.0);

			//* UV wrap-around
			Vector2i highUV(false,false);
			SpatialVector lowUV(0.0,0.0,0.0);

			const U32 subCount=spInputVertices->subCount(primitiveIndexIn);

			I32* pointIndices=new I32[subCount];

			SpatialVector* subuv=new SpatialVector[subCount];
			SpatialVector* uv=new SpatialVector[subCount];

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndexIn=
						spInputVertices->integer(primitiveIndexIn,subIndex);
				pointIndices[subIndex]=pointIndexIn;
				pointSum+=m_pointsIn[pointIndexIn];

				//* TODO set subuv at any subCount

				if(spInputSubuv.isValid())
				{
					subuv[subIndex]=spInputSubuv->spatialVector(
							primitiveIndexIn,subIndex);
				}
				else if(subCount==5)
				{
					switch(subIndex)
					{
						case 0:
							set(subuv[subIndex],0.0,0.0);
							break;
						case 1:
							set(subuv[subIndex],1.0,0.0);
							break;
						case 2:
							set(subuv[subIndex],1.0,0.5);
							break;
						case 3:
							set(subuv[subIndex],0.5,1.0);
							break;
						case 4:
							set(subuv[subIndex],0.0,1.0);
							break;
					}
				}
				else if(subCount==3)
				{
					set(subuv[subIndex],
							Real(subIndex==1),
							Real(subIndex==2));
				}
				else
				{
					set(subuv[subIndex],
							Real(subIndex==1 || subIndex==2),
							Real(subIndex==2 || subIndex==3));
				}
				subuvSum+=subuv[subIndex];

				if(spInputUV.isValid())
				{
					SpatialVector& rUv=uv[subIndex];
					rUv=(m_uvRate==e_primitive)?
							spInputUV->spatialVector(primitiveIndexIn,subIndex):
							spInputUV->spatialVector(pointIndexIn);

					if(m_uvRate==e_point)
					{
						FEASSERT(spOutputUV.isValid());
						spOutputUV->set(pointIndexIn,rUv);
					}

					uvSum+=rUv;

					if(bridgeUVs)
					{
						if(FE_SDO_UV_HI(rUv[0]))
						{
							highUV[0]=true;
						}
						if(FE_SDO_UV_HI(rUv[1]))
						{
							highUV[1]=true;
						}
						lowUV[0]+=FE_SDO_UV_LO(rUv[0]);
						lowUV[1]+=FE_SDO_UV_LO(rUv[1]);
					}
				}

				if(haveNormals && !recomputeNormals)
				{
					normalSum+=m_normalsIn[pointIndexIn];
				}
			}

			if(bridgeUVs)
			{
				if(highUV[0])
				{
					uvSum[0]+=lowUV[0];
				}
				if(highUV[1])
				{
					uvSum[1]+=lowUV[1];
				}
			}

			if(subCount>1)
			{
				const Real scalar=1.0/Real(subCount);

				pointSum*=scalar;
				subuvSum*=scalar;
				uvSum*=scalar;

				if(haveNormals && !recomputeNormals)
				{
					normalizeSafe(normalSum);
				}
			}

			FE_SDO_WRAP_UV(wrapUVs,uvSum);

#if FE_SDO_PROFILE
			m_spProfileFacePoint->replace(m_spProfileFace);
#endif

			//* face point

			const I32 pointIndexOut=pointCountIn+primitiveIndexIn;

			m_facePointIndex[primitiveIndexIn]=pointIndexOut;

			if(primVertCount+subCount>=primVertSize)
			{
#if FE_SDO_PROFILE
				m_spProfileVertSize->replace(m_spProfileFacePoint);
#endif

				primVertSize+=primVertInc;
				primVerts.resize(primVertSize);
				primOrig.resize(primVertSize);
				primSubuvs.resize(primVertSize);

				if(m_uvRate==e_primitive)
				{
					primUVs.resize(primVertSize);
				}

#if FE_SDO_PROFILE
				m_spProfileFacePoint->replace(m_spProfileVertSize);
#endif
			}

#if FE_SDO_PROFILE
			m_spProfileEdgeMap->replace(m_spProfileFacePoint);
#endif

			String hullVerts;

			I32 wrapIndex= -1;
			I32 lastEdgePointIndex= -1;
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				//* edge face
				const I32 pointIndexA=pointIndices[subIndex];
				const I32 pointIndexB=pointIndices[(subIndex+1)%subCount];

				const U64 edgeKey=(pointIndexA>pointIndexB)?
						(U64(pointIndexA)<<32)+pointIndexB:
						(U64(pointIndexB)<<32)+pointIndexA;

				Edge& rEdge=m_edgeMap[edgeKey];

				rEdge.m_faceSet.insert(primitiveIndexIn);

#if FE_SDO_PROFILE
				m_spProfileEdgePoint->replace(m_spProfileEdgeMap);
#endif

				if(rEdge.m_pointIndex<0)
				{
					rEdge.m_pointIndex=spOutputPoint->append();
				}

#if FE_SDO_PROFILE
				m_spProfileVertex->replace(m_spProfileEdgePoint);
#endif

				//* face influence on pre-existing points
				m_facePointSum[pointIndexA]+=pointSum;
				m_faceCount[pointIndexA]++;

				primOrig[primVertCount]=primitiveIndexIn;

				Array<I32>& rVerts=primVerts[primVertCount];
				rVerts.resize(4);

				Array<SpatialVector>& rSubuvs=primSubuvs[primVertCount];
				rSubuvs.resize(4);

				Array<SpatialVector>* pUVs=NULL;
				if(m_uvRate==e_primitive)
				{
					pUVs=&primUVs[primVertCount];
					(*pUVs).resize(4);
				}

				primVertCount++;

				//* vertex 0: original point
				rVerts[0]=pointIndexA;

				//* vertex 1: midpoint
				rVerts[1]=rEdge.m_pointIndex;

				//* vertex 2: face point
				rVerts[2]=pointIndexOut;

				//* vertex 3: prev midpoint
				rVerts[3]=lastEdgePointIndex;

				if(subIndex==subCount-1)
				{
					primVerts[wrapIndex][3]=rEdge.m_pointIndex;
				}
				if(!subIndex)
				{
					wrapIndex=primVertCount-1;
				}

				lastEdgePointIndex=rEdge.m_pointIndex;

				rSubuvs[0]=subuv[subIndex];

				rSubuvs[1]=0.5*(subuv[subIndex]+subuv[(subIndex+1)%subCount]);

				rSubuvs[2]=subuvSum;

				rSubuvs[3]=0.5*(subuv[subIndex]+
						subuv[(subIndex+subCount-1)%subCount]);

				if(m_uvRate==e_primitive)
				{
					FEASSERT(pUVs);

					(*pUVs)[0]=uv[subIndex];

					(*pUVs)[1]=0.5*(uv[subIndex]+uv[(subIndex+1)%subCount]);

					(*pUVs)[2]=uvSum;

					(*pUVs)[3]=0.5*
							(uv[subIndex]+uv[(subIndex+subCount-1)%subCount]);
				}

				if(generateHull)
				{
					String addVert;
					if(spInputHullVerts.isValid())
					{
						addVert+=spInputHullVerts->string(pointIndexA);
					}
					if(addVert.empty())
					{
						addVert.sPrintf("%d",pointIndexA);
					}

					if(subIndex)
					{
						hullVerts+=" ";
					}
					hullVerts+=addVert;
				}

#if FE_SDO_PROFILE
				m_spProfileEdgeMap->replace(m_spProfileVertex);
#endif
			}


			spOutputPoint->set(pointIndexOut,pointSum);

			if(spOutputHullVerts.isValid())
			{
				spOutputHullVerts->set(pointIndexOut,hullVerts);
			}

			if(spOutputUV.isValid() && m_uvRate==e_point)
			{
				spOutputUV->set(pointIndexOut,uvSum);
			}

			if(!recomputeNormals && spOutputNormal.isValid())
			{
				FEASSERT(spOutputNormal.isValid());
				spOutputNormal->set(pointIndexOut,normalSum);
			}

#if FE_SDO_PROFILE
			m_spProfileFace->replace(m_spProfileEdgeMap);
#endif

			delete[] pointIndices;
			delete[] uv;
			delete[] subuv;
		}

		primVerts.resize(primVertCount);

#if FE_SDO_PROFILE
		m_spProfileAppend->replace(m_spProfileFace);
#endif

		spOutputVertices->append(primVerts);

		if(spOutputSubuv.isValid())
		{
			for(U32 primVertIndex=0;primVertIndex<primVertCount;primVertIndex++)
			{
				Array<SpatialVector>& rSubuvs=primSubuvs[primVertIndex];

				spOutputSubuv->set(primVertIndex,0,rSubuvs[0]);
				spOutputSubuv->set(primVertIndex,1,rSubuvs[1]);
				spOutputSubuv->set(primVertIndex,2,rSubuvs[2]);
				spOutputSubuv->set(primVertIndex,3,rSubuvs[3]);
			}
		}

		if(m_uvRate==e_primitive && spOutputUV.isValid())
		{
			for(U32 primVertIndex=0;primVertIndex<primVertCount;primVertIndex++)
			{
				Array<SpatialVector>& rUVs=primUVs[primVertIndex];

				spOutputUV->set(primVertIndex,0,rUVs[0]);
				spOutputUV->set(primVertIndex,1,rUVs[1]);
				spOutputUV->set(primVertIndex,2,rUVs[2]);
				spOutputUV->set(primVertIndex,3,rUVs[3]);
			}
		}

		if(spOutputPartition.isValid())
		{
			for(U32 primVertIndex=0;primVertIndex<primVertCount;primVertIndex++)
			{
				String partition;
				if(iteration)
				{
					partition=spInputPartition->string(primOrig[primVertIndex]);
				}
				else
				{
					partition.sPrintf("%d",primOrig[primVertIndex]);
				}

				spOutputPartition->set(primVertIndex,partition);
			}
		}

#if FE_SDO_PROFILE
		m_spProfileEdge->replace(m_spProfileAppend);
#endif

		//* set edge points
		adjustThreads();	//* in case auto was just turned on
		const U32 threadCount=catalog<I32>("Threads");
		setNonAtomicRange(0,threadCount-1);
		runStage("edge");

#if FE_SDO_PROFILE
		m_spProfilePoint->replace(m_spProfileEdge);
#endif

		//* adjust original points
		setAtomicRange(m_spInputAccessible,e_pointsOnly);
		runStage("point");

#if FE_SDO_PROFILE
		m_spProfileExpand->replace(m_spProfilePoint);
#endif

//		expandIncremental();

#if FE_SDO_PROFILE
		m_spProfileAccess->replace(m_spProfileExpand);
#endif

#if FE_SDO_DEBUG
		feLog("SubdivideOp::handle"
				" iteration %d subdivided points %d primitives %d\n",
				iteration,spOutputPoint->count(),spOutputVertices->count());
#endif

		delete[] normalsIn;
		delete[] pointsIn;
	}

	expandPost();

	if(generateHull && !writeHull)
	{
		m_spOutputAccessible->discard(SurfaceAccessibleI::e_point,hullAttr);
	}

	if(recomputeNormals)
	{
#if FE_SDO_PROFILE
		m_spProfileNormal->replace(m_spProfileAccess);
#endif

		sp<Catalog> spOtherCatalog=m_spSurfaceNormalOp;
		if(spOtherCatalog.isValid())
		{
			spOtherCatalog->catalog<I32>("Threads")=
					catalog<I32>("Threads");
			spOtherCatalog->catalog<bool>("AutoThread")=
					catalog<bool>("AutoThread");
			spOtherCatalog->catalog<bool>("Paging")=
					catalog<bool>("Paging");
			spOtherCatalog->catalog<bool>("StayAlive")=
					catalog<bool>("StayAlive");
		}

		m_spSurfaceNormalOp->handle(a_rSignal);

#if FE_SDO_PROFILE
		m_spProfileAccess->replace(m_spProfileNormal);
#endif
	}

	clearData();

#if FE_SDO_PROFILE
	m_spProfileAccess->finish();
	m_spProfiler->end();

	feLog("%s:\n%s\n",m_spProfiler->name().c_str(),
			m_spProfiler->report().c_str());
#endif
}

void SubdivideOp::run(I32 a_id,sp<SpannedRange> a_spRange,String a_stage)
{
#if FE_SDO_DEBUG
	feLog("SubdivideOp::run id %d stage \"%s\" range %s\n",
			a_id,a_stage.c_str(),a_spRange->brief().c_str());
#endif

	if(a_stage=="edge")
	{
		run_edge(a_id,a_spRange);
	}
	else if(a_stage=="point")
	{
		run_point(a_id,a_spRange);
	}
}

void SubdivideOp::run_edge(I32 a_id,sp<SpannedRange> a_spRange)
{
#if FE_SDO_DEBUG
	feLog("SubdivideOp::run_edge id %d\n",a_id);
#endif

	const BWORD subdivideNormals=
			(m_normalsIn && !catalog<bool>("recomputeNormals"));
	const BWORD wrapUVs=catalog<bool>("wrapUVs");
	const BWORD bridgeUVs=catalog<bool>("bridgeUVs");
	const Real bridgeLo=catalog<Real>("bridgeLo");
	const Real bridgeHi=catalog<Real>("bridgeHi");
	const String hullAttr=catalog<String>("hullAttr");
	const BWORD writeHull=(!hullAttr.empty() && catalog<bool>("writeHull"));
	const BWORD expand=catalog<bool>("expand");
	const BWORD generateHull=(writeHull || expand);

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputNormal;
	if(subdivideNormals &&
			!access(spOutputNormal,m_spOutputAccessible,
			e_point,e_normal)) return;

	sp<SurfaceAccessorI> spInputUV;
	access(spInputUV,m_spInputAccessible,m_uvRate,e_uv,e_quiet,e_refuseMissing);

	sp<SurfaceAccessorI> spOutputUV;
	if(spInputUV.isValid())
	{
		if(!access(spOutputUV,m_spOutputAccessible,m_uvRate,e_uv)) return;
	}

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,m_spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputHullVerts;
	sp<SurfaceAccessorI> spOutputHullVerts;
	if(generateHull)
	{
		access(spInputHullVerts,m_spInputAccessible,
				e_point,hullAttr,e_quiet);

		if(!access(spOutputHullVerts,m_spOutputAccessible,
				e_point,hullAttr)) return;
	}

	//* assuming contiguous
	SpannedRange::Iterator it=a_spRange->begin();
	if(it.atEnd())
	{
		return;
	}

	const U32 modulus=it.value();

#if FE_SDO_THREAD
	const U32 pointCountIn=spInputPoint->count();

	const U32 threadCount=catalog<I32>("Threads");
	const I32 pointMin=modulus*pointCountIn/threadCount;
	const I32 pointMax=(modulus==threadCount-1)? pointCountIn-1:
			(modulus+1)*pointCountIn/threadCount-1;

//	feLog("mod %d/%d count %d span %d %d\n",
//			modulus,threadCount,pointCountIn,pointMin,pointMax);

#else
	//* all on one thread
	if(modulus)
	{
		return;
	}
//	const I32 pointMin=0;
//	const I32 pointMax=pointCountIn-1;
#endif

	for(std::map<U64,Edge>::iterator it=m_edgeMap.begin();
			it!=m_edgeMap.end();it++)
	{
		if(interrupted())
		{
			break;
		}

		const U64 combo=it->first;
		const Edge& rEdge=it->second;

		const I32 pointIndexA=combo>>32;
		const I32 pointIndexB=combo&0xFFFFFFFF;

		SpatialVector uvA(0.0,0.0,0.0);
		SpatialVector uvB(0.0,0.0,0.0);
		if(spInputUV.isValid() && m_uvRate==e_point)
		{
			uvA=spInputUV->spatialVector(pointIndexA);
			uvB=spInputUV->spatialVector(pointIndexB);
		}

#if FE_SDO_THREAD
		const BWORD changeA=(pointIndexA>=pointMin && pointIndexA<=pointMax);
		const BWORD changeB=(pointIndexB>=pointMin && pointIndexB<=pointMax);

		if(!changeA && !changeB)
		{
			continue;
		}
#endif

		const SpatialVector pointA=m_pointsIn[pointIndexA];
		const SpatialVector pointB=m_pointsIn[pointIndexB];

		SpatialVector pointSum=pointA+pointB;
		SpatialVector uvSum=uvA+uvB;

		SpatialVector normalA;
		SpatialVector normalB;
		SpatialVector normalSum;
		if(subdivideNormals)
		{
			normalA=m_normalsIn[pointIndexA];
			normalB=m_normalsIn[pointIndexB];
			normalSum=normalA+normalB;
		}

		const SpatialVector midPoint=0.5*pointSum;
		SpatialVector midUv=0.5*uvSum;
		I32 divisor=2;

		//* UV wrap-around
		Vector2i highUV(
				(FE_SDO_UV_HI(uvA[0]) || FE_SDO_UV_HI(uvB[0])),
				(FE_SDO_UV_HI(uvA[1]) || FE_SDO_UV_HI(uvB[1])));
		Vector2i lowUV(
				FE_SDO_UV_LO(uvA[0])+FE_SDO_UV_LO(uvB[0]),
				FE_SDO_UV_LO(uvA[1])+FE_SDO_UV_LO(uvB[1]));

		if(bridgeUVs)
		{
			if(highUV[0])
			{
				midUv[0]+=0.5*lowUV[0];
			}
			if(highUV[1])
			{
				midUv[1]+=0.5*lowUV[1];
			}
		}

		const std::set<I32>& rFaceSet=rEdge.m_faceSet;

		if(!m_flat && rFaceSet.size()>1)
		{
			//* also add adjacent face points
			for(std::set<I32>::iterator it=rFaceSet.begin();
					it!=rFaceSet.end();it++)
			{
				const I32 primitiveIndexIn=*it;
				const I32 pointIndexOut=m_facePointIndex[primitiveIndexIn];

				pointSum+=spOutputPoint->spatialVector(pointIndexOut);
				divisor++;

				if(subdivideNormals)
				{
					normalSum+=spOutputNormal->spatialVector(pointIndexOut);
				}
			}

#if FE_SDO_THREAD
			if(changeA)
#endif
			{
				m_midPointSum[pointIndexA]+=midPoint;
				m_midpointCount[pointIndexA]++;

#if FE_SDO_VERBOSE
				feLog("midSum A %d + %s = %s count %d\n",
						pointIndexA,c_print(midPoint),
						c_print(m_midPointSum[pointIndexA]),
						m_midpointCount[pointIndexA]);
#endif
			}

#if FE_SDO_THREAD
			if(changeB)
#endif
			{
				m_midPointSum[pointIndexB]+=midPoint;
				m_midpointCount[pointIndexB]++;

#if FE_SDO_VERBOSE
				feLog("midSum B %d + %s = %s count %d\n",
						pointIndexB,c_print(midPoint),
						c_print(m_midPointSum[pointIndexB]),
						m_midpointCount[pointIndexB]);
#endif
			}
		}
		else
		{
#if FE_SDO_THREAD
			if(changeA)
#endif
			{
				m_boundaryPointSum[pointIndexA]+=midPoint;
				m_boundaryCount[pointIndexA]++;

#if FE_SDO_VERBOSE
				feLog("boundSum A %d + %s = %s count %d\n",
						pointIndexA,c_print(midPoint),
						c_print(m_boundaryPointSum[pointIndexA]),
						m_boundaryCount[pointIndexA]);
#endif
			}

#if FE_SDO_THREAD
			if(changeB)
#endif
			{
				m_boundaryPointSum[pointIndexB]+=midPoint;
				m_boundaryCount[pointIndexB]++;

#if FE_SDO_VERBOSE
				feLog("boundSum B %d + %s = %s count %d\n",
						pointIndexB,c_print(midPoint),
						c_print(m_boundaryPointSum[pointIndexB]),
						m_boundaryCount[pointIndexB]);
#endif
			}
		}

		//* NOTE rEdge.m_pointIndex isn't pointIndexA,
		//* but this should keep it exclusive
#if FE_SDO_THREAD
		if(!changeA)
		{
			continue;
		}
#endif

		if(bridgeUVs)
		{
			if(highUV[0])
			{
				uvSum[0]+=Real(lowUV[0]);
			}
			if(highUV[1])
			{
				uvSum[1]+=Real(lowUV[1]);
			}
		}

		const Real scalar=1.0/Real(divisor);

		pointSum*=scalar;

		uvSum*=0.5;

		FE_SDO_WRAP_UV(wrapUVs,uvSum);

		spOutputPoint->set(rEdge.m_pointIndex,pointSum);

		if(generateHull)
		{
			String addVertA;
			String addVertB;

			if(spInputHullVerts.isValid())
			{
				addVertA=spInputHullVerts->string(pointIndexA);
				addVertB=spInputHullVerts->string(pointIndexB);
			}
			if(addVertA.empty())
			{
				addVertA.sPrintf("%d",pointIndexA);
			}
			if(addVertB.empty())
			{
				addVertB.sPrintf("%d",pointIndexB);
			}

			String hullVerts=addVertA+" "+addVertB;
			spOutputHullVerts->set(rEdge.m_pointIndex,hullVerts);
		}

		//* NOTE already set when e_primitive
		if(spOutputUV.isValid() && m_uvRate==e_point)
		{
			spOutputUV->set(rEdge.m_pointIndex,uvSum);
		}

		if(subdivideNormals)
		{
			normalSum=unitSafe(normalSum);

			spOutputNormal->set(rEdge.m_pointIndex,normalSum);
		}
	}
}

void SubdivideOp::run_point(I32 a_id,sp<SpannedRange> a_spRange)
{
#if FE_SDO_DEBUG
	feLog("SubdivideOp::run_point id %d\n",a_id);
#endif

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	I32 faults=0;

	if(m_flat)
	{
		for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
		{
			const U32 pointIndexIn=it.value();

			spOutputPoint->set(pointIndexIn,m_pointsIn[pointIndexIn]);
		}
	}
	else
	{
		for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
		{
			if(interrupted())
			{
				break;
			}

			const U32 pointIndexIn=it.value();

			const SpatialVector pointIn=m_pointsIn[pointIndexIn];

			const I32 pointFaceCount=m_faceCount[pointIndexIn];
			if(pointFaceCount<1)
			{
				String message;
				message.sPrintf("pointIndex %d has zero adjacent faces;",
						pointIndexIn);
				catalog<String>("warning")+=message;

				continue;
			}

			const I32 pointBoundaryCount=m_boundaryCount[pointIndexIn];

			const SpatialVector facePointAverage=
					m_facePointSum[pointIndexIn]/Real(pointFaceCount);

			SpatialVector pointOut;

			if(pointFaceCount==1)
			{
				//* boundary corner
				pointOut=pointIn;
			}
//~			else if(pointFaceCount>2 || pointBoundaryCount<1)
			else if(pointBoundaryCount<1)
			{
				if(pointFaceCount<3)
				{
					FEASSERT(!pointBoundaryCount);

					String message;
					message.sPrintf("pointIndex %d is used on only %d %s,"
							" but has no adjacent boundary edges;",
							pointIndexIn,pointFaceCount,
							pointFaceCount==1? "face": "faces");
					catalog<String>("warning")+=message;
				}

				const I32 divisor=m_midpointCount[pointIndexIn]+
						pointBoundaryCount;
				FEASSERT(divisor>0);
				if(divisor<1)
				{
					String message;
					message.sPrintf("pointIndex %d has a zero divisor;",
							pointIndexIn);
					catalog<String>("error")+=message;

					faults++;
					continue;
				}

				const SpatialVector midPointAverage=
						(m_midPointSum[pointIndexIn]+
						m_boundaryPointSum[pointIndexIn])/Real(divisor);

				pointOut=(facePointAverage+2.0*midPointAverage+
						(pointFaceCount-3)*pointIn)/Real(pointFaceCount);

#if FE_SDO_VERBOSE
				feLog("mid %d f %d b %d\n",
						pointIndexIn,pointFaceCount,pointBoundaryCount);
#endif
			}
			else
			{
				//* TODO find out what everyone else does on boundaries

				FEASSERT(pointBoundaryCount>0);

				const SpatialVector boundaryPointAverage=
						m_boundaryPointSum[pointIndexIn]/
						Real(pointBoundaryCount);

				pointOut=0.5*(boundaryPointAverage+pointIn);

#if FE_SDO_VERBOSE
				feLog("boundary %d f %d b %d in %s bpa %s\n",
						pointIndexIn,pointFaceCount,pointBoundaryCount,
						c_print(pointIn),c_print(boundaryPointAverage));
#endif
			}

			//* only storing change along normal
			if(m_normalsIn)
			{
				const SpatialVector norm=m_normalsIn[pointIndexIn];
				m_expand[pointIndexIn]=dot(norm,pointIn-pointOut);
			}

			spOutputPoint->set(pointIndexIn,pointOut);
		}
	}

	if(faults)
	{
		feX("SubdivideOp::run_point","input mesh is non-compliant");
	}
}

void SubdivideOp::expandIncremental(void)
{
#if FE_SDO_DEBUG
	feLog("SubdivideOp::expandIncremental\n");
#endif

	const BWORD expand=catalog<bool>("expand");
	const Real expansion=expand? catalog<Real>("expansion"): 0.0;
	if(expansion==0.0)
	{
		return;
	}

	const I32 pointCountIn=m_expand.size();

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,
			e_primitive,e_vertices)) return;

	const I32 primitiveCountIn=spInputVertices->count();

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

#if FE_CODEGEN<=FE_DEBUG
	const I32 pointCountOut=spOutputPoint->count();
#endif

	for(I32 pointIndexOut=0;pointIndexOut<pointCountIn;pointIndexOut++)
	{
		FEASSERT(pointIndexOut<pointCountOut);

		const SpatialVector point=spOutputPoint->spatialVector(pointIndexOut);
		const SpatialVector norm=m_normalsIn[pointIndexOut];

		const Real scalar=expansion*m_expand[pointIndexOut];
		const SpatialVector expanded=point+norm*scalar;

		spOutputPoint->set(pointIndexOut,expanded);

#if FE_SDO_EXPAND_DEBUG
		feLog("expand orig pt %d along %s by %.6G\n",
				pointIndexOut,c_print(norm),scalar);
		feLog("  from %s radius %.6G\n",
				c_print(point),magnitude(point));
		feLog("  to %s radius %.6G\n",
				c_print(expanded),magnitude(expanded));
#endif
	}

	for(I32 primitiveIndexIn=0;primitiveIndexIn<primitiveCountIn;
			primitiveIndexIn++)
	{
		const I32 pointIndexOut=pointCountIn+primitiveIndexIn;
		FEASSERT(pointIndexOut<pointCountOut);

		SpatialVector normalSum(0.0,0.0,0.0);
		Real expandSum=0.0;

		const U32 subCount=spInputVertices->subCount(primitiveIndexIn);
		if(!subCount)
		{
			continue;
		}

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndexIn=
					spInputVertices->integer(primitiveIndexIn,subIndex);
			normalSum+=m_normalsIn[pointIndexIn];
			expandSum+=m_expand[pointIndexIn];
		}

		normalizeSafe(normalSum);

		const SpatialVector point=spOutputPoint->spatialVector(pointIndexOut);

		const Real scalar=expansion*expandSum/Real(subCount);
		const SpatialVector expanded=point+normalSum*scalar;

		spOutputPoint->set(pointIndexOut,expanded);

#if FE_SDO_EXPAND_DEBUG
		feLog("expand face pt %d along %s by %.6G\n",
				pointIndexOut,c_print(normalSum),scalar);
		feLog("  from %s radius %.6G\n",
				c_print(point),magnitude(point));
		feLog("  to %s radius %.6G\n",
				c_print(expanded),magnitude(expanded));
#endif
	}

	for(std::map<U64,Edge>::iterator it=m_edgeMap.begin();
			it!=m_edgeMap.end();it++)
	{
		const U64 combo=it->first;
		const Edge& rEdge=it->second;

		const I32 pointIndexA=combo>>32;
		const I32 pointIndexB=combo&0xFFFFFFFF;

		const I32 pointIndexOut=rEdge.m_pointIndex;
		FEASSERT(pointIndexOut<pointCountOut);

		const SpatialVector point=spOutputPoint->spatialVector(pointIndexOut);
		const SpatialVector norm=
				unitSafe(m_normalsIn[pointIndexA]+m_normalsIn[pointIndexB]);

		const Real scalar=
				expansion*0.5*(m_expand[pointIndexA]+m_expand[pointIndexB]);
		const SpatialVector expanded=point+norm*scalar;

		spOutputPoint->set(pointIndexOut,expanded);

#if FE_SDO_EXPAND_DEBUG
		feLog("expand edge pt %d along %s by %.6G\n",
				pointIndexOut,c_print(norm),scalar);
		feLog("  from %s radius %.6G\n",
				c_print(point),magnitude(point));
		feLog("  to %s radius %.6G\n",
				c_print(expanded),magnitude(expanded));
#endif
	}
}

void SubdivideOp::expandPost(void)
{
#if FE_SDO_DEBUG
	feLog("SubdivideOp::expandPost\n");
#endif

	const BWORD expand=catalog<bool>("expand");
	const Real expansion=expand? catalog<Real>("expansion"): 0.0;
	if(expansion==0.0)
	{
		return;
	}

	const String hullAttr=catalog<String>("hullAttr");

	sp<SurfaceAccessibleI> spOriginalAccessible;
	if(!access(spOriginalAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spOriginalPoint;
	if(!access(spOriginalPoint,spOriginalAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOriginalNormal;
	if(!access(spOriginalNormal,spOriginalAccessible,e_point,e_normal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputHullVerts;
	if(!access(spOutputHullVerts,m_spOutputAccessible,
			e_point,hullAttr)) return;

	const U32 pointCountOrig=spOriginalPoint->count();

	SpatialVector* normalIn=new SpatialVector[pointCountOrig];

	Real* offset=new Real[pointCountOrig];
	for(U32 pointIndexOrig=0;pointIndexOrig<pointCountOrig;pointIndexOrig++)
	{
		normalIn[pointIndexOrig]=
				spOriginalNormal->spatialVector(pointIndexOrig);
		offset[pointIndexOrig]=expansion*
				dot(spOriginalPoint->spatialVector(pointIndexOrig)-
				spOutputPoint->spatialVector(pointIndexOrig),
				normalIn[pointIndexOrig]);
	}

	const U32 pointCountOut=spOutputPoint->count();
	for(U32 pointIndexOut=0;pointIndexOut<pointCountOut;pointIndexOut++)
	{
		String hullVerts=spOutputHullVerts->string(pointIndexOut);
		U32 hullCount=0;

		Real change=0.0;
		SpatialVector norm(0.0,0.0,0.0);

		String token;
		while(!(token=hullVerts.parse()).empty())
		{
			const I32 pointIndexIn=token.integer();

//			feLog("expand %d with %d\n",pointIndexOut,pointIndexIn);

			change+=offset[pointIndexIn];
			norm+=normalIn[pointIndexIn];
			hullCount++;
		}

		if(hullCount)
		{
			const SpatialVector point=
					spOutputPoint->spatialVector(pointIndexOut);
			const SpatialVector expanded=point+
					unitSafe(norm)*(change/Real(hullCount));

			spOutputPoint->set(pointIndexOut,expanded);
		}
	}

	delete[] offset;
	delete[] normalIn;
}

} /* namespace ext */
} /* namespace fe */
