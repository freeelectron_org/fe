/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#ifndef __MESSAGEI_H__
#define __MESSAGEI_H__

#pragma pack(push, 1)

#define MESSAGEGRAM_MAXDATA 1400
#define MESSAGE_UDP_MAXDATA 65507
#define MAX_RANDOM_BIND_TRIES 1024

namespace fe
{
namespace ext
{

//~typedef unsigned int uint32;
//~typedef unsigned short uint16;
//~typedef unsigned char uint8;

// message payload
struct Messagegram
{
	uint16_t	dataLen;
	uint8_t		data[MESSAGE_UDP_MAXDATA];
};

/**************************************************************************//**
    @brief Message Sender/Receiver

	@ingroup message
*//***************************************************************************/
class FE_DL_EXPORT MessageI : virtual public fe::Component, public fe::CastableAs<MessageI>
{
public:
	virtual bool recvFrom(Messagegram *) = 0;
	virtual bool recvFrom(Messagegram *, long seconds, long uSeconds) = 0;
	virtual bool recvLargeFrom(Messagegram *) = 0;
	virtual void getRecvAddress(uint32_t &address) = 0;
	virtual void convertAddressToStr(const uint32_t address, char *addStr) = 0;
	virtual bool replyLast(const Messagegram &) = 0;
	virtual bool sendLargeTo(const Messagegram &, const char *, uint16_t) = 0;
	virtual bool sendTo(const Messagegram &, const char *, uint16_t) = 0;
	virtual bool bind(const char *, uint16_t) = 0;
	virtual bool bind(uint16_t) = 0;
	virtual uint16_t bindRandom() = 0;
	virtual bool postInit() = 0;
	virtual bool setReceiveBufferSize(int bufferSize) = 0;
	virtual bool setSendBufferSize(int bufferSize) = 0;
	virtual bool setBroadcast(bool boardcast) = 0;
	virtual bool setReusePort(bool reuse) = 0;
	virtual bool setReuseAddress(bool reuse) = 0;
	virtual void shutdown() = 0;
};

} // namespace ext
} // namespace fe

#pragma pack(pop)

#endif // __MESSAGEI_H__
