/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

namespace fe
{

template<class T, int N>
class InfoVectorF32 : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					const Vector<N,T> *pV = (Vector<N,T> *)instance;
					String string;
					for(U32 n=0;n<N;n++)
						string.catf("%.6G%s",(*pV)[n],(n==N-1)? "": " ");
					return string;
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii)
					{
						ostrm << '"';
					}
					IWORD n = 0;
					Vector<N,T> *pV = (Vector<N,T> *)instance;
					for(int i = 0; i < N; i++)
					{
						T t=(*pV)[i];
						if(i && mode == e_ascii)
						{
							ostrm << ' ';
						}
						n += helpF32.output(ostrm, (void *)&t, mode);
					}
					if(mode == e_ascii)
					{
						ostrm << '"';
					}
					return (mode == e_ascii)? c_ascii: n;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					Vector<N,T> *pV = (Vector<N,T> *)instance;
					for(int i = 0; i < N; i++)
					{
						T t=0.0f;
						helpF32.input(istrm, (void *)&t, mode);
						setAt(*pV,i,t);
					}
				}
virtual	IWORD	iosize(void) { return sizeof(Vector<N,T>); }
virtual	FE_UWORD	alignment(void)
				{
#if FE_MEM_ALIGNMENT
					return (N*sizeof(T)==FE_MEM_ALIGNMENT ||
							(N+1)*sizeof(T)==FE_MEM_ALIGNMENT)?
							FE_MEM_ALIGNMENT: 0;
#else
					return 0;
#endif
				}
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	set(*new(instance)Vector<N,T>); }
virtual	void	destruct(void *instance)
				{	((Vector<N,T> *)instance)->~Vector<N,T>();}
	private:
		InfoF32 helpF32;
};

template<class T, int N>
class InfoVectorF64 : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					const Vector<N,T> *pV = (Vector<N,T> *)instance;
					String string;
					for(U32 n=0;n<N;n++)
						string.catf("%.15G%s",(*pV)[n],(n==N-1)? "": " ");
					return string;
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii)
			{
				String text=InfoVectorF64::print(instance);

				ostrm << '"' << text.c_str() << '"';
				return c_ascii;
			}
			IWORD n = 0;
			Vector<N,T> *pV = (Vector<N,T> *)instance;
			for(int i = 0; i < N; i++)
			{
				T t=(*pV)[i];
				n += helpF64.output(ostrm, (void *)&t, mode);
			}
			return n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			Vector<N,T> *pV = (Vector<N,T> *)instance;
			for(int i = 0; i < N; i++)
			{
				T t=0.0f;
				helpF64.input(istrm, (void *)&t, mode);
				setAt(*pV,i,t);
			}
		}
virtual	IWORD	iosize(void) { return sizeof(Vector<N,T>); }
virtual	FE_UWORD	alignment(void)
				{
#if FE_MEM_ALIGNMENT
					return (N*sizeof(T)==FE_MEM_ALIGNMENT)?
							FE_MEM_ALIGNMENT: 0;
#else
					return 0;
#endif
				}
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	set(*new(instance)Vector<N,T>); }
virtual	void	destruct(void *instance)
				{	((Vector<N,T> *)instance)->~Vector<N,T>();}
	private:
		InfoF64 helpF64;
};

template<class T, int N>
class InfoVectorI32 : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					const Vector<N,T> *pV = (Vector<N,T> *)instance;
					String string;
					for(U32 n=0;n<N;n++)
						string.catf("%.6d%s",(*pV)[n],(n==N-1)? "": " ");
					return string;
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			IWORD n = 0;
			Vector<N,T> *pV = (Vector<N,T> *)instance;
			for(int i = 0; i < N; i++)
			{
				T t=(*pV)[i];
				if(i && mode == e_ascii)
				{
					ostrm << ' ';
				}
				n += helpI32.output(ostrm, (void *)&t, mode);
			}
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			return (mode == e_ascii)? c_ascii: n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			Vector<N,T> *pV = (Vector<N,T> *)instance;
			for(int i = 0; i < N; i++)
			{
				T t;
				helpI32.input(istrm, (void *)&t, mode);
				setAt(*pV,i,t);
			}
		}
virtual	IWORD	iosize(void) { return sizeof(Vector<N,T>); }
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	new(instance)Vector<N,T>; }
virtual	void	destruct(void *instance)
				{	((Vector<N,T> *)instance)->~Vector<N,T>(); }
	private:
		InfoI32 helpI32;
};

template<class T, int M, int N>
class InfoMatrixF32 : public BaseType::Info
{
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			IWORD n = 0;
			Matrix<M,N,T> *pM = (Matrix<M,N,T> *)instance;
			for(int i = 0; i < M; i++)
			{
				for(int j = 0; j < N; j++)
				{
					if((i || j) && mode == e_ascii)
					{
						ostrm << ' ';
					}
					n += helpF32.output(ostrm, (void *)&(*pM)(i,j), mode);
				}
			}
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			return (mode == e_ascii)? c_ascii: n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			Matrix<M,N,T> *pM = (Matrix<M,N,T> *)instance;
			for(int i = 0; i < M; i++)
			{
				for(int j = 0; j < N; j++)
				{
					helpF32.input(istrm, (void *)&(*pM)(i,j), mode);
				}
			}
		}
virtual	IWORD	iosize(void) { return sizeof(Matrix<M,N,T>); }
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	setIdentity(*new(instance)Matrix<M,N,T>); }
virtual	void	destruct(void *instance)
				{	((Matrix<M,N,T> *)instance)->~Matrix<M,N,T>();}
	private:
		InfoF32 helpF32;
};

template<class T, int M, int N, class H>
class InfoMatrix : public BaseType::Info
{
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			IWORD n = 0;
			Matrix<M,N,T> *pM = (Matrix<M,N,T> *)instance;
			for(int i = 0; i < M; i++)
			{
				for(int j = 0; j < N; j++)
				{
					if((i || j) && mode == e_ascii)
					{
						ostrm << ' ';
					}
					n += help.output(ostrm, (void *)&(*pM)(i,j), mode);
				}
			}
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			return (mode == e_ascii)? c_ascii: n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			Matrix<M,N,T> *pM = (Matrix<M,N,T> *)instance;
			for(int i = 0; i < M; i++)
			{
				for(int j = 0; j < N; j++)
				{
					help.input(istrm, (void *)&(*pM)(i,j), mode);
				}
			}
		}
virtual	IWORD	iosize(void) { return sizeof(Matrix<M,N,T>); }
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{
					setIdentity(*new(instance)Matrix<M,N,T>);
				}
virtual	void	destruct(void *instance)
				{	((Matrix<M,N,T> *)instance)->~Matrix<M,N,T>();}
	private:
		H		help;
};

template<class T, class H>
class InfoMatrix3x4 : public BaseType::Info
{
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			IWORD n = 0;
			Matrix<3,4,T> *pM = (Matrix<3,4,T> *)instance;
			for(int i = 0; i < 3; i++)
			{
				for(int j = 0; j < 4; j++)
				{
					if((i || j) && mode == e_ascii)
					{
						ostrm << ' ';
					}
					n += help.output(ostrm, (void *)&(*pM)(i,j), mode);
				}
			}
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			return (mode == e_ascii)? c_ascii: n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			Matrix<3,4,T> *pM = (Matrix<3,4,T> *)instance;
			setIdentity(*pM);
			for(int i = 0; i < 3; i++)
			{
				for(int j = 0; j < 4; j++)
				{
					help.input(istrm, (void *)&(*pM)(i,j), mode);
				}
			}
		}
virtual	IWORD	iosize(void) { return sizeof(Matrix<3,4,T>); }
virtual	FE_UWORD	alignment(void)
				{
#if FE_MEM_ALIGNMENT
					return (4*sizeof(T)==FE_MEM_ALIGNMENT)?
							FE_MEM_ALIGNMENT: 0;
#else
					return 0;
#endif
				}
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{
					setIdentity(*new(instance)Matrix<3,4,T>);
				}
virtual	void	destruct(void *instance)
				{	((Matrix<3,4,T> *)instance)->~Matrix<3,4,T>();}
	private:
		H		help;
};

#if 0
class InfoVector3f : public BaseType::Info
{
virtual	void	output(std::ostream &ostrm, void *instance)
		{
			Vector3f *pV3 = (Vector3f *)instance;
			helpF32.output(ostrm, (void *)&(*pV3)[0]);
			helpF32.output(ostrm, (void *)&(*pV3)[1]);
			helpF32.output(ostrm, (void *)&(*pV3)[2]);
		}
virtual	void	input(std::istream &istrm, void *instance)
		{
			Vector3f *pV3 = (Vector3f *)instance;
			helpF32.input(istrm, (void *)&(*pV3)[0]);
			helpF32.input(istrm, (void *)&(*pV3)[1]);
			helpF32.input(istrm, (void *)&(*pV3)[2]);
		}
virtual	IWORD	iosize(void) { return sizeof(Vector3f); }
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance) { new(instance)Vector3f; }
virtual	void	destruct(void *instance) {((Vector3f *)instance)->~Vector3f();}
	private:
		Info32 helpF32;
};
#endif

template<class T, int N>
class InfoVectorArray:
	public TypeInfoArray< Array< Vector<N,T> > >,
	public CastableAs< InfoVectorArray<T,N> >
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance,
					BaseType::Info::t_serialMode mode)
				{
					if(mode == BaseType::Info::e_binary)
					{
						Array< Vector<N,T> > &rArray =
								*(Array< Vector<N,T> > *)instance;
						long cnt = (long)rArray.size();
						I32 n = htonl(cnt);

						ostrm.write((char *)&n, sizeof(I32));
						for(long i = 0; i < cnt; i++)
						{
							Vector<N,T> vector = (Vector<N,T>)rArray[i];
							ostrm.write((char *)&vector,sizeof(Vector<N,T>));
						}
						return sizeof(I32) + cnt * sizeof(Vector<N,T>);
					}
					else
					{
						I32 precision=7;
#if FE_CPLUSPLUS >= 201103L
						if(std::is_same<T,double>::value)
						{
							precision=15;
						}
#endif

						Array< Vector<N,T> > &rArray =
								*(Array< Vector<N,T> > *)instance;
						long cnt = (long)rArray.size();
						ostrm << cnt;
						for(long i = 0; i < cnt; i++)
						{
							ostrm << ",";
							for(I32 j=0;j<N;j++)
							{
								if(j)
								{
									ostrm << " ";
								}
								ostrm << std::setprecision(precision)
										<< rArray[i][j];
							}
						}
						return BaseType::Info::c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance,
					BaseType::Info::t_serialMode mode)
				{
					if(mode == BaseType::Info::e_binary)
					{
						I32 n;
						istrm.read((char *)&n, sizeof(I32));
						n = ntohl(n);

						Array< Vector<N,T> > &rArray =
								*(Array< Vector<N,T> > *)instance;
						rArray.resize(n);
						for(I32 i = 0; i < n; i++)
						{
							Vector<N,T> vector;
							istrm.read((char *)&vector,sizeof(Vector<N,T>));
							rArray[i] = vector;
						}
					}
					else
					{
						I32 n;
						istrm >> n;
						Array< Vector<N,T> > &rArray =
								*(Array< Vector<N,T> > *)instance;
						rArray.resize(n);
						for(I32 i = 0; i < n; i++)
						{
							char comma;
							istrm >> comma;
							Vector<N,T> vector;
							for(I32 j=0;j<N;j++)
							{
								istrm >> vector[j];
							}
							rArray[i] = vector;
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					Array< Vector<N,T> > &rArray =
								*(Array< Vector<N,T> > *)instance;
					const I32 count = rArray.size();
					const I32 max = 8;
					s.sPrintf("%d:", count);
					for(I32 i = 0; i < count && i < max; i++)
					{
						s.catf(" <%s>", fe::print(rArray[i]).c_str());
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

void assertMath(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spT;
	spT = spTypeMaster->assertType<Vector2f>("vector2f");
	spT->setInfo(new InfoVectorF32<float,2>());

	spT = spTypeMaster->assertType<Vector2d>("vector2d");
	spT->setInfo(new InfoVectorF64<double,2>());

	spT = spTypeMaster->assertType<Vector3f>("vector3f");
	spT->setInfo(new InfoVectorF32<float,3>());

	spT = spTypeMaster->assertType<Vector3d>("vector3d");
	spT->setInfo(new InfoVectorF64<double,3>());

	spT = spTypeMaster->assertType<Vector4f>("vector4f");
	spT->setInfo(new InfoVectorF32<float,4>());

	spT = spTypeMaster->assertType<Vector4d>("vector4d");
	spT->setInfo(new InfoVectorF64<double,4>());

	spT = spTypeMaster->assertType<Quaternionf>("Quaternionf");
	spT->setInfo(new InfoVectorF32<float,4>());
	spT = spTypeMaster->assertType<Quaterniond>("Quaterniond");
	spT->setInfo(new InfoVectorF64<double,4>());
	spT = spTypeMaster->assertType<SpatialQuaternion>("quaternion");
	spT->setInfo(new InfoVectorF32<float,4>());

	spT = spTypeMaster->assertType<Eulerf>("Eulerf");
	spT->setInfo(new InfoVectorF32<float,3>());
	spT = spTypeMaster->assertType<Eulerd>("Eulerd");
	spT->setInfo(new InfoVectorF64<double,3>());
	spT = spTypeMaster->assertType<SpatialEuler>("euler");
	spT->setInfo(new InfoVectorF32<float,3>());

	spT = spTypeMaster->assertType<Vector2i>("vector2i");
	spT->setInfo(new InfoVectorI32<int,2>());

	spT = spTypeMaster->assertType<Vector3i>("vector3i");
	spT->setInfo(new InfoVectorI32<int,3>());

	spT = spTypeMaster->assertType<Vector4i>("vector4i");
	spT->setInfo(new InfoVectorI32<int,4>());

	spT = spTypeMaster->assertType<Vector2>("vector2");
	spT = spTypeMaster->assertType<Vector3>("vector3");
	spT = spTypeMaster->assertType<Vector4>("vector4");
	spT = spTypeMaster->assertType<SpatialVector>("spatial_vector");

	spT = spTypeMaster->assertType<Color>("color");
#if FE_DOUBLE_REAL
	spT->setInfo(new InfoVectorF64<double,4>());
#else
	spT->setInfo(new InfoVectorF32<float,4>());
#endif

	spT = spTypeMaster->assertType< Matrix<3,3,float> >("matrix3x3f");
	spT->setInfo(new InfoMatrixF32<float,3,3>());

	spT = spTypeMaster->assertType< Matrix<3,3,double> >("matrix3x3d");
	spT->setInfo(new InfoMatrix<double,3,3,InfoF64>());

	spT = spTypeMaster->assertType< Matrix<3,3,Real> >("matrix3x3");

	spT = spTypeMaster->assertType< Matrix<4,4,float> >("matrix4x4f");
	spT->setInfo(new InfoMatrixF32<float,4,4>());

	spT = spTypeMaster->assertType< Matrix<4,4,double> >("matrix4x4d");
	spT->setInfo(new InfoMatrix<double,4,4,InfoF64>());

	spT = spTypeMaster->assertType< Matrix<4,4,Real> >("matrix4x4");

	spT = spTypeMaster->assertType<Matrix3x4f>("matrix3x4f");
	spT->setInfo(new InfoMatrix3x4<F32,InfoF32>());

	spT = spTypeMaster->assertType<Matrix<3,4,double> >("matrix3x4d");
	spT->setInfo(new InfoMatrix3x4<double,InfoF64>());

	spT = spTypeMaster->assertType<Matrix<3,4,Real> >("matrix3x4");

	spT = spTypeMaster->assertType<SpatialTransform>("transform");

	spT = spTypeMaster->assertType< Array< Vector<2,int> > >("vector2iarray");
	spT->setInfo(new InfoVectorArray<int,2>());

	spT = spTypeMaster->assertType< Array< Vector<3,int> > >("vector3iarray");
	spT->setInfo(new InfoVectorArray<int,3>());

	spT = spTypeMaster->assertType< Array< Vector<4,int> > >("vector4iarray");
	spT->setInfo(new InfoVectorArray<int,4>());

	spT = spTypeMaster->assertType< Array< Vector<2,float> > >("vector2farray");
	spT->setInfo(new InfoVectorArray<float,2>());

	spT = spTypeMaster->assertType< Array< Vector<3,float> > >("vector3farray");
	spT->setInfo(new InfoVectorArray<float,3>());

	spT = spTypeMaster->assertType< Array< Vector<4,float> > >("vector4farray");
	spT->setInfo(new InfoVectorArray<float,4>());

	spT = spTypeMaster->assertType< Array< Vector<2,double> > >(
			"vector2darray");
	spT->setInfo(new InfoVectorArray<double,2>());

	spT = spTypeMaster->assertType< Array< Vector<3,double> > >(
			"vector3darray");
	spT->setInfo(new InfoVectorArray<double,3>());

	spT = spTypeMaster->assertType< Array< Vector<4,double> > >(
			"vector4darray");
	spT->setInfo(new InfoVectorArray<double,4>());

	spT = spTypeMaster->assertType< Array< Vector<2,Real> > >("vector2array");
	spT = spTypeMaster->assertType< Array< Vector<3,Real> > >("vector3array");
	spT = spTypeMaster->assertType< Array< Vector<4,Real> > >("vector4array");
	spT = spTypeMaster->assertType< Array<SpatialVector> >("vectorarray");
}

} /* namespace */
