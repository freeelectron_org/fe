import os
import sys
forge = sys.modules["forge"]

def get_std():
#   if forge.compiler_brand == 'clang' and forge.compiler_version < 5.0:
#       return "c++1z"

    return "c++17"

def setup(module):
    srcList = [ "stdthread.pmh",
                "stdthreadDL" ]

    module.includemap = {}

    module.cppmap = { 'std' : forge.use_std(get_std()) }

    dll = module.DLL( "fexStdThread", srcList )

    deplibs = forge.basiclibs[:]

    dll.linkmap = {}

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["stdthread"] = "-lpthread"

    forge.deps( ["fexStdThreadLib"], deplibs )

def auto(module):
    test_file = """
#include <thread>

int main(void)
{
    std::thread thread;

    return 0;
}
    \n"""

    std_orig = forge.cppmap.get('std', "")
    forge.cppmap['std'] = forge.use_std(get_std())

    result = forge.cctest(test_file)

    forge.cppmap['std'] = std_orig

    return result
