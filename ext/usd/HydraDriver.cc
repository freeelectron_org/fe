//
// Copyright 2016 Pixar
//
// Licensed under the Apache License, Version 2.0 (the "Apache License")
// with the following modification; you may not use this file except in
// compliance with the Apache License and the following modification to it:
// Section 6. Trademarks. is deleted and replaced with:
//
// 6. Trademarks. This License does not grant permission to use the trade
//	names, trademarks, service marks, or product names of the Licensor
//	and its affiliates, except as required to comply with Section 4(c) of
//	the License and to reproduce the content of the NOTICE file.
//
// You may obtain a copy of the Apache License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Apache License with the above modification is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied. See the Apache License for the specific
// language governing permissions and limitations under the Apache License.

// originally from pxr/imaging/hd/unitTestHelper.cpp
// adapted for Free Electron

#include <usd/hydra.pmh>

#include "pxr/imaging/hd/rprimCollection.h"
#include "pxr/imaging/hd/sceneDelegate.h"
#include "pxr/imaging/hd/mesh.h"

#include "pxr/imaging/hgi/hgi.h"
#include "pxr/imaging/hgi/tokens.h"

#include "pxr/imaging/glf/contextCaps.h"
#include "pxr/imaging/glf/diagnostic.h"

#include "pxr/base/gf/matrix4d.h"
#include "pxr/base/gf/frustum.h"
#include "pxr/base/tf/getenv.h"
#include "pxr/base/tf/staticTokens.h"

#include <boost/functional/hash.hpp>

#include <string>
#include <sstream>

#define FE_HDR_TICKER			FALSE

HydraDriver::HydraDriver(void):
	m_pRenderIndex(nullptr),
	m_pSceneDelegate(nullptr)
{
	TfToken reprName = HdReprTokens->hull;
	if(TfGetenv("HD_ENABLE_SMOOTH_NORMALS", "CPU") == "CPU" or
			TfGetenv("HD_ENABLE_SMOOTH_NORMALS", "CPU") == "GPU")
	{
		reprName = HdReprTokens->smoothHull;
	}
	_Init(reprName);
}

HydraDriver::HydraDriver(TfToken const &reprName):
	m_pRenderIndex(nullptr),
	m_pSceneDelegate(nullptr)
{
	_Init(reprName);
}

HydraDriver::~HydraDriver(void)
{
	delete m_pRenderIndex;
}

void HydraDriver::_Init(TfToken const &reprName)
{
#if HD_API_VERSION>=35
	m_hgi=Hgi::CreatePlatformDefaultHgi();
#else
	m_hgi=std::unique_ptr<class Hgi>(Hgi::GetPlatformDefaultHgi());
#endif

	m_hgiDriver={HgiTokens->renderDriver, VtValue(m_hgi.get())};

#if 0
	std::unique_ptr<class Hgi> hgi = Hgi::CreatePlatformDefaultHgi();
	HdDriver hgiDriver;
	hgiDriver.name = HgiTokens->renderDriver;
	hgiDriver.driver = VtValue(hgi.get());

	m_pRenderIndex = HdRenderIndex::New(&m_renderDelegate, {&hgiDriver});
#else
	HdDriverVector driverVector(1);
	driverVector[0]=&m_hgiDriver;
	m_pRenderIndex = HdRenderIndex::New(&m_renderDelegate, driverVector);
#endif

	TF_VERIFY(m_pRenderIndex != nullptr);

	GlfContextCaps::InitInstance();

//	m_renderPassState=std::dynamic_pointer_cast<HdStRenderPassState>(
//			m_renderDelegate.CreateRenderPassState());

	m_pSceneDelegate = new HydraDelegate(
			m_pRenderIndex, SdfPath::AbsoluteRootPath());

	m_reprName = reprName;

	GfMatrix4d viewMatrix = GfMatrix4d().SetIdentity();
	viewMatrix *= GfMatrix4d().SetTranslate(GfVec3d(0.0, 1000.0, 0.0));
	viewMatrix *= GfMatrix4d().SetRotate(
			GfRotation(GfVec3d(1.0, 0.0, 0.0), -90.0));

	GfFrustum frustum;
	frustum.SetPerspective(45, true, 1, 1.0, 10000.0);
	GfMatrix4d projMatrix = frustum.ComputeProjectionMatrix();

	SetCamera(viewMatrix, projMatrix, GfVec4d(0, 0, 512, 512));
}

void HydraDriver::Draw(HdEngine& a_rEngine,bool withGuides)
{
	Draw(a_rEngine,GetRenderPass(withGuides));
}

void HydraDriver::Draw(HdEngine& a_rEngine,
	HdRenderPassSharedPtr const &renderPass)
{
#if FE_HDR_TICKER
	const unsigned long tickStart=fe::systemTick();
#endif

#if HD_API_VERSION>=35
	HdRenderIndex& rRenderIndex=m_pSceneDelegate->GetRenderIndex();
	HdTaskSharedPtr task=rRenderIndex.GetTask(SdfPath("/RenderSetupTask"));
	HdxRenderSetupTaskSharedPtr renderSetupTask=
			std::dynamic_pointer_cast<HdxRenderSetupTask>(task);
	m_renderPassState=renderSetupTask->GetRenderPassState();
	if(m_renderPassState)
	{
		// set depthfunc to GL default
		m_renderPassState->SetDepthFunc(HdCmpFuncLess);
	}
#else
	m_renderPassState=std::dynamic_pointer_cast<HdStRenderPassState>(
			m_renderDelegate.CreateRenderPassState());
#endif

	//* protect OpenGL state
//	glPushAttrib(GL_ALL_ATTRIB_BITS);
//	glPushAttrib(GL_COLOR_BUFFER_BIT);

	VtValue vtValue(m_renderPassState);
	a_rEngine.SetTaskContextData(HdxTokens->renderPassState,vtValue);

	HdTaskSharedPtrVector tasks=m_pSceneDelegate->GetRenderTasks();

	a_rEngine.Execute(&m_pSceneDelegate->GetRenderIndex(),&tasks);

//	glPopAttrib();

	GLF_POST_PENDING_GL_ERRORS();

#if FE_HDR_TICKER
	const unsigned long tickEnd=fe::systemTick();
	const unsigned long tickDiff=
			fe::SystemTicker::tickDifference(tickStart,tickEnd);
	const float ms=1e-3f*tickDiff*fe::SystemTicker::microsecondsPerTick();
//	if(ms>1.0)
	{
		feLog("HydraDriver::Draw execute %.6G ms\n",ms);
	}
#endif
}

void HydraDriver::SetCamera(GfMatrix4d const &modelViewMatrix,
	GfMatrix4d const &projectionMatrix,GfVec4d const &viewport)
{
	m_pSceneDelegate->SetCamera(modelViewMatrix,projectionMatrix);

	//* TODO get name from delegate (or set the delegate)
	SdfPath cameraId = SdfPath("/camera");

	HdSprim const *cam = m_pRenderIndex->GetSprim(
			HdPrimTypeTokens->camera, cameraId);

	if(m_renderPassState)
	{
		m_renderPassState->SetCameraAndViewport(
				dynamic_cast<HdCamera const*>(cam),viewport);
	}
}

HdRenderPassSharedPtr const& HydraDriver::GetRenderPass(bool withGuides)
{
	if(withGuides)
	{
		if(!m_geomAndGuidePass)
		{
			m_geomAndGuidePass = HdRenderPassSharedPtr(
					new NullRenderPass(&m_pSceneDelegate->GetRenderIndex(),
					HdRprimCollection(TestTokens->geometryAndGuides,
					HdReprSelector(m_reprName))));
		}
		return m_geomAndGuidePass;
	}
	else
	{
		if(!m_geomPass)
		{
			m_geomPass = HdRenderPassSharedPtr(
					new NullRenderPass(&m_pSceneDelegate->GetRenderIndex(),
					HdRprimCollection(HdTokens->geometry,
					HdReprSelector(m_reprName))));
		}
		return m_geomPass;
	}
}

void HydraDriver::SetRepr(TfToken const &reprName)
{
	m_reprName = reprName;

	if(m_geomAndGuidePass)
	{
		m_geomAndGuidePass->SetRprimCollection(
				HdRprimCollection(TestTokens->geometryAndGuides,
					HdReprSelector(m_reprName)));
	}

	if(m_geomPass)
	{
		m_geomPass->SetRprimCollection(
				HdRprimCollection(HdTokens->geometry,
					HdReprSelector(m_reprName)));
	}
}
