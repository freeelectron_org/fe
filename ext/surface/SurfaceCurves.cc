/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SC_SAMPLE_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceCurves::SurfaceCurves(void)
{
	m_pImpactPool=&m_curveImpactPool;
}

Protectable* SurfaceCurves::clone(Protectable* pInstance)
{
	feLog("SurfaceCurves::clone\n");

	SurfaceCurves* pSurfaceCurve= pInstance?
			fe_cast<SurfaceCurves>(pInstance): new SurfaceCurves();

	checkCache();

	//* TODO

	return pSurfaceCurve;
}

void SurfaceCurves::cache(void)
{
	if(!m_record.isValid())
	{
		feLog("SurfaceCurves::cache record invalid\n");
		return;
	}

	//* NOTE not using SurfaceSearchable::cache()
	SurfaceSphere::cache();

	m_elements=0;
	m_vertices=0;

	//* TODO

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_record);

	const SpatialVector zero(0.0,0.0,0.0);

	sp<RecordGroup> spPointRG=surfaceModelRV.surfacePointRG();
	for(RecordGroup::iterator it=spPointRG->begin();it!=spPointRG->end();it++)
	{
		sp<RecordArray> spRA= *it;

		RecordArrayView<SurfacePoint> surfacePointRAV;
		surfacePointRAV.bind(spRA);
		for(SurfacePoint& surfacePointRV: surfacePointRAV)
		{
			const SpatialVector& at=surfacePointRV.at();
//			const SpatialVector& up=surfacePointRV.up();

			resizeArrays(m_elements,m_vertices+1);

			SpatialVector& rVertex=m_pVertexArray[m_vertices];

			m_pNormalArray[m_vertices]=zero;
//			m_pRadiusArray[m_vertices]=1.0;
//			m_pColorArray[m_vertices]=white;
//			m_pUVArray[m_vertices]=zero;

			m_pPointIndexArray[m_vertices]=m_vertices;
			m_pTriangleIndexArray[m_vertices]= -1;
			m_pPrimitiveIndexArray[m_vertices]= -1;

			rVertex=at;

			feLog("vertex %d  %s\n",m_vertices,c_print(rVertex));
			m_vertices++;
		}
	}

	sp<RecordGroup> spPrimRG=surfaceModelRV.surfacePrimitiveRG();
	for(RecordGroup::iterator it=spPrimRG->begin();it!=spPrimRG->end();it++)
	{
		sp<RecordArray> spRA= *it;

		RecordArrayView<SurfacePrimitive> surfacePrimitiveRAV;
		surfacePrimitiveRAV.bind(spRA);
		for(SurfacePrimitive& surfacePrimitiveRV: surfacePrimitiveRAV)
		{
			const Array<I32>& pointsArray=surfacePrimitiveRV.points();
			const I32 properties=surfacePrimitiveRV.properties();
			const String part=surfacePrimitiveRV.part();

			const I32 partitionIndex=lookupPartition(part);

			const I32 vertexCount=pointsArray.size();
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 pointIndex=pointsArray[vertexIndex];
				feLog("  pointIndex %d\n",pointIndex);

				m_pTriangleIndexArray[pointIndex]=m_elements;
				m_pPrimitiveIndexArray[pointIndex]=m_elements;
				m_pPartitionIndexArray[pointIndex]=partitionIndex;
			}

			resizeArrays(m_elements+1,m_vertices);

			Vector3i& rElement=m_pElementArray[m_elements];

			//* NOTE presumes consecutive vertices
			//* TODO should reorder, if neccessary
			set(rElement,pointsArray[0],vertexCount,properties);

			feLog("element %d  %s\n",m_elements,c_print(rElement));
			m_elements++;
		}
	}

	calcBoundingSphere();
}

sp<SurfaceI::ImpactI> SurfaceCurves::sampleImpact(I32 a_triangleIndex,
	SpatialBary a_barycenter,SpatialVector a_tangent) const
{
	//* TODO clean up duplicate code with sample()
	const I32 startIndex=m_pElementArray[a_triangleIndex][0];
	const I32 subCount=m_pElementArray[a_triangleIndex][1];
	if(startIndex+subCount>I32(m_vertices))
	{
		return sp<SurfaceI::ImpactI>(NULL);
	}

	const Real realIndex=a_barycenter[0]*(subCount-1);
	const I32 subIndex=I32(realIndex>0.0?
			(realIndex<(subCount-1.0)? realIndex: subCount-2.0): 0.0);
	const Real weight1=realIndex-subIndex;
	const Real weight0=1.0-weight1;

	const I32 arrayIndex=startIndex+subIndex;
	const U32 arrayIndex1=(subIndex<subCount-1)? arrayIndex+1: arrayIndex;

	const SpatialVector& rVertex0=m_pVertexArray[arrayIndex];
	const SpatialVector& rVertex1=m_pVertexArray[arrayIndex1];

	const SpatialVector& rNormal0=m_pNormalArray[arrayIndex];
	const SpatialVector& rNormal1=m_pNormalArray[arrayIndex1];

	const SpatialTransform xform=
			sample(a_triangleIndex,a_barycenter,a_tangent);

	//* TODO refactor with nearly identical code in SurfaceTriangles.cc
	sp<Impact> spImpact=m_pImpactPool->get();

	spImpact->setSurface(this);

	spImpact->setTriangleIndex(a_triangleIndex);
	spImpact->setPrimitiveIndex(m_pPrimitiveIndexArray[arrayIndex]);
	spImpact->setPartitionIndex(-1);

	Vector2 uv(0,0);
	SpatialVector du(0,0,0);
	SpatialVector dv(0,0,0);
	SpatialVector tangent(0,0,0);
	if(m_pUVArray)
	{
		const SpatialVector& rUV0=m_pUVArray[arrayIndex];
		const SpatialVector& rUV1=m_pUVArray[arrayIndex1];

		uv=weight0*rUV0+weight1*rUV1;
		triangleDuDv(du,dv,rVertex0,rVertex1,rVertex1,rUV0,rUV1,rUV1);
	}
	if(m_pTangentArray)
	{
		tangent=weight0*m_pTangentArray[arrayIndex]+
				weight1*m_pTangentArray[arrayIndex];
	}
	else if(m_pUVArray)
	{
		const SpatialVector norm=unitSafe(rNormal0*weight0+rNormal1*weight1);
		tangentFromDuDvN(tangent,du,dv,norm);
	}

	spImpact->setUV(uv);
	spImpact->setDu(du);
	spImpact->setDv(dv);
	spImpact->setTangent(tangent);
	spImpact->setBarycenter(a_barycenter);

	spImpact->setPointIndex0(m_pPointIndexArray[arrayIndex]);
	spImpact->setPointIndex1(m_pPointIndexArray[arrayIndex1]);

	spImpact->setVertex0(rVertex0);
	spImpact->setVertex1(rVertex1);

	spImpact->setNormal0(rNormal0);
	spImpact->setNormal1(rNormal1);

	const SpatialVector intersection=xform.translation();
	const Real distance=magnitude(intersection);
	spImpact->setDistance(distance);
	spImpact->setDirection(intersection*(distance>0.0? 1.0/distance: 1.0));

	return spImpact;
}

//* in this context, a_triangleIndex is really the curve primitive index
//* TODO convert triangle logic to segment logic
SpatialTransform SurfaceCurves::sample(I32 a_triangleIndex,
	SpatialBary a_barycenter,SpatialVector a_tangent) const
{
#if FE_SC_SAMPLE_DEBUG
	feLog("SurfaceCurves::sample triangleIndex %d bary %s tan %s\n",
			a_triangleIndex,c_print(a_barycenter),c_print(a_tangent));
#endif

	const I32 startIndex=m_pElementArray[a_triangleIndex][0];
	const I32 subCount=m_pElementArray[a_triangleIndex][1];
	if(startIndex+subCount>I32(m_vertices))
	{
#if FE_SC_SAMPLE_DEBUG
		//* WARNING multi-threaded logging can segfault
		feLog("SurfaceCurves::sample"
				" excess triangleIndex %d (%d>%d)\n",
				a_triangleIndex,startIndex+subCount,m_vertices);
#endif
		return SpatialTransform::identity();
	}

	const Real realIndex=a_barycenter[0]*(subCount-1);
	const I32 subIndex=I32(realIndex>0.0?
			(realIndex<(subCount-1.0)? realIndex: subCount-2.0): 0.0);
	const Real weight1=realIndex-subIndex;
	const Real weight0=1.0-weight1;

	const I32 arrayIndex=startIndex+subIndex;
	const I32 arrayIndex1=(subIndex<subCount-1)? arrayIndex+1: arrayIndex;

	const SpatialVector& rVertex0=m_pVertexArray[arrayIndex];
	const SpatialVector& rVertex1=m_pVertexArray[arrayIndex1];

	const SpatialVector& rNormal0=m_pNormalArray[arrayIndex];
	const SpatialVector& rNormal1=m_pNormalArray[arrayIndex1];

	SpatialVector point;
	SpatialVector norm;
	if(m_sampleMethod==e_spline)
	{
		const U32 arrayIndexM=(subIndex>0)? arrayIndex-1: arrayIndex;
		const U32 arrayIndex2=(subIndex<subCount-2)? arrayIndex+2: arrayIndex1;

		const SpatialVector& rVertexM=m_pVertexArray[arrayIndexM];
		const SpatialVector& rVertex2=m_pVertexArray[arrayIndex2];

		const SpatialVector& rNormalM=m_pNormalArray[arrayIndexM];
		const SpatialVector& rNormal2=m_pNormalArray[arrayIndex2];

		point=Spline<SpatialVector,Real>::Cardinal2D(weight1,
				rVertexM,rVertex0,rVertex1,rVertex2);
		norm=unitSafe(Spline<SpatialVector,Real>::Cardinal2D(weight1,
				rNormalM,rNormal0,rNormal1,rNormal2));
	}
	else if(m_sampleMethod==e_pointNormal)
	{
		//* TODO can we cache the configure?

		TrianglePN<Real> trianglePN;
		trianglePN.configure(rVertex0,rVertex1,rVertex1+1e-3*rNormal1,
				rNormal0,rNormal1,rNormal1);
		SpatialBary segmentBary(weight0,weight1,0.0);
		trianglePN.solve(segmentBary,point,norm);
	}
	else if(m_sampleMethod==e_linear)
	{
		point=rVertex0*weight0+rVertex1*weight1;
		norm=rNormal0*weight0+rNormal1*weight1;
	}
	else //* flat
	{
		point=rVertex0*weight0+rVertex1*weight1;
		norm=unitSafe(rNormal0+rNormal1);
	}

	const Real mag=magnitude(norm);
	SpatialTransform result;
	if(mag>0.0)
	{
		SpatialVector tangent=a_tangent;

		if(m_pTangentArray)
		{
			tangent=m_pTangentArray[arrayIndex];
		}
		else if(m_pUVArray)
		{
			const SpatialVector& rUV0=m_pUVArray[arrayIndex];
			const SpatialVector& rUV1=m_pUVArray[arrayIndex1];

			SpatialVector du;
			SpatialVector dv;
			triangleDuDv(du,dv,rVertex0,rVertex1,rVertex1,rUV0,rUV1,rUV1);

			tangentFromDuDvN(tangent,du,dv,norm);

#if FE_SC_SAMPLE_DEBUG
			feLog("du %s dv %s\n",c_print(du),c_print(dv));
#endif
		}
		if(magnitudeSquared(tangent)<1e-6)
		{
			tangent=rVertex1-rVertex0;
		}
		if(magnitudeSquared(tangent)<1e-12)
		{
			if(arrayIndex!=arrayIndex1)
			{
				feX("SurfaceCurves::sample","coincident consecutive vertices");
			}
			else
			{
				feX("SurfaceCurves::sample","zero length tangent");
			}
		}
		makeFrameNormalY(result,point,tangent,norm/mag);

#if FE_SC_SAMPLE_DEBUG
		feLog("a_tangent %s tangent %s\n",c_print(a_tangent),c_print(tangent));
#endif
	}
	else
	{
		setIdentity(result);
		setTranslation(result,point);
	}

#if FE_SC_SAMPLE_DEBUG
	feLog("vertices %d start %d+%d\n",m_vertices,startIndex,subCount);
	feLog("realIndex %.6G subIndex %d/%d weight0 %.6G weight1 %.6G\n",
			realIndex,subIndex,subCount,weight0,weight1);
	feLog("arrayIndex %d %d\n",arrayIndex,arrayIndex1);
	feLog("rVertex0 %s\n",c_print(rVertex0));
	feLog("rVertex1 %s\n",c_print(rVertex1));
	feLog("rNormal0 %s\n",c_print(rNormal0));
	feLog("rNormal1 %s\n",c_print(rNormal1));
	feLog("point %s\n",c_print(point));
	feLog("norm %s\n",c_print(norm));
	feLog("result %s\n",c_print(result));
#endif

	return result;
}

SpatialTransform SurfaceCurves::sample(Vector2 a_uv) const
{
	const SpatialVector tangent(0.0,0.0,0.0);
	const SpatialBary barycenter(a_uv[0],a_uv[1],1.0-a_uv[0]-a_uv[1]);
	return sample(0,barycenter,tangent);
}

void SurfaceCurves::Impact::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_pColor,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
	const Color green(0.0,1.0,0.0);
	const Color lightgreen(0.5,1.0,0.5);
	const Color magenta(1.0,0.0,1.0);

	SpatialVector line[2];
	line[0]=m_vertex0;
	line[1]=m_vertex1;

	if(m_pointIndex0!=m_pointIndex1)
	{
		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,a_pColor);

		const Real normalLength=0.2*
				magnitude(m_vertex1-m_vertex0);

		line[0]=m_vertex0;
		line[1]=m_vertex0+m_normal0*normalLength;

		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,&green);

		line[0]=m_vertex1;
		line[1]=m_vertex1+m_normal1*normalLength;

		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,&green);

		const SpatialVector unitSegment=unitSafe(m_vertex1-m_vertex0);
		const Real along=dot(m_intersection-m_vertex0,unitSegment);
		const SpatialVector snapped=m_vertex0+unitSegment*along;

		line[0]=snapped;
		line[1]=snapped+m_normal*normalLength;

		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,&lightgreen);

		line[1]=snapped+m_tangent*normalLength;

		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,&magenta);
	}
	else
	{
		a_spDrawI->drawTransformedPoints(a_rTransform,NULL,
				&m_vertex0,&m_normal,1,false,a_pColor);

		line[1]=m_vertex0+m_normal;

		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,&green);

		line[1]=m_vertex0+m_tangent;

		a_spDrawI->drawTransformedLines(a_rTransform,NULL,
				line,NULL,2,DrawI::e_strip,false,&magenta);
	}
}

void SurfaceCurves::drawInternal(BWORD a_transformed,
	const SpatialTransform* a_pTransform,sp<DrawI> a_spDrawI,
	const fe::Color* a_pColor,sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
	const Color white(1.0,1.0,1.0);
	const Color* pColor=a_pColor? a_pColor: &white;

    const BWORD multicolor = (!a_pColor && m_pColorArray);
    if(multicolor)
	{
        pColor = m_pColorArray;
    }

	BWORD transformed=a_transformed;
	if(!a_pTransform || (transformed &&
			a_pTransform->column(0)==SpatialVector(1.0,0.0,0.0) &&
			a_pTransform->column(1)==SpatialVector(0.0,1.0,0.0) &&
			a_pTransform->column(2)==SpatialVector(0.0,0.0,1.0) &&
			a_pTransform->column(3)==SpatialVector(0.0,0.0,0.0)))
	{
		transformed=FALSE;
	}

	BWORD multiradius=(m_pRadiusArray!=NULL);
	Real* pRadius=m_pRadiusArray;

	sp<PartitionI> spPartition=
			a_spPartition.isValid()? a_spPartition: m_spPartition;

	BWORD drawPartitioned=FALSE;
	const I32 partCount=
			spPartition.isValid()? spPartition->partitionCount(): 0;
	if(partCount)
	{
		for(I32 partIndex=0;partIndex<partCount;partIndex++)
		{
			if(!spPartition->match(partIndex))
			{
				drawPartitioned=TRUE;
				break;
			}
		}
	}

//	feLog("SurfaceCurves::drawInternal"
//			" vertices %d a_spPartition %d"
//			" partCount %d transformed %d partitioned %d\n  %s\n",
//			m_vertices,spPartition.isValid(),
//			partCount,transformed,drawPartitioned,
//			c_print(a_spDrawI->drawMode()));

	//* NOTE rarely both
	BWORD hasLines=FALSE;
	BWORD hasPoints=FALSE;
	for(U32 elementIndex=0;elementIndex<m_elements;elementIndex++)
	{
		if(m_pElementArray[elementIndex][1]==1)
		{
			hasPoints=TRUE;
		}
		else
		{
			hasLines=TRUE;
		}
	}

	if(!hasLines)
	{
		a_spDrawI->drawPoints(m_pVertexArray,m_pNormalArray,
				m_elements,multicolor,pColor,a_spDrawBuffer);
		return;
	}

	if(!hasPoints && !drawPartitioned)
	{
		//* multiple lines
		if(transformed) {
			a_spDrawI->drawTransformedLines(*a_pTransform, NULL,
					m_pVertexArray,m_pNormalArray,
					m_vertices, DrawI::e_strip, multicolor, pColor,
					multiradius,pRadius,
					m_pElementArray, m_elements, a_spDrawBuffer);
		}
		else
		{
			a_spDrawI->drawLines(m_pVertexArray,m_pNormalArray,
					m_vertices, DrawI::e_strip, multicolor, pColor,
					multiradius,pRadius,
					m_pElementArray, m_elements, a_spDrawBuffer);
		}
    }
	else
	{
		const I32 bufferSize=512;
		SpatialVector pointVertexBuffer[bufferSize];
		SpatialVector pointNormalBuffer[bufferSize];
		Color pointColorBuffer[bufferSize];
		I32 pointCount=0;

		SpatialVector lineVertexBuffer[bufferSize];
		SpatialVector lineNormalBuffer[bufferSize];
		Color lineColorBuffer[bufferSize];
		Real lineRadiusBuffer[bufferSize];
		I32 linePointCount=0;
		Vector3i elementBuffer[bufferSize];
		I32 elementCount=0;

		lineColorBuffer[0]=pColor[0];
		lineRadiusBuffer[0]=pRadius? pRadius[0]: Real(0);

		//* mixed points and lines
		for(U32 elementIndex=0;elementIndex<m_elements;elementIndex++)
		{
			const U32 start=m_pElementArray[elementIndex][0];
			const U32 subCount=m_pElementArray[elementIndex][1];
			const BWORD openCurve=m_pElementArray[elementIndex][2];
			if(!subCount || !openCurve)
			{
				continue;
			}

			if(drawPartitioned)
			{
				const I32 partitionIndex=m_pPartitionIndexArray[start];
				const BWORD show=spPartition->match(partitionIndex);
				if(!show)
				{
					continue;
				}
			}

			if(multicolor)
			{
				pColor=&m_pColorArray[start];
			}

			if(multiradius)
			{
				pRadius=&m_pRadiusArray[start];
			}

			if(subCount==1)
			{
				pointVertexBuffer[pointCount]=m_pVertexArray[start];
				pointColorBuffer[pointCount]= *pColor;

				if(m_pNormalArray)
				{
					pointNormalBuffer[pointCount]=m_pNormalArray[start];
				}

				if(++pointCount==bufferSize)
				{
					a_spDrawI->drawPoints(pointVertexBuffer,
							m_pNormalArray? pointNormalBuffer: NULL,
							pointCount,TRUE,pointColorBuffer,
							sp<DrawBufferI>(NULL));
					pointCount=0;
				}
				continue;
			}

			//* NOTE oversize curves skip the buffer altogether
			if(subCount>bufferSize)
			{
				if(transformed)
				{
					a_spDrawI->drawTransformedLines(*a_pTransform,NULL,
							&m_pVertexArray[start],
							m_pNormalArray? &m_pNormalArray[start]: NULL,
							subCount,DrawI::e_strip,multicolor,pColor,
							multiradius,pRadius,
							NULL,0,sp<DrawBufferI>(NULL));
				}
				else
				{
					a_spDrawI->drawLines(&m_pVertexArray[start],
							m_pNormalArray? &m_pNormalArray[start]: NULL,
							subCount,DrawI::e_strip,multicolor,pColor,
							multiradius,pRadius,
							NULL,0,sp<DrawBufferI>(NULL));
				}
				continue;
			}

			if(linePointCount+subCount>bufferSize)
			{
				if(transformed)
				{
					a_spDrawI->drawTransformedLines(*a_pTransform,NULL,
							lineVertexBuffer,
							m_pNormalArray? lineNormalBuffer: NULL,
							linePointCount,DrawI::e_strip,
							multicolor,lineColorBuffer,
							multiradius,lineRadiusBuffer,
							elementBuffer,elementCount,sp<DrawBufferI>(NULL));
				}
				else
				{
					a_spDrawI->drawLines(lineVertexBuffer,
							m_pNormalArray? lineNormalBuffer: NULL,
							linePointCount,DrawI::e_strip,
							multicolor,lineColorBuffer,
							multiradius,lineRadiusBuffer,
							elementBuffer,elementCount,sp<DrawBufferI>(NULL));
				}
				linePointCount=0;
				elementCount=0;
			}
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 fromIndex=start+subIndex;
				const U32 toIndex=linePointCount+subIndex;
				lineVertexBuffer[toIndex]=m_pVertexArray[fromIndex];

				if(m_pNormalArray)
				{
					lineNormalBuffer[toIndex]=m_pNormalArray[fromIndex];
				}
				if(multicolor)
				{
					lineColorBuffer[toIndex]=m_pColorArray[fromIndex];
				}
				if(multiradius)
				{
					lineRadiusBuffer[toIndex]=m_pRadiusArray[fromIndex];
				}
			}

			elementBuffer[elementCount++]=
					Vector3i(linePointCount,subCount,TRUE);
			linePointCount+=subCount;
		}

		if(pointCount)
		{
			a_spDrawI->drawPoints(pointVertexBuffer,
					m_pNormalArray? pointNormalBuffer: NULL,
					pointCount,TRUE,pointColorBuffer,sp<DrawBufferI>(NULL));
		}

		if(elementCount)
		{
			//* TODO
		}
	}
}

void SurfaceCurves::resolveImpact(sp<ImpactI> a_spImpactI) const
{
	sp<Impact> spImpact(a_spImpactI);

	// TEMP if no nearest
	if(!spImpact.isValid() || spImpact->distance()<Real(0))
	{
		return;
	}

	const I32 subCount=m_pElementArray[spImpact->triangleIndex()][1];
	const Real realIndex=spImpact->barycenter()[0]*(subCount-1);
	const I32 subIndex=I32(realIndex>0.0?
			(realIndex<(subCount-1.0)? realIndex: subCount-2.0): 0.0);
	const Real weight1=realIndex-subIndex;
	const Real weight0=1.0-weight1;

	const SpatialVector normal=spImpact->normal0()*weight0+
			spImpact->normal1()*weight1;

	spImpact->setNormalLocal(normal);
	spImpact->setNearestSubIndex((weight1>=0.5)? subIndex+1: subIndex);
}

} /* namespace ext */
} /* namespace fe */
