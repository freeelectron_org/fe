#!/usr/bin/python

import sys

import fe
import fe.data
import fe.signal

import make_data

# create a scope 
scope = fe.data.Scope()


rg = make_data.make(scope)


# new clean style
for ra in rg:
    for r in ra:
        print r._ID_

