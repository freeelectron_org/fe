/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <flight/flight.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<SurfaceAccessibleDrone>(
			"SurfaceAccessibleI.SurfaceAccessibleDrone.fe.drone");

	pLibrary->add<DroneMission>("FlightMissionI.DroneMission.fe");

	pLibrary->add<DroneControl>("HandlerI.DroneControl.fe");
	pLibrary->add<DroneDynamics>("HandlerI.DroneDynamics.fe");
	pLibrary->add<DroneInput>("HandlerI.DroneInput.fe");

	pLibrary->add<SurfaceDrone>("SurfaceI.SurfaceDrone.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

