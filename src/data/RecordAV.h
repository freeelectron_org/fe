/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordAV_h__
#define __data_RecordAV_h__

namespace fe
{


template <class T>
class Accessor;

/**	@brief Reference to an instance of a Layout

	@ingroup data

	A record is simply a pointer to a Layout and a pointer to a
	state block.  A state block is a block of memory with Attributes laid
	out as specified by a Layout object.
	*/
class FE_DL_EXPORT RecordAV
{
friend	class WeakRecordAV;
friend	class LayoutAV;
friend	class RecordArrayAV;

	public:
		RecordAV(I32 ignored=0);
		RecordAV(const RecordAV &other);
virtual	~RecordAV(void);

		/** Return the Layout. */
		sp<Layout>	layout(void) const;

		RecordAV	&operator=(const RecordAV &r_other);

		bool		operator==(const RecordAV &r_other) const;
		bool		operator!=(const RecordAV &r_other) const;

		/** Return a unique runtime id (id-runtime, idr, since 'rid' has
			other meaning. */
		FE_UWORD		idr(void) const;

		/** Return true if the Record points to a valid state block. */
		bool		isValid(void) const;
		bool		extractInstance(Instance &instance, const String &attrName);

		RecordAV	clone(void);

		template <class T>
		T			&accessAttribute(FE_UWORD aLocator) const;

		void		*rawAttribute(FE_UWORD aLocator) const;

	private:
		void		acquire(void);
		void		release(void);
		void		set(FE_UWORD aArrayIndex, sp<LayoutAV> a_spLayout);

		FE_UWORD		arrayIndex(void) const { return m_arrayIndex; }

	private:
		/// WARNING: this constructor avoids reference counting
		//RecordAV(FE_UWORD aArrayIndex, LayoutAV *pLayout);

		sp<LayoutAV>		m_spLayout;
		FE_UWORD				m_arrayIndex;

static
const	FE_DL_PUBLIC FE_UWORD	ms_invalidIndex = (FE_UWORD)(-1);
};

struct hash_record_av
{ FE_UWORD operator()(const RecordAV r_rec) const
	{ return r_rec.idr() % 17191; } };

inline bool operator<(const RecordAV &lhs, const RecordAV &rhs)
{
	return (lhs.idr() < rhs.idr());
}

class RecordAVInfo : public BaseType::Info
{
	public:
virtual	String	print(void *instance);
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	IWORD	iosize(void);
virtual	bool	getConstruct(void);
virtual	void	construct(void *instance);
virtual	void	destruct(void *instance);
};

inline RecordAV::RecordAV(I32)
{
	m_arrayIndex = ms_invalidIndex;
}


inline RecordAV::RecordAV(const RecordAV &other)
{
	m_spLayout = other.m_spLayout;
	m_arrayIndex = other.m_arrayIndex;
	acquire();
}

inline RecordAV::~RecordAV(void)
{
	release();
}

inline RecordAV &RecordAV::operator=(const RecordAV &other)
{
	if(this != &other)
	{
		release();
		m_spLayout = other.m_spLayout;
		m_arrayIndex = other.m_arrayIndex;
		acquire();
	}
	return *this;
}

inline bool RecordAV::operator==(const RecordAV &other) const
{
	if (m_spLayout != other.m_spLayout) { return false; }
	return (m_arrayIndex == other.m_arrayIndex);
}

inline bool RecordAV::operator!=(const RecordAV &other) const
{
	if (m_spLayout != other.m_spLayout) { return true; }
	return (m_arrayIndex != other.m_arrayIndex);
}

inline void RecordAV::set(FE_UWORD aArrayIndex, sp<LayoutAV> a_spLayout)
{
	release();
	m_arrayIndex = aArrayIndex;
	m_spLayout = a_spLayout;
	acquire();
}

inline FE_UWORD RecordAV::idr(void) const
{
	return m_spLayout->idr(m_arrayIndex);
}

template <class T>
inline T &RecordAV::accessAttribute(FE_UWORD aLocator) const
{
#if 0
	return *(T *)(m_spLayout
			->m_attributeVector[m_spLayout->m_locatorTable[aLocator]]
			->raw_at(m_arrayIndex));
#endif
	return *(T *)(m_spLayout->voidAccess(aLocator, m_arrayIndex));
}

inline void *RecordAV::rawAttribute(FE_UWORD aLocator) const
{
#if 0
	return (void *)(m_spLayout
			->m_attributeVector[m_spLayout->m_locatorTable[aLocator]]
			->raw_at(m_arrayIndex));
#endif
	return (m_spLayout->voidAccess(aLocator, m_arrayIndex));
}

inline sp<Layout> RecordAV::layout(void) const
{
	return m_spLayout;
}

inline void RecordAV::release(void)
{
	if(m_arrayIndex != ms_invalidIndex)
	{
		m_spLayout->releaseArrayIndex(m_arrayIndex);
	}
}

inline void RecordAV::acquire(void)
{
	if(m_arrayIndex != ms_invalidIndex)
	{
		m_spLayout->acquireArrayIndex(m_arrayIndex);
	}
}

inline bool RecordAV::isValid(void) const
{
	return (m_arrayIndex != ms_invalidIndex);
}


} /* namespace */

#endif /* __data_RecordAV_h__ */

