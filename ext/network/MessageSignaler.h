/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __network_MessageSignaler_h__
#define __network_MessageSignaler_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Convert network messages to signals

	As a StateCatalog::ListenerI this class is intended to be handed to
	a StateCatalog with its addListener method.

	As a SignalerI, any HandlerI objects inserted to this signaler will
	receive relevant signal as they appear on the select key
	on the StateCatalog.
	This object removes each message from the StateCatalog's deque
	as it create and sends the resulting signal.

	@ingroup network
*//***************************************************************************/
class FE_DL_EXPORT MessageSignaler:
		virtual public StateCatalog::ListenerI,
		public ChainSignaler,
		public Initialize<MessageSignaler>
{
	public:
				MessageSignaler(void)										{}

		void	initialize(void);

				//* as StateCatalog::ListenerI
virtual	void	bind(sp<StateCatalog> a_spStateCatalog,String a_name)
				{	m_spStateCatalog=a_spStateCatalog; }
virtual	void	notify(String a_name,String a_property);

	private:
		sp<StateCatalog>	m_spStateCatalog;
		sp<BaseType>		m_spRecordType;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __network_MessageSignaler_h__ */

