/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

#define FE_PS_DRAWPOINTS			FALSE
#define FE_PS_DISCRETELINES			FALSE
#define FE_PS_DRAWSHADOWS			FALSE//TRUE
#define FE_PS_DRAWLEAVES			TRUE
#define FE_PS_DRAWTEXT				FALSE
#define FE_PS_PROFILE				FALSE

#define FE_PS_GROUP_CYLINDERS		TRUE
#define FE_PS_DETAILS				FALSE
#define FE_PS_IMPACT_ONLY			FALSE

#define FE_PS_LEAF_DIAMETER			0.05	// TODO param
#define FE_PS_LINE_WIDTH			1.5f
#define FE_PS_VISIBLE_MIN_RADIUS	0.001f

namespace fe
{
namespace ext
{

void PlantSketch::initialize(void)
{
	m_pLines=NULL;
	m_pLineNormals=NULL;
	m_pLineRadius=NULL;
	m_pOldLines=NULL;
	m_pShadowLines=NULL;

	m_paused=FALSE;

	m_stickForm="line";
	m_leafForm="point";

	setIdentity(m_transform);

	m_spCylinderDrawMode=new DrawMode();
	m_spCylinderDrawMode->setDrawStyle(DrawMode::e_solid);
	m_spCylinderDrawMode->setAntialias(TRUE);
	m_spCylinderDrawMode->setLayer(2);

	m_spLineDrawMode=new DrawMode();
	m_spLineDrawMode->setDrawStyle(DrawMode::e_wireframe);
	m_spLineDrawMode->setAntialias(TRUE);
	m_spLineDrawMode->setLineWidth(FE_PS_LINE_WIDTH);
	m_spLineDrawMode->setLayer(3);

	m_spLeafDrawMode=new DrawMode();
	m_spLeafDrawMode->setDrawStyle(DrawMode::e_solid);
	m_spLeafDrawMode->setAntialias(TRUE);
	m_spLeafDrawMode->setPointSize(5.0f);
	m_spLeafDrawMode->setLayer(4);

#if FE_PS_PROFILE
	//* initialize profiling
	m_spProfiler=new Profiler("Plant Sketch");
	m_spProfileStartup=new Profiler::Profile(m_spProfiler,"Startup");
	m_spProfileCamera=new Profiler::Profile(m_spProfiler,"Camera");
	m_spProfileIterate=new Profiler::Profile(m_spProfiler,"Iterate");
	m_spProfileSpan=new Profiler::Profile(m_spProfiler,"Span");
	m_spProfileLineNormals=new Profiler::Profile(m_spProfiler,"Line Normals");
	m_spProfileDetails=new Profiler::Profile(m_spProfiler,"Details");
	m_spProfilePoints=new Profiler::Profile(m_spProfiler,"Points");
	m_spProfileCylinders=new Profiler::Profile(m_spProfiler,"Cylinders");
	m_spProfileDrawCylinders=new Profiler::Profile(m_spProfiler,
			"DrawCylinders");
	m_spProfileShadows=new Profiler::Profile(m_spProfiler,"Shadows");
	m_spProfileText=new Profiler::Profile(m_spProfiler,"Text");
	m_spProfileLines=new Profiler::Profile(m_spProfiler,"Lines");
	m_spProfileLeaves=new Profiler::Profile(m_spProfiler,"Leaves");
#endif
}
PlantSketch::~PlantSketch(void)
{
#if FE_PS_PROFILE
	feLog("%s:\n%s\n",m_spProfiler->name().c_str(),
			m_spProfiler->report().c_str());
#endif

	delete[] m_pShadowLines;
	delete[] m_pOldLines;
	delete[] m_pLineNormals;
	delete[] m_pLineRadius;
	delete[] m_pLines;
}

void PlantSketch::bind(sp<Component> spComponent)
{
	m_spPlantModelI=spComponent;
	FEASSERT(m_spPlantModelI.isValid());
	const U32& sticks=m_spPlantModelI->numSticks();
	const U32 numVectors=2*sticks;

	delete[] m_pShadowLines;
	delete[] m_pOldLines;
	delete[] m_pLineNormals;
	delete[] m_pLineRadius;
	delete[] m_pLines;
	m_pLines=new SpatialVector[numVectors];
	m_pLineNormals=new SpatialVector[numVectors];
	m_pLineRadius=new Real[numVectors];
	m_pShadowLines=new SpatialVector[numVectors];
	m_pOldLines=new SpatialVector[numVectors];
}

void PlantSketch::draw(sp<DrawI> spDrawI)
{
#if FE_PS_PROFILE
	m_spProfiler->begin();
	m_spProfileStartup->start();
#endif

	const Color black(0.0f,0.0f,0.0f,0.5f);
	const Color white(1.0f,1.0f,1.0f);
	const Color cyan(0.0f,1.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f);
	const Color green(0.0f,1.0f,0.0f);
	const Color blue(0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,0.5f);
	const Color lightblue(0.5f,0.5f,1.0f);
	const Color grey(0.4f,0.4f,0.4f);
	const Color midred(0.7f,0.3f,0.3f);
	const Color midblue(0.3f,0.3f,0.7f);
	const Color darkred(0.7f,0.0f,0.0f);
	const Color darkblue(0.0f,0.0f,0.7f);
	const Color yellowhaze(1.0f,1.0f,0.0f,0.5f);
	const Color yellowopaque(1.0f,1.0f,0.0f,1.0f);
	const Color brownopaque(1.0f,0.6f,0.3f,1.0f);
	const Color yellowbranch(1.0f,1.0f,0.0f,0.5f);
	const Color brownbranch(0.5f,0.3f,0.15f,1.0f);
	const Color redbranch(0.4,0.0f,0.0f,1.0f);

	const U32 sticks=m_spPlantModelI->numSticks();

	const BWORD drawLines=(m_stickForm=="line");
	const BWORD drawCylinders=(m_stickForm=="cylinder");
	const BWORD drawCurves=(m_stickForm=="curve");

//	F32 min_radius=FE_PS_VISIBLE_MIN_RADIUS;	// don't draw anything smaller

	if(drawLines && !m_paused)
	{
		memcpy((void*)m_pOldLines,m_pLines,sizeof(SpatialVector)*2*sticks);
	}

#if FE_PS_PROFILE
	m_spProfileCamera->replace(m_spProfileStartup);
#endif

	SpatialVector eye;
#ifdef FE_VEG_VIEWER
	if(m_spCameraControllerI.isValid())
	{
		SpatialTransform camera;
		invert(camera,m_spCameraControllerI->camera()->cameraMatrix());
		eye=camera.translation();
	}
	else
#endif
	{
		set(eye);
	}

	SpatialVector to_eye;
	if(sticks)
	{
		PlantModelI::Stick* pStick=m_spPlantModelI->stick(0);

		sp<ViewI> spView;
#if FE_VEG_VIEWER
		spView=spDrawI->view();
		if(spView.isValid())
		{
			F32 pixels=spView->pixelSize(pStick->base(),
					0.5f*FE_PS_LEAF_DIAMETER);
			m_spLeafDrawMode->setPointSize(pixels);
		}
#endif

		if(drawLines)
		{
//			if(spView.isValid())
//			{
//				min_radius=spView->worldSize(pStick->base(),FE_PS_LINE_WIDTH);
//			}

			to_eye=eye-pStick->base();
			F32 distance=magnitude(to_eye);
			if(distance>0.0f)
			{
				normalize(to_eye);
			}
			else
			{
				set(to_eye,0.0f,0.0f,1.0f);
			}
		}
	}

	if(FE_PS_DRAWSHADOWS || drawLines || drawCurves ||
			FE_PS_DETAILS || FE_PS_DRAWPOINTS ||
			(drawCylinders && !FE_PS_GROUP_CYLINDERS))
	{

#if FE_PS_PROFILE
		m_spProfileIterate->replace(m_spProfileCamera);
#endif

		for(U32 index=0;index<sticks;index++)
		{
#if FE_PS_PROFILE
			m_spProfileSpan->start();
#endif

			PlantModelI::Stick* pStick=m_spPlantModelI->stick(index);

			const SpatialVector tip=pStick->base()+pStick->span();

			m_pLines[2*index]=pStick->base();
			m_pLines[2*index+1]=tip;

#if FE_PS_PROFILE
			m_spProfileLineNormals->replace(m_spProfileSpan);
#endif

			if(drawLines || drawCurves)
			{
				const F32 length=magnitude(pStick->span());
				SpatialVector cross;
				SpatialVector unitSpan=pStick->span()/length;
				cross3(cross,to_eye,unitSpan);
				cross3(m_pLineNormals[2*index],unitSpan,cross);

				m_pLineNormals[2*index+1]=m_pLineNormals[2*index];

				if(drawCurves)
				{
					m_pLineRadius[2*index]=pStick->radius1();
					m_pLineRadius[2*index+1]=pStick->radius2();
				}
			}

#if FE_PS_DISCRETELINES
			if(drawLines)
			{
				Tree::Stick* pTreeStick=fe_cast<Tree::Stick>(pStick);
				if(pTreeStick)
				{
					const U32 level=pTreeStick->m_level;
					SpatialVector line[2];
					line[0]=pTreeStick->base();
					line[1]=tip;
					const Color &lineColor=
							(level==0)? green:
							(level==1)? red:
							(level==2)? blue:
							white;
					spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
							DrawI::e_discrete,false,&lineColor);
				}
			}
			if(drawCurves)
			{
				Tree::Stick* pTreeStick=fe_cast<Tree::Stick>(pStick);
				if(pTreeStick)
				{
					const U32 level=pTreeStick->m_level;
					SpatialVector line[2];
					line[0]=pTreeStick->base();
					line[1]=tip;
					Real radius[2];
					radius[0]=pTreeStick->radius1();
					radius[1]=pTreeStick->radius2();
					const Color &lineColor=
							(level==0)? green:
							(level==1)? red:
							(level==2)? blue:
							white;

					spDrawI->drawTransformedCurve(m_transform,NULL,line,NULL,
							radius,2,false,&lineColor);
				}
			}
#endif

#if FE_PS_PROFILE
			m_spProfileDetails->replace(m_spProfileLineNormals);
#endif

#if FE_PS_DETAILS
			const F32 intensityScale=10.0f;
			const F32 velocityScale=0.1f;

			Tree::Stick* pTreeStick=fe_cast<Tree::Stick>(pStick);
			if(pTreeStick
#if FE_PS_IMPACT_ONLY
					&& magnitude(pTreeStick->m_intensity)>=1e-3f
#endif
					)
			{
				SpatialVector line[2];
				Vector2f intensity=intensityScale*pTreeStick->m_intensity;
				Vector2f velocity=velocityScale*pTreeStick->m_velocity;

				//* arbitrary "intensity" metric
				line[0]=tip+0.1f*(pTreeStick->base()-tip);
				line[1]=line[0]+pTreeStick->m_dir[0]*intensity[0];
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&pink);
				line[1]=line[0]+pTreeStick->m_dir[1]*intensity[1];
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&lightblue);

				//* velocity
				line[0]=tip+0.3f*(pTreeStick->base()-tip);
				line[1]=line[0]+pTreeStick->m_dir[0]*velocity[0];
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&midred);
				line[1]=line[0]+pTreeStick->m_dir[1]*velocity[1];
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&midblue);

				//* position change versus angle increase
				const F32 dirScale=0.3f;
				line[0]=tip;
				line[1]=tip+pTreeStick->m_dir[0]*dirScale;
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&darkred);
				line[1]=tip+pTreeStick->m_dir[1]*dirScale;
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&darkblue);

				line[0]=pTreeStick->m_contact;
				line[1]=pTreeStick->m_contact2;
				spDrawI->drawTransformedLines(m_transform,NULL,line,NULL,2,
						DrawI::e_discrete,false,&grey);

				spDrawI->drawTransformedPoints(m_transform,
						&pTreeStick->m_contact,NULL,1,false,&cyan);
				spDrawI->drawTransformedPoints(m_transform,
						&pTreeStick->m_contact2,NULL,1,false,&green);
			}
#endif

#if FE_PS_PROFILE
			m_spProfileText->replace(m_spProfileDetails);
#endif

#if FE_PS_DRAWTEXT
			spDrawI->drawAlignedText(tip,pStick->stateString().c_str(),cyan);
#endif

#if FE_PS_PROFILE
			m_spProfilePoints->replace(m_spProfileText);
#endif

#if FE_PS_DRAWPOINTS
			spDrawI->drawTransformedPoints(m_transform,&pStick->base(),
					NULL,1,false,&darkred);
#endif

#if FE_PS_PROFILE
			m_spProfileCylinders->replace(m_spProfilePoints);
#endif

			if(drawCylinders)
			{
#if !FE_PS_GROUP_CYLINDERS
				spDrawI->setDrawMode(m_spCylinderDrawMode);
#if FE_PS_PROFILE
				m_spProfileDrawCylinders->start();
#endif
				spDrawI->drawCylinder(cylTransform,
						&m_pCylinderScale[cylinder_count],
						m_pCylinderBaseScale[cylinder_count],
						drawLines? yellowhaze: brownopaque,
						m_pCylinderSlices[cylinder_count]);
#if FE_PS_PROFILE
				m_spProfileDrawCylinders->finish();
#endif
				spDrawI->setDrawMode(sp<DrawMode>(NULL));
#endif	// !FE_PS_GROUP_CYLINDERS
		}
#if FE_PS_PROFILE
			m_spProfileCylinders->finish();
#endif
		}	// loop of sticks
#if FE_PS_PROFILE
		m_spProfileIterate->finish();
#endif
	}
	else	// else didn't loop sticks
	{
#if FE_PS_PROFILE
		m_spProfileCamera->finish();
#endif
	}

	if(drawCylinders)
	{
#if FE_PS_GROUP_CYLINDERS
#if FE_PS_PROFILE
		m_spProfileDrawCylinders->start();
#endif
		spDrawI->setDrawMode(m_spCylinderDrawMode);

		const U32 bundles=m_spPlantModelI->stickBundles();
		for(U32 bundle=0;bundle<bundles;bundle++)
		{
			if(m_spPlantModelI->stickBundleSize(bundle))
			{
				spDrawI->drawTransformedCylinders(m_transform,
						m_spPlantModelI->stickTransforms(bundle),
						m_spPlantModelI->stickScale(bundle),
						m_spPlantModelI->stickBaseScale(bundle),
						m_spPlantModelI->stickSlices(bundle),
						m_spPlantModelI->stickBundleSize(bundle),FALSE,
						drawLines? &yellowhaze: &brownopaque);
			}
		}

		spDrawI->setDrawMode(sp<DrawMode>(NULL));
#if FE_PS_PROFILE
		m_spProfileShadows->replace(m_spProfileDrawCylinders);
#endif
#else	// not grouped
#if FE_PS_PROFILE
		m_spProfileShadows->start();
#endif
#endif	// whether grouped
	}
#if FE_PS_PROFILE
	else	// no cylinders
	{
		m_spProfileShadows->start();
	}
#endif

#if FE_PS_DRAWSHADOWS
	for(U32 m=0;m<2*sticks;m++)
	{
		m_pShadowLines[m]=m_pLines[m];
		m_pShadowLines[m][2]=0.0f;
	}
	spDrawI->drawTransformedLines(m_transform,m_pShadowLines,NULL,2*sticks,
			DrawI::e_discrete,false,&black);
#endif

#if FE_PS_PROFILE
	m_spProfileLines->replace(m_spProfileShadows);
#endif

#if !FE_PS_DISCRETELINES
	if(drawLines)
	{
		spDrawI->setDrawMode(m_spLineDrawMode);
		if(m_paused)
		{
			spDrawI->drawTransformedLines(m_transform,NULL,m_pOldLines,NULL,
					2*sticks,DrawI::e_discrete,false,&redbranch);
		}

		spDrawI->drawTransformedLines(m_transform,NULL,m_pLines,m_pLineNormals,
				2*sticks,DrawI::e_discrete,false,&brownbranch);
		spDrawI->setDrawMode(sp<DrawMode>(NULL));
	}
	if(drawCurves)
	{
		spDrawI->setDrawMode(m_spLineDrawMode);

//~		spDrawI->drawTransformedCurve(m_transform,NULL,m_pLines,m_pLineNormals,
//~				m_pLineRadius,2*sticks,false,&brownbranch);

		const U32 maxPointCount=128;
		SpatialVector line[maxPointCount];
		Real radius[maxPointCount];
		Array<I32> drawn(sticks,0);

		for(U32 index=0;index<sticks;index++)
		{
			if(drawn[index])
			{
				continue;
			}

			PlantModelI::Stick* pStick=m_spPlantModelI->stick(index);
			Tree::Stick* pTreeStick=fe_cast<Tree::Stick>(pStick);
			U32 level=pTreeStick? pTreeStick->m_level: 0;

			line[0]=pStick->base();
			line[1]=line[0]+pStick->span();
			radius[0]=pStick->radius1();
			radius[1]=pStick->radius2();
			U32 pointCount=2;

			for(U32 other=index+1;other<sticks;other++)
			{
				PlantModelI::Stick* pOther=m_spPlantModelI->stick(other);

				Tree::Stick* pTreeOther=fe_cast<Tree::Stick>(pOther);
				if(pTreeOther && pTreeOther->m_pParent==pStick &&
						pTreeOther->m_level==level)
				{
					line[pointCount]=pOther->base()+pOther->span();
					radius[pointCount]=pOther->radius2();
					pointCount++;

					pStick=pOther;
					drawn[other]=1;

					if(pointCount==maxPointCount)
					{
						break;
					}
				}
			}

			spDrawI->drawTransformedCurve(m_transform,NULL,line,
					NULL,//m_pLineNormals,
					radius,pointCount,false,&brownbranch);
		}

		spDrawI->setDrawMode(sp<DrawMode>(NULL));
	}
#endif

#if FE_PS_PROFILE
	m_spProfileLeaves->replace(m_spProfileLines);
#endif

#if FE_PS_DRAWLEAVES
	if(m_spPlantModelI->numLeaves())
	{
		if(m_leafForm=="point")
		{
			spDrawI->setDrawMode(m_spLeafDrawMode);
			spDrawI->drawTransformedPoints(m_transform,NULL,
					m_spPlantModelI->leafPoints(),
					m_spPlantModelI->leafNormals(),
					m_spPlantModelI->numLeaves(),false,&green);
			spDrawI->setDrawMode(sp<DrawMode>(NULL));
		}

		//* TODO other forms
	}
#endif

#if FE_PS_PROFILE
	m_spProfileLeaves->finish();
	m_spProfiler->end();
#endif
}

} /* namespace ext */
} /* namespace fe */
