//
// Copyright 2016 Pixar
//
// Licensed under the Apache License, Version 2.0 (the "Apache License")
// with the following modification; you may not use this file except in
// compliance with the Apache License and the following modification to it:
// Section 6. Trademarks. is deleted and replaced with:
//
// 6. Trademarks. This License does not grant permission to use the trade
//	names, trademarks, service marks, or product names of the Licensor
//	and its affiliates, except as required to comply with Section 4(c) of
//	the License and to reproduce the content of the NOTICE file.
//
// You may obtain a copy of the Apache License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Apache License with the above modification is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied. See the Apache License for the specific
// language governing permissions and limitations under the Apache License.

// originally from pxr/imaging/hd/unitTestDelegate.cpp
// and pxr/imaging/hdx/unitTestDelegate.cpp
// adapted for Free Electron

// see also pxr/usdImaging/usdImagingGL/engine.cpp

#include <usd/hydra.pmh>

#include "pxr/imaging/hd/basisCurves.h"
#include "pxr/imaging/hd/sprim.h"
#include "pxr/imaging/hd/mesh.h"
#include "pxr/imaging/hd/meshTopology.h"
#include "pxr/imaging/hd/points.h"
#include "pxr/imaging/hd/material.h"
#include "pxr/imaging/hd/texture.h"
#include "pxr/imaging/hd/textureResource.h"
#include "pxr/imaging/hdSt/textureResource.h"

//#include "pxr/imaging/hd/surfaceShader.h"

#include "pxr/base/tf/staticTokens.h"
#include "pxr/base/gf/matrix4f.h"
#include "pxr/base/gf/rotation.h"

#include "pxr/imaging/glf/simpleLight.h"
#include "pxr/imaging/glf/textureRegistry.h"
#include "pxr/imaging/glf/ptexTexture.h"

//TF_DEFINE_PRIVATE_TOKENS(
//	_tokens,
//	(rotate)
//	(scale)
//	(translate)
//);

TF_DEFINE_PUBLIC_TOKENS(TestTokens,TEST_TOKENS);

namespace {
class ShadowMatrix: public HdxShadowMatrixComputation
{
	public:

					ShadowMatrix(GlfSimpleLight const& light,
						GfRange3d const& extent)
					{
						const GfVec4d pos = light.GetPosition();

						const GfVec3d min=extent.GetMin();
						const GfVec3d max=extent.GetMax();

						const fe::SpatialVector center(
								0.5*(min[0]+max[0]),
								0.5*(min[1]+max[1]),
								0.5*(min[2]+max[2]));
						const fe::Real radius=0.5*fe::maximum(double(0),
								fe::maximum(max[0]-min[0],
								fe::maximum(max[1]-min[1],max[2]-min[2])));

//						feLog("\n");
//						feLog("pos %.6G %.6G %.6G %.6G\n",
//								pos[0],pos[1],pos[2],pos[3]);
//						feLog("min %.6G %.6G %.6G max %.6G %.6G %.6G\n",
//								min[0],min[1],min[2],
//								max[0],max[1],max[2]);
//						feLog("center %s radius %.6G\n",c_print(center),radius);

						const fe::SpatialVector shadowDir=unitSafe(
								fe::SpatialVector(pos[0],pos[1],pos[2]));
//								fe::SpatialVector(pos[0],0.0,pos[2]));
						const fe::SpatialVector shadowPos=
								center+(2*radius)*shadowDir;
						const fe::Real near=0.9*radius;
						const fe::Real far=3.1*radius;
						const fe::Real halfWidth=1.1*radius;

						GfFrustum frustum;
						frustum.SetProjectionType(GfFrustum::Orthographic);
						frustum.SetWindow(GfRange2d(
								GfVec2d(-halfWidth, -halfWidth),
								GfVec2d(halfWidth, halfWidth)));
						frustum.SetNearFar(GfRange1d(near, far));

						frustum.SetPosition(GfVec3d(shadowPos[0],
								shadowPos[1],shadowPos[2]));

						const GfRotation axisAngle(GfVec3d(0, 0, 1),
								GfVec3d(shadowDir[0],
								shadowDir[1],shadowDir[2]));
						frustum.SetRotation(axisAngle);

//						feLog("shadowPos %s\n",c_print(shadowPos));

//						const GfVec3d& axis=axisAngle.GetAxis();
//						const double angle=axisAngle.GetAngle();
//						feLog("frustum axis %.6G %.6G %.6G angle %.6G\n",
//								axis[0],axis[1],axis[2],angle);

//						const GfMatrix4d viewMatrix=frustum.ComputeViewMatrix();
//						for(I32 m=0;m<16;m++)
//						{
//							feLog(" %12.6G",viewMatrix.GetArray()[m]);
//							if((m%4)==3) feLog("\n");
//						}

#if TRUE
						m_shadowMatrix = frustum.ComputeViewMatrix()*
								frustum.ComputeProjectionMatrix();
#else
						const fe::Real heading=
								-atan2(shadowDir[0],shadowDir[2]);
						const fe::Real pitch= -asin(shadowDir[1]);
//						feLog("heading %.6G pitch %.6G\n",heading,pitch);

						fe::SpatialTransform cam=
								fe::SpatialEuler(pitch,-heading,0.0);
						setTranslation(cam,shadowPos);

						fe::SpatialTransform xform;
						invert(xform,cam);

						const GfMatrix4d cleanViewMatrix(
								xform(0,0),xform(1,0),xform(2,0),0.0,
								xform(0,1),xform(1,1),xform(2,1),0.0,
								xform(0,2),xform(1,2),xform(2,2),0.0,
								xform(0,3),xform(1,3),xform(2,3),1.0);

//						for(I32 m=0;m<16;m++)
//						{
//							feLog(" %12.6G",cleanViewMatrix.GetArray()[m]);
//							if((m%4)==3) feLog("\n");
//						}

						m_shadowMatrix = cleanViewMatrix *
								frustum.ComputeProjectionMatrix();
#endif

						//* NOTE center should project to (0,0,0,1)
//						const GfVec4d center4d(center[0],center[1],
//								center[2],1.0);
//						const GfVec4d projected = center4d * m_shadowMatrix;
//						feLog("projected %.6G %.6G %.6G %.6G\n",
//								projected[0],projected[1],
//								projected[2],projected[3]);
					}

virtual	std::vector<GfMatrix4d>	Compute(const GfVec4f &viewport,
						CameraUtilConformWindowPolicy policy)
					{	return std::vector<GfMatrix4d>(1, m_shadowMatrix); }

	private:

		GfMatrix4d m_shadowMatrix;
};
} //* namespace

HydraDelegate::HydraDelegate(HdRenderIndex *parentIndex,
	SdfPath const& delegateID):
	HdSceneDelegate(parentIndex, delegateID),
	m_runShadows(false),
	m_hasInstancePrimVars(true),
	m_refineLevel(0)
{
	HdChangeTracker &tracker = GetRenderIndex().GetChangeTracker();
	tracker.AddCollection(TestTokens->geometryAndGuides);

	m_cameraId = SdfPath("/camera");
	GetRenderIndex().InsertSprim(HdPrimTypeTokens->camera, this, m_cameraId);

	GfFrustum frustum;
	frustum.SetPosition(GfVec3d(0, 0, 200));
	SetCamera(frustum.ComputeViewMatrix(), frustum.ComputeProjectionMatrix());
}

void HydraDelegate::SetRefineLevel(int level)
{
	m_refineLevel = level;

	TF_FOR_ALL (it, m_meshes)
	{
		MarkRprimDirty(it->first, HdChangeTracker::DirtyDisplayStyle);
	}

	TF_FOR_ALL (it, m_curves)
	{
		MarkRprimDirty(it->first, HdChangeTracker::DirtyDisplayStyle);
	}

	TF_FOR_ALL (it, m_refineLevels)
	{
		it->second = level;
	}
}

void HydraDelegate::SetCamera(GfMatrix4d const &viewMatrix,
	GfMatrix4d const &projMatrix)
{
	SetCamera(m_cameraId, viewMatrix, projMatrix);
}

void HydraDelegate::SetCamera(SdfPath const &cameraId,
	GfMatrix4d const &viewMatrix,GfMatrix4d const &projMatrix)
{
	ValueCache &cache = m_valueCacheMap[cameraId];

#if HD_API_VERSION>=32
	cache[HdCameraTokens->windowPolicy] = VtValue(CameraUtilFit);
	cache[HdCameraTokens->worldToViewMatrix] = VtValue(viewMatrix);
	cache[HdCameraTokens->projectionMatrix] = VtValue(projMatrix);

	GetRenderIndex().GetChangeTracker().MarkSprimDirty(cameraId,
			HdCamera::AllDirty);
#else
	cache[HdStCameraTokens->windowPolicy] = VtValue(CameraUtilFit);
	cache[HdStCameraTokens->worldToViewMatrix] = VtValue(viewMatrix);
	cache[HdStCameraTokens->projectionMatrix] = VtValue(projMatrix);

	GetRenderIndex().GetChangeTracker().MarkSprimDirty(cameraId,
			HdStCamera::AllDirty);
#endif
}

void HydraDelegate::AddCamera(SdfPath const &id)
{
	GetRenderIndex().InsertSprim(HdPrimTypeTokens->camera, this, id);
	ValueCache &cache = m_valueCacheMap[id];

#if HD_API_VERSION>=32
	cache[HdCameraTokens->windowPolicy] = VtValue(CameraUtilFit);
	cache[HdCameraTokens->worldToViewMatrix] = VtValue(GfMatrix4d(1));
	cache[HdCameraTokens->projectionMatrix] = VtValue(GfMatrix4d(1));
#else
	cache[HdStCameraTokens->windowPolicy] = VtValue(CameraUtilFit);
	cache[HdStCameraTokens->worldToViewMatrix] = VtValue(GfMatrix4d(1));
	cache[HdStCameraTokens->projectionMatrix] = VtValue(GfMatrix4d(1));
#endif
}

bool HydraDelegate::HasLight(SdfPath const &id)
{
	return (m_valueCacheMap.find(id)!=m_valueCacheMap.end());
}

void HydraDelegate::AddLight(SdfPath const &id, GlfSimpleLight const &light)
{
#if HD_API_VERSION>=32
	GetRenderIndex().InsertSprim(HdPrimTypeTokens->simpleLight, this, id);
#else
	GetRenderIndex().InsertSprim(HdPrimTypeTokens->light, this, id);
#endif

	ValueCache &cache = m_valueCacheMap[id];

	HdxShadowParams shadowParams;

	shadowParams.enabled = light.HasShadow();
	shadowParams.resolution = light.GetShadowResolution();
	shadowParams.bias = light.GetShadowBias();
	shadowParams.blur = light.GetShadowBlur();

	const GfRange3d extent=GetExtents();
	shadowParams.shadowMatrix =HdxShadowMatrixComputationSharedPtr(
			new ShadowMatrix(light,extent));

#if HD_API_VERSION>=32
	cache[HdLightTokens->params] = light;
	cache[HdLightTokens->shadowParams] = shadowParams;
	cache[HdLightTokens->shadowCollection] =
			HdRprimCollection(HdTokens->geometry,
			HdReprSelector(HdReprTokens->refined));
#else
	cache[HdStLightTokens->params] = light;
	cache[HdStLightTokens->shadowParams] = shadowParams;
	cache[HdStLightTokens->shadowCollection] =
			HdRprimCollection(HdTokens->geometry, HdReprTokens->refined);
#endif

	//* TODO multiple lights
	m_runShadows=light.HasShadow();
}

void HydraDelegate::SetLight(SdfPath const &id,
	TfToken const &key,VtValue value)
{
	ValueCache &cache = m_valueCacheMap[id];
	cache[key] = value;

#if HD_API_VERSION>=32
	if(key == HdLightTokens->params)
#else
	if(key == HdStLightTokens->params)
#endif
	{
		GlfSimpleLight light = value.Get<GlfSimpleLight>();

		m_runShadows=light.HasShadow();

#if HD_API_VERSION>=32
		HdxShadowParams shadowParams =
				cache[HdLightTokens->shadowParams].Get<HdxShadowParams>();
#else
		HdxShadowParams shadowParams =
				cache[HdStLightTokens->shadowParams].Get<HdxShadowParams>();
#endif

		shadowParams.enabled = light.HasShadow();
		shadowParams.resolution = light.GetShadowResolution();
		shadowParams.bias = light.GetShadowBias();
		shadowParams.blur = light.GetShadowBlur();

		//* update shadow matrix too
		const GfRange3d extent=GetExtents();
		shadowParams.shadowMatrix = HdxShadowMatrixComputationSharedPtr(
				new ShadowMatrix(light,extent));

#if HD_API_VERSION>=32
		cache[HdLightTokens->shadowParams] = shadowParams;

		GetRenderIndex().GetChangeTracker().MarkSprimDirty(
				id, HdLight::DirtyParams|HdLight::DirtyShadowParams);
#else
		cache[HdStLightTokens->shadowParams] = shadowParams;

		GetRenderIndex().GetChangeTracker().MarkSprimDirty(
				id, HdStLight::DirtyParams|HdStLight::DirtyShadowParams);
#endif
	}
#if HD_API_VERSION>=32
	else if(key == HdTokens->transform)
	{
		GetRenderIndex().GetChangeTracker().MarkSprimDirty(
				id, HdLight::DirtyTransform);
	}
	else if(key == HdLightTokens->shadowCollection)
	{
		GetRenderIndex().GetChangeTracker().MarkSprimDirty(
				id, HdLight::DirtyCollection);
	}
#else
	else if(key == HdStLightTokens->transform)
	{
		GetRenderIndex().GetChangeTracker().MarkSprimDirty(
				id, HdStLight::DirtyTransform);
	}
	else if(key == HdStLightTokens->shadowCollection)
	{
		GetRenderIndex().GetChangeTracker().MarkSprimDirty(
				id, HdStLight::DirtyCollection);
	}
#endif
}

void HydraDelegate::AddRenderTask(SdfPath const &id)
{
	GetRenderIndex().InsertTask<HdxRenderTask>(this, id);
	ValueCache &cache = m_valueCacheMap[id];
	cache[HdTokens->collection]=
			HdRprimCollection(HdTokens->geometry,
			HdReprSelector(HdReprTokens->smoothHull));

    // Don't filter on render tag.
    // XXX: However, this will mean no prim passes if any stage defines a tag
    cache[HdTokens->renderTags] = TfTokenVector();
}

void HydraDelegate::AddRenderSetupTask(SdfPath const &id)
{
	GetRenderIndex().InsertTask<HdxRenderSetupTask>(this, id);
	ValueCache &cache = m_valueCacheMap[id];

	HdxRenderTaskParams params;
	params.camera = m_cameraId;
	params.viewport = GfVec4f(0,0,512,512);
	params.enableLighting = true;
	params.wireframeColor = GfVec4f(0,0,0,1);

//	params.cullStyle = HdCullStyleFront;
	params.cullStyle = HdCullStyleNothing;

	params.depthFunc = HdCmpFuncLess;

//	params.enableAlphaToCoverage = false;
	params.alphaThreshold = 0.01;

	cache[HdTokens->params] = VtValue(params);
}

void HydraDelegate::AdjustRenderSetupTask(SdfPath const &id,BWORD a_lighting,
	const GfVec4f& viewport)
{
	ValueCache &cache = m_valueCacheMap[id];

	HdxRenderTaskParams params =
			cache[HdTokens->params].Get<HdxRenderTaskParams>();

	params.enableLighting = a_lighting;
	params.viewport = viewport;

	cache[HdTokens->params] = VtValue(params);

	GetRenderIndex().GetChangeTracker().MarkTaskDirty(
			id, HdChangeTracker::DirtyParams);
}

void HydraDelegate::AddSimpleLightTask(SdfPath const &id)
{
	GetRenderIndex().InsertTask<HdxSimpleLightTask>(this, id);
	ValueCache &cache = m_valueCacheMap[id];

	HdxSimpleLightTaskParams params;
	params.cameraPath = m_cameraId;
	params.viewport = GfVec4f(0,0,512,512);
	params.enableShadows = true;
	params.material.SetShininess(64.0);

	cache[HdTokens->params] = VtValue(params);
}

void HydraDelegate::AddShadowTask(SdfPath const &id)
{
	GetRenderIndex().InsertTask<HdxShadowTask>(this, id);
	ValueCache &cache = m_valueCacheMap[id];

	HdxShadowTaskParams params;
	params.camera = m_cameraId;
//	params.viewport = GfVec4f(0,0,512,512);
//	params.enableLighting = true;

	cache[HdTokens->params] = VtValue(params);
}

void
HydraDelegate::AddSelectionTask(SdfPath const &id)
{
	GetRenderIndex().InsertTask<HdxSelectionTask>(this, id);
}

void HydraDelegate::SetTaskParam(SdfPath const &id,
	TfToken const &name, VtValue val)
{
	ValueCache &cache = m_valueCacheMap[id];
	cache[name] = val;

	if(name == HdTokens->collection)
	{
		GetRenderIndex().GetChangeTracker().MarkTaskDirty(
				id, HdChangeTracker::DirtyCollection);
	}
	else if(name == HdTokens->params)
	{
		GetRenderIndex().GetChangeTracker().MarkTaskDirty(
				id, HdChangeTracker::DirtyParams);
	}
}

VtValue HydraDelegate::GetTaskParam(SdfPath const &id, TfToken const &name)
{
	return m_valueCacheMap[id][name];
}

//* see pxr/usdImaging/lib/usdImaging/defaultTaskDelegate.cpp
HdTaskSharedPtrVector HydraDelegate::GetRenderTasks(void)
{
	HdTaskSharedPtrVector tasks;

	tasks.push_back(GetRenderIndex().GetTask(SdfPath("/SimpleLightTask")));
	if(m_runShadows)
	{
		tasks.push_back(GetRenderIndex().GetTask(SdfPath("/ShadowTask")));
	}
	tasks.push_back(GetRenderIndex().GetTask(SdfPath("/RenderSetupTask")));
	tasks.push_back(GetRenderIndex().GetTask(SdfPath("/RenderTask")));

	return tasks;
}

void HydraDelegate::AddMesh(SdfPath const &id,
	GfMatrix4f const &transform,
	VtVec3rArray const &points,
	VtIntArray const &numVerts,
	VtIntArray const &verts,
	VtVec3rArray const &normals,
	PxOsdSubdivTags const &subdivTags,
	VtValue const &uv,
	HdInterpolation uvInterpolation,
	VtValue const &color,
	HdInterpolation colorInterpolation,
	bool guide,
	SdfPath const &instancerId,
	TfToken const &scheme,
	TfToken const &orientation,
	bool doubleSided)
{
	HD_TRACE_FUNCTION();

	SdfPath shaderId;
	TfMapLookup(m_surfaceShaderBindings, id, &shaderId);

	HdRenderIndex& index = GetRenderIndex();
	index.InsertRprim(HdPrimTypeTokens->mesh,
			this, id, instancerId);

	m_meshes[id] = Mesh(scheme, orientation, transform,
			points, numVerts, verts, normals, subdivTags,
			uv, uvInterpolation, color, colorInterpolation,
			guide, doubleSided);

	if(!instancerId.IsEmpty())
	{
		m_instancers[instancerId].prototypes.push_back(id);
	}
}

void HydraDelegate::AddBasisCurves(SdfPath const &id,
	GfMatrix4f const &transform,
	VtVec3rArray const &points,
	VtIntArray const &curveVertexCounts,
	VtVec3rArray const &normals,
	TfToken const &basis,
	VtValue const &color,
	HdInterpolation colorInterpolation,
	VtValue const &width,
	HdInterpolation widthInterpolation,
	SdfPath const &instancerId)
{
	HD_TRACE_FUNCTION();

	SdfPath shaderId;
	TfMapLookup(m_surfaceShaderBindings, id, &shaderId);

	HdRenderIndex& index = GetRenderIndex();
	index.InsertRprim(HdPrimTypeTokens->basisCurves,
			this, id, instancerId);

	VtIntArray curveIndices;

#if FE_HYDRA_CURVE_INDICES
//	if(FALSE)	//* HACK

	if(basis!=HdTokens->linear && basis!=HdTokens->bezier)
	{
		const I32 uniquePointCount=points.size();
		const I32 curveCount=curveVertexCounts.size();

		curveIndices.resize(uniquePointCount+2*curveCount);

		I32 uniquePointIndex=0;
		I32 pointIndex=0;

		for(I32 curveIndex=0;curveIndex<curveCount;curveIndex++)
		{
//			feLog("  cvc %d\n",curveVertexCounts[curveIndex]);
			const I32 subCount=curveVertexCounts[curveIndex]-2;
			if(subCount<2)
			{
				continue;
			}
//			feLog("  A %d %d\n",pointIndex,uniquePointIndex);
			curveIndices[pointIndex++]=uniquePointIndex;
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
//				feLog("  B %d %d\n",pointIndex,uniquePointIndex);
				curveIndices[pointIndex++]=uniquePointIndex++;
			}
//			feLog("  C %d %d\n",pointIndex,uniquePointIndex-1);
			curveIndices[pointIndex++]=uniquePointIndex-1;
		}

//		feLog("HydraDelegate::AddBasisCurves unique %d/%d indexed %d/%d\n",
//				uniquePointCount,uniquePointIndex,pointIndex,
//				curveIndices.size());
	}
#endif

	m_curves[id] = Curves(
			transform,
			curveVertexCounts,
			curveIndices,
			points,
			normals,
			basis,
			color, colorInterpolation,
			width, widthInterpolation);

	if(!instancerId.IsEmpty())
	{
		m_instancers[instancerId].prototypes.push_back(id);
	}
}

void HydraDelegate::AddPoints(SdfPath const &id,
	GfMatrix4f const &transform,
	VtVec3rArray const &points,
	VtVec3rArray const &normals,
	VtValue const &color,
	HdInterpolation colorInterpolation,
	VtValue const &width,
	HdInterpolation widthInterpolation,
	SdfPath const &instancerId)
{
	HD_TRACE_FUNCTION();

	HdRenderIndex& index = GetRenderIndex();
	SdfPath shaderId;
	TfMapLookup(m_surfaceShaderBindings, id, &shaderId);
	index.InsertRprim(HdPrimTypeTokens->points,
			this, id, instancerId);

	m_points[id] = Points(
			transform,
			points,
			normals,
			color, colorInterpolation,
			width, widthInterpolation);

	if(!instancerId.IsEmpty())
	{
		m_instancers[instancerId].prototypes.push_back(id);
	}
}

void HydraDelegate::AddInstancer(SdfPath const &id,
	SdfPath const &parentId,
	GfMatrix4f const &rootTransform)
{
	HD_TRACE_FUNCTION();

	HdRenderIndex& index = GetRenderIndex();
	// add instancer
	index.InsertInstancer(this, id, parentId);
	m_instancers[id] = Instancer();
	m_instancers[id].rootTransform = rootTransform;

	if(!parentId.IsEmpty())
	{
		m_instancers[parentId].prototypes.push_back(id);
	}

	// Instancers don't have initial dirty bits, so we need to add them.
	HdChangeTracker& tracker = GetRenderIndex().GetChangeTracker();
	tracker.MarkInstancerDirty(id,
			HdChangeTracker::DirtyTransform |
			HdChangeTracker::DirtyPrimvar |
			HdChangeTracker::DirtyInstanceIndex);
}

void HydraDelegate::SetInstancerProperties(SdfPath const &id,
	VtIntArray const &prototypeIndex,
	VtVec3rArray const &scale,
	VtVec4fArray const &rotate,
	VtVec3rArray const &translate)
{
	HD_TRACE_FUNCTION();

	if(!TF_VERIFY(prototypeIndex.size() == scale.size()) or
		!TF_VERIFY(prototypeIndex.size() == rotate.size()) or
		!TF_VERIFY(prototypeIndex.size() == translate.size()))
	{
		return;
	}

	m_instancers[id].scale = scale;
	m_instancers[id].rotate = rotate;
	m_instancers[id].translate = translate;
	m_instancers[id].prototypeIndices = prototypeIndex;

	HdChangeTracker& tracker = GetRenderIndex().GetChangeTracker();
	tracker.MarkInstancerDirty(id,
			HdChangeTracker::DirtyPrimvar |
			HdChangeTracker::DirtyInstanceIndex);

}

//void HydraDelegate::AddSurfaceShader(SdfPath const &id,
//							   std::string const &source,
//							   HdShaderParamVector const &params)
//{
//	HdRenderIndex& index = GetRenderIndex();
//    index.InsertSprim(HdPrimTypeTokens->shader, this, id);
//	m_surfaceShaders[id] = SurfaceShader(source, params);
//}

void HydraDelegate::AddTexture(SdfPath const& id,
	GlfTextureRefPtr const& texture)
{
	HdRenderIndex& index = GetRenderIndex();
    index.InsertBprim(HdPrimTypeTokens->texture, this, id);
	m_textures[id] = Texture(texture);
}

void HydraDelegate::AddMaterialResource(SdfPath const &id,
	VtValue materialResource)
{
	HdRenderIndex& index = GetRenderIndex();
	index.InsertSprim(HdPrimTypeTokens->material, this, id);
	m_materials[id] = materialResource;
}

void HydraDelegate::UpdateMaterialResource(SdfPath const &materialId,
											VtValue materialResource)
{
	m_materials[materialId] = materialResource;

	HdChangeTracker& tracker = GetRenderIndex().GetChangeTracker();
	tracker.MarkSprimDirty(materialId, HdMaterial::DirtyResource);

	/// XXX : Make sure all rprims know they have an invalid binding,
	//        some backends need to be notified when a material has
	//        been updated. This is a temp solution.
	for(auto const &p: m_materialBindings)
	{
		if(p.second == materialId)
		{
			tracker.MarkRprimDirty(p.first, HdChangeTracker::DirtyMaterialId);
		}
	}
}

void HydraDelegate::BindMaterial(SdfPath const &rprimId,
	SdfPath const &materialId)
{
	m_materialBindings[rprimId] = materialId;
}

void HydraDelegate::RebindMaterial(SdfPath const &rprimId,
	SdfPath const &materialId)
{
	BindMaterial(rprimId, materialId);

	// Mark the rprim material binding as dirty so sync gets
	// called on that rprim and also increase
	// the version of the global bindings so batches get rebuild (if needed)
	HdChangeTracker& tracker = GetRenderIndex().GetChangeTracker();
	tracker.MarkRprimDirty(rprimId, HdChangeTracker::DirtyMaterialId);
}

SdfPath HydraDelegate::GetMaterialId(SdfPath const& rprimId)
{
	SdfPath materialId;
	TfMapLookup(m_materialBindings, rprimId, &materialId);
	return materialId;
}

VtValue HydraDelegate::GetMaterialResource(SdfPath const &materialId)
{
	if(VtValue *material = TfMapLookupPtr(m_materials, materialId))
	{
		return *material;
	}
	return VtValue();
}

void HydraDelegate::HideRprim(SdfPath const& id)
{
    m_hiddenRprims.insert(id);
    MarkRprimDirty(id, HdChangeTracker::DirtyRenderTag);
}

void HydraDelegate::UnhideRprim(SdfPath const& id)
{
    m_hiddenRprims.erase(id);
    MarkRprimDirty(id, HdChangeTracker::DirtyRenderTag);
}

void HydraDelegate::SetReprSelector(SdfPath const &id,
	HdReprSelector const &reprSelector)
{
	if(m_meshes.find(id) != m_meshes.end())
	{
		m_meshes[id].reprSelector = reprSelector;
		MarkRprimDirty(id, HdChangeTracker::DirtyRepr);
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		m_curves[id].reprSelector = reprSelector;
		MarkRprimDirty(id, HdChangeTracker::DirtyRepr);
	}
	else if(m_points.find(id) != m_points.end())
	{
		m_points[id].reprSelector = reprSelector;
		MarkRprimDirty(id, HdChangeTracker::DirtyRepr);
	}
}

void HydraDelegate::SetRefineLevel(SdfPath const &id, int refineLevel)
{
	m_refineLevels[id] = refineLevel;

	MarkRprimDirty(id, HdChangeTracker::DirtyDisplayStyle);
}

static VtVec3rArray _AnimatePositions(VtVec3rArray const &positions, float time)
{
	VtVec3rArray result = positions;
	for(size_t i = 0; i < result.size(); ++i)
	{
		result[i] += GfVec3f((float)(0.5*sin(0.5*i + time)), 0, 0);
	}
	return result;
}

void HydraDelegate::UpdatePositions(SdfPath const &id, float time)
{
	if(m_meshes.find(id) != m_meshes.end())
	{
		m_meshes[id].points = _AnimatePositions(m_meshes[id].points, time);
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		m_curves[id].points = _AnimatePositions(m_curves[id].points, time);
	}
	else if(m_points.find(id) != m_points.end())
	{
		m_points[id].points = _AnimatePositions(m_points[id].points, time);
	}
	else
	{
		return;
	}

	MarkRprimDirty(id, HdChangeTracker::DirtyPoints);
}

void HydraDelegate::UpdateRprims(float time)
{
	const float delta = 0.01f;
	int meshIndex = 0;

	TF_FOR_ALL (it, m_meshes)
	{
#if HD_API_VERSION>=32
		MarkRprimDirty(it->first, HdChangeTracker::DirtyPrimvar);
#else
		MarkRprimDirty(it->first, HdChangeTracker::DirtyPrimVar);
#endif

		if(it->second.colorInterpolation == HdInterpolationConstant)
		{
			GfVec4f color = it->second.color.Get<GfVec4f>();
			color[0] = fmod(color[0] + delta, 1.0f);
			color[1] = fmod(color[1] + delta*2, 1.0f);
			it->second.color = VtValue(color);
		}
		++meshIndex;
	}
}

void HydraDelegate::UpdateInstancerPrimVars(float time)
{
	// update instancers
	TF_FOR_ALL (it, m_instancers)
	{
		for(size_t i = 0; i < it->second.rotate.size(); ++i)
		{
			const GfQuaternion q =
					GfRotation(GfVec3d(1, 0, 0), i*time).GetQuaternion();

			const GfVec4f quat(
					q.GetReal(),
					q.GetImaginary()[0],
					q.GetImaginary()[1],
					q.GetImaginary()[2]);

			it->second.rotate[i] = quat;
		}

#if HD_API_VERSION>=32
		GetRenderIndex().GetChangeTracker().MarkInstancerDirty(
				it->first,HdChangeTracker::DirtyPrimvar);
#else
		GetRenderIndex().GetChangeTracker().MarkInstancerDirty(
				it->first,HdChangeTracker::DirtyPrimVar);
#endif

		// propagate dirtiness to all prototypes
//		TF_FOR_ALL (protoIt, it->second.prototypes)
//		{
//			if(m_instancers.find(*protoIt) != m_instancers.end()) continue;
//
//			MarkRprimDirty(*protoIt, HdChangeTracker::DirtyInstancer);
//		}
	}
}

void HydraDelegate::UpdateInstancerPrototypes(float time)
{
	// update instancer prototypes
	TF_FOR_ALL (it, m_instancers)
	{
		// rotate prototype indices
		const int numInstances = it->second.prototypeIndices.size();

		if(numInstances > 0)
		{
			int firstPrototype = it->second.prototypeIndices[0];
			for(int i = 1; i < numInstances; ++i)
			{
				it->second.prototypeIndices[i-1] =
						it->second.prototypeIndices[i];
			}

			it->second.prototypeIndices[numInstances-1] = firstPrototype;
		}

		// invalidate instance index
		TF_FOR_ALL (protoIt, it->second.prototypes)
		{
			if(m_instancers.find(*protoIt) != m_instancers.end()) continue;

			MarkRprimDirty(*protoIt, HdChangeTracker::DirtyInstanceIndex);
		}
	}
}

bool HydraDelegate::IsInCollection(SdfPath const& id,
	TfToken const& collectionName)
{
	HD_TRACE_FUNCTION();
	if(m_hiddenRprims.find(id) != m_hiddenRprims.end())
	{
		return false;
	}

	// Visible collection.
	if(collectionName == HdTokens->geometry)
	{
		if(Mesh *mesh = TfMapLookupPtr(m_meshes, id))
		{
			return !mesh->guide;
		}
		else if(m_curves.count(id) > 0)
		{
			return true;
		}
		else if(m_points.count(id) > 0)
		{
			return true;
		}
	}
	else if(collectionName == TestTokens->geometryAndGuides)
	{
		return (m_meshes.count(id) > 0 or
				m_curves.count(id) > 0 or
				m_points.count(id));
	}

	// All other collections are considered coding errors, with no constituent
	// prims.
	TF_CODING_ERROR("Rprim Collection is unknown to HydraDelegate: %s",
			collectionName.GetString().c_str());

	return false;
}

HdMeshTopology HydraDelegate::GetMeshTopology(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	HdMeshTopology topology;
	const Mesh &mesh = m_meshes[id];

	return HdMeshTopology(mesh.scheme, mesh.orientation,
			mesh.numVerts, mesh.verts);
}

HdBasisCurvesTopology HydraDelegate::GetBasisCurvesTopology(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	const Curves &curve = m_curves[id];

	// Need to implement testing support for basis curves
	return HdBasisCurvesTopology(
			(curve.basis==HdTokens->linear)? HdTokens->linear: HdTokens->cubic,
			curve.basis,		//* bSpline, catmullRom, bezier, linear
			HdTokens->nonperiodic,
			curve.curveVertexCounts, curve.curveIndices);
}

PxOsdSubdivTags HydraDelegate::GetSubdivTags(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	const Mesh &mesh = m_meshes[id];

	return mesh.subdivTags;
}

GfRange3d HydraDelegate::GetExtent(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	VtVec3rArray points;
	if(m_meshes.find(id) != m_meshes.end())
	{
		points = m_meshes[id].points;
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		points = m_curves[id].points;
	}
	else if(m_points.find(id) != m_points.end())
	{
		points = m_points[id].points;
	}

	GfRange3d range;
	TF_FOR_ALL(it, points)
	{
		range.UnionWith(*it);
	}

	return range;
}

GfRange3d HydraDelegate::GetExtents(void)
{
	GfRange3d range;

	for(std::map<SdfPath, Mesh>::const_iterator it=m_meshes.begin();
			it!=m_meshes.end(); it++)
	{
		TF_FOR_ALL(it2, it->second.points)
		{
			range.UnionWith(*it2);
		}
	}

	for(std::map<SdfPath, Curves>::const_iterator it=m_curves.begin();
			it!=m_curves.end(); it++)
	{
		TF_FOR_ALL(it2, it->second.points)
		{
			range.UnionWith(*it2);
		}
	}

	for(std::map<SdfPath, Points>::const_iterator it=m_points.begin();
			it!=m_points.end(); it++)
	{
		TF_FOR_ALL(it2, it->second.points)
		{
			range.UnionWith(*it2);
		}
	}

	return range;
}

bool HydraDelegate::GetDoubleSided(SdfPath const& id)
{
	if(m_meshes.find(id) != m_meshes.end())
	{
		return m_meshes[id].doubleSided;
	}
	return false;
}

//int HydraDelegate::GetRefineLevel(SdfPath const& id)
//{
//	if(m_refineLevels.find(id) != m_refineLevels.end())
//	{
//		feLog("HydraDelegate::GetRefineLevel %d\n",m_refineLevels[id]);
//		return m_refineLevels[id];
//	}
//
//	feLog("HydraDelegate::GetRefineLevel fallback %d\n",m_refineLevel);
//
//	// returns fallback refinelevel
//	return m_refineLevel;
//}

HdDisplayStyle HydraDelegate::GetDisplayStyle(SdfPath const& id)
{
    if (m_refineLevels.find(id) != m_refineLevels.end()) {
        return HdDisplayStyle(m_refineLevels[id]);
    }
    // returns fallback refinelevel
    return HdDisplayStyle(m_refineLevel);
}

VtIntArray HydraDelegate::GetInstanceIndices(SdfPath const& instancerId,
	SdfPath const& prototypeId)
{
	HD_TRACE_FUNCTION();
	VtIntArray indices(0);

	// XXX: this is very naive implementation for unit test.

	//   transpose prototypeIndices/instances to instanceIndices/prototype
	if(Instancer *instancer = TfMapLookupPtr(m_instancers, instancerId))
	{
		size_t prototypeIndex = 0;

		for(;prototypeIndex < instancer->prototypes.size(); ++prototypeIndex)
		{
			if(instancer->prototypes[prototypeIndex] == prototypeId) break;
		}

		if(prototypeIndex == instancer->prototypes.size())
		{
			return indices;
		}

		// XXX use const_ptr
		for(size_t i = 0; i < instancer->prototypeIndices.size(); ++i)
		{
			if(instancer->prototypeIndices[i] ==
					static_cast<int>(prototypeIndex))
			{
				indices.push_back(i);
			}
		}
	}

	return indices;
}

GfMatrix4d HydraDelegate::GetInstancerTransform(SdfPath const& instancerId)
{
	HD_TRACE_FUNCTION();

	if(Instancer *instancer = TfMapLookupPtr(m_instancers, instancerId))
	{
		return GfMatrix4d(instancer->rootTransform);
	}
	return GfMatrix4d(1);
}

VtValue HydraDelegate::GetCameraParamValue(SdfPath const &cameraId,
	TfToken const &paramName)
{
	ValueCache *pCache = TfMapLookupPtr(m_valueCacheMap, cameraId);
	VtValue ret;
	if (pCache && TfMapLookup(*pCache, paramName, &ret)) {
		return ret;
	}

	return VtValue();
}


//std::string HydraDelegate::GetSurfaceShaderSource(SdfPath const &shaderId)
//{
//	if(SurfaceShader *shader = TfMapLookupPtr(m_surfaceShaders, shaderId))
//	{
//		return shader->source;
//	}
//	else
//	{
//		return TfToken();
//	}
//}

//TfTokenVector HydraDelegate::GetSurfaceShaderParamNames(SdfPath const &shaderId)
//{
//	TfTokenVector names;
//	if(SurfaceShader *shader = TfMapLookupPtr(m_surfaceShaders, shaderId))
//	{
//		TF_FOR_ALL(paramIt, shader->params)
//		{
//			names.push_back(TfToken(paramIt->GetName()));
//		}
//	}
//	return names;
//}

//HdShaderParamVector HydraDelegate::GetSurfaceShaderParams(
//	SdfPath const &shaderId)
//{
//	if(SurfaceShader *shader = TfMapLookupPtr(m_surfaceShaders, shaderId))
//	{
//		return shader->params;
//	}
//
//	return HdShaderParamVector();
//}

//VtValue HydraDelegate::GetSurfaceShaderParamValue(SdfPath const &shaderId,
//	TfToken const &paramName)
//{
//	if(SurfaceShader *shader = TfMapLookupPtr(m_surfaceShaders, shaderId))
//	{
//		TF_FOR_ALL(paramIt, shader->params)
//		{
//			if(paramIt->GetName() == paramName)
//			{
//				return paramIt->GetFallbackValue();
//			}
//		}
//	}
//
//	return VtValue();
//}

//HdTextureResource::ID HydraDelegate::GetTextureResourceID(
//	SdfPath const& textureId)
//{
//	return SdfPath::Hash()(textureId);
//}
//
//HdTextureResourceSharedPtr HydraDelegate::GetTextureResource(
//	SdfPath const& textureId)
//{
//	GlfTextureHandleRefPtr texture =
//			GlfTextureRegistry::GetInstance().GetTextureHandle(
//			m_textures[textureId].texture);
//
//	// Simple way to detect if the glf texture is ptex or not
//	HdTextureType textureType = HdTextureType::Uv;
//#ifdef PXR_PTEX_SUPPORT_ENABLED
//	GlfPtexTextureRefPtr pTex =
//			TfDynamic_cast<GlfPtexTextureRefPtr>(m_textures[textureId].texture);
//	if (pTex)
//	{
//		textureType = HdTextureType::Ptex;
//	}
//#endif
//
//	return HdTextureResourceSharedPtr(new HdStSimpleTextureResource(
//			texture,
//			textureType,
//			HdWrapUseMetadata,
//			HdWrapUseMetadata,
//			HdWrapUseMetadata,
//			HdMinFilterNearestMipmapLinear,
//			HdMagFilterLinear,
//			0));
//}

void HydraDelegate::SetTransform(SdfPath const& id,
	GfMatrix4f const &transform)
{
	HD_TRACE_FUNCTION();

//	feLog("HydraDelegate::SetTransform \"%s\"\n",id.GetText());

	if(m_meshes.find(id) != m_meshes.end())
	{
		m_meshes[id].transform=transform;
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		m_curves[id].transform=transform;
	}
	else if(m_points.find(id) != m_points.end())
	{
		m_points[id].transform=transform;
	}

    MarkRprimDirty(id, HdChangeTracker::DirtyTransform);
}

GfMatrix4d HydraDelegate::GetTransform(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

//	feLog("HydraDelegate::GetTransform \"%s\"\n",id.GetText());

	if(m_meshes.find(id) != m_meshes.end())
	{
		return GfMatrix4d(m_meshes[id].transform);
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		return GfMatrix4d(m_curves[id].transform);
	}
	else if(m_points.find(id) != m_points.end())
	{
		return GfMatrix4d(m_points[id].transform);
	}
	return GfMatrix4d(1);
}

bool HydraDelegate::GetVisible(SdfPath const& id)
{
	HD_TRACE_FUNCTION();
	return true;
}

VtValue HydraDelegate::Get(SdfPath const& id, TfToken const& key)
{
	HD_TRACE_FUNCTION();

//	feLog("HydraDelegate::Get \"%s\" \"%s\"\n",id.GetText(),key.GetText());

#if HD_API_VERSION==30
	if(key == "matrices")
	{
		ValueCache &cache = m_valueCacheMap[id];

		HdStCameraMatrices matrices;
		matrices.viewMatrix=
				cache[HdStCameraTokens->worldToViewMatrix].Get<GfMatrix4d>();
		matrices.projMatrix=
				cache[HdStCameraTokens->projectionMatrix].Get<GfMatrix4d>();
		return VtValue(matrices);
	}
#endif

	// tasks
	ValueCache *vcache = TfMapLookupPtr(m_valueCacheMap, id);
	VtValue ret;
	if(vcache and TfMapLookup(*vcache, key, &ret))
	{
		return ret;
	}

	VtValue value;
	if(key == HdTokens->points)
	{
		if(m_meshes.find(id) != m_meshes.end())
		{
			return VtValue(m_meshes[id].points);
		}
		else if(m_curves.find(id) != m_curves.end())
		{
			return VtValue(m_curves[id].points);
		}
		else if(m_points.find(id) != m_points.end())
		{
			return VtValue(m_points[id].points);
		}
	}
	else if(key == HdTokens->normals)
	{
		if(m_meshes.find(id) != m_meshes.end())
		{
			return VtValue(m_meshes[id].normals);
		}
		if(m_curves.find(id) != m_curves.end())
		{
			return VtValue(m_curves[id].normals);
		}
		if(m_points.find(id) != m_points.end())
		{
			return VtValue(m_points[id].normals);
		}
	}
	else if(key == TfToken("uv"))
	{
		if(m_meshes.find(id) != m_meshes.end())
		{
			return m_meshes[id].uv;
		}
	}
	else if(key == HdTokens->displayColor)
	{
		if(m_meshes.find(id) != m_meshes.end())
		{
			return m_meshes[id].color;
		}
		else if(m_curves.find(id) != m_curves.end())
		{
			return m_curves[id].color;
		}
		else if(m_points.find(id) != m_points.end())
		{
			return m_points[id].color;
		}
	}
	else if(key == HdTokens->widths)
	{
		if(m_curves.find(id) != m_curves.end())
		{
			return m_curves[id].width;
		}
		else if(m_points.find(id) != m_points.end())
		{
			return m_points[id].width;
		}
	}
	else if(key == HdInstancerTokens->scale)
	{
		if(m_instancers.find(id) != m_instancers.end())
		{
			return VtValue(m_instancers[id].scale);
		}
	}
	else if(key == HdInstancerTokens->rotate)
	{
		if(m_instancers.find(id) != m_instancers.end())
		{
			return VtValue(m_instancers[id].rotate);
		}
	}
	else if(key == HdInstancerTokens->translate)
	{
		if(m_instancers.find(id) != m_instancers.end())
		{
			return VtValue(m_instancers[id].translate);
		}
	}
//	else if(key == HdShaderTokens->surfaceShader)
//	{
//		SdfPath shaderId;
//		TfMapLookup(m_surfaceShaderBindings, id, &shaderId);
//
//		return VtValue(shaderId);
//	}

//	feLog("HydraDelegate::Get \"%s\" \"%s\" NOT FOUND\n",
//			id.GetText(),key.GetText());

	return value;
}

HdReprSelector HydraDelegate::GetReprSelector(SdfPath const &id)
{
    HD_TRACE_FUNCTION();

//	feLog("HydraDelegate::GetReprSelector \"%s\"\n",id.GetText());

    if(m_meshes.find(id) != m_meshes.end())
	{
        return m_meshes[id].reprSelector;
    }
    if(m_curves.find(id) != m_curves.end())
	{
        return m_curves[id].reprSelector;
    }
    if(m_points.find(id) != m_points.end())
	{
        return m_points[id].reprSelector;
    }
    return HdReprSelector();
}

#if HD_API_VERSION>=32
HdPrimvarDescriptorVector HydraDelegate::GetPrimvarDescriptors(
	SdfPath const& id,HdInterpolation interpolation)
{
	HdPrimvarDescriptorVector primvars;
	if(interpolation == HdInterpolationVertex)
	{
		primvars.emplace_back(HdTokens->points, interpolation,
				HdPrimvarRoleTokens->point);
	}

	if(m_meshes.find(id) != m_meshes.end())
	{
		if(m_meshes[id].uvInterpolation == interpolation)
		{
			primvars.emplace_back(TfToken("uv"), interpolation,
					HdPrimvarRoleTokens->textureCoordinate);
		}

		if(m_meshes[id].colorInterpolation == interpolation)
		{
			primvars.emplace_back(HdTokens->displayColor, interpolation,
					HdPrimvarRoleTokens->color);
		}

		if(interpolation == HdInterpolationVertex &&
				m_meshes[id].normals.size() > 0)
		{
			primvars.emplace_back(HdTokens->normals, interpolation,
					HdPrimvarRoleTokens->normal);
		}
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		if(m_curves[id].colorInterpolation == interpolation)
		{
			primvars.emplace_back(HdTokens->displayColor, interpolation,
					HdPrimvarRoleTokens->color);
		}

		if(m_curves[id].widthInterpolation == interpolation)
		{
			primvars.emplace_back(HdTokens->widths, interpolation);
		}

		if(interpolation == HdInterpolationVertex &&
				m_curves[id].normals.size() > 0)
		{
			primvars.emplace_back(HdTokens->normals, interpolation,
					HdPrimvarRoleTokens->normal);
		}
	}
	else if(m_points.find(id) != m_points.end())
	{
		if(m_points[id].colorInterpolation == interpolation)
		{
			primvars.emplace_back(HdTokens->displayColor, interpolation,
					HdPrimvarRoleTokens->color);
		}

		if(m_points[id].widthInterpolation == interpolation)
		{
			primvars.emplace_back(HdTokens->widths, interpolation);
		}

		if(interpolation == HdInterpolationVertex &&
				m_points[id].normals.size() > 0)
		{
			primvars.emplace_back(HdTokens->normals, interpolation,
					HdPrimvarRoleTokens->normal);
		}
	}

	if (interpolation == HdInterpolationInstance &&
		m_instancers.find(id) != m_instancers.end())
	{
		primvars.emplace_back(HdInstancerTokens->scale, interpolation);
		primvars.emplace_back(HdInstancerTokens->rotate, interpolation);
		primvars.emplace_back(HdInstancerTokens->translate, interpolation);
	}
	return primvars;
}
#else
TfTokenVector HydraDelegate::GetPrimVarVertexNames(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	TfTokenVector names;

	names.push_back(HdTokens->points);

	if(m_meshes.find(id) != m_meshes.end())
	{
		if(m_meshes[id].colorInterpolation == HdInterpolationVertex)
		{
			names.push_back(HdTokens->color);
		}

		if(m_meshes[id].normals.size() > 0)
		{
			names.push_back(HdTokens->normals);
		}
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		if(m_curves[id].colorInterpolation == HdInterpolationVertex)
		{
			names.push_back(HdTokens->color);
		}

		if(m_curves[id].widthInterpolation == HdInterpolationVertex)
		{
			names.push_back(HdTokens->widths);
		}

		if(m_curves[id].normals.size() > 0)
		{
			names.push_back(HdTokens->normals);
		}
	}
	else if(m_points.find(id) != m_points.end())
	{
		if(m_points[id].colorInterpolation == HdInterpolationVertex)
		{
			names.push_back(HdTokens->color);
		}

		if(m_points[id].widthInterpolation == HdInterpolationVertex)
		{
			names.push_back(HdTokens->widths);
		}

		if(m_points[id].normals.size() > 0)
		{
			names.push_back(HdTokens->normals);
		}
	}
	return names;
}

TfTokenVector HydraDelegate::GetPrimVarVaryingNames(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	TfTokenVector names;
	if(m_curves.find(id) != m_curves.end())
	{
		if(m_curves[id].widthInterpolation == HdInterpolationVarying)
		{
			names.push_back(HdTokens->widths);
		}
	}
	return names;
}

TfTokenVector HydraDelegate::GetPrimVarFacevaryingNames(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	TfTokenVector names;
	if(m_meshes.find(id) != m_meshes.end())
	{
		if(m_meshes[id].colorInterpolation == HdInterpolationFaceVarying)
		{
			names.push_back(HdTokens->color);
		}
	}
	return names;
}

TfTokenVector HydraDelegate::GetPrimVarUniformNames(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	TfTokenVector names;
	if(m_meshes.find(id) != m_meshes.end())
	{
		if(m_meshes[id].colorInterpolation == HdInterpolationUniform)
		{
			names.push_back(HdTokens->color);
		}
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		if(m_curves[id].colorInterpolation == HdInterpolationUniform)
		{
			names.push_back(HdTokens->color);
		}

		if(m_curves[id].widthInterpolation == HdInterpolationUniform)
		{
			names.push_back(HdTokens->widths);
		}
	}
	return names;
}

TfTokenVector HydraDelegate::GetPrimVarConstantNames(SdfPath const& id)
{
	HD_TRACE_FUNCTION();

	TfTokenVector names;
	if(m_meshes.find(id) != m_meshes.end())
	{
		if(m_meshes[id].colorInterpolation == HdInterpolationConstant)
		{
			names.push_back(HdTokens->color);
		}
	}
	else if(m_curves.find(id) != m_curves.end())
	{
		if(m_curves[id].colorInterpolation == HdInterpolationConstant)
		{
			names.push_back(HdTokens->color);
		}
		if(m_curves[id].widthInterpolation == HdInterpolationConstant)
		{
			names.push_back(HdTokens->widths);
		}
	}
	else if(m_points.find(id) != m_points.end())
	{
		if(m_points[id].colorInterpolation == HdInterpolationConstant)
		{
			names.push_back(HdTokens->color);
		}

		if(m_points[id].widthInterpolation == HdInterpolationConstant)
		{
			names.push_back(HdTokens->widths);
		}
	}
	return names;
}

TfTokenVector HydraDelegate::GetPrimVarInstanceNames(SdfPath const &id)
{
	HD_TRACE_FUNCTION();

	TfTokenVector names;
	if(!m_hasInstancePrimVars)
	{
		return names;
	}

	if(m_instancers.find(id) != m_instancers.end())
	{
		names.push_back(HdInstancerTokens->scale);
		names.push_back(HdInstancerTokens->rotate);
		names.push_back(HdInstancerTokens->translate);
	}
	return names;
}

int HydraDelegate::GetPrimVarDataType(SdfPath const& id, TfToken const& key)
{
	HD_TRACE_FUNCTION();
	return 1;
}

int HydraDelegate::GetPrimVarComponents(SdfPath const& id, TfToken const& key)
{
	HD_TRACE_FUNCTION();
	return 1;
}
#endif

template <typename T>
static VtArray<T> _BuildArray(T values[], int numValues)
{
	VtArray<T> result(numValues);
	std::copy(values, values+numValues, result.begin());
	return result;
}

void HydraDelegate::Remove(SdfPath const &id)
{
//	feLog("HydraDelegate::Remove \"%s\"\n",id.GetText());

	GetRenderIndex().RemoveRprim(id);

	m_meshes.erase(id);
	m_curves.erase(id);
	m_points.erase(id);
	m_instancers.erase(id);
//	m_surfaceShaders.erase(id);
	m_surfaceShaderBindings.erase(id);
	m_textures.erase(id);
	m_hiddenRprims.erase(id);
	m_refineLevels.erase(id);
	m_valueCacheMap.erase(id);
}

void HydraDelegate::Clear(void)
{
	GetRenderIndex().Clear();
}

void HydraDelegate::MarkRprimDirty(SdfPath path,
	HdChangeTracker::RprimDirtyBits flag)
{
	GetRenderIndex().GetChangeTracker().MarkRprimDirty(path, flag);
}
