import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs+ [
                "fexGeometryDLLib",
                "fexThreadDLLib",
                "fexOperatorDLLib",
                "fexGrassDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexOperateDLLib",
                        "fexDrawDLLib",
                        "fexSurfaceDLLib" ]

    tests = [   'xBlade'    ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xBlade.exe",          "",                         None,       None) ]
