/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"
#include "window/WindowEvent.h"

using namespace fe;
using namespace fe::ext;

class MyDraw: public HandlerI
{
	public:
						MyDraw(sp<QuickViewerI> spQuickViewerI,
								sp<DrawI> spDrawI,
								sp<PointI> spPickPoint,
								sp<ImageI> spImageI):
							m_spQuickViewerI(spQuickViewerI),
							m_spDrawI(spDrawI),
							m_spPickPoint(spPickPoint)
						{
							m_spSolid=new DrawMode();
							m_spSolid->setDrawStyle(DrawMode::e_solid);
							m_spSolid->setLayer(1);

							m_spEdged=new DrawMode();
							m_spEdged->setDrawStyle(DrawMode::e_edgedSolid);
							m_spEdged->setLayer(2);

							if(spImageI.isValid())
							{
								m_spSolid->setTextureImage(spImageI);
							}

							m_spWireframe=new DrawMode();
							m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
							m_spWireframe->setLineWidth(1.5f);
						}

virtual void			handle(Record &record);


	private:
		sp<DrawMode>		m_spEdged;
		sp<DrawMode>		m_spSolid;
		sp<DrawMode>		m_spWireframe;
		sp<QuickViewerI>	m_spQuickViewerI;
		sp<DrawI>			m_spDrawI;
		sp<PointI>			m_spPickPoint;
		sp<ViewI>			m_spViewI;
		AsViewer			m_asViewer;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	int viewercount = 1;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

#if FALSE
		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexOpenILDL");
		if(!successful(result))
		{
			feLog("fexOpenILDL not found, trying fexSDLDL\n");
			result=spRegistry->manage("fexSDLDL");
		}
#else
		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));
#endif

		const char* libname="fexViewerDL";
		const char* viewername="QuickViewerI";

		U32 frames=(argc>1)? atoi(argv[argc-1])+1: 0;
		if(argc>4)
		{
			libname=argv[1];
			viewername=argv[2];
			viewercount=atoi(argv[3]);
		}
		if(argc>3)
		{
			libname=argv[1];
			viewername=argv[2];
		}
		else if(argc>2)
		{
			libname=argv[1];
			if(frames<2)
			{
				frames=0;
				viewername=argv[2];
			}
		}
		else if(argc>1)
		{
			if(frames<2)
			{
				frames=0;
				libname=argv[1];
			}
		}

		feLog("libname=%s\n",libname);
		feLog("viewername=%s\n",viewername);
		feLog("viewercount=%d\n",viewercount);
		feLog("frames=%d\n",frames);

		result=spRegistry->manage(libname);
		UNIT_TEST(successful(result));

		{
			std::vector<sp<QuickViewerI>> viewers;
			std::vector<sp<MyDraw>> drawers;
			// this is to keep those drawers from self-destructing!

			for(int i = 0; i < viewercount; i++)
			{
				sp<QuickViewerI> spQuickViewerI(spRegistry->create(viewername));
				sp<PointI> spPickPoint(spRegistry->create("*.PickPoint"));
				sp<ImageI> spImageI(spRegistry->create("ImageI"));
				if(!spQuickViewerI.isValid())
					feX(argv[0], "couldn't create components");

				spQuickViewerI->open();

				sp<WindowI> spWindowI = spQuickViewerI->getWindowI();
				if(spWindowI.isValid())
				{
					sp<EventContextI> spEventContextI =
							spWindowI->getEventContextI();
					spEventContextI->setPointerMotion(TRUE);
				}

				sp<DrawI> spDrawI = spQuickViewerI->getDrawI();
				UNIT_TEST(spDrawI.isValid());

				if(spPickPoint.isValid())
				{
					spQuickViewerI->insertEventHandler(spPickPoint);
				}

				if(spImageI.isValid())
				{
					String longname=
							spMaster->catalog()->catalog<String>("path:media")+
							"/image/FreeElectron64.png";
					spImageI->loadSelect(longname);

					feLog("spImageI %d\n",spImageI.isValid());
				}

				sp<MyDraw> spMyDraw(new MyDraw(
						spQuickViewerI, spDrawI, spPickPoint, spImageI));
				spQuickViewerI->insertEventHandler(spMyDraw);
				spQuickViewerI->insertDrawHandler(spMyDraw);

				viewers.push_back(spQuickViewerI);
				drawers.push_back(spMyDraw);
			}

			unsigned long framecount = 0;
			while(frames == 0 || framecount <= frames)
			{
				for (int i = 0; i < viewercount; i++)
				{
					viewers[i]->run(1);
				}

				framecount++;
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4 + viewercount);
	UNIT_RETURN();
}

void MyDraw::handle(Record &render)
{
	if(!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(render.layout()->scope());
	}

	if(m_asViewer.viewer_spatial.check(render) &&
			m_asViewer.viewer_layer.check(render) &&
			m_asViewer.viewer_layer(render))
	{
		const Color white(1.0f,1.0f,1.0f,1.0f);
		const Color yellow(1.0f,1.0f,0.0f,1.0f);
		const Color pink(1.0f,0.5f,0.5f,1.0f);
		const Color cyan(0.0f,1.0f,1.0f,1.0f);
		const Color green(0.0f,1.0f,0.0f,1.0f);
		const Color blue(0.0f,0.0f,1.0f,1.0f);

		const SpatialVector origin(0.0f,0.0f,0.0f);
		const SpatialVector corner(10.0f,40.0f,0.0f);
		const SpatialVector point(0.0f,2.0f,3.0f);

		switch(m_asViewer.viewer_spatial(render))
		{
			case 1:
			{
				m_spViewI=m_spDrawI->view();
				m_spDrawI->drawAxes(1.0f);
				m_spDrawI->drawAlignedText(origin,"origin",yellow);

				m_spDrawI->setDrawMode(m_spEdged);

				SpatialVector vector[2];
				vector[0]=point;
				set(vector[1],1.0f,4.0f,6.0f);
				m_spDrawI->drawRectangles(vector,2,false,&cyan);

				if(m_spPickPoint.isValid())
				{
					m_spPickPoint->setView(m_spDrawI->view());

					const Real radius1=0.5f;
					const Real radius2=1.0f;
					m_spPickPoint->drawFeedback(m_spDrawI);

					m_spDrawI->setDrawMode(m_spSolid);

					SpatialTransform transform;
					SpatialVector scale(radius1,radius1,radius1);

					WindowEvent::Mask mask(WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,WindowEvent::e_statePress);
					setIdentity(transform);
					translate(transform,m_spPickPoint->point(mask));
					m_spDrawI->drawSphere(transform,&scale,white);

					mask.setItem(WindowEvent::e_itemMiddle);
					setIdentity(transform);
					translate(transform,m_spPickPoint->point(mask));
					m_spDrawI->drawSphere(transform,&scale,green);

					mask.setItem(WindowEvent::e_itemRight);
					setIdentity(transform);
					translate(transform,m_spPickPoint->point(mask));
					m_spDrawI->drawSphere(transform,&scale,blue);

					set(scale,radius2,radius2,radius2);
					mask.setState(WindowEvent::e_stateRelease);

					mask.setItem(WindowEvent::e_itemLeft);
					setIdentity(transform);
					translate(transform,m_spPickPoint->point(mask));
					m_spDrawI->drawSphere(transform,&scale,white);

					mask.setItem(WindowEvent::e_itemMiddle);
					setIdentity(transform);
					translate(transform,m_spPickPoint->point(mask));
					m_spDrawI->drawSphere(transform,&scale,green);

					mask.setItem(WindowEvent::e_itemRight);
					setIdentity(transform);
					translate(transform,m_spPickPoint->point(mask));
					m_spDrawI->drawSphere(transform,&scale,blue);
				}

				SpatialVector corners[6];
				SpatialVector normals[6];
				Vector2 uvs[6];
				for(I32 m=0;m<6;m++)
				{
					set(normals[m],0.0,0.0,1.0);
				}
				set(corners[0],-1.0,-1.0,-2.0);
				set(corners[1],1.0,-1.0,-2.0);
				set(corners[2],1.0,1.0,-2.0);
				set(corners[3],-1.0,1.0,-2.0);
				corners[4]=corners[0];
				corners[5]=corners[2];
				set(uvs[0],0.0,0.0);
				set(uvs[1],1.0,0.0);
				set(uvs[2],1.0,1.0);
				set(uvs[3],0.0,1.0);
				uvs[4]=uvs[0];
				uvs[5]=uvs[2];
				m_spDrawI->setDrawMode(m_spSolid);
				m_spDrawI->drawTriangles(corners,normals,uvs,6,
							DrawI::e_discrete,FALSE,&white);
			}
				break;

			case 0:
			{
				const String string="Howdy";

				I32 ascent=0;
				I32 descent=0;
				U32 width=0;

//~				sp<WindowI> spWindowI=m_spQuickViewerI->getWindowI();
//~				if(spWindowI.isValid())
//~				{
//~					sp<EventContextI> spEventContextI=
//~							spWindowI->getEventContextI();
//~					spEventContextI->fontHeight(&ascent,&descent);
//~					width=spEventContextI->pixelWidth(string);
//~				}

				sp<FontI> spFontI=m_spDrawI->font();
				if(spFontI.isValid())
				{
					spFontI->fontHeight(&ascent,&descent);
					width=spFontI->pixelWidth(string);
				}

//				feLog("ascent=%d descent=%d width=%d\n",ascent,descent,width);

				const I32 margin=1;
				SpatialVector vertex[2];
				vertex[0]=corner+SpatialVector(-margin,-descent-margin);
				vertex[1]=corner+SpatialVector(width+margin,ascent+margin);

//				feLog("%s  %s\n",print(vertex[0]).c_str(),
//						print(vertex[1]).c_str());

				SpatialVector vertexB[2];
				vertexB[0]=vertex[0];
				vertexB[1]=vertex[1];
				vertexB[1][0]=corner[0]-margin+width/3;

				m_spDrawI->drawRectangles(vertexB,2,false,&blue);

				m_spDrawI->drawAlignedText(
						corner+SpatialVector(0.0f,0.0f,1.0f),string,pink);

				vertexB[0][0]=corner[0]+width*2/3;
				vertexB[1][0]=corner[0]+margin+width;
				m_spDrawI->drawRectangles(vertexB,2,false,&blue);

				if(m_spViewI.isValid())
				{
					const SpatialVector scale(5.0f,5.0f,5.0f);
					SpatialTransform transform;
					setIdentity(transform);
					SpatialVector center=m_spViewI->project(point,
							ViewI::e_perspective);
					center[2]=1.0f;
					translate(transform,center);

					m_spDrawI->setDrawMode(m_spWireframe);
					m_spDrawI->drawCircle(transform,&scale,yellow);
				}
			}
				break;
		}
	}
}
