/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_ChannelBinder_h__
#define __viewer_ChannelBinder_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT ChannelBinder :
	virtual public HandlerI,
	virtual public Config,
	public Initialize<ChannelBinder>
{
	public:
				ChannelBinder(void);
virtual			~ChannelBinder(void);

		void	initialize(void);

virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void	handle(Record &r_signal);

	private:
		AsChannelBinding	m_asChannelBinding;
		AsInit				m_asInit;
		sp<BaseType>		m_vectorType;
		sp<BaseType>		m_realType;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_ChannelBinder_h__ */

