/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fex/pyfe.h"
#include <boost/python.hpp>
using namespace boost::python;
#include "fe_data.h"

ScopePtr::ScopePtr(void)
{
	m_spScope = new fe::Scope(pyfe::feMaster());
}

ScopePtr::ScopePtr(fe::sp<fe::Scope> spScope)
{
	m_spScope = spScope;
}

ScopePtr::~ScopePtr(void)
{
//	m_spScope->shutdown();
}

LayoutPtr	ScopePtr::declare(const std::string &name)
{
	LayoutPtr lptr;
	fe::String s = name.c_str();
	lptr = m_spScope->declare(s);
	return lptr;
}
LayoutPtr	ScopePtr::lookupLayout(const std::string &name)
{
	LayoutPtr lptr;
	fe::String s = name.c_str();
	lptr = m_spScope->lookupLayout(s);
	return lptr;
}
PyObject	*ScopePtr::createRecord(const LayoutPtr &layoutPtr)
{
	return pyfe::bindRecord(m_spScope->createRecord(layoutPtr.ptr()));
}
PyObject	*ScopePtr::createRecordArray(const LayoutPtr &layoutPtr)
{
	return pyfe::bindRecordArray(
		m_spScope->createRecordArray(layoutPtr.ptr(), 0));
}
bool		ScopePtr::clear(const std::string &name)
{
	fe::String s = name.c_str();
	return m_spScope->clear(s);
}

void ScopePtr::support(const std::string &a, const std::string &b)
{
	fe::String aa = a.c_str();
	fe::String bb = b.c_str();
	m_spScope->support(aa,bb);
}

void ScopePtr::enforce(const std::string &a, const std::string &b)
{
	fe::String aa = a.c_str();
	fe::String bb = b.c_str();
	m_spScope->enforce(aa,bb);
}

void ScopePtr::populate(const std::string &a, const std::string &b)
{
	fe::String aa = a.c_str();
	fe::String bb = b.c_str();
	m_spScope->populate(aa,bb);
}

void ScopePtr::addWatcher(WatcherFS &watcher)
{
	m_spScope->addWatcher(watcher.ptr());
}


void		ScopePtr::dump(void)
{
	fe::Peeker peek;
	peek(*m_spScope->typeMaster());
	peek(*m_spScope);
	feLog(peek.output().c_str());
}

fe::sp<fe::Scope> ScopePtr::ptr(void) const
{
	return m_spScope;
}

LayoutPtr::LayoutPtr(void)
{
}

LayoutPtr::LayoutPtr(PyObject *pObj)
{
	fe::Record rec = pyfe::extractRecord(pObj);
	m_spLayout = rec.layout();
}

LayoutPtr &LayoutPtr::operator=(fe::sp<fe::Layout> spLayout)
{
	m_spLayout = spLayout;
	return *this;
}

LayoutPtr LayoutPtr::populate(const std::string &attr, const std::string &typ)
{
	m_spLayout->scope()->support(attr.c_str(), typ.c_str());
	m_spLayout->populate(attr.c_str());
	return *this;
}

ScopePtr LayoutPtr::scope(void)
{
	ScopePtr scope_ptr(m_spLayout->scope());
	return scope_ptr;
}

fe::sp<fe::Layout>	LayoutPtr::ptr(void) const
{
	return m_spLayout;
}

void LayoutPtr::assign(fe::sp<fe::Layout> &spL)
{
	m_spLayout = spL;
}

std::string LayoutPtr::name(void)
{
	return m_spLayout->name().c_str();
}

PyWatcher::PyWatcher(PyObject *pPyObject)
{
	m_pObject = pPyObject;
	Py_INCREF(m_pObject);
}

PyWatcher::~PyWatcher(void)
{
	Py_DECREF(m_pObject);
}


void PyWatcher::add(const fe::Record &record)
{
	PyObject *pRecord = pyfe::bindRecord(record);
	boost::python::handle<PyObject> h(pRecord);
	call_method<void>(m_pObject, "add", h);
}

void PyWatcher::remove(const fe::Record &record)
{
	PyObject *pRecord = pyfe::bindRecord(record);
	boost::python::handle<PyObject> h(pRecord);
	call_method<void>(m_pObject, "add", h);
}

void PyWatcher::clear(void)
{
	call_method<void>(m_pObject, "clear");
}

WatcherBase::WatcherBase(void)
{
}

WatcherBase::WatcherBase(PyObject *pP)
{
}

WatcherBase::~WatcherBase(void)
{
}

WatcherFS::WatcherFS(PyObject *self_)
	: FunctionSet<fe::WatcherI>(
		fe::sp<fe::Component>(new PyWatcher(self_)))
{
}

WatcherFS::~WatcherFS(void)
{
}

PyObject *WatcherFS::object(void)
{
	return FunctionSet<fe::WatcherI>::object();
}


