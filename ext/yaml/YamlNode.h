/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __yaml_YamlNode
#define __yaml_YamlNode

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief YAML::Node wrapped for sp<>

	@ingroup yaml
*//***************************************************************************/
class FE_DL_EXPORT YamlNode: public Component
{
	public:
		YAML::Node&	node(void)		{ return m_yamlNode; }

	private:
		YAML::Node		m_yamlNode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __yaml_YamlNode */
