/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

using namespace fe;

int main(void)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		feLogDirect("confirm\n");
		Mutex::confirm();

		feLogDirect("dismiss\n");
		Mutex::dismiss();

		feLogDirect("complete\n");

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLogDirect("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}
