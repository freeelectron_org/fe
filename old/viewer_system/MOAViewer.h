/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * MOA compatible Viewer.
 */
class FE_DL_EXPORT MOAViewer :
    public ThreadedViewerI,
    public Initialize<MOAViewer>
{
public:
    MOAViewer() : viewerTask(*this), isOpen(false) {};
    ~MOAViewer();
    void initialize();

	void open(sp<OrchestratorI> orchestrator) override;
	void close() override;

    Real frameRate(void) const override { return 24; }; //temp implementation

private:
    /**
     * Worker object for the internal viewer worker.
     */
    class ViewerTask : public Thread::Functor
    {
    public:
        ViewerTask(MOAViewer& a_parent) : parent(a_parent) {};

        /** Setup the task. Creates everything neccessary. */
        void setup();

        virtual void operate(void) override;

        /** Poison to kill the thread */
        Poison poison;

        /** Used for signalling, scope and dataset. */
        sp<OrchestratorI> orchestrator;

    private:
        /** A reference to the outer component. Required to access the registry. */
        MOAViewer& parent;

        /** Accessors for viewport data. */
        AsViewport			         viewportAccessorSet;
        /** Heartbeat layout for window event signaling. */
        sp<Layout>			 heartbeatLayout;

        sp<WindowI>     window;
        sp<CameraI>     camera;
        sp<CameraControllerI> controller;
        sp<SignalerI>   signaler;
    };

    /** Is the viewer window open? */
    bool isOpen;
    /** The worker object. */
    ViewerTask viewerTask;
    /** The worker thread. */
    Thread *viewerThread;

    /** The window events run on their own scope for now. */
    sp<Scope> windowScope;

    /**
     * Get the default background color.
     *
     * Can be set using the FE_VIEWER_BG_COLOR environment variable.
     */
    Color getDefaultBgColor() const;
};

} /* namespace ext */
} /* namespace fe */
