/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Function.h"

namespace fe
{
namespace ext
{

t_stdstring toLwr(const String &a_str)
{
	t_stdstring s = a_str.c_str();
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

t_eval_real lopass(t_eval_real a_prev, t_eval_real a_in, t_eval_real a_dt, t_eval_real a_c)
{
	t_eval_real a = a_dt / (a_dt + a_c);
	return a_prev + a * (a_in - a_prev);
}

void Multiply::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	m_asVariable->value(a_return) = 1.0;
	for(unsigned int i = 0; i < a_argv.size(); i++)
	{
		m_asVariable->value(a_return) = m_asVariable->value(a_return)
			* m_asVariable->value(a_argv[i]);
	}
}

void Add::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	m_asVariable->value(a_return) = 0.0;
	for(unsigned int i = 0; i < a_argv.size(); i++)
	{
		m_asVariable->value(a_return) = m_asVariable->value(a_return)
			+ m_asVariable->value(a_argv[i]);
	}
}

void Subtract::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	if(a_argv.size() >= 2)
	{
		m_asVariable->value(a_return) = m_asVariable->value(a_argv[0]);
		for(unsigned int i = 1; i < a_argv.size(); i++)
		{
			m_asVariable->value(a_return) = m_asVariable->value(a_return)
				- m_asVariable->value(a_argv[i]);
		}
	}
	else if(a_argv.size() >= 1)
	{
		m_asVariable->value(a_return) = -m_asVariable->value(a_argv[0]);
	}
}

void Divide::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	if(a_argv.size() >= 2)
	{
		m_asVariable->value(a_return) = m_asVariable->value(a_argv[0]);
		for(unsigned int i = 1; i < a_argv.size(); i++)
		{
			m_asVariable->value(a_return) = m_asVariable->value(a_return)
				/ (m_asVariable->value(a_argv[i]) + 1.0e-8);
		}
	}
}

void AbsoluteValue::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	if(a_argv.size() >= 1)
	{
		m_asVariable->value(a_return) = fabs(m_asVariable->value(a_argv[0]));
	}
}

bool Smooth::compile(sp<RecordGroup> a_rg, Record &a_return, t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_aggregate = new Aggregate();
	m_asSmooth = new AsSmooth();
	m_aggregate->bakeTypes(m_asSmooth, a_rg->scope()->declare("AsSmoothLayout"));
	a_return = a_rg->scope()->createRecord(m_aggregate->m_spLayout);

	if(a_argv.size() >= 3)
	{
		if(		m_asSmooth->value.check(a_return)
			&&	m_asSmooth->value.check(a_argv[0])
			&&	m_asSmooth->value.check(a_argv[1])
			&&	m_asSmooth->value.check(a_argv[2]))
		{
			m_valid = true;
		}
		else
		{
			a_msg = "argument type mismatch";
		}
	}
	else
	{
		a_msg = "not enough arguments";
	}

	return m_valid;
}

void Smooth::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	if(m_valid)
	{
		m_smoothed = lopass(m_smoothed, m_asSmooth->value(a_argv[0]), m_asSmooth->value(a_argv[1]), m_asSmooth->value(a_argv[2]));
		m_asSmooth->value(a_return) = m_smoothed;
	}
}

void Smooth::initialize(void)
{
	m_smoothed = 0.0;
	m_valid = false;
}

void Compressor::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	if(a_argv.size() >= 6)
	{
		t_eval_real magnitude = fabs(m_asVariable->value(a_argv[0]));
		t_eval_real sign = 1.0;
		if( m_asVariable->value(a_argv[0]) < 0.0 ) { sign = -1.0; }
		t_eval_real dt = m_asVariable->value(a_argv[1]);
		t_eval_real &threshold = m_asVariable->value(a_argv[2]);
		t_eval_real inv_ratio = 1.0 / (m_asVariable->value(a_argv[3]) + 1.0e-8);
		t_eval_real attack = (1.0 / (m_asVariable->value(a_argv[4]) + 1.0e-8)) * dt;
		t_eval_real release = (1.0 / (m_asVariable->value(a_argv[5]) + 1.0e-8)) * dt;
		t_eval_real exceed = magnitude - threshold;

#if 1
		if(magnitude > threshold)
		{
			t_eval_real target_lvl = threshold + inv_ratio * exceed;
			t_eval_real target_gain = target_lvl / magnitude;
			if(m_gain > target_gain)
			{
				m_gain -= attack * (1.0 - target_gain);
				if(m_gain < target_gain) { m_gain = target_gain; }
			}

		}
		else if(m_gain < 1.0)
		{
			m_gain += release * (1.0 - m_gain);
			if(m_gain > 1.0) { m_gain = 1.0; }
		}
		magnitude = m_gain * magnitude;
		m_asVariable->value(a_return) = m_gain;
#endif

#if 0
		if(magnitude > threshold)
		{
			if(m_gain > inv_ratio)
			{
				m_gain -= attack * (1.0 - inv_ratio);
				if(m_gain < inv_ratio) { m_gain = inv_ratio; }
			}

		}
		else if(m_gain < 1.0)
		{
			m_gain += release * (1.0 - inv_ratio);
			if(m_gain > 1.0) { m_gain = 1.0; }
		}

		if(magnitude > 1.0e-8)
		{
			m_asVariable->value(a_return) = (threshold + m_gain * exceed) / magnitude;
		}
		else
		{
			m_asVariable->value(a_return) = 0.0;
		}
#endif

	}
	else
	{
		m_asVariable->value(a_return) = 0.0;
	}
}


void SoftClipper::eval(Record &a_return, t_stdvector<WeakRecord> &a_argv)
{
	if(a_argv.size() >= 3)
	{
		t_eval_real &soft_clip_input = m_asVariable->value(a_argv[0]);
		t_eval_real &soft_clip_half_in = m_asVariable->value(a_argv[1]);
		if(soft_clip_half_in < 1.0e-8)
		{
			//return soft_clip_input;
		}

		t_eval_real &soft_clip_unity = m_asVariable->value(a_argv[2]);
		t_eval_real soft_headroom = 1.0;
		if(a_argv.size() >= 4)
		{
			soft_headroom = m_asVariable->value(a_argv[3]);
		}
		t_eval_real base = 1.0 - soft_headroom;
		t_eval_real sign = 1.0;
		if(soft_clip_input < 0.0) { sign = -1.0; }

		if(fabs(soft_clip_input) > base)
		{
			t_eval_real abs_overflow = fabs(soft_clip_input) - base;

			t_eval_real scale_overflow = abs_overflow / (abs_overflow + soft_clip_half_in);

			t_eval_real clipScale = 1.0;
			if(soft_clip_unity > 1.0e-8)
			{
				clipScale = (soft_clip_unity + soft_clip_half_in) / soft_clip_unity;
			}

			//return sign * (base + scale_overflow * soft_headroom);
		}
		//return soft_clip_input;
	}
	//return 0.0;
}

} /* namespace ext */
} /* namespace fe */

