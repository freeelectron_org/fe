import os
import stat
import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["operator"]

def setup(module):
    module.summary = []

    opencl_include = os.environ["FE_OPENCL_INCLUDE"]
    opencl_lib = os.environ["FE_OPENCL_LIB"]

    version = ""
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        cl_version_h = forge.vcpkg_include + "/CL/cl_version.h"
    else:
        cl_version_h = opencl_include + "/CL/cl_version.h"
    if os.path.lexists(cl_version_h):
        for line in open(cl_version_h).readlines():
            if re.match("#define CL_TARGET_OPENCL_VERSION.*", line):
                version = line.split()[2]

    if version != "":
        module.summary += [ version ]

    forge.cppmap['opencl'] = '-DFE_USE_OPENCL'
    forge.cppmap['opencl'] += ' -DFE_GL_OPENCL'

    srcList = [ "opencl.pmh",
                "BallCollideCLOp",
                "CacheCLOp",
                "ContractCLOp",
                "ClumpCLOp",
                "CurlCLOp",
                "FlatnessCLOp",
                "KinkCLOp",
                "OpenCLContext",
                "OpenCLOp",
                "PostGuideCLOp",
                "SurfaceAccessibleOpenCL",
                "SurfaceAccessorOpenCL",
                "SurfaceCurvesOpenCL",
                "SurfaceTrianglesOpenCL",
                "SurfaceWrapCLOp",
                "SwayCLOp",
                "TransformCLOp",
                "openclDL" ]

    relativePath = forge.RelativePath(forge.rootPath,module.modPath)
    modObjPath = os.path.join(forge.objPath, relativePath)
    clcPath = modObjPath + "/clc"
    if os.path.isdir(clcPath) == 0:
        os.makedirs(clcPath, 0o755)

    sourceFiles = os.listdir(module.modPath)
    for sourceFile in sourceFiles:
        if re.compile(r'(.*)\.cl$').match(sourceFile):
            clFile = module.modPath + "/" + sourceFile
            clcFile = clcPath + "/" + sourceFile + ".c"

            clModification = os.stat(clFile)[stat.ST_MTIME]
            if os.path.lexists(clcFile):
                clcModification = os.stat(clcFile)[stat.ST_MTIME]
            else:
                clcModification = 0

            if clModification > clcModification:
                command = module.modPath + "/preprocess.py " + clFile + " " + clcFile
                os.system(forge.cleanup_command(command))

    dll = module.DLL( "fexOpenCLDL", srcList )

    module.includemap = {}
    module.includemap['opencl'] = opencl_include
    module.includemap['clc'] = clcPath

    module.cppmap= { 'std': forge.use_std("c++11") }

    dll.linkmap = {}

    deplibs = forge.corelibs + [
                "fexSignalLib",
                "fexGeometryDLLib",
                "fexOperatorDLLib",
                "fexDataToolDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_LINUX':
        dll.linkmap['opencl'] = "-Wl,-rpath='" + opencl_lib + "'"
        dll.linkmap['opencl'] += ' -L' + opencl_lib + ' -lOpenCL'
    elif forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        dll.linkmap['opencl'] = "OpenCL.lib Cfgmgr32.lib"

        deplibs += [    "fexOperateDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib", ]

    forge.deps( ["fexOpenCLDLLib"], deplibs )

def auto(module):
    opencl_include = os.environ["FE_OPENCL_INCLUDE"]

    test_file = """
#include <CL/cl.h>

int main(void)
{
    return(0);
}
    \n"""

    # TODO also configurable (as above)
    forge.includemap['opencl'] = opencl_include

    result = forge.cctest(test_file)

    forge.includemap.pop('opencl', None)

    return result
