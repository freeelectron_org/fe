import sys
import os
import re
import pathlib
import shutil

import utility
forge = sys.modules["forge"]

def prerequisites():
    return [ "opengl", "surface" ]

def setup(module):
    module.summary = []

    # skip Hydra support until we can update it
    hydra_support = False

    usd_include = os.environ["FE_USD_INCLUDE"]
    usd_lib = os.environ["FE_USD_LIB"]
    usd_prefix = os.environ["FE_USD_PREFIX"]
    usd_postfix = os.environ["FE_USD_POSTFIX"]

    usd_imaging_include = os.environ["FE_USD_IMAGING_INCLUDE"]
    usd_imaging_lib = os.environ["FE_USD_IMAGING_LIB"]
    usd_imaging_prefix = os.environ["FE_USD_IMAGING_PREFIX"]
    usd_imaging_postfix = os.environ["FE_USD_IMAGING_POSTFIX"]

    version_paths = [   "imaging/hd",
                        "imaging/hdx",
                        "usdImaging/usdImaging",
                        "usdImaging/usdImagingGL" ]

    if usd_imaging_include == "":
        usd_imaging_include = usd_include

    compound_version = ""
    delim = " "
    for path in version_paths:
        version = "-"
        header = usd_imaging_include + "/pxr/" + path + "/version.h"
        if os.path.exists(header):
            for line in open(header).readlines():
                if re.match("#define .*_API_VERSION", line):
                    version = line.split()[2].rstrip()
        if version != "":
            compound_version += delim + version
        delim = "/"
    if compound_version != "":
        module.summary += [ compound_version ]

    module.cppmap = {}
    module.cppmap['std'] = forge.use_std("c++14")
    module.cppmap["usd"] = "-D BUILD_COMPONENT_SRC_PREFIX=\\\"pxr/\\\""
    module.cppmap["usd"] += " -D BUILD_OPTLEVEL_OPT"
    module.cppmap["cxx11_abi"] = "-D_GLIBCXX_USE_CXX11_ABI=0"

    if forge.compiler_brand == 'clang':
        module.cppmap["stdlib"] = "-stdlib=libstdc++"

    module.includemap = {}

    if forge.fe_os == "FE_LINUX":
        module.includemap["python"] = "/usr/include/python2.7"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        pass
#       module.includemap["python"] = "c:/Program Files/Side Effects Software/Houdini 19.5.303/toolkit/include/python3.9"

#   module.includemap["tbb"] = "/opt/hfs15.0.416/toolkit/include"

#   module.includemap["exr"] = module.modPath + "/OpenEXR/include/OpenEXR"
#   module.includemap["exr"] = os.environ["FE_OPENEXR_SOURCE"] + "/include/OpenEXR"

    module.includemap["usd"] = usd_include
    module.includemap["usd_boost"] = usd_include + "/boost-1_70"

    # for Houdini, if FE_USD_LIB not overridden in local.env
    if not os.path.exists(usd_lib):
        usd_lib = usd_include + "/../../dsolib"

    if hydra_support:
        srcList = [ "hydra.pmh",
                    "DrawHydra",
                    "HydraDelegate",
                    "HydraDriver",
                    "hydraDL" ]

        dll = module.DLL( "fexHydraDL", srcList )
        dll.linkmap = {}
        dll.linkmap["usd"] = "-Wl,-rpath='" + usd_lib + "'"
        dll.linkmap["usd"] += " -L " + usd_lib

        deplibs = forge.corelibs + [
                    "fexOpenGLDLLib",
                    "fexSurfaceDLLib"   ]

        forge.deps( ["fexHydraDLLib"], deplibs )

        if usd_imaging_include != usd_include:
            module.includemap["usd_imaging"] = usd_imaging_include

            usd_imaging_lib = usd_imaging_lib
            dll.linkmap["usd"] += " -Wl,-rpath='" + usd_imaging_lib + "'"
            dll.linkmap["usd"] += " -L " + usd_imaging_lib

        dll.linkmap["usd"] += " -l" + usd_imaging_prefix + "hd" + usd_imaging_postfix
        dll.linkmap["usd"] += " -l" + usd_imaging_prefix + "hdx" + usd_imaging_postfix
        dll.linkmap["gfxlibs"] = forge.gfxlibs

        #HACK for OIIO dependency
    #   dll.linkmap["usd"] += " -lssl"

    #   dll.linkmap["glew"] = "-lGLEW"
    #   dll.linkmap["python"] = "-lpython2.7"

    srcList = [ "usd.pmh",
                "SurfaceAccessibleUsd",
                "SurfaceAccessorUsd",
                "SurfaceAccessorUsdGraph",
                "usdDL" ]

    dll = module.DLL( "fexUsdDL", srcList )
    dll.linkmap = {}

    deplibs = forge.corelibs + [
                "fexSurfaceDLLib"   ]

    use_unreal_usd = int(os.environ["FE_USD_USE_UNREAL"])

    if use_unreal_usd:
        module.summary += [ "unreal" ]

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["usd"] = "-Wl,-rpath='" + usd_lib + "'"
        dll.linkmap["usd"] += " -L " + usd_lib
        dll.linkmap["usd"] += " -l" + usd_prefix + "usd" + usd_postfix
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        dll.linkmap["usd"] = '/LIBPATH:"' + usd_lib + '"'
        dll.linkmap["usd"] += ' "' + usd_lib + '/*.lib"'

        deplibs += [    "fexDataToolLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

        if not use_unreal_usd:
            libPath_usd = forge.libPath+"/usd"
            if not os.path.exists(libPath_usd):
                os.mkdir(libPath_usd)
            utility.copy_all(usd_lib+"/usd",libPath_usd)

            utility.copy_files(usd_lib+"/*.dll",forge.libPath)
            utility.copy_files(usd_lib+"/../bin/tbb.dll",forge.libPath)

    if use_unreal_usd:
        module.cppmap["boost"] = "-D BOOST_ALL_NO_LIB"
        module.cppmap["boost"] += " -D FORCE_ANSI_ALLOCATOR"

        unreal_build = os.environ["FE_UNREAL_BUILD"]
        unreal_third_party = unreal_build + "/Engine/Source/ThirdParty"
        unreal_usd_lib = unreal_build + "/Engine/Plugins/Importers/USDImporter/Source/ThirdParty/USD/lib"

        # find Boost/boost-*
        boost_dirs = [ d for d in pathlib.Path(unreal_third_party + "/Boost").glob("boost-*") ]
        if len(boost_dirs):
            unreal_boost = str(boost_dirs[0])
        else:
            # no good default
            unreal_boost = unreal_third_party + "/Boost/boost"

        module.includemap["usd_boost"] = unreal_boost + "/include"
        unreal_boost_lib = unreal_boost + "/lib/Win64"

        module.includemap["usd_tbb"] = unreal_third_party + "/Intel/TBB/IntelTBB-2019u8/include"
        module.includemap["usd_python"] = unreal_third_party + "/Python3/Win64/include"

        if forge.fe_os == "FE_LINUX":
            # TODO
            pass
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            dll.linkmap["unreal_lib"] = '/LIBPATH:"' + unreal_third_party + '/Python3/Win64/libs"'
            dll.linkmap["unreal_usd"] = '/LIBPATH:"' + unreal_usd_lib + '"'
            dll.linkmap["unreal_usd"] += ' "' + unreal_usd_lib + '/*.lib"'
            dll.linkmap["unreal_boost"] = '/LIBPATH:"' + unreal_boost_lib + '"'
            dll.linkmap["unreal_boost"] += ' "' + unreal_boost_lib + '/*-mt-x64*.lib"'

    forge.deps( ["fexUsdDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexUsdDL",             None,       None) ]

    if not 'ext.opview' in forge.modules_confirmed:
        module.summary += [ "-opview" ]

    if not 'ext.viewer' in forge.modules_confirmed:
        module.summary += [ "-viewer" ]

    # TEMP hydra tests not exiting
#   if 'ext.opview' in forge.modules_confirmed and 'ext.viewer' in forge.modules_confirmed and forge.media:
#       forge.tests += [ ("model_view.bash",    "-count 60 -hydra " + forge.media + "/model/skin.rg",   None,   None) ]
#       if 'ext.grass' in forge.modules_confirmed:
#           forge.tests += [ ("model_view.bash",    "-count 180 -hydra -live " + forge.media + "/lua/opencl.lua",   None,   None) ]

    # TODO find out why Hydra need a threading module loaded first
    if 'ext.tbb' in forge.modules_confirmed:
        forge.tests += [
            ("inspect.exe", "fexTbb fexHydraDL",            None,   None) ]
    elif 'ext.boostthread' in forge.modules_confirmed and 'ext.operator' in forge.modules_confirmed:
        forge.tests += [
            ("inspect.exe", "fexBoostThread fexHydraDL",    None,   None) ]

def auto(module):
    if os.getenv("FE_MS_RT") == "MT":
        return 'TODO Windows MT'

    usd_include = os.environ["FE_USD_INCLUDE"]
    usd_imaging_include = os.environ["FE_USD_IMAGING_INCLUDE"]

    test_file = """
#include "pxr/imaging/hd/version.h"

int main(void)
{
    return(0);
}
    \n"""

    includemap = { "usd": usd_include }

    if usd_imaging_include != usd_include:
        includemap["usd_imaging"] = usd_imaging_include

    linkmap = {}

    return forge.cctest(test_file, linkmap, includemap)
