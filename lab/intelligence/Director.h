/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Director_h__
#define __intelligence_Director_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Manipulate ControlCenter Behaviors

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Director: virtual public DirectorI
{
	public:
						Director(void)										{}

						//* as DirectorI
virtual	bool			addBehavior(Record& rControlRecord,
								String name) const;
virtual	void			moveRecordGroup(sp<RecordGroup> spRecordGroup,
								const SpatialVector displacement) const;
virtual	void			adoptRecordGroup(Record arena,
								sp<RecordGroup> spRecordGroup) const;
virtual	Record			searchRecordGroup(sp<RecordGroup> spRG,
								String particlename);

	private:
		RecordArrayView<Matter>		m_matterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Director_h__ */
