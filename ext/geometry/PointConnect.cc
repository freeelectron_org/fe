/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <geometry/geometry.pmh>

namespace s_hull
{
using namespace std;
#include "s_hull/s_hull.C"
}

namespace fe
{
namespace ext
{

//* static
void PointConnect::solve(Array<Vector2>& a_location,
		Array<Vector3i>& a_triangle)
{
	Array<s_hull::Shx> pointArray;
	s_hull::Shx point;

	I32 id=0;
	const I32 pointCount=a_location.size();

	if(pointCount<3)
	{
		feLog("PointConnect::solve vertex count %d (need at least 3)\n",
				pointCount);
		a_triangle.clear();
		return;
	}

	Array<I32> reindex(pointCount);

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const Vector2& rLocation=a_location[pointIndex];

		//* brute force redundancy filter
		I32 otherIndex;
		for(otherIndex=0;otherIndex<pointIndex;otherIndex++)
		{
			const Vector2& rOther=a_location[otherIndex];
			if(equivalent(rLocation,rOther,fe::tol))
			{
				feLog("PointConnect::solve index %d coincident with %d\n",
						pointIndex,otherIndex);
				break;
			}
		}
		if(otherIndex==pointIndex)
		{
			reindex[id]=pointIndex;
			point.id=id++;
			point.r=rLocation[0];
			point.c=rLocation[1];

			pointArray.push_back(point);
		}
	}

	Array<s_hull::Triad> triads;
	s_hull::s_hull_del_ray2(pointArray,triads);

	const I32 primitiveCount=triads.size();
	a_triangle.resize(primitiveCount);

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const s_hull::Triad& rTriad=triads[primitiveIndex];

		set(a_triangle[primitiveIndex],
				reindex[rTriad.a],reindex[rTriad.b],reindex[rTriad.c]);
	}
}

} /* namespace ext */
} /* namespace fe */