/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

String Profiler::Profile::report(void) const
{
	String result;
#if FE_PROFILER_ON
	result.sPrintf("%-15s %8u %10.0f %10.3f",
			m_name.c_str(),m_count,
			m_us,m_us/F64(m_count? m_count: 1));
#endif
	return result;
}

String Profiler::report(void)
{
	String result;
#if FE_PROFILER_ON
	result.sPrintf("Profile          "
			"  count         us        per\n");

	List< hp<Profile> >::Iterator iterator(
			m_profileList);
	iterator.toHead();
	hp<Profile> node;
	while((node= *iterator++).isValid())
	{
		result+=node->report();
		result+="\n";
	}
#endif
	return result;
}

} /* namespace */


