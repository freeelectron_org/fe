/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define FE_SAH_ATOM_DEBUG	FALSE
#define FE_SAH_GROUP_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceAccessibleHoudini::SurfaceAccessibleHoudini(void):
	m_pGdp(NULL),
	m_pGdpChangeable(NULL),
	m_selfGdp(FALSE)
{
}

SurfaceAccessibleHoudini::~SurfaceAccessibleHoudini(void)
{
	if(m_selfGdp)
	{
		delete m_pGdp;
	}
}

void SurfaceAccessibleHoudini::reset(void)
{
	SurfaceAccessibleBase::reset();

	if(m_pGdpChangeable)
	{
		m_primitiveMap.clear();
		m_pGdpChangeable->clearAndDestroy();
		m_spSurfaceI=NULL;
	}
}

void SurfaceAccessibleHoudini::setGdp(const GU_Detail* a_pGdp)
{
	lock(I64(a_pGdp));

	m_primitiveMap.clear();
	m_pointGroupMap.clear();
	m_primitiveGroupMap.clear();

//	if(m_pGdp!=a_pGdp || m_pGdpChangeable)
	{
		m_pGdp=a_pGdp;
		m_pGdpChangeable=NULL;
		m_spSurfaceI=NULL;
	}

	if(m_spSurfaceI.isValid())
	{
		sp<SurfaceTrianglesHoudini> spSurface=m_spSurfaceI;
		if(spSurface.isValid())
		{
			spSurface->setGdp(a_pGdp);
		}
	}

	unlock(I64(a_pGdp));
}

void SurfaceAccessibleHoudini::setGdpChangeable(GU_Detail* a_pGdp)
{
	lock(I64(a_pGdp));

	m_primitiveMap.clear();
	m_pointGroupMap.clear();
	m_primitiveGroupMap.clear();

//	if(m_pGdp!=a_pGdp || m_pGdpChangeable!=a_pGdp)
	{
		m_pGdp=a_pGdp;
		m_pGdpChangeable=a_pGdp;
		m_spSurfaceI=NULL;
	}

	if(m_spSurfaceI.isValid())
	{
		sp<SurfaceTrianglesHoudini> spSurface=m_spSurfaceI;
		if(spSurface.isValid())
		{
			spSurface->setGdp(a_pGdp);
		}
	}

	unlock(I64(a_pGdp));
}

#if FE_HOUDINI_HARDENING
sp<SurfaceAccessorHoudini::Hardening> SurfaceAccessibleHoudini::hardening(
	String a_key)
{
	//* NOTE already locked from SurfaceAccessibleHoudini::harden()

	hp<SurfaceAccessorHoudini::Hardening>& rhpHardening=m_hardeningMap[a_key];
	if(rhpHardening.isValid())
	{
		return rhpHardening;
	}

	sp<SurfaceAccessorHoudini::Hardening> spHardening(
			new SurfaceAccessorHoudini::Hardening());

	rhpHardening=spHardening;
	return rhpHardening;
}
#endif

void SurfaceAccessibleHoudini::harden(sp<SurfaceAccessorHoudini>& rspAccessor)
{
#if FE_HOUDINI_HARDENING
	if(m_pGdpChangeable && threading()!=SurfaceAccessibleI::e_singleThread)
	{
		lock(I64(rspAccessor.raw()));
		rspAccessor->harden();
		unlock(I64(rspAccessor.raw()));
	}
#endif
}

#ifdef FE_HOUDINI_USE_GA
//* static
GA_AttributeOwner SurfaceAccessibleHoudini::attributeOwner(
	SurfaceAccessibleI::Element a_element)
{
	GA_AttributeOwner owner;
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
		case SurfaceAccessibleI::e_pointGroup:
			owner=GA_ATTRIB_POINT;
			break;
		case SurfaceAccessibleI::e_vertex:
			owner=GA_ATTRIB_VERTEX;
			break;
		case SurfaceAccessibleI::e_primitive:
		case SurfaceAccessibleI::e_primitiveGroup:
			owner=GA_ATTRIB_PRIMITIVE;
			break;
		case SurfaceAccessibleI::e_detail:
			owner=GA_ATTRIB_DETAIL;
			break;
		default:
			owner=GA_AttributeOwner(-1);
	}
	return owner;
}
#else
#endif

void SurfaceAccessibleHoudini::groupNames(Array<String>& a_rNameArray,
	SurfaceAccessibleI::Element a_element) const
{
	a_rNameArray.clear();

#ifdef FE_HOUDINI_USE_GA
	GA_AttributeOwner owner=attributeOwner(a_element);
	if(owner<0)
	{
		feLog("SurfaceAccessibleHoudini::groupNames no owner\n");
		return;
	}

	const GA_ElementGroupTable& groupTable=m_pGdp->getElementGroupTable(owner);

	GA_GroupTable::iterator<GA_ElementGroup> it=groupTable.beginTraverse();
	while(!it.atEnd())
	{
#if FE_HOUDINI_15_PLUS
		const String groupName=it.name().c_str();
#else
		const String groupName=it.name();
#endif

		//* excluding anything starting with '__'
		if(groupName.prechop("__")==groupName)
		{
			a_rNameArray.push_back(groupName);
		}

		++it;
	}
#else
	if(a_element!=SurfaceAccessibleI::e_point &&
			a_element!=SurfaceAccessibleI::e_primitive)
	{
		return;
	}

	const GB_GroupList&	groupList=(a_element==SurfaceAccessibleI::e_point)?
			m_pGdp->pointGroups(): m_pGdp->primitiveGroups();

	GB_Group* pGbGroup=groupList.head();
	while(pGbGroup)
	{
		const String groupName=pGbGroup->getName().buffer();

		//* excluding anything starting with '__'
		if(groupName.prechop("__")==groupName)
		{
			a_rNameArray.push_back(groupName);
		}

		pGbGroup=pGbGroup->next();
	}
#endif
}

void SurfaceAccessibleHoudini::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
/*			owner:
	0		GA_ATTRIB_VERTEX
	1		GA_ATTRIB_POINT
	2		GA_ATTRIB_PRIMITIVE
	3		GA_ATTRIB_GLOBAL
	4		GA_ATTRIB_OWNER_N
	3		GA_ATTRIB_DETAIL=GA_ATTRIB_GLOBAL
*/

/*			storage:
	-1		GA_STORECLASS_INVALID = -1,         // Invalid storage
	0		GA_STORECLASS_INT,                  // Any integer type
	1		GA_STORECLASS_REAL,                 // Any real type
	1		GA_STORECLASS_FLOAT = GA_STORECLASS_REAL,
	2		GA_STORECLASS_STRING,
	3		GA_STORECLASS_OTHER
*/

	a_rSpecs.clear();

	//* NOTE a_node ignored

#ifdef FE_HOUDINI_USE_GA
	GA_AttributeOwner owner=attributeOwner(a_element);
	if(owner<0)
	{
		return;
	}

	if(a_element==SurfaceAccessibleI::e_pointGroup ||
			a_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		const GA_ElementGroupTable& groupTable=
				m_pGdp->getElementGroupTable(owner);

		GA_GroupTable::iterator<GA_ElementGroup> it=groupTable.beginTraverse();
		while(!it.atEnd())
		{
			const String groupName=it.name().c_str();

			//* excluding anything starting with '__'
			if(groupName.prechop("__")==groupName)
			{
				SurfaceAccessibleI::Spec spec;
				spec.set(groupName,"integer");

				a_rSpecs.push_back(spec);
			}

			++it;
		}
		return;
	}

	const GA_AttributeSet& attrSet=m_pGdp->getAttributes();
	for(GA_AttributeDict::iterator it=attrSet.begin(owner,GA_SCOPE_PUBLIC);
			!it.atEnd();++it)
	{
		GA_Attribute* pAttr=it.attrib();
		const GA_AttributeType& attrType=pAttr->getType();

#if FE_HOUDINI_15_PLUS
		const String attrName=pAttr->getName().c_str();
		const String attrTypeName=attrType.getTypeName().c_str();
#else
		const String attrName=pAttr->getName();
		const String attrTypeName=attrType.getTypeName();
#endif

		const GA_StorageClass storage=pAttr->getStorageClass();

		const int size=pAttr->getTupleSize();

//		feLog("  attr \"%s\" type \"%s\" storage %d size %d\n",
//				attrName.c_str(),attrTypeName.c_str(),storage,size);

		String typeName;

		if(attrTypeName=="numeric")
		{
			if(storage==GA_STORECLASS_REAL)
			{
				//* float
				if(size==1)
				{
					typeName="real";
				}
				else if(size==3 || size==4)
				{
					typeName="vector3";
				}
				else
				{
					continue;
				}
			}
			else if(storage==GA_STORECLASS_INT && size==1)
			{
				typeName="integer";
			}
			else
			{
				continue;
			}
		}
		else if(attrTypeName=="string")
		{
			FEASSERT(storage==GA_STORECLASS_STRING);
			typeName="string";
		}
		else
		{
			continue;
		}

//		feLog("  typeName \"%s\"\n",typeName.c_str());

		SurfaceAccessibleI::Spec spec;
		spec.set(attrName,typeName);

		a_rSpecs.push_back(spec);
	}
#else
	const GB_AttributeDict* pDict=NULL;
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			pDict=&m_pGdp->pointAttribs();
			break;
		case SurfaceAccessibleI::e_vertex:
			pDict=&m_pGdp->vertexAttribs();
			break;
		case SurfaceAccessibleI::e_primitive:
			pDict=&m_pGdp->primitiveAttribs();
			break;
		case SurfaceAccessibleI::e_detail:
			pDict=&m_pGdp->attribs();
			break;
		default:
			return;
	}

	GB_Attribute* pAttr=pDict->getHead();
	while(pAttr)
	{
		const String attrName=pAttr->getName();

		const GB_AttribType& attrType=pAttr->getType();

		String typeName;
		switch(attrType)
		{
			case GB_ATTRIB_FLOAT:
				typeName="F32";
				break;
			case GB_ATTRIB_INT:
				typeName="I32";
				break;
			case GB_ATTRIB_MIXED:
			case GB_ATTRIB_INDEX:
			default:
				continue;
		}

		const int size=pAttr->getSize();

		if(typeName=="F32" && (size==3 || size==4))
		{
			typeName="SpatialVector";
		}

		feLog("  attr \"%s\" type \"%s\" size %d\n",
				attrName.c_str(),typeName.c_str(),size);

		SurfaceAccessibleI::Spec spec;
		spec.set(attrName,typeName);

		a_rSpecs.push_back(spec);

		pAttr=(GB_Attribute*)pAttr->next();
	}
#endif
}

sp<SurfaceAccessorI> SurfaceAccessibleHoudini::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	String a_name,SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	if(a_name.contains(" ") || a_name.contains(":"))
	{
		feLog("SurfaceAccessibleHoudini::accessor"
				" illegal attribute name \"%s\"\n",a_name.c_str());
		return sp<SurfaceAccessorI>(NULL);
	}

	sp<SurfaceAccessorHoudini> spAccessor=accessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				(m_pGdpChangeable &&
				a_create==SurfaceAccessibleI::e_createMissing))
		{
			harden(spAccessor);
			return accessorOf(spAccessor);
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleHoudini::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute,
	SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorHoudini> spAccessor=accessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_attribute) ||
				(m_pGdpChangeable &&
				a_create==SurfaceAccessibleI::e_createMissing))
		{
			harden(spAccessor);
			return accessorOf(spAccessor);
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorHoudini> SurfaceAccessibleHoudini::accessor(void)
{
	if(!m_pGdp)
	{
		return sp<SurfaceAccessorI>(NULL);
	}
	sp<SurfaceAccessorHoudini> spAccessorHoudini(new SurfaceAccessorHoudini);
	if(m_pGdpChangeable)
	{
		spAccessorHoudini->setGdpChangeable(m_pGdpChangeable);
	}
	else if(m_pGdp)
	{
		spAccessorHoudini->setGdp(m_pGdp);
	}

	return spAccessorHoudini;
}
sp<SurfaceAccessorI> SurfaceAccessibleHoudini::accessorOf(
	sp<SurfaceAccessorI> a_spAccessor)
{
//#ifdef FE_HOUDINI_USE_GA
#if FALSE
	FEASSERT(a_spAccessor.isValid());
	if(!m_pGdpChangeable || threading()==SurfaceAccessibleI::e_singleThread)
	{
		return a_spAccessor;
	}

	sp<SurfaceAccessorCached> spAccessorCached=
			registry()->create("*.SurfaceAccessorCached");
	FEASSERT(spAccessorCached.isValid());
	spAccessorCached->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
	spAccessorCached->setAccessorChain(a_spAccessor);
	return spAccessorCached;
#else
	return a_spAccessor;
#endif
}

BWORD SurfaceAccessibleHoudini::discard(SurfaceAccessibleI::Element a_element,
	String a_name)
{
	if(!m_pGdpChangeable)
	{
		return FALSE;
	}

#ifdef FE_HOUDINI_USE_GA
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			m_pGdpChangeable->destroyPointAttrib(a_name.c_str());
			break;
		case SurfaceAccessibleI::e_vertex:
			m_pGdpChangeable->destroyVertexAttrib(a_name.c_str());
			break;
		case SurfaceAccessibleI::e_primitive:
			m_pGdpChangeable->destroyPrimAttrib(a_name.c_str());
			break;
		case SurfaceAccessibleI::e_detail:
			m_pGdpChangeable->destroyGlobalAttrib(a_name.c_str());
			break;
		case SurfaceAccessibleI::e_pointGroup:
			return m_pGdpChangeable->destroyPointGroup(a_name.c_str());
		case SurfaceAccessibleI::e_primitiveGroup:
			return m_pGdpChangeable->destroyPrimitiveGroup(a_name.c_str());
		default:
			return FALSE;
	}
#else
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
		{
			GEO_PointAttribDict& attrDict=
					m_pGdpChangeable->pointAttribs();
			GB_Attribute* pAttr=attrDict.find(a_name.c_str());
			if(pAttr)
			{
				GB_AttribType attrType=pAttr->getType();
				int attrSize=pAttr->getSize();
				m_pGdpChangeable->destroyPointAttrib(a_name.c_str(),
						attrSize,attrType);
			}
		}
			break;
		case SurfaceAccessibleI::e_vertex:
		{
			GEO_VertexAttribDict& attrDict=
					m_pGdpChangeable->vertexAttribs();
			GB_Attribute* pAttr=attrDict.find(a_name.c_str());
			if(pAttr)
			{
				GB_AttribType attrType=pAttr->getType();
				int attrSize=pAttr->getSize();
				m_pGdpChangeable->destroyVertexAttrib(a_name.c_str(),
						attrSize,attrType);
			}
		}
			break;
		case SurfaceAccessibleI::e_primitive:
		{
			GEO_PrimAttribDict& attrDict=
					m_pGdpChangeable->primitiveAttribs();
			GB_Attribute* pAttr=attrDict.find(a_name.c_str());
			if(pAttr)
			{
				GB_AttribType attrType=pAttr->getType();
				int attrSize=pAttr->getSize();
				m_pGdpChangeable->destroyPrimAttrib(a_name.c_str(),
						attrSize,attrType);
			}
		}
			break;
		case SurfaceAccessibleI::e_detail:
		{
			GB_AttributeTable& attrDict=
					m_pGdpChangeable->attribs();
			GB_Attribute* pAttr=attrDict.find(a_name.c_str());
			if(pAttr)
			{
				GB_AttribType attrType=pAttr->getType();
				int attrSize=pAttr->getSize();
				m_pGdpChangeable->destroyAttrib(a_name.c_str(),
						attrSize,attrType);
			}
		}
			break;
		default:
			return FALSE;
	}
#endif

	return TRUE;
}

void SurfaceAccessibleHoudini::append(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	const SpatialTransform* a_pTransform)
{
	if(!m_pGdp)
	{
		setGdpChangeable(new GU_Detail());
		m_selfGdp=TRUE;
	}
	if(!m_pGdpChangeable)
	{
		feLog("SurfaceAccessibleHoudini::append not changeable\n");
		return;
	}

	lock(I64(a_spSurfaceAccessibleI.raw()));

//	feLog("SurfaceAccessibleHoudini::append\n");
	sp<SurfaceAccessibleHoudini> spSurfaceOther=a_spSurfaceAccessibleI;
	if(!spSurfaceOther.isValid())
	{
		unlock(I64(a_spSurfaceAccessibleI.raw()));

//		feLog("SurfaceAccessibleHoudini::append"
//				" not SurfaceAccessibleHoudini\n");

		//* cross copy
		SurfaceAccessibleBase::append(a_spSurfaceAccessibleI,a_pTransform);
		return;
	}

#ifdef FE_HOUDINI_USE_GA
	const I32 existingPoints=m_pGdp->getNumPoints();
#else
	const I32 existingPoints=m_pGdp->points().entries();
#endif

	GU_Detail* pOtherGdp=const_cast<GU_Detail*>(spSurfaceOther->gdp());

	if(a_pTransform)
	{
		GU_Detail copyGdp;
		copyGdp.duplicate(*pOtherGdp);

		const SpatialTransform& xform= *a_pTransform;

		UT_Matrix4 utMatrix4(
				xform(0,0),xform(1,0),xform(2,0),0.0,
				xform(0,1),xform(1,1),xform(2,1),0.0,
				xform(0,2),xform(1,2),xform(2,2),0.0,
				xform(0,3),xform(1,3),xform(2,3),1.0);

		copyGdp.transform(utMatrix4);

		if(!existingPoints)
		{
			m_pGdpChangeable->copy(copyGdp);
		}
		else
		{
			m_pGdpChangeable->merge(copyGdp);
		}
	}
	else
	{
		if(!existingPoints)
		{
			m_pGdpChangeable->copy(*pOtherGdp);
		}
		else
		{
			m_pGdpChangeable->merge(*pOtherGdp);
		}
	}

	m_primitiveMap.clear();

	unlock(I64(a_spSurfaceAccessibleI.raw()));
}

void SurfaceAccessibleHoudini::instance(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	const Array<SpatialTransform>& a_rTransformArray)
{
	if(!m_pGdp)
	{
		setGdpChangeable(new GU_Detail());
		m_selfGdp=TRUE;
	}
	if(!m_pGdpChangeable)
	{
		feLog("SurfaceAccessibleHoudini::instance not changeable\n");
		return;
	}

	lock(I64(a_spSurfaceAccessibleI.raw()));

	sp<SurfaceAccessibleHoudini> spSurfaceOther=a_spSurfaceAccessibleI;
	if(!spSurfaceOther.isValid())
	{
		unlock(I64(a_spSurfaceAccessibleI.raw()));

		//* cross copy
		SurfaceAccessibleBase::instance(
				a_spSurfaceAccessibleI,a_rTransformArray);
		return;
	}

#ifdef FE_HOUDINI_USE_GA
	const I32 existingPoints=m_pGdp->getNumPoints();
#else
	const I32 existingPoints=m_pGdp->points().entries();
#endif

	GU_Detail* pOtherGdp=const_cast<GU_Detail*>(spSurfaceOther->gdp());

	GU_Detail copyGdp;
	copyGdp.duplicate(*pOtherGdp);

	const U32 transformCount=a_rTransformArray.size();
	for(U32 transformIndex=0;transformIndex<transformCount;transformIndex++)
	{
		const SpatialTransform& xform=a_rTransformArray[transformIndex];

		UT_Matrix4 utMatrix4(
				xform(0,0),xform(1,0),xform(2,0),0.0,
				xform(0,1),xform(1,1),xform(2,1),0.0,
				xform(0,2),xform(1,2),xform(2,2),0.0,
				xform(0,3),xform(1,3),xform(2,3),1.0);

		copyGdp.transform(utMatrix4);

		if(!transformIndex && !existingPoints)
		{
			m_pGdpChangeable->copy(copyGdp);
		}
		else
		{
			m_pGdpChangeable->merge(copyGdp);
		}

		if(transformIndex<transformCount-1)
		{
			SpatialTransform inv;
			invert(inv,xform);

			UT_Matrix4 utMatrix4Inv(
					inv(0,0),inv(1,0),inv(2,0),0.0,
					inv(0,1),inv(1,1),inv(2,1),0.0,
					inv(0,2),inv(1,2),inv(2,2),0.0,
					inv(0,3),inv(1,3),inv(2,3),1.0);

			copyGdp.transform(utMatrix4Inv);
		}
	}

	m_primitiveMap.clear();

	unlock(I64(a_spSurfaceAccessibleI.raw()));
}

void SurfaceAccessibleHoudini::initPrimitiveMap(void)
{
	if(!m_pGdp)
	{
		return;
	}

	m_hasDiscretePolygons=FALSE;

#ifdef FE_HOUDINI_USE_GA
	const I32 primitiveCount=m_pGdp->getNumPrimitives();
#else
	const I32 primitiveCount=m_pGdp->primitives().entries();
#endif

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
#ifdef FE_HOUDINI_USE_GA
		const GEO_Primitive* pPrimitive=m_pGdp->getGEOPrimitive(
				m_pGdp->primitiveOffset(primitiveIndex));
#else
		const GEO_Primitive* pPrimitive=m_pGdp->primitives()(primitiveIndex);
#endif

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMPOLY)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMPOLY)
#endif
		{
			m_hasDiscretePolygons=TRUE;
			break;
		}
	}

	I32 mapCount=0;
	if(m_hasDiscretePolygons)
	{
		m_primitiveMap[mapCount++]= -1;
	}

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
#ifdef FE_HOUDINI_USE_GA
		const GEO_Primitive* pPrimitive=m_pGdp->getGEOPrimitive(
				m_pGdp->primitiveOffset(primitiveIndex));
#else
		const GEO_Primitive* pPrimitive=m_pGdp->primitives()(primitiveIndex);
#endif

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMPOLY)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMPOLY)
#endif
		{
			continue;
		}

		m_primitiveMap[mapCount++]=primitiveIndex;
	}
}

I32 SurfaceAccessibleHoudini::lookupPrimitive(I32 a_surfaceIndex) const
{
	std::map<I32,I32>::const_iterator it=m_primitiveMap.find(a_surfaceIndex);
	if(it==m_primitiveMap.end())
	{
		return -2;
	}
	return it->second;
}

#ifdef FE_HOUDINI_USE_GA
sp<SpannedRange> SurfaceAccessibleHoudini::atomize(AtomicChange a_atomicChange,
	String a_group,U32 a_desiredCount)
{
#if FE_SAH_ATOM_DEBUG
	feLog("SurfaceAccessibleHoudini::atomize\n");
#endif

#if FE_HOUDINI_HARDENING
	if(!paging())
	{
		return SurfaceAccessibleBase::atomize(a_atomicChange,
				a_group,a_desiredCount);
	}
#endif

	//* TODO use a_desiredCount

	if(a_atomicChange==e_pointsOfPrimitives ||
			a_atomicChange==e_primitivesWithPoints)
	{
//		return atomizeCrossover(a_atomicChange,a_group);

		const BWORD checkPages=TRUE;
		return atomizeConnectivity(a_atomicChange,a_group,
				a_desiredCount,checkPages);
	}

	sp<SpannedRange> spSpannedRange(new SpannedRange());

	const BWORD asPoints=(a_atomicChange==e_pointsOnly);

	const GA_ElementGroup* pGroup=NULL;
	if(!a_group.empty())
	{
		if(asPoints)
		{
			pGroup=m_pGdp->findPointGroup(a_group.c_str());
		}
		else
		{
			pGroup=m_pGdp->findPrimitiveGroup(a_group.c_str());
		}
	}

	GA_Range range=asPoints?
			m_pGdp->getPointRange((const GA_PointGroup*)pGroup):
			m_pGdp->getPrimitiveRange((const GA_PrimitiveGroup*)pGroup);
	GA_SplittableRange splitRange(range);
	for(GA_PageIterator pit=splitRange.beginPages();!pit.atEnd();++pit)
	{
		SpannedRange::MultiSpan& rMultiSpan=spSpannedRange->addAtomic();

		GA_Offset start,end;
		for(GA_Iterator it(pit.begin());it.blockAdvance(start,end); )
		{
			const U32 startIndex=asPoints?
					m_pGdp->pointIndex(start): m_pGdp->primitiveIndex(start);
			const U32 endIndex=asPoints?
					m_pGdp->pointIndex(end-1): m_pGdp->primitiveIndex(end-1);

#if FE_SAH_ATOM_DEBUG
			feLog("createRange span offset %d %d index %d %d\n",
					start,end,startIndex,endIndex);
#endif

			rMultiSpan.add(startIndex,endIndex);
		}
	}

#if FE_SAH_ATOM_DEBUG
	feLog("brief: %s\n",spSpannedRange->brief().c_str());
	feLog("dump:\n%s\n",spSpannedRange->dump().c_str());
#endif

	return spSpannedRange;
}

void SurfaceAccessibleHoudini::preparePaging(AtomicChange a_atomicChange,
	String a_group)
{
#if FE_HOUDINI_HARDENING
	if(!paging())
	{
		SurfaceAccessibleBase::preparePaging(a_atomicChange,a_group);
		return;
	}
#endif

	BWORD asPoints=(a_atomicChange==e_pointsOnly);

	//* POINTS
	const U32 pointCount=m_pGdp->getNumPoints();

	m_pageOfPoint.clear();
	m_pageOfPoint.resize(pointCount,-1);

	const GA_ElementGroup* pGroup=NULL;
	if(asPoints && !a_group.empty())
	{
		pGroup=m_pGdp->findPointGroup(a_group.c_str());
	}

	U32 pageIndex=1;
	GA_Range pointRange=
			m_pGdp->getPointRange((const GA_PointGroup*)pGroup);
	GA_SplittableRange splitPointRange(pointRange);
	for(GA_PageIterator pit=splitPointRange.beginPages();!pit.atEnd();++pit)
	{
		GA_Offset start,end;
		for(GA_Iterator it(pit.begin());it.blockAdvance(start,end); )
		{
			const U32 startIndex=m_pGdp->pointIndex(start);
			const U32 endIndex=m_pGdp->pointIndex(end-1);

			for(U32 m=startIndex;m<=endIndex;m++)
			{
				//* no repeats
				if(m_pageOfPoint[m]>=0)
				{
					feX("surface point paging failed");
				}

				m_pageOfPoint[m]=pageIndex;
			}
		}
		pageIndex++;
	}

	if(!asPoints)
	{
		//* PRIMITIVES
		const U32 primitiveCount=m_pGdp->getNumPrimitives();

		m_pageOfPrimitive.clear();
		m_pageOfPrimitive.resize(primitiveCount,-1);

		pGroup=NULL;
		if(!asPoints && !a_group.empty())
		{
			pGroup=m_pGdp->findPrimitiveGroup(a_group.c_str());
		}

		pageIndex=1;
		GA_Range primitiveRange=
				m_pGdp->getPrimitiveRange((const GA_PrimitiveGroup*)pGroup);
		GA_SplittableRange splitPrimitiveRange(primitiveRange);
		for(GA_PageIterator pit=splitPrimitiveRange.beginPages();
				!pit.atEnd();++pit)
		{
			GA_Offset start,end;
			for(GA_Iterator it(pit.begin());it.blockAdvance(start,end); )
			{
				const U32 startIndex=m_pGdp->primitiveIndex(start);
				const U32 endIndex=m_pGdp->primitiveIndex(end-1);

				for(U32 m=startIndex;m<=endIndex;m++)
				{
					//* no repeats
					if(m_pageOfPrimitive[m]>=0)
					{
						feX("surface primitive paging failed");
					}

					m_pageOfPrimitive[m]=pageIndex;
				}
			}
			pageIndex++;
		}
	}
}

I32 SurfaceAccessibleHoudini::pointPage(U32 a_pointIndex) const
{
#if FE_HOUDINI_HARDENING
	if(!paging())
	{
		return 0;
	}
#endif

	FEASSERT(!m_pageOfPoint.size() ||
			a_pointIndex<m_pageOfPoint.size());

	return a_pointIndex<m_pageOfPoint.size()?
			m_pageOfPoint[a_pointIndex]: 0;
}

I32 SurfaceAccessibleHoudini::primitivePage(U32 a_primitiveIndex) const
{
#if FE_HOUDINI_HARDENING
	if(!paging())
	{
		return 0;
	}
#endif

	FEASSERT(!m_pageOfPrimitive.size() ||
			a_primitiveIndex<m_pageOfPrimitive.size());

	return a_primitiveIndex<m_pageOfPrimitive.size()?
			m_pageOfPrimitive[a_primitiveIndex]: 0;
}

#endif	// FE_HOUDINI_USE_GA

#if FALSE
//* TODO remove deprecated
sp<SpannedRange> SurfaceAccessibleHoudini::atomizeCrossover(
		AtomicChange a_atomicChange,String a_group)
{
	sp<SpannedRange> spSpannedRange(new SpannedRange());

	const GA_PrimitiveGroup* pGroup=a_group.empty()? NULL:
			m_pGdp->findPrimitiveGroup(a_group.c_str());

	const U32 pointCount=m_pGdp->getNumPoints();
	const U32 primitiveCount=
			pGroup? pGroup->entries(): m_pGdp->getNumPrimitives();

	if(a_atomicChange==e_primitivesWithPoints)
	{
		//* TEMP one big atom
		SpannedRange::MultiSpan& rMultiSpan=spSpannedRange->addAtomic();
		rMultiSpan.add(0,primitiveCount);
		return spSpannedRange;
	}

	Array<U32> pointAtom;
	Array<U32> primitiveAtom;

	pointAtom.resize(pointCount);
	primitiveAtom.resize(primitiveCount);

	for(U32 m=0;m<pointCount;m++)
	{
		pointAtom[m]=0;
	}
	for(U32 m=0;m<primitiveCount;m++)
	{
		primitiveAtom[m]=0;
	}

	U32 maxPage=1;

	GA_Range pointRange=m_pGdp->getPointRange();
	GA_SplittableRange splitPointRange(pointRange);
	for(GA_PageIterator pit=splitPointRange.beginPages();!pit.atEnd();++pit)
	{
		GA_Offset start,end;
		for(GA_Iterator it(pit.begin());it.blockAdvance(start,end); )
		{
			const U32 startIndex=m_pGdp->pointIndex(start);
			const U32 endIndex=m_pGdp->pointIndex(end-1);

			for(U32 m=startIndex;m<=endIndex;m++)
			{
				pointAtom[m]=maxPage;
			}
		}
		maxPage++;
	}

	GA_Range primitiveRange=m_pGdp->getPrimitiveRange(pGroup);
	GA_SplittableRange splitPrimitiveRange(primitiveRange);
	for(GA_PageIterator pit=splitPrimitiveRange.beginPages();!pit.atEnd();++pit)
	{
		GA_Offset start,end;
		for(GA_Iterator it(pit.begin());it.blockAdvance(start,end); )
		{
			for(U32 m=start;m<end;m++)
			{
				const U32 primitiveIndex=m_pGdp->primitiveIndex(m);

				const GEO_Primitive* pPrimitive=m_pGdp->getGEOPrimitive(m);

				U32 firstPage=0;

				const U32 subCount=pPrimitive->getVertexCount();
				U32 subIndex;
				for(subIndex=0;subIndex<subCount;subIndex++)
				{
					const GA_Offset offset=
							pPrimitive->getPointOffset(subIndex);
					const U32 pointIndex=m_pGdp->pointIndex(offset);

					const U32 page=pointAtom[pointIndex];

					if(!firstPage)
					{
						firstPage=pointAtom[pointIndex];
					}
					else if(pointAtom[pointIndex]!=firstPage)
					{
						feLog("crossover prim %d point %d page %d vs %d\n",
								primitiveIndex,pointIndex,page,firstPage);

						primitiveAtom[primitiveIndex]=0;
						break;
					}
				}

				if(subIndex==subCount)
				{
					primitiveAtom[primitiveIndex]=firstPage;
				}
			}
		}
	}

	for(U32 page=0;page<maxPage;page++)
	{
		SpannedRange::MultiSpan* pMultiSpan=NULL;
		BWORD spanning=FALSE;
		U32 start;
		for(U32 m=0;m<primitiveCount;m++)
		{
			if(primitiveAtom[m]==page)
			{
				if(!pMultiSpan)
				{
					pMultiSpan=page? &spSpannedRange->addAtomic():
							&spSpannedRange->postAtomic();
				}
				if(!spanning)
				{
					start=m;
					spanning=TRUE;
				}
			}
			else
			{
				if(spanning)
				{
					pMultiSpan->add(start,m-1);
					spanning=FALSE;
				}
			}
		}
		if(spanning)
		{
			pMultiSpan->add(start,primitiveCount-1);
		}
	}

	feLog("brief: %s\n",spSpannedRange->brief().c_str());
	feLog("dump:\n%s\n",spSpannedRange->dump().c_str());

	return spSpannedRange;
}
#endif

//* TODO SurfaceBag
sp<SurfaceI> SurfaceAccessibleHoudini::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
	if(!m_pGdp)
	{
		return sp<SurfaceI>(NULL);
	}

	if(m_spSurfaceI.isValid() && m_spSurfaceI->restrictions()==a_restrictions)
	{
		return m_spSurfaceI;
	}

	String signature;
	signature.sPrintf("%x:%x:%s:%d",
			m_pGdp,m_pGdp->getMetaCacheCount(),a_group.c_str(),a_restrictions);
//	feLog("SurfaceAccessibleHoudini::surface signature \"%s\"\n",
//			signature.c_str());

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	const String keyname="SurfaceAccessibleHoudini:surfaces";

	lock();

	std::map< String,hp<Component> >& rStringHandleMap=
			spMasterCatalog->catalog< std::map< String,hp<Component> > >(
			keyname);
	sp<SurfaceI> spResult=rStringHandleMap[signature];

//	if(spResult.isValid())
//	{
//		feLog("matching cache\n");
//	}

	if(spResult.isNull())
	{
#ifdef FE_HOUDINI_USE_GA
		const I32 entries=m_pGdp->getNumPrimitives();
#else
		const I32 entries=m_pGdp->primitives().entries();
#endif

		if(!entries ||
			((a_restrictions & SurfaceI::e_excludeCurves) &&
			(a_restrictions & SurfaceI::e_excludePolygons)))
		{
#ifdef FE_HOUDINI_USE_GA
			const I32 pointCount=m_pGdp->getNumPoints();
#else
			const I32 pointCount=m_pGdp->points().entries();
#endif
			if(!pointCount)
			{
				unlock();

				feLog("SurfaceAccessibleHoudini::surface"
						" no points or primitives\n");

				return sp<SurfaceI>(NULL);
			}

			//* point cloud
			spResult=registry()->create("SurfaceI.SurfacePointsHoudini");
			if(spResult.isValid())
			{
				sp<SurfacePointsHoudini> spSurface=spResult;
				if(spSurface.isValid())
				{
					spSurface->setGdp(m_pGdp);
					spSurface->setGroup(a_group);
				}
			}
		}
		else
		{
			//* NOTE naive presumption
			if(entries==1)
			{
				unlock();

				//* single primitive (perhaps a sphere)
				return subSurface(0,a_group);
			}

#ifdef FE_HOUDINI_USE_GA
			const GEO_Primitive* pPrimitive=
					m_pGdp->getGEOPrimitive(m_pGdp->primitiveOffset(0));
#else
			const GEO_Primitive* pPrimitive=m_pGdp->primitives()(0);
#endif

			//* Houdini 12.5+
#ifdef FE_HOUDINI_VDB
			if(pPrimitive->getTypeId().get()==GEO_PRIMVDB)
			{
				unlock();

				//* potentially a bunch of VDB grids
				return subSurface(0,a_group);
			}
#endif

			//* HACK presuming they're all the same
			const GEO_Face* pGeoFace=dynamic_cast<const GEO_Face*>(pPrimitive);
			const BWORD openPoly=pGeoFace && !pGeoFace->isClosed();
			if(openPoly)
			{
				if(!(a_restrictions&SurfaceI::e_excludeCurves))
				{
					//* HACK presuming uniform CV counts
//					const U32 subCount=pGeoFace->getVertexCount();

					//* all curves
					spResult=registry()->create(
							"SurfaceI.SurfaceCurvesHoudini");
					if(spResult.isValid())
					{
						sp<SurfaceCurvesHoudini> spSurface=spResult;
						if(spSurface.isValid())
						{
							spSurface->setGdp(m_pGdp);
							spSurface->setGroup(a_group);
						}
					}
				}
			}
			else if(a_restrictions&SurfaceI::e_excludePolygons)
			{
				//* not curves and polygons not permitted, so points
				spResult=registry()->create("SurfaceI.SurfacePointsHoudini");
				if(spResult.isValid())
				{
					sp<SurfacePointsHoudini> spSurface=spResult;
					if(spSurface.isValid())
					{
						spSurface->setGdp(m_pGdp);
						spSurface->setGroup(a_group);
					}
				}
			}
			else
			{
				//* all triangles (presumed)
				spResult=registry()->create("SurfaceI.SurfaceTrianglesHoudini");
				if(spResult.isValid())
				{
					sp<SurfaceTrianglesHoudini> spSurface=spResult;
					if(spSurface.isValid())
					{
						spSurface->setGdp(m_pGdp);
						spSurface->setGroup(a_group);
					}
				}
			}
		}

		rStringHandleMap[signature]=spResult;
	}

	if(spResult.isValid())
	{
		spResult->setRestrictions(a_restrictions);
	}

	unlock();

	//* don't cache if it can change
	if(!m_pGdpChangeable)
	{
		m_spSurfaceI=spResult;
	}

	return spResult;
}

sp<SurfaceI> SurfaceAccessibleHoudini::subSurface(U32 a_subIndex,
	String a_group,SurfaceI::Restrictions a_restrictions)
{
//	feLog("SurfaceAccessibleHoudini::subSurface %d \"%s\"\n",
//			a_subIndex,a_group.c_str());

	if(!m_pGdp)
	{
		return sp<SurfaceI>(NULL);
	}

	lock();

	if(m_primitiveMap.empty())
	{
		initPrimitiveMap();
	}

	const I32 lookupIndex=lookupPrimitive(a_subIndex);
	if(lookupIndex< -1)
	{
		unlock();

		return sp<SurfaceI>(NULL);
	}

#ifdef FE_HOUDINI_USE_GA
	const I32 primitiveCount=m_pGdp->getNumPrimitives();
#else
	const I32 primitiveCount=m_pGdp->primitives().entries();
#endif

	const I32 primitiveIndex=(lookupIndex>=0)? lookupIndex: a_subIndex;
	if(primitiveIndex>=primitiveCount)
	{
		unlock();

		return sp<SurfaceI>(NULL);
	}


#ifdef FE_HOUDINI_USE_GA
	const GEO_Primitive* pPrimitive=m_pGdp->getGEOPrimitive(
			m_pGdp->primitiveOffset(primitiveIndex));
#else
	const GEO_Primitive* pPrimitive=m_pGdp->primitives()(primitiveIndex);
#endif

	if(!pPrimitive)
	{
		unlock();

		return sp<SurfaceI>(NULL);
	}

	if(lookupIndex>=0)
	{

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMSPHERE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMSPHERE)
#endif
		{
			sp<SurfaceSphere> spSphere=
					registry()->create("SurfaceI.SurfaceSphere");
			if(!spSphere.isValid())
			{
				unlock();

				return sp<SurfaceI>(NULL);
			}

			const GEO_PrimSphere* pPrimSphere=
					dynamic_cast<const GEO_PrimSphere*>(pPrimitive);

			UT_Matrix4 utMatrix4;
			pPrimSphere->getTransform4(utMatrix4);
			SpatialTransform matrix;
			set(matrix,utMatrix4.data());

//			fpointOffseteLog("SurfaceAccessibleHoudini::subSurface(%d) prim %d"
//					" sphere matrix\n%s\n",
//					a_subIndex,lookupIndex,c_print(matrix));

			spSphere->setCenter(matrix.translation());
			spSphere->setRadius(matrix(0,0));

			unlock();

			return spSphere;
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMCIRCLE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMCIRCLE)
#endif
		{
			sp<SurfaceDisk> spDisk=
					registry()->create("SurfaceI.SurfaceDisk");
			if(!spDisk.isValid())
			{
				unlock();

				return sp<SurfaceI>(NULL);
			}

			const GEO_PrimCircle* pPrimCircle=
					dynamic_cast<const GEO_PrimCircle*>(pPrimitive);

			UT_Matrix4 utMatrix4;
			pPrimCircle->getTransform4(utMatrix4);
			SpatialTransform matrix;
			set(matrix,utMatrix4.data());

			UT_Vector3 utVector3=pPrimCircle->computeNormal();

//			feLog("SurfaceAccessibleHoudini::subSurface(%d) prim %d"
//					" disk matrix\n%s\n",
//					a_subIndex,lookupIndex,c_print(matrix));

			spDisk->setCenter(matrix.translation());
			spDisk->setRadius(matrix(0,0));
			spDisk->setSpan(
					SpatialVector(utVector3[0],utVector3[1],utVector3[2]));

			unlock();

			return spDisk;
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMTUBE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMTUBE)
#endif
		{
			sp<SurfaceCylinder> spCylinder=
					registry()->create("SurfaceI.SurfaceCylinder");
			if(!spCylinder.isValid())
			{
				unlock();

				return sp<SurfaceI>(NULL);
			}

			const GEO_PrimTube* pPrimTube=
					dynamic_cast<const GEO_PrimTube*>(pPrimitive);

			UT_Matrix4 utMatrix4;
			pPrimTube->getTransform4(utMatrix4);
			SpatialTransform matrix;
			set(matrix,utMatrix4.data());

			const Real taper=pPrimTube->getTaper();

//			feLog("SurfaceAccessibleHoudini::subSurface(%d) prim %d"
//					" cylinder taper %.6G matrix\n%s\n",
//					a_subIndex,lookupIndex,taper,c_print(matrix));

			const Real scaleX=magnitude(matrix.column(0)); // same as scaleZ

//			feLog("scale %.6G %.6G %.6G\n",scaleX,scaleY,scaleZ);

			const SpatialVector span=matrix.column(1);	//* already scaled

			spCylinder->setLocation(matrix.translation()-0.5*span);
			spCylinder->setSpan(span);
			spCylinder->setBaseRadius(scaleX);
			spCylinder->setEndRadius(scaleX*taper);

			unlock();

			return spCylinder;
		}

		//* Houdini 12.5+
#ifdef FE_HOUDINI_VDB
		if(pPrimitive->getTypeId().get()==GEO_PRIMVDB)
		{
//			feLog("SurfaceAccessibleHoudini::subSurface(%d) VDB\n",a_subIndex);

			sp<SurfaceI> spVDB=registry()->create("SurfaceI.SurfaceVDB");
			if(!spVDB.isValid())
			{
				unlock();

				return sp<SurfaceI>(NULL);
			}

			U32 vdbIndex=a_subIndex;
			const GEO_PrimVDB* pPrimVDB=
					dynamic_cast<const GEO_PrimVDB*>(pPrimitive);

			while(pPrimVDB)
			{
				openvdb::GridBase::ConstPtr gridPtr=pPrimVDB->getConstGridPtr();

//				feLog("SurfaceAccessibleHoudini::subSurface VDB"
//						" \"%s\" type \"%s\" valueType \"%s\"\n",
//						gridPtr->getName().c_str(),
//						gridPtr->type().c_str(),
//						gridPtr->valueType().c_str());

				//* TODO better
				void* pPtr=&gridPtr;

				sp<TypeMaster> spTypeMaster=registry()->master()->typeMaster();
				Instance instance;
				instance.set(spTypeMaster,pPtr);
				spVDB->bind(instance);

				if(++vdbIndex>=primitiveCount)
				{
					break;
				}

				pPrimVDB=dynamic_cast<const GEO_PrimVDB*>(
						m_pGdp->getGEOPrimitive(
						m_pGdp->primitiveOffset(vdbIndex)));
			}

			unlock();

			return spVDB;
		}
#endif
	}

	//* HACK presuming they're all the same
	const GEO_Face* pGeoFace=dynamic_cast<const GEO_Face*>(pPrimitive);
	const BWORD openPoly=pGeoFace && !pGeoFace->isClosed();
	if(openPoly)
	{
		//* HACK presuming uniform CV counts
//		const U32 subCount=pGeoFace->getVertexCount();

		sp<SurfaceCurvesHoudini> spCurves=
				registry()->create("SurfaceI.SurfaceCurvesHoudini");
		if(!spCurves.isValid())
		{
			unlock();

			return sp<SurfaceI>(NULL);
		}

		spCurves->setGdp(m_pGdp);
		spCurves->setGroup(a_group);
		if(lookupIndex>=0)
		{
			spCurves->setSubIndex(a_subIndex);
		}

		unlock();

		return spCurves;
	}

	sp<SurfaceTrianglesHoudini> spTriangles=
			registry()->create("SurfaceI.SurfaceTrianglesHoudini");
	if(!spTriangles.isValid())
	{
		unlock();

		return sp<SurfaceI>(NULL);
	}

	spTriangles->setGdp(m_pGdp);
	spTriangles->setGroup(a_group);
	if(lookupIndex>=0)
	{
		spTriangles->setSubIndex(a_subIndex);
	}

	unlock();

	return spTriangles;
}

void SurfaceAccessibleHoudini::MultiGroupHoudini::add(I32 a_index)
{
	if(!m_pSolitaryGroup)
	{
		feLog("SurfaceAccessibleHoudini::MultiGroupHoudini::add"
				" can not change plural compound group\n");
		return;
	}

	m_pSolitaryGroup->addIndex(a_index);

	SurfaceAccessibleBase::MultiGroup::add(a_index);
}

void SurfaceAccessibleHoudini::MultiGroupHoudini::remove(I32 a_index)
{
	if(!m_pSolitaryGroup)
	{
		feLog("SurfaceAccessibleHoudini::MultiGroupHoudini::remove"
				" can not change plural compound group\n");
		return;
	}

	m_pSolitaryGroup->removeIndex(a_index);

	SurfaceAccessibleBase::MultiGroup::remove(a_index);
}

sp<SurfaceAccessibleBase::MultiGroup> SurfaceAccessibleHoudini::generateGroup(
	SurfaceAccessibleI::Element a_element,String a_groupString)
{
#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessibleHoudini::generateGroup %s \"%s\"\n",
			elementLayout(a_element).c_str(),a_groupString.c_str());
#endif

	sp<MultiGroupHoudini> spMultiGroupHoudini=
			sp<MultiGroupHoudini>(new MultiGroupHoudini());

	const String depletedString=
			addGroupRanges(spMultiGroupHoudini,a_groupString);
	const BWORD depleted=(depletedString!=a_groupString);

	Array<String> patternArray;
	String buffer=depletedString;
	while(!buffer.empty())
	{
		patternArray.push_back(buffer.parse());
	}
	const U32 patternCount=patternArray.size();

	Array<String> nameArray;
	groupNames(nameArray,a_element);

	U32 groupCount(0);
	GA_ElementGroup* pElementGroup(NULL);

	const U32 nameCount=nameArray.size();
	for(U32 nameIndex=0;nameIndex<nameCount;nameIndex++)
	{
		const String groupName=nameArray[nameIndex];

#if FE_SAH_GROUP_DEBUG
		feLog("  groupName %d/%d \"%s\"\n",
				nameIndex,nameCount,groupName.c_str());
#endif

		for(U32 patternIndex=0;patternIndex<patternCount;patternIndex++)
		{
			const String pattern=patternArray[patternIndex];

#if FE_SAH_GROUP_DEBUG
			feLog("  pattern %d/%d \"%s\"\n",
					patternIndex,patternCount,pattern.c_str());
#endif

			if(!groupName.match(pattern))
			{
				continue;
			}

#if FE_SAH_GROUP_DEBUG
			feLog("  MATCH\n");
#endif

			GA_Iterator* pIterator(NULL);

			switch(a_element)
			{
				case SurfaceAccessibleI::e_pointGroup:
				{
					GA_Range range;
					GA_PointGroup* pPointGroup(NULL);
					if(m_pGdpChangeable)
					{
						pPointGroup=m_pGdpChangeable->findPointGroup(
								groupName.c_str());
						pElementGroup=pPointGroup;
						range=m_pGdpChangeable->getPointRange(pPointGroup);
					}
					else
					{
						const GA_PointGroup *pPointGroup=
								m_pGdp->findPointGroup(groupName.c_str());
						range=m_pGdp->getPointRange(pPointGroup);
					}
					pIterator=new GA_Iterator(range);
				}
					break;

				case SurfaceAccessibleI::e_primitiveGroup:
				{
					GA_Range range;
					GA_PrimitiveGroup* pPrimitiveGroup(NULL);
					if(m_pGdpChangeable)
					{
						pPrimitiveGroup=m_pGdpChangeable->findPrimitiveGroup(
								groupName.c_str());
						pElementGroup=pPrimitiveGroup;
						range=m_pGdpChangeable->getPrimitiveRange(
								pPrimitiveGroup);
					}
					else
					{
						pPrimitiveGroup=const_cast<GA_PrimitiveGroup*>(
								m_pGdp->findPrimitiveGroup(groupName.c_str()));
						range=m_pGdp->getPrimitiveRange(pPrimitiveGroup);
					}
					pIterator=new GA_Iterator(range);
				}
					break;

				default:
					;
			}

			if(pIterator)
			{
				for(;!pIterator->atEnd();pIterator->advance())
				{
					const I32 index=pIterator->getIndex();
//					feLog("index %d\n",index);

					spMultiGroupHoudini->insert(index);
				}
				groupCount++;

				delete pIterator;
			}
		}
	}

//	feLog("groupCount %d pElementGroup %p\n",groupCount,pElementGroup);

	if(m_pGdpChangeable && patternCount==1 && !groupCount && !depleted)
	{
		if(a_element==SurfaceAccessibleI::e_pointGroup)
		{
			pElementGroup=
					m_pGdpChangeable->newPointGroup(a_groupString.c_str());
#if FE_SAH_GROUP_DEBUG
			feLog("  new point group \"%s\" %p\n",
					a_groupString.c_str(),pElementGroup);
#endif
		}
		else
		{
			pElementGroup=
					m_pGdpChangeable->newPrimitiveGroup(a_groupString.c_str());
#if FE_SAH_GROUP_DEBUG
			feLog("  new primitive group \"%s\" %p\n",
					a_groupString.c_str(),pElementGroup);
#endif
		}
		groupCount=1;
	}

	if(groupCount==1)
	{
#if FE_SAH_GROUP_DEBUG
		feLog("  solitary group %p\n",pElementGroup);
#endif
		spMultiGroupHoudini->setSolitaryGroup(pElementGroup);
	}

	return spMultiGroupHoudini;
}

} /* namespace ext */
} /* namespace fe */
