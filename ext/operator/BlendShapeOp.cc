/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_BSO_DEBUG	FALSE

namespace fe
{
namespace ext
{

void BlendShapeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("envelope")=1.0;
	catalog<String>("envelope","label")="Envelope";
	catalog<String>("envelope","hint")="Linearly adjust all effects.";

	catalog<String>("pointGroups")="";
	catalog<String>("pointGroups","label")="Point Groups";
	catalog<String>("pointGroups","suggest")="pointGroups";
	catalog<bool>("pointGroups","joined")=true;
	catalog<String>("pointGroups","hint")=
		"Space delimited list of point groups on first input to blend.";

	catalog<bool>("pointRegex")=false;
	catalog<String>("pointRegex","label")="Regex";
	catalog<String>("pointRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<bool>("differencing")=true;
	catalog<String>("differencing","label")="Differencing";
	catalog<String>("differencing","hint")=
			"Instead of a directly weighted contributions of all meshes,"
			" add weighted deltas of the additional meshes on top of"
			" the first mesh.";

	catalog<Real>("inputWeight")=1.0;
	catalog<String>("inputWeight","label")="Input Weight";
	catalog<String>("inputWeight","enabler")="differencing==0";
	catalog<String>("inputWeight","hint")="Contribution of original surface.";

	catalog<bool>("weighted")=false;
	catalog<String>("weighted","label")="Per Point";
	catalog<bool>("weighted","joined")=true;
	catalog<String>("weighted","hint")=
			"Use point attribute to resist change."
			"  When differencing, this first attribute scales all effects.";

	catalog<String>("weightAttr")="weight";
	catalog<String>("weightAttr","label")="Attribute";
	catalog<String>("weightAttr","enabler")="weighted";
	catalog<String>("weightAttr","hint")=
			"Name of real point attribute describing participation in change"
			" (1.0 is full effect).";

	catalog<Real>("secondWeight")=1.0;
	catalog<String>("secondWeight","label")="Second Weight";
	catalog<String>("secondWeight","hint")="Contribution of second input.";

	catalog<bool>("secondWeighted")=false;
	catalog<String>("secondWeighted","label")="Per Point";
	catalog<bool>("secondWeighted","joined")=true;
	catalog<String>("secondWeighted","hint")=
			"Use point attribute to resist change."
			"  When differencing,"
			" this can be scaled by the first input's weight attribute.";

	catalog<String>("secondWeightAttr")="weight";
	catalog<String>("secondWeightAttr","label")="Attribute";
	catalog<String>("secondWeightAttr","enabler")="secondWeighted";
	catalog<String>("secondWeightAttr","hint")=
			catalog<String>("weightAttr","hint");

	catalog<Real>("thirdWeight")=1.0;
	catalog<String>("thirdWeight","label")="Third Weight";
	catalog<String>("thirdWeight","hint")="Contribution of third input.";

	catalog<bool>("thirdWeighted")=false;
	catalog<String>("thirdWeighted","label")="Per Point";
	catalog<bool>("thirdWeighted","joined")=true;
	catalog<String>("thirdWeighted","hint")=
			catalog<String>("secondWeighted","hint");

	catalog<String>("thirdWeightAttr")="weight";
	catalog<String>("thirdWeightAttr","label")="Attribute";
	catalog<String>("thirdWeightAttr","enabler")="thirdWeighted";
	catalog<String>("thirdWeightAttr","hint")=
			catalog<String>("weightAttr","hint");

	catalog<Real>("fourthWeight")=1.0;
	catalog<String>("fourthWeight","label")="Fourth Weight";
	catalog<String>("fourthWeight","hint")="Contribution of fourth input.";

	catalog<bool>("fourthWeighted")=false;
	catalog<String>("fourthWeighted","label")="Per Point";
	catalog<bool>("fourthWeighted","joined")=true;
	catalog<String>("fourthWeighted","hint")=
			catalog<String>("secondWeighted","hint");

	catalog<String>("fourthWeightAttr")="weight";
	catalog<String>("fourthWeightAttr","label")="Attribute";
	catalog<String>("fourthWeightAttr","enabler")="fourthWeighted";
	catalog<String>("fourthWeightAttr","hint")=
			catalog<String>("weightAttr","hint");

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Second Surface");
	catalog<bool>("Second Surface","optional")=true;

	catalog< sp<Component> >("Third Surface");
	catalog<bool>("Third Surface","optional")=true;

	catalog< sp<Component> >("Fourth Surface");
	catalog<bool>("Fourth Surface","optional")=true;
}

void BlendShapeOp::handle(Record& a_rSignal)
{
#if FE_BSO_DEBUG
	feLog("BlendShapeOp::handle\n");
#endif

	catalog<String>("summary")="OFF";

	const Real envelope=catalog<Real>("envelope");
	if(envelope<=0.0)
	{
		return;
	}

	const BWORD differencing=catalog<bool>("differencing");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spSurfacePoint[4];
	sp<SurfaceAccessorI> spSurfaceWeight[4];
	if(!access(spSurfacePoint[0],spOutputAccessible,e_point,e_position)) return;

	const U32 pointCount=spSurfacePoint[0]->count();

	const String weightAttr=catalog<String>("weightAttr");
	if(catalog<bool>("weighted") && !weightAttr.empty())
	{
		access(spSurfaceWeight[0],spOutputAccessible,
				e_point,weightAttr,e_warning,e_refuseMissing);
	}

	sp<SurfaceAccessibleI> spFirstAccessible;
	if(!access(spFirstAccessible,"Input Surface"))
	{
		return;
	}

	sp<SurfaceAccessorI> spOriginalPoint;
	if(!access(spOriginalPoint,spFirstAccessible,e_point,e_position))
	{
		return;
	}

	sp<SurfaceAccessibleI> spSecondAccessible;
	if(access(spSecondAccessible,"Second Surface",e_quiet))
	{
		access(spSurfacePoint[1],spSecondAccessible,
				e_point,e_position,e_quiet);

		const String secondWeightAttr=catalog<String>("secondWeightAttr");
		if(catalog<bool>("secondWeighted") && !secondWeightAttr.empty())
		{
			access(spSurfaceWeight[1],spSecondAccessible,
					e_point,secondWeightAttr,e_warning);
		}

		if(spSurfacePoint[1].isValid() &&
				spSurfacePoint[1]->count()!=pointCount)
		{
			String warning;
			warning.sPrintf("second input surface has %d/%d points;",
					spSurfacePoint[1]->count(),pointCount);
			catalog<String>("warning")+=warning;
		}
	}

	sp<SurfaceAccessibleI> spThirdAccessible;
	if(access(spThirdAccessible,"Third Surface",e_quiet))
	{
		access(spSurfacePoint[2],spThirdAccessible,
				e_point,e_position,e_quiet);

		const String thirdWeightAttr=catalog<String>("thirdWeightAttr");
		if(catalog<bool>("thirdWeighted") && !thirdWeightAttr.empty())
		{
			access(spSurfaceWeight[2],spThirdAccessible,
					e_point,thirdWeightAttr,e_warning);
		}

		if(spSurfacePoint[2].isValid() &&
				spSurfacePoint[2]->count()!=pointCount)
		{
			String warning;
			warning.sPrintf("third input surface has %d/%d points;",
					spSurfacePoint[2]->count(),pointCount);
			catalog<String>("warning")+=warning;
		}
	}

	sp<SurfaceAccessibleI> spFourthAccessible;
	if(access(spFourthAccessible,"Fourth Surface",e_quiet))
	{
		access(spSurfacePoint[3],spFourthAccessible,
				e_point,e_position,e_quiet);

		const String fourthWeightAttr=catalog<String>("fourthWeightAttr");
		if(catalog<bool>("fourthWeighted") && !fourthWeightAttr.empty())
		{
			access(spSurfaceWeight[3],spFourthAccessible,
					e_point,fourthWeightAttr,e_warning);
		}

		if(spSurfacePoint[3].isValid() &&
				spSurfacePoint[3]->count()!=pointCount)
		{
			String warning;
			warning.sPrintf("fourth input surface has %d/%d points;",
					spSurfacePoint[3]->count(),pointCount);
			catalog<String>("warning")+=warning;
		}
	}

	String pointGroups=catalog<String>("pointGroups");
	if(!pointGroups.empty() && !catalog<bool>("pointRegex"))
	{
		pointGroups=pointGroups.convertGlobToRegex();
	}

	const BWORD pointGrouped=(!pointGroups.empty());

	sp<SurfaceAccessorI> spPointGroup;
	if(pointGrouped)
	{
		if(!access(spPointGroup,spFirstAccessible,
				e_pointGroup,pointGroups)) return;
	}

	const U32 count=pointGrouped? spPointGroup->count(): pointCount;

	const Real inputWeight=catalog<Real>("inputWeight");
	const Real secondWeight=catalog<Real>("secondWeight");
	const Real thirdWeight=catalog<Real>("thirdWeight");
	const Real fourthWeight=catalog<Real>("fourthWeight");

	sp<SurfaceAccessorI> spOutputPoint=spSurfacePoint[0];
	sp<SurfaceAccessorI> spOutputWeight=spSurfaceWeight[0];

	if(differencing)
	{
		Real weight[4];
		weight[0]=1.0;
		weight[1]=envelope*secondWeight;
		weight[2]=envelope*thirdWeight;
		weight[3]=envelope*fourthWeight;

		for(I32 inputIndex=1;inputIndex<4;inputIndex++)
		{
			sp<SurfaceAccessorI> spOtherPoint=spSurfacePoint[inputIndex];
			if(spOtherPoint.isNull())
			{
				continue;
			}
			sp<SurfaceAccessorI> spOtherWeight=spSurfaceWeight[inputIndex];

			const U32 otherCount=spOtherPoint->count();

			const U32 minCount=fe::minimum(count,otherCount);

			if(spOutputWeight.isNull() && spOtherWeight.isNull())
			{
				for(U32 index=0;index<minCount;index++)
				{
					if(interrupted())
					{
						break;
					}

					const I32 pointIndex=
							pointGrouped? spPointGroup->integer(index): index;

					const SpatialVector point=
							spOutputPoint->spatialVector(pointIndex)+
							weight[inputIndex]*
							(spOtherPoint->spatialVector(index)-
							spOriginalPoint->spatialVector(pointIndex));

					spOutputPoint->set(pointIndex,point);
				}
			}
			else
			{
				for(U32 index=0;index<minCount;index++)
				{
					if(interrupted())
					{
						break;
					}

					const I32 pointIndex=
							pointGrouped? spPointGroup->integer(index): index;

					const Real outputWeight=spOutputWeight.isValid()?
							spOutputWeight->real(pointIndex): Real(1);

					const Real otherWeight=spOtherWeight.isValid()?
							spOtherWeight->real(index): Real(1);

					const SpatialVector point=
							spOutputPoint->spatialVector(pointIndex)+
							outputWeight*otherWeight*weight[inputIndex]*
							(spOtherPoint->spatialVector(index)-
							spOriginalPoint->spatialVector(pointIndex));

					spOutputPoint->set(pointIndex,point);
				}
			}
		}
	}
	else
	{
		if(spOutputWeight.isNull() && spSurfaceWeight[1].isNull() &&
				spSurfaceWeight[2].isNull() && spSurfaceWeight[3].isNull())
		{
			const Real weightSum=inputWeight+
					(spSurfacePoint[1].isValid()? secondWeight: Real(0))+
					(spSurfacePoint[2].isValid()? thirdWeight: Real(0))+
					(spSurfacePoint[3].isValid()? fourthWeight: Real(0));

			Real weight[4];
			weight[0]=envelope*inputWeight/weightSum+(Real(1)-envelope);
			weight[1]=envelope*secondWeight/weightSum;
			weight[2]=envelope*thirdWeight/weightSum;
			weight[3]=envelope*fourthWeight/weightSum;

			for(U32 index=0;index<count;index++)
			{
				if(interrupted())
				{
					break;
				}

				const I32 pointIndex=
						pointGrouped? spPointGroup->integer(index): index;

				const SpatialVector point=
						weight[0]*spOutputPoint->spatialVector(pointIndex);

				spOutputPoint->set(pointIndex,point);
			}

			for(I32 inputIndex=1;inputIndex<4;inputIndex++)
			{
				sp<SurfaceAccessorI> spOtherPoint=spSurfacePoint[inputIndex];
				if(spOtherPoint.isNull())
				{
					continue;
				}

				const U32 otherCount=spOtherPoint->count();

				const U32 minCount=fe::minimum(pointCount,otherCount);
				U32 index=0;
				for(;index<minCount;index++)
				{
					if(interrupted())
					{
						break;
					}

					const I32 pointIndex=
							pointGrouped? spPointGroup->integer(index): index;

					const SpatialVector point=
							spOutputPoint->spatialVector(pointIndex)+
							weight[inputIndex]*
							spOtherPoint->spatialVector(index);

					spOutputPoint->set(pointIndex,point);
				}
				for(;index<count;index++)
				{
					if(interrupted())
					{
						break;
					}

					const I32 pointIndex=
							pointGrouped? spPointGroup->integer(index): index;

					const SpatialVector point=
							spOutputPoint->spatialVector(pointIndex)+
							weight[inputIndex]*
							spOriginalPoint->spatialVector(pointIndex);

					spOutputPoint->set(pointIndex,point);
				}
			}
		}
		else
		{
			Real sliderWeight[4];
			sliderWeight[0]=envelope*inputWeight;
			sliderWeight[1]=envelope*secondWeight;
			sliderWeight[2]=envelope*thirdWeight;
			sliderWeight[3]=envelope*fourthWeight;

			Real inputCount[4];
			for(I32 m=0;m<4;m++)
			{
				inputCount[m]=spSurfacePoint[m].isValid()?
						spSurfacePoint[m]->count(): 0;
			}

			for(U32 index=0;index<count;index++)
			{
				if(interrupted())
				{
					break;
				}

				const I32 pointIndex=
						pointGrouped? spPointGroup->integer(index): index;

				Real weightSum(0);
				Real pointWeight[4];
				for(I32 m=0;m<4;m++)
				{
					const I32 reIndex=m? index: pointIndex;

					pointWeight[m]=sliderWeight[m]*
							(reIndex<inputCount[m]?
							(spSurfaceWeight[m].isValid()?
							(m? spSurfaceWeight[m]->real(reIndex):
							spSurfaceWeight[m]->real(reIndex)):
							Real(1)): Real(0));
					weightSum+=pointWeight[m];
				}

				if(weightSum<=Real(0))
				{
					continue;
				}

				SpatialVector point(0,0,0);

				for(I32 m=0;m<4;m++)
				{
					const I32 reIndex=m? index: pointIndex;
					if(reIndex<inputCount[m])
					{
						point+=spSurfacePoint[m]->spatialVector(reIndex)*
								pointWeight[m];
					}
				}

				point*=Real(1)/weightSum;

				spOutputPoint->set(pointIndex,point);
			}
		}
	}

	String summary;
	summary.sPrintf("%d points",pointCount);
	catalog<String>("summary")=summary;

#if FE_BSO_DEBUG
	feLog("BlendShapeOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
