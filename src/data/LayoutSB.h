/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_LayoutSB_h__
#define __data_LayoutSB_h__

namespace fe
{


class Scope;
//class OffsetTable;
class RecordSB;
class BaseAccessor;
class StoreI;


/** @brief Record "type" definition

	@ingroup data

	A Layout is a definition of Attribute layout in Record instances.

	Layout objects can only be created via a Scope.  For typical use
	all operations on Layout objects are also done through Scope methods.

	@todo full data system guide
	*/
class FE_DL_EXPORT LayoutSB: public Layout, public Initialize<LayoutSB>,
		public ClassSafe<GlobalHolder>
{
	friend class RecordSB;
	friend class WeakRecordSB;
	friend class RecordArraySB;
	friend class SegmentStore;
	public:
		LayoutSB(sp<Scope> &scope);
		LayoutSB(sp<Scope> &scope, const String &name);
		LayoutSB(Scope *pScope, const String &name);
	private:
		LayoutSB(const LayoutSB &other);
		LayoutSB &operator=(const LayoutSB &other);
	public:
virtual	~LayoutSB(void);
		typedef FE_UWORD Offset;

#if 1
	private:
		class OffsetTable : public Counted
		{
			public:
						OffsetTable(void);
						~OffsetTable(void);


				Offset	*table(void) { return m_table; }

				void	resize(unsigned int a_size);

				void	attach(sp<LayoutSB> a_layout);
			private:
				std::vector< hp<LayoutSB> >	m_layouts;
				Offset						*m_table;
				unsigned int				m_size;
		};
	friend class LayoutSB::OffsetTable;
#endif


	public:
		LayoutSB		&copy(const LayoutSB &other);
virtual	void			initialize(void);

		/** Convienience function to Scope::populate */
virtual	void			populate(const String &attribute_name);
		/** Convienience function to Scope::populate */
virtual	void			populate(const String &attribute_name,
								const String &attribute_type);
		/** Convienience function to Scope::populate.  This particular one
			does a check if the layout is locked and if so verifys the
			exsitence of the attribute. */
virtual	void			populate(const BaseAccessor &accessor);
		/** Convienience function to Scope::populate */
virtual	void			populate(sp<Attribute> spAttribute);
		/** Convienience function to Scope::populate */
		void			populate(sp<LayoutSB> spLayoutSB);

virtual	void			setName(const String &name);
virtual	const String&	name(void) const;
virtual	const String	verboseName(void) const;

#ifdef FE_LOCK_SUPPRESSION
		/**	Set lock suppression.  If lock suppression is on (true) then an
			exception is thrown if locking is attempted. */
		void			suppressLock(bool suppression);
		bool			lockSuppressed(void) const;
#endif

virtual	void			setConstruct(bool construct);
virtual	const bool		&construct(void) const;

virtual	hp<Scope>&		scope(void);

virtual	void			peek(Peeker &peeker);

virtual	FE_UWORD		attributeCount(void) const {return (FE_UWORD)0;}
virtual	sp<Attribute>	attribute(FE_UWORD localIndex) { return sp<Attribute>(NULL); }

		RecordSB		createRecord(void);

virtual	bool			checkAttribute(FE_UWORD aLocator) const;
virtual	bool			checkAttributeStr(const String &a_name);
virtual	void			resizeLocatorTable(FE_UWORD aSize);

		void			createRecord(RecordSB &r_new);
		void			freeRecord(RecordSB &r_old);

virtual	void			notifyOfAttributeChange(sp<Depend> &depend);

	private:
		void			setOffsetTable(Offset *offset_table);
		const Offset	*offsetTable(void) const;
		const Offset	*rawOffsetTable(void) const;
		void			setSize(const FE_UWORD record_size);
		/// Size of an instance of the layout
		FE_UWORD			size(void) const;

		void			setLocked(bool locked);
		const bool		&locked(void) const;
		void			lock(void);

		void			depAttributeCheck(sp<Depend> &depend);
		void			depRecordCheck(sp<Depend> &depend);

		void			constructAttributes(void *stateBlock);
		void			destructAttributes(void *stateBlock);

	private:
						LayoutSB(void);
		void			constructor(sp<Scope> &scope);

		void			cacheIndices(void);
		void			setOffset(const FE_UWORD index, const Offset offset);

		unsigned int		serialIndex(void) const { return m_serialIndex; }

	private:
		String				m_name;
		bool				m_init;
		bool				m_construct;
		bool				m_suppressLock;
		Offset				*m_pTable;
		Offset				m_size;
		hp<Scope>			m_hpScope;
		std::vector<FE_UWORD>	m_offsetIndex;
		sp<StoreI>			m_store;
		sp<OffsetTable>		m_offsetTable;
		unsigned int		m_serialIndex;

	public:
static
const	FE_DL_PUBLIC Offset	offsetNone = 0xFFFFFFFF;

};

#if FE_DATA_STORE==FE_SB
typedef LayoutSB LayoutDefault;
#endif

inline bool LayoutSB::checkAttribute(FE_UWORD aLocator) const
{
	return (offsetTable()[aLocator] != offsetNone);
}

inline void LayoutSB::setName(const String &name)
{
	SAFEGUARDCLASS;
	m_name = name;
}
inline const String& LayoutSB::name(void) const
{
	SAFEGUARDCLASS;
	return m_name;
}
inline const String LayoutSB::verboseName(void) const
{
	return "LayoutSB " + name();
}

inline const bool &LayoutSB::locked(void) const
{
	SAFEGUARDCLASS;
	return m_init;
}

inline void LayoutSB::setConstruct(bool construct)
{
	SAFEGUARDCLASS;
	m_construct = construct;
}
inline const bool &LayoutSB::construct(void) const
{
	SAFEGUARDCLASS;
	return m_construct;
}

#if 1
inline void LayoutSB::setSize(const FE_UWORD size)
{
	SAFEGUARDCLASS;
	m_size = size;
}
inline FE_UWORD LayoutSB::size(void) const
{
	SAFEGUARDCLASS;
	return m_size;
}
#endif

inline void LayoutSB::setOffsetTable(LayoutSB::Offset *offset_table)
{
	SAFEGUARDCLASS;
	m_pTable = offset_table;
}

inline const LayoutSB::Offset *LayoutSB::offsetTable(void) const
{
	//SAFEGUARDCLASS; // too slow /// @todo remove
#if FE_CODEGEN<=FE_DEBUG
	if(!locked())
	{
		feX("LayoutSB::offsetTable",
			"layout not locked");
	}
#endif
	return m_pTable;
}

inline const LayoutSB::Offset *LayoutSB::rawOffsetTable(void) const
{
	//SAFEGUARDCLASS; // too slow /// @todo remove
	return m_pTable;
}

#if 1
inline void LayoutSB::setOffset(const FE_UWORD index, const LayoutSB::Offset offset)
{
	m_pTable[index] = offset;
}
#endif

inline hp<Scope>& LayoutSB::scope(void)
{
	SAFEGUARDCLASS;
	return m_hpScope;
}

} /* namespace */

#endif /* __data_LayoutSB_h__ */

