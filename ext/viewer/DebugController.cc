/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

DebugController::DebugController(void)
{
	m_init = false;
}

DebugController::~DebugController(void)
{
}

void DebugController::initialize(void)
{
	maskDefault("fe_debug_dump_scope",
		WindowEvent::Mask(
			WindowEvent::e_sourceKeyboard,
			(WindowEvent::Item)100 /* 'd' */,
			WindowEvent::e_statePress));
}

void DebugController::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	if(!m_init)
	{
		m_init = true;
		m_spScope = l_sig->scope();
	}
}

void DebugController::handle(Record &r_sig)
{
	if(!m_spScope.isValid()) { return; }

	m_dumpScope = maskGet("fe_debug_dump_scope");

	WindowEvent wev;
	wev.bind(r_sig);
	if(wev.is(m_dumpScope))
	{
		Peeker peek;
		peek(*m_spScope);
		feLog(peek.output().c_str());
	}
}

} /* namespace ext */
} /* namespace fe */
