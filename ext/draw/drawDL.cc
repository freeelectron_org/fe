/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <draw/draw.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
//	list.append(new String("fexWindowDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<DrawCached>("DrawI.DrawCached.fe");
#if FE_OS != FE_OSX
	pLibrary->add<DrawThreaded>("DrawI.DrawThreaded.fe");
#endif

	pLibrary->add<ViewCommon>("ViewI.ViewCommon.fe");
	pLibrary->add<DrawNull>("DrawI.DrawNull.fe");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
