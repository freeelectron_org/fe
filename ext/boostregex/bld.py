import sys
import os
import re
import string

import utility
import common
forge = sys.modules["forge"]

def setup(module):
    module.summary = []

    boost_sources = utility.safer_eval(os.environ["FE_BOOST_SOURCES"])
    for boost_source in boost_sources:
        if not os.path.exists(boost_source[2]) or not os.path.exists(boost_source[3]):
            module.summary += [ "-" + boost_source[0] ]
            continue

        boost_include = common.find_boost_include(boost_source[2], 'regex')
        boost_lib = common.find_boost_lib(boost_source[1], boost_source[3], 'regex')
        if boost_include == '' or boost_lib == '':
            module.summary += [ "-" + boost_source[0] ]
            continue

        if boost_source[0] == "":
            module.summary += [ "/" ]
        else:
            module.summary += [ boost_source[0] ]

#       module.summary += [ boost_lib ]

        srcList = [ "boostregex.pmh",
                    "boostregexDL" ]

        variant = boost_source[0]
        if variant != "":
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink(".", variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        dllname = "fexBoostRegex" + variant

        dll = module.DLL( dllname, srcList )

        alt_cxx = boost_source[4]

        if alt_cxx != '':
            dll.cxx = alt_cxx

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)
            srcTarget.includemap = { 'boostregex' : boost_source[2] }

            srcTarget.cppmap = {}
            srcTarget.cppmap["boost_namespace"] = "-DBOOST_NAMESPACE=" + boost_include
            srcTarget.cppmap["boost_include"] = "-DBOOST_INCLUDE_REGEX='\"" + boost_include + "/regex.hpp\"'"

            # Houdini hboost hack
            if boost_include == "hboost":
                srcTarget.cppmap["cxx11_abi"] = "-D_GLIBCXX_USE_CXX11_ABI=0"

            if alt_cxx != '':
                srcTarget.cxx = alt_cxx

        dll.linkmap = {}

        if forge.fe_os == "FE_LINUX":
            dll.linkmap["boostregex"] = "-L" + boost_source[3] + ' -l' + boost_lib
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            dll.linkmap["boostregex"] = "/LIBPATH:" + boost_source[3] + " " + boost_lib

#       if forge.fe_os == 'FE_LINUX' and not re.compile(".*houdini.*").match(boost_source[3]) and not re.compile(".*\/hfs.*").match(boost_source[3]):
        if forge.fe_os == 'FE_LINUX':
            dll.linkmap["boostregex"] += ' -Wl,-rpath=' + boost_source[3]

        deplibs = forge.basiclibs[:]

        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            deplibs += [    "feMemoryLib",
                            "feImportMemoryLib" ]

        forge.deps( [ dllname + "Lib" ], deplibs )
