/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <networkhost/networkhost.pmh>

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
// https://stackoverflow.com/questions/46013382/c-strndup-implicit-declaration/46013414
char* strndup(const char *s, size_t n)
{
	char *p;
	size_t n1;

	for (n1 = 0; n1 < n && s[n1] != '\0'; n1++)
		continue;
	p = (char*)malloc(n + 1);
	if (p != NULL)
	{
		memcpy(p, s, n1);
		p[n1] = '\0';
	}
	return p;
}
#endif

#define FE_NETHOST_EXTERN_STRING_MAX		8196

using namespace fe;
using namespace fe::ext;

static sp<NetHost> gs_spNetHost;
static int gs_netHostCount=0;
static Array< sp<StateCatalog> > gs_spaces;
static Array< sp<StateCatalog::Snapshot> > gs_snapshots;
static Array<I32> gs_snapshotCount;

sp<StateCatalog> fe_nethost_lookup_space(int a_spaceIndex)
{
	if(a_spaceIndex<0 || a_spaceIndex>=(I32)gs_spaces.size())
	{
		return sp<StateCatalog>(NULL);
	}

	return gs_spaces[a_spaceIndex];
}

sp<StateCatalog::Snapshot> fe_nethost_lookup_snapshot(int a_snapshotIndex)
{
	if(a_snapshotIndex<0 || a_snapshotIndex>=(I32)gs_snapshots.size())
	{
		return sp<StateCatalog::Snapshot>(NULL);
	}

	return gs_snapshots[a_snapshotIndex];
}

extern "C"
{

//* NOTE the lowercase/underscore deviation is intentional for distinction

FE_DL_EXPORT int fe_nethost_execute(
		int a_output_size,char* a_output,const char* a_input)
{
	String result="command not recognized";
	strncpy(a_output,result.c_str(),a_output_size);
	return 1;
}

FE_DL_EXPORT int fe_nethost_acquire(void)
{
	feLog("fe_nethost_acquire %d\n",gs_netHostCount);
	return ++gs_netHostCount;
}

FE_DL_EXPORT int fe_nethost_release(void)
{
	feLog("fe_nethost_release %d\n",gs_netHostCount);
	if(--gs_netHostCount<1)
	{
		gs_spNetHost=NULL;
	}
	return gs_netHostCount;
}

FE_DL_EXPORT int fe_nethost_access_space(char* a_space_name,
	char* a_implementation)
{
	feLog("fe_nethost_access_space \"%s\" \"%s\"\n",
			a_space_name,a_implementation);

	if(gs_spNetHost.isNull())
	{
		gs_spNetHost=NetHost::create();
		FEASSERT(gs_spNetHost.isValid());

		sp<Master> spMaster=gs_spNetHost->master();
		sp<Registry> spRegistry=spMaster->registry();

		assertMath(spMaster->typeMaster());

		Result result=spRegistry->manage("feAutoLoadDL");
		if(failure(result))
		{
			return -1;
		}

		result=spRegistry->manage("fexNetworkHostDL");
		if(failure(result))
		{
			return -1;
		}
	}
	if(gs_spNetHost.isNull())
	{
		return -1;
	}

	sp<StateCatalog> spSpace=
			gs_spNetHost->accessSpace(a_space_name,a_implementation);
	if(spSpace.isNull())
	{
		return -1;
	}

	const I32 spaceCount=gs_spaces.size();
	for(I32 spaceIndex=0;spaceIndex<spaceCount;spaceIndex++)
	{
		if(spSpace == gs_spaces[spaceIndex])
		{
			return spaceIndex;
		}
	}

	gs_spaces.push_back(spSpace);
	return spaceCount;
}

FE_DL_EXPORT int fe_nethost_space_start(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	spSpace->start();

	return 1;
}

FE_DL_EXPORT int fe_nethost_space_stop(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	spSpace->stop();

	return 1;
}

FE_DL_EXPORT int fe_nethost_space_wait_for_connection(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	spSpace->waitForConnection();

	return 1;
}

FE_DL_EXPORT int fe_nethost_space_flush(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	spSpace->flush();

	return 1;
}

FE_DL_EXPORT int fe_nethost_space_wait_for_update(int a_spaceIndex,
	int* a_pFlushCount,int* a_pSpins)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	BWORD keepWaiting(TRUE);
	I32 microSleep(10);

	return successful(spSpace->waitForUpdate(
			*a_pFlushCount,*a_pSpins,keepWaiting,microSleep));
}

FE_DL_EXPORT int fe_nethost_space_lock_after_update(int a_spaceIndex,
	int* a_pFlushCount,int* a_pSpins)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	BWORD keepWaiting(TRUE);
	I32 microSleep(10);

	return successful(spSpace->lockAfterUpdate(
			*a_pFlushCount,*a_pSpins,keepWaiting,microSleep));
}

FE_DL_EXPORT int fe_nethost_space_lock(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	spSpace->safeLockShared();

	return 1;
}

FE_DL_EXPORT int fe_nethost_space_unlock(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	spSpace->safeUnlockShared();

	return 1;
}

FE_DL_EXPORT int fe_nethost_space_set_state_bools(int a_spaceIndex,
	char* a_key_name,char* a_property,bool* a_value,int a_count)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=spSpace->setState<bool>(
				a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_space_get_state_bools(int a_spaceIndex,
	char* a_key_name,char* a_property,bool* a_value,int a_count,bool a_safe)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=a_safe?
				spSpace->getState<bool>(a_key_name,a_property,a_value[0]):
				spSpace->getStateUnsafe<bool>(a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_space_set_state_ints(int a_spaceIndex,
	char* a_key_name,char* a_property,int* a_value,int a_count)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=spSpace->setState<I32>(
				a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_space_get_state_ints(int a_spaceIndex,
	char* a_key_name,char* a_property,int* a_value,int a_count,bool a_safe)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=a_safe?
				spSpace->getState<I32>(a_key_name,a_property,a_value[0]):
				spSpace->getStateUnsafe<I32>(a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_space_set_state_floats(int a_spaceIndex,
	char* a_key_name,char* a_property,float* a_value,int a_count)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=spSpace->setState<Real>(
				a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	if(a_count==16)
	{
		SpatialTransform xform;
		set(xform,a_value);

		const Result result=spSpace->setState<SpatialTransform>(
				a_key_name,a_property,xform);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_space_get_state_floats(int a_spaceIndex,
	char* a_key_name,char* a_property,float* a_value,int a_count,bool a_safe)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		Real temp(0);
		const Result result=a_safe?
				spSpace->getState<Real>(a_key_name,a_property,temp):
				spSpace->getStateUnsafe<Real>(a_key_name,a_property,temp);
		a_value[0]=temp;
		return successful(result);
	}
	if(a_count==16)
	{
		SpatialTransform xform;

		const Result result=a_safe?
				spSpace->getState<SpatialTransform>(
				a_key_name,a_property,xform):
				spSpace->getStateUnsafe<SpatialTransform>(
				a_key_name,a_property,xform);
		if(failure(result))
		{
			return 0;
		}

		xform.copy16(a_value);
		return 1;
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_space_set_state_string(int a_spaceIndex,
	char* a_key_name,char* a_property,char* a_value)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	const Result result=spSpace->setState<String>(
			a_key_name,a_property,a_value);
	return successful(result);
}

FE_DL_EXPORT char* fe_nethost_space_get_state_string(int a_spaceIndex,
	char* a_key_name,char* a_property,bool a_safe)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return NULL;
	}

	String temp;

	const Result result=a_safe?
			spSpace->getState<String>(a_key_name,a_property,temp):
			spSpace->getStateUnsafe<String>(a_key_name,a_property,temp);
	if(failure(result))
	{
		return NULL;
	}

	return strndup(temp.c_str(),FE_NETHOST_EXTERN_STRING_MAX);
}

FE_DL_EXPORT int fe_nethost_space_set_state_bytes(int a_spaceIndex,
	char* a_key_name,char* a_property,U8* a_value,int a_size)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	Array<U8> temp;
	temp.resize(a_size);
	memcpy(temp.data(),a_value,a_size);

	const Result result=spSpace->setState< Array<U8> >(
			a_key_name,a_property,temp);
	return successful(result);
}

FE_DL_EXPORT U8* fe_nethost_space_get_state_bytes(int a_spaceIndex,
	char* a_key_name,char* a_property,bool a_safe,int* a_pBytesRead)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return NULL;
	}

	Array<U8> temp;

	const Result result=a_safe?
			spSpace->getState< Array<U8> >(a_key_name,a_property,temp):
			spSpace->getStateUnsafe< Array<U8> >(a_key_name,a_property,temp);
	*a_pBytesRead=temp.size();
	if(failure(result) || !(*a_pBytesRead))
	{
		return NULL;
	}

	U8* buffer=(U8*)allocate(*a_pBytesRead);
	memcpy(buffer,temp.data(),*a_pBytesRead);

	return buffer;
}

FE_DL_EXPORT int fe_nethost_space_send_message_string(int a_spaceIndex,
	char* a_key_name,char* a_value)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return 0;
	}

	const Result result=spSpace->sendMessage<String>(
			a_key_name,a_value);
	return successful(result);
}

FE_DL_EXPORT char* fe_nethost_space_next_message_string(int a_spaceIndex,
	char* a_key_name)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return NULL;
	}

	String temp;

	const Result result=spSpace->nextMessage<String>(a_key_name,temp);
	if(failure(result))
	{
		return NULL;
	}

	return strndup(temp.c_str(),FE_NETHOST_EXTERN_STRING_MAX);
}

///////////////////////////////////////////////////////////////////////////////

FE_DL_EXPORT int fe_nethost_snapshot_acquire(int a_spaceIndex)
{
	sp<StateCatalog> spSpace=fe_nethost_lookup_space(a_spaceIndex);
	if(spSpace.isNull())
	{
		return -1;
	}

	sp<StateCatalog::Snapshot> spSnapshot;
	Result result=spSpace->snapshot(spSnapshot);
	if(!successful(result))
	{
		return -1;
	}

	const I32 snapshotCount=gs_snapshots.size();
	for(I32 snapshotIndex=0;snapshotIndex<snapshotCount;snapshotIndex++)
	{
		if(spSnapshot == gs_snapshots[snapshotIndex])
		{
			gs_snapshotCount[snapshotIndex]++;
//			feLog("fe_nethost_snapshot_acquire repeat %d/%d count %d\n",
//					snapshotIndex,snapshotCount,
//					gs_snapshotCount[snapshotIndex]);
			return snapshotIndex;
		}
	}
	for(I32 snapshotIndex=0;snapshotIndex<snapshotCount;snapshotIndex++)
	{
		if(gs_snapshots[snapshotIndex].isNull())
		{
//			feLog("fe_nethost_snapshot_acquire replace %d/%d\n",
//					snapshotIndex,snapshotCount);
			gs_snapshots[snapshotIndex]=spSnapshot;
			gs_snapshotCount[snapshotIndex]=1;
			return snapshotIndex;
		}
	}
//	feLog("fe_nethost_snapshot_acquire append %d\n",snapshotCount);
	gs_snapshots.push_back(spSnapshot);
	gs_snapshotCount.push_back(1);
	return snapshotCount;
}

FE_DL_EXPORT int fe_nethost_snapshot_release(int a_snapshotIndex)
{
//	feLog("fe_nethost_snapshot_release %d/%d\n",a_snapshotIndex,gs_snapshots.size());
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return -1;
	}

	const I32 count= --gs_snapshotCount[a_snapshotIndex];

//	feLog("fe_nethost_snapshot_release %d count %d\n",a_snapshotIndex,count);
	if(count<1)
	{
		gs_snapshots[a_snapshotIndex]=NULL;
		return 0;
	}
	return count;
}

FE_DL_EXPORT int fe_nethost_snapshot_get_flush_count(int a_snapshotIndex)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return -1;
	}

	return spSnapshot->flushCount();
}

FE_DL_EXPORT int fe_nethost_snapshot_get_serial(int a_snapshotIndex)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return -1;
	}

	return spSnapshot->serial();
}

FE_DL_EXPORT int fe_nethost_snapshot_get_state_bools(int a_snapshotIndex,
	char* a_key_name,char* a_property,bool* a_value,int a_count)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=
				spSnapshot->getState<bool>(a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_snapshot_get_state_ints(int a_snapshotIndex,
	char* a_key_name,char* a_property,int* a_value,int a_count)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		const Result result=
				spSnapshot->getState<I32>(a_key_name,a_property,a_value[0]);
		return successful(result);
	}

	return 0;
}

FE_DL_EXPORT int fe_nethost_snapshot_get_state_floats(int a_snapshotIndex,
	char* a_key_name,char* a_property,float* a_value,int a_count)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return 0;
	}

	if(a_count==1)
	{
		Real temp(0);
		const Result result=
				spSnapshot->getState<Real>(a_key_name,a_property,temp);
		a_value[0]=temp;
		return successful(result);
	}
	if(a_count==16)
	{
		SpatialTransform xform;

		const Result result=
				spSnapshot->getState<SpatialTransform>(
				a_key_name,a_property,xform);
		if(failure(result))
		{
			return 0;
		}

		xform.copy16(a_value);
		return 1;
	}

	return 0;
}

FE_DL_EXPORT U8* fe_nethost_snapshot_get_state_bytes(int a_snapshotIndex,
	char* a_key_name,char* a_property,int* a_pBytesRead)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return 0;
	}

	Array<U8> temp;

	const Result result=
			spSnapshot->getState< Array<U8> >(a_key_name,a_property,temp);
	*a_pBytesRead=temp.size();
	if(failure(result) || !(*a_pBytesRead))
	{
		return NULL;
	}

	U8* buffer=(U8*)allocate(*a_pBytesRead);
	memcpy(buffer,temp.data(),*a_pBytesRead);

	return buffer;
}

FE_DL_EXPORT char* fe_nethost_snapshot_get_state_string(int a_snapshotIndex,
	char* a_key_name,char* a_property)
{
	sp<StateCatalog::Snapshot> spSnapshot=
			fe_nethost_lookup_snapshot(a_snapshotIndex);
	if(spSnapshot.isNull())
	{
		return NULL;
	}

	String temp;

	const Result result=
			spSnapshot->getState<String>(a_key_name,a_property,temp);
	if(failure(result))
	{
		return NULL;
	}

	return strndup(temp.c_str(),FE_NETHOST_EXTERN_STRING_MAX);
}

FE_DL_EXPORT int fe_nethost_free_string(char* a_buffer)
{
	free(a_buffer);
	return 1;
}

}
