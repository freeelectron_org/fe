/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <usd/hydra.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOpenGLDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<DrawHydra>("DrawI.DrawHydra.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	//* USD thread-safe call to glewInit()
	GlfGlewInit();

	//* NOTE this is not what the manifest says
	//* Once USD gets loaded, it takes priority.
	spLibrary->registry()->prioritize("DrawI.DrawHydra.fe",2);
}

}
