/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceBindOp_h__
#define __operator_SurfaceBindOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to correlate a attachable surface with a driving surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceBindOp:
	public OperatorThreaded,
	public Initialize<SurfaceBindOp>
{
	public:

						SurfaceBindOp(void)									{}
virtual					~SurfaceBindOp(void)								{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handleBind(sp<SignalerI> a_spSignalerI,
								sp<Layout> a_spLayout);
virtual	void			handle(Record& a_rSignal);

						using OperatorThreaded::run;

virtual	void			run(I32 a_id,sp<SpannedRange> a_spRange);

	protected:
		sp<SurfaceAccessibleI>	subdivide(
								sp<SurfaceAccessibleI>& a_rspAccessibleI,
								I32 a_depth,I32 a_flatDepth,Record& a_rSignal);
		void			relayMessages(void);

		String			retrieveSignature(
								sp<SurfaceAccessibleI>& a_rspAccessibleI);
		void			storeSignature(sp<SurfaceAccessibleI>& a_rspAccessibleI,
								String a_signature);
		void			discardSignature(
								sp<SurfaceAccessibleI>& a_rspAccessibleI);

		void			retrieveParameters(
								sp<SurfaceAccessibleI>& a_rspAccessibleI,
								String& a_rDriverGroup,
								String& a_rFragmentAttribute,
								String& a_rInputGroup,
								I32& a_rBindings,I32& a_rSamples,
								BWORD& a_rBindPrimitives,
								BWORD& a_rBindFragments,String& a_rCovertKey,
								String& a_rLocators,I32& a_rSubdiv,
								BWORD& a_rPointsOnly);
		void			storeParameters(
								sp<SurfaceAccessibleI>& a_rspAccessibleI,
								String a_driverGroup,String a_fragmentAttribute,
								String a_inputGroup,
								I32 a_bindCount,I32 a_samples,
								BWORD a_bindPrimitives,
								BWORD a_bindFragments,String a_covertKey,
								String a_locators,I32 a_subdiv,
								BWORD a_pointsOnly);
		void			discardParameters(
								sp<SurfaceAccessibleI>& a_rspAccessibleI);

		I32				accessBindings(sp<SurfaceAccessibleI>& a_rspAccessibleI,
								Array< sp<SurfaceAccessorI> >& a_rFaces,
								Array< sp<SurfaceAccessorI> >& a_rBarys,
								I32 a_bindCount,
								BWORD a_bindPrimitives,
								BWORD a_allowCreation,
								BWORD a_writable);
		void			discardBindings(
								sp<SurfaceAccessibleI>& a_rspAccessibleI,
								I32 a_bindCount,BWORD a_bindPrimitives);

		sp<SurfaceAccessibleI>					m_spBindingAccessible;
		sp<SurfaceAccessibleI>					m_spOutputAccessible;
		sp<SurfaceAccessibleI>					m_spDriverAccessible;

		sp<SurfaceI>							m_spDriver;
		sp<SurfaceI>							m_spSnapDriver;

		sp<OperatorSurfaceI>					m_spSubdivideOp;

		BWORD									m_fragmented;
		BWORD									m_bindPrimitives;
		BWORD									m_conform;
		BWORD									m_partitionDriver;
		String									m_peakName;

		Element									m_element;
		Attribute								m_attribute;

		Array<I32>								m_masterBinding;
		Array<I32>								m_pivotIndex;
		Array<I32>								m_driverFacePrimitive;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceBindOp_h__ */
