#! /usr/bin/python3

import pyfe.context

fe = pyfe.context.Context()

bloatOp = fe.create("BloatOp")
bloatOp["Bloat"] = 16.0

blendShapeOp = fe.create("BlendShapeOp")
blendShapeOp["secondWeight"] = 0.5

inputSurface = fe.load("../media/model/teapot.obj", name = "teapot")

bloatedSurface = bloatOp.operate(inputSurface, name = "bloated")

blendedSurface = blendShapeOp.operate(inputSurface, bloatedSurface, name = "blended")

blendedSurface.save("test/blended.obj")
