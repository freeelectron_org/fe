#! /usr/bin/python

import sys

if len(sys.argv) < 2:
    print "Usage: " + sys.argv[0] + " [-frame FRAME] [-frames START END] [-options \"OPTIONS...\"] FILE"
    exit()

printMax = 4
filename = ""
frameRange = [ 0 ]
options = ""
argCount = len(sys.argv)

surfaceOptions = {}

argIndex = 1
while True:
    arg = sys.argv[argIndex]
    argIndex += 1

    if arg == "-frame":
        frame = int(sys.argv[argIndex])
        argIndex += 1

        frameRange = [ frame ]
    elif arg == "-frames":
        start = int(sys.argv[argIndex])
        end = int(sys.argv[argIndex + 1])
        argIndex += 2

        frameRange = range(start, end + 1)
    elif arg == "-options":
        options = sys.argv[argIndex]
        argIndex += 1
    else:
        filename = arg

        surfaceOptions = options

    if argIndex >= argCount:
        break;

if filename == "":
    print sys.argv[0] + ": missing file operand"
    exit

import pyfe.context

fe = pyfe.context.Context()
fe.manage("fexTerminalDL")

for frame in frameRange:
    fe.setFrame(frame)

    pathname = filename.replace("$F", str(frame))
    surfaceName = pathname
    surface = fe.load(pathname, surfaceName, surfaceOptions)

    print "\nFrame %.6G Surface \"%s\"" % (frame, surface.name())

    for element in ["point", "primitive"]:
        elementCount = int(surface.count(element))
        print "%s count %d" % (element, elementCount)

        printCount = elementCount
        if printCount > printMax:
            printCount = printMax
        specs = surface.specs(element)

        if not len(specs):
            continue

        print "%s attributes:" % element

        for attrName in specs:
            attrType = specs[attrName]
            print "  %s \"%s\"" % (attrType, attrName)

            accessor = surface.access(element, attrName)

            for elementIndex in range(0, printCount):
                value = accessor[elementIndex]

                print "    %d/%d %s" % (elementIndex, elementCount, value)

    primitiveCount = int(surface.count("primitive"))
    if primitiveCount:
        print "primitive vertices:"

        printCount = primitiveCount
        if printCount > printMax:
            printCount = printMax

        primitiveVertices = surface.access("primitive", "<vertices>")

        for primitiveIndex in range(0, printCount):
            vertices = primitiveVertices[primitiveIndex]

            print "  %d/%d %s" % (primitiveIndex, primitiveCount, vertices)

print
