/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdiniEngine.pmh>

#define FE_SAHE_DEBUG		TRUE
#define FE_SAHE_VERBOSE		TRUE

namespace fe
{
namespace ext
{

SurfaceAccessibleHapi::SurfaceAccessibleHapi(void)
{
#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::SurfaceAccessibleHapi\n");
#endif
}

SurfaceAccessibleHapi::~SurfaceAccessibleHapi(void)
{
#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::~SurfaceAccessibleHapi\n");
#endif
}

BWORD SurfaceAccessibleHapi::isBound(void)
{
	return (HAPI_IsSessionValid(&m_hapiSession)==HAPI_RESULT_SUCCESS);
}

void SurfaceAccessibleHapi::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
	a_rSpecs.clear();

	//* NOTE a_node ignored

	SurfaceAccessibleI::Spec spec;

	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			a_rSpecs=m_pointSpecs;
			break;

		case SurfaceAccessibleI::e_vertex:
			a_rSpecs=m_vertexSpecs;
			break;

		case SurfaceAccessibleI::e_primitive:
			a_rSpecs=m_primitiveSpecs;

			spec.set("properties","integer");
			a_rSpecs.push_back(spec);
			break;

		case SurfaceAccessibleI::e_detail:
			a_rSpecs=m_detailSpecs;
			break;
		default:
			;
	}
}

sp<SurfaceAccessorI> SurfaceAccessibleHapi::accessor(
	String a_node,
	SurfaceAccessibleI::Element a_element,
	String a_name,
	SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
//~	if(a_name!="instance")
//~	{
//~		feLog("get Hapi accessor string \"%s\"\n",a_name.c_str());
//~	}

	sp<SurfaceAccessorHapi> spAccessor=accessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
//~			feLog("SUCCESS\n");
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleHapi::accessor(
	String a_node,
	SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute,
	SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
//~	feLog("get Hapi accessor attr \"%s\"\n",
//~			SurfaceAccessibleBase::attributeString(
//~			a_attribute).c_str());

	sp<SurfaceAccessorHapi> spAccessor=accessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
//~			feLog("SUCCESS\n");
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorHapi> SurfaceAccessibleHapi::accessor(void)
{
	if(!isBound())
	{
		return sp<SurfaceAccessorI>(NULL);
	}
	sp<SurfaceAccessorHapi> spAccessorHapi(new SurfaceAccessorHapi);
	spAccessorHapi->setSession(&m_hapiSession);
	spAccessorHapi->setNodeId(m_hapiNodeId);

	return spAccessorHapi;
}

BWORD SurfaceAccessibleHapi::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load \"%s\"\n",a_filename.c_str());
#endif

	//* TODO convert relative path to absolute
	if(a_filename.length() && a_filename.c_str()[0]!='/')
	{
		feLog("SurfaceAccessibleHapi::load"
				" relative path will probably fail\n");
	}

	String options;

	if(a_spSettings.isValid())
	{
		m_frame=a_spSettings->catalog<Real>("frame");
		options=a_spSettings->catalog<String>("options");
//		spFilter=a_spSettings->catalog< sp<Component> >("filter");
	}
	else
	{
		m_frame=0.0;
		options="";
	}

	m_optionMap.clear();

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load options \"%s\"\n",options.c_str());
#endif

	String buffer=options;
	String option;
	while(!(option=buffer.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

#if FE_SAHE_DEBUG
		feLog("SurfaceAccessibleHapi::load parse \"%s\" \"%s\"\n",
				property.c_str(),value.c_str());
#endif

		m_optionMap[property]=value;
	}

	String nodePath;
	if(m_optionMap.find("path")!=m_optionMap.end())
	{
		nodePath=m_optionMap["path"];

#if FE_SAHE_DEBUG
		feLog("SurfaceAccessibleHapi::load nodePath \"%s\"\n",
				nodePath.c_str());
#endif
	}

	m_curveParts=0;
	m_meshParts=0;

	m_pointSpecs.clear();
	m_vertexSpecs.clear();
	m_primitiveSpecs.clear();
	m_detailSpecs.clear();

	HAPI_Result result;

	//* TODO check filename and options for changes
	if(HAPI_IsSessionValid(&m_hapiSession)!=HAPI_RESULT_SUCCESS)
	{
		const BWORD useSocket=FALSE;
		const BWORD usePipe=TRUE;

		HAPI_ThriftServerOptions thriftServerOptions;
		thriftServerOptions.autoClose=true;
		thriftServerOptions.timeoutMs=Real(4000);

		if(useSocket)
		{
			const int port=10101;

#if FE_SAHE_DEBUG
			feLog("SurfaceAccessibleHapi::load create socket server\n");
#endif
			HAPI_ProcessId process_id;
#if FE_HAPI_19_5_PLUS
			result=HAPI_StartThriftSocketServer(&thriftServerOptions,
					port,&process_id,NULL);
#else
			result=HAPI_StartThriftSocketServer(&thriftServerOptions,
					port,&process_id);
#endif
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessibleHapi::load"
						" create socket server failed\n  \"%s\"\n",
						lastError(&m_hapiSession).c_str());
			}

#if FE_SAHE_DEBUG
			feLog("SurfaceAccessibleHapi::load create socket session\n");
#endif

			result=HAPI_CreateThriftSocketSession(&m_hapiSession,
					"localhost",port);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessibleHapi::load"
						" create socket session failed\n  \"%s\"\n",
						lastError(&m_hapiSession).c_str());
			}


		}
		else if(usePipe)
		{
			String pipe_name;

			pipe_name.sPrintf("/tmp/hapi.%d.%s.%.6G",
					getpid(),a_filename.basename().c_str(),m_frame);

			const String path=getenv("PATH");
			const String ld_library_path=getenv("LD_LIBRARY_PATH");

			const String fe_houdini_dso=FE_STRING(FE_HOUDINI_DSO);

			const String new_path=
					path+":"+fe_houdini_dso+"/../bin";
			const String new_ld_library_path=
					ld_library_path+":"+fe_houdini_dso;

			//* WARNING could be repeated; probably not thread safe
			setenv("PATH",new_path.c_str(),TRUE);
			setenv("LD_LIBRARY_PATH",new_ld_library_path.c_str(),TRUE);

#if FE_SAHE_DEBUG
			feLog("SurfaceAccessibleHapi::load create pipe server\n");
#endif
			HAPI_ProcessId process_id=0;
#if FE_HAPI_19_5_PLUS
			result=HAPI_StartThriftNamedPipeServer(&thriftServerOptions,
					pipe_name.c_str(),&process_id,NULL);
#else
			result=HAPI_StartThriftNamedPipeServer(&thriftServerOptions,
					pipe_name.c_str(),&process_id);
#endif
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessibleHapi::load"
						" create pipe server failed\n  \"%s\"\n",
						lastError(NULL).c_str());
			}

//			setenv("PATH",path.c_str(),TRUE);
//			setenv("LD_LIBRARY_PATH",ld_library_path.c_str(),TRUE);

#if FE_SAHE_DEBUG
			feLog("SurfaceAccessibleHapi::load create pipe session\n");
#endif

			result=HAPI_CreateThriftNamedPipeSession(&m_hapiSession,
					pipe_name.c_str());
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessibleHapi::load"
						" create socket session failed\n  \"%s\"\n",
						lastError(NULL).c_str());
			}
		}
		else
		{
#if FE_SAHE_DEBUG
			feLog("SurfaceAccessibleHapi::load create local session\n");
#endif

			result=HAPI_CreateInProcessSession(&m_hapiSession);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessibleHapi::load"
						" create local session failed\n  \"%s\"\n",
						lastError(&m_hapiSession).c_str());
			}
		}

#if FE_SAHE_DEBUG
		feLog("SurfaceAccessibleHapi::load initialize\n");
#endif

		HAPI_CookOptions cook_options;
		cook_options.cookTemplatedGeos=true;

		HAPI_Bool use_cooking_thread=false;
		int cooking_thread_stack_size= -1;
		String houdini_environment_files;
		String otl_search_path;

		//* WARNING build files are likely to get moved elsewhere
		const String dso_search_path=FE_STRING(FE_HOUDINI_BUILD);

#if FE_SAHE_DEBUG
		feLog("SurfaceAccessibleHapi::load dso_search_path\n  \"%s\"\n",
				dso_search_path.c_str());
#endif

		String image_dso_search_path;
		String audio_dso_search_path;
		result=HAPI_Initialize(&m_hapiSession,
				&cook_options,use_cooking_thread,cooking_thread_stack_size,
				houdini_environment_files.c_str(),
				otl_search_path.c_str(),
				dso_search_path.c_str(),
				image_dso_search_path.c_str(),
				audio_dso_search_path.c_str());
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load initialize failed\n  \"%s\"\n",
					lastError(&m_hapiSession).c_str());
		}

#if FE_SAHE_DEBUG
		feLog("SurfaceAccessibleHapi::load read hip\n");
#endif

		const HAPI_Bool cook_on_load=false;
		result=HAPI_LoadHIPFile(&m_hapiSession,a_filename.c_str(),cook_on_load);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load read hip failed\n  \"%s\"\n",
					lastError(&m_hapiSession).c_str());
		}
	}

	HAPI_TimelineOptions timeline_options;
	result=HAPI_GetTimelineOptions(&m_hapiSession,&timeline_options);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load get timeline failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load startTime %.6G endTime %.6G fps %.6G\n",
			timeline_options.startTime,timeline_options.endTime,
			timeline_options.fps);
#endif

	//* TODO conversion required?
	const Real time=m_frame/timeline_options.fps;

	result=HAPI_SetTime(&m_hapiSession,time);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load set time failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load get root\n");
#endif

	HAPI_NodeId root_node_id= -1;
	result=HAPI_GetManagerNodeId(&m_hapiSession,
			HAPI_NODETYPE_OBJ,&root_node_id);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load get root failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}


#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load compose under node %d\n",root_node_id);
#endif

	HAPI_Bool recursive=true;
	int nodeCount= -1;
	result=HAPI_ComposeChildNodeList(&m_hapiSession,root_node_id,
			HAPI_NODETYPE_ANY,
//			HAPI_NODEFLAGS_EDITABLE|HAPI_NODEFLAGS_NETWORK,
			HAPI_NODEFLAGS_ANY,
			recursive,&nodeCount);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load compose failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load composed %d nodes\n",nodeCount);
#endif

	HAPI_NodeId child_node_ids[nodeCount];
	result=HAPI_GetComposedChildNodeList(&m_hapiSession,root_node_id,
			child_node_ids,nodeCount);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load get composed failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}

	for(I32 nodeIndex=-1;nodeIndex<nodeCount;nodeIndex++)
	{
		HAPI_NodeId child_id=
				nodeIndex>=0? child_node_ids[nodeIndex]: root_node_id;

		HAPI_NodeInfo node_info;
		result=HAPI_GetNodeInfo(&m_hapiSession,child_id,&node_info);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load"
					" get node info failed on %d (%d/%d)\n  \"%s\"\n",
					child_id,nodeIndex,nodeCount,
					lastError(&m_hapiSession).c_str());
		}

//		HAPI_AssetInfo asset_info;
//		result=HAPI_GetAssetInfo(&m_hapiSession,child_id,&asset_info);
//		if(result!=HAPI_RESULT_SUCCESS)
//		{
//			feLog("SurfaceAccessibleHapi::load"
//					" get asset info failed on %d (%d/%d)\n  \"%s\"\n",
//					child_id,nodeIndex,nodeCount,
//					lastError(&m_hapiSession).c_str());
//		}

		HAPI_StringHandle stringHandle;

//		stringHandle=node_info.nameSH;
		stringHandle=node_info.internalNodePathSH;
//		result=HAPI_GetNodePath(&m_hapiSession,child_id,-1,&stringHandle);
//		stringHandle=asset_info.nameSH;
//		stringHandle=asset_info.fullOpNameSH;

		int length=1024;
		char string_value[length];
		string_value[0]=0;

		result=HAPI_GetString(&m_hapiSession,stringHandle,string_value,length);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load"
					" get name failed on %d (%d/%d)\n  \"%s\"\n",
					child_id,nodeIndex,nodeCount,
					lastError(&m_hapiSession).c_str());
		}

		const String nodeName(string_value);

#if FE_SAHE_VERBOSE
		feLog("  %d/%d %d \"%s\"\n",nodeIndex,nodeCount,
				child_id,nodeName.c_str());
#endif

		//* TODO if nodePath empty, check display flags

		if(!nodePath.empty())
		{
			if(nodeName!=nodePath)
			{
				continue;
			}
		}
		else if(nodeIndex!=nodeCount-1)
		{
			//* TEMP only examine last node
			continue;
		}

		m_hapiNodeId=child_id;

#if FE_SAHE_VERBOSE
		const I32 paramCount=node_info.parmCount;
		feLog("    params %d\n",paramCount);

		HAPI_ParmInfo parm_infos_array[paramCount];
		result=HAPI_GetParameters(&m_hapiSession,child_id,
				parm_infos_array,0,paramCount);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load"
					" get parameters failed on %d (%d/%d)\n  \"%s\"\n",
					child_id,nodeIndex,nodeCount,
					lastError(&m_hapiSession).c_str());
		}

		for(I32 paramIndex=0;paramIndex<paramCount;paramIndex++)
		{
			HAPI_ParmInfo& rParamInfo=parm_infos_array[paramIndex];

			if(HAPI_GetString(&m_hapiSession,rParamInfo.nameSH,
					string_value,length)==HAPI_RESULT_SUCCESS)
			{
				feLog("      %d/%d \"%s\"\n",
						paramIndex,paramCount,string_value);
			}

//~			if(string_value==String("filename"))
//~			{
//~				result=HAPI_SetParmStringValue(&m_hapiSession,child_id,
//~						"torus.geo",rParamInfo.id,0);
//~				if(result!=HAPI_RESULT_SUCCESS)
//~				{
//~					feLog("SurfaceAccessibleHapi::load"
//~							" failed to set param on %d\n  \"%s\"\n",
//~							child_id,lastError(&m_hapiSession).c_str());
//~				}
//~			}
		}
#endif

		result=HAPI_CookNode(&m_hapiSession,child_id,NULL);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load"
					" failed to cook %d\n  \"%s\"\n",
					child_id,lastError(&m_hapiSession).c_str());
		}

		HAPI_GeoInfo geo_info;
		result=HAPI_GetGeoInfo(&m_hapiSession,m_hapiNodeId,&geo_info);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceTrianglesHapi::load"
					" get geo info failed on %d\n  \"%s\"\n",m_hapiNodeId,
					SurfaceAccessibleHapi::lastError(&m_hapiSession).c_str());
		}

		const I32 partCount=geo_info.partCount;
		for(I32 partIndex=0;partIndex<partCount;partIndex++)
		{
			const HAPI_PartId part_id=partIndex;
			HAPI_PartInfo part_info;

			result=HAPI_GetPartInfo(&m_hapiSession,m_hapiNodeId,
					part_id,&part_info);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceTrianglesHapi::load"
						" get part info failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(
						&m_hapiSession).c_str());
			}

			// HAPI_PARTTYPE_INVALID
			// HAPI_PARTTYPE_MESH
			// HAPI_PARTTYPE_CURVE
			// HAPI_PARTTYPE_VOLUME
			// HAPI_PARTTYPE_INSTANCER
			// HAPI_PARTTYPE_BOX
			// HAPI_PARTTYPE_SPHERE
			// HAPI_PARTTYPE_MAX
			if(part_info.type==HAPI_PARTTYPE_MESH)
			{
#if FE_SAHE_VERBOSE
				feLog("    type MESH\n");
#endif
				m_meshParts++;
			}
			else if(part_info.type==HAPI_PARTTYPE_CURVE)
			{
#if FE_SAHE_VERBOSE
				feLog("    type CURVE\n");
#endif
				m_curveParts++;
			}
			else
			{
#if FE_SAHE_VERBOSE
				feLog("    type %d\n",part_info.type);
#endif
			}

			SurfaceAccessibleI::Spec spec;

			for(I32 rateIndex=0;rateIndex<4;rateIndex++)
			{
				const HAPI_AttributeOwner owner=
						(rateIndex==0)? HAPI_ATTROWNER_POINT:
						((rateIndex==1)? HAPI_ATTROWNER_VERTEX:
						((rateIndex==2)? HAPI_ATTROWNER_PRIM:
						HAPI_ATTROWNER_DETAIL));

				const I32 attrCount=part_info.attributeCounts[owner];
				HAPI_StringHandle attribute_names_array[attrCount];

				result=HAPI_GetAttributeNames(&m_hapiSession,child_id,part_id,
						owner,attribute_names_array,attrCount);
				if(result!=HAPI_RESULT_SUCCESS)
				{
					feLog("SurfaceAccessibleHapi::load"
							" get attributes failed on %d (%d/%d)\n  \"%s\"\n",
							child_id,nodeIndex,nodeCount,
							lastError(&m_hapiSession).c_str());
				}

#if FE_SAHE_VERBOSE
				feLog("    attributes %d\n",attrCount);
#endif

				for(I32 attrIndex=0;attrIndex<attrCount;attrIndex++)
				{
					result=HAPI_GetString(&m_hapiSession,
							attribute_names_array[attrIndex],string_value,length);
					if(result!=HAPI_RESULT_SUCCESS)
					{
						feLog("SurfaceAccessibleHapi::load"
								" get name failed on %d node %d/%d attr %d/%d\n"
								"  \"%s\"\n",
								child_id,nodeIndex,nodeCount,
								attrIndex,attrCount,
								lastError(&m_hapiSession).c_str());
					}

					const String attrName(string_value);
					HAPI_AttributeInfo attr_info;

					result=HAPI_GetAttributeInfo(&m_hapiSession,m_hapiNodeId,
							part_id,attrName.c_str(),owner,&attr_info);
					if(result!=HAPI_RESULT_SUCCESS)
					{
						feLog("SurfaceTrianglesHapi::cache"
								" get attr info failed on %d part %d/%d\n"
								"  \"%s\"\n",
								m_hapiNodeId,partIndex,partCount,
								SurfaceAccessibleHapi::lastError(
								&m_hapiSession).c_str());
					}

					if(!attr_info.exists)
					{
						continue;
					}

					// HAPI_STORAGETYPE_INVALID
					// HAPI_STORAGETYPE_INT
					// HAPI_STORAGETYPE_INT64
					// HAPI_STORAGETYPE_FLOAT
					// HAPI_STORAGETYPE_FLOAT64
					// HAPI_STORAGETYPE_STRING
					// HAPI_STORAGETYPE_MAX
					const HAPI_StorageType storage=attr_info.storage;

					const I32 tupleSize=attr_info.tupleSize;

					String attrType;
					if(storage==HAPI_STORAGETYPE_STRING && tupleSize==1)
					{
						attrType="string";
					}
					else if(storage==HAPI_STORAGETYPE_INT && tupleSize==1)
					{
						attrType="integer";
					}
					else if(storage==HAPI_STORAGETYPE_FLOAT && tupleSize==1)
					{
						attrType="real";
					}
					else if(storage==HAPI_STORAGETYPE_FLOAT && tupleSize==3)
					{
						attrType="vector3";
					}

					if(!attrType.empty())
					{
						spec.set(attrName,attrType);

						switch(rateIndex)
						{
							case 0:
								m_pointSpecs.push_back(spec);
								break;

							case 1:
								m_vertexSpecs.push_back(spec);
								break;

							case 2:
								m_primitiveSpecs.push_back(spec);
								break;

							case 3:
								m_detailSpecs.push_back(spec);
								break;
						}
					}

#if FE_SAHE_VERBOSE
					feLog("      rate %d %d/%d \"%s\" \"%s\"\n",
							rateIndex,attrIndex,attrCount,
							attrName.c_str(),attrType.c_str());
					feLog("        storage %d count %d tupleSize %d\n",
							storage,attr_info.count,tupleSize);
#endif
				}
			}
		}

		/*

		HAPI_ObjectInfo object_info;
		result=HAPI_GetObjectInfo(&m_hapiSession,child_id,&object_info);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessibleHapi::load"
					" get object info failed on %d\n  \"%s\"\n",
					child_id,lastError(&m_hapiSession).c_str());
		}

		*/
	}

	/*

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load cleanup\n");
#endif

	result=HAPI_Cleanup(&m_hapiSession);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load cleanup failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load close session\n");
#endif

	result=HAPI_CloseSession(&m_hapiSession);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessibleHapi::load close session failed\n  \"%s\"\n",
				lastError(&m_hapiSession).c_str());
	}

	*/

#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::load done\n");
#endif

	return TRUE;
}

sp<SurfaceI> SurfaceAccessibleHapi::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
#if FE_SAHE_DEBUG
	feLog("SurfaceAccessibleHapi::surface bound %d\n",isBound());
#endif

	if(!isBound())
	{
		return sp<SurfaceI>(NULL);
	}

	feLog("SurfaceAccessibleHapi::surface curveParts %d meshParts %d\n",
			m_curveParts,m_meshParts);

	sp<SurfaceI> spSurface;

	if(m_meshParts && !(a_restrictions&SurfaceI::e_excludePolygons))
	{
		spSurface=registry()->create("SurfaceI.SurfaceTrianglesHapi");
		if(spSurface.isValid())
		{
			sp<SurfaceTrianglesHapi> spSurfaceTrianglesHapi=spSurface;
			if(spSurface.isValid())
			{
				spSurfaceTrianglesHapi->setSession(&m_hapiSession);
				spSurfaceTrianglesHapi->setNodeId(m_hapiNodeId);
			}
		}
	}
	else
	{
		//* TODO check for point cloud and use SurfacePointsAccessible

		spSurface=registry()->create("SurfaceI.*.*.SurfaceCurvesAccessible");

		sp<SurfaceCurvesAccessible> spSurfaceCurvesAccessible=spSurface;
		if(spSurfaceCurvesAccessible.isValid())
		{
			spSurfaceCurvesAccessible->bind(sp<SurfaceAccessibleI>(this));
		}
	}

	if(spSurface.isValid())
	{
		spSurface->setRestrictions(a_restrictions);
	}

	return spSurface;
}

String SurfaceAccessibleHapi::lastError(HAPI_Session* a_pHapiSession)
{
    int buffer_length;
    HAPI_GetStatusStringBufLength(a_pHapiSession,HAPI_STATUS_CALL_RESULT,
//			HAPI_STATUSVERBOSITY_ERRORS,
			HAPI_STATUSVERBOSITY_ALL,
			&buffer_length);

    char* buffer=new char[buffer_length];
    HAPI_GetStatusString(a_pHapiSession,HAPI_STATUS_CALL_RESULT,
			buffer,buffer_length);

    String result(buffer);
    delete[] buffer;
    return result;
}


} /* namespace ext */
} /* namespace fe */
