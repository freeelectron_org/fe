/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <window/window.pmh>

//* see https://stackoverflow.com/questions/421860/capture-characters-from-standard-input-without-waiting-for-enter-to-be-pressed

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64

#include <conio.h>

#else

#include <stdio.h>
#include <sys/ioctl.h> // For FIONREAD
#include <termios.h>
#include <stdbool.h>

#endif

#define FE_KEY_DEBUG	FALSE
#define FE_KEY_VERBOSE	FALSE
#define FE_KEY_LOG		FALSE

namespace fe
{
namespace ext
{

ConsoleKeys::ConsoleKeys(void)
{
}

void ConsoleKeys::initialize(void)
{
}

ConsoleKeys::~ConsoleKeys(void)
{
}

void ConsoleKeys::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_spSignalerI=spSignalerI;

	sp<Scope> spScope=spLayout->scope();
	m_keyEvent.bind(spScope);
	m_keyRecord=m_keyEvent.createRecord();

	m_keyEvent.setSIS(WindowEvent::e_sourceKeyboard,WindowEvent::e_itemNull,
			WindowEvent::e_statePress);
	m_keyEvent.setState2(WindowEvent::e_state2NoRelease);
}

void ConsoleKeys::handle(Record &record)
{
	m_event.bind(record);

#if FE_KEY_DEBUG
	if(m_event.isKeyPress(WindowEvent::e_itemAny))
	{
		feLog("ConsoleKeys::handle %s\n",c_print(m_event));
	}
#endif

	if(m_event.isPoll())
	{
#if FE_KEY_DEBUG
		feLog("ConsoleKeys::handle %s\n",c_print(m_event));
#endif

		//* WARNING possibly reentrant
		poll();
	}
}

I32 ConsoleKeys::keysPending(void)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	return kbhit();
#else
	static bool initflag=false;

	if(!initflag)
	{
		// Use termios to turn off line buffering
		struct termios term;
		tcgetattr(STDIN_FILENO, &term);
		term.c_lflag &= ~ICANON;
		term.c_cc[VTIME] = 0;
		tcsetattr(STDIN_FILENO, TCSANOW, &term);
		setbuf(stdin, NULL);
		initflag=true;
	}

	I32 nbbytes;
	ioctl(STDIN_FILENO, FIONREAD, &nbbytes);
	return nbbytes;
#endif
}

void ConsoleKeys::poll(void)
{
	//setbuf(stdout, NULL); // Optional: No buffering.
	//setbuf(stdin, NULL);  // Optional: No buffering.
	if(!keysPending())
	{
#if FE_KEY_DEBUG
		feLog("ConsoleKeys::poll no key\n");
#endif
		return;
	}

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	const char c = _getch();
#else
	const char c = getchar();
#endif

#if FE_KEY_DEBUG
	feLog("ConsoleKeys::poll key 0x%2x '%c'\n",c,c);
#endif

	m_keyEvent.setItem(WindowEvent::Item(c));

#if FE_KEY_LOG
	feLog("ConsoleKeys::poll signal %s\n",c_print(m_keyEvent));
#endif

	m_spSignalerI->signal(m_keyRecord);
}

} /* namespace ext */
} /* namespace fe */
