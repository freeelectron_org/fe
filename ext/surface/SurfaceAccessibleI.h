/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleI_h__
#define __surface_SurfaceAccessibleI_h__

namespace fe
{
namespace ext
{

class SurfaceAccessorI;

/**************************************************************************//**
	@brief Surface Element Access and Alteration

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleI:
	virtual public Component,
	public CastableAs<SurfaceAccessibleI>
{
	public:
		enum	Element
				{
					e_point,
					e_pointGroup,
					e_vertex,
					e_primitive,
					e_primitiveGroup,
					e_detail,
					e_elementEnums
				};
		enum	Attribute
				{
					e_generic,
					e_position,
					e_normal,
					e_uv,
					e_color,
					e_vertices,
					e_properties
					// TODO common attributes
				};
		enum	Properties
				{
					e_openCurve,
					e_countU,
					e_countV,
					e_wrappedU,
					e_wrappedV,
					e_depth
				};
		enum	Form
				{
					e_closed,
					e_open
				};
		enum	Message
				{
					e_quiet,
					e_warning,
					e_error
				};
		enum	Creation
				{
					e_refuseMissing,
					e_createMissing
				};
		enum	Writable
				{
					e_readOnly,
					e_readWrite
				};
				//* e_unpaged or e_paged
		enum	AtomicChange
				{
					e_pointsOnly,
					e_pointsOfPrimitives,
					e_primitivesOnly,
					e_primitivesWithPoints	//* TODO
				};

				//* duplicate from WorkI
		enum	Threading
				{
					e_unknown=WorkI::e_unknown,
					e_singleThread=WorkI::e_singleThread,
					e_multiThread=WorkI::e_multiThread
				};

	/// @brief view to a reduced set in indices
	class FE_DL_EXPORT FilterI:
		virtual public Counted,
		public CastableAs<FilterI>
	{
		public:
	virtual	I32		filterCount(void) const									=0;
	virtual	I32		filter(I32 a_filterIndex) const							=0;
	};

	//* TODO not pure -> move out
	class Spec
	{
		public:
							Spec(void)										{}

			void			set(String a_name,String a_typeName)
							{
								m_name=a_name;
								m_typeName=a_typeName;
							}
	const	String&			name(void) const		{ return m_name; }
	const	String&			typeName(void) const	{ return m_typeName; }

			bool			operator==(const Spec& a_rOtherSpec) const
							{	return (a_rOtherSpec.m_name==m_name &&
										a_rOtherSpec.m_typeName==m_typeName); }

		private:
			String			m_name;
			String			m_typeName;
	};

	//* TODO not pure -> move out
	class ThreadingState: public Counted
	{
		public:
											ThreadingState(void):
												m_threading(e_singleThread)	{}
			void							set(SurfaceAccessibleI::Threading
													a_threading)
											{	m_threading=a_threading; }
			SurfaceAccessibleI::Threading	threading(void) const
											{	return m_threading; }
		private:
			SurfaceAccessibleI::Threading	m_threading;
	};

								/// @brief Return true if surface is available
virtual	BWORD					isBound(void)								=0;
								/// @brief associate with model data
virtual	void					bind(Instance a_instance)					=0;
								/// @brief choose a data scope
virtual	void					bind(sp<Scope> a_scope)						=0;
								/// @brief Inform threading intentions
virtual	void					bind(sp<SurfaceAccessibleI::ThreadingState>
										a_spThreadingState)					=0;

								/// @brief load model data from file
virtual	BWORD					load(String a_filename)						=0;
								/// @brief load model data with settings
virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings)			=0;
								/// @brief save model data to file
virtual	BWORD					save(String a_filename)						=0;
								/// @brief save model data with settings
virtual	BWORD					save(String a_filename,
										sp<Catalog> a_spSettings)			=0;

								/** @brief Indicate whether an implementation
									can be accessed from multiple threads. */
virtual	BWORD					threadable(void)							=0;

								/// @brief Start a thread-unsafe action
virtual	void					lock(void)									=0;
virtual	void					lock(I64 a_id)								=0;
								/// @brief Finish a thread-unsafe action
virtual	void					unlock(void)								=0;
virtual	void					unlock(I64 a_id)							=0;

virtual void					groupNames(Array<String>& a_rNameArray,
										SurfaceAccessibleI::Element
										a_element) const					=0;
virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										SurfaceAccessibleI::Element
										a_element) const					=0;
virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_nodeName,
										SurfaceAccessibleI::Element
										a_element) const					=0;

virtual	void					clear(void)									=0;

virtual	I32						count(String a_nodeName,
										SurfaceAccessibleI::Element
										a_element) const					=0;
virtual	I32						count(SurfaceAccessibleI::Element
										a_element) const					=0;

								/// @brief Return access to an attribute
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable a_writable
										)									=0;
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable a_writable
										)									=0;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create)
																			=0;
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create)
																			=0;

virtual sp<SurfaceAccessorI>	accessor(SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create
										)									=0;
virtual sp<SurfaceAccessorI>	accessor(SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create
										)									=0;

virtual sp<SurfaceAccessorI>	accessor(SurfaceAccessibleI::Element a_element,
										String a_name)						=0;
virtual sp<SurfaceAccessorI>	accessor(SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute)						=0;

								/** @brief Remove attributes/groups
											for entire surface

									The pattern argument is space delimited
									regex patterns. */
virtual	I32						discardPattern(
										SurfaceAccessibleI::Element a_element,
										String a_pattern)					=0;
virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name)						=0;
virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute)						=0;

								/** @brief Remove elements from surface

									The group string argument is space delimited
									regex patterns of groups and/or
									numerical ranges (like "2-5 17").

									If a_retainGroups is true,
									remove all elements not in the groups. */
virtual	I32						deleteElements(
										SurfaceAccessibleI::Element a_element,
										String a_groupString,
										BWORD a_retainGroups)				=0;
virtual	I32						deleteElements(
										SurfaceAccessibleI::Element a_element,
										std::set<I32>& a_rIntSet,
										BWORD a_retainGroups)				=0;

								/// @brief Return current underlying SurfaceI
virtual	sp<SurfaceI>			surface(void)								=0;
virtual	sp<SurfaceI>			surface(String a_group)						=0;
virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions
										a_restrictions)						=0;

virtual	sp<SurfaceI>			subSurface(U32 a_subIndex)					=0;
virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group)	=0;
virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group,
										SurfaceI::Restrictions
										a_restrictions)						=0;

virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible)				=0;
virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										String a_nodeName)					=0;
virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										sp<FilterI> a_spFilter)				=0;

virtual	void					append(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										const SpatialTransform*
										a_pTransform)						=0;

virtual	void					append(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										String a_nodeName,
										const Array<SpatialTransform>&
										a_rTransformArray,
										sp<FilterI> a_spFilter)				=0;

virtual	void					instance(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										const Array<SpatialTransform>&
										a_rTransformArray)					=0;

virtual	sp<SpannedRange>		atomize(SurfaceAccessibleI::AtomicChange
										a_atomicChange,String a_group,
										U32 a_desiredCount)					=0;

virtual	sp<SpannedRange>		atomizeConnectivity(
										SurfaceAccessibleI::AtomicChange
										a_atomicChange,String a_group,
										U32 a_desiredCount,
										BWORD a_checkPages)					=0;

virtual void					setPaging(BWORD a_paging)					=0;
virtual BWORD					paging(void) const							=0;

virtual void					preparePaging(SurfaceAccessibleI::AtomicChange
										a_atomicChange,String a_group)		=0;
virtual I32						pointPage(U32 a_pointIndex) const			=0;
virtual I32						primitivePage(U32 a_primitiveIndex) const	=0;

virtual BWORD					copyOutline(Array<String>&
										a_rStringArray) const				=0;

virtual	sp<Component>			payload(void)								=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleI_h__ */
