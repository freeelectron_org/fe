import sys
import os
import re
forge = sys.modules["forge"]

def setup(module):
    module.summary = []

    version_h = "/usr/include/graphviz/graphviz_version.h"
    version = ""
    if not os.path.exists(version_h):
        version_h = ""

    if version_h != "":
        for line in open(version_h).readlines():
            if re.match("#define PACKAGE_VERSION.*", line):
                version = line.split()[2].rstrip().lstrip('"').rstrip('"')

    if version != "":
        module.summary += [ version ]

    srcList = [ "graphviz.pmh",
                "GraphDot",
                "graphvizDL" ]

    dll = module.DLL( "fexGraphVizDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["imagelibs"] = "-lgvc"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        dll.linkmap["imagelibs"] = "???"

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexImageDLLib" ]

    forge.deps( ["fexGraphVizDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexGraphVizDL",            None,       None) ]

def auto(module):
    test_file = """
#include <graphviz/gvc.h>
//#include <graphviz/graph.h>     // permit old version (not ready for new)

int main(void)
{
    return(0);
}
    \n"""

    gvmap = { "gfxlibs": forge.gfxlibs }
    if forge.fe_os == "FE_LINUX":
        gvmap["imagelibs"] = "-lgvc"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        gvmap["imagelibs"] = "???"

    return forge.cctest(test_file, gvmap)
