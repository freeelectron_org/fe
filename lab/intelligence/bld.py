import sys
forge = sys.modules["forge"]

def prerequisites():
    return [    "architecture",
                "audio",
                "terrain",
                "viewer" ]

def setup(module):

    srcList = [ "intelligence.pmh",
                "Attract",
                "Collect",
                "Communicate",
                "Construct",
                "Control",
                "Detonate",
                "Director",
                "Discard",
                "Dominate",
                "DrawConstruct",
                "DrawParticle",
                "DrawRadio",
                "DrawScan",
                "DrawShape",
                "DrawTargeting",
                "DrawThrust",
                "FactionFilter",
                "LuaBehavior",
                "Scan",
                "Shoot",
                "Sing",
                "Thrust",
                "Tick",
                "intelligenceDL" ]

    dll = module.DLL( "fexIntelligenceDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    forge.deps( ["fexIntelligenceDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexIntelligenceDL",            None,       None) ]

    module.Module( 'test' )
