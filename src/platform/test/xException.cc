/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

#include <stdexcept>

void test_segv(void)
{
	int* pInt=NULL;
	*pInt=5;
}

void test_function(void)
{
//	feLogDirect("provoking seg fault\n");
//	test_segv();

//	feLogDirect("throwing std::runtime_error\n");
//	throw std::runtime_error("test error");

	feLogDirect("throwing fe::Exception\n");
	feX("test_function","Exception test (expected)");
}

int main(void)
{
	fe::UnitTest unitTest;
	BWORD tried=FALSE;
	BWORD excepted=FALSE;
	BWORD completed=FALSE;

	feLogDirect("doing manual backtrace\n");
	feLogBacktrace();

	feLogDirect("register segv handler\n");
	feRegisterSegvHandler();
	feLogDirect("reregister segv handler\n");
	feRegisterSegvHandler();

	try
	{
		tried=TRUE;
		test_function();
		completed=TRUE;
	}
	catch(fe::Exception &e)
	{
		excepted=TRUE;

		feLogDirect("caught fe::Exception\n");
		e.log();

#if FALSE
		//* test terminate handler for uncaught exception (unit test will fail)
		feLogDirect("rethrow exception\n");
		throw;
#endif

		feLogDirect("tests complete\n");
	}

#if FALSE
	//* test terminate handler without exception (unit test will fail)
	feLogDirect("call terminate\n");
	std::terminate();
#endif

	unitTest(tried);
	unitTest(excepted);
	unitTest(!completed);

	unitTest.track(3);
	return unitTest.failures();
}

