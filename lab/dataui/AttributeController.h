/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_AttributeController_h__
#define __dataui_AttributeController_h__

#include <dataui/dataui.pmh>
namespace fe
{
namespace ext
{

class FE_DL_EXPORT AttributeController :
	public Initialize<AttributeController>,
	virtual public Config,
	virtual public Mask,
	virtual public HandlerI
{
	public:
				AttributeController(void);
virtual			~AttributeController(void);

		void	initialize(void);

virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig);
virtual void	handle(	Record &r_sig);

		void	addAttribute(const String &a_name);
		void	addUpdate(sp<RecordGroup> spUpdate);
		void	addOutput(sp<RecordGroup> spOutput);

	private:
virtual void	handleCallback(Record &r_sig);
virtual void	handleUpdate(Record &r_sig);
		class Label
		{
			public:
				String		m_name;
		};
		AsStringEntry					m_asStringEntry;
		AsSignal						m_asSignal;
		AsWindata						m_asWindata;
		AsCallback						m_asCallback;
		sp<Layout>						m_l_callback;
		sp<Layout>						m_l_callback_sig;
		sp<Layout>						m_l_stringentry;
		WindowEvent::Mask				m_start;
		WindowEvent::Mask				m_end;
		WindowEvent::Mask				m_update;
		sp<BaseType>					m_spStringType;
		std::vector<Label>				m_attrs;
		Accessor<Record>				m_aSE;
		Accessor<Record>				m_aTargetRecord;
		Accessor<String>				m_aTargetAttr;
		Accessor<int>					m_aTargetIndex;
		AsGeneric						m_asGeneric;
		StringAccessor					m_stringer;
		sp<RecordGroup>					m_active;
		std::vector< sp<RecordGroup> >	m_outputs;
		std::vector< sp<RecordGroup> >	m_updates;
		bool							m_live;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_AttributeController_h__ */

