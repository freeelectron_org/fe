import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexYamlDLLib" ]

    tests = [   'xReadCatalogYaml',
                'xReadManifestYaml',
                'xCatalogWriterYaml' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xCatalogWriterYaml.exe",  "output/test/yaml/xCatalogWriterYaml.yaml", None,None) ]

    forge.tests += [
        ("xReadCatalogYaml.exe",    "ext/yaml/test/catalog.yaml",   None,None) ]
