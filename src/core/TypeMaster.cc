/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

TypeMaster::TypeMaster(void):
	m_name("TypeMaster")
{
}

TypeMaster::~TypeMaster(void)
{
#if FALSE
	for(type_string_map::iterator it = m_hTypeMap.begin();
			it != m_hTypeMap.end(); it++)
	{
		feLog("has type \"%s\"\n",it->second.c_str());
		const fe_type_info& rInfo=it->first->typeinfo().ref();
		feLog("  name \"%s\"\n",rInfo.name());
	}
#endif
}

sp<BaseType> TypeMaster::lookupNameLoaded(const String &name)
{
	SAFEGUARDCLASS;

	sp<BaseType> spType;

	string_type_map::iterator it = m_hNameMap.find(name);
	if(it != m_hNameMap.end())
	{
		spType = it->second;
	}
	return spType;
}

sp<BaseType> TypeMaster::lookupName(const String &name)
{
	SAFEGUARDCLASS;

	sp<BaseType> spType=lookupNameLoaded(name);
	if(spType.isValid())
	{
		return spType;
	}

	if(successful(loadType(name)))
	{
		string_type_map::iterator it = m_hNameMap.find(name);
		if(it != m_hNameMap.end())
		{
			spType = it->second;
		}
	}

	return spType;
}

String TypeMaster::lookupBaseType(const sp<BaseType>& type)
{
	SAFEGUARDCLASS;
	type_string_map::iterator it = m_hTypeMap.find(type);
	String spName;
	if(it != m_hTypeMap.end())
	{
		spName = it->second;
	}

	return spName;
}

void TypeMaster::dumpTypes(void) const
{
	for(type_info_map::const_iterator t_it = m_hTypeidMap.begin();
			t_it != m_hTypeidMap.end();
			t_it++)
	{
		feLog("%s\n", t_it->first.ref().name());
	}
}

sp<BaseType> TypeMaster::lookupType(const TypeInfo &typeInfo)
{
	SAFEGUARDCLASS;

#if FALSE
	feLog("TypeMaster::lookupType %s\n",typeInfo.ref().name());
	dumpTypes();
#endif

	type_info_map::iterator it = m_hTypeidMap.find(typeInfo);
	if(it != m_hTypeidMap.end())
	{
		return it->second;
	}

	if(successful(loadType(typeInfo.ref().name())))
	{
		it = m_hTypeidMap.find(typeInfo);
		if(it != m_hTypeidMap.end())
		{
			return it->second;
		}
	}

	feLog("TypeMaster::lookupType find [%s]\n",typeInfo.ref().name());
	for(it = m_hTypeidMap.begin(); it != m_hTypeidMap.end(); it++)
	{
		feLog("  matched %d [%s]\n", it->first==typeInfo,
				it->first.ref().name());
	}
	feLog("TypeMaster::lookupType not found: %s\n", typeInfo.ref().name());
	sp<BaseType> spType;
	return spType;
}

Result TypeMaster::addTypeLoader(sp<TypeLoader> a_spTypeLoader)
{
	if(a_spTypeLoader.isNull())
	{
		return e_invalidHandle;
	}
	m_typeLoaderArray.push_back(a_spTypeLoader);
	return e_ok;
}

Result TypeMaster::loadType(String a_name)
{
//	feLog("TypeMaster::loadType \"%s\"\n",a_name.c_str());

	const I32 loaderCount=m_typeLoaderArray.size();
	for(I32 loaderIndex=0;loaderIndex<loaderCount;loaderIndex++)
	{
		sp<TypeLoader>& rspTypeLoader=m_typeLoaderArray[loaderIndex];
		if(rspTypeLoader.isNull())
		{
			continue;
		}
		Result result=rspTypeLoader->loadType(a_name);
		if(successful(result))
		{
			return result;
		}
	}

	return e_cannotFind;
}

void TypeMaster::peek(Peeker &peeker)
{
	SAFEGUARDCLASS;
	peeker.nl();
	peeker.cat("known types:");
	peeker.in();
	for(	type_info_map::iterator t_it = m_hTypeidMap.begin();
			t_it != m_hTypeidMap.end();
			t_it++)
	{
		peeker.nl();
		peeker.str().catf("%-30s", t_it->first.ref().name());
		String tnames;
		for(	string_type_map::iterator n_it = m_hNameMap.begin();
				n_it != m_hNameMap.end();
				n_it++)
		{
			if(n_it->second == t_it->second)
			{
				peeker.bold();
				peeker.str().catf(" %s", n_it->first.c_str());
				peeker.normal();
			}
		}
	}
	peeker.out();

	peeker.nl();
	peeker.cat("conversions:");
	peeker.in();
	for(	in_out_converter_map::iterator io_it = m_hConverter.begin();
			io_it != m_hConverter.end();
			io_it++)
	{
		for(	out_converter_map::iterator o_it = io_it->second.begin();
				o_it != io_it->second.end();
				o_it++)
		{
			String sIn, sOut;
			for(	type_info_map::iterator t_it = m_hTypeidMap.begin();
					t_it != m_hTypeidMap.end();
					t_it++)
			{
				if(t_it->second == io_it->first)
				{	sIn = t_it->first.ref().name(); }
				if(t_it->second == o_it->first)
				{	sOut = t_it->first.ref().name(); }
			}
			peeker.nl();
			peeker.str().catf("in:%-10s out:%-10s", sIn.c_str(), sOut.c_str());
			if(o_it->second.isValid())
			{
			}
			else
			{
				peeker.bold();
				peeker.str().cat(" invalid");
				peeker.normal();
			}
		}
	}
	peeker.out();
}


void TypeMaster::registerConversion(sp<TypeConversion> spConverter,
		sp<BaseType> spInType, sp<BaseType> spOutType)
{
	SAFEGUARDCLASS;
	sp<TypeConversion> existing=lookupConversion(spInType,spOutType);
	if(existing.isValid())
	{
		return;
	}

	m_hConverter[spInType][spOutType] = spConverter;
}


sp<TypeConversion> TypeMaster::lookupConversion(sp<BaseType> spInType,
		sp<BaseType> spOutType)
{
	SAFEGUARDCLASS;
	return m_hConverter[spInType][spOutType];
}

void TypeMaster::reverseLookup(sp<BaseType> basetype, std::list<String> &names)
{
	SAFEGUARDCLASS;
	for(string_type_map::iterator it = m_hNameMap.begin();
			it != m_hNameMap.end(); it++)
	{
		if(it->second == basetype)
		{
			names.push_back(it->first);
		}
	}
}

String TypeMaster::reverseLookup(sp<BaseType> basetype)
{
	String namelist;
	std::list<String> names;
	reverseLookup(basetype, names);
	for(std::list<String>::iterator i_name = names.begin();
		i_name != names.end(); i_name++)
	{
		if(i_name != names.begin())
		{
			namelist.cat(" ");
		}
		namelist.cat(*i_name);
	}
	return namelist;
}

sp<BaseType> TypeMaster::assertTypeInternal(const TypeInfo &typeInfo,
		sp<BaseType> spT, const String &name)
{
	SAFEGUARDCLASS;
	type_info_map::iterator it = m_hTypeidMap.find(typeInfo);
	// did not find type
	if(it == m_hTypeidMap.end())
	{
		// but name already used
		if(this->lookupNameLoaded(name).isValid())
		{
			feX("fe::TypeMaster::assertType",
				"type name mismatch on %s", name.c_str());
		}
		m_hTypeidMap[typeInfo] = spT;
		m_hNameMap[name] = spT;
		m_hTypeMap[spT] = name;
		return spT;
	}
	// found type
	else
	{
		sp<BaseType> spType = it->second;
		// and the name exists
		sp<BaseType> namedType = this->lookupNameLoaded(name);
		if(namedType.isValid())
		{
			// but is not the same type (should now be impossible)
			if(namedType != spType)
			{
				feX("fe::TypeMaster::assertType",
					"(impossibility) type name mismatch on %s", name.c_str());
			}
		}
		// name does not exist; add it
		else
		{
			m_hNameMap[name] = spType;
			m_hTypeMap[spT] = name;
		}
		return spType;
	}
}

}
