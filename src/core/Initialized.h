/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Initialized_h__
#define __core_Initialized_h__

namespace fe
{

#define	FE_INIT_DEBUG	FALSE

/**************************************************************************//**
	@brief Base class providing collective initialization (post-constructor)

	@ingroup core

	This mechanism allows you to supply multiple initialization functions
	in a class hierarchy, all of which are called.  This operation is
	similar to construction and is intended to supply objects with the ability
	to do initialization that is not safe or possible during construction.

	This mechanism works for multiple inheritance and virtual inheritance.
	In order to follow the same call order as the constructors, any base
	classes that may contain initialize functions should precede the
	Initialize template in the inheritance specification.

	To participate, any derived class @em T supplying an @em initialize method
	needs to also derive from Initialize <T>.

	@code
class A: public Initialize<A>
{
	public:
virtual			~A(void)			{}
		void	initialize(void)	{ feLog("initialize A\n");
};

class B: public A, public Initialize<B>
{
	public:
virtual			~B(void)			{}
		void	initialize(void)	{ feLog("initialize B\n");
};

int main(int argc,char** argv,char** env)
{
	B b;
	b.initialize();		// just calls B::initialize
	b.initializeAll();	// calls A::initialize and B::initialize
}
	@endcode
*//***************************************************************************/
class Initialized: virtual public Castable
{
	public:
				Initialized(void):
					m_active(true)
				{
#if FE_INIT_DEBUG
					feLog("Initialized::Initialized %p\n",this);
#endif

#if FE_CODEGEN>FE_DEBUG && FE_COMPILER==FE_GNU
					// HACK revisit and retry later
					// gnu optimized can try to eliminate constructor entirely

					// something trivial to con compiler to keep constructor
					SystemTicker t; t.timeMS();
#endif
				}
virtual			~Initialized(void)
				{
#if FE_INIT_DEBUG
					feLog("Initialized::~Initialized %p\n",this);
#endif
				}

		void	initializeAll(void)
				{
#if FE_INIT_DEBUG
					feLog("Initialized::initializeAll %p %d %d\n",
							this,m_active,m_initializers.size());
#endif
					if(!m_active)
					{
						return;
					}
					const int size=m_initializers.size();
					for(int m=0;m<size;m++)
					{
						m_initializers[m](this);
					}
				}

		void	setActive(bool a_active) { m_active = a_active; }

	protected:

typedef void	(*InitializeFunction)(Initialized*);

		void	addInitializer(InitializeFunction function)
				{	m_initializers.push_back(function); }

	private:

		Array<InitializeFunction>		m_initializers;
		bool							m_active;
};

/**************************************************************************//**
	@brief Per-class participation in the Initialized <> mechanism
*//***************************************************************************/
template<class T>
class Initialize: public CastableAs<T>, virtual public Initialized
{
	public:
				Initialize(void)
				{
#if FE_INIT_DEBUG
					feLog("Initialize::Initialize %p \"%s\"\n",
							(T*)this,FE_TYPESTRING(T).c_str());
#endif
					addInitializer(&Initialize<T>::static_initialize);
				}

	private:

static	void	static_initialize(Initialized* obj)
				{
#if FE_INIT_DEBUG
					feLog("Initialize::static_initialize %p \"%s\"\n",
							obj,FE_TYPESTRING(T).c_str());
#endif
					fe_cast<T>(obj)->initialize();
				}
};

} // namespace

#endif // __core_Initialized_h__
