/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaHandler_h__
#define __lua_LuaHandler_h__

namespace fe
{
namespace ext
{

/** lua execution component.  Can also operate as a handler.

	Similar to LuaExec except that on handle() the lua function
	handle is called instead of the whole script.  This allows for
	initialization to be done by the script at some preceding time (via
	calling execute()).

	@copydoc LuaHandler_info
	*/
class LuaHandler : public Initialize<LuaHandler>,
	virtual public Dispatch,
	virtual public HandlerI,
	virtual public LuaI
{
	public:
						LuaHandler(void);
virtual					~LuaHandler(void);

		void			initialize(void);

		// AS LuaI
virtual	bool			preloadString(const String &text);
virtual	Result			loadFile(const String &filename);
virtual	Result			loadString(const String &text);

virtual	void			set(const String &key, Record r_value);
virtual	void			set(const String &key, sp<RecordArray> ra_value);
virtual	void			set(const String &key, const String &value);
virtual	void			set(const String &key, const Real &value);
virtual	void			set(const String &key, Vector2f &value);
virtual	void			set(const String &key, Vector2d &value);
virtual	void			set(const String &key, Vector3f &value);
virtual	void			set(const String &key, Vector3d &value);
virtual	void			set(const String &key, Vector4f &value);
virtual	void			set(const String &key, Vector4d &value);
virtual	void			set(const String &key, sp<Layout> l_value);
virtual	void			set(const String &key, sp<Component> spValue);
virtual	void			set(const String &key, sp<Scope> spValue);
virtual	void			set(const String &key, sp<StateCatalog> spValue);

virtual	Record			getRecord(const String &key);
virtual	String			getString(const String &key);
virtual	Real			getReal(const String &key);
virtual	Vector2f		getVector2f(const String &key);
virtual	Vector2d		getVector2d(const String &key);
virtual	Vector3f		getVector3f(const String &key);
virtual	Vector3d		getVector3d(const String &key);
virtual	Vector4f		getVector4f(const String &key);
virtual	Vector4d		getVector4d(const String &key);

virtual	bool			execute(void);
virtual	bool			execute(const String &a_fnName);
virtual	void			flush(void);
virtual	void			alias(String a_aliasName,String a_trueName);

		// AS HandlerI
virtual	void			handle(Record& r_sig);

		using Dispatch::call;

		// AS DispatchI
virtual	bool			call(const String &a_name, Array<Instance>& a_argv);

	private:
		Result			compile(void);

		lua_State				*m_pLuaState;
		String					m_preload;
		String					m_chunk;
		String					m_source;
		sp<LuaContext>			m_spContext;
		String					m_name;
#if FE_COUNTED_TRACK
		Array<void*>			m_trackArray;
#endif
};

} /* namespace ext */
} /* namespace fe */


#endif /* __lua_LuaHandler_h__ */

