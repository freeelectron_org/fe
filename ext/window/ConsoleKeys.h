/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __sdl_ConsoleKeys_h__
#define __sdl_ConsoleKeys_h__

#include "window/WindowEvent.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief WindowEvent handler that watches the console for key presses

	@ingroup sdl
*//***************************************************************************/
class FE_DL_EXPORT ConsoleKeys:
	virtual public KeyI,
	public Initialize<ConsoleKeys>
{
	public:
				ConsoleKeys(void);
virtual			~ConsoleKeys(void);

		void	initialize(void);

				//* As HandlerI
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void	handle(Record &record);

virtual	BWORD	generatesReleaseEvents(void)	{ return FALSE; }

	private:

		I32		keysPending(void);
		void	poll(void);

		sp<SignalerI>		m_spSignalerI;

		WindowEvent			m_event;
		WindowEvent			m_keyEvent;
		Record				m_keyRecord;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __sdl_ConsoleKeys_h__ */
