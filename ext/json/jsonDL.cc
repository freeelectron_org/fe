/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSurfaceDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<CatalogReaderJson>(
			"CatalogReaderI.CatalogReaderJson.fe.json");

	pLibrary->add<CatalogWriterJson>(
			"CatalogWriterI.CatalogWriterJson.fe.json");

	pLibrary->add<StringFilterJson>(
			"StringFilterI.StringFilterJson.fe.json");

	pLibrary->add<SurfaceAccessibleGeo>(
			"SurfaceAccessibleI.SurfaceAccessibleGeo.fe.geo");

	pLibrary->add<fe::ext::data::JsonStream>(
			"StreamI.JsonStream.fe.json");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
