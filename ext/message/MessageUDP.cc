/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#include "message/message.pmh"
#include "message/osheader.h"
#include "message/MessageUDP.h"
#include "message/NetUtil.h"
#include <sstream>

#define FE_MESSAGE_UDP_VERBOSE FALSE

// Port 0 is a standard for selecting a random open port
#define RANDOM_PORT_VALUE 0

namespace fe
{
namespace ext
{

MessageUDP::MessageUDP()
{
	m_socket = SOCKET(0);
}

MessageUDP::~MessageUDP()
{
	// clean up
	shutdown();
}

void MessageUDP::shutdown()
{
	if(m_socket)
	{
		closesocket(m_socket);
		m_socket = SOCKET(0);
	}
}

bool MessageUDP::postInit()
{
	NetUtil &nu = NetUtil::Instance();

	if(!nu.IsNetworkInit())
	{
		feLog("MessageUDP::postInit network initialization error\n");
		return false;
	}

	m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

#if FE_MESSAGE_UDP_VERBOSE
	feLog("MessageUDP::postInit created socket %d\n", m_socket);
#endif

	if(m_socket == INVALID_SOCKET)
	{
		feLog("MessageUDP::postInit socket open error\n");
		return false;
	}

	return true;
}


bool MessageUDP::setReceiveBufferSize(int bufferSize)
{
	int recBufferSize = bufferSize;
	int recBuffeSizeof = sizeof(int);
	int result = setsockopt(m_socket, SOL_SOCKET, SO_RCVBUF,
			(char*)&recBufferSize, recBuffeSizeof);
	if(result == SOCKET_ERROR)
	{
		feLog("MessageUDP::setReceiveBufferSize socket open error\n");
		return false;
	}

	return true;
}


bool MessageUDP::setSendBufferSize(int bufferSize)
{
	int recBufferSize = bufferSize;
	int recBuffeSizeof = sizeof(int);
	int result = setsockopt(m_socket, SOL_SOCKET, SO_SNDBUF,
			(char*)&recBufferSize, recBuffeSizeof);
	if(result == SOCKET_ERROR)
	{
		feLog("MessageUDP::setSendBufferSize socket open error\n");
		return false;
	}

	return true;
}

bool MessageUDP::setBroadcast(bool broadcast)
{
	int Param = broadcast ? 1 : 0;
	int result = setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR | SO_BROADCAST,
			(char*)&Param,sizeof(Param));
	if(result == SOCKET_ERROR)
	{
		feLog("MessageUDP::setBroadcast socket open error\n");
		return false;
	}

	return true;
}

bool MessageUDP::setReusePort(bool reuse)
{
#ifdef _WIN32
	return false;
#else
	int Param = reuse ? 1 : 0;
	int result = setsockopt(m_socket, SOL_SOCKET, SO_REUSEPORT,
			(char*)&Param,sizeof(Param));
	if(result == SOCKET_ERROR)
	{
		feLog("MessageUDP::setReusePort socket open error\n");
		return false;
	}
	return true;
#endif
}


bool MessageUDP::setReuseAddress(bool reuse)
{
	int Param = reuse ? 1 : 0;
	int result = setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR,
			(char*)&Param,sizeof(Param));
	if(result == SOCKET_ERROR)
	{
		feLog("MessageUDP::setReuseAddress socket open error\n");
		return false;
	}

	return true;
}

bool MessageUDP::recvFrom(Messagegram *m)
{
	sockaddr_in addr;
	bool ret = receiveFrom(reinterpret_cast<char *>(m->data),
			&(m->dataLen), MESSAGEGRAM_MAXDATA, &addr, 0);

	if(!ret)
	{
		return false;
	}

	m_replyAddr = MessageAddress(addr);

	return true;
}

bool MessageUDP::recvFrom(Messagegram *m, long seconds, long uSeconds)
{
	sockaddr_in addr;
	bool ret = receiveFrom(reinterpret_cast<char *>(m->data),
			&(m->dataLen), MESSAGEGRAM_MAXDATA, &addr, seconds, uSeconds, 0);

	if(!ret)
	{
		return false;
	}

	m_replyAddr = MessageAddress(addr);

	return true;
}


void MessageUDP::getRecvAddress(uint32_t &address)
{
	struct sockaddr_in addr_in = m_replyAddr.getNetAddress();
	address = addr_in.sin_addr.s_addr;
}


void MessageUDP::convertAddressToStr(const uint32_t address, char *addStr)
{
	inet_ntop(AF_INET, &address, addStr, INET_ADDRSTRLEN);
}


bool MessageUDP::recvLargeFrom(Messagegram *m)
{
	sockaddr_in addr;
	bool ret = receiveLargeFrom(reinterpret_cast<char *>(m->data),
			&(m->dataLen), MESSAGE_UDP_MAXDATA, &addr, 0);

	if(!ret)
	{
		return false;
	}

	m_replyAddr = MessageAddress(addr);

	return true;
}

bool MessageUDP::replyLast(const Messagegram &m)
{
	if(!m_replyAddr.isValid())
	{
		feLog("MessageUDP::replyLast no valid reply addr\n");
		return false;
	}

	sockaddr_in replyAddr = m_replyAddr.getNetAddress();
	return sendTo(replyAddr, reinterpret_cast<const char *>(m.data), m.dataLen);
}

bool MessageUDP::sendTo(const Messagegram &m, const char *host, uint16_t port)
{
	MessageAddress ma(host, port);

	if(m.dataLen > MESSAGEGRAM_MAXDATA)
	{
		feLog("MessageUDP::sendTo message payload exceeds max data size\n");
		return false;
	}

	sockaddr_in addr = ma.getNetAddress();
	return sendTo(addr, reinterpret_cast<const char *>(m.data), m.dataLen);
}

bool MessageUDP::sendLargeTo(const Messagegram &m,
	const char *host, uint16_t port)
{
	MessageAddress ma(host, port);

	if(m.dataLen > MESSAGE_UDP_MAXDATA)
	{
		feLog("MessageUDP::sendLargeTo"
				" message payload exceeds max data size\n");
		return false;
	}

	sockaddr_in addr = ma.getNetAddress();
	return sendTo(addr, reinterpret_cast<const char *>(m.data), m.dataLen);
}


bool MessageUDP::bind(const char *host, uint16_t port)
{
	MessageAddress ma(host, port);
	return bindTo(ma.getHost().c_str(), ma.getPort());
}


bool MessageUDP::bind(uint16_t port)
{
	return bindTo(port);
}

uint16_t MessageUDP::bindRandom()
{
	int tryCount = 0;

	while(!bindTo(RANDOM_PORT_VALUE))
	{
		feLog("MessageUDP::bindRandom random bind failed trying again\n");
		tryCount++;
		if(tryCount > MAX_RANDOM_BIND_TRIES)
		{
			return 0;
		}
	}

	struct sockaddr_in addr;

	socklen_t len = sizeof(addr);
	getsockname(m_socket, (struct sockaddr *) &addr, &len);
	uint16_t port = ntohs(addr.sin_port);

	return port;
}

bool MessageUDP::receiveFrom(char *mesg, uint16_t *bytesRecv, uint16_t maxLen,
	sockaddr_in *recvFrom, long seconds, long uSeconds, int flags)
{
	// Setup timeval variable
    timeval timeout;
    timeout.tv_sec = seconds;
    timeout.tv_usec = uSeconds;

    // Setup fd_set structure
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(m_socket, &fds);

    //  -1: error occurred
    //   0: timed out
    // > 0: data ready to be read
	if(select(m_socket+1, &fds, 0,0, &timeout) <= 0)
	{
		return false;
	}
	else
	{
		return receiveFrom(mesg, bytesRecv, maxLen, recvFrom, flags);
	}
}

bool MessageUDP::receiveFrom(char *mesg, uint16_t *bytesRecv,
	uint16_t maxLen, sockaddr_in *addr, int flags)
{
	socklen_t addrSize = sizeof(*addr);
	memset(addr,0,addrSize);

	int ret = recvfrom(m_socket, mesg, maxLen, flags,
			reinterpret_cast<SOCKADDR *>(addr), &addrSize);

	if(ret > MESSAGEGRAM_MAXDATA)
	{
		feLog("MessageUDP::receiveFrom recvfrom max payload size exceeded\n");
		return false;
	}

	if(ret == SOCKET_ERROR)
	{
		feLog("MessageUDP::receiveFrom recvfrom socket error\n");
		return false;
	}

	*bytesRecv = ret;

#if FE_MESSAGE_UDP_VERBOSE
	char ip[256];
	uint16_t port;
	memset(ip, 0, 256);
	inet_ntop(AF_INET, &(*addr).sin_addr, ip, 256);
	port = ntohs(addr->sin_port);
	feLog("MessageUDP::receiveFrom \"%s:%d\"\n", ip, port);
#endif

	return true;
}

bool MessageUDP::receiveLargeFrom(char *mesg, uint16_t *bytesRecv,
	uint16_t maxLen, sockaddr_in *addr, int flags)
{
	socklen_t addrSize = sizeof(*addr);
	int ret = recvfrom(m_socket, mesg, maxLen, flags,
			reinterpret_cast<SOCKADDR *>(addr), &addrSize);

	if(ret > MESSAGE_UDP_MAXDATA)
	{
		feLog("MessageUDP::receiveLargeFrom"
				" recvfrom max payload size exceeded\n");
		return false;
	}

	if(ret == SOCKET_ERROR)
	{
		feLog("MessageUDP::receiveLargeFrom recvfrom socket error\n");
		return false;
	}

	*bytesRecv = ret;

#if FE_MESSAGE_UDP_VERBOSE
	char ip[256];
	uint16_t port;
	memset(ip, 0, 256);
	inet_ntop(AF_INET, &(*addr).sin_addr, ip, 256);
	port = ntohs(addr->sin_port);
	feLog("MessageUDP::receiveLargeFrom \"%s:%d\"\n", ip, port);
#endif

	return true;
}

bool MessageUDP::sendTo(sockaddr_in &addr, const char *mesg,
	uint16_t mesgLen, int flags)
{
	int ret = sendto(m_socket, mesg, mesgLen, flags,
			reinterpret_cast<SOCKADDR *>(&addr), sizeof(addr));

	if(ret == SOCKET_ERROR)
	{
		feLog("MessageUDP::sendto socket error\n");
		return false;
	}

	return true;
}

bool MessageUDP::bindTo(const char *host, unsigned short port)
{
#if FE_MESSAGE_UDP_VERBOSE
	feLog("MessageUDP::bindTo Binding to host %s port %d\n",host,port);
#endif

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(host);
	addr.sin_port = htons(port);

	int ret = ::bind(m_socket,
			reinterpret_cast<SOCKADDR *>(&addr), sizeof(addr));

	if(ret == SOCKET_ERROR)
	{
		feLog("MessageUDP::bindTo socket error\n");
#ifdef _WIN32
		feLog("Code: %d\n", WSAGetLastError());
		feLog("SockVal: %d\n", m_socket);
#endif
		return false;
	}

	return true;
}

bool MessageUDP::bindTo(unsigned short port)
{
#if FE_MESSAGE_UDP_VERBOSE
	std::string msg;

	if(port == RANDOM_PORT_VALUE)
	{
		msg = "a random port";
	}
	else {
		std::stringstream pmsg;
		pmsg << "port " << port;
		msg = pmsg.str();
	}
	feLog("MessageUDP::bindTo Binding %s\n", msg.c_str());
#endif

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);

	int ret = ::bind(m_socket,
			reinterpret_cast<SOCKADDR *>(&addr), sizeof(addr));

	if(ret == SOCKET_ERROR)
	{
		feLog("MessageUDP::bindTo socket error\n");
#ifdef _WIN32
		feLog("Code: %d\n", WSAGetLastError());
		feLog("SockVal: %d\n", m_socket);
#endif
		return false;
	}

#if FE_MESSAGE_UDP_VERBOSE
	socklen_t len = sizeof(addr);
	char ip[256];
	memset(ip, 0, 256);
	getsockname(m_socket, (struct sockaddr*)&addr, &len);
	inet_ntop(AF_INET, &addr.sin_addr, ip, 256);
	uint16_t boundport = ntohs(addr.sin_port);

	feLog("MessageUDP::bindTo successfully bound to %s:%d\n", ip, boundport);
#endif

	return true;
}

} /* namespace ext */
} /* namespace fe */
