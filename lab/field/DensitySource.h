/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __field_DensitySource_h__
#define __field_DensitySource_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "math/math.h"
#include "shape/shape.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT DensitySource :
	virtual public HandlerI,
	virtual public Config
{
	public:
				DensitySource(void);
virtual			~DensitySource(void);

		// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsParticle					m_asParticle;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __field_DensitySource_h__ */

