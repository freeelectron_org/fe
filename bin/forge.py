#!/usr/bin/python3

r"""
forge is a python based build mechanism that is partially inspired by
build.exe (www.imonk.com) and partially inspired by scons (www.scons.org).

All of forge is implemented in the single forge.py file, which is to say
both the executable and the module.

The primary goals of forge are:
- easy to write build description files
- portability
- powerful, as in the power of a full language at hand
- simple building blocks without a lot of usage restrictions or assumptions

Execution Environment and Configuration Files
=============================================
To build a configured project using forge, run forge.py from any directory
within the project.  Forge searches up the directory path to find the
root.py file.  The root.py file is the main project file, so there should
be only one root.py per project.  Use the -f flag to override the name
root.py.

The root.py may import modules with the setup() method which does a
specialized python 'import' to bring in build modules.  The setup function
will look for a 'bld.py' in the specified module.  The module subdirectory
is derived from the module name (and visa versa such as when modules are
imported from other modules).  The method setup(module) within the bld.py
file is called upon import.

To access forge as a python module from a build module, do so by finding it
in the sys provided modules dictionary:
forge = sys.modules["forge"]

Primary targets are specified on the command line as TargetRegistry entry
names (see Target Registry below).

To check if a target is up-to-date it must be at least as new as some
'reference time'.  This reference time is specifed to the build function,
but is also modified by dependencies.  Therefore, the reference time will
be the newest of all dependencies and the specified reference time.

The default specified reference time to primary targets on the command line
is 0 (in other words, very long ago).  Therefore, the default build mode
is to make sure primary targets are as new as their dependencies only.
The -x flag sets the specified reference time to the current time which
will have the effect of rebuilding almost everything.  This is often
preferable to an explicitly driven full rebuild via `forge.py clean root`,
since 'clean' is more prone to missing something (because forge has no
way of implicitly knowing what all transient targets are; clean uses
pattern matching to get what it gets).

Platform Abstraction Variables
==============================
Core forge feature platform abstraction is provided through variables.
These are setup by the initialize() function.  Currently, only Win32/VC++
and Linuc/g++ are built-in.  To provide a new platform simply reassign
these variables.

Targets and the Dependency Graph
================================
Targets are python objects representing some entity that has the concept
of being 'up to date'.  Typically, a target represents a file.

Dependencies represent the relationship between targets that if one target
is dependent on another target it must be at least as new as that target.

Currently, the dependency graph is built from scratch every time forge
is executed.  So far this has not been a significant performance issue.
(If it becomes an issue, graph caching to file is a reasonably easy fix.  This
is in effect what typical 'make dep' does by appending to makefiles.).

Dependencies are added with Target::AddDep(), Target::AddDeps(), dep(),
or deps().  dep() and deps() now do the same thing, which they now both
handle list AND non-list arguments.  They both also handle Target objects
in additional to the previously supported string arguments.

Configuration files may add dependencies explicitly.  Also, dependencies
are added automatically via several mechanisms:
- Included C/C++ file scanner; recursively scans header dependencies.
- Build file automatic additions
Of course, automatic dependency addition is not restricted to included
methods.

Target Registry and Target Dictionary
-------------------------------------
All targets are also inserted into the target dictionary, keyed off
of their target names.  This dictionary is used for global operations,
such as the C/C++ header dependency scanner.

Targets may also be added to the target registry which is not necessarily
keyed off of target names.  Forge employs naming conventions on the
registry to facilitate useful cross-module dependency specification.
Some conventions:
- ROOT_BUILD_FILE: the main root.py file
- <module name>.buildfile: a particular module's build file
- <module name>: the module target
- <Module::Exe() specified name>Exe: executable target
- <Module::Lib() specified name>Lib: library target
- <Module::DLL() specified name>Lib: library target
- clean: clean target
- print: print target
- root: root module target

Note that Libs and DLLs collide in Registry namespace.  This is for several
reasons.  First, cross-module dependencies are easier to specify in an
project where Libs and DLLs may be switched.  Second, in Win32 there is
an IMPLIB 'Lib' for every 'DLL', so having a true static 'Lib' and a 'DLL' at
the same time is problematic.  This is of course all overridable.

The target registry is also used to look up primary targets from the
command line.  So to build just a particular executable you may run:
forge.py particularExe

Rules and Resolving Dependencies
================================
A rule is a regular expresion and an associated python function with the
prototype:
def rule(target)

Rule Contexts
-------------
A rule context is a collection of rules.  The three significant contexts
are the pre-processing context (preContext), the build
context (depContext), and the post-processing context (postContext).
The depContext can be overridden on a per target basis by simply assigning
the data member 'depContext' of a target.

The default depContext (defaultContext) currently includes some
reasonable default rules for Win32 and Linux C/C++ projects.

A rule is executed against a target if the target name is matched
by the regular expression of the rule.  When the matching is checked
depends upon the context (see Target::Build()).

For example, to add a rule to print a message every time a win32 or linux
object file is built you might have:
def print_obj(target):
    print('building: ' + target.name)
forge.defaultContext.AppendRule(r"(.*)\.(obj|o)$", print_obj)

Target::Build(reference_time), the heart of forge
-------------------------------------------------
(see Target::Build())

Some Useful Forge Variables
===========================

executable command line maps
----------------------------
These dictionaries are used to assemble command lines for compiling and
linking.  These are global and per target dictionaries.  A module
dictionary entry overrides a global dictionary entry.  All existing keys are
used.

includemap -  Header file include directories
              includeScanSuppress is a list of includemap keys to suppress
              the dependency scan for.  This is useful to remove the overhead
              of maintaining dependencies on large unchanging third party
              packages.
cppmap -      Compiler command line flags
linkmap -     Linker command line flags

others
------
preContext -  The pre-processing rule context
depContext -  The main build rule context.
postContext - The post-processing rule context.
rootPath -    The absolute path to the project root directory.
libPath -     Used by the module convienience methods, Exe, Lib, and DLL to
              place the targets.  Also added to library search path.
objPath -     Used by automatic object file target generation function,
              ObjAndSrcTargets(), to create object file targets.  Note that
              the relative path structure is preserved (so for objPath=obj
              and target root/module/a.cpp, target root/obj/module/a.o will
              be added)
binPath -     Not used by forge directly; potentially used by root.py.


root.py guide
=============

bld.py guide
============

A Super Simple Example
======================

root build file (root.py)
-------------------------
import sys
forge = sys.modules["forge"]
forge.rootModule.Exe("HelloWorld")


A More Interesting Example
==========================

the desired dependency graph
----------------------------

                root
                /|\
         ------- | --------
        /        |         \\
  moduleLib  common.h    moduleExe
   /    \   /       \     /    \\
  /      \ /         \   /      \\
a.cc -> a.h <------- b.h <--- b.cc

root build file (root.py)
-------------------------
import sys

forge = sys.modules["forge"]

forge.includemap['common_include'] = '.'
forge.includemap['lib_include'] = 'moduleLib'

module_list = [ 'moduleLib',
                'moduleExe' ]

for mod in module_list:
    forge.setup(mod)

moduleLib build file (moduleLib/bld.py)
---------------------------------------
import sys

forge = sys.modules["forge"]

def setup(module):
    module.Lib("My", ["a"])

moduleExe build file (moduleExe/bld.py)
---------------------------------------
import sys

forge = sys.modules["forge"]

def setup(module):
    exe = module.Exe("My", ["b"])
    forge.dep("MyExe", "MyLib")


"""













































__author__ = "Andrew J. Weber <orang@imonk.com>"
__date__ = "3 September 2003"

# -----------------------------------------------------------------------------
# General imports needed for both module and main modes.
# NOTE: eventually, that distinction should dissolve to become nearly
# non-existent (at least as far as imports are concerned).

import os
import sys
import json
import errno
import re
import subprocess
import textwrap
import traceback

# Necessary for importing utility.
sys.path.insert(1, os.path.join(sys.path[0], '..'))

# NOTE: The utility module enforces a python version check at import time.  So
# we import it as soon as possible, which causes an immediate abort if the
# check fails.  This is more implicit than is desired, but we can't directly
# import the version check method, since doing so just causes the module to
# experience import processing in full, so there's no way to isolate the check
# ourselves -- we just defer to the module doing it for us.  This could/would
# change if forge becomes a proper package in the future.
import utility

# -----------------------------------------------------------------------------
# Module constants & defaults:

cpp_11=0
ccache=0
distcc=0
fig2dev=0
ENCODING_DEFAULT = 'utf-8'
PRETTY_DEFAULT = 2

# -----------------------------------------------------------------------------
# regex patterns

RE_FILENAME_PMH = re.compile(r'(.*)\.pmh$')

# -----------------------------------------------------------------------------
# Setup some basic logging assistance.

from utility import clogger
clog = clogger()
clog.pretty = PRETTY_DEFAULT

# Crafting a little func we can use with the clog decorator.
# It needs to exist in this module, and run globals() *IN* this module in order
# for the context to be what's intended.
@clogger.pretty_inject(lambda: globals())
def plog(*args, **kwargs):
    clog.plog(*args, **kwargs)


###############################################################################
# AS THE DRIVER
###############################################################################
if __name__ == '__main__':
    import time
    import shutil

    # TEMP workaround for RecursionError associated with ScanIncludes.
    sys.setrecursionlimit(10**6)

    import forge

    forge.reset_buildtime()

    clog.info('\nconfiguring')

    # -------------------------------------------------------------------------
    # Anything done within initialize() becomes potentially immune to the
    # layered env cfg file ingestion, because that ingestion logic treats the
    # starting os.environ as sacrosanct, refusing to overwrite any var whose
    # key already exists in os.environ. So unless the same key in os.environ,
    # or any directly assigned property on forge, are explicitly assigned a
    # different value after the env file ingestion, then whatever occurs in
    # initialize() is effectively permanent and (critically) was never exposed
    # to or affected by anything in the cumulative set of env files.

    forge.initialize()

    # -------------------------------------------------------------------------
    # auto_env represents key:var pairs that participate in the (potential)
    # override during ingestion of the layered env file ingestion. Note that if
    # the same key exists in os.environ already, then *that* value will survive
    # as authoritative afterwards, as anything in auto_env (as well as anything
    # encountered in any env file) is considered subordinate to the starting
    # os.environ.

    auto_env = {}
    auto_env["FE_API_CODEGEN"] = forge.apiCodegen
    auto_env["FE_COMPACT_API_CODEGEN"] = forge.compactApiCodegen
    if not "FE_LIB" in os.environ:
        auto_env["FE_LIB"] = forge.rootPath

    # Note, however, that because auto_env is being seeded with things that
    # were likely set in initialize(), and initialize() itself is entirely
    # uninfluenced by anything in the env files, that it's highly likely that
    # what is put into auto_env here remains unchanged and ends up in
    # os.environ, despite anything encountered in any of the env files. The
    # only circumstance where something in auto_env mutates into a different
    # value as the dust settles (eventually) into a 'final' state of
    # os.environ, is if the key is encountered in an env file, AND the key was
    # NOT initially in os.environ, implying that the corresponding forge.*
    # attribute that got crafted in initialize() did not simply pass through
    # from a matching key which initialize() used. In other words, only if the
    # forge attribute was derivative from other ingredients (which themselves
    # may or may not have derived from the original os.environ), and so the key
    # in auto_env is one that could easily be overridden within an env file
    # without being implicilty 'protected' by starting conditions.

    (env, forge.modset_roots) = utility.applyLocalEnvFiles(
        forge.rootPath, auto_env)

    # We need to give anything in forge that should be eligible for influence
    # by any encountered env file the opportunity for that to occur *AFTER* the
    # full ingestion of the layered env files (into the distilled state of
    # os.environ). Because of the nature of initialize(), this has to occur
    # after that, and obviously after applyLocalEnvFiles(), and ideally
    # *BEFORE* as much as possible of everything else that can occur, so that a
    # maximum amount of forge behavior is capable of being configured through
    # the layered env file feature.

    forge.reinitialize()

    # -------------------------------------------------------------------------
    # Establish product list.

    forge.product = {}
    forge.build_product = "default"

    if "FE_PRODUCT" in os.environ:
        forge.build_product = os.environ["FE_PRODUCT"]

    forge.read_products()
    # -------------------------------------------------------------------------

    forge.post_initialize()

    rootPath = forge.rootPath
    lastPath = ""
    haveRoot = 0
    while rootPath != lastPath:
        lastPath = rootPath
        if os.path.isfile(os.path.join(rootPath,forge.rootFile)):
            forge.SetRootPath(rootPath)
            haveRoot = 1
            break
        rootPath = os.path.dirname(rootPath)

    # make deferral convienience
    if not haveRoot:
        forge.cprint(forge.RED,1,'no root.py found')
        if os.path.isfile('makefile') or os.path.isfile('Makefile'):
            forge.cprint(forge.RED,1,'found makefile, deferring to make')
            command = 'make'
            for s in sys.argv[1:]:
                if s == 'root':
                    s = 'all'
                if s == '-x':
                    s = 'clean'
                command = command + ' ' + s
            os.system(command)
            sys.exit(0)

    # if root.py defers to another root
    if forge.path_to_root != "":
        rootPath = os.path.join(os.getcwd(),forge.path_to_root)
        rootPath = os.path.abspath(rootPath)
        forge.SetRootPath(rootPath)

    # import configuration
    config_name_list = ""
    for c in forge.configModules:
        try:
            utility.import_from(c, forge.rootPath)
        except:
            pass

    # Ensure immediate import processing of the located root file. Failure will
    # abort the session.
    utility.import_file_as_module(
        'forge.root', os.path.join(rootPath, 'root.py'), tolerant=False)

    # Reluctantly delayed help handling until this point.
    # This is still super broke. There's a nasty catch-22 that has crept in,
    # where the help text can get hacked by other modules, so even when we
    # notice that the user asked for help, we can't emit it and early-exit
    # (like a well-behaved app) until after all kinds of non-help-related
    # execution occurs, like... all of the two icebergs lurking inside root.py
    # and common.py that happens at import time (and shouldn't). So, we'd try
    # to do the help behavior here, BUT! .. turns out some of what root&common
    # do is so pervasive that they decide conditions warrant a complete session
    # abort and they raw call sys.exit(), so we never even get to this spot.
    # As is, there's basically no usable help behavior at all. It can either
    # occur earlier and be incomplete, or it can pretend to occur here but odds
    # are that it won't actually happen because a basic `forge.py -h` (with
    # default env) will early abort before root/common let us continue.

    # TODO: fix. with fire.
    if forge.print_help == 1:
        print(forge.UsageHelp())
        sys.exit(0)

    if forge.ccache != 0:
        ccache_dir=os.getenv('CCACHE_DIR')
        if ccache_dir == None:
            libPath=os.getenv('FE_LIB_PATH')
            if libPath != None:
                ccache_dir = libPath + '/ccache'
                if forge.pretty < 2:
                    forge.cprint(forge.CYAN,0,'using ccache "'+ccache_dir+'" under FE_LIB_PATH')
            else:
                ccache_dir = 'lib/ccache'
                if forge.pretty < 2:
                    forge.cprint(forge.CYAN,0,'using ccache "'+ccache_dir+'" by default')
            forge.cxx_pre = 'CCACHE_DIR="' + ccache_dir + '" ' + forge.cxx_pre
        elif forge.pretty < 2:
            forge.cprint(forge.CYAN,0,'using ccache "'+ccache_dir+'" from CCACHE_DIR')
    elif forge.pretty < 2:
        forge.color_on(0, forge.YELLOW)
        sys.stdout.write('not using ccache')
        forge.color_on(0, forge.CYAN)
        sys.stdout.write(' (not installed)\n')
        forge.color_off()

    if forge.pretty < 2:
        if forge.distcc != 0:
            forge.cprint(forge.CYAN,0,'using distcc "' + os.environ["DISTCC_HOSTS"] + '"')
        else:
            forge.color_on(0, forge.YELLOW)
            sys.stdout.write('not using distcc')
            forge.color_on(0, forge.CYAN)
            sys.stdout.write(' (not installed or DISTCC_HOSTS not set)\n')
            forge.color_off()

        if forge.fig2dev != 0:
            forge.cprint(forge.CYAN,0,'using fig2dev')
        else:
            forge.color_on(0, forge.YELLOW)
            sys.stdout.write('not using fig2dev\n')
            forge.color_off()

    # post hook mechanism
    for (k,v) in forge.targetDictionary.items():
        if v.post_hook != None:
            v.post_hook(v)

    if os.getenv('FE_CONFIG_ONLY') == "1":
        forge.cprint(forge.MAGENTA,0,'building config only')
    else:
        forge.ScanAllTargets()

        if not forge.failed:
            for arg in forge.args:
                match = forge.MatchTarget(arg)
                forge.buildNum += 1
                if match != None:
                    match.CycleCheck()
                    match.Build(forge.referenceTime,"ROOT")
                else:
                    # scan for a ShortName match
                    have_target = False
                    for (k,target) in forge.targetDictionary.items():
                        if isinstance(target,forge.FileTarget):
                            if target.ShortName() == arg:
                                forge.buildNum += 1
                                target.CycleCheck()
                                target.Build(forge.referenceTime,"ROOT")
                                have_target = True
                    if not have_target:
                        forge.cprint(forge.RED,0,'WARNING: unknown target: ' + arg)

        forge.cprint(forge.CYAN,1,'targets built')

        if forge.hotreload_mode:
            forge.hotreload_manifest.Write()

        if forge.do_plot:
            forge.process_logging()

        if forge.printCommon != 0:
            keylist = list(forge.targetRegistry.keys())
            keylist.sort()
            for k in keylist:
                try:
                    forge.targetRegistry[k].help
                    forge.cprint(forge.WHITE,0,k)
                    print(forge.targetRegistry[k].help)
                except AttributeError:
                    pass

        if forge.printRegistry != 0:
            keylist = list(forge.targetRegistry.keys())
            keylist.sort()
            for k in keylist:
                print(k)

        if forge.printDictionary != 0:
            keylist = list(forge.targetDictionary.keys())
            keylist.sort()
            for k in keylist:
                print(k)

        if forge.pretty < 2:
            forge.cprint(forge.CYAN, 0, "Per Module Timings:")
            for k in forge.buildTimes.keys():
                print("%-50s %5.2f" % (k, forge.buildTimes[k]))

        forge.cprint(forge.CYAN, 0, forge.get_formatted_elapsedtime())

    if "FE_COPY_BINARIES_TO_PATH" in os.environ:
        copy_binaries_to_path = utility.safer_eval(os.environ["FE_COPY_BINARIES_TO_PATH"])

        for dest_dir in copy_binaries_to_path.keys():
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir)
                forge.cprint(forge.YELLOW,0,'created directory "%s"' % dest_dir)
            forge.cprint(forge.CYAN, 0, "copying binaries to \"%s\"" % dest_dir)

            src_pattern = re.compile(copy_binaries_to_path[dest_dir])

            src_dir = os.path.join("lib", forge.apiCodegen)
            dir_list = os.listdir(src_dir)

            binary_count = 0
            for binary in dir_list:
#               print("BINARY %s" % binary)
                if src_pattern.match(binary):
                    binary_path = os.path.join(src_dir, binary)
#                   print("  PATH %s" % binary_path)
                    if os.path.isdir(binary_path):
                        dest_path = os.path.join(dest_dir, binary)

                        # dirs_exist_ok requires python 3.8
#                       shutil.copytree(binary_path, dest_path, dirs_exist_ok=False)

                        # TEMP only copies directories to one level
                        if not os.path.isdir(dest_path):
                            os.makedirs(dest_path)
                        files = os.listdir(binary_path)
                        for filename in files:
                            try:
                                shutil.copy(os.path.join(binary_path, filename), dest_path)
                                binary_count = binary_count + 1
                            except PermissionError:
                                forge.cprint(forge.YELLOW,0,'No write permissions, skipping "%s" (it\'s probably a loaded library)' % filename)
                    else:
                        try:
                            shutil.copy(binary_path,dest_dir)
                            binary_count = binary_count + 1
                        except PermissionError:
                            forge.cprint(forge.YELLOW,0,'No write permissions, skipping "%s" (it\'s probably a loaded library)' % binary)

            forge.cprint(forge.CYAN, 0, "  %d binaries copied" % binary_count)

    forge.modal_quit('forge complete', 0)




###############################################################################
# AS A MODULE
###############################################################################
else:
    import getopt
    import string
    import stat
    import time
    import shutil
    import copy
    import random
    import types
#   import popen2
    import threading
    import platform
    import subprocess
#   import shlex
    import fnmatch
    import pathlib

    help = []
    print_help = 0
    failed = 0
    do_auto = 1
    objlists = {}
    logging = []
    do_plot = 0
    collapse_obj = 0
    collapse_hdr = 0
    collapse_exe = 0
    collapse_lib = 0

    utility.replace_symlink_on_ms()

    ###########################################################################
    def HandleCommandLine(argv):
        """process the command line"""
        global codegen, recursionDepth, printRegistry, referenceTime, pretty
        global printDictionary, rootFile, printCommon, jobs, blddep, notify
        global rootPath, configModules, print_help, do_auto, do_plot
        global collapse_obj, collapse_hdr, collapse_exe, collapse_lib
        try:
                opts, args = getopt.getopt(sys.argv[1:], "xhnmkCctdobr:p:f:F:g:j:", [
                "config=",
                "codegen=",
                "recurse=",
                "auto=",
                "dot=",
                "pretty="])
        except getopt.GetoptError:
            cprint(RED,0,'ERROR: invalid command line')
            print(UsageHelp())
            sys.exit(2)
        for o, a in opts:
            if o == "-h":
                print_help = 1
            if o == "-n":
                notify = 1
            if o == "-m":
                print(__doc__)
                sys.exit(0)
            if o == "-V":
                codegen = 'validate'
            if o == "-d":
                codegen = 'debug'
            if o == "-o":
                codegen = 'optimize'
            if o == "-b":
                blddep = 0
            if o == "-F":
                configModules.append(a)
            if o == "--codegen":
                codegen = a
            if o == "--recurse":
                recursionDepth = int(a)
            if o == "-r":
                recursionDepth = int(a)
            if o == "-f":
                rootFile = a
            if o == "-g":
                rootPath = a
            if o == "-j":
                jobs = int(a)
            if o == "-t":
                printCommon = 1
            if o == "-k":
                printRegistry = 1
            if o == "-c":
                printDictionary = 1
            if o == "-x":
                referenceTime = time.time()
            if o == "--auto":
                if int(a):
                    do_auto = 1
                else:
                    do_auto = 0
            if o in ['--pretty', '-p']:
                try:
                    clog.pretty = pretty = int(a)
                except ValueError:
                    clog.pretty = pretty = 0
            if o == "--dot":
                do_plot = 1
                for c in a:
                    if c == "O":
                        collapse_obj = 1
                    elif c == "H":
                        collapse_hdr = 1
                    elif c == "E":
                        collapse_exe = 1
                    elif c == "L":
                        collapse_lib = 1

        if not len(args):
            #args = ["root"]
            pass

        return args


    def UsageHelp():
        '''
        Depends on global help var.
        '''

        txt = textwrap.dedent(
        """
        USAGE: [python] ' + sys.argv[0] + ' [options] [targets]

        OPTIONS:
        -j <int>           run up to N jobs in parallel
        -n                 popup modal notify when forge completes
        -m                 print manual
        -d                 debug build
        -o                 optimized build
        -V                 validation build (slow debug)
        --codegen=<string> overrides CODEGEN environment variable
        --recurse=<int>    set build recursion depth limit
        --auto=<0/1>       do auto configuration processing
        -b                 suppress target dependence on bld files
        -r <int>           set build recursion depth limit
        -k                 print the target registry
        -t                 print documented command line targets
        -c                 print the target dictionary
        -x                 force a deep rebuild
        -f <string>        override root.py as root buildfile name
        -g <string>        explicitly set the rootPath
        --pretty=<int>     use formatted output (default: 2)
        -p                 --pretty
        --dot=<collapses>  output dependencies.dot dependency tree
                             H - collapse headers
                             O - collapse objects
                             L - collapse libraries
                             E - collapse executables

        BUILTIN TARGETS:
        clean              remove target files
        here               module for cwd or 'root' for root dir
        root               root target
        print              print stuff for targets

        ENVIRONMENT VARIABLES:
        CODEGEN            set code generation mode
                           debug, optimize, profile, validate have
                           default forge settings.  A project may
                           have more modes supported in its root.py
        PRETTY             sets pretty output mode
        AUTO               sets auto mode
        PYTHON             name of python executable for spawns

        COMMON EXAMPLES

          build everything from the root directory:
            python forge.py root

          rebuild everything from the root directory:
            python forge.py -x root

          cleaning libraries and object files just for a module
          from that module's directory:
            python forge.py -r2 clean here
        """)

        if help:
            txt = "{t}\n\nSupplemental help:\n{h}".format(
                t=txt, h='\n'.join(help))

        return txt

    ###########################################################################
    #
    def SetRootPath(path):
        """reset the root path"""
        global rootPath, rootModule
        rootPath = path
        os.environ["FE_ROOT"] = path
        targetRegistry['root'].AsRootReset()
        targetRegistry['ROOT_BUILD_FILE'] = FileTarget(
            os.path.join(rootPath,rootFile))

        rootModule = Module(None, "", "")
        targetRegistry['.buildfile'] = targetRegistry['ROOT_BUILD_FILE']

    ###########################################################################
    # convienience functions for build rules
    errno_tkmodal = 0
    def quit_callback():
        sys.exit(errno_tkmodal)

    def modal_quit(message, exit_code):
        if not notify:
            return
        import tkinter as tk
        root = tk.Tk()

        frame = tk.Frame(root, bg="black")
        frame.pack()

        w = tk.Label(frame, text=message, fg="white", bg="black",
            width=50, height=5)
        w.pack()

        button = tk.Button(frame, text="ok", bg="black", fg="red",
            relief=tk.FLAT, activebackground="black", activeforeground="green",
            command=quit_callback)
        button.pack()

        global errno_tkmodal
        errno_tkmodal = exit_code
        root.mainloop()

    def end_build(name):
        """color coded failed build"""
        if pretty == 0:
            print("failed build: " + name)
        elif pretty < 3:
            cprint(RED,1,"failed build: " + name)

        modal_quit("failed build: " + name, 1)

        if do_plot:
            process_logging()

        sys.exit(1)

    def confirm_path(target):
        (head,tail) = os.path.split(target.name)
        if not os.path.exists(head):
            try:
                os.makedirs(head)
            except OSError as e:
                if e.errno != 17:
                    print("failed creating " + head + " (" + e.strerror + ")")
        return tail

    def confirm_dl_path(target):
        (head,tail) = os.path.split(target.HotReloadableName())
        if not os.path.exists(head):
            try:
                os.makedirs(head)
            except OSError as e:
                if e.errno != 17:
                    print("failed creating " + head + " (" + e.strerror + ")")
        return tail

    class file_reader(threading.Thread):
        def __init__(self, file):
            threading.Thread.__init__(self)
            self.file = file
        def run(self):
            self.output = self.file.read()

    def remove_file(fullname, onlyIfExists = False):
        if not onlyIfExists or os.path.exists(fullname):
#           print("REMOVE '%s'" % fullname)
            os.remove(fullname)

    def cleanup_command(command):

        clean_command = command

        # ---------------------------------------------------------------------
        # Sanitizing how a direct python script is invoked.
        #
        # For full env consistency across any spawned processes, we want to not
        # inadvertently break out of the python sandbox in which the
        # originating build or forge command was executed. It might be a venv,
        # or the system python installation, doesn't matter - we want to stay
        # within the same one.
        #
        # This is accomplished by detecting whether the first component of the
        # provided command is a string with no whitespace that also ends in
        # '.py'.  If so, we prepend the command with the explicit python
        # executable that is hosting the process executing this function.
        # Thus, any command that might look like `foo.py a b c` is adjusted to
        # become `<full path to python interpretor> foo.py a b c`.

        parts = clean_command.split(maxsplit=1)

        # Note that split() with no sep argument will treat any whitespace as a
        # single separator. Also, specifying `maxsplit=1` ensures that we'll
        # retain any irregular whitespacing that was put into the full command.
        # Who knows, it could matter to something.

        if parts[0].endswith('.py'):
            clean_command = f'"{sys.executable}" {clean_command}'

        # ---------------------------------------------------------------------
        # The following clean-up action has been mothballed.

        # WARNING post-filter approach can't really be sure of delimiters
        # NOTE does not fix trailing space (indistiguishable from delimiter)

        #last_cleanup = ""
        #while last_cleanup != clean_command:
        #    last_cleanup = clean_command
        #
        #    # escape isolated spaces in paths
        #    clean_command = re.sub(r"([/ \w])((?:\\ )|/|\w|\s)\s(\w+)((?:\\ )|/)",r"\1\2\ \3\4",clean_command)
        #    clean_command = re.sub(r"((?:\\ )|/)(\w*) ((?:\w+[ ]?)*\w)/",r"\1\2\ \3/",clean_command)
        #
        #    # propagate escapes backwards through spaces
        #    clean_command = re.sub(r"([^\\]) \\ ",r"\1\ \ ",clean_command)

        # ---------------------------------------------------------------------

        if pretty == 0 and clean_command != command:
            print(f"CLEANUP:\n\t{clean_command}")

        return clean_command

    def execute(command,name):
        clean_command = cleanup_command(command)

        proc = subprocess.run(
            clean_command,
            # TODO: consider security implications of `shell=True`.
            # See: https://docs.python.org/3/library/subprocess.html#security-considerations
            shell=True,
            capture_output=True,
            encoding=ENCODING_DEFAULT,
        )

        rc=proc.returncode

        # apparently, Windows-only minimally-useful messages
        if (rc != 0 and pretty < 3) or pretty < 1:
            if proc.stdout:
                print(process_output(proc.stdout, name))

        # warnings and errors
        if (rc != 0 and pretty < 3) or pretty < 2:
            if proc.stderr:
                print(process_output(proc.stderr, name))

        if rc != 0:
            end_build(name)

    def execute_long(command,arguments,name):
        if sys.platform != "win32":
            return execute(" ".join((command,arguments)),name)
        if pretty == 0:
            print("Diverting Arguments to File")
#       argFilename = "tmp.lnk"
        argFilename = name + ".lnk"
        f = open(argFilename,"w")
        f.write(arguments)
        f.close()
        command = " ".join((command,"@" + argFilename))
        if pretty == 0:
            print(command)
        result = execute(command,name)
        remove_file(argFilename)
        return result

    def add_cpps(command, target):
        for d in target.deps:
            match = re.compile(r'(.*)\.(' + c_ext + ')$').match(d.name)
            if match != None:
                cppfile = match.group(0)
                if cppfile != None:
                    command = " ".join((command,forcecxx + ' ' + cppfile))
        return command

    def add_objs(command, target):
        for d in target.deps:
            match = re.compile(r'(.*)\.(o|obj|pch)$').match(d.name)
            if match != None:
                obj = match.group(0)
                if re.compile(r'(.*)\.pch$').match(d.name) != None:
                    obj = obj + ".obj"
                if obj != None:
                    command = " ".join((command,obj))
        return command

    def add_libs(command, target):
        for d in target.deps:
            match = re.compile(r'.*' + libpre + r'(.*)\.(so|a)$').match(d.name)
            if match != None:
                lib = match.group(1)
                if lib != None:
                    command = " ".join((command,linkformat%lib))
            match = re.compile(r'.*' + libpre + r'(.*)\.(dylib)$').match(d.name)
            if match != None:
                lib = match.group(1)
                if lib != None:
                    command = " ".join((command,'-l'+lib))
            match = re.compile(r'(.*)\.(dll|lib|pyd)$').match(d.name)
            if match != None:
                lib = match.group(1)
                if lib != None:
                    command = " ".join((command,lib+'.lib'))
        return command

    def add_cppflags(command, target):
        """build cppflags portion of command line"""
        #########################################################
        try :
            target.module.cppflags
            cprint(RED,1,"cppflags deprecated: " + target.name)
            sys.exit(2)
        except AttributeError:
            pass
        #########################################################
        try :
            target.cppflags
            cprint(RED,1,"cppflags deprecated: " + target.name)
            sys.exit(2)
        except AttributeError:
            pass
        #########################################################
        try :
            cppflags
            cprint(RED,1,"cppflags deprecated: " + target.name)
            sys.exit(2)
        except NameError:
            pass
        #########################################################
        #########################################################
        cppDict = copy.deepcopy(cppmap)
        try:
            target.module.cppmap
            for key in target.module.cppmap.keys():
                cppDict[key] = target.module.cppmap[key]
        except AttributeError:
            pass
        try:
            target.cppmap
            for key in target.cppmap.keys():
                cppDict[key] = target.cppmap[key]
        except AttributeError:
            pass
        if target.module != None:
            mod_parts = target.module.modName.split('.')
            if api == 'x86_win32' or api == 'x86_win64':
                cppDict['defmodule'] = '-DMODULE_' + mod_parts[-1]
            cppDict['definemodule'] = '-DMODULE=\\"%s\\"' % target.module.modName
        for key in cppDict.keys():
            #cprint(CYAN,1, 'CPPFLAG: ' + key + ' = ' + cppDict[key])
            command = " ".join((command,cppDict[key]))
        #########################################################
        incl_re = re.compile(r'\-I\s*([^"\s]\S*)')  # unquoted
        incls = incl_re.findall(command)
        incl_re = re.compile(r'\-I\s*("[^"]+")')    # quoted
        incls += incl_re.findall(command)
        if len(incls) > 0:
            cprint(RED,1,
                'WARNING: these include paths are specified in cppmap')
        for i in incls:
            cprint(RED,0,"  " + i)
        #########################################################
        includeDict = TargetIncludeMap(target)
        keys = list(includeDict.keys())
        keys.sort()
        for key in keys:
            fullpath = includeDict[key]
#           print('includemap ' + key + ': ' + fullpath)
            if fullpath == "":
                continue
            if not os.path.isabs(fullpath):
                fullpath = os.path.join(rootPath,fullpath)
                fullpath = os.path.abspath(fullpath)
            if " " in fullpath:
                command = " ".join((command,'"-I' + fullpath + '"'))
            else:
                command = " ".join((command,'-I' + fullpath))
        return command

    def add_linkflags(command, target):
        linkDict = copy.deepcopy(linkmap)
        try:
            target.module.linkmap
            for key in target.module.linkmap.keys():
                linkDict[key] = target.module.linkmap[key]
        except AttributeError:
            pass
        try:
            target.linkmap
            for key in target.linkmap.keys():
                linkDict[key] = target.linkmap[key]
        except AttributeError:
            pass
        for key in linkDict.keys():
            command = " ".join((command,linkDict[key]))

        return command

    def TargetIncludeMap(target):
        includeDict = copy.deepcopy(includemap)
        try:
            target.module.includemap
            for key in target.module.includemap.keys():
                includeDict[key] = target.module.includemap[key]
        except AttributeError:
            pass
        try:
            target.includemap
            for key in target.includemap.keys():
                includeDict[key] = target.includemap[key]
        except AttributeError:
            pass
        return includeDict

    def RelativePath(basePath,fullPath):
        remainder = os.path.abspath(fullPath)

        has_drive_letter = re.compile(r'^[A-z]:')
        if has_drive_letter.match(basePath):
            basePath = basePath[2:]
        if has_drive_letter.match(remainder):
            remainder = remainder[2:]

        resultPath = None
        lastRemainder = ""
        while remainder != basePath:
            (remainder,tail) = os.path.split(remainder)
            if resultPath:
                resultPath = os.path.join(tail, resultPath)
            else:
                resultPath = tail
            if remainder == lastRemainder:
                print()
                print("ERROR: forge.py RelativePath() failed to resolve")
                print('basePath \"%s\"' % basePath)
                print('fullPath \"%s\"' % fullPath)
                print('remainder \"%s\"' % remainder)
                break
            if remainder == '/' or remainder == '\\':
                return (remainder + resultPath)
            lastRemainder = remainder
        return resultPath

    ###########################################################################
    # build rules

    def resolve_cxx(target):
        use_cxx = cxx
        if target:
            try:
                target.module.cxx
                use_cxx = target.module.cxx
            except AttributeError:
                pass
            try:
                target.cxx
                use_cxx = target.cxx
            except AttributeError:
                pass
        return use_cxx

    def resolve_full_cxx(target):
        evaluate_compiler(target, False)
        just_cxx = resolve_cxx(target)
        return ' '.join([cxx_pre, just_cxx, cxx_post])

    def trim_transient(target):
        if os.environ["FE_TRIM_EXCESS_FILES"] != "0":
            for d in target.deps:
                match = re.compile(r'(.*)\.(o|obj|pch|gch)$').match(d.name)
                if match != None:
                    d.uses = d.uses + 1
                    backdepCount = len(d.backdeps)
                    suffix = match.group(2)
                    if d.uses < backdepCount and (suffix == "obj" or suffix == "o"):
                        # NOTE lost pch is not critical; lost obj is critical
                        if pretty == 0:
                            print('not removing %s (use %d of %d)' % (d.name, d.uses, backdepCount))
                        elif pretty < 2:
                            shortPath = short_path(RelativePath(objPath,d.name), 4)
                            cprint(CYAN,1,'not removing %s (use %d of %d)' % (shortPath, d.uses, backdepCount))
                    else:
                        if pretty == 0:
                            print('removing ' + d.name)
                        elif pretty < 2:
                            shortPath = short_path(RelativePath(objPath,d.name), 4)
                            cprint(CYAN,0,'removing ' + shortPath)
                        remove_file(d.name, True);

    def compile_obj(target):
        """default object file rule"""

        (modObjPath, tail) = os.path.split(target.name)
        if createpch != '':
            if not hasattr(target, "includemap"):
                target.includemap = {}
            target.includemap["PCH"] = modObjPath + "/.."

        full_cxx = resolve_full_cxx(target)

        name = confirm_path(target)
        command = " ".join((full_cxx, nolink))
        command = add_cppflags(command, target)

        if api == "x86_win32" or api == "x86_win64":

            # Ensure that the value terminates in the path separation char.
            # The compiler docs state clearly that only a trailing slash will
            # cause the value to be treated as a directory - all other values
            # will be treated as a filename.
            # Python docs explicitly mention that joining a final empty value
            # will produce a trailing separation character.
            pdbpath = os.path.join(libPath, '')
            command += f" /Fd{pdbpath}"

        pch_file=''
        pmh_file=''
        post_command=''
        if target.module != None:
            mod_parts = target.module.modName.split('.')
            pch_file = modObjPath + '\\' + mod_parts[-1] + '.pmh' + pchsuf
            pmh_file = mod_parts[-1] + '\\' + mod_parts[-1] + ".pmh"

        creating_pmh = False

        for d in target.deps:
            match = re.compile(r'(.*)\.(' + c_ext + ')$').match(d.name)
            if match != None:
                src = match.group(0)
                if src != None:
                    matchPMH = re.compile(r'(.*)\.pmh$').match(d.name)
                    if matchPMH == None:
                        if (usepch != ''
                            and usepmh != ''
                            and os.path.exists(pch_file)
                            and utility.file_has_include(d.name, pmh_file)
                        ):
                            command = " ".join((command,usepmh + pmh_file))
                            command = " ".join((command,usepch + pch_file))
                        command = " ".join((command,forcecxx + ' ' + src))
                    elif as_pch == '':
                        return
                    else:
                        if createpch != "":
                            command = " ".join((command, objout + target.name + ".obj"))
                            command = " ".join((command, createpch + target.name))

                        command = " ".join((command,as_pch + ' ' + src))

                        creating_pmh = True

        if createpch == "" or not creating_pmh:
            command = " ".join((command, objout+target.name))

        shortPath = short_path(RelativePath(objPath,target.name), 4)
        plog(2, GREEN, 0, f"compiling {shortPath}")
        clog.debug("\n" + command)

        try:
            execute(command,name)
            if post_command != '':
                execute(post_command,name)
        except:
            cprint(YELLOW,1,"removing " + name)
            remove_file(target.name, True)
            raise

    # reduce full path to 'count' number of path tokens (at end)
    def short_path(target, count):
        tokens = target.split('/')
        if len(tokens) > count:
            return "/".join(tokens[-count:])

        return target

    def compile_exe(target):
        """default executable rule"""
        full_cxx = resolve_full_cxx(target)
        name = confirm_path(target)
        command = " ".join((full_cxx, exeout+target.name))

        # when building cc->exe on Windows, don't leave obj file in root dir
        if api == "x86_win32" or api == "x86_win64":

            # Ensure that the value terminates in the path separation char.
            # The compiler docs state clearly that only a trailing slash will
            # cause the value to be treated as a directory - all other values
            # will be treated as a filename.
            # Python docs explicitly mention that joining a final empty value
            # will produce a trailing separation character.
            libpathslash = os.path.join(libPath, '')
            command += f" {objout}{libpathslash}"

            # We could attempt to replace the exesuf in the name, but there's
            # actually no guarantee that an exe target's name conforms to that
            # expectation. So we'll just tack on the pdb.
            pdbpath = os.path.join(libPath, f'{target.name}.pdb')
            command += f" /Fd{pdbpath}"

        command = add_cppflags(command, target)
        command = add_objs(command, target)
        command = add_cpps(command, target)
        command = " ".join((command,ld1))
        command = " ".join((command,libincl+libPath))
        command = " ".join((command,fat1))
        command = add_libs(command, target)
        command = add_linkflags(command, target)
        command = " ".join((command,fat2))
        command = " ".join((command,ld2))

        plog(2, MAGENTA, 0, f"compiling {name}")
        clog.debug(command)

        try:
            execute(command,name)

            if strip != '':
                command = " ".join((strip, target.name))

                if pretty < 1:
                    print(command)
                elif pretty > 2:
                    pass
                else:
                    cprint(YELLOW,0,'stripping ' + name)

                execute(command,name)
        except:
            if not name.startswith("forge_auto_test"):
                cprint(YELLOW,1,"removing " + name)
            remove_file(target.name, True)
            raise

        if os.environ["FE_TRIM_EXCESS_FILES"] != "0":
            trim_excess_files(target)

    def link_lib(target):
        """default static library rule"""
#       remove_file(target.name)
        name = confirm_path(target)
        command = ar;
        # this ordering is important '<ar> <flags> <lib> <objs>'
        arguments = " ".join((arflags, aroutprefix + target.name))
        arguments = add_objs(arguments, target)
        if pretty > 0:
            cprint(YELLOW,0,'linking ' + name)
        else:
            print(command,arguments)
        execute_long(command,arguments,name)

    def link_so(target):
        """default shared library rule"""
        full_cxx = resolve_full_cxx(target)
        name = confirm_dl_path(target)

        if hotreload_mode and target.module.modName.startswith("src."):
            cprint(RED, 0, "SKIPPING "+target.libname+" since it's not hotreloadable")
            return

        command = '%s -shared -o' % full_cxx
        command = " ".join((command,target.HotReloadableName()))
        command = " ".join((command,'-Wl,--whole-archive'))
        command = " ".join((command,'-Wl,--no-as-needed'))
        command = add_cppflags(command, target)
        command = add_objs(command, target)
        command = add_cpps(command, target)
        command = " ".join((command,'-ldl',libincl+libPath))
        command = add_libs(command, target)
        command = " ".join((command,'-Wl,--no-whole-archive'))
        command = " ".join((command,'-Wl,--as-needed'))
        command = add_linkflags(command, target)
        if pretty > 0:
            cprint(YELLOW,0,'linking ' + name)
        else:
            print(command)

        try:
            execute(command,name)

            if strip != '':
                command = " ".join((strip, target.HotReloadableName()))

                if pretty < 1:
                    print(command)
                elif pretty > 2:
                    pass
                else:
                    cprint(YELLOW,0,'stripping ' + name)

                execute(command,name)
        except:
            cprint(YELLOW,1,"removing " + name)
            remove_file(target.name, True)
            raise

        if hotreload_mode and target.IsHotReloadTarget():
            hotreload_manifest.AddDL(target.libname, target.libname + '-' + str(target.hotreload_number), target.HotReloadableName())

    def link_dylib(target):
        """default shared library rule"""
        full_cxx = resolve_full_cxx(target)
        name = confirm_path(target)
        command = '%s -dynamiclib -o ' % full_cxx
        command = " ".join((command,target.name))
        command = " ".join((command,"-install_name @rpath/%s" % name))
        command = add_cppflags(command, target)
        command = add_objs(command, target)
        command = add_cpps(command, target)
        command = " ".join((command,'-ldl',libincl+libPath))
        command = add_libs(command, target)
        command = add_linkflags(command, target)
        if pretty > 0:
            cprint(YELLOW,0,'linking ' + name)
        else:
            print(command)

        try:
            execute(command,name)
        except:
            cprint(YELLOW,1,"removing " + name)
            remove_file(target.name, True)
            raise

    def link_dll(target):
        """default dynamic link library rule"""
        name = confirm_dl_path(target)

        if hotreload_mode and target.module.modName.startswith("src."):
            cprint(RED, 0, "SKIPPING "+target.libname+" since it's not hotreloadable")
            return

        match = re.compile(r'(.*)\.dll').match(target.HotReloadableName())
        command = linker
        arguments = " ".join((linkerflags, dlflags, '/OUT:'+target.HotReloadableName()))
        arguments = " ".join((arguments, '/IMPLIB:'+ match.group(1) + '.lib' ))
        arguments = add_objs(arguments, target)
        arguments = add_cpps(arguments, target)
        arguments = add_libs(arguments, target)
        arguments = add_linkflags(arguments, target)
        if pretty > 0:
            cprint(YELLOW,0,'linking ' + name)
        else:
            print(command, arguments)
        execute_long(command,arguments,name)

        if embed != '':
            command = embed % (target.HotReloadableName(), target.HotReloadableName())

            if pretty < 1:
                print(command)
            elif pretty > 2:
                pass
            else:
                cprint(YELLOW,0,'embedding manifest in ' + name)

            try:
                execute(command,name)
            except:
                cprint(YELLOW,1,"removing " + name)
                remove_file(target.name, True)
                raise

        if hotreload_mode and target.IsHotReloadTarget():
            hotreload_manifest.AddDL(target.libname, target.libname + '-' + str(target.hotreload_number), target.HotReloadableName())

        if os.environ["FE_TRIM_EXCESS_FILES"] != "0":
            trim_excess_files(target)

    def copy_from_srcfile(target):
        (head,tail) = os.path.split(target.name)
        cprint(CYAN,1,'copying ' + tail)
        try:
            src = target.srcfile
        except AttributeError:
            cprint(RED,1,'ERROR: srcfile not specified for ' + target.name)
            return
        try:
            if os.path.exists(src):
                confirm_path(target)
                shutil.copy(src,target.name)
            else:
                cprint(RED,1,'ERROR: could not find ' + src)
        except IOError:
            cprint(RED,1,'ERROR: could not copy from ' + src)

    def noop(target):
        pass

    def build_exe_all(target):
        (head,tail) = os.path.split(target.name)
        cprint(RED,1,"WARNING: " + tail + " falling back to build.exe")
        match = re.compile(r'BUILD:(.*)').match(target.name)
        if match != None:
            dir = match.group(1)
            cwd = os.getcwd()
            os.chdir(dir)
            os.system("build.exe all")
            os.chdir(cwd)

    def build_exe_clean(target):
        (head,tail) = os.path.split(target.name)
        cprint(RED,1,"WARNING: " + tail + " falling back to build.exe")
        match = re.compile(r'BUILD:(.*)').match(target.name)
        if match != None:
            dir = match.group(1)
            cwd = os.getcwd()
            os.chdir(dir)
            os.system("build.exe clean")
            os.chdir(cwd)

    def clean_file(target):
        if isinstance(target,FileTarget):
            target.scanned = 0
            if os.path.exists(target.name):
                (head,tail) = os.path.split(target.name)
                if pretty > 0:
                    cprint(MAGENTA,0,'removing ' + tail)
                else:
                    print("removing " + target.name)
                remove_file(target.name)

    def trim_excess_files(target, clean=False):
        if isinstance(target,FileTarget):
            target.scanned = 0
            if os.path.exists(target.name):
                (head,tail) = os.path.split(target.name)
                if not tail.startswith("forge_auto_test"):
                    if clean or pretty < 2:
                        if clean:
                            verb = "removing"
                        else:
                            verb = "trimming"
                        if pretty > 0:
                            cprint(CYAN,0,verb + ' ' + tail)
                        else:
                            print(verb + " " + target.name)

                basename = target.name[:-4]
                suffix = target.name[-3:]

                attempts = 3
                for attempt in range(attempts):
                    try:
                        if clean:
                            remove_file(target.name, True)
                        if clean or suffix == "exe":
                            remove_file(basename + '.lib', True)
                        remove_file(basename + '.ilk', True)
                        remove_file(basename + '.pdb', True)
                        remove_file(basename + '.exp', True)

                        if attempt:
                            cprint(BLUE,0,"trim succeeded")

                        return
                    except Exception as e:
                        cprint(YELLOW,1,"WARNING: trim error")
                        print(e)

                        if attempt < attempts - 1:
                            cprint(BLUE,0,"retrying")

                cprint(YELLOW,1,"WARNING: trim failed")

    def clean_dll_file(target):
        trim_excess_files(target, True)

    def print_divider(target):
        if pretty == 0:
            print('------------------------------------------------------------')

    def print_pre_target(target):
        global print_depth
        for i in range(0,print_depth):
            sys.stdout.write("|  ")
        print(target.name)
        print_depth += 1

    def print_post_target(target):
        global print_depth
        print_depth -= 1

    def cctest(file_text, linkmap = None, includemap = None):
        global pretty
        global auto_serial

        if pretty == 0:
            cprint(YELLOW,1,file_text)

        srcname = os.path.join(libPath, "forge_auto_test" + str(auto_serial) + ".cc")
        srctarget = FileTarget(srcname)
        confirm_path(srctarget)

        exename = srcname.replace(".cc",".exe")
        exetarget = FileTarget(exename)
        confirm_path(exetarget)

        auto_serial = auto_serial + 1

        exetarget.AddDep(srctarget)
        if includemap != None:
            exetarget.includemap = includemap
        if linkmap != None:
            exetarget.linkmap = linkmap
        exetarget.buildNum = -1
        f = open(srctarget.name, 'w')
        f.write(file_text)
        f.close()
        tmp_pretty = pretty
        if pretty > 0:
            pretty = 3
        try:
            exetarget.Build(time.time(),"cc_test")
        except:
            pretty = tmp_pretty
            del targetDictionary[srctarget.name]
            del targetDictionary[exetarget.name]
            remove_file(srctarget.name)
            trim_excess_files(exetarget, True)
            return 'build'

        try:
            env = copy.copy(os.environ)
            env["LIBC_FATAL_STDERR_"] = "1"

            proc = subprocess.run(
                cleanup_command(exetarget.name),
                shell=True,
                encoding=ENCODING_DEFAULT,
                env=env,
            )

            rc=proc.returncode

            if pretty < 1:
                print(proc.stdout)

        except:
            pretty = tmp_pretty
            del targetDictionary[srctarget.name]
            del targetDictionary[exetarget.name]
            remove_file(srctarget.name)
            trim_excess_files(exetarget, True)
            return 'exception'

        trim_excess_files(exetarget, True)

        try:
            if rc != 0:
                pretty = tmp_pretty
                del targetDictionary[srctarget.name]
                del targetDictionary[exetarget.name]
                if os.path.exists(srctarget.name):
                    remove_file(srctarget.name)
                if os.path.exists(exetarget.name):
                    remove_file(exetarget.name)
                return 'run'

            pretty = tmp_pretty
            del targetDictionary[srctarget.name]
            del targetDictionary[exetarget.name]
            if os.path.exists(srctarget.name):
                remove_file(srctarget.name)
            if os.path.exists(exetarget.name):
                remove_file(exetarget.name)

        except Exception as e:
            cprint(YELLOW,1,"WARNING: auto cleanup error")
            print(e)

        return None

    def AddSourceDeps(tgt, module, sourcelist):
        for s in sourcelist:
            obj = ObjAndSrcTargets(module,s)
            if obj and tgt:
                tgt.AddDep(obj)
        return

    def AddObjDeps(tgt, module, objlist):
        for obj in objlist:
            if obj and tgt:
                tgt.AddDep(obj)
        return

    def ObjAndSrcTargets(module, src):
        # NOTE  os.path.abspath() resolves each "../"
        #       pathlib.Path.resolve() also resolves symlinks
        #       pathlib.Path.absolute() does neither
        absModDir = os.path.abspath(
            pathlib.Path(os.path.join(rootPath, module.modDir)))

        relativePath = RelativePath(absModDir, module.modPath)

        if relativePath:
            modObjPath = os.path.join(objPath, relativePath)
        else:
            modObjPath = objPath

        threadable = False
        matchPMH = RE_FILENAME_PMH.match(src)

        if matchPMH == None:
            threadable = True

            match = re.compile(r'(.*)\.(' + c_ext + ')$').match(src)

            if match == None:
                sourcename = os.path.join(module.modPath,src + cxx_extension)
            else:
                sourcename = os.path.join(module.modPath,match.group(0))
                src = match.group(1)

            objname = os.path.join(modObjPath, src + objsuf)

        elif createpch != '':
            # Source filename indicates a pmh, so its obj would be a pch.
            sourcename = os.path.join(module.modPath,src)
            objname = os.path.join(modObjPath, src + pchsuf )
        else:
            # Souce filename indicates a pmh, but createpch is empty, meaning
            # that the build toolchain is not pch capable. So, no target.
            return None

        obj = targetDictionary.get(objname, None)

        if not obj:
            obj = FileTarget(objname)
            obj.module = module
            if blddep:
                obj.AddDep(targetRegistry[module.modName + '.buildfile'])
            if sourcename not in targetDictionary:
                source = FileTarget(sourcename)
                source.module = module
                obj.AddDep(source)
                if blddep:
                    source.AddDep(targetRegistry[module.modName + '.buildfile'])

        obj.threadable = threadable

        return obj


    ###########################################################################

    def process_output(output, name):
        for rule in outputContext.GetRules(name):
            output = rule.action(output)
        return output


    ###########################################################################
    def process_logging():
        dep_txt = ""
        fp = open("dependencies.dot", "w")
        dep_txt += "strict digraph G {\n"
        dep_txt += 'rankdir=LR\n'
        dep_txt += 'compound=true\n'
        dep_txt += 'node [shape=box;color=grey50;style=filled;fillcolor="#eeeeee"]\n'

        sources = {}
        rawpairs = {}
        pairs = {}

        for dep in logging:
                if dep[0] == "deep":
                        rawpairs[(dep[1],dep[2])] = 1

        action_map = {}
        origin_map = {}
        for dep in logging:
                if dep[0] == "action":
                        action_map[dep[1]] = 1
                elif dep[0] == "origin":
                        origin_map[dep[1]] = 1

        for (k,v) in action_map.items():
                (name,tag) = k.LoggingName()
                dep_txt += '"%s" [shape=box;color=red;style=filled;fillcolor="#ffdddd"];\n' % name

        for (k,v) in origin_map.items():
                (name,tag) = k.LoggingName()
                dep_txt += '"%s" [shape=box;color=green;style=filled;fillcolor="#ddffdd"];\n' % name


        for (k,v) in rawpairs.items():

                (L,tag) = k[0].LoggingName()
                if tag=="OBJ":
                        dep_txt += '"%s" [width=2.0;height=1.0;shape=box;color=black;style=filled;fillcolor="#aadddd"];\n' % L
                if tag=="HDR":
                        dep_txt += '"%s" [width=2.0;height=1.0;shape=box;color=black;style=filled;fillcolor="#ddddaa"];\n' % L
                if tag=="EXE":
                        dep_txt += '"%s" [width=2.0;height=2.0;shape=circle;color=black;style=filled;fillcolor="#ddaadd"];\n' % L
                if tag=="LIB":
                        dep_txt += '"%s" [width=2.0;height=2.0;shape=circle;color=black;style=filled;fillcolor="#eeffdd"];\n' % L
                (R,tag) = k[1].LoggingName()
                if tag=="OBJ":
                        dep_txt += '"%s" [width=2.0;height=1.0;shape=box;color=black;style=filled;fillcolor="#aadddd"];\n' % R
                if tag=="HDR":
                        dep_txt += '"%s" [width=2.0;height=1.0;shape=box;color=black;style=filled;fillcolor="#ddddaa"];\n' % R
                if tag=="EXE":
                        dep_txt += '"%s" [width=2.0;height=2.0;shape=circle;color=black;style=filled;fillcolor="#ddaadd"];\n' % R
                if tag=="LIB":
                        dep_txt += '"%s" [width=2.0;height=2.0;shape=circle;color=black;style=filled;fillcolor="#eeffdd"];\n' % R

                if L != R:
                        pairs[(L,R)] = 1

        for (k,v) in pairs.items():
                dep_txt += '"%s" -> "%s"\n' % (k[0], k[1])

        dep_txt += "}\n"

        fp.write(dep_txt)
        fp.close()

    ###########################################################################
    def setup(parent, modPath, autoOnly=False):
        """special import function"""
        global moduleDict, failed

        # modPath is relative from FE root to module
        modDir = os.path.dirname(modPath)
        modName = os.path.basename(modPath)

        if modName in moduleDict:
            return moduleDict[modName]

        if not autoOnly:
            moduleDict[modName] = 1

        importName = modName + '.bld'
        mod_parts = modName.split('.')
        modPath = os.path.join(rootPath, modDir)
        for m in mod_parts:
            modPath = os.path.join(modPath,m)
        return mod_setup(parent, modDir, modName, modPath, autoOnly)

    def mod_setup(parent, modDir, modName, modPath, autoOnly=False):
        global moduleDict, currentFile, failed, do_auto

        py_mod_loc = os.path.join(modPath, 'bld.py')

        if not os.path.isfile(py_mod_loc):
            cprint(RED,0,f"WARNING: {modName} bld.py not found")
            traceback.print_stack()
            buildTarget = BuildExeTarget(modPath)
            moduleDict[modName] = buildTarget
            targetRegistry[modName] = buildTarget
            return

        # Otherwise, proceed with bld.py awareness for this mod.

        pythons_modname = f'forge.{modName}'

        py_mod = utility.import_file_as_module(pythons_modname, py_mod_loc)

        if not py_mod:
            cprint(RED,0,f"WARNING: {modName} bld.py failed importing")
            return

        try:
            parentFile = currentFile
            currentFile = pythons_modname

            module = Module(parent, modDir, modName, autoOnly)
            module.ok = None # None = ok, otherwise string error message

            if do_auto and not modName in dot_modules_confirmed:
                try:
                    if hasattr(py_mod, "auto"):
                        module.ok = py_mod.auto(module)
                        if module.ok:
                            if module.ok[:5] == "abort":
                                cprint(RED,1,f"ERROR: {modName} -- {module.ok}")

                                # implicit sticky overall success indicator.
                                failed = 1

                except Exception as e:
                    cprint(RED,1,f"ERROR: {modName} auto implementation error")
                    clog.exception(e)
                    module.ok = 'implementation'

            if autoOnly:
                # Skip running the module's setup().
                return module

            if module.ok == None:
                py_mod.setup(module)

            moduleDict[modName] = module
            targetRegistry[modName] = module
            build_file = FileTarget(py_mod_loc)

            #temporarily disabled to prevent widespread rebuilding bld.py change
            #module.AddDep(build_file)
            #build_file.AddDep(targetRegistry['ROOT_BUILD_FILE'])

            targetRegistry[f'{modName}.buildfile'] = build_file

            currentFile = parentFile

            return module

        except Exception as e:
            cprint(RED,1,f"WARNING: {modName} setup failed")
            clog.exception(e)
            return

    # -------------------------------------------------------------------------
    def deferTo(path_or_depth):
        global path_to_root
        if type(path_or_depth) is types.StringType:
            path_to_root = path
        else:
            for i in range(0,path_or_depth):
                path_to_root = os.path.join(path_to_root,'..')

    def start_timing():
        global t_start
        t_start = time.time()

    def end_timing(target):
        global t_start
        elapsed = time.time() - t_start
        if target.parentFile in buildTimes:
            buildTimes[target.parentFile] += elapsed
        else:
            buildTimes[target.parentFile] = elapsed

    ###########################################################################
    # RULE SUPPORT
    ###########################################################################
    class Rule:
        """Rule base class"""
        def __init__(self, expression, action):
            self.expr = expression
            if api == 'x86_win32' or api == 'x86_win64':
                lookLikePath = re.compile("[A-z]:\\.*")
                if lookLikePath.match(expression):
                    expression = expression.replace("\\","\\\\")
            self.compiledExpr = re.compile(expression,re.M)
            self.action = action
            self.currentMatch = None

    class RuleContext:
        def __init__(self):
            self.ruleList = []
        def AppendRule(self, expression, action):
            rule = Rule(expression, action)
            self.ruleList.append(rule)
        def PrependRule(self, expression, action):
            rule = Rule(expression, action)
            self.ruleList = [rule] + self.ruleList
        def GetRules(self, target):
            ruleSubset = []
            for rule in self.ruleList:
                rule.currentMatch = rule.compiledExpr.match(target)
                if rule.currentMatch:
                    ruleSubset.append(rule)
            return ruleSubset


    ###########################################################################
    # TARGET SUPPORT
    ###########################################################################

#   import thread
    import threading
    class BuildThread(threading.Thread):
        def __init__(self,target,ref_time,for_name,stack):
            threading.Thread.__init__(self)
            self.target = target
            self.ref_time = ref_time
            self.new_time = ref_time
            self.for_name = for_name
            self.stack = stack
            self.exc_info = None
        def run(self):
            try:
                self.new_time = self.target.Build(self.ref_time,self.for_name,self.stack)
            except SystemExit:
                print("build thread exited")
                self.exc_info = sys.exc_info()
            except Exception as e:
                print("build thread excepted")
                print(e)
                self.exc_info = sys.exc_info()

        def join(self, timeout=None):
            threading.Thread.join(self, timeout)
            if self.exc_info:
                (exc_type, exc_value, exc_traceback) = self.exc_info
                raise exc_type(exc_value)

    class Target:
        """Target base class"""
        def __init__(self,name):
            self.deps = []
            self.backdeps = []
            self.name = name
            if self.name in targetDictionary:
                raise Exception('new target collision: ' + self.name)
            targetDictionary[self.name] = self
            self.uses = 0
            self.scanned = 0
            self.buildNum = 0
            self.depContext = None
            self.module = None
            self.threadable = False
            self.building = utility.Atomic(0)
            self.built = False
            self.visit = 0
            self.parentFile = currentFile
            self.post_hook = None
        def AppendOverrideRule(self, pattern, method):
            if self.depContext == None:
                self.depContext = RuleContext()
            self.depContext.AppendRule(pattern, method)
        def AddDep(self, dependency):
            self.deps.append(dependency)
            dependency.backdeps.append(self)
        def AddDeps(self, dependencyList):
            for dependency in dependencyList:
                self.AddDep(dependency)
        def RemoveDep(self, dependency):
            self.deps.remove(dependency)
            dependency.backdeps.remove(self)
        def ReplaceDep(self, original, dependency):
            id=self.deps.index(original)
            self.deps.insert(id,dependency)
            self.deps.remove(original)
            original.backdeps.remove(self)
        def Touch(self):
            pass
        def SetTime(self, newTime):
            pass
        def Deps(self):
            return self.deps
        def Exists(self):
            return 0
        def LastBuild(self):
            return 0
        def PrintDeps(self):
            for d in self.deps:
                (head,tail) = os.path.split(d.name)
                print('     ' + tail)
        def PrintBackDeps(self):
            for d in self.backdeps:
                (head,tail) = os.path.split(d.name)
                print('     ' + tail)
        def CycleCheck(self):
            if pretty < 1:
                cprint(CYAN,1,'cycle check %s' % self.name)
            self.Visit()
        def Visit(self):
            if self.visit == 1:
                if pretty < 2:
                    color_on(1, YELLOW)
                    sys.stdout.write('WARNING: #include dependency cycle')
                    color_off()
                    cprint(YELLOW,0,' (only reporting first cycle found)')
                    cprint(YELLOW,0,'   %s' % RelativePath(rootPath,self.name))
                return self
            if self.visit == 2:
                return None

            self.visit = 1

            for d in self.deps:
                v2 = d.Visit()
                if v2 != None:
                    if pretty < 2:
                        cprint(YELLOW,0,'   %s' % RelativePath(rootPath,self.name))
                    if v2 != self:
                        self.visit = 2
                        return v2

                    # NOTE don't want to report ourselves again
                    self.visit = 2

            self.visit = 2

            return None

        def DeepLog(self, refbuild):
            global logging
            if self.LastBuild() <= refbuild:
                more = 0
                for d in self.deps:
                    if d.DeepLog(refbuild):
                        more = 1
                        logging.append( ("deep", d, self) )
                if self.LastBuild() == refbuild:
                    if more == 0:
                        logging.append( ("origin", self) )
                    return 1
                else:
                    return 0
            return 0

        def Build(self, reference_time, for_name, stack = []):
            """\
The basic principal of forge is to ensure that a given target is up
to date.  The purpose of the Build() method of a target is to bring
the target up to date with respect to the given reference time.
An updated reference time is returned.  Typically the returned reference
time is the most recent time the target has been updated or changed.

This method has four main phases:
- Execute matching Pre-Processing Context rules
- Recurse dependency graph, calling Target::Build() on dependencies, getting
  an updated reference time
- If the updated reference time is more recent than the last update time
  for the target, matching Build Context rules are executed
- Execute Post-Processing Context rules
            """
            global logging, do_plot
            global recursionLevel
            (head,tail) = os.path.split(self.name)

#           sys.stdout.write("\n" + str(recursionLevel) + " " + self.name + "\n")
#           for d in self.deps:
#               sys.stdout.write("  DEP " + d.name + " " + str(d.threadable) + " " + str(d.built) + "\n")
#           for target in stack:
#               sys.stdout.write("  STACK " + target + "\n")

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # no need if already built
            if self.built:
#               sys.stdout.write("\n" + str(recursionLevel) + " REPEAT " + self.name + "\n")
                return self.LastBuild()

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # wait if already building (also, built may get set just now)
            alreadyBuilding = self.building.exchange(1)
            if alreadyBuilding or self.built:
                if self.name in stack:
                    # cycle protection
#                   sys.stdout.write("\n" + str(recursionLevel) + " CYCLE " + self.name + "\n")
                    if pretty < 1:
                        cprint(YELLOW,1,"breaking dependency cycle in " + self.name)
                else:
                    # wait if being built on another thread
                    waitCount = 0
                    while not self.built:
#                       sys.stdout.write("\n" + str(recursionLevel) + " WAITING FOR " + self.name + "\n")
                        if waitCount%10 == 9:
                            if waitCount > 100:
                                cprint(RED,1,"excessive wait for %s" % (self.name))
                            else:
                                cprint(BLUE,1,"waiting for %s" % (self.name))
                        waitCount += 1
                        time.sleep(0.1)
                return self.LastBuild()

            # TODO can we drop buildNum variable?

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # skip if recently built, by buildNum (just auto-tests now?)
#           if self.buildNum >= buildNum:
#               sys.stdout.write("\n" + str(recursionLevel) + " SKIP " + str(self.buildNum) + " " + str(buildNum) + " " + str(self.built) + " " + str(self.built) + " " + self.name + "\n")
#               return self.LastBuild()

            self.buildNum = buildNum

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # pre context check
            if preContext != None:
                for rule in preContext.GetRules(self.name):
                    start_timing()
                    rule.action(self)
                    end_timing(self)

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # check and build dependencies
            newRefTime = reference_time
            if recursionDepth < 0 or recursionLevel < recursionDepth:
                recursionLevel += 1
                threadables = []
                serials = []

                # non-threadable before threadable, even if not parallel
                # (pmh files are set to not threadable)
                for d in self.deps:
                    if not d.threadable:
                        serials.append(d)

                for d in self.deps:
                    if d.threadable:
                        if jobs > 0:
                            threadables.append(d)
                        else:
                            serials.append(d)

                for d in serials:
#                   sys.stdout.write("\n" + str(recursionLevel) + " SERIAL " + d.name + " " + str(d.LastBuild()) + "\n  for " + self.name + " " + str(self.LastBuild()) + " (" + str(self.LastBuild()-d.LastBuild()) + ")\n")
                    newTime = d.Build(reference_time, self.name, stack + [self.name])
                    if newTime > newRefTime:
#                       sys.stdout.write("\n  REREF " + self.name + "\n  from " + str(time.ctime(newRefTime)) + " to " + str(time.ctime(newTime)) + "\n  because " + d.name + "\n")
                        newRefTime = newTime
#                   sys.stdout.write("\n" + str(recursionLevel) + " COMPLETE " + d.name + "\n  for " + self.name + "\n")

                threads = []

                for d in threadables:
                    while len(threads) >= jobs:
                        threads[0].join()
#                       cprint(BLUE,0,'<< ' + threads[0].target.name)
                        newTime = threads[0].new_time
                        if newTime > newRefTime:
                            newRefTime = newTime
                        threads.remove(threads[0])
#                   sys.stdout.write("\n" + str(recursionLevel) + " THREAD " + d.name + "\n  for " + self.name + "\n")
                    t = BuildThread(d,reference_time, self.name, stack + [self.name])
                    threads.append(t)
#                   cprint(BLUE,1,'>> ' + t.target.name)
                    t.start()
                for t in threads:
                    t.join()
#                   sys.stdout.write("\n" + str(recursionLevel) + " FINISH " + t.target.name + "\n  for " + self.name + "\n")
#                   cprint(BLUE,0,'<< ' + t.target.name)
                    newTime = t.new_time
                    if newTime > newRefTime:
                        newRefTime = newTime

                recursionLevel -= 1

            if debug_forge:
                print(self.name + " lastbuilt:" + time.ctime(
                    self.LastBuild()) + " reftime:" + time.ctime(newRefTime))

#           sys.stdout.write("\n" + str(recursionLevel) + " CHECK " + self.name + "\n  file " + str(time.ctime(self.LastBuild())) + " vs ref " + str(time.ctime(newRefTime)) + " (" + str(self.LastBuild()-newRefTime) + ") exists=" + str(self.Exists()) + "\n")

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # if necessary build self via actions
            if self.LastBuild() < newRefTime or not self.Exists():
                if self.depContext != None and depContext != None:
                    depC = self.depContext
                else:
                    depC = depContext
                if depC != None:
                    for rule in depC.GetRules(self.name):
#                       sys.stdout.write("\n" + str(recursionLevel) + " ACTION " + self.name + "\n")
                        start_timing()
                        if do_plot:
                            logging.append( ("action", self) )
                            self.DeepLog(newRefTime)
                        rule.action(self)
                        end_timing(self)
                if self.LastBuild() < newRefTime:
                    self.SetTime(newRefTime)

            if not self.Exists():
                self.Touch()

            if self.LastBuild() > newRefTime:
                newRefTime = self.LastBuild()

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # post context check
            if postContext != None:
                for rule in postContext.GetRules(self.name):
                    start_timing()
                    rule.action(self)
                    end_timing(self)

            tryWrites = 3
            while not os.path.exists(self.name):
                if not tryWrites:
                    cprint(YELLOW,0,"never wrote %s" % (self.name))
                    break
                cprint(CYAN,0,"expecting %s" % (self.name))
                time.sleep(1.0)
                tryWrites -= 1

#           sys.stdout.write("\n" + str(recursionLevel) + " BUILT %s\n" % (self.name))
            self.built = True
            self.building.exchange(0)


            return newRefTime

    class FileTarget(Target):
        def __init__(self,name):
            self.name = os.path.abspath(name)
            self.lastBuild = 0
            Target.__init__(self,name)
        def LastBuild(self):
            if self.lastBuild != 0:
                return self.lastBuild
            if os.path.exists(self.name):
                self.lastBuild = os.stat(self.name)[stat.ST_MTIME]
            return self.lastBuild
        def Touch(self):
            self.lastBuild = time.time()
        def SetTime(self,newTime):
            self.lastBuild = newTime
        def Exists(self):
            return os.path.exists(self.name)
        def Remove(self):
            targetDictionary.pop(self.name)
        def IsHeldOpenByProcess(self):
            if not self.Exists():
                return False
            if api == "x86_win32" or api == "x86_win64":
                # On windows we check for dlopen handles by checking for the file write lock.
                try:
                    file = open(self.name, 'r+')
                    file.close()
                    return False
                except IOError as e:
                    if e.errno == errno.EACCES:
                        return True
                    else:
                        raise
                return False
            else:
                # On linux we use lsof to check if the file is open.
                # NOTE: For some reason lsof doesn't work correctly in WSL.
                if subprocess.call(["lsof", "-F", "p", self.name], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == 0:
                    # Zero code means it returned something.
                    return True
                else:
                    # lsof returns an error code if no processes are found
                    return False
        def ShortName(self):
            (head,tail) = os.path.split(self.name)
            return tail
        def LoggingInfo(self):
            tails = []
            (base, tail) = os.path.split(os.path.relpath(self.name, rootPath))
            while base != '':
                tails.append(tail)
                (base, tail) = os.path.split(base)
            tails.append(tail)
            return tails
        def LoggingName(self):
            info = self.LoggingInfo()
            name = self.ShortName()
            tag = None
            if collapse_obj:
                if info[-1] == 'obj':
                    name = info[-4] + " impl"
                    tag = "OBJ"
                elif os.path.splitext(info[0])[1] == '.cc':
                    name = info[-2] + " impl"
                    tag = "OBJ"
            if collapse_hdr:
                if os.path.splitext(info[0])[1] == '.h':
                    name = info[-2] + " hdr"
                    tag = "HDR"
                elif os.path.splitext(info[0])[1] == '.pmh':
                    name = info[-2] + " hdr"
                    tag = "HDR"
            if info[-1] == 'lib':
                if os.path.splitext(info[0])[1] == '.exe':
                    if collapse_exe:
                        name = "executables"
                        tag = "EXE"
                elif collapse_lib:
                    name = "libraries"
                    tag = "LIB"
            return (name,tag)

    class HotReloadableFileTarget(FileTarget):
        def __init__(self, path, nameprefix, libname, namesuffix, file_extension, hotreload_number = None):
            self.path = path
            self.nameprefix = nameprefix
            self.libname = libname
            self.hotreload_number = hotreload_number
            self.namesuffix = namesuffix
            self.file_extension = file_extension
            FileTarget.__init__(self, os.path.join(self.path, self.nameprefix+self.libname+self.namesuffix+self.file_extension))
        def HotReloadableName(self):
            if self.hotreload_number:
                return os.path.join(self.path, self.nameprefix+self.libname+'-'+str(self.hotreload_number)+self.namesuffix+self.file_extension)
            else:
                return self.name
        def LastBuild(self):
            if self.lastBuild != 0:
                return self.lastBuild
            if os.path.exists(self.HotReloadableName()):
                self.lastBuild = os.stat(self.HotReloadableName())[stat.ST_MTIME]
            return self.lastBuild
        def IsHeldOpenByProcess(self):
            if not os.path.exists(self.HotReloadableName()):
                return False
            if api == "x86_win32" or api == "x86_win64":
                # On windows we check for dlopen handles by checking for the file write lock.
                try:
                    file = open(self.name, 'r+')
                    file.close()
                    return False
                except IOError as e:
                    if e.errno == errno.EACCES:
                        return True
                    else:
                        raise
                return False
            else:
                # On linux we use lsof to check if the file is open.
                # NOTE: For some reason lsof doesn't work correctly in WSL.
                if subprocess.call(["lsof", "-F", "p", self.name], stdout=subprocess.DEVNULL) == 0:
                    # Zero code means it returned something.
                    return True
                else:
                    # lsof returns an error code if no processes are found
                    return False
        def IsHotReloadTarget(self):
            return self.hotreload_number

    class CopyTarget(FileTarget):
        def __init__(self,name,src):
            FileTarget.__init__(self,name)
            if isinstance(src,FileTarget):
                self.srcfile = src.name
                srctarget = src
            else:
                self.srcfile = src
                srctarget = LookupTarget(self.srcfile)
                if not srctarget:
                    srctarget = FileTarget(self.srcfile)
                srctarget.AppendOverrideRule(".*", noop)
            self.AppendOverrideRule(".*", copy_from_srcfile)
            self.AddDep(srctarget)


    def FindFileTarget(filepath):
        if filepath in targetDictionary:
            return targetDictionary[filepath]
        else:
            return FileTarget(filepath)

    def LookupTarget(name):
        if name in targetDictionary:
            return targetDictionary[name]

        # watch out for arbitrary Windows path delimters
        clean_name = name.replace("\\","/")
        for key in targetDictionary:
            if clean_name == key.replace("\\","/"):
                return targetDictionary[key]

        return None

    def FindTarget(name):
#       if name in targetDictionary:
#           return targetDictionary[name]

        target = LookupTarget(name)
        if target:
            return target

        return Target(name)

    def MatchTarget(arg):
        matches = []
        if arg == 'here':
            if os.getcwd() == rootPath:
                arg = 'root'
            else:
                moduleName = None
                cwd = os.path.abspath(os.getcwd())
                remainder = cwd
                resultPath = None
                while remainder != rootPath:
                    (remainder,tail) = os.path.split(remainder)
                    if moduleName:
                        moduleName = tail + '.' + moduleName
                    else:
                        moduleName = tail
                    if remainder == '/':
                        cprint(YELLOW,1,"Can't find " + cwd + " as 'here'")
                        cprint(YELLOW,1,"\tin " + rootPath + " -> using 'root'")
                        moduleName = 'root'
                        break
                arg = moduleName

        if targetRegistry.has_key(arg):
            matches.append(targetRegistry[arg])
            return targetRegistry[arg]
        x = os.path.join(os.getcwd(),arg)
        if targetRegistry.has_key(x):
            return targetRegistry[x]
        return None

    ###########################################################################
    # DEPENDENCY SUPPORT
    ###########################################################################


    def FindInPath(target, filename):
        # first check file's local directory
        (head,tail) = os.path.split(target.name)
        filepath = os.path.join(head, filename)
        if os.path.isfile( filepath ):
            return filepath

        # then check the specified include directories
        include_map = TargetIncludeMap(target)
        for suppress_key in includeScanSuppress:
            include_map.pop(suppress_key, None)
        for key in include_map.keys():
            fullpath = include_map[key]
            if not os.path.isabs(fullpath):
                fullpath = os.path.join(rootPath,fullpath)
                fullpath = os.path.abspath(fullpath)
            filepath = os.path.join(fullpath, filename)
            if os.path.isfile( filepath ):
                return filepath

#       print("include not found \"%s\"" % filename)

        return None

    def ScanIncludes(fileTarget):
        #print('scan ' + fileTarget.name)
        if fileTarget.scanned == 1:
            return
        fileTarget.scanned = 1
        isPCH = re.compile(r'.*\..?ch$').match(fileTarget.name)
        if isPCH != None:
            return
        if not os.path.isfile( fileTarget.name ):
            return

        try:
            if sys.version_info.major>=3:
                f = open(fileTarget.name, encoding="ascii")
            else:
                f = open(fileTarget.name)
            content = f.read()
        except UnicodeDecodeError as e:
            f.close()
            f = open(fileTarget.name, errors="replace")
            content = f.read()

            if pretty < 2:
                cprint(YELLOW,0,fileTarget.name + " has non-ascii characters")
            if pretty < 1:
                cprint(BLUE,0,"(" + str(e) + ")")

                byteIndex = int(str(e).split(" ")[8].split(":")[0])
                firstByte = max(byteIndex-16,0)
                lastByte = min(byteIndex+16,len(content)-1)

                print(content[firstByte: lastByte])

        f.close()
        # re has four parts: <|" filename >|" comment
        includes = include_re.findall(content)
        for include in includes:
            if "SUPPRESS_SCAN" in include[3]:
                continue
            filepath = FindInPath(fileTarget,include[1])
            if filepath != None:
                filepath = os.path.normpath(filepath)
                includeTarget = FindFileTarget(filepath)
                #print('bind ' + fileTarget.name + ' -> ' + includeTarget.name)
                fileTarget.AddDep(includeTarget)
                ScanIncludes(includeTarget)
            else:
                #cprint(RED,0,"      " + include[1] + " " + fileTarget.ShortName())
                pass

    def AddScanInclude(include_path):
        if not os.path.isabs(include_path):
            include_path = os.path.join(rootPath, include_path)
            include_path = os.path.abspath(include_path)
        scanIncludePath.append(include_path)
        return include_path

    def dep_lookup(consumer, provider):
        if isinstance(consumer,Target):
            c = consumer
        else:
            c = targetRegistry[consumer]
        if isinstance(provider,Target):
            p = provider
        else:
            p = targetRegistry[provider]
        c.AddDep(p)

    def deps(consumers, providers):
        dep(consumers, providers)

    def dep(consumers, providers):
        if isinstance(consumers, list):
            consumer_list = consumers
        else:
            consumer_list = [ consumers ]

        if isinstance(providers, list):
            provider_list = copy.deepcopy(providers)
        else:
            provider_list = [ providers ]

        # note that --no-as-needed should make this not really necessary
        # but it seems like the correct thing for gnu (bad for VS)
        if compiler_brand == 'gnu':
            provider_list.reverse()

        for consumer in consumer_list:
            for provider in provider_list:
                dep_lookup(consumer, provider)

    class PlaceholderTarget(Target):
        def __init__(self, name):
            Target.__init__(self, name)
        def Build(self, reference_time, for_name, stack = []):
            cprint(RED,1,"ERROR: unresolved placeholder target %s for %s " % (self.name, for_name))
            sys.exit(1)

    class TargetRegistry:
        def __init__(self):
            self.map = {}
        def __setitem__(self, key, value):
            if key in self.map:
                oldvalue = self.map[key]
                if isinstance(oldvalue, PlaceholderTarget):
                    for d in oldvalue.deps:
                        value.AddDep(d)
                    while len(oldvalue.backdeps) > 0:
                        d = oldvalue.backdeps[0]
                        d.ReplaceDep(oldvalue,value)
                    oldvalue.PrintBackDeps()
                else:
                    cprint(RED,1,"ERROR: attempt to redefine target: " + key)
                    sys.exit(1)
            self.map[key] = value
        def __getitem__(self, key):
            if key in self.map:
                return self.map[key]
            placeholder = PlaceholderTarget(key)
            self.map[key] = placeholder
            return placeholder
        def has_key(self, key):
            return key in self.map
        def keys(self):
            return self.map.keys()

    ###########################################################################
    class Module(FileTarget):
        """Module target class. A module is a directory with its own
        bld.py file."""
        def __init__(self, parent, modDir, modName, autoOnly=False):
            self.parent = parent
            self.modDir = modDir
            self.modName = modName
            dirstruct = modName.split('.')
            if modDir != "":
                self.modPath = modDir
            else:
                self.modPath = rootPath
            for dir in dirstruct:
                self.modPath = os.path.join(self.modPath,dir)
            self.modPath = os.path.abspath(self.modPath)
            if not autoOnly:
                FileTarget.__init__(self, self.modPath)
                targetRegistry['root'].AddDep(self)
            self.hasDLL = 0
            self.hasLib = 0
            self.hasExe = 0
            self.hasManifest = 0
            self.hasYaml = 0
        def Module(self, subName, autoOnly=False):
            subModName = self.modName + '.' + subName
            for glob_pattern in mod_kill:
                re_pattern = fnmatch.translate(glob_pattern)
                if re.compile(re_pattern).match(subModName):
                    return None

            n = setup(self, os.path.join(self.modDir, self.modName) + '.' + subName, autoOnly)
            if not autoOnly and n != None:
                self.AddDep(n)
            return n
        def SrcTargetName(self,SrcnameWithExtension):
            return os.path.join(self.modPath,SrcnameWithExtension)
        def ObjTargetName(self,ObjNameWithoutExtension):
            modObjPath = os.path.join(objPath, RelativePath(rootPath,self.modPath))
            return os.path.join(modObjPath, ObjNameWithoutExtension + objsuf)
        def Exe(self, name, srclist = None):
            binary_suffix=os.environ["FE_BINARY_SUFFIX"]
            self.hasExe = 1
            if srclist == None:
                srclist = [ name ]
            exeTarget = FileTarget(os.path.join(libPath,name + binary_suffix + exesuf))
            exeTarget.module = self

            exeTarget.threadable = True

            # We don't build exe's in hotreload mode
            if not hotreload_mode:
                targetRegistry[name + 'Exe'] = exeTarget
                AddSourceDeps(exeTarget,self,srclist)
                self.AddDep(exeTarget)
                if blddep:
                    exeTarget.AddDep(targetRegistry[self.modName + '.buildfile'])
            return exeTarget
        def Lib(self, name, srclist, registeredName = None):
            binary_suffix=os.environ["FE_BINARY_SUFFIX"]
            self.hasLib = 1
            libTarget = FileTarget(os.path.join(libPath,libpre + name + binary_suffix + libsuf))
            libTarget.module = self
            #global targetRegistry
            if registeredName != None:
                targetRegistry[registeredName] = libTarget
            else:
                targetRegistry[name + 'Lib'] = libTarget
            AddSourceDeps(libTarget,self,srclist)
            self.AddDep(libTarget)
            if blddep:
                libTarget.AddDep(targetRegistry[self.modName + '.buildfile'])
            return libTarget
        def SrcToObj(self, srcList):
            objlist = []
            for s in srcList:
                t = ObjAndSrcTargets(self, s)
                if t != None:
                    objlist.append(t)
            return objlist
        def LibFromObjs(self, name, objlist, registeredName = None):
            binary_suffix=os.environ["FE_BINARY_SUFFIX"]
            self.hasLib = 1
            libTarget = FileTarget(os.path.join(libPath,libpre + name + binary_suffix + libsuf))
            libTarget.module = self
            if registeredName != None:
                targetRegistry[registeredName] = libTarget
            else:
                targetRegistry[name + 'Lib'] = libTarget
            AddObjDeps(libTarget,self,objlist)
            self.AddDep(libTarget)
            if blddep:
                libTarget.AddDep(targetRegistry[self.modName + '.buildfile'])
            return libTarget
        def DLL(self, name, srclist, registeredName = None):
            binary_suffix=os.environ["FE_BINARY_SUFFIX"]
            self.hasDLL = 1
            libTarget = HotReloadableFileTarget(libPath, libpre, name, binary_suffix, libdl)
            if hotreload_mode:
                # Search for unused suffix.
                hotreload_number = 0
                loaded_library_lastBuild = libTarget.LastBuild()
                while libTarget.IsHeldOpenByProcess():
                    loaded_library_lastBuild = libTarget.LastBuild() # We need to save the lastBuild time of the last library to use it for the new one.
                    libTarget.Remove()
                    hotreload_number += 1
                    libTarget = HotReloadableFileTarget(libPath, libpre, name, binary_suffix, libdl, hotreload_number)
                libTarget.lastBuild = loaded_library_lastBuild
            libTarget.module = self
            if registeredName != None:
                targetRegistry[registeredName] = libTarget
            else:
                targetRegistry[name + 'Lib'] = libTarget
            AddSourceDeps(libTarget,self,srclist)
            self.AddDep(libTarget)
            if blddep:
                libTarget.AddDep(targetRegistry[self.modName + '.buildfile'])
            return libTarget
        def Build(self, reference_time, for_name, stack = []):
            return FileTarget.Build(self,reference_time, for_name, stack)
        def FindObjTargetForSrc(self, srcName):
            re_pmh = re.compile(r'(.*)\.pmh$')
            re_cpp = re.compile(r'(.*)\.cpp$')
            relativePath = RelativePath(rootPath, self.modPath)
            modObjPath = os.path.join(objPath, relativePath)
            objName = modObjPath + '/' + srcName
            if re_cpp.match(objName):
                objName = objName.strip(".cpp")
            if re_pmh.match(objName):
                objName += pchsuf
            else:
                objName += objsuf
            return FindTarget(objName)

        # Add the manifest from this module.
        # Returns false if manifest not found.
        def AddManifest(self):
            manifest = os.path.join(self.modPath, 'manifest.cc')
            manifests_new = os.path.join(rootPath, 'src/autoload/manifests_new.cc')
            if not os.path.exists(manifest):
                return 0
            with open(manifests_new, 'a') as outfile:
                with open(manifest) as infile:
                    outfile.write('\t// '+self.modName+'\n')
                    outfile.write(infile.read())
                    self.hasManifest = 1
            return 1

        # Add any lib yamls from this module.
        # Returns number of yamls found.
        def AddYaml(self):
            dot_yaml = re.compile(r'(.*)DL\.yaml$')
            yamlCount = 0

            dir_list = os.listdir(self.modPath)
            for file in dir_list:
                if dot_yaml.match(file):
                    source = os.path.join(self.modPath,file)

                    # Ensure that any pre-existing destination file can be
                    # over-written.
                    dest = os.path.join(libPath, file)
                    if os.path.exists(dest):
                        os.chmod(dest, stat.S_IWRITE)

                    shutil.copy(source,libPath)
                    yamlCount = yamlCount + 1
                    self.hasYaml = 1

            return yamlCount

    class RootModule(Module):
        def __init__(self):
            self.parent = None
            self.modName = 'root'
            self.modPath = rootPath
            FileTarget.__init__(self, self.modPath)
        def AsRootReset(self):
            if self.name in targetDictionary:
                del targetDictionary[self.name]
            self.modPath = rootPath
            self.name = self.modPath

    class CleanTarget(Target):
        def __init__(self,str):
            Target.__init__(self,str)
            self.help = """\
remove target files
example to clean all target files:
  forge.py clean root
"""
        def Build(self, reference_time, for_name, stack = []):
            global preContext
            global depContext
            if preContext:
                cleanContext = preContext
            else:
                preContext = defaultCleanContext
            depContext = None

    class BuildTarget(Target):
        def Build(self, reference_time, for_name, stack = []):
            global preContext
            global depContext
            depContext = defaultContext


    class PrintTarget(Target):
        def __init__(self,str):
            Target.__init__(self,str)
            self.help = """\
print stuff for targets
this is useful for debugging, although somewhat verbose
"""
        def Build(self, reference_time, for_name, stack = []):
            global preContext
            global depContext
            global postContext
            if preContext:
                printPreContext = preContext
            else:
                printPreContext = RuleContext()
                preContext = printPreContext
            if postContext:
                printPostContext = preContext
            else:
                printPostContext = RuleContext()
                postContext = printPostContext

            printPreContext.AppendRule(".*", print_pre_target)
            printPostContext.AppendRule(".*", print_post_target)

    class BuildExeTarget(Target):
        def __init__(self, name):
            self.lastBuild = 0
            Target.__init__(self, "BUILD:" + name)
            targetRegistry['root'].AddDep(self)
            (head,tail) = os.path.split(name)
            targetRegistry["build.exe:" + tail] = self
        def LastBuild(self):
            return self.lastBuild
        def Touch(self):
            self.lastBuild = time.time()
        def SetTime(self,newTime):
            self.lastBuild = newTime

    def ScanAllTargets():
        cprint(CYAN,1,'scanning dependencies')
        keyList = list(targetDictionary)
        for key in keyList:
            if re.compile(r'(.*)\.(' + c_ext + ')$').match(key):
                ScanIncludes(targetDictionary[key])
        depcnt = 0
        for t in targetDictionary.values():
            depcnt += len(t.deps)
        cprint(CYAN,0,'%d targets %d dependencies' % (len(targetDictionary), depcnt))

    class HotReloadManifest():
        def __init__(self):
            self.exists = False
            self.manifest = {}
            self.cleanup_list = []
            self.manifest_filepath = os.environ["FE_LIB_PATH"]+"/"+"FEhotreload"+os.environ["FE_BINARY_SUFFIX"]+".json"
            try:
                with open(self.manifest_filepath, "r+") as file:
                    json_root = json.load(file)
                    self.manifest = json_root['Modules']
                    self.cleanup_list = json_root['CleanupList']
                    self.exists = True
            except FileNotFoundError:
                self.exists = False
        def Exists(self):
            return self.exists
        def AddDL(self, oldname, newname, filepath):
            self.manifest[oldname] = newname
            self.cleanup_list.append(filepath)
        def Write(self):
            cprint(CYAN, 1, 'writing hotreload manifest:')
            with open(self.manifest_filepath, "w") as file:
                json_root = {
                    'Modules': self.manifest,
                    'CleanupList': self.cleanup_list
                }
                json.dump(json_root, file)
            for original_dl_name, new_dl_name in self.manifest.items():
                cprint(MAGENTA, 1, '    ' + original_dl_name + ' -> ' + new_dl_name)
        def Cleanup(self):
            for filepath in self.cleanup_list:
                if os.path.exists(filepath):
                    (head, tail) = os.path.split(filepath)
                    cprint(YELLOW, 1, 'removing ' + tail)
                    os.remove(filepath)
            cprint(YELLOW, 1, 'removing hotreload manifest')
            os.remove(self.manifest_filepath)

    # Check for a write lock on feMemory since it's a core required DLL and will indicate if there
    # are any open dll handles present.
    def ShouldHotReload():
        binary_suffix = os.environ["FE_BINARY_SUFFIX"]
        name = os.path.join(libPath, libpre + "feMemory" + binary_suffix + libdl)
        # Create a temp target for testing
        libTarget = FileTarget(name)
        should_hot_reload = False
        if libTarget.IsHeldOpenByProcess():
            should_hot_reload = True
        libTarget.Remove() # Remove the temp target so it doesn't conflict with the real one
        return should_hot_reload

    ###########################################################################
    ###########################################################################
    # forge module load time code
    ###########################################################################

    coloramaColor = 0
    wconioColor = 0

    if sys.platform == "win32":
        try:
            import colorama
            colorama.init()
            coloramaColor = 1
        except ImportError:
            print("\ninstall the package colorama to get color output on Win32")

#       try:
#           import WConio
#           wconioColor = 1
#       except ImportError:
#           print("\ninstall the package WConio to get color output on Win32")

    ###########################################################################
    # defaults
    codegen = 'optimize'
    compiler = 'echo'
    alignment = None    # use default alignment
    clog.pretty = pretty = PRETTY_DEFAULT
    blddep = 1
    vcpkg_include = ""
    vcpkg_lib = ""

    ###########################################################################
    # initializations

    debug_forge = None
    scanIncludePath = []
    targetDictionary = {}
    preContext = None
    depContext = None
    postContext = None
    buildNum = 0
    dlflags = ""
    moduleDict = {}
    cppmap = {}
    linkmap = {}
    includemap = {}
    includeScanSuppress = []
    rootPath = os.getcwd()
    libPath = rootPath
    objPath = rootPath
    binPath = rootPath
    print_depth = 0

    include_re = re.compile(r'^[ \t]*#[ \t]*include[ \t]*(<|")([\w./\\]+)(>|")(.*)$', re.M)
    # seems better to not chase system includes
    #include_re = re.compile(r'^[ \t]*#[ \t]*include[ \t]*(")([\w./\\]+)(")', re.M)

    recursionDepth = -1
    recursionLevel = 0
    printRegistry = 0
    notify = 0
    printCommon = 0
    printDictionary = 0
    cxx_extension = '.cc' # when creating transient targets
    c_ext = 'c|cc|cpp|C|cxx|pmh'
    referenceTime = 0
    path_to_root = ""

    ###########################################################################
    # color support
    RED     = utility.RED
    GREEN   = utility.GREEN
    YELLOW  = utility.YELLOW
    BLUE    = utility.BLUE
    MAGENTA = utility.MAGENTA
    CYAN    = utility.CYAN
    WHITE   = utility.WHITE
    NORMAL  = utility.NORMAL
    BOLD    = utility.BOLD

    def ansi(c):
        return utility.ansi(c)

    def color_on(bold, hue):
        utility.color_on(bold, hue)

    def color_off():
        return utility.color_off()

    def cprint(color, bold, string, eol='\n'):
        return utility.cprint(color, bold, string, eol=eol)

    def initialize():

        # ---------------------------------------------------------------------
        # Globals assigned in this function:

        global api, codegen, apiCodegen, compactApiCodegen, pretty, do_auto, \
            PythonExe, args, rootFile, strip, embed, forcecxx, createpch, \
            as_pch, usepch, usepmh, pchsuf, jobs, auto_serial, \
            configModules, currentFile, buildTimes, fe_config
        # ---------------------------------------------------------------------

        #######################################################################
        # environment variables
        if "CODEGEN" in os.environ:
            codegen = os.environ["CODEGEN"]
        if "PRETTY" in os.environ:
            clog.pretty = pretty = int(os.environ["PRETTY"])
        if "AUTO" in os.environ:
            do_auto = int(os.environ["AUTO"])
        if "PYTHON" in os.environ:
            PythonExe = os.environ["PYTHON"]
        else:
            PythonExe = 'python'

        #######################################################################
        rootFile = 'root.py'
        jobs = 0
        configModules = []
        currentFile = 'forge'
        buildTimes = {}
        args = HandleCommandLine(sys.argv)
        strip = ''
        embed = ''
        forcecxx = ''
        createpch = ''
        as_pch = ''
        usepch = ''
        usepmh = ''
        pchsuf = ''
        fe_config = {}
        auto_serial = 0

        #######################################################################
        # main platform dependent setup
        # LINUX G++ #######################################

        os.environ["FE_TARGET_ARCH"] = utility.determine_target_architecture()

        api = utility.determine_api()
        apiCodegen = api + "_" + codegen

        compactApiCodegen = utility.compact_api()
        if len(compactApiCodegen) > 0:
            compactApiCodegen = "-" + compactApiCodegen
        if codegen != "optimize":
            compactApiCodegen += "-" + codegen

        return

    def reinitialize():
        '''
        Collection of configuration-related actions that should occur after env
        file ingestion has completed and all related effects on os.environ are
        in place.
        '''

        # ---------------------------------------------------------------------
        # Globals assigned in this function:

        global codegen, cxx, cxx_pre, cxx_post, objout, exeout, libincl, \
            nolink, ar, arflags, aroutprefix, objsuf, exesuf, linkformat, \
            libpre, libsuf, libdl, pymodsuf, ld1, ld2, forcecxx, pchsuf, \
            fat1, fat2, winlibs, mod_kill, gllibs, targetRegistry, \
            defaultContext, depContext, defaultCleanContext, cleanTarget, \
            bldTarget, rootPath, dlflags, linker, linkerflags, ccache, \
            fig2dev, outputContext
        # ---------------------------------------------------------------------

        cxx = ''
        cxx_pre = ''
        cxx_post = ''

        if api == "x86_linux" or api == "x86_64_linux":
            if os.path.isfile( '/usr/bin/ccache' ):
                cxx_pre = 'ccache'
                ccache=1
            elif os.path.isfile( '/usr/bin/ccache-swig' ):
                cxx_pre = 'ccache-swig'
                ccache=1
            if os.path.isfile( '/usr/bin/fig2dev' ):
                fig2dev=1
            # fails on athlon (duh):
            #cxx += ' -fPIC -march=pentium3 '
            cxx_post = '-fPIC'
            #cxx_post += ' -ftls-model=global-dynamic'
            objout = '-o'
            exeout = '-o'

            if codegen == 'default' or codegen == '':
                cprint(YELLOW,0,"using default CODEGEN of 'optimize'")
                codegen = "optimize"

            # TODO consider -pedantic
            if codegen == 'validate':
                cppmap['codegen'] = '-D_DEBUG -g3 -Wall -Wextra'

                # NOTE if FE is run indirectly, like through python, fsanitize=address may require something like:
                # % export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libasan.so.5
                cppmap['codegen'] += ' -fsanitize=address -fno-omit-frame-pointer'
            if codegen == 'debug':
                cppmap['codegen'] = '-D_DEBUG -g2 -Wall -Wextra'
            if codegen == 'fast':
                cppmap['codegen'] = '-D_DEBUG -Wall -O0'
            if codegen == 'profile':
                cppmap['codegen'] = '-O3 -Wall'

                # maybe you want debug; maybe you don't
                cppmap['codegen'] += ' -g'

                # gprof profiling
                #cppmap['codegen'] += ' -g -pg'
            if codegen == 'optimize':
                #cppmap['codegen'] = '-march=pentium3 -msse -mmmx -mfpmath=sse'
                #cppmap['codegen'] = '-g -O3 -malign-double -m64'
                # above had " error: -malign-double makes no sense in the 64bit mode"

                cppmap['codegen'] = '-O3 -Wall'
                #cppmap['codegen'] = ' -fno-tree-slp-vectorize'
                #cppmap['codegen'] += ' -g'

                #strip = 'strip --strip-unneeded'

            if api == "x86_64_linux":
                cppmap['codegen'] += ' -m64'

            # whole lotta these; block now, but we should fix eventually
            cppmap['codegen'] += ' -Wno-unused-parameter'

            # tell when overloaded virtuals get hidden
            cppmap['codegen'] += ' -Woverloaded-virtual'

            # questionable approximations
#           cppmap['codegen'] += ' -ffast-math'

            # no colorization
#           cppmap['codegen'] += ' -fno-diagnostics-color'

            # 80 columns
            cppmap['codegen'] += ' -fmessage-length=80'

            libincl = '-L'
            nolink = '-c'
            ar = 'ar'
#           arflags = '-cur'
            arflags = '-cr'
            aroutprefix = ''
            objsuf = '.o'
            exesuf = '.exe'
            linkformat = '-l%s'
            libpre = 'lib'
            libsuf = '.a'
            libdl = '.so'
            pymodsuf = '.so'
            ld1 = '-ldl'
            ld2 = ''
#           forcecxx = '-x c++'     # distcc doesn't like
            fat1 = '-rdynamic -Wl,--export-dynamic -Wl,--whole-archive'
            fat2 = '-Wl,--no-whole-archive'

            # link in every .so even if any of them appear to not be needed
            fat1 += " -Wl,--no-as-needed"
            fat2 += " -Wl,--as-needed"

            winlibs = ''
            gllibs = ''

        # PPC linux ###########################################
        elif api == "ppc_linux":
            if os.path.isfile( '/usr/bin/ccache' ):
                cxx_pre = 'ccache'
                ccache=1
            elif os.path.isfile( '/usr/bin/ccache-swig' ):
                cxx_pre = 'ccache-swig'
                ccache=1
            # fails on athlon (duh):
            #cxx += ' -fPIC -march=pentium3 '
            cxx_post = '-fPIC'
            objout = '-o'
            exeout = '-o'

            # TODO consider -pedantic
            if codegen == 'validate':
                cppmap['codegen'] = '-D_DEBUG -g3 -Wall -Wextra'
            if codegen == 'debug':
                cppmap['codegen'] = '-D_DEBUG -g2 -Wall -Wextra'
            if codegen == 'fast':
                cppmap['codegen'] = '-D_DEBUG -Wall -O0'
            if codegen == 'profile':
                cppmap['codegen'] = '-O3 -Wall'
                cppmap['codegen'] += ' -g'
                # gprof profiling
                #cppmap['codegen'] = ' -pg'
            if codegen == 'optimize':
                #cppmap['codegen'] = '-march=pentium3 -msse -mmmx -mfpmath=sse'
                cppmap['codegen'] = '-O3'
                strip = 'strip --strip-unneeded'

            # whole lotta these; block now, but we should fix eventually
            cppmap['codegen'] += ' -Wno-unused-parameter'

            # questionable approximations
#           cppmap['codegen'] += ' -ffast-math'

            libincl = '-L'
            nolink = '-c'
            ar = 'ar'
            arflags = '-ur'
            aroutprefix = ''
            objsuf = '.o'
            exesuf = '.exe'
            linkformat = '-l%s'
            libpre = 'lib'
            libsuf = '.a'
            libdl = '.so'
            pymodsuf = '.so'
            ld1 = '-ldl'
            ld2 = ''
#           forcecxx = '-x c++'     # distcc doesn't like
            fat1 = '-rdynamic -Wl,--export-dynamic -Wl,--whole-archive'
            fat2 = '-Wl,--no-whole-archive'

            winlibs = ''
            gllibs = ''

        # CYGWIN ###########################################
        elif api == "x86_cygwin":
            objout = '-o'
            exeout = '-o'
            if codegen == 'debug' or codegen == 'validate':
                cppmap['codegen'] = '-g3 -Wall'
            if codegen == 'optimize':
                #cppmap['codegen'] = '-O3 -march=pentium3 -msse -mmmx -mfpmath=sse'
                cppmap['codegen'] = '-O3'

            libincl = '-L'
            nolink = '-c'
            ar = 'ar'
            arflags = '-ur'
            aroutprefix = ''
            objsuf = '.o'
            exesuf = '.exe'
            linkformat = '-l%s'
            libpre = 'lib'
            libsuf = '.a'
            libdl = '.so'
            pymodsuf = '.so'
            ld1 = ''
            ld2 = ''
            fat1 = '-rdynamic -Wl,--export-dynamic -Wl,--whole-archive'
            fat2 = '-Wl,--no-whole-archive'

            winlibs = ''
            gllibs = ''

        # WIN32 VC ########################################
        elif api == "x86_win32" or api == "x86_win64":
            objout = '/Fo'
            exeout = '/Fe'
            dlflags += ' /DLL /MANIFEST'
            add_symbols = False

            # MT or MD
            runtime = os.environ['FE_MS_RT']

            if codegen == 'debug' or codegen == 'validate':
                # /MDd causes initialization failure dialogs (VS express)
                #TODO: Delete these lines and document options instead.
                #cppmap['codegen'] = '/D _DEBUG /MTd /MDd /Od /Gm- /Zi /GZ'
                #cppmap['codegen'] = '/D _DEBUG /MTd /MD /Od /Gm- /Zi /GZ'
                #cppmap['codegen'] = '/D _DEBUG /MD /Zi /EHsc'

                cppmap['codegen'] = '/D _DEBUG /' + runtime + 'd'

                add_symbols = True

            elif codegen == 'optimize':
                #cppmap['codegen'] = '/D NDEBUG /MT /MD /O2 /EHsc'
                # crap, this one is the only one for now w/ TBB:
                #cppmap['codegen'] = '/D _DEBUG /MD /O2 /EHsc'
                #cppmap['codegen'] = '/MT /GS- /GF /fp:precise /O2 /EHsc /Ob1 /Oi /Ot /Oy /GT'
                #cppmap['codegen'] = '/D _DEBUG /MD /GS- /GF /fp:precise /O2 /EHsc /Ob1 /Oi /Ot /Oy /GT'
                #cppmap['codegen'] = '/MD /GS- /GF /O2 /EHsc- /Ob1 /Oi /Ot /Oy'

                # From a win32 game project, this was best:
                # cppmap['codegen'] = '/MT /O2 /Zi /fp:fast'
                # ..but apparently /fp:fast is bad for high precision math
                # consistency.

                cppmap['codegen'] = '/' + runtime
                cppmap['codegen'] += ' /O2'
                cppmap['codegen'] += ' /D NDEBUG'

            else:
                cppmap['codegen'] = '/' + runtime

#           cppmap['codegen'] += ' /Zc:ForScope'
            cppmap['codegen'] += ' /Zc:__cplusplus'
            cppmap['codegen'] += ' /bigobj'

            libincl = '/LIBPATH:'
            nolink = '/c'
            linker = 'link.exe'
            linkerflags = '/NOLOGO'
            arflags = '/NOLOGO'

            # Allow env override for symbols:
            env_key = 'FE_ADD_DEBUG_SYMBOLS'
            env_add_symbols = utility.env(env_key, default='auto')

            if env_add_symbols != 'auto':
                # An override occurred within the env files.
                add_symbols = utility.cfg_as_bool(
                    env_add_symbols, name=env_key)
            # Otherwise, the codegen-derived default is used.
            # The default would likely become platform-sensitive if ever
            # moved out of this win block.

            if add_symbols:
                if codegen == 'optimize' and pretty < 2:
                    cprint(YELLOW, 0, "adding symbols to optimized code")
                cppmap['codegen'] += ' /FS /Zi'
                linkerflags += ' /DEBUG'

            ar = 'lib.exe'
            aroutprefix = '/OUT:'
            objsuf = '.obj'
            exesuf = '.exe'
            linkformat = '%s.lib'
            libpre = ''
            libsuf = '.lib'
            libdl = '.dll'
            pymodsuf = '.pyd'
            ld1 = '/link'
            ld2 = ''
            forcecxx = '/Tp'

            pchsuf = '.pch'
            fat1 = ''
            fat2 = ''
            embed = 'mt.exe /NOLOGO -manifest %s.manifest -outputresource:%s;2'

            winlibs = ''
            gllibs = ''

            cppmap['win32'] ='/nologo'

            # exception handling
#           cppmap['win32'] +=' /EHsc'
            cppmap['win32'] +=' /EHsc-'
            # GX is the same as EHsc
#           cppmap['win32'] +=' /GX'

            if api == "x86_win64":
                cppmap['win64'] = ' /D "WIN64"'
            else:
                cppmap['win32'] += ' /D "WIN32"'
            cppmap['win32'] += ' /D "WIN32_LEAN_AND_MEAN"'

            strmode = utility.env('FE_STRING_MODE', default='unicode')

            if strmode == 'unicode':
                cppmap['win32'] += ' /D _UNICODE /D UNICODE'
            elif strmode == 'mbcs':
                cppmap['win32'] += ' /D "_MBCS"'
            else:
                msg = f"Invalid value for FE_STRING_MODE: {strmode}"
                clog.error(msg)
                raise ValueError(msg)

#           cppmap['win32'] += ' /FD'
            cppmap['win32'] += ' /D "_LIB"'
            cppmap['win32_warning'] = '/W3'
#           cppmap['win64_warning'] = '/Wp64'

        # MAC OSX #########################################
        elif api[-4:] == "_osx":
            cxx_post = '-fPIC -dynamic'
            objout = '-o '
            exeout = '-o '
            if codegen == 'validate':
                cppmap['codegen'] = '-D_DEBUG -g3 -Wall'
            if codegen == 'debug':
                cppmap['codegen'] = '-D_DEBUG -g2 -Wall'
            if codegen == 'fast':
                cppmap['codegen'] = '-D_DEBUG -Wall -O0'
            if codegen == 'profile':
                cppmap['codegen'] = '-O3 -Wall'
                cppmap['codegen'] += ' -g'
            if codegen == 'optimize':
                cppmap['codegen'] = '-O3'
                strip = 'strip -S -x'


            libincl = '-L'
            nolink = '-c'
            ar = 'ar'
            arflags = '-ur'
            aroutprefix = ''
            objsuf = '.o'
            exesuf = '.exe'
            linkformat = '-l%s'
            libpre = 'lib'
            libsuf = '.a'
            libdl = '.dylib'
            pymodsuf = '.so'
            ld1 = '-ldl'
            ld2 = ''
            fat1 = '-bind_at_load'
            fat2 = ''

            winlibs = ''
            gllibs = ''
        else:
            print('unsupported api "' + api + '"')
            sys.exit(1)

        targetRegistry = TargetRegistry()
        mod_kill = []

        #######################################################################
        # Default Context
        #######################################################################
        defaultContext = RuleContext()
        defaultContext.AppendRule(r"(.*)\.(obj|o|pch|gch)$", compile_obj)
#       defaultContext.PrependRule(r"(.*)$", print_divider)
        defaultContext.AppendRule(r"(.*)\.exe$", compile_exe)
        defaultContext.AppendRule(r"(.*)\.(lib|a)$", link_lib)
        defaultContext.AppendRule(r"(.*)\.so$", link_so)
        defaultContext.AppendRule(r"(.*)\.dylib$", link_dylib)
        defaultContext.AppendRule(r"(.*)\.dll$", link_dll)
        defaultContext.AppendRule(r"BUILD:(.*)", build_exe_all)

        # pch and obj removal
        defaultContext.AppendRule(r"(.*)\.exe$", trim_transient)
        defaultContext.AppendRule(r"(.*)\.lib$", trim_transient)
        defaultContext.AppendRule(r"(.*)\.dll$", trim_transient)
        defaultContext.AppendRule(r"(.*)\.so$", trim_transient)
        defaultContext.AppendRule(r"(.*)\.a$", trim_transient)

        depContext = defaultContext

        defaultCleanContext = RuleContext()
        defaultCleanContext.AppendRule(r"(.*)\.(obj|o|exe|lib|a|so|pch|gch|dylib)$", clean_file)
        defaultCleanContext.AppendRule(r"(.*)\.(dll)$", clean_dll_file)
        defaultCleanContext.AppendRule(r"BUILD:(.*)", build_exe_clean)

        outputContext = RuleContext()

        cleanTarget = CleanTarget("clean")
        targetRegistry['clean'] = cleanTarget
        bldTarget = BuildTarget("build")
        targetRegistry['build'] = bldTarget
        printTarget = PrintTarget("print")
        targetRegistry['print'] = printTarget
        if not rootPath:
            rootPath = os.getcwd()
        rootTarget = RootModule()
        targetRegistry['root'] = rootTarget
        rootTarget.help = """\
root target
"""

        # ---------------------------------------------------------------------
        # Establish PCH behavior, sensitive to API designation.

        if os.environ["FE_USE_PCH"] == "auto":
            if api == "x86_win32" or api == "x86_win64":
                os.environ["FE_USE_PCH"] = "1"
            else:
                os.environ["FE_USE_PCH"] = "0"

        # Done with reinitialize().
        return

    def pick_compiler():
        global cxx

        if "FE_CC" in os.environ:
            cxx = os.environ["FE_CC"]
        elif api == "x86_win32" or api == "x86_win64":
            cxx = 'cl.exe'
        elif api[-4:] == "_osx":
            cxx = 'clang++'
        else:
            cxx = 'g++'

    def read_products():
        global rootPath,extensions,modset_roots,mod_kill,pretty
        global product,build_product

        # local.py and products.py

        allow_local_py = False

        scripts = [ "products" ]

        if allow_local_py:
            scripts += [ "local" ]

            local_py = os.path.join(rootPath, "local.py")
            local_py_dist = os.path.join(rootPath, "local.py.dist")

            if not os.path.exists(local_py_dist):
                cprint(YELLOW,1,"missing %s" % local_py_dist)
                sys.exit(1)
            elif not os.path.exists(local_py):
                cprint(CYAN,1,'copying local.py.dist to non-existent local.py')
                shutil.copy(local_py_dist, local_py)
            if not os.path.exists(local_py):
                cprint(RED,1,"missing %s" % local_py)
                sys.exit(1)

            import local as local_py

            local_py_version = 0
            local_py_dist_version = 0
            for line in open(local_py):
                if '# VERSION ' in line:
                    local_py_version = int(line.split(' ')[2].rstrip())
                    break
            for line in open(local_py_dist):
                if '# VERSION ' in line:
                    local_py_dist_version = int(line.split(' ')[2].rstrip())
                    break
            if local_py_version < local_py_dist_version:
                cprint(RED,0,"local.py.dist appears newer than local.py")
                cprint(CYAN,0,"please update your local.py with changes, including the new version number")
            elif local_py_version > local_py_dist_version:
                cprint(RED,0,"local.py has a higher version number than local.py.dist; something may be broken")

        products_list = []

        import products as products_py

        for script in scripts:
            if script == "products":
                products_py.add_products()
                script_function = "add_products"
            else:
                local_py.local_setup()
                script_function = "local_setup"

            script_py = script + ".py"
            modset_list = []
            modset_root_list = modset_roots.split(':')
            local_parent_paths = utility.findModsetFiles(
                rootPath, script_py, modset_root_list)

            for local_parent in local_parent_paths:
                py_mod_loc = os.path.join(local_parent, script_py)
                if not os.path.isfile(py_mod_loc):
                    break
                try:
                    py_mod = utility.import_file_as_module(script, py_mod_loc)

                    try:
                        if hasattr(py_mod, script_function):
                            if script == "products":
                                ok = py_mod.add_products()

                                rel_path = RelativePath(rootPath,local_parent)
                                products_list += [ rel_path ]
                            else:
                                ok = py_mod.local_setup()
                            if ok and ok[:5] == "abort":
                                cprint(RED, 1,
                                    f"ERROR: {local_parent} -- {module.ok}")
                                failed = 1
                                continue
                    except Exception as e:
                        cprint(RED, 1,
                            f"WARNING: {local_parent} {script_py} error")
                        clog.exception(e)
                        continue

                except Exception as e:
                    cprint(RED, 1,
                        f"WARNING: {local_parent} {script_py} failed")
                    clog.exception(e)
                    continue

        if len(products_list):
            cprint(CYAN, 0, f"added products in {' '.join(products_list)}")

        if not build_product in product.keys():
            cprint(RED, 1, f"ERROR: product '{build_product}' was not defined")
            sys.exit(1)

        # NOTE env variables listed in FE_FORCE_ENV are ignored from product
        force_env = []
        if "FE_FORCE_ENV" in os.environ:
            force_env = os.environ["FE_FORCE_ENV"].split(":")
        for key in product[build_product].keys():
            if not key in force_env:
                os.environ[key] = product[build_product][key]
        extensions = ""
        if "FE_EXTENSIONS" in os.environ:
            extensions = os.environ["FE_EXTENSIONS"]

        if "FE_LIST_ONLY" in os.environ and os.environ["FE_LIST_ONLY"] != "0":

            print("")
            cprint(MAGENTA,1,"Products:")

            productNames = [k for k in product]
            productNames.sort()

            for productName in productNames:
                cprint(CYAN,0,productName)

                # TODO: this should only occur when increased verbosity is requested.
                variables = [v for v in product[productName]]
                variables.sort()
                for variable in variables:
                    print('  %s="%s"' % (variable, product[productName][variable]))
                print("")

            sys.exit(0)

        if "FE_MOD_KILL" in os.environ:
            mod_kill = utility.env_list("FE_MOD_KILL")
            if pretty < 2:
                cprint(YELLOW,0,'skipping modules: %s' % " ".join(mod_kill))

    # -------------------------------------------------------------------------
    # for env lookups after default.env and local.env

    def post_initialize():
        global regex,filesystem,cxx,cxx_pre
        global createpch,as_pch,usepch,usepmh,pchsuf,distcc

        pick_compiler()
        evaluate_compiler(None, True)

        if os.path.isfile( '/usr/bin/distcc' ) and "DISTCC_HOSTS" in os.environ and os.environ["DISTCC_HOSTS"] != "":
            if ccache:
                cxx_pre = 'CCACHE_PREFIX="distcc" ' + cxx_pre
            else:
                cxx_pre = 'distcc'
            distcc=1

        regex = 0
        if "FE_BOOST_REGEX" in os.environ:
            if int(os.getenv('FE_BOOST_REGEX')) == 1:
                cprint(YELLOW,0,'boost regex on')
                cppmap['regex'] = '-DFE_BOOST_REGEX=1'
            else:
#               cprint(CYAN,0,'boost regex off')
                cppmap['regex'] = '-DFE_BOOST_REGEX=0'
                regex = 0

        filesystem = 0
        if "FE_BOOST_FILESYSTEM" in os.environ:
            if int(os.getenv('FE_BOOST_FILESYSTEM')) == 1:
                cprint(YELLOW,0,'boost filesystem on')
                cppmap['filesystem'] = '-DFE_BOOST_FILESYSTEM=1'
            else:
#               cprint(CYAN,0,'boost filesystem off')
                cppmap['filesystem'] = '-DFE_BOOST_FILESYSTEM=0'
                filesystem = 0

        if "FE_USE_TSS" in os.environ:
            if int(os.getenv('FE_USE_TSS')) == 1:
                cprint(YELLOW,0,'thread safe storage on')
                cppmap['tss'] = '-DFE_USE_TSS=1'
            else:
#               cprint(CYAN,0,'thread safe storage off')
                cppmap['tss'] = '-DFE_USE_TSS=0'

        if os.getenv('FE_CPPFLAG') != None:
            cppmap['cppflag'] = os.getenv('FE_CPPFLAG')

        if api == "x86_linux" or api == "x86_64_linux":
            if os.getenv('FE_USE_PCH') != "0":
                createpch = '-o'
        elif api == "x86_win32" or api == "x86_win64":
            if os.getenv('FE_USE_PCH') != "0":
                createpch = '/Yc /Fp'
                as_pch = '/Tp'
                usepmh = '/Yu'
                usepch = '/Fp'

    # end post_initialize()

    # -------------------------------------------------------------------------
    def evaluate_compiler(target, verbose):
        global compiler_brand,compiler_version
        global createpch,as_pch,pchsuf,usepmh,usepch

        if pretty > 1:
            verbose = False

        if api[-6:] == "_linux" or api[-4:] == "_osx":

            #* TODO don't change if same cxx string as last time
            just_cxx = resolve_cxx(target)

            command = just_cxx.replace("ccache","") + " --version"
            p = os.popen(command,'r')
            output = p.read()
            n = p.close()
            lines = str(output).split('\n')
            words = lines[0].split()
            compiler_version = 0.0
            cppmap['sse'] = ''
            cppmap['compiler'] = ''
            compiler_brand = ''

            if len(words)>2:
                index = len(words)-1
                while compiler_version<= 0.0 and index > 1:
                    if words[0] == 'icc' or words[0] == 'icpc':
                        index = index - 1
                    if words[index][0] == '(':
                        index = index - 1
                        continue
                    digits = words[index].split('.')
                    if len(digits) > 1:
                        compiler_version = float(digits[0] + '.' + digits[1])
                    else:
                        compiler_version = float(digits[0])
                    if compiler_version >= 1000:
                        compiler_version = 0.0
                    index = index - 1

                compiler_brand = words[0]
                if not utility.known_compiler(compiler_brand) and utility.known_compiler(words[1]):
                    compiler_brand = words[1]
            elif len(words)>1:
                compiler_brand = words[0]

            compiler_brand = compiler_brand.replace("(","").replace(")","")
            compiler_brand = compiler_brand.split("-")[0]

            if compiler_brand == "g++":
                compiler_brand = "gnu"

            if verbose:
                cprint(CYAN,0,"using " + compiler_brand + " " + str(compiler_version) + " compiler")

            cppmap['warning'] = ''
            if compiler_brand == "ICC":
                cppmap['warning'] = '-wd10120'  # sse flag replacements

            if compiler_version >= 4.3:

#               NOTE boost 1.35 not ready for C++0x
#               (debug seems to work; optimize refuses to compile)

#               cprint(CYAN,0,"C++0x: supported")
#               cppmap['std'] += ' -std=c++0x'

#               if verbose:
#                   cprint(CYAN,0,"C++0x: boost not ready")

                cppmap['compiler'] += ' -Wno-deprecated'
#           else:
#               if verbose:
#                   cprint(CYAN,0,"C++0x: not supported by compiler")

            if compiler_version >= 4.0:
                use_visibility=os.getenv('FE_USE_VISIBILITY')
                if use_visibility == '1':
                    if verbose:
                        cprint(CYAN,0,'using GNU class visibility')
                    cppmap['visibility'] = ' -DGCC_HASCLASSVISIBILITY'
                    cppmap['visibility'] += ' -fvisibility-inlines-hidden'
                    cppmap['visibility'] += ' -fvisibility=hidden'

            if compiler_version >= 3.4:
                if createpch != "":
                    if verbose:
                        cprint(CYAN,0,'using GNU pre-compiled headers')
                    cppmap['compiler'] += ' -Winvalid-pch'
#                   cppmap['compiler'] += ' -H'
                    as_pch = '-x c++-header'
                    pchsuf = '.gch'
                elif verbose:
                    cprint(YELLOW,0,'not using pre-compiled headers')

            cppmap['arch'] = ''
            cppmap['sse'] = ''
            cppmap['avx'] = ''

            if compiler_version >= 3.3:
                sse_version = 0.0
                use_ssse3 = False

                if "FE_USE_SSE" in os.environ:
                    sse_string = os.getenv('FE_USE_SSE')
                    if sse_string=="auto":
                        cppmap['arch'] = '-march=native'
                        if verbose:
                            cprint(CYAN,0,'using GNU SSE auto')
                        fe_config['FE_USE_SSE'] = -1
                    else:
                        match=re.compile(r'(.*)s$').match(sse_string)
                        if match != None:
                            use_ssse3 = True
                            sse_string = match.group(1)

                        sse_version = utility.safer_eval(sse_string)
                        fe_config['FE_USE_SSE'] = int(sse_version)

                if sse_version >= 4.0:
                    use_ssse3 = True

                # arch settings are by what we use,
                # not by properly determining the target system
                if sse_version >= 1.0:
                    if verbose:
                        message = ''
                        if use_ssse3 and sse_version==3.0:
                            message = "s"
                        cprint(CYAN,0,'using GNU SSE ' + str(sse_version) + message)

                    if sse_version >= 1.0:
                        cppmap['sse'] += ' -msse -mfpmath=sse'

#                   if sse_version < 2.0:
#                       # for prefetch
#                       cppmap['sse'] += ' -mtune=pentium3'

                    if sse_version >= 2.0:
#                       cppmap['sse'] += ' -msse -msse2 -mmmx -mfpmath=sse'
                        cppmap['sse'] += ' -pipe'
                        cppmap['sse'] += ' -msse2'

                    if sse_version >= 3.0:
                        cppmap['sse'] += ' -msse3'

                    if use_ssse3:
                        cppmap['sse'] += ' -mssse3'

                    if sse_version >= 4.0:
                        cppmap['sse'] += ' -msse4'

                    if sse_version >= 4.1:
                        cppmap['sse'] += ' -msse4.1'

                    if sse_version >= 4.2:
                        cppmap['sse'] += ' -msse4.2'

                    if cppmap['arch'] == '':
                        if sse_version < 3.0 or compiler_version < 4.3:
                            cppmap['arch'] = '-march=k8'
                        else:
                            cppmap['arch'] = '-march=core2'

            if compiler_version >= 4.8:
                avx_version = 0.0

                if "FE_USE_AVX" in os.environ:
                    avx_string = os.getenv('FE_USE_AVX')
                    avx_version = utility.safer_eval(avx_string)
                    fe_config['FE_USE_AVX'] = int(avx_version)

                if compiler_brand == "gnu" and avx_version > 0.0:
                    if verbose:
                        cprint(CYAN,0,'using GNU AVX ' + str(avx_version))

                    if avx_version >= 1.0:
#                       cppmap['arch'] = '-march=corei7-avx'
                        cppmap['avx'] += ' -mavx'
                    if avx_version >= 2.0:
#                       cppmap['arch'] = '-march=core-avx2'
                        cppmap['avx'] += ' -mavx2'
                    if avx_version >= 512.0:
                        cppmap['avx'] += ' -mavx512f -mavx512pf -mavx512er -mavx512cd'

            cppmap['arch'] += ' -mtune=native'

            cppmap['no-tree-slp-vectorize'] = ''
            # TODO figure out what is broken
            if compiler_brand == "gnu" and compiler_version >= 10:
                cppmap['no-tree-slp-vectorize'] = ' -fno-tree-slp-vectorize'

            if compiler_brand == "gnu" and compiler_version >= 6 and codegen == 'optimize':
                cppmap['no-tree-slp-vectorize'] = ' -fno-tree-slp-vectorize'

        elif api == "x86_win32" or api == "x86_win64":
            compiler_brand = "VS"

            cppmap['sse'] = ''
            sse_version = 2.0
            # NOTE compiler always builds win64 with at least SSE2
            if api == "x86_win32":
                if "FE_USE_SSE" in os.environ:
                    sse_string = os.getenv('FE_USE_SSE')
                    if sse_string == "auto":
                        sse_version = 2.0
                    else:
                        sse_version = utility.safer_eval(sse_string)
                    fe_config['FE_USE_SSE'] = int(sse_version)

                if sse_version >= 1.0:
                    if verbose:
                        cprint(CYAN,0,'using SSE' + str(sse_version))
                    if sse_version >= 2.0:
                        cppmap['sse'] = '/arch:SSE2'
                    else:
                        cppmap['sse'] = '/arch:SSE'
                else:
                    # force no sse (SSE2 is the compiler default)
                    cppmap['sse'] = '/arch:IA32'

            avx_version = 0.0
            if "FE_USE_AVX" in os.environ:
                avx_string = os.getenv('FE_USE_AVX')
                avx_version = utility.safer_eval(avx_string)
                fe_config['FE_USE_AVX'] = int(avx_version)
            if avx_version > 0.0:
                if verbose:
                    cprint(CYAN,0,'using AVX ' + str(avx_version))

                if avx_version >= 512.0:
                    cppmap['sse'] = ' /arch:AVX512'
                elif avx_version >= 2.0:
                    cppmap['sse'] = ' /arch:AVX2'
                elif avx_version >= 1.0:
                    cppmap['sse'] = ' /arch:AVX'

    def reset_buildtime():

        global start_buildtime
        start_buildtime = time.time()

    def get_elapsed_buildtime():

        global start_buildtime,end_buildtime

        end_buildtime = time.time()
        return (end_buildtime - start_buildtime)


    def get_formatted_elapsedtime():

        import math
        duration = get_elapsed_buildtime()
        (temp,hours)   = math.modf(duration / 3600.0)
        (temp,minutes) = math.modf(temp*60.0)
        (temp,seconds) = math.modf(temp*60.0)

        return "Total Elapsed build time: %02d:%02d:%02d   (total seconds: %.2f)" % (hours,minutes,seconds,duration)

    def use_std(value):
        if api == "x86_win32" or api == "x86_win64":
            if value == "c++11":
                # TODO: Improve logically.
                # According MS docs, VS compiler Does not support less than
                # c++17. I think that is the reason for rejecting attempted
                # arguments to craft for 'c++11' with a win-flavored api. But
                # if someone's calling use_std('c++11'), why isn't that treated
                # like an error when what is being requestion cannot be
                # accommodate at all by the underlying compiler. And if what
                # we mean here is that we're going to surreptitiously null out
                # that argument, covertly, because we're not checking or caring
                # what actual compiler is being used, that seems even worse.
                # The practical consequence of this is that we end up with
                # cppmap settings where cppmap['std'] = ''. Without special
                # accommodation elsewhere of that situation, what does that
                # even mean?
                return ""
            return "/std:" + value
        else:
            return "-std=" + value
