/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vdb/vdb.pmh>

#define FE_SAV_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceAccessibleVDB::SurfaceAccessibleVDB(void)
{
}

SurfaceAccessibleVDB::~SurfaceAccessibleVDB(void)
{
}

void SurfaceAccessibleVDB::reset(void)
{
	SurfaceAccessibleBase::reset();
}

sp<SurfaceAccessorI> SurfaceAccessibleVDB::accessor(
	String a_node,Element a_element,String a_name,
	Creation a_create,Writable a_writable)
{
	sp<SurfaceAccessorVDB> spAccessor(new SurfaceAccessorVDB);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);

		if(m_spSurfaceVDB.isNull())
		{
			m_spSurfaceVDB=surface();
		}
		m_spSurfaceVDB->prepareForSample();

		spAccessor->setSurfaceVDB(m_spSurfaceVDB);
		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleVDB::accessor(
	String a_node,Element a_element,Attribute a_attribute,
	Creation a_create,Writable a_writable)
{
	sp<SurfaceAccessorVDB> spAccessor(new SurfaceAccessorVDB);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);

		if(m_spSurfaceVDB.isNull())
		{
			m_spSurfaceVDB=surface();
		}
		m_spSurfaceVDB->prepareForSample();

		spAccessor->setSurfaceVDB(m_spSurfaceVDB);
		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

void SurfaceAccessibleVDB::attributeSpecs(Array<SurfaceAccessibleI::Spec>&
	a_rSpecs,String a_node,SurfaceAccessibleI::Element a_element) const
{
	a_rSpecs.clear();

	//* NOTE a_node ignored

	if(a_element==SurfaceAccessibleI::e_point)
	{
		SurfaceAccessibleI::Spec spec;

		spec.set("P","vector3");
		a_rSpecs.push_back(spec);

		spec.set("cpt","vector3");
		a_rSpecs.push_back(spec);

		spec.set("value","real");
		a_rSpecs.push_back(spec);

//		spec.set("Cd","vector3");
//		a_rSpecs.push_back(spec);
	}
}

BWORD SurfaceAccessibleVDB::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAV_DEBUG
	feLog("SurfaceAccessibleVDB::load \"%s\"\n",a_filename.c_str());
#endif

	m_spSurfaceVDB=NULL;

	outlineClear();

	std::string filename(a_filename.c_str());
	openvdb::io::File vdbFile(filename);

	try
	{
		if(!vdbFile.open())
		{
			feLog("SurfaceAccessibleVDB::load open file failed\n");
			return FALSE;
		}
	}
	catch(const std::exception& std_e)
	{
		feLog("SurfaceAccessibleVDB::load exception:\n  \"%s\"\n",
				std_e.what());
		return FALSE;
	}

#if FE_SAV_DEBUG
	feLog("SurfaceAccessibleVDB::load file opened\n");
#endif

	openvdb::Name name;

	openvdb::io::File::NameIterator it=vdbFile.beginName();
	while(it!=vdbFile.endName())
	{
		name= *it;
		++it;

		openvdb::GridBase::ConstPtr gridPtr=vdbFile.readGrid(name);

//		gridPtr->print(std::cout,TRUE);

		String text;
		text.sPrintf("\"%s\" %s\n",name.c_str(),
				gridPtr->valueType().c_str());
		outlineAppend(text);

		text.sPrintf("  type %s\n",gridPtr->type().c_str());
		outlineAppend(text);

		openvdb::Vec3d voxel3d=gridPtr->voxelSize();
		text.sPrintf("  voxelSize %.3G %.3G %.3G\n",
				voxel3d[0],voxel3d[1],voxel3d[2]);
		outlineAppend(text);

		text.sPrintf("  uniform %d\n",gridPtr->hasUniformVoxels());
		outlineAppend(text);

		text.sPrintf("  active %d\n",gridPtr->activeVoxelCount());
		outlineAppend(text);

		text.sPrintf("  memUsage %d\n",gridPtr->memUsage());
		outlineAppend(text);

		const String creator=gridPtr->getCreator().c_str();
		if(!creator.empty())
		{
			text.sPrintf("  creator %s\n",creator.c_str());
			outlineAppend(text);
		}

		for(openvdb::MetaMap::ConstMetaIterator it = gridPtr->beginMeta();
				it!=gridPtr->endMeta();++it)
		{
			const String metaName=it->first.c_str();
			openvdb::Metadata::Ptr metaPtr = it->second;
			const String valueString=metaPtr->str().c_str();
			text.sPrintf("  [%s] %s\n",
					metaName.c_str(),valueString.maybeQuote().c_str());
			outlineAppend(text);
		}

#if FE_SAV_DEBUG
		feLog("SurfaceAccessibleVDB::load found grid \"%s\"\n",name.c_str());
#endif
	}

	m_gridPtr=vdbFile.readGrid(name);

	return bool(m_gridPtr);
}

sp<SurfaceI> SurfaceAccessibleVDB::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
#if FE_SAV_DEBUG
	feLog("SurfaceAccessibleVDB::surface group \"%s\"\n",a_group.c_str());
#endif

	if(m_spSurfaceVDB.isValid())
	{
		return m_spSurfaceVDB;
	}

	m_spSurfaceVDB=registry()->create("*.SurfaceVDB");
	if(m_spSurfaceVDB.isValid())
	{
		//* TODO better
		void* pPtr=&m_gridPtr;

		sp<TypeMaster> spTypeMaster=registry()->master()->typeMaster();
		Instance instance;
		instance.set(spTypeMaster,pPtr);
		m_spSurfaceVDB->bind(instance);
	}

#if FE_SAV_DEBUG
	feLog("SurfaceAccessibleVDB::surface valid %d\n",m_spSurfaceVDB.isValid());
#endif

	if(m_spSurfaceVDB.isValid())
	{
		m_spSurfaceVDB->setRestrictions(a_restrictions);
	}

	return m_spSurfaceVDB;
}

} /* namespace ext */
} /* namespace fe */
