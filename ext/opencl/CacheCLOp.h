/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_CacheCLOp_h__
#define __opencl_CacheCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief cache ti input the first time and then copy it back thereafter

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT CacheCLOp:
	public OperatorSurfaceCommon,
	public Initialize<CacheCLOp>
{
	public:
				CacheCLOp(void);
virtual			~CacheCLOp(void);

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:

		sp<SurfaceAccessibleI>	m_spSurfaceAccessibleI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_CacheCLOp_h__ */
