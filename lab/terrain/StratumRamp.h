/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumRamp_h__
#define __terrain_StratumRamp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Elevation of a slant

	@ingroup terrain

	y=x
*//***************************************************************************/
class FE_DL_EXPORT StratumRamp: public StratumPlane
{
	public:
						StratumRamp(void)									{}
virtual					~StratumRamp(void)									{}

						//* As StratumI
virtual Real			height(Real x,Real y) const		{ return offset()+x; }
virtual SpatialVector	normal(Real x,Real y) const
						{	return SpatialVector(-0.707f,0.707f,0.0f); }
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumRamp_h__ */
