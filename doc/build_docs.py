#!/usr/bin/python3
import configparser
import multiprocessing
import os
import re
import subprocess
import sys
from optparse import OptionParser

SUCCESS = 0
FAIL = 1

# -----------------------------------------------------------------------------
def getGitRevision():
    cmd = ['git', 'rev-parse', 'HEAD']
    proc = subprocess.run(cmd, capture_output=True, text=True)
    rev = proc.stdout[:8]
    return rev

# -----------------------------------------------------------------------------
def ingestDoxyfile(filename):

    config = configparser.ConfigParser(allow_no_value=True)
    with open(filename, 'r') as fp:
        doxyfile = '[DEFAULT]\n' + fp.read()
    config.read_string(doxyfile)
    return config

# -----------------------------------------------------------------------------
def buildDoxygen(update):

    revFilename     = '_lastrevision'
    doxlogFilename  = 'doxygen.log'
    indexFilename   = 'index.html'
    loghtmlFilename = 'doxygenlog.html'
    outputPath      = os.path.join('.', 'doxyout')

    rev = getGitRevision()

    logLink = '<A href="{}">doxygen log</A>'.format(loghtmlFilename)
    with open('_version.dox', 'w') as fp:
        fp.write("/** @page versioninfo Version Information\n")
        fp.write("<HR>\n@b src @b revision: {} ({})\n".format(
            rev, logLink))
        fp.write("*/\n")

    bBuild = True

    if (update):

        # Determine if the revision is different than that of the last build:
        if (os.path.isfile(revFilename) == True):
            with open(revFilename, 'r') as fp :
                oldrev = fp.read()

            if (rev != oldrev):
                print("skipping build (HEAD rev {} != previous rev " \
                    "{})".format(rev, oldrev))
                bBuild = False

    if (bBuild):

        # Ensure output path exists so that the log file can get created on a
        # first run:
        if (not os.path.isdir(outputPath)):
            os.mkdir(outputPath, 0o0755);

        proc = subprocess.run('doxygen', text=True)
        proc.check_returncode()

        # Groom the logfile so that it's accessible from within the dox.

        filepath = os.path.join(outputPath, doxlogFilename)
        with open(filepath, 'r') as fp:
            log = fp.read()

        filepath = os.path.join(outputPath, 'html', indexFilename)
        with open(filepath, 'r') as fp:
            indexlines = fp.readlines()

        headerText = ''.join(indexlines[:33])
        headerText += '\n<H1>Doxygen Log</H1><HR>\n'
        headerText += '\n<pre>\n'
        footerText = ''.join(indexlines[-2:])
        footerText = '\n</pre>\n' + footerText

        log = headerText + log + footerText

        filepath = os.path.join(outputPath, 'html', loghtmlFilename)
        with open(filepath, 'w') as fp:
            fp.write(log)

    # Put the revision number in a crumb file, for subsequent comparison:
    with open(revFilename, 'w') as fp:
        fp.write(rev)

    return

# -----------------------------------------------------------------------------
def build(directory=None, update=False):

    if directory is None:
        directory = os.getcwd()
    directory = os.path.abspath(directory)

    # Henceforth, operations occur within the specified doxygen directory.
    os.chdir(directory)

    try:
        doxcfg = ingestDoxyfile('Doxyfile')
    except Exception as e:
        print("Error ingesting Doxyfile:")
        print(e)
        return FAIL

    inputdir = doxcfg.get('DEFAULT', 'INPUT')

    for page in (
        ['./attrdox.py', 'attributes.dox'],
        ['./catalogdox.py', 'catalog.dox'],
        ['./compdox.py', 'component.dox'],
    ):

        if not os.path.isfile(page[0]):
            print("Warning: process script missing ('{}')".format(page[0]))
            continue

        cmd = ['python3', page[0], inputdir, page[1]]
        proc = subprocess.run(cmd, capture_output=True, text=True)
        if proc.returncode != SUCCESS:
            print("Error building page '{} {}':".format(*page[:2]))
            print(proc.stderr)
            return FAIL

    try:
        buildDoxygen(update)
    except Exception as e:
        print("Error running doxygen:")
        print(e)
        return FAIL

    return SUCCESS

# -----------------------------------------------------------------------------
def main():

    multiprocessing.set_start_method('spawn')

    # -------------------------------------------------------------------------
    # Configure cli usage.

    usage = "usage: %prog [options]"
    parser = OptionParser(usage=usage)

    parser.add_option('-d', '--dir', dest='dir', default=None,
        help="Designate the doxygen direction, where the build will occur. "
            "Will default to the current working dir, if omitted.")

    parser.add_option('-u', '--update', dest='update', action='store_true',
        default=False,
        help="Indicate that the documentation should only be built if "
            "the HEAD revision is different than the revision of the last "
            "build.")

    (options, args) = parser.parse_args()
    # -------------------------------------------------------------------------

    return build(options.dir, options.update)

# -----------------------------------------------------------------------------
if __name__ == '__main__':
    sys.exit(main())
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
