import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexThreadDLLib",
                "fexSurfaceDLLib" ]

    tests = [ 'xXGen' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xXGen.exe",   "something.xgen",   None,   None) ]
