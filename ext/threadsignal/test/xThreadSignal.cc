/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

using namespace fe;
using namespace fe::ext;

class Handler1:
	virtual public HandlerI
{
	public:
				Handler1(void)		{ setName("HandlerI"); }
virtual	void	handle(Record &signal)
				{
					feLog("Handler1::handle\n");
					milliSleep(0);
					feLog("Handler1::handle done\n");
				}
};

class Handler2:
	virtual public HandlerI
{
	public:
				Handler2(void)			{ setName("Handler2"); }
virtual	void	handle(Record &signal)
				{
					feLog("Handler2::handle %d\n", m_aA(signal));
					milliSleep(0);
					feLog("Handler2::handle done\n");
				}
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
					m_aA.initialize(spLayout->scope(), "a");
				}
	private:
		Accessor<int>	m_aA;
};

int main(int argc,char** argv)
{
	UnitTest unitTest;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		sp<Scope> spScope(new Scope(*spMaster.raw()));

		sp<SignalerI> spSignalerI=spRegistry->create("*.ThreadSignaler");
		UNIT_TEST(spSignalerI.isValid());

		if(spSignalerI.isNull())
		{
			feX(argv[0], "couldn't create ThreadSignaler");
		}

		Accessor<int> a(spScope,"a");

		sp<Layout> spSignal = spScope->declare("SIGNAL");
		sp<Layout> spNullLayout;

		spSignal->populate(a);

		sp<HandlerI> spHandler2(new Handler2);
		sp<HandlerI> spHandler1(new Handler1);

		spSignalerI->insert(spHandler2, spSignal);
		spSignalerI->insert(spHandler1, spNullLayout);
		spSignalerI->insert(spHandler2, spSignal);

		Record signal = spScope->createRecord(spSignal);
		a(signal) = 42;

		spSignalerI->signal(signal);
	}
	catch(Exception &e)
	{
		e.log();
	}
}

