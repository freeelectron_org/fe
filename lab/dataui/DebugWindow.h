/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_DebugWindow_h__
#define __dataui_DebugWindow_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT DebugWindow: public Initialize<DebugWindow>,
		virtual public HandlerI
{
	public:
					DebugWindow(void);
virtual				~DebugWindow(void);

		void		initialize(void);

virtual void		handleSignal(Record &signal, sp<SignalerI> spSignalerI);

	private:
		sp<WindowI>		m_spWindowI;
		sp<DrawI>		m_spDrawI;
		sp<SignalerI>	m_spSubSignalerI;
		Record			m_drawSignal;
		Record			m_winData;
		BWORD			m_bound;
		U32				m_drawCount;

		WindowEvent		m_windowEvent;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_DebugWindow_h__ */

