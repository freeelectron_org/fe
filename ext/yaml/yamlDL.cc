/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <yaml/yaml.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<CatalogReaderYaml>(
			"CatalogReaderI.CatalogReaderYaml.fe.yaml");

	pLibrary->add<CatalogWriterYaml>(
			"CatalogWriterI.CatalogWriterYaml.fe.yaml");

	pLibrary->add<ManifestReaderYaml>(
			"ManifestReaderI.ManifestReaderYaml.fe.yaml");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
