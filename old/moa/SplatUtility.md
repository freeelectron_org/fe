# Splat Primer {#SplatUtility}

## Introduction

The term "splat" in this codebase means setting or accumulating values into an
N dimensional buffer.  In the 2D case, this amounts to the same thing as
painting values to an image.

## Reasons to Choose Splat

The most obvious alternative to splatting to retain the samples as a dataset.
For data visualization, for example, one can then use a plotter (such as
gnuplot), a spreadsheet, or one of any number of data visuailzation tools.
This is often the best choice, although the following reasons are sometimes
strong enough to choose a splatting approach.

### Fixed Memory Size

With a splat the dataset is a fixed length buffer (basically an image in 2D
case).  Size is not a function of time.

This also means that many computational constrained uses cases can run with
fixed (and presumably small) overhead.

### Traditional Image Processing Ecosystem

In the 2D case, image processing tools, libraries, and common techniques are
available.  The are many use cases which can take advantage of this.

This can include well known machine learning techniques to processes "datasets
of datasets" (aka a dataset of images).

### The Human Brain

The brain is very good at detecting patterns in imagery.  Splatting data in
ways that do not generate known visualizations of data has proven useful in
correlating simulation run effects (both in finding bugs, and in calibration)


## Some Use Cases

### Test Results Visualization

This includes simulation test runs with static results (such as plots) as visualization.
Data is simply sampled during a test run and used to set values (to splat).

### Runtime/Realtime Data Visualization

This works the same as above, where state is simply sampled and set, but is
applied to real time visualization, such as a HUD element in a simulator.  Data
can accrue and age out over time, to provide very useful real time telemetry
widgets.

### Lookup Tables

Captured or generated data can be used to fill a table from which to sample
from later.  The data can either be splatted in completely, or splatted in
sparsely and filled out via methods such as image fills.

#### Physics Acceleration

There are cases where machine learning can be used in conjunction with a lookup
table use case to accelerate physics simulation.

_TODO: fill this out more with tire example_

### Regression Tests

Unit Tests can use image (or other) diff approaches to compare results with
known good results to check for regression.

