import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool" ]

def setup(module):

    srcList = [ "shape.pmh",
                "ShapeSelector",
                "shapeDL" ]

    dll = module.DLL( "fexShapeDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    forge.deps( ["fexShapeDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexShapeDL",                   None,       None) ]

#   module.Module( 'test' )
