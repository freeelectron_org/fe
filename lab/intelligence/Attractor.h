/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Attractor_h__
#define __intelligence_Attractor_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Attractor RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Attractor: public Scanner
{
	public:
		Functor<F32>				attraction;
		Functor<F32>				focus;

				Attractor(void)			{ setName("Attractor"); }
virtual	void	addFunctors(void)
				{
					Scanner::addFunctors();

					add(attraction,	FE_SPEC("ai:attraction",
							"Attractive force"));
					add(focus,		FE_SPEC("ai:focus",
							"Distance to point of effect"));
				}
virtual	void	initializeRecord(void)
				{
					Scanner::initializeRecord();

					identifier()="Attractor";

					Observation observationRV;
					observationRV.bind(purview());
					Cylinder cylinderRV;
					cylinderRV.bind(observationRV.shape());

					set(cylinderRV.span(),0.0f,-4.0f,0.0f);
					cylinderRV.baseRadius()=1.0f;
					cylinderRV.endRadius()=5.0f;

					set(autoRotate());

					attraction()=1.0f;
					focus()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Attractor_h__ */
