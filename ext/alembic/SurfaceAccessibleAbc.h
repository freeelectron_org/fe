/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __alembic_SurfaceAccessibleAbc_h__
#define __alembic_SurfaceAccessibleAbc_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Alembic Surface Binding

	@ingroup alembic
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleAbc:
	public SurfaceAccessibleJoint
{
	public:
								SurfaceAccessibleAbc(void)					{}
virtual							~SurfaceAccessibleAbc(void)					{}

								//* as SurfaceAccessibleI

								using SurfaceAccessibleJoint::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

static	String					scopeClass(Alembic::AbcGeom::GeometryScope
										a_geometryScope);
static	String					scopeRate(Alembic::AbcGeom::GeometryScope
										a_geometryScope);

	private:

	class FileContext: public Component
	{
		public:
						FileContext(void);
	virtual				~FileContext(void);

			Alembic::AbcGeom::IArchive	m_archive;
	};

		enum	Mode
				{
					e_scan,
					e_points,
					e_meshes,
					e_curves,
					e_joints
				};

		BWORD					setContext(String a_filename);

		void					visit(Alembic::AbcGeom::IObject a_iObj);

		void					visitProperty(
										Alembic::AbcGeom::IScalarProperty
										a_properties,
										String a_indent,
										String a_parentPath,String a_path,
										Alembic::AbcGeom::GeometryScope
										a_geometryScope);
		void					visitProperty(
										Alembic::AbcGeom::IArrayProperty
										a_properties,
										String a_indent,
										String a_parentPath,String a_path,
										Alembic::AbcGeom::GeometryScope
										a_geometryScope);
		void					visitProperty(
										Alembic::AbcGeom::ICompoundProperty
										a_properties,
										String& a_rIndent,
										String a_parentPath,String a_path);
		void					visitProperties(
										Alembic::AbcGeom::ICompoundProperty
										a_properties,
										String& a_rIndent,
										String a_parentPath,String a_path);

		void					outlineProperty(String a_indent,
										String a_propName,
										String a_interpretation,
										String a_dataType,
										String a_rate,
										I32 a_arrayCount,
										I32 a_sampleCount);

static	String					basenameOf(String a_path);

		BWORD							m_refMode;
		Real							m_fps;
		Real							m_minStep;
		Mode							m_mode;
		I32								m_xformCount;
		I32								m_meshCount;
		I32								m_curveCount;
		I32								m_instanceCount;

		sp<FileContext>					m_spContext;

		String							m_filename;
		SpatialTransform				m_refWorld;
		SpatialTransform				m_currentXform;

		Array<String>					m_pathArray;
		Array<String>					m_matchArray;
		Array<SpatialVector>			m_pointArray;
		Array<SpatialVector>			m_pointRefArray;
		Array<SpatialVector>			m_colorArray;
		Array<SpatialVector>			m_normalArray;
		Array<SpatialVector>			m_uvArray;
		Array<I32>						m_uvLookupArray;
		Array<I32>						m_vertCountArray;
		Array<I32>						m_vertIndexArray;
		Array<Real>						m_radiusArray;
		Array< Array<I32> >				m_primVerts;

		std::map<String, Alembic::AbcGeom::GeometryScope >	m_scopeMap;
		std::map<String, Array<String> >					m_stringArrayMap;
		std::map<String, Array<I32> >						m_integerArrayMap;
		std::map<String, Array<Real> >						m_realArrayMap;
		std::map<String, Array<SpatialVector> >				m_vectorArrayMap;

		std::map<String,I32>			m_firstFaceMap;

		Alembic::AbcGeom::GeometryScope	m_normalScope;
		Alembic::AbcGeom::GeometryScope	m_colorScope;
		Alembic::AbcGeom::GeometryScope	m_uvScope;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __alembic_SurfaceAccessibleAbc_h__ */
