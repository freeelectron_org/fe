import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "Mitosis",
                "Metabolism",
                "LinearReaction",
                "bioDL" ]

    dll = module.DLL( "fexBioDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolLib"
                ]

    forge.deps( ["fexBioDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexBioDL",                 None,       None) ]

