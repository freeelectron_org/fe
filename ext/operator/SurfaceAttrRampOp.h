/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceAttrRampOp_h__
#define __operator_SurfaceAttrRampOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to populate an attribute with ramped values

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAttrRampOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceAttrRampOp>
{
	public:

					SurfaceAttrRampOp(void)									{}
virtual				~SurfaceAttrRampOp(void)								{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceAttrRampOp_h__ */

