/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "platform/dlCore.cc"
#include "Mitosis.h"
#include "Metabolism.h"
#include "LinearReaction.h"
#include "bio.pmh"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
{
}

FE_DL_EXPORT fe::Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<Mitosis>("HandlerI.Mitosis.bio.fe");
	pLibrary->add<Metabolism>("HandlerI.Metabolism.bio.fe");
	pLibrary->add<LinearReaction>("HandlerI.LinearReaction.bio.fe");

	return pLibrary;
}

}
