/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __pyfe_fe_network_h__
#define __pyfe_fe_network_h__

class ClientFS : public FunctionSet<fe::ClientI>
{
	public:
				ClientFS(PyObject *pP) : FunctionSet<fe::ClientI>(pP) {};
		void	setScope(const ScopePtr &scopePtr);
		void	addLayout(const LayoutPtr &layoutPtr);
		void	start(SignalerFS &signalerFS,
			const std::string &host, UWORD port, const LayoutPtr &layoutPtr);
		void	stop(void);
};

class ServerFS : public FunctionSet<fe::ServerI>
{
	public:
				ServerFS(PyObject *pP) : FunctionSet<fe::ServerI>(pP) {};
		void	addLayout(const LayoutPtr &layoutPtr);
		void	start(SignalerFS &signalerFS,
			const std::string &host, UWORD port, const LayoutPtr &layoutPtr);
		void	stop(void);
};

#endif /* __pyfe_fe_network_h__ */

