/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Discard_h__
#define __intelligence_Discard_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Delete entries with discard bit set

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Discard: virtual public HandlerI
{
	public:
				Discard(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater					m_theaterRV;
		RecordArrayView<Matter>	m_matterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Discard_h__ */
