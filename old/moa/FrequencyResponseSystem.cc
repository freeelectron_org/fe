/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

FrequencyResponseSystem::FrequencyResponseSystem(void)
{
}

FrequencyResponseSystem::~FrequencyResponseSystem(void)
{
}

void FrequencyResponseSystem::initialize(void)
{
}

void FrequencyResponseSystem::connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator)
{
	m_spOrchestrator = a_spOrchestrator;
	a_spOrchestrator->connect(this,&FrequencyResponseSystem::start,
		FE_NOTE_START);
}

void FrequencyResponseSystem::start(const t_note_id &a_note_id)
{
	sp<RecordGroup> rg_dataset = m_spOrchestrator->dataset();

	t_note_id m_note_loop_start = m_spOrchestrator->note_id(FE_NOTE_LOOP_START);
	t_note_id m_note_loop_stop = m_spOrchestrator->note_id(FE_NOTE_LOOP_STOP);
	t_note_id m_note_line_start = m_spOrchestrator->note_id(FE_NOTE_LINE_START);
	t_note_id m_note_line_stop = m_spOrchestrator->note_id(FE_NOTE_LINE_STOP);
	t_note_id m_note_datapoint = m_spOrchestrator->note_id(FE_NOTE_DATAPOINT);
	t_note_id m_note_image = m_spOrchestrator->note_id(FE_NOTE_IMAGE);

	AsFrequencyResponse asFrequencyResponse(rg_dataset->scope());
	std::vector<Record>	systems;
	asFrequencyResponse.filter(systems, rg_dataset);

	AsDatasetMeta asDatasetMeta;
	asDatasetMeta.digest(rg_dataset);

	for(unsigned int i_system = 0; i_system < systems.size(); i_system++)
	{
		Record r_system = systems[i_system];

		asDatasetMeta.set("runcode", asFrequencyResponse.name(r_system));

		sp<ext::Evaluator> spStepEval;
		spStepEval = registry()->create("EvaluatorI.EvaluatorStd.fe");
		spStepEval->bind(rg_dataset);
		sp<AsVariable> asVariable = spStepEval->asVariable();
		Record r_mod = spStepEval->create("MOD", asVariable);
		Record r_outer = spStepEval->create("OUTER", asVariable);
		Record r_inner = spStepEval->create("INNER", asVariable);
		spStepEval->compile(asFrequencyResponse.step(r_system).c_str());

		sp<ext::Evaluator> spHomeEval;
		spHomeEval = registry()->create("EvaluatorI.EvaluatorStd.fe");
		spHomeEval->bind(rg_dataset);
		spHomeEval->compile(asFrequencyResponse.home(r_system).c_str());

		// over non power of 2 will be dropped by fft based splats
		unsigned int nSamples = asFrequencyResponse.samples(r_system);
		t_moa_real dt = 1.0/asFrequencyResponse.frequency(r_system);

		m_spOrchestrator->setDT(dt);

		EvalRange inner_range(asFrequencyResponse.inner(r_system));
		EvalRange outer_range(asFrequencyResponse.outer(r_system));

		spHomeEval->evaluate();
		spStepEval->evaluate();

		// frequencies
		for(EvalRange::iterator i_outer = outer_range.begin();
			i_outer != outer_range.end(); i_outer++)
		{
			t_moa_real f = *i_outer;

			m_spOrchestrator->perform(m_note_loop_start);

			for(EvalRange::iterator i_inner = inner_range.begin();
				i_inner != inner_range.end(); i_inner++)
			{
				asVariable->value(r_mod) = 0.0;
				asVariable->value(r_inner) = *i_inner;
				spStepEval->evaluate();

				// RUN-UP
				m_spOrchestrator->step(e_step_no_more_than, 1.0);

				m_spOrchestrator->perform(m_note_line_start);

				t_moa_real st = m_spOrchestrator->getTime();

				// a long enough window, for a frequency
				for(unsigned int i = 0; i < nSamples; i++)
				{
					// simple oscillator driver
					asVariable->value(r_mod) = sin( (2.0*fe::pi) * f *
						(m_spOrchestrator->getTime()-st));
					spStepEval->evaluate();

					// use home eval to reset home state, such as temperatures
					spHomeEval->evaluate();

					m_spOrchestrator->step();

					m_spOrchestrator->perform(m_note_datapoint);


				}
				m_spOrchestrator->perform(m_note_line_stop);
			}
			m_spOrchestrator->perform(m_note_loop_stop);
		}

		m_spOrchestrator->perform(m_note_image);
	}
}

} /* namespace ext */
} /* namespace fe */
