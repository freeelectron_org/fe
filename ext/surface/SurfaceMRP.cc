/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceMRP::SurfaceMRP(void):
	m_divisions(4)
{
#if FE_COUNTED_STORE_TRACKER
	setName("SurfaceSphere::Impact");
#endif

	feLog("SurfaceMRP()\n");
}

Protectable* SurfaceMRP::clone(Protectable* pInstance)
{
	feLog("SurfaceMRP::clone\n");

	SurfaceMRP* pSurfaceMRP= pInstance?
			fe_cast<SurfaceMRP>(pInstance): new SurfaceMRP();

	checkCache();

	SurfaceTriangles::clone(pSurfaceMRP);

	// TODO copy attributes
	pSurfaceMRP->m_divisions=m_divisions;
	pSurfaceMRP->m_sourceTransformArray=m_sourceTransformArray;
	pSurfaceMRP->m_sourceInverseArray=m_sourceInverseArray;
	pSurfaceMRP->m_transformArray=m_transformArray;
	pSurfaceMRP->m_sourceWidthArray=m_sourceWidthArray;
	pSurfaceMRP->m_widthArray=m_widthArray;

	return pSurfaceMRP;
}

void SurfaceMRP::cache(void)
{
	feLog("SurfaceMRP::cache\n");

	if(!m_record.isValid())
	{
		feLog("SurfaceMRP::cache record invalid\n");
		return;
	}

	// TODO prevent SurfaceTriangles from reading records

	SurfaceTriangles::cache();

	deallocate(m_pNormalArray);
	deallocate(m_pVertexArray);
	m_pNormalArray=NULL;
	m_pVertexArray=NULL;
	m_vertices=0;

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_record);

	sp<RecordGroup> spRG=surfaceModelRV.surfacePointRG();
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;

		RecordArrayView<SurfaceTransform> surfaceTransformRAV;
		surfaceTransformRAV.bind(spRA);
		for(SurfaceTransform& surfaceTransformRV: surfaceTransformRAV)
		{
			SpatialVector& at=surfaceTransformRV.at();
			SpatialVector& direction=surfaceTransformRV.direction();
			SpatialVector& up=surfaceTransformRV.up();

			SpatialVector right=cross(direction,up);
//			SpatialVector up2=cross(right,direction);

			U32 index=m_sourceTransformArray.size();
			m_sourceTransformArray.resize(index+1);
			m_sourceInverseArray.resize(index+1);
			m_sourceWidthArray.resize(index+1);

			SpatialTransform& rTransform=m_sourceTransformArray[index];
			SpatialTransform& rInverse=m_sourceInverseArray[index];
			m_sourceWidthArray[index]=surfaceTransformRV.uvw()[2];

			setColumn(0,rTransform,-right);
			setColumn(1,rTransform,up);
			setColumn(2,rTransform,direction);
			setColumn(3,rTransform,at);

			invert(rInverse,rTransform);

			feLog("transform %d\n%s\n",index,c_print(rTransform));
		}
	}

	SpatialVector lastLeft(0.0f,0.0f,0.0f);
	SpatialVector lastRight(0.0f,0.0f,0.0f);
	SpatialVector lastUp(0.0f,1.0f,0.0f);
	U32 size=m_sourceTransformArray.size();
	for(I32 index= -1;index<I32(size);index++)
	{
		const BWORD primer=(index<0);
		const U32 reIndex=primer? size-1: index;
		for(U32 sub=primer? m_divisions-1: 0;sub<m_divisions;sub++)
		{
			const Real t=sub/Real(m_divisions);

			SpatialTransform inv;
			invert(inv,m_sourceTransformArray[reIndex]);

			const U32 backIndex=(reIndex+size-1)%size;
			const U32 nextIndex=(reIndex+1)%size;
			const U32 secondIndex=(reIndex+2)%size;
			SpatialTransform delta=inv*m_sourceTransformArray[nextIndex];

			SpatialTransform partial;
			m_matrixPower.solve(partial,delta,t);

			const SpatialTransform transform=
					m_sourceTransformArray[reIndex]*partial;
			const Real width=Spline<Real>::Cardinal2D(t,
					m_sourceWidthArray[backIndex],
					m_sourceWidthArray[reIndex],
					m_sourceWidthArray[nextIndex],
					m_sourceWidthArray[secondIndex]);

			const SpatialVector beamLeft(0.5f*width,0.0f,0.0f);
			const SpatialVector beamRight(-0.5f*width,0.0f,0.0f);
			const SpatialVector beamUp(0.0f,1.0f,0.0f);

			SpatialVector left;
			SpatialVector right;
			SpatialVector up;

			transformVector(transform,beamLeft,left);
			transformVector(transform,beamRight,right);
			rotateVector(transform,beamUp,up);

			if(!primer)
			{
				const U32 subindex=reIndex*m_divisions+sub;
				m_transformArray.resize(subindex+1);
				m_widthArray.resize(subindex+1);

				m_transformArray[subindex]=transform;
				m_widthArray[subindex]=width;

				//* tesselate
				m_pVertexArray=(SpatialVector*)reallocate(m_pVertexArray,
						(m_vertices+6)*sizeof(SpatialVector));
				m_pNormalArray=(SpatialVector*)reallocate(m_pNormalArray,
						(m_vertices+6)*sizeof(SpatialVector));

				m_pVertexArray[m_vertices]=lastLeft;
				m_pVertexArray[m_vertices+1]=lastRight;
				m_pVertexArray[m_vertices+2]=right;

				m_pVertexArray[m_vertices+3]=lastLeft;
				m_pVertexArray[m_vertices+4]=right;
				m_pVertexArray[m_vertices+5]=left;

				m_pNormalArray[m_vertices]=lastUp;
				m_pNormalArray[m_vertices+1]=lastUp;
				m_pNormalArray[m_vertices+2]=up;

				m_pNormalArray[m_vertices+3]=lastUp;
				m_pNormalArray[m_vertices+4]=up;
				m_pNormalArray[m_vertices+5]=up;

				m_vertices+=6;
			}

			lastLeft=left;
			lastRight=right;
			lastUp=up;
		}
	}
}

SpatialTransform SurfaceMRP::sample(Vector2 a_uv) const
{
	Real u=a_uv[0];
	if(u<0.0f)
	{
		u=0.0f;
	}
	else if(u>1.0f)
	{
		u=1.0f;
	}

	const U32 size=m_sourceTransformArray.size();
	U32 index=U32(u*size);
	const Real t=u*size-index;
	index%=size;

	const SpatialTransform& rInv=m_sourceInverseArray[index];

	const U32 index2=(index+1)%size;
	SpatialTransform delta=rInv*m_sourceTransformArray[index2];

	SpatialTransform partial;
	m_matrixPower.solve(partial,delta,t);

	const Real width=m_sourceWidthArray[index]+
			t*(m_sourceWidthArray[index2]-m_sourceWidthArray[index]);
	SpatialTransform result=
			m_sourceTransformArray[index]*partial;
	translate(result,SpatialVector((a_uv[1]-0.5f)*width,0.0f,0.0f));

	return result;
}

void SurfaceMRP::draw2(sp<DrawI> a_spDrawI,const Color* color) const
{
	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);

	SpatialVector beam[2];
	set(beam[0],-1.0f,0.01f,0.0f);
	set(beam[1],1.0f,0.01f,0.0f);

	U32 size=m_transformArray.size();
	for(U32 index=0;index<size;index++)
	{
		const SpatialTransform& rTransform=m_transformArray[index];

		if(!(index%m_divisions))
		{
			a_spDrawI->drawTransformedAxes(rTransform,3.0f);
		}
		if(index%10==4)
		{
			a_spDrawI->drawTransformedMarker(rTransform,1.0f,yellow);
		}
		beam[0][0]= -0.5f*m_widthArray[index];
		beam[1][0]= 0.5f*m_widthArray[index];
		a_spDrawI->drawTransformedLines(rTransform,NULL,beam,NULL,2,
				DrawI::e_discrete,FALSE,color? color: &white);
	}
}

} /* namespace ext */
} /* namespace fe */
