/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SBO_DEBUG	FALSE
#define FE_SBO_VERBOSE	FALSE

using namespace fe;
using namespace fe::ext;

void SurfaceBindOp::initialize(void)
{
	//* page Config
	catalog<String>("Threads","page")="Config";
	catalog<String>("AutoThread","page")="Config";
	catalog<String>("Paging","page")="Config";
	catalog<String>("StayAlive","page")="Config";
	catalog<String>("Work","page")="Config";

	catalog<String>("BindPrefix")="bind";
	catalog<String>("BindPrefix","suggest")="word";
	catalog<String>("BindPrefix","label")="Bind Attribute Prefix";
	catalog<String>("BindPrefix","page")="Config";
	catalog<bool>("BindPrefix","joined")=true;
	catalog<String>("BindPrefix","hint")=
			"Prefix of attribute names added to output surface describing"
			" where each point on the input surface is attached to"
			" the associated driver.";

	catalog<String>("InputGroup")="";
	catalog<String>("InputGroup","label")="Input Group";
//	catalog<String>("InputGroup","suggest")="pointGroups";	(point or primitive)
	catalog<String>("InputGroup","page")="Config";
	catalog<String>("InputGroup","hint")=
			"Group in Input Surface that limits which"
			" point or primitives are affected."
			"  If no group is specified, the entire input is affected.";

	catalog<String>("DriverGroup")="";
	catalog<String>("DriverGroup","label")="Driver Group";
	catalog<String>("DriverGroup","suggest")="primitiveGroups";
	catalog<String>("DriverGroup","page")="Config";
	catalog<String>("DriverGroup","hint")=
			"Primitive Group in Driver Surface that encompass the binding."
			"  Primitives outside this group will not be"
			" considered during binding."
			"  If no group is specified, the entire driver is made available.";

	//* page Bind
	catalog<bool>("PartitionDriver")=false;
	catalog<String>("PartitionDriver","label")="Partition Driver";
	catalog<String>("PartitionDriver","page")="Bind";
	catalog<bool>("PartitionDriver","joined")=true;
	catalog<String>("PartitionDriver","hint")=
			"Restrict bindings by matching attribute values.";

	catalog<String>("DriverPartitionAttr")="part";
	catalog<String>("DriverPartitionAttr","label")="Driver Attr";
	catalog<String>("DriverPartitionAttr","page")="Bind";
	catalog<String>("DriverPartitionAttr","enabler")="PartitionDriver";
	catalog<bool>("DriverPartitionAttr","joined")=true;
	catalog<String>("DriverPartitionAttr","hint")=
			"Primitive string attribute on driver to use for bind matching."
			"  Generally, these values are just simple names.";

	catalog<String>("InputPartitionAttr")="part";
	catalog<String>("InputPartitionAttr","label")="Input Attr";
	catalog<String>("InputPartitionAttr","page")="Bind";
	catalog<String>("InputPartitionAttr","enabler")="PartitionDriver";
	catalog<String>("InputPartitionAttr","hint")=
			"Point string attribute on input to use for bind matching."
			"  Each value can be a simple name or a full regex pattern."
			"  For example, use \"prefix.*\" for every name starting"
			" with particular value"
			" (the '.' is required to indicate 'any character' and"
			" the '*' means 'repeated without limit')"
			" or \"one|two|three\" to allow any one name from a list."
			"  Be careful with non-letter characters that may have"
			" a special meaning in regular expressions.";

	catalog<bool>("subdiv")=false;
	catalog<String>("subdiv","label")="Subdivide Driver";
	catalog<String>("subdiv","page")="Bind";
	catalog<bool>("subdiv","joined")=true;
	catalog<String>("subdiv","hint")="Apply subdivision to driver surfaces.";

	catalog<I32>("subdivDepth")=1;
	catalog<I32>("subdivDepth","min")=1;
	catalog<I32>("subdivDepth","high")=4;
	catalog<I32>("subdivDepth","max")=10;
	catalog<String>("subdivDepth","label")="Depth";
	catalog<String>("subdivDepth","page")="Bind";
	catalog<String>("subdivDepth","enabler")="subdiv";
	catalog<String>("subdivDepth","hint")=
			"Number of subdivision iterations to apply to driver surfaces.";

	catalog<bool>("bindToPoints")=false;
	catalog<String>("bindToPoints","label")="Bind To Points";
	catalog<String>("bindToPoints","page")="Bind";
	catalog<String>("bindToPoints","hint")=
			"Even if the driver has polygons, bind only to its points.";

	catalog<bool>("Bind Fragments")=false;
	catalog<String>("Bind Fragments","page")="Bind";
	catalog<bool>("Bind Fragments","joined")=true;
	catalog<String>("Bind Fragments","hint")=
			"Bind collections of points or primitives with"
			" matching fragment attribute.";

	catalog<String>("FragmentAttr")="part";
	catalog<String>("FragmentAttr","label")="Attr";
	catalog<String>("FragmentAttr","page")="Bind";
	catalog<bool>("FragmentAttr","joined")=true;
	catalog<String>("FragmentAttr","enabler")="Bind Fragments";
	catalog<String>("FragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives.";

	catalog<I32>("samples")=1;
	catalog<I32>("samples","min")=1;
	catalog<I32>("samples","high")=4;
	catalog<I32>("samples","max")=16;
	catalog<String>("samples","label")="Samples";
	catalog<String>("samples","page")="Bind";
	catalog<String>("samples","enabler")="Bind Fragments";
	catalog<String>("samples","hint")=
			"Name of steps to sample along each fragment.";

	catalog<bool>("generateLocators")=false;
	catalog<String>("generateLocators","label")="Generate Locators";
	catalog<String>("generateLocators","page")="Bind";
	catalog<String>("generateLocators","enabler")="Bind Fragments";
	catalog<bool>("generateLocators","joined")=true;
	catalog<String>("generateLocators","hint")=
			"Add detail string attributes describing the location"
			" and orientation of each fragment.";

	catalog<String>("locatorPrefix")="loc";
	catalog<String>("locatorPrefix","suggest")="word";
	catalog<String>("locatorPrefix","label")="Prefix";
	catalog<String>("locatorPrefix","page")="Bind";
	catalog<String>("locatorPrefix","enabler")="Bind Fragments";
	catalog<String>("locatorPrefix","hint")=
			"Text at the beginning of every locator attribute.";

	catalog<SpatialVector>("locatorOffset")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("locatorOffset","label")="Locator Offset";
	catalog<String>("locatorOffset","page")="Bind";
	catalog<String>("locatorOffset","enabler")="Bind Fragments";
	catalog<String>("locatorOffset","hint")=
			"Translate locators in their local orientation"
			" by this amount.";

	catalog<bool>("Bind Primitives")=false;
	catalog<String>("Bind Primitives","page")="Bind";
	catalog<bool>("Bind Primitives","joined")=true;
	catalog<String>("Bind Primitives","hint")=
			"Affect whole primitives instead of individual points";

	catalog<bool>("Conform")=false;
	catalog<String>("Conform","label")="Conform Primitives";
	catalog<String>("Conform","page")="Bind";
	catalog<bool>("Conform","joined")=true;
	catalog<String>("Conform","hint")=
			"Force all points of each input primitive to bind to"
			" the same subset of driver primitives."
			"  This can be useful to coerce all points on"
			" each input curve to follow the same driver curve(s).";

	catalog<bool>("Scalable")=false;
	catalog<String>("Scalable","page")="Bind";
	catalog<String>("Scalable","hint")=
			"Allow for binding coordinates outside each contact triangle."
			"This is not a common option.";

	catalog<I32>("Bindings")=1;
	catalog<I32>("Bindings","min")=1;
	catalog<I32>("Bindings","high")=100;
	catalog<I32>("Bindings","max")=1e3;
	catalog<String>("Bindings","page")="Bind";
	catalog<bool>("Bindings","joined")=true;
	catalog<String>("Bindings","hint")=
			"Maximum number of attachment contacts.";

	catalog<bool>("Covert")=false;
	catalog<String>("Covert","label")="Covert Bindings";
	catalog<String>("Covert","page")="Bind";
	catalog<String>("Covert","hint")=
			"Store bindings apart from the surface.";

	catalog<String>("PivotPoint")="First";
	catalog<String>("PivotPoint","label")="Pivot Point";
	catalog<String>("PivotPoint","page")="Bind";
	catalog<String>("PivotPoint","choice:0")="First";
	catalog<String>("PivotPoint","choice:1")="Center";
	catalog<String>("PivotPoint","choice:2")="Nearest";
	catalog<bool>("PivotPoint","joined")=true;
	catalog<String>("PivotPoint","hint")=
			"Location used to determine attachment.";

	catalog<SpatialVector>("pivotOffset")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("pivotOffset","label")="Pivot Offset";
	catalog<String>("pivotOffset","page")="Bind";
	catalog<String>("pivotOffset","enabler")="Bind Fragments";
	catalog<bool>("pivotOffset","joined")=true;
	catalog<String>("pivotOffset","hint")=
			"Translate pivot in its local orientation"
			" by this amount.";

	catalog<bool>("snapToDriver")=false;
	catalog<String>("snapToDriver","label")="Snap To Driver";
	catalog<String>("snapToDriver","page")="Bind";
	catalog<String>("snapToDriver","hint")=
			"Snap pivot to Driver Surface."
			"  If Bind To Points is used,"
			" this will still snap the original driver";

	catalog<String>("Metric")="spatial";
	catalog<String>("Metric","page")="Bind";
	catalog<String>("Metric","choice:0")="spatial";
	catalog<String>("Metric","label:0")="Spatial Distance";
	catalog<String>("Metric","choice:1")="uv";
	catalog<String>("Metric","label:1")="UV Match";
	catalog<String>("Metric","hint")=
			"Method for choosing attachment points.";

	catalog<bool>("LimitDistance")=false;
	catalog<String>("LimitDistance","label")="Limit Distance";
	catalog<String>("LimitDistance","page")="Bind";
	catalog<bool>("LimitDistance","joined")=true;
	catalog<String>("LimitDistance","hint")=
			"Dismiss potential bindings beyond a specific distance.";

	catalog<Real>("MaxDistance")=1.0;
	catalog<Real>("MaxDistance","high")=10.0;
	catalog<Real>("MaxDistance","max")=1e6;
	catalog<String>("MaxDistance","label")="To";
	catalog<String>("MaxDistance","page")="Bind";
	catalog<String>("MaxDistance","enabler")="LimitDistance";
	catalog<String>("MaxDistance","hint")=
			"Disallow bindings beyond this distance.";

	catalog< sp<Component> >("Input Surface");
	catalog<String>("Input Surface","label")="Reference Input Surface";
	catalog<String>("Input Surface","iterate")="Input Surfaces";

	catalog< sp<Component> >("Driver Surface");
	catalog<String>("Driver Surface","label")="Reference Driver Surface";

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<String>("Output Surface","iterate")="Output Surfaces";

	catalog< Array< sp<Component> > >("Input Surfaces");
	catalog<String>("Input Surfaces","label")="Reference Input Surfaces";

	catalog< Array< sp<Component> > >("Output Surfaces");
	catalog<String>("Output Surfaces","IO")="output";

	m_spSubdivideOp=registry()->create("OperatorSurfaceI.SubdivideOp");
}

void SurfaceBindOp::handleBind(sp<SignalerI> a_spSignalerI,
	sp<Layout> a_spLayout)
{
	OperatorSurfaceCommon::handleBind(a_spSignalerI,a_spLayout);

	sp<OperatorSurfaceCommon> spCommon=m_spSubdivideOp;
	if(spCommon.isValid())
	{
		spCommon->handleBind(a_spSignalerI,a_spLayout);
	}
}

void SurfaceBindOp::handle(Record& a_rSignal)
{
#if FE_SBO_DEBUG
	feLog("SurfaceBindOp::handle\n");
#endif

	catalog<String>("summary")="";

	const String inputGroup=catalog<String>("InputGroup");
	m_partitionDriver=catalog<bool>("PartitionDriver");
	m_bindPrimitives=catalog<bool>("Bind Primitives");
	m_conform=!m_bindPrimitives && catalog<bool>("Conform");

	const String fragmentAttribute=catalog<String>("FragmentAttr");
	const String driverPartitionAttr=catalog<String>("DriverPartitionAttr");
	const BWORD covert=catalog<bool>("Covert");
	const BWORD bindToPoints=catalog<bool>("bindToPoints");
	const BWORD subdiv=catalog<bool>("subdiv");
	const I32 subdivDepth=subdiv? catalog<I32>("subdivDepth"): 0;

	BWORD warnedConformTriangle=FALSE;

	if(m_bindPrimitives && catalog<bool>("Conform"))
	{
		catalog<String>("warning")+=
				"Conforming only supports point bindings;";
	}

	//* output surface is copy of input surface
	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	m_element=m_bindPrimitives? e_primitive: e_point;
	m_attribute=m_bindPrimitives? e_vertices: e_position;

	//* TODO path also?
	String covertKey=name();

	m_spBindingAccessible=m_spOutputAccessible;
	if(covert)
	{
		sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
				registry()->create("*.SurfaceAccessibleCatalog");
		if(spSurfaceAccessibleCatalog.isValid())
		{
			const I32 outputIteration=
					catalogOrDefault<I32>("Output Surface","iteration",-1);
//			const I32 outputIterations=
//					catalogOrDefault<I32>("Output Surface","iterations",0);

			String covertKeyIt=covertKey;
			if(outputIteration>=0)
			{
				covertKeyIt.catf(":%d",outputIteration);
			}

			spSurfaceAccessibleCatalog->setCatalog(
					registry()->master()->catalog());
			spSurfaceAccessibleCatalog->setKey(covertKeyIt);
			spSurfaceAccessibleCatalog->setPersistent(TRUE);
			m_spBindingAccessible=spSurfaceAccessibleCatalog;
		}
	}

	const String driverGroup=catalog<String>("DriverGroup");

	sp<SurfaceAccessibleI> spDriverAccessible;
	if(!access(spDriverAccessible,"Driver Surface")) return;

	sp<SurfaceAccessorI> spDriverNormals;
	if(!access(spDriverNormals,"Driver Surface",e_point,e_normal) ||
			!spDriverNormals->count())
	{
		catalog<String>("warning")+=
				"Driver Surface has no normals (expect rotational problems);";
	}
	spDriverNormals=NULL;

	const SurfaceI::Restrictions restrictions=bindToPoints?
			SurfaceI::Restrictions(
			SurfaceI::e_excludeCurves | SurfaceI::e_excludePolygons):
			SurfaceI::e_unrestricted;

	if(subdivDepth)
	{
		sp<SurfaceAccessibleI> spSubdividedAccessible=
				subdivide(spDriverAccessible,subdivDepth,0,a_rSignal);
		if(spSubdividedAccessible.isValid())
		{
			m_spDriver=
					spSubdividedAccessible->surface(driverGroup,restrictions);
			m_spSnapDriver=bindToPoints?
					spSubdividedAccessible->surface(driverGroup):m_spDriver;
		}
		else
		{
			m_spDriver=NULL;
			m_spSnapDriver=NULL;
		}
	}
	else
	{
//		if(!access(m_spDriver,spDriverAccessible,driverGroup)) return;
		m_spDriver=spDriverAccessible->surface(driverGroup,restrictions);
		m_spSnapDriver=bindToPoints?
				spDriverAccessible->surface(driverGroup):m_spDriver;
	}

	if(m_spDriver.isNull())
	{
		catalog<String>("error")+="Driver Surface access failed;";
		return;
	}

	if(m_spSnapDriver.isNull())
	{
		catalog<String>("error")+="Snap Driver Surface access failed;";
		return;
	}

	m_spDriver->partitionWith(m_partitionDriver? driverPartitionAttr: "");
	m_spSnapDriver->partitionWith(m_partitionDriver? driverPartitionAttr: "");

	if(m_spDriver->radius()<1e-6)
	{
		catalog<String>("error")+=
				"Driver Surface appears empty (zero radius bounding sphere);";
		return;
	}

	if(m_partitionDriver)
	{
		const String inputPartitionAttr=catalog<String>("InputPartitionAttr");

		sp<SurfaceAccessorI> spInputPartition;
		if(access(spInputPartition,"Input Surface",
				m_element,inputPartitionAttr,e_error))
		{
			I32 simplePartitioning=TRUE;

			std::set<String> patternSet;

			const I32 elementCount=spInputPartition->count();
			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				const String pattern=
						spInputPartition->string(elementIndex);
				if(patternSet.find(pattern)==patternSet.end())
				{
					patternSet.insert(pattern);

					//* naive quick check whether this is a regex pattern
					if(strcspn(pattern.c_str(),"^$.*(|)")!=
							strlen(pattern.c_str()))
					{
						simplePartitioning=FALSE;
						break;
					}
				}
			}
			if(simplePartitioning)
			{
//				feLog("SurfaceBindOp::handle"
//						" using simple partition matching\n");

				catalog<String>("message")+="using simple partition matching;";
				m_spDriver->setSearch("SpatialTreeI.MapTree");
				m_spSnapDriver->setSearch("SpatialTreeI.MapTree");
			}
		}
	}

	if(catalog<String>("Metric")=="uv")
	{
		m_spDriver->prepareForUVSearch();
		m_spSnapDriver->prepareForUVSearch();
	}
	else
	{
		m_spDriver->prepareForSearch();
		m_spSnapDriver->prepareForSearch();
	}

	sp<SurfaceAccessorI> spOutputVertices;
	if((m_conform || m_bindPrimitives) && !access(spOutputVertices,
			m_spOutputAccessible,e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputElements;
	if(!access(spOutputElements,m_spOutputAccessible,
			m_element,m_attribute)) return;

	const U32 elementCount=spOutputElements->count();
	if(!elementCount)
	{
		catalog<String>("error")+="nothing to bind;";
		return;
	}

	const BWORD bindFragments=catalog<bool>("Bind Fragments");
	m_fragmented=(bindFragments && spOutputElements.isValid());

	I32 fragmentCount=1;
	if(m_fragmented)
	{
		spOutputElements->fragmentWith(SurfaceAccessibleI::Element(m_element),
				fragmentAttribute,inputGroup);
		fragmentCount=spOutputElements->fragmentCount();
	}

	if(!access(m_spDriverAccessible,"Driver Surface")) return;

	const I32 samples=m_fragmented? catalog<I32>("samples"): 1;

	Array< sp<SurfaceAccessorI> > faces;
	Array< sp<SurfaceAccessorI> > barys;
	const I32 bindingsAccessed=accessBindings(m_spBindingAccessible,
			faces,barys,catalog<I32>("Bindings")*samples,
			m_bindPrimitives,TRUE,TRUE);
	if(bindingsAccessed<1)
	{
		return;
	}

	const String bindPrefix=catalog<String>("BindPrefix");
	m_peakName.sPrintf("%sPeak",bindPrefix.c_str());

	//* create peak attribute before multi-threading
	sp<SurfaceAccessorI> spOutputPeak;
	if(bindingsAccessed>1)
	{
		access(spOutputPeak,m_spOutputAccessible,m_element,m_peakName);
		spOutputPeak->set(0,Real(0));
		spOutputPeak=NULL;
	}

	String locatorPrefix=catalog<bool>("generateLocators")?
			catalog<String>("locatorPrefix"): "";

	if(catalog<String>("Metric")=="uv")
	{
		if(catalog<I32>("Bindings")>1)
		{
			catalog<String>("warning")+=
					"Multiple bindings not yet supported using UV metric;";
		}
		if(catalog<bool>("Scalable"))
		{
			catalog<String>("warning")+=
					"Scalable bindings not yet supported using UV metric;";
		}
		if(catalog<bool>("generateLocators"))
		{
			catalog<String>("warning")+=
					"Locator generation not yet supported using UV metric;";

			locatorPrefix="";
		}
		if(m_conform)
		{
			catalog<String>("warning")+=
					"Conforming primitives not yet supported using UV metric;";
		}
	}

	const String signature=generateSignature(m_spDriverAccessible);
	storeSignature(m_spOutputAccessible,signature);

	storeParameters(m_spOutputAccessible,driverGroup,fragmentAttribute,
			inputGroup,bindingsAccessed,samples,m_bindPrimitives,bindFragments,
			covert? covertKey: "",locatorPrefix,subdivDepth,bindToPoints);

	sp<SurfaceAccessorI> spDriverVertices;
	if(m_conform)
	{
		if(!access(spDriverVertices,m_spDriverAccessible,
				e_primitive,e_vertices)) return;

		//* default -1 = no master
		m_masterBinding.clear();
		m_masterBinding.resize(elementCount,-1);
		m_pivotIndex.clear();
		m_pivotIndex.resize(elementCount,-1);

		const U32 driverPrimitiveCount=spDriverVertices->count();
		m_driverFacePrimitive.resize(driverPrimitiveCount*2,-1);

		sp<SurfaceAccessorI> spOutputProperties;
		if(!access(spOutputProperties,m_spOutputAccessible,
				e_primitive,e_properties)) return;

		const U32 primitiveCount=spOutputVertices->count();
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			if(!spOutputProperties->integer(primitiveIndex,e_openCurve))
			{
				if(!warnedConformTriangle)
				{
					catalog<String>("warning")+="Conforming only supports"
							" open curves in Input Surface;";
					warnedConformTriangle=TRUE;
				}
				continue;
			}

			const U32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				continue;
			}

			const I32 conformIndex=0;
			const I32 masterIndex=
					spOutputVertices->integer(primitiveIndex,conformIndex);

			for(U32 subIndex=1;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spOutputVertices->integer(primitiveIndex,subIndex);
				m_masterBinding[pointIndex]=masterIndex;
				m_pivotIndex[pointIndex]=pointIndex;
			}

			//* WARNING first index could be too close to a parent curve
			const I32 firstIndex=
					spOutputVertices->integer(primitiveIndex,0);
			if(subCount>3)
			{
				const I32 secondIndex=
						spOutputVertices->integer(primitiveIndex,1);
				m_pivotIndex[firstIndex]=secondIndex;
			}
			else
			{
				m_pivotIndex[firstIndex]=firstIndex;
			}
		}
	}

	const String elementText=m_bindPrimitives? "primitives": "points";
	const Element groupElement=(m_element==e_primitive)?
			e_primitiveGroup: e_pointGroup;

	//* TODO fragment-aware ranges
	//* NOTE conform unsafe by using m_driverFacePrimitive table
	//* NOTE covert access to Catalog is not thread safe
	//* NOTE partition filter setting is per surface
	if(m_fragmented || m_conform || covert || m_partitionDriver)
	{
		//* serial

		sp<SpannedRange> spSerialRange=sp<SpannedRange>(new SpannedRange());

		if(m_fragmented)
		{
			spSerialRange->nonAtomic().add(0,fragmentCount-1);

			catalog<String>("summary")=fragmentCount+String(" fragments");
		}
		else if(inputGroup.empty())
		{
			spSerialRange->nonAtomic().add(0,elementCount-1);

			catalog<String>("summary")=
					I32(elementCount)+String(" ")+elementText;
		}
		else
		{
			//* input group, not fragmented

			sp<SurfaceAccessorI> spElementGroup;
			access(spElementGroup,"Input Surface",groupElement,inputGroup);

			spElementGroup->appendGroupSpans(spSerialRange->nonAtomic());

			catalog<String>("summary")=I32(spElementGroup->count())+
					String(" elements in ")+inputGroup;
		}

		run(-1,spSerialRange);
	}
	else
	{
		setAtomicRange(m_spOutputAccessible,
				m_bindPrimitives? e_pointsOfPrimitives: e_pointsOnly,
				inputGroup);

		OperatorThreaded::handle(a_rSignal);

		if(inputGroup.empty())
		{
			catalog<String>("summary")=I32(elementCount)+
					String(" ")+elementText;
		}
		else
		{
			sp<SurfaceAccessorI> spElementGroup;
			access(spElementGroup,"Input Surface",groupElement,inputGroup);

			catalog<String>("summary")=I32(spElementGroup->count())+
					String(" ")+elementText+" in "+inputGroup;
		}
	}

#if FE_SBO_DEBUG
	feLog("SurfaceBindOp::handle done\n");
#endif
}

sp<SurfaceAccessibleI> SurfaceBindOp::subdivide(
	sp<SurfaceAccessibleI>& a_rspAccessibleI,
	I32 a_depth,I32 a_flatDepth,Record& a_rSignal)
{
	sp<SurfaceAccessibleI> spResultAccessible;

	sp<Catalog> spOtherCatalog=m_spSubdivideOp;
	if(spOtherCatalog.isValid())
	{
#if FALSE
//			spSubdividedAccessible=registry()->create("SurfaceAccessibleI");

		sp<Scope> spScope=registry()->create("Scope");
		spSubdividedAccessible->bind(spScope);
#else
		sp<SurfaceAccessibleI> spSubdividedAccessible=
				registry()->create("*.SurfaceAccessibleCatalog");
#endif

		spOtherCatalog->catalog<bool>("AutoThread")=false;
		spOtherCatalog->catalog<I32>("Threads")=1;
		spOtherCatalog->catalog<I32>("depth")=a_depth;
		spOtherCatalog->catalog<I32>("flatDepth")=a_flatDepth;
//		spOtherCatalog->catalog<bool>("recomputeNormals")=false;
		spOtherCatalog->catalog< sp<Component> >("Input Surface")=
				a_rspAccessibleI;
		spOtherCatalog->catalog< sp<Component> >("Output Surface")=
				spSubdividedAccessible;

		sp<Component> spPriorOutput=surfaceOutput(a_rSignal);

		setSurfaceOutput(a_rSignal,spSubdividedAccessible);

		try
		{
			m_spSubdivideOp->handle(a_rSignal);
		}
		catch(const Exception& e)
		{
			relayMessages();
			throw e;
		}
		relayMessages();

		catalog<String>("message")+=
					spOtherCatalog->catalog<String>("message");
		catalog<String>("warning")+=
					spOtherCatalog->catalog<String>("warning");
		catalog<String>("error")+=
					spOtherCatalog->catalog<String>("error");

		setSurfaceOutput(a_rSignal,spPriorOutput);

		sp<SurfaceAccessorI> spSubdividedPoints;
		if(access(spSubdividedPoints,spSubdividedAccessible,
				e_point,e_position))
		{
//			spSubdividedAccessible->save("bind.rg");

			spResultAccessible=spSubdividedAccessible;
		}
		else
		{
			catalog<String>("error")+="SurfaceBindOp::subdivide"
					" subdivided points are inaccessible;";
		}
	}

	return spResultAccessible;
}

void SurfaceBindOp::relayMessages(void)
{
	sp<Catalog> spOtherCatalog=m_spSubdivideOp;
	if(spOtherCatalog.isValid())
	{
		String text=spOtherCatalog->catalog<String>("message");
		if(!text.empty())
		{
			catalog<String>("message")+="[SubdivideOp] "+text;
			spOtherCatalog->catalog<String>("message")="";
		}
		text=spOtherCatalog->catalog<String>("warning");
		if(!text.empty())
		{
			catalog<String>("warning")+="[SubdivideOp] "+text;
			spOtherCatalog->catalog<String>("warning")="";
		}
		text=spOtherCatalog->catalog<String>("error");
		if(!text.empty())
		{
			catalog<String>("error")+="[SubdivideOp] "+text;
			spOtherCatalog->catalog<String>("error")="";
		}
	}
}

void SurfaceBindOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
	const BWORD snapToDriver=catalog<bool>("snapToDriver");
	const String fragmentAttribute=catalog<String>("FragmentAttr");
	const String inputGroup=catalog<String>("InputGroup");
	const String inputPartitionAttr=catalog<String>("InputPartitionAttr");
	const String pivotPoint=catalog<String>("PivotPoint");
	const String metric=catalog<String>("Metric");
	const BWORD limitDistance=catalog<bool>("LimitDistance");
	const String locatorPrefix=catalog<String>("locatorPrefix");
	const SpatialVector locatorOffset=catalog<SpatialVector>("locatorOffset");
	const SpatialVector pivotOffset=catalog<SpatialVector>("pivotOffset");
	const Real maxDistance=limitDistance? catalog<Real>("MaxDistance"): -1.0;

	sp<SurfaceAccessorI> spOutputElements;

	const BWORD matchUV=(metric=="uv");
	if(matchUV)
	{
		if(!access(spOutputElements,m_spOutputAccessible,
				m_element,e_uv,e_error,e_refuseMissing)) return;
	}
	else
	{
		if(!access(spOutputElements,m_spOutputAccessible,
				m_element,m_attribute,e_error,e_refuseMissing)) return;
	}

#if FE_SBO_VERBOSE
	feLog("SurfaceBindOp::run running thread %d %s\n",
			a_id,a_spRange->brief().c_str());
#endif

	//* NOTE UV metric warnings preceed parallel function
	const I32 samples=m_fragmented? catalog<I32>("samples"): 1;
	const I32 bindingsRequested=matchUV? 1: catalog<I32>("Bindings");
	const BWORD scalable=matchUV? FALSE: catalog<bool>("Scalable");
	const BWORD generateLocators=matchUV? FALSE:
			(m_fragmented && catalog<bool>("generateLocators"));

	Array< sp<SurfaceAccessorI> > faces;
	Array< sp<SurfaceAccessorI> > barys;
	const I32 bindingsAccessed=accessBindings(m_spBindingAccessible,
			faces,barys,bindingsRequested*samples,m_bindPrimitives,FALSE,TRUE);
	if(bindingsAccessed<1)
	{
		return;
	}

	sp<SurfaceAccessorI> spOutputPeak;
	if(bindingsAccessed>1)
	{
		access(spOutputPeak,m_spOutputAccessible,
				m_element,m_peakName,e_error,e_refuseMissing);
	}

	//* TODO don't redo fragmentation
	if(m_fragmented)
	{
		spOutputElements->fragmentWith(SurfaceAccessibleI::Element(m_element),
				fragmentAttribute,inputGroup);
	}

	sp<SurfaceAccessorI> spInputPartition;
	if(m_partitionDriver)
	{
		if(!access(spInputPartition,"Input Surface",
				m_element,inputPartitionAttr,e_error)) return;
	}

	BWORD conform=matchUV? FALSE: m_conform;
	sp<SurfaceAccessorI> spDriverVertices;
	sp<SurfaceAccessorI> spDriverProperties;
	if(conform)
	{
		if(!access(spDriverVertices,m_spDriverAccessible,
				e_primitive,e_vertices)) return;
		if(!spDriverVertices->count())
		{
			spDriverVertices=NULL;
			conform=FALSE;
		}

		if(!access(spDriverProperties,m_spDriverAccessible,
				e_primitive,e_properties)) return;
		if(!spDriverProperties->count())
		{
			spDriverProperties=NULL;
		}
	}

	I32 driverFace= -1;
	Barycenter<Real> driverBary;

	sp<SurfaceAccessibleI::FilterI> spFilter;

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 index=it.value();

		String fragment;
		String locatorPreface;
		I32 filterCount=1;
		if(m_fragmented)
		{
			fragment=spOutputElements->fragment(index);
			if(!spOutputElements->filterWith(fragment,spFilter) ||
					spFilter.isNull())
			{
				feLog("SurfaceBindOp::run element %d no filter\n",index);
				continue;
			}

			filterCount=spFilter->filterCount();
			if(!filterCount)
			{
				feLog("SurfaceBindOp::run"
						" element %d zero filter count\n",index);
				continue;
			}

			locatorPreface=locatorPrefix+"_"+fragment+"_";
		}

		if(m_partitionDriver)
		{
			const I32 reIndex=m_fragmented? spFilter->filter(0): index;

			const String partition=spInputPartition->string(reIndex);
			if(!m_spDriver->setPartitionFilter(partition))
			{
				feLog("SurfaceBindOp::run"
						" element %d failed to partition\n",index);
				continue;
			}
			if(m_spSnapDriver!=m_spDriver &&
					!m_spSnapDriver->setPartitionFilter(partition))
			{
				feLog("SurfaceBindOp::run"
						" element %d failed to partition\n",index);
				continue;
			}

#if FE_SBO_VERBOSE
			feLog("SurfaceBindOp::run element %d filter \"%s\"\n",
					index,partition.c_str());
#endif
		}

		SpatialVector pivot;

		const BWORD pivotPointNearest=(pivotPoint=="Nearest");
		if(pivotPointNearest)
		{
			set(pivot);
			Real bestDistance= -1.0;
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=m_fragmented?
						spFilter->filter(filterIndex): index;
				const U32 subCount=spOutputElements->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=
							spOutputElements->spatialVector(reIndex,subIndex);
					sp<SurfaceI::ImpactI> spImpact=matchUV?
							m_spSnapDriver->nearestPoint(Vector2(point)):
							m_spSnapDriver->nearestPoint(point);

					if(spImpact.isValid())
					{
						const Real distance=spImpact->distance();
						if(bestDistance<0.0 || bestDistance>distance)
						{
							bestDistance=distance;
							pivot=snapToDriver? spImpact->intersection(): point;
						}
					}
				}
			}
			if(bestDistance<0.0)
			{
				feLog("SurfaceBindOp::run"
						" element %d bestDistance zero\n",index);
				continue;
			}
		}
		else if(pivotPoint=="Center")
		{
			U32 sum=0;
			set(pivot);
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=m_fragmented?
						spFilter->filter(filterIndex): index;
				const U32 subCount=spOutputElements->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					pivot+=spOutputElements->spatialVector(reIndex,subIndex);
				}
				sum+=subCount;
			}
			if(!sum)
			{
				feLog("SurfaceBindOp::run"
						" element %d failed to find center\n",index);
				continue;
			}
			pivot*=1.0/Real(sum);
		}
		else
		{
			const I32 reIndex=m_fragmented? spFilter->filter(0): index;
			if(!spOutputElements->subCount(reIndex))
			{
				feLog("SurfaceBindOp::run"
						" element %d zero subCount\n",index);
				continue;
			}

			//* HACK
//			pivot=spOutputElements->spatialVector(reIndex,0);
			pivot=spOutputElements->spatialVector(
					conform? m_pivotIndex[reIndex]: reIndex,0);
		}

		if(snapToDriver && !pivotPointNearest)
		{
			sp<SurfaceI::ImpactI> spImpact=matchUV?
					m_spSnapDriver->nearestPoint(Vector2(pivot)):
					m_spSnapDriver->nearestPoint(pivot);

			if(spImpact.isValid())
			{
				pivot=spImpact->intersection();
			}
		}

		if(conform)
		{
			const I32 reIndex=m_fragmented? spFilter->filter(0): index;
			const I32 masterIndex=m_masterBinding[reIndex];
			if(masterIndex>=0)
			{
				for(I32 binding=0;binding<bindingsAccessed;binding++)
				{
					driverFace=faces[binding]->integer(masterIndex);
					faces[binding]->set(reIndex,driverFace);

					if(driverFace<0)
					{
						continue;
					}

					const I32 driverPrimitiveIndex=
							m_driverFacePrimitive[driverFace];

					if(driverPrimitiveIndex>=0 &&
							spDriverProperties.isValid() &&
							spDriverProperties->integer(
							driverPrimitiveIndex,e_openCurve))
					{
						const U32 subCount=spDriverVertices.isValid()?
								spDriverVertices->subCount(driverFace): 0;

						set(driverBary);
						driverBary[0]=subCount? nearestPointAlongCurve(pivot,
								spDriverVertices,
								driverFace)/(subCount-1.0): 0.0;
						driverBary[1]=1.0-driverBary[0];
					}
					else
					{
						driverBary=barys[binding]->spatialVector(
								masterIndex);
					}

					barys[binding]->set(reIndex,driverBary);
				}

				if(spOutputPeak.isValid())
				{
					const Real peakDistance=spOutputPeak->real(masterIndex);
					spOutputPeak->set(reIndex,peakDistance);
				}

				continue;
			}
		}

		pivot+=pivotOffset;

		Array< sp<SurfaceI::ImpactI> > impacts;
		if(matchUV)
		{
			//* NOTE just one hit
			sp<SurfaceI::ImpactI> spImpact=
					m_spDriver->nearestPoint(Vector2(pivot));
			impacts.push_back(spImpact);
		}
		else
		{
			const U32 maxHits=bindingsAccessed>1? bindingsAccessed+1: 1;

			impacts=m_spDriver->nearestPoints(pivot,maxDistance,maxHits);
		}

		if(samples>1)
		{
			SpatialVector farthest=pivot;
			Real farDistance2=0.0;
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=spFilter->filter(filterIndex);
				const U32 subCount=spOutputElements->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=
							spOutputElements->spatialVector(reIndex,subIndex);
					const Real distance2=magnitudeSquared(point-pivot);
					if(farDistance2<distance2)
					{
						farthest=point;
						farDistance2=distance2;
					}
				}
			}

			const SpatialVector span=farthest-pivot;

			for(I32 sampleIndex=1;sampleIndex<samples;sampleIndex++)
			{
				const SpatialVector point=
						pivot+span*(sampleIndex/Real(samples-1));
				impacts[sampleIndex]=matchUV?
						m_spDriver->nearestPoint(Vector2(point)):
						m_spDriver->nearestPoint(point);
			}
		}

		Real peakDistance=0.0;

		const I32 impactCount=impacts.size();
		for(I32 impactIndex=0;impactIndex<impactCount;impactIndex++)
		{
			const I32 binding=impactIndex;

			I32 face= -1;
			SpatialBary bary;
			set(bary);

			if(impactIndex<impactCount && impacts[impactIndex].isValid())
			{
				sp<SurfaceI::ImpactI>& rspImpact=impacts[impactIndex];
				const Real distance=rspImpact->distance();
				if(peakDistance<distance)
				{
					peakDistance=distance;
				}
				if(!limitDistance || distance<=maxDistance)
				{
					face=rspImpact->triangleIndex();

					sp<SurfaceTriangles::Impact> spTriImpact=rspImpact;
					if(scalable && spTriImpact.isValid())
					{
						const SpatialVector normal=rspImpact->normal();
						const Real apart=
								dot(normal,pivot-rspImpact->intersection());
						const SpatialVector inPlane=pivot-normal*apart;

						bary.solve(spTriImpact->vertex0(),
								spTriImpact->vertex1(),
								spTriImpact->vertex2(),
								inPlane);
					}
					else
					{
						bary=rspImpact->barycenter();
					}

					if(generateLocators)
					{
						const SpatialVector normal=rspImpact->normal();
						const SpatialVector tangent=
								unitSafe(rspImpact->du()+rspImpact->dv());

						SpatialTransform xform;
						makeFrameNormalY(xform,pivot,tangent,normal);
						translate(xform,locatorOffset);

						sp<SurfaceAccessorI> spLocPosition;
						if(!access(spLocPosition,m_spOutputAccessible,
								e_detail,locatorPreface+"P")) return;

						sp<SurfaceAccessorI> spLocNormal;
						if(!access(spLocNormal,m_spOutputAccessible,
								e_detail,locatorPreface+"N")) return;

						sp<SurfaceAccessorI> spLocTangent;
						if(!access(spLocTangent,m_spOutputAccessible,
								e_detail,locatorPreface+"T")) return;

						spLocPosition->set(0,xform.translation());
						spLocNormal->set(0,normal);
						spLocTangent->set(0,tangent);
					}

					if(conform)
					{
						FEASSERT(face<I32(m_driverFacePrimitive.size()));
						m_driverFacePrimitive[face]=
								rspImpact->primitiveIndex();
					}
				}
			}

			if(binding<bindingsAccessed)
			{
				if(m_fragmented)
				{
					for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
					{
						const I32 filtered=spFilter->filter(filterIndex);

						faces[binding]->set(filtered,face);
						barys[binding]->set(filtered,bary);
					}
				}
				else
				{
					faces[binding]->set(index,face);
					barys[binding]->set(index,bary);
				}
			}

#if FE_SBO_VERBOSE
			feLog("SurfaceBindOp::run element %d bind %d/%d"
					" impactCount %d pivot %s face %d bary %s\n",
					index,binding,bindingsAccessed,impactCount,
					c_print(pivot),face,c_print(bary));
#endif
		}

		if(spOutputPeak.isValid())
		{
			if(m_fragmented)
			{
				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 filtered=spFilter->filter(filterIndex);

					spOutputPeak->set(filtered,peakDistance);
				}
			}
			else
			{
				spOutputPeak->set(index,peakDistance);
			}
		}
	}
}

String SurfaceBindOp::retrieveSignature(
	sp<SurfaceAccessibleI>& a_rspAccessibleI)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	String signatureName;
	signatureName.sPrintf("%sSignature",bindPrefix.c_str());

	sp<SurfaceAccessorI> spSignatureAccessor;
	if(!access(spSignatureAccessor,a_rspAccessibleI,
			e_detail,signatureName,e_warning))
	{
		return "";
	}

	return spSignatureAccessor->string(0);
}

void SurfaceBindOp::storeSignature(sp<SurfaceAccessibleI>& a_rspAccessibleI,
	String a_signature)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	String signatureName;
	signatureName.sPrintf("%sSignature",bindPrefix.c_str());

	sp<SurfaceAccessorI> spSignatureAccessor;
	if(!access(spSignatureAccessor,a_rspAccessibleI,
			e_detail,signatureName,e_error,e_createMissing)) return;

	spSignatureAccessor->set(0,a_signature);

}

void SurfaceBindOp::discardSignature(sp<SurfaceAccessibleI>& a_rspAccessibleI)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	String signatureName;
	signatureName.sPrintf("%sSignature",bindPrefix.c_str());

	discard(a_rspAccessibleI,e_detail,signatureName);
}

void SurfaceBindOp::retrieveParameters(sp<SurfaceAccessibleI>& a_rspAccessibleI,
	String& a_rDriverGroup,String& a_rFragmentAttribute,String& a_rInputGroup,
	I32& a_rBindings,I32& a_rSamples,
	BWORD& a_rBindPrimitives,BWORD& a_rBindFragments,
	String& a_rCovertKey,String& a_rLocators,I32& a_rSubdiv,
	BWORD& a_rBindToPoints)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	String attributeName;
	sp<SurfaceAccessorI> spAccessor;

	attributeName.sPrintf("%sDriverGroup",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rDriverGroup=spAccessor->string(0);
	}

	attributeName.sPrintf("%sFragment",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rFragmentAttribute=spAccessor->string(0);
	}

	attributeName.sPrintf("%sInputGroup",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rInputGroup=spAccessor->string(0);
	}

	attributeName.sPrintf("%sCount",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rBindings=spAccessor->integer(0);
	}

	attributeName.sPrintf("%sSamples",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rSamples=spAccessor->integer(0);
	}

	attributeName.sPrintf("%sPrimitives",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rBindPrimitives=spAccessor->integer(0);
	}

	attributeName.sPrintf("%sFragments",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rBindFragments=spAccessor->integer(0);
	}

	attributeName.sPrintf("%sCovert",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rCovertKey=spAccessor->string(0);
	}

	attributeName.sPrintf("%sLocators",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rLocators=spAccessor->string(0);
	}

	attributeName.sPrintf("%sSubdiv",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rSubdiv=spAccessor->integer(0);
	}

	attributeName.sPrintf("%sBindToPoints",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning))
	{
		a_rBindToPoints=spAccessor->integer(0);
	}
}

void SurfaceBindOp::storeParameters(sp<SurfaceAccessibleI>& a_rspAccessibleI,
	String a_driverGroup,String a_fragmentAttribute,String a_inputGroup,
	I32 a_bindCount,I32 a_samples,
	BWORD a_bindPrimitives,BWORD a_bindFragments,
	String a_covertKey,String a_locatorPrefix,I32 a_subdiv,
	BWORD a_bindToPoints)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	String attributeName;
	sp<SurfaceAccessorI> spAccessor;

	attributeName.sPrintf("%sDriverGroup",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_driverGroup);
	}

	attributeName.sPrintf("%sFragment",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_fragmentAttribute);
	}

	attributeName.sPrintf("%sInputGroup",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_inputGroup);
	}

	attributeName.sPrintf("%sCount",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_bindCount);
	}

	attributeName.sPrintf("%sSamples",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_samples);
	}

	attributeName.sPrintf("%sPrimitives",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,(I32)a_bindPrimitives);
	}

	attributeName.sPrintf("%sFragments",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,(I32)a_bindFragments);
	}

	attributeName.sPrintf("%sCovert",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_covertKey);
	}

	attributeName.sPrintf("%sLocators",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_locatorPrefix);
	}

	attributeName.sPrintf("%sSubdiv",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_subdiv);
	}

	attributeName.sPrintf("%sBindToPoints",bindPrefix.c_str());
	if(access(spAccessor,a_rspAccessibleI,
			e_detail,attributeName,e_warning,e_createMissing))
	{
		spAccessor->set(0,a_bindToPoints);
	}
}

void SurfaceBindOp::discardParameters(sp<SurfaceAccessibleI>& a_rspAccessibleI)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	String attributeName;
	sp<SurfaceAccessorI> spAccessor;

	attributeName.sPrintf("%sDriverGroup",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sFragment",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sInputGroup",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sCount",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sSamples",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sPrimitives",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sFragments",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sCovert",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sLocators",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sSubdiv",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);

	attributeName.sPrintf("%sBindToPoints",bindPrefix.c_str());
	discard(a_rspAccessibleI,e_detail,attributeName);
}

I32 SurfaceBindOp::accessBindings(sp<SurfaceAccessibleI>& a_rspAccessibleI,
	Array< sp<SurfaceAccessorI> >& a_rFaces,
	Array< sp<SurfaceAccessorI> >& a_rBarys,
	I32 a_bindCount,BWORD a_bindPrimitives,
	BWORD a_allowCreation,BWORD a_writable)
{
	const SurfaceAccessibleI::Element element=a_bindPrimitives?
			SurfaceAccessibleI::e_primitive:
			SurfaceAccessibleI::e_point;

	const SurfaceAccessibleI::Creation create=a_allowCreation?
			SurfaceAccessibleI::e_createMissing:
			SurfaceAccessibleI::e_refuseMissing;

	U32 bindingsAccessed=0;

	const String bindPrefix=catalog<String>("BindPrefix");

	a_rFaces.resize(a_bindCount);
	a_rBarys.resize(a_bindCount);

	for(I32 binding=0;binding<a_bindCount;binding++)
	{
		String faceName;
		String baryName;
		faceName.sPrintf("%sFace%d",bindPrefix.c_str(),binding);
		baryName.sPrintf("%sBary%d",bindPrefix.c_str(),binding);

		a_rFaces[binding]=a_rspAccessibleI->accessor(element,faceName,create);
		if(a_rFaces[binding].isNull())
		{
			if(a_allowCreation)
			{
				feLog("SurfaceBindOp face attribute"
						" \"%s\" not accessible\n",faceName.c_str());
				catalog<String>("error")=
						"inaccessible face attribute \""+faceName+"\"";
				return -1;
			}
			break;
		}

		a_rFaces[binding]->setWritable(a_writable);

		if(a_allowCreation)
		{
#if FALSE
			//* force immediate creation
			a_rFaces[binding]->set(0,0);
#else
//			const U32 elementCount=a_rFaces[binding]->count();
			const U32 elementCount=m_spOutputAccessible->count(element);
			for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				a_rFaces[binding]->set(elementIndex,I32(-1));
			}
#endif
		}

		a_rBarys[binding]=a_rspAccessibleI->accessor(element,baryName,create);
		if(a_rBarys[binding].isNull())
		{
			if(a_allowCreation)
			{
				feLog("SurfaceBindOp barycentric attribute"
						" \"%s\" not accessible\n",baryName.c_str());
				catalog<String>("error")="inaccessible barycentric"
						" attribute \""+baryName+"\"";
				return -1;
			}
			break;
		}

		a_rBarys[binding]->setWritable(a_writable);

		if(a_allowCreation)
		{
			//* force immediate creation
			a_rBarys[binding]->set(0,Barycenter<Real>(0.0,0.0));
		}

		bindingsAccessed++;
	}

	return bindingsAccessed;
}

void SurfaceBindOp::discardBindings(sp<SurfaceAccessibleI>& a_rspAccessibleI,
	I32 a_bindCount,BWORD a_bindPrimitives)
{
	const String bindPrefix=catalog<String>("BindPrefix");

	const Element element=a_bindPrimitives? e_primitive: e_point;

	for(I32 binding=0;binding<a_bindCount;binding++)
	{
		String faceName;
		String baryName;
		faceName.sPrintf("%sFace%d",bindPrefix.c_str(),binding);
		baryName.sPrintf("%sBary%d",bindPrefix.c_str(),binding);

		discard(a_rspAccessibleI,element,faceName);
		discard(a_rspAccessibleI,element,baryName);
	}
}
