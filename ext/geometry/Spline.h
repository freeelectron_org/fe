/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_Spline_h__
#define __geometry_Spline_h__

#define FE_CARDINALSCALE (0.5f)	// (0.5) tangent length in cardinal spline

namespace fe
{
namespace ext
{
/**************************************************************************//**
	@brief A stateless namespace for assorted spline functions

	@ingroup geometry

	The Cardinal spline was introduced by I.J. Schoenberg in 1973.

	The Cardinal spline guarantees control-point intersection.
	It assumes fairly evenly spaced samples.

	A Catmull-Rom spline appears to be a Cardinal spline with the
	"tension" adjustment fixed at 0.5.
*//***************************************************************************/
template <typename T,typename U=T>
class Spline
{
	public:

		/// Compute f(t) for 0<=t<=1 where f(-1)=p0, f(0)=p1, f(1)=p2, f(2)=p3
static	T Cardinal2D(U t,T p0,T p1,T p2,T p3);
		/// Compute 1st derivitive of f(t)
static	T Cardinal2D_d1(U t,T p0,T p1,T p2,T p3);
		/// Compute 2nd derivitive of f(t)
static	T Cardinal2D_d2(U t,T p0,T p1,T p2,T p3);

static	FE_DL_EXPORT T Basis2D(U a_t,I32 a_count,
			const T* a_pKnot,const T* a_pControl);
};

/******************************************************************************
	letting f(-1) = p0
			f( 0) = p1
			f( 1) = p2
			f( 2) = p3

	returns f(t)  for  0.0 < t < 1.0

	value	= [t^3 t^2 t 1] / -a 2-a a-2   a \  / p0 \
							| 2a a-3 3-2a -a |  | p1 |
							| -a  0   a    0 |  | p2 |
							\  0  1   0    0 /  \ p3 /

			= p0 (-at^3 + 2at^2 - at)
			+ p1 (2t^3 - at^3 - 3t^2 + at^2 + 1)
			+ p2 (-2t^3 + at^3 + 3t^2 - 2at^2 + at)
			+ p3 (at^3 - at^2)

	d1		= p0 (-3at^2 + 4at - a)
			+ p1 (6t^2 - 3at^2 - 6t + 2at)
			+ p2 (-6t^2 + 3at^2 + 6t - 4at + a)
			+ p3 (3at^2 - 2at)

	d2		= p0 (-6at + 4a)
			+ p1 (12t - 6at - 6 + 2a)
			+ p2 (-12t + 6at + 6 - 4a)
			+ p3 (6at - 2a)

******************************************************************************/
template <typename T,typename U>
T Spline<T,U>::Cardinal2D(U t,T p0,T p1,T p2,T p3)
{
	const U t2=t*t;
	const U t3=t2*t;
	const U at=FE_CARDINALSCALE*t;
	const U at2=at*t;
	const U at3=at2*t;
	const U t3_2=2.0f*t3;
	const U at2_2=2.0f*at2;
	const U tt2=3.0f*t2;
	const U mid=t3_2-at3-tt2;

	return	p0*(at2_2-at3-at)+
			p1*(mid+at2+1)+
			p2*(at-at2_2-mid)+
			p3*(at3-at2);
}

template <typename T,typename U>
T Spline<T,U>::Cardinal2D_d1(U t,T p0,T p1,T p2,T p3)
{
	const U at=FE_CARDINALSCALE*t;
	const U t2_6=6.0f*t*t;
	const U at2_3=3.0f*at*t;
	const U at_2=2.0f*at;
	const U at_4=4.0f*at;
	const U t_6=6.0f*t;
	const U mid=t2_6-at2_3-t_6;

	return	p0*(at_4-at2_3-FE_CARDINALSCALE)+
			p1*(mid+at_2)+
			p2*(FE_CARDINALSCALE-mid-at_4)+
			p3*(at2_3-at_2);
}

template <typename T,typename U>
T Spline<T,U>::Cardinal2D_d2(U t,T p0,T p1,T p2,T p3)
{
	const U t_12=12.0f*t;
	const U at_6=6.0f*t*FE_CARDINALSCALE;
	const U a_2=2.0f*FE_CARDINALSCALE;
	const U a_4=4.0f*FE_CARDINALSCALE;
	const U mid=t_12-at_6-6;

	return	p0*(a_4-at_6)+
			p1*(mid+a_2)+
			p2*(-mid-a_4)+
			p3*(at_6-a_2);
}

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_Spline_h__*/
