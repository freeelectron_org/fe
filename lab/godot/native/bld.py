import os
import re
import sys
forge = sys.modules["forge"]

# NOTE directly copied from parent bld.py
# TODO put in a shared python file
def locate_godot():
    confirmed_build = None
    confirmed_cpp = None

    godot_build = os.environ["FE_GODOT_BUILD"]
    godot_cpp = os.environ["FE_GODOT_CPP"]

    if os.path.exists(godot_build):
        confirmed_build = godot_build

    if os.path.exists(godot_cpp):
        confirmed_cpp = godot_cpp

    windows_local = os.environ["FE_WINDOWS_LOCAL"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        if not confirmed_build:
            local_build = windows_local + "/godot"
            if os.path.exists(local_build):
                confirmed_build = local_build
        if not confirmed_cpp:
            local_cpp = windows_local + "/godot-cpp"
            if os.path.exists(local_cpp):
                confirmed_cpp = local_cpp

        if confirmed_build:
            confirmed_build = confirmed_build.replace("/","\\")
        if confirmed_cpp:
            confirmed_cpp = confirmed_cpp.replace("/","\\")

    return (confirmed_build, confirmed_cpp)

def setup(module):

    (confirmed_build, confirmed_cpp) = locate_godot()

    srcList = [ "GodotNode",
                "godotNative.pmh",
                "godotNativeDL" ]

    module.includemap = {}
    module.includemap['godot_native_headers'] = confirmed_build + "/modules/gdnative/include"
    module.includemap['godot_native_cpp'] = confirmed_cpp + "/include"
    module.includemap['godot_native_core'] = confirmed_cpp + "/include/core"
    module.includemap['godot_native_gen'] = confirmed_cpp + "/include/gen"

    dll = module.DLL( "fexGodotNativeDL", srcList )

    # NOTE using forge.libPath in rpath makes the result less portable
    dll.linkmap = {}

    deplibs = forge.corelibs+ [
                "fexMetaDLLib",
                "fexSurfaceDLLib",
                "fexTerminalDLLib", ]

    godot_cpp_lib = "godot-cpp"
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        godot_cpp_lib = "lib" + godot_cpp_lib
        godot_cpp_lib += ".windows"
    else:
        godot_cpp_lib += ".linux"
    if forge.codegen == 'debug':
        godot_cpp_lib += ".debug"
    else:
        godot_cpp_lib += ".release"
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        godot_cpp_lib += ".default.lib"
    else:
        godot_cpp_lib += ".64"

    module.cppmap= { 'std': forge.use_std("c++17") }

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]


        dll.linkmap["godot_native"] = " /LIBPATH:" + confirmed_cpp + "/bin " + godot_cpp_lib
    else:
        dll.linkmap["godot_native"] = " -Wl,-rpath='" + forge.libPath + "/'"
        dll.linkmap["godot_native"] += " -L " + confirmed_cpp + "/bin -lgodot-cpp.linux.debug.64"


    forge.deps( ["fexGodotNativeDLLib"], deplibs )

    # build Godot before the GDNative lib
    relativePath = forge.RelativePath(forge.rootPath,module.modPath)
    modObjPath = os.path.join(forge.objPath, relativePath)
    for src in srcList:
        # HACK
        if not re.compile(r'(.*)\.pmh$').match(src):
            break

        srcTarget = module.FindObjTargetForSrc(src)
        srcTarget.AppendOverrideRule(".*", generate_module)

def auto(module):
    (confirmed_build, confirmed_cpp) = locate_godot()

    if not confirmed_build or not confirmed_cpp:
        return 'not found'

    version_h = os.path.join(confirmed_build, "core", "version.h")
    if not os.path.exists(version_h):
        return 'missing'

    # TODO x86 also
    if forge.fe_os == 'FE_WIN32':
        return 'x64 only'

    # NOTE a generated header doesn't exist if Godot was never built

    test_file = """
#include <Godot.hpp>

int main(void)
{
    return(0);
}
    \n"""

    std_orig = forge.cppmap.get('std', "")
    forge.cppmap['std'] = forge.use_std("c++17")

    forge.includemap['godot_headers'] = confirmed_build + "/modules/gdnative/include"
    forge.includemap['godot_cpp'] = confirmed_cpp + "/include"
    forge.includemap['godot_core'] = confirmed_cpp + "/include/core"
    forge.includemap['godot_gen'] = confirmed_cpp + "/include/gen"

    result = forge.cctest(test_file)

    forge.cppmap['std'] = std_orig

    forge.includemap.pop('godot_headers', None)
    forge.includemap.pop('godot_cpp', None)
    forge.includemap.pop('godot_core', None)
    forge.includemap.pop('godot_gen', None)
    return result
