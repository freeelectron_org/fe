import os
import sys
import shutil
forge = sys.modules["forge"]

def prerequisites():
    return [ "draw" ]

def setup(module):
    module.summary = []

    srcList = [ "opengl.pmh",
                "DrawOpenGL",
                "FontOpenGL",
                "ViewOpenGL",
                "openglDL" ]

    dll = module.DLL( "fexOpenGLDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexDrawDLLib" ]

#   if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
#       found = False
#       if forge.glew_dll:
#           if os.path.exists(forge.glew_dll):
#               binDest = os.path.join(forge.libPath, "glew32.dll")
#               shutil.copyfile(forge.glew_dll, binDest)
#               shutil.copymode(forge.glew_dll, binDest)
#               module.summary += [ "glew" ]
#               found = True
#       if not found:
#           forge.color_on(0, forge.BLUE)
#           module.summary += [ "-glew" ]
#           forge.color_off()

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexThreadDLLib" ]

    # TODO check for -DFE_GL_OPENCL specifically
    if "opencl" in forge.cppmap:
        deplibs += [ "fexOpenCLDLLib" ]

        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            deplibs += [    "fexGeometryDLLib",
                            "fexOperateDLLib",
                            "fexOperatorDLLib",
                            "fexSurfaceDLLib" ]

            dll.linkmap['opencl'] = "OpenCL.lib Cfgmgr32.lib"

    forge.deps( ["fexOpenGLDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexOpenGLDL",              None,       None) ]

#   module.Module('test')

def auto(module):
    forge.linkmap['gfxlibs'] = forge.gfxlibs

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        test_file = """
#include "windows.h"
#include "winuser.h"
#include <gl/glew.h>
#include <gl/GL.h>
#include <gl/glu.h>

int main(void)
{
    return 0;
}
    \n"""
    else:
        test_file = """
#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>

int main(void)
{
    return 0;
}
    \n"""

    result = forge.cctest(test_file)

    forge.linkmap.pop('gfxlibs', None)

    return result
