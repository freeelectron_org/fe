import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool", "thread" ]

def setup(module):
    srcList = [ "draw.pmh",
                "DrawCached",
                "DrawCommon",
                "DrawThreaded",
                "ViewCommon",
                "drawDL" ]

    dll = module.DLL( "fexDrawDL", srcList )

#   dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexThreadDLLib" ]

    forge.deps( ["fexDrawDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexDrawDL",                None,       None) ]

#   module.Module('test')
