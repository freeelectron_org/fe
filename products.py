import os
import re
import sys

import utility
import common

forge = sys.modules["forge"]

# NOTE this file is read AFTER default.env and local.env
# Replacing an env variable here will not change other env variables that
# used that variable in substitution.

def add_products():

    standard = {}
    standard["FE_EXTENSIONS"] = "stdregex"
    standard["FE_EXTENSIONS"] += ":stdthread"
    forge.product["standard"] = standard

    signal = {}
    signal["FE_EXTENSIONS"] = standard["FE_EXTENSIONS"]
    signal["FE_EXTENSIONS"] += ":signal"
    forge.product["signal"] = signal

    datatool = {}
    datatool["FE_EXTENSIONS"] = signal["FE_EXTENSIONS"]
    datatool["FE_EXTENSIONS"] += ":datatool"
    forge.product["datatool"] = datatool

    rtti_off = {}
    rtti_off["FE_RTTI"] = "0"
    rtti_off["FE_EXTENSIONS"] = datatool["FE_EXTENSIONS"]
    forge.product["rtti_off"] = rtti_off

    # NOTE less than datatool (an example of using FE_MOD_KILL)
    math_kill = utility.env_list("FE_MOD_KILL")
    math_kill += [ "src.autoload", "src.data" ]
    math = {}
    math["FE_MOD_KILL"] = ':'.join(math_kill)
    math["FE_EXTENSIONS"] = "stdregex"
    forge.product["math"] = math

    plugin_kill = math_kill + [ "src.math" ]
    plugin = {}
    plugin["FE_MOD_KILL"] = ':'.join(plugin_kill)
    plugin["FE_EXTENSIONS"] = "stdregex"
    forge.product["plugin"] = plugin

    core_kill = plugin_kill + [ "src.plugin" ]
    core = {}
    core["FE_MOD_KILL"] = ':'.join(core_kill)
    forge.product["core"] = core

    platform_kill = core_kill + [ "src.core" ]
    platform = {}
    platform["FE_MOD_KILL"] = ':'.join(platform_kill)
    forge.product["platform"] = platform

    eigen = {}
    eigen["FE_EXTENSIONS"] = "eigen"
    forge.product["eigen"] = eigen

    openal = {}
    openal["FE_EXTENSIONS"] = signal["FE_EXTENSIONS"]
    openal["FE_EXTENSIONS"] += ":audio"
    openal["FE_EXTENSIONS"] += ":openal"
    forge.product["openal"] = openal

    image = {}
    image["FE_EXTENSIONS"] = standard["FE_EXTENSIONS"]
    image["FE_EXTENSIONS"] += ":image"
    forge.product["image"] = image

    openil = {}
    openil["FE_EXTENSIONS"] = image["FE_EXTENSIONS"]
    openil["FE_EXTENSIONS"] += ":openil"
    forge.product["openil"] = openil

    thread = {}
    thread["FE_EXTENSIONS"] = signal["FE_EXTENSIONS"]
    thread["FE_EXTENSIONS"] += ":thread"
    forge.product["thread"] = thread

    threadsignal = {}
    threadsignal["FE_EXTENSIONS"] = thread["FE_EXTENSIONS"]
    threadsignal["FE_EXTENSIONS"] += ":thread"
    threadsignal["FE_EXTENSIONS"] += ":threadsignal"
    forge.product["threadsignal"] = threadsignal

    network = {}
    network["FE_EXTENSIONS"] = signal["FE_EXTENSIONS"]
    network["FE_EXTENSIONS"] += ":network"
    forge.product["network"] = network

    enet = {}
    enet["FE_EXTENSIONS"] = network["FE_EXTENSIONS"]
    enet["FE_EXTENSIONS"] += ":enet"
    forge.product["enet"] = enet

    zeromq = {}
    zeromq["FE_EXTENSIONS"] = network["FE_EXTENSIONS"]
    zeromq["FE_EXTENSIONS"] += ":zeromq"
    forge.product["zeromq"] = zeromq

    shm = {}
    shm["FE_EXTENSIONS"] = network["FE_EXTENSIONS"]
    shm["FE_EXTENSIONS"] += ":shm"
    forge.product["shm"] = shm

    netsignal = {}
    netsignal["FE_EXTENSIONS"] = network["FE_EXTENSIONS"]
    netsignal["FE_EXTENSIONS"] += ":netsignal"
    netsignal["FE_EXTENSIONS"] += ":datatool"
    forge.product["netsignal"] = netsignal

    nethost = {}
    nethost["FE_EXTENSIONS"] = forge.product["network"]["FE_EXTENSIONS"]
    nethost["FE_EXTENSIONS"] += ":networkhost"
    nethost["FE_EXTENSIONS"] += ":shm"
    nethost["FE_EXTENSIONS"] += ":zeromq"
    forge.product["nethost"] = nethost

    message = {}
    message["FE_EXTENSIONS"] = "stdregex"
    message["FE_EXTENSIONS"] += ":message"
    forge.product["message"] = message

    beacon = {}
    beacon["FE_EXTENSIONS"] = forge.product["nethost"]["FE_EXTENSIONS"]
    beacon["FE_EXTENSIONS"] += ":beacon"
    beacon["FE_EXTENSIONS"] += ":message"
    beacon["FE_EXTENSIONS"] += ":datatool"
    beacon["FE_EXTENSIONS"] += ":signal"
    beacon["FE_EXTENSIONS"] += ":window"
    forge.product["beacon"] = beacon

    guide = {}
    guide["FE_EXTENSIONS"] = forge.product["beacon"]["FE_EXTENSIONS"]
    guide["FE_EXTENSIONS"] += ":guide"
    forge.product["guide"] = guide

    samples = {}
    samples["FE_EXTENSIONS"] = "sample_text"
    forge.product["samples"] = samples

    yaml = {}
    yaml["FE_EXTENSIONS"] = "yaml"
    forge.product["yaml"] = yaml

    draw = {}
    draw["FE_EXTENSIONS"] = datatool["FE_EXTENSIONS"]
    draw["FE_EXTENSIONS"] += ":draw"
    draw["FE_EXTENSIONS"] += ":thread"
    forge.product["draw"] = draw

    evaluate = {}
    evaluate["FE_EXTENSIONS"] = datatool["FE_EXTENSIONS"]
    evaluate["FE_EXTENSIONS"] += ":evaluate"
    forge.product["evaluate"] = evaluate

    freetype = {}
    freetype["FE_EXTENSIONS"] = draw["FE_EXTENSIONS"]
    freetype["FE_EXTENSIONS"] += ":freetype"
    forge.product["freetype"] = freetype

    geometry = {}
    geometry["FE_EXTENSIONS"] = draw["FE_EXTENSIONS"]
    geometry["FE_EXTENSIONS"] += ":geometry"
    geometry["FE_EXTENSIONS"] += ":yaml"
    forge.product["geometry"] = geometry

    surface = {}
    surface["FE_EXTENSIONS"] = geometry["FE_EXTENSIONS"]
    surface["FE_EXTENSIONS"] += ":image"
    surface["FE_EXTENSIONS"] += ":shape"
    surface["FE_EXTENSIONS"] += ":surface"
    forge.product["surface"] = surface

    json = {}
    json["FE_EXTENSIONS"] = surface["FE_EXTENSIONS"]
    json["FE_EXTENSIONS"] += ":json"
    forge.product["json"] = json

    lua = {}
    lua["FE_EXTENSIONS"] = geometry["FE_EXTENSIONS"]
    lua["FE_EXTENSIONS"] += ":lua"
    lua["FE_EXTENSIONS"] += ":shape"
    lua["FE_EXTENSIONS"] += ":surface"
    forge.product["lua"] = lua

    usd = {}
    usd["FE_EXTENSIONS"] = geometry["FE_EXTENSIONS"]
    usd["FE_EXTENSIONS"] += ":opengl"
    usd["FE_EXTENSIONS"] += ":shape"
    usd["FE_EXTENSIONS"] += ":surface"
    usd["FE_EXTENSIONS"] += ":usd"
    forge.product["usd"] = usd

    operator = {}
    operator["FE_EXTENSIONS"] = surface["FE_EXTENSIONS"]
    operator["FE_EXTENSIONS"] += ":json"
    operator["FE_EXTENSIONS"] += ":lua"
    operator["FE_EXTENSIONS"] += ":meta"
    operator["FE_EXTENSIONS"] += ":operate"
    operator["FE_EXTENSIONS"] += ":operator"
    operator["FE_EXTENSIONS"] += ":window"
    forge.product["operator"] = operator

    modeling = {}
    modeling["FE_EXTENSIONS"] = operator["FE_EXTENSIONS"]
    modeling["FE_EXTENSIONS"] += ":boostregex"
    modeling["FE_EXTENSIONS"] += ":boostthread"
#   modeling["FE_EXTENSIONS"] += ":glew"
    modeling["FE_EXTENSIONS"] += ":opengl"
#   modeling["FE_EXTENSIONS"] += ":pcre"
    modeling["FE_EXTENSIONS"] += ":usd"
    forge.product["modeling"] = modeling

    godot = {}
    godot["FE_EXTENSIONS"] = operator["FE_EXTENSIONS"]
    godot["FE_EXTENSIONS"] += ":godot"
    godot["FE_EXTENSIONS"] += ":network"
    godot["FE_EXTENSIONS"] += ":terminal"
    godot["FE_EXTENSIONS"] += ":zeromq"
    forge.product["godot"] = godot

    terminal = {}
    terminal["FE_EXTENSIONS"] = modeling["FE_EXTENSIONS"]
    terminal["FE_EXTENSIONS"] += ":grass:ironworks:vegetation"
    terminal["FE_EXTENSIONS"] += ":alembic"
    terminal["FE_EXTENSIONS"] += ":oiio"
    terminal["FE_EXTENSIONS"] += ":openil"
    terminal["FE_EXTENSIONS"] += ":opensubdiv"
    terminal["FE_EXTENSIONS"] += ":tbb"
    terminal["FE_EXTENSIONS"] += ":terminal"
    terminal["FE_EXTENSIONS"] += ":tiff"
    terminal["FE_EXTENSIONS"] += ":xml"
    forge.product["terminal"] = terminal

    no_rtti = {}
    no_rtti["FE_RTTI"] = "0"
    no_rtti["FE_EXTENSIONS"] = operator["FE_EXTENSIONS"]
    no_rtti["FE_EXTENSIONS"] += ":audio"
    no_rtti["FE_EXTENSIONS"] += ":fbx"
    no_rtti["FE_EXTENSIONS"] += ":grass"
    no_rtti["FE_EXTENSIONS"] += ":ironworks"
    no_rtti["FE_EXTENSIONS"] += ":native"
    no_rtti["FE_EXTENSIONS"] += ":network"
    no_rtti["FE_EXTENSIONS"] += ":oiio"
    no_rtti["FE_EXTENSIONS"] += ":openal"
    no_rtti["FE_EXTENSIONS"] += ":opencl"
    no_rtti["FE_EXTENSIONS"] += ":opengl"
    no_rtti["FE_EXTENSIONS"] += ":openil"
    no_rtti["FE_EXTENSIONS"] += ":opview"
    no_rtti["FE_EXTENSIONS"] += ":ray"
    no_rtti["FE_EXTENSIONS"] += ":terminal"
    no_rtti["FE_EXTENSIONS"] += ":tiff"
    no_rtti["FE_EXTENSIONS"] += ":vegetation"
    no_rtti["FE_EXTENSIONS"] += ":viewer"
    no_rtti["FE_EXTENSIONS"] += ":xml"
    no_rtti["FE_EXTENSIONS"] += ":zeromq"
    forge.product["no_rtti"] = no_rtti

    # NOTE build FE with the same compiler/version that Unreal will use
    # mixing Visual Studio versions can cause strange errors
    unreal = {}
#   unreal["FE_RTTI"] = "0"     # usd requires compiler RTTI
    unreal["FE_TYPEID"] = "0"   # avoid compiler based typeid
    if forge.api == "x86_linux" or forge.api == "x86_64_linux":
        unreal["FE_CC"] = "clang++"
    elif forge.api == 'x86_win32' or forge.api == 'x86_win64':
        unreal["FE_MEM_ALIGNMENT"] = "8"
        unreal["FE_USE_PRINTF"] = "0"

#       usd_include = os.environ["FE_USD_INCLUDE"]
#       unreal["FE_USD_INCLUDE"] = os.path.dirname(usd_include) + "_Zp8/" + os.path.basename(usd_include)

        unreal_build = os.environ["FE_UNREAL_BUILD"]
        unreal["FE_USD_INCLUDE"] = unreal_build + "/Engine/Plugins/Importers/USDImporter/Source/ThirdParty/USD/include"
        unreal["FE_USD_LIB"] = unreal_build + "/Engine/Binaries/Win64"
        unreal["FE_USD_USE_UNREAL"] = "1"
    unreal["FE_LIB"] = os.path.realpath(os.path.abspath("lab/unreal/unreal_plugins/FreeElectron/FEOps/Source/fe"))
    unreal["FE_LIB_PATH"] = unreal["FE_LIB"] + "/lib/" + os.environ["FE_API_CODEGEN"]
    unreal["FE_EXTENSIONS"] = operator["FE_EXTENSIONS"]
    unreal["FE_EXTENSIONS"] += ":fbx"
    unreal["FE_EXTENSIONS"] += ":opencl"
    unreal["FE_EXTENSIONS"] += ":opengl"    # required for usd module
    unreal["FE_EXTENSIONS"] += ":terminal"
    unreal["FE_EXTENSIONS"] += ":unreal"
    unreal["FE_EXTENSIONS"] += ":usd"       # requires compiler RTTI
    unreal["FE_EXTENSIONS"] += ":vegetation"
    forge.product["unreal"] = unreal

    houdini = {}
#   houdini["FE_HOUDINI_LIBS"] += "fexHoudiniDL${HOUDINI_VERSION}" ... TODO
    houdini["FE_EXTENSIONS"] = modeling["FE_EXTENSIONS"]
    houdini["FE_EXTENSIONS"] += ":houdini"
    houdini["FE_EXTENSIONS"] += ":opencl"
    houdini["FE_EXTENSIONS"] += ":tbb"
    forge.product["houdini"] = houdini

    maya = {}
#   maya["FE_MAYA_LIBS"] += "fexMayaDL"
    maya["FE_EXTENSIONS"] = modeling["FE_EXTENSIONS"]
    maya["FE_EXTENSIONS"] += ":maya:tbb"
    maya["FE_EXTENSIONS"] += ":opencl"
    maya["FE_EXTENSIONS"] += ":tbb"
    forge.product["maya"] = maya

    alembic = {}
    alembic["FE_EXTENSIONS"] = houdini["FE_EXTENSIONS"]
    alembic["FE_EXTENSIONS"] += ":alembic"
    forge.product["alembic"] = alembic

    vdb = {}
    vdb["FE_EXTENSIONS"] = houdini["FE_EXTENSIONS"]
    vdb["FE_EXTENSIONS"] += ":vdb"
    forge.product["vdb"] = vdb

    grass = {}
    grass["FE_EXTENSIONS"] = modeling["FE_EXTENSIONS"]
    grass["FE_EXTENSIONS"] += ":grass:ironworks:vegetation"
    grass["FE_EXTENSIONS"] += ":houdini:maya"
    grass["FE_EXTENSIONS"] += ":alembic"
    grass["FE_EXTENSIONS"] += ":oiio"
    grass["FE_EXTENSIONS"] += ":opencl"
    grass["FE_EXTENSIONS"] += ":openil"
    grass["FE_EXTENSIONS"] += ":opensubdiv"
    grass["FE_EXTENSIONS"] += ":optest"
    grass["FE_EXTENSIONS"] += ":opview"
    grass["FE_EXTENSIONS"] += ":ptex"
    grass["FE_EXTENSIONS"] += ":ray"
    grass["FE_EXTENSIONS"] += ":tbb"
    grass["FE_EXTENSIONS"] += ":terminal"
    grass["FE_EXTENSIONS"] += ":tiff"
    grass["FE_EXTENSIONS"] += ":vdb"
    grass["FE_EXTENSIONS"] += ":xgen"
    grass["FE_EXTENSIONS"] += ":xml"
    forge.product["grass"] = grass

    most = {}
    most["FE_EXTENSIONS"] = grass["FE_EXTENSIONS"]
    most["FE_EXTENSIONS"] += ":arnold:shade:shader"
    most["FE_EXTENSIONS"] += ":audio"
    most["FE_EXTENSIONS"] += ":beacon"
    most["FE_EXTENSIONS"] += ":console"
    most["FE_EXTENSIONS"] += ":eigen"
    most["FE_EXTENSIONS"] += ":enet"
    most["FE_EXTENSIONS"] += ":evaluate"
#   most["FE_EXTENSIONS"] += ":godot"
    most["FE_EXTENSIONS"] += ":graphviz"
    most["FE_EXTENSIONS"] += ":guide"
    most["FE_EXTENSIONS"] += ":fbx"
    most["FE_EXTENSIONS"] += ":flight"
    most["FE_EXTENSIONS"] += ":freetype"
    most["FE_EXTENSIONS"] += ":imgui"
    most["FE_EXTENSIONS"] += ":katana"
    most["FE_EXTENSIONS"] += ":mechanics"
    most["FE_EXTENSIONS"] += ":message"
    most["FE_EXTENSIONS"] += ":native"
    most["FE_EXTENSIONS"] += ":network"
    most["FE_EXTENSIONS"] += ":networkhost"
    most["FE_EXTENSIONS"] += ":openal"
    most["FE_EXTENSIONS"] += ":oplab"
    most["FE_EXTENSIONS"] += ":sdl"
    most["FE_EXTENSIONS"] += ":shm"
    most["FE_EXTENSIONS"] += ":solve"
    most["FE_EXTENSIONS"] += ":threadsignal"
    most["FE_EXTENSIONS"] += ":viewer"
    most["FE_EXTENSIONS"] += ":zeromq"
    forge.product["most"] = most

    # TODO clean up boost dependencies in solve
#   most["FE_EXTENSIONS"] += ":solve"

    # TODO clean up tbb dependencies in spatial
#   most["FE_EXTENSIONS"] += ":spatial"

    clang = {}
    clang["FE_RTTI"] = "1"
    clang["FE_RTTI_HOMEBREW"] = "1"
    clang["FE_CC"] = "clang++"
    clang["FE_EXTENSIONS"] = most["FE_EXTENSIONS"]
    forge.product["clang"] = clang

    opencl = {}
    opencl["FE_EXTENSIONS"] = modeling["FE_EXTENSIONS"]
    opencl["FE_EXTENSIONS"] += ":grass"
    opencl["FE_EXTENSIONS"] += ":native"
    opencl["FE_EXTENSIONS"] += ":opencl"
    opencl["FE_EXTENSIONS"] += ":opview"
    opencl["FE_EXTENSIONS"] += ":terminal"
    opencl["FE_EXTENSIONS"] += ":viewer"
    forge.product["opencl"] = opencl

    viewer_limited = {}
    viewer_limited["FE_EXTENSIONS"] = geometry["FE_EXTENSIONS"]
    viewer_limited["FE_EXTENSIONS"] += ":freetype"
#   viewer_limited["FE_EXTENSIONS"] += ":glew"
    viewer_limited["FE_EXTENSIONS"] += ":native"
    viewer_limited["FE_EXTENSIONS"] += ":opengl"
    viewer_limited["FE_EXTENSIONS"] += ":viewer"
    viewer_limited["FE_EXTENSIONS"] += ":window"
    forge.product["viewer_limited"] = viewer_limited

    architecture = {}
    architecture["FE_EXTENSIONS"] = viewer_limited["FE_EXTENSIONS"]
    architecture["FE_EXTENSIONS"] += ":architecture"
    forge.product["architecture"] = architecture

    imgui = {}
    imgui["FE_EXTENSIONS"] = viewer_limited["FE_EXTENSIONS"]
    imgui["FE_EXTENSIONS"] += ":imgui"
    imgui["FE_EXTENSIONS"] += ":json"
    imgui["FE_EXTENSIONS"] += ":shape"
    imgui["FE_EXTENSIONS"] += ":surface"
    imgui["FE_EXTENSIONS"] += ":operate"
    forge.product["imgui"] = imgui

    # TODO rm (old)
    moa = {}
    moa["FE_EXTENSIONS"] = imgui["FE_EXTENSIONS"]
    moa["FE_EXTENSIONS"] += ":evaluate"
    moa["FE_EXTENSIONS"] += ":moa"
    forge.product["moa"] = moa

    # TODO rm (old)
    viewer_system = {}
    viewer_system["FE_EXTENSIONS"] = moa["FE_EXTENSIONS"]
    viewer_system["FE_EXTENSIONS"] += ":viewer_system"
    forge.product["viewer_system"] = viewer_system

    # TODO rm (old)
    tire = {}
    tire["FE_EXTENSIONS"] = viewer_system["FE_EXTENSIONS"]
    tire["FE_EXTENSIONS"] += ":mechanics"
    tire["FE_EXTENSIONS"] += ":solve"
    tire["FE_EXTENSIONS"] += ":tire"
    forge.product["tire"] = tire

    # TODO rm (old)
    driveline = {}
    driveline["FE_EXTENSIONS"] = moa["FE_EXTENSIONS"]
    driveline["FE_EXTENSIONS"] += ":driveline"
    driveline["FE_EXTENSIONS"] += ":mechanics"
    driveline["FE_EXTENSIONS"] += ":solve"
    forge.product["driveline"] = driveline

    vegetation = {}
    vegetation["FE_EXTENSIONS"] = operator["FE_EXTENSIONS"]
    vegetation["FE_EXTENSIONS"] += ":native"
    vegetation["FE_EXTENSIONS"] += ":opengl"
    vegetation["FE_EXTENSIONS"] += ":surface"
    vegetation["FE_EXTENSIONS"] += ":vegetation"
    vegetation["FE_EXTENSIONS"] += ":viewer"
    forge.product["vegetation"] = vegetation

    opview = {}
    opview["FE_EXTENSIONS"] = operator["FE_EXTENSIONS"]
    opview["FE_EXTENSIONS"] += ":native"
    opview["FE_EXTENSIONS"] += ":opencl"
    opview["FE_EXTENSIONS"] += ":opengl"
    opview["FE_EXTENSIONS"] += ":opview"
    opview["FE_EXTENSIONS"] += ":surface"
    opview["FE_EXTENSIONS"] += ":terminal"
    opview["FE_EXTENSIONS"] += ":viewer"
    forge.product["opview"] = opview

    fbx = {}
    fbx["FE_EXTENSIONS"] = opview["FE_EXTENSIONS"]
    fbx["FE_EXTENSIONS"] += ":fbx"
    forge.product["fbx"] = fbx

    hydra = {}
    hydra["FE_EXTENSIONS"] = opview["FE_EXTENSIONS"]
    hydra["FE_EXTENSIONS"] += ":usd"
    forge.product["hydra"] = hydra

    sdl = {}
#   sdl["FE_EXTENSIONS"] = viewer_limited["FE_EXTENSIONS"]
    sdl["FE_EXTENSIONS"] = datatool["FE_EXTENSIONS"]
    sdl["FE_EXTENSIONS"] += ":image"
    sdl["FE_EXTENSIONS"] += ":sdl"
    sdl["FE_EXTENSIONS"] += ":window"
    forge.product["sdl"] = sdl

    flight = {}
    flight["FE_EXTENSIONS"] = opview["FE_EXTENSIONS"]
    flight["FE_EXTENSIONS"] += ":flight"
    flight["FE_EXTENSIONS"] += ":network"
    flight["FE_EXTENSIONS"] += ":sdl"
    flight["FE_EXTENSIONS"] += ":usd"
    flight["FE_EXTENSIONS"] += ":zeromq"
    forge.product["flight"] = flight

    # TODO derive from opview
    viewer = {}
    viewer["FE_EXTENSIONS"] = geometry["FE_EXTENSIONS"]
    viewer["FE_EXTENSIONS"] += ":alembic"
#   viewer["FE_EXTENSIONS"] += ":arnold:shade:shader"
#   viewer["FE_EXTENSIONS"] += ":boostregex"
#   viewer["FE_EXTENSIONS"] += ":boostthread"
    viewer["FE_EXTENSIONS"] += ":fbx"
    viewer["FE_EXTENSIONS"] += ":freetype"
#   viewer["FE_EXTENSIONS"] += ":glew"
#   viewer["FE_EXTENSIONS"] += ":graphviz"
#   viewer["FE_EXTENSIONS"] += ":grass"
#   viewer["FE_EXTENSIONS"] += ":houdini"
    viewer["FE_EXTENSIONS"] += ":image"
#   viewer["FE_EXTENSIONS"] += ":ironworks"
#   viewer["FE_EXTENSIONS"] += ":json"
#   viewer["FE_EXTENSIONS"] += ":lua"
#   viewer["FE_EXTENSIONS"] += ":maya"
    viewer["FE_EXTENSIONS"] += ":meta"
    viewer["FE_EXTENSIONS"] += ":native"
    viewer["FE_EXTENSIONS"] += ":network"
#   viewer["FE_EXTENSIONS"] += ":oiio"
    viewer["FE_EXTENSIONS"] += ":opencl"
    viewer["FE_EXTENSIONS"] += ":opengl"
#   viewer["FE_EXTENSIONS"] += ":openil"
#   viewer["FE_EXTENSIONS"] += ":opensubdiv"
    viewer["FE_EXTENSIONS"] += ":operate"
    viewer["FE_EXTENSIONS"] += ":operator"
    viewer["FE_EXTENSIONS"] += ":oplab"
#   viewer["FE_EXTENSIONS"] += ":optest"
    viewer["FE_EXTENSIONS"] += ":opview"
#   viewer["FE_EXTENSIONS"] += ":pcre"
    viewer["FE_EXTENSIONS"] += ":ptex"
#   viewer["FE_EXTENSIONS"] += ":ray"
    viewer["FE_EXTENSIONS"] += ":shape"
    viewer["FE_EXTENSIONS"] += ":surface"
    viewer["FE_EXTENSIONS"] += ":tbb"
    viewer["FE_EXTENSIONS"] += ":terminal"
#   viewer["FE_EXTENSIONS"] += ":tiff"
    viewer["FE_EXTENSIONS"] += ":usd"
#   viewer["FE_EXTENSIONS"] += ":vdb"
#   viewer["FE_EXTENSIONS"] += ":vegetation"
    viewer["FE_EXTENSIONS"] += ":viewer"
    viewer["FE_EXTENSIONS"] += ":window"
#   viewer["FE_EXTENSIONS"] += ":xgen"
#   viewer["FE_EXTENSIONS"] += ":xml"
    forge.product["viewer"] = viewer

    # extended build test
    buildable = {}
    buildable["FE_EXTENSIONS"] = most["FE_EXTENSIONS"]
    buildable["FE_EXTENSIONS"] += ":architecture"
    buildable["FE_EXTENSIONS"] += ":dataui"
    buildable["FE_EXTENSIONS"] += ":element"
#   buildable["FE_EXTENSIONS"] += ":godot"
    buildable["FE_EXTENSIONS"] += ":griddb"
    buildable["FE_EXTENSIONS"] += ":intelligence"
#   buildable["FE_EXTENSIONS"] += ":ode"
    buildable["FE_EXTENSIONS"] += ":planet"
    buildable["FE_EXTENSIONS"] += ":spatial"
    buildable["FE_EXTENSIONS"] += ":solve"
    buildable["FE_EXTENSIONS"] += ":terrain"
    forge.product["buildable"] = buildable

    ci_build = {}
    ci_build["FE_EXTENSIONS"] = buildable["FE_EXTENSIONS"]
    ci_build["FE_EXTENSIONS"] = ci_build["FE_EXTENSIONS"].replace(":spatial:", ":")
    forge.product["ci_build"] = ci_build

    # NOTE not running windowed tests until we figure out how
    ci_run = {}
    ci_run["FE_EXTENSIONS"] = most["FE_EXTENSIONS"]
    ci_run["FE_EXTENSIONS"] = ci_run["FE_EXTENSIONS"].replace(":openal:", ":")
    ci_run["FE_EXTENSIONS"] = ci_run["FE_EXTENSIONS"].replace(":native:", ":")
    forge.product["ci_run"] = ci_run

    forge.product["default"] = most
