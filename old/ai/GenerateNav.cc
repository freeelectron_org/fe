/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "GenerateNav.h"

namespace fe
{

GenerateNav::GenerateNav(void)
{
}

GenerateNav::~GenerateNav(void)
{
}

void GenerateNav::initialize(void)
{
}

void GenerateNav::handle(Record &r_sig)
{
	m_asNav.bind(r_sig.layout()->scope());
	m_asNavDebug.bind(r_sig.layout()->scope());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(	m_asNav.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if((!m_asNav.field(spRA, i).isValid()) ||
					(!(m_asNav.location(spRA, i) == m_asNav.calculated(spRA, i))))
				{
					sp<GridVectorField> spField(new GridVectorField());
					Continuum<SpatialVector>::t_index c(10,10,10);

					Vector<4,Real> scalefactor(10.0f, 10.0f, 10.0f);
					SpatialTransform transform = SpatialTransform::identity();
					translate(transform, Vector<4,Real>(2.0,0.0,0.0));
					scale(transform, scalefactor);
					sp<AffineSpace> spSpace(new AffineSpace());

					spSpace->setTransform(transform);

					Continuum<Real> local_potential_field;
					Continuum<Real> *potential_field =
							&local_potential_field;
					sp<GridScalarField> spF(new GridScalarField());
					if(m_asNavDebug.potential.check(spRA))
					{
						m_asNavDebug.potential(spRA, i) = spF;
					}
					potential_field = &(spF->continuum());
					spF->create(spSpace, c);
					//potential_field.create(c, b, 0.0f);
					sp<StepCostI> spCostFn(registry()->create("StepCostI"));
					SpatialVector target, r;
					target = m_asNav.location(spRA, i);
					wavefrontPotential(*potential_field, spCostFn,
						target, (Real)1e10, (Real)0.0);
					m_asNav.calculated(spRA, i) = m_asNav.location(spRA, i);


					derive<SpatialVector>(spField->continuum(),
						*potential_field);

					m_asNav.field(spRA, i) = spField;
				}
			}
		}
	}
}



} /* namespace */

