/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

RecordArrayAV::RecordArrayAV(void)
{
	m_weak = FALSE;
	m_duplicates = false;
	m_maintainOrder = false;
}

RecordArrayAV::RecordArrayAV(sp<LayoutAV> spLayout)
{
	m_weak = FALSE;
	m_duplicates = false;
	m_maintainOrder = false;
	setLayout(spLayout);
}

RecordArrayAV::RecordArrayAV(const RecordArrayAV &other):
	Castable(),
	Counted()
{
	copy(other);
}

RecordArrayAV::RecordArrayAV(sp<LayoutAV> spLayout, FE_UWORD aCount)
{
	m_weak = FALSE;
	m_duplicates = false;
	m_maintainOrder = false;
	setLayout(spLayout);
	m_pArrayIndex.reserve(10000);
	m_pSN.reserve(10000);

	sp<Scope> spScope = spLayout->scope();

	RecordAV r_av;
	for(FE_UWORD i = 0;i < aCount; i++)
	{
		r_av = spLayout->createRecord();
		add(r_av);

#if FE_SCOPE_SUPPORT_WATCHERS
		spScope->watch(r_av);
#endif
	}
}

RecordArrayAV::~RecordArrayAV(void)
{
	clear();
}

void RecordArrayAV::enableDuplicates(bool a_bool)
{
	if(m_duplicates == a_bool) { return; }
	m_duplicates = a_bool;

	if(!m_duplicates)
	{
		m_av_map.clear();
		unsigned int n = m_pArrayIndex.size();
		for(unsigned int i = 0; i < n; i++)
		{
			m_av_map[m_pArrayIndex[i]] = i;
		}
	}
	else
	{
		m_av_map.clear();
	}
}

void RecordArrayAV::setLayout(const sp<LayoutAV>& rspLayout)
{
	m_spLayout = rspLayout;

	U32 index = m_spLayout->scope()->serialIndex();
	m_serialLocator = m_spLayout->locatorTable()[index];
	if(m_weak && m_serialLocator==LayoutAV::locatorNone)
	{
		feX(e_refused,"RecordArray::setLayout",
				"weak array%s requires serial number in layout%s",
#if FE_COUNTED_TRACK
				(" \""+name()+"\"").c_str(),
				(" \""+layout()->name()+"\"").c_str()
#else
				"",""
#endif
				);
	}
}

void RecordArrayAV::clear(void)
{
	for(FE_UWORD i = 0;i < m_pArrayIndex.size(); i++)
	{
		releaseAV(i);
	}
	m_pArrayIndex.clear();
	m_pSN.clear();

	if(!m_duplicates)
	{
		m_av_map.clear();
	}
}

void RecordArrayAV::copy(const RecordArrayAV &other)
{
	setLayout(other.m_spLayout);
	m_pArrayIndex = other.m_pArrayIndex;
	if(m_weak && other.m_weak)
	{
		m_pSN = other.m_pSN;
	}
	for(FE_UWORD i = 0;i < m_pArrayIndex.size(); i++)
	{
		acquireAV(i);
	}
	if(m_weak && !other.m_weak)
	{
		m_pSN.resize(m_pArrayIndex.size());
		RecordAV record;
		for(FE_UWORD i = 0;i < m_pArrayIndex.size(); i++)
		{
			record.set(m_pArrayIndex[i], m_spLayout);
			m_pSN[i]=readSerialNumber(i);
		}
	}

	m_duplicates = other.m_duplicates;
	if(!m_duplicates)
	{
		m_av_map = other.m_av_map;
	}
}

RecordArrayAV &RecordArrayAV::operator=(const RecordArrayAV &other)
{
	if(this != &other)
	{
		clear();
		copy(other);
	}
	return *this;
}

bool RecordArrayAV::add(sp<RecordArrayAV> spRA, IWORD start, IWORD size)
{
	if(addCovert(spRA,start,size))
	{
		if(m_hpRecordGroup.isValid())
		{
			U32 end = m_pArrayIndex.size();
			U32 start = end - size;
			for(U32 i = start; i < end; i++)
			{
// disable on an SB build?
#if FE_DATA_STORE==FE_AV
				m_hpRecordGroup->watcherAdd(getRecord(i));
#endif
			}
		}

		return true;
	}

	return false;
}

bool RecordArrayAV::addCovert(sp<RecordArrayAV> spRA, IWORD start, IWORD size)
{
	if(m_spLayout.isValid())
	{
		if(m_spLayout != spRA->layout())
		{
			return false;
		}
	}
	else
	{
		setLayout(spRA->layout());
	}

	unsigned int old_size = m_pArrayIndex.size();
	unsigned int new_size = old_size + size;
	m_pArrayIndex.resize(new_size);

	RecordArrayAV *pRA = spRA.raw();

	unsigned int j = start;
	for(unsigned int i = old_size; i < new_size; i++, j++)
	{
		m_pArrayIndex[i] = pRA->m_pArrayIndex[j];
		acquireAV(i);
		if(!m_duplicates)
		{
			m_av_map[m_pArrayIndex[i]] = i;
		}
	}

	if(m_weak)
	{
		m_pSN.resize(new_size);
		j = start;
		if(pRA->m_weak)
		{
			for(unsigned int i = old_size; i < new_size; i++, j++)
			{
				m_pSN[i] = pRA->m_pSN[j];
			}
		}
		else
		{
			for(unsigned int i = old_size; i < new_size; i++, j++)
			{
				m_pSN[i]=readSerialNumber(i);
			}
		}
	}

	return true;
}

bool RecordArrayAV::addCreateCovert(U32 a_count)
{
	//* since we are creating the record, we can avoid the find

	if(!a_count || m_spLayout.isNull())
	{
		return false;
	}

	if(m_weak)
	{
		feX("RecordArrayAV::addCreate",
			"useless call when array is weak...record will instantly destruct");
		return false;
	}

#if TRUE
	RecordAV firstRecord;
	m_spLayout->createRecords(firstRecord,a_count);

	const FE_UWORD firstRecordArrayIndex=firstRecord.arrayIndex();
	const FE_UWORD firstArrayIndex = m_pArrayIndex.size();

	sp<Scope> spScope = m_spLayout->scope();

	Record record=firstRecord;
	for(U32 index=0;index<a_count;index++)
	{
		record.m_arrayIndex=firstRecordArrayIndex+index;
		spScope->assignIDNumber<RecordAV>(record);

		m_pArrayIndex.push_back(firstRecordArrayIndex+index);
	}
	record.m_arrayIndex=firstRecordArrayIndex;

	//* NOTE after first, indexes were acquired without Record to destruct
	acquireAV(firstArrayIndex);

	if(!m_duplicates)
	{
		for(U32 index=0;index<a_count;index++)
		{
			const FE_UWORD arrayIndex=firstArrayIndex+index;
			m_av_map[m_pArrayIndex[arrayIndex]] = arrayIndex;
		}
	}
#else
	for(U32 index=0;index<a_count;index++)
	{
		RecordAV record = m_spLayout->createRecord();

		const FE_UWORD idx = m_pArrayIndex.size();
		m_pArrayIndex.push_back(record.arrayIndex());

		acquireAV(idx);

		if(!m_duplicates)
		{
			m_av_map[m_pArrayIndex[idx]] = idx;
		}
	}
#endif

	return true;
}

bool RecordArrayAV::add(const RecordAV &record, IWORD *index)
{
	if(addCovert(record,index))
	{
		if(m_hpRecordGroup.isValid())
		{
#if FE_DATA_STORE==FE_AV
			m_hpRecordGroup->watcherAdd(record);
#endif
		}
		return true;
	}

	return false;
}

bool RecordArrayAV::addCovert(const RecordAV &record, IWORD *index)
{
	if(!m_duplicates)
	{
		if(find(record))
		{
			return false;
		}
	}

	if(m_spLayout.isValid())
	{
		if(m_spLayout != record.layout())
		{
			if(m_pArrayIndex.size())
			{
				return false;
			}
			else
			{
				/*  This may be fine.  need to discuss with Jason.
					He has been doing
					this for some time while using recordviews.
					this issue exists in RecordArraySB as well */
				feX("RecordArrayAV::add",
						"attempting to add record of layout %s to"
						" RecordArray of layout %s",
						record.layout()->name().c_str(),
						m_spLayout->name().c_str());
				setLayout(record.layout());
			}
		}
	}
	else
	{
		setLayout(record.layout());
	}

	FE_UWORD idx = m_pArrayIndex.size();
	m_pArrayIndex.push_back(record.arrayIndex());
	acquireAV(idx);
	if(!m_duplicates)
	{
		m_av_map[m_pArrayIndex[idx]] = idx;
	}

	if(m_weak)
	{
		m_pSN.push_back(readSerialNumber(idx));
	}

	return true;
}

bool RecordArrayAV::find(const RecordAV &record)
{
	if(!m_duplicates)
	{
		t_av_map::iterator i_r = m_av_map.find(record.arrayIndex());
		return (i_r != m_av_map.end());
	}
	else
	{
		unsigned int n = m_pArrayIndex.size();
		for(IWORD i = 0; i < n; i++)
		{
			if(m_pArrayIndex[i] == record.arrayIndex())
			{
				return true;
			}
		}
		return false;
	}
}

void RecordArrayAV::prune(void)
{
	if(m_weak)
	{
		unsigned int n = m_pArrayIndex.size();
		for(IWORD i = n-1; i >= 0; i--)
		{
			if(m_pArrayIndex[i]==LayoutAV::arrayindexNone ||
				m_pSN[i]!=readSerialNumber(i))
			{
				remove(i);
			}
		}
	}
}

bool RecordArrayAV::remove(const RecordAV &record, IWORD *index)
{
	if(removeCovert(record,index))
	{
		if(m_hpRecordGroup.isValid())
		{
#if FE_DATA_STORE==FE_AV
			m_hpRecordGroup->watcherRemove(record);
#endif
		}
		return true;
	}

	return false;
}

bool RecordArrayAV::removeCovert(const RecordAV &record, IWORD *index)
{
	if(!m_duplicates)
	{
		t_av_map::iterator i_r = m_av_map.find(record.arrayIndex());
		if(i_r != m_av_map.end())
		{
			int i = i_r->second;
			if(m_pArrayIndex[i] == record.arrayIndex())
			{
				if(index)
				{
					*index = i;
				}
				remove(i);
				return true;
			}
			else
			{
				feX("impossibility");
			}
		}
		return false;
	}
	else
	{
		bool rv = false;
		unsigned int n = m_pArrayIndex.size();
		for(IWORD i = 0; i < n; i++)
		{
			if(m_pArrayIndex[i] == record.arrayIndex())
			{
				if(index)
				{
					*index = i;
				}
				remove(i);
				rv = true;
			}
		}
		return rv;
	}
}

void RecordArrayAV::remove(IWORD index)
{
#if FE_DATA_STORE==FE_AV
	Record record=getRecord(index);
	removeCovert(index);

	if(m_hpRecordGroup.isValid() && record.isValid())
	{
		m_hpRecordGroup->watcherRemove(record);
	}
#endif
}

void RecordArrayAV::removeCovert(IWORD index)
{
	if(!m_duplicates)
	{
		m_av_map.erase(m_pArrayIndex[index]);
	}
	releaseAV(index);
	if(m_weak)
	{
		m_pSN[index]= -1;
	}
	const unsigned int size = m_pArrayIndex.size();

	if(m_maintainOrder)
	{
		// shift all that follows

		memmove(&m_pArrayIndex[index],&m_pArrayIndex[index+1],
				(size-index-1)*sizeof(m_pArrayIndex[0]));

		if(m_weak)
		{
			memmove(&m_pSN[index],&m_pSN[index+1],
					(size-index-1)*sizeof(m_pSN[0]));
		}

		if(!m_duplicates)
		{
			for(I32 n=index;n<I32(size-1);n++)
			{
				m_av_map[m_pArrayIndex[n]] = n;
			}
		}
	}
	else
	{
		// swap end to removal slot
		if(size > 0 && (index < (size-1)))
		{
			m_pArrayIndex[index] = m_pArrayIndex[size-1];
			if(!m_duplicates)
			{
				m_av_map[m_pArrayIndex[index]] = index;
			}
			if(m_weak)
			{
				m_pSN[index] = m_pSN[size-1];
			}
		}
	}

	// pop end
	m_pArrayIndex.pop_back();
	if(m_weak)
	{
		m_pSN.pop_back();
	}
}


IWORD PtrRecordArrayAVInfo::output(std::ostream &ostrm, void *instance,
		t_serialMode mode)
{
	feX(e_unsupported,
		"PtrRecordArrayInfo::output",
		"record array output should be done via fe::Stream");
	return 0;
}

void PtrRecordArrayAVInfo::input(std::istream &istrm, void *instance,
		t_serialMode mode)
{
	feX(e_unsupported,
		"PtrRecordArrayInfo::input",
		"record array input should be done via fe::Stream");
}

IWORD PtrRecordArrayAVInfo::iosize(void)
{
	return c_implicit;
}

bool PtrRecordArrayAVInfo::getConstruct(void)
{
	return true;
}

typedef sp<RecordArrayAV> RecordArrayAVPtr;

void PtrRecordArrayAVInfo::construct(void *instance)
{
	new(instance)RecordArrayAVPtr;
}

void PtrRecordArrayAVInfo::destruct(void *instance)
{
	((RecordArrayAVPtr *)instance)->~RecordArrayAVPtr();
}

};

