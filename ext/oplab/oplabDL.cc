/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
#ifdef FE_USE_GRAPHVIZ
//	list.append(new fe::String("fexGraphVizDL"));
#endif

//	list.append(new String("fexNetSignalDL"));
	list.append(new String("fexOperateDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<NexusOp>(
			"OperatorSurfaceI.NexusOp.fe.nohoudini");

	pLibrary->add<BindEditOp>(
			"OperatorSurfaceI.BindEditOp.fe.prototype");
	pLibrary->add<CurvaceousOp>(
			"OperatorSurfaceI.CurvaceousOp.fe.prototype");
	pLibrary->add<FusionOp>(
			"OperatorSurfaceI.FusionOp.fe.prototype");
	pLibrary->add<GridWrapOp>(
			"OperatorSurfaceI.GridWrapOp.fe.prototype");
	pLibrary->add<MultiModOp>(
			"OperatorSurfaceI.MultiModOp.fe.prototype");
	pLibrary->add<PreviewOp>(
			"OperatorSurfaceI.PreviewOp.fe.prototype");
	pLibrary->add<SharpenOp>(
			"OperatorSurfaceI.SharpenOp.fe.prototype");
	pLibrary->add<XRayOp>(
			"OperatorSurfaceI.XRayOp.fe.prototype");

#ifdef __oplab_NetworkOp_h__
	pLibrary->add<NetworkOp>(
			"OperatorSurfaceI.NetworkOp.fe.prototype");
#endif

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
#if FALSE
	sp<Registry> spRegistry=spLibrary->registry();

	//* load Alembic support
	String nameOrder="fexAlembic10102:fexAlembic";
	String name;
	while(!(name=nameOrder.parse("\"",":")).empty())
	{
		if(successful(spRegistry->manage(name)))
		{
			if(System::getVerbose()=="all")
			{
				feLog("  using %s\n",name.c_str());
			}
			return;
		}
	}
#endif
}

}
