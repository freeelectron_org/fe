# must first run powershell as admin
# then run:
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

pip install virtualenv
python -m virtualenv venvwin
venvwin\Scripts\activate
venvwin\Scripts\python.exe -m pip install --upgrade pip
pip install -r requirements.txt
