/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SPO_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAttrPromoteOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("attributes")="";
	catalog<String>("attributes","label")="Attributes";
	catalog<bool>("attributes","joined")=true;
	catalog<String>("attributes","hint")=
		"Space delimited list of attributes to promote.";

	catalog<bool>("regex")=false;
	catalog<String>("regex","label")="Regex";
	catalog<String>("regex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("fromClass")="auto";
	catalog<String>("fromClass","choice:0")="auto";
	catalog<String>("fromClass","label:0")="Auto";
	catalog<String>("fromClass","choice:1")="point";
	catalog<String>("fromClass","label:1")="Point";
	catalog<String>("fromClass","choice:2")="vertex";
	catalog<String>("fromClass","label:2")="Vertex";
	catalog<String>("fromClass","choice:3")="primitive";
	catalog<String>("fromClass","label:3")="Primitive";
	catalog<String>("fromClass","label")="From";

	catalog<String>("toClass")="auto";
	catalog<String>("toClass","choice:0")="auto";
	catalog<String>("toClass","label:0")="Auto";
	catalog<String>("toClass","choice:1")="point";
	catalog<String>("toClass","label:1")="Point";
	catalog<String>("toClass","choice:2")="vertex";
	catalog<String>("toClass","label:2")="Vertex";
	catalog<String>("toClass","choice:3")="primitive";
	catalog<String>("toClass","label:3")="Primitive";
	catalog<String>("toClass","label")="To";
	catalog<String>("toClass","hint")=
			"For auto, point rate is the default,"
			" except for existing point attributes,"
			" which will default to primitive rate.";

	catalog<String>("mode")="arbitrary";
	catalog<String>("mode","choice:0")="arbitrary";
	catalog<String>("mode","label:0")="Arbitrary (single choice)";
	catalog<String>("mode","label")="Mode";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void SurfaceAttrPromoteOp::handle(Record& a_rSignal)
{
#if FE_SPO_DEBUG
	feLog("SurfaceAttrPromoteOp::handle\n");
#endif

	const String mode=catalog<String>("mode");

	String attributes=catalog<String>("attributes");
	if(!attributes.empty() && !catalog<bool>("regex"))
	{
		attributes=attributes.convertGlobToRegex();
	}

	const String fromClass=catalog<String>("fromClass");
	const String toClass=catalog<String>("toClass");

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	Array<String> patterns;
	while(TRUE)
	{
		String token=attributes.parse();
		if(token.empty())
		{
			break;
		}

		patterns.push_back(token);
	}

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	I32 promotionCount(0);

	for(I32 pass=0;pass<3;pass++)
	{
		SurfaceAccessibleI::Element surfaceElementIn;
		Element elementIn;
		Element elementOut=e_point;

		switch(pass)
		{
			case 0:
				if(fromClass=="vertex" || fromClass=="primitive" ||
						toClass=="point")
				{
					continue;
				}
				surfaceElementIn=SurfaceAccessibleI::e_point;
				elementIn=e_point;
				break;
			case 1:
				if(fromClass=="point" || fromClass=="primitive" ||
						toClass=="vertex")
				{
					continue;
				}
				surfaceElementIn=SurfaceAccessibleI::e_vertex;
				elementIn=e_vertex;
				break;
			case 2:
				if(fromClass=="point" || fromClass=="vertex" ||
						toClass=="primitive")
				{
					continue;
				}
				surfaceElementIn=SurfaceAccessibleI::e_primitive;
				elementIn=e_primitive;
				break;
//			case 3:
//				surfaceElementIn=SurfaceAccessibleI::e_detail;
//				elementIn=e_detail;
			default:
				continue;
		}

		if(toClass=="vertex")
		{
			elementOut=e_vertex;
		}
		else if(toClass=="primitive" || fromClass=="point")
		{
			elementOut=e_primitive;
		}

		//* point -> vertex				1:many
		//* point/vertex -> primitive	many:1
		//* vertex -> point				many:1
		//* primitive -> point			many:1
		//* primitive -> vertex			1:1

#if FE_SPO_DEBUG
		feLog("SurfaceAttrPromoteOp::handle from %s to %s\n",
				SurfaceAccessibleBase::elementLayout(surfaceElementIn).c_str(),
				SurfaceAccessibleBase::elementLayout(
				SurfaceAccessibleI::Element(elementOut)).c_str());
#endif

		Array<SurfaceAccessibleI::Spec> specs;
		spInputAccessible->attributeSpecs(specs,surfaceElementIn);
		const U32 specCount=specs.size();

		for(U32 specIndex=0;specIndex<specCount;specIndex++)
		{
			const SurfaceAccessibleI::Spec& spec=specs[specIndex];
			const String& specName=spec.name();

			for(String& rPattern: patterns)
			{
				if(specName.match(rPattern))
				{
					sp<SurfaceAccessorI> spSourceAttr;
					if(!access(spSourceAttr,spInputAccessible,
							elementIn,specName,e_warning,e_refuseMissing))
					{
						continue;
					}

					sp<SurfaceAccessorI> spDestAttr;
					if(!access(spDestAttr,spOutputAccessible,
							elementOut,specName))
					{
						continue;
					}

					const String& specType=spec.typeName();

					AttrType attrType(e_none);
					if(specType=="string")
					{
						attrType=e_string;
					}
					else if(specType=="integer")
					{
						attrType=e_integer;
					}
					else if(specType=="real")
					{
						attrType=e_real;
					}
					else if(specType=="vector3")
					{
						attrType=e_spatialVector;
					}
					else
					{
						continue;
					}

					const I32 sourceCount=spSourceAttr->count();
					const I32 destCount=spDestAttr->count();

					if(elementOut==e_point)
					{
						//* vertex/prim -> point
						for(I32 sourceIndex=0;
								sourceIndex<sourceCount;
								sourceIndex++)
						{
							const I32 subCount=spOutputVertices->subCount(
									sourceIndex);
							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								const I32 destIndex=
										spOutputVertices->integer(
										sourceIndex,subIndex);

								copyAttribute(attrType,
										spSourceAttr,sourceIndex,
										(elementIn==e_vertex)? subIndex: 0,
										spDestAttr,destIndex,0);
							}
						}
					}
					else if(elementIn==e_primitive && elementOut==e_vertex)
					{
						//* prim -> vertex
						for(I32 sourceIndex=0;sourceIndex<sourceCount;
								sourceIndex++)
						{
							const I32 destIndex=sourceIndex;

							const I32 subCount=spOutputVertices->subCount(
									sourceIndex);
							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								copyAttribute(attrType,
										spSourceAttr,sourceIndex,0,
										spDestAttr,destIndex,subIndex);
							}
						}
					}
					else if(elementOut==e_vertex)
					{
						//* point -> vertex
						for(I32 destIndex=0;destIndex<destCount;
								destIndex++)
						{
							const I32 subCount=spOutputVertices->subCount(
									destIndex);
							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								const I32 sourceIndex=
										spOutputVertices->integer(
										destIndex,subIndex);


								copyAttribute(attrType,
										spSourceAttr,sourceIndex,0,
										spDestAttr,destIndex,subIndex);
							}
						}
					}
					else if(elementIn==e_point)
					{
						//* point -> prim
						for(I32 destIndex=0;destIndex<destCount;
								destIndex++)
						{
							//* TODO some sort of blend mode
							const I32 subIndex=0;

							const I32 sourceIndex=
									spOutputVertices->integer(
									destIndex,subIndex);

							copyAttribute(attrType,
									spSourceAttr,sourceIndex,0,
									spDestAttr,destIndex,0);
						}
					}
					else if(elementIn==e_vertex)
					{
						//* vertex -> prim
						for(I32 sourceIndex=0;sourceIndex<sourceCount;
								sourceIndex++)
						{
							const I32 subIndex=0;
							const I32 destIndex=sourceIndex;

							copyAttribute(attrType,
									spSourceAttr,sourceIndex,subIndex,
									spDestAttr,destIndex,0);
						}
					}

					spOutputAccessible->discard(surfaceElementIn,specName);
					promotionCount++;
				}
			}
		}
	}

	String summary;
	summary.sPrintf("%s %s %d promotion%s",
			fromClass.c_str(),toClass.c_str(),
			promotionCount,promotionCount==1? "": "s");
	catalog<String>("summary")=summary;

#if FE_SPO_DEBUG
	feLog("SurfaceAttrPromoteOp::handle done\n");
#endif
}

void SurfaceAttrPromoteOp::copyAttribute(AttrType a_attrType,
		sp<SurfaceAccessorI>& a_rspSourceAttr,
		I32 a_sourceIndex,I32 a_sourceSubIndex,
		sp<SurfaceAccessorI>& a_rspDestAttr,
		I32 a_destIndex,I32 a_destSubIndex)
{
	switch(a_attrType)
	{
		case e_string:
		{
			const String value=a_rspSourceAttr->string(
					a_sourceIndex,a_sourceSubIndex);

			a_rspDestAttr->set(a_destIndex,a_destSubIndex,value);
		}
			break;

		case e_integer:
		{
			const I32 value=a_rspSourceAttr->integer(
					a_sourceIndex,a_sourceSubIndex);

			a_rspDestAttr->set(a_destIndex,a_destSubIndex,value);
		}
			break;

		case e_real:
		{
			const Real value=a_rspSourceAttr->real(
					a_sourceIndex,a_sourceSubIndex);

			a_rspDestAttr->set(a_destIndex,a_destSubIndex,value);
		}
			break;

		case e_spatialVector:
		{
			const SpatialVector value=a_rspSourceAttr->spatialVector(
					a_sourceIndex,a_sourceSubIndex);

			a_rspDestAttr->set(a_destIndex,a_destSubIndex,value);
		}
			break;

		default:
			/* NOOP */ ;
	}
}

} /* namespace ext */
} /* namespace fe */
