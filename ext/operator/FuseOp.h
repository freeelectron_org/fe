/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_FuseOp_h__
#define __operator_FuseOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Combine proximal points

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT FuseOp:
	public OperatorSurfaceCommon,
	public Initialize<FuseOp>
{
	public:
				FuseOp(void)											{}
virtual			~FuseOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_FuseOp_h__ */

