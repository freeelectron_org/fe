import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = [ "fePlatformLib",
                "feMemoryLib",
                "fePluginLib",
                "feCoreLib",
                "feDataLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "feImportMemoryLib" ]


    tests = [   "xAttribute",
                "xBitset",
                "xCookbook",
                "xScale",
                "xScope",
                "xRecordGroup",
                "xRecordType",
                #"xSrcDataExamples",
                "xSansGroups",
                "xDataPrimer",
                "xStateEaseOfUse",
                "xSerialization",
                "xSpeed",
                "xFileStream",
                "xWeakRecord" ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + 'Exe'], deplibs)

    forge.tests += [
        ("xAttribute.exe",      "",                 None,       None),
        ("xBitset.exe",         "",                 None,       None),
        ("xCookbook.exe",       "",                 None,       None),
        ("xScope.exe",          "",                 None,       None),
        ("xSpeed.exe",          "",                 None,       None),
        ("xRecordGroup.exe",    "100",              None,       None),
        ("xRecordType.exe",     "",                 None,       None),
        ("xWeakRecord.exe",     "",                 None,       None),
        ("xSerialization.exe",  "binary",           None,       None),
        ("xSerialization.exe",  "",                 None,       None),
        ("xStateEaseOfUse.exe", "",                 None,       None) ]

