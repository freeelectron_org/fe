/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

#include "GroupExtract.h"
#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
	list.append(new String("fexDataToolDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertData(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<Fork>("ForkI.Fork.fe");
	pLibrary->add<Fork>("HandlerI.Fork.fe");
	pLibrary->add<Annotate>("AnnotateI.Annotate.fe");
	pLibrary->add<Annotate>("HandlerI.Annotate.fe");
	pLibrary->add<Assign>("AssignI.Assign.fe");
	pLibrary->add<Assign>("HandlerI.Assign.fe");
	pLibrary->add<GroupExtract>("HandlerI.GroupExtract.fe");
	pLibrary->add<PaletteMap>("PaletteI.PaletteMap.fe");
	pLibrary->add<WindowPipeline>("PipelineI.Window.fe");
	pLibrary->add<SimulationPipeline>("PipelineI.Simulation.fe");
	pLibrary->add<PlayerPipeline>("PipelineI.Player.fe");
//	pLibrary->add<ConfiguredApplication>("ApplicationI.Configured.fe");
	pLibrary->add<Executor>("RecordFactoryI.Executor.fe");
	pLibrary->add<AppDebugWindow>("ApplicationWindowI.debug.fe");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
