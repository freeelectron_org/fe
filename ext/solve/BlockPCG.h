/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BlockPCG_h__
#define __BlockPCG_h__

#if 0
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace ublas = boost::numeric::ublas;
#endif

namespace fe
{
namespace ext
{

template <typename T>
class FE_DL_EXPORT Preconditioner
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;
virtual			~Preconditioner(void){}
virtual	void	apply(		std::vector<t_v3> &a_r,
							const std::vector<t_v3> &a_x) const	= 0;
};

template <typename T>
class FE_DL_EXPORT BlockDiagonalPreconditioner : public Preconditioner<T>
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;
				BlockDiagonalPreconditioner(void);
virtual			~BlockDiagonalPreconditioner(void);

virtual	void	apply(		std::vector<t_v3> &a_r,
							const std::vector<t_v3> &a_x) const;

		std::vector<t_matrix> &diagonal(void);

	private:
		std::vector<t_matrix>	m_diagonal;
};

#if 0
class FE_DL_EXPORT OverlappedInversionPreconditioner : public Preconditioner
{
	public:
				OverlappedInversionPreconditioner(void){}
virtual			~OverlappedInversionPreconditioner(void){}

virtual	void	apply(		std::vector<t_v3> &a_r,
							const std::vector<t_v3> &a_x) const;

		class Block
		{
			public:
				ublas::matrix<t_real>		m_inverse;
				unsigned int			m_L;
				unsigned int			m_H;
				unsigned int			m_m0;
				unsigned int			m_m1;
		};

		std::vector<Block> &block(void) { return m_block; }

	private:
		std::vector<Block>	m_block;
};
#endif

template <typename T>
class FE_DL_EXPORT FilterConstraint
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;
						FilterConstraint(void);
virtual					~FilterConstraint(void);

		const t_matrix	&filter(void) const;
		void			setIndex(unsigned long a_index);
		unsigned long	index(void) const;

		void			makePointConstraint(void);
		void			makeLineConstraint(const t_v3 &a_direction);
		void			makePlaneConstraint(const t_v3 &a_normal);
		void			makeInequalityConstraint(const t_v3 &a_axis, t_real a_delta);

		const t_v3		&axis(void) const {return m_axis;}
		t_real			delta(void) const;
		bool			isInequality(void) const;

		bool			m_inequality;
	private:
		t_matrix		m_filter;
		unsigned long	m_index;
		t_real			m_delta;
		t_v3			m_axis;
};


template <typename T>
class FE_DL_EXPORT BlockPCG
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;
				BlockPCG(void);
virtual			~BlockPCG(void);

		bool	solve(		std::vector<t_v3> &a_x,
							const sp< SparseMatrix3x3<t_real> > a_A,
							const std::vector<t_v3> &a_b,
							const Preconditioner<T> &a_P,
							std::vector< FilterConstraint<T> > &a_filters);

		const std::vector<t_v3> &residual(void) const;

		unsigned int		iterations(void) const { return m_iter; }

	private:
		t_real	inner(		const std::vector<t_v3> &a_left,
							const std::vector<t_v3> &a_right);

		void	residual(	std::vector<t_v3> &a_r,
							const sp< SparseMatrix3x3<t_real> > a_A,
							const std::vector<t_v3> &a_x,
							const std::vector<t_v3> &a_b);

		void	setSize(	unsigned long a_size);

		void	applyFilters(std::vector<t_v3> &a_r,
							std::vector< FilterConstraint<T> > &a_filters);

		bool						m_restart;
		t_real						m_tol;
		unsigned int				m_maxIterations;
		unsigned int				m_iter;
		std::vector<t_v3>	m_r;
		std::vector<t_v3>	m_q;
		std::vector<t_v3>	m_d;
		std::vector<t_v3>	m_s;
		std::vector<t_v3>	m_tmp0;
		std::vector<t_v3>	m_tmp1;
		unsigned int				m_n;
		std::vector<t_v3>  *m_x;

};

template <typename T>
inline
BlockPCG<T>::BlockPCG(void)
{
	m_tol = fe::tol;
//	m_tol = 1.0e-10;
	m_maxIterations = 100000;
	m_iter = 0;
	m_n = 0;
	m_restart = false;
}

template <typename T>
inline
BlockPCG<T>::~BlockPCG(void)
{
}

template <typename T>
inline
T BlockPCG<T>::inner(const std::vector<t_v3> &a_left, const std::vector<t_v3> &a_right)
{
	t_real s = 0.0;
	unsigned int n = a_left.size();
	for(unsigned int i = 0; i < n; i++)
	{
		s += dot(a_left[i], a_right[i]);
	}
	return s;
}

template <typename T>
inline
void BlockPCG<T>::residual(std::vector<t_v3> &a_r, sp< SparseMatrix3x3<t_real> > a_A, const std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b)
{
	unsigned int n = a_b.size();

	a_A->multiply(a_r, a_x);

	for(unsigned int i = 0; i < n; i++)
	{
		a_r[i] = a_b[i] - a_r[i];
	}
}

template <typename T>
inline
void BlockPCG<T>::setSize(unsigned long a_size)
{
	if(a_size != m_n)
	{
		m_n = a_size;
		m_r.resize(m_n);
		m_q.resize(m_n);
		m_d.resize(m_n);
		m_s.resize(m_n);
		m_tmp0.resize(m_n);
		m_tmp1.resize(m_n);
	}
}

template <typename T>
inline
void BlockPCG<T>::applyFilters(std::vector<t_v3> &a_r, std::vector< FilterConstraint<T> > &a_filters)
{
	for(unsigned int i = 0; i < a_filters.size(); i++)
	{
		unsigned long index = a_filters[i].index();

// TODO: probably remove this inequality stuff
		if(a_filters[i].isInequality())
		{
			t_v3 &v = (*m_x)[index];
			t_real d = dot(a_filters[i].axis(), v);

			d = (*m_x)[index][2];

//fe_fprintf(stderr, "index %d delta %f d %f\n", index, a_filters[i].delta(), d);
			if(d < a_filters[i].delta())
			{
//fe_fprintf(stderr, "<***********************\n");
				//(*m_x)[index] = (*m_x)[index] - a_filters[i].axis() * d;
				//(*m_x)[index] = (*m_x)[index] + a_filters[i].axis() * a_filters[i].delta();
				(*m_x)[index][2] = a_filters[i].delta();
				a_r[index] = fe::multiply(a_filters[i].filter(), a_r[index]);
				a_filters[i].m_inequality = false;
				m_restart = true;
			}
		}
		else
		{
			a_r[index] = fe::multiply(a_filters[i].filter(), a_r[index]);
		}
	}
//fe_fprintf(stderr, "-----\n");
}

template <typename T>
inline
const std::vector< Vector<3,T> > &BlockPCG<T>::residual(void) const
{
	return m_r;
}

//#define FE_BLOCKPCG_RESTART
template <typename T>
inline
bool BlockPCG<T>::solve(std::vector<t_v3> &a_x, const sp< SparseMatrix3x3<t_real> > a_A, const std::vector<t_v3> &a_b, const Preconditioner<T> &a_P, std::vector< FilterConstraint<T> > &a_filters)
{
	bool converged = false;

	setSize(a_b.size());

	t_real delta0, deltaNew, deltaOld, deltaStop;

#ifdef FE_BLOCKPCG_RESTART
	unsigned int modn = 50;
#endif

	m_x = &(a_x);

	m_tmp0 = a_b;
	applyFilters(m_tmp0, a_filters);
	a_P.apply(m_tmp1, m_tmp0);
	delta0 = inner(m_tmp0, m_tmp1);

	residual(m_r, a_A, a_x, a_b);
	applyFilters(m_r, a_filters);

	a_P.apply(m_d, m_r);
	applyFilters(m_d, a_filters);

	deltaNew = inner(m_r,m_d);
	deltaStop = m_tol * m_tol * delta0;


	m_iter = 0;
	while((m_iter < m_maxIterations) && (deltaNew > deltaStop))
	{
		a_A->multiply(m_q, m_d);
		applyFilters(m_q, a_filters);

		t_real alpha = deltaNew / inner(m_d, m_q);

		for(unsigned int i = 0; i < m_n; i++)
		{
			a_x[i] += alpha * m_d[i];
		}

#if 0
		if(m_restart)
		{
			m_restart = false;
			residual(m_r, a_A, a_x, a_b);
			applyFilters(m_r, a_filters);
		}
		else
		{
			for(unsigned int i = 0; i < m_n; i++)
			{
				m_r[i] -= alpha * m_q[i];
			}
		}
#endif
			for(unsigned int i = 0; i < m_n; i++)
			{
				m_r[i] -= alpha * m_q[i];
			}

#if 0
		//if(!(m_iter % 1))
		if(m_restart)
		{
fe_fprintf(stderr, "CONV HARD RESTART %d\n", m_iter);
			m_restart = false;
			m_tmp0 = a_b;
			applyFilters(m_tmp0, a_filters);
			a_P.apply(m_tmp1, m_tmp0);
			delta0 = inner(m_tmp0, m_tmp1);

			residual(m_r, a_A, a_x, a_b);
			applyFilters(m_r, a_filters);

			a_P.apply(m_d, m_r);
			applyFilters(m_d, a_filters);

			deltaNew = inner(m_r,m_d);
			deltaStop = m_tol * m_tol * delta0;

			a_A->multiply(m_q, m_d);
			applyFilters(m_q, a_filters);

			t_real alpha = deltaNew / inner(m_d, m_q);

			for(unsigned int i = 0; i < m_n; i++)
			{
				a_x[i] += alpha * m_d[i];
			}
		}
		else
		{
			for(unsigned int i = 0; i < m_n; i++)
			{
				m_r[i] -= alpha * m_q[i];
			}
		}
#endif

#if 0
#ifdef FE_BLOCKPCG_RESTART
		if(!(m_iter % modn))
		{
			residual(m_r, a_A, a_x, a_b);
			applyFilters(m_r, a_filters);
		}
		else
		{
#endif
			for(unsigned int i = 0; i < m_n; i++)
			{
				m_r[i] -= alpha * m_q[i];
			}
#ifdef FE_BLOCKPCG_RESTART
		}
#endif
#endif

		a_P.apply(m_s, m_r);

		deltaOld = deltaNew;

		deltaNew = inner(m_r, m_s);

		t_real beta = deltaNew/deltaOld;

		for(unsigned int i = 0; i < m_n; i++)
		{
			m_d[i] = m_s[i] + beta*m_d[i];
		}

		applyFilters(m_d, a_filters);

		m_iter++;
	}

	if(m_iter < m_maxIterations)
	{
		converged = true;
	}
	residual(m_r, a_A, a_x, a_b);

	//feLogGroup("BlockPCG", "CONVERGED IN %d ITERATIONS\n", m_iter);

	return converged;
}

template <typename T>
inline
BlockDiagonalPreconditioner<T>::BlockDiagonalPreconditioner(void)
{
}

template <typename T>
inline
BlockDiagonalPreconditioner<T>::~BlockDiagonalPreconditioner(void)
{
}

template <typename T>
inline
std::vector< Matrix<3,3,T> > &BlockDiagonalPreconditioner<T>::diagonal(void)
{
	return m_diagonal;
}

template <typename T>
inline
void BlockDiagonalPreconditioner<T>::apply(std::vector<t_v3> &a_r, const std::vector<t_v3> &a_x) const
{
	unsigned int n = a_r.size();
	for(unsigned int i = 0; i < n; i++)
	{
		multiply(a_r[i], m_diagonal[i], a_x[i]);
//fe_fprintf(stderr, "apply P %f %f %f\n", a_x[i][0], a_x[i][1], a_x[i][2]);
//fe_fprintf(stderr, "apply P %f %f %f\n", a_r[i][0], a_r[i][1], a_r[i][2]);
	}
}

#if 0
void OverlappedInversionPreconditioner::apply(std::vector<t_v3> &a_r, const std::vector<t_v3> &a_x) const
{
fe_fprintf(stderr, ".");
	for(unsigned int i = 0; i < a_r.size(); i++)
	{
		a_r[i] = a_x[i];
	}
	unsigned int n = m_block.size();
	for(unsigned int i = 0; i < n; i++)
	{
		ublas::vector<t_real> v, vr;
		unsigned int nn = 3*(m_block[i].m_H - m_block[i].m_L);
		v.resize(nn);
		for(unsigned int j = m_block[i].m_L; j < m_block[i].m_H; j++)
		{
//fe_fprintf(stderr, "%d <- %d\n",(j-m_block[i].m_L)*3+0, j);
			v[(j-m_block[i].m_L)*3+0] = a_x[j][0];
			v[(j-m_block[i].m_L)*3+1] = a_x[j][1];
			v[(j-m_block[i].m_L)*3+2] = a_x[j][2];
		}
#if 0
fe_fprintf(stderr, ">>>>>>> ");
for(unsigned int k = 0; k < v.size(); k++)
{
fe_fprintf(stderr, "%f ", v[k]);
}
fe_fprintf(stderr, "\n");
std::cerr << m_block[i].m_inverse << "\n";
#endif
		v = prod(m_block[i].m_inverse, v);
#if 0
fe_fprintf(stderr, "<<<<<<< ");
for(unsigned int k = 0; k < v.size(); k++)
{
fe_fprintf(stderr, "%f ", v[k]);
}
fe_fprintf(stderr, "\n");
#endif

		//for(unsigned int j = m_block[i].m_m0; j < m_block[i].m_m1; j++)
		for(unsigned int j = m_block[i].m_L; j < m_block[i].m_H; j++)
		{
			if(j < m_block[i].m_m0) continue;
			if(j >= m_block[i].m_m1) continue;
//fe_fprintf(stderr, "%d -> %d\n",j, (j-m_block[i].m_L)*3+0);
			a_r[j][0] = v[(j-m_block[i].m_L)*3+0];
			a_r[j][1] = v[(j-m_block[i].m_L)*3+1];
			a_r[j][2] = v[(j-m_block[i].m_L)*3+2];
		}
	}
}
#endif

template <typename T>
inline
FilterConstraint<T>::FilterConstraint(void)
{
	m_inequality = false;
}

template <typename T>
inline
FilterConstraint<T>::~FilterConstraint(void)
{
}

template <typename T>
inline
void FilterConstraint<T>::setIndex(unsigned long a_index)
{
	m_index = a_index;
}

template <typename T>
inline
const Matrix<3,3,T> &FilterConstraint<T>::filter(void) const
{
	return m_filter;
}

template <typename T>
inline
unsigned long FilterConstraint<T>::index(void) const
{
	return m_index;
}

template <typename T>
inline
void FilterConstraint<T>::makePointConstraint(void)
{
	setAll(m_filter, 0.0);
	m_inequality = false;
}

template <typename T>
inline
void FilterConstraint<T>::makeLineConstraint(const t_v3 &a_direction)
{
	setAll(m_filter, 0.0);
	outerProduct(m_filter, a_direction, a_direction);
	m_inequality = false;
}

template <typename T>
inline
void FilterConstraint<T>::makePlaneConstraint(const t_v3 &a_normal)
{

	setAll(m_filter, 0.0);
	m_filter(0,0) = 1.0 - a_normal[0] * a_normal[0];
	m_filter(0,1) =  - a_normal[0] * a_normal[1];
	m_filter(0,2) =  - a_normal[0] * a_normal[2];
	m_filter(1,0) = m_filter(0,1);
	m_filter(1,1) = 1.0 - a_normal[1] * a_normal[1];
	m_filter(1,2) =  - a_normal[1] * a_normal[2];
	m_filter(2,0) = m_filter(0,2);
	m_filter(2,1) = m_filter(1,2);
	m_filter(2,2) = 1.0 - a_normal[2] * a_normal[2];

	m_inequality = false;
}

template <typename T>
inline
void FilterConstraint<T>::makeInequalityConstraint(const t_v3 &a_axis, t_real a_delta)
{
	m_axis = a_axis;
	normalize(m_axis);
	makePlaneConstraint(m_axis);

	m_inequality = true;
	m_delta = a_delta;
}

template <typename T>
inline
bool FilterConstraint<T>::isInequality(void) const
{
	return m_inequality;
}

template <typename T>
inline
T FilterConstraint<T>::delta(void) const
{
	return m_delta;
}


} /* namespace ext */
} /* namespace fe */

#endif /* __BlockPCG_h__ */

