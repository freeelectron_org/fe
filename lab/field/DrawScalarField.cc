/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawScalarField.h"

namespace fe
{
namespace ext
{

DrawScalarField::DrawScalarField(void)
{
	m_bufferCnt = 0;
	m_va = NULL;
	m_na = NULL;
	m_ca = NULL;

	m_spDrawmode = new(DrawMode);
	m_spDrawmode->setDrawStyle(DrawMode::e_solid);
	m_spDrawmode->setTwoSidedLighting(true);
	m_spDrawmode->setBackfaceCulling(false);
}

DrawScalarField::~DrawScalarField(void)
{
	clean();
}

void DrawScalarField::clean(void)
{
	if(m_va) { delete [] m_va; }
	if(m_na) { delete [] m_na; }
	if(m_ca) { delete [] m_ca; }
}

void DrawScalarField::resize(int a_size)
{
	if(a_size != m_bufferCnt)
	{
		clean();
		m_va = new SpatialVector[a_size];
		m_na = new SpatialVector[a_size];
		m_ca = new Color[a_size];
		m_bufferCnt = a_size;
	}
}

void DrawScalarField::initialize(void)
{
	setAll(m_cellCount, 1.0f);

	cfg<Real>("min") = 0.0;
	cfg<Real>("max") = 1.0;
	cfg<Real>("zdepth") = 0.0;
	cfg<Real>("alpha") = 1.0;
	cfg<Vector3f>("cellCount") = Vector3f(20.0,20.0,4.0);
	cfg<String>("style") = "solid";
}

void DrawScalarField::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_asColor.bind(l_sig->scope());
}

void DrawScalarField::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	m_minColor =	m_drawview.getColor("background",	Color(0.0f,0.0f,0.0f));
	m_maxColor =	m_drawview.getColor("foreground",	Color(1.0f,1.0f,1.0f));
	m_maxSat =		m_drawview.getColor("hilight",		Color(1.0f,0.0f,0.0f));
	m_minSat =		m_drawview.getColor("lolight",		Color(0.0f,0.0f,1.0f));

	hp<DrawI> spDraw(m_drawview.drawI());

	sp<RecordGroup> spRG(m_drawview.group());

	Real zdepth = cfg<Real>("zdepth"); // Z depth of plane
	Real alpha = cfg<Real>("alpha"); // alpha setting
	m_cellCount = cfg<Vector3f>("cellCount"); // cell sampling count

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input");

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);

		m_asScalarField.bind(spRA->layout()->scope());

		if(m_asScalarField.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				sp<ScalarFieldI> spField = m_asScalarField.field(spRA, i);
				if(spField.isValid())
				{
					draw(spDraw, spField, spField->space(), zdepth, alpha);
				}
			}
		}
	}

}

void DrawScalarField::draw(sp<DrawI> spDraw, sp<ScalarFieldI> spField,
		sp<SpaceI> spSpace, Real a_zdepth, Real a_alpha)
{
	Real min = cfg<Real>("min");
	Real max = cfg<Real>("max");
	Real range = max - min;
	SpatialVector location(0.0f, 0.0f, 0.0f);
	SpatialVector cell(1.0/m_cellCount[0],1.0/m_cellCount[1],
			1.0/m_cellCount[2]);
	SpatialVector xcell;
	xcell = cell;
	int cellcnt[3];
	cellcnt[0] = (int)m_cellCount[0];
	cellcnt[1] = (int)m_cellCount[1];
	cellcnt[2] = (int)m_cellCount[2];
	int vcnt = 6*cellcnt[0]*cellcnt[1];
	resize(vcnt);
	int ia = 0;
	spSpace->to(xcell, xcell);
	location[0] = cell[0]/2.0f;

	//for(int i = 0; location[0] < 1.0f; i++)
	for(int i = 0; i < cellcnt[0]; i++)
	{
		location[1] = cell[1]/2.0f;
		for(int j = 0; j < cellcnt[1]; j++)
		{
			location[2] = a_zdepth;
			{
				Real value;
				SpatialVector xlocation;
				xlocation = location;
				spSpace->to(xlocation, xlocation);
				xlocation[2] = a_zdepth;
				value = spField->sample(xlocation);

				SpatialVector vertices[4];
				SpatialVector normals[4];
				for(int ii = 0; ii < 4; ii++)
				{
					set(vertices[ii]);
					set(normals[ii], 0.0, 0.0, 1.0);
				}

				float size[2];
				size[0] = cell[0]/2.0f;
				size[1] = cell[1]/2.0f;
				vertices[0][0] = location[0] - size[0];
				vertices[0][1] = location[1] - size[1];
				vertices[0][2] = location[2];
				SpatialVector l;
				l = vertices[0];
				spSpace->to(l,l);
				l[2] = xlocation[2];
				vertices[0] = l;

				vertices[1][0] = location[0] + size[0];
				vertices[1][1] = location[1] - size[1];
				vertices[1][2] = location[2];
				l = vertices[1];
				spSpace->to(l,l);
				l[2] = xlocation[2];
				vertices[1] = l;

				vertices[2][0] = location[0] - size[0];
				vertices[2][1] = location[1] + size[1];
				vertices[2][2] = location[2];
				l = vertices[2];
				spSpace->to(l,l);
				l[2] = xlocation[2];
				vertices[2] = l;

				vertices[3][0] = location[0] + size[0];
				vertices[3][1] = location[1] + size[1];
				vertices[3][2] = location[2];
				l = vertices[3];
				spSpace->to(l,l);
				l[2] = xlocation[2];
				vertices[3] = l;

				Color color;
				if(value > max)
				{
					color = m_maxSat;
					color[3] = 1.0;
				}
				else if(value < min)
				{
					color = m_minSat;
					color[3] = 1.0;
				}
				else
				{
					Real scale = (value - min)/range;
					color = (((F32)(1.0-scale))*m_minColor)
						+ (((F32)scale)*m_maxColor);
				}
				color[3] *= a_alpha;

				m_ca[ia+0] = color;
				m_ca[ia+1] = color;
				m_ca[ia+2] = color;
				m_ca[ia+3] = color;
				m_ca[ia+4] = color;
				m_ca[ia+5] = color;

				m_va[ia+0] = vertices[0];
				m_va[ia+1] = vertices[1];
				m_va[ia+2] = vertices[2];
				m_va[ia+3] = vertices[2];
				m_va[ia+4] = vertices[1];
				m_va[ia+5] = vertices[3];
				m_na[ia+0] = normals[0];
				m_na[ia+1] = normals[1];
				m_na[ia+2] = normals[2];
				m_na[ia+3] = normals[2];
				m_na[ia+4] = normals[1];
				m_na[ia+5] = normals[3];
				ia += 6;
				location[2] += cell[2];
			}
			location[1] += cell[1];
		}
		location[0] += cell[0];
	}
	if(vcnt > 0)
	{
		m_spDrawmode->setDrawStyle(
			DrawMode::stringToStyle(cfg<String>("style")));
		spDraw->pushDrawMode(m_spDrawmode);
		spDraw->drawTriangles(m_va,m_na,NULL,vcnt,DrawI::e_discrete,true,m_ca);
		spDraw->popDrawMode();
	}
}


} /* namespace ext */
} /* namespace fe */
