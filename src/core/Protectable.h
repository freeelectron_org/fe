/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Protectable_h__
#define __core_Protectable_h__

#define FE_PROTECTED_DEBUG	FALSE
#define FE_CP_DEBUG			FALSE

#define FE_PROTECTED_MT		TRUE

#if FE_PROTECTED_MT
#define SAFEGUARD_PROTECTED													\
		RecursiveMutex::Guard __fe_thread_safeguard(m_counted_mutex)
#else
#define SAFEGUARD_PROTECTED
#endif

namespace fe
{

/**************************************************************************//**
	@brief Base class providing protection counting for cp<>
*//***************************************************************************/
class FE_DL_EXPORT Protectable: public Counted
{
	public:
					Protectable(void):
						m_protection(0)										{}

virtual	I32			protection(void) const
					{
#if FE_PROTECTED_DEBUG
						feLog("Protectable::protection"
								" %p SAFEGUARD_PROTECTED\n",this);
#endif
						SAFEGUARD_PROTECTED;
						FEASSERT(m_protection>=0);
						return m_protection;
					}
virtual	void		protect(void)
					{
#if FE_PROTECTED_DEBUG
						feLog("Protectable::protect"
								" %p SAFEGUARD_PROTECTED\n",this);
#endif
						SAFEGUARD_PROTECTED;
						FEASSERT(m_protection>=0);
						m_protection++;
					}
virtual	void		unprotect(void)
					{
#if FE_PROTECTED_DEBUG
						feLog("Protectable::unprotect"
								" %p SAFEGUARD_PROTECTED\n",this);
#endif
						SAFEGUARD_PROTECTED;
						m_protection--;
						FEASSERT(m_protection>=0);
					}

virtual	Protectable*	clone(Protectable* pInstance=NULL)
					{
						return NULL;
					}

	private:
		I32						m_protection;

#if FE_PROTECTED_MT
mutable	RecursiveMutex	m_counted_mutex;
#endif
};

/**************************************************************************//**
	@brief Copy-On-Write shared pointer

	@ingroup core

	This mechanism is very similar to sp<> except that the
	plain pointer operator will return only as const.

	To access a member in a writable fashion, you must use
	the writable() method.
	If the shared object is protected by any cp<> except the caller,
	the writable() method will clone the object.
	A clone starts unprotected.
*//***************************************************************************/
template<typename T>
class cp: public sp<T>
{
	public:
				cp(void): sp<T>(), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp\n",FE_TYPESTRING(T));
#endif
				}
explicit		cp(T* pT): sp<T>(pT), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp(T*) %p\n",pT,FE_TYPESTRING(T));
#endif
				}
				cp(const cp<T>& rcpT): sp<T>(rcpT), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp(const cp<T>&) %p\n",
							FE_TYPESTRING(T),rcpT.raw());
#endif
				}
				cp(const sp<T>& rspT): sp<T>(rspT), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp(const sp<T>&) %p\n",
							FE_TYPESTRING(T),rspT.raw());
#endif
				}
				template<typename X>
				cp(const cp<X>& rcpX): sp<T>(rcpX), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp(const cp<X>&) %p\n",
							FE_TYPESTRING(T),rcpX.raw());
#endif
				}
				template<typename X>
				cp(const sp<X>& rspX): sp<T>(rspX), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp(const sp<X>&) %p\n",
							FE_TYPESTRING(T),rspX.raw());
#endif
				}
				template<typename X>
				cp(const hp<X>& rhpX): sp<T>(rhpX), m_protection(0)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::cp(const hp<X>&) %p\n",
							FE_TYPESTRING(T),rhpX.raw());
#endif
				}

				// not virtual
				~cp(void)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::~cp %p %d\n",
							FE_TYPESTRING(T),sp<T>::raw(),m_protection);
#endif
					releaseProtection();
				}

		cp<T>	&operator=(T* pT)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::operator=(T*) %p -> %p\n",
							FE_TYPESTRING(T),sp<T>::raw(),pT);
#endif
					if(pT!=sp<T>::raw())
					{
						releaseProtection();
						sp<T>::operator=(pT);
					}
					return *this;
				}
		cp<T>	&operator=(const cp<T>& rcpT)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::operator=(const cp<T>) %p -> %p\n",
							FE_TYPESTRING(T),sp<T>::raw(),rcpT.raw());
#endif
					if(rcpT.raw() != sp<T>::raw())
					{
						releaseProtection();
						sp<T>::operator=(rcpT);
					}
					return *this;
				}
		cp<T>	&operator=(const sp<T>& rspT)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::operator=(const sp<T>& rspT) %p -> %p\n",
							FE_TYPESTRING(T),sp<T>::raw(),rspT.raw());
#endif
					if(rspT.sp<T>::raw() != sp<T>::raw())
					{
						releaseProtection();
						sp<T>::operator=(rspT);
					}
					return *this;
				}
				template<typename X>
		cp<T>	&operator=(const cp<X>& rcpX)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::operator=(const cp<X>& rcpX) %p -> %p\n",
							FE_TYPESTRING(T),sp<T>::raw(),rcpX.raw());
#endif
					releaseProtection();
					sp<T>::operator=(rcpX);
					return *this;
				}
				template<typename X>
		cp<T>	&operator=(const sp<X>& rspX)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::operator=(const sp<X>& rspX) %p -> %p\n",
							FE_TYPESTRING(T),sp<T>::raw(),rspX.raw());
#endif
					releaseProtection();
					sp<T>::operator=(rspX);
					return *this;
				}
				template<typename X>
		cp<T>	&operator=(const hp<X>& rhpX)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::operator=(const hp<X>& rhpX) %p -> %p\n",
							FE_TYPESTRING(T),sp<T>::raw(),rhpX.raw());
#endif
					releaseProtection();
					sp<T>::operator=(rhpX);
					return *this;
				}

const	T*		operator->(void) const
				{
					return sp<T>::operator->();
				}

const	T*		raw(void) const
				{
					return sp<T>::raw();
				}

		T*		writable(void)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::writable %p %d\n",
							FE_TYPESTRING(T),sp<T>::raw(),m_protection);
#endif
					// if anyone else is protecting this data
					if(sp<T>::raw()->protection()>m_protection)
					{
						U32 reprotect=m_protection;
						releaseProtection();

						sp<T>::operator=(
								fe_cast<T>(sp<T>::raw()->clone()));
						while(reprotect)
						{
							protect();
							reprotect--;
						}
					}
					return sp<T>::operator->();
				}

		I32		protection(void) const
				{	FEASSERT(m_protection>=0);
					return m_protection; }
		void	protect(void)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::protect %p %d\n",
							FE_TYPESTRING(T),sp<T>::raw(),m_protection);
#endif
					FEASSERT(m_protection>=0);
					if(!m_protection)
					{
						m_protection++;
						sp<T>::raw()->protect();
					}
				}
		void	unprotect(void)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::unprotect %p %d\n",
							FE_TYPESTRING(T),sp<T>::raw(),m_protection);
#endif
					if(m_protection)
					{
						m_protection--;
						sp<T>::raw()->unprotect();
					}
					FEASSERT(m_protection>=0);
				}

	private:
		void	releaseProtection(void)
				{
#if FE_CP_DEBUG
					feLog("cp< %s >::releaseProtection %p %d\n",
							FE_TYPESTRING(T),
							sp<T>::raw(),m_protection);
#endif
					while(m_protection)
					{
						m_protection--;
						sp<T>::raw()->unprotect();
					}
				}

		I32		m_protection;

#if FE_PTR_TRACK
virtual	void	track(void)
				{
					if(sp<T>::raw())
					{
						sp<T>::raw()->trackReference(this,
								"cp<"+sp<T>::raw()->name()+">");
					}
				}
#endif
};

} /* namespace */

#endif /* __core_Protectable_h__ */

