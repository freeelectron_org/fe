import os
import sys
import string
forge = sys.modules["forge"]

def prerequisites():
    return [    "opview" ]

def setup(module):
    srclist = [ "flight.pmh",
                "DroneControl",
                "DroneDynamics",
                "DroneInput",
                "DroneMission",
                "SurfaceAccessibleDrone",
                "SurfaceAccessorDrone",
                "SurfaceDrone",
                "flightDL",
                ]

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib" ]

    deplibs +=  [ "fexWindowLib",]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexDataToolDLLib",
                "fexGeometryDLLib",
                "fexThreadDLLib" ]

    dll = module.DLL( "fexFlightDL", srclist)

    forge.deps( ["fexFlightDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexFlightDL",                  None,       None) ]

    module.Module('test')
