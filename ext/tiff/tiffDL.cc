/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <tiff/tiff.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexImageDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<ImageTiff>("ImageI.ImageTiff.fe.tif");
	pLibrary->add<ImageTiff>("ImageI.ImageTiff.fe.tiff");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	//* force this version instead of OpenIL version
//	spLibrary->registry()->prioritize("ImageI.ImageTiff.fe.tif",2);
}

}
