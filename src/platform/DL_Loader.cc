/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

namespace fe
{

String DL_Loader::adaptName(String a_filename,String a_prefix,String a_format)
{
	a_filename+=System::getBinarySuffix();

	String longname;
	longname.sPrintf("%s%s.%s",
			a_prefix.c_str(),a_filename.c_str(),a_format.c_str());
	return longname;
}

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64

Result DL_Loader::addPath(String a_absPath)
{
	if(a_absPath.contains("/"))
	{
		a_absPath=a_absPath.substitute("/","\\");
	}

	const size_t len=a_absPath.length()+1;
	wchar_t* wc=new wchar_t[len];
	mbstowcs(wc,a_absPath.c_str(),len);

	//* NOTE add path for any following LoadLibrary*() call
	AddDllDirectory(wc);
	SetDefaultDllDirectories(LOAD_LIBRARY_SEARCH_DEFAULT_DIRS);

	delete[] wc;

	return e_ok;
}

BWORD DL_Loader::openDL(String a_filename,BWORD a_adaptname)
{
	m_name=a_filename;

	if(a_adaptname)
	{
		a_filename=adaptName(a_filename,"","dll");
	}

	if(a_filename.contains("/"))
	{
		a_filename=a_filename.substitute("/","\\");
	}
	if(a_filename.contains("\\"))
	{
		//* NOTE if full path, allow implicit dependencies in that path
		m_handle=LoadLibraryExA(a_filename.c_str(),NULL,
				LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR|
				LOAD_LIBRARY_SEARCH_DEFAULT_DIRS);
	}
	else
	{
		m_handle=LoadLibraryA(a_filename.c_str());
	}

	if(m_handle)
	{
		return TRUE;
	}

	if(System::getVerbose()=="all")
	{
		feLogDirect("%sDL_Loader::openDL \"%s\" failed\n",
				PrefixLog::prefix().c_str(),a_filename.c_str());
		feLogDirect("%s    %s\n",
				PrefixLog::prefix().c_str(),
				errorString(GetLastError()).c_str());
	}

	return FALSE;
}

BWORD DL_Loader::closeDL(void)
{
	if(!m_handle)
	{
		return FALSE;
	}

	if(!FreeLibrary(m_handle))
	{
		feLogDirect("%sDL_Loader::closeDL \"%s\" failed\n",
				PrefixLog::prefix().c_str(),m_name.c_str());
		feLogDirect("%s    %s\n",
				PrefixLog::prefix().c_str(),
				errorString(GetLastError()).c_str());
		return FALSE;
	}
	return TRUE;
}

void *DL_Loader::findDLFunction(String a_symbol)
{
	return GetProcAddress(m_handle,a_symbol.c_str());
}

#else
//* Unix

FE_DL_PUBLIC std::vector<String> DL_Loader::ms_pathArray;

Result DL_Loader::addPath(String a_absPath)
{
	const I32 pathCount=ms_pathArray.size();
	for(I32 pathIndex=0;pathIndex<pathCount;pathIndex++)
	{
		if(ms_pathArray[pathIndex]==a_absPath)
		{
			return e_alreadyAvailable;
		}
	}

	ms_pathArray.push_back(a_absPath);
	return e_ok;
}

BWORD DL_Loader::openDL(String a_filename,BWORD a_adaptname)
{
	if(a_adaptname)
	{
#if FE_OS==FE_OSX
		a_filename=adaptName(a_filename,"lib","dylib");
#else
		a_filename=adaptName(a_filename,"lib","so");
#endif
	}

	m_name=a_filename;

//	feLogDirect("%sDL_Loader::openDL pid %d dlopen \"%s\"\n",
//			PrefixLog::prefix().c_str(),getpid(),a_filename.c_str());

	//* RTLD_LAZY don't resolve symbols until you need them
	//* RTLD_NOW resolves symbols immediately or fails
	//* exactly one of RTLD_LAZY and RTLD_NOW must be specified
	//* RTLD_GLOBAL symbols from the DL can be used in upcoming DL's
	//* RTLD_DEEPBIND (newer?) chooses local symbols over existing symbols
	//* RTLD_NOLOAD can be used on already loaded DL to change flags
	//* RTLD_GLOBAL was required for cross-lib dynamic_cast<> (not anymore?)
	int dlflags=RTLD_NOW;

	const char builtInPath[4][32]=
	{
		"",
		"@rpath/",
		"@loader_path/",
		"@executable_path/"
	};

#if FE_OS==FE_OSX
	const I32 builtInCount=4;
#else
	const I32 builtInCount=1;
#endif

	std::vector<String> failures;

	const I32 pathCount=ms_pathArray.size()+builtInCount;
	for(I32 pathIndex=0;pathIndex<pathCount;pathIndex++)
	{
		const String fullname=
				(pathIndex<builtInCount? builtInPath[pathIndex]:
				ms_pathArray[pathIndex-builtInCount]+"/")+a_filename;

		m_handle=dlopen(fullname.c_str(),dlflags);
//		feLogDirect("DL_Loader::openDL dlopen %s %p\n",
//				fullname.c_str(),m_handle);

		if(m_handle)
		{
//			feLogDirect("DL_Loader::OpenDL() dlopen \"%s\" succeeded\n",
//					fullname.c_str());

			return TRUE;
		}

		String failure;
		failure.sPrintf("(%s) %s\n",fullname.c_str(),dlerror());
		failures.push_back(failure);
	}

	if(System::getVerbose()=="all")
	{
		feLogDirect("%sDL_Loader::openDL \"%s\" failed\n",
				PrefixLog::prefix().c_str(),a_filename.c_str());
		const I32 failureCount=failures.size();
		for(I32 failureIndex=0;failureIndex<failureCount;failureIndex++)
		{
			feLogDirect("%s    %s\n",PrefixLog::prefix().c_str(),
					failures[failureIndex].c_str());
		}
	}
	return FALSE;
}

BWORD DL_Loader::closeDL(void)
{
//	feLogDirect("%sDL_Loader::closeDL pid %d dlclose %p \"%s\"\n",
//			PrefixLog::prefix().c_str(),getpid(),m_handle,m_name.c_str());
	if(!m_handle)
	{
		return FALSE;
	}

#if FALSE&& FE_CODEGEN==FE_PROFILE
	feLogDirect("DL_Loader::closeDL ignored for FE_PROFILE (%s)\n",
			m_name.c_str());
	return TRUE;
#endif

	dlerror();	//* clear previous
	int fail=dlclose(m_handle);
	if(fail)
	{
		feLogDirect("%sDL_Loader::closeDL failed\n",
				PrefixLog::prefix().c_str());
		feLogDirect("%s    %s\n",
				PrefixLog::prefix().c_str(),dlerror());
	}
	m_handle=NULL;

#if FALSE
	//* NOTE dangerous
	void* pCheck=dlopen(m_name.c_str(),RTLD_LAZY|RTLD_NOLOAD);
	if(pCheck)
	{
		feLogDirect("%sDL_Loader::closeDL library still open \"%s\"\n",
				PrefixLog::prefix().c_str(),m_name.c_str());
		dlclose(pCheck);
	}
#endif

#if FALSE
	String command;
	command.sPrintf("grep %s /proc/%d/maps | grep 00000000"
			" | awk '{ printf \"STILL USING %s\\n\" }'",
			m_name.c_str(),getpid(),m_name.c_str());
	system(command.c_str());
#endif

	return !fail;
}

void *DL_Loader::findDLFunction(String a_symbol)
{
	dlerror();	//* clear previous
	void* address=dlsym(m_handle,a_symbol.c_str());
#if FALSE // FE_CODEGEN<=FE_DEBUG
	if(!address)
	{
		feLogDirect("DL_Loader::findDLFunction(%s) failed\n",
				a_symbol.c_str());
		feLogDirect("      %s\n",dlerror());
	}
#endif
	return address;
}

#endif

} // namespace
