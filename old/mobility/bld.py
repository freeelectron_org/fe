import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "UnitThrust",
                "FormDrag",
                "OpaqueDrag",
                "UnitRepulse",
                "Jitter",
                "SimpleNav",
                "mobilityDL" ]

    dll = module.DLL( "fexMobilityDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                ]

    forge.deps( ["fexMobilityDLLib"], deplibs )

