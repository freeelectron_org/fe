/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexWindowDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->addSingleton<TypeNative>("Component.TypeNative.fe");

	pLibrary->add<NativeEventContext>(
			"EventContextI.NativeEventContext.fe");

	pLibrary->add<NativeWindow>("WindowI.NativeWindow.fe");

#if FE_2DGL==FE_2D_X_GFX
	pLibrary->add<NativeKeyboard>("KeyI.NativeKeyboard.fe");
#endif

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
