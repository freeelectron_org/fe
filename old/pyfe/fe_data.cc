/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fex/pyfe.h"
#include "pyfe/Convertor.h"
#include <boost/python.hpp>
using namespace boost::python;
#include "fe_data.h"
#include <iterator>

void exceptionTranslator(fe::Exception const& e)
{
	PyErr_SetString(PyExc_UserWarning, e.getMessage().c_str());
}

class RecordGroupPtr
{
	public:
		RecordGroupPtr(void)
		{
			m_spRG = new fe::RecordGroup();
		}
		void		add(PyObject *pRecord)
		{
			m_spRG->add(pyfe::extractRecord(pRecord));
		}
		void		remove(PyObject *pRecord)
		{
			m_spRG->remove(pyfe::extractRecord(pRecord));
		}
		void		clear(void)
		{
			m_spRG->clear();
		}
		bool		bind(PyObject *pInstance)
		{
			fe::Instance instance = pyfe::extractInstance(pInstance);
			if(instance.cast<fe::sp<fe::RecordGroup> >().isValid())
			{
				m_spRG = instance.cast<fe::sp<fe::RecordGroup> >();
				return true;
			}
			return false;
		}
		void		put(PyObject *pInstance)
		{
			fe::Instance instance = pyfe::extractInstance(pInstance);
			instance.cast<fe::sp<fe::RecordGroup> >() = m_spRG;
		}
		class iterator : public std::iterator<std::forward_iterator_tag,PyObject *>
		{
			public:
				friend class RecordGroupPtr;
#if 0
				PyObject		*operator*(void)
				{
					fe::sp<fe::RecordArray> spRA = *m_iterator;
					return pyfe::bindRecordArray(spRA);
				}
#endif
				PyObject		*&operator*(void)
				{
					fe::sp<fe::RecordArray> spRA = *m_iterator;
					m_pObject = pyfe::bindRecordArray(spRA);
					return m_pObject;
				}
				iterator		operator++(int)
				{
					iterator it = (*this);
					m_iterator++;
					return it;
				}
				iterator		operator++()
				{
					m_iterator++;
					return *this;
				}
				bool			operator==(const iterator &other)
				{
					return (m_iterator == other.m_iterator);
				}
				bool			operator!=(const iterator &other)
				{
					return (m_iterator != other.m_iterator);
				}
			private:
				fe::RecordGroup::iterator	m_iterator;
				PyObject					*m_pObject;
		};
		iterator	begin(void)
		{
			iterator it;
			it.m_iterator = m_spRG->begin();
			return it;
		}
		iterator	end(void)
		{
			iterator it;
			it.m_iterator = m_spRG->end();
			return it;
		}
	private:
		fe::sp<fe::RecordGroup>	m_spRG;
};


FE_PYTHON_MODULE(data)
{
	fe::sp<fe::TypeMaster> spTypeMaster(pyfe::feMaster().typeMaster());
	fe::sp<fe::BaseType> spBT=
			spTypeMaster->assertPtr<fe::WatcherI>("WatcherI");

	register_exception_translator<fe::Exception>(&exceptionTranslator);
	class_<ScopePtr>("Scope")
		.def("declare", &ScopePtr::declare)
		.def("lookupLayout", &ScopePtr::lookupLayout)
		.def("clear", &ScopePtr::clear)
		.def("createRecord", &ScopePtr::createRecord, "create a record")
		.def("createRecordArray", &ScopePtr::createRecordArray)
		.def("support", &ScopePtr::support)
		.def("enforce", &ScopePtr::enforce)
		.def("populate", &ScopePtr::populate)
		.def("addWatcher", &ScopePtr::addWatcher)
		.def("dump", &ScopePtr::dump)
	;
	class_<LayoutPtr>("Layout")
		.def(init<PyObject *>())
		.def("populate", &LayoutPtr::populate)
		.def("scope", &LayoutPtr::scope)
		.def("name", &LayoutPtr::name)
	;
	class_<RecordGroupPtr>("RecordGroup")
		.def("add", &RecordGroupPtr::add)
		.def("remove", &RecordGroupPtr::remove)
		.def("clear", &RecordGroupPtr::clear)
		.def("bind", &RecordGroupPtr::bind)
		.def("put", &RecordGroupPtr::put)
		.def("__iter__", boost::python::iterator<RecordGroupPtr>())
	;
	object watcherClass=
			class_<WatcherBase, WatcherFS, boost::noncopyable>("WatcherFS")
		.def(init<PyObject *>())
		.def("object", &WatcherFS::object)
	;
	fe::sp<pyfe::BaseConvertor> spC(
		new pyfe::Convertor<fe::sp<fe::WatcherI>,WatcherFS>(watcherClass));
	pyfe::addConvertor(spBT, spC);
}


