#ifndef XS_H_INCLUDED
#define XS_H_INCLUDED

#include "xs_defs.h"
#include "xstring.h"
#include "fixed_char_buf.h"
#include "var_char_buf.h"

XS_NAMESPACE(xstr)

// Convenience typedefs.  These give some generic names
// for some reasonable choices of implementation and
// buffer size.
typedef xstring<fixed_char_buf<32 > > string32;
typedef xstring<fixed_char_buf<64 > > string64;
typedef xstring<fixed_char_buf<128 > > string128;

typedef xstring<var_char_buf<32> > varchar;

XS_END_NAMESPACE

#endif
