/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#ifndef __NETUTIL_H__
#define __NETUTIL_H__

#include "osheader.h"

namespace fe
{
namespace ext
{

class NetUtil
{
public:
	static NetUtil &Instance()
	{
		static NetUtil instance;
		return instance;
	}

	~NetUtil()
	{
		Cleanup();
	}

	bool IsNetworkInit()
	{
#ifdef _WIN32
		SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (s == INVALID_SOCKET && WSAGetLastError() == WSANOTINITIALISED)
		{
			return false;
		}

		closesocket(s);
#endif
		return true;
	}

	int GetLastError()
	{
#ifdef _WIN32
		return WSAGetLastError();
#else
		return errno;
#endif
	}

protected:
	NetUtil() { Init(); }

	bool Init()
	{
#ifdef _WIN32
		WSADATA wsaData;
		WORD version = MAKEWORD(2, 2);

		if (WSAStartup(version, &wsaData) != 0)
		{
			feLog("winsock initialization error\n");
			return false;
		}
#endif
		return true;
	}

	void Cleanup()
	{
#ifdef _WIN32
		WSACleanup();
#endif
	}
};

} // namespace ext
} // namespace fe

#endif
