import sys
import os
forge = sys.modules["forge"]

def setup(module):

    deplibs = [ ]

    # TODO others
    if forge.fe_os == 'FE_LINUX':
        test_output = "output/test/terminal"
        if not os.path.exists(test_output):
            os.makedirs(test_output, 0o755)

        tests = [   'xTerminal' ]

        for t in tests:
            module.Exe(t)
            forge.deps([t + "Exe"], deplibs)

        fe_binary_suffix = os.environ.get('FE_BINARY_SUFFIX', '')

        forge.tests += [
            ("xTerminal.exe",   "libfexTerminalDL"+fe_binary_suffix+".so ext/terminal/test/bloat_test.fesh",    None,   None),
            ("xTerminal.exe",   "libfexTerminalDL"+fe_binary_suffix+".so ext/terminal/test/wrap_test.fesh", None,   None) ]

        if forge.media:
            forge.tests += [
                ("xTerminal.exe",   "libfexTerminalDL.so ext/terminal/test/rg_test.fesh",   None,   None) ]

