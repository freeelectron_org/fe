/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Matter_h__
#define __intelligence_Matter_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Matter RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Matter: public Particle
{
	public:
		typedef enum
				{
					e_null=		0x00,
					e_discard=	0x01
				} Flags;

		Functor<String>		identifier;
		Functor<WeakRecord>	pattern;
		Functor<I32>		flags;
		Functor<F32>		metal;
		Functor<F32>		fuel;
		Functor<F32>		gem;

				Matter(void)		{ setName("Matter"); }
virtual	void	addFunctors(void)
				{
					Particle::addFunctors();

					add(identifier,	FE_SPEC("comp:identifier",
							"Readable non-unique identifier"));
					add(pattern,	FE_SPEC("comp:pattern",
							"What this Record wants to be like"));
					add(flags,		FE_SPEC("comp:flags",
							"Bit-wise flags"));
					add(metal,		FE_SPEC("comp:metal",
							"Mass of structural content"));
					add(fuel,		FE_SPEC("comp:fuel",
							"Mass of combustible content"));
					add(gem,		FE_SPEC("comp:gem",
							"Mass of computing content"));
				}
virtual	void	initializeRecord(void)
				{
					Particle::initializeRecord();

					identifier()="Matter";
					flags()=e_null;
					metal()=0.0f;
					fuel()=0.0f;
					gem()=0.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Matter_h__ */

