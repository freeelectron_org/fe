/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "lua/lua.h"
#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

class TestDispatch:
	virtual public Dispatch,
	Initialize<TestDispatch>
{
	public:
virtual void	initialize(void)
		{
			dispatch<Record>("my_function");
			dispatch<int>("my_function");

			dispatch<float>("sqr");
		}

		using Dispatch::call;

virtual	bool	call(const String &a_name, Array<Instance>& a_argv)
		{
			if(a_name == "my_function")
			{
				Record r = a_argv[0].cast<Record>();
				int &i = a_argv[1].cast<int>();
				feLog("myfunction(record, %d)\n", i);
				i = 42;
			}

			if(a_name == "sqr")
			{
				float &f = a_argv[0].cast<float>();
				f = f*f;
			}


			return true;
		}
virtual	SignatureMap	&signatures(void)
		{
			return m_signatures;
		}
	private:
		SignatureMap	m_signatures;
		sp<TypeMaster>	m_spTM;
};

extern "C"
{

FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
{
}

FE_DL_EXPORT fe::Library* CreateLibrary(fe::sp<fe::Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<TestDispatch>("DispatchI.TestDispatch.fe");
	return pLibrary;
}

}
