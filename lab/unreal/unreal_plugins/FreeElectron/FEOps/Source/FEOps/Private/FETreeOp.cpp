#include "FETreeOp.h"

AFETreeOp::AFETreeOp(const FObjectInitializer& ObjectInitializer):
	AFEOperator(ObjectInitializer)
{
	feLog("AFETreeOp::AFETreeOp\n");

	m_componentName="TreeOp";
	m_nodeName="TreeActor";
}

void AFETreeOp::OperatorInit(void)
{
	feLog("AFETreeOp::OperatorInit \"%s\" \"%s\"\n",
			m_componentName.c_str(),m_nodeName.c_str());

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();

	spCatalog->catalog<fe::SpatialVector>("gravity")=
			fe::SpatialVector(0.0,0.0,0.0);
	spCatalog->catalog<bool>("YUp")=true;
	spCatalog->catalog<fe::String>("stickForm")="cylinder";
//	spCatalog->catalog<fe::String>("leafForm")="point";
}

void AFETreeOp::OperatorUpdate(void)
{
	feLog("AFETreeOp::OperatorUpdate \"%s\"\n",m_nodeName.c_str());

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feLog("AFETreeOp::OperatorUpdate spCatalog is NULL\n");
		return;
	}

	spCatalog->catalog<fe::SpatialVector>("WindDirection")=fe::SpatialVector(
			0.5*sin(m_time)+0.2*sin(7*m_time),
			0.2*sin(11*m_time),
			0.5*sin(13*m_time));
}
