/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

using namespace fe;
using namespace fe::ext;

void MergeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Input Surface 2");
	catalog<bool>("Input Surface 2","optional")=true;

	catalog< sp<Component> >("Input Surface 3");
	catalog<bool>("Input Surface 3","optional")=true;

	catalog< sp<Component> >("Input Surface 4");
	catalog<bool>("Input Surface 4","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
}

void MergeOp::handle(Record& a_rSignal)
{
	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	I32 mergeCount(1);

	for(I32 index=2;index<4;index++)
	{
		String inputName;
		inputName.sPrintf("Input Surface %d",index);

		sp<SurfaceAccessibleI> spInputAccessible;
		if(!access(spInputAccessible,inputName,e_quiet))
		{
			continue;
		}

		spOutputAccessible->append(spInputAccessible,NULL);
		mergeCount++;
	}

	String summary;
	summary.sPrintf("%d input%s",mergeCount,(mergeCount!=1)? "s": "");
	catalog<String>("summary")=summary;
}
