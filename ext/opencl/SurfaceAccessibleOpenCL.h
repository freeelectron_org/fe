/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_SurfaceAccessibleOpenCL_h__
#define __opencl_SurfaceAccessibleOpenCL_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL Surface Binding

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleOpenCL:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleOpenCL>
{
	public:
								SurfaceAccessibleOpenCL(void);
virtual							~SurfaceAccessibleOpenCL(void);

static	void					updateDrawBuffer(
										sp<DrawBufferI> a_spDrawBuffer,
										sp<Master> a_spMaster,
										sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI,
										I32 a_vertCount);

								//* As Protectable
virtual	Protectable*			clone(Protectable* pInstance=NULL);

								//* as SurfaceAccessibleI
virtual	BWORD					isBound(void)				{ return TRUE; }

								//* TODO guard the allocations
virtual	BWORD					threadable(void)			{ return FALSE; }

								using SurfaceAccessibleBase::count;

virtual	I32						count(String a_nodeName,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);

								using SurfaceAccessibleBase::discard;

virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name);

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

virtual	sp<MultiGroup>			generateGroup(
										SurfaceAccessibleI::Element a_element,
										String a_groupString);

								//* OpenCL specific

	class ClMem:
		public Counted,
		public CastableAs<ClMem>
	{
		public:
					ClMem(void):
						m_clMem(NULL)										{}

					~ClMem(void)
					{	clReleaseMemObject(m_clMem); }

			cl_mem		m_clMem;
	};

	class Bridge:
		public ObjectSafeShared<Bridge>,
		public Protectable,
		public CastableAs<Bridge>
	{
		public:
							Bridge(cl_context a_clContext):
								m_bufferData(NULL),
								m_elementCount(0),
								m_allocated(0),
								m_sizeof(0),
								m_clContext(a_clContext),
								m_cpuCurrent(TRUE),
								m_gpuCurrent(TRUE)							{}

							~Bridge(void);

							//* As Protectable
	virtual	Protectable*	clone(Protectable* pInstance=NULL);

							//* will copy CPU data to GPU,
							//* if GPU data not current
			sp<ClMem>		getClMem(void);

							//* copy GPU data to CPU, if CPU data not current
			void			refresh(void);


			String			m_name;
			String			m_type;
			String			m_rate;

			std::set<I32>	m_groupSet;
			void*			m_bufferData;
			I32				m_elementCount;
			I32				m_allocated;
			I32				m_sizeof;

			cl_context		m_clContext;
			sp<ClMem>		m_spClMem;
			sp<ClMem>		m_spClMemPrevious;

			sp<Master>		m_spMaster;
			BWORD			m_cpuCurrent;
			BWORD			m_gpuCurrent;
	};

		cp<Bridge>&				lookupBridge(String a_name,BWORD a_create);

	protected:
virtual	void					reset(void);

	private:
		void					updateContext(void);

		sp<SurfaceAccessorI>	createAccessor(void);

		std::map< String, cp<Bridge> >	m_bridgeMap;
		cp<Bridge>						m_bridgeNull;

		cl_context						m_clContext;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_SurfaceAccessibleOpenCL_h__ */
