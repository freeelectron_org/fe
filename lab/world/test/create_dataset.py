#!/usr/bin/python
import whrandom

x_range = 10.0
y_range = 10.0
cnt = 20

f = open("ds.rg", "w")

txt = """INFO 5
ATTRIBUTE spc:velocity spatial_vector
ATTRIBUTE spc:at spatial_vector
ATTRIBUTE sim:force spatial_vector
ATTRIBUTE sim:mass real
ATTRIBUTE bnd:radius real

ATTRIBUTE sel:selectable void
ATTRIBUTE sel:wld:sphere void
ATTRIBUTE proj:location spatial_vector

LAYOUT "particle"
    spc:velocity
    spc:at
    sim:force
    sim:mass
    bnd:radius
    sel:selectable
    sel:wld:sphere
    proj:location

"""

f.write(txt)
txt = ""



for i in range(1,cnt):
    x = x_range * whrandom.random()
    y = y_range * whrandom.random()

    record = """
RECORD %d particle
    spc:velocity "0.0 0.0 0.0"
    sim:force "0.0 0.0 0.0"
    spc:at "%f %f 0.0"
    sim:mass 1.0
    bnd:radius 0.2
""" % (i, x, y)

    txt += record
    txt += "\n"

    f.write(txt)
    txt = ""

txt += "RECORDGROUP 1\n"
f.write(txt)
txt = ""


for i in range(1,cnt):
    txt += "\t%d\n" % (i)
    f.write(txt)
    txt = ""

txt += "END\n"
f.write(txt)
txt = ""

f.close()

