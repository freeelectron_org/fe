/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_vertStart,__global int* a_vertCount,
	__global float4* a_position2,
	__global int* a_vertStart2,__global int* a_vertCount2,
	__global int* a_guideIndex,
	float a_convergenceRoot,float a_convergenceTip,float a_convergenceGamma,
	__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];
	if(subCount<1)
	{
		return;
	}

	int primitiveIndex2=a_guideIndex[primitiveIndex];
	if(primitiveIndex2<0)
	{
		return;
	}

	int subStart2=a_vertStart2[primitiveIndex2];
	int subCount2=a_vertCount2[primitiveIndex2];

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		int pointIndex=subStart+subIndex;
		float4 input=a_position[pointIndex];

		int pointIndex2=subStart2+subIndex;
		float4 clumpTo=a_position2[pointIndex2];

		float along=subIndex/(float)(subCount-1);

		float convergenceAlong=a_convergenceRoot+
				(a_convergenceTip-a_convergenceRoot)*
				powr(along,a_convergenceGamma);

		float4 output=input*(1-convergenceAlong)+
				clumpTo*convergenceAlong;

		a_position[pointIndex]=output;
	}
}
