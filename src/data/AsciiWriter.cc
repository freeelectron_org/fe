/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#include <sstream>

namespace fe
{
namespace data
{

AsciiWriter::AsciiWriter(sp<Scope> spScope) : Scanner(spScope)
{
}

AsciiWriter::~AsciiWriter(void)
{
}

void AsciiWriter::output(std::ostream &a_ostrm, sp<RecordGroup> spRG)
{
	String s;
	scan(spRG);
	setupSBIDs(m_spScannedRecords);

	m_defaultRGID=0;

	// info
	s.sPrintf("%s %d\n", FE_KW_INFO, FE_SERIAL_VERSION);
	a_ostrm << s.c_str();

	// attributes
	for(unsigned int i = m_sentAttrID; i < m_attrArray.size(); i++)
	{
		write(a_ostrm, m_attrArray[i]);
	}
	m_sentAttrID = m_attrArray.size();

	// layouts
	for(t_layout_loinfo::iterator it = m_layouts.begin();
		it != m_layouts.end(); it++)
	{
		if(!it->second.m_sent)
		{
			write(a_ostrm, it->first, it->second.m_id);
			it->second.m_sent = true;
		}
	}

	// state blocks (records)
	int sb_id = 1;
	for(RecordGroup::iterator it = m_spScannedRecords->begin();
		it != m_spScannedRecords->end(); it++)
	{
		sp<RecordArray> spRA = *it;
		for(int i = 0; i < spRA->length(); i++)
		{
			write(a_ostrm, spRA->getRecord(i), sb_id++);
		}
	}
	m_spScannedRecords->clear();

	// record groups
	for(t_rg_id::iterator it = m_rgs.begin(); it != m_rgs.end(); it++)
	{
		write(a_ostrm, it->first, it->second);
	}

	// record arrays as groups
	for(t_ra_id::iterator it = m_ras.begin(); it != m_ras.end(); it++)
	{
		write(a_ostrm, it->first, it->second);
	}

	a_ostrm << FE_KW_END << "\n";

	m_nextRGID = 1;
	m_rgs.clear();
	m_ras.clear();
	m_sbs.clear();

}

void AsciiWriter::write(std::ostream &a_ostrm, Record r_out, int a_sb_id)
{
	const U32 defaultRGID=m_firstRGID[a_sb_id];

	if(m_defaultRGID!=defaultRGID)
	{
		a_ostrm << FE_KW_DEFAULTGROUP;

		m_defaultRGID=defaultRGID;
		FEASSERT(defaultRGID>0);

		if(m_defaultRGID>0)
		{
			a_ostrm << " ";
			if(m_defaultRGID>1)
			{
				a_ostrm << "RG";
			}
			a_ostrm << m_defaultRGID;
		}
		a_ostrm << "\n";
	}

	sp<Layout> spLayout=r_out.layout();
	a_ostrm << FE_KW_RECORD << " "
			<< (spLayout->name() + a_sb_id).maybeQuote().c_str() << " "
			<< spLayout->name().maybeQuote().c_str()<< "\n";
	sp<Scope> spScope = spLayout->scope();
	FE_UWORD cnt = spScope->getAttributeCount();
	for(FE_UWORD i = 0; i < cnt; i++)
	{
		if(spLayout->checkAttribute(i))
		{
			if(spScope->attribute(i)->isSerialize())
			{
				sp<BaseType> spBT = spScope->attribute(i)->type();
				if(spBT == m_spVoidType)
				{
					continue;
				}
				a_ostrm <<	"\t"
						<<	spScope->attribute(i)->name().c_str()
						<<	" ";
				if(spBT == m_spRecordGroupType)
				{
					sp<RecordGroup> *pspRG;
					void *instance = r_out.rawAttribute(i);
					pspRG = reinterpret_cast<sp<RecordGroup> *>(instance);

					// OUTPUT: r_out group id
					if(pspRG->isValid())
					{
						U32 id=(U32)getID(*pspRG);
						a_ostrm << (id==1? "": "RG") << id;
					}
					else
					{
						a_ostrm << 0;
					}
				}
				else if(spBT == m_spRecordArrayType)
				{
					sp<RecordArray> *pspRA;
					void *instance = r_out.rawAttribute(i);
					pspRA = reinterpret_cast<sp<RecordArray> *>(instance);

					// OUTPUT: record array (group) id
					if(pspRA->isValid())
					{
						U32 id=(U32)getID(*pspRA);
						a_ostrm << (id==1? "": "RG") << id;
					}
					else
					{
						a_ostrm << 0;
					}
				}
				else if(spBT == m_spRecordType)
				{
					Record *pR;
					void *instance = r_out.rawAttribute(i);
					pR = reinterpret_cast<Record *>(instance);

					// OUTPUT: record id
					if(pR->isValid())
					{
						if(m_sbs.find(pR->idr()) == m_sbs.end())
						{
							a_ostrm << 0;
						}
						a_ostrm << (pR->layout()->name() +
								m_sbs[pR->idr()]).maybeQuote().c_str();
					}
					else
					{
						a_ostrm << 0;
					}
				}
				else if(spBT == m_spWeakRecordType)
				{
					WeakRecord *pR;
					void *instance = r_out.rawAttribute(i);
					pR = reinterpret_cast<WeakRecord *>(instance);

					// OUTPUT: record id
					if(pR->isValid())
					{
						if(m_sbs.find(pR->idr()) == m_sbs.end())
						{
							a_ostrm << 0;
						}
						a_ostrm << (pR->layout()->name() +
								m_sbs[pR->idr()]).maybeQuote().c_str();
					}
					else
					{
						a_ostrm << 0;
					}
				}
				else if(spBT->getInfo().isValid())
				{
					void *instance = r_out.rawAttribute(i);

					// OUTPUT: instance data via Info
					if(-1 == spBT->getInfo()->output(a_ostrm, instance,
						BaseType::Info::e_ascii))
					{
						std::ostringstream str_ostrm;
						int n = spBT->getInfo()->output(str_ostrm, instance,
							BaseType::Info::e_binary);

						std::string string=str_ostrm.str();
#if 0
						feLog("\"%s\" %d\n",string.c_str(),string.size());
						for(U32 m=0;m<string.size();m++)
							feLog("0x%x\n",string.data()[m]);
#endif

						writeBinaryBlock(a_ostrm,
							(const void *)(str_ostrm.str().data()),n);
//						feLog("written\n");
						a_ostrm.flush();
					}
				}

				a_ostrm << "\n";
			}
		}
	}
}

void AsciiWriter::writeBinaryBlock(std::ostream &a_ostrm,
		const void *a_ptr, int a_size)
{
	a_ostrm << FE_KW_STARTBINARY;
	I32 n = htonl(a_size);
	a_ostrm.write((char *)&n, sizeof(I32));
	a_ostrm.write((char *)a_ptr, a_size);
	a_ostrm << FE_KW_ENDBINARY;
}

void AsciiWriter::write(std::ostream &a_ostrm, sp<Attribute> spAttribute)
{
	sp<BaseType> spBT = spAttribute->type();

	// OUTPUT: attribute name
	a_ostrm << FE_KW_ATTRIBUTE << " " << spAttribute->name().c_str();

	sp<Scope> spScope = m_attrs[spAttribute].m_scope;

	if(spScope != m_spScope)
	{
		a_ostrm << " @" << spScope->name().c_str();
	}

	std::list<String> typenames;
	spScope->typeMaster()->reverseLookup(spBT, typenames);

	std::list<String>::iterator it;
	for(it = typenames.begin();it != typenames.end(); it++)
	{
		// OUTPUT: typename
		a_ostrm << " " << it->c_str();
	}
	a_ostrm << "\n";
}

void AsciiWriter::write(std::ostream &a_ostrm, sp<RecordGroup> spRG, int a_id)
{
	// OUTPUT: rg id
	a_ostrm << FE_KW_RECORDGROUP << (spRG->isWeak()? "(weak)": "")
			<< (a_id==1? " ": " RG") << a_id << "\n";

	for(RecordGroup::iterator it = spRG->begin(); it != spRG->end(); it++)
	{
		sp<RecordArray> spRA = *it;
		for(int i = 0; i < spRA->length(); i++)
		{
			const U32 record_id=spRA->idr(i);

			// OUTPUT: record id
			if(m_sbs.find(record_id) == m_sbs.end())
			{
				feX(e_cannotFind,
					"AsciiWriter::write RG",
					"attempt to write unknown record");
			}

			const I32 sb_id=m_sbs[record_id];

			if(m_firstRGID[sb_id]==a_id)
			{
				//* already added using DEFAULTGROUP
				continue;
			}

			a_ostrm << "\t"
					<< (spRA->layout()->name() + sb_id).maybeQuote().c_str()
					<< "\n";
		}
	}
}

void AsciiWriter::write(std::ostream &a_ostrm, sp<RecordArray> spRA, int a_id)
{
	// OUTPUT: rg id
	a_ostrm << FE_KW_RECORDGROUP << (spRA->isWeak()? "(weak)": "")
			<< (a_id==1? " ": " RG") << a_id << "\n";

	for(int i = 0; i < spRA->length(); i++)
	{
		// OUTPUT: record id
		if(m_sbs.find(spRA->idr(i)) == m_sbs.end())
		{
			feX(e_cannotFind,
				"AsciiWriter::write RA",
				"attempt to write unknown record");
		}
		a_ostrm << "\t" << (spRA->layout()->name() +
				m_sbs[spRA->idr(i)]).maybeQuote().c_str() << "\n";
	}
}

void AsciiWriter::write(std::ostream &a_ostrm, sp<Layout> spLayout, int a_id)
{
	spLayout->initialize();

	// OUTPUT: layout id
	a_ostrm << FE_KW_LAYOUT << " "
			<< spLayout->name().maybeQuote().c_str();

	sp<Scope> spScope = m_spScope;
	if(spLayout->scope() != m_spScope)
	{
		spScope = spLayout->scope();
		a_ostrm << " @" << spScope->name().c_str();
	}
	a_ostrm << "\n";

	FE_UWORD cnt = spScope->getAttributeCount();
	for(FE_UWORD i = 0; i < cnt; i++)
	{
		if(spLayout->checkAttribute(i))
		{
			sp<BaseType> spBT = spScope->attribute(i)->type();
			if(spBT->getInfo().isValid())
			{
				if(spScope->attribute(i)->isSerialize())
				{
					a_ostrm << "\t" << spScope->attribute(i)->name().c_str();
					a_ostrm << "\n";
				}
			}
		}
	}
}

} /* namespace */
} /* namespace */

