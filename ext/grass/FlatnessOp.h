/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_FlatnessOp_h__
#define __grass_FlatnessOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief lay curves down towards a direction, such as a skin tangent

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT FlatnessOp:
	public OperatorSurfaceCommon,
	public Initialize<FlatnessOp>
{
	public:
				FlatnessOp(void)											{}
virtual			~FlatnessOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		Noise	m_noise;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_FlatnessOp_h__ */
