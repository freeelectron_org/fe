import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):
    srcList = [ "shade.pmh",
                "ShaderCommon",
                "shadeDL" ]

    dll = module.DLL( "fexShadeDL", srcList )

    deplibs = forge.corelibs+ [
                "fexThreadDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexDataToolDLLib",
                "fexDrawDLLib" ]

    forge.deps( ["fexShadeDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexShadeDL",                   None,   None) ]
