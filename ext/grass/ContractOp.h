/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_ContractOp_h__
#define __grass_ContractOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief receed points back along on their original curves

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT ContractOp:
	public OperatorSurfaceCommon,
	public Initialize<ContractOp>
{
	public:
				ContractOp(void)												{}
virtual			~ContractOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_ContractOp_h__ */
