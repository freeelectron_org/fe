/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "OpaqueDrag.h"

namespace fe
{

OpaqueDrag::OpaqueDrag(void)
{
}

OpaqueDrag::~OpaqueDrag(void)
{
	cfg<String>("waste") = "0.0";
}

void OpaqueDrag::handle(Record &r_sig)
{
	AsParticle asParticle;
	asParticle.bind(r_sig.layout()->scope());

	Real timestep = cfg<Real>("timestep");

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	Real waste = atof(cfg<String>("waste").c_str());

	Real decay = exp(-waste*timestep);

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				asParticle.velocity(spRA, i) *= decay;
			}
		}
	}
}

} /* namespace */


