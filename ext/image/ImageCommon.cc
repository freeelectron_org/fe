/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <image/image.pmh>

namespace fe
{
namespace ext
{

ImageCommon::ImageCommon(void)
{
}

ImageCommon::~ImageCommon(void)
{
}

I32 ImageCommon::serial(void) const
{
	const I32 index=selected();
	return index<I32(m_serialArray.size())? m_serialArray[index]: -1;
}

void ImageCommon::incrementSerial(void)
{
	const I32 index=selected();
	const I32 size=m_serialArray.size();
	if(index>=size)
	{
		m_serialArray.resize(index+1,0);
	}
	m_serialArray[index]++;
}

} /* namespace ext */
} /* namespace fe */
