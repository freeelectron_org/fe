/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawScan_h__
#define __intelligence_DrawScan_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw scan shapes

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawScan: public DrawShape, public HandlerI
{
	public:
				DrawScan(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		sp<DrawMode>				m_spWireframe;

		sp<SelectorI>				m_spSelector;
		Theater						m_theaterRV;
		Observation					m_observationRV;
		Cylinder					m_cylinderRV;
		RecordArrayView<Scanner>	m_scannerRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawScan_h__ */
