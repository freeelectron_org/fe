/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperatorPlugin_h__
#define __operate_OperatorPlugin_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Plugin back reference for OperatorSurfaceI

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperatorPlugin
{
	public:

virtual	void	dirty(BWORD a_aggressive)									=0;
virtual	void	select(void)												=0;
virtual	BWORD	chronicle(String a_change,sp<Counted> a_spOldCounted,
					sp<Counted> a_spNewCounted)								=0;
virtual	BWORD	interrupted(void)											=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperatorPlugin_h__ */
