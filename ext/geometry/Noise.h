/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_Noise_h__
#define __geometry_Noise_h__


namespace fe
{
namespace ext
{
/**************************************************************************//**
	@brief Perlin Noise

	@ingroup geometry

	https://github.com/Auburns/FastNoise

*//***************************************************************************/
class FE_DL_EXPORT Noise
{
	public:
				Noise(void);
	virtual		~Noise(void);

		void	setSeed(I32 a_seed);

		Real	perlin2d(const Vector2& a_rPosition);
		Real	perlin3d(const Vector3& a_rPosition);

		Real	whiteNoise2d(const Vector2& a_rPosition);
		Real	whiteNoise3d(const Vector3& a_rPosition);

	private:

		void*	m_pFastNoise;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_Noise_h__ */
