/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

Cloner::Cloner(sp<Scope> spScope) : fe::data::Scanner(spScope)
{
}

Cloner::~Cloner(void)
{
}

void Cloner::handleRecord(Record r_src)
{
	sp<Layout> spLayout=r_src.layout();
	sp<Scope> spScope = spLayout->scope();
	FE_UWORD cnt = spScope->getAttributeCount();
	Record r_dst = spScope->createRecord(spLayout);
	m_src_idr_to_dst_record[r_src.idr()] = r_dst;
	for(FE_UWORD i = 0; i < cnt; i++)
	{
		sp<Attribute> spAttribute = spScope->attribute(i);
		if(spLayout->checkAttribute(i))
		{
			sp<BaseType> spBT = spScope->attribute(i)->type();
			String attr_name = spScope->attribute(i)->name();
			if(spBT == m_spVoidType)
			{
				continue;
			}
			if(spBT == m_spRecordGroupType)
			{
				sp<RecordGroup> *pspRG;
				void *instance = r_dst.rawAttribute(i);
				pspRG = reinterpret_cast<sp<RecordGroup> *>(instance);

				sp<RecordGroup> spRGsrc = r_src.accessAttribute< sp<RecordGroup> >(i);
				if(spRGsrc.isValid())
				{
					U32 id=(U32)getID(spRGsrc);
					sp<RecordGroup> spRGdst = m_new_rgs[id];
					*pspRG = spRGdst;
				}
			}
			else if(spBT == m_spRecordArrayType)
			{
				sp<RecordArray> *pspRA;
				void *instance = r_dst.rawAttribute(i);
				pspRA = reinterpret_cast<sp<RecordArray> *>(instance);

				sp<RecordArray> spRAsrc = r_src.accessAttribute< sp<RecordArray> >(i);

				if(spRAsrc.isValid())
				{
					U32 id=(U32)getID(spRAsrc);
					sp<RecordArray> spRAdst = m_new_ras[id];

					*pspRA = spRAdst;
				}
			}
			else if(spBT == m_spRecordType)
			{
				//Record *pR;
				//void *instance = r_dst.rawAttribute(i);
				//pR = reinterpret_cast<Record *>(instance);

				RecordWiringInfo wiring;
				wiring.m_record = r_dst;
				wiring.m_aRecord.setup(spScope, attr_name);
				Record r_target = r_src.accessAttribute<Record>(i);
				if(r_target.isValid())
				{
					wiring.m_idr = r_target.idr();
					m_wiringList.push_back(wiring);
				}
			}
			else if(spBT == m_spWeakRecordType)
			{

				//WeakRecord *pR;
				//void *instance = r_dst.rawAttribute(i);
				//pR = reinterpret_cast<WeakRecord *>(instance);

				WeakRecordWiringInfo wiring;
				wiring.m_record = r_dst;
				wiring.m_aRecord.setup(spScope, attr_name);
				Record r_target = r_src.accessAttribute<WeakRecord>(i);
				if(r_target.isValid())
				{
					wiring.m_idr = r_target.idr();
					m_wkWiringList.push_back(wiring);
				}
			}
			else if(spBT->getInfo().isValid())
			{
				void *dst_instance = r_dst.rawAttribute(i);
				void *src_instance = r_src.rawAttribute(i);

				if(spAttribute->isCloneable())
				{
					spBT->assign(
						dst_instance,
						src_instance);
				}
			}
		}
	}
}

Record Cloner::clone(Record r_src)
{
	scan(r_src);

	deepclone();

	return m_src_idr_to_dst_record[r_src.idr()];
}

sp<RecordGroup> Cloner::clone(sp<RecordGroup> rg_src)
{
	scan(rg_src);

	deepclone();

	return m_new_rgs[m_rgs[rg_src]];
}


sp<RecordGroup> Cloner::cloneShallow(sp<RecordGroup> rg_src)
{
	sp<RecordGroup> spRG(new RecordGroup());

	for(RecordGroup::iterator i_rg = rg_src->begin();
			i_rg != rg_src->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		for(int i = 0; i < spRA->length(); i++)
		{
			spRG->add(spRA->getRecord(i));
		}
	}

	return spRG;
}

void Cloner::deepclone()
{
	// create the new record groups
	for(data::t_rg_id::iterator i_rg = m_rgs.begin(); i_rg != m_rgs.end(); i_rg++)
	{
		m_new_rgs[i_rg->second] = new RecordGroup();
	}

	// create the new record arrays
	for(data::t_ra_id::iterator i_ra = m_ras.begin(); i_ra != m_ras.end(); i_ra++)
	{
		m_new_ras[i_ra->second] = new RecordArray();
	}

	// clone the new records
	for(RecordGroup::iterator it = m_spScannedRecords->begin();
		it != m_spScannedRecords->end(); it++)
	{
		sp<RecordArray> spRA = *it;
		for(int i = 0; i < spRA->length(); i++)
		{
			handleRecord(spRA->getRecord(i));
		}
	}
	m_spScannedRecords->clear();

	for(data::t_rg_id::iterator i_rg = m_rgs.begin(); i_rg != m_rgs.end(); i_rg++)
	{
		sp<RecordGroup> spRGsrc = i_rg->first;
		sp<RecordGroup> spRGdst = m_new_rgs[i_rg->second];
		for(RecordGroup::iterator it = spRGsrc->begin(); it != spRGsrc->end(); it++)
		{
			sp<RecordArray> spRAsrc = *it;
			for(int i = 0; i < spRAsrc->length(); i++)
			{
				spRGdst->add(m_src_idr_to_dst_record[spRAsrc->idr(i)]);
			}
		}
	}

	for(data::t_ra_id::iterator i_ra = m_ras.begin(); i_ra != m_ras.end(); i_ra++)
	{
		sp<RecordArray> spRAsrc = i_ra->first;
		sp<RecordArray> spRAdst = m_new_ras[i_ra->second];

		for(int i = 0; i < spRAsrc->length(); i++)
		{
			spRAdst->add(m_src_idr_to_dst_record[spRAsrc->idr(i)]);
		}
	}

	// wiring
	for(t_wiring::iterator it = m_wiringList.begin();
		it != m_wiringList.end(); it++)
	{
		it->m_aRecord(it->m_record) = m_src_idr_to_dst_record[it->m_idr];
	}

	for(t_wk_wiring::iterator it = m_wkWiringList.begin();
		it != m_wkWiringList.end(); it++)
	{
		it->m_aRecord(it->m_record) = m_src_idr_to_dst_record[it->m_idr];
	}


}


} /* namespace */


