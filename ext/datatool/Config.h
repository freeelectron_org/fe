/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Config_h__
#define __data_Config_h__

#include "ConfigI.h"

namespace fe
{
namespace ext
{

/**	Convienience base class for specification and use of path accessors.
	*/
class FE_DL_EXPORT Config : virtual public ConfigI
{
	public:
				Config(void);
virtual			~Config(void);


		template <typename T>
			T		&cfg(const String &a_key);

virtual		void	configParent(sp<ConfigI> a_parent);
virtual		bool	configLookup(const String &a_key, Instance &a_instance);

virtual		void	configSet(const String &a_key, Instance &a_instance);
virtual		void	configSet(const String &a_key, Record &a_record,
						const String &a_attr);

	private:
		//std::map<String, String>		m_dictionary;
		sp<ConfigI>						m_parent;
		std::map<String, Instance>		m_catalog;
};

template <typename T>
T &Config::cfg(const String &a_key)
{
	Instance instance;
	bool preexists = configLookup(a_key, instance);

	if(preexists)
	{
		return instance.cast<T>();
	}

	if(!registry().isValid())
	{
		feX("Config Components must be created so as to have a registry");
	}
	const sp<TypeMaster> &spTM = registry()->master()->typeMaster();
	Instance &new_instance = m_catalog[a_key];
	new_instance.create<T>(spTM);
	return new_instance.cast<T>();
}

} /* namespace ext */
} /* namespace fe */

#endif /* __data_Config_h__ */
