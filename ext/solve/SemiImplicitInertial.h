/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_SemiImplicitInertial_h__
#define __solve_SemiImplicitInertial_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
//#include "mechanics/mechanics.h"

namespace fe
{
namespace ext
{

/**	Semi Implicit time integration

	@copydoc SemiImplicitInertial_info
	*/
class FE_DL_EXPORT SemiImplicitInertial:
	virtual public HandlerI,
	virtual public Config,
	public Initialize<SemiImplicitInertial>
{
	public:
				SemiImplicitInertial(void);
virtual			~SemiImplicitInertial(void);
virtual	void	initialize(void);

				// AS HandlerI
virtual	void	handleBind(sp<SignalerI> spSignaler,
					sp<Layout> l_sig);
virtual	void	handle(Record &r_sig);

	private:
		AsParticle				m_asParticle;
		AsSolverParticle		m_asSolverParticle;
		AsLineConstrained		m_asLineConstrained;
		AsPlaneConstrained		m_asPlaneConstrained;
		AsBodyPoint				m_asBodyPoint;
		AsComponent				m_asComponent;
		AsForcePoint			m_asForcePoint;
		AsTemporal				m_asTemporal;
		AsValidate				m_asValidate;
		AsAccumulate			m_asAccumulate;
		AsClear					m_asClear;
		AsUpdate				m_asUpdate;
		AsForceFilter			m_asForceFilter;
		hp<SignalerI>			m_hpSignaler;
		sp<Scope>				m_spScope;
		Record					r_clear;
		Record					r_accum;
		Record					r_validate;
		Record					r_update;

		SystemTicker			m_ticker;

		class Cache
		{
			public:
				Cache(void)
				{
				}
				sp<UpperTriangularVMR>		m_dfdx;
				sp<UpperTriangularVMR>		m_dfdv;
		};

		sp< UpperTriangularBCRS<Real> >		m_lhs;
		sp<UpperTriangularVMR>				m_dfdx;
		sp<UpperTriangularVMR>				m_dfdv;
		BlockDiagonalPreconditioner<Real>	m_preconditioner;
		BlockPCG<Real>					m_solver;
		std::vector<SpatialVector>	m_rhs;
		std::vector<SpatialVector>	m_dv;
		std::vector<SpatialVector>	m_tmp;

		typedef std::map< sp<RecordGroup>, Cache > t_cache;
		t_cache	m_cache;

		Real						m_dv2dxRatio;
		Real						m_dxImplicitness;
		Real						m_dvImplicitness;

		Real						m_ratio;
		unsigned int				m_subdivcnt;
		unsigned int				m_subdivsz;
		Real						m_subdivmult;

	void	clear(sp<RecordGroup> rg_input, Real a_timestep);
	void	accumulate(sp<RecordGroup> rg_input, Real a_timestep);
	void	step(sp<RecordGroup> rg_input, Real a_timestep,
				std::vector<Record> &r_particles,
				SpatialVector &a_totalConstraintForce);
	bool	validate(sp<RecordGroup> rg_input, Real a_timestep);
};

} /* namespace ext */
} /* namespace fe */


#endif /* __solve_SemiImplicitInertial_h__ */

