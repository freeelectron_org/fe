import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "GotoOrigin",
                "PotentialNav",
                "GenerateNav",
                "DistCost",
                "aiDL" ]

    dll = module.DLL( "fexAiDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexSpatialDLLib"
                ]

    forge.deps( ["fexAiDLLib"], deplibs )

