/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

#define FE_EX_MESSAGEBOX	FALSE

namespace fe
{

FE_DL_PUBLIC String Exception::ms_file = "";
FE_DL_PUBLIC int Exception::ms_line = 0;
FE_DL_PUBLIC String Exception::ms_function = "";

#if 0
void Exception::stage(const char *file, int line,const char* function) throw()
{
	ms_file = file;
	ms_line = line;
	ms_function = function;
}
#endif

Exception::Exception(void) throw()
{
	m_file = ms_file;
	m_line = ms_line;
	m_function = ms_function;
	m_location = "unknown";
	m_result = e_undefinedFailure;

	checkAssert();
}

Exception::Exception(const char *fmt, ...) throw()
{
	m_file = ms_file;
	m_line = ms_line;
	m_function = ms_function;
	m_location = "unknown";
	m_result = e_undefinedFailure;

	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		m_message.vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	checkAssert();
}

Exception::Exception(const char *location, const char *fmt, ...) throw()
{
	m_file = ms_file;
	m_line = ms_line;
	m_function = ms_function;
	m_location = location;
	m_result = e_undefinedFailure;

	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		m_message.vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	checkAssert();
}

Exception::Exception(const Result & result, const char *fmt, ...) throw()
{
	m_file = ms_file;
	m_line = ms_line;
	m_function = ms_function;
	m_location = "unknown";
	m_result = result;

	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		m_message.vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	checkAssert();
}

Exception::Exception(const Result & result, const char *location,
		const char *fmt, ...) throw()
{
	m_file = ms_file;
	m_line = ms_line;
	m_function = ms_function;
	m_location = location;
	m_result = result;

	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		m_message.vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	checkAssert();
}

Exception::Exception(const Result & result) throw()
{
	m_file = ms_file;
	m_line = ms_line;
	m_function = ms_function;
	m_result = result;
	m_message = "";
	checkAssert();
}

Exception::Exception(const Exception &other) throw()
{
	*this = other;

	//* should have asserted on the first one,
	//* otherwise causes duplicate messages
//	checkAssert();
}

Exception::~Exception(void) throw()
{
}

Exception &Exception::operator=(const Exception &other) throw()
{
	if(this != &other)
	{
		m_message = other.m_message;
		m_location = other.m_location;
		m_result = other.m_result;
		m_stack = other.m_stack;
		m_file = other.m_file;
		m_line = other.m_line;
		m_function = other.m_function;
	}

	return *this;
}

void Exception::checkAssert(void) throw()
{
//	feLogDirect("Exception::checkAssert\n");

	fe::String value;
	if(!fe::System::getEnvironmentVariable("FE_E_UNCAUGHT_BACKTRACE", value) ||
			atoi(value.c_str()))
	{
		m_stack = feGenerateBacktrace(3);
	}

	if(System::getEnvironmentVariable("FE_E_ASSERT", value) &&
			atoi(value.c_str()))
	{
		feLogDirect("[33mFE ASSERTION:[0m %s\n",m_message.c_str());
		feLogDirect("  in %s\n",m_location.c_str());
		feLogBacktrace(3);
		feAttachDebugger();
		FEASSERT(false);
	}

	const String verbose=System::getVerbose();
	if(verbose!="none")
	{
		feLogDirect("[33mFE EXCEPTION INITIATED:[0m %s\n",
				print(*this).c_str());
	}

	if(System::getEnvironmentVariable("FE_E_BACKTRACE", value) &&
			atoi(value.c_str()))
	{
		feLogBacktrace(3);
	}
}

void Exception::log(void) throw()
{
	if(m_message == "")
	{
		feLogDirect("[31m%p %s [36m%s[0m\n",
			m_result, resultString(m_result), m_location.c_str());
	}
	else
	{
		feLogDirect("[31m%p %s [36m%s[37m %s[0m\n",
				m_result, resultString(m_result),
				m_location.c_str(), m_message.c_str());
	}
	if(m_line != 0)
	{
		feLogDirect("%s:%d %s\n", m_file.c_str(), m_line, m_function.c_str());
	}
#if FE_EX_MESSAGEBOX && (FE_OS==FE_WIN32 || FE_OS==FE_WIN64)
	MessageBox( NULL, (LPCTSTR)m_message.c_str(),
			"Error", MB_OK | MB_ICONINFORMATION );
#endif
}

String print(const Exception& a_rException)
{
	String text;

	const String& message=a_rException.getMessage();
	const String& location=a_rException.getLocation();
	const Result result=a_rException.getResult();
	const I32 line=a_rException.getLine();

	if(result!=e_undefinedFailure)
	{
		text.sPrintf("\"%s\" ",resultString(result));
	}

	if(location!="unknown")
	{
		text.catf("in %s ",location.c_str());
	}

	if(!message.empty())
	{
		text.catf("because \"%s\"",message.c_str());
	}

	if(line != 0)
	{
		const String& file=a_rException.getFile();
		const String& function=a_rException.getFunction();

		text.catf("\n%s:%d %s",file.c_str(),line,function.c_str());
	}

	return text;
}

} /* namespace */

