/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_XYZ_DEBUG		FALSE

namespace fe
{
namespace ext
{

BWORD SurfaceAccessibleXyz::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_XYZ_DEBUG
	feLog("SurfaceAccessibleXyz::load \"%s\"\n",a_filename.c_str());
#endif

	std::ifstream stream;

	stream.open(a_filename.c_str(),std::fstream::in);
	if(!stream.good() && a_filename.c_str()[0]!='/')
	{
		const String path=
				registry()->master()->catalog()->catalog<String>("path:media")+
				"/"+a_filename;
		stream.open(path.c_str(),std::fstream::in);
	}

	if(!stream.good())
	{
		feLog("SurfaceAccessibleXyz::load failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_createMissing;

	sp<SurfaceAccessorI> spOutputPoint=accessor(e_point,e_position,creation);
	sp<SurfaceAccessorI> spOutputLabel;

	const I32 maxLine(256);
	I32 fileVertexCount(-1);
	I32 vertexCount(0);

	char line[maxLine];
	char comment[maxLine];
	char token0[maxLine];
	char token1[maxLine];
	char token2[maxLine];
	char token3[maxLine];
	SpatialVector position;

	const char* numerical="-0123456789.eE";

	BWORD begun(FALSE);

	while(stream.good())
	{
		if(!stream.getline(line,maxLine))
		{
			break;
		}
		I32 scanned=sscanf(line,"%s%s%s%s",token0,token1,token2,token3);
		if(!scanned)
		{
			continue;
		}

		if(!begun)
		{
			begun=TRUE;

			if(scanned!=3 ||
					strspn(token0,numerical)!=strlen(token0) ||
					strspn(token1,numerical)!=strlen(token1) ||
					strspn(token2,numerical)!=strlen(token2))
			{
				spOutputLabel=accessor(e_point,"label",creation);

				if(scanned==1)
				{
					fileVertexCount=atof(token0);

					stream.getline(line,maxLine);
					scanned=sscanf(line,"%s%s%s%s",token0,token1,token2,token3);
				}

				if(scanned!=4 ||
						strspn(token1,numerical)!=strlen(token1) ||
						strspn(token2,numerical)!=strlen(token2) ||
						strspn(token3,numerical)!=strlen(token3))
				{
					strcpy(comment,line);

					stream.getline(line,maxLine);
					scanned=sscanf(line,"%s%s%s%s",token0,token1,token2,token3);
				}
			}
		}

		spOutputPoint->append(1);

		if(spOutputLabel.isValid())
		{
			spOutputLabel->set(vertexCount,token0);

			position[0]=atof(token1);
			position[1]=atof(token2);
			position[2]=atof(token3);
		}
		else
		{
			position[0]=atof(token0);
			position[1]=atof(token1);
			position[2]=atof(token2);
		}

		spOutputPoint->set(vertexCount++,position);
	}

	stream.close();

	feLog("SurfaceAccessibleXyz::load name \"%s\" points %d\n",
			verboseName().c_str(),spOutputPoint->count());

	if(strlen(comment))
	{
		feLog("SurfaceAccessibleXyz::load comment \"%s\"\n",comment);
	}

	if(fileVertexCount>=0 && vertexCount!=fileVertexCount)
	{
		feLog("SurfaceAccessibleXyz::load %d points loaded of %d promised\n",
				vertexCount,fileVertexCount);
	}

#if FALSE
	save("output/test.xyz",sp<Catalog>(NULL));
	SurfaceAccessibleRecord::save("output/test.rg",sp<Catalog>(NULL));
#endif

	return TRUE;
}

BWORD SurfaceAccessibleXyz::save(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_XYZ_DEBUG
	feLog("SurfaceAccessibleXyz::save \"%s\"\n",a_filename.c_str());
#endif

	std::ofstream stream(a_filename.c_str());
	if(!stream.is_open())
	{
		feLog("SurfaceAccessibleXyz::save failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_refuseMissing;

	sp<SurfaceAccessorI> spOutputPoint=accessor(e_point,e_position,creation);
	if(spOutputPoint.isNull())
	{
		feLog("SurfaceAccessibleObj::save no points in surface\n");
		return FALSE;
	}

	sp<SurfaceAccessorI> spOutputLabel=accessor(e_point,"label",creation);

	String line;

	const I32 pointCount=spOutputPoint->count();

	if(spOutputLabel.isValid())
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			line.sPrintf("%s %s\n",
					spOutputLabel->string(pointIndex).c_str(),
					c_print(spOutputPoint->spatialVector(pointIndex)));
			stream<<line.c_str();
		}
	}
	else
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			line.sPrintf("%s\n",
					c_print(spOutputPoint->spatialVector(pointIndex)));
			stream<<line.c_str();
		}
	}

#if FE_XYZ_DEBUG
	feLog("SurfaceAccessibleXyz::save pointCount %d\n",pointCount);
#endif

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
