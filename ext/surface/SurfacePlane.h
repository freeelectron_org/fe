/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfacePlane_h__
#define __surface_SurfacePlane_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Planar Surface

	@ingroup surface

	A renderer may choose to limit the plane by the radius attribute.
	Ray casting should not be limited.
*//***************************************************************************/
class FE_DL_EXPORT SurfacePlane:
	public SurfaceDisk,
	public CastableAs<SurfacePlane>
{
	protected:
	class Impact:
		public SurfaceDisk::Impact,
		public CastableAs<Impact>
	{
		public:
							Impact(void)
							{
#if FE_COUNTED_STORE_TRACKER
								setName("SurfaceDisk::Impact");
#endif
							}
	virtual					~Impact(void)									{}

	virtual	SpatialVector	intersection(void)
							{
								if(!m_resolved)
								{
									resolve();
								}
								return m_intersection;
							}

	virtual	SpatialVector	normal(void)
							{
								if(!m_resolved)
								{
									resolve();
								}
								return m_normal;
							}

		protected:
			SpatialVector	m_axis;
	};
	public:

							SurfacePlane(void);
virtual						~SurfacePlane(void)							{}

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							using SurfaceDisk::nearestPoint;

							//* As SurfaceI
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance) const
							{
								SpatialVector direction;
								Real distance=
										PointPlaneNearest<Real>::solve(
										m_center,m_span,m_radius,
										a_origin,direction);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}
								sp<Impact> spImpact=m_planeImpactPool.get();
								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_center);
								spImpact->setAxis(m_span/m_range);
								spImpact->setRadius(m_radius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(direction);
								spImpact->setDistance(distance);
								return spImpact;
							}

							using SurfaceDisk::rayImpact;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const
							{
								Real distance=
										RayPlaneIntersect<Real>::solve(
										m_center,m_span,m_radius,
										a_origin,a_direction);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=m_planeImpactPool.get();
								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_center);
								spImpact->setAxis(m_span/m_range);
								spImpact->setRadius(m_radius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(a_direction);
								spImpact->setDistance(distance);
								return spImpact;
							}

							using SurfaceDisk::draw;

virtual	void				draw(sp<DrawI> a_spDrawI,const Color* color,
								sp<PartitionI> a_spPartition) const;

	protected:

virtual	void				resolveImpact(sp<ImpactI> a_spImpactI) const
							{
								// TODO m_baseRadius and m_endRadius scaling

								sp<Impact> spImpact(a_spImpactI);

								SpatialVector intersection;
								SpatialVector normal;
								RayPlaneIntersect<Real>::resolveContact(
										spImpact->locationLocal(),
										spImpact->axis(),
										spImpact->radius(),
										spImpact->origin(),
										spImpact->direction(),
										spImpact->distance(),
										intersection,
										normal);
								spImpact->setIntersectionLocal(intersection);
								spImpact->setNormalLocal(normal);
							}

	private:
mutable	CountedPool<Impact>	m_planeImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfacePlane_h__ */

