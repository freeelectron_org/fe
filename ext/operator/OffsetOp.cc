/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

using namespace fe;
using namespace fe::ext;

void OffsetOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");

	catalog< sp<Component> >("Displaced Surface");
}

/*
BWORD triangleDuDv(			Vector<3,Real> &a_du,
							Vector<3,Real> &a_dv,
							const Vector<3,Real> &a_point0,
							const Vector<3,Real> &a_point1,
							const Vector<3,Real> &a_point2,
							const Vector2 &a_uv0,
							const Vector2 &a_uv1,
							const Vector2 &a_uv2);
*/

void OffsetOp::handle(Record& a_rSignal)
{
	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spReferenceAccessible;
	if(!access(spReferenceAccessible,"Reference Surface")) return;

	sp<SurfaceAccessibleI> spDisplacedAccessible;
	if(!access(spDisplacedAccessible,"Displaced Surface")) return;

	sp<SurfaceAccessorI> spOutputPoints;
	if(!access(spOutputPoints,spOutputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,spInputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputNormals;
	if(!access(spInputNormals,spInputAccessible,
			e_point,e_normal)) return;

	sp<SurfaceAccessorI> spInputUVs;
	if(!access(spInputUVs,spInputAccessible,
			e_point,e_uv)) return;

	sp<SurfaceAccessorI> spReferenceVertices;
	if(!access(spReferenceVertices,spReferenceAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spReferencePoints;
	if(!access(spReferencePoints,spReferenceAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spReferenceNormals;
	if(!access(spReferenceNormals,spReferenceAccessible,
			e_point,e_normal)) return;

	sp<SurfaceAccessorI> spDisplacedVertices;
	if(!access(spDisplacedVertices,spDisplacedAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spDisplacedPoints;
	if(!access(spDisplacedPoints,spDisplacedAccessible,
			e_point,e_position)) return;

	const I32 pointCount=spInputPoints->count();
	const I32 primitiveCount=spInputVertices->count();

	if(I32(spReferenceVertices->count()) != primitiveCount)
	{
		catalog<String>("error")+="Reference Surface"
				" does not have the same number of primitives;";
		return;
	}
	if(I32(spReferencePoints->count()) != pointCount)
	{
		catalog<String>("error")+="Reference Surface"
				" does not have the same number of points;";
		return;
	}
	if(I32(spDisplacedVertices->count()) != primitiveCount)
	{
		catalog<String>("error")+="Displaced Surface"
				" does not have the same number of primitives;";
		return;
	}
	if(I32(spDisplacedPoints->count()) != pointCount)
	{
		catalog<String>("error")+="Displaced Surface"
				" does not have the same number of points;";
		return;
	}

	Array<I32> pointPrimitive(pointCount,-1);

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const I32 subCount=spInputVertices->subCount(primitiveIndex);
//		FEASSERT(subCount==3);
		if(subCount>1)
		{
			const SpatialVector p0=
					spInputVertices->spatialVector(primitiveIndex,0);
			const SpatialVector p1=
					spInputVertices->spatialVector(primitiveIndex,1);

			if(magnitudeSquared(p1-p0)<1e-18)
			{
				//* degenerate
				continue;
			}

			if(subCount>2)
			{
				const SpatialVector p2=
						spInputVertices->spatialVector(primitiveIndex,2);

				if(magnitudeSquared(p2-p1)<1e-18 ||
						magnitudeSquared(p0-p2)<1e-18)
				{
					//* degenerate
					continue;
				}
			}
		}

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					spInputVertices->integer(primitiveIndex,subIndex);
			FEASSERT(pointIndex<pointCount);
			pointPrimitive[pointIndex]=primitiveIndex;
		}
	}

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const I32 primitiveIndex=pointPrimitive[pointIndex];
		FEASSERT(primitiveIndex<primitiveCount);

		if(primitiveIndex<0)
		{
			String message;
			message.sPrintf(" degenerate point %d;",pointIndex);
			catalog<String>("warning")+=message;
			continue;
		}

		const I32 index0=
				spInputVertices->integer(primitiveIndex,0);
		const I32 index1=
				spInputVertices->integer(primitiveIndex,1);
		const I32 index2=
				spInputVertices->integer(primitiveIndex,2);

		const SpatialVector uv0=spInputUVs->spatialVector(index0);
		const SpatialVector uv1=spInputUVs->spatialVector(index1);
		const SpatialVector uv2=spInputUVs->spatialVector(index2);

		//* input
		const SpatialVector p0=spInputPoints->spatialVector(index0);
		const SpatialVector p1=spInputPoints->spatialVector(index1);
		const SpatialVector p2=spInputPoints->spatialVector(index2);

		const SpatialVector point=spInputPoints->spatialVector(pointIndex);
		const SpatialVector norm=spInputNormals->spatialVector(pointIndex);

		SpatialVector du;
		SpatialVector dv;
#if FE_CODEGEN<=FE_DEBUG
		BWORD success=
#endif
				triangleDuDv(du,dv,p0,p1,p2,uv0,uv1,uv2);
		FEASSERT(success);

		SpatialVector tangent;
		tangentFromDuDvN(tangent,du,dv,norm);

		SpatialTransform xformInput;
		makeFrameNormalY(xformInput,point,tangent,norm);

		//* reference
		const SpatialVector ref0=spReferencePoints->spatialVector(index0);
		const SpatialVector ref1=spReferencePoints->spatialVector(index1);
		const SpatialVector ref2=spReferencePoints->spatialVector(index2);

		const SpatialVector refPoint=
				spReferencePoints->spatialVector(pointIndex);
		const SpatialVector refNorm=
				spReferenceNormals->spatialVector(pointIndex);

		SpatialVector refDu;
		SpatialVector refDv;
#if FE_CODEGEN<=FE_DEBUG
		success=
#endif
			triangleDuDv(refDu,refDv,ref0,ref1,ref2,uv0,uv1,uv2);

		SpatialVector refTangent;
		tangentFromDuDvN(refTangent,refDu,refDv,refNorm);

		SpatialTransform xformRef;
		makeFrameNormalY(xformRef,refPoint,refTangent,refNorm);

		//* displaced
		const SpatialVector displacedPoint=
				spDisplacedPoints->spatialVector(pointIndex);

		const SpatialVector localDisplacement=
				inverseTransformVector(xformRef,displacedPoint);

		const SpatialVector newPoint=
				transformVector(xformInput,localDisplacement);

		if(magnitude(du)<1e-6 && magnitude(dv)<1e-6)
		{
			feLog("%d/%d prim %d ref %s disp %s local %s input %s output %s\n",
					pointIndex,pointCount,primitiveIndex,
					c_print(refPoint),
					c_print(displacedPoint),
					c_print(localDisplacement),
					c_print(point),
					c_print(newPoint));
			feLog("  index0 %s index1 %s index2 %s\n",
					c_print(index0),
					c_print(index1),
					c_print(index2));
			feLog("  p0 %s p1 %s p2 %s\n",
					c_print(p0),
					c_print(p1),
					c_print(p2));
			feLog("  uv0 %s uv1 %s uv2 %s\n",
					c_print(uv0),
					c_print(uv1),
					c_print(uv2));
			feLog("  norm %s du %s dv %s tangent %s\n",
					c_print(norm),
					c_print(du),
					c_print(dv),
					c_print(tangent));
			feLog("  xformRef\n%s\n",c_print(xformRef));
			feLog("  xformInput\n%s\n",c_print(xformInput));
		}

		spOutputPoints->set(pointIndex,newPoint);
	}
}
