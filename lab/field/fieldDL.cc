/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "platform/dlCore.cc"
#include "DrawScalarField.h"
#include "SourceSink.h"
#include "DensitySink.h"
#include "DensitySource.h"
#include "SimpleDiffusion.h"

#include <field/field.pmh>

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<DrawScalarField>("HandlerI.DrawScalarField.field.fe");
	pLibrary->add<DensityField>("ScalarFieldI.Density.field.fe");
	pLibrary->add<BasicSourceSink>("HandlerI.BasicSourceSink.field.fe");
	pLibrary->add<DensitySink>("HandlerI.DensitySink.field.fe");
	pLibrary->add<DensitySource>("HandlerI.DensitySource.field.fe");
	pLibrary->add<SimpleDiffusion>("HandlerI.SimpleDiffusion.field.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
