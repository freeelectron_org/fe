/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_JSO_DEBUG			FALSE
#define FE_JSO_DEBUG_JOINTS		FALSE
#define FE_JSO_AUTO_DEACTIVATE	FALSE	//* WARNING self-triggering

using namespace fe;
using namespace fe::ext;

//* static
void JointSeparateOp::changeCB(MNodeMessage::AttributeMessage a_message,
	MPlug & a_plug,MPlug & a_otherPlug,void* a_pClientData)
{
	if(a_pClientData && a_message&MNodeMessage::kAttributeSet)
	{
		JointSeparateOp* pOperator=(JointSeparateOp*)a_pClientData;
		if(!pOperator->m_deactivate)
		{
			feLog("JointSeparateOp::changeCB deactivate %p\n",pOperator);

			const BWORD aggressive=FALSE;
			pOperator->dirty(aggressive);
			pOperator->m_deactivate=TRUE;
		}
	}
}

//* static
void JointSeparateOp::renameCB(MObject &a_node, const MString &a_string,
	void* a_pClientData)
{
	if(a_pClientData)
	{
		JointSeparateOp* pOperator=(JointSeparateOp*)a_pClientData;
		if(!pOperator->m_dirtied)
		{
			const BWORD aggressive=FALSE;
			pOperator->dirty(aggressive);
			pOperator->m_dirtied=TRUE;
		}
	}
}

JointSeparateOp::~JointSeparateOp(void)
{
	cancelCallbacks();
}

void JointSeparateOp::cancelCallbacks(void)
{
	const U32 callbackCount=m_callbackIDs.size();
	for(U32 callbackIndex=0;callbackIndex<callbackCount;callbackIndex++)
	{
		MMessage::removeCallback(m_callbackIDs[callbackIndex]);
	}
	m_callbackIDs.clear();
}

void JointSeparateOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("active")=true;
	catalog<String>("active","label")="Update Joints";

#if FE_JSO_AUTO_DEACTIVATE
	catalog<String>("active","IO")="input output";
#endif

	catalog<bool>("discardUnknown")=true;
	catalog<String>("discardUnknown","label")="Discard Unknown Joints";

	catalog<bool>("useRef")=false;
	catalog<String>("useRef","label")="Use Reference";

	catalog<String>("root")="joints";
	catalog<String>("root","label")="Root Node Name";
	catalog<String>("root","IO")="input output";

	catalog<String>("selectJoint")="";
	catalog<String>("selectJoint","label")="Select Node Name";

	catalog< sp<Component> >("Input Surface");

	//* hook up root joint geometry attribute
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
//	catalog<String>("Output Surface","implementation")="curve";
	catalog<bool>("Output Surface","array")=true;

	catalog<bool>("Output Trigger");
	catalog<bool>("Output Trigger","hidden")=true;
	catalog<String>("Output Trigger","IO")="output";
}

void JointSeparateOp::handle(Record& a_rSignal)
{
#if FE_JSO_DEBUG
	feLog("JointSeparateOp::handle node \"%s\"\n",
				name().c_str());
#endif

	m_dirtied=FALSE;

	MObject rootObject;

	sp<SurfaceAccessibleI> spRootAccessible;
	if(accessOutput(spRootAccessible,a_rSignal))
	{
		sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=spRootAccessible;
		rootObject=spSurfaceAccessibleMaya->meshNode();

		MFnDagNode dagNode(rootObject);
		const String dagName=dagNode.fullPathName().asChar();

//		feLog("JointSeparateOp::handle bound %d root \"%s\"\n",
//				spRootAccessible->isBound(),dagName.c_str());

		if(!dagName.empty())
		{
			catalog<String>("root")=dagName;
		}
	}

	if(m_deactivate)
	{
		catalog<bool>("active")=false;
		m_deactivate=FALSE;
	}

	if(!catalog<bool>("active"))
	{
		return;
	}

	const String rootName=catalog<String>("root");
	const String selectJoint=catalog<String>("selectJoint");
	const BWORD useRef=catalog<bool>("useRef");

#if FE_JSO_DEBUG
	feLog("JointSeparateOp::handle frame %.6G\n",currentFrame(a_rSignal));
#endif

	//* TODO find real node name
	const String selectName=fullMayaName(rootName,selectJoint);

	if(!selectName.empty() && selectName!=m_selectedJoint)
	{
		MGlobal::selectByName(selectName.c_str(),MGlobal::kReplaceList);
	}
	m_selectedJoint=selectName;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputName;
	if(!access(spInputName,spInputAccessible,e_primitive,"name")) return;

	sp<SurfaceAccessorI> spInputParent;
	if(!access(spInputParent,spInputAccessible,e_primitive,"parent")) return;

	sp<SurfaceAccessorI> spInputAnimX;
	if(!access(spInputAnimX,spInputAccessible,e_primitive,"animX")) return;

	sp<SurfaceAccessorI> spInputAnimY;
	if(!access(spInputAnimY,spInputAccessible,e_primitive,"animY")) return;

	sp<SurfaceAccessorI> spInputAnimZ;
	if(!access(spInputAnimZ,spInputAccessible,e_primitive,"animZ")) return;

	sp<SurfaceAccessorI> spInputAnimT;
	if(!access(spInputAnimT,spInputAccessible,e_primitive,"animT")) return;

	sp<SurfaceAccessorI> spInputAnimS;
	if(!access(spInputAnimS,spInputAccessible,e_primitive,"animS")) return;

	sp<SurfaceAccessorI> spInputRefX;
	if(!access(spInputRefX,spInputAccessible,e_primitive,"refX")) return;

	sp<SurfaceAccessorI> spInputRefY;
	if(!access(spInputRefY,spInputAccessible,e_primitive,"refY")) return;

	sp<SurfaceAccessorI> spInputRefZ;
	if(!access(spInputRefZ,spInputAccessible,e_primitive,"refZ")) return;

	sp<SurfaceAccessorI> spInputRefT;
	if(!access(spInputRefT,spInputAccessible,e_primitive,"refT")) return;

	const U32 primitiveCount=spInputName->count();
#if FE_JSO_DEBUG
	feLog("JointSeparateOp::handle primitiveCount %d\n",primitiveCount);
#endif

	if(!primitiveCount)
	{
		feLog("JointSeparateOp::handle node \"%s\" has empty input\n",
				name().c_str());

		catalog<String>("error")+="no input primitives";
		return;
	}

	MStatus status;

	String sourceName;
	Array<String>* pJointNameArray=NULL;

	//* snoop on the input node
	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=spInputAccessible;
	if(spSurfaceAccessibleMaya.isValid())
	{
		MObject meshNode=spSurfaceAccessibleMaya->meshNode();
		MFnDependencyNode dependencyNode(meshNode,&status);
		if(status)
		{
			MPxNode *pNode=dependencyNode.userNode(&status);
			if(status)
			{
				MayaNode* pMayaNode=dynamic_cast<MayaNode*>(pNode);
				if(pMayaNode)
				{
					sp<OperatorSurfaceI> spOperator=
							pMayaNode->operatorSurface();
					sp<Catalog> spCatalog=spOperator;
					if(spCatalog.isValid() &&
							spCatalog->cataloged("jointName") &&
							spCatalog->cataloged("jointShift") &&
							spCatalog->cataloged("jointStretch") &&
							spCatalog->cataloged("jointBend") &&
							spCatalog->cataloged("jointResize") &&
							spCatalog->cataloged("jointForward") &&
							spCatalog->cataloged("jointBackward"))
					{
						sourceName=dependencyNode.name().asChar();
						pJointNameArray=
								&spCatalog->catalog< Array<String> >(
								"jointName");
					}
				}
			}
		}
	}

	MSelectionList originalList;
	MGlobal::getActiveSelectionList(originalList);

	if(rootObject.isNull())
	{
#if FE_JSO_DEBUG
	feLog("JointSeparateOp::handle null root\n");
#endif

		//* TODO is there an easier way to get a node object by name?
		MGlobal::selectByName(rootName.c_str(),MGlobal::kReplaceList);

		MSelectionList selectionList;
		MGlobal::getActiveSelectionList(selectionList);

		if(selectionList.length())
		{
			selectionList.getDependNode(0,rootObject);
		}
		if(rootObject.isNull())
		{
			MFnTransform fnTransform;
			rootObject=fnTransform.create(MObject::kNullObj,&status);
			if(!status)
			{
				feLog("JointSeparateOp::handle create root failed: %s\n",
						status.errorString().asChar());

				MGlobal::setActiveSelectionList(originalList);
				return;
			}
			fnTransform.setName(rootName.c_str());

			//* TODO reconnect SeparateOp Output_Surface[0] to root geometry
			//* TODO reconnect SeparateOp Output_Trigger to root trigger
		}

		//* also provokes UI refresh
		MGlobal::setActiveSelectionList(originalList);
	}

	MFnDagNode dagRoot(rootObject);

#if FE_JSO_DEBUG
	feLog("JointSeparateOp::handle root \"%s\" dag \"%s\" \"%s\" \"%s\"\n",
			rootName.c_str(),
			dagRoot.name().asChar(),
			dagRoot.partialPathName().asChar(),
			dagRoot.fullPathName().asChar());
#endif

	cancelCallbacks();

	std::map<String,MObject> objectMap;

	m_keepList.clear();
	m_salvageList.clear();

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
#if FE_JSO_DEBUG_JOINTS
		feLog("JointSeparateOp::handle %d/%d\n",primitiveIndex,primitiveCount);
#endif

		const String jointName=spInputName->string(primitiveIndex);

		const String parentName=spInputParent->string(primitiveIndex);
		MObject parentObject=objectMap[parentName];
		if(parentObject==MObject::kNullObj)
		{
			parentObject=rootObject;
		}

		//* watch for any manual change on each output joint
		void* pClientData=this;
		MCallbackId callbackID=MNodeMessage::addNameChangedCallback(
				parentObject,renameCB,pClientData,&status);
		m_callbackIDs.push_back(callbackID);
#if FE_JSO_AUTO_DEACTIVATE
		callbackID=MNodeMessage::addAttributeChangedCallback(
				parentObject,changeCB,pClientData,&status);
		m_callbackIDs.push_back(callbackID);
#endif

		SpatialTransform xformLocalRef;
		xformLocalRef.column(0)=spInputRefX->spatialVector(primitiveIndex);
		xformLocalRef.column(1)=spInputRefY->spatialVector(primitiveIndex);
		xformLocalRef.column(2)=spInputRefZ->spatialVector(primitiveIndex);
		xformLocalRef.column(3)=spInputRefT->spatialVector(primitiveIndex);

		SpatialTransform xformLocal;
		SpatialVector scaleLocal(1.0,1.0,1.0);
		if(useRef)
		{
			xformLocal=xformLocalRef;
		}
		else
		{
			xformLocal.column(0)=spInputAnimX->spatialVector(primitiveIndex);
			xformLocal.column(1)=spInputAnimY->spatialVector(primitiveIndex);
			xformLocal.column(2)=spInputAnimZ->spatialVector(primitiveIndex);
			xformLocal.column(3)=spInputAnimT->spatialVector(primitiveIndex);
			scaleLocal=spInputAnimS->spatialVector(primitiveIndex);
		}

		SpatialTransform invLocalRef;
		invert(invLocalRef,xformLocalRef);

		const SpatialVector localDisplace=xformLocal.translation();

		const SpatialQuaternion quatRotation=xformLocal*invLocalRef;
		const SpatialQuaternion quatOrientation=xformLocalRef;

		const MVector mDisplace(
				localDisplace[0],localDisplace[1],localDisplace[2]);
		const MQuaternion mRotation(
				quatRotation[0],quatRotation[1],
				quatRotation[2],quatRotation[3]);
		const MQuaternion mOrientation(
				quatOrientation[0],quatOrientation[1],
				quatOrientation[2],quatOrientation[3]);

#if FE_JSO_DEBUG_JOINTS
		feLog("  \"%s\" parent \"%s\"\n",jointName.c_str(),parentName.c_str());
		feLog("  point %s localDisplace %s\n",
				c_print(xformLocal.translation()),c_print(localDisplace));
		feLog("  xformLocalRef\n%s\n",c_print(xformLocalRef));
		feLog("  xformLocal\n%s\n",c_print(xformLocal));
		feLog("  quatRotation %s\n",c_print(quatRotation));
		feLog("  quatOrientation %s\n",c_print(quatOrientation));
#endif

		MObject jointObject;
		String jointNode;

		MFnDagNode fnDagNode(parentObject);
		const U32 childCount=fnDagNode.childCount(&status);
		for(U32 childIndex=0;childIndex<childCount;childIndex++)
		{
			//* exact naming
			MObject child=fnDagNode.child(childIndex,&status);
			MFnDagNode dagChild(child);
			String dagName(dagChild.name().asChar());
			if(dagName==jointName)
			{
				jointObject=child;
				jointNode=dagName;
				break;
			}
		}
		if(jointObject.isNull())
		{
			//* examine joint attribute
			for(U32 childIndex=0;childIndex<childCount;childIndex++)
			{
				MObject child=fnDagNode.child(childIndex,&status);
				MFnDagNode dagChild(child);
				String dagName(dagChild.name().asChar());

				const bool wantNetworkedPlug=true;
				MPlug plug=dagChild.findPlug("joint",wantNetworkedPlug,&status);
				const String plugValue=plug.asString().asChar();
				if(plugValue==jointName)
				{
#if FE_JSO_DEBUG_JOINTS
					feLog("attribute match of \"%s\" on \"%s\"\n",
							jointName.c_str(),dagName.c_str());
#endif
					jointObject=child;
					jointNode=dagName;
					break;
				}
			}
		}
		if(jointObject.isNull())
		{
			//* relaxed naming
			for(U32 childIndex=0;childIndex<childCount;childIndex++)
			{
				MObject child=fnDagNode.child(childIndex,&status);
				MFnDagNode dagChild(child);
				String dagName(dagChild.name().asChar());
				if(dagName.match(jointName+"[0-9]*"))
				{
#if FE_JSO_DEBUG_JOINTS
					feLog("relaxed match of \"%s\" with \"%s\"\n",
							jointName.c_str(),dagName.c_str());
#endif
					jointObject=child;
					jointNode=dagName;
					break;
				}
			}
		}

		MFnIkJoint fnJoint(jointObject);

		if(jointObject.isNull())
		{
			jointObject=fnJoint.create(parentObject,&status);
			if(!status)
			{
				feLog("JointSeparateOp::handle create failed: %s\n",
						status.errorString().asChar());
				continue;
			}

			fnJoint.setName(jointName.c_str());

			//* may not get the name we asked for
			jointNode=fnJoint.name().asChar();
		}

		FEASSERT(!jointObject.isNull());

		MFnDagNode dagJoint(jointObject);
		const bool wantNetworkedPlug=true;

		MFnTypedAttribute typedAttr;
		MPlug plug;

		const char* triggerAttrName="trigger";

		const char* jointAttrName="jointName";
		const char* jointAttrLabel="Joint Name";

		const char* scaleAttrName="localScale";
		const char* scaleAttrLabel="Local Scale";

		const char* shiftAttrName="shiftFactor";
		const char* shiftAttrLabel="Shift Factor";
		const char* shiftAttrTarget="jointShift";

		const char* stretchAttrName="stretchFactor";
		const char* stretchAttrLabel="Stretch Factor";
		const char* stretchAttrTarget="jointStretch";

		const char* bendAttrName="bendFactor";
		const char* bendAttrLabel="Bend Factor";
		const char* bendAttrTarget="jointBend";

		const char* resizeAttrName="resizeFactor";
		const char* resizeAttrLabel="Resize Factor";
		const char* resizeAttrTarget="jointResize";

		const char* rescaleAttrName="localRescale";
		const char* rescaleAttrLabel="Local Rescale";
		const char* rescaleAttrTarget="jointRescale";

		const char* forwardAttrName="forwardLimit";
		const char* forwardAttrLabel="Forward Limit";
		const char* forwardAttrTarget="jointForward";

		const char* backwardAttrName="backwardLimit";
		const char* backwardAttrLabel="Backward Limit";
		const char* backwardAttrTarget="jointBackward";

		//* assure jointName exists
		if(!fnJoint.hasAttribute(jointAttrName))
		{
			MFnStringData fnStringData;
			MObject defaultString=fnStringData.create(jointName.c_str());
			MObject jointObj=typedAttr.create(jointAttrName,jointAttrName,
					MFnData::kString,defaultString);
			typedAttr.setNiceNameOverride(jointAttrLabel);
			typedAttr.setStorable(true);
			fnJoint.addAttribute(jointObj);

			plug=dagJoint.findPlug(jointAttrName,
					wantNetworkedPlug,&status);
			if(!plug.isNull())
			{
				plug.setLocked(true);
			}
		}

		plug=dagJoint.findPlug(jointAttrName,
				wantNetworkedPlug,&status);
		if(!plug.isNull() && plug.asString().asChar()!=jointName)
		{
			plug.setLocked(false);
			plug.setString(jointName.c_str());
			plug.setLocked(true);
		}

		//* assure localScale exists
		MFnNumericAttribute numAttr;

		if(!fnJoint.hasAttribute(scaleAttrName))
		{
			MObject scaleObj=numAttr.create(scaleAttrName,scaleAttrName,
					MFnNumericData::k3Double);
			numAttr.setNiceNameOverride(scaleAttrLabel);
			numAttr.setDefault(1.0,1.0,1.0);
			numAttr.setStorable(true);
			fnJoint.addAttribute(scaleObj);

			plug=dagJoint.findPlug(scaleAttrName,
					wantNetworkedPlug,&status);
			if(!plug.isNull())
			{
				plug.setLocked(true);
			}
		}

		plug=dagJoint.findPlug(scaleAttrName,
				wantNetworkedPlug,&status);
		if(!plug.isNull())
		{
			plug.setLocked(false);
			plug.child(0).setDouble(scaleLocal[0]);
			plug.child(1).setDouble(scaleLocal[1]);
			plug.child(2).setDouble(scaleLocal[2]);
			plug.setLocked(true);
		}

		//* assure destination trigger exists
		if(!dagRoot.hasAttribute(triggerAttrName))
		{
			MObject triggerObj=numAttr.create(
					triggerAttrName,triggerAttrName,
					MFnNumericData::kBoolean,false);
			numAttr.setStorable(true);
			numAttr.setHidden(true);
			dagRoot.addAttribute(triggerObj);
		}

		//* assure source trigger exists
		if(!fnJoint.hasAttribute(triggerAttrName))
		{
			MObject triggerObj=numAttr.create(
					triggerAttrName,triggerAttrName,
					MFnNumericData::kBoolean,false);
			numAttr.setStorable(true);
			numAttr.setHidden(true);
			fnJoint.addAttribute(triggerObj);
		}

		//* full joint path
		const String fullName=fullMayaName(rootName,jointNode);

		MString response;
		String command;

		//* wire root joint trigger to each joint trigger
		command.sPrintf("fe_connectAttr(\"%s.trigger\","
				"\"%s.trigger\")",
				rootName.c_str(),fullName.c_str());

		MGlobal::executeCommand(command.c_str(),response);

		if(pJointNameArray)
		{
			MFnNumericAttribute numAttr;

			if(!fnJoint.hasAttribute(shiftAttrName))
			{
				MObject shiftObj=numAttr.create(
						shiftAttrName,shiftAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(shiftAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(1.0,1.0,1.0);
				fnJoint.addAttribute(shiftObj);
			}

			if(!fnJoint.hasAttribute(stretchAttrName))
			{
				MObject stretchObj=numAttr.create(
						stretchAttrName,stretchAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(stretchAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(1.0,1.0,1.0);
				fnJoint.addAttribute(stretchObj);
			}

			if(!fnJoint.hasAttribute(bendAttrName))
			{
				MObject bendObj=numAttr.create(
						bendAttrName,bendAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(bendAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(1.0,1.0,1.0);
				fnJoint.addAttribute(bendObj);
			}

			if(!fnJoint.hasAttribute(resizeAttrName))
			{
				MObject resizeObj=numAttr.create(
						resizeAttrName,resizeAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(resizeAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(1.0,1.0,1.0);
				fnJoint.addAttribute(resizeObj);
			}

			if(!fnJoint.hasAttribute(rescaleAttrName))
			{
				MObject rescaleObj=numAttr.create(
						rescaleAttrName,rescaleAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(rescaleAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(1.0,1.0,1.0);
				fnJoint.addAttribute(rescaleObj);
			}

			if(!fnJoint.hasAttribute(forwardAttrName))
			{
				MObject forwardObj=numAttr.create(
						forwardAttrName,forwardAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(forwardAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(180.0,180.0,180.0);
				fnJoint.addAttribute(forwardObj);
			}

			if(!fnJoint.hasAttribute(backwardAttrName))
			{
				MObject backObj=numAttr.create(
						backwardAttrName,backwardAttrName,
						MFnNumericData::k3Double);
				numAttr.setNiceNameOverride(backwardAttrLabel);
				numAttr.setKeyable(true);
				numAttr.setDefault(180.0,180.0,180.0);
				fnJoint.addAttribute(backObj);
			}

			Array<String>& rJointNameArray=*pJointNameArray;

			const U32 nameCount=rJointNameArray.size();
			for(U32 nameIndex=0;nameIndex<nameCount;nameIndex++)
			{
				if(rJointNameArray[nameIndex]==jointName)
				{
					if(fullName.empty())
					{
						feLog("JointSeparateOp::handle"
								" could not find \"%s\" under \"%s\"\n",
								jointNode.c_str(),rootName.c_str());
						continue;
					}

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),shiftAttrName,
							sourceName.c_str(),shiftAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),stretchAttrName,
							sourceName.c_str(),stretchAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),bendAttrName,
							sourceName.c_str(),bendAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),resizeAttrName,
							sourceName.c_str(),resizeAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),rescaleAttrName,
							sourceName.c_str(),rescaleAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),forwardAttrName,
							sourceName.c_str(),forwardAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);

					command.sPrintf("fe_connectAttr(\"%s.%s\","
							"\"%s.%s[%d]\")",
							fullName.c_str(),backwardAttrName,
							sourceName.c_str(),backwardAttrTarget,
							nameIndex+1);

					MGlobal::executeCommand(command.c_str(),response);
				}
			}
		}

		objectMap[jointName]=jointObject;
		m_keepList.push_back(jointObject);

		fnJoint.setTranslation(mDisplace,MSpace::kTransform);
		fnJoint.setRotation(mRotation,MSpace::kTransform);
		fnJoint.setOrientation(mOrientation);
//		fnJoint.setRotationOrder(MTransformationMatrix::kXYZ,true);

#if FALSE
		MEulerRotation eulerRotation;
		fnJoint.getRotation(eulerRotation);
		feLog("  maya euler -> %.6G %.6G %.6G\n",
				eulerRotation[0],eulerRotation[1],eulerRotation[2]);

		const SpatialEuler feEuler=SpatialTransform(quatRotation);
		feLog("  fe euler -> %s\n",c_print(feEuler));

		Real radians;
		SpatialVector axis;
		quatRotation.computeAngleAxis(radians,axis);
		feLog("  angle axis-> %.6G about %s\n",
				radians/fe::degToRad,c_print(axis));
#endif
	}

	//* delete unused pre-existing joints
	if(catalog<bool>("discardUnknown"))
	{
		scanDag(rootObject);
	}

	const U32 salvageCount=m_salvageList.size();
	if(salvageCount)
	{
		catalog<String>("warning")+=
				"found unknown joints with influence locks"
				" (check your skin weights);";

		feLog("JointSeparateOp::handle not discarding"
				" joint trees with influence locks:\n");
		for(U32 salvageIndex=0;salvageIndex<salvageCount;salvageIndex++)
		{
			feLog("  \"%s\"\n",m_salvageList[salvageIndex].c_str());
		}
	}

	m_keepList.clear();
	m_salvageList.clear();

#if FE_JSO_DEBUG
	feLog("JointSeparateOp::handle done\n");
#endif
}

BWORD JointSeparateOp::scanDag(MObject a_parent)
{
	if(a_parent.isNull())
	{
		return FALSE;
	}

	BWORD anyBound=FALSE;

	MFnDagNode dagParent(a_parent);

	Array<MObject> deleteList;

	MStatus status;
	const U32 childCount=dagParent.childCount(&status);
	for(U32 childIndex=0;childIndex<childCount;childIndex++)
	{
		MObject child=dagParent.child(childIndex,&status);

		BWORD boundJoint=FALSE;
		MFnDagNode dagNode(child);
		const bool wantNetworkedPlug=true;
		MPlug plug=dagNode.findPlug("lockInfluenceWeights",
					wantNetworkedPlug,&status);
		if(!plug.isNull())
		{
			boundJoint=plug.isConnected();

			if(boundJoint)
			{
				anyBound=TRUE;
			}
		}

		const U32 keepCount=m_keepList.size();
		U32 keepIndex=0;
		for(;keepIndex<keepCount;keepIndex++)
		{
			if(m_keepList[keepIndex]==child)
			{
				if(scanDag(child))
				{
					anyBound=TRUE;
				}
				break;
			}
		}
		if(keepIndex==keepCount)
		{
			if(anyBound)
			{
				m_salvageList.push_back(dagNode.fullPathName().asChar());
			}
			else
			{
				deleteList.push_back(child);
			}
		}
	}

	const U32 deleteCount=deleteList.size();
	for(U32 deleteIndex=0;deleteIndex<deleteCount;deleteIndex++)
	{
		MObject child=deleteList[deleteIndex];

		MGlobal::deleteNode(child);
	}

	return anyBound;
}

fe::String JointSeparateOp::fullMayaName(String a_rootPath,String a_leafName)
{
	String command;

	command.sPrintf("fe_listNodes(\"%s\")",a_rootPath.c_str());

//	MString response;
//	MGlobal::executeCommand(command.c_str(),response,true);
	MString response=MGlobal::executeCommandStringResult(command.c_str());

	String tokens=response.asChar();

	String fullName;

	String token;
	while(!(token=tokens.parse()).empty())
	{
		if(token.match(".*[|]"+a_leafName+"$"))
		{
			fullName=token;
			break;
		}
	}

	return fullName;
}

