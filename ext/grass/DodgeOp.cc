/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

#define FE_DODGE_DEBUG			FALSE
#define FE_DODGE_IMPACT_DEBUG	FALSE

#define	FE_DODGE_PROFILE		(FE_CODEGEN==FE_PROFILE)
#define	FE_DODGE_THRESHOLD		1e-3
#define	FE_DODGE_TINY			1e-4

using namespace fe;
using namespace fe::ext;

void DodgeOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	catalog<String>("Threads","page")="Config";

	catalog<String>("AutoThread","page")="Config";

	catalog<String>("Paging","page")="Config";

	catalog<String>("StayAlive","page")="Config";

	catalog<String>("Work","page")="Config";

	catalog<String>("Group");
	catalog<String>("Group","label")="Primitive Group";
	catalog<String>("Group","page")="Config";
	catalog<String>("Group","suggest")="primitiveGroups";

	catalog<bool>("PartitionCollider")=false;
	catalog<String>("PartitionCollider","label")="Partition Collider";
	catalog<String>("PartitionCollider","page")="Config";
	catalog<bool>("PartitionCollider","joined")=true;
	catalog<String>("PartitionCollider","hint")=
			"Control responses based on sections of the collider.";

	catalog<String>("ColliderPartitionAttr")="part";
	catalog<String>("ColliderPartitionAttr","label")="Collider Attr";
	catalog<String>("ColliderPartitionAttr","page")="Config";
	catalog<String>("ColliderPartitionAttr","enabler")="PartitionCollider";
	catalog<String>("ColliderPartitionAttr","hint")=
			"Primitive string attribute on collider to use for sectioning.";

	catalog<String>("RepulsionPartition")=".*";
	catalog<String>("RepulsionPartition","label")="Repulsion Match";
	catalog<String>("RepulsionPartition","page")="Config";
	catalog<String>("RepulsionPartition","enabler")="PartitionCollider";
	catalog<bool>("RepulsionPartition","joined")=true;
	catalog<String>("RepulsionPartition","hint")=
			"Regex pattern used to choose what sections of the collider"
			" provoke repulsion."
			"  A non-empty repulsion attribute on the input"
			" will override this common value.";

	catalog<String>("CollisionPartition")=".*";
	catalog<String>("CollisionPartition","label")="Collision Match";
	catalog<String>("CollisionPartition","page")="Config";
	catalog<String>("CollisionPartition","enabler")="PartitionCollider";
	catalog<String>("CollisionPartition","hint")=
			"Regex pattern used to choose what sections of the collider"
			" provoke contact collision."
			"  A non-empty repulsion attribute on the input"
			" will override this common value.";

	catalog<String>("inputRepulsionAttr")="repulsor";
	catalog<String>("inputRepulsionAttr","label")="Repulsion Attr";
	catalog<String>("inputRepulsionAttr","page")="Config";
	catalog<String>("inputRepulsionAttr","enabler")="PartitionCollider";
	catalog<bool>("inputRepulsionAttr","joined")=true;
	catalog<String>("inputRepulsionAttr","hint")=
			"Primitive string attribute on input providing per-curve"
			" regex pattern choosing what sections of the collider"
			" provoke repulsion."
			"  If the string is empty,"
			" the common repulsion pattern will be used.";

	catalog<String>("inputCollisionAttr")="collider";
	catalog<String>("inputCollisionAttr","label")="Collision Attr";
	catalog<String>("inputCollisionAttr","page")="Config";
	catalog<String>("inputCollisionAttr","enabler")="PartitionCollider";
	catalog<String>("inputCollisionAttr","hint")=
			"Primitive string attribute on input providing per-curve"
			" regex pattern choosing what sections of the collider"
			" provoke collision."
			"  If the string is empty,"
			" the common collision pattern will be used.";

	catalog<I32>("Refinement")=0;
	catalog<I32>("Refinement","high")=4;
	catalog<I32>("Refinement","max")=8;
	catalog<String>("Refinement","page")="Config";
	catalog<String>("Refinement","hint")=
			"Maximum level of collider subdivision."
			"  Refinement may be adaptive and non-uniform.";

	//* TODO map, fixed uv, or other options
	catalog<String>("directionMethod")="normal";
	catalog<String>("directionMethod","label")="Direction";
	catalog<String>("directionMethod","choice:0")="normal";
	catalog<String>("directionMethod","label:0")="Nearest Normal";
	catalog<String>("directionMethod","choice:1")="tangent";
	catalog<String>("directionMethod","label:1")="Tangent Attribute";
	catalog<String>("directionMethod","page")="Config";
	catalog<bool>("directionMethod","joined")=true;
	catalog<String>("directionMethod","hint")=
			"Method used to determine the direction of correction.";

	catalog<String>("TangentAttr")="tangent";
	catalog<String>("TangentAttr","label")="Tangent Attr";
	catalog<String>("TangentAttr","page")="Config";
	catalog<String>("TangentAttr","hint")=
			"Name of vector primitive attribute on input curves"
			" describing the direction along which rotations can occur.";

	catalog<I32>("DeadZone")=1;
	catalog<String>("DeadZone","label")="Dead Zone";
	catalog<I32>("DeadZone","min")=1;
	catalog<I32>("DeadZone","high")=8;
	catalog<String>("DeadZone","page")="Config";
	catalog<bool>("DeadZone","joined")=true;
	catalog<String>("DeadZone","hint")=
		"Number on CV's on each curve to ignore,"
		" starting with the root."
		"  If the Dead Zone Attr is a valid attribute and"
		" is more than zero for a particular curve,"
		" that value is used instead.";

	catalog<String>("DeadZoneAttr")="deadzone";
	catalog<String>("DeadZoneAttr","label")="Dead Zone Attr";
	catalog<String>("DeadZoneAttr","page")="Config";
	catalog<String>("DeadZoneAttr","hint")=
		"Primitive integer attribute specifying the dead zone per curve.";

	catalog<I32>("Seed")=0;
	catalog<I32>("Seed","max")=RAND_MAX;
	catalog<String>("Seed","page")="Config";
	catalog<String>("Seed","hint")="Set of random results.";

	catalog<bool>("DebugCollider")=false;
	catalog<String>("DebugCollider","label")="Debug Collider";
	catalog<String>("DebugCollider","page")="Config";
	catalog<bool>("DebugCollider","joined")=true;
	catalog<String>("DebugCollider","hint")=
			"Draws geometry over the output showing the collider."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<bool>("DebugCollision")=false;
	catalog<String>("DebugCollision","label")="Debug Collision";
	catalog<String>("DebugCollision","page")="Config";
	catalog<bool>("DebugCollision","joined")=true;
	catalog<String>("DebugCollision","hint")=
			"Draws lines over the output showing the collision response."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<bool>("DebugRepulsion")=false;
	catalog<String>("DebugRepulsion","label")="Debug Repulsion";
	catalog<String>("DebugRepulsion","page")="Config";
	catalog<bool>("DebugRepulsion","joined")=true;
	catalog<String>("DebugRepulsion","hint")=
			"Draws lines over the output showing the repulsion effect."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<bool>("DebugRay")=false;
	catalog<String>("DebugRay","label")="Debug Ray";
	catalog<String>("DebugRay","page")="Config";
	catalog<String>("DebugRay","hint")=
			"Draws geometry over the output showing the ray casting."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<I32>("DebugSegment")= -1;
	catalog<I32>("DebugSegment","min")= -1;
	catalog<I32>("DebugSegment","high")=10;
	catalog<String>("DebugSegment","label")="Debug Segment";
	catalog<String>("DebugSegment","page")="Config";
	catalog<String>("DebugSegment","hint")=
			"Only draw debug for a particular segment.";

	catalog<bool>("Temporal")=false;
	catalog<String>("Temporal","page")="Behavior";
	catalog<String>("Temporal","hint")=
			"Tries to retain state of the prior frame."
			"  If the prior frame was not the last evaluated,"
			" the node may choose not to use temporal data.";
	catalog<bool>("Temporal","joined")=true;

	catalog<Real>("Response")=1.0;
	catalog<String>("Response","enabler")="Temporal";
	catalog<String>("Response","page")="Behavior";
	catalog<String>("Response","hint")=
			"When temporal, this defines how quickly the surface returns"
			" to its original shaped when released.";
	catalog<bool>("Response","joined")=true;

	catalog<Real>("ResponseRandom")=0.0;
	catalog<Real>("ResponseRandom","high")=1.0;
	catalog<String>("ResponseRandom","label")="+-";
	catalog<String>("ResponseRandom","enabler")="Temporal";
	catalog<String>("ResponseRandom","page")="Behavior";

	catalog<Real>("Compensation")=1.0;
	catalog<String>("Compensation","enabler")="Temporal";
	catalog<String>("Compensation","page")="Behavior";
	catalog<String>("Compensation","hint")=
			"When temporal, this defines how much the surface holds"
			" to changes in the input (ignoring momentum).";
	catalog<bool>("Compensation","joined")=true;

	catalog<Real>("CompensationRandom")=0.0;
	catalog<Real>("CompensationRandom","high")=1.0;
	catalog<String>("CompensationRandom","label")="+-";
	catalog<String>("CompensationRandom","enabler")="Temporal";
	catalog<String>("CompensationRandom","page")="Behavior";

	catalog<Real>("Threshold")=0.0;
	catalog<Real>("Threshold","high")=1.0;
	catalog<Real>("Threshold","max")=1e6;
	catalog<String>("Threshold","page")="Behavior";
	catalog<String>("Threshold","hint")="Gap to maintain around collider.";
	catalog<bool>("Threshold","joined")=true;

	catalog<Real>("ThresholdRandom")=0.0;
	catalog<Real>("ThresholdRandom","high")=1.0;
	catalog<Real>("ThresholdRandom","max")=1e6;
	catalog<String>("ThresholdRandom","label")="+-";
	catalog<String>("ThresholdRandom","page")="Behavior";

	catalog<Real>("Projection")=1.0;
	catalog<Real>("Projection","max")=1.0;
	catalog<String>("Projection","page")="Behavior";
	catalog<String>("Projection","hint")=
			"Amount that trailing points are affected by preceding impacts.";
	catalog<bool>("Projection","joined")=true;

	catalog<Real>("ProjectionRandom")=0.0;
	catalog<Real>("ProjectionRandom","high")=1.0;
	catalog<String>("ProjectionRandom","label")="+-";
	catalog<String>("ProjectionRandom","page")="Behavior";

	catalog<Real>("Repulsion")=0.0;
	catalog<Real>("Repulsion","high")=1.0;
	catalog<Real>("Repulsion","max")=10.0;
	catalog<String>("Repulsion","page")="Behavior";
	catalog<String>("Repulsion","hint")=
			"Push away nearby curves, even if not colliding.";
	catalog<bool>("Repulsion","joined")=true;

	catalog<Real>("RepulsionRandom")=0.0;
	catalog<Real>("RepulsionRandom","high")=0.1;
	catalog<String>("RepulsionRandom","label")="+-";
	catalog<String>("RepulsionRandom","page")="Behavior";

	catalog<Real>("RepulsionShrink")=0.0;
	catalog<Real>("RepulsionShrink","high")=1.0;
	catalog<Real>("RepulsionShrink","max")=10.0;
	catalog<String>("RepulsionShrink","label")="Repulsion Shrink";
	catalog<String>("RepulsionShrink","page")="Behavior";
	catalog<String>("RepulsionShrink","hint")=
			"Reduce the length relative to the magnitude of repulsion.";

	catalog<Real>("Falloff")=1.0;
	catalog<Real>("Falloff","high")=4.0;
	catalog<Real>("Falloff","max")=100.0;
	catalog<String>("Falloff","page")="Behavior";
	catalog<String>("Falloff","hint")=
			"Fade repulsion to zero over this distance.";
	catalog<bool>("Falloff","joined")=true;

	catalog<Real>("FalloffRandom")=0.0;
	catalog<Real>("FalloffRandom","high")=1.0;
	catalog<String>("FalloffRandom","label")="+-";
	catalog<String>("FalloffRandom","page")="Behavior";

	catalog<Real>("Retraction")=1.0;
	catalog<Real>("Retraction","high")=1.0;
	catalog<Real>("Retraction","max")=10.0;
	catalog<String>("Retraction","page")="Behavior";
	catalog<String>("Retraction","hint")=
			"Restore stretched segments by pulling back.";
	catalog<bool>("Retraction","joined")=true;

	catalog<Real>("RetractionRandom")=0.0;
	catalog<Real>("RetractionRandom","high")=1.0;
	catalog<String>("RetractionRandom","label")="+-";
	catalog<String>("RetractionRandom","page")="Behavior";

	catalog<Real>("lead")=1.0;
	catalog<Real>("lead","high")=1.0;
	catalog<Real>("lead","max")=100.0;
	catalog<String>("lead","label")="Lead In";
	catalog<String>("lead","page")="Behavior";
	catalog<String>("lead","hint")=
			"Adjust leading segments when any segment changes.";

	catalog<Real>("stepAngle")=0.01;
	catalog<Real>("stepAngle","high")=0.1;
	catalog<Real>("stepAngle","max")=2.0;
	catalog<String>("stepAngle","label")="Step Angle";
	catalog<String>("stepAngle","page")="Behavior";
	catalog<String>("stepAngle","hint")=
			"Radians of incremental rotation of a colliding segment.";

	catalog<Real>("stepDist")=0.0;
	catalog<Real>("stepDist","high")=0.1;
	catalog<Real>("stepDist","max")=1e6;
	catalog<String>("stepDist","label")="Step Distance";
	catalog<String>("stepDist","page")="Behavior";
	catalog<String>("stepDist","hint")=
			"Incremental base shift of a colliding segment.";

	catalog<Real>("stepShrink")=0.0;
	catalog<Real>("stepShrink","high")=0.1;
	catalog<Real>("stepShrink","max")=1e6;
	catalog<String>("stepShrink","label")="Step Shrink";
	catalog<String>("stepShrink","page")="Behavior";
	catalog<String>("stepShrink","hint")=
			"Incremental length reduction of a colliding segment.";

	catalog<I32>("subSteps")=0;
	catalog<I32>("subSteps","high")=10;
	catalog<I32>("subSteps","max")=100;
	catalog<String>("subSteps","label")="Sub Steps";
	catalog<String>("subSteps","page")="Behavior";
	catalog<String>("subSteps","hint")=
			"Fine increments per step.";

	catalog<Real>("proximityOuter")=0.1;
	catalog<Real>("proximityOuter","high")=1.0;
	catalog<Real>("proximityOuter","max")=100.0;
	catalog<String>("proximityOuter","label")="Proximity Outer";
	catalog<String>("proximityOuter","page")="Behavior";
	catalog<bool>("proximityOuter","joined")=true;
	catalog<String>("proximityOuter","hint")=
			"Distance to begin proximity response."
			"  This is more intense form of repulsion.";

	catalog<Real>("proximityInner")=0.0;
	catalog<Real>("proximityInner","high")=1.0;
	catalog<Real>("proximityInner","max")=100.0;
	catalog<String>("proximityInner","label")="Inner";
	catalog<String>("proximityInner","page")="Behavior";
	catalog<String>("proximityInner","hint")=
			"Distance of full proximity response.";

	catalog<Real>("skinAvoidance")=1.0;
	catalog<Real>("skinAvoidance","max")=1.0;
	catalog<String>("skinAvoidance","label")="Skin Avoidance";
	catalog<String>("skinAvoidance","page")="Behavior";
	catalog<String>("skinAvoidance","hint")=
			"Push hairs out of skin.";

	catalog<Real>("skinAffinity")=1.0;
	catalog<Real>("skinAffinity","high")=1.0;
	catalog<Real>("skinAffinity","max")=100.0;
	catalog<String>("skinAffinity","label")="Skin Affinity";
	catalog<String>("skinAffinity","page")="Behavior";
	catalog<String>("skinAffinity","hint")=
			"Bias rotation directly towards skin when laying close to skin.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Collider Surface");

	catalog< sp<Component> >("Skin Surface");
	catalog<bool>("Skin Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;
	catalog<bool>("Output Surface","temporal")=true;

	m_spDebugGroup=new DrawMode();
	m_spDebugGroup->setGroup("debug");
}

void DodgeOp::handle(Record& a_rSignal)
{
#if FE_DODGE_DEBUG
	feLog("DodgeOp::handle\n");
#endif

#if FE_DODGE_PROFILE
	m_spProfiler=new Profiler("DodgeOp");
	m_spProfileAccess=new Profiler::Profile(m_spProfiler,"Access");
	m_spProfilePrepare=new Profiler::Profile(m_spProfiler,"Prepare");
	m_spProfileCache=new Profiler::Profile(m_spProfiler,"Cache");
	m_spProfileRun=new Profiler::Profile(m_spProfiler,"Run");
	m_spProfileFlush=new Profiler::Profile(m_spProfiler,"Flush");
	m_spProfileNearest=new Profiler::Profile(m_spProfiler,"Nearest");
	m_spProfileRepulsion=new Profiler::Profile(m_spProfiler,"Repulsion");
	m_spProfileFirstRay=new Profiler::Profile(m_spProfiler,"FirstRay");
	m_spProfileStepRay=new Profiler::Profile(m_spProfiler,"StepRay");
	m_spProfileSubRay=new Profiler::Profile(m_spProfiler,"SubRay");
	m_spProfileLength=new Profiler::Profile(m_spProfiler,"Length");
	m_spProfileLead=new Profiler::Profile(m_spProfiler,"Lead");
	m_spProfileProjection=new Profiler::Profile(m_spProfiler,"Projection");

	m_spProfiler->begin();
	m_spProfileAccess->start();
#endif

	catalog<String>("summary")="";

	const BWORD partitionCollider=catalog<bool>("PartitionCollider");
	const String colliderPartitionAttr=catalog<String>("ColliderPartitionAttr");

	const String group=catalog<String>("Group");
	const I32 refinement=catalog<I32>("Refinement");
	const BWORD temporal=catalog<bool>("Temporal");
	const Real response=catalog<Real>("Response");
	const Real responseRandom=catalog<Real>("ResponseRandom");
	const Real compensation=catalog<Real>("Compensation");
	const Real compensationRandom=catalog<Real>("CompensationRandom");
	const I32 deadZone=catalog<I32>("DeadZone");
	const String deadZoneAttr=catalog<String>("DeadZoneAttr");

	const BWORD doTemporal=temporal && (response<1.0 || responseRandom>0.0);

	m_repulsionScale=doTemporal? response: 1.0;

	//* output curves is copy of input curves
	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	if(!access(m_spInputAccessible,"Input Surface")) return;

	const String directionMethod=catalog<String>("directionMethod");
	if(directionMethod=="tangent")
	{
		const String tangentName=catalog<String>("TangentAttr");

		sp<SurfaceAccessorI> spInputTangent;
		if(!access(spInputTangent,m_spInputAccessible,
				e_primitive,tangentName)) return;
	}

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,
			e_primitiveGroup,group)) return;

	const U32 primitiveCount=spInputVertices->count();
	if(!primitiveCount)
	{
		catalog<String>("error")="Input Surface has no primitives;";
		return;
	}

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,m_spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spDeadZone;
	access(spDeadZone,m_spInputAccessible,e_primitive,deadZoneAttr,e_quiet);

	//* TODO not if VDB
#if FALSE
	sp<SurfaceAccessorI> spColliderNormals;
	if(!access(spColliderNormals,"Collider Surface",e_point,e_normal) ||
			!spColliderNormals->count())
	{
		catalog<String>("error")="Collider Surface lacks normals;";
		return;
	}
	spColliderNormals=NULL;
#endif

#if FE_DODGE_PROFILE
	m_spProfilePrepare->replace(m_spProfileAccess);
#endif

	//* reset if partitioning toggled
	if(m_spCollider.isValid() &&
			(m_spCollider->partitionCount()>0)!=partitionCollider)
	{
		m_spCollider=NULL;
	}

	const BWORD colliderReplaced=
			catalogOrDefault<bool>("Collider Surface","replaced",true);
	if(colliderReplaced || m_spCollider.isNull())
	{
		if(!access(m_spCollider,"Collider Surface",e_warning)) return;

		if(partitionCollider)
		{
			m_spCollider->partitionWith(colliderPartitionAttr);
		}

		m_spCollider->setRefinement(refinement);
		m_spCollider->prepareForSearch();
	}

	const BWORD skinReplaced=
			catalogOrDefault<bool>("Skin Surface","replaced",true);
	if(skinReplaced || m_spSkin.isNull())
	{
		access(m_spSkin,"Skin Surface",e_quiet);

		if(m_spSkin.isValid())
		{
			m_spSkin->setRefinement(refinement);
			m_spSkin->prepareForSearch();
		}
	}

#if FE_DODGE_PROFILE
	m_spProfileCache->replace(m_spProfilePrepare);
#endif

	//* access DrawI interpreter
	const BWORD debugCollider=catalog<bool>("DebugCollider");
	const BWORD debugCollision=catalog<bool>("DebugCollision");
	const BWORD debugRepulsion=catalog<bool>("DebugRepulsion");
	const BWORD debugRay=catalog<bool>("DebugRay");
	if(debugCollider || debugCollision || debugRepulsion || debugRay)
	{
		if(!accessGuide(m_spDrawDebug,a_rSignal)) return;
	}
	else
	{
		m_spDrawDebug=NULL;
	}

	if(m_spDrawDebug.isValid())
	{
		m_spDrawDebug->pushDrawMode(m_spDebugGroup);

		if(debugCollider)
		{
			m_spDrawDebug->draw(m_spCollider,NULL);
		}
	}

	const U32 pointCount=spInputPoints->count();
	m_pointCache.resize(pointCount);
	m_lastCache.resize(pointCount);
	m_changedCache.resize(pointCount);

	const I32 frame=currentFrame(a_rSignal);
	const BWORD outputReplaced=catalog<bool>("Output Surface","replaced");
	const BWORD reuseCache=
			(frame==m_frame+1 || (frame==m_frame && !outputReplaced));

#if FE_DODGE_DEBUG
	feLog("DodgeOp::handle frame %d outputReplaced %d colliderReplaced %d\n",
			frame,outputReplaced,colliderReplaced);
#endif

	//* presuming always replaced with frame change
	if(doTemporal && reuseCache)
	{
#if FE_DODGE_DEBUG
		if(frame==m_frame+1)
		{
			feLog("DodgeOp::handle temporal step\n");
		}
		else
		{
			feLog("DodgeOp::handle temporal repeat\n");
		}
#endif

		sp<SurfaceAccessorI> spOutputVertices;
		if(!access(spOutputVertices,spOutputAccessible,
				e_primitiveGroup,group)) return;

		const I32 seed=catalog<I32>("Seed");

		//* output = history:prior blended with pure input
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
				primitiveIndex++)
		{
			I32 oneDeadZone=spDeadZone.isValid()?
					spDeadZone->integer(primitiveIndex): 0;
			if(oneDeadZone<1)
			{
				oneDeadZone=deadZone;
			}
			FEASSERT(oneDeadZone>0);

			U32 oneSeed=seed+primitiveCount+primitiveIndex;
			const Real oneResponse=response+
					responseRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
			const Real oneCompensation=compensation+
					compensationRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
			if(oneResponse<=0.0 && !outputReplaced)
			{
				continue;
			}
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);

				SpatialVector& rCachePoint=m_pointCache[pointIndex];
				SpatialVector& rCacheLast=m_lastCache[pointIndex];

				const SpatialVector point=
						spInputPoints->spatialVector(pointIndex);
				SpatialVector adjusted=point;

				if(subIndex>=oneDeadZone && oneResponse<1.0)
				{
					const SpatialVector shift=point-rCacheLast;
					const SpatialVector shifted=
							rCachePoint+oneCompensation*shift;

					adjusted=point*oneResponse+shifted*(1.0-oneResponse);
				}

				rCachePoint=adjusted;
				rCacheLast=point;

				m_changedCache[pointIndex]=TRUE;
			}
		}
	}
	else
	{
#if FE_DODGE_DEBUG
		feLog("DodgeOp::handle rebuild\n");
#endif
		//* just use new output
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector& rPoint=
					spInputPoints->spatialVector(pointIndex);
			m_pointCache[pointIndex]=rPoint;
			m_lastCache[pointIndex]=rPoint;
			m_changedCache[pointIndex]=TRUE;	//* TODO more selective
		}
	}
	m_frame=frame;

#if FE_DODGE_DEBUG
		feLog("DodgeOp::handle run\n");
#endif

#if FE_DODGE_PROFILE
	m_spProfileRun->replace(m_spProfileCache);
#endif

	const U32 threadCount=catalog<I32>("Threads");
	m_passLimitCount.clear();

	if(threadCount<2 || partitionCollider)
	{
		//* serial

		if(threadCount>1)
		{
			catalog<String>("warning")+=
					"multi-threading disallowed with partitioned colliders;";
		}

		sp<SpannedRange> spSerialRange=sp<SpannedRange>(new SpannedRange());
		spSerialRange->nonAtomic().add(0,primitiveCount-1);

		run(-1,spSerialRange);
	}
	else
	{
		//* parallel

		setNonAtomicRange(0,primitiveCount-1);
		OperatorThreaded::handle(a_rSignal);
	}

	catalog<String>("summary")=I32(primitiveCount)+String(" curves");

	U32 passLimitSum=0;
	const U32 limitSize=m_passLimitCount.size();
	for(U32 limitIndex=0;limitIndex<limitSize;limitIndex++)
	{
		passLimitSum+=m_passLimitCount[limitIndex];
	}

	if(passLimitSum)
	{
		String text;
		text.sPrintf("correction limit reached %d time%s;",
				passLimitSum,passLimitSum>1? "s": "");
		catalog<String>("warning")+=text;
	}

#if FE_DODGE_DEBUG
		feLog("DodgeOp::handle ran\n");
#endif

#if FE_DODGE_PROFILE
	m_spProfileFlush->replace(m_spProfileRun);
#endif

	//* flush result to output

	sp<SurfaceAccessorI> spOutputPoints;
	if(!access(spOutputPoints,spOutputAccessible,
			e_point,e_position)) return;

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(m_changedCache[pointIndex])
		{
			spOutputPoints->set(pointIndex,m_pointCache[pointIndex]);
			m_changedCache[pointIndex]=FALSE;
		}
	}

	if(m_spDrawDebug.isValid())
	{
		m_spDrawDebug->popDrawMode();
	}

#if FE_DODGE_DEBUG
	feLog("DodgeOp::handle done\n");
#endif

#if FE_DODGE_PROFILE
	m_spProfileFlush->finish();
	m_spProfiler->end();

	feLog("%s:\n%s\n",m_spProfiler->name().c_str(),
			m_spProfiler->report().c_str());
#endif
}

void DodgeOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color cyan(0.0,1.0,1.0);
	const Color yellow(1.0,1.0,0.0);
	const Color orange(1.0,0.5,0.0);
	const Color magenta(1.0,0.0,1.0);
	const Color black(0.0,0.0,0.0);
	const Color white(1.0,1.0,1.0);

	const SpatialVector tinyOffset(0.01,0.01,0.01);

	const String group=catalog<String>("Group");

	const I32 seed=catalog<I32>("Seed");

	const BWORD partitionCollider=catalog<bool>("PartitionCollider");
	const String collisionPartition=catalog<String>("CollisionPartition");
	const String repulsionPartition=catalog<String>("RepulsionPartition");
	const String inputRepulsionAttr=catalog<String>("inputRepulsionAttr");
	const String inputCollisionAttr=catalog<String>("inputCollisionAttr");

	const Real threshold=catalog<Real>("Threshold");
	const Real thresholdRandom=catalog<Real>("ThresholdRandom");
	const Real projection=catalog<Real>("Projection");
	const Real projectionRandom=catalog<Real>("ProjectionRandom");
	const Real repulsion=catalog<Real>("Repulsion");
	const Real repulsionRandom=catalog<Real>("RepulsionRandom");
	const Real falloff=catalog<Real>("Falloff");
	const Real falloffRandom=catalog<Real>("FalloffRandom");
	const Real repulsionShrink=catalog<Real>("RepulsionShrink");
	const Real retraction=catalog<Real>("Retraction");
	const Real retractionRandom=catalog<Real>("RetractionRandom");
	const Real lead=catalog<Real>("lead");
	const BWORD debugCollision=catalog<bool>("DebugCollision");
	const BWORD debugRepulsion=catalog<bool>("DebugRepulsion");
	const BWORD debugRay=catalog<bool>("DebugRay");
	const I32 debugSegment=catalog<I32>("DebugSegment");
	const String directionMethod=catalog<String>("directionMethod");
	const String tangentName=catalog<String>("TangentAttr");
	const Real proximityOuter=catalog<Real>("proximityOuter");
	const Real proximityInner=catalog<Real>("proximityInner");
	const Real skinAvoidance=catalog<Real>("skinAvoidance");
	const Real skinAffinity=catalog<Real>("skinAffinity");
	const I32 deadZone=catalog<I32>("DeadZone");
	const String deadZoneAttr=catalog<String>("DeadZoneAttr");
	const Real stepAngle=catalog<Real>("stepAngle");
	const Real stepDist=catalog<Real>("stepDist");
	const Real stepShrink=catalog<Real>("stepShrink");
	const I32 subSteps=catalog<I32>("subSteps");

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,
			e_primitiveGroup,group)) return;

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,m_spInputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputTangent;
	if(directionMethod=="tangent")
	{
		if(!access(spInputTangent,m_spInputAccessible,
				e_primitive,tangentName,e_quiet)) return;
	}

	sp<SurfaceAccessorI> spInputRepulsor;
	sp<SurfaceAccessorI> spInputCollider;

	if(partitionCollider)
	{
		access(spInputRepulsor,m_spInputAccessible,
			e_primitive,inputRepulsionAttr,e_quiet);
		access(spInputCollider,m_spInputAccessible,
			e_primitive,inputCollisionAttr,e_quiet);
	}

#if FE_CODEGEN<=FE_DEBUG
	const I32 pointCount=spInputPoints->count();
#endif

	sp<SurfaceAccessorI> spDeadZone;
	access(spDeadZone,m_spInputAccessible,e_primitive,deadZoneAttr,e_quiet);

	Real radius=m_spCollider->radius();
	SpatialVector center=m_spCollider->center();

#if FE_DODGE_DEBUG
	feLog("DodgeOp::run running thread %d %s\n",
			a_id,a_spRange->brief().c_str());
	feLog("  collider center %s radius %.6G\n",c_print(center),radius);
#endif

	Array< sp<SurfaceI::ImpactI> > impactArray;

	I32 maxCount=0;
	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		const U32 primitiveIndex=it.value();
		const I32 subCount=spInputVertices->subCount(primitiveIndex);
		if(maxCount<subCount)
		{
			maxCount=subCount;
		}
	}

	SpatialQuaternion* repulsed=new SpatialQuaternion[maxCount];

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 primitiveIndex=it.value();
		const I32 subCount=spInputVertices->subCount(primitiveIndex);

#if FE_DODGE_DEBUG
		feLog("DodgeOp::run primitive %d subCount %d\n",
				primitiveIndex,subCount);
#endif

		I32 oneDeadZone=
				spDeadZone.isValid()? spDeadZone->integer(primitiveIndex): 0;
		if(oneDeadZone<1)
		{
			oneDeadZone=deadZone;
		}

		FEASSERT(oneDeadZone>0);
		if(subCount<=oneDeadZone)
		{
#if FE_DODGE_IMPACT_DEBUG
			feLog(" subCount %d < oneDeadZone %d\n",
					subCount,oneDeadZone);
#endif
			continue;
		}

		U32 oneSeed=seed+primitiveIndex;
		const Real oneThreshold=threshold+
				thresholdRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
		const Real oneProjection=projection+
				projectionRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
		Real oneRepulsion=m_repulsionScale*(repulsion+
				repulsionRandom*randomRealReentrant(&oneSeed,-1.0,1.0));
		const Real oneFalloff=falloff+
				falloffRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
		const Real oneRetraction=retraction+
				retractionRandom*randomRealReentrant(&oneSeed,-1.0,1.0);

		const Real extendedRadius=radius+oneThreshold+falloff;
		const Real extendedRadius2=extendedRadius*extendedRadius;

		//* push curve points out of collider in direction of nearest normal

		BWORD hit=FALSE;
		SpatialVector pivotPoint=
				spInputVertices->spatialVector(primitiveIndex,0);
		SpatialVector lastPivotPoint=pivotPoint;
		I32 pivotSub=0;
		I32 lastPivotSub=0;
		SpatialQuaternion pivotQuat;

		const SpatialVector rootPoint=
				spInputVertices->spatialVector(primitiveIndex,0);
		const SpatialVector tipPoint=
				spInputVertices->spatialVector(primitiveIndex,subCount-1);

		const Real spanLength=magnitude(tipPoint-rootPoint);
//		if(spanLength<FE_DODGE_THRESHOLD)
//		{
//			feLog("DodgeOp::run primitive %d spanLength %.6G\n",
//					primitiveIndex,spanLength);
//		}
		if(spanLength<FE_DODGE_TINY)
		{
			feLog("DodgeOp::run primitive %d is too short (%.6G<%.6G)\n",
					primitiveIndex,spanLength,FE_DODGE_THRESHOLD);
			continue;
		}

		const Real scopeRadius=extendedRadius+spanLength;
		const Real scopeRadius2=scopeRadius*scopeRadius;

		if(magnitudeSquared(rootPoint-center)>scopeRadius2)
		{
#if FE_DODGE_IMPACT_DEBUG
			feLog(" root %s outside center %s by %.6G"
					" extendedRadius %.6G spanLength %.6G\n",
					c_print(rootPoint),c_print(center),
					magnitude(rootPoint-center),
					extendedRadius,spanLength);
#endif
			continue;
		}

		const Real maxDistance=spanLength+oneThreshold+falloff;

#if FE_DODGE_PROFILE
		m_spProfileNearest->replace(m_spProfileRun);
#endif

		if(partitionCollider)
		{
			String repulsionPattern=spInputRepulsor.isValid()?
					spInputRepulsor->string(primitiveIndex): "";
			if(repulsionPattern.empty())
			{
				repulsionPattern=repulsionPartition;
			}

			m_spCollider->setPartitionFilter(repulsionPattern);
		}

		sp<SurfaceI::ImpactI> spRootImpact=
				m_spCollider->nearestPoint(rootPoint,maxDistance);

		if(partitionCollider)
		{
			String collisionPattern=spInputCollider.isValid()?
					spInputCollider->string(primitiveIndex): "";
			if(collisionPattern.empty())
			{
				collisionPattern=collisionPartition;
			}

			m_spCollider->setPartitionFilter(collisionPattern);
		}

#if FE_DODGE_PROFILE
		m_spProfileRun->replace(m_spProfileNearest);
#endif

//		feLog("root %s distance %.6G/%.6G\n",
//				c_print(rootPoint),
//				spRootImpact.isValid()? spRootImpact->distance(): -1.0,
//				maxDistance);

		if(spRootImpact.isNull() || spRootImpact->distance()<0.0)
		{
#if FE_DODGE_IMPACT_DEBUG
			if(spRootImpact.isNull())
			{
				feLog(" root impact NULL\n");
			}
			else
			{
				feLog(" root impact dist %.6G\n",spRootImpact->distance());
			}
#endif
			if(!partitionCollider)
			{
				continue;
			}
		}

		sp<SurfaceI::ImpactI> spReImpact;
		if(partitionCollider)
		{
			spReImpact=m_spCollider->nearestPoint(rootPoint,maxDistance);
		}

		SpatialVector escape(0.0,-1.0,0.0);
		if(directionMethod=="tangent")
		{
			escape=unitSafe(spInputTangent->spatialVector(primitiveIndex));
		}
		else if(directionMethod=="normal")
		{
			if(spRootImpact.isValid())
			{
				escape=spRootImpact->normal();
			}
			else if(spReImpact.isValid())
			{
				escape=spReImpact->normal();
			}
		}

		SpatialVector unitSpan=(1.0/spanLength)*(tipPoint-rootPoint);
		SpatialVector alongVector=unitSpan*dot(unitSpan,escape);
		SpatialVector side=unitSafe(cross(unitSpan,escape));
//		SpatialVector show(0.0,0.0,0.0);

		//* check if hair pokes back into skin
		if(m_spSkin.isValid())
		{
			sp<SurfaceI::ImpactI> spImpact=
					m_spSkin->nearestPoint(tipPoint,maxDistance);

			if(spImpact.isValid() && spImpact->distance()>=0.0)
			{
				const SpatialVector skinNormal=spImpact->normal();

				const Real blendDot=dot(unitSpan,skinNormal);

				Real blendTip=blendDot;
				if(blendTip<0.0)
				{
					blendTip=0.0;
				}
				blendTip=pow(blendTip,skinAffinity);

				//* blend to root segment direction
//				const I32 safeIndex=(subCount+1)>>1;
				const I32 safeIndex=1;

				const SpatialVector safePoint=
						spInputVertices->spatialVector(
						primitiveIndex,safeIndex);

				const SpatialVector blendPoint=
						(1.0-blendTip)*safePoint+
						blendTip*tipPoint;

				unitSpan=unitSafe(blendPoint-rootPoint);

				//* if necessary, flip normal component
				const Real safeDot=dot(unitSpan,skinNormal);
				if(safeDot<0.0)
				{
					const SpatialVector withNormal=safeDot*skinNormal;

					unitSpan=unitSafe(unitSpan-2.0*withNormal);
				}

				alongVector=unitSpan*dot(unitSpan,escape);

				sp<SurfaceI::ImpactI> spImpact2=
						m_spSkin->nearestPoint(blendPoint,maxDistance);

				if(spImpact2.isValid() && spImpact2->distance()>=0.0)
				{
					const SpatialVector intersection=spImpact2->intersection();

					//* rotate directly towards skin
					const SpatialVector unitSkim=
							unitSafe(intersection-rootPoint);

					//* NOTE checks for opposing tangent
					const SpatialVector axis=unitSafe(cross(unitSpan,unitSkim));
					const SpatialVector sideSkim=
							(dot(unitSpan,escape)>=0.0)? axis: -axis;

					side=unitSafe((1.0-blendTip)*sideSkim+blendTip*side);

//					show=axis;
				}

				if(debugRepulsion && m_spDrawDebug.isValid())
				{
					SAFEGUARD;

					//* orange: blendPoint
					SpatialVector line[2];
					line[0]=rootPoint+tinyOffset;
					line[1]=line[0]+2.0*(blendPoint-rootPoint);

					Color colors[2];
					colors[0]=black;
					colors[1]=orange;

					m_spDrawDebug->drawLines(line,NULL,2,
							DrawI::e_discrete,TRUE,colors);
				}
			}
		}

		//* for repulsion
		const SpatialVector pushVector=unitSafe(escape-alongVector);

		I32 lastIndex=spInputVertices->integer(primitiveIndex,oneDeadZone-1);
		SpatialVector lastPoint=m_pointCache[lastIndex];

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			set(repulsed[subIndex]);
		}

#if FE_DODGE_PROFILE
		m_spProfileRepulsion->replace(m_spProfileRun);
#endif

		Real initialScale=1.0;

		if(spRootImpact.isValid() && oneRepulsion>0.0)
		{
			const SpatialVector intersection=spRootImpact->intersection();

			const SpatialVector away=rootPoint-intersection;
			const Real proximity=magnitude(away);

			SpatialVector lastOrigPoint=spInputVertices->spatialVector(
					primitiveIndex,oneDeadZone-1);

			const Real rampIn=oneFalloff-proximity;

#if FE_DODGE_IMPACT_DEBUG
			feLog(" escape %s intersection %s prox %.6G\n",
					c_print(escape),c_print(intersection),proximity);
			feLog(" rampIn %.6G/%.6G\n",rampIn,oneFalloff);
#endif

			SpatialVector unitRepulsion=pushVector;

			Real scalarRepulsion=oneRepulsion;

			if(proximity>0.0)
			{
				if(oneFalloff>0.0 && rampIn<oneFalloff)
				{
					//* falloff zone outside collider
					const Real ratio=rampIn/oneFalloff;
					scalarRepulsion*=ratio;
					unitRepulsion=unitSafe(pushVector*ratio+
							escape*(1.0-ratio));
				}
				else
				{
					scalarRepulsion=0.0;
				}
			}

#if FE_DODGE_IMPACT_DEBUG
			feLog("repulse %.6G * %s\n",scalarRepulsion,
					c_print(unitRepulsion));
#endif

			if(debugRepulsion && m_spDrawDebug.isValid())
			{
				SAFEGUARD;

				//* TODO adaptive
				const Real normalScale=0.1;

				//* white root: inside collider
				//* black root: outside collider

				//* yellow: repulsion
				SpatialVector line[2];
				line[0]=rootPoint+tinyOffset;
				line[1]=line[0]+scalarRepulsion*unitRepulsion;
//				line[1]=line[0]+show;

				Color colors[2];
				colors[0]=(proximity<0.0? white: black);
				colors[1]=yellow;

				m_spDrawDebug->drawLines(line,NULL,2,
						DrawI::e_discrete,TRUE,colors);

				//* green: escape
				line[0]=rootPoint;
				line[1]=rootPoint+escape;

				colors[1]=green;

				m_spDrawDebug->drawLines(line,NULL,2,
						DrawI::e_discrete,TRUE,colors);

				//* blue: pushVector
				line[0]=rootPoint;
				line[1]=rootPoint+pushVector;

				colors[1]=blue;

				m_spDrawDebug->drawLines(line,NULL,2,
						DrawI::e_discrete,TRUE,colors);

				//* magenta: alongVector
				line[0]=rootPoint+tinyOffset;
				line[1]=line[0]+alongVector;

				colors[1]=magenta;

				m_spDrawDebug->drawLines(line,NULL,2,
						DrawI::e_discrete,TRUE,colors);

				//* cyan: intersection
				line[0]=rootPoint;
				line[1]=intersection;

				colors[1]=cyan;

				m_spDrawDebug->drawLines(line,NULL,2,
						DrawI::e_discrete,TRUE,colors);

				//* red: impact normal
				const SpatialVector impactNormal=spRootImpact->normal();
				line[0]=intersection;
				line[1]=intersection+impactNormal*normalScale;

				colors[1]=red;

				m_spDrawDebug->drawLines(line,NULL,2,
						DrawI::e_discrete,TRUE,colors);
			}

			if(scalarRepulsion>0.0)
			{
				initialScale=fmax(1.0-scalarRepulsion*repulsionShrink,
						FE_DODGE_THRESHOLD);

				const Real spanDot=(proximity>0.0)?
						-dot(away*(1.0/proximity),unitSpan): 0.0;

				const Real dotRamp=spanDot>0.0? 1.0: 1.0+spanDot;
				const Real pseudoProximity=proximity*dotRamp;
				const Real angleScale=(pseudoProximity<proximityInner)? 1.0:
						((proximityOuter<=proximityInner)? -1.0:
						(pseudoProximity-proximityOuter)/
						(proximityInner-proximityOuter));

#if FE_DODGE_IMPACT_DEBUG
				feLog("spanDot %.6G dotRamp %.6G"
						" pseudoProximity %.6G angleScale %.6G\n",
						spanDot,dotRamp,pseudoProximity,angleScale);
#endif

				for(I32 subIndex=oneDeadZone;subIndex<subCount;subIndex++)
				{
					const SpatialVector origPoint=
							spInputVertices->spatialVector(
							primitiveIndex,subIndex);

					const I32 pointIndex=
							spInputVertices->integer(primitiveIndex,subIndex);

					//* excess repulsion can cause temporal bouncing
//					if(scalarRepulsion>rampIn)
//					{
//						scalarRepulsion=rampIn;
//					}

					const SpatialVector point=m_pointCache[pointIndex];
					const SpatialVector segmentVector=point-lastPoint;
					const Real origLength=magnitude(segmentVector);
					if(origLength<FE_DODGE_TINY)
					{
						continue;
					}

					const SpatialVector unitSegment=segmentVector/origLength;

//					const SpatialVector lastVector=origPoint-lastOrigPoint;
//					const Real origLength=magnitude(lastVector)+FE_DODGE_TINY;
//					Real angle=tan(scalarRepulsion/origLength);
					Real angle=tan(scalarRepulsion);

					//* HACK
					angle*=subIndex;

					const Real escapeAngle=acos(dot(unitSegment,escape));
					if(angle>escapeAngle)
					{
						angle=escapeAngle;
					}
					else if(angleScale>0.0)
					{
						FEASSERT(angleScale<1.001);

						const Real minAngle=escapeAngle*angleScale;
						if(angle<minAngle)
						{
							angle=minAngle;
						}
					}

					const SpatialQuaternion rotation(angle,side);
					SpatialVector rearm;
					rotateVector(rotation,segmentVector,rearm);

					m_pointCache[pointIndex]=lastPoint+rearm;
					m_changedCache[pointIndex]=TRUE;

					if(oneRetraction>0.0)
					{
						const SpatialVector firstOrig=
								spInputVertices->spatialVector(
								primitiveIndex,subIndex);
						const SpatialVector secondOrig=
								spInputVertices->spatialVector(
								primitiveIndex,subIndex-1);
						const Real maxLength=initialScale*
								magnitude(secondOrig-firstOrig);

						if(maxLength>0.0)
						{
							const SpatialVector delta=
									m_pointCache[pointIndex]-lastPoint;
							const Real length=magnitude(delta);

							if(length>maxLength)
							{
								const Real reLength=length+
										oneRetraction*(maxLength-length);

								m_pointCache[pointIndex]=
										m_pointCache[lastIndex]+
										delta*(reLength/length);
								m_changedCache[pointIndex]=TRUE;
							}
						}
					}

					repulsed[subIndex]=m_pointCache[pointIndex]-point;

					if(debugRepulsion && m_spDrawDebug.isValid() &&
							(debugSegment<0 || debugSegment==subIndex))
					{
						SAFEGUARD;

						//* cyan: side
						SpatialVector line[2];
						line[0]=point;
						line[1]=point+side;

						Color colors[2];
						colors[0]=black;
						colors[1]=cyan;

						m_spDrawDebug->drawLines(line,NULL,2,
								DrawI::e_discrete,TRUE,colors);
					}

					lastIndex=pointIndex;
					lastPoint=m_pointCache[lastIndex];

					lastOrigPoint=origPoint;
				}
			}

			for(I32 subIndex=subCount-1;subIndex>0;subIndex--)
			{
				repulsed[subIndex]*=inverse(repulsed[subIndex-1]);
			}

			//* root point
			lastIndex=spInputVertices->integer(primitiveIndex,oneDeadZone-1);
			lastPoint=m_pointCache[lastIndex];
		}

#if FE_DODGE_PROFILE
		m_spProfileRun->replace(m_spProfileRepulsion);
#endif

		if(partitionCollider)
		{
			//* just distance from collision partition
			spRootImpact=spReImpact;
		}

		if(spRootImpact.isNull() ||
				spRootImpact->distance()>spanLength+oneThreshold)
		{
#if FE_DODGE_IMPACT_DEBUG
			if(spRootImpact.isValid())
			{
				feLog("dist %.6G > span %.6G + threshold %.6G\n",
						spRootImpact->distance(),spanLength,oneThreshold);
			}
#endif
			continue;
		}

		if(I32(impactArray.size())<subCount)
		{
			impactArray.resize(subCount);
		}

		//* TODO param
		const U32 passMax=(stepAngle>0.0)?
				2.0*(subCount-oneDeadZone)/stepAngle: 100;
//		const U32 passMax=(stepAngle>0.0)?
//				1.6/stepAngle: 1;
		U32 pass=0;

		if(stepAngle<=0.0 && stepDist<=0.0)
		{
			continue;
		}

		Real* shrinkScale=new Real[subCount];
		for(I32 m=0;m<subCount;m++)
		{
			shrinkScale[m]=initialScale;
		}

		BWORD anyChanged=FALSE;

		//* NOTE skip root points including dead zone
		for(I32 subIndex=oneDeadZone;subIndex<subCount;subIndex++)
		{
			BWORD changed=FALSE;
			I32 subChanged=subIndex;

			const I32 pointIndex=
					spInputVertices->integer(primitiveIndex,subIndex);

			SpatialVector point=m_pointCache[pointIndex];
			const SpatialVector origPoint=point;

			const Real mag2=magnitudeSquared(point-center);

#if FE_DODGE_IMPACT_DEBUG
			feLog("\nDodgeOp::handle %d %d/%d point %d/%d  %s\n",
					primitiveIndex,subIndex,subCount,
					pointIndex,pointCount,c_print(point));
			feLog("pivotPoint %s\n",c_print(pivotPoint));
			feLog("pivotQuat %s\n",c_print(pivotQuat));
			feLog("hit %d mag2 %.6G/%.6G (%.6G/%.6G)\n",hit,
					mag2,extendedRadius2,sqrt(mag2),sqrt(extendedRadius2));
#endif

			FEASSERT(pointIndex<pointCount);
			FEASSERT(magnitude(lastPoint)<1e12);
			FEASSERT(magnitude(point)<1e12);

			if(!hit && mag2>extendedRadius2)
			{
				if(mag2>scopeRadius2)
				{
					subIndex=subCount;
				}
				lastPoint=origPoint;
				lastIndex=pointIndex;
				continue;
			}

			sp<SurfaceI::ImpactI>& rspRayImpact=impactArray[subIndex];

			const BWORD checkSkin=(m_spSkin.isValid() && skinAvoidance>0.0);

			for(U32 collisionSurface=0;
					collisionSurface<((checkSkin && anyChanged)? 2: 1);
					collisionSurface++)
			{
				const SpatialVector segmentVector=point-lastPoint;
				const Real origLength=magnitude(segmentVector);
				if(origLength<FE_DODGE_TINY)
				{
					break;
				}

				const SpatialVector unitSegment=segmentVector/origLength;

				SpatialVector correction;
				set(correction);

				if(collisionSurface)
				{
					sp<SurfaceI::ImpactI> spImpact=
							m_spSkin->nearestPoint(point,maxDistance);

					if(spImpact.isNull() || spImpact->distance()<0.0)
					{
						continue;
					}

					const SpatialVector skinNormal=spImpact->normal();
					const SpatialVector intersection=spImpact->intersection();
					const SpatialVector goal=
							intersection+oneThreshold*skinNormal;

					correction=skinAvoidance*(goal-point);
					const SpatialVector towards=unitSafe(correction);

					if(dot(towards,skinNormal)<0.0)
					{
						continue;
					}
				}
				else
				{
					const Real rayDistance=origLength+oneThreshold;

					const BWORD anyHit=TRUE;

					sp<DrawI> spRayDebug=
							(debugRay &&
							(debugSegment<0 || debugSegment==subIndex))?
							m_spDrawDebug: sp<DrawI>(NULL);

#if FE_DODGE_PROFILE
					m_spProfileFirstRay->replace(m_spProfileRun);
#endif

					rspRayImpact=hitCheck(lastPoint,unitSegment,
							rayDistance,oneThreshold,anyHit,
							spRayDebug,debugRay,rspRayImpact);

#if FE_DODGE_PROFILE
					m_spProfileRun->replace(m_spProfileFirstRay);
#endif

					if(rspRayImpact.isNull() || rspRayImpact->distance()<0.0)
					{
						continue;
					}

					const Real stepShrinkScale=1.0-stepShrink;
					shrinkScale[subIndex]*=stepShrinkScale;

					const SpatialVector shrunkSegment=
							unitSegment*stepShrinkScale;

					const Real along=1.0-rspRayImpact->distance()/rayDistance;
					const Real scaledStepDist=stepDist*origLength*along;

					SpatialQuaternion rotation(stepAngle,side);

					SpatialVector rotated;
					rotateVector(rotation,shrunkSegment,rotated);

					const BWORD push=
							(scaledStepDist>0.0 && subIndex>oneDeadZone);
					SpatialVector pushed=
							push? lastPoint+escape*scaledStepDist: lastPoint;

					if(subSteps)
					{
#if FE_DODGE_PROFILE
						m_spProfileStepRay->replace(m_spProfileRun);
#endif

						rspRayImpact=hitCheck(pushed,rotated,
								rayDistance,oneThreshold,anyHit,
								spRayDebug,debugRay,rspRayImpact);

#if FE_DODGE_PROFILE
						m_spProfileRun->replace(m_spProfileStepRay);
#endif

						if(rspRayImpact.isNull() ||
								rspRayImpact->distance()<0.0)
						{
							//* TODO halvsies?
							for(I32 subStep=0;subStep<subSteps;subStep++)
							{
								const Real inbetween=
										(subStep+1.0)/(subSteps+1.0);
								const Real subAngle=stepAngle*inbetween;
								const Real subDist=scaledStepDist*inbetween;

								SpatialQuaternion rotation(subAngle,side);
								rotateVector(rotation,shrunkSegment,rotated);

								if(push)
								{
									pushed=lastPoint+escape*(subDist*along);
								}

#if FE_DODGE_PROFILE
								m_spProfileSubRay->replace(m_spProfileRun);
#endif

								rspRayImpact=hitCheck(pushed,rotated,
										rayDistance,oneThreshold,anyHit,
										spRayDebug,debugRay,rspRayImpact);

#if FE_DODGE_PROFILE
								m_spProfileRun->replace(m_spProfileSubRay);
#endif

								if(rspRayImpact.isNull() ||
										rspRayImpact->distance()<0.0)
								{
									break;
								}
							}
						}
					}

					const SpatialVector midpoint=
							lastPoint+0.5*segmentVector;

					if(push)
					{
						lastPoint=pushed;

						m_pointCache[lastIndex]=lastPoint;
						m_changedCache[pointIndex]=TRUE;

						if(subChanged>subIndex-1)
						{
							subChanged=subIndex-1;
						}
					}
					correction=origLength*(rotated-shrunkSegment);

					if(debugCollision && m_spDrawDebug.isValid() &&
							(debugSegment<0 || debugSegment==subIndex))
					{
						SAFEGUARD;

						SpatialVector line[2];
						line[0]=midpoint;
						line[1]=midpoint+side;

						Color colors[2];
						colors[0]=white;
						colors[1]=magenta;

						m_spDrawDebug->drawLines(line,NULL,2,
								DrawI::e_discrete,TRUE,colors);
					}
				}

				const SpatialVector oldPoint=point;
				point+=correction;

				m_pointCache[pointIndex]=point;
				m_changedCache[pointIndex]=TRUE;

#if FE_DODGE_PROFILE
				m_spProfileLength->replace(m_spProfileRun);
#endif

				if(oneRetraction>0.0)
				{
					//* prevent stretching

					//* TODO optimize

					//* pull ancestors forward
					const I32 reCount=subIndex-oneDeadZone;

					for(U32 rePass=0;rePass<2;rePass++)
					{
						//* rePass 0: pull ancestors forward
						//* rePass 1: retract children back to root

						for(U32 reIndex=0;reIndex<reCount+rePass;reIndex++)
						{
							const I32 firstSub=rePass? oneDeadZone-1+reIndex:
									subIndex-reIndex;
							const I32 secondSub=rePass? firstSub+1:
									firstSub-1;
							const I32 scaleSub=rePass? secondSub: firstSub;

							const SpatialVector firstOrig=
									spInputVertices->spatialVector(
									primitiveIndex,firstSub);
							const SpatialVector secondOrig=
									spInputVertices->spatialVector(
									primitiveIndex,secondSub);
							const Real maxLength=shrinkScale[scaleSub]*
									magnitude(secondOrig-firstOrig);

//							feLog("subIndex %d/%d reCount %d rePass %d"
//									" re %d %d max %.6G\n",
//									subIndex,subCount,
//									reCount,rePass,firstSub,secondSub,
//									maxLength);

							if(maxLength<=0.0)
							{
								continue;
							}

							const U32 firstIndex=spInputVertices->integer(
									primitiveIndex,firstSub);
							const U32 secondIndex=spInputVertices->integer(
									primitiveIndex,secondSub);
							const SpatialVector delta=
									m_pointCache[secondIndex]-
									m_pointCache[firstIndex];
							const Real length=magnitude(delta);

//							feLog(" index %d %d len %.6G/%.6G\n",
//									firstIndex,secondIndex,length,maxLength);

							//* HACK change threshold
							const Real threshold=FE_DODGE_THRESHOLD;

							if(length<=maxLength+threshold)
							{
								continue;
							}

							const Real reLength=length+
									oneRetraction*(maxLength-length);

							m_pointCache[secondIndex]=m_pointCache[firstIndex]+
									delta*(reLength/length);
							m_changedCache[secondIndex]=TRUE;
							if(subChanged>secondSub)
							{
								subChanged=secondSub;
							}
						}
					}

					lastPoint=m_pointCache[lastIndex];
					point=m_pointCache[pointIndex];
				}

#if FE_DODGE_PROFILE
				m_spProfileRun->replace(m_spProfileLength);
#endif

				if(debugCollision && m_spDrawDebug.isValid() &&
						(debugSegment<0 || debugSegment==subIndex))
				{
					SAFEGUARD;

					const Real colorScale=collisionSurface? 0.5: 1.0;

					//* white root: inside collider
					//* black root: outside collider

					//* blue: correction
					SpatialVector line[2];
					line[0]=oldPoint;
					line[1]=oldPoint+correction;

					Color colors[2];
					colors[0]=colorScale*white;
					colors[1]=colorScale*blue;

					m_spDrawDebug->drawLines(line,NULL,2,
							DrawI::e_discrete,TRUE,colors);

				}

				//* TODO param
				const Real minChange=FE_DODGE_THRESHOLD;

				SpatialVector newPivotPoint=m_pointCache[lastIndex];
				if(magnitudeSquared(point-newPivotPoint)>(minChange*minChange))
				{
					pivotPoint=newPivotPoint;
					pivotSub=subIndex-1;
					changed=TRUE;
					anyChanged=TRUE;
				}
			}

#if FE_DODGE_IMPACT_DEBUG
			feLog("pivotQuat %s\n",c_print(pivotQuat));
			feLog("pivotPoint %s\n",c_print(pivotPoint));
			feLog("lastPivotPoint %s\n",c_print(lastPivotPoint));
			feLog("point %s -> %s\n",
					c_print(m_pointCache[pointIndex]),c_print(point));
#endif

			const SpatialVector wasPoint=m_pointCache[pointIndex];

			m_pointCache[pointIndex]=point;
			m_changedCache[pointIndex]=TRUE;

			if(changed)
			{
				hit=TRUE;

#if FE_DODGE_PROFILE
				m_spProfileLead->replace(m_spProfileRun);
#endif

				if(lead>0.0)
				{
					//* lead in with even curvature
					const SpatialVector delta1=wasPoint-lastPivotPoint;
					const SpatialVector delta2=point-lastPivotPoint;
					const Real len1=magnitude(delta1);
					const Real len2=magnitude(delta2);

					//* TODO check for zero length
					const Real preScale=len2/len1;
					const SpatialVector rearm1=(wasPoint-lastPivotPoint)/len1;
					const SpatialVector rearm2=(point-lastPivotPoint)/len2;
					SpatialQuaternion preQuat;
					set(preQuat,rearm1,rearm2);
					const SpatialQuaternion partialQuat=
							angularlyScaled(preQuat,
							lead/(subIndex-oneDeadZone+1.0));

#if FE_DODGE_IMPACT_DEBUG
					feLog("preQuat %s\n",c_print(preQuat));
					feLog("partialQuat %s\n",c_print(partialQuat));
#endif

					const U32 start=(oneDeadZone>lastPivotSub+1)?
							oneDeadZone: lastPivotSub+1;
					for(I32 subIndex2=start;subIndex2<subIndex;subIndex2++)
					{
						const I32 reIndex=spInputVertices->integer(
								primitiveIndex,subIndex2);
						SpatialVector repoint=m_pointCache[reIndex];

						const SpatialVector rearm1=repoint-lastPivotPoint;

						SpatialVector rearm2;
						rotateVector(angularlyScaled(partialQuat,
								subIndex2-oneDeadZone+1.0),
								rearm1,rearm2);

						//* TODO lengthen hit segment instead of shrinking prior
						repoint=lastPivotPoint+preScale*rearm2;

						m_pointCache[reIndex]=repoint;
						m_changedCache[reIndex]=TRUE;

						if(subChanged>reIndex)
						{
							subChanged=reIndex;
						}
					}
				}

#if FE_DODGE_PROFILE
				m_spProfileProjection->replace(m_spProfileLead);
#endif

				FEASSERT(lastIndex>=0);
				FEASSERT(lastIndex<pointCount);

				const SpatialVector shiftedPivotPoint=m_pointCache[lastIndex];
				const SpatialVector arm1=unitSafe(origPoint-pivotPoint);
				const SpatialVector arm2=unitSafe(point-shiftedPivotPoint);

#if FE_DODGE_IMPACT_DEBUG
				feLog("arm1 %s\n",c_print(arm1));
				feLog("arm2 %s\n",c_print(arm2));
#endif

				//* follow through with projection
				set(pivotQuat,arm1,arm2);

				for(I32 subIndex2=subIndex+1;subIndex2<subCount;subIndex2++)
				{
					const I32 reIndex=
							spInputVertices->integer(primitiveIndex,subIndex2);
					SpatialVector repoint=m_pointCache[reIndex];

					const SpatialVector rearm1=repoint-pivotPoint;
					const Real rearmLength=magnitude(rearm1);


					const Real pivotScale=1.0+(oneProjection-1.0)*
							rearmLength/magnitude(tipPoint-pivotPoint);

					SpatialQuaternion subQuat=
							angularlyScaled(pivotQuat,pivotScale);

					Real subAngle;
					SpatialVector subAxis;
					subQuat.computeAngleAxis(subAngle,subAxis);

					Real rotatedAngle;
					SpatialVector rotatedAxis;
					repulsed[subIndex2].computeAngleAxis(
							rotatedAngle,rotatedAxis);

					if(rotatedAngle>FE_DODGE_THRESHOLD)
					{
						//* when already repulsed, drain if same direction
						const Real alignment=dot(subAxis,rotatedAxis);
						if(alignment>0.0)
						{
							Real drainAngle=subAngle*alignment;

							//* TODO cleanup
							const I32 preIndex=spInputVertices->integer(
									primitiveIndex,subIndex2-1);
							Real unrotateScale=rearmLength/
									magnitude(repoint-m_pointCache[preIndex]);

							Real unrotateAngle=drainAngle*unrotateScale;
							if(unrotateAngle>rotatedAngle)
							{
								unrotateAngle=rotatedAngle;
								drainAngle=unrotateAngle/unrotateScale;
							}

							const SpatialQuaternion drain(
									drainAngle,-rotatedAxis);
							const SpatialQuaternion unrotate(
									unrotateAngle,-rotatedAxis);

							subQuat*=drain;
							repulsed[subIndex2]*=unrotate;
						}
					}

					SpatialVector rearm2;
					rotateVector(subQuat,rearm1,rearm2);
					repoint=shiftedPivotPoint+rearm2;

#if TRUE
					const Real inputLength=shrinkScale[subIndex2]*
							magnitude(
							spInputVertices->spatialVector(
							primitiveIndex,subIndex2)-
							spInputVertices->spatialVector(
							primitiveIndex,subIndex2-1));

					const I32 lastIndex2=spInputVertices->integer(
							primitiveIndex,subIndex2-1);
					SpatialVector lastPoint2=m_pointCache[lastIndex2];
					SpatialVector reSegment=repoint-lastPoint2;
					const Real reLength=magnitude(reSegment);
					repoint=lastPoint2+reSegment*(inputLength/reLength);
#endif

					m_pointCache[reIndex]=repoint;
					m_changedCache[reIndex]=TRUE;

					if(subChanged>reIndex)
					{
						subChanged=reIndex;
					}
				}

#if FE_DODGE_PROFILE
				m_spProfileRun->replace(m_spProfileProjection);
#endif

				if(pass<passMax)
				{
#if FALSE
					//* rerun this segment
					subIndex=subIndex-1;
					lastIndex=spInputVertices->integer(primitiveIndex,subIndex);
					lastPoint=m_pointCache[lastIndex];
#else

					//* restart from last segment
//					subIndex=(subIndex>1)? subIndex-1: subIndex;

					//* restart from root
//					subIndex=oneDeadZone;

					//* only back to furthest parent that moved
					subIndex=subChanged;
					if(subIndex<oneDeadZone)
					{
						subIndex=oneDeadZone;
					}

					subIndex--;		//* loop is going to immediately increment

					FEASSERT(subIndex>=0);
					FEASSERT(subIndex<subCount);

					lastIndex=spInputVertices->integer(primitiveIndex,subIndex);
					lastPoint=m_pointCache[lastIndex];
					pivotPoint=spInputVertices->spatialVector(primitiveIndex,0);
					lastPivotPoint=pivotPoint;
					lastPivotSub=pivotSub;
#endif
					pass++;
					continue;
				}

				const I32 safeId=a_id>0? a_id: 0;
				if(I32(m_passLimitCount.size())<safeId+1)
				{
					m_passLimitCount.resize(safeId+1);
				}
				m_passLimitCount[safeId]++;
			}

			FEASSERT(magnitude(point)<1e12);

#if FE_DODGE_IMPACT_DEBUG
			feLog("pivotQuat %s\n",c_print(pivotQuat));
#endif
			lastIndex=pointIndex;
			lastPoint=m_pointCache[lastIndex];
			lastPivotPoint=pivotPoint;
			lastPivotSub=pivotSub;
		}

		delete[] shrinkScale;
	}

	delete[] repulsed;
}

sp<SurfaceI::ImpactI> DodgeOp::hitCheck(SpatialVector a_origin,
	SpatialVector a_direction,Real a_distance,Real a_radius,BWORD a_anyHit,
	sp<DrawI> a_spDebug,BWORD a_guard,sp<SurfaceI::ImpactI> a_spLastImpact)
{
	sp<SurfaceI::ImpactI> spImpact;

	if(a_guard)
	{
		SAFEGUARD;
		spImpact=m_spCollider->capsuleImpact(a_origin,a_direction,
				a_distance,a_radius,a_anyHit,a_spDebug,a_spLastImpact);
	}
	else
	{
		spImpact=m_spCollider->capsuleImpact(a_origin,a_direction,
				a_distance,a_radius,a_anyHit,a_spDebug,a_spLastImpact);
	}

	return spImpact;
}
