import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs[:]

    deplibs += [    "fexNetworkHostDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += ["fexSignalLib",
#                   "fexDataToolLib",
                    "fexNetworkDLLib"]

    tests = [   'xNetHost',
                'xNetHostClient',
                'xNetHostPlaster',
                'xNetHostRestart',
                'xNetHostServer',
                'xPureNetHostServer'
                ]

    for t in tests:
        exe = module.Exe(t)
        if forge.fe_os == "FE_LINUX":
            exe.linkmap = { "stdthread": "-lpthread" }
        forge.deps([t + "Exe"], deplibs)

    if forge.fe_os == "FE_LINUX":
        # compiled version of nethost_loader for link testing
        dll = module.DLL( "fexNetworkHostLoaderDL", ["nethost_loader"] )
        forge.deps( ["fexNetworkHostLoaderDLLib"], [] )

        clean_tests = [ 'xNetHostClientSymbolic' ]

        for t in clean_tests:
            exe = module.Exe(t)
            # link in nethost_loader redundantly to make sure no redefinitions
            # should work without any FE linkage
            forge.deps([t + "Exe"], ["fexNetworkHostLoaderDLLib"])

    forge.tests += [
        ("xNetHost.exe",        "",         None,   None),
        ("xNetHostRestart.exe", "",         None,   None),
        ("xNetHostRestart.exe", "inc",      None,   None),
        ("xNetHostRestart.exe", "keep",     None,   None),
        ("xNetHostRestart.exe", "keep inc", None,   None) ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        implEnetCat = "*.EnetCatalog"
        implNetCat = "*.NetworkCatalog"
        implShm = "*.SharedMemCatalog"
        implZero = "*.ZeroCatalog"
    else:
        implEnetCat = "'*.EnetCatalog'"
        implNetCat = "'*.NetworkCatalog'"
        implShm = "'*.SharedMemCatalog'"
        implZero = "'*.ZeroCatalog'"

#   if 'enet' in forge.modules_confirmed:
#       forge.tests += [
#           ("xNetHostServer.exe",      implEnetCat,    "",
#           ("xNetHostClient.exe",      implEnetCat,    "",     None)) ]

    if 'netsignal' in forge.modules_confirmed:
        forge.tests += [
            ("xNetHostServer.exe",          implNetCat,     "",
            ("xNetHostClient.exe",          implNetCat,     "",     None)) ]

        forge.tests += [
            ("xNetHostServer.exe",          implNetCat,     "",
            ("xNetHostClientSymbolic.exe",  implNetCat,     "",     None)) ]

    if 'shm' in forge.modules_confirmed:
        forge.tests += [
            ("xNetHostServer.exe",          implShm,        "",
            ("xNetHostClient.exe",          implShm,        "",     None)) ]

        forge.tests += [
            ("xNetHostServer.exe",          implShm,        "",
            ("xNetHostClientSymbolic.exe",  implShm,        "",     None)) ]

    if 'zeromq' in forge.modules_confirmed:
        forge.tests += [
            ("xNetHostServer.exe",          implZero,       "",
            ("xNetHostClient.exe",          implZero,       "",     None)) ]

        forge.tests += [
            ("xNetHostServer.exe",          implZero,       "",
            ("xNetHostClientSymbolic.exe",  implZero,       "",     None)) ]
