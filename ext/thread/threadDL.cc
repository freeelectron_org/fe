/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <thread/thread.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("feDataDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<WorkGang>("WorkForceI.WorkGang.fe");

#ifdef _OPENMP
	pLibrary->add<WorkOmp>("WorkForceI.WorkOmp.fe");
#endif

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
#ifdef _OPENMP
	//* prioritize OpenMP version over WorkGang version
	spLibrary->registry()->prioritize("WorkForceI.WorkOmp.fe",1);
#endif
}

}


