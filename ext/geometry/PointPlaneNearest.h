/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_PointPlaneNearest_h__
#define __geometry_PointPlaneNearest_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find point nearest to a circular solid

	@ingroup solve
*//***************************************************************************/
template <typename T>
class PointPlaneNearest
{
	public:
static	T	solve(const Vector<3,T>& center,const Vector<3,T>& facing,
					const T radius,const Vector<3,T>& origin,
					Vector<3,T>& direction);
};

template <typename T>
inline T PointPlaneNearest<T>::solve(const Vector<3,T>& center,
		const Vector<3,T>& facing,T radius,
		const Vector<3,T>& origin,Vector<3,T>& direction)
{
	const Vector<3,T> to_origin=origin-center;
	const T displacement=dot(facing,to_origin);
	direction= -facing;
	return displacement;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_PointPlaneNearest_h__ */



