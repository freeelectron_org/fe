/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __MPP_h__
#define __MPP_h__

// Multi-Player Protocol (MPP)
// Protocol of multi-Peer racing

namespace beacon
{

// Messages from client nodes to the Beacon
enum NodeMessageType
{
    registrationRequest = 0x40, // Registration message
    shutdown,                   // Shutdown message (unregister node and inform other nodes)
    keepAlive,                  // Message to tell the beacon that the node is still alive
    timeSyncRequest,            // Time sync request

    // Global calatalog messages
    getRequest,
    setRequest,
    unsetRequest,
	clearRequest,
	getRegexRequest
};

// Messages from the Beacon to the client nodes
enum BeaconMessageType
{
    nodeList = 0x80,
    currentTime,
    keepAliveResponse,

    // Global calatalog messages
    getResults,
    setResults,
    unsetResults,
    clearResults,
	getRegexResults
};

#pragma pack(push,1)
struct MPPmessageHeader
{
    uint8_t msgType;
    uint32_t msgLen;
};

#define REG_TYPE_SIZE   4

struct RegisterNodeMsg
{
    MPPmessageHeader header;
    uint16_t responsePort;          // Port to use for responses to request
    uint32_t CRC;
};

struct ShutdownSimMsg
{
    MPPmessageHeader header;
	uint32_t id;
    uint32_t CRC;
};

struct KeepAliveMsg
{
    MPPmessageHeader header;
    uint16_t responsePort;
	uint32_t id;
    uint32_t CRC;
};

struct KeepAliveResponseMsg
{
    MPPmessageHeader header;
	uint64_t globalDictionaryUpdateCounter;
    uint32_t CRC;
};

struct TimeSyncRequestMsg
{
    MPPmessageHeader header;
    uint16_t responsePort;
    int64_t clientTime;
    int64_t estBeaconTime;
    uint32_t CRC;
};

struct CurrentTimeMsg
{
    MPPmessageHeader header;
    int64_t clientTime;
    int64_t estBeaconTime;
    int64_t BeaconTime;
    uint32_t CRC;
};

struct GDSimpleRequestMsg
{
    MPPmessageHeader header;
    uint16_t responsePort;
	uint8_t id;
    uint32_t CRC;
};

struct GDRequestMsg
{
    MPPmessageHeader header;
    uint16_t responsePort;
	uint8_t id;
    char data[1]; // place holder for string data
};

struct GDResponseMsg
{
    MPPmessageHeader header;
    uint16_t responsePort;
	uint64_t updateCounter;
    char data[1]; // place holder for string data
};

struct GDSimpleResponseMsg
{
    MPPmessageHeader header;
    uint16_t result;
	uint64_t updateCounter;
    uint32_t CRC;
};

struct Node
{
	uint8_t id;
	uint32_t ipAddress;

    Node(const uint8_t _id, const uint32_t _ipdAddress)
    {
        id = _id;
        ipAddress = _ipdAddress;
    }
};

#define MAX_BEACON_NODES 200

struct NodeListMsg
{
    MPPmessageHeader header;
    uint8_t yourID;
	uint64_t globalDictionaryUpdateCounter;
    uint8_t numNodes;
    Node nodeList[MAX_BEACON_NODES];
    uint32_t CRC;
};

#pragma pack(pop)

} // namespace beacon

#endif // __MPP_h__
