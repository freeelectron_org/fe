/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceMRP_h__
#define __surface_SurfaceMRP_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Matrix Rational Power Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceMRP:
	public SurfaceTriangles,
	public CastableAs<SurfaceMRP>
{
	public:
							SurfaceMRP(void);
virtual						~SurfaceMRP(void)								{}

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							using SurfaceTriangles::sample;

							//* As SurfaceI
virtual SpatialTransform	sample(Vector2 a_uv) const;

virtual	void				draw2(sp<DrawI> a_spDrawI,const Color* color) const;

	private:

virtual	void				cache(void);

		U32								m_divisions;

		MatrixPower<SpatialTransform>	m_matrixPower;

		Array<SpatialTransform>			m_sourceTransformArray;
		Array<SpatialTransform>			m_sourceInverseArray;
		Array<SpatialTransform>			m_transformArray;

		Array<Real>						m_sourceWidthArray;
		Array<Real>						m_widthArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceMRP_h__ */
