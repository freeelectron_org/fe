/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_h__
#define __plugin_h__

#include "fe/core.h"

#ifdef MODULE_plugin
#define FE_PLUGIN_PORT FE_DL_EXPORT
#else
#define FE_PLUGIN_PORT FE_DL_IMPORT
#endif

namespace fe
{
class Component;
class Registry;
class Master;
}

#include "plugin/Library.h"
#include "plugin/Registry.h"
#include "plugin/Component.h"
#include "plugin/Catalog.h"
#include "plugin/CatalogBuffer.h"
#include "plugin/CatalogReaderI.h"
#include "plugin/CatalogReaderBase.h"
#include "plugin/CatalogWriterI.h"
#include "plugin/ManifestReaderI.h"
#include "plugin/Master.h"
#include "plugin/SingleMaster.h"
#include "plugin/StateCatalog.h"
#include "plugin/assertPlugin.h"

#endif /* __plugin_h__ */

