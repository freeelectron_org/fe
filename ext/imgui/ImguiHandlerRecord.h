/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __imgui_ImguiHandlerRecord_h__
#define __imgui_ImguiHandlerRecord_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Imgui Handler for generic RecordGroup

	@ingroup imgui
*//***************************************************************************/
class FE_DL_EXPORT ImguiHandlerRecord:
	public ImguiHandler,
	public Initialize<ImguiHandlerRecord>
{
	public:
				ImguiHandlerRecord(void);
virtual			~ImguiHandlerRecord(void);

		void	initialize(void);

virtual void	updateGUI(void)
				{
					update(m_spRecordGroup,"RecordGroup");

//					ImGui::ShowDemoWindow();
				}

virtual void	drawOverlay(void);

		void	bind(sp<RecordGroup> a_spRecordGroup)
				{	m_spRecordGroup=a_spRecordGroup; }

	private:

		void	update(sp<RecordGroup> a_spRecordGroup,String a_label);

		void	updateRecordGroup(sp<RecordGroup> a_spRecordGroup,
					String a_label);
		void	updateRecord(Record a_record,String a_label,I32 a_recordIndex);

		sp<RecordGroup>			m_spRecordGroup;
		sp<Oscilloscope>		m_spOscilloscope;

		sp<BaseType>			m_spRecordGroupType;
		sp<BaseType>			m_spRecordArrayType;
		sp<BaseType>			m_spRecordType;
		sp<BaseType>			m_spWeakRecordType;
		sp<BaseType>			m_spU8Type;
		sp<BaseType>			m_spU32Type;
		sp<BaseType>			m_spI32Type;
		sp<BaseType>			m_spRealType;
		sp<BaseType>			m_spF64Type;
		sp<BaseType>			m_spSpatialVectorType;
		sp<BaseType>			m_spSpatialEulerType;
		sp<BaseType>			m_spColorType;
		sp<BaseType>			m_spSpatialQuaternionType;
		sp<BaseType>			m_spSpatialTransformType;
		sp<BaseType>			m_spVoidType;

		I32						m_columnOffset;
		I32						m_scopeOffset;
		Record					m_selectedRecord;
		I32						m_selectedAttrIndex;
		Record					m_lastSelectedRecord;
		I32						m_lastSelectedAttrIndex;

		BWORD					m_drawn;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __imgui_ImguiHandlerRecord_h__ */
