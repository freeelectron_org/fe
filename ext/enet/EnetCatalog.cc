/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "enet/enetComs.pmh"

#define FE_ENET_VERBOSE			FALSE

namespace fe
{
namespace ext
{

EnetCatalog::EnetCatalog(void):
	m_pReceiverThread(nullptr),
	m_pEnetConnection(nullptr)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::EnetCatalog\n");
#endif

	m_pEnetConnection = new EnetConnection();
}

EnetCatalog::~EnetCatalog(void)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::~EnetCatalog\n");
#endif

//~	if(m_pEnetConnection != nullptr)
//~	{
//~		((EnetConnection *)m_pEnetConnection)->disconnect();
//~	}

	disconnect();
}

void EnetCatalog::initialize(void)
{
	m_receiverTask.m_pEnetCatalog = this;
}

Result EnetCatalog::connectAsServer(String a_address, U16 a_port)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::connectAsServer port %d\n", a_port);
#endif
	bool asServer = true;
	std::string address = "";

	if(!((EnetConnection *)m_pEnetConnection)->connect(asServer,
			address, a_port))
	{
		return e_refused;
	}

#if FE_ENET_VERBOSE
	feLog("EnetCatalog::connectAsServer creating thread\n");
#endif

	m_receiverTask.m_pEnetConnection = m_pEnetConnection;
	m_pReceiverThread = new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsServer(a_address,a_port);
}

Result EnetCatalog::connectAsClient(String a_address, U16 a_port)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::connectAsClient \"%s\" port %d\n",
			a_address.c_str(), a_port);
#endif

	bool asServer = false;
	if(!((EnetConnection *)m_pEnetConnection)->connect(asServer,
			std::string(a_address.c_str()), a_port))
	{
		return e_refused;
	}

#if FE_ENET_VERBOSE
	feLog("EnetCatalog::connectAsClient bound to %d\n", a_port);
#endif

	m_receiverTask.m_pEnetConnection = m_pEnetConnection;
	m_pReceiverThread = new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsClient(a_address,a_port);
}

void EnetCatalog::sendString(String a_text)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::sendString \"%s\"\n",a_text.c_str());
#endif

	sendBytes((U8*)a_text.c_str(),a_text.length()+1);
}

void EnetCatalog::sendBytes(const U8* a_pByteBlock,const I32 a_byteCount)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::sendBytes %d %p\n",a_byteCount,a_pByteBlock);
#endif

	if(!((EnetConnection *)m_pEnetConnection)->sendReliable(
			(const char *)a_pByteBlock, a_byteCount) )
	{
		feLog("EnetCatalog::sendBytes failed to init %d bytes of data\n",
				a_byteCount);
	}
}

void EnetCatalog::broadcastSelect(String a_name,String a_property,
	String a_message,
	I32 a_includeCount,const String* a_pIncludes,
	I32 a_excludeCount,const String* a_pExcludes,
	const U8* a_pRawBytes,I32 a_byteCount)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::broadcastSelect \"%s\" \"%s\"\n",
			a_name.c_str(),a_property.c_str());
#endif

	const BWORD removing=(a_message.empty() &&
			!cataloged(a_name,a_property));
	const BWORD isSignal=catalogOrDefault<bool>(a_name,"signal",false);
	const BWORD isTick=(a_name=="net:tick");

	//* NOTE a_message is a forced payload; m_message is the Enet MessageBuffer

	m_message.Reset();

	const BWORD isServer=(role() == "server");
	const I32 recipientCount=isServer? m_identityCount: 1;
	for(I32 recipientIndex=0;recipientIndex<recipientCount;recipientIndex++)
	{
		if(isServer)
		{
			Identity& rIdentity=m_identityArray[recipientIndex];

			if(a_includeCount && a_pIncludes)
			{
				I32 includeIndex=0;
				for(;includeIndex<a_includeCount;includeIndex++)
				{
					if(rIdentity==a_pIncludes[includeIndex])
					{
						break;
					}
				}
				if(includeIndex>=a_includeCount)
				{
					continue;
				}
			}
			if(a_excludeCount && a_pExcludes)
			{
				I32 excludeIndex=0;
				for(;excludeIndex<a_excludeCount;excludeIndex++)
				{
					if(rIdentity==a_pExcludes[excludeIndex])
					{
						break;
					}
				}
				if(excludeIndex<a_excludeCount)
				{
					continue;
				}
			}

			//sendBytes(rIdentity.m_data,FE_CNC_IDENTITY_BYTES);
			m_message.AddBytes(rIdentity.m_data, FE_CNC_IDENTITY_BYTES);
		}

		const String typeString=catalogTypeName(a_name,a_property);

		if(!isServer)
		{
			static U8 id[] = { '1', '2', '3', '4', '5' };
			//sendBytes(id, FE_CNC_IDENTITY_BYTES);
			m_message.AddBytes(id, FE_CNC_IDENTITY_BYTES);
		}
		//sendString("update");
		//sendString(a_name);
		//sendString(a_property);
		//sendString(typeString);

		if(isTick)
		{
			m_message.AddString("tick");
		}
		else if(removing)
		{
			m_message.AddString("remove");
			m_message.AddString(a_name);
			m_message.AddString(a_property);
		}
		else
		{
			m_message.AddString(isSignal? "signal": "update");
			m_message.AddString(a_name);
			m_message.AddString(a_property);

			if(!a_message.empty())
			{
				m_message.AddString("string");
				m_message.AddString(a_message);
			}
			else
			{
				m_message.AddString(typeString);

				if(typeString=="bytearray")
				{
					Array<U8>& rByteArray=
							catalog< Array<U8> >(a_name,a_property);
					//sendBytes(rByteArray.data(),rByteArray.size());
					m_message.AddArray(rByteArray);
#if FE_ENET_VERBOSE
					feLog("Sending array update: %s %s %s %d\n",
							a_name, a_property, typeString, rByteArray.size());
#endif
				}
				else
				{
					const String valueString = catalogValue(a_name,a_property);
					//sendString(valueString);
					m_message.AddString(valueString);
#if FE_ENET_VERBOSE
					feLog("Sending update: %s %s %s %s\n",
							a_name, a_property, typeString, valueString);
#endif
				}
			}
		}

		//sendString("EOM");
		m_message.AddString("EOM");

		sendBytes(m_message.Data(), m_message.Size());
	}

	if(isServer && !removing && a_name!="net:flushed")
	{
		//* TODO also properties other than "value"
		catalog<bool>(a_name,"broadcasted")=true;
	}

	heartbeat();
}

Result EnetCatalog::disconnect(void)
{
#if FE_ENET_VERBOSE
	feLog("EnetCatalog::disconnect\n");
#endif
	if(m_pEnetConnection == nullptr)
	{
		return e_notInitialized;
	}

	Result result=ConnectedCatalog::disconnect();
	if(fe::failure(result))
	{
		return result;
	}

	((EnetConnection *)m_pEnetConnection)->disconnect();

	m_receiverTask.m_terminate = TRUE;
	if(m_pReceiverThread->joinable())
	{
		m_pReceiverThread->join();
	}
	delete m_pReceiverThread;
	m_pReceiverThread = nullptr;

	return result;
}

I32 EnetCatalog::ReceiverTask::readBuffer(Array<U8>& a_pMsg)
{
	if(!((EnetConnection *)m_pEnetConnection)->receiveBlocking(
			(std::vector<char> &)a_pMsg))
	{
		return -1;
	}
	else
	{
#if FE_ENET_VERBOSE
		feLog("EnetCatalog::ReceiverTask::readBuffer \"%d\"\n", a_pMsg.size());
#endif
		return a_pMsg.size();
	}
}

I32 EnetCatalog::ReceiverTask::readString(String& a_rString)
{
	m_receiveMessage.GetString(a_rString);
	return a_rString.length()+1;

#if false
	Array<U8> msg;
	const I32 bytesRead = readBuffer(msg);

	if(bytesRead > 0)
	{
#if FE_ENET_VERBOSE
		feLog("EnetCatalog::ReceiverTask::readString \"%s\"\n",msg.data());
#endif
		a_rString=(char*)msg.data();
	}
	else
	{
		a_rString = "";
	}

	return bytesRead;
#endif
}

I32 EnetCatalog::ReceiverTask::readBytes(Array<U8>& a_rByteArray)
{
	const I32 bytesRead = m_receiveMessage.GetArray(a_rByteArray);
	return bytesRead;

#if FALSE
	Array<U8> msg;
	const I32 bytesRead = readBuffer(msg);

	if(bytesRead>0)
	{
		a_rByteArray.resize(bytesRead);
		memcpy(a_rByteArray.data(), msg.data(), bytesRead);
#if FE_ENET_VERBOSE
		feLog("EnetCatalog::ReceiverTask::readBytes \"%d\"\n", bytesRead);
#endif
	}
	else
	{
		a_rByteArray.resize(0);
	}

	return bytesRead;
#endif
}

I32 EnetCatalog::ReceiverTask::readIdentity(EnetCatalog::Identity& a_identity)
{
	const I32 bytesRead =
			m_receiveMessage.GetBytes(a_identity.m_data, FE_CNC_IDENTITY_BYTES);

	if(bytesRead != FE_CNC_IDENTITY_BYTES)
	{
		memset(a_identity.m_data, 0, FE_CNC_IDENTITY_BYTES);
	}

	return bytesRead;

#if FALSE
	Array<U8> msg;
	const I32 bytesRead = readBuffer(msg);


	if(bytesRead == FE_CNC_IDENTITY_BYTES)
	{
		memcpy(a_identity.m_data, (U8*)msg.data(), bytesRead);
#if FE_ENET_VERBOSE
		feLog("EnetCatalog::ReceiverTask::readIdentity \"%d\"\n", bytesRead);
#endif
	}
	else
	{
		memset(a_identity.m_data, 0, FE_CNC_IDENTITY_BYTES);
	}

	a_identity.m_text.sPrintf("0x%02x%02x%02x%02x%02x",
			a_identity.m_data[0],
			a_identity.m_data[1],
			a_identity.m_data[2],
			a_identity.m_data[3],
			a_identity.m_data[4]);

	return bytesRead;
#endif
}

void EnetCatalog::ReceiverTask::operate(void)
{
#if FE_ENET_VERBOSE
	BWORD wasConnected=FALSE;
#endif

	while(TRUE)
	{
#if FE_ENET_VERBOSE
		feLog("EnetCatalog::ReceiverTask::operate checking for message\n");
#endif

		if(!m_pEnetCatalog)
		{
			feLog("EnetCatalog::ReceiverTask::operate"
					" EnetCatalog disappeared\n");
			return;
		}

		if(!m_pEnetCatalog->started())
		{
			m_pEnetCatalog->yield();
			continue;
		}

		if(!m_pEnetCatalog->connected())
		{
			m_pEnetCatalog->timerRestart();
		}

		const BWORD isServer=(m_pEnetCatalog->role() == "server");

		if(!isServer && m_pEnetCatalog->connected() &&
				m_pEnetCatalog->timerReached())
		{
			m_pEnetCatalog->dropConnection();
			return;
		}

#if FE_ENET_VERBOSE
		const BWORD nowConnected=m_pEnetCatalog->connected();
		if(wasConnected && !nowConnected)
		{
			feLog("EnetCatalog::ReceiverTask::operate disconnect detected\n");
		}
		wasConnected=nowConnected;
#endif

		if(m_terminate)
		{
#if FE_ENET_VERBOSE
			feLog("EnetCatalog::ReceiverTask::operate terminate detected\n");
#endif
			return;
		}

		String source;
		m_receiveMessage.Reset();
		readBuffer(m_receiveMessage.DataArray());

		if(isServer)
		{
			Identity identity;
			I32 bytesRead=readIdentity(identity);
			if(bytesRead<0)
			{
				if(errno==EAGAIN)
				{
#if FE_ENET_VERBOSE
					feLog("EnetCatalog::ReceiverTask::operate no identity\n");
#endif
					m_pEnetCatalog->yield();
				}
				else
				{
					feLog("EnetCatalog::ReceiverTask::operate"
							" error reading identity\n");
				}
				continue;
			}
			if(bytesRead!=FE_CNC_IDENTITY_BYTES)
			{
#if FE_ENET_VERBOSE
				feLog("EnetCatalog::ReceiverTask::operate"
						" bad identity, bytes read %d\n", bytesRead );
#endif
				continue;
			}

			I32& rIdentityCount=m_pEnetCatalog->m_identityCount;
			Identity *const pIdentityArray=m_pEnetCatalog->m_identityArray;
			I32 identityIndex=0;
			for(;identityIndex<rIdentityCount;identityIndex++)
			{
				if(identity==pIdentityArray[identityIndex])
				{
					break;
				}
			}
			if(identityIndex>=rIdentityCount)
			{
				Identity& rNewIdentity=pIdentityArray[rIdentityCount];
				rNewIdentity=identity;

#if FE_ENET_VERBOSE
				feLog("EnetCatalog::ReceiverTask::operate"
						" new identity %d %s\n",
						rIdentityCount,rNewIdentity.m_text.c_str());
#endif
				rIdentityCount++;	//* atomic-ish
			}
			source=identity.m_text;
		}
		else
		{
			Identity identity;
//			I32 bytesRead=
					readIdentity(identity);
		}

		String command;
		I32 bytesRead=readString(command);
		if(bytesRead<0)
		{
			if(errno==EAGAIN)
			{
#if FE_ENET_VERBOSE
				feLog("EnetCatalog::ReceiverTask::operate no message\n");
#endif
				m_pEnetCatalog->yield();
			}
			else
			{
				feLog("EnetCatalog::ReceiverTask::operate"
						" error reading 'command'\n");
			}
			continue;
		}

		m_pEnetCatalog->timerRestart();

		const BWORD isSignal=(command=="signal");
		if(isSignal || command == "update")
		{
			String key;
			bytesRead = readString(key);

			String property;
			bytesRead = readString(property);

			String typeString;
			bytesRead = readString(typeString);

			if(typeString == "bytearray")
			{
				Array<U8> byteArray;
				bytesRead = readBytes(byteArray);

#if FE_ENET_VERBOSE
				feLog("Update bytearray: %s, %s %s, %d\n",
						key, property, typeString, byteArray.size());
#endif

				m_pEnetCatalog->update(isSignal? e_signal: e_update,source,
						key,property,typeString,"",
						byteArray.data(),byteArray.size());
			}
			else
			{
				String valueString;
				bytesRead = readString(valueString);

#if FE_ENET_VERBOSE
				feLog("Update: %s %s %s %s\n",
						key, property, typeString, valueString);
#endif

				m_pEnetCatalog->update(isSignal? e_signal: e_update,source,
						key,property,typeString,valueString);
			}

			String eom;
			bytesRead = readString(eom);
			if(bytesRead <= 0 || eom!="EOM")
			{
				feLog("EnetCatalog::ReceiverTask::operate no EOM\n");
			}
		}
		else if(command == "remove")
		{
			String key;
			bytesRead = readString(key);

			String property;
			bytesRead = readString(property);

			m_pEnetCatalog->update(e_remove,source,key,property,"","");

			String eom;
			bytesRead = readString(eom);
			if(bytesRead <= 0 || eom!="EOM")
			{
				feLog("EnetCatalog::ReceiverTask::operate no EOM\n");
			}
		}
		else if(command=="tick")
		{
//			feLog("TICK\n");

			String eom;
			bytesRead=readString(eom);
			if(bytesRead <= 0 || eom!="EOM")
			{
				feLog("EnetCatalog::ReceiverTask::operate no EOM\n");
			}
		}
		else
		{
			feLog("EnetCatalog::ReceiverTask::operate"
					" unknown command \"%s\"\n",command.c_str());
		}
	}
}

void EnetCatalog::MessageBuffer::AddString(const String &str)
{
	int len = str.length()+1;
	m_data.resize(m_currentOffset + len);
	memcpy((m_data.data() + m_currentOffset), str.c_str(), len);
	m_currentOffset += len;
}

void EnetCatalog::MessageBuffer::GetString(String &str)
{
	if(m_currentOffset >= I32(m_data.size()))
	{
		str = "";
	}
	else
	{
		str = (m_data.data() + m_currentOffset);
		m_currentOffset += str.length() + 1;
	}
}

void EnetCatalog::MessageBuffer::AddBytes(const U8 *data, const I32 size)
{
	m_data.resize(m_currentOffset + size);
	memcpy((m_data.data() + m_currentOffset), data, size);
	m_currentOffset += size;
}

void EnetCatalog::MessageBuffer::AddArray(const Array<U8> &data)
{
	I32 len = data.size();
	AddBytes((const U8 *)&len, sizeof(I32));

	m_data.resize(m_currentOffset + len);
	memcpy((m_data.data() + m_currentOffset), data.data(), len);
	m_currentOffset += len;
}

I32 EnetCatalog::MessageBuffer::GetArray(Array<U8> &data)
{
	I32 len = 0;
	if( GetBytes((U8 *)&len, sizeof(I32)) != sizeof(I32) )
	{
		feLog("EnetCatalog::MessageBuffer::GetArray"
				" error reading array size\n");
		return 0;
	}

	if( len <= 0 )
	{
		data.resize(0);
	}
	else
	{
		data.resize(len);
		memcpy(data.data(), (m_data.data() + m_currentOffset), len );
		m_currentOffset += len;
	}

	return len;
}

void EnetCatalog::MessageBuffer::Reset()
{
	m_currentOffset = 0;
	m_data.resize(0);
}

I32 EnetCatalog::MessageBuffer::GetBytes(U8 *data, const int size)
{
	int len = m_data.size() - m_currentOffset;

	if( len > size )
	{
		len = size;
	}

	if( len > 0 )
	{
		memcpy(data, (m_data.data() + m_currentOffset), len);
		m_currentOffset += len;
	}

	return len;
}

} /* namespace ext */
} /* namespace fe */

