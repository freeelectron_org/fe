/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Allocator_h__
#define __core_Allocator_h__

namespace fe
{

class Allocator : public Counted
{
	public:
virtual			~Allocator(void)											{}
virtual	void	*allocate(FE_UWORD byteCount)								=0;
virtual	void	deallocate(void* pMemory)									=0;

#if FE_COUNTED_TRACK
				Allocator(void): m_name("Allocator")						{}
const	String&	name(void) const	{ return m_name; }
	private:
		String	m_name;
#endif
};

class BasicAllocator : public Allocator
{
	public:
virtual	~BasicAllocator(void) {}
virtual	void	*allocate(FE_UWORD byteCount)
		{
			void *ptr = fe::allocate(byteCount);
//			feLog("BasicAllocator::allocate %p\n",ptr);
			return ptr;
		}
virtual	void	deallocate(void* pMemory)
		{
			fe::deallocate(pMemory);
		}
};

} /* namespace */

#endif /* __core_Allocator_h__ */

