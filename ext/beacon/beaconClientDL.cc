/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "beaconClient.pmh"
#include "platform/dlCore.cc"

using namespace fe;
using namespace ext;
using namespace beacon;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<BeaconClient>("BeaconClientI.BeaconClient.fe");
	pLibrary->add<BeaconClientStartup>(
			"BeaconClientStartupI.BeaconClientStartup.fe");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
