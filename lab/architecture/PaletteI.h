/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_PaletteI_h__
#define __architecture_PaletteI_h__
namespace fe
{
namespace ext
{

/** Palette */
class FE_DL_EXPORT PaletteI : virtual public Component
{
	public:
virtual	void			set(const String &a_name, sp<Component> spCmp)		= 0;
virtual	sp<Component>	get(const String &a_name)							= 0;
virtual	void			remove(const String &a_name)						= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_PaletteI_h__ */

