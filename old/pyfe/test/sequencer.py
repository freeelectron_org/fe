#!/usr/bin/python

import sys
import time

import fe
import fe.data
import fe.signal

class MyInitializer(fe.data.WatcherFS):
    def add(self, r_add):
        print 'could initialize record ' + str(r_add)
    def remove(self, r_remove):
        pass
    def clear(self):
        pass

# a python implemented signal handler
class MyHandler(fe.signal.HandlerFS, fe.data.WatcherFS):
    def handle(self,r_sig):
        time.sleep(0.1)
        try:
            print "MyHandler: ", r_sig.message
        except AttributeError, e:
            print "MyHandler: record w/o message"
    def bind(self, signaler, layout):
        layout.scope().addWatcher(MyInitializer())


def Test():
    # create a scope 
    scope = fe.data.Scope()

    # plug in signal system components
    fe.manage("fexSignalDL")

    # create a signaler from the plugin
    c_signaler = fe.signal.SignalerFS(fe.create("SignalerI"))

    # create a sequencer
    c_sobj = fe.create("SequencerI")
    c_sequencer = fe.signal.SequencerFS(c_sobj)

    # create a heartbeat signal (to push the PushSignaler)
    l_hb = scope.declare("heartbeat")
    r_hb = scope.createRecord(l_hb)

    # register sequencer for heartbeat signal
    c_signaler.insert(fe.signal.HandlerFS(c_sequencer.object()), l_hb)

    # set time to zero for easier output reading
    c_sequencer.setTime(0)

    # setup the pulse layout
    l_pulse = scope.declare("pulse")
    l_pulse.populate("message", "string");

    # have the sequencer finalize the pulse layout and give us the pulse signal
    r_pulse = c_sequencer.add(l_pulse, (c_sequencer.getCurrentTime()+500), 100)

    # tweak the pulse signal
    r_pulse._count_ = 8
    r_pulse._periodic_ = 1
    r_pulse.message = "hello"

    # create and insert my handler
    c_handler = MyHandler()
    c_signaler.insert(c_handler, l_pulse)

    scope.createRecord(l_hb)

    
    # push some heartbeats
    start_time = c_sequencer.getCurrentTime()
    while((c_sequencer.getCurrentTime() - start_time) < 2000):
        print c_sequencer.getCurrentTime()
        c_signaler.signal(r_hb)
        time.sleep(0.2)


    # dump
    c_signaler.dump()
    scope.dump()


if __name__ == "__main__" :
    try:
        Test()
        print 'success'
    except Exception, e:
        print e
        sys.exit(1)
    
    sys.exit(0)
