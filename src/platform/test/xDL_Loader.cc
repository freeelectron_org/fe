/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <typeinfo>
#include "fe/platform.h"

class MyClass
{
	int x;
};

int main(void)
{
	fe::UnitTest unitTest;

//	feLogDirect("%p %s\n",typeid(F32).name(),typeid(F32).name());

	fe::DL_Loader loader;
	BWORD success=loader.openDL("xDL_Library",TRUE);
	unitTest(success, "loader.openDL");

	success=loader.closeDL();
	unitTest(success, "loader.closeDL");

	return unitTest.failures();
}

