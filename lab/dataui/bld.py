import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "viewer" ]

def setup(module):

    srcList = [ "DrawButtons",
                "DrawAttributeLabels",
                "DrawStringEntry",
                "ButtonController",
                "DebugWindow",
                "StringEntryController",
                "AttributeController",
                "DataIOController",
                "ExistController",
                "RegionalDebug",
                "ScopeDebug",
                "RecordGroupDebug",
                "MaskMapDebug",
                "CatalogDebug",
                "RootLocate",
                "PaneLocate",
                "Manipulator",
                "datauiDL" ]

    dll = module.DLL( "fexDatauiDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexViewerLib",
                "fexWindowLib",
                "fexDataToolLib"
                ]

    forge.deps( ["fexDatauiDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexDatauiDL",                  None,       None) ]

    module.Module( 'test' )

