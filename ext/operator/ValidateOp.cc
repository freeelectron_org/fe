/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void ValidateOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("mode")="fail";
	catalog<String>("mode","choice:0")="warn";
	catalog<String>("mode","choice:1")="fail";
	catalog<String>("mode","choice:2")="replace";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Replacement Surface");
	catalog<bool>("Replacement Surface","optional")=true;
}

void ValidateOp::handle(Record& a_rSignal)
{
	catalog<String>("summary")="";

	const String mode=catalog<String>("mode");

	const BWORD modeWarn=(mode=="warn");
	const BWORD modeFail=(mode=="fail");
	const BWORD modeReplace=(mode=="replace");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessibleI> spReplaceAccessible;
	sp<SurfaceAccessorI> spReplacePoint;
	if(modeReplace)
	{
		access(spReplaceAccessible,"Replacement Surface",e_quiet);

		if(spReplaceAccessible.isValid())
		{
			access(spReplacePoint,spReplaceAccessible,
					e_point,e_position,e_quiet);
		}
	}

	catalog<String>("summary")=mode;

	const I32 pointCount=spOutputPoint->count();
	const I32 replaceCount=spReplacePoint.isValid()? spReplacePoint->count(): 0;

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const SpatialVector point=spOutputPoint->spatialVector(pointIndex);

		if(checkValid(point))
		{
			continue;
		}

		feLog("ValidateOp::handle point %d/%d position %s\n",
				pointIndex,pointCount,c_print(point));

		if(modeFail)
		{
			confirmValid(point);	//* should cause exception
			break;
		}

		if(modeWarn)
		{
			String text;
			text.sPrintf("invalid point at index %d (%s);",
					pointIndex,c_print(point));

			catalog<String>("warning")+=text;
		}
		else
		{
			SpatialVector replacement(0.0,0.0,0.0);

			if(pointIndex<replaceCount)
			{
				replacement=spReplacePoint->spatialVector(pointIndex);
			}

			feLog("  replace with %s\n",c_print(replacement));

			spOutputPoint->set(pointIndex,replacement);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
