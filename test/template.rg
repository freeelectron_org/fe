
# save recipe or Record in Scope
TEMPLATE Sphere ""
	spc:at "0 0 0"
	bnd:radius 0.5
	bnd:picked 0

# will pre-run extra recipe or overlay onto copied Record
TEMPLATE Cylinder Sphere
	bnd:span "0 0 1"
	bnd:baseRadius 0.5
	bnd:endRadius 0.5

# finds existing recipe/Record and run/clone
RECORD my_Sphere Sphere

# levels?
TEMPLATE Sphere(100) ""
	bnd:radius 2

# non-Layout specialization
TEMPLATE high_sphere Sphere
	spc:at "0 10 0"

# finds shallowest Layout and instantiate
RECORD my_high_sphere high_sphere


# Perhaps, store recipe and create/cache clonable Record on first instance
# and recreate on any instance after any new data dirties the template.

# Recipe:
#	1. Ordered std::vector of parent recipes
#	2. std::vector of String[2] for attributes

# Recipes happen after optional RecordFactory applies defaults.
# Dependence on template values would have to be /after/ initialize().
