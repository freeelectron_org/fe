import sys
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "group.pmh",
                "Projector",
                "Insert",
                "groupDL" ]

    dll = module.DLL( "fexGroupDL", srcList )

    forge.deps( ["fexGroupDLLib"], forge.corelibs )

    forge.tests += [
        ("inspect.exe",     "fexGroupDL",                   None,       None) ]

    #module.Module( 'test' )
