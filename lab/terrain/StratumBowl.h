/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumBowl_h__
#define __terrain_StratumBowl_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Elevation of a pit

	@ingroup terrain

	z=radius^2
*//***************************************************************************/
class FE_DL_EXPORT StratumBowl: public StratumPlane
{
	public:
						StratumBowl(void)									{}
virtual					~StratumBowl(void)									{}

						//* As StratumI
virtual Real			height(Real x,Real y) const;
virtual SpatialVector	normal(Real x,Real y) const;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumBowl_h__ */
