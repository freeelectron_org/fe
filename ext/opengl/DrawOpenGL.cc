/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opengl/opengl.pmh>

namespace fe
{
namespace ext
{

#define DGL_VERTEXARRAY				TRUE	// allow render calls in group form
#define DGL_LINEWIDTH				TRUE	// allow custom linewidth
#define DGL_ANTIALIAS_LINE			TRUE	// allow antialiased lines
#define DGL_POINT_ON_LINE			FALSE	// make sure short lines show up
#define DGL_THREAD_LOCKING			TRUE	// lock the event context

#define DGL_CHECK_ERROR		(FE_CODEGEN<=FE_DEBUG)	// check glGetError

		// assert if alpha<=0.0
#define	DGL_CHECK_ALPHA		FALSE //(FE_CODEGEN<=FE_DEBUG)

#if FE_DOUBLE_REAL
#define	DGL_REAL			GL_DOUBLE
#else
#define	DGL_REAL			GL_FLOAT
#endif

DrawOpenGL::Texture::Texture(void):
	m_id(0),
	m_width(0),
	m_height(0),
	m_type(0),
	m_pData(NULL)
{
	glGenTextures(1,&m_id);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::Texture::Texture glGenTextures");
#endif
}

DrawOpenGL::Texture::~Texture(void)
{
	glDeleteTextures(1,&m_id);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::Texture::~Texture glDeleteTextures");
#endif

	m_id=0;
}

DrawOpenGL::DrawOpenGL(void):
	m_currentTextureId(0),
	m_multiplication(0.0),
	m_isDirect(TRUE),
	m_lockThreads(TRUE)
{

#if FE_OS==FE_LINUX
	Display *pDisplay=glXGetCurrentDisplay();
	GLXContext glxContext=glXGetCurrentContext();
	m_isDirect=glXIsDirect(pDisplay,glxContext);
//	feLog("pDisplay=%p glxContext=%p direct=%d\n",
//			pDisplay,glxContext,m_isDirect);
#endif

	if(!m_isDirect)
	{
		feLog("DrawOpenGL:DrawOpenGL indirect context detected\n");
	}

	fe::String value;
	if(!fe::System::getEnvironmentVariable("FE_GL_LOCKING", value) ||
			!atoi(value.c_str()))
	{
		m_lockThreads=FALSE;
	}

	m_pQuadric=gluNewQuadric();

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::DrawOpenGL gluNewQuadric");
#endif
}

DrawOpenGL::~DrawOpenGL()
{
	gluDeleteQuadric(m_pQuadric);

	if(m_spFont.isValid() && m_spFont->count()==1)
	{
		m_spFont=NULL;
	}
}

void DrawOpenGL::initialize(void)
{
#if FE_2DGL==FE_2D_X_GFX
	//* HACK
	sp<Catalog> spMasterCatalog=registry()->master()->catalog();
	void* pVoidDisplay=
			spMasterCatalog->catalogOrDefault<void*>("xDisplay",NULL);
	void* pVoidGlxContext=
			spMasterCatalog->catalogOrDefault<void*>("glxContext",NULL);
	if(pVoidDisplay==NULL)
	{
		pVoidDisplay=(void*)(glXGetCurrentDisplay());
		spMasterCatalog->catalog<void*>("xDisplay")=pVoidDisplay;
	}
	if(pVoidGlxContext==NULL)
	{
		pVoidGlxContext=(void*)(glXGetCurrentContext());
		spMasterCatalog->catalog<void*>("glxContext")=pVoidGlxContext;
	}
#endif

	const char* shading=(const char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
	feLog("DrawOpenGL::initialize GLSL version string \"%s\"\n",shading);

	BWORD found_context=FALSE;
#if FE_2DGL==FE_2D_X_GFX	// Linux or X11-mode OSX
	found_context=(glXGetCurrentContext()!=NULL);
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	found_context=(wglGetCurrentContext()!=NULL);
#endif

	if(!found_context)
	{
		feX(e_cannotCreate,"DrawOpenGL::initialize()","no GL context");
		return;
	}

	glEnable(GL_DEPTH_TEST);

	createDefaultTexture();
	setupLights();

#ifdef FE_USE_OPENCL
	feLog("DrawOpenGL::initialize confirm OpenCL with GLX\n");

	const BWORD allowGLX=TRUE;
	sp<Master> spMaster=registry()->master();
	OpenCLContext::confirm(spMaster,allowGLX);
#endif
}

void DrawOpenGL::assureFont(void)
{
	if(!m_spFont.isValid())
	{
		m_spFont=registry()->create("FontI");
	}
}

Real DrawOpenGL::multiplication(void)
{
	if(m_multiplication<=Real(0))
	{
		const Real dpi=fe::compute_dpi();
		m_multiplication=fe::determine_multiplication(dpi);
	}

	return m_multiplication;
}

void DrawOpenGL::pushMatrixState(void)
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
}

void DrawOpenGL::popMatrixState(void)
{
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glPopAttrib();
}

void DrawOpenGL::pushMatrix(MatrixMode a_mode,Real a_values[16])
{
	switch(a_mode)
	{
		case DrawI::e_modelview:
			glMatrixMode(GL_MODELVIEW);
			break;
		case DrawI::e_projection:
			glMatrixMode(GL_PROJECTION);
			break;
		case DrawI::e_texture:
			glMatrixMode(GL_TEXTURE);
			break;
		case DrawI::e_color:
			glMatrixMode(GL_COLOR);
			break;
		default:
			return;
	}

	glPushMatrix();

#if FE_DOUBLE_REAL
	glLoadMatrixd(a_values);
#else
	glLoadMatrixf(a_values);
#endif
}

void DrawOpenGL::popMatrix(MatrixMode a_mode)
{
	switch(a_mode)
	{
		case DrawI::e_modelview:
			glMatrixMode(GL_MODELVIEW);
			break;
		case DrawI::e_projection:
			glMatrixMode(GL_PROJECTION);
			break;
		case DrawI::e_texture:
			glMatrixMode(GL_TEXTURE);
			break;
		case DrawI::e_color:
			glMatrixMode(GL_COLOR);
			break;
		default:
			return;
	}

	glPopMatrix();
}

void DrawOpenGL::unbindVertexArray(void)
{
	glBindVertexArray(0);
}

void DrawOpenGL::setBrightness(Real a_brightness)
{
	DrawCommon::setBrightness(a_brightness);
	setupLights();
}

void DrawOpenGL::setContrast(Real a_contrast)
{
	DrawCommon::setContrast(a_contrast);
	setupLights();
}

void DrawOpenGL::createDefaultView(void)
{
	m_spDefaultView=registry()->create("ViewI.ViewOpenGL");
	m_spDefaultView->setName("DrawOpenGL default "+m_spDefaultView->name());
#if FALSE
	if(!m_spDefaultView.isValid())
	{
		feX(e_cannotCreate,"DrawOpenGL::initialize",
				"failed to create a ViewI.ViewOpenGL");
	}
#endif
}

void DrawOpenGL::threadLock(void)
{
	sp<WindowI> spWindowI=view()->window();
	if(spWindowI.isValid())
	{
		sp<EventContextI> spEventContextI=spWindowI->getEventContextI();
		if(spEventContextI.isValid())
		{
			spEventContextI->threadLock();
		}
	}
}

void DrawOpenGL::threadUnlock(void)
{
	sp<WindowI> spWindowI=view()->window();
	if(spWindowI.isValid())
	{
		sp<EventContextI> spEventContextI=spWindowI->getEventContextI();
		if(spEventContextI.isValid())
		{
			spEventContextI->threadUnlock();
		}
	}
}

void DrawOpenGL::flush(void)
{
	sp<WindowI> spWindowI=view()->window();
	if(spWindowI.isValid())
	{
		spWindowI->makeCurrent();
		spWindowI->swapBuffers();
		spWindowI->clear();
	}
}

void DrawOpenGL::checkForError(String a_location)
{
	GLenum error=glGetError();
	if(error!=GL_NO_ERROR)
	{
		String errorString="<unknown>";

		switch(error)
		{
			case GL_NO_ERROR:
				errorString="GL_NO_ERROR";
				break;
			case GL_INVALID_ENUM:
				errorString="GL_INVALID_ENUM";
				break;
			case GL_INVALID_VALUE:
				errorString="GL_INVALID_VALUE";
				break;
			case GL_INVALID_OPERATION:
				errorString="GL_INVALID_OPERATION";
				break;
			case GL_STACK_OVERFLOW:
				errorString="GL_STACK_OVERFLOW";
				break;
			case GL_STACK_UNDERFLOW:
				errorString="GL_STACK_UNDERFLOW";
				break;
			case GL_OUT_OF_MEMORY:
				errorString="GL_OUT_OF_MEMORY";
				break;
			case GL_TABLE_TOO_LARGE:
				errorString="GL_TABLE_TOO_LARGE";
				break;
		}
		feX("DrawOpenGL::checkForError","OpenGL reported %s at '%s'",
				errorString.c_str(),a_location.c_str());
	}
}

void DrawOpenGL::setDrawMode(sp<DrawMode> spMode)
{
	SAFEGUARD;
	DrawCommon::setDrawMode(spMode);

	const BWORD texture=(drawStyle()==DrawMode::e_solid);

	setup(drawStyle(),isLit(),twoSidedLighting(),texture,
			frontfaceCulling(),backfaceCulling(),zBuffering());
	glPointSize(pointSize());
	bindTexture(textureImage(),textureImageID());
}

void DrawOpenGL::pushAntialiasPoints(void)
{
	glPushAttrib(GL_COLOR_BUFFER_BIT |
//			GL_DEPTH_BUFFER_BIT |
			GL_ENABLE_BIT | GL_HINT_BIT | GL_LIGHTING_BIT);

//	glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

	glEnable(GL_POINT_SMOOTH);
	glShadeModel(GL_FLAT);
//	glDepthMask(GL_FALSE);

//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void DrawOpenGL::pushAntialiasLines(void)
{
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
			GL_ENABLE_BIT | GL_HINT_BIT | GL_LIGHTING_BIT);

//	glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	glEnable(GL_LINE_SMOOTH);
	glShadeModel(GL_FLAT);
	glDepthMask(GL_FALSE);

	activateBlending();
}

void DrawOpenGL::pushBlending(void)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_DEPTH_BUFFER_BIT);

//	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
//	glEnable(GL_POLYGON_SMOOTH);

	activateBlending();
}

void DrawOpenGL::activateBlending(void)
{
	glEnable(GL_BLEND);
	if(drawStyle()==DrawMode::e_ghost)
	{
		glDepthMask(GL_FALSE);

		const Real forcedAlpha=0.4;	//* TODO param
		glBlendColor(1.0,1.0,1.0,forcedAlpha);

		//* keep fraction of existing color
//		glBlendFunc(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);

		//* purely cumulative
		glBlendFunc(GL_CONSTANT_ALPHA, GL_ONE);
	}
	else
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
}

void DrawOpenGL::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
//~	if(!vertices && spDrawBuffer.isValid())
//~	{
//~		sp<Catalog> spCatalog(spDrawBuffer);
//~		if(spCatalog.isValid())
//~		{
//~			vertices=spCatalog->catalog<I32>("cl:pointCount");
//~		}
//~	}

//	FEASSERT(vertex);
	FEASSERT(vertices>0);

//	feLog("DrawOpenGL::drawPoints %p vertices %d buffer %d vbo %d\n",
//			vertex,vertices,
//			spDrawBuffer.isValid(),
//			spDrawBuffer.isValid()? sp<Buffer>(spDrawBuffer)->m_vboVertex: -1);

	const BWORD lit=isLit();

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

	const BWORD normals=normal && lit;
	const BWORD toggle_lighting=!normal && lit;
	if(toggle_lighting)
	{
		glDisable(GL_LIGHTING);
	}

	if(antialias())
	{
		pushAntialiasPoints();
	}

	Real alpha=color? color[0][3]: 1.0f;
	const BWORD blending=(multicolor || alpha!=1.0f);
	if(blending && drawStyle()!=DrawMode::e_foreshadow)
	{
		pushBlending();

		//* NOTE presumptive
		glDepthMask(GL_FALSE);
	}

	const U32 passes=1+(drawStyle()==DrawMode::e_foreshadow);
	for(U32 pass=0;pass<passes;pass++)
	{
		if(pass)
		{
			if(blending)
			{
				pushBlending();
				glDepthMask(GL_FALSE);
			}

			setup(drawStyle(),lit,twoSidedLighting(),FALSE,
					frontfaceCulling(),backfaceCulling(),FALSE);
		}

#if DGL_VERTEXARRAY
//~		if (multicolor && color)
//~		{

//			glVertexPointer(3,DGL_REAL,sizeof(SpatialVector),vertex);
//			glEnableClientState(GL_VERTEX_ARRAY);
//
//			glColorPointer(4,DGL_REAL,sizeof(Color),color);
//			glEnableClientState(GL_COLOR_ARRAY);
//
//			if(normals)
//			{
//				glNormalPointer(DGL_REAL,sizeof(SpatialVector),normal);
//				glEnableClientState(GL_NORMAL_ARRAY);
//			}

			beginArrays(vertex,normal,NULL,NULL,vertices,
					multicolor,color,spDrawBuffer,normals,blending);

			glDrawArrays(GL_POINTS,0,vertices);

			endArrays(NULL,NULL,multicolor,color,spDrawBuffer,normals);

//			if(normals)
//			{
//				glDisableClientState(GL_NORMAL_ARRAY);
//			}
//
//			glDisableClientState(GL_COLOR_ARRAY);
//			glDisableClientState(GL_VERTEX_ARRAY);

//~		}
//~		else
//~		{
//~			if (color)
//~			{
//~#if FE_DOUBLE_REAL
//~				glColor4dv(color[0].temp());
//~#else
//~				//* TODO clean up and change all cases
//~				if(passes>1 && !pass)
//~				{
//~					Color recolor=color[0];
//~					recolor[3]=1.0;
//~					glColor4fv(recolor.temp());
//~				}
//~				else
//~				{
//~					glColor4fv(color[0].temp());
//~				}
//~#endif
//~			}
//~
//~			glVertexPointer(3,DGL_REAL,sizeof(SpatialVector),vertex);
//~			glEnableClientState(GL_VERTEX_ARRAY);
//~
//~			if(normals)
//~			{
//~				glNormalPointer(DGL_REAL,sizeof(SpatialVector),normal);
//~				glEnableClientState(GL_NORMAL_ARRAY);
//~			}
//~
//~			glDrawArrays(GL_POINTS,0,vertices);
//~
//~			if(normals)
//~			{
//~				glDisableClientState(GL_NORMAL_ARRAY);
//~			}
//~
//~			glDisableClientState(GL_VERTEX_ARRAY);
//~		}
#else
		glBegin(GL_POINTS);

		if(normals)
		{
			U32 m;
			if(multicolor && color)
			{
				for(m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[m].temp());
					glNormal3dv(normal[m].temp());
					glVertex3dv(vertex[m].temp());
#else
					glColor4fv(color[m].temp());
					glNormal3fv(normal[m].temp());
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
			else
			{
				if (color)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[0].temp());
#else
					glColor4fv(color[0].temp());
#endif
				}
				for (m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
						glNormal3dv(normal[m].temp());
						glVertex3dv(vertex[m].temp());
#else
						glNormal3fv(normal[m].temp());
						glVertex3fv(vertex[m].temp());
#endif
				}
			}
		}
		else
		{
			U32 m;
			if(multicolor && color)
			{
				for(m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[m].temp());
					glVertex3dv(vertex[m].temp());
#else
					glColor4fv(color[m].temp());
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
			else
			{
				if (color)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[0].temp());
#else
					glColor4fv(color[0].temp());
#endif
				}
				for (m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glVertex3dv(vertex[m].temp());
#else
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
		}

		glEnd();
#endif
	}

	if(blending)
	{
		glPopAttrib();
	}

	if(toggle_lighting)
	{
		glEnable(GL_LIGHTING);
	}

	//* restore from point smooth
	if(antialias())
	{
		glPopAttrib();
	}

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawPoints complete");
#endif

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawLines(const SpatialVector* vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	BWORD multiradius,const Real* radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	const Vector2* texture=NULL;
	drawLinesInternal(vertex,normal,texture,vertices,strip,
			multicolor,color,multiradius,radius,
			element,elementCount,spDrawBuffer);
}

void DrawOpenGL::drawLinesInternal(const SpatialVector* vertex,
	const SpatialVector* normal,const Vector2* texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	BWORD multiradius,const Real* radius,
	const Vector3i* element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	if(drawStyle()==DrawMode::e_pointCloud)
	{
		drawPoints(vertex,NULL,vertices,multicolor,color,spDrawBuffer);
		return;
	}

	FEASSERT(vertex);
	FEASSERT(vertices > 1);
	FEASSERT(strip != e_fan);

#if DGL_CHECK_ALPHA
	if(!multicolor && color && (*color)[3]<=0.0f)
	{
		feX("DrawOpenGL::drawLinesInternal",
				"alpha<=0 is pointless\n  check your color: %s\n",
				c_print(*color));
	}
#endif

	const BWORD lit=isLit();

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

#if FALSE
	feLog("drawLines %d normal %p lit %d aa %d width %.6G"
			" multicolor %d color %p\n"
			" strip %d lock %d window %d camera %d"
			" element %p elementCount %d\n",
			vertices,normal,lit,antialias(),lineWidth(),
			multicolor,color,strip,m_lockThreads,
			view()->window().isValid(),
			view()->camera().isValid(),
			element,elementCount);
	feLog("  %s\n",c_print(drawMode()));
#endif

	const BWORD normals=normal && lit;
	const BWORD toggle_lighting=!normal && lit;
	if(toggle_lighting)
	{
		glDisable(GL_LIGHTING);
	}

#if DGL_ANTIALIAS_LINE
	if(antialias())
		pushAntialiasLines();
#endif

#if DGL_LINEWIDTH
	const BWORD pushLineWidth=(lineWidth()!=1.0f);
	if(pushLineWidth)
	{
		glPushAttrib(GL_LINE_BIT);
		glLineWidth(lineWidth());
	}
#endif

	Real alpha=color? color[0][3]: 1.0f;
	const BWORD blending=(multicolor || alpha!=1.0f ||
			drawStyle()==DrawMode::e_ghost);
	if(blending && drawStyle()!=DrawMode::e_foreshadow)
	{
		pushBlending();
	}

	if(strip==e_triple)
	{
		setup(DrawMode::e_outline,FALSE,FALSE,FALSE,
				FALSE,FALSE,zBuffering());

		glEnable(GL_POLYGON_OFFSET_LINE);
		glPolygonOffset(-1.0f,-1.0f);
	}

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawLinesInternal prep");
#endif

	const U32 passes=1+(drawStyle()==DrawMode::e_foreshadow);
	for(U32 pass=0;pass<passes;pass++)
	{
		if(pass)
		{
			if(blending)
			{
				pushBlending();
			}

			setup(drawStyle(),lit,twoSidedLighting(),FALSE,
					frontfaceCulling(),backfaceCulling(),FALSE);

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::drawLinesInternal re-prep");
#endif
		}

#if DGL_VERTEXARRAY
//~		if (multicolor && color)
//~		{
//			glVertexPointer(3,DGL_REAL,sizeof(SpatialVector),vertex);
//			glEnableClientState(GL_VERTEX_ARRAY);
//
//			glColorPointer(4,DGL_REAL,sizeof(Color),color);
//			glEnableClientState(GL_COLOR_ARRAY);
//
//			if(normals)
//			{
//				glNormalPointer(DGL_REAL,sizeof(SpatialVector),normal);
//				glEnableClientState(GL_NORMAL_ARRAY);
//			}

			beginArrays(vertex,normal,NULL,texture,vertices,
					multicolor,color,spDrawBuffer,normals,blending);

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::drawLinesInternal array begin");
#endif

			if(strip==e_triple)
			{
#if FALSE
				for(GLint first=0;first<GLint(vertices-2);first+=3)
				{
					glDrawArrays(GL_TRIANGLES,first,3);
				}
#else
				glDrawArrays(GL_TRIANGLES,0,vertices);
#endif

#if DGL_CHECK_ERROR
				checkForError("DrawOpenGL::drawLinesInternal glDrawArrays");
#endif
			}
			else if(element && elementCount)
			{
				GLint* first=new GLint[elementCount];
				GLsizei* count=new GLsizei[elementCount];

				for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					const Vector2i& rElement=element[elementIndex];
					first[elementIndex]=rElement[0];
					count[elementIndex]=rElement[1];
				}

				if(glMultiDrawArrays)
				{
					glMultiDrawArrays(GL_LINE_STRIP,first,count,elementCount);
				}
				else
				{
					//* old OpenGL (or a GLEW problem)
					for(U32 elementIndex=0;elementIndex<elementCount;
							elementIndex++)
					{
						glDrawArrays(GL_LINE_STRIP,
								first[elementIndex],count[elementIndex]);
					}
				}

#if DGL_CHECK_ERROR
				checkForError(
						"DrawOpenGL::drawLinesInternal glMultiDrawArrays");
#endif

				delete[] first;
				delete[] count;
			}
			else
			{
				glDrawArrays(strip==e_strip? GL_LINE_STRIP: GL_LINES,
						0,vertices);

#if DGL_CHECK_ERROR
				checkForError("DrawOpenGL::drawLinesInternal glDrawArrays");
#endif
			}

			endArrays(NULL,texture,multicolor,color,spDrawBuffer,normals);

//			if(normals)
//			{
//				glDisableClientState(GL_NORMAL_ARRAY);
//			}
//			glDisableClientState(GL_COLOR_ARRAY);
//			glDisableClientState(GL_VERTEX_ARRAY);
//~		}
//~		else
//~		{
//~			if(color)
//~			{
//~#if FE_DOUBLE_REAL
//~				glColor4dv(color[0].temp());
//~#else
//~				//* TODO clean up and change all cases
//~				if(passes>1 && !pass)
//~				{
//~					Color recolor=color[0];
//~					recolor[3]=1.0;
//~					glColor4fv(recolor.temp());
//~				}
//~				else
//~				{
//~					glColor4fv(color[0].temp());
//~				}
//~#endif
//~			}
//~
//~			glVertexPointer(3,DGL_REAL,sizeof(SpatialVector),vertex);
//~			glEnableClientState(GL_VERTEX_ARRAY);
//~
//~			if(normals)
//~			{
//~				glNormalPointer(DGL_REAL,sizeof(SpatialVector),normal);
//~				glEnableClientState(GL_NORMAL_ARRAY);
//~			}
//~
//~			if(strip==e_triple)
//~			{
//~				for(GLint first=0;first<GLint(vertices-2);first+=3)
//~				{
//~					glDrawArrays(GL_TRIANGLES,first,3);
//~				}
//~			}
//~			else
//~			{
//~				glDrawArrays(strip==e_strip? GL_LINE_STRIP: GL_LINES,
//~						0,vertices);
//~			}
//~
//~			if(normals)
//~			{
//~				glDisableClientState(GL_NORMAL_ARRAY);
//~			}
//~			glDisableClientState(GL_VERTEX_ARRAY);
//~		}
#else
		//* TODO e_triple

		glBegin(strip==e_strip? GL_LINE_STRIP: GL_LINES);

		U32 m;

		if(normals)
		{
			if (multicolor && color)
			{
				for (m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[m].temp());
					glNormal3dv(normal[m].temp());
					glVertex3dv(vertex[m].temp());
#else
					glColor4fv(color[m].temp());
					glNormal3fv(normal[m].temp());
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
			else
			{
				if (color)
#if FE_DOUBLE_REAL
					glColor4dv(color[0].temp());
#else
					glColor4fv(color[0].temp());
#endif
				for (m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glNormal3dv(normal[m].temp());
					glVertex3dv(vertex[m].temp());
#else
					glNormal3fv(normal[m].temp());
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
		}
		else
		{
			if (multicolor && color)
			{
				for (m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[m].temp());
					glVertex3dv(vertex[m].temp());
#else
					glColor4fv(color[m].temp());
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
			else
			{
				if (color)
#if FE_DOUBLE_REAL
					glColor4dv(color[0].temp());
#else
					glColor4fv(color[0].temp());
#endif
				for (m=0; m < vertices; m++)
#if FE_DOUBLE_REAL
					glVertex3dv(vertex[m].temp());
#else
					glVertex3fv(vertex[m].temp());
#endif
			}
		}

		glEnd();
#endif

#if DGL_POINT_ON_LINE
		// make sure it shows up when only a point
#if FE_DOUBLE_REAL
		glColor4dv(color[0].temp());
#else
		glColor4fv(color[0].temp());
#endif
		glBegin(GL_POINTS);
#if FE_DOUBLE_REAL
		glVertex3dv(vertex[0].temp());
#else
		glVertex3fv(vertex[0].temp());
#endif
		glEnd();
#endif
	}

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawLinesInternal draw");
#endif

	if(strip==e_triple)
	{
		setup(drawStyle(),lit,twoSidedLighting(),FALSE,
				frontfaceCulling(),backfaceCulling(),zBuffering());
		glDisable(GL_POLYGON_OFFSET_LINE);
	}

	if(blending)
	{
		glPopAttrib();
	}

#if DGL_LINEWIDTH
	if(pushLineWidth)
	{
		//* restore line width
		glPopAttrib();
	}
#endif

#if DGL_ANTIALIAS_LINE
	if(antialias())
		glPopAttrib();
#endif

	if(toggle_lighting)
	{
		glEnable(GL_LIGHTING);
	}

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawLinesInternal complete");
#endif

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawTriangles(const SpatialVector* vertex,
	const SpatialVector* normal,const Vector2* texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	if(drawStyle()==DrawMode::e_wireframe ||
			drawStyle()==DrawMode::e_pointCloud)
	{
		if(vertexMap)
		{
			//* TODO vertexMap strip and fan
			if(strip==e_strip)
			{
				feLog("DrawOpenGL::drawTriangles"
						" vertex mapped strip not properly implemented\n");
			}
			else if(strip==e_fan)
			{
				feLog("DrawOpenGL::drawTriangles"
						" vertex mapped fan not properly implemented\n");
			}

			const I32 triCount=vertexMap->size()/3;
			const U32 vertCount=triCount*3;

			SpatialVector* tempVertex=new SpatialVector[vertCount];
			SpatialVector* tempNormal=new SpatialVector[vertCount];
			Vector2* tempTexture=new Vector2[vertCount];
			Color* tempColor=new Color[vertCount];

			for(I32 triIndex=0;triIndex<triCount;triIndex++)
			{
				const I32 mapIndex0=triIndex*3;
				const I32 mapIndex1=mapIndex0+1;
				const I32 mapIndex2=mapIndex0+2;
				const I32 mappedIndex0=(*vertexMap)[mapIndex0];
				const I32 mappedIndex1=(*vertexMap)[mapIndex1];
				const I32 mappedIndex2=(*vertexMap)[mapIndex2];

				tempVertex[mapIndex0]=vertex[mappedIndex0];
				tempVertex[mapIndex1]=vertex[mappedIndex1];
				tempVertex[mapIndex2]=vertex[mappedIndex2];

				if(normal)
				{
					tempNormal[mapIndex0]=normal[mappedIndex0];
					tempNormal[mapIndex1]=normal[mappedIndex1];
					tempNormal[mapIndex2]=normal[mappedIndex2];
				}

				if(texture)
				{
					tempTexture[mapIndex0]=texture[mappedIndex0];
					tempTexture[mapIndex1]=texture[mappedIndex1];
					tempTexture[mapIndex2]=texture[mappedIndex2];
				}

				if(multicolor && color)
				{
					tempColor[mapIndex0]=color[mappedIndex0];
					tempColor[mapIndex1]=color[mappedIndex1];
					tempColor[mapIndex2]=color[mappedIndex2];
				}
			}

			drawLinesInternal(tempVertex,
					normal? tempNormal: NULL,
					texture? tempTexture: NULL,
					vertCount,e_triple,
					multicolor,
					(multicolor && color)? tempColor: color,
					FALSE,NULL,NULL,0,sp<DrawBufferI>(NULL));

			delete[] tempColor;
			delete[] tempTexture;
			delete[] tempNormal;
			delete[] tempVertex;

			return;
		}
		if(strip==e_strip)
		{
			const I32 triCount=vertices-2;
			for(I32 triIndex=0;triIndex<triCount;triIndex++)
			{
				drawLinesInternal(&vertex[triIndex],
						normal? &normal[triIndex]: NULL,
						texture? &texture[triIndex]: NULL,
						3,e_triple,
						multicolor,
						(multicolor && color)? &color[triIndex]: color,
						FALSE,NULL,NULL,0,sp<DrawBufferI>(NULL));
			}

			return;
		}
		if(strip==e_fan)
		{
			//* not exactly...
			strip=e_strip;
		}
		else
		{
			strip=e_triple;
		}

		drawLinesInternal(vertex,normal,texture,vertices,strip,
				multicolor,color,FALSE,NULL,NULL,0,spDrawBuffer);
	}
	else
	{
		drawPolys(3,vertex,normal,texture,vertices,strip,
				multicolor,color,vertexMap,hullPointMap,hullFacePoint,
				spDrawBuffer);
	}
}

void DrawOpenGL::drawRectangles(const SpatialVector *vertex,
		U32 vertices,BWORD multicolor,const Color *color)
{
	drawPolys(2,vertex,NULL,NULL,vertices,e_discrete,multicolor,color,
			NULL,NULL,NULL,sp<DrawBufferI>(NULL));
}

sp<DrawOpenGL::Texture> DrawOpenGL::createTexture(void)
{
	sp<Library> spLibrary(new Library());
	spLibrary->setRegistry(registry());

	sp<DrawOpenGL::Texture> spTexture(
			Library::create<DrawOpenGL::Texture>("DrawOpenGL::Texture",
			spLibrary.raw()));

	return spTexture;
}

sp<DrawBufferI> DrawOpenGL::createBuffer(void)
{
	sp<Library> spLibrary(new Library());
	spLibrary->setRegistry(registry());

	sp<DrawOpenGL::Buffer> spDrawBuffer(
			Library::create<DrawOpenGL::Buffer>("DrawOpenGL::Buffer",
			spLibrary.raw()));

	return spDrawBuffer;
}

DrawOpenGL::Buffer::Buffer(void):
	m_vboVertex(0),
	m_vboVertexMap(0),
	m_vboNormal(0),
	m_vboTexture(0),
	m_vboColor(0)
{
#ifdef FE_GL_OPENCL
	m_clMemVertex=NULL;
	m_clMemColor=NULL;
#endif
}

DrawOpenGL::Buffer::~Buffer(void)
{
	releaseAll();
}

void DrawOpenGL::Buffer::releaseAll(void)
{
#if DGL_THREAD_LOCKING
	if(!m_vboVertex && !m_vboVertexMap && !m_vboNormal &&
			!m_vboTexture && !m_vboColor)
	{
		return;
	}

	sp<DrawOpenGL> spDrawOpenGL(m_spDrawI);
	const BWORD lockThreads=
			(spDrawOpenGL.isValid() && spDrawOpenGL->m_lockThreads);

	if(lockThreads)
	{
		spDrawOpenGL->threadLock();
	}
#endif

	if(m_vboVertex)
	{
		glDeleteBuffers(1,&m_vboVertex);
		m_vboVertex=0;
	}
	if(m_vboVertexMap)
	{
		glDeleteBuffers(1,&m_vboVertexMap);
		m_vboVertexMap=0;
	}
	if(m_vboNormal)
	{
		glDeleteBuffers(1,&m_vboNormal);
		m_vboNormal=0;
	}
	if(m_vboTexture)
	{
		glDeleteBuffers(1,&m_vboTexture);
		m_vboTexture=0;
	}
	if(m_vboColor)
	{
		glDeleteBuffers(1,&m_vboColor);
		m_vboColor=0;
	}

#ifdef FE_GL_OPENCL
	if(m_clMemVertex)
	{
		clReleaseMemObject(m_clMemVertex);
	}
	if(m_clMemColor)
	{
		clReleaseMemObject(m_clMemColor);
	}
#endif

#if DGL_THREAD_LOCKING
	if(lockThreads)
	{
		spDrawOpenGL->threadUnlock();
	}
#endif
}

void DrawOpenGL::beginArrays(const SpatialVector* vertex,
		const SpatialVector* normal,const Array<I32>* vertexMap,
		const Vector2* texture,U32 vertices,
		BWORD multicolor,const Color* color,sp<DrawBufferI> spDrawBuffer,
		BWORD normals,BWORD blending)
{
	sp<DrawOpenGL::Buffer> spOpenGLBuffer(spDrawBuffer);

//	feLog("DrawOpenGL::beginArrays vertices %d buffer valid %d vbo %p"
//			" UV %d texture %p\n",
//			vertices, spOpenGLBuffer.isValid(),
//			spOpenGLBuffer.isValid()? spOpenGLBuffer->m_vboVertex: 0x0,
//			isUvSpace(),texture);

	const BWORD uvSpace=isUvSpace();
	if(uvSpace && !texture)
	{
		feLog("DrawOpenGL::beginArrays"
				" vertices %d in UV space without UV coordinates\n",
				vertices);
		return;
	}

#ifdef FE_USE_OPENCL
	sp<Master> spMaster=registry()->master();
	sp<Catalog> spMasterCatalog=spMaster->catalog();
#endif

	if(spOpenGLBuffer.isValid())
	{
		if(spOpenGLBuffer->m_spDrawI!=sp<DrawI>(this))
		{
			spOpenGLBuffer->releaseAll();
			spOpenGLBuffer->m_spDrawI=sp<DrawI>(this);
		}

//~#ifdef FE_USE_OPENCL
//~		const BWORD allowGLX=TRUE;
//~		OpenCLContext::confirm(spMaster,allowGLX);
//~#endif

		if(!spOpenGLBuffer->m_vboVertex)
		{
			glGenBuffers(1,&spOpenGLBuffer->m_vboVertex);

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::beginArrays buffer generate");
#endif

			glBindBuffer(GL_ARRAY_BUFFER,spOpenGLBuffer->m_vboVertex);

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::beginArrays buffer bind");
#endif

			if(uvSpace)
			{
				glBufferData(GL_ARRAY_BUFFER,
						sizeof(Vector2)*vertices,texture,
						GL_STATIC_DRAW);
			}
			else
			{
				glBufferData(GL_ARRAY_BUFFER,
						sizeof(SpatialVector)*vertices,vertex,
						GL_STATIC_DRAW);
			}

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::beginArrays buffer data");
#endif

#ifdef FE_GL_OPENCL
			cl_context clContext=
					(cl_context)spMasterCatalog->catalogOrDefault<void*>(
					"clContext",NULL);

			if(clContext)
			{
				cl_int clError=0;
				if(spOpenGLBuffer->m_vboVertex)
				{
					spOpenGLBuffer->m_clMemVertex=
							clCreateFromGLBuffer(clContext,
							CL_MEM_READ_WRITE,spOpenGLBuffer->m_vboVertex,
							&clError);
					if(clError)
					{
						feLog("DrawOpenGL::beginArrays"
								" clCreateFromGLBuffer vertex clError %d\n",
								clError);
					}
				}
			}

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::beginArrays create cl_context");
#endif
#endif
		}

#ifdef FE_USE_OPENCL
		if(spOpenGLBuffer->m_vboVertex)
		{
			//* TODO use SurfaceAccessibleOpenCL::Bridge::refresh

			cl_command_queue clCommandQueue=
					(cl_command_queue)spMasterCatalog->catalogOrDefault<void*>(
					"clCommandQueue2",NULL);

			if(clCommandQueue)
			{
				const I32 passes=spOpenGLBuffer->m_vboColor? 2: 1;

				//* TODO other attributes
				for(I32 pass=0;pass<passes;pass++)
				{
					const String memName=pass? "cl:pt:Cd": "cl:pt:P";

					sp<SurfaceAccessibleOpenCL::ClMem> spClMem=
							spOpenGLBuffer->catalog< sp<Counted> >(memName);

					cl_mem clMem4=(spClMem.isValid())?
							spClMem->m_clMem: NULL;

//					feLog("DrawOpenGL::beginArrays"
//							" VBO vertices %d copy catalog \"%s\" buffer %p\n",
//							vertices,memName.c_str(),clMem4);

					if(clMem4)
					{
						cl_mem& rClMemOpenGL=pass?
								spOpenGLBuffer->m_clMemColor:
								spOpenGLBuffer->m_clMemVertex;
#ifdef FE_GL_OPENCL
						if(rClMemOpenGL)
						{
							feLog("DrawOpenGL::beginArrays"
									" VBO GPU direct \"%s\"\n",
									memName.c_str());

							cl_int clError=clEnqueueAcquireGLObjects(
									clCommandQueue,1,
									&rClMemOpenGL,
									0,NULL,NULL);
//							feLog("clEnqueueAcquireGLObjects clError %d\n",
//									clError);

							if(pass)
							{
#if FALSE
								//* copy CL 4-vector to GL 4-vector
								clError=clEnqueueCopyBuffer(
										clCommandQueue,clMem4,
										rClMemOpenGL,
										0,0,16*vertices,
										0,NULL,NULL);
#else
								//* HACK replace color; keep existing alpha
								const size_t src_origin[3]={0,0,0};
								const size_t dest_origin[3]={0,0,0};
								const size_t region[3]={12,vertices,1};
								clError=clEnqueueCopyBufferRect(
										clCommandQueue,clMem4,
										rClMemOpenGL,
										src_origin,dest_origin,region,
										16,16*vertices,
										16,16*vertices,
										0,NULL,NULL);
#endif
							}
							else
							{
								//* convert CL 4-vector to GL 3-vector
								const size_t src_origin[3]={0,0,0};
								const size_t dest_origin[3]={0,0,0};
								const size_t region[3]={12,vertices,1};
								clError=clEnqueueCopyBufferRect(
										clCommandQueue,clMem4,
										rClMemOpenGL,
										src_origin,dest_origin,region,
										16,16*vertices,
										12,12*vertices,
										0,NULL,NULL);
							}
							if(clError)
							{
								feLog("clEnqueueCopyBufferRect clError %d\n",
										clError);
							}

							clError=clEnqueueReleaseGLObjects(
									clCommandQueue,1,
									&rClMemOpenGL,
									0,NULL,NULL);
//							feLog("clEnqueueReleaseGLObjects clError %d\n",
//									clError);

							clError=clFinish(clCommandQueue);
//							feLog("clFinish clError %d\n",clError);
						}
						else
#endif
						{
							feLog("DrawOpenGL::beginArrays"
									" VBO CPU round trip \"%s\"\n",
									memName.c_str());

							float* floatArray=new float[vertices*4];
							cl_int clError=clEnqueueReadBuffer(clCommandQueue,
									clMem4,CL_TRUE,0,
									vertices*sizeof(Vector4),
									floatArray,0,NULL,NULL);
							if(clError)
							{
								feLog("clEnqueueReadBuffer clError %d\n",
										clError);
							}

							glBindBuffer(GL_ARRAY_BUFFER,
									pass? spOpenGLBuffer->m_vboColor:
									spOpenGLBuffer->m_vboVertex);

#if DGL_CHECK_ERROR
							checkForError(String("DrawOpenGL::beginArrays"
									" opencl bind ")+
									(pass? "color": "vertex"));
#endif


							float* pFloat=floatArray;

							if(pass)
							{
								Color* pReplacement=
										new Color[vertices];

								for(U32 m=0;m<vertices;m++)
								{
									set(pReplacement[m],
											pFloat[0],pFloat[1],
											pFloat[2],float(1));
									pFloat+=4;
								}

								glBufferData(GL_ARRAY_BUFFER,
										sizeof(Color)*vertices,
										pReplacement,
										GL_STATIC_DRAW);

								delete[] pReplacement;

#if DGL_CHECK_ERROR
								checkForError("DrawOpenGL::beginArrays"
										" opencl color data");
#endif

							}
							else
							{
								SpatialVector* pReplacement=
										new SpatialVector[vertices];

								for(U32 m=0;m<vertices;m++)
								{
									set(pReplacement[m],
											pFloat[0],pFloat[1],pFloat[2]);
									pFloat+=4;
								}

								glBufferData(GL_ARRAY_BUFFER,
										sizeof(SpatialVector)*vertices,
										pReplacement,
										GL_STATIC_DRAW);

								delete[] pReplacement;

#if DGL_CHECK_ERROR
								checkForError("DrawOpenGL::beginArrays"
										" opencl vertex data");
#endif
							}

							delete[] floatArray;
						}
					}
#if FALSE
					else
					{
						//* replace from passed buffers
						//* (which usually don't change)

						if(!pass)
						{
							glBindBuffer(GL_ARRAY_BUFFER,
									spOpenGLBuffer->m_vboVertex);
							glBufferData(GL_ARRAY_BUFFER,
									sizeof(SpatialVector)*vertices,
									vertex,
									GL_STATIC_DRAW);
						}
						else if(multicolor && color)
						{
							glBindBuffer(GL_ARRAY_BUFFER,
									spOpenGLBuffer->m_vboColor);
							glBufferData(GL_ARRAY_BUFFER,
									sizeof(Color)*vertices,
									color,
									GL_STATIC_DRAW);
						}
					}
#endif
				}
			}
		}
#endif

		glBindBuffer(GL_ARRAY_BUFFER,spOpenGLBuffer->m_vboVertex);

#if DGL_CHECK_ERROR
		checkForError("DrawOpenGL::beginArrays vertex bind");
#endif

		glVertexPointer(uvSpace? 2: 3,DGL_REAL,0,NULL);
	}
	else if(uvSpace)
	{
		glVertexPointer(2,DGL_REAL,sizeof(Vector2),texture);
	}
	else
	{
		glVertexPointer(3,DGL_REAL,sizeof(SpatialVector),vertex);
	}

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::beginArrays vertex pointer");
#endif

	glEnableClientState(GL_VERTEX_ARRAY);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::beginArrays enable vertices");
#endif

	if(multicolor && color)
	{
		if(spOpenGLBuffer.isValid())
		{
			if(!spOpenGLBuffer->m_vboColor)
			{
//				feLog("new color buffer\n");
				glGenBuffers(1,&spOpenGLBuffer->m_vboColor);
				glBindBuffer(GL_ARRAY_BUFFER,
						spOpenGLBuffer->m_vboColor);
				glBufferData(GL_ARRAY_BUFFER,
						sizeof(Color)*vertices,color,
						GL_STATIC_DRAW);

#ifdef FE_GL_OPENCL
				cl_context clContext=
						(cl_context)spMasterCatalog->catalogOrDefault<void*>(
						"clContext",NULL);

				if(clContext)
				{
					cl_int clError=0;
					if(spOpenGLBuffer->m_vboColor)
					{
						spOpenGLBuffer->m_clMemColor=
								clCreateFromGLBuffer(clContext,
								CL_MEM_READ_WRITE,spOpenGLBuffer->m_vboColor,
								&clError);
						if(clError)
						{
							feLog("DrawOpenGL::beginArrays"
									" clCreateFromGLBuffer color clError %d\n",
									clError);
						}
					}
				}
#endif
			}

			glBindBuffer(GL_ARRAY_BUFFER,spOpenGLBuffer->m_vboColor);
			glColorPointer(4,DGL_REAL,0,NULL);
		}
		else
		{
			glColorPointer(4,DGL_REAL,sizeof(Color),color);
		}

		glEnableClientState(GL_COLOR_ARRAY);
	}
	else if(color)
	{
		if(blending)
		{
#if FE_DOUBLE_REAL
			glColor4dv(color[0].temp());
#else
			glColor4fv(color[0].temp());
#endif
		}
		else
		{
#if FE_DOUBLE_REAL
			glColor3dv(color[0].temp());
#else
			glColor3fv(color[0].temp());
#endif
		}

#if DGL_CHECK_ERROR
		checkForError("DrawOpenGL::beginArrays enable [multi] color");
#endif
	}

	if(normals)
	{
		if(spOpenGLBuffer.isValid())
		{
			if(!spOpenGLBuffer->m_vboNormal)
			{
//				feLog("new normal buffer\n");
				glGenBuffers(1,&spOpenGLBuffer->m_vboNormal);
				glBindBuffer(GL_ARRAY_BUFFER,
						spOpenGLBuffer->m_vboNormal);
				glBufferData(GL_ARRAY_BUFFER,
						sizeof(SpatialVector)*vertices,normal,
						GL_STATIC_DRAW);
			}

			glBindBuffer(GL_ARRAY_BUFFER,spOpenGLBuffer->m_vboNormal);
			glNormalPointer(DGL_REAL,0,NULL);
		}
		else
		{
			glNormalPointer(DGL_REAL,sizeof(SpatialVector),normal);
		}

		glEnableClientState(GL_NORMAL_ARRAY);

#if DGL_CHECK_ERROR
		checkForError("DrawOpenGL::beginArrays enable normals");
#endif
	}

	if(vertexMap)
	{
		if(spOpenGLBuffer.isValid())
		{
			if(!spOpenGLBuffer->m_vboVertexMap)
			{
//				feLog("new normal buffer\n");
				glGenBuffers(1,&spOpenGLBuffer->m_vboVertexMap);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
						spOpenGLBuffer->m_vboVertexMap);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
						sizeof(I32)*vertexMap->size(),
						&(*vertexMap)[0],
						GL_STATIC_DRAW);
			}

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
					spOpenGLBuffer->m_vboVertexMap);
		}

		glEnableClientState(GL_NORMAL_ARRAY);

#if DGL_CHECK_ERROR
		checkForError("DrawOpenGL::beginArrays enable normals");
#endif
	}

	if(texture)
	{
		if(spOpenGLBuffer.isValid())
		{
			if(!spOpenGLBuffer->m_vboTexture)
			{
//				feLog("new texture buffer\n");
				glGenBuffers(1,&spOpenGLBuffer->m_vboTexture);
				glBindBuffer(GL_ARRAY_BUFFER,
						spOpenGLBuffer->m_vboTexture);
				glBufferData(GL_ARRAY_BUFFER,
						sizeof(Vector2)*vertices,texture,
						GL_STATIC_DRAW);
			}

			glBindBuffer(GL_ARRAY_BUFFER,spOpenGLBuffer->m_vboTexture);
			glTexCoordPointer(2,DGL_REAL,0,NULL);
		}
		else
		{
			glTexCoordPointer(2,DGL_REAL,sizeof(Vector2),texture);
		}

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

#if DGL_CHECK_ERROR
		checkForError("DrawOpenGL::beginArrays enable texture");
#endif
	}
}

void DrawOpenGL::endArrays(const Array<I32>* vertexMap,
		const Vector2* texture,
		BWORD multicolor,const Color* color,sp<DrawBufferI> spDrawBuffer,
		BWORD normals)
{
	sp<DrawOpenGL::Buffer> spOpenGLBuffer(spDrawBuffer);

	if(vertexMap)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	}
	if(texture)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	if(normals)
	{
		glDisableClientState(GL_NORMAL_ARRAY);
	}
	if(multicolor && color)
	{
		glDisableClientState(GL_COLOR_ARRAY);
	}
	glDisableClientState(GL_VERTEX_ARRAY);

	if(spOpenGLBuffer.isValid())
	{
		glBindBuffer(GL_ARRAY_BUFFER,0);
	}
}

//* private: not generalized; only well defined in how it is called internally
void DrawOpenGL::drawPolys(U32 grain,const SpatialVector* vertex,
	const SpatialVector* normal,const Vector2* texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
#if FALSE
	feLog("DrawOpenGL::drawPolys"
			" %d gr %d st %d mc %d v0 <%s> n0 <%s>\n"
			"  c0 <%s> hm %d bf %d\n",
			vertices,grain,(U32)strip,multicolor,
			vertex? print(vertex[0]).c_str(): "NULL",
			normal? print(normal[0]).c_str(): "NULL",
			color? print(color[0]).c_str(): "NULL",
			vertexMap? (*vertexMap)[0]: -1,
			spDrawBuffer.isValid());

//	for(I32 m=0;m<16 && m<vertices;m++)
//	{
//		feLog("  %d/%d v %s n %s c %s m %d\n",m,vertices,
//				vertex? print(vertex[m]).c_str(): "NULL",
//				normal? print(normal[m]).c_str(): "NULL",
//				color? print(color[m]).c_str(): "NULL",
//				vertexMap? (*vertexMap)[m]: -1);
//	}
#endif

#if FALSE
	if(hullPointMap && hullFacePoint)
	{
		const I32 pointCount=(*hullPointMap).size();
		const I32 primitiveCount=(*hullFacePoint).size();

		feLog("DrawOpenGL::drawPolys vertices %d hull points %d faces %d\n",
				vertices,pointCount,primitiveCount);

		for(I32 pointIndex=0;pointIndex<pointCount && pointIndex<32;
				pointIndex++)
		{
			feLog("map %d -> %d\n",pointIndex,(*hullPointMap)[pointIndex]);
		}
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount &&
				primitiveIndex<32;primitiveIndex++)
		{
			feLog("face %d  %s\n",primitiveIndex,
					c_print((*hullFacePoint)[primitiveIndex]));
		}
	}
#endif

	FEASSERT(grain>1);
	FEASSERT(vertices>=grain);
	FEASSERT(strip!=e_discrete || !(vertices%grain) || vertexMap);
	FEASSERT(vertex);
//	FEASSERT(drawStyle()==DrawMode::e_outline || normal || grain<3);

#if DGL_CHECK_ALPHA
	if(!multicolor && color && (*color)[3]<=0.0f)
	{
		feX("DrawOpenGL::drawPolys",
				"alpha<=0 is pointless\n  check your color: %s\n",
				c_print(*color));
	}
#endif

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawPolys start");
#endif

	GLenum primitive=GL_QUADS;
	if(grain==3)
	{
		primitive=(strip==e_discrete)? GL_TRIANGLES:
				((strip==e_strip)? GL_TRIANGLE_STRIP:
				GL_TRIANGLE_FAN);
	}

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

	const BWORD pushLineWidth=(lineWidth()!=1.0f);
	Real alpha=color? color[0][3]: 1.0f;
	const BWORD lit=isLit();
//	const BWORD blending=((multicolor || alpha!=1.0f) && lit);
	const BWORD blending=(multicolor || alpha!=1.0f ||
			drawStyle()==DrawMode::e_ghost);
	if(blending && drawStyle()!=DrawMode::e_foreshadow)
	{
		pushBlending();
	}

	const U32 passes=1+(drawStyle()==DrawMode::e_edgedSolid ||
			drawStyle()==DrawMode::e_foreshadow);

	for(U32 pass=0;pass<passes;pass++)
	{
		if(pass)
		{
#if DGL_LINEWIDTH
			if(pushLineWidth)
			{
				glPushAttrib(GL_LINE_BIT);
				glLineWidth(lineWidth());
			}
#endif

			if(drawStyle()==DrawMode::e_edgedSolid)
			{
				setup(DrawMode::e_outline,FALSE,FALSE,FALSE,
						FALSE,FALSE,zBuffering());

				glEnable(GL_POLYGON_OFFSET_LINE);
				glPolygonOffset(-1.0f, 0.0f);
			}
			else
			{
				FEASSERT(drawStyle()==DrawMode::e_foreshadow);
				if(blending)
				{
					pushBlending();
				}

				setup(drawStyle(),lit,twoSidedLighting(),FALSE,
						frontfaceCulling(),backfaceCulling(),FALSE);
			}
		}

#if DGL_CHECK_ERROR
		checkForError("DrawOpenGL::drawPolys setup");
#endif

#if DGL_VERTEXARRAY
		const BWORD normals=!pass && lit;

		if(grain>2)
		{
			beginArrays(vertex,normal,vertexMap,texture,vertices,
					multicolor,color,spDrawBuffer,normals,blending);

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::drawPolys array prep");
#endif

			if(vertexMap)
			{
				glDrawElements(GL_TRIANGLES,vertexMap->size(),
						GL_UNSIGNED_INT,0);
			}
			else
			{
				glDrawArrays(primitive,0,vertices);
			}

			endArrays(vertexMap,texture,multicolor,color,
					spDrawBuffer,normals);
		}
		else
		{
			if(color)
			{
				if(blending)
				{
#if FE_DOUBLE_REAL
					glColor4dv(color[0].temp());
#else
					glColor4fv(color[0].temp());
#endif
				}
				else
				{
#if FE_DOUBLE_REAL
					glColor3dv(color[0].temp());
#else
					glColor3fv(color[0].temp());
#endif
				}
			}

#if DGL_CHECK_ERROR
			checkForError("DrawOpenGL::drawPolys single color");
#endif

			glBegin(primitive);
			for(U32 m=0; m < vertices; m++)
			{
#if FE_DOUBLE_REAL
				glVertex3d(vertex[m].temp()[0],vertex[m+1].temp()[1],
						vertex[m].temp()[2]);
				glVertex3d(vertex[m].temp()[0],vertex[m].temp()[1],
						vertex[m].temp()[2]);
#else
				glVertex3f(vertex[m].temp()[0],vertex[m+1].temp()[1],
						vertex[m].temp()[2]);
				glVertex3f(vertex[m].temp()[0],vertex[m].temp()[1],
						vertex[m].temp()[2]);
#endif

				m++;
#if FE_DOUBLE_REAL
				glVertex3d(vertex[m].temp()[0],vertex[m-1].temp()[1],
						vertex[m].temp()[2]);
				glVertex3d(vertex[m].temp()[0],vertex[m].temp()[1],
						vertex[m].temp()[2]);
#else
				glVertex3f(vertex[m].temp()[0],vertex[m-1].temp()[1],
						vertex[m].temp()[2]);
				glVertex3f(vertex[m].temp()[0],vertex[m].temp()[1],
						vertex[m].temp()[2]);
#endif
			}
			glEnd();
		}
#else
		glBegin(primitive);

		if(multicolor && color)
		{
			for(U32 m=0; m < vertices; m++)
			{
				if (color[m][3] != 1.0f)
#if FE_DOUBLE_REAL
					glColor4dv(color[m].temp());
#else
					glColor4fv(color[m].temp());
#endif
				else
#if FE_DOUBLE_REAL
					glColor3dv(color[m].temp());
#else
					glColor3fv(color[m].temp());
#endif

#if FE_DOUBLE_REAL
				glNormal3dv(normal[m].temp());
				glVertex3dv(vertex[m].temp());
#else
				glNormal3fv(normal[m].temp());
				glVertex3fv(vertex[m].temp());
#endif
			}
		}
		else
		{
			if(color)
			{
				if(blending)
#if FE_DOUBLE_REAL
					glColor4dv(color[0].temp());
#else
					glColor4fv(color[0].temp());
#endif
				else
#if FE_DOUBLE_REAL
					glColor3dv(color[0].temp());
#else
					glColor3fv(color[0].temp());
#endif
			}

			if(grain>2)
			{
				for(U32 m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glNormal3dv(normal[m].temp());
					glVertex3dv(vertex[m].temp());
#else
					glNormal3fv(normal[m].temp());
					glVertex3fv(vertex[m].temp());
#endif
				}
			}
			else
			{
				for(U32 m=0; m < vertices; m++)
				{
#if FE_DOUBLE_REAL
					glVertex3d(vertex[m].temp()[0],vertex[m+1].temp()[1],
							vertex[m].temp()[2]);
					glVertex3d(vertex[m].temp()[0],vertex[m].temp()[1],
							vertex[m].temp()[2]);
#else
					glVertex3f(vertex[m].temp()[0],vertex[m+1].temp()[1],
							vertex[m].temp()[2]);
					glVertex3f(vertex[m].temp()[0],vertex[m].temp()[1],
							vertex[m].temp()[2]);
#endif

					m++;
#if FE_DOUBLE_REAL
					glVertex3d(vertex[m].temp()[0],vertex[m-1].temp()[1],
							vertex[m].temp()[2]);
					glVertex3d(vertex[m].temp()[0],vertex[m].temp()[1],
							vertex[m].temp()[2]);
#else
					glVertex3f(vertex[m].temp()[0],vertex[m-1].temp()[1],
							vertex[m].temp()[2]);
					glVertex3f(vertex[m].temp()[0],vertex[m].temp()[1],
							vertex[m].temp()[2]);
#endif
				}
			}
		}

		glEnd();
#endif

		if(pass)
		{
			setup(drawStyle(),lit,twoSidedLighting(),FALSE,
					frontfaceCulling(),backfaceCulling(),zBuffering());
			glDisable(GL_POLYGON_OFFSET_LINE);

#if DGL_LINEWIDTH
			if(pushLineWidth)
			{
				//* restore line width
				glPopAttrib();
			}
#endif
		}
	}

	if(blending)
	{
		glPopAttrib();
	}

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawPolys complete");
#endif

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawAlignedText(const SpatialVector &location,
		const String text,const Color &color)
{
	const BWORD lit=isLit();

	if(m_spFont.isNull())
	{
		assureFont();
	}

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

	if(lit)
	{
		glDisable(GL_LIGHTING);
	}

	const Real alpha=color[3];
	const BWORD blending=(alpha<1.0);
	if(blending)
	{
		pushBlending();
	}

	m_spFont->drawAlignedText(sp<Component>(this),location,text,color);

	if(blending)
	{
		glPopAttrib();
	}

	if(lit)
	{
		glEnable(GL_LIGHTING);
	}

//	glFinish();

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawAlignedText complete");
#endif

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawSphereGLU(const SpatialTransform &transform,
		const SpatialVector *scale,const Color &color)
{
	const GLint slices=8;	// 6
	const GLint stacks=6;	// 4

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

#if DGL_ANTIALIAS_LINE
	BWORD antialiasAll=antialias() && drawStyle()<DrawMode::e_solid;
	if(antialiasAll)
		pushAntialiasLines();
#endif

#if DGL_LINEWIDTH
	const BWORD pushLineWidth=(lineWidth()!=1.0f);
	if(pushLineWidth)
	{
		glPushAttrib(GL_LINE_BIT);
		glLineWidth(lineWidth());
	}
#endif

#if DGL_CHECK_ALPHA
	if(color[3]<=0.0f)
	{
		feX("DrawOpenGL::drawSphereGLU",
				"alpha<=0 is pointless\n  check your color: %s\n",
				c_print(color));
	}
#endif

	const Real alpha=color[3];
	const BWORD blending=(alpha<1.0);
	if(blending)
	{
		pushBlending();
	}
	glPushMatrix();
	Real raw[16];
	transform.copy16(raw);
#if FE_DOUBLE_REAL
	glMultMatrixd(raw);
#else
	glMultMatrixf(raw);
#endif

#if FE_DOUBLE_REAL
	glColor4dv(color.temp());
#else
	glColor4fv(color.temp());
#endif

	//* TODO scale appropriately

	gluSphere(m_pQuadric,(*scale)[0],slices,stacks);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::drawSphereGLU gluSphere");
#endif

	if(drawStyle()==DrawMode::e_edgedSolid)
	{
#if DGL_ANTIALIAS_LINE
		if(antialias())
			pushAntialiasLines();
#endif

		setup(DrawMode::e_outline,FALSE,twoSidedLighting(),FALSE,
				frontfaceCulling(),backfaceCulling(),zBuffering());
		glEnable(GL_POLYGON_OFFSET_LINE);
		glPolygonOffset(-1.0f, 0.0f);

		gluSphere(m_pQuadric,(*scale)[0],slices,stacks);

		setup(drawStyle(),TRUE,twoSidedLighting(),FALSE,
				frontfaceCulling(),backfaceCulling(),zBuffering());
		glDisable(GL_POLYGON_OFFSET_LINE);

#if DGL_ANTIALIAS_LINE
		if(antialias())
			glPopAttrib();
#endif
	}

	glPopMatrix();

	if(blending)
	{
		glPopAttrib();
	}

#if DGL_LINEWIDTH
	if(pushLineWidth)
	{
		//* restore line width
		glPopAttrib();
	}
#endif

#if DGL_ANTIALIAS_LINE
	if(antialiasAll)
		glPopAttrib();
#endif

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawCylinderGLU(const SpatialTransform &transform,
		const SpatialVector *scale,Real baseScale,const Color &color,
		U32 slices)
{
	const U32 stacks=1;

	if(slices<1)
	{
		slices=6;	// tweak
	}
	else if(slices<3)
	{
		slices=3;
	}

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

#if DGL_ANTIALIAS_LINE
	BWORD antialiasAll=antialias() && drawStyle()<DrawMode::e_solid;
	if(antialiasAll)
		pushAntialiasLines();
#endif
#if DGL_LINEWIDTH
	const BWORD pushLineWidth=(lineWidth()!=1.0f);
	if(pushLineWidth)
	{
		glPushAttrib(GL_LINE_BIT);
		glLineWidth(lineWidth());
	}
#endif

#if DGL_CHECK_ALPHA
	if(color[3]<=0.0f)
	{
		feX("DrawOpenGL::drawCylinderGLU",
				"alpha<=0 is pointless\n  check your color: %s\n",
				c_print(color));
	}
#endif

	const Real alpha=color[3];
	const BWORD lit=isLit();
	const BWORD blending=(alpha!=1.0f && lit);
	if(blending)
	{
		pushBlending();
	}

	glPushMatrix();
	Real raw[16];
	transform.copy16(raw);
#if FE_DOUBLE_REAL
	glMultMatrixd(raw);
#else
	glMultMatrixf(raw);
#endif

	if(blending)
	{
#if FE_DOUBLE_REAL
		glColor4dv(color.temp());
#else
		glColor4fv(color.temp());
#endif
	}
	else
	{
#if FE_DOUBLE_REAL
		glColor3dv(color.temp());
#else
		glColor3fv(color.temp());
#endif
	}

	//* TODO scale appropriately

	gluCylinder(m_pQuadric,
			(*scale)[0]*baseScale,	//* base radius
			(*scale)[0],			//* top radius
			(*scale)[2],			//* height
			slices,					//* around
			stacks);				//* along

	if(drawStyle()==DrawMode::e_edgedSolid)
	{
#if DGL_ANTIALIAS_LINE
		if(antialias())
			pushAntialiasLines();
#endif

		setup(DrawMode::e_outline,FALSE,twoSidedLighting(),FALSE,
				frontfaceCulling(),backfaceCulling(),zBuffering());
		glEnable(GL_POLYGON_OFFSET_LINE);
		glPolygonOffset(-1.0f, 0.0f);

		gluCylinder(m_pQuadric,
				(*scale)[0]*baseScale,	//* base radius
				(*scale)[0],			//* top radius
				(*scale)[2],			//* height
				slices,					//* around
				stacks);				//* along

		setup(drawStyle(),TRUE,twoSidedLighting(),FALSE,
				frontfaceCulling(),backfaceCulling(),zBuffering());
		glDisable(GL_POLYGON_OFFSET_LINE);

#if DGL_ANTIALIAS_LINE
		if(antialias())
			glPopAttrib();
#endif
	}

	glPopMatrix();

	if(blending)
	{
		glPopAttrib();
	}

#if DGL_LINEWIDTH
	if(pushLineWidth)
	{
		//* restore line width
		glPopAttrib();
	}
#endif

#if DGL_ANTIALIAS_LINE
	if(antialiasAll)
		glPopAttrib();
#endif

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawTransformedPoints(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	Real matrix[16];
	transform.copy16(matrix);

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

	glPushMatrix();
#if FE_DOUBLE_REAL
	glMultMatrixd(matrix);
#else
	glMultMatrixf(matrix);
#endif

	if(scale)
	{
		glScalef((*scale)[0],(*scale)[1],(*scale)[2]);
	}

	drawPoints(vertex,normal,vertices,multicolor,color,spDrawBuffer);
	glPopMatrix();

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawTransformedLines(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	Real matrix[16];
	transform.copy16(matrix);

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

	glPushMatrix();
#if FE_DOUBLE_REAL
	glMultMatrixd(matrix);
#else
	glMultMatrixf(matrix);
#endif

	if(scale)
	{
		glScalef((*scale)[0],(*scale)[1],(*scale)[2]);
	}

	drawLines(vertex,normal,vertices,strip,multicolor,
			color,multiradius,radius,element,elementCount,spDrawBuffer);
	glPopMatrix();

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::drawTransformedTriangles(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	Real matrix[16];
	transform.copy16(matrix);

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

	glPushMatrix();
#if FE_DOUBLE_REAL
	glMultMatrixd(matrix);
#else
	glMultMatrixf(matrix);
#endif

	if(scale)
	{
		glScalef((*scale)[0],(*scale)[1],(*scale)[2]);
	}

	drawTriangles(vertex,normal,texture,vertices,strip,multicolor,
			color,vertexMap,hullPointMap,hullFacePoint,spDrawBuffer);
	glPopMatrix();

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::setupLights(void)
{
	m_brightness=fe::minimum(Real(1),fe::maximum(Real(0),m_brightness));
	m_contrast=fe::minimum(Real(3),fe::maximum(Real(0),m_contrast));

	const GLfloat position[]={ 1.0f, 1.0f, 3.0f, 0.0f };
	const GLfloat ambient[]=
			{ m_brightness, m_brightness, m_brightness, 1.0f };
	const GLfloat diffuse[]={ m_contrast, m_contrast, m_contrast, 1.0f };
	const GLfloat specular[]={ 1.0f, 1.0f, 1.0f, 1.0f };

	//* NOTE position light in camera space
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glLightfv(GL_LIGHT0,GL_POSITION,position);
	glLightfv(GL_LIGHT0,GL_AMBIENT,ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,specular);

	glPopMatrix();

#if FALSE
	const SpatialVector direction=
			unitSafe(SpatialVector(position[0],position[1],position[2]));
	const GLfloat spot_direction[]=
			{ direction[0], direction[1], direction[2] };
	const GLfloat spot_exponent[]= { 0.0f };
	const GLfloat spot_cutoff[]= { 180.0f };

	glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,spot_direction);
	glLightfv(GL_LIGHT0,GL_SPOT_EXPONENT,spot_exponent);
	glLightfv(GL_LIGHT0,GL_SPOT_CUTOFF,spot_cutoff);
#endif

	// glScalef will affect lighting unless auto-normalized
	glEnable(GL_NORMALIZE);
}

void DrawOpenGL::setup(DrawMode::DrawStyle style,BWORD lit,BWORD dual,
		BWORD texture,BWORD frontcull,BWORD backcull,BWORD zBuffer)
{
	gluQuadricDrawStyle(m_pQuadric,
			drawStyle()==DrawMode::e_wireframe? GLU_SILHOUETTE:
			(drawStyle()==DrawMode::e_pointCloud? GLU_POINT: GLU_FILL));

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::setup gluQuadricDrawStyle");
#endif

	gluQuadricTexture(m_pQuadric,texture? GL_TRUE: GL_FALSE);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::setup gluQuadricTexture");
#endif

	if(style==DrawMode::e_outline)
	{
		glPolygonMode(GL_FRONT,GL_LINE);
		glPolygonMode(GL_BACK,GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	}

	if(lit)
	{
		if(texture)
		{
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
//			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
//			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

//			glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
			glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

			glEnable(GL_TEXTURE_2D);
//			glShadeModel(GL_FLAT);
		}
		else
		{
			glDisable(GL_TEXTURE_2D);
		}

#if FALSE
		GLfloat colormap[]={ 11.0f, 159.0f, 159.0f };
		glMaterialfv(GL_FRONT,GL_COLOR_INDEXES,colormap);
#endif

//		const GLfloat alpha=0.7f;

		if(drawStyle()==DrawMode::e_ghost)
		{
			//* TODO param
			const GLfloat specular[]={ 0.5f, 0.5f, 0.5f, 1.0f };
			const GLfloat shininess[]={ 32.0f };

			glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,specular);
			glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,shininess);
		}
		else
		{
			//* default (off)
			const GLfloat specular[]={ 0.0f, 0.0f, 0.0f, 1.0f };
			glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,specular);
		}

#if FALSE
		const GLfloat black[]={ 0.0f, 0.0f, 0.0f, 1.0f };
		const GLfloat diffuse0[]={ 0.4f, 0.4f, 0.4f, alpha };

		const GLfloat ambient1[]={ 0.0f, 0.4f, 0.0f, alpha };
		const GLfloat diffuse1[]={ 0.0f, 0.6f, 0.0f, alpha };

		const GLfloat ambient2[]={ 0.0f, 0.0f, 0.4f, alpha };
		const GLfloat diffuse2[]={ 0.0f, 0.0f, 0.6f, alpha };

		if(dual)
		{
			glMaterialfv(GL_BACK,GL_AMBIENT,ambient2);
			glMaterialfv(GL_BACK,GL_DIFFUSE,diffuse2);
		}
		else
		{
			glMaterialfv(GL_BACK,GL_AMBIENT,ambient1);
			glMaterialfv(GL_BACK,GL_DIFFUSE,diffuse1);
		}

		if(style==DrawMode::e_edgedSolid)
		{
			glMaterialfv(GL_FRONT,GL_AMBIENT,black);
			glMaterialfv(GL_FRONT,GL_DIFFUSE,diffuse0);
		}
		else
		{
			glMaterialfv(GL_FRONT,GL_AMBIENT,ambient1);
			glMaterialfv(GL_FRONT,GL_DIFFUSE,diffuse1);
		}
		glDisable(GL_COLOR_MATERIAL);
#else
		//* follow glColor calls
		glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);
#endif

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glShadeModel(GL_SMOOTH);
	}
	else
	{
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);
	}

	//* Culling (Backface and/or Frontface)
//	if(frontcull || backcull || style==DrawMode::e_outline)
	if((frontcull || backcull) && style!=DrawMode::e_outline)
	{
		if(style==DrawMode::e_outline || (frontcull && backcull))
		{
			glCullFace(GL_FRONT_AND_BACK);
		}
		else if(backcull)
		{
			glCullFace(GL_BACK);
		}
		else
		{
			glCullFace(GL_FRONT);
		}
		glEnable(GL_CULL_FACE);
	}
	else
	{
		glDisable(GL_CULL_FACE);
	}

	//* Two Sided Lighting
	if(frontcull || backcull || style==DrawMode::e_outline || !dual)
	{
		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,GL_FALSE);
	}
	else
	{
		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);
	}

	if(zBuffer)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
}

void DrawOpenGL::createDefaultTexture(void)
{
	const long bit=0x20;
	unsigned char c;
	long m,n;
	for(m=0;m<DGL_DEFAULT_TEXTURE_SIZE;m++)
	{
		for(n=0;n<DGL_DEFAULT_TEXTURE_SIZE;n++)
		{
#if FALSE
			c=128+127*(((m&bit)==0)^(((n&bit))==0));
			m_defaultTextureData[m][n][0]=383-c;
			m_defaultTextureData[m][n][1]=c;
			m_defaultTextureData[m][n][2]=c;
#else
			c=192+63*(((m&bit)==0)^(((n&bit))==0));
			m_defaultTextureData[m][n][0]=c;
			m_defaultTextureData[m][n][1]=c;
			m_defaultTextureData[m][n][2]=c;
#endif
		}
	}

	m_spDefaultTexture=getTexture(
			DGL_DEFAULT_TEXTURE_SIZE,DGL_DEFAULT_TEXTURE_SIZE,
			GL_RGB,GL_UNSIGNED_BYTE,m_defaultTextureData,-1);
}

sp<DrawOpenGL::Texture> DrawOpenGL::getTexture(
		GLsizei a_width,GLsizei a_height,
		GLenum a_format,GLenum a_type,
		const GLvoid* a_pData,I32 a_serial)
{
	sp<Texture> spTexture=m_textureTable[a_pData];
	if(spTexture.isValid())
	{
		//* NOTE could do memcmp on data, but that takes time
		if(spTexture->m_width==a_width &&
				spTexture->m_height==a_height &&
				spTexture->m_type==a_type &&
				spTexture->m_serial==a_serial)
		{
			return spTexture;
		}
	}

	spTexture=createTexture();

	spTexture->m_width=a_width;
	spTexture->m_height=a_height;
	spTexture->m_type=a_type;
	spTexture->m_pData=a_pData;
	spTexture->m_serial=a_serial;

	//* temporary bind
	glBindTexture(GL_TEXTURE_2D,spTexture->m_id);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::getTexture glBindTexture");
#endif

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);

	//* NOTE could be set to something different
	const GLint internalformat=a_format;

	glTexImage2D(GL_TEXTURE_2D,0,internalformat,
			a_width,a_height,0,
			a_format,a_type,a_pData);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::getTexture glTexImage2D");
#endif

	m_textureTable[a_pData]=spTexture;

	glBindTexture(GL_TEXTURE_2D,m_currentTextureId);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::getTexture glBindTexture (restore)");
#endif

	return spTexture;
}

void DrawOpenGL::useDefaultTexture(void)
{
	m_currentTextureId=m_spDefaultTexture->m_id;
	glBindTexture(GL_TEXTURE_2D,m_currentTextureId);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::useDefaultTexture glBindTexture");
#endif
}

void DrawOpenGL::queryFormat(sp<ImageI> spImageI,GLenum& format,
		GLenum& type,GLint& components)
{
	switch(spImageI->format())
	{
		case ImageI::e_colorindex:
			format=GL_COLOR_INDEX;
			type=GL_UNSIGNED_SHORT;
			components=1;
			break;
		case ImageI::e_rgba:
			format=GL_RGBA;
			type=GL_UNSIGNED_BYTE;
			components=4;
			break;
		default:
			format=GL_RGB;
			type=GL_UNSIGNED_BYTE;
			components=3;
			break;
	}
}

void DrawOpenGL::drawRaster(sp<ImageI> spImageI,const SpatialVector& location)
{
	if(spImageI.isNull())
	{
		return;
	}

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadLock();
	}
#endif

//	glPushAttrib(GL_DEPTH_BUFFER_BIT);
//	glDisable(GL_DEPTH_TEST);

	glRasterPos3f((GLfloat)location[0],(GLfloat)location[1],
			(GLfloat)location[2]);
	glPixelZoom(1.0f,-1.0f);

	GLenum format;
	GLenum type;
	GLint components;
	queryFormat(spImageI,format,type,components);

#if TRUE
	glDrawPixels((GLsizei)spImageI->width(),(GLsizei)spImageI->height(),
			format,type,(const GLvoid*)spImageI->raw());
#else
	const char* data=(const char*)spImageI->raw();
	const I32 width=spImageI->width();
	const I32 height=spImageI->height();
	SpatialVector points[width];
	Color colors[width];
	const Real colorScale=0.8/255.5;
	I32 index(0);
	for(I32 y=0;y>=1-height;y--)
	{
		for(I32 x=0;x<width;x++)
		{
			const Real red=data[index++]*colorScale;
			const Real green=data[index++]*colorScale;
			const Real blue=data[index++]*colorScale;
			colors[x]=Color(red,green,blue,1);

			points[x]=location+SpatialVector(x,y,0);
		}
		drawPoints(points,NULL,width,TRUE,colors);
	}
#endif

//	glPopAttrib();

#if DGL_THREAD_LOCKING
	if(m_lockThreads)
	{
		threadUnlock();
	}
#endif
}

void DrawOpenGL::bindTexture(sp<ImageI> spImageI,I32 imageID)
{
	if(spImageI==m_spCurrentTexture)
	{
		return;
	}

//	feLog("DrawOpenGL::bindTexture %p -> %p id %d\n",
//			m_spCurrentTexture.raw(),spImageI.raw(),imageID);

	m_spCurrentTexture=spImageI;

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::bindTexture start");
#endif

	if(spImageI.isNull())
	{
		m_currentTextureId=0;
		glBindTexture(GL_TEXTURE_2D,m_currentTextureId);
		return;
	}

	if(imageID>=0)
	{
		spImageI->select(imageID);
	}

	GLsizei width=spImageI->width();
	GLsizei height=spImageI->height();
	const GLvoid* pData=spImageI->raw();
	const I32 serial=spImageI->serial();

	GLenum format;
	GLenum type;
	GLint components;
	queryFormat(spImageI,format,type,components);

//	feLog("DrawOpenGL::bindTexture size %d %d components %d pData %p\n",
//			width,height,components,pData);

	if(width<1 || height<1 || components<1 || components>4)
	{
		useDefaultTexture();
		return;
	}

	sp<Texture> spTexture=getTexture(width,height,format,type,pData,serial);

	m_currentTextureId=spTexture->m_id;
	glBindTexture(GL_TEXTURE_2D,m_currentTextureId);

#if DGL_CHECK_ERROR
	checkForError("DrawOpenGL::bindTexture glBindTexture");
#endif
}

} /* namespace ext */
} /* namespace fe */
