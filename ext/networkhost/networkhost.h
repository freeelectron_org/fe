/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __networkhost_networkhost_h__
#define __networkhost_networkhost_h__

#include "math/math.h"
#include "network/network.h"
#include "signal/signal.h"

#ifdef MODULE_networkhost
#define FE_NETWORKHOST_PORT FE_DL_EXPORT
#else
#define FE_NETWORKHOST_PORT FE_DL_IMPORT
#endif

#include "networkhost/NetHost.h"

#endif /* __networkhost_networkhost_h__ */
