import os
import sys
import re
import shutil
import string
import copy
from distutils.core import setup as dist_setup, Extension
from distutils.util import get_platform
import distutils.sysconfig
forge = sys.modules["forge"]

def create_init_file(target):
    forge.cprint(forge.WHITE,0,"creating __init__.py")
    forge.confirm_path(target)
    f = file(target.name, "w")
    f.write("""\
import sys
if sys.platform != "win32":
    sys.setdlopenflags(257)
    # dl not available on older install
    #import dl
    #sys.setdlopenflags(dl.RTLD_LAZY | dl.RTLD_GLOBAL)
from fe.pyfe import *
    """)
    f.close()

def build_python_module(target):
    match = re.compile(r'(.*)(pyfe(_d)?' + forge.pymodsuf + ')').match(target.name)
    if match == None:
        forge.cprint(forge.RED,1,"build_python_module called for wrong target: " + target.name)
        return
    forge.cprint(forge.WHITE,0,'building pyfe python module')
    pwd = os.getcwd()
    os.chdir(os.path.join(forge.extPath,'pyfe'))
    cache_argv = sys.argv
    sys.argv = ["","build"]
    xtracompileargs = [ ]
    xtralinkargs = [ ]
    macros = []
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        pyoslibs = [ "User32" ]
        pyoslibs += [ "ws2_32" ]
        pyoslibs += [ forge.boost_thread_lib ]
        xtracompileargs += [ '/wd4251','/wd4275', '/wd4800' ]
        xtralinkargs += [ "/NODEFAULTLIB:MSVCRT.lib" ]
        xtralinkargs += [ "/NODEFAULTLIB:MSVCPRT.lib" ]
        if forge.codegen == 'debug':
            xtralinkargs += [ "/NODEFAULTLIB:MSVCPRTD.lib" ]
            xtralinkargs += [ "/NODEFAULTLIB:libcmt.lib" ]
            pyoslibs = pyoslibs + [ "feImportMemory", "libcmtd" ]
        macros = [  ("WIN32",None),
                    ("WIN32_LEAN_AND_MEAN",None),
                    ("_MBCS",None),
                    ("_LIB",None) ]
    if forge.fe_os == "FE_LINUX":
        pyoslibs = [ "stdc++" ]
        macros += [ ("BOOST_HAS_THREADS",None) ]
    if forge.codegen == 'debug':
        macros += [ ("_DEBUG",None) ]
    incl_dirs = []
    for inc in forge.includemap.keys():
        incl_dirs.append(forge.includemap[inc])
    lib_dirs = []
    if "INCLUDE" in os.environ:
        for incl in string.split(os.environ["INCLUDE"],';'):
            incl_dirs += [ os.path.normpath(incl) ]
        incl_cache = os.environ["INCLUDE"]
    if "LIB" in os.environ:
        for lib in string.split(os.environ["LIB"],';'):
            lib_dirs += [ os.path.normpath(lib) ]
        lib_cache = os.environ["LIB"]


    dist_setup(name="pyfe", version="1.0",
        ext_modules=[Extension("pyfe", ["initialize.cc"],
        include_dirs= [ "..",
                        "../include"
                        ] + incl_dirs,
        extra_compile_args = xtracompileargs,
        extra_link_args = xtralinkargs,
        libraries=  [   "pyfe",
                        "feData",
                        "fePlatform",
                        "feMemory",
                        "feCore",
                        "fePlugin",
                        "feMath"
                    ] + pyoslibs,
        library_dirs=[ forge.libPath ] + lib_dirs,
        define_macros= macros
        )])

    forge.cprint(forge.WHITE,0,'copying pyfe python module')
    src_file = 'pyfe' + forge.pymodsuf
    forge.confirm_path(target)

#   NOTE depends on old functions
#   from distutils.util import get_platform     (deprecated)
#   def python_build_name():
#       v = sys.version_info
#       return get_platform() + "-" + str(v[0]) + "." + str(v[1])

    shutil.copyfile(os.path.join('build','lib.'+python_build_name(),
        src_file), target.name)

    sys.argv = cache_argv
    if "INCLUDE" in os.environ.has_key:
        os.environ["INCLUDE"] = incl_cache
    if "LIB" in os.environ:
        os.environ["LIB"] = lib_cache

    # clean out temporary files
    shutil.rmtree(os.path.join(forge.extPath,'pyfe','build'))

    os.chdir(pwd)


def verlib():
    path,tail = os.path.split(distutils.sysconfig.get_python_lib())
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        path,tail = os.path.split(path)
        return forge.libincl + path + "\\libs"
    else:
        return forge.libincl + path

def verinc():
    return distutils.sysconfig.get_python_inc()

def python_build_name():
    v = sys.version_info
    return get_platform() + "-" + str(v[0]) + "." + str(v[1])


def setup(module):
    ###########################################################################
    # base python support library
    srcList = [ "pyfe.pmh",
                "pyobjects",
                "internal" ]

    deplist = [ "feCoreLib",
                "fePlatformLib",
                "feMemoryLib",
                "feDataLib",
                "fePluginLib",
                "feMathLib" ]

    lib = module.DLL("pyfe", srcList)

    forge.deps( ["pyfeLib"], deplist )

    module.includemap = { 'pyfe' : verinc() }
    module.linkmap = { 'pyfe' : verlib() }

    ###########################################################################
    # custom python module
    pyfe = 'pyfe' + forge.pymodsuf
    pyfe_file = os.path.join(forge.libPath, 'fe', pyfe)
    pymod = forge.FileTarget( pyfe_file )
    pymod.AddDep(forge.targetRegistry["pyfeLib"])
    pymod.AppendOverrideRule(".*", build_python_module)
    module.AddDep(pymod)
    forge.AddSourceDeps(pymod, module, [ "initialize" ])

    init_file = os.path.join(forge.libPath, 'fe', '__init__.py')
    init_target = forge.FileTarget( init_file )
    init_target.AddDep(pymod)
    init_target.AppendOverrideRule(".*", create_init_file)
    module.AddDep(init_target)

    ###########################################################################
    ###########################################################################

    ###########################################################################
    # boosted python modules
    basic_fe_libs = [   "fePlatform",
                        "feMemory",
                        "feCore",
                        "fePlugin",
                        "pyfe",
                        "feData",
                        "feMath" ]

    # data
    lib = module.Lib("pyfeData", ["pyfeData"])
    boost_tgt = forge.boost_target(module, 'fe_data', 'fe.data',
        basic_fe_libs + [ "pyfeData" ] )
    boost_tgt.AddDep(forge.targetRegistry["feDataLib"])

    # signal
    lib = module.Lib("pyfeSignal", ["pyfeSignal"])
    boost_tgt = forge.boost_target(module, 'fe_signal', 'fe.signal',
        basic_fe_libs + [ "fexSignal", "pyfeData", "pyfeSignal" ] )
    boost_tgt.AddDep(forge.targetRegistry["fexSignalLib"])

    # window
    boost_tgt = forge.boost_target(module, 'fe_window', 'fe.window',
        basic_fe_libs )

    # math
    boost_tgt = forge.boost_target(module, 'fe_math', 'fe.math',
        basic_fe_libs )

    # network
    boost_tgt = forge.boost_target(module, 'fe_network', 'fe.network',
        basic_fe_libs + [ "pyfeData" ] )


