import sys
import os
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
        'qtLib',
        'fexSignalLib',
        'fexWindowLib',
        'fexViewerLib',
        'fexQTDLLib',
        'fexDataToolLib' ]

    #tests = [  'xQT' ]
    tests = [    ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)
