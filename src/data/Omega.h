/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

/*  This header simply has stuff that has to be after all the inter
	dependendencies have been defined.  */

#ifndef __data_Omega_h__
#define __data_Omega_h__

namespace fe
{

template <typename R>
void Scope::assignIDNumber(const R &record)
{
	SAFEGUARD_IF(m_locking);

#if FE_SCOPE_GETRECORD
	int *pID;
	pID = m_paIDNumber->queryAttribute(record);
	if(pID)
	{
		if(m_freeIDNumbers.size() != 0)
		{
			m_idNumbered[m_freeIDNumbers.back()] = record;
			*pID = m_freeIDNumbers.back();
//			feLog("re assignIDNumber %p %d\n",record.data(),*pID);
			m_freeIDNumbers.pop_back();
		}
		else
		{
			int index = m_idNumbered.size();
//			feLog("assignIDNumber %p %d\n",record.data(),index);
			m_idNumbered.resize(index + 1);
			*pID = index;
			WeakRecord weak_record(record);
			m_idNumbered[index] = weak_record;
		}
	}
#endif

	//* piggyback
	std::atomic<int> *pSN;
	pSN = m_paSerialNumber->queryAttribute(record);
	if(pSN)
	{
		*pSN=m_serialCount++;
//		feLog("assignIDNumber SN %p %d %s\n",record.data(),*pSN,
//				record.layout()->name().c_str());
		if(m_serialCount<0)
		{
			m_serialCount=0;
		}
	}

pSN = m_paSerialNumber->queryAttribute(record);
}

template <typename R>
void Scope::freeIDNumber(const R &r_old)
{
	SAFEGUARD_IF(m_locking);

#if FE_SCOPE_GETRECORD
	int *pID;
	if(m_paIDNumber &&
		m_paIDNumber->queryAttribute(r_old, pID))
	{
//		feLog("freeIDNumber %p %d\n",stateblock,*pID);
		m_freeIDNumbers.push_back(*pID);
		m_idNumbered[*pID] = WeakRecord();
	}
#endif

	//* piggyback
	std::atomic<int> *pSN;
	if(m_paSerialNumber &&
		m_paSerialNumber->queryAttribute(r_old, pSN))
	{
//		feLog("freeIDNumber SN %p %d\n",stateblock,*pSN);
		*pSN= -1;
	}
}



} /* namespace*/

#endif /* __data_Omega_h__ */

