/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_MethodI_h__
#define __plan_MethodI_h__

namespace fe
{

class MethodI : virtual public Component
{
	public:
virtual	bool		method(Record &r_object, Record &r_arg,
						sp<RecordGroup> rg_plan)							= 0;
};

} /* namespace */

#endif /* __plan_MethodI_h__ */

