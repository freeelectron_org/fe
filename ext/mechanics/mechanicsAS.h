/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mechanics_mechanicsAS_h__
#define __mechanics_mechanicsAS_h__

#include "math/math.h"
#include "shape/shape.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT AsFrame :
	public AccessorSet,
	public Initialize<AsFrame>
{
	public:
		void initialize(void)
		{
			//add(location,		FE_SPEC("spc:location", "spatial location, possibly embedded in transform"));
			add(transform,		FE_SPEC("spc:transform", "spatial transform"));
			//scope()->enforce("spc:transform", "spc:location");
			//scope()->within("", "spc:location", "spc:transform", 3*sizeof(SpatialVector));
		}
#if 0
virtual	void	populate(sp<Layout> spLayout)
		{
			AccessorSet::populate(spLayout);

			// embed location within transform
			scope()->within(spLayout->name(), "spc:location",
							"spc:transform", 3*sizeof(SpatialVector));
		}
#endif
		/// translation within transform
		//Accessor<SpatialVector>		location;
		/// transform
		Accessor<SpatialTransform>	transform;
};

class FE_DL_EXPORT AsChildFrame:
	public AsFrame,
	public Initialize<AsChildFrame>
{
	public:
		void initialize(void)
		{
			add(parent,		FE_SPEC("frame:parent", "parent record"));
		}
		Accessor<Record>			parent;
};

class FE_DL_EXPORT AsMark:
	public AccessorSet,
	public Initialize<AsMark>
{
	public:
		void initialize(void)
		{
			add(mark,		FE_SPEC("generic:mark", "transient mark for algorithm usage"));
		}
		Accessor<bool>			mark;
};

class FE_DL_EXPORT AsGenericInt:
	public AccessorSet,
	public Initialize<AsGenericInt>
{
	public:
		void initialize(void)
		{
			add(integer,		FE_SPEC("generic:i_0", "transient generic integer"));
		}
		Accessor<int>	integer;
};

class FE_DL_EXPORT AsZYXTFrame:
	public AsChildFrame,
	public Initialize<AsZYXTFrame>
{
	public:
		void initialize(void)
		{
			add(zyx,		FE_SPEC("frame:zyx", "Euler angle rotations in ZYX order"));
			add(t,			FE_SPEC("frame:t", "translation"));
		}
		Accessor<SpatialVector>		zyx;
		Accessor<SpatialVector>		t;
};

class FE_DL_EXPORT AsRelativeFrame:
	public AsChildFrame,
	public Initialize<AsRelativeFrame>
{
	public:
		void initialize(void)
		{
			add(bound,		FE_SPEC("frame:bound", "whether frame has been bound to the parent"));
			add(reference,	FE_SPEC("frame:reference", "reference frame to use for binding, relative to parent"));
			add(delta,		FE_SPEC("frame:delta", "the transform representing the binding"));
		}
		Accessor<bool>				bound;
		Accessor<Record>			reference;
		Accessor<SpatialTransform>	delta;
};

class FE_DL_EXPORT AsBodyFrame :
	public AsFrame,
	public Initialize<AsBodyFrame>
{
	public:
		void initialize(void)
		{
			add(component,		FE_SPEC("component", "a generic component"));
		}
		Accessor< sp<Component> >	component;
};

class FE_DL_EXPORT AsWeightedPoint :
	public AccessorSet,
	public Initialize<AsWeightedPoint>
{
	public:
		void initialize(void)
		{
			add(weight,		FE_SPEC("spc:weight", "weight of a point"));
			add(point,		FE_SPEC("spc:point", "spatial point"));
		}
		Accessor< Real >		weight;
		Accessor< Record >		point;
};

class FE_DL_EXPORT AsBarycentricPoint :
	public AccessorSet,
	public Initialize<AsBarycentricPoint>
{
	public:
		void initialize(void)
		{
			add(location,			FE_SPEC("spc:location", "location of a point defined with weighted points"));
			add(velocity,			FE_SPEC("spc:velocity", "velocity of a point defined with weighted points"));
			add(weightedPoints,		FE_SPEC("spc:weightedPoints", "collection of weighted points"));
		}
		Accessor<SpatialVector>		location;
		Accessor<SpatialVector>		velocity;
		Accessor< sp<RecordGroup> >	weightedPoints;
};

typedef enum
{
	e_forcePoint		= (int)(1 << 0),
	e_solverParticle	= (int)(1 << 1),
	e_barycentricPoint	= (int)(1 << 2)
} pointType;

class FE_DL_EXPORT AsExplicitPointType :
	public AccessorSet,
	public Initialize<AsExplicitPointType>
{
	public:
		void initialize(void)
		{
			add(type,			FE_USE("spc:pointType"));
		}
		Accessor<int>				type;
};


class FE_DL_EXPORT AsLocatedFrame :
	public AsFrame,
	public Initialize<AsLocatedFrame>
{
	public:
		void initialize(void)
		{
			add(locator0,		FE_USE("bdy:locator0"));
			add(locator1,		FE_USE("bdy:locator1"));
			add(locator2,		FE_USE("bdy:locator2"));
			add(offset,			FE_USE("bdy:offset"));
		}
		/// locator reference point
		Accessor<Record>		locator0;
		/// locator reference point
		Accessor<Record>		locator1;
		/// locator reference point
		Accessor<Record>		locator2;
		/// locator offset
		Accessor<SpatialVector>	offset;

};


class FE_DL_EXPORT AsPhysicalFrame :
	public AsFrame,
	public Initialize<AsPhysicalFrame>
{
	public:
		void initialize(void)
		{
			add(force,			FE_USE("bdy:force"));
			add(torque,			FE_USE("bdy:torque"));

#if 1
			m_asBodyFrame.bind(scope());
			m_asLocatedFrame.bind(scope());
			m_asForcePoint.bind(scope());
			m_asChildFrame.bind(scope());
#endif
		}
		Accessor<SpatialVector>		force;
		Accessor<SpatialVector>		torque;

		/// velocity inferred from either a BodyFrame or LocatedFrame
		SpatialVector velocity(Record r_frame,
			const SpatialVector &a_worldLocation);
	private:
		AsBodyFrame		m_asBodyFrame;
		AsLocatedFrame	m_asLocatedFrame;
		AsForcePoint	m_asForcePoint;
		AsChildFrame	m_asChildFrame;
};


#if 0
// sort of an example of a virtual attribute
inline SpatialVector AsPhysicalFrame::velocity(Record r_frame, const SpatialVector &a_worldLocation)
{
	SpatialVector velocity;
	//m_asBodyFrame.bind(r_frame.layout()->scope());
	if(m_asBodyFrame.check(r_frame))
	{
		sp<BodyI> spBody(m_asBodyFrame.component(r_frame));
		if(spBody.isValid())
		{
			spBody->getVelocity(velocity, a_worldLocation);
		}
		return velocity;
	}

	//m_asLocatedFrame.bind(r_frame.layout()->scope());
	//m_asForcePoint.bind(r_frame.layout()->scope());
	if(m_asLocatedFrame.check(r_frame))
	{
		Record r_locator0 = m_asLocatedFrame.locator0(r_frame);
		if(!r_locator0.isValid() || !m_asForcePoint.check(r_locator0))
		{
			return velocity;
		}
		Record r_locator1 = m_asLocatedFrame.locator1(r_frame);
		if(!r_locator1.isValid() || !m_asForcePoint.check(r_locator1))
		{
			return velocity;
		}
		Record r_locator2 = m_asLocatedFrame.locator2(r_frame);
		if(!r_locator2.isValid() || !m_asForcePoint.check(r_locator2))
		{
			return velocity;
		}

		Real h = 0.00001;

		SpatialTransform t0_inv;
		invert(t0_inv, m_asLocatedFrame.transform(r_frame));

		SpatialVector loc;
		transformVector(t0_inv, a_worldLocation, loc);

		SpatialTransform t1;
		const SpatialVector &x0 = m_asForcePoint.location(r_locator0);
		const SpatialVector &x1 = m_asForcePoint.location(r_locator1);
		const SpatialVector &x2 = m_asForcePoint.location(r_locator2);
		const SpatialVector &v0 = m_asForcePoint.velocity(r_locator0);
		const SpatialVector &v1 = m_asForcePoint.velocity(r_locator1);
		const SpatialVector &v2 = m_asForcePoint.velocity(r_locator2);
		const SpatialVector &offset = m_asLocatedFrame.offset(r_frame);
		makeFrame(t1, x0+v0*h, x1+v1*h, x2+v2*h, offset);
		SpatialTransform t1_inv;
		invert(t1_inv, t1);

		SpatialVector worldLocation1;
		transformVector(t1, loc, worldLocation1);

		velocity = (worldLocation1 - a_worldLocation) / h;
		return velocity;
	}

	//m_asZYXTFrame.bind(r_frame.layout()->scope());
	if(m_asChildFrame.check(r_frame))
	{
		Record r_parent = m_asChildFrame.parent(r_frame);

		if(r_parent.isValid())
		{
			velocity = this->velocity(r_parent, a_worldLocation);
		}
	}

	return velocity;
}
#endif


#if 0
class FE_DL_EXPORT AsTire :
	public AccessorSet,
	public Initialize<AsTire>
{
	public:
		void initialize(void)
		{
			add(frame,		FE_USE("spc:frame"));
			add(radius,		FE_USE("bdy:radius"));
			add(stiffness,	FE_USE("mat:stiffness"));
			add(damping,	FE_USE("mat:damping"));
			add(grip,		FE_USE("mat:grip"));
		}
		/// Frame Record
		Accessor<Record>			frame;
		///
		Accessor<Real>				radius;
		///
		Accessor<Real>				stiffness;
		///
		Accessor<Real>				damping;
		///
		Accessor<Real>				grip;
};

class FE_DL_EXPORT AsNaiveTire :
	public AsTire,
	public Initialize<AsNaiveTire>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("tire:naive"));
		}
		Accessor<void>				is;
};

class FE_DL_EXPORT AsNaiveTireData :
	public AsNaiveTire,
	public Initialize<AsNaiveTireData>
{
	public:
		void initialize(void)
		{
			add(inContact,			FE_USE("naive_tire:inContact"));
			add(component,			FE_USE("component"));
			add(thrust,				FE_USE("naive_tire:thrust"));
			add(sidethrust,			FE_USE("naive_tire:sidethrust"));
			add(contactRecord,		FE_USE("naive_tire:contact"));
		}
		/// Frame Record
		Accessor<bool>				inContact;
		/// Single point contact location
		Accessor< sp<Component> >	component;
		/// external applied force
		Accessor<Real>				thrust;
		/// external applied force
		Accessor<Real>				sidethrust;
		/// contact record
		Accessor<Record>			contactRecord;
};
#endif


#if 0
class FE_DL_EXPORT AsPointCloud :
	public AccessorSet,
	public Initialize<AsPointCloud>
{
	public:
		void initialize(void)
		{
			add(points,			FE_USE("bdy:points"));
		}
		/// collection of points
		Accessor< sp<RecordGroup> >	points;
};
#endif



class FE_DL_EXPORT AsRod :
	public AccessorSet,
	public Initialize<AsRod>
{
	public:
		void initialize(void)
		{
			add(radius,			FE_USE("bdy:radius"));
			add(length,			FE_USE("bdy:length"));
			add(material,		FE_USE("bdy:material"));
		}
		/// physical body dimension, not bounding volume
		Accessor<Real>		radius;
		/// physical body dimension
		Accessor<Real>		length;
		/// homogenous material
		Accessor<Record>	material;
};

#if 0
class FE_DL_EXPORT AsWheel :
	public AccessorSet,
	public Initialize<AsWheel>
{
	public:
		void initialize(void)
		{
			add(radius,			FE_USE("bdy:radius"));
			add(width,			FE_USE("bdy:width"));
		}
		/// physical body dimension, not bounding volume
		Accessor<Real>		radius;
		/// physical body dimension
		Accessor<Real>		width;
};
#endif

class FE_DL_EXPORT AsFrameConstraint :
	public AccessorSet,
	public Initialize<AsFrameConstraint>
{
	public:
		void initialize(void)
		{
			add(frame,			FE_USE("constraint:frame"));
			add(point,			FE_USE("constraint:point"));
		}
		/// frame to constrain to
		Accessor<Record>		frame;
		/// point to constrain
		Accessor<Record>		point;
};

#if 1
class FE_DL_EXPORT AsChild :
	public AccessorSet,
	public Initialize<AsChild>
{
	public:
		void initialize(void)
		{
			add(parent,			FE_USE("bdy:parent"));
			add(local,			FE_USE("spc:local"));
		}
		Accessor<Record>			parent;
		Accessor<SpatialTransform>	local;
};
#endif

#if 0
class FE_DL_EXPORT AsTube :
	public AccessorSet,
	public Initialize<AsTube>
{
	public:
		void initialize(void)
		{
			add(radius,			FE_USE("bdy:radius"));
			add(thickness,		FE_USE("bdy:thickness"));
			add(length,			FE_USE("bdy:length"));
			add(material,		FE_USE("bdy:material"));
		}
		/// physical body dimension, not bounding volume
		Accessor<Real>		radius;
		/// physical body dimension
		Accessor<Real>		thickness;
		/// physical body dimension
		Accessor<Real>		length;
		/// homogenous material
		Accessor<Record>	material;
};
#endif

// TODO: rename AsHinge to AsJoint
class FE_DL_EXPORT AsHinge :
	public AccessorSet,
	public Initialize<AsHinge>
{
	public:
		void initialize(void)
		{
			add(anchor,			FE_USE("jnt:anchor"));
			add(axis,			FE_USE("spc:direction"));
			add(left,			FE_USE("pair:left"));
			add(right,			FE_USE("pair:right"));
		}
		/// location of anchor point
		Accessor<Record>			anchor;
		/// axis of hinge
		Accessor<SpatialVector>		axis;
		/// connected body
		Accessor<WeakRecord>		left;
		/// connected body
		Accessor<WeakRecord>		right;
};

class FE_DL_EXPORT AsMaterial :
	public AccessorSet,
	public Initialize<AsMaterial>
{
	public:
		void initialize(void)
		{
			add(density,		FE_USE("mat:density"));
		}
		/// density
		Accessor<Real>				density;
};


#if 0
class FE_DL_EXPORT AsForcePair :
	public AccessorSet,
	public Initialize<AsForcePair>
{
	public:
		void initialize(void)
		{
			add(a,			FE_USE("pair:left"));
			add(b,			FE_USE("pair:right"));
		}
};
#endif

class FE_DL_EXPORT AsPointThrust :
	public AccessorSet,
	public Initialize<AsPointThrust>
{
	public:
		void initialize(void)
		{
			add(point,			FE_USE("spc:point"));
			add(force,			FE_USE("sim:force"));
		}
		Accessor<Record>			point;
		Accessor<SpatialVector>		force;
};


class FE_DL_EXPORT AsRadialFake :
	public AccessorSet,
	public Initialize<AsRadialFake>
{
	public:
		void initialize(void)
		{
			add(r0,				FE_USE("pair:left"));
			add(r1,				FE_USE("pair:right"));
			add(r2,				FE_USE("fake:r2"));
			add(r3,				FE_USE("fake:r3"));
			add(anchor,			FE_USE("fake:anchor"));
			add(stiffness,		FE_USE("mat:stiffness"));
		}

		/// connected point
		Accessor<WeakRecord>		r0;
		/// connected point
		Accessor<WeakRecord>		r1;
		/// connected point
		Accessor<WeakRecord>		r2;
		/// connected point
		Accessor<WeakRecord>		r3;
		/// connected point
		Accessor<WeakRecord>		anchor;
		/// stiffness
		Accessor<Real>				stiffness;

};

class FE_DL_EXPORT AsDifferential :
	public AccessorSet,
	public Initialize<AsDifferential>
{
	public:
		void initialize(void)
		{
			add(shaft,			FE_USE("diff:shaft"));
			add(mount,			FE_USE("diff:mount"));
			add(left,			FE_USE("pair:left"));
			add(right,			FE_USE("pair:right"));
			add(flags,			FE_USE("mat:flags"));
		}
		/// connected fix frame (chassis)
		Accessor<WeakRecord>		mount;
		/// connected drive shaft
		Accessor<WeakRecord>		shaft;
		/// connected left halfshaft
		Accessor<WeakRecord>		left;
		/// connected right halfshaft
		Accessor<WeakRecord>		right;
		/// flags
		Accessor<int>				flags;
};

#define FE_DIFF_LOCKED		(int)(1 << 0)
#define FE_DIFF_OPEN		(int)(1 << 1)
#define FE_DIFF_HLSD		(int)(1 << 2)
#define FE_DIFF_VLSD		(int)(1 << 3)

/// Basic Hooke's law linear spring
class FE_DL_EXPORT AsLinearSpring :
	public AccessorSet,
	public Initialize<AsLinearSpring>
{
	public:
		void initialize(void)
		{
			add(left,			FE_USE("pair:left"));
			add(right,			FE_USE("pair:right"));
			add(length,			FE_USE("bdy:length"));
			add(stiffness,		FE_USE("mat:stiffness"));
			add(damping,		FE_USE("mat:damping"));
		}
		/// connected point
		Accessor<WeakRecord>		left;
		/// connected point
		Accessor<WeakRecord>		right;
		/// rest length
		Accessor<Real>				length;
		/// stiffness
		Accessor<Real>				stiffness;
		/// damping
		Accessor<Real>				damping;
};

#define FE_LSF_NOCOMPRESS	(int)(1 << 0)
#define FE_LSF_XIGNORE		(int)(1 << 1)
#define FE_LSF_YIGNORE		(int)(1 << 2)
#define FE_LSF_ZIGNORE		(int)(1 << 3)
#define FE_LSF_PULLCOMPRESS	(int)(1 << 4)
#define FE_LSF_OFFSET_MODE	(int)(1 << 5)

class FE_DL_EXPORT AsLinearSpringExtended :
	public AsLinearSpring,
	public Initialize<AsLinearSpringExtended>
{
	public:
		void initialize(void)
		{
			add(flags,			FE_USE("mat:flags"));
		}
		Accessor<int>		flags;
};


class FE_DL_EXPORT AsLinearSpringForceReturn :
	public AccessorSet,
	public Initialize<AsLinearSpringForceReturn>
{
	public:
		void initialize(void)
		{
			add(spring,			FE_USE("lsf:spring"));
		}
		Accessor<void*>		spring;
};


///
class FE_DL_EXPORT AsLinearSpringDelta :
	public AsLinearSpring,
	public Initialize<AsLinearSpringDelta>
{
	public:
		void initialize(void)
		{
			add(a,				FE_USE("quad:a"));
			add(b,				FE_USE("quad:b"));
			add(c,				FE_USE("quad:c"));
			add(d,				FE_USE("quad:d"));
			add(stiffness,		FE_USE("mat:stiffness"));
			add(damping,		FE_USE("mat:damping"));
			add(is,				FE_USE("is:linsprdelta"));
		}
		Accessor<WeakRecord>		a;
		Accessor<WeakRecord>		b;
		Accessor<WeakRecord>		c;
		Accessor<WeakRecord>		d;
		/// stiffness
		Accessor<Real>				stiffness;
		/// damping
		Accessor<Real>				damping;
		Accessor<void>				is;
};


class FE_DL_EXPORT AsLinearSpringDeltaData :
	public AccessorSet,
	public Initialize<AsLinearSpringDeltaData>
{
	public:
		void initialize(void)
		{
			add(abSpring,		FE_USE("pair:left"));
			add(cdSpring,		FE_USE("pair:right"));
		}
		Accessor<WeakRecord>		abSpring;
		Accessor<WeakRecord>		cdSpring;
};

/// For bending element based on Volino 2006 bending model
class FE_DL_EXPORT AsBend :
	public AccessorSet,
	public Initialize<AsBend>
{
	public:
		void initialize(void)
		{
			add(a,				FE_USE("quad:a"));
			add(b,				FE_USE("quad:b"));
			add(c,				FE_USE("quad:c"));
			add(d,				FE_USE("quad:d"));
			add(stiffness,		FE_USE("mat:stiffness"));
			add(damping,		FE_USE("mat:damping"));
			add(is,				FE_USE("is:bend"));
		}
		/// connected tip point -- A is opposite across axis from B
		Accessor<WeakRecord>		a;
		/// connected tip point -- B is opposite across axis from A
		Accessor<WeakRecord>		b;
		/// connected tip point -- CD is axis
		Accessor<WeakRecord>		c;
		/// connected tip point -- CD is axis
		Accessor<WeakRecord>		d;
		/// stiffness
		Accessor<Real>				stiffness;
		/// damping
		Accessor<Real>				damping;
		Accessor<void>				is;
};

class FE_DL_EXPORT AsLinearBend :
	public AsBend,
	public Initialize<AsLinearBend>
{
	public:
		void initialize(void)
		{
			add(length,			FE_USE("bend:length"));
			add(alpha,			FE_USE("bend:alpha"));
			add(lambda,			FE_USE("bend:lambda"));
			add(gamma,			FE_USE("bend:gamma"));

			add(bendJacobians,	FE_USE("bdy:bendJacobians"));
		}
		/// rest curvature 'length' per Volino 2006
		Accessor<Real>				length;
		/// Volino 2006 alpha
		Accessor<Vector4>			alpha;
		/// Volino 2006 lambda
		Accessor<Real>				lambda;
		/// Volino 2006 gamma
		Accessor<Real>				gamma;
		/// Bend Jacobian Object
		Accessor< sp<Component> >	bendJacobians;
};

/* face is a quad for now.  While I think of ti as the more pure
	surface element, for RL cases at the time of this writing,
	there is a slight performance and packaging/memory upside to using
	quads */
class FE_DL_EXPORT AsFace :
	public AccessorSet,
	public Initialize<AsFace>
{
	public:
		void initialize(void)
		{
			add(a,				FE_USE("face:a"));
			add(b,				FE_USE("face:b"));
			add(c,				FE_USE("face:c"));
			add(d,				FE_USE("face:d"));
		}
		/// vertex
		Accessor<WeakRecord>		a;
		/// vertex
		Accessor<WeakRecord>		b;
		/// vertex
		Accessor<WeakRecord>		c;
		/// vertex
		Accessor<WeakRecord>		d;
};

class FE_DL_EXPORT AsFacePressure :
	public AsFace,
	public Initialize<AsFacePressure>
{
	public:
		void initialize(void)
		{
			add(pressure,		FE_USE("face:pressure"));
		}
		/// pressure
		Accessor<Real>				pressure;
};

class FE_DL_EXPORT AsFaceStrain :
	public AsFace,
	public Initialize<AsFaceStrain>
{
	public:
		void initialize(void)
		{
			add(angle,			FE_USE("face:angle"));
			add(stiffness,		FE_USE("mat:uv_stiffness"));
			add(damping,		FE_USE("mat:uv_damping"));
			add(poisson,		FE_USE("mat:poisson"));
			add(shear,			FE_USE("mat:uv_shear_stiffness"));
		}
		/// angle of cords/fibers/grain
		Accessor<Real>				angle;
		/// uv stiffness
		Accessor<Vector2>			stiffness;
		/// uv damping
		Accessor<Vector2>			damping;
		/// poisson ratio
		Accessor<Real>				poisson;
		/// uv shear modulus
		Accessor<Real>				shear;
};


class FE_DL_EXPORT AsStrand :
	public AccessorSet,
	public Initialize<AsStrand>
{
	public:
		void initialize(void)
		{
			add(a,				FE_USE("strand:a"));
			add(b,				FE_USE("strand:b"));
			add(c,				FE_USE("strand:c"));
			add(stiffness,		FE_USE("mat:stiffness"));
			add(damping,		FE_USE("mat:damping"));
			add(is,				FE_USE("strand:bend"));
		}
		/// connected tip point
		Accessor<WeakRecord>		a;
		/// connected tip point
		Accessor<WeakRecord>		b;
		/// connected tip point
		Accessor<WeakRecord>		c;
		/// stiffness
		Accessor<Real>				stiffness;
		/// damping
		Accessor<Real>				damping;

		// to distinct from AsDotElement
		Accessor<void>				is;
};


class FE_DL_EXPORT AsDotElement :
	public AccessorSet,
	public Initialize<AsDotElement>
{
	public:
		void initialize(void)
		{
			add(a,				FE_USE("strand:a"));
			add(b,				FE_USE("strand:b"));
			add(c,				FE_USE("strand:c"));
			add(stiffness,		FE_USE("mat:stiffness"));
			add(damping,		FE_USE("mat:damping"));
			add(restdot,		FE_USE("strand:restdot"));
		}
		/// connected tip point
		Accessor<WeakRecord>		a;
		/// connected tip point
		Accessor<WeakRecord>		b;
		/// connected tip point
		Accessor<WeakRecord>		c;
		/// stiffness
		Accessor<Real>				stiffness;
		/// damping
		Accessor<Real>				damping;
		/// rest dot
		Accessor<Real>				restdot;
};


class FE_DL_EXPORT AsLinearStrand :
	public AsStrand,
	public Initialize<AsLinearStrand>
{
	public:
		void initialize(void)
		{
			add(length,			FE_USE("bend:length"));
			add(alpha,			FE_USE("bend:alpha"));
			add(lambda,			FE_USE("bend:lambda"));
			add(gamma,			FE_USE("bend:gamma"));

			add(bendJacobians,	FE_USE("bdy:bendJacobians"));
		}
		Accessor<Real>				length;
		Accessor<Vector4>			alpha;
		Accessor<Real>				lambda;
		Accessor<Real>				gamma;
		Accessor< sp<Component> >	bendJacobians;
};

class FE_DL_EXPORT AsStrandTwist :
	public AccessorSet,
	public Initialize<AsStrandTwist>
{
	public:
		void initialize(void)
		{
			add(a,				FE_USE("strand:a"));
			add(b,				FE_USE("strand:b"));
			add(c,				FE_USE("strand:c"));
			add(d,				FE_USE("strand:d"));
			add(stiffness,		FE_USE("mat:rotStiffness"));
			add(damping,		FE_USE("mat:rotDamping"));
		}
		Accessor<WeakRecord>		a;
		Accessor<WeakRecord>		b;
		Accessor<WeakRecord>		c;
		Accessor<WeakRecord>		d;
		Accessor<Real>				stiffness;
		Accessor<Real>				damping;
};

class FE_DL_EXPORT AsLinearStrandTwist:
	public AsStrandTwist,
	public Initialize<AsLinearStrandTwist>
{
	public:
		void initialize(void)
		{
			add(length,			FE_USE("bend:length"));
			add(alpha,			FE_USE("bend:alpha"));
			add(lambda,			FE_USE("bend:lambda"));
			add(gamma,			FE_USE("bend:gamma"));

			add(jacobians,		FE_USE("bdy:bendJacobians"));
		}
		Accessor<Real>				length;
		Accessor<Vector4>			alpha;
		Accessor<Real>				lambda;
		Accessor<Real>				gamma;
		Accessor< sp<Component> >	jacobians;
};

class FE_DL_EXPORT AsElastic:
	public AccessorSet,
	public Initialize<AsElastic>
{
	public:
		void initialize(void)
		{
			add(stiffness,		FE_USE("mat:stiffness"));
		}
		Accessor<Real>			stiffness;
};

// jammed in here for liquid sandbox demo
class FE_DL_EXPORT AsLiquidSand:
	public AccessorSet,
	public Initialize<AsLiquidSand>
{
	public:
		void initialize(void)
		{
			add(hardRadius,		FE_USE("liquid:hardRadius"));
			add(attractRate,	FE_USE("liquid:attractRate"));
		}
		Accessor<Real>			hardRadius;
		Accessor<Real>			attractRate;
};

class FE_DL_EXPORT AsHasChannel:
	public AccessorSet,
	public Initialize<AsHasChannel>
{
	public:
		void initialize(void)
		{
			add(name,		FE_USE("channel:name"));
		}
		Accessor<String>			name;
};

class FE_DL_EXPORT AsDrivenHinge:
	public AccessorSet,
	public Initialize<AsDrivenHinge>
{
	public:
		void initialize(void)
		{
			add(angle,		FE_USE("jnt:angle"));
			add(stiffness,	FE_USE("mat:rotStiffness"));
			add(damping,	FE_USE("mat:rotDamping"));
		}
		Accessor<Real>			angle;
		Accessor<Real>			stiffness;
		Accessor<Real>			damping;
};

class FE_DL_EXPORT ContactRV : public RecordView
{
	public:
		ContactRV(void) { setName("ContactRV"); }
virtual	void	addFunctors(void)
		{
			add(frame,		FE_USE("spc:frame"));
			add(location,	FE_USE("spc:location"));
			add(velocity,	FE_USE("spc:velocity"));
			add(normal,		FE_USE("spc:normal"));
			add(depth,		FE_USE("spc:depth"));
			add(stiffness,	FE_USE("mat:stiffness"));
			add(damping,	FE_USE("mat:damping"));
			add(group,		FE_USE("group"));
			add(transient,	FE_USE("generic:mark"));
		}
		Functor<Record>				frame;
		Functor<SpatialVector>		location;
		Functor<SpatialVector>		velocity;
		Functor<SpatialVector>		normal;
		Functor<Real>				depth;
		Functor<Real>				stiffness;
		Functor<Real>				damping;
		Functor< sp<RecordGroup> >	group;
		Functor<bool>				transient;
};


class FE_DL_EXPORT AsRelocate :
	public AsFrame,
	public Initialize<AsRelocate>
{
	public:
		void initialize(void)
		{
			add(group,			FE_USE("group"));
		}
		Accessor< sp<RecordGroup> >			group;
};

class FE_DL_EXPORT AsReferenceFrame :
	public AsFrame,
	public Initialize<AsReferenceFrame>
{
	public:
		void initialize(void)
		{
			add(is,				FE_USE("sim:refframe"));
		}
		Accessor<void>			is;
};

class FE_DL_EXPORT AsSmoothedParticle :
	public AsParticle, public AsBounded,
	public Initialize<AsSmoothedParticle>
{
	public:
		void initialize(void)
		{
			add(density,		FE_USE("sim:density"));
			add(heat,			FE_USE("sim:heat"));
			add(heatAccumulator,FE_USE("sim:heat_accum"));
		}
		// density
		Accessor<Real>			density;
		Accessor<Real>			heat;
		Accessor<Real>			heatAccumulator;
};

class FE_DL_EXPORT AsActuator :
	public AccessorSet,
	public Initialize<AsActuator>
{
	public:
		void initialize(void)
		{
			add(transform,	FE_USE("liquid:transform"));
			add(velocity,	FE_USE("liquid:velocity"));
			add(spin,		FE_USE("liquid:spin"));
			add(radius,		FE_USE("liquid:radius"));
			add(mode,		FE_USE("liquid:mode"));
		}

		/// location, rotation, scale
		Accessor<SpatialTransform>	transform;

		// velocity might be useful to make interaction robust
		Accessor<SpatialVector>		velocity;

		// axis and magnitude of 'spin'
		Accessor<SpatialVector>		spin;

		// radius of influence
		Accessor<Real>				radius;

		// mode (attract, repulse, other)
		Accessor<int>				mode;

};

} /* namespace */
} /* namespace */


#endif /* __mechanics_mechanicsAS_h__ */


