import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs + [
            'fexSignalLib',
            'fexSolveDLLib',
            'fexTireDLLib',
            'fexDataToolLib',
            'fexEvaluateDLLib',
            "fexThreadDLLib",
            "fexWindowLib",
            "fexViewerDLLib",
            "fexViewerSystemDLLib",
            'feMathLib' ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexMechanicsDLLib" ]

    tests = [ 'xTireRig', 'xTireViewer' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        #exe.linkmap['tbb'] = ' -ltbb'

        forge.deps([t + "Exe"], deplibs)

    plugin_path = os.path.join(module.modPath, 'plugin')

    forge.tests += [
        ("xTireRig.exe",        "", "", None),
        ("xTireViewer.exe",     "", "", None) ]
