/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_fe_signal_h__
#define __signal_fe_signal_h__

/**	A proxy handler component for python derived handlers.
	*/
class PyHandler : public fe::HandlerI
{
	public:
		PyHandler(PyObject *pPyObject);
virtual	~PyHandler(void);
virtual	void	handle(fe::Record &signal);
virtual	void	bind(fe::sp<fe::SignalerI> spS, fe::sp<fe::Layout> spL);
	private:
		PyObject *m_pObject;
};

/**	The base class for HandlerFS to enable HandlerFS to be python derivable.
	*/
class HandlerBase
{
	public:
		HandlerBase(void);
		HandlerBase(PyObject *pP);
virtual	~HandlerBase(void);
};

/**	HandlerI Function Set.  This is for python exposure of both python and C++
	derived handlers.  C++ derived handler components must be derived from
	fe::HandlerI.
	*/
class HandlerFS : public HandlerBase, public FunctionSet<fe::HandlerI>
{
	public:
		HandlerFS(PyObject *self_, PyObject *pP):
				FunctionSet<fe::HandlerI>(pP)								{}
		HandlerFS(PyObject *self_);
		PyObject	*object(void);
virtual	~HandlerFS(void);
};


/** SignalerI Function Set.
	*/
class SignalerFS : public FunctionSet<fe::SignalerI>
{
	public:
		SignalerFS(PyObject *pP) : FunctionSet<fe::SignalerI>(pP) {};
		void	insert(HandlerFS &handlerPtr, const LayoutPtr &layoutPtr);
		void	remove(HandlerFS &handlerPtr, const LayoutPtr &layoutPtr);
		void	signal(PyObject *pRecord);
		void	dump(void);
};

/** Parser Function Set.
	*/
class ParserFS : public FunctionSet<fe::ParserI>
{
	public:
		ParserFS(PyObject *pP) : FunctionSet<fe::ParserI>(pP) {};
		void	parse(PyObject *pTokens);
};

/**	SequencerI Function Set
	*/
class SequencerFS : public FunctionSet<fe::SequencerI>
{
	public:
		SequencerFS(PyObject *pP) : FunctionSet<fe::SequencerI>(pP) {};
		U32			getCurrentTime(void);
		void		setTime(	U32 counter);
		PyObject	*add(		const LayoutPtr &layoutPtr,
								U32 startTime,
								U32 interval);
		PyObject	*lookup(	const LayoutPtr &layoutPtr,
								U32 interval);
		bool		remove(		PyObject *record);
};

#endif /* __signal_fe_signal_h__ */

