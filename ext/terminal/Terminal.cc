/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terminal/terminal.pmh>

#define	FE_TERM_DEBUG		FALSE
#define	FE_TERM_TIME_DEBUG	FALSE
#define	FE_TERM_COPY_DEBUG	FALSE

namespace fe
{
namespace ext
{

/***
	incoming commands:
		<result> = set "callback" <pointer>
		<result> = set <name> <variable> <property> <type> <value>
		<result> = set MASTER <variable> <property> <type> <value>

		<result> = manage <dl>
		<result> = unmanage <dl>
		<result> = create <implementation> [name|surf]
		<result> = signal <name> [surfOut] [surfIn0] ...
		<result> = release <name>

		<result> = load <surf> <filename> [options]
		<result> = save <surf> <filename> [options]
		<result> = clone <surf> <surf2>
		<result> = count <surf> point|primitive
		<result> = access <surf> point|primitive <attribute>

	callback requests:
		<value> = get <name> <variable> <property>
		<value> = get MASTER <variable> <property>
		<value> = keys <name>
		<value> = keys MASTER

	issues:
		need an abstracted get() to be used by the HandlerI instance
*/

Terminal::Terminal(void):
	m_frame(0.0)
{
#if FE_TERM_DEBUG
	feLog("Terminal::Terminal\n");
#endif
}

Terminal::~Terminal(void)
{
#if FE_TERM_DEBUG
	feLog("Terminal::~Terminal\n");
#endif

#if FALSE
	for(std::map<String,ComponentInfo>::const_iterator it=m_surfaceMap.begin();
			it!=m_surfaceMap.end();it++)
	{
		const ComponentInfo& rComponentInfo=it->second;
		feLog("  %s %d %d\n",it->first.c_str(),
				rComponentInfo.m_spComponent.isValid(),
				rComponentInfo.m_serial);
	}
#endif
}

void Terminal::initialize(void)
{
#if FE_TERM_DEBUG
	feLog("Terminal::initialize\n");
#endif

	feRegisterSegvHandler();

	m_spScope=registry()->master()->catalog()->catalogComponent(
			"Scope","SimScope");
//	FEASSERT(m_spScope.isValid());

	if(m_spScope.isNull())
	{
		feX(e_cannotCreate,"Terminal::initialize",
				"failed to create a Scope");
	}
}

String Terminal::execute(String a_commandline)
{
#if FE_TERM_DEBUG
	feLog("Terminal::execute \"%s\"\n",a_commandline.c_str());
#endif

	const String command=a_commandline.parse("\"'");

	if(command=="keys")
	{
		const String target=a_commandline.parse("\"'");

		sp<Catalog> spCatalog;
		if(target=="MASTER")
		{
			spCatalog=registry()->master()->catalog();
		}
		else
		{
			if(m_nodeMap.find(target)==m_nodeMap.end())
			{
				return "error: unknown target";
			}

			spCatalog=m_nodeMap[target].catalog();
		}

		String result;

		Array<String> keys;
		spCatalog->catalogKeys(keys);

		const I32 keyCount=keys.size();
		for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			const String key=keys[keyIndex];

			const fe_type_info& typeInfo=spCatalog->catalogTypeInfo(key);

			String keyType;
			if(typeInfo==getTypeId<bool>())
			{
				keyType="bool";
			}
			else if(typeInfo==getTypeId<String>())
			{
				keyType="string";
			}
			else if(typeInfo==getTypeId<I32>())
			{
				keyType="integer";
			}
			else if(typeInfo==getTypeId<Real>())
			{
				keyType="real";
			}
			else if(typeInfo==getTypeId<SpatialVector>())
			{
				keyType="vector3";
			}

			result+=(keyIndex? " \"": "\"")+key+"\"="+keyType;
		}

		return result;
	}

	const BWORD isGet=(command=="get");
	if(isGet || command=="set")
	{
		const String target=a_commandline.parse("\"'");
		if(target=="callback")
		{
			String pointer=a_commandline.parse("\"'");

			sscanf(pointer.c_str(),"%p",&m_fpCallback);
			return "changed callback";
		}

		if(target=="frame")
		{
			String frame=a_commandline.parse("\"'");

			double value(0);
			sscanf(frame.c_str(),"%lf",&value);

			m_frame=value;
			return "changed frame";
		}

		ComponentInfo& rComponentInfo=m_surfaceMap[target];
		sp<SurfaceAccessorI> spSurfaceAccessorI=rComponentInfo.m_spComponent;
		if(spSurfaceAccessorI.isValid())
		{
			String indexString=a_commandline.parse();

			I32 index=0;
			sscanf(indexString.c_str(),"%d",&index);

			const String valueString=a_commandline.parse();

			String& rTypeName=rComponentInfo.m_typeName;
			if(rTypeName.empty())
			{
				rTypeName=spSurfaceAccessorI->type();
			}
//			feLog("target %s type \"%s\"\n",target.c_str(),rTypeName.c_str());

			if(rTypeName=="string")
			{
				if(isGet)
				{
					return spSurfaceAccessorI->string(index);
				}
				spSurfaceAccessorI->set(index,valueString);
			}
			if(rTypeName=="integer")
			{
				if(isGet)
				{
					return fe::print(spSurfaceAccessorI->integer(index));
				}
				const I32 value=atol(valueString.c_str());
				spSurfaceAccessorI->set(index,value);
			}
			if(rTypeName=="real")
			{
				if(isGet)
				{
					return fe::print(spSurfaceAccessorI->real(index));
				}
				const Real value=atof(valueString.c_str());
				spSurfaceAccessorI->set(index,value);
			}
			if(rTypeName=="vector3")
			{
				if(isGet)
				{
					return fe::print(spSurfaceAccessorI->spatialVector(index));
				}
				double x(0);
				double y(0);
				double z(0);
				sscanf(valueString.c_str(),"%lf%lf%lf",&x,&y,&z);

				SpatialVector value(x,y,z);

				spSurfaceAccessorI->set(index,value);
			}
			if(rTypeName=="vertices")
			{
				if(isGet)
				{
					String result;

					const I32 subCount=spSurfaceAccessorI->subCount(index);
					for(I32 subIndex=0;subIndex<subCount;subIndex++)
					{
						String number;
						number.sPrintf("%d",
								spSurfaceAccessorI->integer(index,subIndex));

						result+=(subIndex? " ": "") + number;
					}

					return result;
				}

				//* TODO set vertices
			}

			return "";
		}

		sp<Catalog> spCatalog;
		if(target=="MASTER")
		{
			spCatalog=registry()->master()->catalog();
		}
		else
		{
			if(m_nodeMap.find(target)==m_nodeMap.end())
			{
				return "error: unknown target";
			}

			spCatalog=m_nodeMap[target].catalog();
		}

		const String variable=a_commandline.parse("\"'");
		if(variable.empty())
		{
			return "error: missing variable name";
		}

		const String property=a_commandline.parse("\"'");
		if(property.empty())
		{
			return "error: missing property name";
		}

//		feLog("variable \"%s\" property \"%s\"\n",
//				variable.c_str(),property.c_str());

		if(isGet)
		{
			if(!spCatalog->cataloged(variable,property))
			{
//				return "error: missing variable";
				return "";
			}

			const Instance& rInstance=
					spCatalog->catalogInstance(variable,property);

			const sp<BaseType> spBaseType=rInstance.type();
			if(spBaseType.isNull())
			{
				return "error: no type";
			}

			const TypeInfo& rTypeInfo=spBaseType->typeinfo();

			String result;

			if(rTypeInfo==getTypeId<String>())
			{
				result=spCatalog->catalog<String>(variable,property);
			}
			else if(rTypeInfo==getTypeId<bool>())
			{
				result.sPrintf("%d",spCatalog->catalog<bool>(variable,property));
			}
			else if(rTypeInfo==getTypeId<I32>())
			{
				result.sPrintf("%d",spCatalog->catalog<I32>(variable,property));
			}
			else if(rTypeInfo==getTypeId<Real>())
			{
				result.sPrintf("%G",spCatalog->catalog<Real>(variable,property));
			}
			else if(rTypeInfo==getTypeId<SpatialVector>())
			{
				result.sPrintf("%s",c_print(spCatalog->catalog<SpatialVector>(
						variable,property)));
			}
			else if(rTypeInfo==getTypeId< sp<Component> >())
			{
				result.sPrintf("%p",spCatalog->catalog< sp<Component> >(
						variable,property).raw());
			}
			else if(rTypeInfo==getTypeId< sp<Catalog> >())
			{
				result.sPrintf("%p",spCatalog->catalog< sp<Catalog> >(
						variable,property).raw());
			}
			else
			{
				result="error: unrecognized type";
			}
			return result;
		}
		else
		{
			const String type=a_commandline.parse("\"'");
			if(type.empty())
			{
				return "error: missing type";
			}
//			feLog("type \"%s\"\n",type.c_str());

			const String value=a_commandline.parse("\"'");
			if(value.empty() && type!="string")
			{
				return "error: missing value";
			}
//			feLog("value \"%s\"\n",value.c_str());

			try
			{
				if(type=="string")
				{
					spCatalog->catalog<String>(variable,property)=value;
				}
				else if(type=="boolean")
				{
					spCatalog->catalog<bool>(variable,property)=
							(value.integer()!=0 || value=="TRUE" ||
							value=="True" || value=="true");
				}
				else if(type=="integer")
				{
					spCatalog->catalog<I32>(variable,property)=value.integer();
				}
				else if(type=="real")
				{
					spCatalog->catalog<Real>(variable,property)=value.real();
				}
				else if(type=="vector3")
				{
					double x(0);
					double y(0);
					double z(0);
					sscanf(value.c_str(),"%lf%lf%lf",&x,&y,&z);
					spCatalog->catalog<SpatialVector>(variable,property)=
							SpatialVector(x,y,z);
				}
				else
				{
					return "error: unknown type";
				}
			}
			catch(...)
			{
				return "error: exception";
			}
			return "changed catalog";
		}
	}

	if(command=="manage")
	{
		const String name=a_commandline.parse("\"'");

		Result result=registry()->manage(name);
		if(successful(result))
		{
			return "managed";
		}
		return "error: unknown library";
	}

	if(command=="unmanage")
	{
		const String name=a_commandline.parse("\"'");

		Result result=registry()->unmanage(name);
		if(successful(result))
		{
			return "unmanaged";
		}
		return "error: unknown library";
	}

	if(command=="create")
	{
		const String implementation=a_commandline.parse("\"'");
		if(implementation.empty())
		{
			return "error: missing implementation";
		}

		const String name=a_commandline.parse("\"'");
		if(name.empty())
		{
			return "error: missing name";
		}

		m_nodeMap[name].setupOperator(m_spScope,implementation,name);
		if(m_nodeMap[name].catalog().isNull())
		{
			return "error: invalid operator";
		}

		return "created";
	}

	if(command=="release")
	{
		const String name=a_commandline.parse("\"'");

		if(m_nodeMap.find(name)!=m_nodeMap.end())
		{
			m_nodeMap.erase(name);
			return "released node";
		}
		else if(m_surfaceMap.find(name)!=m_surfaceMap.end())
		{
#if FE_TERM_COPY_DEBUG
			feLog("Terminal::execute release \"%s\"\n",name.c_str());
#endif

			m_surfaceMap.erase(name);
			return "released surface";
		}

		return "error: unknown target";
	}

	if(command=="signal")
	{
#if	FE_TERM_TIME_DEBUG
		const U32 us1=systemMicroseconds();
#endif

		const String instance=a_commandline.parse("\"'");

		std::map<String,TerminalNode>::iterator it=m_nodeMap.find(instance);
		if(it==m_nodeMap.end())
		{
			return "error: invalid node";
		}

		sp<Catalog> spCatalog=it->second.catalog();
		FEASSERT(spCatalog.isValid());

		if(spCatalog.isNull())
		{
			return "error: invalid operator";
		}

		BWORD recycle=FALSE;
		String copy;
		String outputName;
		String outputKey;

		BWORD copied=FALSE;

#if FE_TERM_COPY_DEBUG
		feLog("Terminal::execute signal \"%s\"\n",instance.c_str());
#endif

		I32 surfaceIndex= -1;
		while(TRUE)
		{
			sp<SurfaceAccessibleI> spSurfaceAccessibleI;

			const String surfaceName=a_commandline.parse("\"'");
			if(surfaceIndex<0)
			{
				outputName=surfaceName;
			}
			if(surfaceName.empty())
			{
				if(a_commandline.empty())
				{
					break;
				}
			}
			else if(m_surfaceMap.find(surfaceName)!=m_surfaceMap.end())
			{
				spSurfaceAccessibleI=
						m_surfaceMap[surfaceName].m_spComponent;
			}
			else if(surfaceIndex>=0)
			{
				return "error: unknown surface \"" + surfaceName + "\"";
			}

			if(surfaceIndex>=0 && spSurfaceAccessibleI.isNull())
			{
				return "error: invalid surface \"" + surfaceName + "\"";
			}

			//* multiple inputs (named by index)

			String lookup="output:0";
			if(surfaceIndex>=0)
			{
				lookup.sPrintf("input:%d",surfaceIndex);
			}
			const String key=spCatalog->catalog<String>(lookup);

			if(surfaceIndex<0)
			{
				if(spCatalog->cataloged(key))
				{
					copy=spCatalog->catalogOrDefault<String>(key,
							"copy","");

					recycle=spCatalog->catalogOrDefault<bool>(key,
							"recycle",false);
				}

#if FE_TERM_COPY_DEBUG
				feLog("  output \"%s\" copies \"%s\" recycle %d\n",
						key.c_str(),copy.c_str(),recycle);
#endif
			}
			else
			{
				ComponentInfo& rComponentInfo=m_surfaceMap[outputName];
				std::vector<String>& rInputKey=rComponentInfo.m_inputKey;
				std::vector<U32>& rInputSerial=rComponentInfo.m_inputSerial;

				if(I32(rInputKey.size())<surfaceIndex+1)
				{
					rInputKey.resize(surfaceIndex+1);
					rInputSerial.resize(surfaceIndex+1);
				}

				String& rLastKey=rInputKey[surfaceIndex];
				U32& rLastSerial=rInputSerial[surfaceIndex];

				const U32 serial=m_surfaceMap[surfaceName].m_serial;

				const BWORD replaced=
						(key!=rLastKey || serial!=rLastSerial);
				if(replaced && recycle && key==copy)
				{
					recycle=FALSE;

#if FE_TERM_COPY_DEBUG
					feLog("  recycle off\n");
#endif
				}

#if FE_TERM_COPY_DEBUG
				feLog("  input \"%s\"/\"%s\" %d/%d replaced %d\n",
						key.c_str(),rLastKey.c_str(),
						serial,rLastSerial,replaced);
#endif

				spCatalog->catalog<bool>(key,"replaced")=replaced;

				rLastKey=key;
				rLastSerial=serial;
			}

//			feLog("surface \"%s\" is really \"%s\"\n",
//					lookup.c_str(),key.c_str());

			spCatalog->catalog< sp<Component> >(key)=spSurfaceAccessibleI;

			//* TODO respect recycle
			if(surfaceIndex>=0 && key==copy)
			{
				ComponentInfo& rComponentInfo=m_surfaceMap[outputName];

				if(spSurfaceAccessibleI.isValid() && (!recycle ||
						rComponentInfo.m_spComponent.isNull()))
				{
#if FE_TERM_COPY_DEBUG
					feLog("  clone \"%s\" \"%s\" to \"%s\"\n",
							spSurfaceAccessibleI->name().c_str(),
							key.c_str(),outputKey.c_str());
#endif

					sp<SurfaceAccessibleI> spOutputAccessible(
							fe_cast<SurfaceAccessibleI>(
							spSurfaceAccessibleI->clone()));
					FEASSERT(spOutputAccessible.isValid());

					spOutputAccessible->setName(spSurfaceAccessibleI->name());
//					spOutputAccessible->setName(outputName);

					spCatalog->catalog< sp<Component> >(outputKey)=
							spOutputAccessible;

					rComponentInfo.m_serial++;
					rComponentInfo.m_spComponent=spOutputAccessible;
					copied=TRUE;
				}
			}
			else if(surfaceIndex<0)
			{
				outputKey=key;
			}

			surfaceIndex++;
		}

		if(!copied)
		{
			sp<SurfaceAccessibleI> spOutputAccessible=
					m_surfaceMap[outputName].m_spComponent;

			if(!recycle || spOutputAccessible.isNull())
			{
#if FE_TERM_COPY_DEBUG
				feLog("  new surface recycle %d null %d\n",
						recycle,spOutputAccessible.isNull());
#endif

				sp<Registry> spRegistry=m_spScope->registry();

				//* Record based
				spOutputAccessible=
						spRegistry->create("*.SurfaceAccessibleRecord");
				if(spOutputAccessible.isValid())
				{
					sp<Scope> spScope=registry()->create("Scope");
					spOutputAccessible->bind(spScope);
					spOutputAccessible->clear();
				}

				if(spOutputAccessible.isNull())
				{
					//* NOTE not verified

					//* json based
					sp<SurfaceAccessibleI> spOutputAccessible=
							spRegistry->create("SurfaceAccessibleI.*.*.geo");

					if(spOutputAccessible.isValid())
					{
						//* initialize empty
						spOutputAccessible=fe_cast<SurfaceAccessibleI>(
								spOutputAccessible->clone());
					}
				}

				if(spOutputAccessible.isNull())
				{
					return "error: failed to create output surface";
				}

//				spOutputAccessible->setName(outputName);

				spCatalog->catalog< sp<Component> >(outputKey)=
						spOutputAccessible;

				m_surfaceMap[outputName].m_spComponent=
						spOutputAccessible;
			}
		}

#if	FE_TERM_TIME_DEBUG
		const U32 us2=systemMicroseconds();
#endif

		const BWORD success=m_nodeMap[instance].cook(m_frame);

#if	FE_TERM_TIME_DEBUG
		const U32 us3=systemMicroseconds();
#endif

		if(!success)
		{
#if FE_TERM_COPY_DEBUG
			feLog("Terminal::execute erase \"%s\"\n",outputName.c_str());
#endif

			m_surfaceMap.erase(outputName);
		}
		else
		{
			sp<SurfaceAccessibleI> spOutputAccessible=
					spCatalog->catalog< sp<Component> >(outputKey);
			if(m_surfaceMap[outputName].m_spComponent!=spOutputAccessible)
			{
				m_surfaceMap[outputName].m_spComponent=spOutputAccessible;
				m_surfaceMap[outputName].m_serial=1;

#if FE_TERM_COPY_DEBUG
#endif
				feLog("Terminal::execute output replaced\n");
			}
		}

/*
		// TEMP make request directly
		if(!m_fpCallback)
		{
			return "error: no callback";
		}

		String request;
		request.sPrintf("get %s args",instance.c_str());

#if FE_TERM_DEBUG
		feLog("Terminal::execute request \"%s\"\n",request.c_str());
#endif

		const std::string response=m_fpCallback(request.c_str());

#if FE_TERM_DEBUG
		feLog("Terminal::execute response \"%s\"\n",response.c_str());
#endif
*/

#if	FE_TERM_TIME_DEBUG
		const U32 us4=systemMicroseconds();
		feLog("Terminal::execute signal cooked \"%s\" %d/%d us\n",
				instance.c_str(),us3-us2,us4-us1);
#endif

		return success? "signaled": "error: signal failed";
	}

	if(command=="load")
	{
		const String surfaceName=a_commandline.parse("\"'");
		if(surfaceName.empty())
		{
			return "error: surface name missing";
		}

		const String filename=a_commandline.parse("\"'");
		if(filename.empty())
		{
			return "error: filename missing";
		}

		const String options=a_commandline.parse("\"'");

		const String suffix=String(strrchr(filename.c_str(),'.')).prechop(".");

		ComponentInfo& rComponentInfo=m_surfaceMap[surfaceName];
		rComponentInfo.m_serial++;

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				rComponentInfo.m_spComponent;

		sp<Catalog> spSettings=
				registry()->master()->createCatalog("Load Settings");
		spSettings->catalog<Real>("frame")=m_frame;

		if(!options.empty())
		{
			spSettings->catalog<String>("options")=options;
		}

		sp<Registry> spRegistry=m_spScope->registry();
		spSurfaceAccessibleI=
				spRegistry->create("SurfaceAccessibleI.*.*."+suffix);
		rComponentInfo.m_spComponent=spSurfaceAccessibleI;

		if(spSurfaceAccessibleI.isNull())
		{
			return "error: create driver failed";
		}

		spSurfaceAccessibleI->setName(surfaceName);
		spSurfaceAccessibleI->bind(sp<Scope>(spRegistry->create("Scope")));

		const BWORD success=spSurfaceAccessibleI->load(filename,spSettings);

#if FALSE
		//* HACK convert to another format
		sp<SurfaceAccessibleI> spSurfaceAccessible2=
				spRegistry->create("*.SurfaceAccessibleOpenCL");
		spSurfaceAccessible2->copy(spSurfaceAccessibleI);
		rComponentInfo.m_spComponent=spSurfaceAccessible2;
#endif

//		feLog("Terminal::execute loaded \"%s\" to \"%s\"\n",
//				filename.c_str(),spSurfaceAccessibleI->name().c_str());

		return success? "loaded": "error: load failed";
	}

	if(command=="save")
	{
		const String surfaceName=a_commandline.parse("\"'");
		if(surfaceName.empty())
		{
			return "error: surface name missing";
		}

		if(m_surfaceMap.find(surfaceName)==m_surfaceMap.end())
		{
			return "error: unknown surface";
		}

		const String filename=a_commandline.parse("\"'");
		if(filename.empty())
		{
			return "error: filename missing";
		}

		const String options=a_commandline.parse("\"'");

		ComponentInfo& rComponentInfo=m_surfaceMap[surfaceName];

		if(rComponentInfo.m_spComponent.isNull())
		{
			return "error: null surface";
		}

		const String suffix=String(strrchr(filename.c_str(),'.')).prechop(".");
		const String dotPattern="SurfaceAccessibleI.*.*."+suffix;
		const String existingFactoryName=
				rComponentInfo.m_spComponent->factoryName();

//		feLog("Terminal::execute save \"%s\" to \"%s\"\n",
//				surfaceName.c_str(),filename.c_str());
//		feLog("Terminal::execute save \"%s\" vs \"%s\"\n",
//				existingFactoryName.c_str(),dotPattern.c_str());

		//* NOTE don't copy if it's already the correct type
		sp<SurfaceAccessibleI> spSaveAccessible=rComponentInfo.m_spComponent;
		if(!existingFactoryName.dotMatch(dotPattern))
		{
			spSaveAccessible=m_spScope->registry()->create(dotPattern);

//			feLog("Terminal::execute copy to \"%s\"\n",
//					spSaveAccessible->factoryName().c_str());

			spSaveAccessible->bind(m_spScope);
			spSaveAccessible->copy(rComponentInfo.m_spComponent);
		}

		if(spSaveAccessible.isNull())
		{
			return "error: unsupported format";
		}

		sp<Catalog> spSettings=
				registry()->master()->createCatalog("Load Settings");
		spSettings->catalog<Real>("frame")=m_frame;

		if(options.empty())
		{
			spSettings->catalog<String>("options")=options;
		}

		const BWORD success=spSaveAccessible->save(filename,spSettings);

		return success? "saved": "error: save failed";
	}

	if(command=="clone")
	{
		const String surfaceName=a_commandline.parse("\"'");
		if(surfaceName.empty())
		{
			return "error: source surface name missing";
		}

		if(m_surfaceMap.find(surfaceName)==m_surfaceMap.end())
		{
			return "error: unknown source surface";
		}

		const String destname=a_commandline.parse("\"'");
		if(destname.empty())
		{
			return "error: destination surface name missing";
		}

		ComponentInfo& rSourceInfo=m_surfaceMap[surfaceName];
		if(rSourceInfo.m_spComponent.isNull())
		{
			return "error: null source surface";
		}

		sp<SurfaceAccessibleI> spSourceAccessible=rSourceInfo.m_spComponent;
		sp<Protectable> spProtectable(spSourceAccessible->clone());
		sp<SurfaceAccessibleI> spCloneAccessible(spProtectable);

		if(spCloneAccessible.isValid())
		{
			spCloneAccessible->setName(spSourceAccessible->name());
		}

		ComponentInfo& rCloneInfo=m_surfaceMap[surfaceName];
		rCloneInfo.m_serial++;
		rCloneInfo.m_spComponent=spCloneAccessible;

		return "cloned";
	}

	if(command=="count")
	{
		const String surfaceName=a_commandline.parse();
		if(surfaceName.empty() ||
				m_surfaceMap.find(surfaceName)==m_surfaceMap.end())
		{
			return "0";
		}

		const String elementName=a_commandline.parse();
		if(elementName.empty() ||
				!(elementName=="point" || elementName=="primitive"))
		{
			return "0";
		}

		ComponentInfo& rComponentInfo=m_surfaceMap[surfaceName];
		if(rComponentInfo.m_spComponent.isNull())
		{
			return "0";
		}

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				rComponentInfo.m_spComponent;

		String result;
		result.sPrintf("%d",spSurfaceAccessibleI->count(
				elementName=="primitive"?
				SurfaceAccessibleI::e_primitive:
				SurfaceAccessibleI::e_point));

		return result;
	}

	if(command=="specs")
	{
		const String surfaceName=a_commandline.parse();
		if(surfaceName.empty() ||
				m_surfaceMap.find(surfaceName)==m_surfaceMap.end())
		{
			return "";
		}

		const String elementName=a_commandline.parse();
		if(elementName.empty() ||
				!(elementName=="point" || elementName=="primitive"))
		{
			return "";
		}

		ComponentInfo& rComponentInfo=m_surfaceMap[surfaceName];
		if(rComponentInfo.m_spComponent.isNull())
		{
			return "";
		}

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				rComponentInfo.m_spComponent;

		SurfaceAccessibleI::Element element=
				(elementName=="primitive")?
				SurfaceAccessibleI::e_primitive:
				SurfaceAccessibleI::e_point;

		String result;

		Array<SurfaceAccessibleI::Spec> specs;
		spSurfaceAccessibleI->attributeSpecs(specs,element);

		const U32 specCount=specs.size();
		for(U32 specIndex=0;specIndex<specCount;specIndex++)
		{
			const SurfaceAccessibleI::Spec& spec=specs[specIndex];
			const String& specName=spec.name();
			const String& specType=spec.typeName();

			result+=(specIndex? ",": "")+specType+" "+specName;

//			feLog("spec %d/%d \"%s\" \"%s\"\n",specIndex,specCount,
//					specType.c_str(),specName.c_str());
		}

		return result;
	}

	if(command=="access")
	{
		const String surfaceName=a_commandline.parse();
		if(surfaceName.empty() ||
				m_surfaceMap.find(surfaceName)==m_surfaceMap.end())
		{
			return "";
		}

		const String elementName=a_commandline.parse();
		if(elementName.empty() ||
				!(elementName=="point" || elementName=="primitive"))
		{
			return "";
		}

		const String attribute=a_commandline.parse();
		if(attribute.empty())
		{
			return "";
		}

		ComponentInfo& rSurfaceInfo=m_surfaceMap[surfaceName];
		if(rSurfaceInfo.m_spComponent.isNull())
		{
			return "";
		}

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				rSurfaceInfo.m_spComponent;
		if(spSurfaceAccessibleI.isNull())
		{
			return "";
		}

		const String accessorName=surfaceName+":"+elementName+":"+attribute;
		ComponentInfo& rAccessorInfo=m_surfaceMap[accessorName];
		rAccessorInfo.m_serial++;

		SurfaceAccessibleI::Element element=
				(elementName=="primitive")?
				SurfaceAccessibleI::e_primitive:
				SurfaceAccessibleI::e_point;

		Array<SurfaceAccessibleI::Spec> specs;
		spSurfaceAccessibleI->attributeSpecs(specs,element);

		const U32 specCount=specs.size();
		for(U32 specIndex=0;specIndex<specCount;specIndex++)
		{
			const SurfaceAccessibleI::Spec& spec=specs[specIndex];
			const String& specName=spec.name();
			const String& specType=spec.typeName();

			if(specName==attribute)
			{
				rAccessorInfo.m_typeName=specType;
			}
		}

		sp<SurfaceAccessorI> spSurfaceAccessorI;

		if(attribute=="<vertices>")
		{
			rAccessorInfo.m_typeName="vertices";

			spSurfaceAccessorI=spSurfaceAccessibleI->accessor(element,
					SurfaceAccessibleI::e_vertices);
		}
		else
		{
			spSurfaceAccessorI=
					spSurfaceAccessibleI->accessor(element,attribute);
		}

		rAccessorInfo.m_spComponent=spSurfaceAccessorI;

		if(spSurfaceAccessorI.isNull())
		{
			return "";
		}

		return accessorName;
	}

	return "error: unknown command";
}

} /* namespace ext */
} /* namespace fe */
