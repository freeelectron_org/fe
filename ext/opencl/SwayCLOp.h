/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_SwayCLOp_h__
#define __opencl_SwayCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to oscillate curves, bending from the root

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT SwayCLOp:
	public OpenCLOp,
	public Initialize<SwayCLOp>
{
	public:
				SwayCLOp(void)												{}
virtual			~SwayCLOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_SwayCLOp_h__ */
