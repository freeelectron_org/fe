/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_h__
#define __platform_h__

#include "fe/config.h"
#include "platform/define.h"

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#define _CRT_RAND_S
#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif
#define FE_CPLUSPLUS	_MSVC_LANG
#else
#define FE_CPLUSPLUS	__cplusplus
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <cstddef>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <stack>
#include <queue>
#include <algorithm>
#include <functional>
#include <atomic>

// hash_map is not 'standard', so we have some hoops
#if defined(FE_UNORDERED_MAP) && FE_CPLUSPLUS >= 201103L
#include <unordered_map>
#include <unordered_set>
#elif defined(HASH_MAP_IS_MAP)
namespace fe_hash
{
	template<class k, class v, class h, class e>
	class hash_map : public std::map< k,v > { };
	template<class k, class h, class e>
	class hash_set : public std::set< k > { };
}
//#elif defined(GNU_STL_HASH_MAP)
#elif FE_COMPILER==FE_GNU
#include <ext/hash_map>
#include <ext/hash_set>
namespace fe_hash = __gnu_cxx;
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#include <hash_map>
#include <hash_set>
#else
#include <ext/hash_map>
#include <ext/hash_set>
#endif

#if defined(FE_UNORDERED_MAP) && FE_CPLUSPLUS >= 201103L
namespace fe
{
	template<class k, class v, class h, class e>
	class HashMap : public std::unordered_map< k,v,h,e > { };
	template<class k, class h, class e>
	class HashSet : public std::unordered_set< k,h,e > { };
}
#else
namespace fe
{
	template<class k, class v, class h, class e>
	class HashMap : public ::fe_hash::hash_map< k,v,h,e > { };
	template<class k, class h, class e>
	class HashSet : public ::fe_hash::hash_set< k,h,e > { };
}
#endif

#include <time.h>

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64

	#include <CRTDbg.h>
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
	#if FE_COMPILER!=FE_DMC
	#ifndef AF_IPX
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#endif
	#endif

	#include <windows.h>	//* causes wacky struct errors (???)
	#include <mmsystem.h>
	#include <direct.h>

	#define mkdir _mkdir

	#define FE_MESSAGE_LENGTH_MAX					512

#elif FE_OS==FE_LINUX
	#include <sys/time.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <limits.h>
	#include <unistd.h>
	#include <errno.h>
	#include <dlfcn.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>

	#define FE_MESSAGE_LENGTH_MAX					512

#elif FE_OS==FE_OSX
	#include <sys/time.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <unistd.h>
	#include <errno.h>
	#include <netinet/in.h>
	#include <dlfcn.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	// and I thought MS was presumptuous...check() is a macro for OSX
	#include <AssertMacros.h>
	#ifdef check
	#undef check
	#endif

	#define FE_MESSAGE_LENGTH_MAX					512

#elif FE_OS==FE_CYGWIN
	#include <sys/time.h>
	#include <sys/types.h>
	#include <unistd.h>
	#include <errno.h>
	#include <netinet/in.h>
	#include <dlfcn.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>

	#define FE_MESSAGE_LENGTH_MAX					512
#endif

#if FE_COMPILER==FE_GNU && FE_OS==FE_OSX
#ifndef GCC_HASCLASSVISIBILITY
#define GCC_HASCLASSVISIBILITY
#endif
#endif

#if FE_COMPILER==FE_MICROSOFT
	#define FE_DL_EXPORT	__declspec(dllexport)
	#define FE_DL_IMPORT	__declspec(dllimport)
	#define FE_DL_PUBLIC
	#define FE_DL_LOCAL
#elif defined(GCC_HASCLASSVISIBILITY)
	//* NOTE GCC manual specifies empty FE_DL_IMPORT
	#define FE_DL_EXPORT	__attribute__ ((visibility("default")))
	#define FE_DL_IMPORT
	#define FE_DL_PUBLIC	FE_DL_EXPORT
	#define FE_DL_LOCAL		__attribute__ ((visibility("hidden")))
#else
	#define FE_DL_EXPORT
	#define FE_DL_IMPORT
	#define FE_DL_PUBLIC
	#define FE_DL_LOCAL
#endif

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#define FE_CDECL __cdecl
#else
#define FE_CDECL
#endif

#if FE_COMPILER==FE_GNU || FE_COMPILER==FE_INTEL
	#include <new>
#elif FE_COMPILER==FE_MICROSOFT
	//* DISABLE:  'this' : used in base member initializer list
	#pragma warning( disable : 4355 )
	//* DISABLE:  dll-interface complaints
	#pragma warning( disable : 4275 )
	#pragma warning( disable : 4251 )
	#include <new>
#else
	#include <new.h>
#endif

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	#include <mbstring.h>
#else
	#include <ctype.h>
#endif

#ifdef FE_BOOST_EXCEPTIONS
#include "boost/exception/diagnostic_information.hpp"
#endif

//* fePlatform is a static lib; FE_PLATFORM_PORT should not be defined.
//* You should not be importing or exporting anything from fePlatform.
//* In Win32, you can get multiple symbols and MSVCRT lib problems.
//* For global static variables, put them in memory.h and memory.cc.
//#ifdef MODULE_platform
//#define FE_PLATFORM_PORT FE_DL_EXPORT
//#else
//#define FE_PLATFORM_PORT FE_DL_IMPORT
//#endif

#ifdef FE_EXPORT_MEMORY
#define FE_MEM_PORT	FE_DL_EXPORT
#else
#define FE_MEM_PORT	FE_DL_IMPORT
#endif

#if FE_CODEGEN>FE_DEBUG
#define FEASSERT(condition)
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#define FEASSERT(condition) _ASSERTE(condition)
#else
#define FEASSERT(condition) assert(condition)
#endif

//* suppress 'unused variable' warning/error
#define FE_MAYBE_UNUSED(x) (void)x

#define FE_SSE		FE_USE_SSE

#if FE_USE_PRINTF
#define fe_printf printf
#define fe_fprintf fprintf
#else
inline void fe_printf_ignore(const char*, ...){}
inline void fe_fprintf_ignore(FILE*, const char *, ...){}
#define fe_printf fe_printf_ignore
#define fe_fprintf fe_fprintf_ignore
#endif

#include "platform/datatypes.h"
#include "platform/error.h"
#include "platform/count.h"
#include "platform/String.h"
#include "platform/type_name.h"
#include "platform/Result.h"
#include "platform/DL_Loader.h"
#include "platform/Mutex.h"
#include "platform/memory.h"
#include "platform/Regex.h"
#include "platform/Thread.h"
#include "platform/backtrace.h"
#include "platform/debug.h"
#include "platform/System.h"
#include "platform/Exception.h"
#include "platform/UnitTest.h"

#endif /* __platform_h__ */

