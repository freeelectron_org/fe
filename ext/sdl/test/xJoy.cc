/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "datatool/datatool.h"
#include "window/WindowEvent.h"

using namespace fe;
using namespace fe::ext;

class MyHandler: virtual public HandlerI
{
	public:
				MyHandler(void)			{ setName("MyHandler"); }
virtual	void	bind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)		{}

virtual	void	handle(Record &signal)
				{
					m_event.bind(signal);

					if(!m_event.isPoll())
					{
						feLog("MyHandler::handle %s\n",c_print(m_event));
					}
				}

	private:
		WindowEvent	m_event;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	I32 frames=(argc>1)? atoi(argv[1])+1: -1;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexWindowDL");
		UNIT_TEST(successful(result));

		{
			sp<SignalerI> spSignalerI(spRegistry->create("SignalerI"));
			if(!spSignalerI.isValid())
			{
				feX(argv[0], "couldn't create signaler");
			}

			sp<Scope> spScope=spRegistry->create("Scope");

			WindowEvent pollEvent;
			pollEvent.bind(spScope);
			Record pollRecord=pollEvent.createRecord();
			sp<Layout> pollLayout=pollRecord.layout();
			pollEvent.setSIS(WindowEvent::e_sourceSystem,
					WindowEvent::e_itemPoll,WindowEvent::e_stateNull);

			sp<HandlerI> spJoyHandler=spRegistry->create("JoyI");
			UNIT_TEST(spJoyHandler.isValid());
			if(spJoyHandler.isValid())
			{
				spSignalerI->insert(spJoyHandler,pollLayout);
			}

			sp<HandlerI> spMyHandler(new MyHandler);
			UNIT_TEST(spMyHandler.isValid());
			if(spMyHandler.isValid())
			{
				spSignalerI->insert(spMyHandler,pollLayout);
			}

			for(I32 frame=0;frame!=frames;frame++)
			{
				spSignalerI->signal(pollRecord);
				milliSleep(100);
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(6);
	UNIT_RETURN();
}
