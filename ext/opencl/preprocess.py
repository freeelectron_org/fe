#! /usr/bin/python3

import sys
import re
import time

argCount = len(sys.argv)

if argCount != 3:
    print("Usage: " + sys.argv[0] + " input.cl output.cl.c")
    exit()

filenames = []

argIndex = 1
while True:
    arg = sys.argv[argIndex]
    argIndex += 1

    if True:
        filenames += [ arg ]

    if argIndex >= argCount:
        break;

inputName = filenames[0]
outputName = filenames[1]

input = open(inputName, 'r')
output = open(outputName, 'w')

line = input.readline()
while line:
    text = '"' + line.rstrip("\n").replace('"','\\"') + '\\n"\n'

    output.write(text)

    line = input.readline()

input.close()
output.close()
