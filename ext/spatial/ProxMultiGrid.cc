/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ProxMultiGrid.h"

namespace fe
{
namespace ext
{

ProxMultiGrid::ProxMultiGrid(void)
{
	m_grids.resize(1);
	m_grids[0].m_grid.resize(1);
	setAll(m_grids[0].m_count,1);
	m_grids[0].m_pMGrid = this;
}

ProxMultiGrid::~ProxMultiGrid(void)
{
}

ProxMultiGrid::Grid::Grid(void)
{
}

void ProxMultiGrid::initialize(void)
{
}

void ProxMultiGrid::addGrid(Real cellSize)
{
	int i = m_grids.size();
	m_grids.resize(i+1);
	m_grids[i].m_sz = cellSize;
	m_grids[i].m_pMGrid = this;
	set<Real>(m_boundary,0.0,0.0,0.0);
	// TODO sort grids from biggest cell to smallest
}


#if 1
bool ProxMultiGrid::rangeCheck(LocalRecord &r_a)
{
//fe_fprintf(stderr,"range check %f %f %f\n", r_a.m_location[0], r_a.m_location[1],
		//r_a.m_location[2]);
	bool rv = true;
	for(unsigned int d = 0; d < 3; d++)
	{
		if(r_a.m_location[d] < 0.0)
		{
			rv = false;
		}
		if(r_a.m_location[d] > m_boundary[d])
		{
			rv = false;
		}
	}
	return rv;
}
#endif

void ProxMultiGrid::add(sp<RecordArray> &ra_in, unsigned int i)
{
	LocalRecord lr;
	lr.m_index = i;
	lr.m_pRA = ra_in.raw();
	lr.m_location = m_asParticle.location(ra_in, i) - m_origin;
	lr.m_radius = m_asBounded.radius(ra_in, i);

	register Real rad_chk = 2.0 * lr.m_radius;

	if(rangeCheck(lr))
	{
		for(int i = m_grids.size() - 1; i > 0; i--)
		{
			Grid *pGrid = &(m_grids[i]);
			if(pGrid->m_sz >= rad_chk)
			{
				t_v3i index;
				t_v3i shadow;
//				bool do_shadow = false;
				LocalRecord lr_sh;
				lr_sh = lr;
				for(int j = 0; j < 3; j++)
				{
					index[j] =
						(int)((lr.m_location[j]) / pGrid->m_sz);
				}
				pGrid->m_grid[pGrid->flatten(index)].push_back(lr);
				return;
			}
		}
	}

	m_grids[0].m_grid[0].push_back(lr);
}

void ProxMultiGrid::log()
{
	for(int i = m_grids.size() - 1; i >= 0; i--)
	{
		Grid *pGrid = &(m_grids[i]);
		int cnt = 0;
		for(int j = 0; j < int(pGrid->m_grid.size()); j++)
		{
			cnt += pGrid->m_grid[j].size();
		}
		feLog("grid sz: %f #: %d\n", pGrid->m_sz, cnt);
	}
}

void ProxMultiGrid::setup(sp<Scope> spScope)
{
	m_spScope = spScope;
	m_asParticle.bind(m_spScope);
	m_asBounded.bind(m_spScope);
	m_asProximity.bind(m_spScope);

	m_filters.clear();
	m_filters.push_back(m_asBounded.radius);
	m_filters.push_back(m_asParticle.location);
}

void ProxMultiGrid::setup(Grid &a_grid, SpatialVector &a_extent)
{
	for(int i = 0; i < 3; i++)
	{
		a_grid.m_count[i] = (int)(a_extent[i]/a_grid.m_sz) + 1;
	}

	a_grid.m_grid.resize(a_grid.m_count[0]*a_grid.m_count[1]*a_grid.m_count[2]);

	for(unsigned int i = 0; i < a_grid.m_grid.size(); i++)
	{
		a_grid.m_grid[i].clear();
	}
//fe_fprintf(stderr,"mgrid dimensions %d %d %d\n",a_grid.m_count[0],a_grid.m_count[1],a_grid.m_count[2]);
}

unsigned int ProxMultiGrid::detect(sp<Layout> l_pair, sp<RecordGroup> rg_in, sp<RecordGroup> rg_out)
{
	SystemTicker ticker("ProxMultiGrid");
	unsigned int collisions = 0;

	m_hpPairLayout = l_pair;

	setup(l_pair->scope());

	Real big = 1.0e30;
	Vector<3, Real> hi(-big,-big,-big);
	Vector<3, Real> lo(big,big,big);

	ticker.log("pre extent pass");
	for(RecordGroup::iterator i_in = rg_in->begin(m_filters);
		i_in != rg_in->end(); i_in++)
	{
		ticker.log("pre loop");
		sp<RecordArray> ra_in = *i_in;
//fe_fprintf(stderr," -- length %d\n", ra_in->length());
		for(int i = 0; i < ra_in->length(); i++)
		{
			SpatialVector v = m_asParticle.location(ra_in,i);
			for(int j = 0; j < 3; j++)
			{
				if(v[j] > hi[j]) { hi[j] = v[j]; }
				if(v[j] < lo[j]) { lo[j] = v[j]; }
			}
		}
		ticker.log("post loop");
	}
	ticker.log("extent pass");


	m_origin = lo;
	m_boundary = hi - lo;

//fe_fprintf(stderr, "origin %f %f %f\n", m_origin[0], m_origin[1], m_origin[2]);
//fe_fprintf(stderr, "extent %f %f %f\n", extent[0], extent[1], extent[2]);
//fe_fprintf(stderr, "boundy %f %f %f\n", m_boundary[0], m_boundary[1], m_boundary[2]);

	if(m_boundary[0] > m_boundary[1]) { m_grids[0].m_sz = m_boundary[0]; }
	else { m_grids[0].m_sz = m_boundary[1]; }
	if(m_boundary[2] > m_grids[0].m_sz) { m_grids[0].m_sz = m_boundary[2]; }

	for(unsigned int i = 1; i < m_grids.size(); i++)
	{
		setup(m_grids[i], m_boundary);
	}

	for(unsigned int i = 0; i < m_grids.size(); i++)
	{
		for(unsigned int j = 0; j < m_grids[i].m_grid.size(); j++)
		{
			m_grids[i].m_grid[j].clear();
		}
	}

	for(RecordGroup::iterator i_in = rg_in->begin(m_filters);
		i_in != rg_in->end(); i_in++)
	{
		sp<RecordArray> ra_in = *i_in;
		for(int i = 0; i < ra_in->length(); i++)
		{
			add(ra_in, i);
		}
	}


	ticker.log("populate");
	//log();

	m_poolIndex = 0;
	if(!m_spPool.isValid())
	{
		m_spPool = new RecordArray(l_pair);
	}
	ticker.log("detect0");

	for(unsigned int i = 0; i < m_grids.size(); i++)
	{
		collisions += m_grids[i].detect();
	}
	ticker.log("detect1");

	for(unsigned int i = 0; i < m_grids.size(); i++)
	{
		for(unsigned int j = i+1; j < m_grids.size(); j++)
		{
			collisions += detect(m_grids[i], m_grids[j]);
		}
	}

	ticker.log("detect2");

	if(rg_out.isValid())
	{
		sp<RecordArray> spPA = rg_out->getArray(l_pair);

fe_fprintf(stderr, "pool index %lu\n", m_poolIndex);
		spPA->add(m_spPool, 0, m_poolIndex);
	}

	ticker.log("output");

	return collisions;
}


unsigned int ProxMultiGrid::detect(t_lrecords &a_a)
{
	unsigned int collisions = 0;


	unsigned int sz = a_a.size();

	for(unsigned int i = 0; i < sz; i++)
	{
		for(unsigned int j = i + 1; j < sz; j++)
		{
			if(check(a_a[i], a_a[j]))
			{
				collisions++;
			}
		}
	}
	return collisions;
}

unsigned int ProxMultiGrid::detect(t_lrecords &a_a, t_lrecords &a_b)
{
	unsigned int collisions = 0;

	unsigned int sz_a = a_a.size();
	unsigned int sz_b = a_b.size();

	for(unsigned int i = 0; i < sz_a; i++)
	{
		for(unsigned int j = 0; j < sz_b; j++)
		{
			if(check(a_a[i], a_b[j]))
			{
				collisions++;
			}
		}
	}
	return collisions;
}

bool ProxMultiGrid::check(LocalRecord &r_a, LocalRecord &r_b)
{
	SpatialVector dist = r_a.m_location - r_b.m_location;
	Real dist_sq = dist[0]*dist[0] + dist[1]*dist[1] + dist[2]*dist[2];
	Real rad_sq = r_a.m_radius + r_b.m_radius;
//fe_fprintf(stderr, "CHECK %f %f\n", rad_sq, dist_sq);
//fe_fprintf(stderr, "  %f %f %f\n", r_a.m_location[0], r_a.m_location[1], r_a.m_location[2]);
//fe_fprintf(stderr, "  %f %f %f\n", r_b.m_location[0], r_b.m_location[1], r_b.m_location[2]);

	rad_sq *= rad_sq;
	if(dist_sq < rad_sq)
	{

		FEASSERT((int)m_poolIndex <= m_spPool->length());
		if((int)m_poolIndex == m_spPool->length())
		{
			m_spPool->add(m_spScope->createRecord(m_hpPairLayout));
		}

		r_a.m_pRA->set(m_asProximity.left(m_spPool, m_poolIndex), r_a.m_index);
		r_b.m_pRA->set(m_asProximity.right(m_spPool, m_poolIndex), r_b.m_index);

		m_poolIndex++;
		return true;
	}
	return false;
}

#define SDT(a) m_pMGrid->detect(m_grid[flatten(a)])
#define DT(a,b) m_pMGrid->detect(m_grid[flatten(a)], m_grid[flatten(b)])
unsigned int ProxMultiGrid::Grid::detect(void)
{
	unsigned int collisions = 0;
	t_v3i a;
	for(a[0] = 0; a[0] < m_count[0]; a[0]++)
	{
		for(a[1] = 0; a[1] < m_count[1]; a[1]++)
		{
			for(a[2] = 0; a[2] < m_count[2]; a[2]++)
			{
				collisions += SDT(a);
				t_v3i b = a;
				b[0]++;
				if(b[0] < m_count[0])
				{
					collisions += DT(a,b);
				}
				b[0] = a[0]-1;
				b[1]++;
				if(b[1] < m_count[1])
				{
					for(b[0] = a[0]-1; b[0] <= a[0]+1; b[0]++)
					{
						if(b[0] < m_count[0] && b[0] >= 0)
						{
							collisions += DT(a,b);
						}
					}
				}
				b[2]++;
				if(b[2] < m_count[2])
				{
					for(b[0] = a[0]-1; b[0] <= a[0]+1; b[0]++)
					{
						if(b[0] < m_count[0] && b[0] >= 0)
						{
							for(b[1] = a[1]-1; b[1] <= a[1]+1; b[1]++)
							{
								if(b[1] < m_count[1] && b[1] >= 0)
								{
									collisions += DT(a,b);
								}
							}
						}
					}
				}
			}
		}
	}
	return collisions;
}

unsigned int ProxMultiGrid::detect(ProxMultiGrid::Grid &a_ga, ProxMultiGrid::Grid &a_gb)
{
	unsigned int collisions = 0;
	t_v3i a;
	for(a[0] = 0; a[0] < a_ga.m_count[0]; a[0]++)
	{
		for(a[1] = 0; a[1] < a_ga.m_count[1]; a[1]++)
		{
			for(a[2] = 0; a[2] < a_ga.m_count[2]; a[2]++)
			{
				t_v3i b;
				Vector<3, Real> lo;
				Vector<3, Real> hi;
				t_v3i l, h;
				for(int i = 0; i < 3; i++)
				{
					lo[i] =		a_ga.m_sz*((Real)a[i] - 0.5)
							-	a_gb.m_sz*0.5;
					hi[i] =		a_ga.m_sz*((Real)a[i] + 1.5)
							+	a_gb.m_sz*0.5;
					l[i] = (int)((lo[i]) / a_gb.m_sz);
					h[i] = (int)((hi[i]) / a_gb.m_sz);
					if(l[i] >= a_gb.m_count[i]) { l[i] = a_gb.m_count[i]-1; }
					if(l[i] < 0) { l[i] = 0; }
					if(h[i] >= a_gb.m_count[i]) { h[i] = a_gb.m_count[i]-1; }
					if(h[i] < 0) { h[i] = 0; }
				}

				for(b[0] = l[0]; b[0] <= h[0]; b[0]++)
				{
					for(b[1] = l[1]; b[1] <= h[1]; b[1]++)
					{
						for(b[2] = l[2]; b[2] <= h[2]; b[2]++)
						{
							collisions += detect(a_ga.m_grid[a_ga.flatten(a)],
								a_gb.m_grid[a_gb.flatten(b)]);
						}
					}
				}
			}
		}
	}
	return collisions;
}

} /* namespace ext */
} /* namespace fe */
