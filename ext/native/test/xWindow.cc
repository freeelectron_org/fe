/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "window/window.h"
#include "draw/draw.h"

#define	FE_XWDW_DOUBLE	TRUE

using namespace fe;
using namespace fe::ext;


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 limit=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexNativeWindowDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexOpenGLDL");
		UNIT_TEST(successful(result));

		{
			sp<WindowI> spWindowI(spRegistry->create("WindowI"));
			sp<WindowI> spWindowI2(spRegistry->create("WindowI"));

			sp<HandlerI> spHandler1(spRegistry->create("*.LogWindowHandler"));
			sp<HandlerI> spHandler2(spRegistry->create("*.LogWindowHandler"));
			sp<SignalerI> spSignalerI(spRegistry->create("SignalerI"));

			sp<Scope> spScope(spRegistry->create("Scope"));

			if(!spWindowI.isValid() || !spWindowI2.isValid() ||
					!spScope.isValid() || !spSignalerI.isValid())
				feX(argv[0], "can't continue");

			UNIT_TEST(spHandler1.isValid() && spHandler2.isValid());


			sp<Layout> spBeatLayout=spScope->declare("BEAT");
			spSignalerI->insert(spWindowI->getEventContextI(),spBeatLayout);
			Record beat=spScope->createRecord(spBeatLayout);

			sp<Layout> spEventLayout=
					spScope->lookupLayout(FE_EVENT_LAYOUT);
			UNIT_TEST(spEventLayout.isValid());

			if(spHandler1.isValid())
			{
				spSignalerI->insert(spHandler1,spEventLayout);
			}
			if(spHandler2.isValid())
			{
				sp<SignalerI> spWindowSignaler=spWindowI;
				UNIT_TEST(spWindowSignaler.isValid());
				if(spWindowSignaler.isValid())
					spWindowSignaler->insert(spHandler2,spEventLayout);
			}

			spWindowI->open("NativeWindow");
#if FE_XWDW_DOUBLE
			spWindowI2->open("NativeWindow 2");
			spWindowI2->setBackground({0, 0, 0, 0.5});
#endif

			sp<DrawI> spDrawI(spRegistry->create("DrawI"));
			sp<DrawI> spDrawI2(spRegistry->create("DrawI"));

			U32 count=0;
			while(++count!=limit)
			{
				if(limit)
				{
					feLog("Frame %d\n",count);
				}
				spSignalerI->signal(beat);

				const U32 windowCount=FE_XWDW_DOUBLE? 2: 1;
				for(U32 windowIndex=0;windowIndex<windowCount;windowIndex++)
				{
					sp<WindowI> spThisWindowI=
							windowIndex? spWindowI2: spWindowI;
					sp<DrawI> spThisDrawI=
							windowIndex? spDrawI2: spDrawI;

					spThisWindowI->makeCurrent();
					spThisWindowI->clear();

					if(spThisDrawI.isValid())
					{
						const Color pink(1.0f,0.5f,0.5f);
						const Vector3f corner(0.1f,0.1f,0.0f);

						String text;
						text.sPrintf("window %d %p",
								windowIndex,spThisWindowI.raw());

						spThisDrawI->drawAlignedText(corner,text,pink);
						spThisDrawI->drawAxes(0.2f);
					}

					spThisWindowI->swapBuffers();
				}

				milliSleep(1);
			}
		}

		feLog(">>>> BLOCK COMPLETE\n");

		complete=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(7);
	UNIT_RETURN();
}
