/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

/*
#if FE_CPLUSPLUS >= 201103L
#include <chrono>
#include <thread>

//	allows for:
//	std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
//	but on Windows, this is just Sleep(), rounded down to milliseconds
#endif
*/

#define FE_TICK_DEBUG	FALSE

namespace fe
{

FE_DL_PUBLIC FE_CORE_PORT double SystemTicker::ms_ticksPerMS = 1;
FE_DL_PUBLIC FE_CORE_PORT float SystemTicker::ms_usPerTick = 1;

void milliSleep(long milliseconds)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	const UINT uPeriod=1;
	MMRESULT result=timeBeginPeriod(uPeriod);
	if(result!=TIMERR_NOERROR)
	{
		feLogDirect("timeBeginPeriod failed\n");
	}
	Sleep(milliseconds);
	result=timeEndPeriod(uPeriod);
	if(result!=TIMERR_NOERROR)
	{
		feLogDirect("timeEndPeriod failed\n");
	}
#elif FE_OS==FE_LINUX || FE_OS==FE_OSX
	struct timeval timeout;

	timeout.tv_sec=milliseconds/1000;
	timeout.tv_usec=(milliseconds-timeout.tv_sec*1000)*1000;

	if(select(0,(fd_set *)NULL,(fd_set *)NULL,(fd_set *)NULL,&timeout) < 0)
	{
		feX("fe::milliSleep",
				(errno==EINTR)? "timer interrupted": " timer failed");
	}
#else
	feX("fe::milliSleep", "not implemented");
#endif
}

void microSleep(long microseconds)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#if FALSE
//	https://stackoverflow.com/questions/5801813/c-usleep-is-obsolete-workarounds-for-windows-mingw/11470617#11470617
//	NOTE WaitForSingleObject waits way too long for short sleeps
//
//	HANDLE timer;
//	LARGE_INTEGER ft;
//
//	// Convert to 100 nanosecond interval
//	// negative value indicates relative time
//	ft.QuadPart = -(10*microseconds);
//
//	timer = CreateWaitableTimer(NULL, TRUE, NULL);
//	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
//	WaitForSingleObject(timer, INFINITE);
//	CloseHandle(timer);
#endif

	//* round to milliseconds
	milliSleep((microseconds+500)/1000);
#elif FE_OS==FE_LINUX || FE_OS==FE_OSX
	struct timeval timeout;

	timeout.tv_sec=0;
	timeout.tv_usec=microseconds;

	if(select(0,(fd_set *)NULL,(fd_set *)NULL,(fd_set *)NULL,&timeout) < 0)
	{
		feX("fe::microSleep",
				(errno==EINTR)? "timer interrupted": " timer failed");
	}
#else
	feX("fe::microSleep", "not implemented");
#endif
}

void minimumSleep(long microseconds)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	const long min=500;
#else
	const long min=1;
#endif

	if(microseconds<min)
	{
		microseconds=min;
	}

	microSleep(microseconds);
}

void nanoSpin(long nanoseconds)
{
	if(nanoseconds<1)
	{
		return;
	}

	const U32 tickCount=
			U32(nanoseconds/(1e3*SystemTicker::microsecondsPerTick()));

	U32 tickStart=systemTick();
	U32 tickEnd=tickStart;
	while(tickEnd-tickStart<tickCount)
	{
		if(tickEnd<tickStart)
		{
			//* if something goes wrong, start over
			tickStart=tickEnd;
		}
		tickEnd=systemTick();
	}
}

SystemTicker::SystemTicker(void)
{
	m_lastexcl = timeMS();
	m_lastincl = timeMS();
	m_lasttick = systemTick();
	m_name = "TICK";
	m_group = "SystemTicker";
	staticInit();
}

SystemTicker::SystemTicker(const char *name)
{
	m_lastexcl = timeMS();
	m_lastincl = timeMS();
	m_lasttick = systemTick();
	m_name = name;
	m_group = "SystemTicker";
	staticInit();
}

SystemTicker::SystemTicker(const char *group, const char *name)
{
	m_lastexcl = timeMS();
	m_lastincl = timeMS();
	m_lasttick = systemTick();
	m_name = name;
	m_group = group;
	staticInit();
}

void SystemTicker::staticInit(void)
{
#ifdef FE_HAS_TIMES
	ms_usPerTick = 1e6 / (float)(sysconf(_SC_CLK_TCK));
	ms_ticksPerMS = 1e3 / ms_usPerTick;
	fe_fprintf(stderr,"_SC_CLK_TCK %.6G\n",(float)(sysconf(_SC_CLK_TCK)));
	fe_fprintf(stderr,"ms_usPerTick %.6G\n",ms_usPerTick);
	fe_fprintf(stderr,"ms_ticksPerMS %.6G\n",ms_ticksPerMS);
#else

#if FE_OS==FE_WIN64
	const U32 defaultTicksPerMS(13e3);
#elif FE_OS==FE_WIN32
	const U32 defaultTicksPerMS(4e6);
#else
	const U32 defaultTicksPerMS(3000e3);
#endif

	const char* buffer=getenv("FE_TICKS_PER_MS");
	ms_ticksPerMS = buffer? atoi(buffer): defaultTicksPerMS;
	ms_usPerTick = 1e3/ms_ticksPerMS;
#endif
}

unsigned long SystemTicker::timeMS(void)
{
#if FE_OS==FE_LINUX
	if( gettimeofday(&m_timer,NULL) )
	{
		feX("fe::SystemTicker::timeMS", "failed");
	}
	else
	{
		return ((m_timer.tv_sec & 0x000FFFFF)*1000+(m_timer.tv_usec/1000));
	}
#elif FE_OS==FE_OSX
	if( gettimeofday(&m_timer,NULL) )
	{
		feX("fe::SystemTicker::timeMS", "failed");
	}
	else
	{
		return ((m_timer.tv_sec & 0x000FFFFF)*1000+(m_timer.tv_usec/1000));
	}
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	return timeGetTime();
#else
	feX("fe::SystemTicker::timeMS", "unimplemented");
#endif
}

#if 0
void SystemTicker::calibrate(void)
{
#ifdef FE_HAS_TIMES
	return;
#endif
#if FE_OS==FE_WIN64
	LARGE_INTEGER frequency;
	BOOL success=QueryPerformanceFrequency(&frequency);
	ms_ticksPerMS=success? frequency.LowPart: 0;
#elif FE_OS!=FE_OSX && FE_HW==FE_X86
	unsigned long ms, nms, tick;

	// First, force us to end and therefore start a new timestep
	// so that it is less likely the calibration will be interrupted
	// between GetTimeMS and GetSystemTick. (assuming milliSleep
	// is not a spin sleep)
	milliSleep(10);
	SystemTicker ticker;
	ms = ticker.timeMS();
	tick = systemTick();
	if(!tick)
	{
		feX("fe::SystemTicker::calibrate", "systemTick failed");
	}

	// Now pause for a while
	milliSleep(1000);

	nms = ticker.timeMS();
	unsigned long tick2=systemTick();

#if FE_TICK_DEBUG
	feLogDirect("SystemTicker::calibrate (%u - %u)/(%u - %u)\n",
			tick2,tick,nms,ms);
#endif
	tick=tickDifference(tick,tick2);

	ms = nms - ms;
	/// @todo sanity check for rollover

	if(ms)
	{
		ms_ticksPerMS = (double(tick)/double(ms));
#if FE_TICK_DEBUG
		feLogDirect("  %.6G = %u/%u ticks/ms\n",ms_ticksPerMS,tick,ms);
#endif

		ms_usPerTick = 1e3*ms/double(tick);
#if FE_TICK_DEBUG
		feLogDirect("  %.6G = 1e3 * %u/%u us/tick\n",ms_usPerTick,ms,tick);
#endif
	}
	else
	{
		feX("fe::SystemTicker::calibrate", "difference failed");
	}
#endif
}
#endif

void SystemTicker::log(const char *fmt, ...)
{
#if 1
	String text;
	unsigned long now;
	unsigned long tick;
	tick = systemTick();
	now = timeMS();

	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		text.vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	//LOG("[33m%s:[32m %5u [37m%s[0m\n",m_name, now-m_last,(const char *)text);


//	fe_fprintf(stderr,"stderr [33m%s:[32m [1m%4u[0m %4u %10u"
//			" [37m%s[0m\n"

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	feLogGroup(m_group.c_str(),
		"%-25s: %4u %4u %12u %s\n"
		,m_name.c_str()
		,(unsigned int)(now-m_lastexcl)
		,(unsigned int)(now-m_lastincl)
		,(unsigned int)(tick-m_lasttick)
		,text.c_str());
#else
	feLogGroup(m_group.c_str(),
		"[33m%-25s:[32m [1m%4u[0m %4u %12u [37m%s[0m\n"
		,m_name.c_str()
		,(unsigned int)(now-m_lastexcl)
		,(unsigned int)(now-m_lastincl)
		,(unsigned int)(tick-m_lasttick)
		,text.c_str());
#endif
	m_lastincl = now;
	m_lastexcl = timeMS();
	m_lasttick = systemTick();
#endif
}

void SystemTicker::log(void)
{
	m_lastincl = timeMS();
	m_lastexcl = timeMS();
	m_lasttick = systemTick();
}

}



