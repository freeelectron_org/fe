/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SWO_DEBUG			FALSE
#define FE_SWO_VERBOSE			FALSE
#define FE_SWO_SAMPLE_VERBOSE	TRUE
#define	FE_SWO_PROFILE			(FE_CODEGEN==FE_PROFILE)

using namespace fe;
using namespace fe::ext;

void SurfaceWrapOp::initialize(void)
{
	//* NOTE inherits attributes from SurfaceBindOp

	catalogRemove("InputGroup");
	catalogRemove("FragmentAttr");
	catalogRemove("samples");
	catalogRemove("DriverGroup");
	catalogRemove("Bindings");
	catalogRemove("Bind Primitives");
	catalogRemove("Bind Fragments");
	catalogRemove("Covert");
	catalogRemove("Conform");
	catalogRemove("LimitDistance");
	catalogRemove("MaxDistance");
	catalogRemove("PartitionDriver");
	catalogRemove("InputPartitionAttr");
	catalogRemove("DriverPartitionAttr");
	catalogRemove("PivotPoint");
	catalogRemove("pivotOffset");
	catalogRemove("Metric");
	catalogRemove("Scalable");
	catalogRemove("generateLocators");
	catalogRemove("locatorPrefix");
	catalogRemove("locatorOffset");
	catalogRemove("subdiv");
	catalogRemove("subdivDepth");
	catalogRemove("bindToPoints");

	catalog<String>("BindPrefix","hint")=
			"Prefix of attribute names on input surface describing"
			" where each point on the input surface is attached to"
			" the associated driver.";

	catalog<bool>("DiscardBindings")=true;
	catalog<String>("DiscardBindings","label")="Discard Bindings";
	catalog<String>("DiscardBindings","page")="Config";
	catalog<String>("DiscardBindings","hint")=
			"Remove used binding attributes from the output.";

	catalog<bool>("checkDeformed")=false;
	catalog<String>("checkDeformed","label")="Check Deformed";
	catalog<String>("checkDeformed","page")="Config";
	catalog<bool>("checkDeformed","joined")=true;
	catalog<String>("checkDeformed","hint")=
			"Verify that the deformed driver surface is like"
			" the reference driver surface."
			"  These checks take time, so this option should probably"
			" only be used temporarily.";

	catalog<I32>("checkLimit")=1;
	catalog<I32>("checkLimit","min")=0;
	catalog<I32>("checkLimit","high")=10;
	catalog<I32>("checkLimit","max")=100;
	catalog<String>("checkLimit","enabler")="checkDeformed";
	catalog<String>("checkLimit","label")="Message Limit";
	catalog<String>("checkLimit","page")="Config";
	catalog<bool>("checkLimit","joined")=true;
	catalog<String>("checkLimit","hint")=
			"Stop reporting mismatches after the given number of errors.";

	catalog<Real>("scaleLimit")=2;
	catalog<Real>("scaleLimit","min")=1;
	catalog<Real>("scaleLimit","high")=10;
	catalog<String>("scaleLimit","enabler")="checkDeformed";
	catalog<String>("scaleLimit","label")="Scale Limit";
	catalog<String>("scaleLimit","page")="Config";
	catalog<String>("scaleLimit","hint")=
			"Threshold for rescale warnings on deformed driver.";

	catalog<bool>("allowReplication")=false;
	catalog<String>("allowReplication","label")="Allow Replication";
	catalog<String>("allowReplication","page")="Config";
	catalog<String>("allowReplication","hint")=
			"Normally, the number of elements for the Driver Surface and"
			" the Deformed Surface are expected to be the same."
			"  With replication, an overpopulated Deformed Surface will be"
			" interpreted as multiple deformed replicants of"
			" the Driver Surface."
			"  Multiple wrapped copies of the Input Surface should result.";

	catalog<Real>("envelope")=1.0;
	catalog<String>("envelope","label")="Envelope";
	catalog<String>("envelope","page")="Bind";

	catalog<Real>("Distance Bias")=1.0;
	catalog<Real>("Distance Bias","min")=0.0;
	catalog<Real>("Distance Bias","high")=10.0;
	catalog<Real>("Distance Bias","max")=1e3;
	catalog<String>("Distance Bias","page")="Bind";
	catalog<bool>("Distance Bias","joined")=true;
	catalog<String>("Distance Bias","hint")=
			"Dominance of closer faces towards weighting";

	catalog<Real>("Distance Scale")=1.0;
	catalog<Real>("Distance Scale","min")=0.0;
	catalog<Real>("Distance Scale","high")=2.0;
	catalog<Real>("Distance Scale","max")=100.0;
	catalog<String>("Distance Scale","page")="Bind";
	catalog<String>("Distance Scale","hint")=
			"Distance gain for weighting";

	catalog<String>("SampleMethod")="linear";
	catalog<String>("SampleMethod","label")="Sampling";
	catalog<String>("SampleMethod","choice:0")="flat";
	catalog<String>("SampleMethod","label:0")="Flat";
	catalog<String>("SampleMethod","choice:1")="linear";
	catalog<String>("SampleMethod","label:1")="Linear";
	catalog<String>("SampleMethod","choice:2")="pointNormal";
	catalog<String>("SampleMethod","label:2")="Point Normal";
	catalog<String>("SampleMethod","choice:3")="spline";
	catalog<String>("SampleMethod","label:3")="Spline";
	catalog<String>("SampleMethod","page")="Bind";
	catalog<bool>("SampleMethod","joined")=true;
	catalog<String>("SampleMethod","hint")=
			"Type of interpolation between driver vertices."
			"  Point Normal takes about four times longer than Linear."
			"  Spline currently only works on curves.";

	catalog<bool>("rotateNormals")=false;
	catalog<String>("rotateNormals","label")="Rotate Normals";
	catalog<String>("rotateNormals","page")="Bind";
	catalog<bool>("rotateNormals","joined")=true;
	catalog<String>("rotateNormals","hint")=
			"Apply transforms to normals in addition to positions.";

	catalog<bool>("scaleOffset")=false;
	catalog<String>("scaleOffset","label")="Scale Offset";
	catalog<String>("scaleOffset","page")="Bind";
	catalog<String>("scaleOffset","hint")=
			"Resize each offset from driver based on deformed curve length"
			" or face area of primitive to which it is bound.";

	catalog<bool>("edgy")=false;
	catalog<String>("edgy","label")="Edgy";
	catalog<String>("edgy","page")="Bind";
	catalog<bool>("edgy","joined")=true;
	catalog<String>("edgy","hint")=
			"Temper the effects of driver subdivision by forcing the"
			" translational component of binding frames to follow"
			" the unsubdivided driver surface."
			"  Edgy options have no effect if subdivision is not used.";

	catalog<String>("edgyAttr")="";
	catalog<String>("edgyAttr","label")="Attr";
	catalog<String>("edgyAttr","page")="Bind";
	catalog<String>("edgyAttr","enabler")="edgy";
	catalog<bool>("edgyAttr","joined")=true;
	catalog<String>("edgyAttr","hint")=
			"Point real attribute that scales the effect of edginess."
			"  These values should probably range between zero and one."
			"  This scalar is not applied if this attribute doesn't exist.";

	catalog<Real>("edginess")=1.0;
	catalog<Real>("edginess","high")=1.0;
	catalog<Real>("edginess","max")=10.0;
	catalog<String>("edginess","label")="Envelope";
	catalog<String>("edginess","page")="Bind";
	catalog<String>("edginess","enabler")="edgy";
	catalog<bool>("edginess","joined")=true;
	catalog<String>("edginess","hint")=
			"Uniformly scale the edginess effect."
			"  If an Edgy Attribute is supplied,"
			" the effective edginess is the product of both numbers.";

	catalog<I32>("edgyDepth")=0;
	catalog<I32>("edgyDepth","high")=4;
	catalog<I32>("edgyDepth","max")=10;
	catalog<String>("edgyDepth","label")="Depth";
	catalog<String>("edgyDepth","page")="Bind";
	catalog<String>("edgyDepth","enabler")="edgy";
	catalog<String>("edgyDepth","hint")=
			"Reduce the edginess effect by subdividing an auxillary version"
			" of the driver to use on just the translational component"
			" of the binding frames."
			"  This usually results in a different effect than"
			" simply reducing the Edgy Envelope."
			"  An Edgy Depth of zero applies no subdivision to the"
			" edgy frame, therefore maximizing the potential edginess."
			"  An Edgy Depth at or above the regular subdivision depth"
			" will completely subvert the edginess effect.";

	//* TODO fixate curves
#if FALSE
	catalog<String>("fixate")="nothing";
	catalog<String>("fixate","label")="Fixate";
	catalog<String>("fixate","choice:0")="nothing";
	catalog<String>("fixate","label:0")="Nothing";
	catalog<String>("fixate","choice:1")="baseShort";
	catalog<String>("fixate","label:1")="Short Curves to Base";
	catalog<String>("fixate","choice:2")="tipShort";
	catalog<String>("fixate","label:2")="Short Curves to Tip";
	catalog<String>("fixate","choice:3")="base";
	catalog<String>("fixate","label:3")="All Curves to Base";
	catalog<String>("fixate","choice:4")="tip";
	catalog<String>("fixate","label:4")="All Curves to Tip";
	catalog<String>("fixate","page")="Bind";
	catalog<String>("fixate","hint")=
			"Force output to retain the reference curve length,"
			" locking the position of either the base or tip."
			"  If only shortened curves are fixated,"
			" lengthened curves are still allowed to cause stretching.";
#endif

	catalog<bool>("spread")=false;
	catalog<String>("spread","label")="Spread";
	catalog<String>("spread","page")="Bind";
	catalog<bool>("spread","joined")=true;
	catalog<String>("spread","hint")=
			"Apportion weights onto neighboring triangles"
					" using barycentric coordinates.";

	catalog<I32>("spreadDepth")=1;
	catalog<I32>("spreadDepth","min")=1;
	catalog<I32>("spreadDepth","high")=10;
	catalog<I32>("spreadDepth","max")=100;
	catalog<String>("spreadDepth","label")="Depth";
	catalog<String>("spreadDepth","page")="Bind";
	catalog<String>("spreadDepth","enabler")="spread";
	catalog<String>("spreadDepth","hint")=
			"Number of iterations to propagate weights.";

	catalog<bool>("singleTransform")=false;
	catalog<String>("singleTransform","label")="Single Transform";
	catalog<String>("singleTransform","page")="Bind";
	catalog<bool>("singleTransform","joined")=true;
	catalog<String>("singleTransform","hint")=
			"Use a single weighted blend of all binding transforms."
			"  Each binding simply translates in that space.";

	catalog<Real>("singleBias")=10.0;
	catalog<Real>("singleBias","min")=0.0;
	catalog<Real>("singleBias","high")=20.0;
	catalog<Real>("singleBias","max")=100.0;
	catalog<String>("singleBias","label")="Singularity";
	catalog<String>("singleBias","page")="Bind";
	catalog<String>("singleBias","enabler")="singleTransform";
	catalog<String>("singleBias","hint")=
			"Dominance of closer faces towards weighting";

	catalog<Real>("bendLimit")=170.0;
	catalog<Real>("bendLimit","min")=0.0;
	catalog<Real>("bendLimit","high")=170.0;
	catalog<Real>("bendLimit","max")=180.0;
	catalog<String>("bendLimit","label")="Fragment Bend Limit";
	catalog<String>("bendLimit","page")="Bind";
	catalog<String>("bendLimit","hint")=
			"For multi-sample fragment bindings,"
			" the maximum overall angle of bending along each fragment.";

	catalog<bool>("fadeToRoot")=false;
	catalog<String>("fadeToRoot","label")="Fade To Root";
	catalog<String>("fadeToRoot","page")="Bind";
	catalog<bool>("fadeToRoot","joined")=true;
	catalog<String>("fadeToRoot","hint")=
			"At distance, transition to binding only onto the root."
			"  Currently, the root is origin, but new options are likely.";

	catalog<Real>("fadeInnerDistance")=0.0;
	catalog<Real>("fadeInnerDistance","min")=0.0;
	catalog<Real>("fadeInnerDistance","high")=10.0;
	catalog<Real>("fadeInnerDistance","max")=1e6;
	catalog<String>("fadeInnerDistance","label")="From";
	catalog<String>("fadeInnerDistance","page")="Bind";
	catalog<String>("fadeInnerDistance","enabler")="fadeToRoot";
	catalog<bool>("fadeInnerDistance","joined")=true;
	catalog<String>("fadeInnerDistance","hint")=
			"Distance to start transition to root-only binding";

	catalog<Real>("fadeOuterDistance")=0.0;
	catalog<Real>("fadeOuterDistance","min")=0.0;
	catalog<Real>("fadeOuterDistance","high")=10.0;
	catalog<Real>("fadeOuterDistance","max")=1e6;
	catalog<String>("fadeOuterDistance","label")="To";
	catalog<String>("fadeOuterDistance","page")="Bind";
	catalog<String>("fadeOuterDistance","enabler")="fadeToRoot";
	catalog<String>("fadeOuterDistance","hint")=
			"Distance to complete transition to root-only binding";

	catalog<bool>("Debug Bind")=false;
	catalog<bool>("Debug Bind","joined")=true;
	catalog<String>("Debug Bind","page")="Bind";
	catalog<String>("Debug Bind","hint")=
			"Draws lines over the output showing the attachments."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<I32>("Debug Index")=-1;
	catalog<I32>("Debug Index","min")=-1;
	catalog<I32>("Debug Index","high")=1000;
	catalog<I32>("Debug Index","max")=1000000;
	catalog<String>("Debug Index","page")="Bind";
	catalog<String>("Debug Index","enabler")="Debug Bind";
	catalog<String>("Debug Index","hint")=
			"Index of element to examine (-1 to see them all).";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","prompt")=
			"Click an output point to see its bindings.";

	catalog<String>("Input Surface","label")="Bound Input Surface";

	catalog<String>("Input Surfaces","label")="Bound Input Surfaces";

	catalog< sp<Component> >("Deformed Surface");
	catalog<String>("Deformed Surface","label")="Deformed Driver Surface";

	//* check problems
	catalog<bool>("Output Surface","recycle")=true;

	m_spDebugGroup=new DrawMode();
	m_spDebugGroup->setGroup("debug");

	m_spBrush=new DrawMode();
	m_spBrush->setDrawStyle(DrawMode::e_foreshadow);
	m_spBrush->setPointSize(8.0);
	m_spBrush->setLineWidth(2.0);
	m_spBrush->setAntialias(TRUE);
	m_spBrush->setLit(FALSE);

	m_spOverlay=new DrawMode();
	m_spOverlay->setLineWidth(3.0);
	m_spOverlay->setLit(FALSE);

	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setPointSize(6.0);
	m_spWireframe->setLineWidth(1.0);
}

void SurfaceWrapOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::handle brush %d brushed %d recook %d\n",
			spDrawBrush.isValid(),m_brushed,m_forceRecook);
#endif

	if(spDrawBrush.isValid())
	{
		m_brushed=TRUE;

		const Color transparent(0.0,0.0,0.0,0.0);
		const Color black(0.0,0.0,0.0);
		const Color white(1.0,1.0,0.8);
		const Color grey(0.8,0.8,1.0);
		const Color red(0.8,0.0,0.0);
		const Color green(0.2,1.0,0.2);
		const Color blue(0.0,0.0,1.0);
		const Color cyan(0.0,0.8,1.0);
		const Color magenta(0.8,0.0,0.8);
		const Color yellow(0.8,0.8,0.0);

		const Real dotRadius=6.0;
		const Real rimSize=6.0;
		const BWORD bevel=TRUE;
		const Real maxAlpha=0.6;
		const Real fadeAlpha=0.5;

		if(m_spOutput.isValid())
		{
			sp<SurfaceAccessorI> spOutputVertices;
			sp<SurfaceAccessorI> spOutputPoints;
			if(m_spOutputAccessible.isValid())
			{
				if(!access(spOutputVertices,m_spOutputAccessible,
						e_primitive,e_vertices)) return;
				if(!access(spOutputPoints,m_spOutputAccessible,
						e_point,e_position)) return;
			}

			const I32 pointCount=spOutputPoints->count();
			if(m_selectIndex>=pointCount)
			{
				m_selectIndex= -1;
			}

			sp<DrawI> spDrawOverlay;
			if(!accessBrushOverlay(spDrawOverlay,a_rSignal)) return;

			spDrawBrush->pushDrawMode(m_spBrush);
			spDrawOverlay->pushDrawMode(m_spOverlay);

			sp<ViewI> spView=spDrawOverlay->view();

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			m_event.bind(windowEvent(a_rSignal));

#if FE_SWO_DEBUG
			feLog("SurfaceWrapOp::handle event %s\n",c_print(m_event));
#endif

			if(!m_event.isExpose())
			{
				const BWORD leftPress=
						m_event.isMousePress(WindowEvent::e_itemLeft);
				if(leftPress)
				{
					m_forceRecook=TRUE;
					catalog<String>("Brush","cook")="once";
				}

				I32 pickIndex= -1;

				const Real maxDistance=100.0;
				sp<SurfaceSearchable::Impact> spImpact=m_spOutput->rayImpact(
						rRayOrigin,rRayDirection,maxDistance);
				if(spImpact.isValid())
				{
//					const I32 primitiveIndex=spImpact->primitiveIndex();
					I32 pointIndex= -1;

					sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
					if(spTriImpact.isValid())
					{
						pointIndex=nearestPointIndex(spTriImpact);
					}

					sp<SurfaceCurves::Impact> spCurveImpact=spImpact;
					if(spCurveImpact.isValid())
					{
						pointIndex=nearestPointIndex(
								spOutputVertices,spCurveImpact);
					}

					if(pointIndex>=0)
					{
						pickIndex=pointIndex;
					}

//					feLog("prim %d point %d\n",primitiveIndex,pointIndex);
				}

				if(pickIndex>=0)
				{
					if(leftPress)
					{
						m_selectIndex=pickIndex;
					}

					if(pickIndex!=m_selectIndex)
					{
						const SpatialVector point=
								spOutputPoints->spatialVector(pickIndex);

						drawDot(spView,spDrawOverlay,point,dotRadius,blue);
					}
				}
				else if(leftPress)
				{
					m_selectIndex=-1;
				}
			}

			if(m_selectIndex>=0)
			{
				const SpatialVector point=spOutputPoints->spatialVector(
						m_selectIndex);

				drawSquare(spDrawOverlay,point,rimSize,bevel,
						black,cyan,NULL,NULL);

				Real maxWeight=1e-9;
				Real minDist=0.0;
				Real maxDist=0.0;

				const U32 feedbackCount=m_feedback.size();
				Real* distanceArray=new Real[feedbackCount];

				for(U32 feedbackIndex=0;feedbackIndex<feedbackCount;
						feedbackIndex++)
				{
					const Feedback& rFeedback=m_feedback[feedbackIndex];
					const Real weight=rFeedback.weight();

					if(maxWeight<weight)
					{
						maxWeight=weight;
					}

					sp<SurfaceI::ImpactI> spImpact=rFeedback.impact();
					const SpatialVector point=spImpact->intersection();
					const Real viewDist=magnitude(point-rRayOrigin);

					if(!feedbackIndex || minDist>viewDist)
					{
						minDist=viewDist;
					}
					if(maxDist<viewDist)
					{
						maxDist=viewDist;
					}

					distanceArray[feedbackIndex]=viewDist;
				}

				const Real deltaDistance=
						(maxDist>minDist)? maxDist-minDist: 1.0;

				for(U32 feedbackIndex=0;feedbackIndex<feedbackCount;
						feedbackIndex++)
				{
					const Feedback& rFeedback=m_feedback[feedbackIndex];
					const Real weight=rFeedback.weight();
					sp<SurfaceI::ImpactI> spImpact=rFeedback.impact();

					const Real viewDist=distanceArray[feedbackIndex];

					FEASSERT(deltaDistance>0.0);
					const Real alpha=maxAlpha-
							fadeAlpha*(viewDist-minDist)/deltaDistance;

					FEASSERT(maxWeight>0.0);
					const Color weighted(1.0,weight/maxWeight,0.0,alpha);
					spDrawBrush->draw(spImpact,&weighted);

					//* draw driver
/*
					sp<SurfaceTriangles> spSurfaceTriangles(m_spDeformed);
					if(spSurfaceTriangles.isValid())
					{
						spDrawBrush->pushDrawMode(m_spWireframe);

						spSurfaceTriangles->draw(spDrawBrush,NULL);

						spDrawBrush->popDrawMode();
					}
*/

#if FALSE
					const U32 margin=1;
					Vector2i projected=spView->project(
							spImpact->intersection(),
							CameraI::e_perspective);
					String text;
					text.sPrintf("%.3f",weight);
					drawLabel(spDrawOverlay,projected,text,
							FALSE,margin,cyan,NULL,&black);
#endif
				}

				delete[] distanceArray;
			}

			spDrawOverlay->popDrawMode();
			spDrawBrush->popDrawMode();
		}

		return;
	}

#if FE_SWO_PROFILE
	m_spProfiler=new Profiler("SurfaceWrapOp");
	m_spProfileAccess=new Profiler::Profile(m_spProfiler,"Access");
	m_spProfileDriver=new Profiler::Profile(m_spProfiler,"Driver");
	m_spProfileDeformed=new Profiler::Profile(m_spProfiler,"Deformed");
	m_spProfilePrepare=new Profiler::Profile(m_spProfiler,"Prepare");
	m_spProfileEdgy=new Profiler::Profile(m_spProfiler,"Edgy");
	m_spProfileMoreAccess=new Profiler::Profile(m_spProfiler,"MoreAccess");
	m_spProfileCache=new Profiler::Profile(m_spProfiler,"Cache");
	m_spProfileRun=new Profiler::Profile(m_spProfiler,"Run");
	m_spProfilePrep=new Profiler::Profile(m_spProfiler,"Prep");
	m_spProfilePreload=new Profiler::Profile(m_spProfiler,"Preload");
	m_spProfileIterate=new Profiler::Profile(m_spProfiler,"Iterate");
	m_spProfileFragment=new Profiler::Profile(m_spProfiler,"Fragment");
	m_spProfileFilter=new Profiler::Profile(m_spProfiler,"Filter");
	m_spProfileSpread=new Profiler::Profile(m_spProfiler,"Spread");
	m_spProfileDistance=new Profiler::Profile(m_spProfiler,"Distance");
	m_spProfileLoop=new Profiler::Profile(m_spProfiler,"Loop");
	m_spProfileBary=new Profiler::Profile(m_spProfiler,"Bary");
	m_spProfileSample=new Profiler::Profile(m_spProfiler,"Sample");
	m_spProfileInvert=new Profiler::Profile(m_spProfiler,"Invert");
	m_spProfileWeight=new Profiler::Profile(m_spProfiler,"Weight");
	m_spProfileTable=new Profiler::Profile(m_spProfiler,"Table");
	m_spProfileTransfer=new Profiler::Profile(m_spProfiler,"Transfer");
	m_spProfileFinish=new Profiler::Profile(m_spProfiler,"Finish");
	m_spProfileResolve=new Profiler::Profile(m_spProfiler,"Resolve");

	m_spProfiler->begin();
	m_spProfileAccess->start();
#endif

	const BWORD scaleOffset=catalog<bool>("scaleOffset");
	const Real frame=currentFrame(a_rSignal);
	const BWORD frameChanged=(frame!= m_lastFrame);
	const BWORD replaced=(catalog<bool>("Output Surface","replaced"));
	const BWORD paramChanged=(!m_brushed && !replaced && !frameChanged);

	const BWORD rebuild=
			(m_forceRecook || paramChanged || frameChanged || replaced);

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::handle frame %.6G->%.6G paramChanged %d"
			" frameChanged %d replaced %d forceRecook %d -> rebuild %d\n",
			m_lastFrame,frame,paramChanged,frameChanged,replaced,
			m_forceRecook,rebuild);
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="";

	m_lastFrame=frame;
	m_forceRecook=FALSE;
	m_brushed=FALSE;

	if(!rebuild)
	{
		//* brush doesn't change output

		return;
	}

	//* output surface is copy of input surface
	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	if(catalog<BWORD>("Debug Bind"))
	{
		if(!accessGuide(m_spDrawDebug,a_rSignal)) return;
	}
	else
	{
		m_spDrawDebug=NULL;
	}

	if(!access(m_spInputAccessible,"Input Surface")) return;
	const I32 inputPointCount=
			m_spInputAccessible->count(SurfaceAccessibleI::e_point);
	const I32 inputPrimitiveCount=
			m_spInputAccessible->count(SurfaceAccessibleI::e_primitive);

	sp<SurfaceAccessibleI> spDriverAccessible;
	if(!access(spDriverAccessible,"Driver Surface")) return;

	sp<SurfaceAccessibleI> spDeformedAccessible;
	if(!access(spDeformedAccessible,"Deformed Surface")) return;

	sp<SurfaceAccessorI> spDriverVertices;
	if(!access(spDriverVertices,spDriverAccessible,
			e_primitive,e_vertices)) return;
	const I32 driverPrimitiveCount=spDriverVertices->count();
	if(!driverPrimitiveCount)
	{
		spDriverVertices=NULL;
	}

	sp<SurfaceAccessorI> spDeformedVertices;
	if(!access(spDeformedVertices,spDeformedAccessible,
			e_primitive,e_vertices)) return;
	const I32 deformedPrimitiveCount=spDeformedVertices->count();
	if(!deformedPrimitiveCount)
	{
		spDeformedVertices=NULL;
	}

	sp<SurfaceAccessorI> spDriverProperties;
	sp<SurfaceAccessorI> spDeformedProperties;
	if(scaleOffset)
	{
		if(!access(spDriverProperties,spDriverAccessible,
			e_primitive,e_properties)) return;

		if(!access(spDeformedProperties,spDeformedAccessible,
				e_primitive,e_properties)) return;
	}

	const String driverSignature=generateSignature(spDriverAccessible);
	const String inputSignature=retrieveSignature(m_spInputAccessible);
	if(driverSignature!=inputSignature)
	{
		String message;
		message.sPrintf("Reference driver surface has a different signature"
				" than when bound (\"%s\" vs \"%s\");",
				driverSignature.c_str(),inputSignature.c_str());
		catalog<String>("warning")+=message;
	}

	String driverGroup;
	String fragmentAttribute;
	String inputGroup;
	String covertKeyIt;
	I32 subdiv;
	BWORD bindToPoints;
	retrieveParameters(m_spInputAccessible,driverGroup,fragmentAttribute,
			inputGroup,m_bindCount,m_samples,m_bindPrimitives,m_fragmented,
			covertKeyIt,m_locatorPrefix,subdiv,bindToPoints);

	const SurfaceI::Restrictions restrictions=bindToPoints?
			SurfaceI::Restrictions(
			SurfaceI::e_excludeCurves | SurfaceI::e_excludePolygons):
			SurfaceI::e_unrestricted;

	const I32 outputIteration=
			catalogOrDefault<I32>("Output Surface","iteration",-1);
	const I32 outputIterations=
			catalogOrDefault<I32>("Output Surface","iterations",0);

	m_spBindingAccessible=m_spInputAccessible;
	if(!covertKeyIt.empty())
	{
		sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
				registry()->create("*.SurfaceAccessibleCatalog");
		if(spSurfaceAccessibleCatalog.isValid())
		{
			if(outputIteration>=0)
			{
				covertKeyIt.catf(":%d",outputIteration);
			}

			spSurfaceAccessibleCatalog->setCatalog(
					registry()->master()->catalog());
			spSurfaceAccessibleCatalog->setKey(covertKeyIt);
			spSurfaceAccessibleCatalog->setPersistent(TRUE);
			m_spBindingAccessible=spSurfaceAccessibleCatalog;
		}
	}

	const BWORD allowReplication=catalog<bool>("allowReplication");
	I32 replicantCount=1;

	I32 outputPrimitiveCount=m_spOutputAccessible->count(
			SurfaceAccessibleI::e_primitive);

	const BWORD checkDeformed=catalog<bool>("checkDeformed");
	if(deformedPrimitiveCount!=driverPrimitiveCount)
	{
		if(allowReplication)
		{
			if(driverPrimitiveCount)
			{
				replicantCount=fe::maximum(I32(1),
						deformedPrimitiveCount/driverPrimitiveCount);
			}

			if(outputPrimitiveCount>inputPrimitiveCount*replicantCount)
			{
				m_spOutputAccessible->clear();
				outputPrimitiveCount=0;
			}

			const I32 duplicationCount=
					replicantCount-outputPrimitiveCount/inputPrimitiveCount;

			//* duplicate input into output, when necessary

//			feLog("input %d output %d driver %d deformed %d"
//					" replicants %d dup %d\n",
//					inputPrimitiveCount,outputPrimitiveCount,
//					driverPrimitiveCount,deformedPrimitiveCount,
//					replicantCount,duplicationCount);

			for(I32 duplicationIndex=0;duplicationIndex<duplicationCount;
					duplicationIndex++)
			{
				m_spOutputAccessible->append(m_spInputAccessible,NULL);
			}

			outputPrimitiveCount=m_spOutputAccessible->count(
					SurfaceAccessibleI::e_primitive);
		}
		else
		{
			String message;
			message.sPrintf("Deformed driver surface has a different number"
					" of primitives than reference (%d vs %d);",
					deformedPrimitiveCount,driverPrimitiveCount);
			catalog<String>(checkDeformed? "error": "warning")+=message;
			if(checkDeformed)
			{
				return;
			}
		}
	}
	else if(checkDeformed)
	{
		sp<SurfaceAccessorI> spDriverPoints;
		if(!access(spDriverPoints,spDriverAccessible,
				e_point,e_position)) return;

		sp<SurfaceAccessorI> spDeformedPoints;
		if(!access(spDeformedPoints,spDeformedAccessible,
				e_point,e_position)) return;

		const I32 checkLimit=catalog<I32>("checkLimit");
		const Real scaleLimit=catalog<Real>("scaleLimit");
		const Real invScaleLimit=scaleLimit>0.0? 1.0/scaleLimit: 0.0;
		I32 warnings=0;
		I32 mismatches=0;
		for(I32 primitiveIndex=0;
				primitiveIndex<driverPrimitiveCount && mismatches<checkLimit;
				primitiveIndex++)
		{
			const I32 driverSubCount=
					spDriverVertices->subCount(primitiveIndex);
			const I32 deformedSubCount=
					spDeformedVertices->subCount(primitiveIndex);
			if(driverSubCount!=deformedSubCount)
			{
				String message;
				message.sPrintf("  primitive %d has different"
						" number of vertices (%d vs %d);\n",primitiveIndex,
						deformedPrimitiveCount,driverPrimitiveCount);
				catalog<String>("error")+=message;
				mismatches++;
			}
			else
			{
				BWORD broken=FALSE;
				SpatialVector lastDriverPoint;
				SpatialVector lastDeformedPoint;
				for(I32 subIndex=0;
						subIndex<driverSubCount &&
						warnings+mismatches<checkLimit;
						subIndex++)
				{
					const U32 driverPointIndex=spDriverVertices->integer(
							primitiveIndex,subIndex);
					const U32 deformedPointIndex=spDeformedVertices->integer(
							primitiveIndex,subIndex);
					const SpatialVector driverPoint=
							spDriverPoints->spatialVector(driverPointIndex);
					const SpatialVector deformedPoint=
							spDeformedPoints->spatialVector(deformedPointIndex);

					if(FE_INVALID_SCALAR(driverPoint[0]) ||
							FE_INVALID_SCALAR(driverPoint[1]) ||
							FE_INVALID_SCALAR(driverPoint[2]))
					{
						String message;
						message.sPrintf(
								"  driver primitive %d vertex %d"
								" has corrupt data;\n",
								primitiveIndex,subIndex);
						catalog<String>("error")+=message;
						mismatches++;
					}
					else if(FE_INVALID_SCALAR(deformedPoint[0]) ||
							FE_INVALID_SCALAR(deformedPoint[1]) ||
							FE_INVALID_SCALAR(deformedPoint[2]))
					{
						String message;
						message.sPrintf(
								"  deformed primitive %d vertex %d"
								" has corrupt data;\n",
								primitiveIndex,subIndex);
						catalog<String>("error")+=message;
						mismatches++;
					}
					else if(driverPointIndex!=deformedPointIndex)
					{
						String message;
						message.sPrintf("  primitive %d has mismatched"
								" vertex %d (%d vs %d);\n",
								primitiveIndex,subIndex,
								deformedPointIndex,driverPointIndex);
						catalog<String>("error")+=message;
						mismatches++;
					}
					else if(subIndex && !broken)
					{
						const Real driverLength=
								magnitude(driverPoint-lastDriverPoint);
						const Real deformedLength=
								magnitude(deformedPoint-lastDeformedPoint);

						if(driverLength<1e-6)
						{
							String message;
							message.sPrintf(
									"  driver primitive %d segment %d"
									" has near zero length;\n",
									primitiveIndex,subIndex-1);
							catalog<String>("error")+=message;
							mismatches++;
						}
						else if(deformedLength<1e-6)
						{
							String message;
							message.sPrintf(
									"  deformed primitive %d segment %d"
									" has near zero length;\n",
									primitiveIndex,subIndex-1);
							catalog<String>("error")+=message;
							mismatches++;
						}
						else
						{
							const Real segmentScale=deformedLength/driverLength;
							if(segmentScale<invScaleLimit ||
									segmentScale>scaleLimit)
							{
								String message;
								message.sPrintf(
										"  deformed primitive %d segment %d"
										" has scaled by %.6G\n",
										primitiveIndex,subIndex-1,
										deformedLength/driverLength);
								catalog<String>("warning")+=message;
								warnings++;
							}
						}
					}
					lastDriverPoint=driverPoint;
					lastDeformedPoint=deformedPoint;
				}
			}
		}
		if(mismatches)
		{
			String message;
			message.sPrintf("Deformed driver has %d%s mismatches:\n",
					mismatches,mismatches>=checkLimit? "+": "");
			catalog<String>("error")=message+catalog<String>("error");
			return;
		}
	}

	if(replicantCount<2 && outputPrimitiveCount>inputPrimitiveCount)
	{
		feLog("SurfaceWrapOp::handle"
				" reseting excessive output primitives (%d vs %d)\n",
				outputPrimitiveCount,inputPrimitiveCount);

		m_spOutputAccessible->copy(m_spInputAccessible);
		outputPrimitiveCount=m_spOutputAccessible->count(
				SurfaceAccessibleI::e_primitive);
	}

	const SurfaceI::SampleMethod sampleMethod=
			(catalog<String>("SampleMethod")=="spline")?
			SurfaceI::e_spline:
			(catalog<String>("SampleMethod")=="pointNormal")?
			SurfaceI::e_pointNormal:
			(catalog<String>("SampleMethod")=="linear")?
			SurfaceI::e_linear: SurfaceI::e_flat;

#if FE_SWO_PROFILE
	m_spProfileDriver->replace(m_spProfileAccess);
#endif

//	feLog("SurfaceWrapOp::handle driver replaced %d %d\n",
//			catalogOrDefault<bool>("Driver Surface","replaced",true),
//			catalogOrDefault<bool>("Deformed Surface","replaced",true));

	if(subdiv!=m_lastSubdiv || bindToPoints!=m_lastBindToPoints)
	{
		m_spDriver=NULL;
		m_spRefEdged=NULL;

		m_spDeformed=NULL;
		m_spDefEdged=NULL;
	}

	const I32 edgyDepth=catalog<I32>("edgyDepth");
	if(edgyDepth!=m_lastEdgyDepth)
	{
		m_spRefEdged=NULL;
		m_spDefEdged=NULL;
	}

	const BWORD doEdgy=(catalog<bool>("edgy") && edgyDepth<subdiv);

	if(frameChanged ||
			catalogOrDefault<bool>("Deformed Surface","replaced",true))
	{
		m_spDeformed=NULL;
		m_spDefEdged=NULL;
	}

	//* original driver
	const BWORD driverReplaced=(m_spDriver.isNull() ||
			catalogOrDefault<bool>("Driver Surface","replaced",true));
	if(driverReplaced)
	{
		if(subdiv)
		{
			sp<SurfaceAccessibleI> spSubdividedAccessible=
					subdivide(spDriverAccessible,subdiv,0,a_rSignal);
			m_spDriver=spSubdividedAccessible.isValid()?
					spSubdividedAccessible->surface(driverGroup,restrictions):
					sp<SurfaceI>(NULL);
		}
		else
		{
//			if(!access(m_spDriver,spDriverAccessible,driverGroup)) return;
			m_spDriver=spDriverAccessible->surface(driverGroup,restrictions);
		}

		if(m_spDriver.isNull())
		{
			catalog<String>("error")=
					"failed to access Reference Driver Surface;";
			return;
		}

		if(m_spDriver->radius()<1e-6)
		{
			catalog<String>("error")="Reference Driver Surface appears empty"
					" (zero radius bounding sphere);";
			return;
		}

		m_curveLengthRef.resize(0);
		m_spRefEdged=NULL;
	}

	//* NOTE populate m_curveLengthRef
	if(scaleOffset && I32(m_curveLengthRef.size())!=driverPrimitiveCount)
	{
		m_curveLengthRef.resize(driverPrimitiveCount);
		for(I32 primitiveIndex=0; primitiveIndex<driverPrimitiveCount;
				primitiveIndex++)
		{
			const I32 subCount=spDriverVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				m_curveLengthRef[primitiveIndex]=Real(0);
				continue;
			}

			Real curveLength(0);

			const SpatialVector firstPoint=
					spDriverVertices->spatialVector(primitiveIndex,0);
			SpatialVector prevPoint=firstPoint;

			for(I32 subIndex=1;subIndex<subCount;subIndex++)
			{
				const SpatialVector point=
							spDriverVertices->spatialVector(
							primitiveIndex,subIndex);
				curveLength+=magnitude(point-prevPoint);
				prevPoint=point;
			}

			if(!spDriverProperties->integer(primitiveIndex,e_openCurve))
			{
				curveLength+=magnitude(prevPoint-firstPoint);
			}

			m_curveLengthRef[primitiveIndex]=curveLength;
		}
	}

	const I32 subdivFaceCount=m_spDriver->triangleCount();

#if FE_SWO_PROFILE
	m_spProfileDeformed->replace(m_spProfileDriver);
#endif

//~	const BWORD slowReplication=(FALSE && replicantCount>1);
//~
//~	sp<SurfaceAccessorI> spDeformedNormals;
//~	sp<SurfaceAccessorI> spDeformedUvs;
//~	sp<SurfaceAccessorI> spDeformedProperties;
//~	sp<SurfaceAccessorI> spSubsetPoints;
//~	sp<SurfaceAccessorI> spSubsetNormals;
//~	sp<SurfaceAccessorI> spSubsetUvs;
//~	sp<SurfaceAccessorI> spSubsetVertices;
//~	sp<SurfaceAccessorI> spSubsetProperties;
//~
//~	Array< Array<I32> > primVerts;

	sp<SurfaceAccessibleI> spSubsetAccessible=spDeformedAccessible;
//~	if(slowReplication)
//~	{
//~		spSubsetAccessible=registry()->create("*.SurfaceAccessibleCatalog");
//~
//~		if(!access(spDeformedNormals,spDeformedAccessible,
//~				e_point,e_normal)) return;
//~
//~		if(!access(spDeformedUvs,spDeformedAccessible,
//~				e_point,e_uv)) return;
//~
//~		if(!access(spDeformedProperties,spDeformedAccessible,
//~				e_primitive,e_properties)) return;
//~
//~		if(!access(spSubsetPoints,spSubsetAccessible,
//~				e_point,e_position)) return;
//~
//~		if(!access(spSubsetNormals,spSubsetAccessible,
//~				e_point,e_normal)) return;
//~
//~		if(!access(spSubsetUvs,spSubsetAccessible,
//~				e_point,e_uv)) return;
//~
//~		if(!access(spSubsetVertices,spSubsetAccessible,
//~				e_primitive,e_vertices)) return;
//~
//~		if(!access(spSubsetProperties,spSubsetAccessible,
//~				e_primitive,e_properties)) return;
//~
//~		primVerts.resize(driverPrimitiveCount);
//~	}

	for(I32 replicantIndex=0;replicantIndex<replicantCount;replicantIndex++)
	{
		m_deformedFaceOffset=replicantIndex*subdivFaceCount;
		m_outputElementOffset=replicantIndex*(m_bindPrimitives?
				inputPrimitiveCount: inputPointCount);

		//* deformed driver
//~		if(slowReplication || m_spDeformed.isNull())
		if(m_spDeformed.isNull())
		{
//~			if(slowReplication)
//~			{
//~				//* TODO abstract a general 'extract primitives' routine
//~
//~				const I32 primitiveOffset=
//~						replicantIndex*driverPrimitiveCount;
//~
//~				if(replicantIndex==0)
//~				{
//~					//* NOTE presumes segregated points
//~					I32 pointMin= -1;
//~					I32 pointMax= -1;
//~
//~					for(I32 primitiveIndex=0;
//~							primitiveIndex<driverPrimitiveCount;
//~							primitiveIndex++)
//~					{
//~						const I32 deformedIndex=
//~								primitiveOffset+primitiveIndex;
//~						const I32 subCount=
//~								spDeformedVertices->subCount(deformedIndex);
//~
//~						for(I32 subIndex=0;subIndex<subCount;subIndex++)
//~						{
//~							const I32 pointIndexDeformed=
//~									spDeformedVertices->integer(
//~									deformedIndex,subIndex);
//~							if(pointMin<0 || pointMin>pointIndexDeformed)
//~							{
//~								pointMin=pointIndexDeformed;
//~							}
//~							if(pointMax<pointIndexDeformed)
//~							{
//~								pointMax=pointIndexDeformed;
//~							}
//~						}
//~					}
//~
//~					const I32 pointCount=pointMax-pointMin+1;
//~
//~					for(I32 primitiveIndex=0;
//~							primitiveIndex<driverPrimitiveCount;
//~							primitiveIndex++)
//~					{
//~						const I32 deformedIndex=
//~								primitiveOffset+primitiveIndex;
//~						const I32 subCount=
//~								spDeformedVertices->subCount(deformedIndex);
//~
//~						Array<I32>& rVerts=primVerts[primitiveIndex];
//~						rVerts.resize(subCount);
//~
//~						for(I32 subIndex=0;subIndex<subCount;subIndex++)
//~						{
//~							const I32 pointIndexDeformed=
//~									spDeformedVertices->integer(
//~									deformedIndex,subIndex);
//~
//~							rVerts[subIndex]=pointIndexDeformed-pointMin;
//~						}
//~					}
//~
//~//					feLog("rep %d/%d offset %d point min %d max %d count %d\n",
//~//							replicantIndex,replicantCount,primitiveOffset,
//~//							pointMin,pointMax,pointCount);
//~
//~					spSubsetPoints->append(pointCount);
//~					spSubsetVertices->append(primVerts);
//~				}
//~
//~				for(I32 primitiveIndex=0;
//~						primitiveIndex<driverPrimitiveCount;
//~						primitiveIndex++)
//~				{
//~					const I32 deformedIndex=
//~							primitiveOffset+primitiveIndex;
//~
//~					const I32 properties=spDeformedProperties->integer(
//~							deformedIndex);
//~					spSubsetProperties->set(primitiveIndex,properties);
//~
//~					Array<I32>& rVerts=primVerts[primitiveIndex];
//~					const I32 subCount=rVerts.size();
//~
//~					for(I32 subIndex=0;subIndex<subCount;subIndex++)
//~					{
//~						const I32 pointIndex=rVerts[subIndex];
//~
//~						const SpatialVector point=
//~								spDeformedVertices->spatialVector(
//~								deformedIndex,subIndex);
//~						spSubsetPoints->set(pointIndex,point);
//~
//~						const I32 pointIndexDeformed=
//~								spDeformedVertices->integer(
//~								deformedIndex,subIndex);
//~
//~						const SpatialVector norm=
//~								spDeformedNormals->spatialVector(
//~								pointIndexDeformed);
//~						spSubsetNormals->set(pointIndex,norm);
//~
//~						const SpatialVector uvw=
//~								spDeformedUvs->spatialVector(
//~								pointIndexDeformed);
//~						spSubsetUvs->set(pointIndex,uvw);
//~					}
//~				}
//~			}

			sp<SurfaceAccessibleI> spSubdividedAccessible=spSubsetAccessible;
			if(subdiv)
			{
				spSubdividedAccessible=
						subdivide(spSubsetAccessible,subdiv,0,a_rSignal);
			}

//			if(!access(m_spDeformed,spSubdividedAccessible,
//					driverGroup,restrictions)) return;
			m_spDeformed=spSubdividedAccessible->surface(
					driverGroup,restrictions);

			m_curveLengthDef.resize(0);
		}

		if(m_spDeformed.isNull())
		{
			catalog<String>("error")=
					"failed to access Deformed Driver Surface;";
			return;
		}

		if(m_spDeformed->radius()<1e-6)
		{
			catalog<String>("error")=
					"Deformed Driver Surface appears empty"
					" (zero radius bounding sphere);";
			return;
		}

		//* NOTE populate m_curveLengthDef
		if(scaleOffset && I32(m_curveLengthDef.size())!=deformedPrimitiveCount)
		{
			m_curveLengthDef.resize(deformedPrimitiveCount);
			for(I32 primitiveIndex=0; primitiveIndex<deformedPrimitiveCount;
					primitiveIndex++)
			{
				const I32 subCount=spDeformedVertices->subCount(primitiveIndex);
				if(subCount<2)
				{
					m_curveLengthDef[primitiveIndex]=Real(0);
					continue;
				}

				Real curveLength(0);

				const SpatialVector firstPoint=
						spDeformedVertices->spatialVector(primitiveIndex,0);
				SpatialVector prevPoint=firstPoint;

				for(I32 subIndex=1;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=
								spDeformedVertices->spatialVector(
								primitiveIndex,subIndex);
					curveLength+=magnitude(point-prevPoint);
					prevPoint=point;
				}

				if(!spDeformedProperties->integer(primitiveIndex,e_openCurve))
				{
					curveLength+=magnitude(prevPoint-firstPoint);
				}

				m_curveLengthDef[primitiveIndex]=curveLength;
			}
		}

#if FE_SWO_PROFILE
		m_spProfilePrepare->replace(m_spProfileDeformed);
#endif

		m_spDriver->setSampleMethod(sampleMethod);
		m_spDeformed->setSampleMethod(sampleMethod);

		m_spDriver->prepareForSample();
		m_spDeformed->prepareForSample();

#if FE_SWO_PROFILE
		m_spProfileEdgy->replace(m_spProfilePrepare);
#endif

		if(doEdgy)
		{
			//* TODO to reuse, also check edgy params

			//* edgy reference driver
			if(m_spRefEdged.isNull())
			{
				sp<SurfaceAccessibleI> spSubdividedAccessible=
						subdivide(spDriverAccessible,
						subdiv,subdiv-edgyDepth,a_rSignal);
				m_spRefEdged=spSubdividedAccessible.isValid()?
						spSubdividedAccessible->surface(driverGroup):
						sp<SurfaceI>(NULL);
			}

			//* edgy deformed driver
//~			if(slowReplication || m_spDefEdged.isNull())
			if(m_spDefEdged.isNull())
			{
				sp<SurfaceAccessibleI> spSubdividedAccessible=
						subdivide(spSubsetAccessible,
						subdiv,subdiv-edgyDepth,a_rSignal);
				m_spDefEdged=spSubdividedAccessible.isValid()?
						spSubdividedAccessible->surface(driverGroup):
						sp<SurfaceI>(NULL);
			}

			m_spRefEdged->setSampleMethod(sampleMethod);
			m_spDefEdged->setSampleMethod(sampleMethod);

			m_spRefEdged->prepareForSample();
			m_spDefEdged->prepareForSample();
		}
		else
		{
			m_spRefEdged=NULL;
			m_spDefEdged=NULL;
		}

		m_lastSubdiv=subdiv;
		m_lastBindToPoints=bindToPoints;
		m_lastEdgyDepth=edgyDepth;

#if FE_SWO_PROFILE
		m_spProfileMoreAccess->replace(m_spProfileEdgy);
#endif

		m_element=m_bindPrimitives? e_primitive: e_point;
		m_attribute=m_bindPrimitives? e_vertices: e_position;

//~		sp<SurfaceAccessorI> spOutputElements;
//~		if(!access(spOutputElements,m_spOutputAccessible,
//~				m_element,m_attribute)) return;
//~
//~		const U32 elementCount=spOutputElements->count();

		const U32 elementCount=m_spInputAccessible->count(m_bindPrimitives?
				SurfaceAccessibleI::e_primitive:
				SurfaceAccessibleI::e_point);

		if(catalog<bool>("rotateNormals"))
		{
			sp<SurfaceAccessorI> spInputNormals;
			sp<SurfaceAccessorI> spOutputNormals;

			if(!access(spInputNormals,m_spInputAccessible,
					e_point,e_normal,e_quiet) ||
					!access(spOutputNormals,m_spOutputAccessible,
					e_point,e_normal,e_quiet))
			{
				catalog<String>("warning")+="can not rotate missing normals;";
			}
			else
			{
				//* flush normals
				const U32 normalCount=spInputNormals->count();
				for(U32 normalIndex=0;normalIndex<normalCount;normalIndex++)
				{
					spOutputNormals->set(normalIndex,
							spInputNormals->spatialVector(normalIndex));
				}
			}
		}

		m_spread=catalog<bool>("spread");
		if(m_spread)
		{
			//* TEMP only allow driver triangles for spread

			if(driverPrimitiveCount)
			{
				for(I32 driverPrimIndex=0;driverPrimIndex<driverPrimitiveCount;
						driverPrimIndex++)
				{
					if(spDriverVertices->subCount(driverPrimIndex)!=3)
					{
						catalog<String>("warning")+="currently, spread is"
								" only allowed on triangulated drivers;";
						m_spread=FALSE;
						break;
					}
				}
			}
			else
			{
				m_spread=FALSE;
			}

			if(m_spread && m_bindCount>1)
			{
				catalog<String>("warning")+=
						"currently, spread only uses single bindings;";
			}
		}

		if(m_spDrawDebug.isValid())
		{
			m_spDrawDebug->pushDrawMode(m_spDebugGroup);
		}

		U32 runCount=elementCount;
		m_spFragmentAccessor=NULL;
		if(m_fragmented)
		{
			if(access(m_spFragmentAccessor,m_spInputAccessible,
					m_element,m_attribute))
			{
				m_spFragmentAccessor->fragmentWith(
						SurfaceAccessibleI::Element(m_element),
						fragmentAttribute,inputGroup);
				runCount=m_spFragmentAccessor->fragmentCount();

				//* was (runCount<2), but there can be one fragment
				if(runCount<1)
				{
					catalog<String>("warning")+=
							"fragmentation failed to divide surface"
							" -> reverting";
					m_fragmented=FALSE;
					runCount=elementCount;
				}
			}
			else
			{
				catalog<String>("warning")+=
						"fragmentation access failed -> ignoring";
				m_fragmented=FALSE;
				runCount=elementCount;
			}
		}

		m_feedback.clear();

#if FE_SWO_PROFILE
		m_spProfileCache->replace(m_spProfileMoreAccess);
#endif

//		const I32 arrayCount=
//				catalog< Array< sp<Component> > >("Input Surfaces").size();

		//* TODO more cases
		const BWORD allowCache=(!replaced && !driverReplaced &&
//				!arrayCount &&
				!paramChanged &&
				!m_fragmented && m_element==OperatorSurfaceCommon::e_point);

#if FE_SWO_DEBUG
		feLog("SurfaceWrapOp::handle"
				" driverReplaced %d -> allowCache %d\n",
				driverReplaced,allowCache);
#endif

		const I32 cacheIndex=outputIteration+1;
		const I32 cacheCount=outputIterations+1;

		FEASSERT(cacheIndex>=0);
		FEASSERT(cacheIndex<cacheCount);

		if(allowCache)
		{
			m_faceCaches.resize(cacheCount);
			m_baryCaches.resize(cacheCount);
			m_driverSampleCaches.resize(cacheCount);
		}

		m_makeCache=(allowCache && !m_faceCaches[cacheIndex].size());
		if(m_makeCache)
		{
			const U32 cacheSize=m_bindCount*elementCount;

			m_faceCaches[cacheIndex].resize(cacheSize);
			m_baryCaches[cacheIndex].resize(cacheSize);
			m_driverSampleCaches[cacheIndex].resize(cacheSize);
		}
		else if(!allowCache)
		{
			m_faceCaches.clear();
			m_baryCaches.clear();
			m_driverSampleCaches.clear();
		}

#if FE_SWO_PROFILE
		m_spProfileRun->replace(m_spProfileCache);
#endif

		//* TODO multi-thread primitive fragments
		const BWORD primitiveFragments=(m_bindPrimitives && m_fragmented);
		if(primitiveFragments && catalog<I32>("Threads")>1)
		{
			catalog<String>("warning")+=
					"no multi-threading for primitive fragments;";
		}

		if(primitiveFragments || m_makeCache)
		{
			//* serial
			sp<SpannedRange> spFullRange=sp<SpannedRange>(new SpannedRange());
			spFullRange->nonAtomic().add(0,runCount-1);

			run(-1,spFullRange);
		}
		else
		{
			if(m_fragmented)
			{
				setNonAtomicRange(0,runCount-1);
			}
			else
			{
				setAtomicRange(m_spInputAccessible,
						m_bindPrimitives? e_pointsOfPrimitives: e_pointsOnly);
			}

			OperatorThreaded::handle(a_rSignal);
		}

#if FE_SWO_PROFILE
		if(replicantIndex!=replicantCount-1)
		{
			m_spProfileDeformed->replace(m_spProfileFinish);
		}
#endif
	}

//~	if(slowReplication)
//~	{
//~		m_spDeformed=NULL;
//~		m_spDefEdged=NULL;
//~	}

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::handle resolve\n");
#endif

#if FE_SWO_PROFILE
	m_spProfileResolve->replace(m_spProfileFinish);
#endif

	if(m_spDrawDebug.isValid())
	{
		m_spDrawDebug->popDrawMode();
	}

	if(catalog<bool>("DiscardBindings"))
	{
		discardBindings(m_spOutputAccessible,m_bindCount,m_bindPrimitives);
		discardSignature(m_spOutputAccessible);
		discardParameters(m_spOutputAccessible);
	}

	//* output for brush probing
	accessOutput(m_spOutput,a_rSignal,e_warning);

	if(m_fragmented)
	{
		rSummary="fragments";
	}
	else if(m_bindPrimitives)
	{
		rSummary="primitives";
	}
	else
	{
		rSummary="points";
	}
	if(m_bindCount>1)
	{
		const I32 trueBind=m_bindCount/m_samples;

		String text;
		if(trueBind>1)
		{
			text.sPrintf(" x%d",trueBind);
			rSummary+=text;
		}
		if(m_samples>1)
		{
			text.sPrintf(" s%d",m_samples);
			rSummary+=text;
		}
	}
	if(subdiv)
	{
		String text;
		text.sPrintf(" d%d",subdiv);
		rSummary+=text;

		if(doEdgy)
		{
			rSummary+="e";
		}
	}
	if(replicantCount>1)
	{
		String text;
		text.sPrintf(" r%d",replicantCount);
		rSummary+=text;
	}

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::handle done\n");
#endif

#if FE_SWO_PROFILE
	m_spProfileResolve->finish();
	m_spProfiler->end();

	feLog("%s:\n%s\n",m_spProfiler->name().c_str(),
			m_spProfiler->report().c_str());
#endif
}

void SurfaceWrapOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
#if FE_SWO_PROFILE
	if(a_id<1)
	{
		m_spProfilePrep->replace(m_spProfileRun);
	}
#endif

	const BWORD scaleOffset=catalog<bool>("scaleOffset");
	const BWORD singleTransform=catalog<bool>("singleTransform");
	const Real singleBias=catalog<Real>("singleBias");
	const I32 spreadDepth=catalog<I32>("spreadDepth");
	const BWORD rotateNormals=catalog<bool>("rotateNormals");
	const Real distanceBias=catalog<Real>("Distance Bias");
	const Real distanceScale=catalog<Real>("Distance Scale");
	const I32 debugIndex=catalog<I32>("Debug Index");
	const Real edginess=catalog<Real>("edginess");
	const String edgyAttr=catalog<String>("edgyAttr");
	const BWORD fadeToRoot=catalog<bool>("fadeToRoot");
	const Real fadeInnerDistance=catalog<Real>("fadeInnerDistance");
	const Real fadeOuterDistance=fe::maximum(Real(fadeInnerDistance+1e-3),
			catalog<Real>("fadeOuterDistance"));

	const BWORD generateLocators=(m_fragmented && !m_locatorPrefix.empty());
	const BWORD cacheOutput=FALSE;

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::run running thread %d %s\n",
			a_id,a_spRange->brief().c_str());
#endif

	const I32 outputIteration=
			catalogOrDefault<I32>("Output Surface","iteration",-1);
	const I32 cacheIndex=outputIteration+1;

	Array<I32>* pFaceCache=NULL;
	Array<SpatialBary>*	pBaryCache=NULL;
	Array<SpatialTransform>* pDriverSampleCache=NULL;

	if(cacheIndex<I32(m_faceCaches.size()))
	{
		pFaceCache= &m_faceCaches[cacheIndex];
		pBaryCache= &m_baryCaches[cacheIndex];
		pDriverSampleCache= &m_driverSampleCaches[cacheIndex];
	}

	const BWORD useCache=(!m_makeCache && pFaceCache);
//	feLog("%d makeCache %d useCache %d\n",a_id,m_makeCache,useCache);

	sp<SurfaceAccessorI> spInputElements;
	if(!access(spInputElements,m_spInputAccessible,
			m_element,m_attribute)) return;

	//* TODO edgy attr with primitive bindings?
	sp<SurfaceAccessorI> spInputEdgy;
	if(m_spRefEdged.isValid() && m_spDefEdged .isValid()&&
			!edgyAttr.empty() && m_element==OperatorSurfaceCommon::e_point)
	{
		access(spInputEdgy,m_spInputAccessible,e_point,edgyAttr,e_warning);
	}

	sp<SurfaceAccessorI> spOutputElements;
	if(cacheOutput)
	{
		sp<SurfaceAccessorI> spOutputElementsDirect;
		if(!access(spOutputElementsDirect,m_spOutputAccessible,
				m_element,m_attribute))
		{
#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileFinish->replace(m_spProfilePrep);
			}
#endif

			return;
		}

		sp<SurfaceAccessorCached> spAccessorCached=
				registry()->create("*.SurfaceAccessorCached");
		FEASSERT(spAccessorCached.isValid());
		spAccessorCached->setSurfaceAccessible(m_spOutputAccessible);
		spAccessorCached->setAccessorChain(spOutputElementsDirect);

		spOutputElements=spAccessorCached;
	}
	else
	{
		if(!access(spOutputElements,m_spOutputAccessible,
				m_element,m_attribute))
		{
#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileFinish->replace(m_spProfilePrep);
			}
#endif

			return;
		}
	}

	sp<SurfaceAccessorI> spInputNormals;
	sp<SurfaceAccessorI> spOutputNormals;
	if(rotateNormals)
	{
		access(spInputNormals,m_spInputAccessible,
				e_point,e_normal,e_quiet);
		access(spOutputNormals,m_spOutputAccessible,
				e_point,e_normal,e_quiet);
	}

	Array< sp<SurfaceAccessorI> > driverFaces;
	Array< sp<SurfaceAccessorI> > driverBarys;
	const I32 bindingsAccessed=accessBindings(m_spBindingAccessible,
			driverFaces,driverBarys,m_bindCount,m_bindPrimitives,FALSE,FALSE);

	if(bindingsAccessed<1)
	{
#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileFinish->replace(m_spProfilePrep);
		}
#endif

		feLog("SurfaceWrapOp::run no bindings\n");
		return;
	}

	sp<SurfaceAccessorI> spInputPeak;
	if(bindingsAccessed>1)
	{
		const String bindPrefix=catalog<String>("BindPrefix");

		String peakName;
		peakName.sPrintf("%sPeak",bindPrefix.c_str());

		access(spInputPeak,m_spInputAccessible,m_element,peakName);
	}

	const SpatialBary baryThird(0.33,0.33);

	sp<SurfaceAccessorI> spDriverVertices;
	if(!access(spDriverVertices,"Driver Surface",
			e_primitive,e_vertices)) return;
	const U32 driverPrimCount=spDriverVertices->count();
	if(!driverPrimCount)
	{
		spDriverVertices=NULL;
	}

	const BWORD doSpread=(m_spread && spDriverVertices.isValid());

	Array< std::set<U32> > pointPrimitives;
	if(doSpread)
	{
		sp<SurfaceAccessorI> spInputPoints;
		if(!access(spInputPoints,m_spInputAccessible,e_point,
				e_position,e_quiet)) return;

		const U32 pointCount=spInputPoints->count();
		pointPrimitives.resize(pointCount);

		for(U32 primitiveIndex=0;primitiveIndex<driverPrimCount;
				primitiveIndex++)
		{
			const U32 subCount=spDriverVertices->subCount(primitiveIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 pointIndex=
						spDriverVertices->integer(primitiveIndex,subIndex);
				pointPrimitives[pointIndex].insert(primitiveIndex);
			}
		}
	}

	//* classic 8500
	//* preload 1100+1000+6900

	const BWORD singular=
//			(!m_fragmented || m_element==SurfaceAccessibleI::e_point);
			(m_fragmented && m_element==OperatorSurfaceCommon::e_point);

	const BWORD sameIndex=
			(!m_fragmented && m_element==OperatorSurfaceCommon::e_point);

	const BWORD preload=sameIndex;

	sp<SurfaceAccessibleI::FilterI> spFilter;

	Array<Vector2i> spreadData;
	Array<SpatialVector> spreadWeight;

	I32 rangeCount=0;

#if FE_SWO_PROFILE
	if(a_id<1)
	{
		m_spProfilePreload->replace(m_spProfilePrep);
	}
#endif

	if(preload)
	{
		rangeCount=a_spRange->valueCount();
	}

	SpatialVector* preloadPoint=new SpatialVector[rangeCount+1];

	if(preload)
	{
		Vector2i* preloadPair=new Vector2i[rangeCount];

		I32 rangeIndex=0;
		for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
		{
			const I32 inputIndex=it.value();
			set(preloadPair[rangeIndex++],inputIndex,0);
		}

		spInputElements->spatialVector(preloadPoint,preloadPair,rangeCount);

		delete[] preloadPair;
	}

#if FE_SWO_PROFILE
	if(a_id<1)
	{
		m_spProfileIterate->replace(m_spProfilePreload);
	}
#endif

	//* NOTE don't instance strings on every iteration
	String locatorPreface;
	String fragment;

	I32 rangeIndex=0;
	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const I32 inputIndex=it.value();
		const I32 outputIndex=m_outputElementOffset+it.value();

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileFragment->replace(m_spProfileIterate);
		}
#endif

		I32 filterCount=1;
		U32 tableCount=1;
		if(m_fragmented)
		{
			tableCount=0;
			const String fragment=m_spFragmentAccessor->fragment(inputIndex);
			if(!m_spFragmentAccessor->filterWith(fragment,spFilter) ||
					spFilter.isNull())
			{
				continue;
			}

			filterCount=spFilter->filterCount();
			if(filterCount<1)
			{
				continue;
			}

			locatorPreface=m_locatorPrefix+"_"+fragment+"_";

			if(singular)
			{
				tableCount+=filterCount;
			}
			else
			{
				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 filtered=spFilter->filter(filterIndex);
					FEASSERT(filtered>=0);
					tableCount+=spInputElements->subCount(filtered);
				}
			}
		}
		else if(!singular)
		{
			tableCount=spInputElements->subCount(inputIndex);
		}

		if(tableCount<1)
		{
#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileIterate->replace(m_spProfileFragment);
			}
#endif

			continue;
		}

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileFilter->replace(m_spProfileFragment);
		}
#endif

		SpatialVector* point=new SpatialVector[tableCount];
		SpatialVector* normal=new SpatialVector[tableCount];
		SpatialVector* resultPoint=new SpatialVector[tableCount];
		SpatialVector* resultNormal=new SpatialVector[tableCount];
		Real* totalWeight=new Real[tableCount];

		I32 feedbackIndex= -1;
		U32 tableIndex=0;

		if(sameIndex)
		{
			if(outputIndex==m_selectIndex)
			{
				feedbackIndex=0;
			}

			point[0]=preload? preloadPoint[rangeIndex]:
					spInputElements->spatialVector(inputIndex);

			if(spInputNormals.isValid())
			{
				normal[0]=spInputNormals->spatialVector(inputIndex);
			}
			set(resultPoint[0]);
			set(resultNormal[0]);
			totalWeight[0]=0.0;
		}
		else
		{
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 filtered=m_fragmented?
						spFilter->filter(filterIndex): inputIndex;
				FEASSERT(filtered>=0);
				const U32 subCount=
						singular? 1: spInputElements->subCount(filtered);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=m_bindPrimitives?
							spInputElements->integer(filtered,subIndex):
							filtered;
					if(pointIndex==m_selectIndex)
					{
						feedbackIndex=tableIndex;
					}

					point[tableIndex]=preload? preloadPoint[rangeIndex]:
							spInputElements->spatialVector(filtered,subIndex);

					if(spInputNormals.isValid())
					{
						normal[tableIndex]=
								spInputNormals->spatialVector(pointIndex);
					}
					set(resultPoint[tableIndex]);
					set(resultNormal[tableIndex]);
					totalWeight[tableIndex]=0.0;
					tableIndex++;
				}
			}
		}

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileSpread->replace(m_spProfileFilter);
		}
#endif

		const I32 keyIndex=m_fragmented? spFilter->filter(0): inputIndex;
		FEASSERT(keyIndex>=0);

		//* spread Vector2i: <face, mask>
		if(doSpread)
		{
			spreadData.clear();
			spreadWeight.clear();

			//* HACK presuming triangles only
			//* TODO quads

			const U32 binding=0;	//* TODO start with multiple bindings

			const I32 face=driverFaces[binding]->integer(keyIndex);
			const SpatialBary bary=
					driverBarys[binding]->spatialVector(keyIndex);

			spreadData.push_back(Vector2i(face,7));
			spreadWeight.push_back(SpatialVector(bary[0],bary[1],
						1.0-bary[0]-bary[1]));

			for(I32 spreadIndex=0;spreadIndex<spreadDepth;spreadIndex++)
			{
				//* accumulate weight per vertex
				std::map<U32,Real> weightMap;
				const U32 faceCount=spreadData.size();
				for(U32 faceIndex=0;faceIndex<faceCount;faceIndex++)
				{
					const I32 spreadFace=spreadData[faceIndex][0];
					const SpatialVector weights=spreadWeight[faceIndex];

					for(U32 subIndex=0;subIndex<3;subIndex++)
					{
						const U32 pointIndex=
								spDriverVertices->integer(spreadFace,subIndex);
						weightMap[pointIndex]+=weights[subIndex];
					}
				}

				spreadData.clear();
				spreadWeight.clear();

				for(std::map<U32,Real>::iterator it2=weightMap.begin();
						it2!=weightMap.end();it2++)
				{
					const U32 pointIndex=it2->first;
					const Real totalWeight=it2->second;

					std::set<U32>& rPrimitiveSet=pointPrimitives[pointIndex];
					const U32 shares=rPrimitiveSet.size();
					FEASSERT(shares);

					const Real shareWeight=totalWeight/Real(shares);

					for(std::set<U32>::iterator it2=rPrimitiveSet.begin();
							it2!=rPrimitiveSet.end();it2++)
					{
						const U32 primitiveIndex= *it2;

						//* HACK
						const I32 spreadFace=primitiveIndex;

						const U32 faceCount=spreadData.size();
						U32 faceIndex;
						for(faceIndex=0;faceIndex<faceCount;faceIndex++)
						{
							const I32 otherFace=spreadData[faceIndex][0];

							if(otherFace==spreadFace)
							{
								spreadWeight[faceIndex]+=shareWeight*
										SpatialVector(1.0/3.0,1.0/3.0,1.0/3.0);
								break;
							}
						}

						if(faceIndex==faceCount)
						{
							spreadData.push_back(Vector2i(spreadFace,7));
							spreadWeight.push_back(shareWeight*
									SpatialVector(1.0/3.0,1.0/3.0,1.0/3.0));
						}
					}
				}
			}

			//* put original binding first
			const U32 faceCount=spreadData.size();
			for(U32 faceIndex=0;faceIndex<faceCount;faceIndex++)
			{
				const I32 spreadFace=spreadData[faceIndex][0];
				if(spreadFace==face)
				{
					if(faceIndex)
					{
						const Vector2i data=spreadData[faceIndex];
						const SpatialVector weights=spreadWeight[faceIndex];

						spreadData[faceIndex]=spreadData[0];
						spreadWeight[faceIndex]=spreadWeight[0];

						spreadData[0]=data;
						spreadWeight[0]=weights;
					}
					break;
				}
			}
		}

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileDistance->replace(m_spProfileSpread);
		}
#endif

		I32* sampleFace=new I32[bindingsAccessed];
		SpatialBary* sampleBary=new SpatialBary[bindingsAccessed];
		SpatialTransform* driverSample=new SpatialTransform[bindingsAccessed];

		Real* bindingDistance=new Real[bindingsAccessed];
		Real peakDistance=(bindingsAccessed<2)? 1.0: 0.0;

		for(I32 binding=0;binding<bindingsAccessed;binding++)
		{
			I32& rFace=sampleFace[binding];
			SpatialBary& rBary=sampleBary[binding];
			SpatialTransform& rDriverSample=driverSample[binding];

			const I32 cacheKey=keyIndex*m_bindCount+binding;

			if(useCache)
			{
				rFace=(*pFaceCache)[cacheKey];
				if(rFace<0)
				{
					bindingDistance[binding]= -1.0;
					set(rBary);
					continue;
				}

				rBary=(*pBaryCache)[cacheKey];
				rDriverSample=(*pDriverSampleCache)[cacheKey];
			}
			else
			{
				rFace=driverFaces[binding]->integer(keyIndex);

				if(rFace<0)
				{
					bindingDistance[binding]= -1.0;
					set(rBary);

					if(m_makeCache)
					{
						(*pFaceCache)[cacheKey]=rFace;
					}

					continue;
				}

				rBary=driverBarys[binding]->spatialVector(keyIndex);
				rDriverSample=m_spDriver->sample(rFace,rBary);

				if(m_makeCache)
				{
					(*pFaceCache)[cacheKey]=rFace;
					(*pBaryCache)[cacheKey]=rBary;
					(*pDriverSampleCache)[cacheKey]=rDriverSample;
				}
			}

			if(bindingsAccessed>1 || fadeToRoot)
			{
				const SpatialVector to=rDriverSample.translation();

				const Real distance=magnitude(to-point[0]);

				bindingDistance[binding]=distance;
				if(peakDistance<distance)
				{
					peakDistance=distance;
				}
			}
		}

		if(spInputPeak.isValid())
		{
			peakDistance=spInputPeak->real(inputIndex);
		}

		SpatialTransform coreConversion;
		if(singleTransform)
		{
			set(coreConversion.column(0));
			set(coreConversion.column(1));
			set(coreConversion.column(2));
			set(coreConversion.column(3));

			Real totalWeight=0.0;
			for(I32 binding=0;binding<bindingsAccessed;binding++)
			{
				const I32 face=sampleFace[binding];
				if(face<0)
				{
					continue;
				}
				const SpatialBary bary=sampleBary[binding];

				SpatialTransform xformDriver=driverSample[binding];
				SpatialTransform xformDeformed=
						m_spDeformed->sample(m_deformedFaceOffset+face,bary);

				SpatialTransform invDriver;
				invert(invDriver,xformDriver);
				SpatialTransform conversion=invDriver*xformDeformed;

				const Real distance=
						magnitude(xformDriver.translation()-point[0]);

				//* in case all points are equally distant
				const Real minWeight=binding? 0.0: 1e-3;

				const Real weight=calculateWeight(distance,peakDistance,
						singleBias,1.0,minWeight);

				coreConversion=coreConversion+weight*conversion;

				totalWeight+=weight;
			}
			SpatialVector coreX=coreConversion.column(0);
			SpatialVector coreY=unitSafe(coreConversion.column(1));
			SpatialVector coreT=coreConversion.translation()*
					(totalWeight>0.0? 1.0/totalWeight: 1.0);
			makeFrameNormalY(coreConversion,coreT,coreX,coreY);
		}

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileLoop->replace(m_spProfileDistance);
		}
#endif

		//* TODO multiple bindings with multiple samples
		const BWORD tubeWrap=(m_fragmented && m_samples>1);
		if(tubeWrap)
		{
			const Real bendLimit=
					catalog<Real>("bendLimit")*fe::degToRad/Real(m_samples-1);

			SpatialTransform* deformedSample=new SpatialTransform[m_samples];

			for(I32 sampleIndex=0;sampleIndex<m_samples;sampleIndex++)
			{
				deformedSample[sampleIndex]=m_spDeformed->sample(
						m_deformedFaceOffset+sampleFace[sampleIndex],
						sampleBary[sampleIndex]);

#if FE_SWO_SAMPLE_VERBOSE
				if(feedbackIndex>=0)
				{
					feLog("sample %d/%d face %d bary %s\n",
							sampleIndex,m_samples,
							sampleFace[sampleIndex],
							c_print(sampleBary[sampleIndex]));
					feLog("driverSample[%d]\n%s\n",sampleIndex,
							c_print(driverSample[sampleIndex]));
					feLog("deformedSample[%d]\n%s\n",sampleIndex,
							c_print(deformedSample[sampleIndex]));
				}
#endif
			}

			const SpatialVector refPivot=
					driverSample[0].translation();
			const SpatialVector refTip=
					driverSample[m_samples-1].translation();
			const Real refLength=magnitude(refTip-refPivot);
			const SpatialVector refArm=(refTip-refPivot)*
					(refLength>Real(0)? Real(1)/refLength: Real(1));

			const SpatialVector defPivot=
					deformedSample[0].translation();
			const SpatialVector defTip=
					deformedSample[m_samples-1].translation();
			const Real defLength=magnitude(defTip-defPivot);
//			const SpatialVector defArm=(defTip-defPivot)*
//					(defLength>Real(0)? Real(1)/defLength: Real(1));

			//* remove reference base rotation

			SpatialTransform* driverAligned=new SpatialTransform[m_samples];
			SpatialTransform* deformedAligned=new SpatialTransform[m_samples];

			SpatialTransform driverBaseInv;
			invert(driverBaseInv,driverSample[0]);

			for(I32 sampleIndex=0;sampleIndex<m_samples;sampleIndex++)
			{
				const SpatialTransform driverSpan=
						driverSample[sampleIndex]*driverBaseInv;

				SpatialTransform rotateDriver=driverSpan;
				set(rotateDriver.translation());

				SpatialTransform invRotateDriver;
				invert(invRotateDriver,rotateDriver);

				driverAligned[sampleIndex]=
						invRotateDriver*driverSample[sampleIndex];
				deformedAligned[sampleIndex]=
						invRotateDriver*deformedSample[sampleIndex];
#if FE_SWO_SAMPLE_VERBOSE
				if(feedbackIndex>=0)
				{
					feLog("driverAligned[%d]\n%s\n",sampleIndex,
							c_print(driverAligned[sampleIndex]));
					feLog("deformedAligned[%d]\n%s\n",sampleIndex,
							c_print(deformedAligned[sampleIndex]));
				}
#endif
			}

			SpatialTransform* driverDelta=new SpatialTransform[m_samples-1];
			SpatialTransform* deformedDelta=new SpatialTransform[m_samples-1];

			for(I32 sampleIndex=0;sampleIndex<m_samples-1;sampleIndex++)
			{
				SpatialTransform driverInv;
				SpatialTransform deformedInv;

				invert(driverInv,driverAligned[sampleIndex]);
				invert(deformedInv,deformedAligned[sampleIndex]);

				//* see CurveCreateOp::interpolateTransform()

				const SpatialTransform driverProposed=
						driverAligned[sampleIndex+1]*driverInv;
				const SpatialTransform deformedProposed=
						deformedAligned[sampleIndex+1]*deformedInv;

				SpatialQuaternion driverQuat(driverProposed);
				SpatialQuaternion deformedQuat(deformedProposed);

				Real driverRadians;
				Real deformedRadians;

				SpatialVector driverAxis;
				SpatialVector deformedAxis;

				driverQuat.computeAngleAxis(driverRadians,driverAxis);
				deformedQuat.computeAngleAxis(deformedRadians,deformedAxis);

				//* NOTE hold back from 180 degrees (bad for power curves)
				const Real driverLimited=
						fe::minimum(bendLimit,driverRadians);
				const Real deformedLimited=
						fe::minimum(bendLimit,deformedRadians);

				SpatialQuaternion driverMod(
						driverLimited,driverAxis);
				SpatialQuaternion deformedMod(
						deformedLimited,deformedAxis);

				driverDelta[sampleIndex]=driverMod;
				deformedDelta[sampleIndex]=deformedMod;

				setTranslation(driverDelta[sampleIndex],
						driverProposed.translation());
				setTranslation(deformedDelta[sampleIndex],
						deformedProposed.translation());

#if FE_SWO_SAMPLE_VERBOSE
				if(feedbackIndex>=0)
				{
					feLog("sample %d/%d\n",sampleIndex,m_samples);
					feLog("driverQuat %s\n",c_print(driverQuat));
					feLog("deformedQuat %s\n",c_print(deformedQuat));
					feLog("driverRadians %.6G driverAxis %s\n",
							driverRadians,c_print(driverAxis));
					feLog("deformedRadians %.6G deformedAxis %s\n",
							deformedRadians,c_print(deformedAxis));
					feLog("driverLimited %.6G deformedLimited %.6G\n",
							driverLimited,deformedLimited);
					feLog("driverProposed\n%s\n",c_print(driverProposed));
					feLog("deformedProposed\n%s\n",c_print(deformedProposed));
					feLog("driverDelta[%d]\n%s\n",sampleIndex,
							c_print(driverDelta[sampleIndex]));
					feLog("deformedDelta[%d]\n%s\n",sampleIndex,
							c_print(deformedDelta[sampleIndex]));
				}
#endif
			}

			Real* driverDistance=new Real[tableCount];
			Real maxDistance=Real(0);
			for(tableIndex=0;tableIndex<tableCount;tableIndex++)
			{
				const SpatialVector refPoint=point[tableIndex];

				//* NOTE without perpendicular component
				Real& rDistance=driverDistance[tableIndex];
				rDistance=dot(refPoint-refPivot,refArm);
				if(maxDistance<rDistance)
				{
					maxDistance=rDistance;
				}
			}
			const Real distanceScale=refLength/maxDistance;
			for(tableIndex=0;tableIndex<tableCount;tableIndex++)
			{
				const SpatialVector refPoint=point[tableIndex];
				const Real oneDistance=
						distanceScale*driverDistance[tableIndex];
				const Real driverAlong=
						refLength>Real(0)? oneDistance/refLength: Real(0);
				const Real deformedAlong=fe::minimum(driverAlong,
						defLength>Real(0)? oneDistance/defLength: Real(0));

				const Real driverReal=driverAlong*(m_samples-1);
				const Real deformedReal=deformedAlong*(m_samples-1);
				const I32 driverIndex=fe::maximum(I32(0),
						fe::minimum(m_samples-2,I32(driverReal)));
				const I32 deformedIndex=fe::maximum(I32(0),
						fe::minimum(m_samples-2,I32(deformedReal)));
				const Real driverWithin=fe::maximum(Real(0),
						fe::minimum(Real(1),driverReal-driverIndex));
				const Real deformedWithin=fe::maximum(Real(0),
						fe::minimum(Real(1),deformedReal-deformedIndex));

				SpatialTransform driverPartial;
				SpatialTransform deformedPartial;

//				MatrixBezier<SpatialTransform> matrixBezier;
//				matrixBezier.solve(partial,deltaConversion,along);
				MatrixPower<SpatialTransform> matrixPower;
#if FALSE
				matrixPower.solve(driverPartial,
						driverDelta[driverIndex],driverWithin);
#else
				//* NOTE driver delta shouldn't have rotation
				setIdentity(driverPartial);
				setTranslation(driverPartial,
						driverDelta[driverIndex].translation()*driverWithin);
#endif
				matrixPower.solve(deformedPartial,
						deformedDelta[deformedIndex],deformedWithin);

				const SpatialTransform driverBetween=
						driverPartial*driverAligned[driverIndex];
				const SpatialTransform deformedBetween=
						deformedPartial*deformedAligned[deformedIndex];

				SpatialTransform invBetweenDriver;
				invert(invBetweenDriver,driverBetween);

				SpatialTransform conversion=invBetweenDriver*deformedBetween;

				SpatialVector transformedPoint;
				transformVector(conversion,refPoint,transformedPoint);
				resultPoint[tableIndex]=transformedPoint;

				if(rotateNormals)
				{
					SpatialVector transformedNormal;
					rotateVector(conversion,normal[tableIndex],
							transformedNormal);
					resultNormal[tableIndex]=transformedNormal;
				}

				totalWeight[tableIndex]=Real(1);

				if(I32(tableIndex)==feedbackIndex)
				{
					sp<SurfaceI::ImpactI> spImpact=m_spDeformed->sampleImpact(
							sampleFace[deformedIndex],
							sampleBary[deformedIndex]);
					if(spImpact.isValid())
					{
						m_feedback.push_back(Feedback(1.0,spImpact));
					}
					spImpact=m_spDeformed->sampleImpact(
							sampleFace[deformedIndex+1],
							sampleBary[deformedIndex+1]);
					if(spImpact.isValid())
					{
						m_feedback.push_back(Feedback(1.0,spImpact));
					}

#if FE_SWO_SAMPLE_VERBOSE
					feLog("%d/%d samples %d dist %.6G along %.6G %.6G\n"
							"  real %.6G %.6G index %d %d within %.6G %.6G\n",
							tableIndex,tableCount,m_samples,oneDistance,
							driverAlong,deformedAlong,
							driverReal,deformedReal,
							driverIndex,deformedIndex,
							driverWithin,deformedWithin);

					feLog("driverPartial\n%s\n",c_print(driverPartial));
					feLog("deformedPartial\n%s\n",c_print(deformedPartial));
					feLog("driverBetween\n%s\n",c_print(driverBetween));
					feLog("deformedBetween\n%s\n",c_print(deformedBetween));
					feLog("conversion\n%s\n",c_print(conversion));
#endif
				}
			}

			delete[] driverDistance;

			delete[] deformedDelta;
			delete[] driverDelta;

			delete[] deformedAligned;
			delete[] driverAligned;

			delete[] deformedSample;
		}

		SpatialVector baryWeights;

		const U32 bindCount=tubeWrap? 0:
				(doSpread? spreadData.size(): bindingsAccessed);
		for(U32 binding=0;binding<bindCount;binding++)
		{
			const I32 face=doSpread? spreadData[binding][0]:
					sampleFace[binding];
			if(face<0)
			{
				continue;
			}

			const I32 reface=m_deformedFaceOffset+face;

#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileBary->replace(m_spProfileLoop);
			}
#endif

			SpatialTransform conversion;
			SpatialVector translateDriver;
			SpatialVector translateDeformed;

			SpatialBary bary=baryThird;
			if(!doSpread || !binding)
			{
				bary=sampleBary[binding];
			}
			else
			{
				bary.solve(
						spDriverVertices->spatialVector(face,0),
						spDriverVertices->spatialVector(face,1),
						spDriverVertices->spatialVector(face,2),
						point[0]);

				//* clamp to triangle
				bary.clamp(
						spDriverVertices->spatialVector(face,0),
						spDriverVertices->spatialVector(face,1),
						spDriverVertices->spatialVector(face,2));
			}

			if(doSpread)
			{
				//* TODO proportional to area

				baryWeights=spreadWeight[binding];
			}

#if FE_SWO_VERBOSE
			feLog("SurfaceWrapOp::run element %d bind %d/%d\n",
					inputIndex,binding,bindingsAccessed);
#endif

#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileSample->replace(m_spProfileBary);
			}
#endif

			Real edgyScale=edginess;
			if(spInputEdgy.isValid())
			{
				edgyScale*=spInputEdgy->real(keyIndex);
			}

			if(!singleTransform)
			{
				SpatialTransform xformDriver=driverSample[binding];
				SpatialTransform xformDeformed=m_spDeformed->sample(
						reface,bary);
				if(m_spDefEdged.isValid())
				{
					SpatialTransform xformRefEdged=
							m_spRefEdged->sample(face,bary);
					SpatialTransform xformDefEdged=
							m_spDefEdged->sample(reface,bary);

#if FE_SWO_VERBOSE
					feLog("xformDriver (pure)\n%s\n",c_print(xformDriver));
					feLog("xformDeformed (pure)\n%s\n",c_print(xformDeformed));

					feLog("xformRefEdged\n%s\n",c_print(xformRefEdged));
					feLog("xformDefEdged\n%s\n",c_print(xformDefEdged));
#endif

					xformDriver.translation()=
							edgyScale*xformRefEdged.translation()+
							(1.0-edgyScale)*xformDriver.translation();
					xformDeformed.translation()=
							edgyScale*xformDefEdged.translation()+
							(1.0-edgyScale)*xformDeformed.translation();
				}

#if FE_SWO_VERBOSE
				feLog("xformDriver\n%s\n",c_print(xformDriver));
				feLog("xformDeformed\n%s\n",c_print(xformDeformed));
#endif

#if FE_SWO_PROFILE
				if(a_id<1)
				{
					m_spProfileInvert->replace(m_spProfileSample);
				}
#endif

				SpatialTransform invDriver;
				invert(invDriver,xformDriver);

				if(scaleOffset)
				{
					SpatialTransform xformScale;
					setIdentity(xformScale);

					const Real refLength=m_curveLengthRef[face];
					const Real defLength=m_curveLengthDef[reface];

					if(refLength>Real(0))
					{
						const Real scalar=defLength/refLength;
						scale(xformScale,SpatialVector(1.0,scalar,scalar));
					}

					conversion=invDriver*xformScale*xformDeformed;
				}
				else
				{
					conversion=invDriver*xformDeformed;
				}

				translateDriver=xformDriver.translation();
				translateDeformed=xformDeformed.translation();

#if FALSE
				feLog("xformDriver\n%s\n",c_print(xformDriver));
				feLog("xformDeformed\n%s\n",c_print(xformDeformed));
#endif

#if FE_SWO_PROFILE
				if(a_id<1)
				{
					m_spProfileWeight->replace(m_spProfileInvert);
				}
#endif
			}
			else
			{
				//* TODO sample translation only
				translateDriver=driverSample[binding].translation();
				translateDeformed=m_spDeformed->sample(
						reface,bary).translation();

				SpatialVector refDeformed;
				transformVector(coreConversion,translateDriver,refDeformed);

				conversion=coreConversion;
				conversion.translation()+=translateDeformed-refDeformed;

#if FE_SWO_PROFILE
				if(a_id<1)
				{
					m_spProfileWeight->replace(m_spProfileSample);
				}
#endif
			}

			Real weight=0.0;

			const Real distance=bindingDistance[binding];

			if(doSpread)
			{
				const I32 mask=spreadData[binding][1];
				if(mask&1)
				{
					weight+=baryWeights[0];
				}
				if(mask&2)
				{
					weight+=baryWeights[1];
				}
				if(mask&4)
				{
					weight+=baryWeights[2];
				}
			}
			else if(bindingsAccessed<2)
			{
				weight=1.0;
			}
			else
			{
				//* in case all points are equally distant
				const Real minWeight=binding? 0.0: 1e-3;

				weight=calculateWeight(distance,peakDistance,
						distanceBias,distanceScale,minWeight);

//				feLog("%d bind %d/%d face %d"
//						" dist %.6G/%.6G weight %.6G\n",
//						inputIndex,binding,bindingsAccessed,face,
//						distance,peakDistance,weight);
			}

			Real rootWeight(0);
			if(fadeToRoot)
			{
				const Real fade=fe::maximum(Real(0),fe::minimum(Real(1),
						(distance-fadeInnerDistance)/
						(fadeOuterDistance-fadeInnerDistance)));
				rootWeight=weight*fade;
				weight*=Real(1)-fade;
			}

#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileTable->replace(m_spProfileWeight);
			}
#endif

			for(tableIndex=0;tableIndex<tableCount;tableIndex++)
			{
				if(I32(tableIndex)==feedbackIndex)
				{
					sp<SurfaceI::ImpactI> spImpact=m_spDeformed->sampleImpact(
							face,bary);
					if(spImpact.isValid())
					{
						m_feedback.push_back(Feedback(weight,spImpact));
					}
				}

				totalWeight[tableIndex]+=weight;

				SpatialVector transformedPoint;
				transformVector(conversion,point[tableIndex],transformedPoint);
				resultPoint[tableIndex]+=weight*transformedPoint;

				if(fadeToRoot)
				{
					//* TODO param
					SpatialTransform rootConversion;
					setIdentity(rootConversion);

					totalWeight[tableIndex]+=rootWeight;

					transformVector(rootConversion,
							point[tableIndex],transformedPoint);
					resultPoint[tableIndex]+=rootWeight*transformedPoint;
				}

				if(rotateNormals)
				{
					SpatialVector transformedNormal;
					rotateVector(conversion,normal[tableIndex],
							transformedNormal);
					resultNormal[tableIndex]+=weight*transformedNormal;
				}

#if FE_SWO_VERBOSE
				feLog("point %d/%d weight %.6G\n",
						tableIndex,tableCount,weight);
				feLog("face %d bary %s\n",
						face,c_print(bary));
				feLog("from %s to %s\n",
						c_print(point[tableIndex]),c_print(transformedPoint));
				feLog("conversion\n%s\n",c_print(conversion));
				feLog("translateDriver\n%s\n",c_print(translateDriver));
				feLog("translateDeformed\n%s\n",c_print(translateDeformed));
#endif
			}

			if(generateLocators)
			{
				//* TEMP presuming single-weighted

				//* transform fragment locator by weighted conversion

				sp<SurfaceAccessorI> spLocPositionIn;
				if(!access(spLocPositionIn,m_spInputAccessible,
						e_detail,locatorPreface+"P")) return;

				sp<SurfaceAccessorI> spLocNormalIn;
				if(!access(spLocNormalIn,m_spInputAccessible,
						e_detail,locatorPreface+"N")) return;

				sp<SurfaceAccessorI> spLocTangentIn;
				if(!access(spLocTangentIn,m_spInputAccessible,
						e_detail,locatorPreface+"T")) return;

				const SpatialVector locPoint=spLocPositionIn->spatialVector(0);
				const SpatialVector locNormal=spLocNormalIn->spatialVector(0);
				const SpatialVector locTangent=spLocTangentIn->spatialVector(0);

				sp<SurfaceAccessorI> spLocPosition;
				if(!access(spLocPosition,m_spOutputAccessible,
						e_detail,locatorPreface+"P")) return;

				sp<SurfaceAccessorI> spLocNormal;
				if(!access(spLocNormal,m_spOutputAccessible,
						e_detail,locatorPreface+"N")) return;

				sp<SurfaceAccessorI> spLocTangent;
				if(!access(spLocTangent,m_spOutputAccessible,
						e_detail,locatorPreface+"T")) return;

				SpatialVector transformedPoint;
				SpatialVector transformedNormal;
				SpatialVector transformedTangent;

				transformVector(conversion,locPoint,transformedPoint);
				rotateVector(conversion,locNormal,transformedNormal);
				rotateVector(conversion,locTangent,transformedTangent);

				spLocPosition->set(0,transformedPoint);
				spLocNormal->set(0,transformedNormal);
				spLocTangent->set(0,transformedTangent);
			}

			if(m_spDrawDebug.isValid() &&
					(debugIndex<0 || debugIndex==outputIndex))
			{
				SpatialVector transformedPoint;
				transformVector(conversion,point[0],transformedPoint);

				const Color yellow(1.0,1.0,0.0,1.0);
				SpatialVector line[2];

				line[0]=transformedPoint;
				line[1]=translateDeformed;

				SAFEGUARD;
				m_spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,
						false,&yellow);
			}

#if FE_SWO_PROFILE
			if(a_id<1)
			{
				m_spProfileLoop->replace(m_spProfileTable);
			}
#endif
		}

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileTransfer->replace(m_spProfileLoop);
		}
#endif

		const Real envelope=catalog<Real>("envelope");
		const Real envelope1=Real(1)-envelope;

		if(sameIndex)
		{
			if(totalWeight[0]>0.0)
			{
				spOutputElements->set(outputIndex,
						envelope*resultPoint[0]/totalWeight[0]+
						envelope1*point[0]);
				if(rotateNormals && spOutputNormals.isValid())
				{
					spOutputNormals->set(outputIndex,unitSafe(
							envelope*resultNormal[0]+envelope1*normal[0]));
				}
			}
		}
		else
		{
			tableIndex=0;
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 filtered=m_fragmented?
						spFilter->filter(filterIndex): inputIndex;
				FEASSERT(filtered>=0);
				const U32 subCount=
						singular? 1: spInputElements->subCount(filtered);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					if(totalWeight[tableIndex]>0.0)
					{
						spOutputElements->set(filtered,subIndex,
								envelope*resultPoint[tableIndex]/
								totalWeight[tableIndex]+
								envelope1*point[tableIndex]);
						if(rotateNormals && spOutputNormals.isValid())
						{
							const I32 pointIndex=m_bindPrimitives?
									spOutputElements->integer(
									filtered,subIndex): filtered;

							spOutputNormals->set(pointIndex,unitSafe(
									envelope*resultNormal[tableIndex]+
									envelope1*normal[tableIndex]));
						}
					}

					tableIndex++;
				}
			}
		}

		delete[] bindingDistance;
		delete[] driverSample;
		delete[] sampleBary;
		delete[] sampleFace;

		delete[] totalWeight;
		delete[] resultNormal;
		delete[] resultPoint;
		delete[] normal;
		delete[] point;

#if FE_SWO_PROFILE
		if(a_id<1)
		{
			m_spProfileIterate->replace(m_spProfileTransfer);
		}
#endif

		rangeIndex++;
	}

	delete[] preloadPoint;

#if FE_SWO_PROFILE
	if(a_id<1)
	{
		m_spProfileFinish->replace(m_spProfileIterate);
	}
#endif

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::run done running thread %d\n",a_id);
#endif
}

Real SurfaceWrapOp::calculateWeight(Real a_distance,Real a_peakDistance,
	Real a_distanceBias,Real a_distanceScale,Real a_minimum)
{
//	Real weight=1.0/(1.0+pow(a_distanceScale*a_distance,a_distanceBias));

	if(a_peakDistance<=0.0)
	{
		feLog("SurfaceWrapOp::calculateWeight peakDistance %.6G\n",
				a_peakDistance);
		return Real(0);
	}

	FEASSERT(a_peakDistance>0.0);
	Real weight=1.0-a_distanceScale*a_distance/a_peakDistance;
	if(a_distanceBias!=1.0)
	{
		weight=pow(fmax(0.0,weight),a_distanceBias);
	}

	if(weight<a_minimum)
	{
		weight=a_minimum;
	}

	return weight;
}
