/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#define FE_SING_DEBUG	FALSE

namespace fe
{
namespace ext
{

void Sing::handle(Record& record)
{
	if(!m_audibleRAV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_audibleRAV.bind(spScope);

		m_spListenerI=registry()->create("ListenerI");

		//* TODO elsewhere
		m_spAudioI=registry()->create("AudioI");
		if(m_spAudioI.isValid())
		{
			String path=registry()->master()->catalog()->catalog<String>(
					"path:media")+"/wav";
			m_spAudioI->load("tick",	(path+"/clocktickfast.wav").c_str());
			m_spAudioI->load("din",		(path+"/motor_a8.wav").c_str());
			m_spAudioI->load("explosion",(path+"/explosion.wav").c_str());
			m_spAudioI->load("cannon",	(path+"/muzzleloadshot.wav").c_str());
			m_spAudioI->load("attract",	(path+"/buzzer2.wav").c_str());
			m_spAudioI->load("thrust",	(path+"/insidejet.wav").c_str());
		}
	}

	if(!m_spAudioI.isValid())
	{
		return;
	}

	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	m_spAudioI->flush();

	if(m_spListenerI.isValid())
	{
/*
		feLog("loc %s\n",print(m_theaterRV.cameraLoc()).c_str());
		feLog("at %s\n",print(m_theaterRV.cameraAt()).c_str());
		feLog("up %s\n",print(m_theaterRV.cameraUp()).c_str());
*/
		m_spListenerI->setLocation(m_theaterRV.cameraLoc());
		m_spListenerI->setOrientation(m_theaterRV.cameraAt(),
				m_theaterRV.cameraUp());
	}

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_audibleRAV.bind(spRA);

		if(!m_audibleRAV.recordView().song.check(spRA))
		{
			continue;
		}

		for(Audible& audibleRV: m_audibleRAV)
		{
			String& song=audibleRV.song();
			String& nextSong=audibleRV.nextSong();

			sp<Component>& rspComponent=audibleRV.voice();

			//* TODO evaluate how often this happens
			if(song.empty() && nextSong.empty())
			{
				if(rspComponent.isValid())
				{
					rspComponent=NULL;
				}
				continue;
			}

			sp<VoiceI> spVoiceI=rspComponent;

			//* TODO clear song if queue finished
			if(spVoiceI.isValid() && spVoiceI->remaining()<1)
			{
				song="";
			}

			//* advance nextSong if song empty
			if(song.empty() && !nextSong.empty())
			{
				if(!rspComponent.isValid())
				{
					rspComponent=registry()->create("VoiceI");
					if(!rspComponent.isValid())
					{
						continue;
					}
					spVoiceI=rspComponent;
				}

				spVoiceI->stop();

				I32& nextPlays=audibleRV.nextPlays();

				U32 count=(nextPlays>1)? nextPlays: 1;
				for(U32 m=0;m<count;m++)
				{
#if FE_SING_DEBUG
					feLog("Sing::handle queue %s %d/%d %p %s\n",
							audibleRV.identifier().c_str(),
							m,nextPlays,spVoiceI.raw(),nextSong.c_str());
#endif
					spVoiceI->queue(nextSong.c_str());
				}

				spVoiceI->loop(nextPlays<1);

				spVoiceI->play();

				F32& nextPitch=audibleRV.nextPitch();
				F32& nextGain=audibleRV.nextGain();

				audibleRV.pitch()=nextPitch;
				audibleRV.gain()=nextGain;
				song=nextSong;

				nextPitch=1.0f;
				nextGain=1.0f;
				nextSong="";
				nextPlays=0;
			}

			//* update current song
			if(spVoiceI.isValid())
			{
				spVoiceI->setLocation(audibleRV.location());
				spVoiceI->setVelocity(audibleRV.velocity());
				spVoiceI->setPitch(audibleRV.pitch());
				spVoiceI->setVolume(audibleRV.gain());
//				feLog("loc %s  %s\n",print(audibleRV.location()).c_str(),
//						print(audibleRV.location()).c_str());
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
