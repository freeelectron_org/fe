/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#define FE_SAOC_CLONE_DEBUG		FALSE
#define FE_SAOC_BRIDGE_DEBUG	FALSE
#define FE_SAOC_UPDATE_TICKER	FALSE
#define FE_SAOC_GROUP_DEBUG		FALSE

namespace fe
{
namespace ext
{

//* static
void SurfaceAccessibleOpenCL::updateDrawBuffer(sp<DrawBufferI> a_spDrawBuffer,
	sp<Master> a_spMaster,sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	I32 a_vertexCount)
{
#if FE_SAOC_UPDATE_TICKER
	const U32 tick0=fe::systemTick();
#endif

	sp<Catalog> spCatalog(a_spDrawBuffer);
	if(spCatalog.isValid())
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				a_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isValid())
		{
			//* TODO other attributes
			for(I32 pass=0;pass<2;pass++)
			{
				const String bridgeName=pass? "pt:Cd": "pt:P";

				cp<SurfaceAccessibleOpenCL::Bridge>& rcpBridge=
						spSurfaceAccessibleOpenCL->lookupBridge(
						bridgeName,FALSE);
				if(rcpBridge.isValid())
				{
					sp<Catalog> spMasterCatalog=a_spMaster->catalog();
					const BWORD openclDoubleBuffer=
							spMasterCatalog->catalogOrDefault<bool>(
							"hint:openclDoubleBuffer",FALSE);

					sp<SurfaceAccessibleOpenCL::ClMem> spClMem;
					if(openclDoubleBuffer)
					{
						rcpBridge->safeLock();
						spClMem=rcpBridge->m_spClMemPrevious;
						rcpBridge->safeUnlock();
					}
					else
					{
						spClMem=rcpBridge->m_spClMem;
					}

					//* perhaps not really double-buffered
					if(spClMem.isNull())
					{
						spClMem=rcpBridge.writable()->getClMem();
					}

					//* NOTE SurfaceTrianglesOpenCL replication
					//* can cause a mismatch

					if(rcpBridge->m_elementCount!=I32(a_vertexCount))
					{
						feLog("SurfaceAccessibleOpenCL::updateDrawBuffer"
								" bridge \"%s\" has %d elements"
								" vs %d cached vertices\n",
								bridgeName.c_str(),
								rcpBridge->m_elementCount,a_vertexCount);

						spClMem=NULL;
					}

//					sp<SurfaceAccessibleOpenCL::ClMem> spClMemNext=
//							rcpBridge->getClMem();
//					feLog("SurfaceAccessibleOpenCL::updateDrawBuffer"
//							" %p \"%s\" mem %p %p\n",
//							a_spDrawBuffer.raw(),
//							bridgeName.c_str(),
//							spClMem.isValid()? spClMem->m_clMem: nullptr,
//							spClMemNext.isValid()? spClMemNext->m_clMem: nullptr);

					if(spClMem.isValid())
					{
						spCatalog->catalog< sp<Counted> >("cl:"+bridgeName)=
								spClMem;
					}
				}
			}
		}
	}

#if FE_SAOC_UPDATE_TICKER
	const U32 tick1=fe::systemTick();

	const U32 tickDiff01=fe::SystemTicker::tickDifference(tick0,tick1);

	const fe::Real msPerTick=1e-3*fe::SystemTicker::microsecondsPerTick();
	const fe::Real ms01=tickDiff01*msPerTick;

	feLog("SurfaceAccessibleOpenCL::updateDrawBuffer %.2f ms\n",ms01);
#endif
}

SurfaceAccessibleOpenCL::Bridge::~Bridge(void)
{
	if(m_type=="string")
	{
		//* make sure any long strings are released
		String* stringArray=(String*)(m_bufferData);
		if(stringArray)
		{
			for(I32 elementIndex=0;elementIndex<m_elementCount;elementIndex++)
			{
				stringArray[elementIndex]="";
			}
		}
	}

	deallocate(m_bufferData);
}

Protectable* SurfaceAccessibleOpenCL::Bridge::clone(Protectable* pInstance)
{
	SurfaceAccessibleOpenCL::Bridge* pBridge=pInstance?
			fe_cast<SurfaceAccessibleOpenCL::Bridge>(pInstance):
			new SurfaceAccessibleOpenCL::Bridge(m_clContext);

#if FE_SAOC_CLONE_DEBUG
	feLog("SurfaceAccessibleOpenCL::Bridge::clone as %p from %p\n",
			pBridge,pInstance);
#endif

	sp<Catalog> spMasterCatalog=m_spMaster->catalog();

	const BWORD openclDoubleBuffer=
			spMasterCatalog->catalogOrDefault<bool>(
			"hint:openclDoubleBuffer",FALSE);

	pBridge->m_spMaster=m_spMaster;
	pBridge->m_name=m_name;
	pBridge->m_type=m_type;
	pBridge->m_rate=m_rate;
	pBridge->m_elementCount=m_elementCount;
	pBridge->m_sizeof=m_sizeof;

	//* TODO shouldn't ever be zero
	const size_t entrySize=pBridge->m_sizeof? pBridge->m_sizeof: sizeof(I32);

	const cl_int bufferSize=pBridge->m_elementCount*entrySize;

#if FE_SAOC_CLONE_DEBUG
	feLog("SurfaceAccessibleOpenCL::Bridge::clone"
			" cpu %d gpu %d size %-8d \"%s\"\n",
			m_cpuCurrent,
			m_gpuCurrent,
			bufferSize,
			pBridge->m_name.c_str());
#endif

	pBridge->m_cpuCurrent=FALSE;
	pBridge->m_gpuCurrent=FALSE;

	//* TODO check m_cpuCurrent and m_gpuCurrent

#if FALSE
	//* NOTE why flush to GPU if it wasn't already current?

	if(m_spClMem.isNull())
	{
		getClMem();
	}
#endif

	if(m_gpuCurrent && m_spClMem.isValid())
	{
		cl_mem clSourceMem=m_spClMem->m_clMem;
		if(clSourceMem)
		{
			sp<ClMem> spNewClMem(new ClMem());

			if(spNewClMem.isValid())
			{

				cl_int clError=0;
				spNewClMem->m_clMem=clCreateBuffer(m_clContext,
						CL_MEM_READ_WRITE,bufferSize,nullptr,&clError);

#if FE_SAOC_CLONE_DEBUG
				feLog("SurfaceAccessibleOpenCL::Bridge::clone"
						" clCreateBuffer %p clError %d\n",
						spNewClMem->m_clMem,clError);
#endif

				if(clError || spNewClMem->m_clMem==nullptr)
				{
					spNewClMem=NULL;
				}
				else
				{
					cl_command_queue clCommandQueue=(cl_command_queue)
							spMasterCatalog->catalogOrDefault<void*>(
							"clCommandQueue",NULL);

					clError=clEnqueueCopyBuffer(
							clCommandQueue,clSourceMem,
							spNewClMem->m_clMem,
							0,0,bufferSize,
							0,NULL,NULL);
#if FE_SAOC_CLONE_DEBUG
					feLog("  clEnqueueCopyBuffer %p to %p"
							" queue %p clError %d\n",
							clSourceMem,spNewClMem->m_clMem,
							clCommandQueue,clError);
#endif

					clError=clFinish(clCommandQueue);
#if FE_SAOC_CLONE_DEBUG
					if(clError)
					{
						feLog("  clFinish clError %d\n",clError);
					}
#endif
				}

				pBridge->m_gpuCurrent=TRUE;
			}

			if(openclDoubleBuffer)
			{
				pBridge->safeLock();
				pBridge->m_spClMemPrevious=pBridge->m_spClMem;
				pBridge->m_spClMem=spNewClMem;
				pBridge->safeUnlock();
			}
			else
			{
				pBridge->m_spClMemPrevious=nullptr;
				pBridge->m_spClMem=spNewClMem;
			}
		}
	}
	else if(!pBridge->m_gpuCurrent &&
			m_cpuCurrent &&
			m_bufferData)
	{
		pBridge->m_groupSet=m_groupSet;
		pBridge->m_allocated=m_allocated;
		const size_t allocSize=pBridge->m_allocated*entrySize;

#if FE_SAOC_CLONE_DEBUG
		feLog("    copy bufferData %d/%d DB %d\n",
				bufferSize,allocSize,openclDoubleBuffer);
#endif
		pBridge->m_bufferData=reallocate(nullptr,allocSize);

		memcpy(pBridge->m_bufferData,m_bufferData,bufferSize);
		pBridge->m_cpuCurrent=TRUE;

		if(openclDoubleBuffer)
		{
			pBridge->safeLock();
			pBridge->m_spClMemPrevious=pBridge->m_spClMem;
			pBridge->m_spClMem=nullptr;
			pBridge->safeUnlock();
		}
		else
		{
			pBridge->m_spClMemPrevious=nullptr;
			pBridge->m_spClMem=nullptr;
		}
	}

	return pBridge;
}

sp<SurfaceAccessibleOpenCL::ClMem>
		SurfaceAccessibleOpenCL::Bridge::getClMem(void)
{
	safeLock();

#if FALSE
	feLog("SurfaceAccessibleOpenCL::Bridge::getClMem %d %p context %p\n",
			m_spClMem.isValid(),
			m_spClMem.isValid()? m_spClMem->m_clMem: nullptr,
			m_clContext);
#endif

	FEASSERT(m_spClMem.isNull() || m_spClMem->m_clMem!=nullptr);

	if(!m_gpuCurrent && m_spClMem.isValid())
	{
#if FALSE
		cl_command_queue clCommandQueue=(cl_command_queue)
				m_spMaster->catalog()->catalogOrDefault<void*>(
				"clCommandQueue",NULL);
		if(clCommandQueue)
		{
			cl_int clError=clEnqueueWriteBuffer(clCommandQueue,
					m_spClMem->m_clMem,CL_FALSE,0,m_elementCount*m_sizeof,
					m_bufferData,0,NULL,NULL);
			if(clError)
			{
				feLog("  clEnqueueWriteBuffer clError %d\n",
						clError);
			}
		}
		else
		{
			feLog("SurfaceAccessibleOpenCL::Bridge::getClMem"
					" no command queue\n");
			m_spClMem=NULL;
		}
#else
		//* just use clCreateBuffer below
		m_spClMem=NULL;
#endif
	}

	sp<ClMem> spClMem=m_spClMem;

	if(spClMem.isNull() && m_clContext)
	{
		spClMem=new ClMem();

		if(spClMem.isValid())
		{
			cl_int bufferSize=m_elementCount*m_sizeof;

			cl_int clError=0;
			spClMem->m_clMem=clCreateBuffer(m_clContext,
					CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,
					bufferSize,m_bufferData,&clError);

#if FALSE
			feLog("SurfaceAccessibleOpenCL::Bridge::getClMem"
					" clCreateBuffer %p clError %d\n",
					spClMem->m_clMem,clError);
			feLog("  \"%s\" \"%s\" \"%s\" count %d sizeof %d\n",
					m_name.c_str(),m_type.c_str(),m_rate.c_str(),
					m_elementCount,m_sizeof);
#endif

			if(clError || spClMem->m_clMem==nullptr)
			{
				spClMem=NULL;
			}
		}

		m_spClMem=spClMem;
		m_gpuCurrent=m_spClMem.isValid();
	}

	safeUnlock();

	return spClMem;
}

void SurfaceAccessibleOpenCL::Bridge::refresh(void)
{
#if FALSE
	if(!m_cpuCurrent || !m_gpuCurrent)
	{
		feLog("SurfaceAccessibleOpenCL::Bridge::refresh %p"
				" \"%s\" cpu %d gpu %d\n",
				this,m_name.c_str(),m_cpuCurrent,m_gpuCurrent);
	}
#endif

	if(m_cpuCurrent)
	{
		return;
	}

	safeLock();

	if(!m_gpuCurrent)
	{
		feLog("SurfaceAccessibleOpenCL::Bridge::refresh"
				" neither side is current for \"%s\"\n",
				m_name.c_str());
		safeUnlock();
		return;
	}

	cl_command_queue clCommandQueue=(cl_command_queue)
			m_spMaster->catalog()->catalogOrDefault<void*>(
			"clCommandQueue",NULL);

	if(clCommandQueue)
	{
		cl_mem clMem=(m_spClMem.isValid())? m_spClMem->m_clMem: NULL;

		if(clMem)
		{
#if FE_SAOC_BRIDGE_DEBUG
			feLog("SurfaceAccessibleOpenCL::Bridge::refresh"
					" read back \"%s\"\n",m_name.c_str());
#endif

			cl_int clError;
			const size_t bufferSize=m_elementCount*m_sizeof;

#if FE_CODEGEN<=FE_DEBUG
			//* buffer size check
			size_t bytes=0;
			size_t returned=0;
			clError=clGetMemObjectInfo(clMem,CL_MEM_SIZE,
					sizeof(bytes),&bytes,&returned);
			if(clError)
			{
				feLog("  clGetMemObjectInfo clError %d\n",clError);
			}
			else if(bytes!=bufferSize)
			{
				feLog("SurfaceAccessibleOpenCL::Bridge::refresh"
						" cl_mem size %d expecting %d\n",
						bytes,bufferSize);
			}
#endif

			//* TODO m_groupSet

			m_bufferData=reallocate(m_bufferData,bufferSize);

			clError=clEnqueueReadBuffer(clCommandQueue,
					clMem,CL_TRUE,0,bufferSize,
					m_bufferData,0,NULL,NULL);
			if(clError)
			{
				feLog("  clEnqueueReadBuffer clError %d\n",
						clError);

//				feLogBacktrace();
//				feAttachDebugger();
			}
		}
	}

	m_cpuCurrent=TRUE;

	safeUnlock();
}

SurfaceAccessibleOpenCL::SurfaceAccessibleOpenCL(void):
	m_clContext(nullptr)
{
}

SurfaceAccessibleOpenCL::~SurfaceAccessibleOpenCL(void)
{
}

void SurfaceAccessibleOpenCL::reset(void)
{
	SurfaceAccessibleBase::reset();

	m_bridgeMap.clear();
}

I32 SurfaceAccessibleOpenCL::count(String a_nodeName,
	SurfaceAccessibleI::Element a_element) const
{
//	feLog("SurfaceAccessibleOpenCL::count element %d\n",a_element);

	String bridgeName;

	switch(a_element)
	{
		case e_point:
			bridgeName="pt:P";
			break;
		case e_primitive:
			bridgeName=":vertCount";
			break;
		case e_detail:
			return 1;
		default:
			;
	}

	if(a_element==e_vertex)
	{
		//* find a Bridge name that starts with "vtx:"
		const String prefix=
				SurfaceAccessibleBase::elementLayout(a_element)+":";
		const U32 bytes=prefix.length();

		char* start=new char[bytes+1];

		for(std::pair<const String, cp<Bridge>> keyValue: m_bridgeMap)
		{
			strncpy(start,keyValue.first.c_str(),bytes);
			start[bytes]=0;

			if(start==prefix)
			{
				bridgeName=keyValue.first;
				break;
			}
		}

		delete[] start;
	}

	if(bridgeName.empty())
	{
		return 0;
	}

	if(m_bridgeMap.find(bridgeName)==m_bridgeMap.end())
	{
//		feLog("SurfaceAccessibleOpenCL::count no bridge for \"%s\"\n",
//				bridgeName.c_str());
		return 0;
	}

	const cp<Bridge>& rcpBridge=m_bridgeMap.at(bridgeName);
	if(rcpBridge.isNull())
	{
		feLog("SurfaceAccessibleOpenCL::count null bridge for \"%s\"\n",
				bridgeName.c_str());
		return 0;
	}

	if(a_element==e_vertex)
	{
		return rcpBridge->m_elementCount*FE_OPENCL_BUFFER_MAX_VERTS;
	}

	return rcpBridge->m_elementCount;
}

Protectable* SurfaceAccessibleOpenCL::clone(Protectable* pInstance)
{
	SurfaceAccessibleOpenCL* pSurfaceAccessibleOpenCL= pInstance?
			fe_cast<SurfaceAccessibleOpenCL>(pInstance):
			new SurfaceAccessibleOpenCL();

#if FE_SAOC_CLONE_DEBUG
	feLog("SurfaceAccessibleOpenCL::clone as %p from %p\n",
			pSurfaceAccessibleOpenCL,pInstance);
#endif

	updateContext();

	if(pSurfaceAccessibleOpenCL->library().isNull())
	{
		pSurfaceAccessibleOpenCL->setLibrary(library());
		pSurfaceAccessibleOpenCL->setFactoryIndex(factoryIndex());
	}
	pSurfaceAccessibleOpenCL->m_clContext=m_clContext;

	for(std::pair<const String, cp<Bridge>> keyValue: m_bridgeMap)
	{
		const String& bridgeName=keyValue.first;

		cp<Bridge>& rcpSourceBridge=keyValue.second;

#if FE_SAOC_CLONE_DEBUG
		feLog("  Bridge \"%s\"\n",bridgeName.c_str());
#endif

		if(rcpSourceBridge.isNull())
		{
			continue;
		}

		cp<Bridge>& rcpDestBridge=
				pSurfaceAccessibleOpenCL->lookupBridge(bridgeName,TRUE);

#if TRUE
		//* Copy-On-Write Bridge sharing
		rcpDestBridge=rcpSourceBridge;
#else
		//* Copy all Bridges in advance
		rcpDestBridge=fe_cast<SurfaceAccessibleOpenCL::Bridge>(
				rcpSourceBridge.writable()->clone());
#endif
		rcpDestBridge.protect();
	}

#if FE_SAOC_CLONE_DEBUG
	feLog("SurfaceAccessibleOpenCL::clone result %p\n",
			pSurfaceAccessibleOpenCL);
#endif

	return pSurfaceAccessibleOpenCL;
}

void SurfaceAccessibleOpenCL::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
//	feLog("SurfaceAccessibleOpenCL::attributeSpecs element %d \"%s\"\n",
//			a_element,SurfaceAccessibleBase::elementLayout(a_element).c_str());

	a_rSpecs.clear();

	//* NOTE a_node ignored

	SurfaceAccessibleI::Spec spec;

	for(std::pair<const String, cp<Bridge>> keyValue: m_bridgeMap)
	{
		const String& bridgeName=keyValue.first;
		const cp<Bridge>& rcpBridge=keyValue.second;
		const String attrRate=rcpBridge->m_rate;
		const String attrType=rcpBridge->m_type;

//		feLog("  spec Bridge \"%s\" type \"%s\" rate \"%s\"\n",
//				bridgeName.c_str(),attrType.c_str(),attrRate.c_str());

		if(attrType.empty() ||
				attrRate!=SurfaceAccessibleBase::elementLayout(a_element))
		{
//			feLog("    wrong rate\n");
			continue;
		}

		String text=bridgeName;
		text.parse("",":");

		const String attrName=text.prechop(":");
		if(attrName.empty())
		{
//			feLog("    no name\n");
			continue;
		}

//		feLog("    add \"%s\" \"%s\"\n",
//				attrName.c_str(),attrType.c_str());

		spec.set(attrName,attrType);
		a_rSpecs.push_back(spec);
	}
}

sp<SurfaceAccessorI> SurfaceAccessibleOpenCL::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	String a_name,SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorOpenCL> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleOpenCL::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute,
	SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorOpenCL> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleOpenCL::createAccessor(void)
{
	sp<SurfaceAccessorOpenCL> spAccessorOpenCL(new SurfaceAccessorOpenCL());

	return spAccessorOpenCL;
}

BWORD SurfaceAccessibleOpenCL::discard(
	SurfaceAccessibleI::Element a_element,String a_name)
{
	const String rate=SurfaceAccessibleBase::elementLayout(a_element);
	const String key=rate+":"+a_name;

	if(m_bridgeMap.find(key)==m_bridgeMap.end())
	{
		return FALSE;
	}

	m_bridgeMap.erase(key);
	return TRUE;
}

void SurfaceAccessibleOpenCL::updateContext(void)
{
	sp<Master> spMaster=registry()->master();
	sp<Catalog> spMasterCatalog=spMaster->catalog();
	if(m_clContext!=
			spMasterCatalog->catalogOrDefault<void*>("clContext",nullptr))
	{
		m_clContext=nullptr;
	}

	if(!m_clContext)
	{
		m_clContext=(cl_context)spMasterCatalog->catalogOrDefault<void*>(
				"clContext",nullptr);

//		feLog("SurfaceAccessibleOpenCL::updateContext new %p\n",m_clContext);

		if(m_clContext)
		{
			for(std::pair<const String, cp<Bridge>> keyValue: m_bridgeMap)
			{
				sp<Bridge> spBridge(keyValue.second.writable());
				if(spBridge.isValid())
				{
					//* read back, if necessary
					spBridge->refresh();

					spBridge->m_clContext=m_clContext;
					spBridge->m_gpuCurrent=FALSE;
					spBridge->m_spClMem=NULL;
					spBridge->m_spClMemPrevious=NULL;
				}
			}
		}
	}
}

cp<SurfaceAccessibleOpenCL::Bridge>& SurfaceAccessibleOpenCL::lookupBridge(
	String a_name,BWORD a_create)
{
	if(a_name.empty())
	{
		feX("SurfaceAccessibleOpenCL::lookupBridge","no name");
	}

	updateContext();

	if(m_bridgeMap.find(a_name)!=m_bridgeMap.end())
	{
		return m_bridgeMap[a_name];
	}

	if(a_create)
	{
		sp<Bridge> spBridge(new Bridge(m_clContext));
		spBridge->m_name=a_name;
		spBridge->m_spMaster=registry()->master();

		cp<Bridge>& rcpBridge=m_bridgeMap[a_name];
		rcpBridge=spBridge;
		rcpBridge.protect();

		return rcpBridge;
	}

	m_bridgeMap.erase(a_name);

	return m_bridgeNull;
}

sp<SurfaceI> SurfaceAccessibleOpenCL::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
	//* TODO select form
	//* TODO group
	sp<SurfaceI> spSurface;

	FEASSERT(registry().isValid());

	if(registry().isValid())
	{
		sp<SurfaceAccessorI> spSurfacePoints=
				accessor(e_point,e_position,e_refuseMissing);
		const I32 pointCount=spSurfacePoints.isValid()?
				spSurfacePoints->count(): 0;

		sp<SurfaceAccessorI> spSurfaceVertices=
				accessor(e_primitive,e_vertices,e_refuseMissing);
		const I32 primitiveCount=spSurfaceVertices.isValid()?
				spSurfaceVertices->count(): 0;

		const BWORD pointCloud=(pointCount && !primitiveCount);

		sp<SurfaceAccessorI> spSurfaceProperties=
				accessor(e_primitive,e_properties,e_refuseMissing);

		const BWORD openCurve=(spSurfaceProperties.isValid() &&
				spSurfaceProperties->count() &&
				spSurfaceProperties->integer(0,
				SurfaceAccessibleI::e_openCurve));

//		feLog("SurfaceAccessibleOpenCL::surface"
//				" \"%s\" pointCount %d primitiveCount %d"
//				" pointCloud %d openCurve %d\n",
//				a_group.c_str(),
//				pointCount,primitiveCount,pointCloud,openCurve);

		//* TODO be better about being points, not curves,
		//* if both e_excludeCurves and e_excludePolygons are set

		if(pointCloud ||
				(openCurve && !(a_restrictions&SurfaceI::e_excludeCurves)) ||
				(a_restrictions&SurfaceI::e_excludePolygons))
		{
//			feLog("SurfaceAccessibleOpenCL::surface"
//					" create SurfaceCurvesOpenCL\n");

			spSurface=registry()->create("*.SurfaceCurvesOpenCL");

			sp<SurfaceCurvesOpenCL> spSurfaceCurvesOpenCL=spSurface;
			if(spSurfaceCurvesOpenCL.isValid())
			{
				spSurfaceCurvesOpenCL->bind(
						sp<SurfaceAccessibleI>(this));
			}
		}
		else
		{
//			feLog("SurfaceAccessibleOpenCL::surface"
//					" create SurfaceTrianglesOpenCL\n");

#if FALSE
			spSurface=registry()->create(
					"*.SurfaceTrianglesAccessible");
			sp<SurfaceTrianglesAccessible> spSurfaceTrianglesAccessible=
					spSurface;
			if(spSurfaceTrianglesAccessible.isValid())
			{
				spSurfaceTrianglesAccessible->bind(
						sp<SurfaceAccessibleI>(this));
			}
#else
			spSurface=registry()->create(
					"*.SurfaceTrianglesOpenCL");
#endif

			sp<SurfaceTrianglesOpenCL> spSurfaceTrianglesOpenCL=spSurface;
			if(spSurfaceTrianglesOpenCL.isValid())
			{
				spSurfaceTrianglesOpenCL->bind(
						sp<SurfaceAccessibleI>(this));

//~				spSurfaceTrianglesOpenCL->setSearchable(FALSE);
			}
		}
	}
	else
	{
		feLog("SurfaceAccessibleOpenCL::surface registry is NULL\n");
	}

	if(spSurface.isValid())
	{
		spSurface->setRestrictions(a_restrictions);
	}
	else
	{
		feLog("SurfaceAccessibleOpenCL::surface failed to access surface\n");
	}

	return spSurface;
}

sp<SurfaceAccessibleBase::MultiGroup> SurfaceAccessibleOpenCL::generateGroup(
	SurfaceAccessibleI::Element a_element,
	String a_groupString)
{
	const String requestedRate=SurfaceAccessibleBase::elementLayout(a_element);

#if FE_SAOC_GROUP_DEBUG
	feLog("SurfaceAccessibleOpenCL::generateGroup '%s' \"%s\"\n",
			requestedRate.c_str(),a_groupString.c_str());
#endif

	sp<MultiGroup> spMultiGroup=
			sp<MultiGroup>(new MultiGroup());

	const String depletedString=
			addGroupRanges(spMultiGroup,a_groupString);

#if FE_SAOC_GROUP_DEBUG
	feLog("  depletedString \"%s\"\n",
			depletedString.c_str());
#endif

	Array<String> patternArray;
	String buffer=depletedString;
	while(!buffer.empty())
	{
		patternArray.push_back(buffer.parse());
	}
	const U32 patternCount=patternArray.size();

	for(std::pair<const String, cp<Bridge>> keyValue: m_bridgeMap)
	{
		const cp<Bridge>& rcpBridge=keyValue.second;

#if FE_SAOC_GROUP_DEBUG
		if(rcpBridge->m_rate!="detail")
		{
			feLog("  key \"%s\" rate '%s'\n",
					keyValue.first.c_str(),rcpBridge->m_rate.c_str());
		}
#endif

		if(rcpBridge->m_rate!=requestedRate)
		{
			continue;
		}

		String text=rcpBridge->m_name;
		text.parse("",":");

		const String groupName=text.prechop(":");
		if(groupName.empty())
		{
			continue;
		}

#if FE_SAOC_GROUP_DEBUG
		feLog("    groupName \"%s\"\n",groupName.c_str());
#endif

		for(U32 patternIndex=0;patternIndex<patternCount;patternIndex++)
		{
			const String pattern=patternArray[patternIndex];

#if FE_SAOC_GROUP_DEBUG
			feLog("    pattern %d/%d \"%s\"\n",
					patternIndex,patternCount,pattern.c_str());
#endif

			if(!groupName.match(pattern))
			{
				continue;
			}

#if FE_SAOC_GROUP_DEBUG
			feLog("    MATCH\n");
#endif

			const String bridgeName=requestedRate+":"+groupName;
			cp<SurfaceAccessibleOpenCL::Bridge>& rcpBridge=
					lookupBridge(bridgeName,FALSE);
			if(rcpBridge.isValid())
			{
				const std::set<I32>& rGroupSet=rcpBridge->m_groupSet;
				for(I32 entry: rGroupSet)
				{
					spMultiGroup->insert(entry);
				}
			}
		}
	}

	return spMultiGroup;
}

} /* namespace ext */
} /* namespace fe */
