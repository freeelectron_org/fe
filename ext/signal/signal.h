/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_h__
#define __signal_h__

#include "fe/data.h"

#include "signal/HandlerI.h"
#include "signal/ParserI.h"
#include "signal/Signature.h"
#include "signal/DispatchI.h"
#include "signal/Dispatch.h"
#include "signal/ReactorI.h"
#include "signal/Reactor.h"
#include "signal/SignalerI.h"
#include "signal/SequencerI.h"

namespace fe
{
namespace ext
{

Library *CreateSignalLibrary(sp<Master> spMaster);

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_h__ */
