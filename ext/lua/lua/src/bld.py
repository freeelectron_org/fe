import sys
import os
import re
forge = sys.modules["forge"]

def setup(module):
    module.summary = []

    major = ""
    minor = ""
    release = ""
    for line in open(module.modPath + "/lua.h").readlines():
        if re.match("#define LUA_VERSION_MAJOR\t.*", line):
            major = line.split()[2].lstrip('"').rstrip('"')
        if re.match("#define LUA_VERSION_MINOR\t.*", line):
            minor = line.split()[2].lstrip('"').rstrip('"')
        if re.match("#define LUA_VERSION_RELEASE\t.*", line):
            release = line.split()[2].lstrip('"').rstrip('"')
    version = ""
    if major != "":
        version = major
        if minor != "":
            version = version + "." + minor
            if release != "":
                version = version + "." + release

    if version != "":
        module.summary += [ version ]

    srcList = [ "lapi.c",
                "lcode.c",
                "lcorolib.c",
                "lctype.c",
                "ldebug.c",
                "ldo.c",
                "ldump.c",
                "lfunc.c",
                "lgc.c",
                "llex.c",
                "lmem.c",
                "linit.c",
                "lobject.c",
                "lopcodes.c",
                "loslib.c",
                "lparser.c",
                "lstate.c",
                "lstring.c",
                "ltable.c",
                "ltm.c",
                "lundump.c",
                "lutf8lib.c",
                "lvm.c",
                "lzio.c" ]

    dll = module.Lib( "lua", srcList )

    module.includemap = { 'lua' : os.path.join(module.modPath,'.') }
    #module.includemap = { 'lualib' : os.path.join(module.modPath,'..','include') }

    srcList = [ "lauxlib.c",
                "lbaselib.c",
                "ldblib.c",
                "liolib.c",
                "lmathlib.c",
                "ltablib.c",
                "lstrlib.c",
                "loadlib.c" ]

    deplibs = forge.corelibs + [ "luaLib" ]

    dll = module.Lib( "lualb", srcList )


    #module.Module('lib')

