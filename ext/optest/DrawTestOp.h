/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_OperatorDrawTest_h__
#define __optest_OperatorDrawTest_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw a Sample of Points and Lines

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT DrawTestOp:
	public OperatorSurfaceCommon,
	public Initialize<DrawTestOp>
{
	public:
				DrawTestOp(void)											{}
virtual			~DrawTestOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_OperatorDrawTest_h__ */
