/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operate/operate.pmh>

#define FE_MPC_DEBUG			FALSE
#define FE_MPC_GRIP_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void ManipulatorCommon::initialize(void)
{
	m_spTubeAura=new DrawMode();
	m_spTubeAura->setDrawStyle(DrawMode::e_solid);
	m_spTubeAura->setAntialias(TRUE);
	m_spTubeAura->setLit(FALSE);
	m_spTubeAura->setFrontfaceCulling(TRUE);
	m_spTubeAura->setBackfaceCulling(FALSE);
}

void ManipulatorCommon::bindOverlay(sp<DrawI>& a_spDrawOverlay)
{
	m_spOverlayCache=a_spDrawOverlay;
}

void ManipulatorCommon::handle(Record& a_rSignal)
{
#if FE_MPC_DEBUG
	feLog("ManipulatorCommon::handle\n");
#endif

	//* TODO fix step-behind draw access
	if(!m_spDrawCache.isValid())
	{
		return;
	}

	sp<ViewI> spView=m_spDrawCache->view();

	m_event.bind(windowEvent(a_rSignal));

	const Vector2i mouse(m_event.mouseX(),m_event.mouseY());

	if(!m_dragging)
	{
		if(m_hotGrip>=0 &&
				m_event.isMousePress(WindowEvent::e_itemLeft))
		{
			m_dragging=TRUE;
			m_lastGrip=m_hotGrip;
		}
		else if(m_lastGrip>=0 &&
				m_event.isMousePress(WindowEvent::e_itemMiddle))
		{
			m_dragging=TRUE;
			m_hotGrip=m_lastGrip;
		}
	}

	if(m_dragging)
	{
		FEASSERT(m_hotGrip>=0);
		if(m_event.isMouseDrag())
		{
			U32 item=m_event.item();
			BWORD shifted=FALSE;
			if(item&WindowEvent::e_keyShift)
			{
				shifted=TRUE;
				item^=WindowEvent::e_keyShift;
			}

			const Real gain=shifted? 0.1: 1.0;

			Grip& rGrip=m_gripArray[m_hotGrip];

			SpatialVector facing=rGrip.m_planeFacing;
			if(magnitudeSquared(facing)<0.5)
			{
//				feLog("\n grip %d change facing %s\n",
//						m_hotGrip,c_print(facing));

				Box2i viewport=spView->viewport();
				Vector2i center(viewport.size()[0]/2,viewport.size()[1]/2);

				facing=unitSafe(
						spView->unproject(center[0],center[1],1.0,
						ViewI::e_perspective)-
						spView->unproject(center[0],center[1],0.0,
						ViewI::e_perspective));
			}

			const SpatialVector lastOrigin=spView->unproject(
					m_lastMouse[0],m_lastMouse[1],0.0,
					ViewI::e_perspective);
			const SpatialVector lastRay=unitSafe(spView->unproject(
					m_lastMouse[0],m_lastMouse[1],1.0,
					ViewI::e_perspective)-lastOrigin);

			const SpatialVector currentOrigin=spView->unproject(
					mouse[0],mouse[1],0.0,ViewI::e_perspective);
			const SpatialVector currentRay=unitSafe(spView->unproject(
					mouse[0],mouse[1],1.0,ViewI::e_perspective)-
					currentOrigin);

			const Real lastDist=RayPlaneIntersect<Real>::solve(
					rGrip.m_planeCenter,facing,0.0,
					lastOrigin,lastRay);
			const Real currentDist=RayPlaneIntersect<Real>::solve(
					rGrip.m_planeCenter,facing,0.0,
					currentOrigin,currentRay);

			const SpatialVector lastHit=lastOrigin+lastDist*lastRay;
			const SpatialVector currentHit=currentOrigin+currentDist*currentRay;

			const SpatialVector movement=currentHit-lastHit;

#if FE_MPC_GRIP_DEBUG
			feLog("\n mouse %d %d\n",mouse[0],mouse[1]);
			feLog(" key \"%s\" \"%s\"\n",
					rGrip.m_key[0].c_str(),rGrip.m_key[1].c_str());
			feLog(" plane %s facing %s\n",
					c_print(rGrip.m_planeCenter),c_print(facing));
			feLog(" last %s  %s dist %.6G\n",
					c_print(lastOrigin),c_print(lastRay),lastDist);
			feLog(" current %s  %s dist %.6G\n",
					c_print(currentOrigin),c_print(currentRay),currentDist);
			feLog(" movement %s\n",c_print(movement));
			feLog(" lastHit %s\n",c_print(lastHit));
			feLog(" currentHit %s\n",c_print(currentHit));
#endif

			for(U32 pass=0;pass<2;pass++)
			{
				const Real scale=magnitude(rGrip.m_alignment[pass]);
				if(scale>0.0)
				{
					const Real along=dot(movement,rGrip.m_alignment[pass]);

#if FE_MPC_GRIP_DEBUG
					feLog("  pass %d along %.6G\n",pass,along);
					feLog("  align %s unitdot %.6G\n",
							c_print(rGrip.m_alignment[pass]),
							dot(unitSafe(movement),rGrip.m_alignment[pass]));
#endif

					const String& key=rGrip.m_key[pass];
					if(!key.empty())
					{
						Real& rValue=catalog<Real>(key);

						//* NOTE non-unit alignment is 'scale by grip length'
						//* divide once to normalize and once for range
						rValue+=along*gain/(scale*scale);

						if(cataloged(key,"min"))
						{
							const Real min=catalog<Real>(key,"min");
							if(rValue<min)
							{
								rValue=min;
							}
						}
						if(cataloged(key,"max"))
						{
							const Real max=catalog<Real>(key,"max");
							if(rValue>max)
							{
								rValue=max;
							}
						}
					}
				}
			}
		}
		else
		{
			const WindowEvent::MouseButtons buttons=m_event.mouseButtons();
			if(!buttons)
			{
				m_dragging=FALSE;
			}
		}
	}
	else
	{
		I32 bestIndex= -1;
		Real bestDist=0.0;

		const U32 gripCount=m_gripArray.size();
		for(U32 gripIndex=0;gripIndex<gripCount;gripIndex++)
		{
			Grip& rGrip=m_gripArray[gripIndex];

//			feLog("grip %d/%d %s radius %.6G align %s %s center %s facing %s\n",
//					gripIndex,gripCount,
//					c_print(rGrip.m_hotSpot),rGrip.m_radius,
//					c_print(rGrip.m_alignment[0]),
//					c_print(rGrip.m_alignment[1]),
//					c_print(rGrip.m_planeCenter),
//					c_print(rGrip.m_planeFacing));

			const Real gripDist=
					magnitudeSquared(SpatialVector(mouse)-rGrip.m_hotSpot);
			if(!gripIndex || gripDist<bestDist)
			{
				bestIndex=gripIndex;
				bestDist=gripDist;
			}
		}

//		feLog("best %d %.6G\n",bestIndex,bestDist);
		m_hotGrip=bestIndex;
	}

//	feLog("ManipulatorCommon::handle hot %d\n",m_hotGrip);

	m_lastMouse=mouse;
}

void ManipulatorCommon::updateGrips(void)
{
	sp<ViewI> spView=m_spDrawCache->view();
	Box2i viewport=spView->viewport();
	Vector2i center(viewport.size()[0]/2,viewport.size()[1]/2);

	m_facing=unitSafe(
			spView->unproject(center[0],center[1],0.0,
			ViewI::e_perspective)-
			spView->unproject(center[0],center[1],1.0,
			ViewI::e_perspective));

	m_upwards=unitSafe(
			spView->unproject(center[0],center[1]+1,1.0,
			ViewI::e_perspective)-
			spView->unproject(center[0],center[1],1.0,
			ViewI::e_perspective));

	m_rightways=unitSafe(cross(m_facing,-m_upwards));

}

void ManipulatorCommon::draw(sp<DrawI> a_spDrawI,const Color *color)
{
#if FE_MPC_DEBUG
	feLog("ManipulatorCommon::draw\n");
#endif

	m_spDrawCache=a_spDrawI;
	updateGrips();

	const U32 gripCount=m_gripArray.size();
	if(m_hotGrip>=I32(gripCount))
	{
		m_hotGrip= -1;
	}

	if(m_hotGrip>=0)
	{
		const BWORD centered=TRUE;

		sp<ViewI> spView=m_spOverlayCache->view();
		const Box2i viewport=spView->viewport();

		const Color black(0.0,0.0,0.0,1.0);
		const Color blue(0.5,0.5,1.0,1.0);
		SpatialVector textPoint(0.5*viewport.size()[0],
				viewport.size()[1]-6*m_spOverlayCache->font()->fontHeight(
				NULL,NULL),
				1.0);
		drawLabel(m_spOverlayCache,textPoint,
				m_gripArray[m_hotGrip].m_message,
				centered,3,blue,NULL,&black);

		if(gripLabels())
		{
			textPoint=m_gripArray[m_hotGrip].m_hotSpot;
			drawLabel(m_spOverlayCache,textPoint,
					m_gripArray[m_hotGrip].m_label,
					centered,3,blue,NULL,&black);
		}
	}
}

void ManipulatorCommon::drawTubeWithAura(sp<DrawI>& a_rspDraw,
	const SpatialVector* a_point,
	const SpatialVector* a_normal,
	const Real* a_radius,
	U32 a_pointCount,CurveMode a_curveMode,
	BWORD a_multicolor,const Color *a_color,
	const SpatialVector& a_rTowardCamera,
	const Real a_auraScale)
{
	const Real tubeAura=aura();

	drawTube(a_rspDraw,a_point,a_normal,a_radius,a_pointCount,a_curveMode,
			a_multicolor,a_color,a_rTowardCamera);
	if(tubeAura>0.0 && a_auraScale>0.0)
	{
		const Color yellow(1.0,1.0,0.0,tubeAura);

		Real* auraRadiusArray=new Real[a_pointCount];

		for(U32 m=0;m<a_pointCount;m++)
		{
			auraRadiusArray[m]=a_radius[m]*a_auraScale;
		}

		a_rspDraw->pushDrawMode(m_spTubeAura);
		drawTube(a_rspDraw,a_point,a_normal,auraRadiusArray,
				a_pointCount,a_curveMode,FALSE,&yellow,a_rTowardCamera);
		a_rspDraw->popDrawMode();

		delete[] auraRadiusArray;
	}
}
