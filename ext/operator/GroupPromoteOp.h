/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_GroupPromoteOp_h__
#define __operator_GroupPromoteOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Project group elements to another rate

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT GroupPromoteOp:
	public OperatorSurfaceCommon,
	public Initialize<GroupPromoteOp>
{
	public:
				GroupPromoteOp(void)										{}
virtual			~GroupPromoteOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_GroupPromoteOp_h__ */
