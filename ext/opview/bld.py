import os
import sys
import re
import shutil
import utility
forge = sys.modules["forge"]

def prerequisites():
    needed = ["operator"]
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        needed += ["viewer"]
    return needed

def setup(module):
    module.summary = []

    srcList = [ "opview.pmh",
                "SurfaceViewerOp",
                "opviewDL" ]

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")
    for src in srcList:
            extra = ""
            if src == "SurfaceViewerOp":
                extra = ".nohoudini"
            if re.compile(r'(.*)Op$').match(src):
                with open(manifest, 'a') as outfile:
                    outfile.write('\tspManifest->catalog<String>(\n'+
                            '\t\t\t"OperatorSurfaceI.' + src + '.fe' +
                            extra + '")='+ '"fexOpViewDL";\n');

    dll = module.DLL( "fexOpViewDL", srcList )

    deplibs = forge.corelibs+ [
                "fexDrawDLLib",
                "fexOperateDLLib",
                "fexOperatorDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

    if 'viewer' in forge.modules_confirmed:
        forge.tests += [
            ("inspect.exe",     "fexOpViewDL",                  None,   None) ]

        deplibs += [
                "fexOpenGLDLLib",
                "fexViewerLib" ]

        if forge.media and forge.fe_os != 'FE_WIN32' and forge.fe_os != 'FE_WIN64':
            forge.tests += [ ("model_view.bash",
                    "-count 60 " + forge.media + "/model/skin.rg",
                                                            None,   None) ]
    else:
        module.summary += [ "-viewer" ]

    forge.deps( ["fexOpViewDLLib"], deplibs )

    utility.copy_all(module.modPath + "/bin", forge.libPath)


