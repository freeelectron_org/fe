/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "GroupExtract.h"

namespace fe
{
namespace ext
{

GroupExtract::GroupExtract(void)
{
}

GroupExtract::~GroupExtract(void)
{
}

void GroupExtract::initialize(void)
{
	sp<RecordGroup> rg_invalid;
	cfg< sp<RecordGroup> >("input") = rg_invalid;
	cfg< sp<RecordGroup> >("output") = rg_invalid;
}

void GroupExtract::handle(Record& r_sig)
{
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input");
	if(!rg_input.isValid()) { return; }
	sp<RecordGroup> rg_output =
		cfg< sp<RecordGroup> >("output");
	if(!rg_output.isValid()) { return; }

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		bool is_matched = true;
		for(unsigned int i = 0; i < m_attrNames.size(); i++)
		{
			if(!spRA->layout()->checkAttributeStr(m_attrNames[i]))
			{
				is_matched = false;
			}
		}

		if(is_matched)
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				rg_output->add(spRA->getRecord(i));
			}
		}
	}
}

void GroupExtract::addRequiredAttribute(const String &a_attr)
{
	m_attrNames.push_back(a_attr);
}



} /* namespace ext */
} /* namespace fe */

