/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

Peeker::Peeker(void)
{
	m_indent = 0;
	base();
}

Peeker::~Peeker(void)
{
}

void Peeker::clear(void)
{
	m_string = "";
	m_indent = 0;
	base();
}

String Peeker::output(void)
{
	return m_string;
}

String &Peeker::str(void)
{
	return m_string;
}

void Peeker::base(void)
{
	m_string.cat("[0m");
}

void Peeker::bold(void)
{
	m_string.cat("[1m");
}

void Peeker::normal(void)
{
	m_string.cat("[22m");
}

void Peeker::red(void)
{
	m_string.cat("[31m");
}

void Peeker::green(void)
{
	m_string.cat("[32m");
}

void Peeker::yellow(void)
{
	m_string.cat("[33m");
}

void Peeker::blue(void)
{
	m_string.cat("[34m");
}

void Peeker::magenta(void)
{
	m_string.cat("[35m");
}

void Peeker::cyan(void)
{
	m_string.cat("[36m");
}

void Peeker::white(void)
{
	m_string.cat("[37m");
}

void Peeker::in(void)
{
	m_indent++;
}

void Peeker::out(void)
{
	m_indent--;
	if(m_indent<0) { m_indent = 0; }
}

void Peeker::indent(void)
{
	for(int i = 0; i < m_indent; i++)
	{
		m_string.cat("   ");
	}
}

void Peeker::nl(void)
{
	cat("\n");
	indent();
}

void Peeker::cat(const String &s)
{
	m_string.cat(s);
}

void Peeker::cat(const char *s)
{
	m_string.cat(s);
}


} /* namespace */

