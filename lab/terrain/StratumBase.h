/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumBase_h__
#define __terrain_StratumBase_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Common Class for Elevation

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumBase: public Stratum, virtual public StratumI,
		virtual public RecordableI
{
	public:
						StratumBase(void)			{ setName("StratumBase"); }
virtual					~StratumBase(void)									{}

						// As RecordableI

						using Stratum::bind;
						using RecordableI::bind;

virtual	void			bind(Record& a_rRecord)
						{	const BWORD weak=TRUE;
							RecordView::bind(a_rRecord,weak); }

						//* As StratumI
virtual Phase			getPhase(void) const		{ return Phase(phase()); }
virtual void			setPhase(Phase a_phase)		{ phase()=a_phase; }

virtual Real			getDensity(void) const		{ return density(); }
virtual void			setDensity(Real a_density)	{ density()=a_density; }

virtual Real			getOffset(void) const		{ return offset(); }
virtual void			setOffset(Real a_offset)	{ offset()=a_offset; }

virtual Real			getScale(void) const		{ return scale(); }
virtual void			setScale(Real a_scale)		{ scale()=a_scale; }
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumBase_h__ */
