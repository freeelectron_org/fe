/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Control::handle(Record &record)
{
	m_theaterRV.bind(record);

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_controlRAV.bind(spRA);

		if(!m_controlRAV.recordView().controlled.check(spRA))
		{
			continue;
		}

		for(ControlCenter& controlRV: m_controlRAV)
		{
			controlRV.time()=m_theaterRV.time();
			controlRV.controlled()->setWeak(TRUE);

//			controlRV.dump();

			//*		`Run Behaviors'

			sp<RecordGroup> spBehaviors=controlRV.behaviors();
			if(!spBehaviors.isValid())
			{
				feLog("Control::handle invalid behavior group");
				continue;
			}

			Record controlRecord=controlRV.record();
			sp<SignalerI> spSignalerI=controlRV.selfSignaler();
			if(!spSignalerI.isValid())
			{
				feLog("Control::handle invalid behavior SignalerI");
				continue;
			}

			//* TODO needs some kind of dirty flag
			//* initialize any new behaviors and refresh signaler list
			for(RecordGroup::iterator it2=spBehaviors->begin();
					it2!=spBehaviors->end();it2++)
			{
				sp<RecordArray> spRA2= *it2;
				m_behaviorRAV.bind(spRA2);
				for(I32 index2=0;index2<spRA2->length();index2++)
				{
					Behavior& behaviorRV=m_behaviorRAV[index2];
					sp<HandlerI> spHandlerI=behaviorRV.behaviorHandle();

					if(!spHandlerI.isValid())
					{
						const String& name=behaviorRV.behaviorName();

						sp<PaletteI> spBehaviorPalette=
								m_theaterRV.behaviorPalette();
						FEASSERT(spBehaviorPalette.isValid());
						spHandlerI=spBehaviorPalette->get(name);

						if(!spHandlerI.isValid())
						{
//							feLog("Control::handle"
//									" %s not found in palette\n",name.c_str());

							const char* buffer=name.c_str();
							const FE_UWORD length=strlen(buffer);
							if(length>4 && !strcmp(&buffer[length-4],".lua"))
							{
								spHandlerI=behaviorRV.scope()->registry()
										->create("*.LuaBehavior");
							}
							else
							{
								spHandlerI=behaviorRV.scope()->registry()
										->create(name);
							}

							if(!spHandlerI.isValid())
							{
								feLog("Control::handle"
										" failed to create behavior \"%s\"\n",
										name.c_str());
								continue;
							}

							sp<RecordableI> spRecordableI=spHandlerI;
							if(spRecordableI.isValid())
							{
								Record behavior=behaviorRV.record();
								spRecordableI->bind(behavior);
							}

							spBehaviorPalette->set(name,spHandlerI);
						}

						behaviorRV.behaviorHandle()=spHandlerI;

//						feLog("Control::handle %s layout %s\n",
//								identifier.c_str(),
//								controlRecord.layout()->name().c_str());
					}

					if(spHandlerI.isValid() && !spSignalerI->contains(
							spHandlerI,controlRecord.layout()))
					{
						spSignalerI->insert(spHandlerI,controlRecord.layout());
#if FALSE
						Peeker peeker;
						spSignalerI->peek(peeker);
						feLog("%s\n",peeker.output().c_str());
#endif
					}
				}
			}

			//* behave
			spSignalerI->signal(controlRecord);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
