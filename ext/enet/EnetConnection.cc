#include <enet/enetComs.pmh>
#include "enet/EnetConnection.h"

namespace fe
{
namespace ext
{

#include <enet/enet.h>

#define FE_ENET_VERBOSE	false

//------------------------------------------------------------------------------
EnetConnection::EnetConnection(void):
	m_initialized(false),
	m_numConnections(0),
	m_isServer(false),
	m_host(nullptr),
	m_peer(nullptr)
{
}

//------------------------------------------------------------------------------
EnetConnection::~EnetConnection(void)
{
#if FE_ENET_VERBOSE
	feLog("EnetConnection::~EnetConnection\n");
#endif

	disconnect();
}

//------------------------------------------------------------------------------
bool EnetConnection::connect(bool asServer, const std::string &a_address,
	const short a_port)
{
#if FE_ENET_VERBOSE
	if( asServer )
	{
		feLog("EnetConnection::connect as server \"%s\" port %d\n",
				a_address.c_str(), a_port);
	}
	else
	{
		feLog("EnetConnection::connect as client \"%s\" port %d\n",
				a_address.c_str(), a_port);
	}
#endif

	if(!m_initialized)
	{
		m_initialized = (enet_initialize() == 0);
		if(!m_initialized)
		{
			feLog("EnetConnection::connect: Failed to initialize enet\n");
			return false;
		}
	}

	m_isServer = asServer;

	if( m_isServer )
	{
		m_address.host = ENET_HOST_ANY;
		m_address.port = a_port;

		m_host = enet_host_create(
				&m_address, // the address to bind the server host to
				32,			// allow up to 32 clients and/or outgoing connections
				2,			// allow up to 2 channels to be used, 0 and 1
				0,			// assume any amount of incoming bandwidth
				0);			// assume any amount of outgoing bandwidth

		if(m_host == nullptr)
		{
			feLog("EnetConnection::connect"
					" Failed to create server for port %d\n", a_port);
			return false;
		}

#if FE_ENET_VERBOSE
		feLog("EnetConnection::connect: server \"%d\"\n", a_port);
#endif
		return true;

	}
	else // If client
	{
		m_host = enet_host_create(
				nullptr,	// create a client host
				1,			// only allow 1 outgoing connection
				2,			// allow up 2 channels to be used, 0 and 1
				0,			// assume any amount of incoming bandwidth
				0);			// assume any amount of outgoing bandwidth
		if(m_host == nullptr)
		{
			feLog("EnetConnection::connect"
					" Failed to create client for port %d\n", a_port);
			return false;
		}

		enet_address_set_host(&m_address, a_address.c_str());
		m_address.port = a_port;

		/* Initiate the connection, allocating the two channels 0 and 1. */
		m_peer = enet_host_connect(m_host, &m_address, 2, 0);

		if( m_peer == nullptr )
		{
			feLog("EnetConnection::connect"
					" No available peers for initiating an ENet connection.\n");
			return false;
		}

		tryToConnectToServer();

		return true;
	}
}

//------------------------------------------------------------------------------
bool EnetConnection::tryToConnectToServer(void)
{
	ENetEvent event;

	/* Wait up to 10ms for the connection attempt to succeed. */
	enet_host_service(m_host, &event, 10);

	switch(event.type)
	{
		case ENET_EVENT_TYPE_NONE:
			break; // Not connected yet but don't wait anymore

		case ENET_EVENT_TYPE_CONNECT:
			m_numConnections++;
#if FE_ENET_VERBOSE
			feLog("EnetConnection::connect: Connected to server\n");
#endif
			break;

		case ENET_EVENT_TYPE_DISCONNECT:
			feLog("EnetConnection::Connect: Server disconnected\n");
			enet_peer_reset(m_peer);

			m_numConnections--;
			break;
	}

	return (m_numConnections != 0);
}

//------------------------------------------------------------------------------
bool EnetConnection::disconnect(void)
{
#if FE_ENET_VERBOSE
	feLog("EnetConnection::disconnect\n");
#endif

	if(m_peer != nullptr)
	{
		ENetEvent event;
		enet_peer_disconnect(m_peer, 0);

		/* Allow up to 3 seconds for the disconnect to succeed
		* and drop any packets received packets.
		*/
		while(enet_host_service(m_host, &event, 3000) > 0)
		{
			switch(event.type)
			{
				case ENET_EVENT_TYPE_RECEIVE:
					enet_packet_destroy(event.packet);
					break;

				case ENET_EVENT_TYPE_DISCONNECT:
#if FE_ENET_VERBOSE
					feLog("EnetConnection::disconnect"
							" Disconnection succeeded.\n");
#endif
					break;
			}
		}

		/* We've arrived here, so the disconnect attempt didn't */
		/* succeed yet.  Force the connection down.             */
		if( event.type != ENET_EVENT_TYPE_DISCONNECT )
		{
			enet_peer_reset(m_peer);
		}

		m_peer = nullptr;
	}

	if(m_host != nullptr)
	{
		enet_host_destroy(m_host);
		m_host = nullptr;
	}

	m_numConnections = 0;

	return true;
}

//------------------------------------------------------------------------------
bool EnetConnection::sendReliable(const std::vector<char> &a_message)
{
	return sendReliable( a_message.data(), a_message.size() );
}

//------------------------------------------------------------------------------
bool EnetConnection::sendReliable(const char *message, const int size)
{
#if FE_ENET_VERBOSE
	feLog("EnetConnection::sendReliable\n");
#endif

	if(m_isServer)
	{
		if(m_host == nullptr)
		{
			return false;
		}

		// If no connection then return to avoid backlog of reliable messages
		if(m_numConnections == 0)
		{
			// For a reliable message should I return true or false
			// in this case.  Not really an error
			return true;
		}

		ENetPacket * packet = enet_packet_create(message, size,
				ENET_PACKET_FLAG_RELIABLE);
		enet_host_broadcast(m_host, 0, packet);
	}
	else
	{
		if(m_host == nullptr || m_peer == nullptr)
		{
			return false;
		}

		// If no connections then try to connect to the server
		if(m_numConnections == 0)
		{
			if(!tryToConnectToServer())
			{
				return true;
			}
		}

		ENetPacket * packet = enet_packet_create(message, size,
				ENET_PACKET_FLAG_RELIABLE);

		/* Send the packet to the peer over channel id 0. */
		/* One could also broadcast the packet by         */
		/* enet_host_broadcast(host, 0, packet);         */
		enet_peer_send(m_peer, 0, packet);

		/* One could just use enet_host_service() instead. */
		enet_host_flush(m_host);
	}

	return true;
}


//------------------------------------------------------------------------------
bool EnetConnection::sendUnreliable(const std::vector<char> &a_message)
{
	return sendUnreliable(a_message.data(), a_message.size());
}

//------------------------------------------------------------------------------
bool EnetConnection::sendUnreliable(const char *message, const int size)
{
#if FE_ENET_VERBOSE
	feLog("EnetConnection::sendUnreliable\n");
#endif

	if(m_isServer)
	{
		if(m_host == nullptr)
		{
			return false;
		}

		// If no connection then return true but don't bother
		// sending the message
		if(m_numConnections == 0)
		{
			return true;
		}

		ENetPacket * packet = enet_packet_create(message, size,
				ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);
		enet_host_broadcast(m_host, 0, packet);
	}
	else
	{
		if(m_host == nullptr || m_peer == nullptr)
		{
			return false;
		}

		// If no connections then try to connect to the server
		if(m_numConnections == 0)
		{
			if(!tryToConnectToServer())
			{
				return true;
			}
		}

		ENetPacket * packet = enet_packet_create(message, size,
				ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);

		/* Send the packet to the peer over channel id 0. */
		/* One could also broadcast the packet by         */
		/* enet_host_broadcast(host, 0, packet);         */
		enet_peer_send(m_peer, 0, packet);

		/* One could just use enet_host_service() instead. */
		enet_host_flush(m_host);
	}

	return true;
}

//------------------------------------------------------------------------------
bool EnetConnection::receiveBlocking(std::vector<char> &a_rMessage)
{
#if FE_ENET_VERBOSE
	feLog("EnetConnection::receiveBlocking\n");
#endif
	ENetEvent event;

	while(true)
	{
		/* Wait up to 1000 milliseconds for an event. */
		while(enet_host_service(m_host, &event, 1000) > 0)
		{
			switch(event.type)
			{
				case ENET_EVENT_TYPE_CONNECT:
#if FE_ENET_VERBOSE
					feLog("EnetConnection::receiveBlocking"
							" A new client connected from %x:%u.\n",
							event.peer->address.host,
							event.peer->address.port);

					/* Store any relevant client information here. */
					event.peer->data = "Client information";
#endif
					m_numConnections++;
					break;

				case ENET_EVENT_TYPE_RECEIVE:
					//feLog("A packet of length %u containing %s"
					//		" was received from %s on channel %u.\n",
					//		event.packet->dataLength,
					//		event.packet->data,
					//		event.peer->data,
					//		event.channelID);

#if FE_ENET_VERBOSE
					feLog("EnetConnection::receiveBlocking: %d\n",
							event.packet->dataLength);
#endif

					if(event.packet->data != nullptr &&
							event.packet->dataLength > 0)
					{
						a_rMessage.resize(event.packet->dataLength);
						memcpy(a_rMessage.data(), event.packet->data,
								event.packet->dataLength);

						/* Clean up the packet now that we're done using it. */
						enet_packet_destroy(event.packet);
						return true;
					}
					break;

				case ENET_EVENT_TYPE_DISCONNECT:
#if FE_ENET_VERBOSE
					feLog("EnetConnection::receiveBlocking"
							" %s disconnected.\n", event.peer -> data);
#endif
					/* Reset the peer's client information. */
					event.peer->data = nullptr;
					m_numConnections--;
					break;
			}
		}
	}

	return false;
}


//------------------------------------------------------------------------------
bool EnetConnection::receiveNonblocking(std::vector<char> &a_rMessage,
	const uint_fast32_t msWait)
{
	ENetEvent event;

#if FE_ENET_VERBOSE
	feLog("EnetConnection::receiveNonblocking\n");
#endif

	/* Wait up to 1000 milliseconds for an event. */
	if(enet_host_service(m_host, &event, msWait) > 0)
	{
		switch(event.type)
		{
			case ENET_EVENT_TYPE_CONNECT:
				feLog("EnetConnection::receiveNonblocking"
						" A new client connected from %x:%u.\n",
						event.peer->address.host,
						event.peer->address.port);
				/* Store any relevant client information here. */
				event.peer->data = (void*)"Client information";
				m_numConnections++;
				break;

			case ENET_EVENT_TYPE_RECEIVE:
				//feLog("A packet of length %u containing %s"
				//		" was received from %s on channel %u.\n",
				//		event.packet->dataLength,
				//		event.packet->data,
				//		event.peer->data,
				//		event.channelID);

				if(event.packet->data != nullptr &&
						event.packet->dataLength > 0)
				{
					a_rMessage.resize(event.packet->dataLength);
					memcpy(a_rMessage.data(), event.packet->data,
							event.packet->dataLength);

					/* Clean up the packet now that we're done using it. */
					enet_packet_destroy(event.packet);
					return true;
				}
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				feLog("EnetConnection::receiveNonblocking"
						" %s disconnected.\n", event.peer->data);

				/* Reset the peer's client information. */
				event.peer->data = nullptr;

				m_numConnections--;
				if(m_numConnections < 0)
				{
					m_numConnections = 0;
				}
				break;
		}
	}

	return false;
}


} /* namespace ext */
} /* namespace fe */
