/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

// CORAL

#include <platform/platform.pmh>

#define FESTRING_SUBST_DEBUG	FESTRING_DEBUG
#define FESTRING_PARSE_DEBUG	FALSE

namespace fe
{

void String::Rep::newBuffer(U32 size)
{
	//* for U8 buffer, allocate double the chars for largest possible multibyte

	if(gs_pAllocateFunction)
	{
		m_pBuffer=(FESTRING_U8 *)allocate(2*size*sizeof(FESTRING_U8));
	}
	else
	{
		FEASSERT(0);
		m_pBuffer=(FESTRING_U8 *)malloc(2*size*sizeof(FESTRING_U8));
	}
}

void String::Rep::deleteBuffer(void)
{
	deallocateFunction *pDeallocateFunction=
			gs_pAllocateFunction? gs_pDeallocateFunction: free;

	FEASSERT(pDeallocateFunction);
	if (m_pBuffer)
	{
		pDeallocateFunction(m_pBuffer);
		m_pBuffer = NULL;
	}
}

String::Rep *String::newRep(void)
{
	Rep *pRep=NULL;

	if(gs_pAllocateFunction)
	{
		pRep=(Rep *)allocate(sizeof(Rep));
	}
	else
	{
		FEASSERT(0);
		pRep=(Rep *)malloc(sizeof(Rep));
	}

	pRep->m_references=1;

	return pRep;
}

void String::deleteRep(void)
{
	deallocateFunction *pDeallocateFunction=
			gs_pAllocateFunction? gs_pDeallocateFunction: free;

	m_pRep->deleteBuffer();

	FEASSERT(pDeallocateFunction);
	pDeallocateFunction(m_pRep);

	m_pRep=NULL;
}

String String::pathname(void) const
{
	const FESTRING_U8* str1=rawU8();
	const U32 len1=length();
	if(len1==0)
	{
		return *this;
	}
	const FESTRING_U8* str2=(const FESTRING_U8*)FEMEMRCHR(str1,'/',len1);
	if(!str2 || str2==str1)
	{
		return String();
	}
	FESTRING_U8* buffer=new FESTRING_U8[len1+1];
	memcpy(buffer,str1,len1);
	buffer[str2-str1]=0;
	String result(buffer);
	delete[] buffer;
	return result;
}

String String::basename(void) const
{
	const FESTRING_U8* str1=rawU8();
	const U32 len1=length();
	if(len1==0)
	{
		return *this;
	}
	const FESTRING_U8* str2=(const FESTRING_U8*)FEMEMRCHR(str1,'/',len1);
	if(!str2)
	{
		return *this;
	}
	if(++str2==str1+len1)
	{
		return String();
	}
	return String(str2);
}

String String::prechop(const String& prefix) const
{
	const FESTRING_U8* str1=rawU8();
	const FESTRING_U8* str2=prefix.rawU8();

	const U32 len1=length();
	const U32 len2=prefix.length();

	if(len1<len2 || FESTRNCMP_MB(str1,str2,len2))
	{
		return *this;
	}
	FESTRING_U8* buffer=new FESTRING_U8[len1-len2+1];
	memcpy(buffer,str1+len2,len1-len2);
	buffer[len1-len2]=0;
	String result(buffer);
	delete[] buffer;
	return result;
}

String String::prechop(I32 count) const
{
	if(count<1)
	{
		return *this;
	}

	const I32 len=length();
	if(len<=count)
	{
		return String();
	}

	FESTRING_U8* buffer=new FESTRING_U8[len-count+1];
	memcpy(buffer,rawU8()+count,len-count);
	buffer[len-count]=0;
	String result(buffer);
	delete[] buffer;
	return result;
}

String String::chop(const String& suffix) const
{
	const FESTRING_U8* str1=rawU8();
	const FESTRING_U8* str2=suffix.rawU8();

	const U32 len1=length();
	const U32 len2=suffix.length();

	if(len1<len2 || FESTRNCMP_MB(str1+len1-len2,str2,len2))
	{
		return *this;
	}
	FESTRING_U8* buffer=new FESTRING_U8[len1-len2+1];
	memcpy(buffer,str1,len1-len2);
	buffer[len1-len2]=0;
	String result(buffer);
	delete[] buffer;
	return result;
}

String String::chop(I32 count) const
{
	if(count<1)
	{
		return *this;
	}

	const I32 len=length();
	if(len<=count)
	{
		return String();
	}

	FESTRING_U8* buffer=new FESTRING_U8[len-count+1];
	memcpy(buffer,rawU8(),len-count);
	buffer[len-count]=0;
	String result(buffer);
	delete[] buffer;
	return result;
}

BWORD String::contains(String find) const
{
	return (strstr(c_str(),find.c_str()) != NULL);
}

String String::substitute(String find,String replace) const
{
	String result;

//	fe_printf("String::substitute \"%s\" \"%s\" \"%s\"\n",
//			c_str(),find.c_str(),replace.c_str());

	const char* search=find.c_str();
	I32 size=FESTRLEN(search);
	const char* replacement=replace.c_str();
	const char* start=c_str();
	char* buffer=new char[FESTRLEN(start)];
	while(start)
	{
		const char* end=strstr(start,search);
		if(end)
		{
			strncpy(buffer,start,end-start);
			buffer[end-start]=0;
			result.cat(buffer);
			result.cat(replacement);

//			fe_printf("String::substitute \"%s\" -> \"%s\"\n",
//					start,result.c_str());
			start=end+size;
		}
		else
		{
			result.cat(start);
			start=NULL;
		}
	}
	delete[] buffer;

//	fe_printf("String::substitute \"%s\"\n",result.c_str());

	return result;
}

String String::substituteEnv(std::map<String,String>& a_rMap,
	BWORD a_alsoUseEnv,BWORD a_throwMissing) const
{
	String result;
#if FESTRING_SUBST_DEBUG
	fe_fprintf(stderr,"String::substituteEnv \"%s\"\n",c_str());
#endif

	const I32 maxlength=1024;
	char substring[maxlength+1];
	const char* string=c_str();
	const char* last=string+m_length-1;
	const char* c;
	I32 quoteCount=0;
	while( (c=strchr(string,'$')) )
	{
		I32 length2=c-string;

		const BWORD filled=(length2>maxlength);
		if(filled)
		{
			//* filled buffer before reaching '$'
			length2=maxlength;
		}
		if(length2)
		{
			strncpy(substring,string,length2);
			substring[length2]=0;
#if FESTRING_SUBST_DEBUG
			fe_fprintf(stderr,"String::substituteEnv substring:\n%s\n",substring);
#endif
			result+=substring;
		}
		if(filled)
		{
			string+=length2;
			continue;
		}

		const char* string2=string;
		const char* cq;
		while( (cq=strchr(string2,'"')) )
		{
			if(cq>c)
			{
				break;
			}

			quoteCount++;
			string2=cq+1;
		}

		string=c;
		if(c+1 < last)
		{
			BWORD bracket=(*(c+1) == '{');
			const char *c2(NULL);
			if(bracket)
			{
				//* opening bracket requires matching bracket
				c2=strchr(string,'}');
			}
			else
			{
				//* NOTE strict env variable is: [a-zA-Z_]+[a-zA-Z0-9_]*
				const char* accept0="_"
						"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
						"abcdefghijklmnopqrstuvwxyz";
				const char* acceptN="0123456789_"
						"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
						"abcdefghijklmnopqrstuvwxyz";
				if(strspn(string+1,accept0))
				{
					c2=string+1+strspn(string+1,acceptN);
				}
			}
			if(c2)
			{
				length2=c2-c-1-bracket;
				if(length2<maxlength)
				{
					strncpy(substring,c+1+bracket,length2);
					substring[length2]=0;

					const char *value=NULL;

					std::map<String,String>::const_iterator it=
							a_rMap.find(substring);
					if(it!=a_rMap.end())
					{
						value=it->second.c_str();
					}
					else if(a_alsoUseEnv)
					{
						value=getenv(substring);
					}
#if FESTRING_SUBST_DEBUG
					fe_fprintf(stderr,"String::substituteEnv"
							" variable %s=%s bracket=%d quoteCount=%d\n",
							substring,value,bracket,quoteCount);
#endif
					if(value)
					{
						if(quoteCount%2)
						{
							//* substitute potentially quoted into quoted
							result+=String(value).maybeUnQuote();
						}
						else
						{
							result+=value;
						}
					}
					else if(a_throwMissing)
					{
						feX(e_readFailed,"String::substituteEnv",
								"variable not defined '%s'",
								substring);
					}
					string=c+length2+(bracket? 3: 1);
				}
			}
		}
		if(string==c)
		{
			result+="$";
			string++;
		}
	}
#if FESTRING_SUBST_DEBUG
	fe_fprintf(stderr,"String::substituteEnv %s\n",string);
#endif
	result+=string;

#if FESTRING_SUBST_DEBUG
	fe_fprintf(stderr,"String::substituteEnv result:\n%s\n",result.c_str());
#endif

	return result;
}

String String::stripComments(String a_start,String a_end,
	BWORD a_keepStart,BWORD a_keepEnd)
{
	String result;

	const I32 maxlength=1024;
	char substring[maxlength+1];

	const char* string=c_str();
//	const char* last=string+m_length-1;
	const char* start=a_start.c_str();
	const char* end=a_end.c_str();

	const I32 lenStart=strlen(start);
	const I32 lenEnd=strlen(end);

	const char* c;
	while( (c=strstr(string,start)) )
	{
		I32 length2=c-string;

		const BWORD filled=(length2>maxlength);
		if(filled)
		{
			//* filled buffer before reaching start delimiter
			length2=maxlength;
		}
		if(length2)
		{
			strncpy(substring,string,length2);
			substring[length2]=0;
			result+=substring;
		}
		if(filled)
		{
			string+=length2;
			continue;
		}

		string=c+lenStart;

		if( (c=strstr(string,end)) )
		{
			if(a_keepStart)
			{
				result+=a_start;
			}
			if(a_keepEnd)
			{
				result+=a_end;
			}
			string=c+lenEnd;
		}
		else
		{
			result+=a_start;
			break;
		}
	}

	result+=string;
	return result;
}

BWORD String::dotMatch(const String& operand) const
{
//	fe_printf("dotMatch() \"%s\" \"%s\"\n",rawU8(),operand.rawU8());

	const char* ostr1=c_str();
	const char* ostr2=operand.c_str();

	const I32 olen1=length();
	const I32 olen2=operand.length();

	const char* end1=ostr1+olen1;
	const char* end2=ostr2+olen2;

#if FALSE
	U32 len1=olen1;
	U32 len2=olen2;
	while(str2[0]=='*' && str2[1]=='.')
	{
		const FESTRING_U8* str=(const FESTRING_U8*)memchr(str1,'.',len1);
		if(!str)
		{
			return false;
		}
		str++;
		str2+=2;
		len1-=str-str1;
		len2-=2;
		str1=str;
		FEASSERT(str1<rawU8()+olen1);
		FEASSERT(str2<operand.rawU8()+olen2);

		fe_printf("  rematch \"%s\" \"%s\"\n",str1,str2);
	}

	if(FESTRLEN(str1)>=len2 && (str1[len2]==0 || str1[len2]=='.') &&
			!FESTRNCMP_MB(str1,str2,len2))
	{
		fe_printf("  matched\n");
		return true;
	}

	fe_printf("  not matched\n");
	return false;
#else
	const char* str1=ostr1;
	const char* str2=ostr2;
	I32 len1=olen1;
	I32 len2=olen2;
	while(len1 && len2)
	{
		const char* token1=strchr((const char*)str1,'.');
		const char* token2=strchr((const char*)str2,'.');

		I32 sublen1=token1? token1-str1: len1;
		I32 sublen2=token2? token2-str2: len2;

//		fe_printf("  sub %s:%d %s:%d\n",str1,sublen1,str2,sublen2);

		if(!(sublen2==1 && *str2=='*') &&
				(sublen1!=sublen2 || strncmp(str1,str2,sublen1)))
		{
//			fe_printf("  not matched\n");
			return false;
		}

		len1-=sublen1+(token1!=0);
		len2-=sublen2+(token2!=0);

		str1=end1-len1;
		str2=end2-len2;

		FEASSERT(str1<=end1);
		FEASSERT(str2<=end2);
	}

//	fe_printf("  matched\n");
	return !len2;
#endif
}

String &String::sPrintf(const char* fmt, ...)
{
	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	return *this;
}

String &String::cat(const char* operand)
{
	String previous(*this);
	sPrintf("%s%s",previous.c_str(),operand);

	return *this;
}

// Brute force.  Rewrite if necessary for speed or space.
String &String::catf(const char* fmt, ...)
{
	String tmp;

	int size=0;
	while(size>=0)
	{
		va_list ap;
		va_start(ap, fmt);

		tmp.vsPrintf(fmt,ap,size);

		va_end(ap);
	}

	String previous(*this);
	sPrintf("%s%s",previous.c_str(),tmp.c_str());

	return *this;
}

// TODO << operator
String &String::cat(std::list<String> &strList, String sep)
{
	for(std::list<String>::iterator it = strList.begin();
		it != strList.end(); it++)
	{
		if(it != strList.begin())
		{
			cat(sep.c_str());
		}
		cat(it->c_str());
	}
	return *this;
}

void String::resize(U32 size,char c)
{
//	fe_fprintf(stderr,"String::resize %d %c \"%s\"\n",size,c,c_str());

	const U32 len1=length();
	if(size==len1)
	{
		return;
	}

	const FESTRING_U8* str1=rawU8();

	FESTRING_U8* buffer=new FESTRING_U8[size+1];

	if(size<len1)
	{
		memcpy(buffer,str1,size);
	}
	else
	{
		memcpy(buffer,str1,len1);
		for(U32 n=len1;n<size;n++)
		{
			buffer[n]=c;
		}
	}
	buffer[size]=0;

	*this=buffer;
	delete[] buffer;
//	fe_fprintf(stderr,"  now \"%s\"\n",c_str());
}

String String::maybeQuote(String beginQuote,String endQuote) const
{
	if(strcspn(c_str()," \t\n") == U32(m_length))
	{
		return *this;
	}
	return beginQuote + *this + endQuote;
}

String String::maybeUnQuote(String beginQuote,String endQuote) const
{
//	fe_fprintf(stderr,"String::maybeUnQuote '%s'\n",c_str());

	const char* str0=c_str();

	const I32 len1=beginQuote.length();
	const I32 len2=endQuote.length();
	if(m_length<len1+len2 ||
			strncmp(str0,beginQuote.c_str(),len1) ||
			strncmp(str0+m_length-len2,endQuote.c_str(),len2) )
	{
		return *this;
	}

	const I32 newLength=m_length-len1-len2;
	FESTRING_U8* buffer=new FESTRING_U8[newLength+1];
	memcpy(buffer,str0+len1,newLength);
	buffer[newLength]=0;
	String result(buffer);
	delete[] buffer;

//	fe_fprintf(stderr,"String::maybeUnQuote -> '%s'\n",result.c_str());

	return result;
}

//* TODO can we reverse the first two arguments?
String String::parse(String quote, String delimiters, BWORD eachDelimiter)
{
//	String result=c_str();
//	*this="";
//	return result;

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"String::parse(\"%s\",\"%s\",%d) \"%s\"\n",
			quote.c_str(),delimiters.c_str(),eachDelimiter,c_str());
#endif

	const I32 bufferSize=m_length+1;
	char* buffer=new char[bufferSize];

	strcpy(buffer,c_str());

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"buffer %p \"%s\" bufferSize %d\n",
			buffer,buffer,bufferSize);
#endif

	const size_t delimiterCount=strspn(buffer,delimiters.c_str());
	char* start=buffer+
			((eachDelimiter && delimiterCount)? 1: delimiterCount);

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"start  %p \"%s\"\n",start,start);
#endif

	U32 len=0;
	U32 quotes=0;

	char* endQuote=new char[2];
	endQuote[0]=0;
	endQuote[1]=0;

	while(TRUE)
	{
		size_t toQuote=strcspn(&start[len],quote.c_str());
		while(toQuote && start[len+toQuote-1]=='\\')
		{
#if FESTRING_PARSE_DEBUG
			fe_fprintf(stderr,"skip escape %d %s\n",
					toQuote,&start[len+toQuote-1]);
#endif

			char* pChar=&start[len+toQuote-1];
			while(*pChar)
			{
				*pChar=*(pChar+1);
				pChar++;
			}

			if(!start[len+toQuote])
			{
				//* no more chars, end quote not found
				toQuote=0;
				break;
			}

#if FESTRING_PARSE_DEBUG
			fe_fprintf(stderr,"trim escape %s\n",&start[len+toQuote-1]);
#endif

			size_t toMore=strcspn(&start[len+toQuote],quote.c_str());
			if(!toMore)
			{
				//* found quote
				break;
			}
			toQuote+=toMore;
		}

		const size_t toDelimiter=strcspn(&start[len],delimiters.c_str());

#if FESTRING_PARSE_DEBUG
		fe_fprintf(stderr,"parse '%s' %d %d\n",&start[len],toQuote,toDelimiter);
#endif

		if(toDelimiter<=toQuote)
		{
			len+=toDelimiter;
			break;
		}

		len+=toQuote;

		endQuote[0]=start[len];

		len++;

		size_t toEndQuote=strcspn(&start[len],endQuote);
		while(toEndQuote && start[len+toEndQuote-1]=='\\')
		{
#if FESTRING_PARSE_DEBUG
			fe_fprintf(stderr,"skip escape2 %d %s\n",
					toEndQuote,&start[len+toEndQuote-1]);
#endif

			char* pChar=&start[len+toEndQuote-1];
			while(*pChar)
			{
				*pChar=*(pChar+1);
				pChar++;
			}

			if(!start[len+toEndQuote])
			{
				//* no more chars, end quote not found
				toEndQuote=0;
				break;
			}

			size_t toMore=strcspn(&start[len+toEndQuote],endQuote);

#if FESTRING_PARSE_DEBUG
			fe_fprintf(stderr,"trim escape2 %s %d\n",
					&start[len+toEndQuote],toMore);
#endif

			if(!toMore)
			{
				//* found end quote
				break;
			}
			toEndQuote+=toMore;
		}

		len+=toEndQuote;

		const BWORD hasEndQuote=(start[len]==endQuote[0]);
		len+=hasEndQuote;

#if FESTRING_PARSE_DEBUG
		fe_fprintf(stderr,"check '%s' %d '%s' %d %d %d\n",
				start,len,&start[len],!toQuote,hasEndQuote);
#endif

//		if(!toQuote && hasEndQuote)
		if(!toQuote && hasEndQuote)
		{
			start++;
			len-=2;
			quotes++;
#if FESTRING_PARSE_DEBUG
			fe_fprintf(stderr,"unquoted '%s'\n",start);
#endif
			break;
		}
	}

	delete[] endQuote;

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"start  %p \"%s\"\n",start,start);
#endif

	char* copy=new char[FESTRLEN(start)+1];
	strcpy(copy,start);

	char* remain=copy+len+quotes;
	*this=remain;

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"copy   %p \"%s\"\n",copy,copy);
	fe_fprintf(stderr,"remain %p \"%s\"\n",remain,remain);
	fe_fprintf(stderr,"len=%d\n",len);
#endif

	copy[len]=0;

	String token=copy;
	delete[] copy;

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"token  %p \"%s\"\n",token.c_str(),token.c_str());
#endif

	delete[] buffer;

#if FESTRING_PARSE_DEBUG
	fe_fprintf(stderr,"String::parse done\n");
#endif

	return token;
}

I32 String::lines(void) const
{
	I32 count(0);
	const char* buffer=c_str();
	while((buffer=strchr(buffer,'\n')) != NULL)
	{
		buffer++;
		count++;
	}
	return count;
}

String String::line(I32 a_lineNumber) const
{
	I32 count(0);
	const char* buffer=c_str();
	while(count<a_lineNumber && (buffer=strchr(buffer,'\n')) != NULL)
	{
		buffer++;
		count++;
	}
	const char* buffer2=strchr(buffer,'\n');

	if(!buffer)
	{
		//* no newlines
		return c_str();
	}
	if(!buffer2)
	{
		//* last line
		return buffer;
	}

	const I32 len=buffer2-buffer;
	char* substring=new char[len+1];
	strncpy(substring,buffer,len);
	substring[len]=0;

	String result(substring);

	delete[] substring;
	return result;
}

String String::join(const String &a_sep, std::list<String> &a_list)
{
	String rv;
	rv.cat(a_list, a_sep);
	return rv;
}

String String::join(const String &a_sep, const String &a_1, const String &a_2)
{
	std::list<String> list;
	list.push_back(a_1);
	list.push_back(a_2);
	String rv;
	rv.cat(list, a_sep);
	return rv;
}

String String::join(const String &a_sep, const String &a_1, const String &a_2,
		const String &a_3)
{
	std::list<String> list;
	list.push_back(a_1);
	list.push_back(a_2);
	list.push_back(a_3);
	String rv;
	rv.cat(list, a_sep);
	return rv;
}

// Brute force.  Rewrite if necessary for speed.
String& String::vsPrintf(const char* fmt, va_list ap,int& size)
{
	if(size<0)
	{
		feX(e_usage,"String::vsPrintf",
				"size<0 indicates unheeded discontinuation");
	}
	if(!size)
	{
		// start at 80 bytes, seems reasonable (1 tty line)
		size=80;
	}
	FESTRING_U8 *tmpbuf = new FESTRING_U8[size];
	FEASSERT(NULL != tmpbuf);

	int n=FEVSNPRINTF((char *)tmpbuf, size, fmt, ap);

	if(n> -1 && n<size)
	{
		size= -1;
		operator=(tmpbuf);
		delete [] tmpbuf;
		return *this;
	}
	if(n> -1)
	{
		size=n+1;
	}
	else
	{
		size*=2;
	}

	delete [] tmpbuf;
	operator=("");
	return *this;
}

#if FALSE
String &String::vsPrintf(const char* fmt, va_list ap)
{
	fe_fprintf(stderr,"String::vsPrintf \"%s\"\n",fmt);

	// start at 80 bytes, seems reasonable (1 tty line)
	int sz = 80;
	FESTRING_U8 *tmpbuf = new FESTRING_U8[sz];
	FEASSERT(NULL != tmpbuf);
	while(true)
	{
		int n=FEVSNPRINTF((char *)tmpbuf, sz, fmt, ap);

		if(n> -1 && n<sz)
			break;
		if(n> -1)
			sz=n+1;
		else
			sz*=2;

		delete [] tmpbuf;
		tmpbuf = new FESTRING_U8[sz];
		FEASSERT(NULL != tmpbuf);

		// NOTE calling vsnprintf again without va_end,va_start is undefined
		feX("String::vsPrintf warning, expanding buffer\n");
	}
	operator=(tmpbuf);
	delete [] tmpbuf;

	return *this;
}
#endif

void String::output(std::ostream &ostrm) const
{
	const U32 nlen = htonl(m_length);
	ostrm.write((char *)&nlen, 4);
	ostrm.write(c_str(), m_length);
}

void String::input(std::istream &istrm)
{
	U32 len;
	istrm.read((char *)&len, 4);
	len = ntohl(len);

	// TODO remove this extra memory buffer.  input directly into raw buffer
	char *buffer = new char[len + 1];
	istrm.read(buffer, len);
	buffer[len] = 0;
	operator=(buffer);
	delete[] buffer;
}

//* TODO optimize array usage
void String::forceCase(BWORD upper)
{
	if(m_pRep)
	{
		String temp=*this;
		*this=temp.c_str();	// replicate
	}

#ifdef FESTR_MB2UPPER
	if(upper)
		FESTR_MB2UPPER(c_str());
	else
		FESTR_MB2LOWER(c_str());
#else
	//* manual 8-bit version
	I32 m;
	for(m=0;m<m_length;m++)
	{
//		if(m_pRep->m_pBuffer[m] < 0x7f)
			buffer()[m]=upper? toupper(c_str()[m]): tolower(c_str()[m]);
	}
#endif
}

String errorString(int error)
{
	String message;
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	// from MSDN example
	LPVOID lpMsgBuf;
	if (!FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			error,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL ))
		return "no error string";

	message = (char *)lpMsgBuf;

	LocalFree( lpMsgBuf );
#else
	message = strerror(error);
#endif
	return message.chop("\n");
}

int String::integer(void) const
{
	return atoi(c_str());
}

Real String::real(void) const
{
	return (Real)atof(c_str());
}

bool String::match(const String a_pattern) const
{
	return Regex(a_pattern.c_str()).match(c_str());
}

bool String::search(const String a_pattern) const
{
	return Regex(a_pattern.c_str()).search(c_str());
}

String String::replace(const String a_pattern,const String a_replacement) const
{
	return Regex(a_pattern.c_str()).replace(c_str(),a_replacement.c_str());
}

String String::convertGlobToRegex(void) const
{
	String result=*this;

	result=result.substitute(".","\\.");
	result=result.substitute("?",".");
	result=result.substitute("*",".*");
	result=result.substitute("[!",".[^");

	return result;
}

U64 String::computeFowlerNollVo1(void) const
{
	const unsigned char *buffer=rawU8();
	const U32 len=length();

	//* 64-bit FNV-1
	U64 value=0xcbf29ce484222325;
	for(U32 index=0;index<len;index++)
	{
		value*=0x100000001b3;
		value^=buffer[index];
	}

	return value;
}

} /* namespace */
