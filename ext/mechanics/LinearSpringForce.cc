/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "LinearSpringForce.h"

namespace fe
{
namespace ext
{

LinearSpringForce::LinearSpringForce(sp<SemiImplicit> a_integrator)
{
	m_integrator = a_integrator;
	m_perturbation = fe::tol;
}

LinearSpringForce::~LinearSpringForce(void)
{
}

void LinearSpringForce::clear(void)
{
}

void LinearSpringForce::pairs(sp<RecordGroup> rg_input, SemiImplicit::t_pairs &a_pairs)
{
	AsLinearSpring		asLinearSpring;
	AsForcePoint		asForcePoint;
	AsSolverParticle	asSolverParticle;

	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearSpring.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());
		asSolverParticle.bind(spRA->layout()->scope());

		if(asLinearSpring.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_spring = spRA->getRecord(i);
				WeakRecord &r_left = asLinearSpring.left(r_spring);
				WeakRecord &r_right = asLinearSpring.right(r_spring);

				int i_L = asSolverParticle.index(r_left);
				int i_R = asSolverParticle.index(r_right);

				int i_0, i_1;
				if(i_L < i_R)
				{
					i_0 = i_L;
					i_1 = i_R;
				}
				else
				{
					i_0 = i_R;
					i_1 = i_L;
				}

				a_pairs.push_back(SemiImplicit::t_pair(i_0, i_1));
			}
		}
	}
}

void LinearSpringForce::compile(sp<RecordGroup> rg_input, std::vector<SemiImplicit::Particle> &a_particles, SemiImplicit::CompileMatrix &a_compileMatrix)
{
	AsLinearSpringExtended		asLinearSpring;
	AsForcePoint				asForcePoint;
	AsSolverParticle			asSolverParticle;
	AsLinearSpringForceReturn	asLSFReturn;

	/* find size and set size of spring structure */
	m_springs.clear();
	unsigned int n = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearSpring.bind(spRA->layout()->scope());

		if(asLinearSpring.check(spRA))
		{
			n += (unsigned int)spRA->length();
		}
	}
	m_springs.resize(n);



	unsigned int ii = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearSpring.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());
		asSolverParticle.bind(spRA->layout()->scope());
		asLSFReturn.bind(spRA->layout()->scope());

		if(asLinearSpring.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_spring = spRA->getRecord(i);
				WeakRecord &r_left = asLinearSpring.left(r_spring);
				WeakRecord &r_right = asLinearSpring.right(r_spring);

//TODO: cannot resize -- need to preallocate since the pointers are hard used
				if(asLSFReturn.check(r_spring))
				{
					asLSFReturn.spring(r_spring) = (void *)&(m_springs[ii]);
				}
				Spring &spring = m_springs[ii];
				ii++;


				spring.m_flags = asLinearSpring.flags(r_spring);

				spring.m_restlength = asLinearSpring.length(r_spring);
				spring.m_stiffness = asLinearSpring.stiffness(r_spring);
				spring.m_damping = asLinearSpring.damping(r_spring);

				int i_L = asSolverParticle.index(r_left);
				int i_R = asSolverParticle.index(r_right);

				int i_0, i_1;
				if(i_L < i_R)
				{
					i_0 = i_L;
					i_1 = i_R;
				}
				else
				{
					i_0 = i_R;
					i_1 = i_L;
				}


				spring.m_location[0] = &(a_particles[i_0].m_location);
				spring.m_location[1] = &(a_particles[i_1].m_location);

				if(spring.m_restlength < 0.0)
				{
					t_solve_v3 d = *(spring.m_location[1]) - *(spring.m_location[0]);
					spring.m_restlength = magnitude(d);
				}

				spring.m_velocity[0] = &(a_particles[i_0].m_velocity);
				spring.m_velocity[1] = &(a_particles[i_1].m_velocity);

				spring.m_force[0] = &(a_particles[i_0].m_force);
				spring.m_force[1] = &(a_particles[i_1].m_force);

				a_compileMatrix.entry(i_0, i_0).first
					.push_back(&(spring.m_diagonal_dfdx[0]));
#if 0
for(unsigned int m = 0; m < a_compileMatrix.entry(i_0, i_0).first.size(); m++)
{
	fe_fprintf(stderr, " -- %d 0x%x\n", m, a_compileMatrix.entry(i_0, i_0).first[m]);
}
#endif
				a_compileMatrix.entry(i_0, i_0).second
					.push_back(&(spring.m_diagonal_dfdv[0]));

				a_compileMatrix.entry(i_1, i_1).first
					.push_back(&(spring.m_diagonal_dfdx[1]));
				a_compileMatrix.entry(i_1, i_1).second
					.push_back(&(spring.m_diagonal_dfdv[1]));

				a_compileMatrix.entry(i_0, i_1).first
					.push_back(&(spring.m_offdiagonal_dfdx));
				a_compileMatrix.entry(i_0, i_1).second
					.push_back(&(spring.m_offdiagonal_dfdv));

			}
		}
	}
}

bool LinearSpringForce::validate(void)
{
	unsigned int n = (unsigned int)m_springs.size();
	for(unsigned int i = 0; i < n; i++)
	{
		Spring &spring = m_springs[i];
		t_solve_v3 x1[2];
		x1[0] = *(spring.m_location[0]);
		x1[1] = *(spring.m_location[1]);

		t_solve_v3 dx1;
		dx1 = x1[1] - x1[0];

#if 0
		t_solve_v3 x0[2];
		x0[0] = *(spring.m_prev_location[0]);
		x0[1] = *(spring.m_prev_location[1]);

		t_solve_v3 dx0;
		dx0 = x0[1] - x0[0];
#endif

		// TODO: expose allowed...maybe via per spring attr
		// TODO: or maybe a hueristic from stiffness/damping
		t_solve_real allowed = 0.1;
		//t_solve_real deltaX = magnitude(dx1) - magnitude(dx0);
		t_solve_real deltaX = spring.m_prev_distance - magnitude(dx1);
		t_solve_real delta = fabs(deltaX);


		if(delta > allowed)
		{
			return false;
		}
	}
	return true;
}


void LinearSpringForce::accumulate(void)
{

	t_solve_matrix dfdx;
	t_solve_matrix dfdv;

	unsigned int n = (unsigned int)m_springs.size();
	for(unsigned int i = 0; i < n; i++)
	{
		t_solve_v3 l[2];

		Spring &spring = m_springs[i];

		l[0] = *(spring.m_location[0]);
		l[1] = *(spring.m_location[1]);

		t_solve_v3 force;
		set(force);

		t_solve_real &length = spring.m_restlength;

		t_solve_v3 delta = (l[1] - l[0]);

		if(spring.m_flags & FE_LSF_XIGNORE)
		{
			delta[0] = 0.0;
		}
		if(spring.m_flags & FE_LSF_YIGNORE)
		{
			delta[1] = 0.0;
		}
		if(spring.m_flags & FE_LSF_ZIGNORE)
		{
			delta[2] = 0.0;
		}

		// TODO: maybe avoid coiincident points altogether
		// slightly perturb coiincident points
		if(isZeroV3(delta, m_perturbation))
		{
#if 0
			// WARNING:  this discontinuity is a problem
			//   if rest length is actually zero, continue
			if(isZero(length, 1.0e-12))
			{
				continue;
			}
#endif
			l[1][0] += m_perturbation;
			l[1][1] += m_perturbation;
			l[1][2] += m_perturbation;

			delta = (l[1] - l[0]);
		}

		t_solve_real distance = magnitude(delta);
		t_solve_v3 dx = delta;
		t_solve_real d = distance;
		spring.m_prev_distance = distance;

		delta *= 1.0/distance;
		distance -= length;


		t_solve_real stiffness = spring.m_stiffness;
		t_solve_real damping = spring.m_damping;

		if(spring.m_flags & FE_LSF_NOCOMPRESS)
		{
			if(distance < 0.0)
			{
//TODO: hmmm
				stiffness *= 0.0;
				damping *= 0.0;
			}
		}

		force += stiffness * delta * distance;

		t_solve_v3 v[2];
		v[0] = *(spring.m_velocity[0]);
		v[1] = *(spring.m_velocity[1]);

		t_solve_v3 dv(0.0,0.0,0.0);
		if(!isZero(v[1] - v[0]))
		{
			dv = v[1] - v[0];
			t_solve_real mdv = magnitude(dv);
			t_solve_v3 ndv = dv/mdv;

			delta *= mdv * dot(ndv, delta);

			delta *= damping;

			force += delta;
		}


#if 0
		if(spring.m_flags & FE_LSF_NOCOMPRESS)
		{
			if(distance > 0.0)
			{
				*(spring.m_force[0]) += force;
				*(spring.m_force[1]) += -force;
			}
			else
			{
			}
		}
		else
		{
			*(spring.m_force[0]) += force;
			*(spring.m_force[1]) += -force;
		}
#endif


		*(spring.m_force[0]) += force;
		*(spring.m_force[1]) += -force;

		// JACOBIANS
		setAll(dfdx, 0.0);
		setAll(dfdv, 0.0);

		t_solve_real dx00 = dx[0] * dx[0];
		t_solve_real dx11 = dx[1] * dx[1];
		t_solve_real dx22 = dx[2] * dx[2];
		t_solve_real dx01 = dx[0] * dx[1];
		t_solve_real dx02 = dx[0] * dx[2];
		t_solve_real dx12 = dx[1] * dx[2];

		t_solve_real d_sqr = d*d;

		t_solve_real m = length / d;

#if 1
		if((spring.m_flags & FE_LSF_PULLCOMPRESS) && (distance < 0.0))
		{
			m = (d+distance) / d;
		}
#endif

		t_solve_real tmp = dv[0]*dx[0] + dv[1]*dx[1] + dv[2]*dx[2];
		t_solve_real s = -damping * tmp/(d_sqr*d_sqr);
		t_solve_real cd_sqr = -damping / d_sqr;

		t_solve_real a = stiffness * (m - 1.0);
		t_solve_real b = -stiffness * (m / d_sqr);

		// set spring dfdx
		dfdx(0,0) = a + b * dx00;
		dfdx(0,1) =		b * dx01;
		dfdx(0,2) =		b * dx02;
		dfdx(1,1) = a + b * dx11;
		dfdx(1,2) =		b * dx12;
		dfdx(2,2) = a + b * dx22;
		dfdx(1,0) = dfdx(0,1);
		dfdx(2,0) = dfdx(0,2);
		dfdx(2,1) = dfdx(1,2);

#if 1
		// add damping dfdx
		dfdx(0,0) += s * (d_sqr - dx00);
		dfdx(0,1) += -s * dx01;
		dfdx(1,0) += -s * dx01;
		dfdx(0,2) += -s * dx02;
		dfdx(2,0) += -s * dx02;
		dfdx(1,1) += s * (d_sqr - dx11);
		dfdx(1,2) += -s * dx12;
		dfdx(2,1) += -s * dx12;
		dfdx(2,2) += s * (d_sqr - dx22);

		// set damping dfdv
		dfdv(0,0) = cd_sqr * dx00;
		dfdv(0,1) = cd_sqr * dx01;
		dfdv(0,2) = cd_sqr * dx02;
		dfdv(1,1) = cd_sqr * dx11;
		dfdv(1,2) = cd_sqr * dx12;
		dfdv(2,2) = cd_sqr * dx22;
		dfdv(1,0) = dfdv(0,1);
		dfdv(2,0) = dfdv(0,2);
		dfdv(2,1) = dfdv(1,2);
#endif

#if 1
		// TODO: expose control over whether to do this or not
		// dfdx is unstable under compression
		if(distance < 0.0)
		{
			//dfdx = dfdx * 0.0;
#if 0
			if( (spring.m_flags & FE_LSF_NOCOMPRESS) )
			{
				dfdv = dfdx;
			}
#endif
		}
#endif

#if 1
		if(spring.m_flags & FE_LSF_PULLCOMPRESS)
		{
			add(*(spring.m_diagonal_dfdx[0]), *(spring.m_diagonal_dfdx[0]), dfdx);
			add(*(spring.m_diagonal_dfdx[1]), *(spring.m_diagonal_dfdx[1]), dfdx);
			dfdx *= -1;
			if(distance >= 0.0)
			{
				add(*(spring.m_offdiagonal_dfdx), *(spring.m_offdiagonal_dfdx), dfdx);
			}

			add(*(spring.m_diagonal_dfdv[0]), *(spring.m_diagonal_dfdv[0]), dfdv);
			add(*(spring.m_diagonal_dfdv[1]), *(spring.m_diagonal_dfdv[1]), dfdv);
			dfdv *= -1;
			if(distance >= 0.0)
			{
				add(*(spring.m_offdiagonal_dfdv), *(spring.m_offdiagonal_dfdv), dfdv);
			}
		}
		else
#endif
		{
			add(*(spring.m_diagonal_dfdx[0]), *(spring.m_diagonal_dfdx[0]), dfdx);
			add(*(spring.m_diagonal_dfdx[1]), *(spring.m_diagonal_dfdx[1]), dfdx);
			dfdx *= -1;
			add(*(spring.m_offdiagonal_dfdx), *(spring.m_offdiagonal_dfdx), dfdx);

			add(*(spring.m_diagonal_dfdv[0]), *(spring.m_diagonal_dfdv[0]), dfdv);
			add(*(spring.m_diagonal_dfdv[1]), *(spring.m_diagonal_dfdv[1]), dfdv);
			dfdv *= -1;
			add(*(spring.m_offdiagonal_dfdv), *(spring.m_offdiagonal_dfdv), dfdv);
		}
	}
}

} /* namespace */
} /* namespace */

