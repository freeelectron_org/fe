import os
import sys
forge = sys.modules["forge"]

def prerequisites():
    return [
        "viewer",
        "moa",
    ]

def setup(module):

    module.cppmap= { 'std': forge.use_std("c++17") }

    srcList = [
        "viewer_system.pmh",
        "ViewerSystemDL",
        "MOAViewer",
        "ViewerSystem"
    ]

    dll = module.DLL("fexViewerSystemDL", srcList)

    deplibs = forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexMechanicsDLLib",
                "fexMoaDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib",
                "fexSignalDLLib",
                "fexViewerDLLib" ]

    forge.deps(["fexViewerSystemDLLib"], deplibs)

    module.Module('test')
