/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

void Habitat::setTime(Real time)
{
	U32 numStrata=numStratum();
	for(U32 m=0;m<numStrata;m++)
	{
		stratum(m)->setTime(time);
	}
}

//* http://mathworld.wolfram.com/SphericalCap.html

//* NOTE drag = b*v^n, with n from 1 at low speeds to 2 at high speeds

void Habitat::calcInfluence(SpatialVector& rForce,
		Real radius,SpatialVector center,SpatialVector velocity)
{
	const Real gravity=9.81f;	//* TODO param
	const Real drag=4.0f;		//* TODO param

	const SpatialVector unitZ(0.0f,0.0f,1.0f);
	const Real radius2=radius*radius;
	const Real radius3=radius2*radius;
	const Real totalVolume=(1.333f*pi)*radius3;
//	Real exposure=(4.0f*pi)*radius2;
	Real exposure=pi*radius2;

	set(rForce);

	const U32 numStrata=numStratum();
	for(U32 m=0;m<numStrata;m++)
	{
		sp<StratumI> spStratumI=stratum(m);
		const StratumI::Phase phase=spStratumI->getPhase();
		const Real height=spStratumI->height(center[0],center[1]);
		const SpatialVector& normal=spStratumI->normal(center[0],center[1]);

		if(phase!=StratumI::e_solid)
		{
			SpatialVector buoyancyDir=unitZ;
			Real density=spStratumI->getDensity();
			Real volume=0.0f;
			Real area=0.0f;

			//* NOTE treating solid as a [presumably] dense liquid

			if(phase==StratumI::e_gas)
			{
				density*=1.0f-center[2]/height;
				if(density<0.0f)
					density=0.0f;

				volume=totalVolume;
				area=exposure;
			}
			else if(phase==StratumI::e_liquid || phase==StratumI::e_solid)
			{
				Real submersion=normal[2]*(height-center[2])+radius;
				if(submersion>0.0f)
				{
					BWORD fullySubmersed=(submersion>2.0f*radius);
					if(fullySubmersed)
						submersion=2.0f*radius;

					volume=(0.333f*pi)*submersion*submersion*
							(3.0f*radius-submersion);
//					area=(2.0f*pi)*radius*submersion;
					area=radius2*acosf((radius-submersion)/radius)-
							(radius-submersion)*
							sqrtf(2.0f*radius*submersion-submersion*submersion);

					//* make shallow submersion similar to hard surface
					if(!fullySubmersed)
					{
						//* quicky LERP
						const Real ratio=volume/totalVolume;
						buoyancyDir=(1.0f-ratio)*normal+ratio*unitZ;
						normalize(buoyancyDir);
					}
				}
			}

			if(phase!=StratumI::e_solid)
			{
				//* drag

				//* NOTE Is this a valid approximation?
				const Real b=drag*density*area;
				rForce-=b*velocity;
			}

			//* buoyancy = weight of displaced matter
			const Real buoyancy=gravity*volume*density;
			rForce+=buoyancy*buoyancyDir;

			exposure-=area;
		}
	}
}

void Habitat::impact(SpatialVector& center,SpatialVector& velocity,
		Real mass,Real radius,Real deltaT)
{
	const Real restitution=0.6f;	//* TODO param

	const Real restoration=1.0f+restitution;

	const U32 numStrata=numStratum();
	for(U32 m=0;m<numStrata;m++)
	{
		sp<StratumI> spStratumI=stratum(m);
		const StratumI::Phase phase=spStratumI->getPhase();
		const Real height=spStratumI->height(center[0],center[1]);
		const SpatialVector& normal=spStratumI->normal(center[0],center[1]);

		if(phase==StratumI::e_solid)
		{
			const Real submersion=normal[2]*(height-center[2])+radius;
			if(submersion>0.0f)
			{
				const SpatialVector lastCenter=center-deltaT*velocity;

				//* use midpoint as contact (otherwise need iterative cast)
				SpatialVector contact=0.5f*(center+lastCenter);
				contact[2]=spStratumI->height(contact[0],contact[1]);

				//* correction
				const Real intrusion=dot(normal,contact-center)+radius;
				if(intrusion>0.0f)
				{
					if(intrusion>0.1f)
					{
						feLog("Habitat::impact"
								" intense correction %.6G along %s\n"
								"\tat %s vel %s\n",
								intrusion,print(normal).c_str(),
								print(center).c_str(),
								print(velocity).c_str());
					}
					center+=normal*intrusion;
				}

				//* reflection
				const Real dotSpeed=dot(normal,velocity);
				if(dotSpeed<0.0f)
				{
					if(dotSpeed< -10.0f)
					{
						feLog("Habitat::impact"
								" intense reflection %.6G along %s\n"
								"\tat %s vel %s\n",
								dotSpeed,print(normal).c_str(),
								print(center).c_str(),
								print(velocity).c_str());
					}
					velocity-=normal*(restoration*dotSpeed);
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */