/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <chrono>
#include "networkhost/networkhost.h"
#include "beacon/beacon.h"

int main(int argc,char** argv)
{
    fe::sp<fe::Master> spMaster(new fe::Master);
    fe::sp<fe::Registry> spRegistry=spMaster->registry();

    spRegistry->manage("fexBeaconClientDL");
	spRegistry->dump();
    fe::sp<beacon::BeaconClientStartupI>spBeaconClientStartup(
			spRegistry->create("*.BeaconClientStartup"));
	feLog("spBeaconClientStartup valid %d\n",spBeaconClientStartup.isValid());

	if(!spBeaconClientStartup.isValid())
    {
        feX(argv[0], "couldn't create Beacon client component");
		return 1;
    }

	if(argc < 2 || !spBeaconClientStartup->startup(argv[1]))
	{
		feLog("Beacon client init failed using file: %s\n", argv[1]);
		return 2;
	}

	feLog("Testing keep alive...\n");
	// Wait for 15 second to make sure the keep alive messages are working
	for(int i = 0; i < 15; i++)
	{
		if(spBeaconClientStartup->isBeaconAlive())
		{
			feLog("Beacon is alive\n");
		}
		else
		{
			feLog("Beacon is dead\n");
		}

		fe::milliSleep(1000);
	}
	feLog("Keep alive test finished\n");


	feLog("Testing global dictionary...\n");
	std::vector<std::pair<fe::String,fe::String>> setList;

	setList.push_back(std::make_pair(
			fe::String("name1"),fe::String("value1")));
	setList.push_back(std::make_pair(
			fe::String("name2"),fe::String("value2")));
	setList.push_back(std::make_pair(
			fe::String("name3"),fe::String("value3")));
	setList.push_back(std::make_pair(
			fe::String("name3"),fe::String("value3a")));

	spBeaconClientStartup->dictionarySet(setList);

	std::vector<std::pair<fe::String,fe::String>> getList;
	getList.push_back(std::make_pair(fe::String("name1"),    fe::String("")));
	getList.push_back(std::make_pair(fe::String("fake_name"),fe::String("")));
	getList.push_back(std::make_pair(fe::String("name3"),    fe::String("")));

	spBeaconClientStartup->dictionaryGet(getList);
	spBeaconClientStartup->dictionaryUnset(getList);

	std::vector<std::pair<fe::String,fe::String>> getList2;
	getList2.push_back(std::make_pair(fe::String("name1"), fe::String("")));
	getList2.push_back(std::make_pair(fe::String("name2"), fe::String("")));

	spBeaconClientStartup->dictionaryGet(getList2);

	spBeaconClientStartup->dictionaryClear();

	spBeaconClientStartup->dictionaryGet(getList2);

	spBeaconClientStartup->dictionarySet(setList);

	spBeaconClientStartup->shutdown();

	// Display time + delta time offset
	while(true)
	{
		int64_t now = spBeaconClientStartup->getBeaconTime();

		int64_t ms = now % 1000;
		now /= 1000;
		int64_t seconds = now % 60;
		now /= 60;
		int64_t minutes = now % 60;
		now /= 60;
		int64_t hours = now;

		std::cout << "h:" << hours
		<< " m:" << minutes
		<< " s:" << seconds
		<< " ms:" << ms
		<< "           \r";
	}

	return 0;
}
