/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecordRecorder_h__
#define __datatool_RecordRecorder_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Saves Record's op:input for every handle call that time advances

	@ingroup datatool

	As RecorderI, the pathname is the directory name and the integer limit
	sets the number of cycled files.
	The scalar for findNameFor() indicates the desired time to find a file for.
*//***************************************************************************/
class FE_DL_EXPORT RecordRecorder: virtual public HandlerI,
		virtual public RecorderI
{
	public:
				RecordRecorder(void):
					m_basename("test/trace"),
					m_index(0),
					m_ringSize(20)
				{	m_timeStamp.resize(m_ringSize); }

				//* as HandlerI
virtual void	handle(Record &record);

				//* as RecorderI
virtual	void	configure(String pathname,U32 limit);
virtual	String	findNameFor(Real scalar);

	private:
		String						m_basename;
		U32							m_index;
		U32							m_ringSize;
		Array<Real>					m_timeStamp;

		AsOperator					m_asOperator;
		AsTemporal					m_asTemporal;
		Accessor< sp<Component> >	m_aRecordRecorder;
};

inline void RecordRecorder::configure(String pathname,U32 limit)
{
	m_basename=pathname;
	m_ringSize=limit;
	m_timeStamp.resize(m_ringSize);
}

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecordRecorder_h__ */
