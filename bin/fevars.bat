@set CODEGEN=optimize
@set FE_ROOT=S:\fe\main
@set FE_LIB=S:\fe\main
@set FE_PATH=%FE_ROOT%\bin;%FE_LIB%\lib\x86_win32_%CODEGEN%;%FE_LIB%\lib\x86_win32_%CODEGEN%\fe
@set HASH_MAP_OVERRIDE=1
@set DevIL=C:\DevIL
@set ODE=C:\ode-0.8
@set OpenAL=C:\OpenAL 1.1 SDK
@set SDL=C:\SDL-1.2.13
@set XXINCLUDE=%BOOST_INCLUDE%;%ODE%\include;%DevIL%\include;%OpenAL%\include;%SDL%\include;%INCLUDE%
@set INCLUDE=%ODE%\include;%DevIL%\include;%OpenAL%\include;%SDL%\include;%INCLUDE%
@set xxlib=%BOOST_LIB%;%ODE%\lib\debugdll;%DevIL%\lib;%OpenAL%\libs;%SDL%\lib;%LIB%;%FE_LIB%\lib\x86_win32_%CODEGEN%
@set lib=%ODE%\lib\debugdll;%DevIL%\lib;%OpenAL%\libs;%SDL%\lib;%LIB%;%FE_LIB%\lib\x86_win32_%CODEGEN%
@set XXPath=%Path%;%PYTHON_ROOT%;%PYTHONPATH%;%BOOST_LIB%;%DevIL%\lib;%OpenAL%\dll;%SDL%\lib;%FE_PATH%
@set Path=%Path%;%PYTHON_ROOT%;%PYTHONPATH%;%DevIL%\lib;%OpenAL%\dll;%SDL%\lib;%FE_PATH%
@set PRETTY=1
@set PYTHON_ROOT=c:\python27
@set PYTHONPATH=%FE_PATH%
@set FE_NO_EXT=operator:raknet:sdl:terrain:audio:intelligence:openal:openil:raknet
@set FE_NO_UNIT=1

