/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconClientI_h__
#define __BeaconClientI_h__

namespace beacon
{

struct BeaconConfig
{
	char ipAddress[16];			/// Beacon IP address (xxx.xxx.xxx.xxx\0)
	uint16_t requestPort;		/// Beacon request port
	uint16_t gdRequestPort;		/// Global dictionary request port
};

/**************************************************************************//**
	@brief Client Node interface to Beacon

	@ingroup beacon
*//***************************************************************************/
class FE_DL_EXPORT BeaconClientI:
	virtual public fe::Component,
	public fe::CastableAs<BeaconClientI>
{
public:
	/// @brief	Registers node with the Beacon
	virtual bool registerWithBeacon(const BeaconConfig &config,
						bool timeSync) = 0;

	/// @brief	Shutdowns the thread monitoring the Beacon and
	///			sending keep alive messages
	virtual void shutdown() = 0;

	/// @brief	Returns the unique ID of this client instance
	virtual uint8_t getID() = 0;

	/// @brief	Returns the time offset between the clock on the Beacon
	///			and the local processor
	virtual int64_t getTimeOffset() = 0;

	/// @brief	Returns the current time plus the offset to give you
	///			the Beacon time
	virtual int64_t getBeaconTime() = 0;

	/// @brief	Returns the estimated latency between the server and the client
	virtual int64_t getLatency() = 0;

	/// @brief	Returns true if the list has of nodes has changed
	virtual bool isListUpdated() = 0;

	/// @brief	Fills in the list of nodes registered with the Beacon
	virtual void getList(std::list<Node> &registeredNode) = 0;

	/// @brief	Returns true if the Beacon is alive
	///			(received a response from the Beacon within the timeout period)
	virtual bool isBeaconAlive() = 0;

	/// Global Dictionary access functions.
	/// The Global Dictionary is a name/values strings list

	/// @brief	Gets the value from the Global Dictionary given a list of names
	virtual bool dictionaryGet(
				std::vector<std::pair<fe::String,fe::String>> &list) = 0;

	/// @brief	Sets name/values in the Global Dictionary
	virtual bool dictionarySet(
				const std::vector<std::pair<fe::String,fe::String>> &list) = 0;

	/// @brief	Unsets name/value pairs in the Global Dictionary
	///			given a list of names
	virtual bool dictionaryUnset(
				const std::vector<std::pair<fe::String,fe::String>> &list) = 0;

	/// @brief	Clears the Global Dictionary
	virtual bool dictionaryClear() = 0;

	/// @brief	Gets the all matching pairs in the Global Dictionary
	///			using the regex search string
	virtual bool dictionaryGetRegex(const fe::String searchString,
				std::vector<std::pair<fe::String,fe::String>> &list) = 0;

	/// @brief	Get the last update counter received for the Global Dictionary
	virtual uint64_t dictionaryGetUpdateCounter() = 0;
};

} // namespace beacon

#endif // __BeaconClientI_h__
