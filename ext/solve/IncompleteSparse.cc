/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <solve/solve.pmh>

namespace fe
{
namespace ext
{

IncompleteSparse::IncompleteSparse(void)
{
}

IncompleteSparse::~IncompleteSparse(void)
{
}

bool IncompleteSparse::solve(std::vector<t_solve_v3> &a_x, sp< UpperTriangularBCRS<t_solve_real> > a_A, const std::vector<t_solve_v3> &a_b)
{
	a_A->incompleteSqrt();

	a_A->backSolve(a_x, a_b);

	return true;
}

} /* namespace ext */
} /* namespace fe */

