/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#define	FE_SAOC_BIND_DEBUG			FALSE
#define	FE_SAOC_BUFFER_CHECK		(FE_CODEGEN<=FE_DEBUG)

#define	FE_SAOC_CHECK_LIMIT			FALSE
#define	FE_SAOC_CHECK_TYPECHANGE	FALSE

namespace fe
{
namespace ext
{

SurfaceAccessorOpenCL::SurfaceAccessorOpenCL(void):
	m_pBridgeConst(NULL),
	m_pPositionBridgeConst(NULL),
	m_pVertStartBridgeConst(NULL),
	m_pVertCountBridgeConst(NULL),
	m_pVertPointBridgeConst(NULL),
	m_pBridge(NULL),
	m_pPositionBridge(NULL),
	m_pVertStartBridge(NULL),
	m_pVertCountBridge(NULL),
	m_pVertPointBridge(NULL)
{
	setName("SurfaceAccessorOpenCL");
}

SurfaceAccessorOpenCL::~SurfaceAccessorOpenCL(void)
{
}

void SurfaceAccessorOpenCL::setAttrType(String a_attrType)
{
#if FE_SAOC_CHECK_TYPECHANGE
	if(m_pBridge && m_attrType==a_attrType)
	{
		return;
	}
	if(!m_attrType.empty())
	{
		feLog("SurfaceAccessorOpenCL::setAttrType"
				" changing type from \"%s\" to \"%s\"\n",
				m_attrType.c_str(),a_attrType.c_str());
	}
#else
	if(m_pBridge && !m_attrType.empty())
	{
		return;
	}
#endif

	m_attrType=a_attrType;

	if(!writable())
	{
		return;
	}

	if(!m_pBridge)
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				m_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isNull())
		{
			return;
		}
		cp<SurfaceAccessibleOpenCL::Bridge>& rcpBridge=
				spSurfaceAccessibleOpenCL->lookupBridge(m_bridgeName,TRUE);
		m_pBridge=rcpBridge.writable();
		m_pBridgeConst=rcpBridge.raw();
	}
	FEASSERT(m_pBridge);

	m_pBridge->m_type=a_attrType;
	m_pBridge->m_rate=SurfaceAccessibleBase::elementLayout(m_element);

	if(m_attrType=="integer")
	{
		m_pBridge->m_sizeof=sizeof(I32);
	}
	else if(m_attrType=="real")
	{
		m_pBridge->m_sizeof=sizeof(Real);
	}
	else if(m_attrType=="vector3")
	{
		m_pBridge->m_sizeof=4*sizeof(Real);
	}
}

U32 SurfaceAccessorOpenCL::count(void) const
{
	if(m_spMultiGroup.isValid())
	{
		return m_spMultiGroup->count();
	}

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		return m_pBridgeConst->m_groupSet.size();
	}

	if(!m_pBridgeConst)
	{
		return m_spSurfaceAccessibleI->count(m_element);
	}

//~	m_pBridge->refresh();

	return m_pBridgeConst->m_elementCount;
}

U32 SurfaceAccessorOpenCL::subCount(U32 a_index) const
{
	if(m_element==SurfaceAccessibleI::e_point ||
			m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup ||
			m_element==SurfaceAccessibleI::e_detail)
	{
		return 1;
	}

	if(!m_pVertCountBridgeConst)
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				m_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isNull())
		{
			return 0;
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertCountBridge=
				spSurfaceAccessibleOpenCL->lookupBridge(
				":vertCount",TRUE);
		if(rcpVertCountBridge.isNull())
		{
			return 0;
		}

		const_cast<SurfaceAccessorOpenCL*>(this)->m_pVertCountBridgeConst=
				rcpVertCountBridge.raw();
	}

	if(I32(a_index)>=m_pVertCountBridgeConst->m_elementCount)
	{
		return 0;
	}

//	feLog("SurfaceAccessorOpenCL::subCount(%d) current %d %d clMem %d %p\n",
//			a_index,
//			m_pVertCountBridgeConst->m_cpuCurrent,
//			m_pVertCountBridgeConst->m_gpuCurrent,
//			m_pVertCountBridgeConst->m_spClMem.isValid(),
//			m_pVertCountBridgeConst->m_spClMem.isValid()?
//			m_pVertCountBridgeConst->m_spClMem->m_clMem: nullptr);

//~	m_pVertCountBridge->refresh();

//	feLog("SurfaceAccessorOpenCL::subCount buffer %p\n",
//			m_pVertCountBridgeConst->m_bufferData);

	const I32* subCountArray=(I32*)(m_pVertCountBridgeConst->m_bufferData);
	FEASSERT(subCountArray);

	return subCountArray[a_index];
}

void SurfaceAccessorOpenCL::ensureBufferSize(
	SurfaceAccessibleOpenCL::Bridge* a_pBridge,I32 a_minSize,I32 a_sizeof)
{
//	feLog("SurfaceAccessorOpenCL::ensureBufferSize \"%s\" \"%s\" \"%s\""
//			" %d vs %d allocated %d sizeof %d buffer %p\n",
//			a_pBridge->m_name.c_str(),
//			m_attrType.c_str(),m_attrName.c_str(),
//			a_minSize,a_pBridge->m_elementCount,a_pBridge->m_allocated,
//			a_sizeof,a_pBridge->m_bufferData);

	a_pBridge->refresh();

	a_pBridge->m_gpuCurrent=FALSE;

	if(!a_pBridge->m_sizeof)
	{
		a_pBridge->m_sizeof=a_sizeof;
	}

#if FALSE
	// size change
	if(a_pBridge->m_sizeof!=a_sizeof || a_sizeof<1)
	{
		feLog("SurfaceAccessorOpenCL::ensureBufferSize \"%s\" \"%s\" \"%s\""
				" %d vs %d allocated %d sizeof %d vs %d\n",
				a_pBridge->m_name.c_str(),
				m_attrType.c_str(),m_attrName.c_str(),
				a_minSize,a_pBridge->m_elementCount,a_pBridge->m_allocated,
				a_sizeof,a_pBridge->m_sizeof);
	}
#endif

#if FE_SAOC_BUFFER_CHECK
	if(!a_pBridge->m_bufferData && a_pBridge->m_allocated)
	{
		feLog("SurfaceAccessorOpenCL::ensureBufferSize"
				" allocated %d with NULL buffer    ERROR\n",
				a_pBridge->m_allocated);
	}
#endif

	const I32 size=a_pBridge->m_elementCount;
	const I32 allocated=a_pBridge->m_allocated;
	if(a_minSize<=size && a_minSize<=allocated)
	{
#if FE_SAOC_BUFFER_CHECK
		if(!a_pBridge->m_bufferData)
		{
			feLog("SurfaceAccessorOpenCL::ensureBufferSize"
					" no change with NULL buffer    ERROR\n");
		}
#endif

		return;
	}

	if(a_minSize>allocated && a_sizeof>0)
	{
		const I32 minAllocSize=fe::maximum(a_minSize,size);

		//* tweak
		const I32 addCount=
				fe::maximum(I32(minAllocSize-allocated),I32(0.2*allocated));

		const I32 oldBytes=a_pBridge->m_allocated*a_sizeof;
		a_pBridge->m_allocated+=addCount;
		const I32 newBytes=a_pBridge->m_allocated*a_sizeof;

#if FALSE
		feLog("SurfaceAccessorOpenCL::ensureBufferSize"
				" \"%s\" reallocate %d*%d for minCount %d\n",
				a_pBridge->m_name.c_str(),
				a_pBridge->m_allocated,a_sizeof,a_minSize);
#endif

		a_pBridge->m_bufferData=
				reallocate(a_pBridge->m_bufferData,newBytes);

#if FE_SAOC_BUFFER_CHECK
		if(!a_pBridge->m_bufferData)
		{
			feLog("SurfaceAccessorOpenCL::ensureBufferSize"
					" reallocate %d->%d got NULL buffer    ERROR\n",
					oldBytes,newBytes);
		}
#endif

		char* byteBuffer=(char*)(a_pBridge->m_bufferData);

		memset(byteBuffer+oldBytes,0,newBytes-oldBytes);
	}

	if(size<a_minSize)
	{
		a_pBridge->m_elementCount=a_minSize;
	}

#if FE_SAOC_BUFFER_CHECK
	if(!a_pBridge->m_bufferData && a_pBridge->m_sizeof)
	{
		feLog("SurfaceAccessorOpenCL::ensureBufferSize"
				" returning with NULL buffer    ERROR\n");
	}
#endif

//	FEASSERT(a_sizeof<1 || a_pBridge->m_elementCount<=a_pBridge->m_allocated);
}

void SurfaceAccessorOpenCL::set(U32 a_index,U32 a_subIndex,String a_string)
{
	setAttrType("string");

#if FE_SAOC_CHECK_LIMIT
	if(I32(a_index)>=count())
	{
		return;
	}
#endif

	if(!m_pBridge)
	{
		feLog("SurfaceAccessorOpenCL::set(U32,U32,String)"
				" no writable Bridge for \"%s\"\n",m_attrName.c_str());
		return;
	}

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
		{
			return;
		}
		ensureBufferSize(m_pBridge,(a_index+1)*FE_OPENCL_BUFFER_MAX_VERTS,
				sizeof(String));

		String* stringArray=(String*)(m_pBridge->m_bufferData);

		stringArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex]=a_string;
	}
	else
	{
		ensureBufferSize(m_pBridge,a_index+1,sizeof(String));

		String* stringArray=(String*)(m_pBridge->m_bufferData);

		stringArray[a_index]=a_string;
	}
}

String SurfaceAccessorOpenCL::string(U32 a_index,U32 a_subIndex)
{
	setAttrType("string");

	if(!m_pBridgeConst)
	{
		feLog("SurfaceAccessorOpenCL::string"
				" no readable Bridge for \"%s\"\n",m_attrName.c_str());
		return "";
	}

//~	m_pBridge->refresh();

	if(I32(a_index)>=m_pBridgeConst->m_elementCount)
	{
		return "";
	}

	const String* stringArray=(String*)(m_pBridgeConst->m_bufferData);

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
		{
			return "";
		}
		return stringArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex];
	}
	else
	{
		return stringArray[a_index];
	}
}

void SurfaceAccessorOpenCL::set(U32 a_index,U32 a_subIndex,I32 a_integer)
{
	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		if(!m_pVertStartBridge || !m_pVertCountBridge || !m_pVertPointBridge)
		{
			sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
					m_spSurfaceAccessibleI);
			if(spSurfaceAccessibleOpenCL.isNull())
			{
				return;
			}

			cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertStartBridge=
					spSurfaceAccessibleOpenCL->lookupBridge(
					":vertStart",TRUE);
			if(rcpVertStartBridge.isNull())
			{
				return;
			}

			cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertCountBridge=
					spSurfaceAccessibleOpenCL->lookupBridge(
					":vertCount",TRUE);
			if(rcpVertCountBridge.isNull())
			{
				return;
			}

			cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertPointBridge=
					spSurfaceAccessibleOpenCL->lookupBridge(
					":vertPoint",TRUE);
			if(rcpVertPointBridge.isNull())
			{
				return;
			}

			m_pVertStartBridge=rcpVertStartBridge.writable();
			m_pVertCountBridge=rcpVertCountBridge.writable();
			m_pVertPointBridge=rcpVertPointBridge.writable();

			m_pVertStartBridgeConst=rcpVertStartBridge.raw();
			m_pVertCountBridgeConst=rcpVertCountBridge.raw();
			m_pVertPointBridgeConst=rcpVertPointBridge.raw();
		}

		ensureBufferSize(m_pVertStartBridge,a_index+1,sizeof(I32));

		I32* subStartArray=(I32*)(m_pVertStartBridge->m_bufferData);

		ensureBufferSize(m_pVertCountBridge,a_index+1,sizeof(I32));

		I32* subCountArray=(I32*)(m_pVertCountBridge->m_bufferData);

		//* TODO what is insertAt if substart and subcount are zero?

		I32& rVertCount=subCountArray[a_index];
		if(I32(a_subIndex)>=rVertCount)
		{
			const I32 insertAt=subStartArray[a_index]+rVertCount;
			const I32 insertCount=a_subIndex+1-rVertCount;

//			if(a_index<16)
//			feLog("    prim %d start %d sub %d/%d insert %d at %d for %d\n",
//					a_index,subStartArray[a_index],a_subIndex,rVertCount,
//					insertCount,insertAt,a_integer);

			ensureBufferSize(m_pVertPointBridge,
					m_pVertPointBridge->m_elementCount+insertCount,
					sizeof(I32));

			I32* vertexPointArray=(I32*)(m_pVertPointBridge->m_bufferData);

			const I32 insertTo=insertAt+insertCount;
			for(I32 vertexIndex=m_pVertPointBridge->m_elementCount-1;
					vertexIndex>=insertTo;vertexIndex--)
			{
				vertexPointArray[vertexIndex]=vertexPointArray[vertexIndex-1];
			}
			for(I32 vertexIndex=insertAt;vertexIndex<insertTo;vertexIndex++)
			{
				vertexPointArray[vertexIndex]= -1;
			}

			const I32 primitiveCount=m_pVertStartBridge->m_elementCount;
			for(I32 primitiveIndex=a_index+1;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				subStartArray[primitiveIndex]+=insertCount;
			}

			rVertCount+=insertCount;
		}

		I32* vertexPointArray=(I32*)(m_pVertPointBridge->m_bufferData);

		const I32 vertexIndex=subStartArray[a_index]+a_subIndex;
		FEASSERT(vertexIndex<m_pVertPointBridge->m_elementCount);

		vertexPointArray[vertexIndex]=a_integer;

		return;
	}

	setAttrType("integer");

	if(!m_pBridge)
	{
		feLog("SurfaceAccessorOpenCL::set(U32,U32,I32)"
				" no writable Bridge for \"%s\"\n",m_attrName.c_str());
		return;
	}

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
		{
			return;
		}

		ensureBufferSize(m_pBridge,
				(a_index+1)*FE_OPENCL_BUFFER_MAX_VERTS,sizeof(I32));

		I32* integerArray=(I32*)(m_pBridge->m_bufferData);

		integerArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex]=a_integer;
	}
	else
	{
		ensureBufferSize(m_pBridge,a_index+1,sizeof(I32));

		I32* integerArray=(I32*)(m_pBridge->m_bufferData);

		integerArray[a_index]=a_integer;
	}
}

I32 SurfaceAccessorOpenCL::integer(U32 a_index,U32 a_subIndex)
{
	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		FEASSERT(m_element==
				SurfaceAccessibleI::e_primitiveGroup
				|| m_element==
				SurfaceAccessibleI::e_primitive);
//~		if(a_subIndex==SurfaceAccessibleI::e_openCurve)
//~		{
//~			//* TODO support non-curves
//~			return 1;
//~		}
		return 0;
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		if(!m_pVertStartBridgeConst || !m_pVertPointBridgeConst)
		{
			sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
					m_spSurfaceAccessibleI);
			if(spSurfaceAccessibleOpenCL.isNull())
			{
				return 0;
			}

			cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertStartBridge=
					spSurfaceAccessibleOpenCL->lookupBridge(
					":vertStart",FALSE);
			if(rcpVertStartBridge.isNull())
			{
				return 0;
			}

			cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertPointBridge=
					spSurfaceAccessibleOpenCL->lookupBridge(
					":vertPoint",FALSE);
			if(rcpVertPointBridge.isNull())
			{
				return 0;
			}

			m_pVertStartBridgeConst=rcpVertStartBridge.raw();
			m_pVertPointBridgeConst=rcpVertPointBridge.raw();
		}

		if(I32(a_index)>=m_pVertStartBridgeConst->m_elementCount)
		{
			return 0;
		}

//~		m_pVertStartBridge->refresh();

		const I32* subStartArray=(I32*)(m_pVertStartBridgeConst->m_bufferData);

		const I32 vertexIndex=subStartArray[a_index]+a_subIndex;

		if(vertexIndex>=m_pVertPointBridgeConst->m_elementCount)
		{
			return 0;
		}

//~		m_pVertPointBridge->refresh();

		const I32* vertexPointArray=
				(I32*)(m_pVertPointBridgeConst->m_bufferData);

//		if(a_index<16)
//		feLog("prim %d sub %d lookup %d\n",
//				a_index,a_subIndex,vertexPointArray[vertexIndex]);

		return vertexPointArray[vertexIndex];
	}

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		if(m_spMultiGroup.isValid())
		{
			return (a_index<m_spMultiGroup->count())?
					m_spMultiGroup->at(a_index): -1;
		}
		return -1;
	}

	setAttrType("integer");

	if(!m_pBridgeConst)
	{
		feLog("SurfaceAccessorOpenCL::integer"
				" no readable Bridge for \"%s\"\n",m_attrName.c_str());
		return 0;
	}

//~	m_pBridge->refresh();

	if(I32(a_index)>=m_pBridgeConst->m_elementCount)
	{
		return 0;
	}

	const I32* integerArray=(I32*)(m_pBridgeConst->m_bufferData);

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
		{
			return 0;
		}
		return integerArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex];
	}
	else
	{
		return integerArray[a_index];
	}
}

//* coordinate with SurfaceAccessorBase::append(I32)
I32 SurfaceAccessorOpenCL::append(I32 a_integer)
{
//	feLog("SurfaceAccessorOpenCL::append(%d) '%s'\n",
//			a_integer,m_attrName.c_str());

	//* lot of points or primitives
	if(m_element==SurfaceAccessibleI::e_point ||
			m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup ||
			(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute!=SurfaceAccessibleI::e_vertices))
	{
		if(a_integer<1)
		{
			return -1;
		}

		FEASSERT(writable());

		if(!m_pBridge)
		{
			sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
					m_spSurfaceAccessibleI);
			if(spSurfaceAccessibleOpenCL.isNull())
			{
				return -1;
			}

			cp<SurfaceAccessibleOpenCL::Bridge>& rcpBridge=
					spSurfaceAccessibleOpenCL->lookupBridge(m_bridgeName,TRUE);
			m_pBridge=rcpBridge.writable();
			m_pBridgeConst=rcpBridge.raw();
			FEASSERT(m_pBridge);

			m_pBridge->m_rate=
					SurfaceAccessibleBase::elementLayout(m_element);
		}
		FEASSERT(m_pBridge);

		if(m_element==SurfaceAccessibleI::e_pointGroup ||
				m_element==SurfaceAccessibleI::e_primitiveGroup)
		{
			if(m_spMultiGroup.isNull())
			{
				return -1;
			}

			m_spMultiGroup->insert(a_integer);
			m_pBridge->m_groupSet.insert(a_integer);

			return 1;
		}

		const I32 first=count();

		switch(m_element)
		{
			case SurfaceAccessibleI::e_point:
				ensureBufferSize(m_pBridge,
						m_pBridge->m_elementCount+a_integer,
						m_pBridge->m_sizeof);
				break;
			case SurfaceAccessibleI::e_vertex:
				break;
			case SurfaceAccessibleI::e_primitive:
				ensureBufferSize(m_pBridge,
						m_pBridge->m_elementCount+a_integer,
						m_pBridge->m_sizeof);
				break;
			case SurfaceAccessibleI::e_detail:
				break;
			default:
				return -1;
		}

//		feLog("SurfaceAccessorOpenCL::append(%d) '%s' to %d\n",
//				a_integer,m_attrName.c_str(),first);

		return first;
	}

	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		append(U32(0),a_integer);
		return 0;
	}

	//* append a primitive with the specified number of vertices
	sp<SurfaceAccessorI> spPointAccessor=m_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);

	const I32 primitiveIndex=append();
	for(I32 vertIndex=0;vertIndex<a_integer;vertIndex++)
	{
		const I32 pointIndex=spPointAccessor->append();
		append(primitiveIndex,pointIndex);
	}

	return primitiveIndex;
}

I32 SurfaceAccessorOpenCL::append(void)
{
	//* point or primitive

	const U32 next=count();

//	feLog("SurfaceAccessorOpenCL::append() '%s' to %d\n",
//			m_attrName.c_str(),next);

	FEASSERT(writable());

	if(!m_pBridge)
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				m_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isNull())
		{
			return -1;
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpBridge=
				spSurfaceAccessibleOpenCL->lookupBridge(m_bridgeName,TRUE);
		m_pBridge=rcpBridge.writable();
		m_pBridgeConst=rcpBridge.raw();
		FEASSERT(m_pBridge);

		m_pBridge->m_rate=
				SurfaceAccessibleBase::elementLayout(m_element);
	}

	if(!m_pBridge)
	{
		feLog("SurfaceAccessorOpenCL::append(void)"
				" no writable Bridge for \"%s\"\n",m_attrName.c_str());
		return -1;
	}

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			ensureBufferSize(m_pBridge,m_pBridge->m_elementCount+1,
					m_pBridge->m_sizeof);
			break;
		case SurfaceAccessibleI::e_vertex:
			break;
		case SurfaceAccessibleI::e_primitive:
			ensureBufferSize(m_pBridge,m_pBridge->m_elementCount+1,
					m_pBridge->m_sizeof);
			break;
		case SurfaceAccessibleI::e_detail:
			break;
		default:
			break;
	}

	return next;
}

I32 SurfaceAccessorOpenCL::append(SurfaceAccessibleI::Form a_form)
{
	//* TODO
	return -1;
}

void SurfaceAccessorOpenCL::append(U32 a_index,I32 a_integer)
{
	//* vertex of primitive
	const I32 vertIndex=subCount(a_index);
	set(a_index,vertIndex,a_integer);
}

//* coordinate with SurfaceAccessorBase::append(a_rPrimVerts)
void SurfaceAccessorOpenCL::append(
	Array< Array<I32> >& a_rPrimVerts)
{
	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		return;
	}

	const U32 appendPrimitiveCount=a_rPrimVerts.size();

//	feLog("SurfaceAccessorOpenCL::append '%s' primVerts %d\n",
//			m_attrName.c_str(),appendPrimitiveCount);

	if(!m_pVertStartBridge || !m_pVertCountBridge || !m_pVertPointBridge)
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				m_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isNull())
		{
			return;
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertStartBridge=
				spSurfaceAccessibleOpenCL->lookupBridge(
				":vertStart",TRUE);
		if(rcpVertStartBridge.isNull())
		{
			return;
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertCountBridge=
				spSurfaceAccessibleOpenCL->lookupBridge(
				":vertCount",TRUE);
		if(rcpVertCountBridge.isNull())
		{
			return;
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertPointBridge=
				spSurfaceAccessibleOpenCL->lookupBridge(
				":vertPoint",TRUE);
		if(rcpVertPointBridge.isNull())
		{
			return;
		}

		m_pVertStartBridge=rcpVertStartBridge.writable();
		m_pVertCountBridge=rcpVertCountBridge.writable();
		m_pVertPointBridge=rcpVertPointBridge.writable();

		m_pVertStartBridgeConst=rcpVertStartBridge.raw();
		m_pVertCountBridgeConst=rcpVertCountBridge.raw();
		m_pVertPointBridgeConst=rcpVertPointBridge.raw();
	}

	m_pVertPointBridge->refresh();

	I32 appendVertexCount=0;
	for(U32 appendPrimitiveIndex=0;appendPrimitiveIndex<appendPrimitiveCount;
			appendPrimitiveIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[appendPrimitiveIndex];
		const U32 subCount=rVertList.size();
		appendVertexCount+=subCount;
	}

	const I32 priorPrimitiveCount=m_pVertStartBridge->m_elementCount;
	const I32 priorVertexCount=m_pVertPointBridge->m_elementCount;

	const I32 newPrimitiveCount=priorPrimitiveCount+appendPrimitiveCount;
	const I32 newVertexCount=priorVertexCount+appendVertexCount;

	ensureBufferSize(m_pVertStartBridge,newPrimitiveCount,sizeof(I32));
	ensureBufferSize(m_pVertCountBridge,newPrimitiveCount,sizeof(I32));
	ensureBufferSize(m_pVertPointBridge,newVertexCount,sizeof(I32));

	I32* subStartArray=(I32*)(m_pVertStartBridge->m_bufferData);
	I32* subCountArray=(I32*)(m_pVertCountBridge->m_bufferData);
	I32* vertexPointArray=(I32*)(m_pVertPointBridge->m_bufferData);

	I32 primitiveIndex=priorPrimitiveCount;
	I32 vertexIndex=priorVertexCount;

	for(U32 appendPrimitiveIndex=0;appendPrimitiveIndex<appendPrimitiveCount;
			appendPrimitiveIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[appendPrimitiveIndex];
		const U32 subCount=rVertList.size();

		subStartArray[primitiveIndex]=vertexIndex;
		subCountArray[primitiveIndex]=subCount;

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=rVertList[subIndex];

//			if(primitiveIndex<8)
//			{
//				feLog("  primVert %d/%d %d/%d prim %d append %d\n",
//						appendPrimitiveIndex,appendPrimitiveCount,
//						subIndex,subCount,
//						primitiveIndex,pointIndex);
//			}

			FEASSERT(vertexIndex<newVertexCount);

			vertexPointArray[vertexIndex++]=pointIndex;
		}
		primitiveIndex++;
	}
}

void SurfaceAccessorOpenCL::set(U32 a_index,U32 a_subIndex,Real a_real)
{
	setAttrType("real");

	if(!m_pBridge)
	{
		feLog("SurfaceAccessorOpenCL::set(U32,U32,Real)"
				" no writable Bridge for \"%s\"\n",m_attrName.c_str());
		return;
	}

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
		{
			return;
		}

		ensureBufferSize(m_pBridge,
				(a_index+1)*FE_OPENCL_BUFFER_MAX_VERTS,sizeof(Real));

		Real* realArray=(Real*)(m_pBridge->m_bufferData);

		realArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex]=a_real;
	}
	else
	{
		ensureBufferSize(m_pBridge,a_index+1,sizeof(Real));

		Real* realArray=(Real*)(m_pBridge->m_bufferData);

		realArray[a_index]=a_real;
	}
}

Real SurfaceAccessorOpenCL::real(U32 a_index,U32 a_subIndex)
{
	setAttrType("real");

	if(!m_pBridgeConst)
	{
		feLog("SurfaceAccessorOpenCL::real"
				" no readable Bridge for \"%s\"\n",m_attrName.c_str());
		return 0.0;
	}

//~	m_pBridge->refresh();

	if(I32(a_index)>=m_pBridgeConst->m_elementCount)
	{
		return 0.0;
	}

	const Real* realArray=(Real*)(m_pBridgeConst->m_bufferData);

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
		{
			return 0;
		}
		return realArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex];
	}
	else
	{
		return realArray[a_index];
	}
}

void SurfaceAccessorOpenCL::set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_spatialVector)
{
//	feLog("SurfaceAccessorOpenCL::set %d %d vector %s\n",
//			a_index,a_subIndex,c_print(a_spatialVector));

	const BWORD isGroup=(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup);

	const BWORD isIndirect=(isGroup ||
			m_attribute==SurfaceAccessibleI::e_vertices);

	if(!isIndirect)
	{
		setAttrType("vector3");
	}

	if(!m_pBridge && !isGroup)
	{
		feLog("SurfaceAccessorOpenCL::set spatialVector"
				" no writable Bridge for \"%s\"\n",m_attrName.c_str());
		return;
	}

	if(!isIndirect)
	{
		if(m_element==SurfaceAccessibleI::e_vertex)
		{
			if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
			{
				return;
			}

			ensureBufferSize(m_pBridge,
					(a_index+1)*FE_OPENCL_BUFFER_MAX_VERTS,4*sizeof(Real));

			Vector4* vectorArray=(Vector4*)(m_pBridge->m_bufferData);

			fe::set(vectorArray[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex],
					a_spatialVector[0],a_spatialVector[1],a_spatialVector[2]);
		}
		else
		{
			ensureBufferSize(m_pBridge,a_index+1,4*sizeof(Real));

			Vector4* vectorArray=(Vector4*)(m_pBridge->m_bufferData);

			fe::set(vectorArray[a_index],
					a_spatialVector[0],a_spatialVector[1],a_spatialVector[2]);
		}

		return;
	}

	const I32 pointIndex=pointIndexForVertex(a_index,a_subIndex);

	if(!m_pPositionBridge)
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				m_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isNull())
		{
			feLog("SurfaceAccessorOpenCL::set spatialVector"
					" not SurfaceAccessibleOpenCL\n");
			return;
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpPositionBridge=
				spSurfaceAccessibleOpenCL->lookupBridge("pt:P",TRUE);
		if(rcpPositionBridge.isNull())
		{
			feLog("SurfaceAccessorOpenCL::set spatialVector no Bridge data\n");
			return;
		}

		m_pPositionBridge=rcpPositionBridge.writable();
		m_pPositionBridgeConst=rcpPositionBridge.raw();
	}

	ensureBufferSize(m_pPositionBridge,pointIndex+1,4*sizeof(Real));

	Vector4* vectorArray=(Vector4*)(m_pPositionBridge->m_bufferData);
	fe::set(vectorArray[pointIndex],
			a_spatialVector[0],a_spatialVector[1],a_spatialVector[2]);
}

SpatialVector SurfaceAccessorOpenCL::spatialVector(
	U32 a_index,U32 a_subIndex)
{
	const BWORD isGroup=(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup);
	const BWORD isIndirect=(isGroup ||
			m_attribute==SurfaceAccessibleI::e_vertices);

	if(!isIndirect)
	{
		setAttrType("vector3");
	}

	if(!m_pBridgeConst && !isGroup)
	{
		feLog("SurfaceAccessorOpenCL::spatialVector"
				" no readable Bridge for \"%s\"\n",m_attrName.c_str());
		return SpatialVector(0.0,0.0,0.0);
	}

//~	m_pBridge->refresh();

#if FE_SAOC_CHECK_LIMIT
	if(a_index>=m_pBridgeConst->m_elementCount && !isGroup)
	{
		return SpatialVector(0.0,0.0,0.0);
	}
#endif

	if(!isIndirect)
	{
		if(m_element==SurfaceAccessibleI::e_vertex)
		{
			if(a_subIndex>=FE_OPENCL_BUFFER_MAX_VERTS)
			{
				return SpatialVector(0.0,0.0,0.0);
			}

			const Vector4& rVector4=
					((Vector4*)(m_pBridgeConst->m_bufferData))
					[a_index*FE_OPENCL_BUFFER_MAX_VERTS+a_subIndex];

			return SpatialVector(rVector4[0],rVector4[1],rVector4[2]);
		}
		else
		{
			const Vector4& rVector4=
					((Vector4*)(m_pBridgeConst->m_bufferData))[a_index];

			return SpatialVector(rVector4[0],rVector4[1],rVector4[2]);
		}
	}

	const I32 pointIndex=pointIndexForVertex(a_index,a_subIndex);

	if(!m_pPositionBridgeConst)
	{
		sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
				m_spSurfaceAccessibleI);
		if(spSurfaceAccessibleOpenCL.isNull())
		{
			feLog("SurfaceAccessorOpenCL::spatialVector"
					" not SurfaceAccessibleOpenCL\n");
			return SpatialVector(0.0,0.0,0.0);
		}

		cp<SurfaceAccessibleOpenCL::Bridge>& rcpPositionBridge=
				spSurfaceAccessibleOpenCL->lookupBridge("pt:P",FALSE);
		if(rcpPositionBridge.isNull())
		{
			feLog("SurfaceAccessorOpenCL::spatialVector no bridge data\n");
			return SpatialVector(0.0,0.0,0.0);
		}
		m_pPositionBridgeConst=rcpPositionBridge.raw();
	}

//~	m_pPositionBridge->refresh();

	const Vector4& rVector4=
			((Vector4*)(m_pPositionBridgeConst->m_bufferData))[pointIndex];

//	if(a_index<8)
//	feLog("VERTEX '%s' vector %d %d pt %d  %s\n",m_attrName.c_str(),
//			a_index,a_subIndex,pointIndex,c_print(vectorArray[pointIndex]));

	return SpatialVector(rVector4[0],rVector4[1],rVector4[2]);
}

I32 SurfaceAccessorOpenCL::pointIndexForVertex(U32 a_index,U32 a_subIndex)
{

	if(m_element!=SurfaceAccessibleI::e_primitiveGroup)
	{
		return integer(a_index,a_subIndex);
	}

	m_element=SurfaceAccessibleI::e_primitive;
	m_attribute=SurfaceAccessibleI::e_vertices;

	const I32 pointIndex=integer(a_index,a_subIndex);

	m_element=SurfaceAccessibleI::e_primitiveGroup;
	m_attribute=SurfaceAccessibleI::e_generic;

	return pointIndex;
}

BWORD SurfaceAccessorOpenCL::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
	m_element=a_element;
	m_attrName=a_name;

	FEASSERT(a_element==SurfaceAccessibleI::e_point || a_name!="P");

#if FE_SAOC_BIND_DEBUG
	feLog("SurfaceAccessorOpenCL::bindInternal \"%s\" \"%s\"\n",
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			a_name.c_str());
#endif

	sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
			m_spSurfaceAccessibleI);
	if(spSurfaceAccessibleOpenCL.isNull())
	{
#if FE_SAOC_BIND_DEBUG
		feLog("SurfaceAccessorOpenCL::bindInternal"
				" no SurfaceAccessibleOpenCL\n");
#endif
		return FALSE;
	}

//	const char firstChar=m_attrName.c_str()[0];
//	if((firstChar>=0 && firstChar<=9) || m_attrName.contains(" "))

	if(m_element==SurfaceAccessibleI::e_pointGroup ||
			m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		if(!m_attrName.empty())
		{
			sp<SurfaceAccessibleBase> spAccessible=m_spSurfaceAccessibleI;
			if(spAccessible.isValid())
			{
				m_spMultiGroup=spAccessible->group(m_element,m_attrName);
			}
		}
		else if(m_element==SurfaceAccessibleI::e_pointGroup)
		{
			m_element=SurfaceAccessibleI::e_point;
			m_attribute=SurfaceAccessibleI::e_position;
			m_attrName="P";
		}
		else
		{
			m_element=SurfaceAccessibleI::e_primitive;
			m_attribute=SurfaceAccessibleI::e_vertices;
			m_attrName="vertStart";
		}
	}

	const String requestedRate=SurfaceAccessibleBase::elementLayout(m_element);

	m_bridgeName=(m_attribute==SurfaceAccessibleI::e_vertices)?
			String(":vertStart"): requestedRate+":"+m_attrName;

	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		return TRUE;
	}

	if(m_spMultiGroup.isValid())
	{
		return TRUE;
	}

	cp<SurfaceAccessibleOpenCL::Bridge>& rcpBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(m_bridgeName,FALSE);
	if(rcpBridge.isNull())
	{
#if FE_SAOC_BIND_DEBUG
		feLog("SurfaceAccessorOpenCL::bindInternal missing \"%s\"\n",
				m_bridgeName.c_str());
#endif
		return FALSE;
	}

	m_attrType=rcpBridge->m_type;

	if(!rcpBridge->m_cpuCurrent && !rcpBridge->m_gpuCurrent)
	{
#if FE_SAOC_BIND_DEBUG
		feLog("SurfaceAccessorOpenCL::bindInternal"
				" neither side of bridge is current\n");
#endif
		return FALSE;
	}

	if(writable())
	{
		m_pBridge=rcpBridge.writable();
	}
	else if(rcpBridge->m_gpuCurrent && !rcpBridge->m_cpuCurrent)
	{
		rcpBridge.sp<SurfaceAccessibleOpenCL::Bridge>::raw()->refresh();
	}
	m_pBridgeConst=rcpBridge.raw();

	if(m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		if(m_pBridge)
		{
			m_pBridge->refresh();
		}

		const String existingRate=rcpBridge->m_rate;
		if(existingRate!=requestedRate)
		{
#if FE_SAOC_BIND_DEBUG
			feLog("SurfaceAccessorOpenCL::bindInternal"
					" \"%s\" \"%s\" does not match existing rate \"%s\"\n",
					requestedRate.c_str(),m_attrName.c_str(),
					existingRate.c_str());
#endif
			return FALSE;
		}
	}

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
