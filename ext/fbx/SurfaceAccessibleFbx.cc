/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <fbx/fbx.pmh>

#define FE_SFB_DEBUG		FALSE
#define FE_SFB_VERBOSE		FALSE
#define FE_SFB_MESH_VERBOSE	FALSE
#define FE_SFB_LOG_MAX		8

namespace fe
{
namespace ext
{

SurfaceAccessibleFbx::SurfaceAccessibleFbx(void)
{
}

SurfaceAccessibleFbx::~SurfaceAccessibleFbx(void)
{
}

SurfaceAccessibleFbx::FileContext::FileContext(void)
{
	//* The first thing to do is to create the FBX Manager which is
	//* the object allocator for almost all the classes in the SDK
	m_pFbxManager=FbxManager::Create();
	if(m_pFbxManager)
	{
		//* Create an IOSettings object.
		//* This object holds all import/export settings.
		FbxIOSettings* pFbxIOSettings=
				FbxIOSettings::Create(m_pFbxManager,IOSROOT);
		m_pFbxManager->SetIOSettings(pFbxIOSettings);

		//* Load plugins from the executable directory (optional)
		FbxString path=FbxGetApplicationDirectory();
		m_pFbxManager->LoadPluginsDirectory(path.Buffer());

		//* Create an FBX scene.
		//* This object holds most objects imported/exported from/to files.
		m_pFbxScene=FbxScene::Create(m_pFbxManager,"My Scene");
	}
}

SurfaceAccessibleFbx::FileContext::~FileContext(void)
{
	m_pFbxScene=NULL;

	//* Delete the FBX Manager.
	//* All the objects that have been allocated using the FBX Manager and that
	//* haven't been explicitly destroyed are also automatically destroyed.
	if(m_pFbxManager)
	{
		m_pFbxManager->Destroy();
		m_pFbxManager=NULL;
	}
}

BWORD SurfaceAccessibleFbx::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SFB_DEBUG
	feLog("SurfaceAccessibleFbx::load file \"%s\"\n",a_filename.c_str());
#endif

	if(a_spSettings.isValid())
	{
		m_frame=a_spSettings->catalog<Real>("frame");
		m_spFilter=a_spSettings->catalog< sp<Component> >("filter");

		const String options=a_spSettings->catalog<String>("options");

#if FE_SFB_DEBUG
		feLog("SurfaceAccessibleFbx::load options \"%s\"\n",options.c_str());
#endif

		if(!options.empty())
		{
			setOptions(options);
		}
	}
	else
	{
		m_frame=0.0;
		m_spFilter=NULL;
	}

	m_fps=24.0;
	if(m_optionMap.find("fps")!=m_optionMap.end())
	{
		m_fps=atof(m_optionMap["fps"].c_str());
	}

	m_subset="skeleton";
	if(m_optionMap.find("mesh")!=m_optionMap.end())
	{
		m_subset="mesh";
	}

	m_ytoz=FALSE;
	if(m_optionMap.find("ytoz")!=m_optionMap.end())
	{
		m_ytoz=TRUE;
	}

	m_no_uvs=FALSE;
	if(m_optionMap.find("no_uvs")!=m_optionMap.end())
	{
		m_no_uvs=TRUE;
	}

	if(m_filename!=a_filename)
	{
		m_spFileContext=NULL;
		m_filename=a_filename;
	}

	if(m_spFileContext.isValid() &&
			(!m_spFileContext->m_pFbxManager || !m_spFileContext->m_pFbxScene))
	{
		m_spFileContext=NULL;
	}

	if(m_spFileContext.isNull())
	{
		setContext(a_filename);

		if(m_spFileContext.isNull())
		{
#if FE_SFB_DEBUG
			feLog("SurfaceAccessibleFbx::load NULL FileContext\n");
#endif

			return FALSE;
		}
	}

	FbxScene* pFbxScene=
			m_spFileContext.isValid()? m_spFileContext->m_pFbxScene: NULL;

	if(pFbxScene)
	{
		startJoints();
		readTransformations(pFbxScene->GetRootNode());
		completeJoints();
	}

	return (pFbxScene != NULL);
};

BWORD SurfaceAccessibleFbx::setContext(String a_filename)
{
	sp<Catalog> spMasterCatalog=registry()->master()->catalog();
	const String key="SurfaceAccessibleFbx:"+a_filename;

	m_spFileContext=spMasterCatalog->catalog< hp<Component> >(key);

	if(m_spFileContext.isValid())
	{
		return TRUE;
	}

#if FE_SFB_DEBUG
	feLog("SurfaceAccessibleFbx::setContext"
			" creating new FileContext\n");
#endif

	m_spFileContext=new FileContext();
	spMasterCatalog->catalog< hp<Component> >(key)=m_spFileContext;

	if(m_spFileContext.isNull())
	{
		feLog("SurfaceAccessibleFbx::setContext"
				" unable to create FileContext\n");
		return FALSE;
	}

	FbxManager* pFbxManager=m_spFileContext->m_pFbxManager;
	if(!pFbxManager)
	{
		feLog("SurfaceAccessibleFbx::setContext"
				" unable to create FbxManager\n");
		m_spFileContext=NULL;
		return FALSE;
	}

#if FE_SFB_DEBUG
	feLog("SurfaceAccessibleFbx::setContext"
			" Autodesk FBX SDK version %s\n",
			pFbxManager->GetVersion());
#endif

	if(!m_spFileContext->m_pFbxScene)
	{
		feLog("SurfaceAccessibleFbx::setContext"
				" unable to create FbxScene\n");
		m_spFileContext=NULL;
		return FALSE;
	}

	if(!loadScene(a_filename.c_str()))
	{
		feLog("SurfaceAccessibleFbx::setContext"
				" failed to load \"%s\"\n",a_filename.c_str());

		m_spFileContext=NULL;
		return FALSE;
	}

	return TRUE;
}

BWORD SurfaceAccessibleFbx::loadScene(const char* a_pFilename)
{
#if FE_SFB_DEBUG
	feLog("SurfaceAccessibleFbx::loadScene\n");
#endif

	//* Get the file version number generate by the FBX SDK.
	I32 sdkMajor=0;
	I32 sdkMinor=0;
	I32 sdkRevision=0;
	FbxManager::GetFileFormatVersion(sdkMajor,sdkMinor,sdkRevision);

#if FE_SFB_DEBUG
	feLog("  FBX format %d.%d.%d for SDK\n",
			sdkMajor,sdkMinor,sdkRevision);
#endif

	//* Create an importer.
	FbxImporter* pFbxImporter=
			FbxImporter::Create(m_spFileContext->m_pFbxManager,"");

	//* Initialize the importer by providing a filename.
	const BWORD importStatus=pFbxImporter->Initialize(
			a_pFilename,-1,m_spFileContext->m_pFbxManager->GetIOSettings());

	I32 fileMajor=0;
	I32 fileMinor=0;
	I32 fileRevision=0;
	pFbxImporter->GetFileVersion(fileMajor,fileMinor,fileRevision);

#if FE_SFB_DEBUG
	feLog("  FBX format %d.%d.%d for \"%s\"\n",
			fileMajor,fileMinor,fileRevision,a_pFilename);
#endif

	if(!importStatus)
	{
		FbxString errorString=pFbxImporter->GetStatus().GetErrorString();
		feLog("SurfaceAccessibleFbx::loadScene"
				"FbxImporter::Initialize() failed: \"%s\"\n",
				errorString.Buffer());

		if (pFbxImporter->GetStatus().GetCode() ==
				FbxStatus::eInvalidFileVersion)
		{
			feLog("  invalid file version %d.%d.%d vs SDK version %d.%d.%d\n",
					fileMajor,fileMinor,fileRevision,
					sdkMajor,sdkMinor,sdkRevision);
		}

		return FALSE;
	}

#if FE_SFB_DEBUG
	if (pFbxImporter->IsFBX())
	{
		//* From this point, it is possible to access animation stack
		//* information without the expense of loading the entire file.

		const I32 animStackCount=pFbxImporter->GetAnimStackCount();

		feLog("  current animation stack: \"%s\"\n",
				pFbxImporter->GetActiveAnimStackName().Buffer());

		for(I32 animStackIndex=0;animStackIndex<animStackCount;animStackIndex++)
		{
			FbxTakeInfo* pFbxTakeInfo=pFbxImporter->GetTakeInfo(animStackIndex);

			feLog("    stack %d/%d \"%s\"\n",
					animStackIndex,animStackCount,pFbxTakeInfo->mName.Buffer());
			feLog("      description \"%s\"\n",
					pFbxTakeInfo->mDescription.Buffer());

			//* Change the value of the import name if the animation stack
			//* should be imported under a different name.
			//* Set the value of the import state to false if the animation
			//* stack should be not be imported.
			feLog("      import \"%s\" state '%s'\n",
					pFbxTakeInfo->mImportName.Buffer(),
					pFbxTakeInfo->mSelect? "true": "false");

			FbxTimeSpan fbxTimeSpan=pFbxTakeInfo->mLocalTimeSpan;
			FbxTime fbxTimeStart=fbxTimeSpan.GetStart();
			FbxTime fbxTimeStop=fbxTimeSpan.GetStop();
			const I32 msStart=fbxTimeStart.GetMilliSeconds();
			const I32 msStop=fbxTimeStop.GetMilliSeconds();

			const Real framesPerMs=(0.001*m_fps);
			const Real frameStart=msStart*framesPerMs;
			const Real frameStop=msStop*framesPerMs;

			feLog("      span %dms to %dms (frame %.6G to %.6G)\n",
					msStart,msStop,frameStart,frameStop);
		}

		//* Set the import states. By default, the import states are always
		//* set to true. The code below shows how to change these states.
//      IOS_REF.SetBoolProp(IMP_FBX_MATERIAL,        true);
//      IOS_REF.SetBoolProp(IMP_FBX_TEXTURE,         true);
//      IOS_REF.SetBoolProp(IMP_FBX_LINK,            true);
//      IOS_REF.SetBoolProp(IMP_FBX_SHAPE,           true);
//      IOS_REF.SetBoolProp(IMP_FBX_GOBO,            true);
//      IOS_REF.SetBoolProp(IMP_FBX_ANIMATION,       true);
//      IOS_REF.SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);
	}
#endif

	FbxScene* pFbxScene=m_spFileContext->m_pFbxScene;

	// Import the scene.
	BWORD success=pFbxImporter->Import(pFbxScene);

	if(!success &&
			pFbxImporter->GetStatus().GetCode()==FbxStatus::ePasswordError)
	{
		feLog("  FBX is password protected\n");
	}

#if FE_SFB_DEBUG
	const I32 poseCount=pFbxScene->GetPoseCount();
	feLog("  poseCount %d\n",poseCount);
	for(I32 poseIndex=0;poseIndex<poseCount;poseIndex++)
	{
		FbxPose* pFbxPose=pFbxScene->GetPose(poseIndex);

		const String poseName=pFbxPose? pFbxPose->GetName(): "<NULL>";
		feLog("    pose %d/%d \"%s\"\n",poseIndex,poseCount,poseName.c_str());
	}
#endif

	// Destroy the importer.
	pFbxImporter->Destroy();

	return success;
}

void SurfaceAccessibleFbx::readTransformations(FbxNode* a_pFbxNode)
{
	const String name=a_pFbxNode->GetName();

	FbxNode* pParentNode=a_pFbxNode->GetParent();
	const String parentName=pParentNode? pParentNode->GetName(): "";

	FbxNodeAttribute::EType nodeAttributeType=
			(a_pFbxNode->GetNodeAttribute()==NULL)?
			FbxNodeAttribute::eNull:
			a_pFbxNode->GetNodeAttribute()->GetAttributeType();

#if FE_SFB_VERBOSE
	feLog("\nnode \"%s\" parent \"%s\" type %d\n",
			name.c_str(),parentName.c_str(),nodeAttributeType);
	switch(nodeAttributeType)
	{
		case FbxNodeAttribute::eUnknown:
			feLog("  type eUnknown\n");
			break;
		case FbxNodeAttribute::eNull:
			feLog("  type eNull\n");
			break;
		case FbxNodeAttribute::eSkeleton:
			feLog("  type eSkeleton\n");
			break;
		case FbxNodeAttribute::eMarker:
			feLog("  type eMarker\n");
			break;
		case FbxNodeAttribute::eMesh:
			feLog("  type eMesh\n");
			break;
		case FbxNodeAttribute::eNurbs:
			feLog("  type eNurbs\n");
			break;
		case FbxNodeAttribute::ePatch:
			feLog("  type ePatch\n");
			break;
		case FbxNodeAttribute::eCamera:
			feLog("  type eCamera\n");
			break;
		case FbxNodeAttribute::eCameraStereo:
			feLog("  type eCameraStereo\n");
			break;
		case FbxNodeAttribute::eCameraSwitcher:
			feLog("  type eCameraSwitcher\n");
			break;
		case FbxNodeAttribute::eLight:
			feLog("  type eLight\n");
			break;
		case FbxNodeAttribute::eOpticalReference:
			feLog("  type eOpticalReference\n");
			break;
		case FbxNodeAttribute::eOpticalMarker:
			feLog("  type eOpticalMarker\n");
			break;
		case FbxNodeAttribute::eNurbsCurve:
			feLog("  type   eNurbsCurve\n");
			break;
		case FbxNodeAttribute::eTrimNurbsSurface:
			feLog("  type eTrimNurbsSurface\n");
			break;
		case FbxNodeAttribute::eBoundary:
			feLog("  type eBoundary\n");
			break;
		case FbxNodeAttribute::eNurbsSurface:
			feLog("  type eNurbsSurface\n");
			break;
		case FbxNodeAttribute::eShape:
			feLog("  type eShape\n");
			break;
		case FbxNodeAttribute::eLODGroup:
			feLog("  type eLODGroup\n");
			break;
		case FbxNodeAttribute::eSubDiv:
			feLog("  type eSubDiv\n");
			break;
		case FbxNodeAttribute::eCachedEffect:
			feLog("  type eCachedEffect\n");
			break;
		case FbxNodeAttribute::eLine:
			feLog("  type eLine\n");
			break;
	}
#endif

	const Real msPerFrame=(1000.0/m_fps);
	FbxTime fbxTime=FBXSDK_TIME_INFINITE;
	fbxTime.SetMilliSeconds(m_frame*msPerFrame);

	FbxAMatrix matrixGlobal=a_pFbxNode->EvaluateGlobalTransform(fbxTime);
	FbxAMatrix matrixLocal=a_pFbxNode->EvaluateLocalTransform(fbxTime);

	SpatialTransform xformWorld;
	for(U32 m=0;m<4;m++)
	{
		const FbxVector4 row=matrixGlobal.GetRow(m);
		set(xformWorld.column(m),row[0],row[1],row[2]);
	}

#if FE_SFB_VERBOSE
	feLog("world:\n%s\n",c_print(xformWorld));
#endif

	SpatialTransform refWorld;
	setIdentity(refWorld);	//* TODO

	SpatialTransform xformRelative;
	for(U32 m=0;m<4;m++)
	{
		const FbxVector4 row=matrixLocal.GetRow(m);
		set(xformRelative.column(m),row[0],row[1],row[2]);
	}

#if FALSE
	//* HACK normalize scale
	normalizeSafe(xformRelative.column(0));
	normalizeSafe(xformRelative.column(1));
	normalizeSafe(xformRelative.column(2));
#endif

#if FE_SFB_VERBOSE
	feLog("local:\n%s\n",c_print(xformRelative));
#endif

//	xformRelative.translation()*=0.03;	//* HACK

	if((nodeAttributeType==FbxNodeAttribute::eNull ||
			nodeAttributeType==FbxNodeAttribute::eSkeleton) &&
			m_subset=="skeleton")
	{
		Color color(1.0,1.0,0.0);

		if(nodeAttributeType==FbxNodeAttribute::eSkeleton)
		{
			FbxSkeleton* pFbxSkeleton=
					(FbxSkeleton*)a_pFbxNode->GetNodeAttribute();
			const FbxColor& rFbxColor=pFbxSkeleton->GetLimbNodeColor();

//			set(color,rFbxColor[0],rFbxColor[1],rFbxColor[2]);

#if FE_SFB_VERBOSE
			if (pFbxSkeleton->GetSkeletonType() == FbxSkeleton::eLimb)
			{
				feLog("  limb length %.6G\n",pFbxSkeleton->LimbLength.Get());
			}
			else if (pFbxSkeleton->GetSkeletonType() == FbxSkeleton::eLimbNode)
			{
				feLog("  limb node size %.6G\n",pFbxSkeleton->Size.Get());
			}
			else if (pFbxSkeleton->GetSkeletonType() == FbxSkeleton::eRoot)
			{
				feLog("  limb root size %.6G\n",pFbxSkeleton->Size.Get());
			}
			else if (pFbxSkeleton->GetSkeletonType() == FbxSkeleton::eEffector)
			{
				feLog("  effector\n");
			}

			feLog("  color %s\n",c_print(color));
#endif
		}

		const Color localScale(1.0,1.0,1.0);	//* TODO

		setJoint(name,parentName,xformRelative,localScale,
				refWorld,color);
	}
	else if(nodeAttributeType==FbxNodeAttribute::eMesh && m_subset=="mesh")
	{
		FbxMesh* pFbxMesh=(FbxMesh*)a_pFbxNode->GetNodeAttribute();

		//* HACK filter
//		if(!name.match(".*Field.*"))

		addMesh(pFbxMesh,xformWorld);
	}

#if FALSE
	FbxProperty property=a_pFbxNode->GetFirstProperty();
	while(property.IsValid())
	{
		feLog("property \"%s\"\n",property.GetName().Buffer());

		property=a_pFbxNode->GetNextProperty(property);
	}
#endif

	const I32 childCount=a_pFbxNode->GetChildCount();
	for(I32 childIndex=0;childIndex<childCount;childIndex++)
	{
		readTransformations(a_pFbxNode->GetChild(childIndex));
	}
}

void SurfaceAccessibleFbx::addMesh(FbxMesh* a_pFbxMesh,
	SpatialTransform& a_rTransform)
{
	m_spOutputUV=accessor(e_point,e_uv,e_createMissing);

	//* TODO multiple color, normal, uv attributes at point rate

	Array< sp<SurfaceAccessorI> > outputVertexColorChannels;
	Array< sp<SurfaceAccessorI> > outputVertexNormalChannels;
	Array< sp<SurfaceAccessorI> > outputVertexUVChannels;
	sp<SurfaceAccessorI> spOutputVertexUV=
			accessor(e_vertex,e_uv,e_createMissing);

	sp<SurfaceAccessorI> spOutputPart=
			accessor(e_primitive,"part",e_createMissing);
	const String part=a_pFbxMesh->GetName();

	const I32 pointCount=a_pFbxMesh->GetControlPointsCount();
	const I32 polygonCount=a_pFbxMesh->GetPolygonCount();
	const I32 primitiveStart=spOutputPart->count();

#if FE_SFB_DEBUG
	feLog("SurfaceAccessibleFbx::addMesh"
			" pointCount %d polygonCount %d part \"%s\"\n",
			pointCount,polygonCount,part.c_str());
#endif

	FbxVector4* controlPoints=a_pFbxMesh->GetControlPoints();

	const I32 pointStart=m_spOutputPoint->append(pointCount);

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const FbxVector4& rPoint=controlPoints[pointIndex];
		SpatialVector local;

		if(m_ytoz)
		{
			set(local,rPoint[2],rPoint[0],rPoint[1]);

			//* HACK
//			set(local,rPoint[0],-rPoint[2],rPoint[1]);
		}
		else
		{
			set(local,rPoint[0],rPoint[1],rPoint[2]);
		}

		SpatialVector transformed;
		transformVector(a_rTransform,local,transformed);

#if FE_SFB_MESH_VERBOSE
		if(pointIndex<FE_SFB_LOG_MAX)
		{
			feLog("control point %d/%d %s\n",
					pointIndex,pointCount,c_print(transformed));
		}
#endif

		m_spOutputPoint->set(pointStart+pointIndex,transformed);
	}

	Array< Array<I32> > primVerts;
	primVerts.resize(polygonCount);

	for(I32 polygonIndex=0;polygonIndex<polygonCount;polygonIndex++)
	{
#if FE_SFB_MESH_VERBOSE
		const BWORD verbose=(polygonIndex<FE_SFB_LOG_MAX);
		if(verbose)
		{
			feLog("polygon %d/%d\n",polygonIndex,polygonCount);
		}
#endif

		const I32 subCount=a_pFbxMesh->GetPolygonSize(polygonIndex);

		Array<I32>& rVerts=primVerts[polygonIndex];
		rVerts.resize(subCount);

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					a_pFbxMesh->GetPolygonVertex(polygonIndex, subIndex);
			rVerts[subCount-1-subIndex]=pointStart+pointIndex;

			if(pointIndex<0)
			{
				feLog("  sub %d/%d pointIndex %d invalid\n",
						subIndex,subCount,pointIndex);
				continue;
			}
			else
			{
#if FE_SFB_MESH_VERBOSE
				if(verbose)
				{
					const FbxVector4& rPoint=controlPoints[pointIndex];
					feLog("  sub %d/%d pointIndex %d at %.6G %.6G %.6G\n",
							subIndex,subCount,pointIndex,
							rPoint[0],rPoint[1],rPoint[2]);
				}
#endif
			}
		}
	}

	m_spOutputVertices->append(primVerts);

	const I32 layerCount=a_pFbxMesh->GetLayerCount();

#if FE_SFB_MESH_VERBOSE
	const I32 uvLayerCount=a_pFbxMesh->GetUVLayerCount();
	for(I32 uvLayerIndex=0;uvLayerIndex<uvLayerCount;uvLayerIndex++)
	{
		FbxArray<FbxLayerElement::EType> layerTypes=
				a_pFbxMesh->GetAllChannelUV(uvLayerIndex);
		const I32 layerTypeCount=layerTypes.Size();
		feLog("uvLayer %d/%d size %d\n",
				uvLayerIndex,uvLayerCount,layerTypeCount);
		for(I32 layerTypeIndex=0;layerTypeIndex<layerTypeCount;layerTypeIndex++)
		{
			feLog("  layerType %d/%d %d\n",
					layerTypeIndex,layerTypeIndex,layerTypes[layerTypeIndex]);
		}
	}

	for(I32 layerIndex=0;layerIndex<layerCount;layerIndex++)
	{
		feLog("layer %d/%d\n",layerIndex,layerCount);

		FbxLayer* pLayer=a_pFbxMesh->GetLayer(layerIndex);

		FbxLayerElementUV* pLayerElementUV=pLayer->GetUVs();
		FbxArray<const FbxLayerElementUV*> layerElementUVs=pLayer->GetUVSets();

		const I32 uvCount=layerElementUVs.Size();
		for(I32 uvIndex=0;uvIndex<uvCount;uvIndex++)
		{
			const FbxLayerElementUV* pLayerElementUV=layerElementUVs[uvIndex];

			feLog("  layer UV %d/%d \"%s\"\n",
					uvIndex,uvCount,pLayerElementUV->GetName());
		}
	}
#endif

	FbxStringList uvNames;
	a_pFbxMesh->GetUVSetNames(uvNames);
	const I32 uvNameCount=uvNames.GetCount();

	Array< FbxArray<FbxVector2> > uvArrays;
	if(!m_no_uvs)
	{
		uvArrays.resize(uvNameCount);
//		Array< FbxArray<int> > uvUnmappedArrays(uvNameCount);
		for(I32 uvNameIndex=0;uvNameIndex<uvNameCount;uvNameIndex++)
		{
			a_pFbxMesh->GetPolygonVertexUVs(uvNames[uvNameIndex].Buffer(),
					uvArrays[uvNameIndex]
//					,uvUnmappedArrays[uvNameIndex]
					);

#if FE_SFB_MESH_VERBOSE
			feLog("uvName %d/%d \"%s\" size %d\n",
					uvNameIndex,uvNameCount,uvNames[uvNameIndex].Buffer(),
					uvArrays[uvNameIndex].Size());
#endif
		}
	}

	I32 vertexId=0;
	for(I32 polygonIndex=0;polygonIndex<polygonCount;polygonIndex++)
	{
		const I32 primitiveIndex=primitiveStart+polygonIndex;
#if FE_SFB_MESH_VERBOSE
		const BWORD verbose=(polygonIndex<FE_SFB_LOG_MAX);
		if(verbose)
		{
			const I32 polygonGroup=a_pFbxMesh->GetPolygonGroup(polygonIndex);

			feLog("polygon %d/%d group %d\n",
					polygonIndex,polygonCount,polygonGroup);
		}
#endif

		spOutputPart->set(primitiveIndex,part);

		const I32 groupCount=a_pFbxMesh->GetElementPolygonGroupCount();
		for(I32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			FbxGeometryElementPolygonGroup* pPolygonGroup=
					a_pFbxMesh->GetElementPolygonGroup(groupIndex);

			switch (pPolygonGroup->GetMappingMode())
			{
				case FbxGeometryElement::eByPolygon:
					if(pPolygonGroup->GetReferenceMode() ==
							FbxGeometryElement::eIndex)
					{
						//* TODO
#if FE_SFB_MESH_VERBOSE
						if(verbose)
						{
							const I32 polyGroupId=
									pPolygonGroup->GetIndexArray().GetAt(
									polygonIndex);
							feLog("  group %d/%d %d\n",
									groupIndex,groupCount,polyGroupId);
						}
#endif
						break;
					}
				default:
					feLog("  unsupported group assignment\n");
					break;
			}
		}

		const I32 subCount=a_pFbxMesh->GetPolygonSize(polygonIndex);

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					a_pFbxMesh->GetPolygonVertex(polygonIndex, subIndex);

#if FE_SFB_MESH_VERBOSE
			if(verbose)
			{
				feLog("  sub %d/%d pointIndex %d\n",
						subIndex,subCount,pointIndex);
			}
#endif

			const I32 colorCount=a_pFbxMesh->GetElementVertexColorCount();
			const I32 colorArrayCount=outputVertexColorChannels.size();
			if(colorArrayCount<colorCount)
			{
				outputVertexColorChannels.resize(colorCount);
				for(I32 colorIndex=colorArrayCount;colorIndex<colorCount;
						colorIndex++)
				{
					if(colorIndex==0)
					{
						outputVertexColorChannels[colorIndex]=
								accessor(e_vertex,e_color,e_createMissing);
					}
					else
					{
						String attrName;
						attrName.sPrintf("Cd%d",colorIndex);
						outputVertexColorChannels[colorIndex]=
								accessor(e_vertex,attrName,e_createMissing);
					}
				}
			}
			for(I32 colorIndex=0;colorIndex<colorCount;colorIndex++)
			{
				sp<SurfaceAccessorI>& rspOutputVertexColorChannel=
						outputVertexColorChannels[colorIndex];

				FbxGeometryElementVertexColor* pElementVertexColor=
						a_pFbxMesh->GetElementVertexColor(colorIndex);

				switch (pElementVertexColor->GetMappingMode())
				{
					case FbxGeometryElement::eByControlPoint:
						switch (pElementVertexColor->GetReferenceMode())
						{
							case FbxGeometryElement::eDirect:
							{
								const FbxColor& rColor=pElementVertexColor
										->GetDirectArray().GetAt(pointIndex);
								if(colorIndex==0 &&
											m_spOutputColor.isValid())
								{
									m_spOutputColor->set(
											pointStart+pointIndex,
											SpatialVector(rColor[0],
											rColor[1],rColor[2]));
								}
#if FE_SFB_MESH_VERBOSE
								if(verbose)
								{
									feLog("    color %d/%d CP"
											" direct %.6G %.6G %.6G\n",
											colorIndex,colorCount,
											rColor[0],rColor[1],rColor[2]);
								}
#endif
								break;
							}
							case FbxGeometryElement::eIndexToDirect:
							{
								const I32 id=pElementVertexColor
										->GetIndexArray().GetAt(pointIndex);
								const FbxColor& rColor=pElementVertexColor
										->GetDirectArray().GetAt(id);
								if(colorIndex==0 &&
											m_spOutputColor.isValid())
								{
									m_spOutputColor->set(
											pointStart+pointIndex,
											SpatialVector(rColor[0],
											rColor[1],rColor[2]));
								}
#if FE_SFB_MESH_VERBOSE
								if(verbose)
								{
									feLog("    color %d/%d CP indexed %d"
											" of %.6G %.6G %.6G\n",
											colorIndex,colorCount,id,
											rColor[0],rColor[1],rColor[2]);
								}
#endif
								break;
							}
							default:
								// other reference modes not shown here!
								break;
						}
						break;

					case FbxGeometryElement::eByPolygonVertex:
						{
							switch (pElementVertexColor->GetReferenceMode())
							{
								case FbxGeometryElement::eDirect:
								{
									const FbxColor& rColor=pElementVertexColor
											->GetDirectArray().GetAt(vertexId);
									if(rspOutputVertexColorChannel.isValid())
									{
										rspOutputVertexColorChannel->set(
												primitiveIndex,subIndex,
												SpatialVector(rColor[0],
												rColor[1],rColor[2]));
									}
#if FE_SFB_MESH_VERBOSE
									if(verbose)
									{
										feLog("    color %d/%d PV"
												" direct %.6G %.6G %.6G\n",
												colorIndex,colorCount,
												rColor[0],rColor[1],rColor[2]);
									}
#endif
									break;
								}
								case FbxGeometryElement::eIndexToDirect:
								{
									const I32 id=pElementVertexColor
											->GetIndexArray()
											.GetAt(vertexId);
									const FbxColor& rColor=
											pElementVertexColor
											->GetDirectArray().GetAt(id);
									if(rspOutputVertexColorChannel.isValid())
									{
										rspOutputVertexColorChannel->set(
												primitiveIndex,subIndex,
												SpatialVector(rColor[0],
												rColor[1],rColor[2]));
									}
#if FE_SFB_MESH_VERBOSE
									if(verbose)
									{
										feLog("    color %d/%d PV indexed %d"
												" of %.6G %.6G %.6G\n",
												colorIndex,colorCount,id,
												rColor[0],rColor[1],rColor[2]);
									}
#endif
									break;
								}
								default:
									// other reference modes not shown here!
									break;
							}
						}
						break;

					case FbxGeometryElement::eByPolygon:
							// doesn't make much sense for UVs
					case FbxGeometryElement::eAllSame:
							// doesn't make much sense for UVs
					case FbxGeometryElement::eNone:
						// doesn't make much sense for UVs
					default:
						break;
				}
			}

			const I32 uvCount=m_no_uvs? 0: a_pFbxMesh->GetElementUVCount();
			const I32 uvArrayCount=outputVertexUVChannels.size();
			if(uvArrayCount<uvCount)
			{
				outputVertexUVChannels.resize(uvCount);
				for(I32 uvIndex=uvArrayCount;uvIndex<uvCount;uvIndex++)
				{
					String attrName;
					if(uvIndex<uvNameCount)
					{
						attrName=uvNames[uvIndex].Buffer();
					}
					else
					{
						attrName.sPrintf("uv%d",uvIndex);
					}
					outputVertexUVChannels[uvIndex]=
							accessor(e_vertex,attrName,e_createMissing);
				}
			}
			for(I32 uvIndex=0;uvIndex<uvCount;uvIndex++)
			{
				sp<SurfaceAccessorI>& rspOutputVertexUVChannel=
						outputVertexUVChannels[uvIndex];

				FbxGeometryElementUV* pUV=a_pFbxMesh->GetElementUV(uvIndex);

				switch (pUV->GetMappingMode())
				{
					case FbxGeometryElement::eByControlPoint:
						switch (pUV->GetReferenceMode())
						{
							case FbxGeometryElement::eDirect:
							{
								const FbxVector2& rUV=pUV
										->GetDirectArray().GetAt(pointIndex);
								if(uvIndex==0 &&
										m_spOutputUV.isValid())
								{
									m_spOutputUV->set(
											pointStart+pointIndex,
											SpatialVector(rUV[0],rUV[1],0.0));
								}
#if FE_SFB_MESH_VERBOSE
								if(verbose)
								{
									feLog("    uv %d/%d CP direct %.6G %.6G\n",
											uvIndex,uvCount,rUV[0],rUV[1]);
								}
#endif
								break;
							}
							case FbxGeometryElement::eIndexToDirect:
							{
								const I32 id=pUV
										->GetIndexArray().GetAt(pointIndex);
								const FbxVector2& rUV=pUV
										->GetDirectArray().GetAt(id);
								if(uvIndex==0 &&
										m_spOutputUV.isValid())
								{
									m_spOutputUV->set(
											pointStart+pointIndex,
											SpatialVector(rUV[0],rUV[1],0.0));
								}
#if FE_SFB_MESH_VERBOSE
								if(verbose)
								{
									feLog("    uv %d/%d CP indexed %d"
											" of %.6G %.6G\n",
											uvIndex,uvCount,id,rUV[0],rUV[1]);
								}
#endif
								break;
							}
							default:
								// other reference modes not shown here!
								break;
						}
						break;

					case FbxGeometryElement::eByPolygonVertex:
						{
#if FALSE
							FbxLayerElement::EType layerType=
									FbxLayerElement::eTextureDiffuse;
							const I32 textureUVIndex=
										a_pFbxMesh->GetTextureUVIndex(
										polygonIndex,subIndex,layerType);
#else
							const I32 textureUVIndex= -1;
#endif
							if(textureUVIndex<0)
							{
								FbxVector2& rUv=uvArrays[uvIndex][vertexId];
								if(rspOutputVertexUVChannel.isValid())
								{
									rspOutputVertexUVChannel->set(
											primitiveIndex,subIndex,
											SpatialVector(
											rUv[0],rUv[1],0.0));
									if(fabs(rUv[0]>1e-6) ||
											fabs(rUv[0]>1e-6))
									{
										spOutputVertexUV->set(
												primitiveIndex,subIndex,
												SpatialVector(
												rUv[0],rUv[1],0.0));
									}
								}
#if FE_SFB_MESH_VERBOSE
								if(verbose)
								{
									feLog("    rUv %d/%d PV entry %d"
											" of %.6G %.6G\n",
											uvIndex,uvCount,textureUVIndex,
											rUv[0],rUv[1]);
								}
#endif
								break;
							}
							switch (pUV->GetReferenceMode())
							{
								case FbxGeometryElement::eDirect:
								case FbxGeometryElement::eIndexToDirect:
								{
									const FbxVector2& rUV=pUV
											->GetDirectArray().GetAt(
											textureUVIndex);
									if(rspOutputVertexUVChannel.isValid())
									{
										rspOutputVertexUVChannel->set(
												primitiveIndex,subIndex,
												SpatialVector(
												rUV[0],rUV[1],0.0));
										if(fabs(rUV[0]>1e-6) ||
												fabs(rUV[0]>1e-6))
										{
											spOutputVertexUV->set(
													primitiveIndex,subIndex,
													SpatialVector(
													rUV[0],rUV[1],0.0));
										}
									}
#if FE_SFB_MESH_VERBOSE
									if(verbose)
									{
										feLog("    uv %d/%d PV indexed %d"
												" of %.6G %.6G\n",
												uvIndex,uvCount,textureUVIndex,
												rUV[0],rUV[1]);
									}
#endif
									break;
								}
								default:
									// other reference modes not shown here!
									break;
							}
						}
						break;

					//* NOTE don't make much sense for UVs
					case FbxGeometryElement::eByPolygon:
					case FbxGeometryElement::eAllSame:
					case FbxGeometryElement::eNone:

					default:
						break;
				}
			}

			const I32 normalCount=a_pFbxMesh->GetElementNormalCount();
			const I32 normalArrayCount=outputVertexNormalChannels.size();
			if(normalArrayCount<normalCount)
			{
				outputVertexNormalChannels.resize(normalCount);
				for(I32 normalIndex=normalArrayCount;normalIndex<normalCount;
						normalIndex++)
				{
					if(normalIndex==0)
					{
						outputVertexNormalChannels[normalIndex]=
								accessor(e_vertex,e_normal,e_createMissing);
					}
					else
					{
						String attrName;
						attrName.sPrintf("N%d",normalIndex);
						outputVertexNormalChannels[normalIndex]=
								accessor(e_vertex,attrName,e_createMissing);
					}
				}
			}
			for(I32 normalIndex=0;normalIndex<normalCount;normalIndex++)
			{
				sp<SurfaceAccessorI>& rspOutputVertexNormalChannel=
						outputVertexNormalChannels[normalIndex];

				FbxGeometryElementNormal* pElementNormal=
						a_pFbxMesh->GetElementNormal(normalIndex);

				if(pElementNormal->GetMappingMode() ==
						FbxGeometryElement::eByPolygonVertex)
				{
					switch (pElementNormal->GetReferenceMode())
					{
						case FbxGeometryElement::eDirect:
						{
							const FbxVector4& rNormal=pElementNormal
									->GetDirectArray().GetAt(vertexId);
							if(rspOutputVertexNormalChannel.isValid())
							{
								rspOutputVertexNormalChannel->set(
										primitiveIndex,subIndex,
										SpatialVector(rNormal[0],
										rNormal[1],rNormal[2]));
							}
#if FE_SFB_MESH_VERBOSE
							if(verbose)
							{
								feLog("    normal %d/%d PV"
										" direct %.6G %.6G %.6G\n",
										normalIndex,normalCount,
										rNormal[0],rNormal[1],rNormal[2]);
							}
#endif
							break;
						}
						case FbxGeometryElement::eIndexToDirect:
						{
							const I32 id=pElementNormal
									->GetIndexArray().GetAt(vertexId);
							const FbxVector4& rNormal=pElementNormal
									->GetDirectArray().GetAt(id);
							if(rspOutputVertexNormalChannel.isValid())
							{
								rspOutputVertexNormalChannel->set(
										primitiveIndex,subIndex,
										SpatialVector(rNormal[0],
										rNormal[1],rNormal[2]));
							}
#if FE_SFB_MESH_VERBOSE
							if(verbose)
							{
								feLog("    normal %d/%d PV indexed %d"
										" of %.6G %.6G %.6G\n",
										normalIndex,normalCount,id,
										rNormal[0],rNormal[1],rNormal[2]);
							}
#endif
							break;
						}
						default:
							// other reference modes not shown here!
							break;
					}
				}
			}

#if FE_SFB_MESH_VERBOSE
			const I32 tangentCount=a_pFbxMesh->GetElementTangentCount();
			for(I32 tangentIndex=0;tangentIndex<tangentCount;tangentIndex++)
			{
				FbxGeometryElementTangent* pTangent=
						a_pFbxMesh->GetElementTangent( tangentIndex);

				if(pTangent->GetMappingMode() ==
						FbxGeometryElement::eByPolygonVertex)
				{
					switch (pTangent->GetReferenceMode())
					{
						case FbxGeometryElement::eDirect:
						{
							const FbxVector4& rTangent=pTangent
									->GetDirectArray().GetAt(vertexId);
							//* TODO
							if(verbose)
							{
								feLog("    tangent %d/%d PV"
										" direct %.6G %.6G %.6G\n",
										tangentIndex,tangentCount,
										rTangent[0],rTangent[1],rTangent[2]);
							}
							break;
						}
						case FbxGeometryElement::eIndexToDirect:
						{
							const I32 id=pTangent
									->GetIndexArray().GetAt(vertexId);
							const FbxVector4& rTangent=pTangent
									->GetDirectArray().GetAt(id);
							//* TODO
							if(verbose)
							{
								feLog("    tangent %d/%d PV indexed %d"
										" of %.6G %.6G\n",
										tangentIndex,tangentCount,id,
										rTangent[0],rTangent[1],rTangent[2]);
							}
							break;
						}
						default:
							// other reference modes not shown here!
							break;
					}
				}

			}
#endif

#if FE_SFB_MESH_VERBOSE
			const I32 binormalCount=a_pFbxMesh->GetElementBinormalCount();
			for(I32 binormalIndex=0;binormalIndex<binormalCount;binormalIndex++)
			{

				FbxGeometryElementBinormal* pBinormal=
						a_pFbxMesh->GetElementBinormal( binormalIndex);

				if(pBinormal->GetMappingMode() ==
						FbxGeometryElement::eByPolygonVertex)
				{
					switch (pBinormal->GetReferenceMode())
					{
						case FbxGeometryElement::eDirect:
						{
							const FbxVector4& rBinormal=pBinormal
									->GetDirectArray().GetAt(vertexId);
							//* TODO
							if(verbose)
							{
								feLog("    binormal %d/%d PV"
										" direct %.6G %.6G %.6G\n",
										binormalIndex,binormalCount,
										rBinormal[0],rBinormal[1],rBinormal[2]);
							}
							break;
						}
						case FbxGeometryElement::eIndexToDirect:
						{
							const I32 id=pBinormal
									->GetIndexArray().GetAt(vertexId);
							const FbxVector4& rBinormal=pBinormal
									->GetDirectArray().GetAt(id);
							//* TODO
							if(verbose)
							{
								feLog("    binormal %d/%d PV indexed %d"
										" of %.6G %.6G\n",
										binormalIndex,binormalCount,id,
										rBinormal[0],rBinormal[1],rBinormal[2]);
							}
							break;
						}
						default:
							// other reference modes not shown here!
							break;
					}
				}
			}
#endif

			vertexId++;
		} // for polygonSize
	} // for polygonCount

#if FE_SFB_MESH_VERBOSE
	//check visibility for the edges of the mesh
	const I32 visibilityCount=a_pFbxMesh->GetElementVisibilityCount();
	for(I32 visibilityIndex=0;visibilityIndex<visibilityCount;visibilityIndex++)
	{
		feLog("    visibility %d/%d\n",visibilityIndex,visibilityCount);

		FbxGeometryElementVisibility* pVisibility=
				a_pFbxMesh->GetElementVisibility(visibilityIndex);

		switch(pVisibility->GetMappingMode())
		{
			case FbxGeometryElement::eByEdge:
			{
				//should be eDirect
				const I32 edgeCount=a_pFbxMesh->GetMeshEdgeCount();
				for(I32 edgeIndex=0;edgeIndex<edgeCount;++edgeIndex)
				{
					feLog("    edge %d/%d visibility %d\n",
							edgeIndex,edgeCount,
							pVisibility->GetDirectArray().GetAt(edgeIndex));
				}
				break;
			}
			default:
				//should be eByEdge
				break;
		}
	}
#endif

	const FbxLayerElement::EType eType=FbxLayerElement::EType::eUserData;

	const I32 userDataLayerCount=a_pFbxMesh->GetLayerCount(eType);

	for(I32 userDataLayerIndex=0;userDataLayerIndex<userDataLayerCount;
			userDataLayerIndex++)
	{
		FbxLayer* pFbxLayer=a_pFbxMesh->GetLayer(userDataLayerIndex,eType);
		if(!pFbxLayer)
		{
			feLog("UserData layer %d/%d is null\n",
					userDataLayerIndex,layerCount);
			continue;
		}
		FbxLayerElementUserData* pUserData=pFbxLayer->GetUserData();
		if(!pUserData)
		{
			feLog("UserData layer %d/%d has null UserData\n",
					userDataLayerIndex,layerCount);
			continue;
		}

		const I32 arrayCount=pUserData->GetDirectArrayCount();

		for(I32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
		{
			const String dataName=pUserData->GetDataName(arrayIndex);

			const FbxDataType fbxDataType=pUserData->GetDataType(arrayIndex);
			const String dataType=fbxDataType.GetName();

			const I32 dataCount=pUserData->GetArrayCount(arrayIndex);

#if FE_SFB_VERBOSE
			feLog("user layer %d/%d %d/%d %s[%d] \"%s\"\n",
					userDataLayerIndex,layerCount,
					arrayIndex,arrayCount,
					dataType.c_str(),dataCount,dataName.c_str());
#endif

			Element element;
			if(dataCount==pointCount)
			{
				element=e_point;
			}
			else if(dataCount==polygonCount)
			{
				element=e_primitive;
			}
			else
			{
				continue;
			}

			bool success(false);
			FbxLayerElementArrayTemplate<void*>* pVoidArray=
					pUserData->GetDirectArrayVoid(arrayIndex,&success);

			if(!success)
			{
#if FE_SFB_VERBOSE
				feLog("  failed to get data\n");
#endif
				continue;
			}

			sp<SurfaceAccessorI> spUserAccessor=
					accessor(element,dataName,e_createMissing);
			if(spUserAccessor.isNull())
			{
#if FE_SFB_VERBOSE
				feLog("  failed to create accessor\n");
				continue;
#endif
			}

			if(fbxDataType==FbxStringDT)
			{
				FbxString* pDirectArrayString(NULL);
				pDirectArrayString=pVoidArray->GetLocked(pDirectArrayString,
						FbxLayerElementArray::eReadLock);

				if(!pDirectArrayString)
				{
#if FE_SFB_VERBOSE
					feLog("  NULL string data\n");
#endif
					continue;
				}

				for(I32 dataIndex=0;dataIndex<dataCount;dataIndex++)
				{
					spUserAccessor->set(dataIndex,
							pDirectArrayString[dataIndex].Buffer());
				}

				pVoidArray->Release((void**)&pDirectArrayString);
			}
			else if(fbxDataType==FbxIntDT)
			{
				FbxInt* pDirectArrayInteger(NULL);
				pDirectArrayInteger=pVoidArray->GetLocked(pDirectArrayInteger,
						FbxLayerElementArray::eReadLock);

				if(!pDirectArrayInteger)
				{
#if FE_SFB_VERBOSE
					feLog("  NULL int data\n");
#endif
					continue;
				}

				for(I32 dataIndex=0;dataIndex<dataCount;dataIndex++)
				{
					spUserAccessor->set(dataIndex,
							pDirectArrayInteger[dataIndex]);
				}

				pVoidArray->Release((void**)&pDirectArrayInteger);
			}
			else if(fbxDataType==FbxFloatDT)
			{
				FbxFloat* pDirectArrayFloat(NULL);
				pDirectArrayFloat=pVoidArray->GetLocked(pDirectArrayFloat,
						FbxLayerElementArray::eReadLock);

				if(!pDirectArrayFloat)
				{
#if FE_SFB_VERBOSE
					feLog("  NULL float data\n");
#endif
					continue;
				}

				for(I32 dataIndex=0;dataIndex<dataCount;dataIndex++)
				{
					spUserAccessor->set(dataIndex,
							pDirectArrayFloat[dataIndex]);
				}

				pVoidArray->Release((void**)&pDirectArrayFloat);
			}
			else if(fbxDataType==FbxDouble3DT || fbxDataType==FbxColor3DT)
			{
				FbxDouble3* pDirectArrayDouble3(NULL);
				pDirectArrayDouble3=pVoidArray->GetLocked(pDirectArrayDouble3,
						FbxLayerElementArray::eReadLock);

				if(!pDirectArrayDouble3)
				{
#if FE_SFB_VERBOSE
					feLog("  NULL float data\n");
#endif
					continue;
				}

				for(I32 dataIndex=0;dataIndex<dataCount;dataIndex++)
				{
					const FbxDouble3& rDouble3=pDirectArrayDouble3[dataIndex];
					const SpatialVector value(
							rDouble3[0],rDouble3[1],rDouble3[2]);

					spUserAccessor->set(dataIndex,value);
				}

				pVoidArray->Release((void**)&pDirectArrayDouble3);
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
