/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_Spool_h__
#define __netsignal_Spool_h__

//#include "boost/thread.hpp"
//#include "boost/thread/thread.hpp"
//#include "boost/thread/tss.hpp"
//#include "boost/thread/condition.hpp"
namespace fe
{
namespace ext
{

/** Thread safe data queue.  Intended for safe communication between threads. */
template<class T>
class Spool : public Component
{
	public:
		Spool(void);
virtual	~Spool(void);

		sp<T>			poll(void);
		sp<T>			wait(void);
		/** Argument is a raw pointer instead of a sp<> because it may be
			unsafe to retain references to an object passed into a Spool. */
		void			put(T *pT);
		void			interrupt(void);
		unsigned int	size(void);

	private:
		Mutex::Condition	m_readAvailable;
		std::queue<sp<T> >	m_spool;
		Poison				m_poison;
		Mutex				m_monitor;
};

template<class T>
inline Spool<T>::Spool(void)
{
}

template<class T>
inline Spool<T>::~Spool(void)
{
}

template<class T>
inline sp<T> Spool<T>::poll(void)
{
	Mutex::Guard guard(m_monitor);
	sp<T> spT;

	if(!m_spool.empty())
	{
		spT = m_spool.front();
		m_spool.pop();
	}

	return spT;
}

template<class T>
inline unsigned int Spool<T>::size(void)
{
	Mutex::Guard guard(m_monitor);
	return m_spool.size();
}

template<class T>
inline sp<T> Spool<T>::wait(void)
{
	Mutex::Guard guard(m_monitor);

	while(m_spool.empty())
	{
		if(m_poison.active())
		{
			m_poison.stop();

//			feLogGroup("netsignal","Spool throwing poison\n");

			throw Poisoned();
		}
		else
		{
			m_readAvailable.wait(guard);
		}
	}

	sp<T> spT = m_spool.front();
	m_spool.pop();
	return spT;
}

template<class T>
inline void Spool<T>::interrupt(void)
{
	Mutex::Guard guard(m_monitor);

	m_poison.start();
	m_readAvailable.notifyAll();
}

template<class T>
inline void Spool<T>::put(T *pT)
{
	Mutex::Guard guard(m_monitor);

	sp<T> spT(pT);

	if(spT->count() > 1)
	{
		feX("Spool",
			"unsafe to retain references to objects put into Spool");
	}

	m_spool.push(spT);

	m_readAvailable.notifyOne();
}

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_Spool_h__ */

