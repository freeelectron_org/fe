import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexNetworkDLLib",
                        "fexZeroMQDLLib" ]

    tests = [   'xZeroRawClient',
                'xZeroRawServer' ]

    for t in tests:
        exe = module.Exe(t)

        exe.linkmap = {}
        exe.linkmap["zmq_libs"] = module.parent.properties["zmq_libs"]

        forge.deps([t + "Exe"], deplibs)
