/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <geometry/geometry.pmh>

using namespace std;

#include "jburkardt/spline.hpp"
#include "jburkardt/spline.cpp"

namespace fe
{
namespace ext
{

template <>
double Spline<double,double>::Basis2D(double a_t,I32 a_count,
	const double* a_pKnot,const double* a_pControl)
{
	return spline_b_val(a_count,const_cast<double*>(a_pKnot),
			const_cast<double*>(a_pControl),a_t);
}

} /* namespace ext */
} /* namespace fe */