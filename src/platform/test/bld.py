import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = [ "fePlatformLib",
                "feMemoryLib" ]

    tests = [   'xTypeName',
                'xDL_Loader',
                'xException',
                'xConfirmMutex',
                'xDate',
                'xPlatform',
                'xSizeof',
                'xString',
                'xUnitTest']

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    dll = module.DLL( "xDL_Library", ["xDL_Library"])
    forge.deps( ["xDL_LibraryLib"], deplibs )

    forge.tests += [
        ("xDL_Loader.exe",      "",                 None,       None),
        ("xUnitTest.exe",       "",                 None,       None),
        ("xException.exe",      "",                 None,       None),
        ("xConfirmMutex.exe",   "",                 None,       None),
        ("xDate.exe",           "",                 None,       None),
        ("xPlatform.exe",       "",                 None,       None),
        ("xSizeof.exe",         "",                 None,       None),
        ("xString.exe",         "",                 None,       None) ]

