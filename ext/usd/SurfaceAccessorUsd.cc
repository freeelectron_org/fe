/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <usd/usd.pmh>

#define FE_SAU_DEBUG	FALSE

namespace fe
{
namespace ext
{

BWORD SurfaceAccessorUsd::bind(SurfaceAccessibleI::Element a_element,
		SurfaceAccessibleI::Attribute a_attribute)
{
	m_attribute=a_attribute;

	String name;
	switch(a_attribute)
	{
		case SurfaceAccessibleI::e_generic:
		case SurfaceAccessibleI::e_position:
			name="P";
			break;
		case SurfaceAccessibleI::e_normal:
			name="N";
			break;
		case SurfaceAccessibleI::e_uv:
			name="uv";
			break;
		case SurfaceAccessibleI::e_color:
			name="Cd";
			break;
		case SurfaceAccessibleI::e_vertices:
			name="vertices";
			FEASSERT(a_element==
					SurfaceAccessibleI::e_primitive);
			break;
		case SurfaceAccessibleI::e_properties:
			name="properties";
			FEASSERT(a_element==
					SurfaceAccessibleI::e_primitive);
			break;
	}
	return bindInternal(a_element,name);
}

BWORD SurfaceAccessorUsd::bind(SurfaceAccessibleI::Element a_element,
		const String& a_name)
{
	String name=a_name;
	m_attribute=SurfaceAccessibleI::e_generic;

	if(name=="P")
	{
		m_attribute=SurfaceAccessibleI::e_position;
	}
	else if(name=="N")
	{
		m_attribute=SurfaceAccessibleI::e_normal;
	}
	else if(name=="uv")
	{
		m_attribute=SurfaceAccessibleI::e_uv;
	}
	else if(name=="Cd")
	{
		m_attribute=SurfaceAccessibleI::e_color;
	}

	return bindInternal(a_element,name);
}

U32	SurfaceAccessorUsd::count(void) const
{
	if(m_isSkeleton && m_element==SurfaceAccessibleI::e_point)
	{
		return 4*m_spPrimNode->primitiveCount();
	}

	if(m_element==SurfaceAccessibleI::e_detail)
	{
		return 1;
	}

	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			m_attribute!=SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	return elementCount(m_element);
}

U32 SurfaceAccessorUsd::subCount(U32 a_index) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			m_attribute!=SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	if(m_isSkeleton && (m_element==SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties))
	{
		return 6;
	}

	if((m_element==SurfaceAccessibleI::e_vertex ||
		m_attribute==SurfaceAccessibleI::e_vertices) &&
			a_index<m_primTable.size())
	{
		FEASSERT(m_element==SurfaceAccessibleI::e_vertex ||
				m_element==SurfaceAccessibleI::e_primitive);

		return m_primTable[a_index][1];
	}

	return 1;
}

void SurfaceAccessorUsd::set(U32 a_index,U32 a_subIndex,String a_string)
{
	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			!isBound())
	{
		addString();
	}

	if(!isBound())
	{
		return;
	}

	if(m_element==SurfaceAccessibleI::e_detail && m_attrName=="isolateNode")
	{
		sp<SurfaceAccessibleUsd> spSurfaceAccessibleUsd=m_spSurfaceAccessibleI;
		if(spSurfaceAccessibleUsd.isValid())
		{
			spSurfaceAccessibleUsd->isolateNode(a_string);
		}
	}

	if(!m_tokenArray.size())
	{
		const I32 elemCount=primElemCount();

		m_tokenArray.resize(elemCount);

		//* TODO fix m_primTable on e_vertices accessors
	}

	I32 index=(m_tokenArray.size()==1)? 0: a_index;

	if(m_element==SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return;
		}
	}

	if(index<I32(m_tokenArray.size()))
	{
		m_tokenArray[index]=TfToken(a_string.c_str());
		m_changed=TRUE;
		return;
	}
}

String SurfaceAccessorUsd::string(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return String();
	}

	if(m_element==SurfaceAccessibleI::e_detail && m_attrName=="isolateNode")
	{
		sp<SurfaceAccessibleUsd> spSurfaceAccessibleUsd=m_spSurfaceAccessibleI;
		if(spSurfaceAccessibleUsd.isValid())
		{
			return spSurfaceAccessibleUsd->isolatedNode();
		}
		return String();
	}

	I32 index=(m_tokenArray.size()==1)? 0: a_index;

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return String();
		}
	}

	if(index<I32(m_tokenArray.size()))
	{
		String result=m_tokenArray[index].GetText();

		if(m_isSkeleton && m_attrName=="name")
		{
			result=result.basename();
		}

		return result;
	}

	return String();
}

void SurfaceAccessorUsd::set(U32 a_index,U32 a_subIndex,I32 a_integer)
{
	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			!isBound())
	{
		addInteger();
	}

	if(!isBound())
	{
		return;
	}

	//* TODO
	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		return;
	}

	if(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute==SurfaceAccessibleI::e_vertices &&
			a_index<m_primTable.size())
	{
		const Vector2i& rEntry=m_primTable[a_index];
		if(I32(a_subIndex)>=rEntry[1])
		{
			return;
		}

		if(m_isCurves)
		{
			return;
		}

		const I32 vertIndex=rEntry[0]+a_subIndex;
		m_intArray[vertIndex]=a_integer;
	}

	if(!m_intArray.size() && !m_boolArray.size())
	{
		const I32 elemCount=primElemCount();

		//* TODO conditionally m_boolArray somehow
		m_intArray.resize(elemCount);

		//* TODO fix m_primTable on e_vertices accessors
	}

	I32 index=(m_intArray.size()==1 ||
			m_boolArray.size()==1)? 0: a_index;

	if(m_element==SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return;
		}
	}

	if(index<I32(m_intArray.size()))
	{
		m_intArray[index]=a_integer;
		m_changed=TRUE;
		return;
	}

	if(index<I32(m_boolArray.size()))
	{
		m_boolArray[index]=a_integer;
		m_changed=TRUE;
		return;
	}
}

I32 SurfaceAccessorUsd::integer(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return 0;
	}

	if(m_isSkeleton)
	{
		if(a_index>=m_mat4Array.size())
		{
			return 0;
		}

		if(m_attribute==SurfaceAccessibleI::e_properties)
		{
			if(a_subIndex==SurfaceAccessibleI::e_openCurve)
			{
				return TRUE;
			}
		}

		const I32 reindex[6]={3,0,3,1,3,2};

		if(m_element==SurfaceAccessibleI::e_vertex ||
				m_attribute==SurfaceAccessibleI::e_vertices)
		{
			return a_index*4+reindex[a_subIndex];
		}

		return 0;
	}

	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		if(a_subIndex==SurfaceAccessibleI::e_openCurve)
		{
			return m_isCurves;
		}

		return 0;
	}

	if(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute==SurfaceAccessibleI::e_vertices &&
			a_index<m_primTable.size())
	{
		const Vector2i& rEntry=m_primTable[a_index];
		if(I32(a_subIndex)>=rEntry[1])
		{
			return 0;
		}
		const I32 vertIndex=rEntry[0]+a_subIndex;
		return m_isCurves? vertIndex: m_intArray[vertIndex];
	}

	if(m_element==SurfaceAccessibleI::e_detail &&
			m_attrName=="rightHandedWindingOrder")
	{
		String result=m_tokenArray[0].GetText();
		return I32(result=="rightHanded");
	}

	I32 index=(m_intArray.size()==1 ||
			m_boolArray.size()==1)? 0: a_index;

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return 0;
		}
	}

	if(index<I32(m_intArray.size()))
	{
		return m_intArray[index];
	}

	if(index<I32(m_boolArray.size()))
	{
		return m_boolArray[index];
	}

	return 0;
}

void SurfaceAccessorUsd::set(U32 a_index,U32 a_subIndex,Real a_real)
{
	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			!isBound())
	{
		addReal();
	}

	if(!isBound())
	{
		return;
	}

	if(!m_floatArray.size() && !m_doubleArray.size())
	{
		const I32 elemCount=primElemCount();

		//* TODO conditionally m_doubleArray somehow
		m_floatArray.resize(elemCount);

		//* TODO fix m_primTable on e_vertices accessors
	}

	I32 index=(m_floatArray.size()==1 || m_doubleArray.size()==1)? 0: a_index;

	if(m_element==SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return;
		}
	}

	if(index<I32(m_floatArray.size()))
	{
		m_floatArray[index]=a_real;
		m_changed=TRUE;
		return;
	}

	if(index<I32(m_doubleArray.size()))
	{
		m_doubleArray[index]=a_real;
		m_changed=TRUE;
		return;
	}
}

Real SurfaceAccessorUsd::real(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return Real(0);
	}

	I32 index=(m_floatArray.size()==1 ||
			m_doubleArray.size()==1)? 0: a_index;

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return Real(0);
		}
	}

	if(index<I32(m_floatArray.size()))
	{
		return m_floatArray[index];
	}

	if(index<I32(m_doubleArray.size()))
	{
		return m_doubleArray[index];
	}

	return Real(0);
}

void SurfaceAccessorUsd::set(U32 a_index,U32 a_subIndex,
		const SpatialVector& a_vector)
{
	if(m_isSkeleton)
	{
		if(!isBound())
		{
			return;
		}

		//* don't create skeleton attributes
		if(!m_usdAttribute.IsValid())
		{
			return;
		}

		I32 index(a_index);
		I32 row(m_row);
		SpatialVector vector(a_vector);

		if(m_attribute==SurfaceAccessibleI::e_position)
		{
			FEASSERT(m_element==SurfaceAccessibleI::e_point);

			//* don't allow primitive based position changes
			if(m_element!=SurfaceAccessibleI::e_point)
			{
				return;
			}

			index=a_index/4;
			row=a_index%4;

			GfMatrix4d& rMat4=m_mat4Array[index];
			const GfVec4d& translate4=rMat4.GetRow(3);

			//* axis arms are stored relative to translation
			if(row==3)
			{
				const GfVec4d delta4(
						vector[0]-translate4[0],
						vector[1]-translate4[1],
						vector[2]-translate4[2],double(0));
				for(I32 m=0;m<3;m++)
				{
					rMat4.SetRow(m,rMat4.GetRow(m)-delta4);
				}
			}
			else
			{
				vector-=SpatialVector(
						translate4[0],translate4[1],translate4[2]);
			}
		}

		if(!m_mat4Array.size())
		{
			const I32 elemCount=primElemCount();

			m_mat4Array.resize(elemCount);
		}

		const GfVec4d vec4(vector[0],vector[1],vector[2],
				double(row==3? 1: 0));

		m_mat4Array[index].SetRow(row,vec4);
		m_changed=TRUE;

		return;
	}

	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			!isBound())
	{
		addSpatialVector();
	}

	if(!isBound())
	{
		return;
	}

	if(!m_vec2Array.size() && !m_vec3Array.size())
	{
		const I32 elemCount=primElemCount();

		m_vec3Array.resize(elemCount);

		//* TODO fix m_primTable on e_vertices accessors
	}

	I32 index=a_index;

	if(m_element==SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return;
		}
	}

	//* NOTE currently no mechanism to alter mapping
	if(m_mappingAttribute)
	{
		if(index>=m_intArray.size())
		{
			return;
		}
		index=m_intArray[index];
	}

	if(m_vec2Array.size()==1 || m_vec3Array.size()==1)
	{
		index=0;
	}

	if(index<I32(m_vec3Array.size()))
	{
		SpatialVector transformed(a_vector);
		if(m_attrName=="P")
		{
			transformed=transformVector(m_inverse,a_vector);
		}
		if(m_attrName=="N")
		{
			transformed=rotateVector(m_inverse,a_vector);
		}
		m_vec3Array[index].Set(transformed[0],transformed[1],transformed[2]);
		m_changed=TRUE;
	}

	if(index<I32(m_vec2Array.size()))
	{
		m_vec2Array[index].Set(a_vector[0],a_vector[1]);
		m_changed=TRUE;
	}
}

SpatialVector SurfaceAccessorUsd::spatialVector(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return SpatialVector(0.0,0.0,0.0);
	}

	if(m_isSkeleton)
	{
		if(m_attribute==SurfaceAccessibleI::e_position)
		{
			const GfMatrix4d& mat4=m_mat4Array[a_index/4];

			GfVec4d vec4=mat4.GetRow(3);
			const I32 row=a_index%4;
			if(row!=3)
			{
				vec4+=mat4.GetRow(row);
			}

			const SpatialVector result(vec4[0],vec4[1],vec4[2]);
			return result;
		}

		if(m_attribute==SurfaceAccessibleI::e_color)
		{
			const Color red(1,0,0);
			const Color green(0,1,0);
			const Color blue(0,0,1);
			const Color black(0,0,0);

			const Color colorTable[4]={ red, green, blue, black };

			return colorTable[a_index%4];
		}

		if(a_index>=m_mat4Array.size())
		{
			return SpatialVector(0.0,0.0,0.0);
		}

		const GfVec4d& vec4=m_mat4Array[a_index].GetRow(m_row);
		const SpatialVector result(vec4[0],vec4[1],vec4[2]);
		return result;
	}

	I32 index=a_index;

	if(m_element==SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		index=indexOfVertex(a_index,a_subIndex);
		if(index<0)
		{
			return SpatialVector(0.0,0.0,0.0);
		}
	}

	if(m_mappingAttribute)
	{
		if(index>=m_intArray.size())
		{
			return SpatialVector(0.0,0.0,0.0);
		}
		index=m_intArray[index];
	}

	if(m_vec2Array.size()==1 || m_vec3Array.size()==1)
	{
		index=0;
	}

	if(index<I32(m_vec3Array.size()))
	{
		const GfVec3f& vec3=m_vec3Array[index];
		SpatialVector result(vec3[0],vec3[1],vec3[2]);
		if(m_attrName=="P" || m_attribute==SurfaceAccessibleI::e_vertices)
		{
			return transformVector(m_transform,result);
		}
		if(m_attrName=="N")
		{
			return rotateVector(m_transform,result);
		}
		return result;
	}

	if(index<I32(m_vec2Array.size()))
	{
		const GfVec2f& vec2=m_vec2Array[index];
		SpatialVector result(vec2[0],vec2[1],0.0);
		return result;
	}

	return SpatialVector(0.0,0.0,0.0);
}

I32 SurfaceAccessorUsd::elementCount(
	SurfaceAccessibleI::Element a_element) const
{
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
//			return m_vec3Array.size();
			return primElemCount();
		case SurfaceAccessibleI::e_vertex:
			return m_primTable.size();
		case SurfaceAccessibleI::e_primitive:
//			return m_primTable.size();
			return primElemCount();
		case SurfaceAccessibleI::e_detail:
			return 1;
		default:
			;
	}

	return 0;
}

I32 SurfaceAccessorUsd::primElemCount(void) const
{
	if(m_element==SurfaceAccessibleI::e_point)
	{
		return m_spPrimNode->pointCount();
	}
	else if(m_element==SurfaceAccessibleI::e_vertex ||
			m_element==SurfaceAccessibleI::e_primitive)
	{
		return m_spPrimNode->primitiveCount();
	}
	return 1;
}

void SurfaceAccessorUsd::addString(void)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::addString \"%s\"\n",
			m_attrName.c_str());
#endif

	//* TODO
}

void SurfaceAccessorUsd::addInteger(void)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::addInteger \"%s\"\n",
			m_attrName.c_str());
#endif

	//* TODO
}

void SurfaceAccessorUsd::addReal(void)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::addReal \"%s\"\n",
			m_attrName.c_str());
#endif

	//* TODO
}

void SurfaceAccessorUsd::addSpatialVector(void)
{
#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::addSpatialVector \"%s\"\n",
			m_attrName.c_str());
#endif

	//* TODO
}

BWORD SurfaceAccessorUsd::bindInternal(SurfaceAccessibleI::Element a_element,
		const String& a_name)
{
	//* TEMP
	if(a_name=="instance")
	{
		return FALSE;
	}

	const String elementName=
			SurfaceAccessibleBase::elementLayout(a_element);

#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::bindInternal %s \"%s\" on \"%s\"\n",
			elementName.c_str(),a_name.c_str(),m_usdPrim.GetName().GetText());
#endif

	if(!m_usdPrim)
	{
		feLog("SurfaceAccessorUsd::bindInternal"
				" invalid UsdPrim for %s \"%s\"\n",
				SurfaceAccessibleBase::elementLayout(m_element).c_str(),
				m_attrName.c_str());
		return FALSE;
	}

	m_element=SurfaceAccessibleI::e_point;
	if(a_element<0 && a_element>5)
	{
		a_element=SurfaceAccessibleI::e_point;
	}

	m_element=a_element;
	m_attrName=a_name;
	m_row=0;

	const String primType=m_usdPrim.GetTypeName().GetText();

	m_isSkeleton=(primType=="Skeleton");
	m_isCurves=(primType=="BasisCurves");
	m_changed=FALSE;

	m_usdAttrName=m_attrName;
	if(m_isSkeleton)
	{
		if(m_usdAttrName=="P" || m_usdAttrName=="refT" ||
				m_attribute==SurfaceAccessibleI::e_color ||
				m_element==SurfaceAccessibleI::e_vertex ||
				m_attribute==SurfaceAccessibleI::e_vertices ||
				m_attribute==SurfaceAccessibleI::e_properties)
		{
			m_usdAttrName="bindTransforms";
			m_row=3;
		}
		else if(m_usdAttrName=="refX")
		{
			m_usdAttrName="bindTransforms";
			m_row=0;
		}
		else if(m_usdAttrName=="refY")
		{
			m_usdAttrName="bindTransforms";
			m_row=1;
		}
		else if(m_usdAttrName=="refZ")
		{
			m_usdAttrName="bindTransforms";
			m_row=2;
		}
		else if(m_usdAttrName=="joint" || m_usdAttrName=="name")
		{
			m_usdAttrName="joints";
		}
	}
	else if(m_usdAttrName=="P")
	{
		m_usdAttrName="points";
	}
	else if(m_usdAttrName=="N")
	{
		m_usdAttrName="normals";
	}
	else if(m_usdAttrName=="rightHandedWindingOrder")
	{
		m_usdAttrName="orientation";
	}

	if(m_usdAttrName=="Cd" &&
			(m_element==SurfaceAccessibleI::e_point ||
			m_element==SurfaceAccessibleI::e_primitive))
	{
		if(m_usdPrim.HasAttribute(TfToken("primvars:Cd")))
		{
			m_usdAttrName="primvars:Cd";
		}
		else if(m_usdPrim.HasAttribute(TfToken("primvars:color")))
		{
			m_usdAttrName="primvars:color";
		}
		else if(m_usdPrim.HasAttribute(TfToken("primvars:displayColor")))
		{
			m_usdAttrName="primvars:displayColor";
		}
	}

	if(m_element==SurfaceAccessibleI::e_vertex && m_usdAttrName=="uv")
	{
		m_usdAttrName="primvars:uv";
	}

	if(m_attrName=="P" || m_attrName=="N" ||
			m_attribute==SurfaceAccessibleI::e_vertices)
	{
		m_transform=getTransform(m_usdPrim);
		invert(m_inverse,m_transform);
	}
	else
	{
		setIdentity(m_transform);
		setIdentity(m_inverse);
	}

#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::bindInternal isSkeleton %d\n",m_isSkeleton);
#endif

	std::vector<UsdProperty> propArray=m_usdPrim.GetProperties();
	const I32 propCount=propArray.size();
	for(I32 propIndex=0;propIndex<propCount;propIndex++)
	{
		UsdProperty& rProperty=propArray[propIndex];
		const String attrName=rProperty.GetName().GetText();

//		feLog("BIND %d/%d e_vertices %d attrName \"%s\" usdAttrName \"%s\"\n",
//				propIndex,propCount,
//				m_attribute==SurfaceAccessibleI::e_vertices,
//				attrName.c_str(),m_usdAttrName.c_str());

		UsdAttribute usdAttribute=rProperty.As<UsdAttribute>();
		if(!usdAttribute)
		{
			continue;
		}

		//* NOTE don't rely on role being set properly
		const String roleName=usdAttribute.GetRoleName().GetText();

		if(m_attribute==SurfaceAccessibleI::e_uv &&
				(roleName=="TextureCoordinate" ||
				attrName=="primvars:st"))
		{
			m_attrName=attrName;
			m_usdAttrName=attrName;

			const TfToken token("primvars:st:indices");
			UsdProperty mappingProperty=m_usdPrim.GetProperty(token);
			if(mappingProperty.IsValid())
			{
				m_mappingAttribute=mappingProperty.As<UsdAttribute>();
			}
		}

		const BWORD hasVertexIndices=
				(m_element==SurfaceAccessibleI::e_vertex ||
				m_attribute==SurfaceAccessibleI::e_vertices ||
				m_attribute==SurfaceAccessibleI::e_properties);
		const BWORD makePrimTable=
				(hasVertexIndices &&
				(attrName=="faceVertexCounts" ||
				attrName=="curveVertexCounts"));
		const BWORD getVertexIndices=
				(hasVertexIndices &&
				attrName=="faceVertexIndices");

		if(makePrimTable || getVertexIndices)
		{
			//* m_usdAttrName will be different
		}
		else if(m_attribute==SurfaceAccessibleI::e_vertices &&
				attrName=="points")
		{
			//* get point positions when accessing primitive vertices
		}
		else if(attrName!=m_usdAttrName)
		{
			continue;
		}

		m_usdAttribute=usdAttribute;

		std::vector<double> timeArray;
		m_usdAttribute.GetTimeSamples(&timeArray);

		const I32 timeCount=timeArray.size();

		const UsdTimeCode timeCode=timeCount?
				UsdTimeCode(m_frame): UsdTimeCode::Default();

		if(attrName=="curveVertexCounts")
		{
			m_isCurves=TRUE;
		}

		if(m_mappingAttribute.IsValid())
		{
			if(!m_mappingAttribute.Get(&m_intArray,timeCode))
			{
				//* NOTE may have empty mapping, so not mapped
#if FE_SAU_DEBUG
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access mapping array\n");
#endif
				m_mappingAttribute=UsdAttribute();
//X				return FALSE;
			}
		}

		if(makePrimTable)
		{
			VtArray<int> intArray;
			if(!m_usdAttribute.Get(&intArray,timeCode))
			{
				continue;
			}
			const I32 primitiveCount=intArray.size();
			m_primTable.resize(primitiveCount);

			I32 firstVertex=0;
			for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				const I32 vertCount=intArray[primitiveIndex];
				fe::set(m_primTable[primitiveIndex],firstVertex,vertCount);
				firstVertex+=vertCount;
			}

//~			if(m_element==SurfaceAccessibleI::e_vertex)
			{
				continue;
			}
		}

		if(getVertexIndices)
		{
			if(!m_usdAttribute.Get(&m_intArray,timeCode))
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access vertex array\n");
			}
			continue;
		}

		const String typeName=
				m_usdAttribute.GetTypeName().GetType().GetTypeName().c_str();

#if FE_SAU_DEBUG
		feLog("  attr \"%s\" type \"%s\" role \"%s\"\n",
				attrName.c_str(),typeName.c_str(),roleName.c_str());
		feLog("  path \"%s\"\n",
				m_usdAttribute.GetPath().GetAsString().c_str());
#endif

		if(typeName=="TfToken")
		{
			TfToken value;
			if(m_usdAttribute.Get(&value,timeCode))
			{
				m_tokenArray.resize(1);
				m_tokenArray[0]=value;
				return TRUE;
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access TfToken %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="bool")
		{
			bool value;
			if(m_usdAttribute.Get(&value,timeCode))
			{
				m_boolArray.resize(1);
				m_boolArray[0]=value;
				return TRUE;
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access bool %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="double")
		{
			double value;
			if(m_usdAttribute.Get(&value,timeCode))
			{
				m_doubleArray.resize(1);
				m_doubleArray[0]=value;
				return TRUE;
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access double %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="VtArray<TfToken>")
		{
			if(m_usdAttribute.Get(&m_tokenArray,timeCode))
			{
				const I32 valueCount=m_tokenArray.size();
				return matchingCount(m_element,valueCount);
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access TfToken array %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="VtArray<int>")
		{
			if(m_usdAttribute.Get(&m_intArray,timeCode))
			{
				const I32 valueCount=m_intArray.size();
				if(!matchingCount(m_element,valueCount))
				{
					return FALSE;
				}

				if(m_attribute!=SurfaceAccessibleI::e_vertices)
				{
					return TRUE;
				}
				//* keep going for both arrays
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access int array %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="VtArray<float>")
		{
			if(m_usdAttribute.Get(&m_floatArray,timeCode))
			{
				const I32 valueCount=m_floatArray.size();
				return matchingCount(m_element,valueCount);
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access float array %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="VtArray<GfVec3f>")
		{
			if(m_usdAttribute.Get(&m_vec3Array,timeCode))
			{
				const I32 valueCount=m_vec3Array.size();
				if(m_attribute!=SurfaceAccessibleI::e_vertices &&
						!matchingCount(m_element,valueCount))
				{
					return FALSE;
				}

				//* TODO generally distinguish coordinates from colors, etc
				if(m_ytoz && (a_name=="P" || a_name=="N" ||
						roleName=="Point" || roleName=="Normal"))
				{
					for(I32 valueIndex=0;valueIndex<valueCount;
							valueIndex++)
					{
						GfVec3f& rValue=m_vec3Array[valueIndex];
						rValue=GfVec3f(
								-rValue[0],rValue[2],rValue[1]);
					}
				}
				return TRUE;
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access GfVec3f array %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="VtArray<GfVec2f>")
		{
			if(m_usdAttribute.Get(&m_vec2Array,timeCode))
			{
				const I32 valueCount=m_vec2Array.size();

				if(m_mappingAttribute)
				{
					return TRUE;
				}

				return matchingCount(m_element,valueCount);
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access GfVec2f array %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
		else if(typeName=="VtArray<GfMatrix4d>")
		{
			if(m_usdAttribute.Get(&m_mat4Array,timeCode))
			{
				const I32 valueCount=m_mat4Array.size();
				return m_isSkeleton? TRUE: matchingCount(m_element,valueCount);
			}
#if FE_SAU_DEBUG
			else
			{
				feLog("SurfaceAccessorUsd::bindInternal"
						" failed to access GfMatrix4d array %s \"%s\"\n",
						elementName.c_str(),m_attrName.c_str());
			}
#endif
		}
	}

	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		return TRUE;
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices && m_isCurves)
	{
		return TRUE;
	}

	if(m_intArray.size() && m_primTable.size())
	{
#if FE_SAU_DEBUG
		feLog("SurfaceAccessorUsd::bindInternal vertices %d primitives %d\n",
				m_intArray.size(),m_primTable.size());
#endif
		return TRUE;
	}

#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::bindInternal %s \"%s\" not found\n",
			elementName.c_str(),a_name.c_str());
#endif

	return FALSE;
}

BWORD SurfaceAccessorUsd::matchingCount(SurfaceAccessibleI::Element a_element,
	I32 m_count) const
{
	if(m_count!=m_spPrimNode->count(m_element) &&
			(m_count!=1 || strncmp(m_usdAttrName.c_str(),"primvars:",9)))
	{
#if FE_SAU_DEBUG
		const String elementName=
				SurfaceAccessibleBase::elementLayout(a_element);
		feLog("SurfaceAccessorUsd::matchingCount"
				" %s \"%s\" (\"%s\") mismatched count %d/%d\n",
				elementName.c_str(),m_attrName.c_str(),
				m_usdAttrName.c_str(),
				m_count,m_spPrimNode->count(m_element));
#endif
		return FALSE;
	}
	return TRUE;
}

SpatialTransform SurfaceAccessorUsd::getTransform(UsdPrim a_usdPrim)
{
	SpatialTransform xform;
	setIdentity(xform);

	//* NOTE direct UsdPrim::GetProperty() doesn't seem to work

	std::vector<UsdProperty> propArray=a_usdPrim.GetProperties();
	const I32 propCount=propArray.size();
	for(I32 propIndex=0;propIndex<propCount;propIndex++)
	{
		UsdProperty& rProperty=propArray[propIndex];

		const String attrName=rProperty.GetName().GetText();
		if(attrName!="xformOp:transform")
		{
			continue;
		}

		if(UsdAttribute attr=rProperty.As<UsdAttribute>())
		{
			std::vector<double> timeArray;
			attr.GetTimeSamples(&timeArray);

			const I32 timeCount=timeArray.size();

			const UsdTimeCode timeCode=timeCount?
					UsdTimeCode(m_frame): UsdTimeCode::Default();

			GfMatrix4d matrix;
			if(attr.Get(&matrix,timeCode))
			{
				fe::set(xform,matrix.GetArray());
			}
			else
			{
				feLog("SurfaceAccessorUsd::getTransform"
						" failed to access transform\n");
			}

			break;
		}
	}

	UsdPrim primParent=a_usdPrim.GetParent();
	if(primParent)
	{
		//* NOTE recursive
		xform=xform*getTransform(primParent);
	}

	return xform;
}

BWORD SurfaceAccessorUsd::writeBack(void)
{
	if(!m_changed)
	{
		return FALSE;
	}

	if(!m_usdPrim)
	{
		feLog("SurfaceAccessorUsd::writeBack"
				" invalid UsdPrim for '%s' \"%s\"\n",
				SurfaceAccessibleBase::elementLayout(m_element).c_str(),
				m_attrName.c_str());
		return FALSE;
	}

	if(m_isSkeleton && !m_usdAttribute.IsValid())
	{
		feLog("SurfaceAccessorUsd::writeBack"
				" not allowed to create skeleton attribute '%s' \"%s\"\n",
				SurfaceAccessibleBase::elementLayout(m_element).c_str(),
				m_attrName.c_str());
		return FALSE;
	}

#if FE_SAU_DEBUG
	feLog("SurfaceAccessorUsd::writeBack '%s' \"%s\" on \"%s\" valid %d\n",
			SurfaceAccessibleBase::elementLayout(m_element).c_str(),
			m_attrName.c_str(),
			m_usdPrim.GetName().GetText(),
			m_usdAttribute.IsValid());
#endif

	if(!m_usdAttribute.IsValid())
	{
		//* create attribute

		TfToken name(m_usdAttrName.c_str());

		String stringTypeName;
		if(m_tokenArray.size()==1)
		{
			stringTypeName="TfToken";
		}
		else if(m_tokenArray.size())
		{
			stringTypeName="VtArray<TfToken>";
		}
		else if(m_boolArray.size()==1)
		{
			stringTypeName="bool";
		}
		else if(m_boolArray.size())
		{
			stringTypeName="VtArray<bool>";
		}
		else if(m_intArray.size()==1)
		{
			stringTypeName="int";
		}
		else if(m_intArray.size())
		{
			stringTypeName="VtArray<int>";
		}
		else if(m_floatArray.size()==1)
		{
			stringTypeName="float";
		}
		else if(m_floatArray.size())
		{
			stringTypeName="VtArray<float>";
		}
		else if(m_doubleArray.size())
		{
			stringTypeName="double";
		}
		else if(m_doubleArray.size())
		{
			stringTypeName="VtArray<double>";
		}
		else if(m_vec3Array.size()==1)
		{
			stringTypeName="GfVec3f";
		}
		else if(m_vec3Array.size())
		{
			stringTypeName="VtArray<GfVec3f>";
		}
		else if(m_vec2Array.size()==1)
		{
			stringTypeName="GfVec2f";
		}
		else if(m_vec2Array.size())
		{
			stringTypeName="VtArray<GfVec2f>";
		}
		else if(m_mat4Array.size())
		{
			stringTypeName="VtArray<GfMatrix4d>";
		}

		const SdfSchema& sdfSchema=SdfSchema::GetInstance();
		SdfValueTypeName valueTypeName;

#if TRUE
		valueTypeName=sdfSchema.FindType(stringTypeName.c_str());
#else
		std::vector<SdfValueTypeName> allTypes=sdfSchema.GetAllTypes();
		const I32 typeCount=allTypes.size();

		for(I32 typeIndex=0;typeIndex<typeCount;typeIndex++)
		{
			const String thisTypeName=
					allTypes[typeIndex].GetType().GetTypeName().c_str();

//			feLog("%d/%d \"%s\"\n",typeIndex,typeCount,thisTypeName.c_str());
			if(thisTypeName==stringTypeName)
			{
				valueTypeName=allTypes[typeIndex];
				break;
			}
		}
#endif

		if(!valueTypeName)
		{
			feLog("SurfaceAccessorUsd::writeBack did not find type \"%s\"\n",
					stringTypeName.c_str());
		}

		m_usdAttribute=m_usdPrim.CreateAttribute(name,valueTypeName);
	}

	//* TODO for e_vertices, should update points as well as vertices proper
	//* currently updates last met, where points always seem to follow indices

	const String typeName=
			m_usdAttribute.GetTypeName().GetType().GetTypeName().c_str();

#if FE_SAU_DEBUG
	const String elementName=
			SurfaceAccessibleBase::elementLayout(m_element);

	feLog("SurfaceAccessorUsd::writeBack type \"%s\"\n",typeName.c_str());
	feLog("  path \"%s\"\n",
			m_usdAttribute.GetPath().GetAsString().c_str());
#endif

	//* TODO handle frames
	const UsdTimeCode timeCode(UsdTimeCode::Default());

	if(typeName=="TfToken")
	{
		FEASSERT(m_tokenArray.size()==1);
		const TfToken value=m_tokenArray[0];
		if(m_usdAttribute.Set(value,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write TfToken '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="bool")
	{
		FEASSERT(m_boolArray.size()==1);
		const bool value=m_boolArray[0];
		if(m_usdAttribute.Set(value,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write bool '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="double")
	{
		FEASSERT(m_doubleArray.size()==1);
		const double value=m_doubleArray[0];
		if(m_usdAttribute.Set(value,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write double '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="VtArray<TfToken>")
	{
		if(m_usdAttribute.Set(m_tokenArray,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write TfToken array '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="VtArray<int>")
	{
		if(m_usdAttribute.Set(m_intArray,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write int array '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="VtArray<float>")
	{
		if(m_usdAttribute.Set(m_floatArray,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write float array '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="VtArray<GfVec3f>")
	{
		const String roleName=m_usdAttribute.GetRoleName().GetText();

		//* TODO generally distinguish coordinates from colors, etc
		if(m_ytoz && (m_attrName=="P" || m_attrName=="N" ||
				roleName=="Point" || roleName=="Normal"))
		{
			const I32 valueCount=m_vec3Array.size();
			for(I32 valueIndex=0;valueIndex<valueCount;
					valueIndex++)
			{
				GfVec3f& rValue=m_vec3Array[valueIndex];
				rValue=GfVec3f(
						rValue[0],rValue[2],rValue[1]);
			}
		}

		if(m_usdAttribute.Set(m_vec3Array,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write GfVec3f array '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="VtArray<GfVec2f>")
	{
		if(m_usdAttribute.Set(m_vec2Array,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write GfVec2f array '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}
	else if(typeName=="VtArray<GfMatrix4d>")
	{
		if(m_usdAttribute.Set(m_mat4Array,timeCode))
		{
			m_changed=FALSE;
			return TRUE;
		}
#if FE_SAU_DEBUG
		else
		{
			feLog("SurfaceAccessorUsd::writeBack"
					" failed to write GfMatrix4d array '%s' \"%s\"\n",
					elementName.c_str(),m_attrName.c_str());
		}
#endif
	}

	return FALSE;
}

} /* namespace ext */
} /* namespace fe */
