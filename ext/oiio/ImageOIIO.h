/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oiio_ImageOIIO_h__
#define __oiio_ImageOIIO_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Image handling using OpenImageIO

	@ingroup oiio

	https://sites.google.com/site/openimageio/
*//***************************************************************************/
class FE_DL_EXPORT ImageOIIO: public ImageCommon
{
	public:
				ImageOIIO(void);
virtual			~ImageOIIO(void);

				//* As ImageI
virtual	I32		createSelect(void);
virtual I32		loadSelect(String filename);
virtual I32		interpretSelect(void* data,U32 size);
virtual I32		interpretSelect(String a_source);
virtual	BWORD	save(String filename);
virtual void	select(I32 id);
virtual I32		selected(void) const	{ return m_selected; }
virtual	void	unload(I32 id);

virtual	void	setFormat(ImageI::Format a_format);
virtual	ImageI::Format	format(void) const;

virtual	void	resize(U32 width,U32 height,U32 depth);
virtual	void	replaceRegion(U32 x,U32 y,U32 z,
						U32 width,U32 height,U32 depth,void* data);

virtual	U32		width(void) const;
virtual	U32		height(void) const;
virtual	U32		depth(void) const;
virtual	void*	raw(void) const;

virtual	U32		regionCount(void) const					{ return 0; }
virtual	String	regionName(U32 a_regionIndex) const		{ return ""; }
virtual	Box2i	regionBox(String a_regionName) const
				{	return Box2i(Vector2i(0,0),Vector2i(0,0)); }
virtual	String	pickRegion(I32 a_x,I32 a_y) const		{ return ""; }

	private:
		Array<OIIO::ImageBuf*>	m_bufferList;
		I32						m_selected;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oiio_ImageOIIO_h__ */
