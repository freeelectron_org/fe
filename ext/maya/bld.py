import os
import re
import sys
import copy
import shutil

import utility
forge = sys.modules["forge"]

# NOTE: The Maya version of boost may be different.
# The boost-regex appears to be sensitive and they don't provide the dso.
# Moving aside their include/boost seems to fix regex,
# but it leaves their other boost dso's cross-versioned with system includes.

def prerequisites():
    return ["opengl", "surface"]

def setup(module):
    module.summary = []

    module.cppmap = {}
    test_include = {}
    test_cpp = {}
    test_link = {}
    test_variant = forge.maya_sources[0][0]

    deplibs = forge.corelibs+ [
                "fexDrawDLLib",
                "fexMetaDLLib",
                "fexOperateDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    maya_oplibs_replace = utility.env_list("FE_MAYA_OPLIBS_REPLACE")
    maya_oplibs_append = utility.env_list("FE_MAYA_OPLIBS_APPEND")

    fe_maya_oplibs = ':'.join(maya_oplibs_replace)
    if fe_maya_oplibs == '':
        if os.path.exists('ext/grass'):
            fe_maya_oplibs += ':fexGrassDL'
            module.summary += [ "grass" ]
        if os.path.exists('ext/ironworks'):
            fe_maya_oplibs += ':fexIronWorksDL'
            module.summary += [ " ironworks" ]
        if os.path.exists('ext/opencl'):
            fe_maya_oplibs += ':fexOpenCLDL'
            module.summary += [ "opencl" ]
        if os.path.exists('ext/operator'):
            fe_maya_oplibs += ':fexOperatorDL'
            module.summary += [ "operator" ]
        if os.path.exists('ext/vegetation'):
            fe_maya_oplibs += ':fexVegetationDL'
            module.summary += [ "vegetation" ]
    else:
        fe_maya_oplibs = ':' + fe_maya_oplibs
    if len(maya_oplibs_append):
        fe_maya_oplibs += ':' + ':'.join(maya_oplibs_append)

    for maya_source in forge.maya_sources:
        if not os.path.exists(maya_source[1]) or not os.path.exists(maya_source[2]):
            module.summary += [ "-" + maya_source[0] ]
            continue

        if maya_source[0] == "":
            module.summary += [ "." ]
        else:
            module.summary += [ maya_source[0] ]

        srcList = [ "maya.pmh",
                    "CurveCombineOp",
                    "CurveSeparateOp",
                    "DrawMaya",
                    "FontMaya",
                    "JointCombineOp",
                    "JointSeparateOp",
                    "MayaBrush",
                    "MayaDraw",
                    "MayaGraph",
                    "MayaNode",
                    "MeshSeparateOp",
                    "SurfaceAccessibleMaya",
                    "SurfaceAccessorMaya",
                    "SurfaceCurvesMaya",
                    "SurfaceTrianglesMaya",
                    "assertMaya",
                    "mayaDL" ]

        variant = maya_source[0]
        if variant != test_variant:
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink('.', variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        variantSplit = maya_source[1].split('/')
        variantSplit = variantSplit[len(variantSplit)-2].split('-')
        variantSplit = variantSplit[0].split('.')
        variantSplit = variantSplit[0].split('pr')
        maya_major = variantSplit[0].replace('maya', '')

        dllname = "fexMayaDL" + variant

        dll = module.DLL( dllname, srcList )

        fe_maya_libs = dllname + fe_maya_oplibs

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)

            srcTarget.includemap = {}
            srcTarget.includemap['maya_src'] = maya_source[1]
                        # for any poorly written includes
            srcTarget.includemap['maya_poor'] = maya_source[1] + '/maya'

            srcTarget.cppmap = { 'maya_src' :
                    ' -DFE_MAYA_MAJOR=' + str(maya_major) }
            srcTarget.cppmap['maya_src'] += ' -DFE_MAYA_LIBS=' + fe_maya_libs
            if variant == test_variant:
                test_include = copy.deepcopy(srcTarget.includemap)
                test_cpp = copy.deepcopy(srcTarget.cppmap)

            # NOTE coordinate with compiler_version in common.py
#           if int(maya_major) < 2018:
#               srcTarget.cppmap['std'] = forge.use_std("c++98")
#           else:
#               srcTarget.cppmap['std'] = forge.use_std("c++11")

            if maya_source[3] != '':
                srcTarget.cxx = maya_source[3]

            srcTarget.cppmap["cxx11_abi"] = "-D_GLIBCXX_USE_CXX11_ABI=0"

        module.cppmap = {}
        dll.linkmap = {}

        if forge.fe_os == "FE_LINUX":
            dsoPrefix = "lib"
            dsoSuffix = ".so"
            dsoSuffix2 = dsoSuffix

            # this overrides previous -W flags in codegen
            module.cppmap['maya'] = ' -Wno-deprecated -Wall -W -Wno-parentheses -Wno-sign-compare -Wno-reorder -Wno-uninitialized -Wunused -Wno-unused-parameter'

            dll.linkmap["mayalibs"] = "-Wl,-rpath='$ORIGIN/../'"
            dll.linkmap["mayalibs"] += " -shared"
            dll.linkmap["mayalibs"] += " -L" + maya_source[2]
            dll.linkmap["mayalibs"] += " -lOpenMaya -lOpenMayaUI -lOpenMayaAnim"
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            dsoPrefix = ""
            dsoSuffix = ".dll"
            dsoSuffix2 = ".mll"

            module.cppmap['maya'] = ""

            dll.linkmap["mayalibs"] = '"/LIBPATH:' + maya_source[2] + '"'
            dll.linkmap["mayalibs"] += " Foundation.lib OpenMaya.lib OpenMayaUI.lib OpenMayaAnim.lib OpenMayaRender.lib MetaData.lib"

            # TODO abstract what is left
            dll.linkmap["gfxlibs"] = forge.gfxlibs

            deplibs += [ "fexDataToolLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

        if variant == test_variant:
            test_link = copy.deepcopy(dll.linkmap)

        forge.deps( [ dllname + "Lib" ], deplibs )

        # make sym links for maya to look at
        # TODO should work on non-linux
        mayaDso = dsoPrefix + dllname + dsoSuffix

        source = os.path.join('..', mayaDso)

        mayaPath = os.path.join(forge.libPath, 'maya') + variant
        if os.path.isdir(mayaPath) == 0:
            os.makedirs(mayaPath, 0o755)

        dest = os.path.join(mayaPath, dsoPrefix + 'fexMayaDL' + dsoSuffix2)
        if os.path.lexists(dest) == 0:
            os.symlink(source, dest)

        if forge.fe_os == "FE_LINUX":
            # provoke RTLD_GLOBAL with empty 'libfexMayaDL.sog'
            sog = open(dest + 'g', "w")
            sog.close()

        mayaScriptPath = os.path.join(mayaPath, 'scripts')
        if os.path.isdir(mayaScriptPath) == 0:
            os.makedirs(mayaScriptPath, 0o755)

        for moduleName in forge.modules_found:
            moduleScriptSource = 'ext/' + moduleName + '/scripts/'

            if os.path.exists(moduleScriptSource):
                melFiles = os.listdir(moduleScriptSource)
                for mel in melFiles:
                    melSource = os.path.join(moduleScriptSource, mel)
                    melScript = os.path.join(mayaScriptPath, mel)
                    shutil.copyfile(melSource, melScript)

        fe_local = 'fe_local.mel'
        if os.path.exists(fe_local):
            melScript = os.path.join(mayaScriptPath, fe_local)
            shutil.copyfile(fe_local, melScript)

        mayaIconPath = os.path.join(mayaPath, 'icons')
        if os.path.isdir(mayaIconPath) == 0:
            os.makedirs(mayaIconPath, 0o755)

        for image in [
                'FE.svg',
                'FE_alpha.svg',
                'FE_beta.svg',
                'FE_maya.svg',
                'FE_maya_alpha.svg',
                'FE_maya_beta.svg',
                'FE_240.svg',
                'FreeElectron.svg' ]:
            doc_image = os.path.join(forge.rootPath, "doc", "image", image)
            icon = os.path.join(mayaIconPath, image)
            shutil.copyfile(doc_image, icon)

        #* TODO automated list
        for image in [
                'BladeOp.svg',
                'BlendShapeOp.svg',
                'DodgeOp.svg',
                'SpineFitOp.svg',
                'TwistBindOp.svg',
                'TwistWrapOp.svg',
                'CurveCombineOp.svg',
                'CurveSeparateOp.svg',
                'BloatOp.svg',
                'ClaspOp.svg',
                'ConnectOp.svg',
                'CurvaceousOp.svg',
                'CurveCreateOp.svg',
                'ExcarnateOp.svg',
                'FollicleOp.svg',
                'FusionOp.svg',
                'GridWrapOp.svg',
                'GroupOp.svg',
                'HammerOp.svg',
                'HobbleOp.svg',
                'ImportOp.svg',
                'JointCombineOp.svg',
                'JointSeparateOp.svg',
                'LengthCorrectOp.svg',
                'NexusOp.svg',
                'OffsetOp.svg',
                'PartitionOp.svg',
                'PuppetOp.svg',
                'RasterOp.svg',
                'SpreadsheetOp.svg',
                'SurfaceAttrCopyOp.svg',
                'SurfaceAttrCreateOp.svg',
                'SurfaceAttrLabOp.svg',
                'SurfaceBindOp.svg',
                'SurfaceCopyOp.svg',
                'SurfaceMetricOp.svg',
                'SurfaceNormalOp.svg',
                'SurfaceSampleOp.svg',
                'SurfaceSummaryOp.svg',
                'SurfaceWrapOp.svg',
                'XRayOp.svg']:
            dest = os.path.join(mayaIconPath, image)
            if os.path.lexists(dest) == 0:
                os.symlink('FE_240.svg', dest)

    if forge.fe_os == "FE_LINUX":
        test = module.Module('test')
        if test:
            test.includemap = test_include
            test.cppmap = {}
            test.cppmap = copy.deepcopy(module.cppmap)
            if 'maya_src' in test_cpp:
                test.cppmap['maya'] += test_cpp['maya_src']
            test.linkmap = copy.deepcopy(test_link)
            test_deplibs =  forge.corelibs + [
                        "fexMayaDL" + test_variant + "Lib" ]

            tests = [ 'xMayaContext' ]
            for t in tests:
                test_exe = test.Exe(t)
                forge.deps([t + "Exe"], test_deplibs)

def auto(module):
    test_file = """
#define _BOOL
#include <maya/MTypes.h>

int main(void)
{
    return(0);
}
    \n"""

    maya_path = os.getenv('MAYA_ROOT')
    if maya_path:
        forge.includemap['maya'] = os.path.join(maya_path,'include')

    result = forge.cctest(test_file)

    forge.includemap.pop('maya', None)

    return result
