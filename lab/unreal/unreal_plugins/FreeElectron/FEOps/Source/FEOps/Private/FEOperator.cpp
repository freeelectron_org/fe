#include "FEOperator.h"

#define FE_OP_TICKER	TRUE

//* NOTE seems like Epic forgot to add this one
namespace UE
{
namespace Geometry
{
bool CopyVertexColorsToOverlay(
	const FDynamicMesh3& Mesh,
	FDynamicMeshColorOverlay& ColorOverlayOut,
	bool bCompactElements=false)
{
	if (!Mesh.HasVertexColors())
	{
		return false;
	}

	if (ColorOverlayOut.ElementCount() > 0)
	{
		ColorOverlayOut.ClearElements();
	}

	ColorOverlayOut.BeginUnsafeElementsInsert();
	for (int32 Vid : Mesh.VertexIndicesItr())
	{
		FVector3f Color = Mesh.GetVertexColor(Vid);
		ColorOverlayOut.InsertElement(Vid, &Color.X, true);
	}
	ColorOverlayOut.EndUnsafeElementsInsert();

	for (int32 Tid : Mesh.TriangleIndicesItr())
	{
		ColorOverlayOut.SetTriangle(Tid, Mesh.GetTriangle(Tid));
	}

	if (bCompactElements)
	{
		FCompactMaps CompactMaps;
		ColorOverlayOut.CompactInPlace(CompactMaps);
	}

	return true;
}
}	//* namespace Geometry
}	//* namespace UE

AFEOperator::AFEOperator(const FObjectInitializer& ObjectInitializer):
	Super(ObjectInitializer),
	m_time(0.0),
	m_lastVertexCount(0),
	m_lastTriangleCount(0)
{
	//* WARNING AFEOperator::AFEOperator can run before FFEOps::StartupModule

	m_spSingleMaster=UnrealGetSingleMaster();

	feLog("AFEOperator::AFEOperator\n");

	m_meshComponent = CreateDefaultSubobject<UDynamicMeshComponent>(
			TEXT("GeneratedMesh"));

	RootComponent = m_meshComponent;

	//* TODO do we need this?
	// New in UE 4.17, multi-threaded PhysX cooking.
#if FE_UNREAL_5_1_PLUS
	m_meshComponent->bUseAsyncCooking = true;
#endif

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.SetTickFunctionEnable(true);
	PrimaryActorTick.bStartWithTickEnabled = true;
	SetActorTickEnabled(true);

//	PrimaryActorTick.bTickEvenWhenPaused = true;
//	PrimaryActorTick.TickGroup = TG_PrePhysics;
}

// This is called when actor is spawned
// (at runtime or when you drop it into the world in editor)
void AFEOperator::PostActorCreated(void)
{
	feLog("\nAFEOperator::PostActorCreated\n");

	Super::PostActorCreated();
	UpdateMesh();
}

// This is called when actor is already in level and map is opened
void AFEOperator::PostLoad(void)
{
	feLog("\nAFEOperator::PostLoad\n");

	Super::PostLoad();
	UpdateMesh();
}

void AFEOperator::Tick(float DeltaTime)
{
//	feLog("\nAFEOperator::TickActor delta %5.1f ms %d FPS\n",
//			1e3*DeltaTime,I32(1.0/DeltaTime));

	m_time+=DeltaTime;
	UpdateMesh();
}

void AFEOperator::OperatorCreate(void)
{
	feLog("AFEOperator::OperatorCreate \"%s\" \"%s\"\n",
			m_componentName.c_str(),m_nodeName.c_str());

	fe::sp<fe::Master> spMaster=m_spSingleMaster->master();

	m_spOutputAccessible=
			spMaster->registry()->create("*.SurfaceAccessibleCatalog");

	//* TODO only need to put in master catalog if used by another node

	fe::sp<fe::ext::SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
			m_spOutputAccessible;
	if(spSurfaceAccessibleCatalog.isValid())
	{
		//* TODO shouldn't be populating Master catalog root
		spSurfaceAccessibleCatalog->setCatalog(spMaster->catalog());
		spSurfaceAccessibleCatalog->setKey(m_nodeName);
		spSurfaceAccessibleCatalog->setPersistent(TRUE);
	}
	else
	{
		feLog("AFEOperator::OperatorCreate not SurfaceAccessibleCatalog\n");
		return;
	}

	fe::sp<fe::Scope> spScope=
			spMaster->catalog()->catalogComponent("Scope","OperatorScope");
	if(spScope.isNull())
	{
		feLog("AFEOperator::OperatorCreate spScope is NULL\n");
	}
	else
	{
		spScope->setLocking(FALSE);
	}

	m_terminalNode.setupOperator(
			spScope,m_componentName,m_componentName+".unreal");

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feLog("AFEOperator::OperatorCreate spCatalog is NULL\n");
		return;
	}

	spCatalog->catalog< fe::sp<fe::Component> >("Output Surface")=
			m_spOutputAccessible;

	feLog("AFEOperator::OperatorCreate created \"%s\" \"%s\"\n",
			m_componentName.c_str(),m_nodeName.c_str());
}

void AFEOperator::UpdateMesh(void)
{
	feLog("AFEOperator::UpdateMesh\n");

	if(m_spOutputAccessible.isNull())
	{
		OperatorCreate();
		OperatorInit();
	}

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feLog("AFEOperator::UpdateMesh spCatalog is NULL\n");
		return;
	}

#if FE_OP_TICKER
	const U32 tick0=fe::systemTick();
#endif

	OperatorUpdate();

#if FE_OP_TICKER
	const U32 tick1=fe::systemTick();
#endif

	//* HACK
	const BWORD copyInput=(spCatalog->catalogOrDefault<fe::String>(
			"Output Surface","IO","").empty() ||
			!spCatalog->catalogOrDefault<fe::String>(
			"Output Surface","copy","").empty());

	const BWORD clearOutput=
			!(spCatalog->catalogOrDefault<bool>("replaceOutput",false));

	const I32 frameCount=1;
	for(I32 frameIndex=0;frameIndex<frameCount;frameIndex++)
	{
		if(copyInput)
		{
			fe::sp<fe::ext::SurfaceAccessibleI> spInputAccessible=
					spCatalog->catalog< fe::sp<fe::Component> >(
					"Input Surface");

			if(spInputAccessible.isValid())
			{
				m_spOutputAccessible->copy(spInputAccessible);
			}
		}
		else if(clearOutput)
		{
			m_spOutputAccessible->clear();
		}

		feLog("AFEOperator::UpdateMesh COOK %d/%d\n",frameIndex,frameCount);
		m_terminalNode.cook(fe::Real(frameIndex));

		feLog("AFEOperator::UpdateMesh cook summary \"%s\"\n",
				spCatalog->catalogOrDefault<fe::String>("summary","").c_str());
	}

#if FE_OP_TICKER
	const U32 tick2=fe::systemTick();
#endif

	fe::sp<fe::ext::SurfaceAccessibleI> spNewOutput=
			spCatalog->catalog< fe::sp<fe::Component> >("Output Surface");
	if(m_spOutputAccessible!=spNewOutput)
	{
		feLog("AFEOperator::UpdateMesh output replaced\n");
		m_spOutputAccessible=spNewOutput;
	}

	fe::sp<fe::ext::SurfaceAccessorI> spOutputPoint=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_point,
			fe::ext::SurfaceAccessibleI::e_position,
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spOutputPoint.isNull())
	{
		feLog("AFEOperator::UpdateMesh no point access\n");
		return;
	}

	fe::sp<fe::ext::SurfaceAccessorI> spOutputVertices=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_primitive,
			fe::ext::SurfaceAccessibleI::e_vertices,
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spOutputVertices.isNull())
	{
		feLog("AFEOperator::UpdateMesh no primitive access\n");
		return;
	}

	const I32 pointCount=spOutputPoint->count();
	const I32 primitiveCount=spOutputVertices->count();

	feLog("AFEOperator::UpdateMesh pointCount %d primitiveCount %d\n",
			pointCount,primitiveCount);

	fe::ext::SurfaceAccessibleI::Element elementNormal=
			fe::ext::SurfaceAccessibleI::e_point;
	fe::sp<fe::ext::SurfaceAccessorI> spOutputNormal=
			m_spOutputAccessible->accessor(elementNormal,
			fe::ext::SurfaceAccessibleI::e_normal,
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spOutputNormal.isNull())
	{
		elementNormal=fe::ext::SurfaceAccessibleI::e_vertex;
		spOutputNormal=m_spOutputAccessible->accessor(elementNormal,
				fe::ext::SurfaceAccessibleI::e_normal,
				fe::ext::SurfaceAccessibleI::e_refuseMissing);
		if(spOutputNormal.isNull())
		{
			elementNormal=fe::ext::SurfaceAccessibleI::e_primitive;
			spOutputNormal=m_spOutputAccessible->accessor(elementNormal,
					fe::ext::SurfaceAccessibleI::e_normal,
					fe::ext::SurfaceAccessibleI::e_refuseMissing);
			if(spOutputNormal.isNull())
			{
				feLog("AFEOperator::UpdateMesh no normal access\n");
				elementNormal=fe::ext::SurfaceAccessibleI::e_point;
			}
		}
	}

	fe::ext::SurfaceAccessibleI::Element elementColor=
			fe::ext::SurfaceAccessibleI::e_point;
	fe::sp<fe::ext::SurfaceAccessorI> spOutputColor=
		m_spOutputAccessible->accessor(elementColor,
		fe::ext::SurfaceAccessibleI::e_color,
		fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spOutputColor.isNull())
	{
		elementColor=fe::ext::SurfaceAccessibleI::e_vertex;
		spOutputColor=m_spOutputAccessible->accessor(elementColor,
				fe::ext::SurfaceAccessibleI::e_color,
				fe::ext::SurfaceAccessibleI::e_refuseMissing);
		if(spOutputColor.isNull())
		{
			elementColor=fe::ext::SurfaceAccessibleI::e_primitive;
			spOutputColor=m_spOutputAccessible->accessor(elementColor,
					fe::ext::SurfaceAccessibleI::e_color,
					fe::ext::SurfaceAccessibleI::e_refuseMissing);
			if(spOutputColor.isNull())
			{
				feLog("AFEOperator::UpdateMesh no color access\n");
				elementColor=fe::ext::SurfaceAccessibleI::e_point;
			}
		}
	}

	fe::ext::SurfaceAccessibleI::Element elementUV=
			fe::ext::SurfaceAccessibleI::e_point;
	fe::sp<fe::ext::SurfaceAccessorI> spOutputUV=
			m_spOutputAccessible->accessor(elementUV,
			fe::ext::SurfaceAccessibleI::e_uv,
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spOutputUV.isNull())
	{
		elementUV=fe::ext::SurfaceAccessibleI::e_vertex;
		spOutputUV=m_spOutputAccessible->accessor(elementUV,
				fe::ext::SurfaceAccessibleI::e_uv,
				fe::ext::SurfaceAccessibleI::e_refuseMissing);
		if(spOutputUV.isNull())
		{
			feLog("AFEOperator::UpdateMesh no UV access\n");
			elementUV=fe::ext::SurfaceAccessibleI::e_point;
		}
	}

	//* winding order (in original right-handed coordinates)
	//* Houdini is generally left-handed (CW)
	//* USD defaults to right-handed (CCW)
	bool rightHandedWindingOrder(false);

	fe::sp<fe::ext::SurfaceAccessorI> spOutputWinding=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_detail,
			"rightHandedWindingOrder",
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spOutputWinding.isValid())
	{
		rightHandedWindingOrder=(spOutputWinding->integer(0)!=0);
	}

	feLog("AFEOperator::UpdateMesh rightHandedWindingOrder %d %d\n",
			spOutputWinding.isValid(),rightHandedWindingOrder);

	const BWORD verticesMapped=
			(elementNormal!=fe::ext::SurfaceAccessibleI::e_point ||
			elementColor!=fe::ext::SurfaceAccessibleI::e_point ||
			elementUV!=fe::ext::SurfaceAccessibleI::e_point);
	BWORD rebuild=(!m_lastVertexCount ||
			(verticesMapped && !m_vertexPoint.size()));

	UDynamicMesh* pUDynamicMesh=m_meshComponent->GetDynamicMesh();
	FEASSERT(pUDynamicMesh);

	FDynamicMesh3* pFDynamicMesh3=NULL;

	if(!rebuild)
	{
		pFDynamicMesh3=pUDynamicMesh->GetMeshPtr();
		if(pFDynamicMesh3)
		{
			const I32 existingVertexCount=pFDynamicMesh3->VertexCount();
			const I32 existingTriangleCount=pFDynamicMesh3->TriangleCount();

			feLog("AFEOperator::UpdateMesh existing vertices %d triangles %d\n",
					existingVertexCount,existingTriangleCount);

			if(!existingVertexCount)
			{
				rebuild=FALSE;
			}

			//* TODO more sophisticated
//~			if(pointCount!=existingVertexCount)
//~			{
//~				pFDynamicMesh3=NULL;
//~			}
		}

		if(pFDynamicMesh3==nullptr)
		{
			rebuild=TRUE;
		}
	}

	FDynamicMesh3 dynamicMesh3;

	if(rebuild)
	{
		feLog("AFEOperator::UpdateMesh rebuilding new mesh\n");

		pFDynamicMesh3=&dynamicMesh3;
		pFDynamicMesh3->EnableAttributes();
	}

	const I32 existingVertexCount=pFDynamicMesh3->VertexCount();
	const I32 existingTriangleCount=pFDynamicMesh3->TriangleCount();

	I32 vertexCount(0);
	if(!verticesMapped)
	{
		m_vertexPoint.resize(0);
		m_vertexPrimitive.resize(0);
		m_vertexSub.resize(0);
	}
	else if(!rebuild)
	{
		vertexCount=m_vertexPoint.size();
	}
	else
	{
		const I32 maxVertexCount(primitiveCount*4);
		m_vertexPoint.resize(maxVertexCount);
		m_vertexPrimitive.resize(maxVertexCount);
		m_vertexSub.resize(maxVertexCount);

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount!=3 && subCount!=4)
			{
				//* TODO triangulate
				continue;
			}

			const I32 pointIndex0=spOutputVertices->integer(primitiveIndex,0);
			const I32 pointIndex1=spOutputVertices->integer(primitiveIndex,1);
			const I32 pointIndex2=spOutputVertices->integer(primitiveIndex,2);
			const I32 pointIndex3=(subCount==4)?
					spOutputVertices->integer(primitiveIndex,3): -1;

			m_vertexPoint[vertexCount]=pointIndex0;
			m_vertexPrimitive[vertexCount]=primitiveIndex;
			m_vertexSub[vertexCount++]=0;

			m_vertexPoint[vertexCount]=pointIndex1;
			m_vertexPrimitive[vertexCount]=primitiveIndex;
			m_vertexSub[vertexCount++]=1;

			m_vertexPoint[vertexCount]=pointIndex2;
			m_vertexPrimitive[vertexCount]=primitiveIndex;
			m_vertexSub[vertexCount++]=2;

			if(subCount==4)
			{
				m_vertexPoint[vertexCount]=pointIndex3;
				m_vertexPrimitive[vertexCount]=primitiveIndex;
				m_vertexSub[vertexCount++]=3;
			}
		}

		m_vertexPoint.resize(vertexCount);
		m_vertexPrimitive.resize(vertexCount);
		m_vertexSub.resize(vertexCount);
	}

	//* TODO query surface for what has changed
	const BWORD updatePositions(TRUE);
	const BWORD updateNormals(updatePositions);
	const BWORD updateColors(rebuild && spOutputColor.isValid());
	const BWORD updateUVs(rebuild && spOutputUV.isValid());

	feLog("AFEOperator::UpdateMesh"
			" %p rebuild %d update pos %d norm %d color %d uv %d\n",
			pFDynamicMesh3,rebuild,
			updatePositions,updateNormals,updateColors,updateUVs);

	if(updatePositions)
	{
		if(rebuild)
		{
			if(verticesMapped)
			{
				for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
				{
					const I32 pointIndex=m_vertexPoint[vertexIndex];
					const fe::SpatialVector point=
							spOutputPoint->spatialVector(pointIndex);
					const I32 newIndex=pFDynamicMesh3->AppendVertex(
							FVector3d(point[0], point[2], point[1]));
					FEASSERT(newIndex==vertexIndex);
				}
			}
			else
			{
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const fe::SpatialVector point=
							spOutputPoint->spatialVector(pointIndex);
					const I32 newIndex=pFDynamicMesh3->AppendVertex(
							FVector3d(point[0], point[2], point[1]));
					FEASSERT(newIndex==pointIndex);
				}
			}
		}
		else
		{
			if(verticesMapped)
			{
				for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
				{
					const I32 pointIndex=m_vertexPoint[vertexIndex];
					const fe::SpatialVector point=
							spOutputPoint->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertex(vertexIndex,
							FVector3d(point[0], point[2], point[1]));
				}
			}
			else
			{
				const I32 minCount=fe::minimum(pointCount,existingVertexCount);
				for(I32 pointIndex=0;pointIndex<minCount;pointIndex++)
				{
					const fe::SpatialVector point=
							spOutputPoint->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertex(pointIndex,
							FVector3d(point[0], point[2], point[1]));
				}
			}
		}
	}

	if(updateNormals && spOutputNormal.isValid())
	{
		if(rebuild)
		{
			pFDynamicMesh3->EnableVertexNormals(FVector3f(1,0,0));
		}
		if(elementNormal==fe::ext::SurfaceAccessibleI::e_point)
		{
			if(verticesMapped)
			{
				for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
				{
					const I32 pointIndex=m_vertexPoint[vertexIndex];
					const fe::SpatialVector norm=
							spOutputNormal->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertexNormal(vertexIndex,
							FVector3f(norm[0], norm[2], norm[1]));
				}
			}
			else
			{
				const I32 minCount=fe::minimum(pointCount,existingVertexCount);
				for(I32 pointIndex=0;pointIndex<minCount;pointIndex++)
				{
					const fe::SpatialVector norm=
							spOutputNormal->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertexNormal(pointIndex,
							FVector3f(norm[0], norm[2], norm[1]));
				}
			}
		}
		else if(elementNormal==fe::ext::SurfaceAccessibleI::e_vertex)
		{
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 primitiveIndex=m_vertexPrimitive[vertexIndex];
				const I32 subIndex=m_vertexSub[vertexIndex];

				const fe::SpatialVector norm=
						spOutputNormal->spatialVector(primitiveIndex,subIndex);
				pFDynamicMesh3->SetVertexNormal(vertexIndex,
						FVector3f(norm[0], norm[2], norm[1]));
			}
		}
		else if(elementNormal==fe::ext::SurfaceAccessibleI::e_primitive)
		{
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 primitiveIndex=m_vertexPrimitive[vertexIndex];

				const fe::SpatialVector norm=
						spOutputNormal->spatialVector(primitiveIndex);
				pFDynamicMesh3->SetVertexNormal(vertexIndex,
						FVector3f(norm[0], norm[2], norm[1]));
			}
		}
	}

	if(updateColors)
	{
		FEASSERT(spOutputColor.isValid());
		if(rebuild)
		{
			pFDynamicMesh3->EnableVertexColors(FVector3f(1,1,1));
			pFDynamicMesh3->Attributes()->EnablePrimaryColors();
		}
		if(elementColor==fe::ext::SurfaceAccessibleI::e_point)
		{
			if(verticesMapped)
			{
				for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
				{
					const I32 pointIndex=m_vertexPoint[vertexIndex];
					const fe::Color color=
							spOutputColor->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertexColor(vertexIndex,
							FVector3f(color[0], color[1], color[2]));
				}
			}
			else
			{
				const I32 minCount=fe::minimum(pointCount,existingVertexCount);
				for(I32 pointIndex=0;pointIndex<minCount;pointIndex++)
				{
					const fe::Color color=
							spOutputColor->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertexColor(pointIndex,
							FVector3f(color[0], color[1], color[2]));
				}
			}
		}
		else if(elementColor==fe::ext::SurfaceAccessibleI::e_vertex)
		{
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 primitiveIndex=m_vertexPrimitive[vertexIndex];
				const I32 subIndex=m_vertexSub[vertexIndex];
				const fe::Color color=
						spOutputColor->spatialVector(primitiveIndex,subIndex);
				pFDynamicMesh3->SetVertexColor(vertexIndex,
						FVector3f(color[0], color[1], color[2]));
			}
		}
		else if(elementColor==fe::ext::SurfaceAccessibleI::e_primitive)
		{
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 primitiveIndex=m_vertexPrimitive[vertexIndex];
				const fe::Color color=
						spOutputColor->spatialVector(primitiveIndex);

				pFDynamicMesh3->SetVertexColor(vertexIndex,
						FVector3f(color[0], color[1], color[2]));
			}
		}
	}

	if(updateUVs)
	{
		FEASSERT(spOutputUV.isValid());
		if(rebuild)
		{
			pFDynamicMesh3->EnableVertexUVs(FVector2f(0,0));
		}
		if(elementUV==fe::ext::SurfaceAccessibleI::e_point)
		{
			if(verticesMapped)
			{
				for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
				{
					const I32 pointIndex=m_vertexPoint[vertexIndex];
					const fe::SpatialVector uv=
							spOutputUV->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertexUV(vertexIndex,
							FVector2f(uv[0], uv[1]));
				}
			}
			else
			{
				const I32 minCount=fe::minimum(pointCount,existingVertexCount);
				for(I32 pointIndex=0;pointIndex<minCount;pointIndex++)
				{
					const fe::SpatialVector uv=
							spOutputUV->spatialVector(pointIndex);
					pFDynamicMesh3->SetVertexUV(pointIndex,
							FVector2f(uv[0], uv[1]));
				}
			}
		}
		else if(elementUV==fe::ext::SurfaceAccessibleI::e_vertex)
		{
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 primitiveIndex=m_vertexPrimitive[vertexIndex];
				const I32 subIndex=m_vertexSub[vertexIndex];
				const fe::SpatialVector uv=
						spOutputUV->spatialVector(primitiveIndex,subIndex);
				pFDynamicMesh3->SetVertexUV(vertexIndex,
						FVector2f(uv[0], uv[1]));
			}
		}
		else if(elementUV==fe::ext::SurfaceAccessibleI::e_primitive)
		{
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				const I32 primitiveIndex=m_vertexPrimitive[vertexIndex];
				const fe::SpatialVector uv=
						spOutputUV->spatialVector(primitiveIndex);
				pFDynamicMesh3->SetVertexUV(vertexIndex,
						FVector2f(uv[0], uv[1]));
			}
		}
	}

	if(rebuild)
	{
		I32 skipCount(0);
		I32 triCount(0);
		I32 vertexIndex(0);

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount!=3 && subCount!=4)
			{
				//* TODO triangulate
				skipCount++;
				continue;
			}

			const I32 pointIndex0=spOutputVertices->integer(primitiveIndex,0);
			const I32 pointIndex1=spOutputVertices->integer(primitiveIndex,1);
			const I32 pointIndex2=spOutputVertices->integer(primitiveIndex,2);
			const I32 pointIndex3=(subCount==4)?
					spOutputVertices->integer(primitiveIndex,3): -1;

			if(verticesMapped)
			{
				const I32 newIndex=rightHandedWindingOrder?
						pFDynamicMesh3->AppendTriangle(
						vertexIndex,vertexIndex+2,vertexIndex+1):
						pFDynamicMesh3->AppendTriangle(
						vertexIndex,vertexIndex+1,vertexIndex+2);
				FEASSERT(newIndex==triCount);
				triCount++;

				if(subCount==4)
				{
					const I32 newIndex2=rightHandedWindingOrder?
							pFDynamicMesh3->AppendTriangle(
							vertexIndex,vertexIndex+3,vertexIndex+2):
							pFDynamicMesh3->AppendTriangle(
							vertexIndex,vertexIndex+2,vertexIndex+3);
					FEASSERT(newIndex2==triCount);
					triCount++;
				}
			}
			else
			{
				const I32 newIndex=rightHandedWindingOrder?
						pFDynamicMesh3->AppendTriangle(
						pointIndex0,pointIndex2,pointIndex1):
						pFDynamicMesh3->AppendTriangle(
						pointIndex0,pointIndex1,pointIndex2);
				FEASSERT(newIndex==triCount);
				triCount++;

				if(subCount==4)
				{
					const I32 newIndex2=rightHandedWindingOrder?
							pFDynamicMesh3->AppendTriangle(
							pointIndex0,pointIndex3,pointIndex2):
							pFDynamicMesh3->AppendTriangle(
							pointIndex0,pointIndex2,pointIndex3);
					FEASSERT(newIndex2==triCount);
					triCount++;
				}
			}

			vertexIndex+=subCount;
		}

		feLog("AFEOperator::UpdateMesh"
				" added %d vertices %d triangles, skipped %d prims\n",
				vertexIndex,triCount,skipCount);

		m_lastVertexCount=vertexIndex;
		m_lastTriangleCount=triCount;
	}

#if FE_OP_TICKER
	const U32 tick3=fe::systemTick();
#endif

	// https://www.reddit.com/r/unrealengine/comments/xv9td8/udynamicmesh_fdynamicmesh3_setvertexcolor/

	if(updateColors)
	{
		if(pFDynamicMesh3->Attributes()->HasPrimaryColors())
		{
			UE::Geometry::FDynamicMeshColorOverlay& rColorOverlayOut=
					*pFDynamicMesh3->Attributes()->PrimaryColors();
			UE::Geometry::CopyVertexColorsToOverlay(
					*pFDynamicMesh3,rColorOverlayOut);
		}
		else
		{
			feLog("AFEOperator::UpdateMesh no attribute colors\n");
		}
	}

	if(updateUVs)
	{
		if(pFDynamicMesh3->Attributes()->NumUVLayers())
		{
			UE::Geometry::FDynamicMeshUVOverlay& rUVOverlayOut=
					*pFDynamicMesh3->Attributes()->PrimaryUV();
			UE::Geometry::CopyVertexUVsToOverlay(*pFDynamicMesh3,rUVOverlayOut);
		}
		else
		{
			feLog("AFEOperator::UpdateMesh no attribute UVs\n");
		}
	}

	if(updateNormals)
	{
		if(spOutputNormal.isNull())
		{
			//* NOTE with vertex mapping, connectivity is lost
			UE::Geometry::FMeshNormals::QuickComputeVertexNormals(
					*pFDynamicMesh3);
		}
		else if(pFDynamicMesh3->Attributes()->NumNormalLayers())
		{
			//* NOTE doesn't seem to be necessary
//			UE::Geometry::FDynamicMeshNormalOverlay& rNormalOverlayOut=
//					*pFDynamicMesh3->Attributes()->PrimaryNormals();
//			UE::Geometry::CopyVertexNormalsToOverlay(
//					*pFDynamicMesh3,rNormalOverlayOut);
		}
		else
		{
			feLog("AFEOperator::UpdateMesh no attribute normals\n");
		}
	}

//	feLog("AFEOperator::UpdateMesh \"%s\"\n",
//			TCHAR_TO_ANSI(*pFDynamicMesh3->MeshInfoString()));

	if(rebuild)
	{
		feLog("AFEOperator::UpdateMesh replacing mesh\n");

		//* NOTE at very end (maybe render is asynchronous)
		m_meshComponent->GetDynamicMesh()->SetMesh(std::move(*pFDynamicMesh3));
	}
	pFDynamicMesh3=NULL;

	const EMeshRenderAttributeFlags meshRenderAttributeFlags=
			(updatePositions? EMeshRenderAttributeFlags::Positions:
			EMeshRenderAttributeFlags::None)|
			(updateNormals? EMeshRenderAttributeFlags::VertexNormals:
			EMeshRenderAttributeFlags::None)|
			(updateColors? EMeshRenderAttributeFlags::VertexColors:
			EMeshRenderAttributeFlags::None)|
			(updateUVs? EMeshRenderAttributeFlags::VertexUVs:
			EMeshRenderAttributeFlags::None);
	if(meshRenderAttributeFlags!=EMeshRenderAttributeFlags::None)
	{
		m_meshComponent->FastNotifyVertexAttributesUpdated(
				meshRenderAttributeFlags);
	}

	//* TODO should we just do this once during init?
	UMaterialInterface* pMaterial=m_meshComponent->GetMaterial(0);
	if(!pMaterial)
	{
		pMaterial=UMaterial::GetDefaultMaterial(MD_Surface);
		m_meshComponent->SetMaterial(0,pMaterial);
	}

#if FE_OP_TICKER
	const U32 tick4=fe::systemTick();

	const U32 tickDiff01=fe::SystemTicker::tickDifference(tick0,tick1);
	const U32 tickDiff12=fe::SystemTicker::tickDifference(tick1,tick2);
	const U32 tickDiff23=fe::SystemTicker::tickDifference(tick2,tick3);
	const U32 tickDiff34=fe::SystemTicker::tickDifference(tick3,tick4);

	const fe::Real msPerTick=1e-3*fe::SystemTicker::microsecondsPerTick();
	const fe::Real ms01=tickDiff01*msPerTick;
	const fe::Real ms12=tickDiff12*msPerTick;
	const fe::Real ms23=tickDiff23*msPerTick;
	const fe::Real ms34=tickDiff34*msPerTick;

	feLog("AFEOperator::UpdateMesh op       %.2f ms\n",ms01);
	feLog("AFEOperator::UpdateMesh cook     %.2f ms\n",ms12);
	feLog("AFEOperator::UpdateMesh transfer %.2f ms\n",ms23);
	feLog("AFEOperator::UpdateMesh notify   %.2f ms\n",ms34);
#endif
}

#if FALSE
	//* old UProceduralMeshComponent

	TArray<FVector> vertices;
	TArray<int32> triangles;
	TArray<FVector> normals;
	TArray<FVector2D> uv0;
	TArray<FProcMeshTangent> tangents;
	TArray<FLinearColor> vertexColors;

	const fe::Real size=0.2;	//* free points as triangles

	I32 pointTotal=0;
	I32 pointStart=0;

	//* raw tristrip
//~	I32 swap=1;

	fe::SpatialVector normalA(1,0,0);
	fe::SpatialVector normalB(1,0,0);
	fe::SpatialVector normalC(1,0,0);
	fe::SpatialVector normalD(1,0,0);

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount!=3 && subCount!=4)
		{
			continue;
		}

		const fe::SpatialVector pointA=
				spOutputVertices->spatialVector(primitiveIndex,0);
		const fe::SpatialVector pointB=
				spOutputVertices->spatialVector(primitiveIndex,1);
		const fe::SpatialVector pointC=
				spOutputVertices->spatialVector(primitiveIndex,2);
//~		const fe::SpatialVector pointB=
//~				spOutputVertices->spatialVector(primitiveIndex,1+swap);
//~		const fe::SpatialVector pointC=
//~				spOutputVertices->spatialVector(primitiveIndex,2-swap);

		if(spOutputNormal.isValid())
		{
			normalA=spOutputNormal->spatialVector(
					spOutputVertices->integer(primitiveIndex,0));
			normalB=spOutputNormal->spatialVector(
					spOutputVertices->integer(primitiveIndex,1));
			normalC=spOutputNormal->spatialVector(
					spOutputVertices->integer(primitiveIndex,2));
		}

		if(rightHandedWindingOrder)
		{
			vertices.Add(FVector(pointA[0], pointA[2], pointA[1]));
			vertices.Add(FVector(pointC[0], pointC[2], pointC[1]));
			vertices.Add(FVector(pointB[0], pointB[2], pointB[1]));

			normals.Add(FVector(normalA[0], normalA[2], normalA[1]));
			normals.Add(FVector(normalC[0], normalC[2], normalC[1]));
			normals.Add(FVector(normalB[0], normalB[2], normalB[1]));
		}
		else
		{
			vertices.Add(FVector(pointA[0], pointA[2], pointA[1]));
			vertices.Add(FVector(pointB[0], pointB[2], pointB[1]));
			vertices.Add(FVector(pointC[0], pointC[2], pointC[1]));

			normals.Add(FVector(normalA[0], normalA[2], normalA[1]));
			normals.Add(FVector(normalB[0], normalB[2], normalB[1]));
			normals.Add(FVector(normalC[0], normalC[2], normalC[1]));
		}

		triangles.Add(pointTotal++);
		triangles.Add(pointTotal++);
		triangles.Add(pointTotal++);

		uv0.Add(FVector2D(0, 0));
		uv0.Add(FVector2D(1, 1));
		uv0.Add(FVector2D(0, 1));

		tangents.Add(FProcMeshTangent(0, 1, 0));
		tangents.Add(FProcMeshTangent(0, 1, 0));
		tangents.Add(FProcMeshTangent(0, 1, 0));

		vertexColors.Add(FLinearColor(0.8, 0.3, 0.1, 1.0));
		vertexColors.Add(FLinearColor(0.8, 0.3, 0.1, 1.0));
		vertexColors.Add(FLinearColor(0.8, 0.3, 0.1, 1.0));

		if(subCount==4)
		{
			const fe::SpatialVector pointD=
					spOutputVertices->spatialVector(primitiveIndex,3);

			if(spOutputNormal.isValid())
			{
				normalD=spOutputNormal->spatialVector(
						spOutputVertices->integer(primitiveIndex,3));
			}

			if(rightHandedWindingOrder)
			{
				vertices.Add(FVector(pointA[0], pointA[2], pointA[1]));
				vertices.Add(FVector(pointD[0], pointD[2], pointD[1]));
				vertices.Add(FVector(pointC[0], pointC[2], pointC[1]));

				normals.Add(FVector(normalA[0], normalA[2], normalA[1]));
				normals.Add(FVector(normalD[0], normalD[2], normalD[1]));
				normals.Add(FVector(normalC[0], normalC[2], normalC[1]));
			}
			else
			{
				vertices.Add(FVector(pointA[0], pointA[2], pointA[1]));
				vertices.Add(FVector(pointC[0], pointC[2], pointC[1]));
				vertices.Add(FVector(pointD[0], pointD[2], pointD[1]));

				normals.Add(FVector(normalA[0], normalA[2], normalA[1]));
				normals.Add(FVector(normalC[0], normalC[2], normalC[1]));
				normals.Add(FVector(normalD[0], normalD[2], normalD[1]));
			}

			triangles.Add(pointTotal++);
			triangles.Add(pointTotal++);
			triangles.Add(pointTotal++);

			uv0.Add(FVector2D(0, 0));
			uv0.Add(FVector2D(1, 1));
			uv0.Add(FVector2D(0, 1));

			tangents.Add(FProcMeshTangent(0, 1, 0));
			tangents.Add(FProcMeshTangent(0, 1, 0));
			tangents.Add(FProcMeshTangent(0, 1, 0));

			vertexColors.Add(FLinearColor(0.8, 0.3, 0.1, 1.0));
			vertexColors.Add(FLinearColor(0.8, 0.3, 0.1, 1.0));
			vertexColors.Add(FLinearColor(0.8, 0.3, 0.1, 1.0));
		}

//~		swap= !swap;

		//* HACK
		pointStart=fe::maximum(pointStart,
				spOutputVertices->integer(primitiveIndex,2)+1);
	}

	feLog("AFEOperator::UpdateMesh pointStart %d/%d\n",pointStart,pointCount);

	//* show unattached points as little triangles
	for(I32 pointIndex=pointStart;pointIndex<pointCount;pointIndex++)
	{
		const fe::SpatialVector point=spOutputPoint->spatialVector(pointIndex);

		vertices.Add(FVector(point[0], point[2],			point[1]));
		vertices.Add(FVector(point[0], point[2]+0.5*size,	point[1]+size));
		vertices.Add(FVector(point[0], point[2]-0.5*size,	point[1]+size));

		triangles.Add(pointTotal++);
		triangles.Add(pointTotal++);
		triangles.Add(pointTotal++);

		if(spOutputNormal.isValid())
		{
			normalA=spOutputNormal->spatialVector(pointIndex);
		}

		normals.Add(FVector(normalA[0], normalA[2], normalA[1]));
		normals.Add(FVector(normalA[0], normalA[2], normalA[1]));
		normals.Add(FVector(normalA[0], normalA[2], normalA[1]));

		uv0.Add(FVector2D(0, 0));
		uv0.Add(FVector2D(1, 1));
		uv0.Add(FVector2D(0, 1));

		tangents.Add(FProcMeshTangent(0, 1, 0));
		tangents.Add(FProcMeshTangent(0, 1, 0));
		tangents.Add(FProcMeshTangent(0, 1, 0));

		vertexColors.Add(FLinearColor(0.4, 0.9, 0.4, 1.0));
		vertexColors.Add(FLinearColor(0.4, 0.9, 0.4, 1.0));
		vertexColors.Add(FLinearColor(0.4, 0.9, 0.4, 1.0));
	}

	feLog("AFEOperator::UpdateMesh pointTotal %d\n",pointTotal);

	int32 sectionIndex=0;
	bool createCollision=false;

	m_meshComponent->CreateMeshSection_LinearColor(
			sectionIndex,vertices,triangles,
			normals,uv0,vertexColors,tangents,createCollision);

	// Enable collision data
//	m_meshComponent->ContainsPhysicsTriMeshData(true);
#endif
