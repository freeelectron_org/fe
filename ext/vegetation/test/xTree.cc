/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "vegetation/vegetation.h"
#include <surface/surface.pmh>
#include "viewer/viewer.h"
#include "window/WindowEvent.h"

#define XTREE_VIDEO			1	//* 1=graphics 2=console 3=null
#define XTREE_SNAPSHOTS		0	//* 1=record images
#define XTREE_FIXED_BALL	0	//* 1=fixed collider 2=orbit
#define XTREE_POPULATION	4	//* number of trees

#define DELTA_T				(1.0f/60.0f)
#define EFFECTOR_MASS		0.002f
#define EFFECTOR_RADIUS		0.5f
#define EFFECTOR_RESCALE	1.0f	//* draw differently
#define FLOOR				0.0f
#define GRAVITY				9.81f
#define RESTITUTION			0.6f
#define IMPACT_FRICTION		0.9f

#define SPEED_STEPS			10
#define KEY_PAUSE			'p'
#define KEY_SPEED			's'
#define KEY_NEXT			'n'
#define KEY_FALL			'f'

using namespace fe;
using namespace fe::ext;

class AdvanceBall: virtual public HandlerI, public Initialize<AdvanceBall>
{
	public:
					AdvanceBall(void)
					{	setIdentity(m_transform);
						rotate(m_transform,180.0f*degToRad,e_xAxis); }
		void		initialize(void)
					{
						m_spSphereDrawMode=new DrawMode();
						m_spSphereDrawMode->setDrawStyle(
								DrawMode::e_outline);	// e_edgedSolid
						m_spSphereDrawMode->setLayer(1);

						m_spCylinderDrawMode=new DrawMode();
						m_spCylinderDrawMode->setDrawStyle(
								DrawMode::e_solid);
						m_spCylinderDrawMode->setBackfaceCulling(FALSE);
						m_spCylinderDrawMode->setTwoSidedLighting(TRUE);
						m_spCylinderDrawMode->setLayer(1);
					}
virtual				~AdvanceBall(void)										{}

					//* As HandlerI
virtual	void		handle(Record &render);

	private:
		Forest				m_forestRV;
		SpatialTransform	m_transform;
		sp<DrawMode>		m_spSphereDrawMode;
		sp<DrawMode>		m_spCylinderDrawMode;
};

class RunTree: virtual public HandlerI, public Initialize<RunTree>
{
	public:

					RunTree(void);
		void		initialize(void);
virtual				~RunTree(void);

					//* As HandlerI
virtual	void		handle(Record &render);

		void		bind(sp<DrawI> spDrawI);
		void		bind(sp<PointI> spPointI)
					{	m_forestRV.pointI()=spPointI; }
		void		bind(sp<CameraControllerI> spCameraControllerI)
					{
						m_spCameraControllerI=spCameraControllerI;
						if(spCameraControllerI.isValid())
						{
							m_forestRV.cameraControllerI()=spCameraControllerI;

							spCameraControllerI->setKeyMax(KEY_PAUSE,1);
							spCameraControllerI->setKeyMax(KEY_FALL,1);
							spCameraControllerI->setKeyMax(KEY_NEXT,1);
							spCameraControllerI->setKeyMax(KEY_SPEED,
									SPEED_STEPS);

							spCameraControllerI->setKeyCount(KEY_FALL,1);
							spCameraControllerI->setKeyCount(KEY_SPEED,
									SPEED_STEPS);

#if XTREE_FIXED_BALL
							spCameraControllerI->setKeyCount(KEY_FALL,0);

							m_spSphere->setCenter(0.2f,2.0f,0.2f);
							m_spSphere->setRadius(EFFECTOR_RADIUS);
#endif
						}
					}

	private:
		Forest					m_forestRV;
		sp<SurfaceSphere>		m_spSphere;
		AsViewer				m_asViewer;
		sp<SignalerI>			m_spSignalerI;
		sp<CameraControllerI>	m_spCameraControllerI;
		WindowEvent				m_event;

		Array< sp<HandlerI> >	m_modifierArray;
};

RunTree::RunTree(void)
{
}

RunTree::~RunTree(void)
{
#if FE_CODEGEN<=FE_OPTIMIZE
	Peeker peeker;
	m_spSignalerI->peek(peeker);

	feLog("\n~RunTree\n%s\n",peeker.str().c_str());
#endif
}

void RunTree::initialize(void)
{
}

void RunTree::bind(sp<DrawI> spDrawI)
{
	sp<Registry> spRegistry=spDrawI->registry();

	m_spSphere=spRegistry->create("*.SurfaceSphere");

	m_spSignalerI=spRegistry->create("SignalerI");
	sp<HandlerI> spSimPlant=spRegistry->create("*.SimPlant");
	sp<HandlerI> spDrawPlant=spRegistry->create("*.DrawPlant");
	sp<HandlerI> spAdvanceBall(Library::create<AdvanceBall>("AdvanceBall"));
	if(!m_spSignalerI.isValid() || !spSimPlant.isValid() ||
			!spDrawPlant.isValid() || !spAdvanceBall.isValid())
	{
		feX("RunTree::initialize", "can't create components");
	}
	m_modifierArray.push_back(spSimPlant);
	m_modifierArray.push_back(spDrawPlant);
	m_modifierArray.push_back(spAdvanceBall);

	sp<Scope> spScope=spRegistry->master()->catalog()->catalogComponent(
			"Scope","PlantScope");
	m_forestRV.bind(spScope);
	m_forestRV.createRecord();
	m_forestRV.drawI()=spDrawI;

	//* TODO added to make test run, but doesn't seem to collide anymore
	m_forestRV.collider()=m_spSphere;

	set(m_forestRV.effectorVelocity());
	set(m_forestRV.uniformVelocity());
	m_spSignalerI->insert(spSimPlant,m_forestRV.layout());
	m_spSignalerI->insert(spDrawPlant,m_forestRV.layout());
	m_spSignalerI->insert(spAdvanceBall,m_forestRV.layout());

#if XTREE_SNAPSHOTS
	sp<RecorderI> spVideoRecorder=spRegistry->create("*.VideoRecorder");
	if(spVideoRecorder.isValid())
	{
		spVideoRecorder->configure("test/treevideo",1000);
		m_modifierArray.push_back(spVideoRecorder);
		m_spSignalerI->insert(spVideoRecorder,m_forestRV.layout());
	}
#endif

	Plant plantRV;
	plantRV.bind(spScope);

#if FALSE
	const F32 separation=3.0f;
	const U32 rows=U32(sqrtf(XTREE_POPULATION));
	for(U32 m=0;m<XTREE_POPULATION;m++)
	{
		Record plant=plantRV.createRecord();
		plantRV.plantName()=name();
		set(plantRV.location(),separation*F32(m/rows),0.0f,
				separation*F32(m%rows));
		m_forestRV.plants()->add(plant);
	}
#else
	String longname;
	longname.sPrintf("../media/vegetation/%s.rg",name().c_str());
	sp<RecordGroup>	spRG=RecordView::loadRecordGroup(spScope,longname);
	RecordGroup::iterator it=spRG->begin();
	sp<RecordArray>& rspRA= *it;
	if(plantRV.plantName.check(rspRA))
	{
		m_forestRV.plants()->add(spRG);
	}
	else
	{
		Record plant=plantRV.createRecord();
		plantRV.plantName()=name();
		set(plantRV.location(),0.0f,0.0f,0.0f);
		m_forestRV.plants()->add(plant);
	}
#endif
}

void RunTree::handle(Record &render)
{
	if(!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(render.layout()->scope());
	}

	if(m_asViewer.viewer_spatial.check(render))
	{
		if(m_asViewer.viewer_spatial(render))
		{
			U32 speed=SPEED_STEPS;
			U32 next=1;
			BWORD paused=FALSE;

			if(m_spCameraControllerI.isValid())
			{
				paused=m_spCameraControllerI->keyCount(KEY_PAUSE);
				speed=m_spCameraControllerI->keyCount(KEY_SPEED);
				next=m_spCameraControllerI->keyCount(KEY_NEXT);

				if(next)
				{
					m_spCameraControllerI->setKeyCount(KEY_NEXT,next-1);
				}
			}

#if XTREE_FIXED_BALL==2
			static F32 angle=0.0f;
			angle+=0.1f;
			m_spSphere->setCenter(0.3f*sinf(angle),1.5f,0.3f*cosf(angle));
			m_spSphere->setRadius(EFFECTOR_RADIUS);
#endif

			const BWORD simulate=(next || !paused);
			m_forestRV.deltaT()=
					simulate? DELTA_T*(speed+1)/(SPEED_STEPS+1.0f): 0.0f;

			Record forest=m_forestRV.record();
			m_spSignalerI->signal(forest);
		}

		return;
	}

	m_event.bind(render);

#if FALSE
	if(!m_event.isIdleMouse() && !m_event.isPoll())
	{
		feLog("RunTree::handle %s\n",c_print(m_event));
	}
#endif

	// else, probably a window event
	if((m_event.isMousePress() || m_event.isMouseDrag()) &&
			!m_event.isModified() && m_forestRV.pointI().isValid())
	{
		if(m_event.mouseButtons()==WindowEvent::e_mbLeft)
		{
			sp<PointI> spPointI=m_forestRV.pointI();
			WindowEvent::Mask mask(WindowEvent::e_sourceMouseButton,
					WindowEvent::e_itemLeft,WindowEvent::e_stateRelease);
			m_spSphere->setCenter(spPointI->point(mask));
			m_spSphere->setRadius(EFFECTOR_RADIUS);
			set(m_forestRV.effectorVelocity());
		}
		if(m_event.mouseButtons()==WindowEvent::e_mbMiddle)
		{
			sp<PointI> spPointI=m_forestRV.pointI();

			WindowEvent::Mask mask(WindowEvent::e_sourceMouseButton,
					WindowEvent::e_itemMiddle,WindowEvent::e_statePress);

			WindowEvent::Mask mask2=mask;
			mask2.setState(WindowEvent::e_stateRelease);

			m_forestRV.uniformVelocity()=
					spPointI->point(mask2)-spPointI->point(mask);
		}
	}
}

void AdvanceBall::handle(Record &record)
{
	m_forestRV.bind(record);
	sp<SurfaceSphere> spSphere=m_forestRV.collider();

	sp<DrawI> spDrawI=m_forestRV.drawI();
	if(!spDrawI.isValid())
	{
		return;
	}

	sp<PointI> spPointI=m_forestRV.pointI();

	if(m_forestRV.deltaT()>0.0f)
	{
		U32 fall=1;

		if(m_forestRV.cameraControllerI().isValid())
		{
			sp<CameraControllerI> spCameraControllerI=
					m_forestRV.cameraControllerI();
			fall=spCameraControllerI->keyCount(KEY_FALL);
		}

		if(!spPointI.isValid() ||
				spPointI->flags()&PointI::e_flagHold)
		{
			fall=0;
		}

		if(fall)
		{
			F32 minz=FLOOR+EFFECTOR_RADIUS;
			m_forestRV.effectorVelocity()+=
					DELTA_T*m_forestRV.effectForce()/EFFECTOR_MASS;

			SpatialVector effector=spSphere->center();
			effector+=DELTA_T*m_forestRV.effectorVelocity();
			if(effector[2]<minz &&
					m_forestRV.effectorVelocity()[2]<0.0f)
			{
				m_forestRV.effectorVelocity()[0]*=IMPACT_FRICTION;
				m_forestRV.effectorVelocity()[1]*=IMPACT_FRICTION;
				m_forestRV.effectorVelocity()[2]=
						-RESTITUTION*m_forestRV.effectorVelocity()[2];
				effector[2]+=(RESTITUTION+1.0f)*(minz-effector[2]);
			}
			spSphere->setCenter(effector);
		}
	}

	set(m_forestRV.effectForce(),0.0f,0.0f,-GRAVITY*EFFECTOR_MASS);

	spDrawI->drawAxes(1.0f);

	if(spPointI.isValid())
	{
		spPointI->setView(spDrawI->view());
		spPointI->drawFeedback(spDrawI);
	}

	const F32 radius=spSphere->radius()*EFFECTOR_RESCALE;

	const Color black(0.0f,0.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color blue(0.0f,0.0f,1.0f,1.0f);
	const Color pink(1.0f,0.5f,0.5f,1.0f);
	const Color point(0.1f,0.1f,0.0f,1.0f);

	if(radius!=0.0f)
	{
		const SpatialVector scale(radius,radius,radius);
		setTranslation(m_transform,spSphere->center());

		spDrawI->setDrawMode(m_spSphereDrawMode);
		spDrawI->drawSphere(m_transform,&scale,red);
		spDrawI->setDrawMode(sp<DrawMode>(NULL));

		SpatialVector groundPoint=spSphere->center();
		groundPoint[2]=0.0;
		setTranslation(m_transform,groundPoint);
		spDrawI->drawCircle(m_transform,&scale,black);
	}

	if(magnitudeSquared(m_forestRV.uniformVelocity())>0.0f)
	{
		sp<PointI> spPointI=m_forestRV.pointI();

		WindowEvent::Mask mask(WindowEvent::e_sourceMouseButton,
				WindowEvent::e_itemMiddle,WindowEvent::e_statePress);
		const SpatialVector source=spPointI->point(mask);

		spDrawI->setDrawMode(m_spCylinderDrawMode);
		spDrawI->drawCylinder(source,source+m_forestRV.uniformVelocity(),
				0.0f,0.3f,blue,16);
		spDrawI->setDrawMode(sp<DrawMode>(NULL));
	}
}

int main(int argc,char** argv)
{
	const Color skyblue(0.4f,0.6f,0.8f,1.0f);

	if(argc<2)
	{
		feLog("%s: No tree name specified\n\nTry: %s <treename> [frames]\n\n",
				argv[0],argv[0]);
		return 1;
	}

	UNIT_START();

	BWORD complete=FALSE;

	const char* treename=argv[1];
	U32 frames=(argc>2)? atoi(argv[2])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

#if XTREE_VIDEO==1
		Result result=spRegistry->manage("fexViewerDL");
#if XTREE_SNAPSHOTS
		String& draw_hint=spMaster->catalog()->catalog<String>("hint:DrawI");
		draw_hint="*.DrawOpenGL";
		spRegistry->manage("fexOpenILDL");
#endif
#else
		Result result=spRegistry->manage("fexConsoleDL");
#endif
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexVegetationDL");
		UNIT_TEST(successful(result));

		{
#if XTREE_VIDEO==3
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("*.NullViewer"));
#else
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
#endif
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "can't create QuickViewerI");
			}

			sp<PointI> spPickPoint(spRegistry->create("*.PickPoint"));

			spQuickViewerI->open();

			sp<WindowI> spWindowI=spQuickViewerI->getWindowI();
			if(spWindowI.isValid())
			{
				sp<EventContextI> spEventContextI=
						spWindowI->getEventContextI();
				spEventContextI->setPointerMotion(TRUE);
				spWindowI->setBackground(skyblue);
			}

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isValid() && spDrawI->drawMode().isValid())
			{
				spDrawI->drawMode()->setPointSize(6.0f);
			}

			sp<CameraControllerI> spCameraControllerI=
					spQuickViewerI->getCameraControllerI();
			if(spCameraControllerI.isValid())
			{
				spCameraControllerI->setLookAngle(20.0f,30.0f,16.0f);
			}

#if FALSE
			sp<HandlerI> spLogger(spRegistry->create("*.LogWindowHandler"));
			if(spLogger.isValid())
			{
				spQuickViewerI->insertEventHandler(spLogger);
			}
#endif

			sp<RunTree> spRunTree(Library::create<RunTree>("RunTree"));
			spRunTree->setName(treename);
			spRunTree->bind(spDrawI);
			spRunTree->bind(spCameraControllerI);

			if(spPickPoint.isValid())
			{
				spRunTree->bind(spPickPoint);
				spQuickViewerI->insertEventHandler(spPickPoint);
			}

			spQuickViewerI->insertEventHandler(spRunTree);
			spQuickViewerI->insertDrawHandler(spRunTree);
			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
