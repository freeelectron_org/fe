//
// Copyright 2016 Pixar
//
// Licensed under the Apache License, Version 2.0 (the "Apache License")
// with the following modification; you may not use this file except in
// compliance with the Apache License and the following modification to it:
// Section 6. Trademarks. is deleted and replaced with:
//
// 6. Trademarks. This License does not grant permission to use the trade
//	names, trademarks, service marks, or product names of the Licensor
//	and its affiliates, except as required to comply with Section 4(c) of
//	the License and to reproduce the content of the NOTICE file.
//
// You may obtain a copy of the Apache License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Apache License with the above modification is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied. See the Apache License for the specific
// language governing permissions and limitations under the Apache License.

// originally from pxr/imaging/lib/hd/unitTestHelper.h
// adapted for Free Electron

#ifndef __usd_HydraDriver_h__
#define __usd_HydraDriver_h__

#include "pxr/imaging/hd/engine.h"
//#include "pxr/imaging/hd/lightingShader.h"
#include "pxr/imaging/hd/renderPass.h"
#include "pxr/imaging/hd/renderPassState.h"
#include "pxr/imaging/hio/glslfx.h"

#include "pxr/base/gf/vec4d.h"
#include "pxr/base/gf/matrix4d.h"

#include <vector>
#include <boost/scoped_ptr.hpp>

/// \class HydraDriver
///
/// A unit test driver that exercises the core engine.
///
/// \note This test driver does NOT assume OpenGL is available; in the event
/// that is is not available, all OpenGL calls become no-ops,
/// but all other work is performed as usual.
class HydraDriver
{
	public:
						HydraDriver(void);
						HydraDriver(TfToken const &reprName);
						~HydraDriver(void);

		void			Draw(HdEngine& a_rEngine,bool withGuides=false);
		void			Draw(HdEngine& a_rEngine,
								HdRenderPassSharedPtr const &renderPass);

						/// Set camera to renderpass
		void			SetCamera(GfMatrix4d const &modelViewMatrix,
							GfMatrix4d const &projectionMatrix,
							GfVec4d const &viewport);

		HdRenderPassSharedPtr const& GetRenderPass(bool withGuides=false);

		HdRenderPassStateSharedPtr const& GetRenderPassState(void) const
						{	return m_renderPassState; }

		HydraDelegate&	GetDelegate(void)
						{	return *m_pSceneDelegate; }

		void			SetRepr(TfToken const &reprName);

	private:

	class NullRenderPass: public HdRenderPass
	{
	public:
					NullRenderPass(HdRenderIndex *index,
						HdRprimCollection const &collection):
						HdRenderPass(index, collection)						{}
	virtual			~NullRenderPass(void)									{}

#if HD_API_VERSION>=31
			void	_Execute(HdRenderPassStateSharedPtr const &renderPassState,
						TfTokenVector const &renderTags) override			{}
#endif
	};

		void		_Init(TfToken const &reprName);

		HdStRenderDelegate			m_renderDelegate;
		HdRenderIndex*				m_pRenderIndex;
		HydraDelegate*				m_pSceneDelegate;
		TfToken						m_reprName;
		HdRenderPassSharedPtr		m_geomPass;
		HdRenderPassSharedPtr		m_geomAndGuidePass;
		HdRenderPassStateSharedPtr	m_renderPassState;

		std::unique_ptr<class Hgi>	m_hgi;
		HdDriver					m_hgiDriver;
};

#endif  // __usd_HydraDriver_h__
