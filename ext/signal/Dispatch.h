/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_dispatch_h__
#define __signal_dispatch_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT Dispatch:
	public virtual DispatchI,
	public CastableAs<Dispatch>
{
	public:
							Dispatch(void);
virtual						~Dispatch(void);

							using DispatchI::call;

virtual	bool				call(const String &a_name,InstanceMap& a_argv)
							{	return false; }

					/** Return a the signature supported by call() */
virtual	SignatureMap		&signatures(void);

		template <class T>
		void				dispatch(const String &a_name);

		bool				checkedCall(const String &a_name,
									Array<Instance> a_argv);

	protected:
		SignatureMap		m_dispatchSignatures;
};

template<class T>
void Dispatch::dispatch(const String &a_name)
{
	m_dispatchSignatures[a_name].append(
			registry()->master()->typeMaster()->lookupType(
			TypeInfo(getTypeId<T>())));
}

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_dispatch_h__ */

