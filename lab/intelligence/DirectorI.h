/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DirectorI_h__
#define __intelligence_DirectorI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Tells others how to behave

	@ingroup intelligence

	The director is a stateless grab-bag of utilities to manipulate the scene.
*//***************************************************************************/
class FE_DL_EXPORT DirectorI: virtual public Component
{
	public:
virtual	bool			addBehavior(Record& rControlRecord,
								String name) const							=0;

virtual	void			moveRecordGroup(sp<RecordGroup> spRecordGroup,
								const SpatialVector displacement) const		=0;

virtual	void			adoptRecordGroup(Record arena,
								sp<RecordGroup> spRecordGroup) const		=0;

virtual	Record			searchRecordGroup(sp<RecordGroup> spRG,
								String particlename)						=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DirectorI_h__ */
