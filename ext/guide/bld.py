import sys
import os
import shutil
import utility
forge = sys.modules["forge"]

def prerequisites():
    return [ "networkhost", "beacon" ]

def setup(module):
    srcList = [ "GuidePostNetHost",
                "guide.pmh",
                "guideDL" ]

    dll = module.DLL( "fexGuideDL", srcList )

    deplibs = forge.corelibs + [
                "fexNetworkHostDLLib",
                "fexBeaconClientDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolLib" ]

    forge.deps( ["fexGuideDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexGuideDL",                   None,   None) ]

    module.Module('test')
