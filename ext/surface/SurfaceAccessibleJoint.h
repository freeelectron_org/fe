/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleJoint_h__
#define __surface_SurfaceAccessibleJoint_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Joint Surface Binding

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleJoint:
	public SurfaceAccessibleCatalog,
	public CastableAs<SurfaceAccessibleJoint>
{
	public:
								SurfaceAccessibleJoint(void):
									m_frame(0.0)							{}
virtual							~SurfaceAccessibleJoint(void)				{}

								using SurfaceAccessibleCatalog::load;

								//* as SurfaceAccessibleI
virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings)
								{	return FALSE; }

		void					startJoints(void);
		void					completeJoints(void);
		BWORD					setJoint(
									String a_name,String a_parentName,
									const SpatialTransform& a_rXformRelative,
									const SpatialVector& a_rScaleLocal,
									const SpatialTransform& a_rRefWorld,
									Color a_color);
		BWORD					addJoint(String a_name,String a_parentName,
									const SpatialTransform& a_rAnimLocal,
									const SpatialTransform& a_rAnimWorld,
									const SpatialVector& a_rScaleLocal,
									const SpatialTransform& a_rRefWorld,
									Real a_length,Color a_color);
		BWORD					addJoint(String a_name,String a_parentName,
									const SpatialTransform& a_rAnimLocal,
									const SpatialTransform& a_rAnimWorld,
									const SpatialVector& a_rScaleLocal,
									Real a_length,Color a_color);

		void					startPoints(void);
		void					completePoints(void);
		void					startMeshes(void);
		void					completeMeshes(void);
		void					startCurves(void);
		void					completeCurves(void);

	protected:

virtual	void					reset(void);
		void					resetJoints(void);
		void					clearAccessors(void);
		void					setOptions(String a_options);

		Real									m_frame;
		std::map<String,String>					m_optionMap;
		sp<StringFilterI>						m_spFilter;

		sp<SurfaceAccessorI>					m_spOutputPoint;
		sp<SurfaceAccessorI>					m_spOutputColor;
		sp<SurfaceAccessorI>					m_spOutputNormal;
		sp<SurfaceAccessorI>					m_spOutputUV;
		sp<SurfaceAccessorI>					m_spOutputPart;
		sp<SurfaceAccessorI>					m_spOutputRadius;
		sp<SurfaceAccessorI>					m_spOutputName;
		sp<SurfaceAccessorI>					m_spOutputParent;
		sp<SurfaceAccessorI>					m_spOutputRefX;
		sp<SurfaceAccessorI>					m_spOutputRefY;
		sp<SurfaceAccessorI>					m_spOutputRefZ;
		sp<SurfaceAccessorI>					m_spOutputRefT;
		sp<SurfaceAccessorI>					m_spOutputAnimX;
		sp<SurfaceAccessorI>					m_spOutputAnimY;
		sp<SurfaceAccessorI>					m_spOutputAnimZ;
		sp<SurfaceAccessorI>					m_spOutputAnimT;
		sp<SurfaceAccessorI>					m_spOutputAnimS;
		sp<SurfaceAccessorI>					m_spOutputVertices;
		sp<SurfaceAccessorI>					m_spOutputProperties;

		Array<String>							m_nameList;
		std::map<String,String>					m_parentMap;
		std::map<String, std::vector<String> >	m_childMap;
		std::map<String,Color>					m_colorMap;
		std::map<String,SpatialVector>			m_targetMap;
		std::map<String,I32>					m_childrenMap;
		std::map<String,BWORD>					m_leafMap;

		//* includes joints filtered out
		std::map<String,SpatialTransform>		m_refMap;
		std::map<String,SpatialTransform>		m_refTweakMap;
		std::map<String,SpatialTransform>		m_animMap;
		std::map<String,SpatialTransform>		m_animTweakMap;
		std::map<String,SpatialVector>			m_animScaleMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleJoint_h__ */
