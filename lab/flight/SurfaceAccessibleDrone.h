/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_SurfaceAccessibleDrone_h__
#define __flight_SurfaceAccessibleDrone_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Drone Surface Loader

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleDrone:
	public SurfaceAccessibleBase
{
	public:
						SurfaceAccessibleDrone(void);
virtual					~SurfaceAccessibleDrone(void);

						//* as SurfaceAccessibleI
virtual	void			reset(void);

virtual	BWORD			isBound(void)
						{	return TRUE; }

						using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
							Element a_element,String a_name,
							Creation a_create,Writable a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
							Element a_element,Attribute a_attribute,
							Creation a_create,Writable a_writable);

						using SurfaceAccessibleBase::load;

virtual	BWORD			load(String a_filename,
								sp<Catalog> a_spSettings);

						using SurfaceAccessibleBase::attributeSpecs;

virtual	void			attributeSpecs(
								Array<SurfaceAccessibleI::Spec>&
								a_rSpecs,
								String a_node,
								SurfaceAccessibleI::Element
								a_element) const
						{	a_rSpecs.clear(); }

						using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>	surface(String a_group,
								SurfaceI::Restrictions a_restrictions);

	private:
		sp<SurfaceI>			m_spSurfaceI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_SurfaceAccessibleDrone_h__ */
