/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_GDI

extern HINSTANCE gs_hinstDLL;
extern NativeEventContext *pLastCreatedWindow;

void NativeEventContext::nativeStartup(void)
{
	m_mousebuttons=WindowEvent::e_mbNone;

	WNDCLASS wndClass;
	HINSTANCE hInstance=gs_hinstDLL;

	wndClass.style=			CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndClass.lpfnWndProc=	(WNDPROC)NativeEventContext::wndProc;
	wndClass.cbClsExtra=	0;
	wndClass.cbWndExtra=	sizeof(NativeEventContext*);
	wndClass.hInstance=		hInstance;
	wndClass.hIcon=			NULL;
	wndClass.hCursor=		LoadCursor(NULL,IDC_ARROW);
	wndClass.hbrBackground=	(HBRUSH)COLOR_BACKGROUND;
	wndClass.lpszMenuName=	NULL;
#ifdef UNICODE
	wndClass.lpszClassName= L"NativeWindowClass";
#else
	wndClass.lpszClassName= "NativeWindowClass";
#endif

	static bool s_classRegistered = false;
	if(!s_classRegistered)
	{
		if (!RegisterClass(&wndClass))
		{
			feX("NativeWindow::nativeStartup RegisterClass failed code=%d\n",
				GetLastError());
		}
		s_classRegistered = true;
	}
}

void NativeEventContext::nativeShutdown(void)
{
}

void NativeEventContext::notifyCreate(FE_WINDOW_HANDLE &windowHandle)
{
//~	if(!m_fontBase)
//~	{
//~		m_fontBase=loadFont(windowHandle);
//~	}
//~
//~	//* each context must do this
//~	useLoadedFont(windowHandle);
//~
//~	feLog("NativeEventContext::notifyCreate() fontbase=%d\n",m_fontBase);
}

void NativeEventContext::setPointerMotion(BWORD active)
{
	m_pointerMotion=active;
}

void NativeEventContext::threadLock(void)
{
}

void NativeEventContext::threadUnlock(void)
{
}

void NativeEventContext::loop(BWORD dropout)
{
	MSG msg;

	m_endloop=false;
	BWORD done=false;
	while(!done && !m_endloop)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			m_event.reset();
			m_event.setSIS(WindowEvent::e_sourceSystem,WindowEvent::e_itemIdle,
					WindowEvent::e_stateRepeat);
			m_event.setMouseButtons(mouseButtons());
			broadcastEvent(&m_lastWindowHandle);

			if(dropout)
				done=true;
		}
	}
}

void NativeEventContext::accumulateTime(U32 exclusive,U32 inclusive)
{
	const U32 update_ms=300;

	m_time_excl+=exclusive;
	m_time_incl+=inclusive;
	m_frames++;

	if(inclusive==0 || m_time_incl>update_ms)
	{
		m_fps_excl=m_time_excl? m_frames/(m_time_excl*0.001f): 0.0f;
		m_fps_incl=inclusive? m_frames/(m_time_incl*0.001f): 0.0f;

		m_time_incl=0;
		m_time_excl=0;
		m_frames=0;
	}
}

U32 NativeEventContext::systemTimer(void)
{
	return timeGetTime();
}

LRESULT CALLBACK NativeEventContext::wndProc(HWND hWnd,UINT message,
		WPARAM wParam,LPARAM lParam)
{
	LONG_PTR longPtr=GetWindowLongPtr(hWnd,0);
	NativeEventContext *pEventContext= (NativeEventContext *)longPtr;

#if FE_WINDOW_EVENT_DEBUG
	feLog("NativeEventContext::wndProc pEventContext=%p\n",pEventContext);
#endif

	if(!pEventContext)
	{
		feLog("wndProc() pEventContext is NULL\n");
//		pEventContext=pLastCreatedWindow;
		return DefWindowProc(hWnd,message,wParam,lParam);
	}
	if(!pEventContext->m_record.isValid())
	{
		pEventContext->createEvent();
	}
	if(!pEventContext->m_record.isValid())
	{
		return DefWindowProc(hWnd,message,wParam,lParam);
	}

	WindowEvent& event=pEventContext->m_event;
	BWORD down=false;

	pEventContext->setLastWindowHandle(hWnd);

	BWORD shifted=		( GetKeyState(VK_SHIFT) & 0x8000 ) != 0;
	BWORD controlled=	( GetKeyState(VK_CONTROL) & 0x8000 ) != 0;
	BWORD alted=		( GetKeyState(VK_MENU) & 0x8000 ) != 0;
	BWORD locked=		( GetKeyState(VK_CAPITAL) & 0x1 ) != 0;

	switch(message)
	{
		case WM_CREATE:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_CREATE\n");
#endif
			break;

		case WM_CLOSE:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_CLOSE\n");
#endif
			event.setSIS(WindowEvent::e_sourceSystem,
					WindowEvent::e_itemCloseWindow,(WindowEvent::State)0);
			event.setState2((WindowEvent::State)0);

//			DestroyWindow(hWnd);
			break;

		case WM_DESTROY:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_DESTROY\n");
#endif
			KillTimer(hWnd,101);

//			PostQuitMessage(0);
			break;

		case WM_SIZE:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_SIZE\n");
#endif
			event.setSIS(WindowEvent::e_sourceSystem,WindowEvent::e_itemResize,
					(WindowEvent::State)LOWORD(lParam));
			event.setState2((WindowEvent::State)HIWORD(lParam));
			break;

		case WM_TIMER:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_TIMER\n");
#endif
			event.setSIS(WindowEvent::e_sourceSystem,
					WindowEvent::e_itemIdle,WindowEvent::e_stateNull);
			break;

		case WM_PAINT:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_PAINT\n");
#endif
			ValidateRect(hWnd,NULL);
			event.setSIS(WindowEvent::e_sourceSystem,WindowEvent::e_itemExpose,
					(WindowEvent::State)LOWORD(lParam));
			event.setState2((WindowEvent::State)HIWORD(lParam));
			break;

		case 0x020A:	// WM_MOUSEWHEEL:
		{
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_MOUSEWHEEL\n");
#endif
			short roll=(short)HIWORD(wParam);
			event.setSIS(WindowEvent::e_sourceMouseButton,
					WindowEvent::e_itemWheel,(WindowEvent::State)roll);
		}
			break;

		case WM_KEYDOWN:
			down=TRUE;
		case WM_KEYUP:
		{
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_KEY*\n");
#endif
			event.setSource(WindowEvent::e_sourceKeyboard);
			event.setState(
				down? WindowEvent::e_statePress: WindowEvent::e_stateRelease);

			switch(wParam)
			{
				case VK_LEFT:
					event.setItem(WindowEvent::e_keyCursorLeft);
					break;

				case VK_UP:
					event.setItem(WindowEvent::e_keyCursorUp);
					break;

				case VK_RIGHT:
					event.setItem(WindowEvent::e_keyCursorRight);
					break;

				case VK_DOWN:
					event.setItem(WindowEvent::e_keyCursorDown);
					break;

				default:
				{
					unsigned char keystate[256];
					unsigned short trans[3];

					if(!GetKeyboardState(keystate))
					{
						event.setSource(WindowEvent::e_sourceNull);
						break;
					}

					trans[0]=0;
					U32 result=ToAscii(wParam,lParam,keystate,trans,FALSE);
					U32 item=trans[0]&255;

					if(alted || controlled)
					{
						item+=(shifted||locked)? 'A'-1: 'a'-1;
					}

					if(result!=1)
						event.setSource(WindowEvent::e_sourceNull);
					else
						event.setItem(WindowEvent::Item(item));
				}
					break;
			}
		}
			break;

		case WM_LBUTTONDOWN:
		case WM_MBUTTONDOWN:
		case WM_RBUTTONDOWN:
			down=true;
		case WM_LBUTTONUP:
		case WM_MBUTTONUP:
		case WM_RBUTTONUP:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_?BUTTON*\n");
#endif
			if(message==WM_LBUTTONUP || message==WM_LBUTTONDOWN)
			{
				event.setItem(WindowEvent::e_itemLeft);
				pEventContext->setMouseButtons(WindowEvent::MouseButtons(
						(pEventContext->mouseButtons()&6)|(down? 1: 0)));
			}
			else if(message==WM_MBUTTONUP || message==WM_MBUTTONDOWN)
			{
				event.setItem(WindowEvent::e_itemMiddle);
				pEventContext->setMouseButtons(WindowEvent::MouseButtons(
						(pEventContext->mouseButtons()&5)|(down<<1)));
			}
			else
			{
				event.setItem(WindowEvent::e_itemRight);
				pEventContext->setMouseButtons(WindowEvent::MouseButtons(
						(pEventContext->mouseButtons()&3)|(down<<2)));
			}

			event.setSource(WindowEvent::e_sourceMouseButton);
			event.setState(down?
					WindowEvent::e_statePress: WindowEvent::e_stateRelease);
			break;

		case WM_MOUSEMOVE:
#if FE_WINDOW_EVENT_DEBUG
			feLog("NativeEventContext::wndProc WM_MOUSEMOVE\n");
#endif

			if(pEventContext->mouseButtons())
				event.setSIS(WindowEvent::e_sourceMousePosition,
						WindowEvent::e_itemDrag,
						WindowEvent::e_stateRepeat);
			else if(pEventContext->m_pointerMotion)
			{
				event.setSIS(WindowEvent::e_sourceMousePosition,
						WindowEvent::e_itemMove,
						WindowEvent::e_stateRepeat);
			}
			else
			{
				event.setSource(WindowEvent::e_sourceNull);
			}
			break;

		default:
#if FE_WINDOW_EVENT_DEBUG
			feLog("  unrecognized event\n");
#endif
			return (DefWindowProc(hWnd,message,wParam,lParam));
	}

	if(event.source()==WindowEvent::e_sourceKeyboard ||
			event.source()==WindowEvent::e_sourceMouseButton ||
			event.source()==WindowEvent::e_sourceMousePosition)
	{
		if(shifted)
			event.setItem(WindowEvent::Item(event.item()|
					WindowEvent::e_keyShift));
		if(controlled)
			event.setItem(WindowEvent::Item(event.item()|
					WindowEvent::e_keyControl));
		if(alted)
			event.setItem(WindowEvent::Item(event.item()|
					WindowEvent::e_keyAlt));
		if(locked)
			event.setItem(WindowEvent::Item(event.item()|
					WindowEvent::e_keyCapLock));
	}

	if(event.source())
	{
		event.setMousePosition(MAKEPOINTS(lParam).x,MAKEPOINTS(lParam).y);
		event.setMouseButtons(pEventContext->mouseButtons());

		pEventContext->broadcastEvent(&hWnd);
	}

	return(0L);
}

//~U32 NativeEventContext::loadFont(FE_WINDOW_HANDLE &windowHandle)
//~{
//~	HFONT fonthandle;
//~	LOGFONT logfont;
//~	HDC hdc=GetDC(windowHandle);
//~
//~	logfont.lfHeight=8;
//~	logfont.lfWidth=0;
//~	strcpy(logfont.lfFaceName,"Arial Narrow");
//~
//~//	fonthandle=CreateFontIndirect(&logfont);
//~	fonthandle=(HFONT)GetStockObject(DEFAULT_GUI_FONT);
//~	if(fonthandle == NULL)
//~	{
//~		feLog("error creating font\n");
//~		return 0;
//~	}
//~
//~	const U32 last=255;
//~	const U32 lists=last+1;
//~
//~	U32 fontBase=glGenLists(lists);
//~	if(fontBase==0)
//~	{
//~		feLog("error generating font display lists\n");
//~
//~		return 0;
//~	}
//~
//~	if(!SelectObject(hdc,fonthandle))
//~	{
//~		feLog("error selecting font\n");
//~		return 0;
//~	}
//~
//~	calcFontHeight(windowHandle);
//~
//~	return fontBase;
//~}

//~void NativeEventContext::calcFontHeight(FE_WINDOW_HANDLE &windowHandle)
//~{
//~	HDC hdc=GetDC(windowHandle);
//~
//~	int height=0;
//~
//~	//* numbers can be taller than letters in Win32 fonts
//~	SIZE size;
//~	if(GetTextExtentPoint32(hdc,"1y",2,&size))
//~	{
//~		height=size.cy;
//~	}
//~
//~	TEXTMETRIC metric;
//~	if(GetTextMetrics(hdc,&metric))
//~	{
//~		m_fontAscent=metric.tmAscent;
//~		m_fontDescent=metric.tmDescent;
//~	}
//~
//~	if(height!=m_fontAscent+m_fontDescent)
//~	{
//~		feLog("NativeEventContext::fontHeight"
//~				" height %d != ascent %d + descent %d\n",
//~				height,m_fontAscent,m_fontDescent);
//~	}
//~
//~//	feLog("NativeEventContext::fontHeight ascent %d descent %d height %d\n",
//~//			m_fontAscent,m_fontDescent,height);
//~}

//~I32 NativeEventContext::pixelWidth(String string)
//~{
//~	HDC hdc=GetDC(m_lastWindowHandle);
//~
//~	SIZE size;
//~
//~	if(GetTextExtentPoint32(hdc,string.c_str(),string.length(),&size))
//~	{
//~		return size.cx;
//~	}
//~
//~	feLog("NativeEventContext::pixelWidth failed getting extent\n");
//~	return 0;
//~}

//~void NativeEventContext::useLoadedFont(FE_WINDOW_HANDLE &windowHandle)
//~{
//~	const U32 first=0;
//~	const U32 last=255;
//~
//~	HDC hdc=GetDC(windowHandle);
//~	if(!wglUseFontBitmaps(hdc,first,last-first+1,m_fontBase+first))
//~	{
//~		feLog("error using font bitmap\n");
//~	}
//~}

#endif

} /* namespace ext */
} /* namespace fe */
