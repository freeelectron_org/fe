import sys
import os
import re
forge = sys.modules["forge"]

def setup(module):
    module.summary = []

    version = ""
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        il_h = forge.vcpkg_include + "/IL/il.h"
    else:
        il_h = "/usr/include/IL/il.h"
    if os.path.exists(il_h):
        expr = re.compile("#define\s+IL_VERSION\s+.*")
        for line in open(il_h).readlines():
            if expr.match(line):
                version = line.split()[2].rstrip()
                break

    if version != "":
        module.summary += [ version ]

    srcList = [ "openil.pmh",
                "ImageIL",
                "openilDL" ]

    dll = module.DLL( "fexOpenILDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexImageDLLib" ]

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["imagelibs"] = "-lIL -lILU -lILUT"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        dll.linkmap["imagelibs"] = "DevIL.lib ILU.lib ILUT.lib"
        if forge.codegen == "debug":
            dll.linkmap["imagelibs"] += " jasperd.lib"
            dll.linkmap["imagelibs"] += " jpeg.lib"
            dll.linkmap["imagelibs"] += " libpng16d.lib"
            dll.linkmap["imagelibs"] += " tiffd.lib"
            dll.linkmap["imagelibs"] += " zlibd.lib"
        else:
            dll.linkmap["imagelibs"] += " jasper.lib"
            dll.linkmap["imagelibs"] += " libpng16.lib"
            dll.linkmap["imagelibs"] += " tiff.lib"
            dll.linkmap["imagelibs"] += " zlib.lib"

        dll.linkmap["imagelibs"] += " jpeg.lib"
        dll.linkmap["imagelibs"] += " lzma.lib"

    forge.deps( ["fexOpenILDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOpenILDL",                      None,   None) ]

    if not 'ray' in forge.modules_found:
        module.summary += [ "-ray" ]

    if 'viewer' in forge.modules_confirmed:
        forge.tests += [
            ("xImage.exe",  "fexOpenILDL 10",                   None,   None) ]

        if forge.media:
            forge.tests += [ ("xImage.exe",
                "fexOpenILDL " + forge.media + "/image/OpenIL.png 10",
                                                                None,   None) ]
        if 'ray' in forge.modules_found:
            forge.tests += [
                ("xRay.exe",    "fexOpenILDL 30",               None,   None) ]
    else:
        module.summary += [ "-viewer" ]

#   module.Module('test')

def auto(module):
    test_file = """
#include <IL/ilut.h>

int main(void)
{
    return(0);
}
    \n"""

    ilmap = { "gfxlibs": forge.gfxlibs }
    if forge.fe_os == "FE_LINUX":
        ilmap["imagelibs"] = "-lIL -lILU -lILUT"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        ilmap["imagelibs"] = "DevIL.lib ILU.lib ILUT.lib"

    return forge.cctest(test_file, ilmap)
