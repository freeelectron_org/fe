#!/usr/bin/python3

import datetime
import multiprocessing
import os
import pathlib
import re
import shutil
import stat
import subprocess
import sys
import time
import traceback
import textwrap
import urllib.request

# NOTE: The utility module enforces a python version check at import time. So
# we import it as soon as possible, which causes an immediate abort if the
# check fails. This is more implicit than is desired, but we can't directly
# import the version check method, since doing so just causes the module to
# experience import processing in full, so there's no way to isolate the check
# ourselves -- we just defer to the module doing it for us. This could/would
# change if forge becomes a proper package in the future.
import utility

SUCCESS = True
FAIL = False
SYS_SUCCESS = 0
SYS_FAIL = 1

DIRECTIVES = [
    'install',
    'remove',
    'export',
    'upload',
    'download',
]

# Note: this would be better as an externally-sourced (cfg) list, to permit set
# adjustments without code change.
PACKAGES = [
    'czmq',
    'eigen3',
    'enet',
    'freetype-gl',
    'glew',
    'libwebsockets',
    'openal-soft',
    'opencl',
    'opencv',
    'rapidjson',
    'sdl2',
]

RE_BUNDLE_LABEL = re.compile(
    r'^vcpkg-export-(?P<stamp>[\d]{8}-[\d]{6})-(?P<triplet>[-\w]+)$')

# NOTE to select Visual Studio version for install,
# you have to hide the more recent versions (can rename directories)
# We are currently building with VS 2019, as 2022 builds can fail on 2019.

# NOTE we can still need to do updates/upgrades by hand.
# Maybe we need another directive or have install do more.

TOOLNAME = pathlib.Path(__file__).stem

# -----------------------------------------------------------------------------
# Logging

clog = utility.clogger()
# No level assignment currently used, though anyone importing build_vcpkg could
# set one if desired, as the logger instance is global.

# -----------------------------------------------------------------------------
# This is an exception subclass for use only to achieve a vanilla exit 'fail'.
# The reason is due to the fact that this module gets used externally via a
# multiprocess.Process() call directly into the Build() function, and as such
# it cannot act like a pass/fail call without some special treatment. The
# context can throw an unhandled exception, which will get logged
# outside of our control and will result in a process exit code of 1
# (failure). OR, execution can terminate with a `sys.exit(n)` and the process
# exit code will be 'n' (and we'll use n=1 to get the intended interpretation
# by the caller).
# To distinguish between `return FAIL` circumstances vs. an actual unhandled
# exception, we expect internal logic in this module to throw a BuildFail
# exception.
class BuildFail(Exception):
    pass

# -----------------------------------------------------------------------------
# A helper func for being able to show signs-of-life during the file download.
def download_async(url, dstfile, password_mgr):

    handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
    opener = urllib.request.build_opener(handler)

    with opener.open(url) as inputObj:
        with open(dstfile, 'wb') as outputObj:
            shutil.copyfileobj(inputObj, outputObj)
    return

# -----------------------------------------------------------------------------
def Build(directive):

    # We have to re-raise any exception that occurs, due to the aforementioned
    # use of this module via multiprocess.Process() calls directly into this
    # function. If we return normally out of it, it always counts as a
    # successful (non-failing) call.  The only way to get a Process()
    # invocation to act like a fail is to cause its underlying proc to have an
    # uncaught exception and produce a system exit error.

    try:
        _build(directive)
    except BuildFail as e:
        # Using an explicit sys.exit() call here is what ensures that the
        # consumer context doing the Process() call knows that the call failed.
        clog.error(str(e))
        sys.exit(1)
    except Exception as e:
        clog.error("build vcpkg exception")
        clog.exception(e)
        raise
    else:
        clog.info("build vcpkg completed")

    return

# -----------------------------------------------------------------------------
def create_manifest(packages, filelocation, baseline_id):

    info = {
        'packages': ',\n'.join([f'"{p}"' for p in packages]),
        'baseline': baseline_id,
        # The overlay ports directory exists two levels up from where we make
        # the manifest file and run vcpkg.exe. Forward slashes are compatible
        # with vcpkg parsing.
        'overlaydir': '../../overlay_ports/custom',
    }

    template = textwrap.dedent("""
        {{
          "builtin-baseline": "{baseline}",
          "dependencies": [
            {packages}
          ]
        }}
        """)

    # If we need custom pkg ports to participate, the manifest content would
    # need to include a section like the following (peer with the
    # 'dependencies' level):
    #
    #      "vcpkg-configuration": {{
    #        "overlay-ports": [
    #            "{overlaydir}"
    #        ]
    #      }}

    with open(filelocation, 'w') as fp:
        fp.write(template.format(**info))

    return

# -----------------------------------------------------------------------------
# Internal use-only function.  External consumers of this module should use
# Build().
#
# TODO: This really needs a major refactor. As a monolithic function handling
# every possible directive, it's gotten messy and cumbersome.

def _build(directive):

    # former package inclusions:
    # jsoncpp - now encapsulated in module ext/json
    # yaml-cpp - now encapsulated in module ext/yaml

    if directive not in DIRECTIVES:
        raise BuildFail(f"{TOOLNAME}: invalid directive '{directive}'"
            f"\nMust be one of: {', '.join(DIRECTIVES)}.")

    packages = PACKAGES[:]

    os.environ["FE_TARGET_ARCH"] = utility.determine_target_architecture()
    (env, modset_roots) = utility.applyLocalEnvFiles()

    # This block of vars needs to be turned into a dict or something that can
    # be generated and used by individual per-directive handler functions.
    fe_vcpkg_triplets = os.environ['FE_VCPKG_TRIPLETS'].split(':')
    fe_vcpkg_triplet_active = os.environ['FE_VCPKG_TRIPLET_ACTIVE']
    fe_vcpkg_upload_path = os.environ["FE_VCPKG_UPLOAD_PATH"]
    fe_vcpkg_download_url = os.environ["FE_VCPKG_DOWNLOAD_URL"]
    fe_vcpkg_download_stamp = os.environ["FE_VCPKG_DOWNLOAD_STAMP"]
    fe_vcpkg_download_google_id = os.environ["FE_VCPKG_DOWNLOAD_GOOGLE_ID"]
    fe_vcpkg_download_authenticated = \
        bool(int(os.environ["FE_VCPKG_DOWNLOAD_AUTHENTICATED"]))
    fe_vcpkg_install_path = \
        pathlib.Path(os.environ["FE_VCPKG_INSTALL_PATH"]).resolve()
    fe_vcpkg_install = pathlib.Path(os.environ["FE_VCPKG_INSTALL"]).resolve()
    fe_vcpkg_native_path = os.environ["FE_VCPKG_NATIVE_BUILD_PATH"]
    fe_vcpkg_native_exports_path = fe_vcpkg_install_path / 'exports'
    fe_vcpkg_baseline_id = os.environ['FE_VCPKG_NATIVE_BASELINE_ID']

    drive = fe_vcpkg_install_path.drive
    local_path = pathlib.Path(*fe_vcpkg_install_path.parts[1:])

    # This is a basis for cwd values in the directive blocks. Path fragments
    # are likely added to create directive-specific paths.
    cd_command = f"{drive} && cd {local_path}"

    def full_native_path(tail=fe_vcpkg_native_path):
        return fe_vcpkg_install_path / tail

    def path_check(path, level=clog.CERR):

        result = SUCCESS

        if not os.path.exists(path):
            clog.log(level, f"Path does not exist: {path}")
            result = FAIL

        return result

    # -------------------------------------------------------------------------
    def directive_export(triplet, stamp):

        result = SUCCESS

        nativepath = full_native_path()

        if not path_check(nativepath):
            clog.error("Location for locally built packages missing. "
                "Try running 'build.py vcpkg install'.")
            return FAIL

        tripletdir = os.path.join(nativepath, 'installed', triplet)

        if not path_check(tripletdir):
            clog.error(f"Missing local install for triplet: {triplet} "
                "Try adjusting configuration and re-running "
                "'build.py vcpkg install'.")
            return FAIL

        dstdir = fe_vcpkg_native_exports_path

        if not path_check(dstdir):
            clog.info(f"Export directory does not exist, creating: {dstdir}")
            os.makedirs(dstdir)

        dstname = f"vcpkg-export-{stamp}-{triplet}"

        cmd = [
            'vcpkg.exe',
            'export',
            "--zip",
            f"--output-dir={dstdir}",
            f"--output={dstname}",
            '--x-install-root=.',
        ]

        clog.info(f"Exporting {triplet} bundle to: {dstname}")
        clog.debug(' '.join(cmd))

        proc = subprocess.run(cmd, cwd=tripletdir)
        rc = proc.returncode

        if rc != SYS_SUCCESS:
            clog.error(f"vcpkg zip process failed ({rc})")
            result = FAIL

        return result

    # -------------------------------------------------------------------------
    if directive == 'export':

        # This is a multi-triplet directive. It will result in the local
        # (native) export of every triplet specified in the env list of
        # eligible triplets, IF that triplet has previously undergone an
        # 'install' action.

        # Create one stamp value that will be used with every triplet exported
        # in this session.
        stamp = datetime.datetime.today().strftime('%Y%m%d-%H%M%S')

        for triplet in fe_vcpkg_triplets:

            result = directive_export(triplet, stamp)
            if not result:
                raise BuildFail(
                    "vcpkg export attempt failed for triplet: {triplet}")

        return

    # -------------------------------------------------------------------------
    if directive == 'upload':

        # NOTE: THIS DIRECTIVE CURRENTLY WON'T WORK!
        # It needs to be refactored to accommodate the new vcpkg bundle
        # management Also: consider whether we should be automating this at
        # all. It feels like some scope creep, where someone should just put
        # the bundle file where they want it, according to whatever
        # mechanisms/authorization are needed to do so.

        raise BuildFail("'upload' directive temporarily unsupported.")

        # An export is assumed to have already occurred. 'upload' will NOT do
        # it. This facilitates improved cli parameterization in the future for
        # management of an accumulation of local (varied) export bundles.

        exports_dir = fe_vcpkg_native_exports_path

        hint = "Try running the export step first."

        if not path_check(exports_dir):
            raise BuildFail(
                f"Location of export bundles does not exist.\n{hint}")

        creds = utility.getUserCredentials(utility.Credential(password=False))

        # NOTE presume listdir sorting will see latest file last
        zipFile = ""

        # Danger! Year 2100 bug!!
        expression = re.compile(r"^vcpkg-export-20.*\.zip$")

        entries = os.listdir(exports_dir)
        for entry in entries:
            if expression.match(entry):
                zipFile = entry

        if zipFile == "":
            raise BuildFail(f"No export bundles found.\n{hint}")

        command = cd_command + f" && cd {exports_dir}"
        command += " && scp " + zipFile
        command += " " + creds.user + "@" + fe_vcpkg_upload_path + "/"

        clog.info("Copying zip file to server")
        clog.debug(command)

        rc = os.system(command)

        if rc != SYS_SUCCESS:
            raise BuildFail(f"file upload process failed ({rc})")

        return

    # -------------------------------------------------------------------------
    if directive == 'download':

        # This directive acts on a single triplet value only (the configured
        # 'active' triplet). The is because of the restrictions in how download
        # variables are configured in the env, particularly involving
        # googledrive-hosted files.

        creds = None
        password_mgr = None

        download_dir = fe_vcpkg_install_path / 'downloads'
        tempdir = download_dir / '.temp'

        if tempdir.exists():
            raise BuildFail("A prior attempt appears to have failed. The "
                f"temporary download directory exists ({tempdir}). "
                "If it is not needed for examination, it must be deleted "
                "before attempting another bundle download.")

        clog.info(
            f"Creating temporary directory for bundle download: {tempdir}")
        os.makedirs(tempdir)

        # Note that we don't append the triplet value yet. This is important!
        stampdir = download_dir / fe_vcpkg_download_stamp

        bundlelabel = 'vcpkg-export-{stamp}-{triplet}'.format(
            stamp=fe_vcpkg_download_stamp, triplet=fe_vcpkg_triplet_active)

        zipname = f'{bundlelabel}.zip'

        # Presumptive URl value (might be discarded if we're fetching from a
        # google drive).
        url = f'{fe_vcpkg_download_url}/{zipname}'

        dstfile = tempdir / zipname

        clog.info(f"Target filename: {dstfile}")

        # A specified google drive ID will take precedence over a specified
        # download URL. The url value crafted above will NOT be used.
        # NOTE: this means that a mistmatch might occur, as the ID has no
        # derivative bearing to any other part of the expected bundle
        # configuration.
        if fe_vcpkg_download_google_id:

            url = "{url}?id={driveid}&export=download".format(
                url='https://drive.usercontent.google.com/download',
                driveid=fe_vcpkg_download_google_id,
            )

            clog.info(f"Using download URL: {url}")

            command = f"curl.exe -s -L \"{url}\""

            popen = subprocess.Popen(command, bufsize=-1, shell=True,
                stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            (html, p_err) = popen.communicate()

            if not html:
                raise BuildFail("Failed to interpret google drive response.")

            match = re.search(
                r'"confirm" value="([a-zA-Z0-9\-_])', html.decode("utf-8"),
            )

            if match and match.lastindex > 0:
                confirm = match.group(1)
            else:
                raise BuildFail("Failed to interpret google drive response.")

            if confirm:
                url = f"{url}&confirm={confirm}"
                clog.info(f"Adjusted url: {url}")

        if fe_vcpkg_download_authenticated:
            creds = utility.getUserCredentials()
            password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            password_mgr.add_password(
                None, fe_vcpkg_download_url, creds.user, creds.password)

        # Avoid redundant download processing.
        full_dstdir = stampdir / fe_vcpkg_triplet_active
        if full_dstdir.exists():
            clog.info(f"vcpkg bundle already exists: {full_dstdir}")
            return

        proc = multiprocessing.Process(
            target=download_async,
            args=(url, dstfile, password_mgr),
        )
        clog.info(f"Downloading {url} to {dstfile}")
        proc.start()

        while (proc.exitcode is None):
            time.sleep(1)
            clog.context('.', eol='')
        clog.context('')
        rc = proc.exitcode

        if rc != SYS_SUCCESS:
            raise BuildFail(f"URl access failed ({rc})")

        cmd = [
            'tar',
            'xf',
            zipname,
        ]

        clog.info(f"Extracting vcpkg bundle: {zipname}")
        clog.debug(' '.join(cmd))

        proc = subprocess.Popen(cmd, cwd=tempdir)
        while (proc.poll() is None):
            time.sleep(1)
            clog.context('.', eol='')
        clog.context('')
        rc = proc.returncode

        if rc != SYS_SUCCESS:
            raise BuildFail(f"Bundle extraction process failed ({rc})")

        clog.info(f"Removing downloaded zip file: {dstfile}")
        os.remove(dstfile)

        # Sanity check! Now that the bundle is unzipped, we can test whether
        # its contents match the configured specs that have been assumed thus
        # far (stamp and triplet).

        # Asking a Path for with_suffix('') will strip off the suffix.
        contents = list(tempdir.iterdir())
        if len(contents) != 1:
            raise BuildFail("Contents of extracted bundle do not conform to "
                "expectations (should be a single subdirectory).")

        srcdir = contents[0]
        downloadlabel = srcdir.stem

        clog.info("Comparing extracted dir to expectations: "
            f"{downloadlabel} ==? {bundlelabel}")

        match = RE_BUNDLE_LABEL.match(downloadlabel)

        if not match:
            raise BuildFail("Extracted bundle subdirectory does not "
                f"conform to specification: {downloadlabel}")

        bundlestamp = match.groupdict()['stamp']
        bundletriplet = match.groupdict()['triplet']

        if downloadlabel != bundlelabel:

            msg = ("Contents of extracted bundle do not conform to "
                "expectations:"
                f"\n\tbundle stamp: {bundlestamp} "
                f"(expected {fe_vcpkg_download_stamp})"
                f"\n\tbundle triplet: {bundletriplet} "
                f"(expected {fe_vcpkg_triplet_active})"
            )

            if fe_vcpkg_download_google_id:
                hint = "Likely that FE_VCPKG_DOWNLOAD_GOOGLE_ID does not " \
                    "refer to a bundle of the active stamp and triplet."
            else:
                hint = "The bundle filename appears to be misleading: " \
                    f"{zipname}"

            raise BuildFail(f"{msg}\n{hint}")

        clog.info(f"Creating partial destination directory: {stampdir}")
        os.makedirs(stampdir)

        # Now move the contents of the unzipped bundle as the triplet dir.
        # We use shutilmove() because it's nicer/easier than executing a
        # shell-sensitive wildcard-based move.
        #
        # The subdirectory from within the zip file has been confirmed to meet
        # the expected criteria, and we know the stamp and triplet values are
        # also consistent with the current cfg.

        clog.info(f"Moving vcpkg bundle: {srcdir} -> {full_dstdir}")
        shutil.move(srcdir, full_dstdir)

        clog.info(f"Removing temporary directory: {tempdir}")
        os.rmdir(tempdir)

        # The extracted bundle actually has a couple subdirectories in it
        # (vcpkg did it, not us), before we get to the parts that matter to
        # forge (the include and lib dirs, etc..).
        full_dstdir = full_dstdir / 'installed' / bundletriplet

        if fe_vcpkg_install != full_dstdir:
            clog.warning("WARNING: not configured to use this vcpkg bundle.")
            clog.warning('\n'.join(
                ["\tBundle located in:", f"\t\t{full_dstdir}"]))
            clog.warning('\n'.join(
                ["\tFE_VCPKG_INSTALL is set to:", f"\t\t{fe_vcpkg_install}"]))

        return

    # -------------------------------------------------------------------------
    if directive == 'remove':

        raise BuildFail("'remove' directive temporarily unsupported.")

    # -------------------------------------------------------------------------
    if directive == 'install':

        # This is a multi-triplet directive. It will result in the local
        # (native) installation of every triplet specified in the env list of
        # eligible triplets.

        nativepath = full_native_path()
        if not path_check(nativepath):
            clog.info(
                f"Native directory does not exist, creating: {nativepath}")
            os.makedirs(nativepath)

        # Need the interim path, as we re-use it when crafting
        # per-triplet install destinations.
        dstdir_parent = os.path.join(nativepath, 'installed')

        if not path_check(dstdir_parent):
            clog.info("Install directory does not exist, creating: "
               f"{dstdir_parent}")
            os.makedirs(dstdir_parent)

        final_forge_dirs = []

        for triplet in fe_vcpkg_triplets:

            # Recreate the (same) manifest file within each triplet destination
            # directory, since vcpkg.exe is .. weird about its behavior when it
            # is run, even for seemingly passive actions like `vcpkg.exe list`.
            # For example:
            # - it doesn't work as intended unless run within the same
            # directory where the vcpkg.json file exists.
            # - wherever it is run (regardless of whether vcpkg.json is in the
            # cwd), it will always result in the creation of some debris
            # content (another 'vcpkg' dir, and inside that some metadata,
            # presumably). This content exists legitimately in the destination
            # directory we're using for the 'install' actions, but any
            # occurrence outside of those directories is superfluous and
            # misleading.
            #
            # SO, in an attempt to minimize this phenomenon, we put the
            # manifest file within the triplet destination directory, and hope
            # to encourage users to only ever run vcpkg.exe from those
            # locations (if in fact they're running it themselves at all).
            #
            # Our manifest file is ephemeral anyway, and we scm-ignore the
            # entirety of the native install directories, so this strategy at
            # least fits in nicely with that.

            dstdir = os.path.join(dstdir_parent, triplet)

            if not path_check(dstdir):
                clog.info(
                    f"Triplet directory does not exist, creating: {dstdir}")
                os.makedirs(dstdir)

            create_manifest(
                packages,
                os.path.join(dstdir, 'vcpkg.json'),
                fe_vcpkg_baseline_id,
            )

            # -----------------------------------------------------------------
            # investigate baseline problems
            cmd = [
                'vcpkg.exe',
                'x-update-baseline',
            ]

            clog.info(f"Update registry baseline availability")
            clog.debug(' '.join(cmd))

            proc = subprocess.run(cmd, cwd=dstdir,
                capture_output=True, encoding='utf-8')
            rc = proc.returncode
            clog.info(proc.stdout)

            if rc != SYS_SUCCESS:
                raise BuildFail(f"vcpkg x-update-baseline failed ({rc})")

            # -----------------------------------------------------------------
            cmd = [
                'vcpkg.exe',
                'install',
                '--clean-after-build',
                '--no-print-usage',
                f'--triplet={triplet}',
                '--x-install-root=.',
            ]

            clog.info(f"Building packages for triplet: {triplet}")
            clog.context(f"Triplet installation destination: {dstdir}")

            clog.debug(' '.join(cmd))

            proc = subprocess.Popen(cmd, cwd=dstdir,
                stdout=sys.stdout, stderr=sys.stderr,
                bufsize=1, text=True,
            )

            # TODO: The poll approach seemed to cause an issue when this runs
            # in the gitlab CI runner. Revisit and find out if it was really
            # this, and if so why. Note that when poll() was used, the stdout
            # and sterr args in the Popen() call above were set to
            # subprocess.PIPE instead (per normal use). When using wait()
            # instead, the direct passthrough of sys.* is needed to not lose
            # the output.
            proc.wait()
            #while (proc.poll() is None):
                #err = proc.stderr.readlines()
                #out = proc.stdout.readlines()
                #if err:
                #    clog.error('\n'.join(err))
                #if out:
                #    clog.debug('\n'.join(out))

            rc = proc.returncode

            if rc != SYS_SUCCESS:
                raise BuildFail(f"vcpkg native build process failed ({rc})")

            # Yes, there's an extra triplet dir, after vcpkg is done actually
            # installing where we tell it to install.
            final_dir = os.path.join(dstdir, triplet)
            final_forge_dirs.append(final_dir)

        if fe_vcpkg_install not in final_forge_dirs:
            clog.warning(
                "WARNING not configured to use any of installed bundles:")
            clog.warning('\n'.join([f"\t{d}" for d in final_forge_dirs]))
            clog.warning('\n'.join(
                ["FE_VCPKG_INSTALL is set to:", f"\t{fe_vcpkg_install}"]))

        return

    clog.error(f"unknown vcpkg argument: '{directive}'")

    return

# -----------------------------------------------------------------------------
if __name__ == "__main__" :

    rc = SYS_SUCCESS

    directive = sys.argv[1]

    # Build()'s only fail indicator is if it throws an exception.
    try:
        Build(directive)
        clog.info(f"Build directive completed: {directive}")
    except Exception as e:
        clog.error(f"Build directive failed: {directive}")
        clog.exception(e)
        rc = SYS_FAIL

    sys.exit(rc)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
