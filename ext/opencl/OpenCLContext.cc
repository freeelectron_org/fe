/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#define FE_OCC_DEBUG	FALSE

namespace fe
{
namespace ext
{

FE_DL_PUBLIC RecursiveMutex OpenCLContext::ms_mutex;
FE_DL_PUBLIC BWORD OpenCLContext::ms_usingGLX=FALSE;
FE_DL_PUBLIC cl_device_id OpenCLContext::ms_clDeviceId=nullptr;
FE_DL_PUBLIC cl_context OpenCLContext::ms_clContext=nullptr;
FE_DL_PUBLIC cl_command_queue OpenCLContext::ms_clCommandQueue=nullptr;
FE_DL_PUBLIC cl_command_queue OpenCLContext::ms_clCommandQueue2=nullptr;

//* static
void OpenCLContext::confirm(sp<Master> a_spMaster,BWORD a_allowGLX)
{
#if FE_OCC_DEBUG
	feLog("OpenCLContext::confirm safe\n");
#endif

	ClassSafe<void>::SafeGuard safeguard(&ms_mutex);

#if FE_OCC_DEBUG
	feLog("OpenCLContext::confirm continue\n");
#endif

	sp<Catalog> spMasterCatalog=a_spMaster->catalog();

	if(spMasterCatalog->catalogOrDefault<void*>("clContext",nullptr) &&
			(ms_usingGLX || !a_allowGLX))
	{
#if FE_OCC_DEBUG
		feLog("OpenCLContext::confirm exists\n");
#endif
		return;
	}

	const BWORD waitForGLX=spMasterCatalog->catalogOrDefault<bool>(
			"hint:openclWaitForGLX",FALSE);
	if(waitForGLX && !a_allowGLX)
	{
#if FE_OCC_DEBUG
		feLog("OpenCLContext::confirm GLX not allowed here\n");
#endif
		return;
	}

	cl_uint platformIdCount=0;
	clGetPlatformIDs(0,NULL,&platformIdCount);

#if FE_OCC_DEBUG
	feLog("platformIdCount %d\n",platformIdCount);
#endif

	if(platformIdCount<1)
	{
		return;
	}

	std::vector<cl_platform_id> platformIdArray(platformIdCount);
	clGetPlatformIDs(platformIdCount,platformIdArray.data(),NULL);

	cl_platform_id clPlatformId=NULL;
	BWORD useGLX(FALSE);
	String platform;
	Real weight(0.0);

	for(I32 platformIdIndex=0;platformIdIndex<I32(platformIdCount);
			platformIdIndex++)
	{
		size_t name_size=0;
		clGetPlatformInfo(platformIdArray[platformIdIndex],
				CL_PLATFORM_NAME,0,NULL,&name_size);

		char* platform_name=new char[name_size+1];
		platform_name[0]=0;
		clGetPlatformInfo(platformIdArray[platformIdIndex],
				CL_PLATFORM_NAME,name_size+1,platform_name,NULL);

		clGetPlatformInfo(platformIdArray[platformIdIndex],
				CL_PLATFORM_VENDOR,0,NULL,&name_size);

		char* vendor_name=new char[name_size+1];
		vendor_name[0]=0;
		clGetPlatformInfo(platformIdArray[platformIdIndex],
				CL_PLATFORM_VENDOR,name_size+1,vendor_name,NULL);

#if FE_OCC_DEBUG
		feLog("platform %d/%d id %p vendor \"%s\" name \"%s\"\n",
				platformIdIndex,platformIdCount,
				platformIdArray[platformIdIndex],
				vendor_name,platform_name);
#endif

		const BWORD matchesNvidia=
				(!strcmp(vendor_name,"NVIDIA Corporation") ||
				!strcmp(platform_name,"NVIDIA CUDA"));
		const BWORD matchesIntel=
				(!strcmp(vendor_name,"Intel(R) Corporation") ||
				!strcmp(platform_name,"Intel(R) OpenCL"));

#if FE_OCC_DEBUG
		feLog("matchesNvidia %d matchesIntel %d\n",matchesNvidia,matchesIntel);
#endif

		Real vendor_weight(1.0);
		if(matchesNvidia)
		{
			vendor_weight=3.0;
		}
		else if(matchesIntel)
		{
			vendor_weight=2.0;
		}

		if(vendor_weight>weight)
		{
			clPlatformId=platformIdArray[platformIdIndex];
			useGLX=matchesNvidia;

			weight=vendor_weight;
			platform=platform_name;
		}

		delete[] vendor_name;
		delete[] platform_name;
	}

	void* pVoidDisplay=
			spMasterCatalog->catalogOrDefault<void*>("xDisplay",NULL);
	void* pVoidGlxContext=
			spMasterCatalog->catalogOrDefault<void*>("glxContext",NULL);

#if FE_OCC_DEBUG
	feLog("pDisplay %p glxContext %p useGLX %d\n",
			pVoidDisplay,pVoidGlxContext,useGLX);
#endif

	if(!pVoidDisplay || !pVoidGlxContext)
	{
		useGLX=FALSE;
	}

	if(waitForGLX && !useGLX)
	{
		//* try again later
#if FE_OCC_DEBUG
		feLog("OpenCLContext::confirm waiting for GLX\n");
#endif
		return;
	}

	cl_device_type clDeviceType=
#ifdef FE_GL_OPENCL
			useGLX? CL_DEVICES_FOR_GL_CONTEXT_KHR:
#endif
			CL_DEVICE_TYPE_ALL;

	cl_uint deviceIdCount=0;
	clGetDeviceIDs(clPlatformId,clDeviceType,
			0,NULL,&deviceIdCount);

#if FE_OCC_DEBUG
	feLog("deviceIdCount %d\n",deviceIdCount);
#endif

	if(deviceIdCount<1)
	{
		return;
	}

	feLog("OpenCLContext::confirm using platform \"%s\" %s GLX link\n",
			platform.c_str(),useGLX? "with": "without");

	std::vector<cl_device_id> deviceIdArray(deviceIdCount);
	cl_int clError=clGetDeviceIDs(clPlatformId,clDeviceType,
			deviceIdCount,deviceIdArray.data(),NULL);
	if(clError)
	{
		feLog("OpenCLContext::confirm"
				" clGetDeviceIDs clError %d\n",clError);
	}

	ms_clDeviceId=deviceIdArray[0];

	feLog("OpenCLContext::confirm ms_clDeviceId %p\n",ms_clDeviceId);

	const cl_context_properties contextProperties[]=
	{
		CL_CONTEXT_PLATFORM,
		reinterpret_cast<cl_context_properties>(clPlatformId),
		0,0
	};
	const cl_context_properties contextPropertiesGLX[]=
	{
#ifdef FE_GL_OPENCL
		CL_GL_CONTEXT_KHR,(cl_context_properties)pVoidGlxContext,
		CL_GLX_DISPLAY_KHR,(cl_context_properties)pVoidDisplay,
#endif
		CL_CONTEXT_PLATFORM,
		reinterpret_cast<cl_context_properties>(clPlatformId),
		0,0
	};

	ms_clContext=clCreateContext(
			useGLX? contextPropertiesGLX: contextProperties,
			deviceIdCount,deviceIdArray.data(),NULL,NULL,&clError);
	if(clError)
	{
		feLog("OpenCLContext::confirm"
				" clCreateContext clError %d\n",clError);
	}

	ms_clCommandQueue=clCreateCommandQueue(ms_clContext,
			ms_clDeviceId,0,&clError);
	if(clError)
	{
		feLog("OpenCLContext::confirm"
				" clCreateCommandQueue %p clError %d\n",
				ms_clCommandQueue,clError);
	}

	//* TEMP consider one cl_command_queue wrapped in a Safe
	ms_clCommandQueue2=clCreateCommandQueue(ms_clContext,
			ms_clDeviceId,0,&clError);
	if(clError)
	{
		feLog("OpenCLContext::confirm"
				" clCreateCommandQueue %p clError %d\n",
				ms_clCommandQueue2,clError);
	}

	spMasterCatalog->catalog<void*>("clDeviceId")=ms_clDeviceId;
	spMasterCatalog->catalog<void*>("clCommandQueue")=ms_clCommandQueue;
	spMasterCatalog->catalog<void*>("clCommandQueue2")=ms_clCommandQueue2;
	spMasterCatalog->catalog<void*>("clContext")=ms_clContext;

	ms_usingGLX=useGLX;
}

} /* namespace ext */
} /* namespace fe */
