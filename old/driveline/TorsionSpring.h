/*	Copyright (C) 2003-2014 Free Electron Organization
	Any use of this software requires a license. */

/** @file */

#ifndef __driveline_TorsionSpring_h__
#define __driveline_TorsionSpring_h__

#include "datatool/datatool.h"
#include "shape/shape.h"
#include "solve/solve.h"
#include "solve/SemiImplicit.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT TorsionSpring : public DrivelineItem,
	public CastableAs<TorsionSpring>
{
	public:
			TorsionSpring(sp<SemiImplicit1D> a_integrator);
virtual		~TorsionSpring(void);
virtual		void accumulate(void);
virtual		bool validate(void);

virtual		void compile(sp<RecordGroup> rg_input,
				std::vector<SemiImplicit1D::Particle> &a_particles,
				SemiImplicit1D::CompileMatrix &a_compileMatrix);

virtual		void pairs(sp<RecordGroup> rg_input,
				SemiImplicit1D::t_pairs &a_pairs);

virtual		void reconfigure(void);

	public:
		class FE_DL_EXPORT Spring
		{
			public:
				t_solve_real				m_restlength;
				t_solve_real				m_stiffness;
				t_solve_real				m_damping;
				t_solve_real				*m_location[2];
				t_solve_real				*m_ratio[2];
				t_solve_real				m_prev_distance;
				t_solve_real				*m_velocity[2];
				t_solve_real				*m_force[2];
				t_solve_real				*m_diagonal_dfdx[2];
				t_solve_real				*m_diagonal_dfdv[2];
				t_solve_real				*m_offdiagonal_dfdx;
				t_solve_real				*m_offdiagonal_dfdv;
				unsigned int				m_flags;
				Record						m_r_parent;
				sp< SemiImplicit1D::t_pair>	m_spPair;
		};

		Spring *spring(const String &a_name);

		hp<SemiImplicit1D>				m_integrator;

		std::vector<Spring>				m_springs;
		std::map<String, Spring *>		m_springmap;

		t_solve_real					m_perturbation;

AsGearTrain			m_asGearTrain;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_TorsionSpring_h__ */
