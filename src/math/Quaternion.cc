/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

namespace fe
{

#define FE_SPIN_DEBUG	FALSE

SpatialQuaternion stepVelocity(const SpatialQuaternion& previous,
		const SpatialQuaternion& desired,
		Real deltaT,Real maxVelocity, Real maxAcceleration)
{
	SpatialQuaternion rotation=desired;

	Real radians;
	Vector<3,Real> axis;
	rotation.computeAngleAxis(radians,axis);
#if FE_SPIN_DEBUG
	feLog("rot %.6G on %s (%s)\n",
			radians/degToRad,print(axis).c_str(),
			print(rotation).c_str());
#endif

	//* limit rotational velocity
	Real maxAngle=degToRad*maxVelocity;
	if(radians>maxAngle)
	{
		set(rotation,maxAngle,axis);
	}

#if FE_SPIN_DEBUG
	rotation.computeAngleAxis(radians,axis);
	feLog("    %.6G on %s (%s)\n",
			radians/degToRad,print(axis).c_str(),
			print(rotation).c_str());
#endif

#if FE_SPIN_DEBUG
	previous.computeAngleAxis(radians,axis);
	feLog("was %.6G on %s (%s)\n",
			radians/degToRad,print(axis).c_str(),
			print(previous).c_str());
#endif
	SpatialQuaternion acceleration=
			(inverse(previous)*rotation);
	acceleration.computeAngleAxis(radians,axis);
#if FE_SPIN_DEBUG
	feLog("acc %.6G on %s (%s)\n",
			radians/degToRad,print(axis).c_str(),
			print(acceleration).c_str());
#endif

	maxAngle=degToRad*maxAcceleration*deltaT;
	if(radians>maxAngle)
	{
		set(acceleration,maxAngle,axis);
	}

	//* TODO predictive deceleration

#if FE_SPIN_DEBUG
	acceleration.computeAngleAxis(radians,axis);
	feLog("    %.6G on %s (%s)\n",
			radians/degToRad,print(axis).c_str(),
			print(acceleration).c_str());
#endif

	SpatialQuaternion result=previous*acceleration;

#if FE_SPIN_DEBUG
	m_scannerRV.rotVelocity().computeAngleAxis(radians,axis);
	feLog("res %.6G on %s (%s)\n",
			radians/degToRad,print(axis).c_str(),
			print(result).c_str());
#endif

	normalize(result);
	return result;
}

} // namespace
