/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceCylinder_h__
#define __surface_SurfaceCylinder_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Cylindrical Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceCylinder:
	public SurfaceDisk,
	public CastableAs<SurfaceCylinder>
{
	protected:
	class Impact:
		public SurfaceDisk::Impact,
		public CastableAs<Impact>
	{
		public:
							Impact(void)
							{
#if FE_COUNTED_STORE_TRACKER
								setName("SurfaceCylinder::Impact");
#endif
							}
	virtual					~Impact(void)									{}

	virtual	SpatialVector	normal(void)
							{
								if(!m_resolved)
								{
									resolve();
								}
								return m_normal;
							}

	virtual	SpatialVector	axis(void)
							{	return m_axis; }
			void			setAxis(SpatialVector a_axis)
							{	m_axis=a_axis; }

	virtual	Real			along(void)
							{	return m_along; }
			void			setAlong(Real a_along)
							{	m_along=a_along; }

		protected:
			SpatialVector	m_axis;
			Real			m_along;

	};
	public:
							SurfaceCylinder(void);
virtual						~SurfaceCylinder(void)							{}

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							using SurfaceDisk::sample;

							//* As SurfaceI
virtual SpatialTransform	sample(Vector2 a_uv) const;

							using SurfaceDisk::nearestPoint;

virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance) const
							{
								SpatialVector direction;
								SpatialVector intersection;
								Real along(Real(0));
								Real distance=
										PointCylinderNearest<Real>::solve(
										m_location,m_span,m_endRadius,
										a_origin,direction,along,
										intersection);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=
										m_cylinderImpactPool.get();
								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_location);
								spImpact->setAxis(m_span);
								spImpact->setRadius(m_endRadius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(direction);
								spImpact->setDistance(distance);
								spImpact->setIntersectionLocal(intersection);
								spImpact->setAlong(along);
								return spImpact;
							}

							using SurfaceDisk::rayImpact;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const
							{
								SpatialVector intersection;
								Real along(Real(0));
								Real distance=
										RayCylinderIntersect<Real>::solve(
										m_location,m_span,m_endRadius,
										a_origin,a_direction,along,
										intersection);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=
										m_cylinderImpactPool.get();
								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_location);
								spImpact->setAxis(m_span);
								spImpact->setRadius(m_endRadius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(a_direction);
								spImpact->setDistance(distance);
								spImpact->setIntersectionLocal(intersection);
								spImpact->setAlong(along);
								return spImpact;
							}

							using SurfaceDisk::draw;

virtual	void				draw(const SpatialTransform& a_transform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_color,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const;

		void				setLocation(const SpatialVector& a_rLocation)
							{	m_location=a_rLocation;
								updateSphere(); }
		SpatialVector		location(void)
							{	checkCache();
								return m_location; }
		SpatialVector		location(void) const
							{	return m_location; }

		void				setBaseRadius(Real a_baseRadius)
							{	m_baseRadius=a_baseRadius;
								updateSphere(); }
		Real				baseRadius(void)
							{	checkCache();
								return m_baseRadius; }
		Real				baseRadius(void) const
							{	return m_baseRadius; }

		void				setEndRadius(Real a_endRadius)
							{	m_endRadius=a_endRadius;
								updateSphere(); }
		Real				endRadius(void)
							{	checkCache();
								return m_endRadius; }
		Real				endRadius(void) const
							{	return m_endRadius; }

	protected:

virtual	void				updateSphere(void);
virtual	void				cache(void);
virtual	void				resolveImpact(sp<ImpactI> a_spImpactI) const
							{
								// TODO m_baseRadius and m_endRadius scaling

								sp<Impact> spImpact(a_spImpactI);

								// TEMP no nearest
								if(!spImpact.isValid())
								{
									return;
								}

								SpatialVector normal;
								RayCylinderIntersect<Real>::resolveContact(
										spImpact->locationLocal(),
										spImpact->axis(),
										spImpact->radius(),
										spImpact->origin(),
										spImpact->direction(),
										spImpact->distance(),
										spImpact->along(),
										spImpact->intersectionLocal(),
										normal);
								spImpact->setNormalLocal(normal);
							}

		SpatialVector		m_location;
		Real				m_baseRadius;
		Real				m_endRadius;

	private:
mutable	CountedPool<Impact>	m_cylinderImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceCylinder_h__ */

