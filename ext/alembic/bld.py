import sys
import os
import glob
import re

import utility
import common
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):
    module.summary = []

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")

    test_dllname = ""

    compilers = utility.safer_eval(os.environ["FE_COMPILERS"])

    compiler = ''
    compilers_allowed = [ "gcc48" ]
    for check_version in compilers_allowed:
        if check_version in compilers:
            compiler = compilers[check_version]
            break

    alembic_sources = utility.safer_eval(os.environ["FE_ALEMBIC_SOURCES"])

    if alembic_sources == []:
        module.summary += [ "none" ]
        return

    for alembic_source in reversed(sorted(alembic_sources)):
        alembic_source_version = alembic_source[0]
#       alembic_source_suffix = alembic_source[1]
        alembic_source_inc = alembic_source[2]
        alembic_source_libs = alembic_source[3]

        if not os.path.exists(alembic_source_inc) or not os.path.exists(alembic_source_libs):
            module.summary += [ "-" + alembic_source_version ]
            continue

        alembic_static = False
        alembic_combo = False
        alembic_lib = common.find_alembic_lib(alembic_source_libs)
        if alembic_lib == '':
            alembic_combo = True
            alembic_lib = common.find_alembic_combo(alembic_source_libs)
            if alembic_lib == '':
                alembic_static = True
                alembic_combo = False
                alembic_lib = common.find_alembic_static(alembic_source_libs)
                if alembic_lib == '':
                    module.summary += [ "-" + alembic_source_version ]
                    continue

        if alembic_source_version == "":
            module.summary += [ "/" ]
        else:
            module.summary += [ ("~" if alembic_static else "") + alembic_source_version ]

#       module.summary += [ alembic_lib ]

        srcList = [ "SurfaceAccessibleAbc",
                    "alembic.pmh",
                    "alembicDL" ]

        variant = alembic_source_version
        if variant != "":
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink('.', variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        alembic_version = common.get_alembic_version(alembic_source_inc)

        if alembic_version != variant:
            module.summary += [ "=" + alembic_version ]

        dllname = "fexAlembicDL" + variant

        with open(manifest, 'a') as outfile:
            suffix = ""
            if variant != "":
                suffix = "." + variant
            outfile.write('\tspManifest->catalog<String>(\n'+
                    '\t\t\t"SurfaceAccessibleI.SurfaceAccessibleAbc.fe.abc'+
                    suffix+'")=\n'+
                    '\t\t\t"fexAlembicDL'+variant+'";\n');

        dll = module.DLL( dllname, srcList )

        fromHoudini = False

        if re.compile(r".*\/hfs.*").match(alembic_source[3]):
            fromHoudini = True

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)
            srcTarget.includemap = { 'alembic' : alembic_source_inc }

            alembic_openexr = alembic_source_inc + '/OpenEXR'
            openexr_source = os.environ["FE_OPENEXR_SOURCE"]
            if os.path.exists(alembic_openexr):
                srcTarget.includemap['openexr'] = alembic_openexr
            else:
                alembic_openexr = alembic_source_inc + '/AlembicPrivate/OpenEXR'
                if os.path.exists(alembic_openexr):
                    srcTarget.includemap['openexr'] = alembic_openexr
                elif openexr_source != '':
                    default_openexr = openexr_source + '/include/OpenEXR'
                    if os.path.exists(default_openexr):
                        srcTarget.includemap['openexr'] = default_openexr

            srcTarget.includemap['imath'] = alembic_source_inc + '/Imath'

            if compiler != "":
                srcTarget.cxx = compiler

            srcTarget.cppmap = {}

            # TODO verify what version changed std
#           if int(alembic_version) >= 10600:
#               srcTarget.cppmap['std'] = forge.use_std("c++11")
#           else:
#               srcTarget.cppmap['std'] = forge.use_std("c++98")

            # Houdini hack
            if fromHoudini:
                srcTarget.cppmap["cxx11_abi"] = "-D_GLIBCXX_USE_CXX11_ABI=0"

#~      dll.linkmap = { "alembic" :
#~              "-L" + alembic_source_libs + ' -l' + alembic_lib }
#~      if alembic_source_version == '':
#~          dll.linkmap["alembic"] += ' -Wl,-rpath=' + alembic_source_libs

        alembicLibPath = os.path.join(forge.libPath, 'alembic' + variant)
        if os.path.isdir(alembicLibPath) == 0:
            os.makedirs(alembicLibPath, 0o755)

        dll.linkmap = { "alembic" : "" }

        dll.linkmap["alembic"] += " -Wl,-rpath=" + alembicLibPath + "/"

        dll.linkmap["alembic"] += " -L" + alembic_source_libs + ' -l' + alembic_lib

#!      if not alembic_combo:
#!#         dll.linkmap['alembic'] += ' -lhdf5'
#!          dll.linkmap['alembic'] += ' -lAlembicAbc'
#!#         dll.linkmap['alembic'] += ' -lAlembicAbcCollection'
#!          dll.linkmap['alembic'] += ' -lAlembicAbcCoreAbstract'
#!          dll.linkmap['alembic'] += ' -lAlembicAbcCoreHDF5'
#!          dll.linkmap['alembic'] += ' -lAlembicAbcGeom'
#!#         dll.linkmap['alembic'] += ' -lAlembicAbcMaterial'
#!#         dll.linkmap['alembic'] += ' -lAlembicIex'
#!
#!          if int(alembic_version) >= 10500:
#!              dll.linkmap['alembic'] += ' -lAlembicAbcCoreFactory'
#!
#!          if os.path.exists(alembic_source_libs + "/libAlembicOgawa.so") or os.path.exists(alembic_source_libs + "/libAlembicOgawa.a"):
#!              dll.linkmap['alembic'] += ' -lAlembicAbcCoreOgawa'
#!              dll.linkmap['alembic'] += ' -lAlembicOgawa'
#!              dll.linkmap['alembic'] += ' -lAlembicUtil'
#!
#!          if os.path.exists(alembic_source_libs + "/libAlembicAbcOpenGL.so"):
#!              dll.linkmap['alembic'] += ' -lAlembicAbcOpenGL'
#!
#!      if os.path.exists(alembic_source_libs + "/libHalf.so"):
#!          dll.linkmap['alembic'] += ' -lHalf'
#!
#!      if os.path.exists(alembic_source_libs + "/libIex.so"):
#!          dll.linkmap['alembic'] += ' -lIex'

        candidates = [
                        "hdf5",
                        "AlembicAbcCollection",
                        "AlembicAbcCoreAbstract",
                        "AlembicAbcCoreFactory",
                        "AlembicAbcCoreHDF5",
                        "AlembicAbcGeom",
                        "AlembicAbcMaterial",
                        "AlembicIex",
                        "AlembicOgawa",
                        "AlembicAbcCoreOgawa",
                        "AlembicUtil",
                        "AlembicAbcOpenGL",
                        "Half",
                        "Iex" ]

        for candidate in candidates:
            if os.path.exists(alembic_source_libs + "/lib" + candidate + ".so") or os.path.exists(alembic_source_libs + "/lib" + candidate + ".a"):
                dll.linkmap['alembic'] += " -l" + candidate

#!      alembicDsos = os.listdir(alembic_source_libs)
#!
#!      # NOTE Houdini dso paths have lots of other stuff (hundreds of files)
#!      if len(alembicDsos) < 20:
#!          module.summary += [ "&" ]
#!
#!          # WARNING any rpath can bring in undesired alternatives of other libraries
#!          dll.linkmap["alembic"] += " -Wl,-rpath='" + alembic_source_libs + "/'"

        alembicPatterns = [
                        "hdf5",
                        "jemalloc",
                        "Alembic",
                        "Half",
                        "Iex",
                        "Imath",
                        "Ilm" ]

        alembicLibs = [ ]

        for alembicPattern in alembicPatterns:
            liblist = glob.glob(alembic_source_libs + '/lib' + alembicPattern + '*')
            for lib in liblist:
                alembicLib = os.path.basename(lib)
                alembicLibs += [ alembicLib ]

        for alembicLib in alembicLibs:
            src = os.path.join(alembic_source_libs, alembicLib)
            dest = os.path.join(alembicLibPath, alembicLib)
            if os.path.lexists(dest) == 0:
                os.symlink(src, dest)

        deplibs = forge.corelibs + [
                    "fexSurfaceDLLib" ]

        forge.deps( [ dllname + "Lib" ], deplibs )

        if test_dllname == "":
            test_dllname = dllname

    if test_dllname != "":
        forge.tests += [
            ("inspect.exe",     test_dllname,               None,       None) ]

    module.Module('test')
