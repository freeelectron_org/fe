/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MayaNode_h__
#define __maya_MayaNode_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Maya node

	@ingroup maya
*//***************************************************************************/
class MayaNode:
	public MPxDeformerNode,
	public MetaPlugin
{
	private:

	class Parameter
	{
		public:
			MObject						m_object;
			MObject						m_objectPaint;
			MObject						m_objectPaintList;
	};
	class Registration
	{
		public:
			U32							m_uniqueId;
			String						m_implementation;
			std::map<String,Parameter>	m_parameterMap;
			std::map<String,String>		m_cleanMap;
			Array<String>				m_inputChannelArray;
			Array<String>				m_outputChannelArray;
			BWORD						m_autoOutput;
	};

	public:

	class Brush
	{
		public:
			MayaNode*					m_pMayaNode;
	};

								MayaNode(void);
virtual							~MayaNode(void);

virtual	void					dirty(BWORD a_aggressive);
virtual	BWORD					interrupted(void);

virtual	void					postConstructor(void);

static	void*					create(void);

static	void					preRegister(U32 a_uniqueId,String a_name,
										String a_implementation);
static	MStatus					initialize(void);
static	void					postRegister(void);

virtual	MStatus					setDependentsDirty(
										const MPlug &a_rPlugBeingDirtied,
										MPlugArray& a_rAffectedPlugs);
virtual	MStatus					compute(const MPlug& a_rPlug,
										MDataBlock& a_rData);
virtual MStatus					shouldSave(const MPlug& a_rPlug,
										bool& a_rResult);

								///	@internal
		void					brush(WindowEvent& a_rEvent,
										const SpatialVector& a_rRayOrigin,
										const SpatialVector& a_rRayDirection);

const	String					prompt(void);

const	String					nodeBeforeShape(void);

static	std::map<String,Brush>			ms_brushMap;
static	std::map<String,Brush>			ms_shapeMap;

	protected:
virtual	String							evalParamString(String a_key,
												Real a_time);

	private:
		void							updateComponents(BWORD a_constructing,
												BWORD a_doInput);
		void							updateComponentArrays(
												I32 a_iterateIndex,
												I32 a_iterateCount);
		void							precompute(Real a_time);
static	void							populateOperator(String a_name);
		void							updateCatalog(MDataBlock& a_rData,
											Real a_time,BWORD a_doInput);
		void							relayErrors(void);
		void							restoreState(void);

static	I32								ms_registerLock;	//* TODO threadsafe
static	String							ms_registerName;

static	MObject							ms_compoundBehavior;

static	std::map<String,Registration>	ms_mayaRegistry;
static	std::map<String,MayaNode*>		ms_shape;

		MComputation					m_computation;

		Array< sp<Component> >			m_originalInputArray;
		sp<Component>					m_spOriginalOutput;

		MayaContext						m_mayaContext;

		String							m_typeName;
		String							m_nodeName;
		String							m_implementation;
		String							m_shapeNode;

		U32								m_paintSize;
		BWORD							m_bound;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MayaNode_h__ */
