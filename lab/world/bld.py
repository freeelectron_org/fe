import sys
import os
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "world.pmh",
                "WorldPipeline",
                "WorldWindow",
                "SimpleGrid",
                "DrawAtoms",
                "DrawPairs",
                "DrawTransform",
                "FormDrag",
                "Gravity",
                "ClientPipeline",
                "worldDL" ]

    dll = module.DLL( "fexWorldDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexSpatialDLLib",
                'fexViewerDLLib',
                "fexSolveDLLib",
                "fexMechanicsDLLib",
                'fexWindowLib',
                'fexWindowDLLib' ]

    forge.deps( ["fexWorldDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexWorldDL",           None,       None) ]

    module.Module( 'test' )
