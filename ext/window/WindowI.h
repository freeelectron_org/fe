/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_WindowI_h__
#define __window_WindowI_h__

namespace fe
{
namespace ext
{

#ifndef FE_EVENT_LAYOUT
#define	FE_EVENT_LAYOUT		"WindowEvent"
#endif

/**************************************************************************//**
	@brief General interface to a window

	@ingroup window
*//***************************************************************************/
class FE_DL_EXPORT WindowI:
	virtual public Component,
	public CastableAs<WindowI>
{
	public:
virtual	Result				open(const String title)						=0;
virtual	Result				close(void)										=0;

virtual	const Box2i&		geometry(void)									=0;
virtual	void				setPosition(I32 x,I32 y)						=0;
virtual	void				setSize(U32 x,U32 y)							=0;
virtual	void				setTitle(const String &title)					=0;

virtual void				makeCurrent(void)								=0;
virtual void				releaseCurrent(void)							=0;
virtual	void				setBackground(const Color& color)				=0;
virtual	const Color&		background(void) const							=0;
virtual void				clear(void)										=0;
virtual void				swapBuffers(void)								=0;

virtual	sp<Component>		getEventContextI(void)							=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __window_WindowI_h__ */
