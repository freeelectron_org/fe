/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "evaluate.pmh"
#include "platform/dlCore.cc"

#include "evaluate/Evaluator.h"
#include "evaluate/Function.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<Evaluator>("EvaluatorI.Evaluator.fe");

	pLibrary->add<Smooth>("FunctionI.Smooth.fe");
	pLibrary->add<Compressor>("FunctionI.Compressor.fe");
	pLibrary->add<Multiply>("FunctionI.Multiply.fe");
	pLibrary->add<Add>("FunctionI.Add.fe");
	pLibrary->add<Subtract>("FunctionI.Subtract.fe");
	pLibrary->add<Divide>("FunctionI.Divide.fe");
	pLibrary->add<AbsoluteValue>("FunctionI.AbsoluteValue.fe");

	return pLibrary;
}

}


