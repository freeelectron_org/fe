/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_IFO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void InfluenceOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("valueAttribute")="Cd";
	catalog<String>("valueAttribute","label")="Value Attribute";

	catalog<String>("createAttribute")="Cd";
	catalog<String>("createAttribute","label")="Create Attribute";
	catalog<String>("createAttribute","enabler")="createAttribute";

	catalog<bool>("setColor")=true;
	catalog<String>("setColor","label")="Also Set Color";

	catalog<String>("radiusAttribute")="radius";
	catalog<String>("radiusAttribute","label")="Radius Attribute";

	catalog<bool>("scaleRadius")=false;
	catalog<String>("scaleRadius","label")="Scale Radius";
	catalog<bool>("scaleRadius","joined")=true;

	catalog<Real>("radiusScale")=1.0;
	catalog<Real>("radiusScale","high")=10.0;
	catalog<Real>("radiusScale","max")=1e3;
	catalog<String>("radiusScale","label")="By";
	catalog<String>("radiusScale","enabler")="scaleRadius";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");

	catalog< sp<Component> >("Influence Surface");
}

void InfluenceOp::handle(Record& a_rSignal)
{
#if FE_IFO_DEBUG
	feLog("InfluenceOp::handle\n");
#endif

	const String valueAttribute=catalog<String>("valueAttribute");
	const String createAttribute=catalog<String>("createAttribute");
	const String radiusAttribute=catalog<String>("radiusAttribute");
	const BWORD scaleRadius=catalog<bool>("scaleRadius");
	const Real radiusScale=scaleRadius? catalog<Real>("radiusScale"): 1.0;

	catalog<String>("summary")=valueAttribute;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoints;
	if(!access(spOutputPoints,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputColor;
	if(!access(spOutputColor,spOutputAccessible,e_point,e_color)) return;

	sp<SurfaceAccessorI> spOutputValue;
	if(!access(spOutputValue,spOutputAccessible,
			e_point,createAttribute)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessibleI> spInfluenceAccessible;
	if(!access(spInfluenceAccessible,"Influence Surface")) return;

	sp<SurfaceAccessorI> spInfluenceVertices;
	if(!access(spInfluenceVertices,spInfluenceAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInfluenceValue;
	access(spInfluenceValue,spInfluenceAccessible,
			e_primitive,valueAttribute,e_warning);

	BWORD realValue=FALSE;
	if(spInfluenceValue.isValid())
	{
		const String attrType=spInfluenceValue->type();
		if(attrType=="real")
		{
			realValue=TRUE;
		}
		else if(!attrType.match("vector."))
		{
			spInfluenceValue=NULL;
		}
		//* TODO other types

#if FE_IFO_DEBUG
		feLog("valueType \"%s\" real %d\n",attrType.c_str(),realValue);
#endif
	}

	sp<SurfaceAccessorI> spInfluenceRadius;
	if(!radiusAttribute.empty())
	{
		access(spInfluenceRadius,spInfluenceAccessible,
				e_primitive,radiusAttribute,e_warning);
	}

	sp<SurfaceAccessibleI> spDriverAccessible;
	if(!access(spDriverAccessible,"Driver Surface")) return;

	sp<SurfaceTrianglesAccessible> spDriverGeodesic=
			registry()->create("*.SurfaceTrianglesAccessible");
	spDriverGeodesic->bind(spDriverAccessible);

	//* NOTE gauging doesn't currently work with quartering
	spDriverGeodesic->setTriangulation(SurfaceI::e_existingPoints);

	const I32 influenceCount=spInfluenceVertices->count();

	Array< sp<SurfaceI::GaugeI> > gaugeArray(influenceCount);
	Array<Real> radiusArray(influenceCount);

	for(I32 influenceIndex=0;influenceIndex<influenceCount;influenceIndex++)
	{
		const I32 subCount=spInfluenceVertices->subCount(influenceIndex);
		if(!subCount)
		{
			continue;
		}

		gaugeArray[influenceIndex]=spDriverGeodesic->gauge();

		const SpatialVector influencePoint=
				spInfluenceVertices->spatialVector(influenceIndex,0);

		Real influenceRadius=radiusScale;
		if(spInfluenceRadius.isValid())
		{
			influenceRadius*=spInfluenceRadius->real(influenceIndex);
		}

#if FE_IFO_DEBUG
		feLog(" influence %d/%d %s radius %.6G\n",
				influenceIndex,influenceCount,c_print(influencePoint),
				influenceRadius);
#endif

		gaugeArray[influenceIndex]->pick(influencePoint,influenceRadius);
		radiusArray[influenceIndex]=influenceRadius;
	}

	const I32 pointCount=spInputPoints->count();
	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		SpatialVector result(0.0,0.0,0.0);

		for(I32 influenceIndex=0;influenceIndex<influenceCount;influenceIndex++)
		{
			sp<SurfaceI::GaugeI>& rspDriverGauge=gaugeArray[influenceIndex];
			if(rspDriverGauge.isNull())
			{
				continue;
			}

			SpatialVector influenceValue(1.0,1.0,1.0);
			if(spInfluenceValue.isValid())
			{
				if(realValue)
				{
					const Real value=spInfluenceValue->real(influenceIndex);
					set(influenceValue,value,value,value);
				}
				else
				{
					influenceValue=
							spInfluenceValue->spatialVector(influenceIndex);
				}
			}

			const Real influenceRadius=radiusArray[influenceIndex];
			const Real invRadius=
					influenceRadius>0.0? 1.0/influenceRadius: 1.0;

			const SpatialVector point=
					spInputPoints->spatialVector(pointIndex);

			const Real distance=rspDriverGauge->distanceTo(point);

			if(distance>=0.0 && distance!=INFINITY)
			{
				result[0]=fe::minimum(Real(1),result[0]+fe::maximum(Real(0),
						influenceValue[0]*(Real(1)-distance*invRadius)));
				result[1]=fe::minimum(Real(1),result[1]+fe::maximum(Real(0),
						influenceValue[1]*(Real(1)-distance*invRadius)));
				result[2]=fe::minimum(Real(1),result[2]+fe::maximum(Real(0),
						influenceValue[2]*(Real(1)-distance*invRadius)));

//				feLog("point %d/%d inf %d/%d %s dist %.6G %.6G result %s\n",
//						pointIndex,pointCount,influenceIndex,influenceCount,
//						c_print(influenceValue),distance,invRadius,
//						c_print(result));
			}
		}

		if(realValue)
		{
			spOutputValue->set(pointIndex,result[0]);
		}
		else
		{
			spOutputValue->set(pointIndex,result);
		}
		spOutputColor->set(pointIndex,result);
	}

#if FE_IFO_DEBUG
	feLog("InfluenceOp::handle done\n");
#endif
}
