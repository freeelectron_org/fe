import sys
#import os
forge = sys.modules["forge"]


def setup(module):
    srcList = [ "evaluate.pmh",
                "Evaluator",
                "Function",
                "evaluateDL"
                ]

    dll = module.DLL( "fexEvaluateDL", srcList )

    deplibs = forge.corelibs + [
                "fexSignalLib" ]

    forge.deps( ["fexEvaluateDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexEvaluateDL",            None,       None) ]

    module.Module('test')


