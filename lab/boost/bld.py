import os
import sys
forge = sys.modules["forge"]
import os.path
import glob
import re

def add_libs(name, libs) -> list:
    if forge.fe_compiler == 'FE_MICROSOFT':
        return []

    added = []
    if not name in forge.linkmap:
        forge.linkmap[name] = ''
    for lib in libs:
        sys.stdout.write(' ' + lib)
        forge.linkmap[name] += ' ' + forge.linkformat%('boost_'+lib)
        added += lib
    return added

def setup(module):
    module.summary = []

    look_in = []
    boost_libs = []
    plain_libs = []

#   plain_libs += ['thread']
#   plain_libs += ['system']
#   plain_libs += ['filesystem']
#   if forge.fe_re == "FE_RE_BOOST":
#       plain_libs += ['regex']

    # if you are building Houdini, use their copy of boost
    if 'houdini' in forge.modules_confirmed:
        ht = os.getenv('HT')
        if ht:
            look_in += [ ht + '/../dsolib' ]

    # env var overrides
    boost_include = os.getenv('FE_BOOST_INCLUDE')
    if boost_include:
        forge.includemap['boost'] = boost_include

    boost_lib = os.getenv('FE_BOOST_LIB')
    if boost_lib:
        forge.linkmap['boost'] = ' -L' + boost_lib
        look_in += [ boost_lib ]

    # default
    look_in += [ '/usr/lib' ]

    # find dso to know what to name it
    for name in plain_libs:
        for look in look_in:
            liblist = glob.glob(look + '/libboost_' + name + '*.so')
            liblist.sort()
            if len(liblist):
                #TODO Win32 also
                lib = liblist[len(liblist)-1]
                lib = re.sub('.*/', '', lib)
                lib = re.sub('libboost_', '', lib)
                lib = re.sub('.so', '', lib)
                boost_libs += [ lib ]
                break

    if not len(boost_libs):
        boost_libs = plain_libs

    added = []
    added += add_libs('boost', boost_libs)

#   added += add_libs('boost', ['filesystem', 'thread', 'regex'])
#   added += add_libs('boost', ['filesystem-gcc41-mt', 'thread-gcc41-mt', 'regex-gcc41-mt'])

    module.summary += added
