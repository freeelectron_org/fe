/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_RouteHandler_h__
#define __plan_RouteHandler_h__

#include "signal/signal.h"
#include "shape/shape.h"
#include "plan/planAS.h"
#include "mobility/mobilityAS.h"

namespace fe
{

class FE_DL_EXPORT RouteHandler :
	virtual public HandlerI,
	virtual public Config,
	virtual public Initialize<RouteHandler>
{
	public:
				RouteHandler(void);
virtual			~RouteHandler(void);
virtual	void	initialize(void);

				// AS HandlerI
virtual void	handle(Record &r_sig);

		typedef enum
		{
			e_hold		= 0,
			e_linger	= 1,
			e_move		= 2
		} t_state;

	private:
		class WpInfo
		{
			public:
				std::vector<Record> m_start_routers;
				std::vector<Record> m_end_routers;
		};
		AsRouted		m_asRouted;
		AsRouter		m_asRouter;
		AsBounded		m_asBounded;
		AsWaypoint		m_asWaypoint;
		AsMobile		m_asMobile;
		AsPair			m_asPair;
		AsPoint			m_asPoint;
};


} /* namespace */

#endif /* __plan_RouteHandler_h__ */


