/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __yaml_ManifestReaderYaml_h__
#define __yaml_ManifestReaderYaml_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief load manifest from yaml file

	@ingroup yaml
*//***************************************************************************/
class FE_DL_EXPORT ManifestReaderYaml:
	virtual public ManifestReaderI
{
	public:

virtual	BWORD	load(String a_filename);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __yaml_ManifestReaderYaml_h__ */
