/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

namespace fe
{
namespace ext
{

Executor::Executor(void)
{
	feLog("Executor()\n");
}

Executor::~Executor(void)
{
	feLog("~Executor()\n");
}

void Executor::resolve(void)
{
	feLog("Executor::resolve\n");

	if(m_spPipeline.isValid())
	{
		storeCatalog();
		RecordView::saveRecordGroup(scope(),m_spPipeline,"pipeline_out.rg");
	}
}

void Executor::bind(Record& record)
{
	feLog("Executor::bind(Record&)\n");

	RecordView::bind(record,TRUE);
}

void Executor::setup(int a_argc, char **a_argv, char** a_env)
{
	feLog("Executor::setup\n");

	sp<Master> spMaster=registry()->master();

	// =====================================================================
	// setup configuration variables

	Regex re_assign("(.*)=(.*)");

	for(int i = 1; i < a_argc; i++)
	{
		char* arg=a_argv[i];
		if(re_assign.search(arg))
		{
			spMaster->catalog()->catalog<String>(re_assign.result(1))=
					re_assign.result(2);
		}
		else
		{
			spMaster->catalog()->catalog<String>(arg) = "1";
		}
	}

	loadPlugins();
	restoreCatalog();

	m_spSignalerI=registry()->create("SignalerI");
	sp<Layout> m_spHeartbeat = scope()->declare("heartbeat");
	m_heartbeat = scope()->createRecord(m_spHeartbeat);

	String mediaPath=spMaster->catalog()->catalog<String>("path:media");
	m_spPipeline=RecordView::loadRecordGroup(scope(),
			mediaPath+"/architecture/"+pipeline());

	populateSignaler();

	feLog("Executor::setup pipeline valid %d\n",m_spPipeline.isValid());
}

void Executor::populateSignaler(void)
{
	U32 handlers=0;
	Accessor< cp<Component> > component(scope(), "rec:component");
	for(RecordGroup::iterator it = m_spPipeline->begin();
			it != m_spPipeline->end(); it++)
	{
		sp<RecordArray> spRA = *it;
		if(component.check(spRA))
		{
			for(U32 i=0; i<spRA->length(); i++)
			{
				cp<Component>& rcpComponent=component(spRA,i);
				sp<HandlerI> spHandlerI=rcpComponent;

				if(spHandlerI.isValid())
				{
					m_spSignalerI->insert(spHandlerI,m_spHeartbeat);

					handlers++;
				}
			}
		}
	}
}

void Executor::loadPlugins(void)
{
	sp<Master> spMaster=registry()->master();
	std::vector<String>& rPluglist=pluginList();
	U32 cnt = rPluglist.size();
	for(U32 j=0; j<cnt; j++)
	{
		String plugin=rPluglist[j];
		feLog("Executor::setup loading %s\n",plugin.c_str());
		spMaster->registry()->manage(plugin);
	}
}

void Executor::restoreCatalog(void)
{
	sp<Master> spMaster=registry()->master();
	std::map<String,String>& rMap=catalogMap();
	std::map<String,String>::const_iterator it=rMap.begin();
	while(it!=rMap.end())
	{
		String key=it->first;
		String value=it->second;
		spMaster->catalog()->catalog<String>(key)=value;

		feLog("Executor::restoreCatalog \"%s\" \"%s\"\n",
				key.c_str(),value.c_str());

		it++;
	}
}

void Executor::storeCatalog(void)
{
	sp<Master> spMaster=registry()->master();
	std::map<String,String>& rMap=catalogMap();
	std::map<String,String>::const_iterator it=rMap.begin();
	while(it!=rMap.end())
	{
		String key=it->first;
		String value=spMaster->catalog()->catalog<String>(key);
		rMap[key]=value;

		feLog("Executor::storeCatalog \"%s\" \"%s\"\n",
				key.c_str(),value.c_str());

		it++;
	}
}

bool Executor::step(int &a_returnCode)
{
	m_spSignalerI->signal(m_heartbeat);
	return false;
}

bool Executor::loop(int &a_returnCode)
{
	feLog("Executor::loop\n");

	sp<Master> spMaster=registry()->master();
	spMaster->catalog()->catalog<String>("run:level")="1";
	spMaster->catalog()->catalog<String>("tmp:test")="junk";

	while(step(a_returnCode))
	{
		milliSleep(0);
	}

	resolve();

	return true;
}

} /* namespace ext */
} /* namespace fe */
