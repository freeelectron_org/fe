import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["message", "window"]

def setup(module):
    srcList = [ "BeaconServer",
                "BeaconServerStartup",
                "CRC",
                "beaconServer.pmh",
                "beaconServerDL",
                "GlobalDictionary"
                ]

    dll = module.DLL( "fexBeaconServerDL", srcList )

    deplibs = forge.corelibs + [
                "fexMessageDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "fexDataToolLib" ]

    forge.deps( ["fexBeaconServerDLLib"], deplibs )


    srcList = [ "BeaconClient",
                "BeaconClientStartup",
                "CRC",
                "beaconClient.pmh",
                "beaconClientDL",
                ]

    dll = module.DLL( "fexBeaconClientDL", srcList )

    deplibs = forge.corelibs + [
                "fexMessageDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "fexDataToolLib" ]

    forge.deps( ["fexBeaconClientDLLib"], deplibs )


    srcList = [ "BeaconTimeSync",
                "CRC",
                "beaconTimeSync.pmh",
                "beaconTimeSyncDL",
                ]

    dll = module.DLL( "fexBeaconTimeSyncDL", srcList )

    deplibs = forge.corelibs + [
                "fexMessageDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "fexDataToolLib" ]

    forge.deps( ["fexBeaconTimeSyncDLLib"], deplibs )


    module.Module('test')
