/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_PaletteMap_h__
#define __architecture_PaletteMap_h__

#include "fe/data.h"
namespace fe
{
namespace ext
{

class PaletteMap :
	virtual public PaletteI
{
	public:
						PaletteMap(void);
virtual					~PaletteMap(void);
virtual	void			set(const String &a_name, sp<Component> spCmp);
virtual	sp<Component>	get(const String &a_name);
virtual	void			remove(const String &a_name);

	private:
		std::map<String, sp<Component> >		m_map;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_PaletteMap_h__ */
