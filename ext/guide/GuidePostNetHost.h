/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __guide_GuidePost_h__
#define __guide_GuidePost_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Facilitate NetHost Connections through Beacon

	@ingroup guide
*//***************************************************************************/
class FE_DL_EXPORT GuidePostNetHost:
	public beacon::BeaconClient,
	virtual public GuidePostI,
	public CastableAs<GuidePostNetHost>
{
	public:
							GuidePostNetHost(void);
virtual						~GuidePostNetHost(void);

virtual	BWORD				connect(String a_address);
virtual	void				listSpaces(Array<String>& a_rSpaceList);
virtual	sp<StateCatalog>	connectToSpace(String a_space);
virtual	sp<StateCatalog>	createSpace(String a_space,sp<Catalog> a_spCatalog);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __guide_GuidePost_h__ */
