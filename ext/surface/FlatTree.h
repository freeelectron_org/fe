/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_FlatTree_h__
#define __surface_FlatTree_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Triangular storage using a simple array

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT FlatTree: public SpatialTreeBase
{
	public:

				FlatTree(void);
virtual			~FlatTree(void);

virtual	void	setRefinement(U32 a_refinement)								{}

virtual	void	populate(
						const Vector3i* a_pElementArray,
						const SpatialVector* a_pVertexArray,
						const SpatialVector* a_pNormalArray,
						const Vector2* a_pUVArray,
						const Color* a_pColorArray,
						const I32* a_pTriangleIndexArray,
						const I32* a_pPointIndexArray,
						const I32* a_pPrimitiveIndexArray,
						const I32* a_pPartitionIndexArray,
						U32 a_primitives,U32 a_vertices,
						const SpatialVector& a_center,F32 a_radius);
virtual	Real	nearestPoint(const SpatialVector& a_origin,
						Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
						const sp<PartitionI>& a_rspPartition,
						Array< sp<SpatialTreeI::Hit> >& a_rHitArray)
						const;
virtual	Real	rayImpact(const SpatialVector& a_origin,
						const SpatialVector& a_direction,
						Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
						const sp<PartitionI>& a_rspPartition,
						Array< sp<SpatialTreeI::Hit> >& a_rHitArray)
						const;

	protected:

	class Ball
	{
		public:
			Vector4	m_sphere;
			I32		m_faceIndex;
			I32		m_triangleIndex;
			I32		m_primitiveIndex;
			I32		m_partitionIndex;
	};

		void	nearestAccumulate(I32 a_face,I32 a_triangleIndex,
						I32 a_primitiveIndex,I32 a_partitionIndex,
						const SpatialVector& a_origin,U32 a_hitLimit,
						Array< sp<SpatialTreeI::Hit> >& a_rHitArray,
						const SpatialVector& a_rCenter,Real a_radius,
						const I32* a_pPointIndex,
						const SpatialVector* a_pVertex,
						const SpatialVector* a_pNormal) const;

static	void	addHit(U32 a_hitLimit,
						Array< sp<SpatialTreeI::Hit> >& a_rHitArray,
						CountedPool<Hit>& a_rHitPool,Real a_distance,
						const SpatialVector& a_rDirection,
						const SpatialVector& a_rIntersection,
						I32 a_face,I32 a_triangleIndex,
						I32 a_primitiveIndex,I32 a_partitionIndex,
						const SpatialBary& a_rBarycenter,
						const Vector2& a_rUV,
						const SpatialVector& a_rDu,
						const SpatialVector& a_rDv,
						const Color& a_rColor,
						const I32 a_subCount,
						const I32* a_pPointIndex,
						const SpatialVector* a_pVertex,
						const SpatialVector* a_pNormal);

	struct Sort
	{
		bool operator()(const sp<SpatialTreeI::Hit>& rspHit1,
				const sp<SpatialTreeI::Hit>& rspHit2)
		{
			return (rspHit1->distance() < rspHit2->distance());
		}
	};

		void	cacheSpheres(void);

const	Vector3i*			m_pElementArray;
const	SpatialVector*		m_pVertexArray;
const	SpatialVector*		m_pNormalArray;
const	Vector2*			m_pUVArray;
const	Color*				m_pColorArray;
const	I32*				m_pTriangleIndexArray;
const	I32*				m_pPointIndexArray;
const	I32*				m_pPrimitiveIndexArray;
const	I32*				m_pPartitionIndexArray;
		U32					m_primitives;
		U32					m_vertices;

		Ball*				m_pBallArray;
		U32					m_ballCount; // != primitive count if clipped

mutable	CountedPool<Hit>	m_hitPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_FlatTree_h__ */
