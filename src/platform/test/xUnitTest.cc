/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

int main(void)
{
	//fprintf(stderr, "test\n");
#if 1
	fe::UnitTest unitTest;

	try
	{
		unitTest(1);
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	unitTest.track(1);
	return unitTest.failures();
#endif
}

