/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_FontI_h__
#define __window_FontI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief General interface to a window

	@ingroup window
*//***************************************************************************/
class FE_DL_EXPORT FontI:
	virtual public Component,
	public CastableAs<FontI>
{
	public:

virtual	I32		fontHeight(I32* a_pAscent,I32* a_pDescent)					=0;
virtual	I32		pixelWidth(String a_string)									=0;
virtual	void	drawAlignedText(sp<Component> a_spDrawComponent,
					const SpatialVector& a_location,
					const String a_text,const Color &color)					=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __window_FontI_h__ */
