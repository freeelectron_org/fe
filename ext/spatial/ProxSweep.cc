/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ProxSweep.h"

namespace fe
{
namespace ext
{

ProxSweep::ProxSweep(void)
{
	m_sweepAxis = 1;
}

ProxSweep::~ProxSweep(void)
{
}

void ProxSweep::initialize(void)
{
}

void ProxSweep::setAxis(unsigned int a_d)
{
	m_sweepAxis = a_d;
}

void ProxSweep::setup(sp<Scope> spScope)
{
	m_spScope = spScope;
	m_asParticle.bind(m_spScope);
	m_asBounded.bind(m_spScope);
	m_asProximity.bind(m_spScope);

	m_filters.clear();
	m_filters.push_back(m_asBounded.radius);
	m_filters.push_back(m_asParticle.location);
}

unsigned int ProxSweep::detect(sp<Layout> l_pair, sp<RecordGroup> rg_in, sp<RecordGroup> rg_out)
{
	SystemTicker ticker("ProxSweep");

	unsigned int collisions = 0;

	m_poolIndex = 0;
	if(!m_spPool.isValid())
	{
		//m_spPool = new RecordArray(l_pair);
		m_spPool = l_pair->scope()->createRecordArray(l_pair, 10000);
	}

	m_hpPairLayout = l_pair;

	setup(l_pair->scope());

	t_lrecords lrecords;
	t_minvals minvals;

	LocalRecord lr;
	MinVal minv;

	ticker.log("start");

	// setup vector for sorting
	int index = 0;
	for(RecordGroup::iterator i_in = rg_in->begin(m_filters);
		i_in != rg_in->end(); i_in++)
	{
		sp<RecordArray> ra_in = *i_in;
		lr.m_pRA = ra_in.raw();
		for(int i = 0; i < ra_in->length(); i++)
		{
			lr.m_index = i;
			lr.m_location = m_asParticle.location(ra_in,i);
			lr.m_radius = m_asBounded.radius(ra_in,i);
			lr.m_hi = lr.m_location[m_sweepAxis] + lr.m_radius;
			lrecords.push_back(lr);
			minv.m_key = lr.m_location[m_sweepAxis] - lr.m_radius;
			minv.m_index = index++;
			minvals.push_back(minv);
		}
	}
	ticker.log("setup");

	// sort vector
	std::sort(minvals.begin(),minvals.end());

	ticker.log("sort");


	for(unsigned int i = 0; i < minvals.size(); i++)
	{
		for(unsigned int j = i+1; j < minvals.size(); j++)
		{
			if(lrecords[minvals[i].m_index].m_hi < minvals[j].m_key)
			{
				break;
			}
			if(check(lrecords[minvals[i].m_index],lrecords[minvals[j].m_index]))
			{
				collisions++;
			}
		}
	}

	ticker.log("check");

	if(rg_out.isValid())
	{
		sp<RecordArray> spPA = rg_out->getArray(l_pair);

		spPA->add(m_spPool, 0, m_poolIndex);
	}

	ticker.log("finish");

	return collisions;
}

bool ProxSweep::check(LocalRecord &r_a, LocalRecord &r_b)
{
	SpatialVector dist = r_a.m_location - r_b.m_location;
	Real dist_sq = dist[0]*dist[0] + dist[1]*dist[1] + dist[2]*dist[2];
	Real rad_sq = r_a.m_radius + r_b.m_radius;

	rad_sq *= rad_sq;
	if(dist_sq < rad_sq)
	{

		FEASSERT((int)m_poolIndex <= m_spPool->length());
		if((int)m_poolIndex == m_spPool->length())
		{
			m_spPool->add(m_spScope->createRecord(m_hpPairLayout));
		}

		r_a.m_pRA->set(m_asProximity.left(m_spPool, m_poolIndex), r_a.m_index);
		r_b.m_pRA->set(m_asProximity.right(m_spPool, m_poolIndex), r_b.m_index);

		m_poolIndex++;
		return true;
	}
	return false;
}


} /* namespace ext */
} /* namespace fe */

