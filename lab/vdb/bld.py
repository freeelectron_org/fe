import os
import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def find_vdb_so():
    openvdb_lib = os.environ["FE_OPENVDB_LIB"]

    if openvdb_lib != "auto":
        libFiles = os.listdir(openvdb_lib)
        for libFile in libFiles:
            if re.match(".*vdb.*\.so", libFile):
                return libFile

    return "libopenvdb.so"

def find_houdini_vdb():
    houdini_source_index = len(forge.houdini_sources)-1
    while houdini_source_index >= 0:
        houdini_source = forge.houdini_sources[houdini_source_index]
        houdini_source_version = None

        tokens = houdini_source[0].split(".")
        if len(tokens) > 1:
            houdini_source_version = float(tokens[0]+"."+tokens[1])
        elif type(houdini_source[0]) == int:
            houdini_source_version = int(houdini_source[0])

        # avoid version with Houdini 14.0 until it gets ironed out
        if houdini_source_version and houdini_source_version!=14:
            return houdini_source

        houdini_source_index -= 1

    return None

def setup(module):
    module.summary = []

    openvdb_include = os.environ["FE_OPENVDB_INCLUDE"]
    openvdb_lib = os.environ["FE_OPENVDB_LIB"]

    srcList = [ "SurfaceAccessibleVDB",
                "SurfaceVDB",
                "vdb.pmh",
                "vdbDL" ]

    dll = module.DLL( "fexVdbDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib" ]

    forge.deps( ["fexVdbDLLib"], deplibs )

    module.cppmap = {}
    module.cppmap['std'] = forge.use_std("c++0x")

    module.includemap = {}
    module.includemap['vdb'] = openvdb_include
    module.includemap['vdb2'] = os.path.join(openvdb_include, '..')

    default_openexr = os.environ["FE_OPENEXR_SOURCE"] + '/include'
    if os.path.exists(default_openexr):
        module.includemap['openexr'] = default_openexr

    # HACK needs to be configurable
    module.linkmap = {}

    # HACK use boost and OpenEXR from last listed houdini version
    houdini_source = find_houdini_vdb()
    if houdini_source:
        module.includemap['zzz_houdini'] = houdini_source[1]
        module.linkmap['zzz_houdini'] = '-L' + houdini_source[2]

#       module.linkmap['vdb'] = ' -lopenvdb'
#   else:

    soFile = find_vdb_so()

    module.linkmap['vdb'] = "-Wl,-rpath='" + openvdb_lib + "'"
    module.linkmap['vdb'] += ' -L' + openvdb_lib + ' -l:' + soFile

    forge.tests += [
        ("inspect.exe",     "fexVdbDL",             None,       None) ]

    for line in open(openvdb_include + "/version.h").readlines():
        if re.match("#define OPENVDB_VERSION_NAME.*", line):
            version = line.split(' ')[2][1:].rstrip()
            module.summary += [ version ]
            break

#   module.Module('openvdb')

def auto(module):
    openvdb_include = os.environ["FE_OPENVDB_INCLUDE"]
    openvdb_lib = os.environ["FE_OPENVDB_LIB"]

    if openvdb_include == '':
        return 'unset'

    test_file = """
#include "openvdb/openvdb.h"

int main(void)
{
    return(0);
}
    \n"""

    houdini_source = find_houdini_vdb()
    if houdini_source:
        forge.includemap['zzz_houdini'] = houdini_source[1]
        forge.linkmap['zzz_houdini'] = '-L' + houdini_source[2]

    if houdini_source:
        if openvdb_include == 'auto':
            openvdb_include = houdini_source[1] + '/openvdb'
        if openvdb_lib == 'auto':
            openvdb_lib = houdini_source[2]

    if openvdb_include == 'auto' or openvdb_lib == 'auto':
        return 'not found'

    os.environ["FE_OPENVDB_INCLUDE"] = openvdb_include
    os.environ["FE_OPENVDB_LIB"] = openvdb_lib

    std_orig = forge.cppmap.get('std', "")
    forge.cppmap['std'] = forge.use_std("c++0x")

    forge.includemap['vdb'] = openvdb_include
    forge.includemap['vdb2'] = os.path.join(openvdb_include, '..')

    default_openexr = os.environ["FE_OPENEXR_SOURCE"] + '/include'
    if os.path.exists(default_openexr):
        forge.includemap['openexr'] = default_openexr

    soFile = find_vdb_so()

    forge.linkmap['vdb'] = "-Wl,-rpath='" + openvdb_lib + "'"
    forge.linkmap['vdb'] += ' -L' + openvdb_lib + ' -l:' + soFile
    forge.linkmap['vdb'] += ' -ltbb'

    result = forge.cctest(test_file)

    forge.cppmap['std'] = std_orig

    forge.includemap.pop('zzz_houdini', None)
    forge.linkmap.pop('zzz_houdini', None)
    forge.includemap.pop('openexr', None)
    forge.includemap.pop('vdb', None)
    forge.includemap.pop('vdb2', None)
    forge.linkmap.pop('vdb', None)

    return result
