import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = [ "fePlatformLib",
                "feMemoryLib",
                "feCoreLib" ]

    tests = [   'xAllocation',
                'xArray',
                'xCounted',
                'xDesignators',
                'xFileLog',
                'xHandled',
                'xInstanceMap',
                'xInitialized',
                'xList',
                'xMutex',
                'xProfiler',
                'xProtect',
                'xStringSpeed',
                'xTick',
                'xType',
                'xTypeMaster' ]

#   if forge.fe_compiler == 'FE_GNU' and forge.compiler_version >= 4.3:
#           tests += [ "xNamedArg" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            deplibs += [ "feImportMemoryLib" ]

    for test in tests:
        target = module.Exe(test)
        forge.deps([test + "Exe"], deplibs)

        if test == "xDesignators":
            srcTarget = module.FindObjTargetForSrc(test)
            srcTarget.cppmap = { 'std': forge.use_std("c++20") }

    forge.tests += [
        ("xArray.exe",          "",         None,       None),
        ("xList.exe",           "",         None,       None),
        ("xMutex.exe",          "",         None,       None),
        ("xAllocation.exe",     "",         None,       None),
        ("xFileLog.exe",        "",         None,       None),
        ("xHandled.exe",        "",         None,       None),
        ("xInstanceMap.exe",    "",         None,       None),
        ("xDesignators.exe",    "",         None,       None),
#       ("xNamedArg.exe",       "",         None,       None),
#       ("xProfiler.exe",       "100",      None,       None),
        ("xProtect.exe",        "",         None,       None),
        ("xInitialized.exe",    "",         None,       None),
        ("xTick.exe",           "",         None,       None),
        ("xType.exe",           "",         None,       None),
        ("xTypeMaster.exe",     "",         None,       None) ]

    if forge.fe_os == 'FE_LINUX':
        forge.tests += [
            ("xCounted.exe",        "",         None,       None) ]

