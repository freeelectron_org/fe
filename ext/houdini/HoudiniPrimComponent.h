/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

//*	derived from Houndini sample: PrimSurface_Surface

#ifndef __houdini_HoudiniPrimComponent_h__
#define __houdini_HoudiniPrimComponent_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini primitive for generic Component

	@ingroup houdini
*//***************************************************************************/
class HoudiniPrimComponent: public HoudiniPrimBase
{
	public:
								HoudiniPrimComponent(GA_Detail& d,
									GA_Offset offset=GA_INVALID_OFFSET);

								HoudiniPrimComponent(const GA_MergeMap& map,
									GA_Detail& detail,
									GA_Offset offset,
									const HoudiniPrimComponent& src);

virtual							~HoudiniPrimComponent(void);

virtual
const	GA_PrimitiveDefinition&	getTypeDef(void) const
								{	return *ms_pDefinition; }

virtual	void					copyPrimitive(const GEO_Primitive* src);
virtual	void					copyOffsetPrimitive(const GEO_Primitive* src,
									GA_Index basept);
virtual	void					copyUnwiredForMerge(const GA_Primitive *src,
									const GA_MergeMap& map);

static
const	GA_PrimitiveTypeId&		getTypeId(void)
								{	return ms_pDefinition->getId(); }

static	void					registerMyself(GA_PrimitiveFactory* pFactory);

		void					bind(sp<Component> a_spPayload)
								{	m_spPayload=a_spPayload; }

		sp<Component>			payload(void) const	{ return m_spPayload; }

	protected:

		GA_DECLARE_INTRINSICS()

	private:

static	GA_PrimitiveDefinition*	ms_pDefinition;

		sp<Component>			m_spPayload;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniPrimComponent_h__ */
