/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconTimeSyncI_h__
#define __BeaconTimeSyncI_h__

namespace beacon
{

/**************************************************************************//**
	@brief Beacon for time synchronization

	@ingroup beacon
*//***************************************************************************/
class FE_DL_EXPORT BeaconTimeSyncI:
	virtual public fe::Component,
	public fe::CastableAs<BeaconTimeSyncI>
{
public:
	/// @brief	Does time synchronization with the Beacon
	///			without registering it
	virtual bool timeSyncWithBeacon(const char *ipAddress,
									const uint16_t requestPort) = 0;

	/// @brief	Returns the time offset between the clock on the Beacon
	///			and the local processor
	virtual int64_t getTimeOffset() = 0;

	/// @brief	Returns the current time plus the offset to give you
	///			the Beacon time
	virtual int64_t getBeaconTime() = 0;

	/// @brief	Returns the estimated latency between the server and the client
	virtual int64_t getLatency() = 0;
};

} // namespace beacon

#endif // __BeaconTimeSyncI_h__
