/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceI_h__
#define __surface_SurfaceI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Spatial boundary

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceI:
	virtual public Component,
	public CastableAs<SurfaceI>
{
	public:
		enum	SampleMethod
				{
					e_flat,
					e_linear,
					e_pointNormal,
					e_spline
				};

		enum	Containment
				{
					e_unknown,
					e_inside,
					e_outside
				};

		enum	Accuracy
				{
					e_sphere,
					e_triangle
				};

		enum	Triangulation
				{
					e_existingPoints,
					e_balanced
				};

		enum	Restrictions
				{
					e_unrestricted=		0x0,
					e_excludeCurves=	0x1,
					e_excludePolygons=	0x2,
				};

	/// @brief results of a surface intersection
	class FE_DL_EXPORT ImpactI:
		virtual public Component,
		public CastableAs<ImpactI>
	{
		public:
	virtual	SpatialVector		origin(void) const							=0;

	virtual	SpatialVector		direction(void) const						=0;

	virtual	Real				distance(void) const						=0;

	virtual	SpatialVector		intersection(void)							=0;

	virtual	SpatialVector		normal(void)								=0;

								/// @brief Internal search index
	virtual	I32					face(void)									=0;

								/** @brief Barycentric element index

									Use this for barycentric sampling.
									For surfaces converted into triangles,
									this is simply that index.
									For curves, this is really
									the curve index. */
	virtual	I32					triangleIndex(void)							=0;

								/** @brief Source's atomic index

									This is an index specific to where
									the pickable element came from.
									One index may be for a free triangle,
									a pair of triangles from a quad,
									a pure quadric, a full NURBS patch,
									a whole curve, or whatever arbitrary
									collective was determined relevant by
									the system it came from. */
	virtual	I32					primitiveIndex(void)						=0;

								/** @brief numerical enumeration
									of the named partitions */
	virtual	I32					partitionIndex(void)						=0;

								/** @brief Position within a triangle or curve

									For triangluar representations,
									this is the barycentric coordinate.
									For curves, the first vector element
									is the unit distance along the curve. */
	virtual	SpatialBary			barycenter(void)							=0;
	virtual Vector2				uv(void)									=0;
	virtual SpatialVector		du(void)									=0;
	virtual SpatialVector		dv(void)									=0;

	/// TEMP HACK for xMechanics test
	/// TODO Material independent from surface
	virtual	Color				diffuse(void) const							=0;
	virtual	void				setDiffuse(Color a_diffuse)					=0;
	};

	/// @brief calculate surface distances
	class FE_DL_EXPORT GaugeI: virtual public Counted
	{
		public:
	virtual	void				pick(SpatialVector a_source,
										Real a_maxDistance)					=0;
	virtual	Real				distanceTo(SpatialVector a_target)			=0;
	};

							/// @brief associate with model data
virtual	void				bind(Instance a_instance)						=0;

virtual	void				setSampleMethod(SampleMethod a_sampleMethod)	=0;

virtual	void				setNodeName(String a_nodeName)					=0;
virtual	String				nodeName(void) const							=0;

virtual	void				setRestrictions(Restrictions
								a_restrictions)								=0;
virtual	Restrictions		restrictions(void) const						=0;

							/** @brief Set limitation of surface improvement,
								such as subdivision

								Refinement may be fixed or adaptive
								and is not necessarily uniformly applied.

								Some surfaces may ignore this setting.*/
virtual	void				setRefinement(U32 a_refinement)					=0;

							/** @brief Set what primitive to search down to

								Using sphere accuracy will only resolve to
								the lowest bounding sphere. */
virtual	void				setAccuracy(Accuracy a_accuracy)				=0;

							/** @brief Set whether this surface may be searched

								Turning this off may allow some
								implementations to make optimization.

								An implemention is permitted to remain
								searchable regardless. */
virtual	void				setSearchable(BWORD a_searchable)				=0;

							/** @brief Set component to use for searching

								This is presumed to be an implementation
								of SpatialTreeI.
								Leave blank for default. */
virtual	void				setSearch(String a_searchName)					=0;

							/** @brief Set how 4+ vertex polygons break down

								Using balanced triangulation may add new
								points that did not already exist
								on the mesh.

								Some implementations may drop
								5+ vertex polygons. */
virtual	void				setTriangulation(Triangulation a_triangulation)	=0;

							/** @brief Number of barycentric elements

								Use this for barycentric sampling.
								For surfaces converted into triangles,
								this is the number of triangle.
								For curves, this is really the curve count.
								If unknown, an implementation may return -1. */
virtual	I32					triangleCount(void)								=0;

							/// @name Return the bounding center
							///@{
virtual SpatialVector		center(void)									=0;
virtual SpatialVector		center(void) const								=0;
							///@}

							/// @name Return the bounding radius
							///@{
virtual Real				radius(void)									=0;
virtual Real				radius(void) const								=0;
							///@}

							/// @brief Return the diffuse color
virtual Color				diffuse(void) const								=0;

							/** @name Sample local transform on surface

								The notion of u and v may have limited sense
								on some surface types.  The full domain of
								u and v may not reach the full range of
								some surfaces.

								An implemention is permitted clamp u or v to
								the range 0 to 1, but may choose to extrapolate
								in one or both axes.

								The const version does not permit caching. */
							///@{
virtual SpatialTransform	sample(Vector2 a_uv)							=0;
virtual SpatialTransform	sample(Vector2 a_uv) const						=0;
							///@}

							/** @name Sample local transform on a surface face

								Surfaces that don't have faces may simply
								return identity.

								An implemention is permitted clamp the
								barycentric coordinates to the range 0 to 1,
								but may choose to extrapolate instead.

								The const version does not permit caching. */
							///@{
virtual SpatialTransform	sample(I32 a_triangleIndex,
									SpatialBary a_barycenter)				=0;
virtual SpatialTransform	sample(I32 a_triangleIndex,
									SpatialBary a_barycenter) const			=0;
virtual SpatialTransform	sample(I32 a_triangleIndex,SpatialBary a_barycenter,
									SpatialVector a_tangent) const			=0;
							///@}

							/** @name Sample surface face as ImpactI

								This method is similar to sample(),
								but populates an ImpactI.
								Not all value may be populated.

								The const version does not permit caching. */
							///@{
virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter)				=0;
virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter) const			=0;
virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter,
									SpatialVector a_tangent) const			=0;
							///@}

							/** @name Sample location on surface

								An implemention may just use the translation
								from the result of sample(), but is encouraged
								to provide a more efficient limited evaluation,
								if reasonable.

								The const version does not permit caching. */
							///@{
virtual SpatialVector		samplePoint(const SpatialTransform& a_transform,
									Vector2 a_uv)							=0;
virtual SpatialVector		samplePoint(Vector2 a_uv)						=0;
virtual SpatialVector		samplePoint(const SpatialTransform& a_transform,
									Vector2 a_uv) const						=0;
virtual SpatialVector		samplePoint(Vector2 a_uv) const					=0;
							///@}

							/** @name Sample direction from surface

								An implemention may just use the up vector
								from the result of sample(), but is encouraged
								to provide a more efficient limited evaluation,
								if reasonable.

								The const version does not permit caching. */
							///@{
virtual SpatialVector		sampleNormal(const SpatialTransform& a_transform,
									Vector2 a_uv)							=0;
virtual SpatialVector		sampleNormal(Vector2 a_uv)						=0;
virtual SpatialVector		sampleNormal(const SpatialTransform& a_transform,
									Vector2 a_uv) const						=0;
virtual SpatialVector		sampleNormal(Vector2 a_uv) const				=0;
							///@}

							/** @name Build data required for searching

								This may need to be called at least once
								before using a const version of nearestPoint
								or rayImpact.  The non-const versions will
								call it automatically. */
							///@{
virtual void				prepareForSample(void)							=0;
virtual void				prepareForSearch(void)							=0;
virtual void				prepareForUVSearch(void)						=0;
							///@}

virtual Containment			containment(const SpatialVector& a_origin)		=0;
virtual Containment			containment(
									const SpatialVector& a_origin) const	=0;

							/** @name Find the closest point(s) on the surface \
										from given point

								Surfaces are not required to support
								point searches.
								Any surface is allowed to return a NULL ImpactI.

								const versions do not permit caching. */
							///@{
virtual	sp<ImpactI>			nearestPoint(const SpatialTransform& a_transform,
									const SpatialVector& a_point,
									Real a_maxDistance)						=0;
virtual	sp<ImpactI>			nearestPoint(const SpatialTransform& a_transform,
									const SpatialVector& a_point,
									Real a_maxDistance) const				=0;

virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_point,
									Real a_maxDistance,BWORD a_anyHit)		=0;
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_point,
									Real a_maxDistance,
									BWORD a_anyHit) const					=0;

virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_point,
									Real a_maxDistance)						=0;
virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_point,
									Real a_maxDistance) const				=0;

virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_point)		=0;
virtual	sp<ImpactI>			nearestPoint(
									const SpatialVector& a_point) const		=0;

virtual	sp<ImpactI>			nearestPoint(const Vector2& a_uv)				=0;
virtual	sp<ImpactI>			nearestPoint(const Vector2& a_uv)const			=0;

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const Vector2& a_uv,
									Real a_maxDistance,U32 a_hitLimit)		=0;
virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const Vector2& a_uv,
									Real a_maxDistance,
									U32 a_hitLimit) const					=0;

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit)		=0;
virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,
									U32 a_hitLimit) const					=0;

virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit,
									sp<PartitionI> a_spPartition)			=0;
virtual	Array< sp<SurfaceI::ImpactI> >
							nearestPoints(const SpatialVector& a_origin,
									Real a_maxDistance,U32 a_hitLimit,
									sp<PartitionI> a_spPartition) const	=0;
							///@}

							/** @name Trace distance(s) to surface in given \
										direction from the originating point

								Surfaces are not required to support
								ray casting.
								Any surface is allowed to return a NULL ImpactI.

								const versions do not permit caching. */
							///@{
virtual	sp<ImpactI>			rayImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,
									BWORD a_anyHit)							=0;
virtual	sp<ImpactI>			rayImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,
									BWORD a_anyHit) const					=0;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,
									BWORD a_anyHit)							=0;
virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,
									BWORD a_anyHit) const					=0;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance)						=0;
virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance) const				=0;

virtual	Array< sp<SurfaceI::ImpactI> >
							rayImpacts(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,U32 a_hitLimit)		=0;
virtual	Array< sp<SurfaceI::ImpactI> >
							rayImpacts(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,U32 a_hitLimit) const=0;

virtual	sp<ImpactI>			coneImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_coneAngle,
									sp<DrawI> a_spDrawI)					=0;
virtual	sp<ImpactI>			coneImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_coneAngle,
									sp<DrawI> a_spDrawI)					=0;

virtual	sp<ImpactI>			capsuleImpact(const SpatialTransform& a_transform,
									const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_radius,
									BWORD a_anyHit,sp<DrawI> a_spDrawI,
									sp<ImpactI> a_spLastImpact)				=0;
virtual	sp<ImpactI>			capsuleImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,Real a_radius,
									BWORD a_anyHit,sp<DrawI> a_spDrawI,
									sp<ImpactI> a_spLastImpact)				=0;
							///@}

virtual	sp<GaugeI>			gauge(void)										=0;

virtual	void				partitionWith(String a_attributeName)			=0;
virtual	U32					partitionCount(void) const						=0;
virtual	String				partitionName(U32 a_index) const				=0;
virtual	Vector4				partitionSphere(U32 a_index) const				=0;
virtual	I32					setPartitionFilter(String a_filterString)		=0;
virtual	I32					setPartitionFilter(String a_filterString,
									PartitionI::FilterMethod a_filterMethod)=0;
virtual	sp<PartitionI>		createPartition(void)							=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceI_h__ */
