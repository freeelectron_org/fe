/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordCookbook_h__
#define __data_RecordCookbook_h__

namespace fe
{

/**************************************************************************//**
	@brief Instructions to set attributes of a Record

	@ingroup data
*//***************************************************************************/
class FE_DL_EXPORT RecordCookbook: public Component,
		public Initialize<RecordCookbook>
{
	public:
	class Clear:
		public Counted,
		public CastableAs<Clear>
	{
		public:
					Clear(sp<RecordCookbook> a_spRecordCookBook):
						m_spRecordCookBook(a_spRecordCookBook)
					{
#if FE_COUNTED_STORE_TRACKER
						setName("RecordCookbook::Clear");
#endif
					}
					~Clear(void)
					{	m_spRecordCookBook->clear(); }

			sp<RecordCookbook>	m_spRecordCookBook;
	};

	class Ingredient: public Instance
	{
		public:
					Ingredient(void)										{}
					~Ingredient(void);

			BWORD	set(FE_UWORD a_locator,sp<BaseType>& a_rspBT,void* a_pData);

			FE_UWORD	m_locator;
	};

typedef AutoHashMap< String, String > StringStringMap;
typedef AutoHashMap< String, Ingredient > StringIngredientMap;
typedef AutoHashMap< hp<Scope>, StringIngredientMap > ScopeCacheMap;

					RecordCookbook(void);
virtual				~RecordCookbook(void);

		void		initialize(void);
		void		registerDependency(sp<Registry> a_spRegistry);

		void		clear(void);

					///	@brief Add a prerequisite to a recipe
		void		derive(String recipeName,String parentName);

					/**	@brief Return the layouts for a recipe

						Often the Layout name will match the recipe name,
						but if a derived recipe is merging Layouts or
						just replacing replacing initial values, it may not.
						In this case, the parent classes are searched
						for a required Layouts. */
		void		findLayoutList(sp<Scope> spScope,String recipeName,
							Array<String>& rLayoutList);

					/** @brief Define an initial value for a recipe's attribute

						Redundant definitions favor the latest. */
		void		add(String recipeName,String attributeName,String value);

					/// @brief Apply initial values for a recipe to a Record
		void		cook(sp<Scope> spScope,String recipeName,Record& rRecord);

					/** @brief Recursively cook recipes

						@internal */
		void		cook_internal(sp<Scope> spScope,String recipeName,
							Record& rRecord,BWORD try_cache,
							StringIngredientMap* pNewCacheMap);

					/** @brief Recursively clear cached Record templates

						@internal */
		void		spoil(String recipeName);

const	String&		name(void) const		{ return m_name; }

		StringStringsMap&	factoryMultiMap(void)
							{	return m_factoryMultiMap; }

	private:

	class Recipe:
		public Counted,
		public CastableAs<Recipe>
	{
		public:
					Recipe(void)
					{
#if FE_COUNTED_STORE_TRACKER
						setName("RecordCookbook::Recipe");
#endif
					}

			void	setup(String name,sp<RecordCookbook> spRecordCookBook);
			void	derive(String recipeName);
			void	adopt(String recipeName);
			void	findLayoutList(sp<Scope> spScope,
							Array<String>& rLayoutList);
			void	add(String attributeName,String value);
			void	cook(sp<Scope> spScope,Record& rRecord,BWORD ignore_cache,
							StringIngredientMap* pNewCacheMap);
			void	spoil(void);

	const	String&	name(void) const		{ return m_name; }
	const	String	verboseName(void) const	{ return "Recipe " + name(); }

		private:

			String				m_name;
			hp<RecordCookbook>	m_hpRecordCookbook;
			Array<String>		m_prerequisites;
			Array<String>		m_dependents;
			StringStringMap		m_attributeMap;
			ScopeCacheMap		m_cacheMap;
	};

		sp<Recipe> lookup(String recipeName);

		String				m_name;

typedef HashMap<	String,
					sp<Recipe>,
					hash_string,
					eq_string > StringRecipeMap;

		StringRecipeMap		m_recipeMap;

		StringStringsMap	m_factoryMultiMap;
};

} /* namespace */

#endif /* __data_RecordCookbook_h__ */
