#! /usr/bin/bash

flake8 \
    --show-source \
    --statistics \
    pytests/*.py
    #utility.py
    #--ignore=<code>,<code>,<code>,...

# Ignored codes:
# - E302: before class definition definition statement:
#       "expected 2 blank lines, found 1"
#       https://www.flake8rules.com/rules/E302.html
# - E305: after body of class or function:
#       "expected 2 blank lines after class or function definition, found 1"
#       https://www.flake8rules.com/rules/E305.html
# - W503: line break occurred before a binary operator
#       https://www.flake8rules.com/rules/W503.html
