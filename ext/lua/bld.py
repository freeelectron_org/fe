import sys
import os
forge = sys.modules["forge"]

def prerequisites():
#   return ["viewer"]
    return ["surface"]

def setup(module):
    module.summary = []

    srcList = [ "lua.pmh",
                "LuaContext",
                "LuaObject",
                "LuaType",
                "LuaComponent",
                "LuaBind",
                "LuaHandler",
                "LuaIterate",
                "SurfaceAccessibleLua",
                "luaDL" ]

    if 'architecture' in forge.modules_confirmed:
        module.cppmap['architecture'] = ' -DFE_LUA_ARCHITECTURE'
    else:
        module.summary += [ "-architecture" ]

    dll = module.DLL( "fexLuaDL", srcList )

    #dll.linkmap = { 'lua' : '-L%s -llua -llualib' %  os.path.join(module.modPath,'lua','lib')}

    deplibs = forge.corelibs+ [
            "fexSignalLib",
            "fexDataToolDLLib",
            "fexSurfaceDLLib",
            "luaLib",
            "lualbLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]
#       deplibs += [    "fexViewerDLLib" ]

    forge.deps( ["fexLuaDLLib"], deplibs )

    module.includemap = { 'lua' : os.path.join(module.modPath,'lua','src') }

    lua = module.Module( 'lua' )
    module.AddDep(lua)

    forge.tests += [
        ("inspect.exe",     "fexLuaDL",                     None,       None) ]

    module.Module( 'test' )
