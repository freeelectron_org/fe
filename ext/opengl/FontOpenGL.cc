/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opengl/opengl.pmh>

namespace fe
{
namespace ext
{

FontOpenGL::FontOpenGL(void):
	m_fontBase(-1),
	m_fontRange(-1),
	m_fontAscent(0),
	m_fontDescent(0),
	m_multiplication(0.0)
{
#if FE_2DGL==FE_2D_X_GFX
	m_pFontStruct=NULL;

	Display* pDisplayHandle=XOpenDisplay(NULL);

	const BWORD logFailures=(System::getVerbose()=="all");
	String name;

	const Real dpi=fe::compute_dpi();
	m_multiplication=fe::determine_multiplication(dpi);

	feLog("FontOpenGL::FontOpenGL multiplication %.6G\n",m_multiplication);

	if(m_multiplication>1)
	{
//		name="-*-terminus-bold-r-*-*-22-*-*-*-*-*-*-*";
		name="-*-lucidatypewriter-bold-r-*-*-20-*-*-*-*-*-*-*";
		m_pFontStruct=XLoadQueryFont(pDisplayHandle,name.c_str());
		if(!m_pFontStruct)
		{
			if(logFailures)
			{
				feLog("FontOpenGL::FontOpenGL tried \"%s\"\n",name.c_str());
			}
			name="-*-fixed-medium-r-*-*-20-*-*-*-*-*-*-*";
			m_pFontStruct=XLoadQueryFont(pDisplayHandle,name.c_str());
		}
		if(!m_pFontStruct)
		{
			if(logFailures)
			{
				feLog("FontOpenGL::FontOpenGL tried \"%s\"\n",name.c_str());
			}
			name="-*-clean-bold-r-*-*-16-*-*-*-*-*-*-*";
			m_pFontStruct=XLoadQueryFont(pDisplayHandle,name.c_str());
		}
		if(!m_pFontStruct && logFailures)
		{
			feLog("FontOpenGL::FontOpenGL tried \"%s\"\n",name.c_str());
		}
	}

	if(!m_pFontStruct)
	{
		name="-*-lucidatypewriter-bold-r-*-*-14-*-*-*-*-*-*-*";
		m_pFontStruct=XLoadQueryFont(pDisplayHandle,name.c_str());
	}
	if(!m_pFontStruct)
	{
//		name="-*-clean-medium-r-*-*-8-*-*-*-*-*-*-*";
		name="-*-fixed-bold-r-*-*-13-*-*-*-*-*-*-*";
		m_pFontStruct=XLoadQueryFont(pDisplayHandle,name.c_str());
	}
	if(m_pFontStruct)
	{
		feLog("using font \"%s\"\n",name.c_str());
	}
	else if(logFailures)
	{
		feLog("FontOpenGL::FontOpenGL tried \"%s\"\n",name.c_str());
	}

	if(!m_pFontStruct)
	{
		feLog("FontOpenGL::Font(): could not load a font\n");
		return;
	}

	m_fontRange=m_pFontStruct->max_char_or_byte2+1;
	m_fontBase=glGenLists(m_fontRange);

	feLog("FontOpenGL::FontOpenGL m_fontBase %d\n",m_fontBase);
	if(m_fontBase<1)
	{
		GLenum error=glGetError();
		feLog("FontOpenGL::FontOpenGL GL Error %d\n",error);
	}

	const ::Font fontId=m_pFontStruct->fid;
	const U32 first=m_pFontStruct->min_char_or_byte2;
	const U32 last=m_pFontStruct->max_char_or_byte2;

	glXUseXFont(fontId,first,last-first+1,m_fontBase+first);

	//* NOTE from NativeEventContext::calcFontHeight()
	int direction;			/* direction hint */
	XCharStruct overall;	/* overall char structure */

	//* quick test of sample string "Qy," (should get high and low extents)
	XTextExtents(m_pFontStruct,"Qy,",
			3,&direction,&m_fontAscent,&m_fontDescent,&overall);
#elif FE_2DGL==FE_2D_GDI
	HWND windowHandle=GetActiveWindow();
	HDC hdc=GetDC(windowHandle);

#if FALSE
	LOGFONT logfont;

	logfont.lfHeight=8;
	logfont.lfWidth=0;
	strcpy(logfont.lfFaceName,"Arial Narrow");

	HFONT fonthandle=CreateFontIndirect(&logfont);
#else
	HFONT fonthandle=(HFONT)GetStockObject(DEFAULT_GUI_FONT);
#endif

	if(fonthandle == NULL)
	{
		feLog("error creating font\n");
		return;
	}

	const U32 first=0;
	const U32 last=255;
	const U32 lists=last+1;

	m_fontBase=glGenLists(lists);
	if(m_fontBase==0)
	{
		feLog("error generating font display lists\n");

		return;
	}

	if(!SelectObject(hdc,fonthandle))
	{
		feLog("error selecting font\n");
		m_fontBase=0;
		return;
	}

	m_fontRange=m_fontBase+256;

	int height=0;

	//* numbers can be taller than letters in Win32 fonts
	SIZE size;
	if(GetTextExtentPoint32A(hdc,"1y",2,&size))
	{
		height=size.cy;
	}

	TEXTMETRIC metric;
	if(GetTextMetrics(hdc,&metric))
	{
		m_fontAscent=metric.tmAscent;
		m_fontDescent=metric.tmDescent;
	}

	if(height!=m_fontAscent+m_fontDescent)
	{
		feLog("loadGdiFont error height %d != ascent %d + descent %d\n",
				height,m_fontAscent,m_fontDescent);
	}

//	feLog("loadGdiFont base %d range %d ascent %d descent %d height %d\n",
//			m_fontBase,m_fontRange,m_fontAscent,m_fontDescent,height);
	feLog("using default gui font\n");

	if(!wglUseFontBitmaps(hdc,first,last-first+1,m_fontBase+first))
	{
		feLog("loadGdiFont error using font bitmap\n");
	}
#endif
}

FontOpenGL::~FontOpenGL(void)
{
	if(m_fontBase>=0 && m_fontRange>=0)
	{
		glDeleteLists(m_fontBase,m_fontRange);
	}

	m_fontBase= -1;
	m_fontRange= -1;

#if FE_2DGL==FE_2D_X_GFX
	if(m_pFontStruct)
	{
		Display* pDisplayHandle=XOpenDisplay(NULL);
		if(pDisplayHandle)
		{
			XFreeFont(pDisplayHandle,m_pFontStruct);
		}
	}

	m_pFontStruct=NULL;
#endif
}

void FontOpenGL::initialize(void)
{
}

void FontOpenGL::drawAlignedText(sp<Component> a_spDrawComponent,
	const SpatialVector& a_location,
	const String a_text,const Color &a_color)
{
	if(!m_fontBase)
	{
		feLog("FontOpenGL::drawAlignedText fontbase==0\n");
		return;
	}

#if FE_DOUBLE_REAL
	glColor4dv(a_color.temp());
	glRasterPos3dv(a_location.temp());
#else
	glColor4fv(a_color.temp());
	glRasterPos3fv(a_location.temp());
#endif

	glPushAttrib(GL_LIST_BIT);

	glListBase(m_fontBase);
	glCallLists(a_text.length(),GL_UNSIGNED_BYTE,
			(unsigned char *)a_text.c_str());

	glPopAttrib();
}

} /* namespace ext */
} /* namespace fe */
