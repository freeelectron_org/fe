/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_SpannedRange_h__
#define __thread_SpannedRange_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SpannedRange of jobs

	@ingroup thread
*//***************************************************************************/
class FE_DL_EXPORT SpannedRange: public Counted
{
	public:

	class FE_DL_EXPORT Span: public Vector<2,Real>
	{
		public:
					Span(void):
						Vector<2,Real>(0.0,-1.0)							{}
	};

	class FE_DL_EXPORT MultiSpan
	{
		public:
								MultiSpan(void)								{}

			void				clear(void)
								{	m_spanArray.clear(); }
			U32					spanCount(void) const
								{	return m_spanArray.size(); }
			Span&				span(U32 a_spanIndex)
								{
									FEASSERT(a_spanIndex<m_spanArray.size());
									return m_spanArray[a_spanIndex];
								}
	const	Span&				span(U32 a_spanIndex) const
								{
									FEASSERT(a_spanIndex<m_spanArray.size());
									return m_spanArray[a_spanIndex];
								}

			void				add(Real a_start,Real a_end);
			void				add(const Span& a_rSpan)
								{	add(a_rSpan[0],a_rSpan[1]); }
			void				add(const MultiSpan& rOther);

								//* TODO MultiSpan::Iterator
			Real				valueAt(U32 a_valueIndex) const;
			U32					valueCount(void) const;

			String				brief(void) const;
			String				dump(void) const;

		private:

			Array<Span>			m_spanArray;
	};

	class FE_DL_EXPORT Iterator
	{
		public:
								Iterator(sp<SpannedRange> a_spSpannedRange):
									m_spSpannedRange(a_spSpannedRange),
									m_atomicIndex(0),
									m_spanIndex(-1),
									m_valueCount(0),
									m_valueIndex(0),
									m_value(0.0)
								{
									FEASSERT(m_spSpannedRange.isValid());
									m_atomicCount=
											m_spSpannedRange->atomicCount();
									advance();
								}

			Real				value(void)		{ return m_value; }
			BWORD				atEnd(void)
								{	return m_atomicIndex>m_atomicCount+1; }

			void				step(void)
								{
									m_valueIndex++;
									if(m_valueIndex>=m_valueCount)
									{
										advance();
									}
									else
									{
										m_value+=1.0;
									}
								}

		private:

			void				advance(void);

			sp<SpannedRange>	m_spSpannedRange;
			U32					m_atomicCount;
			U32					m_atomicIndex;
			I32					m_spanIndex;
			U32					m_valueCount;
			U32					m_valueIndex;
			Real				m_value;
	};

					SpannedRange(void)										{}

		void		clear(void)
					{
						m_atomArray.clear();
						m_postAtomic.clear();
						m_nonAtomic.clear();
					}

		MultiSpan&	addAtomic(void);

		U32			atomicCount(void) const	{ return m_atomArray.size(); }

		MultiSpan&	atomic(U32 a_index)
					{	return m_atomArray[a_index]; }
const	MultiSpan&	atomic(U32 a_index) const
					{	return m_atomArray[a_index]; }

		MultiSpan&	postAtomic(void)		{ return m_postAtomic; }
		MultiSpan&	nonAtomic(void)			{ return m_nonAtomic; }
const	MultiSpan&	nonAtomic(void) const	{ return m_nonAtomic; }

		BWORD		empty(void) const
					{	return !m_atomArray.size() &&
								!m_postAtomic.spanCount() &&
								!m_nonAtomic.spanCount(); }

		Iterator	begin(void)	{ return Iterator(sp<SpannedRange>(this)); }

		U32			valueCount(void) const;

					/** @brief merge atoms into counts of the given granularity

						The granularity size is only a goal.
						The results can be various sizes
						above and below the goal.
						No atoms are broken up to make them smaller.
					*/
		sp<SpannedRange> combineAtoms(I32 a_size) const;

		void		split(sp<SpannedRange>& rspSpan0,
							sp<SpannedRange>& rspSpan1) const;

		String		brief(void) const;
		String		dump(void) const;

	private:

		Array<MultiSpan>		m_atomArray;	//* each thread-safe piece
		MultiSpan				m_postAtomic;	//* unsafe to do unthreaded
		MultiSpan				m_nonAtomic;	//* always safe
};

inline String print(const sp<SpannedRange>& a_rspSpannedRange)
{
	return a_rspSpannedRange.isNull()? "NULL": a_rspSpannedRange->brief();
}

} /* namespace ext */
} /* namespace fe */

#endif // __thread_SpannedRange_h__
