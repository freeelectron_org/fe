#!/usr/bin/python

import re
import sys
import os

class_expr = re.compile(r'^\s*class\s+(\w+)(.*)$')
data_match = re.compile(r'^.*//~\s*(\S*)\s(.*)\s*$')
#data_match = re.compile(r'^.*@(mt|em)\s*(\S*)\s(.*)\s*$')


classes = {}
all_keys = {}

class cppClass:
    def __init__(self, name):
        self.name = name
        self.dict = {}


def process(filename):
    global classes, all_keys
    cache = {}
    f = open(filename,'r')
    line = f.readline()
    while line:
        match = data_match.match(line)
        if match:
            cache[match.group(1)] = match.group(2)
            all_keys[match.group(1)] = 1
        match = class_expr.match(line)
        if match:
            if not classes.has_key(match.group(1)):
                classes[match.group(1)] = cppClass(match.group(1))
            current = classes[match.group(1)]
            for k in cache.keys():
                current.dict[k] = cache[k]
            cache = {}
        line = f.readline()


def scan(keys):
    clscol = '%-25s'
    col = '%-12s'
    sys.stdout.write(clscol % 'class')
    for key in keys:
        sys.stdout.write(col % key)
    sys.stdout.write('\n')
    for ck in classes.keys():
        c = classes[ck]
        sys.stdout.write(clscol % (c.name))
        for key in keys:
            if c.dict.has_key(key):
                value = c.dict[key]
            else:
                value =  '-'
            sys.stdout.write(col % (value))
        sys.stdout.write('\n')

header_expr = re.compile(r'^\w*\.h$')

def visit(arg, dirname, names):
    for n in names:
        if header_expr.match(n):
            process(os.path.join(dirname,n))

os.walk('.', visit, None)

if len(sys.argv) > 1:
    scan(sys.argv[1:])
else:
    scan(all_keys.keys())

