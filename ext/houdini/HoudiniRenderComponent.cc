/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define GL_GLEXT_PROTOTYPES

#include <houdini/houdini.pmh>

#define FE_HRC_DEBUG	FALSE

#include <RE/RE_Render.h>

namespace fe
{
namespace ext
{

FE_DL_PUBLIC sp<DrawI> HoudiniRenderHookComponent::ms_spDraw;

HoudiniRenderHookComponent::HoudiniRenderHookComponent()
    : GUI_PrimitiveHook("fe::Component")
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderHookComponent::HoudiniRenderHookComponent\n");
#endif
}

HoudiniRenderHookComponent::~HoudiniRenderHookComponent()
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderHookComponent::~HoudiniRenderHookComponent\n");
#endif
}

GR_Primitive* HoudiniRenderHookComponent::createPrimitive(
	const GT_PrimitiveHandle& gt_prim,
	const GEO_Primitive* geo_prim,
	const GR_RenderInfo* info,
	const char* cache_name,
	GR_PrimAcceptResult& processed)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderHookComponent::createPrimitive\n");
#endif

    return new HoudiniRenderComponent(info, cache_name, geo_prim);
}

HoudiniRenderComponent::HoudiniRenderComponent(
	const GR_RenderInfo *info,
	const char *cache_name,
	const GEO_Primitive *prim):
    GR_Primitive(info, cache_name, GA_PrimCompat::TypeMask(0)),
	m_pRenderBrush(NULL)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::HoudiniRenderComponent\n");
#endif

    m_typeID=prim->getTypeId().get();
}

HoudiniRenderComponent::~HoudiniRenderComponent(void)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::~HoudiniRenderComponent\n");
#endif

	delete m_pRenderBrush;
}

GR_PrimAcceptResult HoudiniRenderComponent::acceptPrimitive(
	GT_PrimitiveType t,
	int geo_type,
	const GT_PrimitiveHandle& ph,
	const GEO_Primitive* prim)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::acceptPrimitive\n");
#endif

    return (geo_type==m_typeID)? GR_PROCESSED: GR_NOT_PROCESSED;
}

void HoudiniRenderComponent::update(RE_Render* a_pRender,
	const GT_PrimitiveHandle& primh,
	const GR_UpdateParms& p)
{
    const HoudiniPrimComponent* pPrimComponent(NULL);

    getGEOPrimFromGT<HoudiniPrimComponent>(primh, pPrimComponent);

#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::update %p prim %p\n",this,pPrimComponent);
#endif

	m_spPayload=pPrimComponent?
			const_cast<HoudiniPrimComponent*>(pPrimComponent)->payload():
			sp<Component>(NULL);
}

#if FE_HOUDINI_16_PLUS
void HoudiniRenderComponent::render(RE_Render* a_pRender,
	GR_RenderMode render_mode,
	GR_RenderFlags flags,
	GR_DrawParms dp)
#else
void HoudiniRenderComponent::render(RE_Render* a_pRender,
	GR_RenderMode render_mode,
	GR_RenderFlags flags,
	const GR_DisplayOption* opt,
	const RE_MaterialList* materials)
#endif
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::render %p %p\n",this,m_spPayload.raw());
#endif

	if(m_spPayload.isNull())
	{
		return;
	}

	if(!m_pRenderBrush)
	{
		m_pRenderBrush=new(HoudiniRenderComponent::RenderBrush);
	}

	if(m_pRenderBrush)
	{
		m_pRenderBrush->render(a_pRender,m_spPayload);
	}

#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::render done\n");
#endif
}

HoudiniRenderComponent::RenderBrush::RenderBrush(void)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::RenderBrush::RenderBrush\n");
#endif

	set(m_viewport);

	m_pOperatorContext=new HoudiniContext();
	sp<Registry> spRegistry=m_pOperatorContext->master()->registry();

	sp<Scope> spScope=m_pOperatorContext->scope();
	initEvent(spScope);

	m_spDrawPerspective=spRegistry->create("DrawI.DrawCached");
	m_spDrawPerspective->setName("RenderBrush.perspective");
	updateDrawInterfaces();
}

HoudiniRenderComponent::RenderBrush::~RenderBrush(void)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::RenderBrush::~RenderBrush\n");
#endif
}

void HoudiniRenderComponent::RenderBrush::render(RE_Render* a_pRender,
	sp<Component> a_spPayload)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::RenderBrush::render %p -> %p\n",
			m_spPayload.raw(),a_spPayload.raw());
#endif

	m_pRender=a_pRender;

	if(m_spPayload!=a_spPayload)
	{
#if FE_HRC_DEBUG
		feLog("HoudiniRenderComponent::RenderBrush::render"
				" replacement payload\n");
#endif

		m_spPayload=a_spPayload;
		m_spDrawable=m_spPayload;
		m_spDrawBuffer=NULL;

		if(m_spPayload.isValid() && m_spDrawable.isNull())
		{
			sp<SurfaceAccessibleI> spPayloadAccessible(m_spPayload);
			if(spPayloadAccessible.isValid())
			{
				m_spDrawable=spPayloadAccessible->surface();
			}
			if(m_spDrawable.isNull())
			{
				feLog("HoudiniRenderComponent::RenderBrush::render"
						"failed to find Drawable in custom primitive\n");
			}
		}
	}

	//* NOTE if Houdini leaves a VBO bound, glVertexPointer will fail
	if(m_spDrawOpenGL.isValid())
	{
		m_spDrawOpenGL->unbindVertexArray();
	}
	else
	{
		feLog("HoudiniBrush::doRender m_spDrawOpenGL invalid\n");
	}

	UT_DimRect rect=a_pRender->getViewport2DI();
	set(m_viewport,rect.x(),rect.y(),rect.w(),rect.h());

#if FE_HOUDINI_HYDRA_HOOK
	if(m_spDrawOpenGL.isNull() && m_spMaster.isValid())
	{
#if FALSE
		if(HoudiniRenderHookComponent::ms_spDraw.isNull())
		{
			HoudiniRenderHookComponent::ms_spDraw=
					m_spMaster->registry()->create("DrawI.DrawHydra");
			if(HoudiniRenderHookComponent::ms_spDraw.isValid())
			{
				feLog("using Hydra as render hook\n");
			}
		}
#endif
		if(HoudiniRenderHookComponent::ms_spDraw.isNull())
		{
			HoudiniRenderHookComponent::ms_spDraw=
					m_spMaster->registry()->create("DrawI.DrawOpenGL");
			if(HoudiniRenderHookComponent::ms_spDraw.isValid())
			{
				feLog("using raw OpenGL as render hook\n");
			}
		}
		if(HoudiniRenderHookComponent::ms_spDraw.isNull())
		{
			feLog("failed to create Draw interface for render hook\n");
		}

		m_spDrawOpenGL=HoudiniRenderHookComponent::ms_spDraw;

		if(m_spDrawOpenGL.isValid())
		{
			m_spDrawOpenGL->setName("RenderBrush Draw");

			m_spDrawOpenGL->setBrightness(0.0);	// 0.4
			m_spDrawOpenGL->setContrast(0.2);	// 0.6

			sp<DrawMode> spDrawMode=m_spDrawOpenGL->drawMode();
			FEASSERT(spDrawMode.isValid());

			spDrawMode->setDrawStyle(DrawMode::e_solid);
			spDrawMode->setPointSize(4);
			spDrawMode->setLineWidth(2);
			spDrawMode->setRefinement(1);
			spDrawMode->setFrontfaceCulling(FALSE);
			spDrawMode->setBackfaceCulling(FALSE);
			spDrawMode->setLit(TRUE);
			spDrawMode->setShadows(FALSE);
		}

		m_spMaster=NULL;
	}
#endif

	draw();

#if FE_HOUDINI_HYDRA_HOOK
	a_pRender->resync();
#endif

#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::RenderBrush::render done\n");
#endif
}

void HoudiniRenderComponent::RenderBrush::drawPost(void)
{
#if FE_HRC_DEBUG
	feLog("HoudiniRenderComponent::RenderBrush::drawPost\n");
#endif

	if(m_spDrawOpenGL.isValid() &&
			m_spDrawable.isValid() && m_spDrawBuffer.isNull())
	{
#if FE_HRC_DEBUG
		feLog("HoudiniRenderComponent::RenderBrush::drawPost createBuffer\n");
#endif

		m_spDrawBuffer=m_spDrawOpenGL->createBuffer();
		m_spDrawBuffer->setName(m_spDrawable->name());
	}

	UT_Matrix4D projection4D=
			m_pRender->getUniform(RE_UNIFORM_PROJECT_MATRIX)->getMatrix4();

	const double* doubles=projection4D.data();

	Matrix<4,4,Real> projection;
	set(projection,doubles);

//	feLog("projection\n%s\n",c_print(projection));

	Real fovy(1.0);
	Real aspect(1.0);
	Real nearplane(1.0);
	Real farplane(1.0);
	decomposePerspective(projection,fovy,aspect,nearplane,farplane);
//	feLog("fovy %.6G aspect %.6G nearplane %.6G farplane %.6G\n",
//			fovy,aspect,nearplane,farplane);

	UT_Matrix4D modelView4D=
			m_pRender->getUniform(RE_UNIFORM_OBJECT_MATRIX)->getMatrix4()*
			m_pRender->getUniform(RE_UNIFORM_VIEW_MATRIX)->getMatrix4();

	doubles=modelView4D.data();

	SpatialTransform modelView;
	set(modelView.column(0),doubles[0],doubles[1],doubles[2]);
	set(modelView.column(1),doubles[4],doubles[5],doubles[6]);
	set(modelView.column(2),doubles[8],doubles[9],doubles[10]);
	set(modelView.column(3),doubles[12],doubles[13],doubles[14]);

//	feLog("modelView\n%s\n",c_print(modelView));

	Real values[16];
	modelView.copy16(values);
	m_spDrawOpenGL->pushMatrix(DrawI::e_modelview,values);

	m_spDrawOpenGL->pushMatrix(DrawI::e_projection,projection.raw());

	//* NOTE hydra requires camera info, plain OpenGL doesn't
	sp<ViewI> spView=m_spDrawOpenGL->view();
	spView->setViewport(m_viewport);
	spView->setProjection(ViewI::e_perspective);

	sp<CameraI> spCamera=spView->camera();
	spCamera->setCameraMatrix(modelView);
	spCamera->setFov(Vector2(0.0,fovy));
	spCamera->setPlanes(nearplane,farplane);

	m_spDrawOpenGL->draw(m_spDrawable,NULL,m_spDrawBuffer);
	m_spDrawOpenGL->flush();

	m_spDrawOpenGL->popMatrix(DrawI::e_projection);

	m_spDrawOpenGL->popMatrix(DrawI::e_modelview);
}

} /* namespace ext */
} /* namespace fe */
