/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

#include <draw/draw.h>

namespace fe
{
namespace ext
{

class PerFrame : public HandlerI
{
	public:
				PerFrame(void) { }
virtual			~PerFrame(void) { }

virtual void	handle(Record &r_sig)
		{
			m_asSignal.bind(r_sig.layout()->scope());
			m_asWindata.bind(r_sig.layout()->scope());
			m_asColor.bind(r_sig.layout()->scope());
			if(!m_asSignal.winData.check(r_sig)) { return; }
			Record r_windata = m_asSignal.winData(r_sig);

			sp<WindowI> spWindow = m_asWindata.windowI(r_windata);
			if(spWindow.isValid())
			{
				sp<RecordGroup> spRG = m_asWindata.group(r_windata);
				Record r_bgColor = spRG->lookup<String>(m_asColor.name,
					String("background"));
				if(r_bgColor.isValid())
				{
					spWindow->setBackground(m_asColor.rgba(r_bgColor));
				}

				sp<DrawI> spDrawI(m_asWindata.drawI(r_windata));
				if(spDrawI.isValid())
				{
					if(spDrawI->view().isValid())
					{
						spDrawI->view()->setWindow(spWindow);
						spDrawI->flush();
					}
				}
			}
		}
	private:
		AsWindata						m_asWindata;
		AsSignal						m_asSignal;
		AsColor							m_asColor;
};

class MakeCurrent : public HandlerI
{
	public:
				MakeCurrent(void) { }
virtual			~MakeCurrent(void) { }

virtual void	handle(Record &r_sig)
		{
			m_asSignal.bind(r_sig.layout()->scope());
			m_asWindata.bind(r_sig.layout()->scope());
			if(!m_asSignal.winData.check(r_sig)) { return; }
			Record r_windata = m_asSignal.winData(r_sig);

			sp<WindowI> spWindow = m_asWindata.windowI(r_windata);
			if(spWindow.isValid())
			{
				spWindow->makeCurrent();
			}
		}
	private:
		AsWindata						m_asWindata;
		AsSignal						m_asSignal;
};

WindowPipeline::WindowPipeline(void)
{
}

WindowPipeline::~WindowPipeline(void)
{
}

void WindowPipeline::initialize(void)
{
}

void WindowPipeline::attach(sp<ApplicationI> a_application,
		sp<SignalerI> a_signaler, sp<SequencerI> a_sequencer, sp<Layout> l_hb,
		sp<RecordGroup> rg_input)
{
	m_spSignaler = a_signaler;

	sp<Scope> spWinScope(registry()->create("Scope"));
	spWinScope->setName("WindowPipelineScope");

	registry()->manage("fexNativeWindowDL");
	registry()->manage("fexWindowDL");
	registry()->manage("fexOpenGLDL");
	registry()->manage("fexViewerDL");
	registry()->manage("fexLuaDL");

	m_spWindatas = new RecordGroup();

	// window event heartbeat
	l_winev_hb = spWinScope->declare("l_winev_hb");
#ifdef FE_LOCK_SUPPRESSION
	l_winev_hb->suppressLock(true);
#endif

	// draw heartbeat
	l_draw_hb = spWinScope->declare("l_draw_hb");
#ifdef FE_LOCK_SUPPRESSION
	l_draw_hb->suppressLock(true);
#endif

	// window data layout
	l_win_data = spWinScope->declare("l_win_data");
#ifdef FE_LOCK_SUPPRESSION
	l_win_data->suppressLock(true);
#endif
	AsWindata asWindata;
	asWindata.populate(l_win_data);
	l_draw_hb->populate(FE_USE("win:data"), "record");

	loadConfig(rg_input);

	// WindowEvent
	l_event = spWinScope->declare("WindowEvent");
	l_event->populate(FE_USE("win:data"), "record");

	if(cfg<String>("debug").match("log"))
	{
		// logging
		sp<HandlerI> spLog(registry()->create("HandlerI.Log"));
		sp<Layout> l_null;
		spLog->adjoin(m_spSignaler);
		m_spSignaler->insert(spLog, l_null);
	}

#ifdef FE_LOCK_SUPPRESSION
	l_win_data->suppressLock(false);
#endif
	loadWindows();

	// window event heartbeat
	AsSequenceSignal asSequenceSignal;
	asSequenceSignal.bind(spWinScope);
#ifdef FE_LOCK_SUPPRESSION
	l_winev_hb->suppressLock(false);
#endif
	Record r_winev_hb = a_sequencer->add(l_winev_hb,
		a_sequencer->getCurrentTime(), 10);
	asSequenceSignal.count(r_winev_hb) = 0;
	asSequenceSignal.periodic(r_winev_hb) = 3;

	// draw heartbeat
#ifdef FE_LOCK_SUPPRESSION
	l_draw_hb->suppressLock(false);
#endif
	Record r_draw_hb = a_sequencer->add(l_draw_hb,
		a_sequencer->getCurrentTime(), 10);
	asSequenceSignal.count(r_draw_hb) = 0;
	asSequenceSignal.periodic(r_draw_hb) = 3;
}


void WindowPipeline::loadConfig(sp<RecordGroup> rg_input)
{
	sp<Registry> spRegistry(m_spSignaler->registry());

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		AsNamed asNamed;
		asNamed.bind(spRA->layout()->scope());
		AsGroup asGroup;
		asGroup.bind(spRA->layout()->scope());
		if(asNamed.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				//sp<ApplicationWindowI> spAppWin = feCast(ApplicationWindowI,
				//	create(asNamed.name(spRA, i)));
				sp<Component> spC = create(asNamed.name(spRA, i));
				sp<ApplicationWindowI> spAppWin(spC);
				spAppWin->populateDataLayout(l_win_data);
				sp<RecordGroup> rg_app;
				if(asGroup.check(spRA))
				{
					rg_app = asGroup.group(spRA,i);
				}
				m_appWindows.push_back(t_appwin(spAppWin, rg_app));
			}
		}
	}
}

void WindowPipeline::loadWindows(void)
{
	for(unsigned i = 0; i < m_appWindows.size(); i++)
	{
		Record r_win_data;
		sp<WindowI> spWindow = createWindow("-- unset --", r_win_data,
			m_appWindows[i].second);
		m_appWindows[i].first->
			makeWindow(spWindow, r_win_data, l_event, l_draw_hb,
				m_appWindows[i].second);
	}
	m_appWindows.clear();
}

sp<WindowI> WindowPipeline::createWindow(const String &a_title,
		Record &r_win_data, sp<RecordGroup> rg_appwin)
{
	// create window component
	sp<Registry> spRegistry(m_spSignaler->registry());
	sp<WindowI>	spWindow(spRegistry->create("WindowI"));
	m_windows.push_back(spWindow);

	// event context must receive event heartbeat
	if(!m_spEventContext.isValid())
	{
		m_spEventContext = spWindow->getEventContextI();
		m_spSignaler->insert(m_spEventContext, l_winev_hb);
	}

	// window must receive draw heartbeat
	m_spSignaler->insert(spWindow, l_draw_hb);

	// annotate window data to draw and event signals
	sp<AnnotateI> spAnnotateWinData(spRegistry->create("HandlerI.Annotate"));
	spAnnotateWinData->adjoin(spWindow);
	sp<SignalerI> spSignaler(spWindow);
	spSignaler->insert(spAnnotateWinData, l_draw_hb);
	spSignaler->insert(spAnnotateWinData, l_event);

	// some debugging nodes
#if 0
	sp<HandlerI> spPrintDraw(spRegistry->create("LuaI"));
	spPrintDraw->adjoin(spWindow);
	sp<LuaI> spLua(spPrintDraw);
	spLua->loadString("io.write(\"---> draw\\n\")");
	spSignaler->insert(spPrintDraw, l_draw_hb);

	sp<HandlerI> spEventDraw(spRegistry->create("LuaI"));
	spEventDraw->adjoin(spWindow);
	spLua = spEventDraw;
	spLua->loadString("io.write(\"---> event\\n\")");
	spSignaler->insert(spEventDraw, l_event);
#endif

	// make window current for draw signals
	sp<HandlerI> spMakeCurrent(new MakeCurrent());
	spMakeCurrent->adjoin(spSignaler);
	spSignaler->insert(spMakeCurrent, l_draw_hb);

	// do per frame per window stuff
	sp<HandlerI> spPerFrame(new PerFrame());
	spPerFrame->adjoin(spSignaler);
	spSignaler->insert(spPerFrame, l_draw_hb);

	// create the window data record itself and set the attributes
	r_win_data = l_draw_hb->scope()->createRecord(l_win_data);
	m_spWindatas->add(r_win_data);
	spAnnotateWinData->attach(FE_USE("win:data"), r_win_data);
	AsWindata asWindata;
	asWindata.bind(l_draw_hb->scope());
	sp<DrawI> spDraw(spRegistry->create("DrawI.DrawOpenGL"));
	asWindata.drawI(r_win_data) = spDraw;
	asWindata.windowI(r_win_data) = spWindow;
	asWindata.group(r_win_data) = new RecordGroup();

	// Transfer input color map to window data
	AsColor inAsColor;
	inAsColor.bind(l_draw_hb->scope());
	sp<Layout> l_color = l_draw_hb->scope()->declare("l_color");
	inAsColor.populate(l_color);

	for(RecordGroup::iterator i_rg = rg_appwin->begin();
		i_rg != rg_appwin->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		AsColor asColor;
		asColor.bind(spRA->layout()->scope());
		if(asColor.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_color = l_draw_hb->scope()->createRecord(l_color);
				inAsColor.name(r_color) = asColor.name(spRA,i);
				inAsColor.rgba(r_color) = asColor.rgba(spRA,i);
				asWindata.group(r_win_data)->add(r_color);
			}
		}
	}

	// open the window
	spWindow->open(a_title);

	return spWindow;
}

} /* namespace ext */
} /* namespace fe */

