/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	const Real tolerance=1e-5;

	try
	{
		SpatialTransform matrix;

		feLog("concatenate XYZ of 30 40 50\n");
		setIdentity(matrix);
		translate(matrix,SpatialVector(1.0f,2.0f));
		rotate(matrix,50.0f*degToRad,e_zAxis);
		rotate(matrix,40.0f*degToRad,e_yAxis);
		rotate(matrix,30.0f*degToRad,e_xAxis);

		SpatialEuler euler(matrix);
		SpatialVector degrees=euler/degToRad;

		feLog("euler degrees %s\n",c_print(degrees));
		UNIT_TEST(isZero(degrees[0]-Real(30),tolerance));
		UNIT_TEST(isZero(degrees[1]-Real(40),tolerance));
		UNIT_TEST(isZero(degrees[2]-Real(50),tolerance));

		SpatialTransform concatenation=euler;
		setTranslation(concatenation,SpatialVector(1.0f,2.0f));
		feLog("original\n%s\n",c_print(matrix));
		feLog("concatenation\n%s\n",c_print(concatenation));
		Real sum=0.0;
		for(U32 j=0;j<3;j++)
		{
			for(U32 i=0;i<4;i++)
			{
				sum+=fabs(concatenation(j,i)-matrix(j,i));
			}
		}
		printf("sum of element differences = %.6G\n",sum);
		UNIT_TEST(isZero(sum,tolerance));

		feLog("\nconcatenate XYZ of 30 90 50\n");
		fe::setIdentity(matrix);
		translate(matrix,SpatialVector(1.0f,2.0f));
		rotate(matrix,50.0f*degToRad,e_zAxis);
		rotate(matrix,90.0f*degToRad,e_yAxis);
		rotate(matrix,30.0f*degToRad,e_xAxis);

		euler=matrix;
		degrees=euler/degToRad;

		feLog("euler degrees %s\n",c_print(degrees));
//		UNIT_TEST(isZero(degrees[2]-degrees[0]-Real(20),tolerance));
//		UNIT_TEST(isZero(degrees[1]-Real(90),tolerance));

		concatenation=euler;
		setTranslation(concatenation,SpatialVector(1.0f,2.0f));
		feLog("original\n%s\n",c_print(matrix));
		feLog("concatenation\n%s\n",c_print(concatenation));
		sum=0.0;
		for(U32 j=0;j<3;j++)
		{
			for(U32 i=0;i<4;i++)
			{
				sum+=fabs(concatenation(j,i)-matrix(j,i));
			}
		}
		printf("sum of element differences = %.6G\n",sum);
		UNIT_TEST(isZero(sum,tolerance));

		feLog("\nconcatenate XYZ of 30 -90 50\n");
		setIdentity(matrix);
		translate(matrix,SpatialVector(1.0f,2.0f));
		rotate(matrix,30.0f*degToRad,e_xAxis);
		rotate(matrix,-90.0f*degToRad,e_yAxis);
		rotate(matrix,50.0f*degToRad,e_zAxis);

		euler=matrix;
		degrees=euler/degToRad;

		feLog("euler degrees %s\n",c_print(degrees));
//		UNIT_TEST(isZero(degrees[2]+degrees[0]-Real(-80),tolerance));
//		UNIT_TEST(isZero(degrees[1]-Real(-90),tolerance));

		concatenation=euler;
		setTranslation(concatenation,SpatialVector(1.0f,2.0f));
		feLog("original\n%s\n",c_print(matrix));
		feLog("concatenation\n%s\n",c_print(concatenation));
		sum=0.0;
		for(U32 j=0;j<3;j++)
		{
			for(U32 i=0;i<4;i++)
			{
				sum+=fabs(concatenation(j,i)-matrix(j,i));
			}
		}
		printf("sum of element differences = %.6G\n",sum);
		UNIT_TEST(isZero(sum,tolerance));

#if FALSE
		// AJ 45.54 -1.53 88.44
		setIdentity(matrix);
		rotate(matrix,	45.0f*degToRad,e_xAxis);
		rotate(matrix,	0.0f*degToRad,e_yAxis);
		rotate(matrix,	90.0f*degToRad,e_zAxis);
		feLog("\nAJ\n%s\n",c_print(matrix));

		SpatialBasis yUp(e_zAxis,e_yAxis);
		yUp.adapt(matrix);

		feLog("converted\n%s\n",c_print(yUp));

		SpatialVector unitX(1.0,0.0,0.0);
		SpatialVector femur_vector(0.69-0.61,0.29-2.35,-3.99-(-2.07));
		feLog("femur_vector\n%s\n",c_print(femur_vector));
		set(femur_vector,0.0,-2.0,-2.0);
		normalize(femur_vector);
		feLog("simplified\n%s\n",c_print(femur_vector));
		set(matrix,unitX,femur_vector);
		feLog("point to point\n%s\n",c_print(matrix));
		SpatialQuaternion quat;
		set(quat,unitX,femur_vector);
		feLog("quat\n%s\n",c_print(quat));
		matrix=quat;
		feLog("matrix\n%s\n",c_print(matrix));

		// MB -86.793 42.879 -87.816
		setIdentity(matrix);
		rotate(matrix,	90.0f*degToRad,e_zAxis);
		rotate(matrix,	45.0f*degToRad,e_yAxis);
		rotate(matrix,	-90.0f*degToRad,e_xAxis);
		feLog("MB\n%s\n",c_print(matrix));
#endif

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(7);
	UNIT_RETURN();
}
