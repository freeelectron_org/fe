/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "native/native.pmh"

using namespace fe;
using namespace fe::ext;


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexNativeWindowDL");
		UNIT_TEST(successful(result));

		{
			Display* pDisplay=XOpenDisplay(NULL);
			feLog("pDisplay %p\n",pDisplay);
			UNIT_TEST(pDisplay);

			if(!pDisplay)
			{
				feX("no display");
			}

			feLog("display \"%s\"\n",DisplayString(pDisplay));

			const int screenCount=ScreenCount(pDisplay);
			feLog("screenCount %d\n",screenCount);
			UNIT_TEST(screenCount);

			for(I32 screenIndex=0;screenIndex<screenCount;screenIndex++)
			{
				feLog("  screen %d/%d\n",screenIndex,screenCount);

				Window parent=RootWindow(pDisplay,screenIndex);
				feLog("    parent %p\n",parent);

				int attrib[]=
				{
					GLX_RGBA,
					GLX_RED_SIZE,	1,
					GLX_GREEN_SIZE,	1,
					GLX_BLUE_SIZE,	1,
					None
				};

				XVisualInfo* pVisualInfo=glXChooseVisual(
						pDisplay,screenIndex,attrib);
				feLog("    pVisualInfo %p\n",pVisualInfo);
			}
		}

		complete=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}

