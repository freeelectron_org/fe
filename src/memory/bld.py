import sys
import os
forge = sys.modules["forge"]

def setup(module):
    srcname = module.SrcTargetName("memory.cc")
    src = forge.FileTarget(srcname)

    expobjname = module.ObjTargetName("memory")
    expobj = forge.FileTarget(expobjname)
    expobj.AddDep(src)
    expobj.cppmap = { 'memory' : '-DFE_EXPORT_MEMORY' }

    impobjname = module.ObjTargetName("feImportMemory")
    impobj = forge.FileTarget(impobjname)
    impobj.AddDep(src)

    dll = module.DLL("feMemory",[])
    dll.AddDep(expobj)
    if forge.api == 'x86_win32' or forge.api == 'x86_win64':
        dll.cppmap = {}
        dll.cppmap['stdlib'] = '/D _STATIC_CPPLIB'
        forge.deps( ["feMemoryLib"], ["fePlatformLib"] )

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        lib = module.Lib("feImportMemory",[])
        lib.AddDep(impobj)
