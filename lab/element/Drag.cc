/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

//* NOTE drag should be b*v^n, with n from 1 at low speeds to 2 at high speeds
void Drag::operator()(const Record &record)
{
	m_particleRV.bind(record);
	const F32 radius=m_particleRV.radius();
	const SpatialVector& velocity=m_particleRV.velocity();
	const F32 b=5.0f*radius*radius;	//* TODO param
	m_particleRV.force()-=b*velocity;
}

} /* namespace ext */
} /* namespace fe */