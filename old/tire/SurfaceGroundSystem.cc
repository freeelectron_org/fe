/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

SurfaceGroundTireSystem::SurfaceGroundTireSystem(void)
{
}

void SurfaceGroundTireSystem::setSurface(sp<ext::SurfaceI> a_spSurface)
{
	m_spSurface = a_spSurface;
}

void SurfaceGroundTireSystem::setTransform(const SpatialTransform &a_transform)
{
	m_transform = a_transform;
}

void SurfaceGroundTireSystem::compile(const t_note_id &a_note_id)
{
	AsTireLive asTireLive(m_rg_dataset->scope());

	enforce<AsTire,AsTireLive>(m_rg_dataset);

	std::vector<Record> tires;
	asTireLive.filter(tires, m_rg_dataset);

	for(unsigned int i_tire = 0; i_tire < tires.size(); i_tire++)
	{
		Tire tire;
		tire.m_r_tire = tires[i_tire];
		m_tires.push_back(tire);
	}
}

void SurfaceGroundTireSystem::step(t_moa_real a_dt)
{
	if(!m_rg_dataset->scope().isValid()) { return; }

	AsTireLive asTireLive(m_rg_dataset->scope());


	sp<ext::SurfaceI::ImpactI> spImpactI;

	for(unsigned int i_tire = 0; i_tire < m_tires.size(); i_tire++)
	{
		//TODO: This collision model is very very poor.   Much better would be shooting ray(s) in directions of interest from the tire.
		Tire &tire = m_tires[i_tire];

		t_moa_v3 Lt =
			transformVector<3, t_moa_real>(asTireLive.transform(tire.m_r_tire),
			SpatialVector(0.0,0.0,0.0));

		SpatialTransform inv_transform;
		invert(inv_transform, m_transform);

		SpatialVector sLt = transformVector<3, t_moa_real>(inv_transform, Lt);

		spImpactI = m_spSurface->nearestPoint(sLt);

		asTireLive.contact_radius(tire.m_r_tire) = spImpactI->distance();

		if(determinant(asTireLive.transform(tire.m_r_tire)) == 0.0) {continue;}

		SpatialTransform inv_tire_transform;
		invert(inv_tire_transform, asTireLive.transform(tire.m_r_tire));

		SpatialVector ground_N_world =
			rotateVector<3, t_moa_real>(m_transform, spImpactI->normal());


		SpatialVector ground_N_tire =
			rotateVector<3, t_moa_real>(inv_tire_transform, ground_N_world);

		ground_N_tire[0] = 0.0;

		SpatialVector up(0,0,1);
		SpatialVector fd(1,0,0);
		SpatialVector result;
		cross3(result,ground_N_tire,up);

		t_moa_real m = dot(fd,result);

		// protect the asin() from floating point slightly out
		if(m > 1.0) { m = 1.0; }
		if(m < -1.0) { m = -1.0; }

		asTireLive.inclination(tire.m_r_tire) = asin(m);
	}
}

SurfaceClosestPointSystem::SurfaceClosestPointSystem(void)
{
}

void SurfaceClosestPointSystem::compile(const t_note_id &a_note_id)
{
	rg_all = m_rg_dataset;
	m_asPoint.bind(rg_all->scope());
	m_asContact.bind(rg_all->scope());
	m_asLocator.bind(rg_all->scope());

	enforce<AsContact,AsContactLive>(m_rg_dataset);

	for(fe::RecordGroup::iterator i_rg = rg_all->begin();
		i_rg != rg_all->end(); i_rg++)
	{
		fe::sp<fe::RecordArray> spRA = *i_rg;

		if(!m_asContact.check(spRA)) { continue; }

		if(m_asLocator.check(spRA))
		{
			for(unsigned int i_r = 0; i_r < spRA->length(); i_r++)
			{
				m_xform_contacts.push_back(spRA->getRecord(i_r));
			}
		}
	}


	m_asSurface.bind(rg_all->scope());
	m_asSurfacePlane.bind(rg_all->scope());
	enforce<AsSurface,AsSurfaceLive>(m_rg_dataset);

	std::vector<fe::Record> surfaces;
	m_asSurface.filter(surfaces, m_rg_dataset);

	for(unsigned int i_surface = 0; i_surface < surfaces.size(); i_surface++)
	{
		fe::Record &r_surface = surfaces[i_surface];

		if(!m_asSurface.spSurfaceI(r_surface).isValid())
		{
			if(m_asSurfacePlane.check(r_surface))
			{
				m_asSurface.spSurfaceI(r_surface) =
					registry()->create("SurfaceI.SurfacePlane");
				if(determinant(m_asSurface.transform(r_surface)) == 0.0)
				{
					setIdentity(m_asSurface.transform(r_surface));
				}
// hacked in tilt to test stick on the open viewer
#if 0
t_moa_xform xform;
setIdentity(xform);
rotate(xform, 0.4, fe::e_xAxis);
translate(xform, t_moa_v3(0,0,-0.3));
m_asSurface.transform(r_surface) = xform;
#endif
			}
		}
		if(!m_asSurface.spSurfaceI(r_surface).isValid())
		{
			continue;
		}

		m_surfaces.push_back(r_surface);
	}
}

void SurfaceClosestPointSystem::step(t_moa_real a_dt)
{
	if(!m_rg_dataset->scope().isValid()) { return; }

	AsTireLive asTireLive(m_rg_dataset->scope());

	sp<ext::SurfaceI::ImpactI> spImpactI;

	for(unsigned int i_contact = 0; i_contact < m_xform_contacts.size();
		i_contact++)
	{
		fe::Record &r_contact = m_xform_contacts[i_contact];

		t_moa_real closest = 1.0e12;

		for(unsigned int i_surface = 0; i_surface < m_surfaces.size();
			i_surface++)
		{
			fe::Record &r_surface = m_surfaces[i_surface];

			SpatialTransform inv_transform;
			invert(inv_transform, m_asSurface.transform(r_surface));

			SpatialVector sLt = transformVector<3, t_moa_real>(inv_transform,
				m_asLocator.transform(r_contact).translation());

			spImpactI = m_asSurface.spSurfaceI(r_surface)->nearestPoint(sLt);

			t_moa_v3 contact = transformVector<3, t_moa_real>(
				m_asSurface.transform(r_surface),
				spImpactI->intersection());

			t_moa_real m = magnitude(contact);
			if(m > closest) { continue; }
			closest = m;

			m_asContact.contact(r_contact) = contact;

			t_moa_v3 N, T, BN;
			N = spImpactI->normal();
			if(m_asSurfacePlane.check(r_surface))
			{
				// on this plane, N should always be 0.00  0.00  1.00
				T = t_moa_v3(1.0,0.0,0.0);
				BN = t_moa_v3(0.0,1.0,0.0);
			}
			else
			{
				BN = t_moa_v3(0.0,1.0,0.0);
				if(fabs(dot(N,BN)) > 0.707)
				{
					BN = t_moa_v3(0.0,0.0,1.0);
				}
				cross3(T, BN, N);
				normalize(T);
				cross3(BN, T, N);
			}

			m_asContact.normal(r_contact) =
				rotateVector<3,t_moa_real>(m_asSurface.transform(r_surface),N);
			m_asContact.tangent(r_contact) =
				rotateVector<3,t_moa_real>(m_asSurface.transform(r_surface),T);
			m_asContact.binormal(r_contact) =
				rotateVector<3,t_moa_real>(m_asSurface.transform(r_surface),BN);

			m_asContact.velocity(r_contact) =
				rotateVector<3, t_moa_real>(m_asSurface.transform(r_surface),
				m_asSurface.velocity(r_surface));
		}
	}
}

} /* namespace ext */
} /* namespace fe */
