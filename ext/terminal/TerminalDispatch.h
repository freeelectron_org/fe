/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terminal_TerminalDispatch_h__
#define __terminal_TerminalDispatch_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Abstract control of a TerminalNode

	@ingroup terminal
*//***************************************************************************/
class FE_DL_EXPORT TerminalDispatch:
	public Dispatch,
	public CastableAs<TerminalDispatch>,
	virtual public ConfigI
{
	public:

				TerminalDispatch(void);
virtual			~TerminalDispatch(void);

				//* as Dispatch
virtual	bool	call(const String &a_name,Array<Instance>& a_argv);
virtual	bool	call(const String &a_name,InstanceMap& a_argv);

				//* as ConfigI
virtual	void	configParent(sp<ConfigI> a_parent)							{}
virtual	bool	configLookup(const String &a_key, Instance &a_instance);
virtual	void	configSet(const String &a_key, Instance &a_instance);
virtual	void	configSet(const String &a_key, Record &a_record,
						const String &a_attr)								{}

	private:

		TerminalNode				m_terminalNode;
		sp<Scope>					m_spScope;
		Record						m_outputRecord;
		sp<Layout>					m_spSurfaceLayout;
		Accessor< sp<Component> >	m_accessorAccessible;

		Array< sp<Component> >		m_lastInputs;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terminal_TerminalDispatch_h__ */
