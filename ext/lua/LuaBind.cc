/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

namespace fe
{
namespace ext
{

LuaBind::LuaBind(void)
{
}

LuaBind::~LuaBind(void)
{
	lua_close(m_pLuaState);
}

void LuaBind::initialize(void)
{
	m_pLuaState = lua_newstate(lua_alloc, NULL);
	luaL_Reg lualibs[] = {
		{ "base",       luaopen_base },
		{ "table",      luaopen_table },
		{ "io",         luaopen_io },
		{ "string",     luaopen_string },
		{ "math",       luaopen_math },
		{ "debug",      luaopen_debug },
		{ "loadlib",    luaopen_package },
		{ NULL,         NULL }
		};

		luaL_Reg *lib;

		for (lib = lualibs; lib->func != NULL; lib++)
		{
			lua_pushcfunction(m_pLuaState, lib->func);
			lua_pushstring(m_pLuaState, lib->name);
			lua_call(m_pLuaState, 1, 0);
			lua_settop(m_pLuaState, 0);
		}
		std::ifstream file;
		file.open("test/test.lua");
		file.seekg (0, std::ios::end);
		int length = file.tellg();
		file.seekg (0, std::ios::beg);

		char *buffer = new char [length+1];
		memset((void *)buffer, 0, length+1);

		file.read (buffer,length);

		m_chunk = buffer;

		delete [] buffer;
		file.close();
}

void LuaBind::bind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_spContext = new LuaContext(spSignaler->registry()->master(), m_pLuaState);
}

void LuaBind::handle(Record &r_sig)
{
	t_lua_record::push(m_spContext.raw(), r_sig);
	lua_setglobal(m_pLuaState, "sig");

	lua_pushcfunction(m_pLuaState, LuaContext::debug);

	int result;
	result = luaL_loadbuffer(m_pLuaState, m_chunk.c_str(), m_chunk.length(),
			"LuaBind");
	if(result != 0)
	{
		feLog("LUA syntax error: %s \n", lua_tostring(m_pLuaState, -1));
	}
	else
	{
		result = lua_pcall(m_pLuaState, 0, LUA_MULTRET, -2);
		if(result)
		{
			String err = lua_tostring(m_pLuaState, -1);
			feLog(err.c_str());
			//feX("LuaBind::handle",
			//	err.c_str());
		}
	}

//	double r = lua_tonumber(m_pLuaState, lua_gettop(m_pLuaState));
}

} /* namespace ext */
} /* namespace fe */
