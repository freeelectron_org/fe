/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <godot/native/godotNative.pmh>

#include "platform/dlCore.cc"

extern "C" void FE_DL_EXPORT godot_gdnative_init(
	godot_gdnative_init_options *o)
{
	feLog("FE godot_gdnative_init()\n");

	godot::Godot::gdnative_init(o);
}

extern "C" void FE_DL_EXPORT godot_gdnative_terminate(
	godot_gdnative_terminate_options *o)
{
	feLog("FE godot_gdnative_terminate()\n");

	godot::Godot::gdnative_terminate(o);
}

extern "C" void FE_DL_EXPORT godot_nativescript_init(void *handle)
{
	feLog("FE godot_nativescript_init()\n");

	godot::Godot::nativescript_init(handle);

	godot::register_class<fe::ext::GodotNode>();
}

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSurfaceDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
