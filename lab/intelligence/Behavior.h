/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Behavior_h__
#define __intelligence_Behavior_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Behavior RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Behavior: public Periodic
{
	public:
		Functor< hp<Component> >	behaviorHandle;
		Functor<String>				behaviorName;
		Functor<I32>				serial;

				Behavior(void)			{ setName("Behavior"); }
virtual	void	addFunctors(void)
				{
					Periodic::addFunctors();

					add(behaviorHandle,	FE_SPEC("ai:hpBehavior",
							"hp<Component> to Behavior"));
					add(behaviorName,	FE_SPEC("ai:behaviorName",
							"Behavior name (may indicate a class or script)"));
					add(serial,			FE_USE(":SN"));
				}
virtual	void	initializeRecord(void)
				{
					Periodic::initializeRecord();

					behaviorHandle.attribute()->setSerialize(FALSE);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Behavior_h__ */
