/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <console/console.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
	list.append(new String("fexDrawDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<ConsoleViewer>("QuickViewerI.ConsoleViewer.fe");
	pLibrary->add<NullViewer>("QuickViewerI.NullViewer.fe");

	pLibrary->add<DrawConsole>("DrawI.DrawConsole.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
