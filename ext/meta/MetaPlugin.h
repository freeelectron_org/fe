/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MetaPlugin_h__
#define __operator_MetaPlugin_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Common functionality of meta plugins for Houdini, Maya, etc

	@ingroup meta
*//***************************************************************************/
class FE_DL_EXPORT MetaPlugin: public OperatorPlugin
{
	public:
								MetaPlugin(void);
virtual							~MetaPlugin(void);

								//* as OperatorPlugin
virtual	void					dirty(BWORD a_aggressive)					{}
virtual	void					select(void)								{}


virtual	BWORD					chronicle(String a_change,
										sp<Counted> a_spOldCounted,
										sp<Counted> a_spNewCounted)
								{	return FALSE; }
virtual	BWORD					interrupted(void)			{ return FALSE; }

		void					setupScope(sp<Scope> a_spScope);
		void					setupBrush(sp<Scope> a_spScope);

		void					insertHandler(sp<HandlerI> a_spHandlerI);
		void					removeHandler(sp<HandlerI> a_spHandlerI);

								///	@internal
		sp<DrawI>				drawBrush(void)
								{	return m_spDrawBrush; }

								///	@internal
		sp<DrawI>				drawBrushOverlay(void)
								{	return m_spDrawBrushOverlay; }

								///	@internal
		sp<DrawI>				drawGuideChain(void)
								{	return m_spDrawGuideChain; }

								///	@internal
		sp<DrawI>				drawGuideCached(void)
								{	return m_spDrawGuideCached; }

								///	@internal
		sp<OperatorSurfaceI>	operatorSurface(void)
								{	return m_spOperatorSurfaceI; }

								///	@internal
		U32						inputCount(void)
								{	return m_surfaceInputArray.size(); }

								///	@internal
		sp<Component>			input(U32 a_index)
								{	return m_surfaceInputArray[a_index]; }

								///	@internal
		sp<Component>			output(void)
								{	return m_spSurfaceOutput; }

	protected:
virtual	bool					evalParamBoolean(String a_key,Real a_time);
virtual	I32						evalParamInteger(String a_key,Real a_time);
virtual	String					evalParamString(String a_key,Real a_time);
		BWORD					evalParamCondition(
										String a_conditions,Real a_time);

		void					clearReferences(void);

		sp<OperatorSurfaceI>			m_spOperatorSurfaceI;
		sp<SignalerI>					m_spSignalerI;
		sp<Layout>						m_spCookLayout;
		sp<Layout>						m_spBrushLayout;

		sp<DrawI>						m_spDrawOutput;
		sp<DrawI>						m_spDrawGuideChain;
		sp<DrawI>						m_spDrawGuideCached;
		sp<DrawI>						m_spDrawBrush;
		sp<DrawI>						m_spDrawBrushOverlay;

		Record							m_cookSignal;
		Record							m_brushSignal;

		Accessor< Real >				m_aFrame;
		Accessor< Real >				m_aStartFrame;
		Accessor< Real >				m_aEndFrame;
		Accessor< Real >				m_aTime;
		Accessor< sp<Component> >		m_aSurfaceOutput;
		Accessor< sp<Component> >		m_aDrawOutput;
		Accessor< sp<Component> >		m_aDrawGuide;
		Accessor< sp<Component> >		m_aDrawBrush;
		Accessor< sp<Component> >		m_aDrawBrushOverlay;
		Accessor<Record>				m_aWindowEvent;
		Accessor<SpatialTransform>		m_aCameraTransform;
		Accessor<SpatialVector>			m_aRayOrigin;
		Accessor<SpatialVector>			m_aRayDirection;

		Array< sp<Component> >			m_surfaceInputArray;
		sp<Component>					m_spSurfaceOutput;
		String							m_nameOutput;
		String							m_lastName;
		Real							m_lastTime;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MetaPlugin_h__ */
