/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "datatool/datatool.h"

#include "xRecordView.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feDataDL");
		UNIT_TEST(successful(result));

		{
			sp<Scope> spScope(spRegistry->create("Scope"));
			spScope->setName("TestScope");

			if(!spScope.isValid())
				feX(argv[0], "can't continue");

			MyDerivedView dv;
			dv.bind(spScope);
			Record dvRecord=dv.createRecord();
			UNIT_TEST(dv.myI32()== -3);
			UNIT_TEST(dv.myF32()== 7.8f);
			UNIT_TEST(dv.moreF32()== 9.1f);

			feLog("dv is \n");
			dv.dump();

			dv.myF32() = 42.17f;
			dv.moreF32() = 1.23f;
			UNIT_TEST(dv.myI32()== -3);
			UNIT_TEST(dv.myF32()== 42.17f);
			UNIT_TEST(dv.moreF32()== 1.23f);

			feLog("dv is \n");
			dv.dump();

			Peeker peek;
			peek(*spScope);
			feLog(peek.output().c_str());
			feLog("\n");

			feLog("\nmethod Print\n");
			dv.myMethod.call();
			feLog("\n");

			sp<RecordGroup> spRG(new RecordGroup());
			Record dvRecord2=dv.createRecord();
			dv.myI32()= -4;
			Record dvRecord3=dv.createRecord();
			dv.myI32()= -5;
			spRG->add(dvRecord);
			spRG->add(dvRecord2);
			spRG->add(dvRecord3);

			RecordArrayView<MyDerivedView> rav;
			sp<RecordArray> spRA= *spRG->begin();
			rav.bind(spRA);

			I32 expected= -3;
			for(MyDerivedView& rView: rav)
			{
				feLog("rav %d\n",rView.myI32());

				UNIT_TEST(rView.myI32() == expected--);
			}
		}

		complete=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(12);
	UNIT_RETURN();
}
