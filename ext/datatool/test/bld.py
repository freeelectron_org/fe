import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    tests = [
#               "xRelay",
                "xRecordView" ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
#       ("xRelay.exe",              "",                 None,       None),
        ("xRecordView.exe",         "",                 None,       None) ]
