/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_ScopeDebug_h__
#define __dataui_ScopeDebug_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
#include "RegionalDebug.h"
namespace fe
{
namespace ext
{

/**	GUI for debug displaying a Scope
	*/
class FE_DL_EXPORT ScopeDebug :
	public Initialize<ScopeDebug>,
	virtual	public RegionalDebug
{
	public:
				ScopeDebug(void);
virtual			~ScopeDebug(void);

		void	initialize(void);

	protected:
		class LayoutRegion : public Region
		{
			public:
							LayoutRegion(void)	{ setName("LayoutRegion"); }
virtual						~LayoutRegion(void){}
				sp<Layout>	m_layout;
		};

		class ScopeRegion : public Region
		{
			public:
							ScopeRegion(void)	{ setName("ScopeRegion"); }
virtual						~ScopeRegion(void){}
				sp<Scope>	m_scope;
		};

virtual	void	handleRegion(sp<Region> a_region);
virtual	void	handleDraw(Record &r_sig);
virtual	void	handleHome(void);

	private:
		sp<Layout>							m_layout;
		sp<Scope>							m_spScope;
		std::map< sp<Scope>, sp<Layout> >	m_layoutMap;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __dataui_ScopeDebug_h__ */

