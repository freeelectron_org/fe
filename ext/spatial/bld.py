import sys
import os
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "GridScalarField",
                "GridVectorField",
                "Continuum",
                "Proximity",
                "ProxBrute",
                "ProxMultiGrid",
                "ProxSweep",
                "ProxHash",
                "Flatten",
                "Space",
                "spatialLib" ]

    srcList += [ "spatialDL" ]

    dll = module.DLL( "fexSpatialDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    forge.deps( ["fexSpatialDLLib"], deplibs )

    #forge.includemap['spatial'] = os.path.join(forge.rootPath, '..', 'dep', 'tbb', 'current', 'include')
    #forge.linkmap['spatial'] = '-L' + os.path.join(forge.rootPath, '..', 'dep', 'tbb', 'current', 'build', 'linux_intel64_gcc_cc4.1.2_libc2.5_kernel2.6.18_release') + " -ltbb"
    #forge.linkmap['spatial'] = '-L' + os.path.join(forge.rootPath, '..', 'dep', 'tbb', 'current', 'build', 'linux_intel64_gcc_cc4.4.1_libc2.10.1_kernel2.6.31_release') + " -ltbb"

    # not sure if these are necessary
    #dll.linkmap = {}
    #dll.linkmap['tbb'] = forge.linkmap['tbb']

    forge.tests += [
        ("inspect.exe",     "fexSpatialDL",                 None,       None) ]

    module.Module( 'test' )

