/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_DEL_DEBUG	FALSE

namespace fe
{
namespace ext
{

void DeleteOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("pointGroups")="";
	catalog<String>("pointGroups","label")="Point Groups";
	catalog<String>("pointGroups","suggest")="pointGroups";
	catalog<bool>("pointGroups","joined")=true;
	catalog<String>("pointGroups","hint")=
		"Space delimited list of point groups to remove.";

	catalog<bool>("pointRegex")=false;
	catalog<String>("pointRegex","label")="Regex";
	catalog<String>("pointRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("primitiveGroups")="";
	catalog<String>("primitiveGroups","label")="Primitive Groups";
	catalog<bool>("primitiveGroups","joined")=true;
	catalog<String>("primitiveGroups","hint")=
		"Space delimited list of primitive groups to remove.";

	catalog<bool>("primitiveRegex")=false;
	catalog<String>("primitiveRegex","label")="Regex";
	catalog<String>("primitiveRegex","hint")=
			catalog<String>("pointRegex","hint");

	catalog<bool>("retainGroups")=false;
	catalog<String>("retainGroups","label")="Delete Not in Groups";
	catalog<String>("retainGroups","hint")=
		"Only keep elements in the groups.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void DeleteOp::handle(Record& a_rSignal)
{
#if FE_DEL_DEBUG
	feLog("DeleteOp::handle\n");
#endif

	String pointGroups=catalog<String>("pointGroups");
	String primitiveGroups=catalog<String>("primitiveGroups");
	const BWORD retainGroups=catalog<bool>("retainGroups");

	if(!pointGroups.empty() && !catalog<bool>("pointRegex"))
	{
		pointGroups=pointGroups.convertGlobToRegex();
	}

	if(!primitiveGroups.empty() && !catalog<bool>("primitiveRegex"))
	{
		primitiveGroups=primitiveGroups.convertGlobToRegex();
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	const I32 classCount=2;

	const I32 pointCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_point);
	const I32 primitiveCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_primitive);

	I32 deleteCount(0);
	I32 discardCount(0);

	for(I32 classIndex=0;classIndex<classCount;classIndex++)
	{
		const Element element=classIndex? e_primitiveGroup: e_pointGroup;
		String groups=classIndex? primitiveGroups: pointGroups;

		if(groups.empty())
		{
			continue;
		}

#if FE_DEL_DEBUG
		feLog("DeleteOp::handle delete '%s' \"%s\"\n",
				elementText(element).c_str(),groups.c_str());
#endif

		deleteCount+=deleteElements(spOutputAccessible,
				element,groups,retainGroups);
	}

	if(!retainGroups)
	{
		//* same as GroupDeleteOp
		for(I32 classIndex=0;classIndex<classCount;classIndex++)
		{
			const Element element=classIndex? e_primitiveGroup: e_pointGroup;
			String groups=classIndex? primitiveGroups: pointGroups;

			if(groups.empty())
			{
				continue;
			}

			while(TRUE)
			{
				String token=groups.parse();
				if(token.empty())
				{
					break;
				}

#if FE_DEL_DEBUG
				feLog("DeleteOp::handle discard %s \"%s\"\n",
						elementText(element).c_str(),token.c_str());
#endif

				discardCount+=discardPattern(spOutputAccessible,element,token);
			}
		}
	}

#if FE_DEL_DEBUG
	feLog("DeleteOp::handle discard deleteCount %d\n",deleteCount);
#endif

	const I32 newPointCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_point);
	const I32 newPrimitiveCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_primitive);

	const I32 lostPointCount=pointCount-newPointCount;
	const I32 lostPrimitiveCount=primitiveCount-newPrimitiveCount;

	String summary;
	if(lostPointCount)
	{
		summary.sPrintf("%d point%s",
				lostPointCount,lostPointCount==1? "": "s");
	}
	if(lostPrimitiveCount)
	{
		summary.sPrintf("%s%s%d primitive%s",
				summary.c_str(),summary.empty()? "": " ",
				lostPrimitiveCount,lostPrimitiveCount==1? "": "s");
	}
	if(discardCount)
	{
		summary.sPrintf("%s%s%d group%s",
				summary.c_str(),summary.empty()? "": " ",
				discardCount,discardCount==1? "": "s");
	}
	if(summary.empty())
	{
		summary="no effect";
	}
	catalog<String>("summary")=summary;

#if FE_DEL_DEBUG
	feLog("DeleteOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
