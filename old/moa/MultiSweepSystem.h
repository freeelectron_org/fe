/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_MultiSweepSystem_h__
#define __moa_MultiSweepSystem_h__

namespace fe
{
namespace ext
{

/** @brief Sweep through a range multiple times.

Uses AsMultiSweep.

composer:step -- s -- eval script run every step
composer:outer -- EvalRange -- outer loop range
composer:inner -- EvalRange -- inner loop range
composer:linger -- real -- time to linger on each data point
composer:runup -- real -- runup time before each inner loop pass

And Eval Range is expressed as three numbers, with the third number optionally
specifying a mode.
<start value> <end value> [optional '*' for multiply step mode]<step value>
examples:
	-10 10 2 results in the sequence: -10 -8 -6 -4 -2 0 2 4 6 8 10
	2 512 *2 results in the sequence: 2 4 8 16 32 64 128 256 512
*/
class MultiSweepSystem :
	virtual public SystemI,
	public Initialize<MultiSweepSystem>
{
	public:
				MultiSweepSystem(void);
virtual			~MultiSweepSystem(void);
		void	initialize(void);

virtual	void	start(const t_note_id &a_note_id);

virtual	void	connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

	private:
		void	performDataset(sp<RecordGroup> a_rg_dataset);

		sp<OrchestratorI>							m_spOrchestrator;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_MultiSweepSystem_h__ */


