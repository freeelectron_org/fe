/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Space.h"

namespace fe
{
namespace ext
{

AffineSpace::AffineSpace(void)
{
	m_forward = SpatialTransform::identity();
	m_inverse = m_forward;
}

AffineSpace::~AffineSpace(void)
{
}

void AffineSpace::initialize(void)
{
}

void AffineSpace::to(SpatialVector &a_out, const SpatialVector &a_in)
{
	transformVector(m_forward, a_in, a_out);
}

void AffineSpace::from(SpatialVector &a_out, const SpatialVector &a_in)
{
	transformVector(m_inverse, a_in, a_out);
}

void AffineSpace::setTransform(const SpatialTransform &a_transform)
{
	m_forward = a_transform;
	invert(m_inverse, m_forward);
}

void AffineSpace::setAxisAligned(const SpatialVector &a_origin,
		const SpatialVector &a_extent)
{
	SpatialTransform transform;
	setIdentity(transform);

	translate(transform, a_origin);
	scale(transform, a_extent);

	setTransform(transform);
}


} /* namespace ext */
} /* namespace fe */

