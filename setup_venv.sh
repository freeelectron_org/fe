#! /usr/bin/env bash

# Ensure errors abort the script.
set -o errexit

PYTHON="python3"
VENVDIR="venv"

function error_exit {
    echo "Script failure on line $1"
    exit 1
}

trap 'error_exit $LINENO' ERR

if ! which virtualenv ; then
    echo "virtualenv command unavailable. Please install."
    exit 1
fi

rm -rf $VENVDIR
virtualenv --always-copy --python $PYTHON $VENVDIR

ACTIVATE="source $VENVDIR/bin/activate"

eval $ACTIVATE

pip install --upgrade pip
pip install -r requirements.txt

# reset signal responses
trap - INT TERM EXIT
