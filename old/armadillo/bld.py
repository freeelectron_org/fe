import sys
import os
import re
import shutil
forge = sys.modules["forge"]

re_lib = re.compile(r'.*\.(dll|so|lib|a)')

def setup(module):
    if forge.fe_os == "FE_LINUX":
        forge.linkmap['armadillo'] = ' -llapack -lblas'
        #forge.linkmap['armadillo'] = ' -larmadillo'
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        forge.linkmap['armadillo'] = 'armadillo.lib' # TODO verify
    elif forge.fe_os == 'FE_OSX':
        forge.linkmap['armadillo'] = '-framework armadillo' # TODO verify

def auto(module):
    test_file = """
#include "fe/config.h"
#include "platform/define.h"
#include "armadillo"

int main(void)
{
    return(0);
}
    \n"""

    return forge.cctest(test_file)
