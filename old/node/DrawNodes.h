/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __node_DrawNodes_h__
#define __node_DrawNodes_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "viewer/DrawView.h"
#include "misc/misc.h"

#include "node/nodeAS.h"

namespace fe
{

/**	Draw nodes

	@copydoc DrawNodes_info
	*/
class FE_DL_EXPORT DrawNodes :
	public Initialize<DrawNodes>,
	virtual public HandlerI
{
	public:
				DrawNodes(void);
virtual			~DrawNodes(void);

		void	initialize(void);

virtual void	bind(	sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(	Record &r_sig);

	private:
		void	setColors(sp<RecordGroup> rg_in);
		AsParticle			m_asParticle;
		AsNode				m_asNode;
		AsSignal			m_asSignal;
		AsWindata			m_asWindata;
		AsColor				m_asColor;

		DrawView			m_drawview;

		Vector4f			m_color[2];
};


} /* namespace */

#endif /* __node_DrawNodes_h__ */

