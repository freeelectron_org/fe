/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __evaluate_h__
#define __evaluate_h__

#include "fe/plugin.h"
#include "fe/data.h"
#include "math/math.h"

namespace fe
{
namespace ext
{

// these abstractions of std are probably unnecessary going forward?
template <typename T>
class t_stdvector : public std::vector<T>
{
	public:
		t_stdvector() {}
		t_stdvector(size_t a_size) : std::vector<T>(a_size) {}
		void sort(void)
		{
			std::sort((*this).begin(), (*this).end());
		}
		template <class Compare>
		void sort(Compare compare)
		{
			std::sort((*this).begin(), (*this).end(), compare);
		}
		bool includes(t_stdvector &a_other)
		{
			return std::includes<
				typename std::vector<T>::iterator,
				typename std::vector<T>::iterator >
				(	(*this).begin(), (*this).end(),
					a_other.begin(), a_other.end());
		}

};

class t_stdstring : public std::string
{
	public:
		t_stdstring(void) {}
		t_stdstring(const char * a_string) : std::string(a_string) {}
		t_stdstring(const char * a_string, int a_i) : std::string(a_string, a_i) {}
		t_stdstring(size_t a_size, char a_c) : std::string(a_size, a_c) {}
		t_stdstring(const t_stdstring &a_other) : std::string(a_other) {}
		t_stdstring(const std::string &a_other) : std::string(a_other) {}
		t_stdstring &operator = (const t_stdstring &a_other)
		{
			std::string::operator=(a_other);
			return *this;
		}
};

template <typename T>
class t_stdlist : public std::list<T>
{
	public:
		t_stdlist(void) {}
		t_stdlist(const t_stdlist<T> &a_other) : std::list<T>(a_other) {}
		t_stdlist(const std::list<T> &a_other) : std::list<T>(a_other) {}
		t_stdlist<T> &operator = (const t_stdlist<T> &a_other)
		{
			std::list<T>::operator=(a_other);
			return *this;
		}
};

template <typename K, typename V>
class t_stdmap : public std::map<K,V>
{
	public:
		t_stdmap(void) {}
		t_stdmap(const t_stdmap<K,V> &a_other) : std::map<K,V>(a_other) {}
		t_stdmap(const std::map<K,V> &a_other) : std::map<K,V>(a_other) {}
		t_stdmap<K,V> &operator = (const t_stdmap<K,V> &a_other)
		{
			std::map<K,V>::operator=(a_other);
			return *this;
		}
};


class t_stdostringstream : public std::ostringstream
{
	public:
		t_stdostringstream(void) {}
};

class t_stdistringstream : public std::istringstream
{
	public:
		t_stdistringstream(void) {}
		t_stdistringstream(const t_stdstring &a_string) : std::istringstream(a_string) {}
};


typedef double t_eval_real;

} /* namespace ext */
} /* namespace fe */


#include "evaluate/FunctionI.h"
#include "evaluate/EvaluatorI.h"


#endif /* __evaluate_h__ */

