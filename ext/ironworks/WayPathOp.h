/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ironworks_WayPathOp_h__
#define __ironworks_WayPathOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to connect triangle locators

	@ingroup ironworks
*//***************************************************************************/
class FE_DL_EXPORT WayPathOp:
	public OperatorSurfaceCommon,
	public Initialize<WayPathOp>
{
	public:

						WayPathOp(void):
							m_brushed(FALSE),
							m_totalLength(0.0)								{}

virtual					~WayPathOp(void)									{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	private:

		void			matchLengths(const SpatialVector a_center,
							SpatialVector& a_rArm1,SpatialVector& a_rArm2);

		SpatialVector	envelopePoint(const I32 a_primitiveIndex,
							const I32 a_primitiveCount,
							const SpatialVector a_inputArray[],
							const Real a_envelopeArray[]);

		SpatialVector	nearPoint(const SpatialVector& a_rPrevious,
							const SpatialVector& a_rNext,
							const SpatialVector& a_rFrom);

		WindowEvent			m_event;
		sp<DrawMode>		m_spSolid;
		sp<DrawMode>		m_spOverlay;

		BWORD				m_brushed;

		Real				m_totalLength;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ironworks_WayPathOp_h__ */

