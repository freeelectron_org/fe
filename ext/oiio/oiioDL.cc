/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oiio/oiio.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
//	list.append(new String("fexSignalDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.bmp");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.cin");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.dpx");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.exr");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.fits");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.gif");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.ico");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.iff");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.jpg");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.pbm");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.pgm");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.png");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.ppm");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.rgb");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.tga");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.tif");
	pLibrary->add<ImageOIIO>("ImageI.ImageOIIO.fe.tiff");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	//* prioritize over OpenIL
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.bmp",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.exr",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.jpg",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.pgm",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.png",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.rgb",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.tga",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.tif",1);
	spLibrary->registry()->prioritize("ImageI.ImageOIIO.fe.tiff",1);
}

}
