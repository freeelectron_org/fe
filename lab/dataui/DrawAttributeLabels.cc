/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawAttributeLabels.h"


namespace fe
{
namespace ext
{

DrawAttributeLabels::DrawAttributeLabels(void)
{
}

DrawAttributeLabels::~DrawAttributeLabels(void)
{
}

void DrawAttributeLabels::initialize(void)
{
}

void DrawAttributeLabels::handleBind(sp<SignalerI> spSignalerI,
	sp<Layout> l_sig)
{
	m_aString.bind(l_sig->scope());
	m_aString.setRealFormat("%5.2f");
}

void DrawAttributeLabels::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }

	sp<ViewI> spView(m_drawview.drawI()->view());
//	sp<EventContextI> spEC(m_drawview.windowI()->getEventContextI());

	Color white(1.0,1.0,1.0,1.0);

	//sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input", r_sig);
	// TODO plumb
	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");

	I32 a, d;
	m_drawview.drawI()->font()->fontHeight(&a, &d);

	ViewI::Projection projection = spView->projection();
	Real zoom;
	Vector2 center;
	sp<CameraI> spCamera=spView->camera();
	spCamera->getOrtho(zoom, center);
	spCamera->setOrtho(1.0, Vector2f());
	spView->use(ViewI::e_ortho);

	AsAttributeLabel asAttributeLabel;

	m_attrs.clear();
	m_labels.clear();
	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		if(asAttributeLabel.bindCheck(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(asAttributeLabel.label(spRA, i) == "")
				{
					Label label;
					label.m_name = asAttributeLabel.name(spRA, i);
					label.m_offset = asAttributeLabel.offset(spRA, i);
					m_attrs.push_back(label);
				}
				else
				{
					AnnoLabel label;
					label.m_label = asAttributeLabel.label(spRA, i);
					label.m_name = asAttributeLabel.name(spRA, i);
					label.m_offset = asAttributeLabel.offset(spRA, i);
					m_labels.push_back(label);
				}
			}
		}
	}

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asProjected.bind(spRA->layout()->scope());
		if(m_asProjected.check(spRA))
		{
			for(unsigned int j = 0; j < m_attrs.size(); j++)
			{
				if(m_aString.setup(spRA->layout(), m_attrs[j].m_name) > 0)
				{
					for(int i = 0; i < spRA->length(); i++)
					{
						String label_str;
						Array<String> values;
						m_aString.get(spRA->getRecord(i), values);
						for(unsigned int k = 0; k < values.size(); k++)
						{
							label_str.cat(values[k]);
							if(k < values.size()-1)
							{
								label_str.cat(" ");
							}
						}
						Vector4 textLoc = m_asProjected.location(spRA,i);
							textLoc[0] += (int)(m_attrs[j].m_offset[0] * a);
							textLoc[1] += (int)(m_attrs[j].m_offset[1] * a);
							m_drawview.drawI()->drawAlignedText(textLoc,
									label_str,white);
					}
				}
			}

			for(unsigned int j = 0; j < m_labels.size(); j++)
			{
				sp<Attribute> spAttr = spRA->layout()->scope()->
					findAttribute(m_labels[j].m_name);
				if(spAttr.isValid())
				{
					BaseAccessor aBase;

					aBase.initialize(spRA->layout()->scope(), spAttr);

					if(aBase.check(spRA))
					{
						for(int i = 0; i < spRA->length(); i++)
						{
							Vector4 textLoc = m_asProjected.location(spRA,i);
							textLoc[0] += (int)(m_labels[j].m_offset[0] * a);
							textLoc[1] += (int)(m_labels[j].m_offset[1] * a);
							m_drawview.drawI()->drawAlignedText(textLoc,
									m_labels[j].m_label,white);
						}
					}
				}
			}
		}
	}
	spCamera->setOrtho(zoom, center);
	spView->use(projection);
}



} /* namespace ext */
} /* namespace fe */

