import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "GotoMethod",
                "PlanHandler",
                "DrawChainOps",
                "ChainOpController",
                "DrawRouter",
                "RouteHandler",
                "RouteController",
                "planDL" ]

    dll = module.DLL( "fexPlanDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexViewerLib",
                "fexWindowDLLib",
                "fexSpatialDLLib"
                ]

    forge.deps( ["fexPlanDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexPlanDL",                    None,       None) ]

