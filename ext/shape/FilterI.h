/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_FilterI_h__
#define __shape_FilterI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief A test on a Record

	@ingroup shape
*//***************************************************************************/
class FE_DL_EXPORT FilterI:
	virtual public Component,
	public CastableAs<FilterI>
{
	public:
				/// Establish criteria
virtual	void	configure(WeakRecord record)								=0;

				/// Test for criteria
virtual	I32		test(WeakRecord rRecord)									=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_FilterI_h__ */
