/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ai_StepCost_h__
#define __ai_StepCost_h__

namespace fe
{

/** Calculate a quantity (cost) associated with a directional step in space */
class FE_DL_EXPORT StepCostI : virtual public Component
{
	public:
virtual	Real	cost(const SpatialVector &a_from, const SpatialVector &a_to)= 0;
};

} /* namespace */


#endif /* __ai_StepCost_h__ */

