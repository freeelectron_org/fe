/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_CameraI_h__
#define __draw_CameraI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief viewing position

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT CameraI:
	virtual public Component,
	public CastableAs<CameraI>
{
	public:
							/** @brief Set camera transform

								The camera transform is the inverse of the
								transform to the viewpoint.
							*/
virtual	void				setCameraMatrix(const SpatialTransform& matrix)	=0;
virtual
							/// @brief Get camera transform
const	SpatialTransform&	cameraMatrix(void) const						=0;

							/// @brief Set perspective Field of View (degrees)
virtual	void				setFov(Vector2 a_fov)							=0;
							/// @brief Get perspective Field of View (degrees)
virtual	Vector2				fov(void) const									=0;

							/// @brief Set distance clipping planes
virtual	void				setPlanes(Real nearplane,Real farplane)			=0;
							/// @brief Get distance clipping planes
virtual	void				getPlanes(Real& nearplane,Real& farplane)		=0;

							/// @brief Set clipping planes
virtual	void				setOrtho(Real a_zoom, const Vector2 &a_center)	=0;
							/// @brief Get clipping planes
virtual	void				getOrtho(Real &a_zoom, Vector2 &a_center) const	=0;

							/** @brief choose drawing to 2D pixel coordinates

								Does not apply the cameraMatrix to the render
								system, but still allows it to be used for
								ViewI::project() and ViewI::unproject().

							*/
virtual	void				setRasterSpace(BWORD a_rasterSpace)				=0;
							/// @brief Get raster space mode (on/off)
virtual	BWORD				rasterSpace(void) const							=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_CameraI_h__ */
