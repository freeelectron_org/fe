/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

PickPoint::PickPoint(void):
	m_flags(0),
	m_zValue(0.0f)
{
	set(m_feedback,1.0f,0.0f,0.0f);
	for(U32 m=0;m<3;m++)
	{
		set(m_point[m][0],1.0f,0.0f,0.0f);
		set(m_point[m][1],1.0f,0.0f,0.0f);
	}

	m_spHalo=new DrawMode();
	m_spHalo->setDrawStyle(DrawMode::e_solid);
	m_spHalo->setAntialias(TRUE);
	m_spHalo->setLayer(10);
}

U32 PickPoint::itemIndex(const WindowEvent::Mask& mask) const
{
	if(mask.source()!=WindowEvent::e_sourceMouseButton)
	{
		return 0;
	}
	if(mask.item()==WindowEvent::e_itemMiddle)
	{
		return 1;
	}
	if(mask.item()==WindowEvent::e_itemRight)
	{
		return 2;
	}
	return 0;
}

SpatialVector& PickPoint::point(const WindowEvent::Mask& mask)
{
	return m_point[itemIndex(mask)][mask.state()!=WindowEvent::e_statePress];
}

const SpatialVector& PickPoint::point(const WindowEvent::Mask& mask) const
{
	return m_point[itemIndex(mask)][mask.state()!=WindowEvent::e_statePress];
}

void PickPoint::handle(Record &render)
{
	m_event.bind(render);

	I32 mx,my;
	m_event.getMousePosition(&mx,&my);

	BWORD updateCenter=FALSE;
	BWORD updateFeedback=FALSE;
	BWORD ctrled=false;
	BWORD shifted=false;
	WindowEvent::Item item=m_event.item();

	if(m_event.usesModifiers())
	{
		if(item&WindowEvent::e_keyControl)
		{
			item=WindowEvent::Item(item^WindowEvent::e_keyControl);
			ctrled=true;
		}
		if(item&WindowEvent::e_keyShift)
		{
			item=WindowEvent::Item(item^WindowEvent::e_keyShift);
			shifted=true;
		}
	}

	U32 itemIndex=0;
	if(m_event.mouseButtons()==WindowEvent::e_mbMiddle)
	{
		itemIndex=1;
	}
	else if(m_event.mouseButtons()==WindowEvent::e_mbRight)
	{
		itemIndex=2;
	}

	if(m_event.isMouseButton() || m_event.isMouseMotion())
	{
		m_flags=(m_event.mouseButtons() && !ctrled && !shifted)?
				(e_flagHold<<itemIndex): e_flagNull;
	}

	if(shifted)
	{
		if(m_event.isMousePress())
		{
			m_event.getMousePosition(&m_mouseState[0],
					&m_mouseState[1]);
		}
		else if(m_event.isMouseDrag())
		{
#if FALSE
			m_zValue-=0.01f*(my-m_mouseState[1]);
#else
			I32 mx,my;
			m_event.getMousePosition(&mx,&my);
			I32 winy=height(m_spViewI->viewport())-1-my;
			SpatialVector v1=m_spViewI->unproject(mx,winy,0.0f,
					ViewI::e_perspective);
			SpatialVector v2=m_spViewI->unproject(mx,winy,1.0f,
					ViewI::e_perspective);

			Real ratio=magnitude(m_point[itemIndex][1]-v1)/magnitude(v2-v1);
			m_zValue=ratio*(v2[2]-v1[2])+v1[2];
#endif
			m_mouseState[1]=my;

			updateFeedback=TRUE;
		}
	}

	if(m_event.isUnmodified(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemWheel,WindowEvent::e_stateAny))
	{
		m_zValue+=m_event.state()*3e-4;		//* TODO param
	}

	if(m_event.is(WindowEvent::e_sourceKeyboard,WindowEvent::e_itemAny,
			WindowEvent::e_statePress))
	{
		feLog("key press '%c'\n",m_event.item());
	}
	else if((m_event.isMouseMove() &&
			m_event.isModifiedBy(WindowEvent::e_itemNull)) ||

			(m_event.isUnmodified(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemWheel,WindowEvent::e_stateAny) &&
			m_event.mouseButtons()==WindowEvent::e_mbNone))
	{
		updateFeedback=TRUE;
	}
/*
	else if(m_event.isUnmodified(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemLeft,WindowEvent::e_statePress) ||

			(m_event.isMouseMotion() &&
			m_event.mouseButtons()==WindowEvent::e_mbLeft &&
			m_event.isModifiedBy(WindowEvent::e_itemNull)) ||

			(m_event.isUnmodified(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemWheel,WindowEvent::e_stateAny) &&
			m_event.mouseButtons()==WindowEvent::e_mbLeft))
*/
	else if((m_event.is(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemAny,WindowEvent::e_statePress) &&
			!m_event.isModified()) ||

			(m_event.isMouseMotion() &&
			m_event.mouseButtons() &&
			m_event.isModifiedBy(WindowEvent::e_itemNull)) ||

			(m_event.isUnmodified(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemWheel,WindowEvent::e_stateAny) &&
			m_event.mouseButtons()))
	{
		updateCenter=TRUE;
		updateFeedback=TRUE;
	}

	if(m_spViewI.isValid() && (updateCenter || updateFeedback))
	{
		I32 mx,my;
		m_event.getMousePosition(&mx,&my);
		I32 winy=height(m_spViewI->viewport())-1-my;
		SpatialVector v1=m_spViewI->unproject(mx,winy,0.0f,
				ViewI::e_perspective);
		SpatialVector v2=m_spViewI->unproject(mx,winy,1.0f,
				ViewI::e_perspective);
		Real ratio=(m_zValue-v1[2])/(v2[2]-v1[2]);

		SpatialVector result=(1.0f-ratio)*v1+ratio*v2;

		if(updateCenter)
		{
			m_point[itemIndex][1]=result;
			if(m_event.isMousePress())
			{
				m_point[itemIndex][0]=result;
			}
		}
		if(updateFeedback)
		{
			m_feedback=result;
		}
	}
}

void PickPoint::drawFeedback(sp<DrawI> spDrawI) const
{
	if(m_spViewI.isNull())
	{
		return;
	}

	const Color origin(0.0f,0.0f,0.0f);
	const Color black(0.0f,0.0f,0.0f,1.0f);
	const Color lightgrey(0.7f,0.7f,0.7f,1.0f);
	const Color haze(0.3f,0.3f,0.3f,0.7f);
	const Color green(0.0f,1.0f,0.0f,1.0f);

	Color background=black;
	sp<WindowI> spWindowI=m_spViewI->window();
	if(spWindowI.isValid())
	{
		background=spWindowI->background();
	}
	Color offcolor=background+Color(0.05f,0.05f,0.05f);

	SpatialVector vector[4];
	Color color[4];
	color[0]=background;
	color[1]=offcolor;
	color[2]=lightgrey;
	color[3]=green;
	vector[0]=origin;
	set(vector[1],m_feedback[0],0.0f,0.0f);
	set(vector[2],m_feedback[0],m_feedback[1],0.0f);
	vector[3]=m_feedback;
	spDrawI->drawLines(vector,NULL,4,DrawI::e_strip,true,color);

	set(vector[1],0.0f,m_feedback[1],0.0f);
	spDrawI->drawLines(vector,NULL,3,DrawI::e_strip,true,color);

	spDrawI->pushDrawMode(m_spHalo);
	SpatialVector scale(0.1f,0.1f,m_feedback[2]);
	SpatialTransform transform;
	setIdentity(transform);
	translate(transform,vector[2]);
//	rotate(transform,-90.0f*degToRad,e_xAxis);
	spDrawI->drawCylinder(transform,&scale,1.0f,haze,0);
	spDrawI->popDrawMode();
}

} /* namespace ext */
} /* namespace fe */
