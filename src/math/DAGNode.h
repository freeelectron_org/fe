/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_DAGNode_h__
#define __math_DAGNode_h__

namespace fe
{

/**************************************************************************//**
	@brief Node in a Directed Acyclic Graph

	@ingroup math
*//***************************************************************************/
class FE_DL_EXPORT DAGNode:
	public Handled<DAGNode>,
	public CastableAs<DAGNode>
{
	public:

	class Connection: public Handled<Connection>
	{
		public:
						Connection(sp<DAGNode> a_spChild,
								hp<DAGNode> a_hpParent,
								const String a_childConnector,
								const String a_parentConnector):
							m_spChild(a_spChild),
							m_hpParent(a_hpParent),
							m_childConnector(a_childConnector),
							m_parentConnector(a_parentConnector)			{}

	virtual				~Connection(void)									{}

			bool		operator==(const sp<Connection>& a_spOther)
						{	return a_spOther->m_spChild.raw()==
									m_spChild.raw(); }

			sp<DAGNode>	child(void)				{ return m_spChild; }
			hp<DAGNode>	parent(void)			{ return m_hpParent; }
			String&		childConnector(void)	{ return m_childConnector; }
			String&		parentConnector(void)	{ return m_parentConnector; }

		private:
			sp<DAGNode>	m_spChild;
			hp<DAGNode>	m_hpParent;
			String		m_childConnector;
			String		m_parentConnector;
	};

						DAGNode(void);
virtual					~DAGNode(void);

		void			addParentConnector(const String a_connector);
		void			addChildConnector(const String a_connector);

		Array<String>&	parentConnectors(void);
		Array<String>&	childConnectors(void);

		U32				parentConnectorCount(void) const;
		U32				childConnectorCount(void) const;

		String			parentConnector(U32 a_index);
		String			childConnector(U32 a_index);

		BWORD			hasParentConnector(String a_connector);
		BWORD			hasChildConnector(String a_connector);

		BWORD			attachTo(sp<DAGNode> a_spParentNode,
								String a_localConnector="",
								String a_remoteConnector="");
		BWORD			detachFrom(sp<DAGNode> a_spParentNode);
		void			detach(void);
		BWORD			detach(String a_localConnector);
		void			validate(void) const;

const	Array< hp<Connection> >&	parentConnections(void) const;
const	Array< sp<Connection> >&	childConnections(void) const;

		Array< hp<Connection> >&	parentConnections(void);
		Array< sp<Connection> >&	childConnections(void);

		U32				parentConnectionCount(void) const;
		U32				childConnectionCount(void) const;

		sp<Connection>	parentConnection(U32 a_index) const;
		sp<Connection>	childConnection(U32 a_index) const;

		sp<Connection>	parentConnection(String a_parentConnector) const;
		sp<Connection>	childConnection(String a_childConnector) const;

	private:

		Array<String>			m_parentConnectors;
		Array<String>			m_childConnectors;

		Array< hp<Connection> >	m_parentConnections;
		Array< sp<Connection> >	m_childConnections;

		std::map< String, hp<Connection> >	m_parentMap;
		std::map< String, sp<Connection> >	m_childMap;
};


} /* namespace */

#endif /* __math_DAGNode_h__ */
