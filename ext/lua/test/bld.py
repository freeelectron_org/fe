import sys
import os
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
            "fexSignalLib",
            "fexLuaDLLib",
            "luaLib",
            "lualbLib" ]

    dll = module.DLL("fexLuaTestDL", ["xLuaDL"])

    forge.deps( ["fexLuaTestDLLib"], deplibs)

    tests = [   'xLua' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("inspect.exe",     "fexLuaTestDL",                 None,       None),
        ("xLua.exe",        "",                             None,       None) ]
