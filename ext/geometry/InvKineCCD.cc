/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <geometry/geometry.pmh>

namespace fe
{
namespace ext
{

void InvKineCCD::solve(Array<SpatialVector>& a_location,
	U32 a_start,U32 a_count,const SpatialVector& a_target)
{
	const U32 tip=a_start+a_count-1;
	for(U32 m=a_count-1;m>0;m--)
	{
		const U32 pivot=a_start+m-1;

		const SpatialVector from=unit(a_location[tip]-a_location[pivot]);
		const SpatialVector to=unit(a_target-a_location[pivot]);
		SpatialQuaternion rotate;
		set(rotate,from,to);

		for(U32 n=m;n<a_count;n++)
		{
			const U32 index=a_start+n;

			const SpatialVector before=a_location[index]-a_location[pivot];
			SpatialVector after;
			rotateVector(rotate,before,after);
			a_location[index]=a_location[pivot]+after;
		}
	}
}

} /* namespace ext */
} /* namespace fe */