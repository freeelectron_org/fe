/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __xml_SurfaceAccessibleXml_h__
#define __xml_SurfaceAccessibleXml_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief XML Surface Binding

	@ingroup xml
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleXml:
	public SurfaceAccessibleJoint
{
	public:
								SurfaceAccessibleXml(void)					{}
virtual							~SurfaceAccessibleXml(void)					{}

								//* as SurfaceAccessibleI

								using SurfaceAccessibleJoint::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

	private:

		SpatialTransform		interpretEuler(pugi::xml_node& a_rNode,
									String a_attrName,String a_attrOrder);
		String					interpretString(pugi::xml_node& a_rNode,
									String a_attrName);
		SpatialVector			interpretVector(pugi::xml_node& a_rNode,
									String a_attrName);
		SpatialTransform		interpretTransform(pugi::xml_node& a_rNode,
									String a_attrName);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __xml_SurfaceAccessibleXml_h__ */
