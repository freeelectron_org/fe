/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Particle_h__
#define __element_Particle_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Particle RecordView

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Particle: public Sphere
{
	public:
		Functor<SpatialVector>	velocity;
		Functor<SpatialVector>	force;
		Functor<SpatialVector>	impulse;
		Functor<F32>			mass;
		Functor<I32>			serial;
		Functor<I32>			tempI32;

				Particle(void)		{ setName("Particle"); }
virtual	void	addFunctors(void)
				{
					Sphere::addFunctors();

					add(velocity,	FE_SPEC("spc:velocity",
							"Magnitude and direction of travel"));
					add(force,		FE_SPEC("sim:force",
							"Accumulator of consistant proximal influence"));
					add(impulse,	FE_SPEC("sim:impulse",
							"Accumulator of instantaneous proximal impact"));

					add(mass,		FE_SPEC("sim:mass","Scalar of substance"));
					add(serial,		FE_USE(":SN"));
					add(tempI32,	FE_SPEC("node:tempI32","Reusable cache"));
				}
virtual	void	initializeRecord(void)
				{
					Sphere::initializeRecord();

					mass()=1.0f;
					set(velocity());
					set(force());
					set(impulse());
					tempI32()=0;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Particle_h__ */
