/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_MaskI_h__
#define __viewer_MaskI_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT MaskI : virtual public Component
{
	public:
virtual	void				maskSet(const String &key,
								const WindowEvent::Mask &value)				= 0;
virtual	void				maskDefault(const String &key,
								const WindowEvent::Mask &value)				= 0;
virtual	WindowEvent::Mask	maskGet(const String &key)						= 0;
virtual	Record				maskCreate(const sp<Layout> l_mask,
								const String &key)							= 0;
virtual	void				maskInit(const String &a_prefix)				= 0;
virtual	bool				maskEnabled(const WindowEvent &wev)				= 0;
virtual	void				maskEnable(bool a_setting)						= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_MaskI_h__ */

