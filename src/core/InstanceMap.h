/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_InstanceMap_h__
#define __core_InstanceMap_h__

namespace fe
{

/**	@brief Group of named instances

	@ingroup core
	*/
class FE_DL_EXPORT InstanceMap: public std::map< String, Instance >
{
	public:
				InstanceMap(void)											{}
				InstanceMap(sp<TypeMaster> a_spTypeMaster):
					m_spTypeMaster(a_spTypeMaster)							{}
				~InstanceMap(void)											{}

		void	deepCopy(const InstanceMap& r_other);

		BWORD	exists(String a_name) const
				{	return find(a_name)!=end(); }

				template<class T>
		T&		create(String a_name)
				{
					FEASSERT(m_spTypeMaster.isValid());
					T& rT=(*this)[a_name].create<T>(m_spTypeMaster);
					return rT;
				}

				template<class T>
		T&		property(String a_name)
				{
					if(!exists(a_name))
					{
						create<T>(a_name);
					}
					return (*this)[a_name].cast<T>();
				}

	private:
		sp<TypeMaster>	m_spTypeMaster;
};

} /* namespace */
#endif /* __core_InstanceMap_h__ */
