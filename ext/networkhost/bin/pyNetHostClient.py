#! /usr/bin/python3

import os
scriptDir = os.path.dirname(os.path.realpath(__file__))

import sys
sys.path.append(scriptDir)

print("add '%s'" % scriptDir)

import pyfe.nethost
import numpy

netHost = pyfe.nethost.NetHost()
space = netHost.accessSpace("world")

space.setState("net:role","client")
space.setState("net:address","127.0.0.1")
space.setState("net:port",7890)
space.start()
space.waitForConnection()

maxPolls = 10000
polls = 0
flushCount = -1
frame = -1
unique = 0
running = True
hint = ""
xform = numpy.arange(16, dtype=numpy.float32).reshape(4, 4)

(success, flushCount, spins) = space.waitForUpdate(flushCount)

while frame>=0:
    (success, frame)=space.getState("frame",frame)

while running:
    lastFrame = frame

    if 0:
        (atomic, flushCount, spins) = space.atomic(flushCount)
        (success, running)=atomic.getState("running",running)
        (success, frame)=atomic.getState("frame",frame)
        (success, xform)=atomic.getState("instances[0].transform",xform)
        atomic = None
    else:
        lastFlushCount = flushCount
        while flushCount == lastFlushCount:
            snapshot = space.snapshot()
            flushCount = snapshot.flushCount()

        (success, running)=snapshot.getState("running",running)
        (success, frame)=snapshot.getState("frame",frame)
        (success, xform)=snapshot.getState("instances[0].transform",xform)

        (success, hint)=snapshot.getStateProperty("status","hint",hint)

#       print("flushCount %d frame %d" % (flushCount, frame))
#       print("transform %s" % str(xform))
#       print("hint: \"%s\"" % hint)

        snapshot = None

    (success, incoming) = space.nextMessage("alert");
    if success:
        print("message: \"%s\" frame %d" % (incoming, frame))

    if (frame%100) == 50:
        space.sendMessage("request","Hello server!");

        message = "client is on frame %d" % frame
        space.sendMessage("request",message);

        space.flush();

    if frame != lastFrame:
        unique = unique + 1

    polls = polls + 1
    if polls > maxPolls:
        print("reached max polls %d" % maxPolls)
        break

print("final %d polls %d unique %d\n" % (frame,polls,unique))

(success, xform)=space.getState("instances[0].transform",xform)

print("last transform:\n %s\n" % str(xform))

print("status hint: \"%s\"\n" % hint)

space.stop()
