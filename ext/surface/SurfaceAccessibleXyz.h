/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleXyz_h__
#define __surface_SurfaceAccessibleXyz_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Point Cloud Surface IO, maybe with labels (for molecules)

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleXyz:
	public SurfaceAccessibleRecord,
	public CastableAs<SurfaceAccessibleXyz>
{
	public:
								SurfaceAccessibleXyz(void)					{}
virtual							~SurfaceAccessibleXyz(void)					{}

								using SurfaceAccessibleRecord::load;

								//* as SurfaceAccessibleI
virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleRecord::save;

								//* as SurfaceAccessibleI
virtual	BWORD					save(String a_filename,
										sp<Catalog> a_spSettings);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleXyz_h__ */
