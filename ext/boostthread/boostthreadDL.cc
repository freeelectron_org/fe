/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <boostthread/boostthread.pmh>

#include "platform/dlCore.cc"

#define FE_BMX_DEBUG	FALSE
#define FE_BTH_DEBUG	FALSE

namespace fe
{
namespace ext
{

void* boostmutex_init(bool a_recursive)
{
	void* pMutex=NULL;
	if(a_recursive)
	{
		pMutex=new BOOST_NAMESPACE::recursive_mutex();
	}
	else
	{
		pMutex=new BOOST_NAMESPACE::shared_mutex();
	}

#if FE_BMX_DEBUG
	feLogError("boostmutex_init recursive %d -> %p\n",
			a_recursive,pMutex);
#endif

	return pMutex;
}

bool boostmutex_lock(bool a_recursive,void* a_pMutex,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_BMX_DEBUG
	feLogError("boostmutex_lock recursive %d mutex %p"
			" un %d try %d read %d\n",
			a_recursive,a_pMutex,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		BOOST_NAMESPACE::recursive_mutex& rMutex=*(BOOST_NAMESPACE::recursive_mutex*)(a_pMutex);

		if(a_un)
		{
			rMutex.unlock();
			return true;
		}
		else if(a_try)
		{
			return rMutex.try_lock();
		}

		rMutex.lock();
		return true;
	}

	BOOST_NAMESPACE::shared_mutex& rMutex=*(BOOST_NAMESPACE::shared_mutex*)(a_pMutex);

	if(a_un)
	{
		rMutex.unlock();
		return true;
	}
	else if(a_readOnly)
	{
		if(a_try)
		{
			return rMutex.try_lock_shared();
		}
		rMutex.lock_shared();
		return true;
	}
	else if(a_try)
	{
		return rMutex.try_lock();
	}
	rMutex.lock();
	return true;
}

void boostmutex_finish(bool a_recursive,void* a_pMutex)
{
#if FE_BMX_DEBUG
	feLogError("boostmutex_finish recursive %d mutex %p\n",
			a_recursive,a_pMutex);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		BOOST_NAMESPACE::recursive_mutex* pMutex=(BOOST_NAMESPACE::recursive_mutex*)(a_pMutex);
		delete pMutex;
		return;
	}

	BOOST_NAMESPACE::shared_mutex* pMutex=(BOOST_NAMESPACE::shared_mutex*)(a_pMutex);
	delete pMutex;
}

void* boostguard_init(bool a_recursive,void* a_pMutex,bool a_readOnly)
{
	FEASSERT(a_pMutex);

	void* pGuard=NULL;
	if(a_recursive)
	{
		BOOST_NAMESPACE::recursive_mutex& rMutex=*(BOOST_NAMESPACE::recursive_mutex*)(a_pMutex);
		pGuard=new BOOST_NAMESPACE::recursive_mutex::scoped_lock(rMutex);
	}
	else if(a_readOnly)
	{
		BOOST_NAMESPACE::shared_mutex& rMutex=*(BOOST_NAMESPACE::shared_mutex*)(a_pMutex);
		pGuard=new BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>(rMutex);
	}
	else
	{
		BOOST_NAMESPACE::shared_mutex& rMutex=*(BOOST_NAMESPACE::shared_mutex*)(a_pMutex);
		pGuard=new BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>(rMutex);
	}

#if FE_BMX_DEBUG
	feLogError("boostguard_init recursive %d mutex %p read %d -> %p\n",
			a_recursive,a_pMutex,a_readOnly,pGuard);
#endif

	return pGuard;
}

bool boostguard_lock(bool a_recursive,void* a_pGuard,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_BMX_DEBUG
	feLogError("boostguard_lock recursive %d guard %p"
			" un %d try %d read %d\n",
			a_recursive,a_pGuard,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	if(a_recursive)
	{
		BOOST_NAMESPACE::recursive_mutex::scoped_lock& rGuard=
				*(BOOST_NAMESPACE::recursive_mutex::scoped_lock*)(a_pGuard);

		if(a_un)
		{
			rGuard.unlock();
			return true;
		}
		else if(a_try)
		{
			return rGuard.try_lock();
		}

		rGuard.lock();
		return true;
	}

	if(a_readOnly)
	{
		BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>& rGuard=
				*(BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>*)(a_pGuard);

		if(a_un)
		{
			rGuard.unlock();
			return true;
		}
		else if(a_try)
		{
			return rGuard.try_lock();
		}

		rGuard.lock();
		return true;
	}

	BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>& rGuard=
			*(BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>*)(a_pGuard);

	if(a_un)
	{
		rGuard.unlock();
		return true;
	}
	else if(a_try)
	{
		return rGuard.try_lock();
	}
	rGuard.lock();
	return true;
}

void boostguard_finish(bool a_recursive,void* a_pGuard,bool a_readOnly)
{
#if FE_BMX_DEBUG
	feLogError("boostguard_finish recursive %d guard %p read %d\n",
			a_recursive,a_pGuard,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	if(a_recursive)
	{
		BOOST_NAMESPACE::recursive_mutex::scoped_lock* pGuard=
				(BOOST_NAMESPACE::recursive_mutex::scoped_lock*)(a_pGuard);
		delete pGuard;
		return;
	}

	if(a_readOnly)
	{
		BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>* pGuard=
				(BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>*)(a_pGuard);
		delete pGuard;
		return;
	}

	BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>* pGuard=
			(BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>*)(a_pGuard);
	delete pGuard;
}

void* boostcondition_init(void)
{
	void* pCondition=new BOOST_NAMESPACE::condition();

#if FE_BMX_DEBUG
	feLogError("boostcondition_init -> %p\n",pCondition);
#endif

	return pCondition;
}

bool boostcondition_wait(void* a_pCondition,bool a_recursive,
	void* a_pGuard,bool a_readOnly)
{
#if FE_BMX_DEBUG
	feLogError("boostcondition_wait %p guard %p\n",a_pCondition,a_pGuard);
#endif

	FEASSERT(a_pGuard);

	BOOST_NAMESPACE::condition& rCondition=
			*(BOOST_NAMESPACE::condition*)(a_pCondition);

	if(a_recursive)
	{
		BOOST_NAMESPACE::recursive_mutex::scoped_lock& rGuard=
				*(BOOST_NAMESPACE::recursive_mutex::scoped_lock*)(a_pGuard);

		rCondition.wait(rGuard);
		return true;
	}

	if(a_readOnly)
	{
		BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>& rGuard=
				*(BOOST_NAMESPACE::shared_lock<BOOST_NAMESPACE::shared_mutex>*)(a_pGuard);

		rCondition.wait(rGuard);
		return true;
	}

	BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>& rGuard=
			*(BOOST_NAMESPACE::unique_lock<BOOST_NAMESPACE::shared_mutex>*)(a_pGuard);

	rCondition.wait(rGuard);
	return true;
}

bool boostcondition_notify(void* a_pCondition,bool a_all)
{
#if FE_BMX_DEBUG
	feLogError("boostcondition_notify %p all %d\n",a_pCondition,a_all);
#endif

	FEASSERT(a_pCondition);

	BOOST_NAMESPACE::condition& rCondition=
			*(BOOST_NAMESPACE::condition*)(a_pCondition);

	if(a_all)
	{
		rCondition.notify_all();
	}
	else
	{
		rCondition.notify_one();
	}

	return true;
}

void boostcondition_finish(void* a_pCondition)
{
#if FE_BMX_DEBUG
	feLogError("boostcondition_finish %p\n",a_pCondition);
#endif

	FEASSERT(a_pCondition);

	BOOST_NAMESPACE::condition* pCondition=(BOOST_NAMESPACE::condition*)(a_pCondition);
	delete pCondition;
}

extern "C"
{

FE_DL_EXPORT bool mutex_init(void)
{
#if FE_BMX_DEBUG
	feLog("BoostThreadDL mutex_init()\n");
#endif

//	feLog("  using boost::mutex\n");

	Mutex::replaceInitFunction(boostmutex_init);
	Mutex::replaceLockFunction(boostmutex_lock);
	Mutex::replaceFinishFunction(boostmutex_finish);

	Mutex::replaceGuardInitFunction(boostguard_init);
	Mutex::replaceGuardLockFunction(boostguard_lock);
	Mutex::replaceGuardFinishFunction(boostguard_finish);

	Mutex::replaceConditionInitFunction(boostcondition_init);
	Mutex::replaceConditionWaitFunction(boostcondition_wait);
	Mutex::replaceConditionNotifyFunction(boostcondition_notify);
	Mutex::replaceConditionFinishFunction(boostcondition_finish);

	return true;
}

}

///////////////////////////////////////////////////////////////////////////////

void* boostthread_default_init(void)
{
	void* pThread=new BOOST_NAMESPACE::thread();

#if FE_BTH_DEBUG
	feLogError("boostthread_default_init -> %p\n",pThread);
#endif

	return pThread;
}

void* boostthread_init(void* a_pFunctor)
{
	FEASSERT(a_pFunctor);

	Thread::Functor& rFunctor=*(Thread::Functor*)(a_pFunctor);
	void* pThread=new BOOST_NAMESPACE::thread(rFunctor);

#if FE_BTH_DEBUG
	feLogError("boostthread_init -> %p\n",pThread);
#endif

	return pThread;
}


void boostthread_interrupt(void* a_pThread)
{
#if FE_BTH_DEBUG
	feLogError("boostthread_interrupt thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	BOOST_NAMESPACE::thread& rThread=*(BOOST_NAMESPACE::thread*)(a_pThread);
	rThread.interrupt();
}

void boostthread_join(void* a_pThread)
{
#if FE_BTH_DEBUG
	feLogError("boostthread_join thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	BOOST_NAMESPACE::thread& rThread=*(BOOST_NAMESPACE::thread*)(a_pThread);
	rThread.join();
}

bool boostthread_joinable(void* a_pThread)
{
#if FE_BTH_DEBUG
	feLogError("boostthread_joinable thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	BOOST_NAMESPACE::thread& rThread=*(BOOST_NAMESPACE::thread*)(a_pThread);
	return rThread.joinable();
}

void boostthread_finish(void* a_pThread)
{
#if FE_BTH_DEBUG
	feLogError("boostthread_finish thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	BOOST_NAMESPACE::thread* pThread=(BOOST_NAMESPACE::thread*)(a_pThread);
	delete pThread;
}

void boostthread_interruption(void)
{
#if FE_BTH_DEBUG
	feLogError("boostthread_interruption\n");
#endif

	BOOST_NAMESPACE::this_thread::interruption_point();
}

int boostthread_concurrency(void)
{
#if FE_BTH_DEBUG
	feLogError("boostthread_concurrency\n");
#endif

	BOOST_NAMESPACE::this_thread::interruption_point();
	return BOOST_NAMESPACE::thread::hardware_concurrency();
}

void* boostgroup_init(void)
{
	BOOST_NAMESPACE::thread_group* pThreadGroup=new BOOST_NAMESPACE::thread_group();

#if FE_BTH_DEBUG
	feLogError("boostgroup_init -> %p\n",pThreadGroup);
#endif

	return pThreadGroup;
}

void* boostgroup_create(void* a_pThreadGroup,void* a_pFunctor)
{
	FEASSERT(a_pThreadGroup);
	FEASSERT(a_pFunctor);

	BOOST_NAMESPACE::thread_group& rThreadGroup=*(BOOST_NAMESPACE::thread_group*)(a_pThreadGroup);
	Thread::Functor& rFunctor=*(Thread::Functor*)(a_pFunctor);
	BOOST_NAMESPACE::thread* pThread=rThreadGroup.create_thread(rFunctor);

#if FE_BTH_DEBUG
	feLogError("boostgroup_create group %p functor %p -> %p\n",
			a_pThreadGroup,a_pFunctor,pThread);
#endif

	return pThread;
}

void boostgroup_join_all(void* a_pThreadGroup)
{
#if FE_BTH_DEBUG
	feLogError("boostgroup_join_all group %p\n",a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);

	BOOST_NAMESPACE::thread_group& rThreadGroup=*(BOOST_NAMESPACE::thread_group*)(a_pThreadGroup);
	rThreadGroup.join_all();
}

void boostgroup_finish(void* a_pThreadGroup)
{
#if FE_BTH_DEBUG
	feLogError("boostgroup_finish %p\n",a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);

	BOOST_NAMESPACE::thread_group* pThreadGroup=(BOOST_NAMESPACE::thread_group*)(a_pThreadGroup);
	delete pThreadGroup;
}

extern "C"
{

FE_DL_EXPORT bool thread_init(void)
{
#if FE_BTH_DEBUG
	feLog("BoostThreadDL thread_init()\n");
#endif

//	feLog("  using boost::thread\n");

	Thread::replaceDefaultInitFunction(boostthread_default_init);
	Thread::replaceInitFunction(boostthread_init);
	Thread::replaceInterruptFunction(boostthread_interrupt);
	Thread::replaceJoinFunction(boostthread_join);
	Thread::replaceJoinableFunction(boostthread_joinable);
	Thread::replaceFinishFunction(boostthread_finish);
	Thread::replaceInterruptionFunction(boostthread_interruption);
	Thread::replaceConcurrencyFunction(boostthread_concurrency);

	Thread::replaceGroupInitFunction(boostgroup_init);
	Thread::replaceGroupCreateFunction(boostgroup_create);
	Thread::replaceGroupJoinAllFunction(boostgroup_join_all);
	Thread::replaceGroupFinishFunction(boostgroup_finish);

	return true;
}

}

} /* namespace ext */
} /* namespace fe */
