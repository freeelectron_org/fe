/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

namespace fe
{

Real epsilon = 1.0;
const EpsilonCalc epscalc;

BWORD intersectRaySphere(Real *a_collisionDistance,
	const SpatialVector &a_rayRoot,
	const SpatialVector &a_rayDirection,
	const SpatialVector &a_sphereCenter,
	const Real &a_sphereRadius)
{
	SpatialVector dir = a_rayDirection;
	if(dir == SpatialVector(0.0,0.0,0.0))
	{
		return false;
	}
	normalize(dir);

	SpatialVector sqr = a_rayRoot - a_sphereCenter;

	Real B = 2.0 *
		(	dir[0] * sqr[0]
		+	dir[1] * sqr[1]
		+	dir[2] * sqr[2]);

	sqr[0] *= sqr[0];
	sqr[1] *= sqr[1];
	sqr[2] *= sqr[2];

	Real C = sqr[0] + sqr[1] + sqr[2] - a_sphereRadius*a_sphereRadius;

	Real discriminant = B*B - 4*C;

	if(discriminant >= 0.0)
	{
		if(a_collisionDistance)
		{
			Real drt = sqrt(discriminant);
			*a_collisionDistance = (-B - drt) / 2.0;
			if(*a_collisionDistance <= 0)
			{
				*a_collisionDistance = (-B + drt) / 2.0;
			}
		}
		return true;
	}

	return false;
}

BWORD intersectLinePlane(SpatialVector &a_intersection,
	BWORD a_loBound, BWORD a_hiBound,
	const SpatialVector &a_lineLo,
	const SpatialVector &a_lineHi,
	const SpatialVector &a_pointOnPlane,
	const SpatialVector &a_planeNormal)
{
	SpatialVector dir = a_lineHi - a_lineLo;

	SpatialVector lineOffset = a_lineLo - a_pointOnPlane;

	Real nrmlLineDot = dot(a_planeNormal, dir);

	Real offset = -dot(a_planeNormal, lineOffset);

	if(fabs(nrmlLineDot) < fe::tol)
	{
		return false;
	}

	Real lineIntersectScale = offset / nrmlLineDot;
	if(a_loBound && lineIntersectScale < 0.0)
	{
		return false;
	}
	if(a_hiBound && lineIntersectScale > 1.0)
	{
		return false;
	}
	a_intersection = a_lineLo + dir * lineIntersectScale;

	return true;
}

BWORD intersectRayPlane(		SpatialVector &a_intersection,
							const SpatialVector &a_lineLo,
							const SpatialVector &a_lineHi,
							const SpatialVector &a_pointOnPlane,
							const SpatialVector &a_planeNormal)
{
	return intersectLinePlane(a_intersection,
		true, false, a_lineLo, a_lineHi, a_pointOnPlane, a_planeNormal);
}

BWORD intersectLineTriangle(	SpatialVector &a_intersection,
	BWORD a_loBound, BWORD a_hiBound,
	const SpatialVector &a_lineLo,
	const SpatialVector &a_lineHi,
	const SpatialVector &a_triA,
	const SpatialVector &a_triB,
	const SpatialVector &a_triC,
	const SpatialVector &a_triNormal)
{
	SpatialVector direction = a_lineHi - a_lineLo;
	SpatialVector lineOffset = a_lineLo - a_triA;
	Real offsetDistance = -dot(a_triC, lineOffset);
	Real normalLineDot = dot(a_triC, direction);

	if(fabs(normalLineDot) < fe::tol)
	{
		return false;
	}

	Real lineIntersectScale = offsetDistance/normalLineDot;

	if(a_loBound && lineIntersectScale < 0.0) { return false; }
	if(a_hiBound && lineIntersectScale > 1.0) { return false; }

	a_intersection = a_lineLo + direction * lineIntersectScale;

	SpatialVector u = a_triB - a_triA;
	SpatialVector v = a_triC - a_triA;
	Real uu = dot(u,u);
	Real uv = dot(u,v);
	Real vv = dot(v,v);
	SpatialVector w = a_intersection - a_triA;
	Real wu = dot(w,u);
	Real wv = dot(w,v);
	Real mag = uv * uv - uu * vv;

	Real p[2];
	p[0] = (uv * wv - vv * wu) / mag;
	if (p[0] < 0.0 || p[0] > 1.0) { return false; }
	p[1] = (uv * wu - uu * wv) / mag;
	if (p[1] < 0.0 || (p[0] + p[1]) > 1.0) { return false; }

	return true;
}

BWORD intersectRayTriangle(	SpatialVector &a_intersection,
	const SpatialVector &a_lineLo,
	const SpatialVector &a_lineHi,
	const SpatialVector &a_triA,
	const SpatialVector &a_triB,
	const SpatialVector &a_triC)
{
	SpatialVector triNormal;
	cross3(triNormal, a_triB-a_triA, a_triC-a_triA);
	normalize(triNormal);
	return intersectLineTriangle(a_intersection,
		true, false, a_lineLo, a_lineHi, a_triA, a_triB, a_triC, triNormal);
}

typedef SpatialVector t_two_vecs[2];

BWORD intersectTrianglePlane( SpatialVector &a_intersectionA,
	SpatialVector &a_intersectionB,
	const SpatialVector &a_triA,
	const SpatialVector &a_triB,
	const SpatialVector &a_triC,
	const SpatialVector &a_pointOnPlane,
	const SpatialVector &a_planeNormal)
{
	Real proj[3];
	proj[0] = dot(a_planeNormal,(a_triA - a_pointOnPlane));
	proj[1] = dot(a_planeNormal,(a_triB - a_pointOnPlane));
	proj[2] = dot(a_planeNormal,(a_triC - a_pointOnPlane));

	t_two_vecs linesegments[2];

	if(proj[0] < 0.0)
	{
		if(proj[1] > 0.0)
		{
			if(proj[2] > 0.0)
			{
				linesegments[0][0] = a_triB;
				linesegments[0][1] = a_triA;
				linesegments[1][0] = a_triC;
				linesegments[1][1] = a_triA;
			}
			else
			{
				linesegments[0][0] = a_triB;
				linesegments[0][1] = a_triA;
				linesegments[1][0] = a_triC;
				linesegments[1][1] = a_triB;
			}
		}
		else
		{
			if(proj[2] > 0.0)
			{
				linesegments[0][0] = a_triC;
				linesegments[0][1] = a_triA;
				linesegments[1][0] = a_triC;
				linesegments[1][1] = a_triB;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		if(proj[1] >= 0.0)
		{
			if(proj[2] >= 0.0)
			{
				return false;
			}
			else
			{
				linesegments[0][0] = a_triC;
				linesegments[0][1] = a_triA;
				linesegments[1][0] = a_triC;
				linesegments[1][1] = a_triB;
			}
		}
		else
		{
			if(proj[2] > 0.0)
			{
				linesegments[0][0] = a_triB;
				linesegments[0][1] = a_triA;
				linesegments[1][0] = a_triC;
				linesegments[1][1] = a_triB;
			}
			else
			{
				linesegments[0][0] = a_triB;
				linesegments[0][1] = a_triA;
				linesegments[1][0] = a_triC;
				linesegments[1][1] = a_triA;
			}
		}
	}

	BWORD hit;
	hit = intersectLinePlane(a_intersectionA, true, true,
			linesegments[0][0], linesegments[0][1],
			a_pointOnPlane, a_planeNormal);
	if(!hit) { return false; }

	hit = intersectLinePlane(a_intersectionB, true, true,
			linesegments[1][0], linesegments[1][1],
			a_pointOnPlane, a_planeNormal);
	if(!hit) { return false; }

	return true;
}


BWORD sideCheck(	const SpatialVector &a_p1, 
				const SpatialVector &a_p2,
				const SpatialVector &a_a,
				const SpatialVector &a_b)
{
	SpatialVector x1, x2;
	cross3(x1, a_b - a_a, a_p1 - a_a);
	cross3(x2, a_b - a_a, a_p2 - a_a);
	return (dot(x1, x2) >= 0.0);
}

BWORD pointInTriangle(		const SpatialVector &a_point,
							const SpatialVector &a_triA,
							const SpatialVector &a_triB,
							const SpatialVector &a_triC)
{
	if(		sideCheck(a_point, a_triA, a_triB, a_triC)
		&&	sideCheck(a_point, a_triB, a_triA, a_triC)
		&&	sideCheck(a_point, a_triC, a_triA, a_triB))
	{
		return true;
	}
	return false;
}

// used this 2D impl to validate the 3D version impl above
#if 0
int
cn_PnPoly( SpatialVector P, SpatialVector* V, int n )
{
	int    cn = 0;	// the crossing number counter

	// loop through all edges of the polygon
	for (int i=0; i<n; i++) {	 // edge from V[i] to V[i+1]
		if (((V[i][1] <= P[1]) && (V[i+1][1] > P[1]))	// an upward crossing
		|| ((V[i][1] > P[1]) && (V[i+1][1] <= P[1]))) { // a downward crossing
			// compute the actual edge-ray intersect x-coordinate
			float vt = (float)(P[1] - V[i][1]) / (V[i+1][1] - V[i][1]);
			if (P[0] < V[i][0] + vt * (V[i+1][0] - V[i][0])) // P.x < intersect
			{
				++cn;	// a valid crossing of y=P.y right of P.x
			}
		}
	}
	return (cn&1);	// 0 if even (out), and 1 if odd (in)

}

BWORD pointInTriangle(		const SpatialVector &a_point,
							const SpatialVector &a_triA,
							const SpatialVector &a_triB,
							const SpatialVector &a_triC)
{
	SpatialVector v[4];
	v[0] = a_triA;
	v[1] = a_triB;
	v[2] = a_triC;
	v[3] = a_triA;
	if(cn_PnPoly(a_point, v, 3) == 0)
	{
		return false;
	}
	return true;
}
#endif

Vector2 uvDelta(const Vector2 &a_uv0,const Vector2 &a_uv1)
{
	Vector2 delta=a_uv1-a_uv0;
	for(I32 n=0;n<2;n++)
	{
		if(delta[n]< -0.5)
		{
			delta[n]+=1;
		}
		if(delta[n]>0.5)
		{
			delta[n]-=1;
		}
	}
	return delta;
}

BWORD triangleDuDv(SpatialVector &a_du,SpatialVector &a_dv,
	const SpatialVector &a_point0,
	const SpatialVector &a_point1,
	const SpatialVector &a_point2,
	const Vector2 &a_uv0,const Vector2 &a_uv1,const Vector2 &a_uv2)
{
	const SpatialVector dp1=a_point1-a_point0;
	const SpatialVector dp2=a_point2-a_point0;

	Vector2 duv1=uvDelta(a_uv0,a_uv1);
	Vector2 duv2=uvDelta(a_uv0,a_uv2);

	const Real det=duv1[0]*duv2[1]-duv1[1]*duv2[0];
	if(fabs(det)<1e-9)
	{
#if FALSE
		set(a_du);
		set(a_dv);
#else
		//* provide poor solution, still indicating failure

		//* duv1,duv2 are either identical or at least one is zero length
		if(magnitudeSquared(duv1)<1e-6)
		{
			duv1=duv2;
		}
		else if(magnitudeSquared(duv2)<1e-6)
		{
			duv2=duv1;
		}

		//* now duv1,duv2 should be identical
		a_du=unitSafe(dp1*duv2[1]-dp2*duv1[1]);
		a_dv=unitSafe(dp2*duv1[0]-dp1*duv2[0]);
#endif
		return FALSE;
	}

	//* even though result is normalized, this gives us the signs
	const Real invDet=1.0/det;

	a_du=unitSafe(invDet*(dp1*duv2[1]-dp2*duv1[1]));
	a_dv=unitSafe(invDet*(dp2*duv1[0]-dp1*duv2[0]));

	return TRUE;
}

BWORD tangentFromDuDvN(Vector<3,Real> &a_tangent,
	const Vector<3,Real> &a_du,
	const Vector<3,Real> &a_dv,
	const Vector<3,Real> &a_normal)
{
//	tangent=unitSafe(du+dv);
//	tangent=unitSafe(du);

	const Real dotD=dot(a_du,a_dv);

#if FALSE
	if(dotD>0.9)
	{
		feLog("tangentFromDuDvN nearly parallel du and dv\n");
	}
	else if(dotD<0.9)
	{
		feLog("tangentFromDuDvN nearly opposite du and dv\n");
	}
#endif

	const Real halfAngle=0.5*(acos(Real(0.999999)*dotD)-0.5*fe::pi);
	a_tangent=a_du*cos(halfAngle)+cross(a_normal,a_du)*sin(halfAngle);

	return TRUE;
}

void randomRealSeed(		const U32 &a_seed)
{
	srand(a_seed);
}
Real randomReal(			const Real &a_min,
							const Real &a_max)
{
	return a_min + (a_max-a_min)*((Real)rand()/(Real)RAND_MAX);
}

Real randomRealReentrant(	U32* a_pSeed,
							const Real &a_min,
							const Real &a_max)
{
	//* TODO use <random> in C++11

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	//* HACK
	srand(*a_pSeed);	//* start with old seed
	rand_s((unsigned int*)a_pSeed);	//* replace seed with new value
	return a_min + (a_max-a_min)*((Real)(*a_pSeed)/(Real)RAND_MAX);
#else
	return a_min + (a_max-a_min)*((Real)rand_r(a_pSeed)/(Real)RAND_MAX);
#endif
}

} /* namespace */

