/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

#include "fe/plugin.h"
#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexOperatorDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	//* TODO check for redundancy
	assertMath(spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<BenderOp>(
			"OperatorSurfaceI.BenderOp.fe");
	pLibrary->add<BladeOp>(
			"OperatorSurfaceI.BladeOp.fe");
	pLibrary->add<ClumpOp>(
			"OperatorSurfaceI.ClumpOp.fe");
	pLibrary->add<ContractOp>(
			"OperatorSurfaceI.ContractOp.fe");
	pLibrary->add<CurlOp>(
			"OperatorSurfaceI.CurlOp.fe");
	pLibrary->add<DodgeOp>(
			"OperatorSurfaceI.DodgeOp.fe");
	pLibrary->add<FlatnessOp>(
			"OperatorSurfaceI.FlatnessOp.fe");
	pLibrary->add<KinkOp>(
			"OperatorSurfaceI.KinkOp.fe");
	pLibrary->add<ScatterOp>(
			"OperatorSurfaceI.ScatterOp.fe");
	pLibrary->add<SpineFitOp>(
			"OperatorSurfaceI.SpineFitOp.fe");
	pLibrary->add<TwistBindOp>(
			"OperatorSurfaceI.TwistBindOp.fe");
	pLibrary->add<TwistWrapOp>(
			"OperatorSurfaceI.TwistWrapOp.fe");
	pLibrary->add<WavyOp>(
			"OperatorSurfaceI.WavyOp.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
