/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __imgui_Oscilloscope_h__
#define __imgui_Oscilloscope_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT Oscilloscope:
	public Component,
	public CastableAs<Oscilloscope>
{
	public:
				Oscilloscope(void);
virtual			~Oscilloscope(void);

		void	bind(sp<DrawI> a_spDrawI);

		void	clear(void);

		void	setPane(I32 a_offsetX,I32 a_offsetY,I32 a_sizeX,I32 a_sizeY);
		void	setPage(I32 a_page)		{ m_historyPage=a_page; }

		void	stepValue(void);
		void	stepValue(F64 a_value);
		void	stepValue(Vector3f a_value);
		void	stepValue(Vector3d a_value);
		void	stepValue(SpatialEuler a_value);
		void	stepValue(Color a_value);
		void	stepValue(SpatialQuaternion a_value);
		void	stepValue(SpatialTransform a_value);

	private:

		void	scopeValue(F64 a_value);
		void	scopeValue(SpatialVector a_value);
		void	scopeValue(Vector4d a_value);
		void	scopeValue(Color a_value);

		void	visualizePlanes(void);

		void	step(void);

		void	setHistory(Vector4d a_value);

		void	visualizePlane(Real a_offsetX,Real a_offsetY,
					Real a_width,Real a_height,I32 a_channel0,I32 a_channel1);

		void	scopeValue(Real a_offsetX,Real a_offsetY,
					Real a_width,Real a_height,F64 a_value,I32 a_channel);

				//* color bars
		void	scopeValue(Real a_offsetX,Real a_offsetY,
					Real a_width,Real a_height,Color a_value);

		sp<DrawI>				m_spDrawI;
		sp<DrawMode>			m_spDrawText;

		I32						m_offsetX;
		I32						m_offsetY;
		I32						m_sizeX;
		I32						m_sizeY;
		I32						m_pageWidth;
		I32						m_indicatorSpace;

		Vector4d				m_historyScale;
		I32						m_historyPage;
		I32						m_historyIndex;
		Array<Vector4d>			m_history[4];
		Vector4d				m_lastValueMin[4];
		Vector4d				m_lastValueMax[4];
		I32						m_warmup;
		BWORD					m_doClear;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __imgui_Oscilloscope_h__ */
