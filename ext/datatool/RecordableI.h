/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecordableI_h__
#define __datatool_RecordableI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Generic binding to a serializable state

	@ingroup datatool
*//***************************************************************************/
class FE_DL_EXPORT RecordableI:
	virtual public Component,
	public CastableAs<RecordableI>
{
	public:
virtual	void	bind(Record& a_rRecord)										=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecordableI_h__ */
