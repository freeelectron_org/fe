/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "driveline/driveline.pmh"

#include "platform/dlCore.cc"

using namespace fe;
using namespace ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("feDataDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertInterface<DrivelineI>(spMaster, "DrivelineI");

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<DrivelineISystem>(		"SystemI.DrivelineISystem.fe");
	pLibrary->add<SemiImplicitDriveline>(	"DrivelineI.SemiImplicit.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

