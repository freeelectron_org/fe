/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawThrust_h__
#define __intelligence_DrawThrust_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw propellant expulsion

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawThrust: public DrawShape, public HandlerI
{
	public:
				DrawThrust(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		sp<DrawMode>				m_spTwoSide;

		Theater						m_theaterRV;
		RecordArrayView<Thruster>	m_thrusterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawThrust_h__ */
