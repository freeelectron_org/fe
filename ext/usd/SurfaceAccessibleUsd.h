/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __usd_SurfaceAccessibleUsd_h__
#define __usd_SurfaceAccessibleUsd_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief  USD Surface Binding

	@ingroup usd
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleUsd:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleUsd>
{
	public:
								SurfaceAccessibleUsd(void)					{}
virtual							~SurfaceAccessibleUsd(void)					{}

								//* As Protectable
virtual	Protectable*			clone(Protectable* pInstance=NULL);

								//* as SurfaceAccessibleI

								using SurfaceAccessibleBase::bind;

virtual	void					bind(Instance a_instance)
								{
									m_usdStage=
											a_instance.cast<UsdStageRefPtr>();
								}
virtual	BWORD					isBound(void)
								{	return (m_usdStage!=0); }

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,
										Creation a_create,Writable a_writable);

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create,Writable a_writable);

								using SurfaceAccessibleBase::discard;

virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name);

								using SurfaceAccessibleBase::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::save;

virtual	BWORD					save(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_nodeName,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::count;

virtual	I32						count(String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::surface;

		sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

								//* USD specific
		void					setUsdStage(UsdStageRefPtr a_usdStage)
								{	m_usdStage=a_usdStage; }

		UsdStageRefPtr			usdStage(void)		{ return m_usdStage; }

		void					isolateNode(String a_nodeName)
								{	m_isolateNode=a_nodeName; }
		String					isolatedNode(void)
								{	return m_isolateNode; }

	class PrimNode: public Counted
	{
		public:
							PrimNode(void)
							{
								setName("PrimNode");
							}

			UsdPrim			usdPrim(void) const		{ return m_usdPrim; }
			void			setUsdPrim(UsdPrim a_usdPrim)
							{	m_usdPrim=a_usdPrim; }

			String			usdType(void) const		{ return m_usdType; }
			void			setUsdType(String a_usdType)
							{	m_usdType=a_usdType; }

			String			usdPath(void) const		{ return m_usdPath; }
			void			setUsdPath(String a_usdPath)
							{	m_usdPath=a_usdPath; }

			String			usdParent(void) const	{ return m_usdParent; }
			void			setUsdParent(String a_usdParent)
							{	m_usdParent=a_usdParent; }

			SpatialVector	extentMin(void) const	{ return m_extentMin; }
			void			setExtentMin(SpatialVector a_extentMin)
							{	m_extentMin=a_extentMin; }

			SpatialVector	extentMax(void) const	{ return m_extentMax; }
			void			setExtentMax(SpatialVector a_extentMax)
							{	m_extentMax=a_extentMax; }

			Array<SurfaceAccessibleI::Spec>&	pointSpecs(void)
							{	return m_pointSpecs; }
			Array<SurfaceAccessibleI::Spec>&	vertexSpecs(void)
							{	return m_vertexSpecs; }
			Array<SurfaceAccessibleI::Spec>&	primitiveSpecs(void)
							{	return m_primitiveSpecs; }
			Array<SurfaceAccessibleI::Spec>&	detailSpecs(void)
							{	return m_detailSpecs; }

			I32				pointCount(void) const
							{	return m_pointCount; }
			I32				vertexCount(void) const
							{	return m_vertexCount; }
			I32				primitiveCount(void) const
							{	return m_primitiveCount; }

			void			setPointCount(I32 a_pointCount)
							{	m_pointCount=a_pointCount; }
			void			setVertexCount(I32 a_vertexCount)
							{	m_vertexCount=a_vertexCount; }
			void			setPrimitiveCount(I32 a_primitiveCount)
							{	m_primitiveCount=a_primitiveCount; }

			I32				count(Element a_element) const;

		private:

			UsdPrim							m_usdPrim;
			String							m_usdType;
			String							m_usdPath;
			String							m_usdParent;
			SpatialVector					m_extentMin;
			SpatialVector					m_extentMax;

			Array<SurfaceAccessibleI::Spec>	m_pointSpecs;
			Array<SurfaceAccessibleI::Spec>	m_vertexSpecs;
			Array<SurfaceAccessibleI::Spec>	m_primitiveSpecs;
			Array<SurfaceAccessibleI::Spec>	m_detailSpecs;

			I32								m_pointCount;
			I32								m_vertexCount;
			I32								m_primitiveCount;
	};

const	Array< sp<PrimNode> >&	nodeArray(void) const
								{	return m_nodeArray; }

	private:

virtual	void					reset(void);

		BWORD					interpret(void);

		sp<PrimNode>			primNodeAt(String a_nodeName) const;

		UsdStageRefPtr					m_usdStage;
		String							m_filename;
		String							m_isolateNode;
		BWORD							m_ytoz;
		Real							m_frame;

		Array< sp<PrimNode> >			m_nodeArray;
		std::map<String, sp<PrimNode> >	m_nodeMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __usd_SurfaceAccessibleUsd_h__ */
