/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <usd/usd.pmh>

#define FE_SAUG_DEBUG	FALSE

namespace fe
{
namespace ext
{

BWORD SurfaceAccessorUsdGraph::bind(SurfaceAccessibleI::Element a_element,
		SurfaceAccessibleI::Attribute a_attribute)
{
	if(a_element==SurfaceAccessibleI::e_point &&
			a_attribute==SurfaceAccessibleI::e_position)
	{
		return bindInternal(a_element,"P");
	}
	if(a_element==SurfaceAccessibleI::e_primitive &&
			a_attribute==SurfaceAccessibleI::e_vertices)
	{
		return bindInternal(a_element,"vertices");
	}
	if(a_element==SurfaceAccessibleI::e_primitive &&
			a_attribute==
			SurfaceAccessibleI::e_properties)
	{
		return bindInternal(a_element,"properties");
	}
	return FALSE;
}

BWORD SurfaceAccessorUsdGraph::bind(SurfaceAccessibleI::Element a_element,
		const String& a_name)
{
	m_attribute=SurfaceAccessibleI::e_generic;
	return bindInternal(a_element,a_name);
}

U32	SurfaceAccessorUsdGraph::count(void) const
{
	if(!isBound())
	{
		return 0;
	}

	return elementCount(m_element);
}

U32	SurfaceAccessorUsdGraph::subCount(U32 index) const
{
	if(m_element==SurfaceAccessibleI::e_primitive &&
			index<m_pNodeArray->size())
	{
		return 10;
	}
	return 0;
}

void SurfaceAccessorUsdGraph::set(U32 a_index,U32 a_subIndex,String a_string)
{
	if(m_element==SurfaceAccessibleI::e_detail &&
			m_attrName=="isolateNode")
	{
		sp<SurfaceAccessibleUsd> spSurfaceAccessibleUsd=m_spSurfaceAccessibleI;
		if(spSurfaceAccessibleUsd.isValid())
		{
			spSurfaceAccessibleUsd->isolateNode(a_string);
		}
	}
}

String SurfaceAccessorUsdGraph::string(U32 a_index,U32 a_subIndex)
{
	if(m_element==SurfaceAccessibleI::e_detail && m_attrName=="isolateNode")
	{
		sp<SurfaceAccessibleUsd> spSurfaceAccessibleUsd=m_spSurfaceAccessibleI;
		if(spSurfaceAccessibleUsd.isValid())
		{
			return spSurfaceAccessibleUsd->isolatedNode();
		}
		return String();
	}

	if(!isBound() || a_subIndex>=m_pNodeArray->size())
	{
		return String();
	}

	if(m_attrName=="type")
	{
		return (*m_pNodeArray)[a_index]->usdType();
	}

	if(m_attrName=="name")
	{
		return (*m_pNodeArray)[a_index]->usdPath();
	}

	if(m_attrName=="parent")
	{
		return (*m_pNodeArray)[a_index]->usdParent();
	}

	return String();
}

I32	SurfaceAccessorUsdGraph::integer(U32 a_index,U32 a_subIndex)
{
	if(m_attrName=="properties")
	{
		if(a_subIndex==SurfaceAccessibleI::e_openCurve)
		{
			return 1;
		}

		return 0;
	}

	if(m_attrName=="vertices")
	{
		if(a_subIndex<10)
		{
			return 8*a_index+offsetOfVertex(a_subIndex);
		}
	}

	return 0;
}

Real SurfaceAccessorUsdGraph::real(U32 a_index,U32 a_subIndex)
{
	return 0.0;
}

SpatialVector SurfaceAccessorUsdGraph::spatialVector(
	U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return SpatialVector(0.0,0.0,0.0);
	}

	if(m_attrName=="P")
	{
		const U32 nodeIndex=a_index/8;
		if(nodeIndex<m_pNodeArray->size())
		{
			return extentCorner(nodeIndex,a_index-nodeIndex*8);
		}
	}

	if(a_index>=m_pNodeArray->size())
	{
		return SpatialVector(0.0,0.0,0.0);
	}

	if(m_attrName=="vertices")
	{
		return extentCorner(a_index,offsetOfVertex(a_subIndex));
	}

	if(m_attrName=="extentMin")
	{
		return (*m_pNodeArray)[a_index]->extentMin();
	}

	if(m_attrName=="extentMax")
	{
		return (*m_pNodeArray)[a_index]->extentMax();
	}

	return SpatialVector(0.0,0.0,0.0);
}

I32 SurfaceAccessorUsdGraph::elementCount(
	SurfaceAccessibleI::Element a_element) const
{
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			return 8*m_pNodeArray->size();
		case SurfaceAccessibleI::e_vertex:
			return 0;
		case SurfaceAccessibleI::e_primitive:
			return m_pNodeArray->size();
		case SurfaceAccessibleI::e_detail:
			if(m_attrName=="isolateNode")
			{
				return 1;
			}
			return 0;
		default:
			;
	}

	return 0;
}

SpatialVector SurfaceAccessorUsdGraph::extentCorner(
	I32 a_primitiveIndex,I32 a_offset)
{
	if(a_primitiveIndex<I32(m_pNodeArray->size()))
	{
		const sp<SurfaceAccessibleUsd::PrimNode>& rspPrimNode=
				(*m_pNodeArray)[a_primitiveIndex];
		const SpatialVector extentMin=rspPrimNode->extentMin();
		const SpatialVector extentMax=rspPrimNode->extentMax();

		switch(a_offset)
		{
			case 0:
				return extentMin;
			case 1:
				return SpatialVector(extentMax[0],extentMin[1],extentMin[2]);
			case 2:
				return SpatialVector(extentMax[0],extentMin[1],extentMax[2]);
			case 3:
				return SpatialVector(extentMin[0],extentMin[1],extentMax[2]);
			case 4:
				return SpatialVector(extentMin[0],extentMax[1],extentMin[2]);
			case 5:
				return SpatialVector(extentMax[0],extentMax[1],extentMin[2]);
			case 6:
				return extentMax;
			case 7:
				return SpatialVector(extentMin[0],extentMax[1],extentMax[2]);
			default:
				;
		}
	}

	return SpatialVector(0.0,0.0,0.0);
}

BWORD SurfaceAccessorUsdGraph::bindInternal(
		SurfaceAccessibleI::Element a_element,
		const String& a_name)
{
	if(a_element!=SurfaceAccessibleI::e_primitive &&
			a_element!=SurfaceAccessibleI::e_detail &&
			!(a_element==SurfaceAccessibleI::e_point && a_name=="P"))
	{
		return FALSE;
	}

	//* TODO
	if(a_name=="instance")
	{
		return FALSE;
	}

	const String elementName=
			SurfaceAccessibleBase::elementLayout(a_element);

#if FE_SAUG_DEBUG
	feLog("SurfaceAccessorUsdGraph::bindInternal %s \"%s\"\n",
			elementName.c_str(),a_name.c_str());
#endif

	m_element=a_element;
	m_attrName=a_name;

	sp<SurfaceAccessibleUsd> spSurfaceAccessibleUsd=m_spSurfaceAccessibleI;
	if(spSurfaceAccessibleUsd.isNull())
	{
		return FALSE;
	}

	m_pNodeArray=&spSurfaceAccessibleUsd->nodeArray();
	const I32 nodeCount=m_pNodeArray? m_pNodeArray->size(): 0;

#if FE_SAUG_DEBUG
	feLog("SurfaceAccessorUsdGraph::bindInternal nodeCount %d\n",nodeCount);
#endif

	if(!nodeCount)
	{
#if FE_SAUG_DEBUG
		feLog("SurfaceAccessorUsdGraph::bindInternal %s \"%s\" not found\n",
				elementName.c_str(),a_name.c_str());
#endif

		return FALSE;
	}

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
