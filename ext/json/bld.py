import os
import sys
import re
import utility
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):

    srcList = [ "CatalogReaderJson",
                "CatalogWriterJson",
                "JsonReader",
                "JsonWriter",
                "JsonStream",
                "StringFilterJson",
                "SurfaceAccessibleGeo",
                "SurfaceAccessorGeo",
                "json.pmh",
                "jsonDL" ]

    # add every "jsoncpp/src/lib_json/*.cpp"
    srcList += utility.find_all(module.modPath,"jsoncpp/src/lib_json",r'(.*)\.cpp')

    dll = module.DLL( "fexJsonDL", srcList )

    module.cppmap = {}
    module.cppmap["jsoncpp_export"] = " -D JSON_DLL_BUILD"

    module.includemap = {}
    module.includemap["jsoncpp"] = "ext/json/jsoncpp/include"

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib" ]

    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexJsonDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexJsonDL",                None,       None) ]

    module.Module('test')
