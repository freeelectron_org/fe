/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_h__
#define __core_h__

#include "fe/platform.h"

#ifdef MODULE_core
#define FE_CORE_PORT FE_DL_EXPORT
#else
#define FE_CORE_PORT FE_DL_IMPORT
#endif

extern FE_DL_PUBLIC FE_CORE_PORT fe::Logger* gs_feLogger;
inline fe::Logger* feLogger(void)
{
	if(gs_feLogger==NULL) { gs_feLogger=new fe::Logger(); }
	FEASSERT(gs_feLogger);
	return gs_feLogger;
}

#define FE_STATIC_BAD static

#include "core/MemStream.h"
#include "core/AutoHashMap.h"
#include "core/Castable.h"
#include "core/Array.h"
#include "core/SystemTicker.h"
#include "core/ptr.h"
#include "core/Delete.h"
#include "core/Safe.h"
#include "core/SafeShared.h"
#include "core/Tracker.h"
#include "core/stream.h"
#include "core/Peeker.h"
#include "core/Counted.h"
#include "core/List.h"
#include "core/Type.h"
#include "core/TypeMaster.h"
#include "core/Instance.h"
#include "core/InstanceMap.h"
#include "core/assertCore.h"
#include "core/Allocator.h"
#include "core/PoolAllocator.h"
#include "core/StdAllocator.h"
#include "core/Protectable.h"
#include "core/Handled.h"
#include "core/Profiler.h"
#include "core/Initialized.h"
#include "core/Poison.h"

#endif // __core_h__
