import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.corelibs + [
            'fexViewerDLLib',
            'fexDatauiDLLib',
            'fexDataToolDLLib',
            'fexWindowDLLib' ]

    tests = [   'xDataui',
                'xDebugWindow' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xDataui.exe",         "100",                      None,       None),
        ("xDebugWindow.exe",    "10",                       None,       None) ]
