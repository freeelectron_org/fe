/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawRouter.h"

namespace fe
{

DrawRouter::DrawRouter(void)
{
	m_spWidgets = new RecordGroup();
	m_arrowheadLayoutName = "l_DrawRouter_arrowhead";
}

DrawRouter::~DrawRouter(void)
{
}

void DrawRouter::initialize(void)
{
}

void DrawRouter::handle(Record &r_sig, sp<SignalerI> spSignalerI)
{
	DrawView drawview;
	if(!drawview.handle(r_sig)) { return; }


	Color nfg = drawview.getColor("foreground", Color(1.0,1.0,1.0,1.0));
	Color sfg = drawview.getColor("selected", Color(1.0,1.0,0.0,1.0));
	Color fg;

	SpatialVector zero(0.0,0.0,0.0);

	sp<DrawMode> solid(new DrawMode());
	solid->setDrawStyle(DrawMode::e_solid);
	solid->setAntialias(true);
	solid->setLineWidth(3.0);

	sp<DrawMode> outline(new DrawMode());
	outline->setDrawStyle(DrawMode::e_outline);
	outline->setAntialias(true);
	outline->setLineWidth(3.0);

	// get required groups
	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");
	sp<RecordGroup> rg_selected = cfg< sp<RecordGroup> >("selected");
	sp<RecordGroup> rg_selectable = cfg< sp<RecordGroup> >("selectable");


	// get rotation for screen alignment
	SpatialTransform rotation;
	drawview.drawI()->view()->screenAlignment(rotation);

	sp<ViewI> spView(drawview.drawI()->view());
	sp<ViewMode> orthoMode(new ViewMode());
	*orthoMode = *(spView->viewMode());
	orthoMode->setOrtho(1.0, Vector2f());

	// clear selectables
	rg_selectable->remove(m_spWidgets);
	m_spWidgets->clear();

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> ra_input(*it);
		sp<Scope> spScope = ra_input->layout()->scope();

		// bindings
		m_asSelTri.bind(spScope);
		m_asSelParent.bind(spScope);
		m_asRouter.bind(spScope);
		m_asPoint.bind(spScope);
		m_asBounded.bind(spScope);

		// ensure widget layouts are ready
		sp<Layout> l_arrowhead;
		l_arrowhead = spScope->lookupLayout(m_arrowheadLayoutName);
		if(!l_arrowhead.isValid())
		{
			l_arrowhead = spScope->declare(m_arrowheadLayoutName);
			m_asSelTri.populate(l_arrowhead);
			m_asSelParent.populate(l_arrowhead);
		}

		if(m_asRouter.check(ra_input))
		{
			for(int i = 0; i < ra_input->length(); i++)
			{
				Record r_input = ra_input->getRecord(i);
				Record r_start = m_asRouter.start(r_input);
				Record r_end = m_asRouter.end(r_input);

				// validate record structure
				if(!r_start.isValid() || !r_end.isValid())
				{
					continue;
				}
				if(!m_asPoint.check(r_start) || !m_asBounded.check(r_start))
				{
					continue;
				}
				if(!m_asPoint.check(r_end) || !m_asBounded.check(r_end))
				{
					continue;
				}

				// check if selected
				if(rg_selected->find(r_input))
				{
					fg = sfg;
				}
				else
				{
					fg = nfg;
				}

				SpatialTransform transform;
				Real sc[2];
				SpatialVector scale;

				// draw start
				transform = rotation;
				sc[0] = m_asBounded.radius(r_start);
				set(scale,sc[0],sc[0],sc[0]);
				setTranslation(transform, m_asPoint.location(r_start));
				drawview.drawI()->pushDrawMode(outline);
				//drawview.drawI()->drawCircle(transform,&scale,fg);
				drawview.drawI()->popDrawMode();

				// draw end
				transform = rotation;
				sc[1] = m_asBounded.radius(r_end);
				set(scale,sc[1],sc[1],sc[1]);
				setTranslation(transform, m_asPoint.location(r_end));
				drawview.drawI()->pushDrawMode(outline);
				//drawview.drawI()->drawCircle(transform,&scale,fg);
				drawview.drawI()->popDrawMode();

				// draw line & arrowhead
				SpatialVector screenline[2];
				Real screenscale[2];
				spView->screenInfo(screenscale[0], screenline[0],
					rotation, m_asPoint.location(r_start));
				spView->screenInfo(screenscale[1], screenline[1],
					rotation, m_asPoint.location(r_end));
				spView->setViewMode(orthoMode);
				spView->use(ViewMode::e_ortho);

				SpatialVector screendiff = screenline[1] - screenline[0];
				screendiff[2] = 0.0;
				if(screendiff != zero)
				{
					// draw line
					normalize(screendiff);
					screenline[0] += screendiff * (sc[0] / screenscale[0]);
					screenline[1] -= screendiff * (sc[1] / screenscale[1]);
					drawview.drawI()->pushDrawMode(solid);
					drawview.drawI()->drawLines(screenline, NULL, 2,
							DrawI::e_strip, false, &fg);
					drawview.drawI()->popDrawMode();

					// draw arrowhead
					SpatialVector invscreendiff;
					invscreendiff[0] = -screendiff[1];
					invscreendiff[1] = screendiff[0];

					SpatialVector tri[4];
					SpatialVector tri_nrml[4];
					for(int z = 0; z < 4; z++)
					{
						tri_nrml[z][2] = 1.0;
					}
#if 1
#define LN 21.0
#define WD 15.0
#else
#define LN 50.0
#define WD 25.0
#endif
					tri[0] = screenline[1];
					tri[1] = screenline[1] -
						(Real)LN * screendiff + (Real)(WD/2.0) * invscreendiff;
					tri[2] = tri[1] - (Real)WD * invscreendiff;
					tri[3] = screenline[1];

					drawview.drawI()->pushDrawMode(solid);
					drawview.drawI()->drawTriangles(tri,
						tri_nrml,
						NULL, 3, DrawI::e_discrete, false,
						&fg);

					drawview.drawI()->popDrawMode();

					// create and place an arrowhead selection record
					Record r_arrowhead = spScope->createRecord(l_arrowhead);
					m_asSelParent.group(r_arrowhead) = new RecordGroup();
					m_asSelParent.group(r_arrowhead)->add(r_input);
					m_asSelTri.vertA(r_arrowhead) = tri[0];
					m_asSelTri.vertB(r_arrowhead) = tri[1];
					m_asSelTri.vertC(r_arrowhead) = tri[2];
					rg_selectable->add(r_arrowhead);
					m_spWidgets->add(r_arrowhead);
				}
				spView->use(ViewMode::e_perspective);

			}
		}
	}

}


} /* namespace */

