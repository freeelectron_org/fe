/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconClient_h__
#define __BeaconClient_h__

namespace beacon
{

/// Max milliseconds between messages before we send a keep alive message
#define KEEP_ALIVE_REFRESH_TIME 4000

/// Max milliseconds between messages from Beacon before it is considered dead
#define BEACON_ALIVE_TIME_LIMIT 6000

class FE_DL_EXPORT BeaconClient : virtual public BeaconClientStartupI
{
public:
	BeaconClient();
	virtual ~BeaconClient();

	virtual bool startup(const char *fileName) { return true; }

	// Registers node with the Beacon
	virtual bool registerWithBeacon(const BeaconConfig &config,
			bool timeSync) override;

	virtual void shutdown() override;
	virtual uint8_t getID() override	{ return m_myID; }
	virtual int64_t getTimeOffset() override;
	virtual int64_t getBeaconTime()override;
	virtual int64_t getLatency() override;

	// Returns true if the list of nodes has changed since
	// the last getList() call
	bool isListUpdated() override;
	// Get a list of the nodes registered with the Beacon
	void getList(std::list<Node> &registeredNode) override;
	// Returns true if the Beacon is alive
	// (received a response from the Beacon within the timeout period)
	bool isBeaconAlive() override;

	// Global Dictionary access functions
	virtual bool dictionaryGet(
			std::vector<std::pair<fe::String,fe::String>> &list) override;
	virtual bool dictionarySet(
			const std::vector<std::pair<fe::String,fe::String>> &list) override;
	virtual bool dictionaryUnset(
			const std::vector<std::pair<fe::String,fe::String>> &list) override;
	virtual bool dictionaryClear() override;
	virtual bool dictionaryGetRegex(const fe::String searchString,
			std::vector<std::pair<fe::String,fe::String>> &list) override;
	virtual uint64_t dictionaryGetUpdateCounter() override;

protected:
	BeaconConfig m_config;

private:
	bool m_initialized;
	bool m_registered;
	bool m_timeSynced;
	bool m_done;
	bool m_listUpdated;
	uint8_t m_myID;
	int64_t m_lastSendTime;
	int64_t m_latency;
	int64_t m_deltaTime;
	uint16_t m_responsePort;

	std::atomic<bool> m_running;
	std::atomic<int64_t> m_lastRecTime;
	std::atomic<uint64_t> m_updateCounter;

	std::thread* m_monitorThread;
	fe::sp<fe::SingleMaster> m_spSingleMaster;
	fe::sp<fe::ext::MessageI> m_sendMsgSystem;
	fe::sp<fe::ext::MessageI> m_recMsgSystem;
	fe::sp<fe::ext::MessageReliableUDPI> m_gdMessageSystem;
	fe::sp<beacon::BeaconTimeSyncI> m_beaconTimeSync;
	std::list<Node> m_registeredNodes;
	std::mutex m_nodeListMutex;

	void initVariables();
	bool init();
	bool syncTimeWithBeacon();
	bool getBeaconMessage(fe::ext::Messagegram *msg);
	bool registerNode();
	void monitorThread();
	int64_t getCurrentTime();

	void sendRegisterRequest();
	void sendShutdown();
	void sendKeepAlive();
	bool sendRequest(const fe::ext::Messagegram &m);
	bool sendGDRequest(
			const std::vector<std::pair<fe::String,fe::String>> &list,
			const bool includeValues,
			const NodeMessageType requestType);
	bool waitForResponse(uint8_t **msg, uint32_t &bytesRecv,
			char *fromIPaddress);
	void displayList();
};

} // namespace beacon

#endif // __BeaconClient_h__
