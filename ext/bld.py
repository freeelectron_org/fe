import sys
import os
import re
import glob
import ast

import utility
import common

forge = sys.modules["forge"]

def setup(module):
#   forge.extPath = os.path.join(forge.rootPath,'ext')
    forge.extPath = module.modPath

    forge.includemap['ext'] = forge.extPath

    forge.ext_list = common.find_extensions(forge.extPath)

    suppress = {}
    ext_no_string = os.getenv('FE_NO_EXT')
    if ext_no_string != None:
        suppress = {ext: 1 for ext in ext_no_string.split(':')}

    forge.boost_found = {}

    if(('boostregex' in forge.ext_list and os.path.exists('ext/boostregex')) or ('boostthread' in forge.ext_list and os.path.exists('ext/boostthread'))):
        scan_boost_installs()

    # creates forge.houdini_sources and adds to FE_BOOST_SOURCES
    forge.houdini_sources = []
    if 'houdini' in forge.ext_list:
        scan_houdini_installs()

    forge.maya_sources = []
    if 'maya' in forge.ext_list:
        scan_maya_installs()

    ext_cnt = len(forge.ext_list)

    forge.cprint(forge.CYAN, 1, f"Extensions: {ext_cnt}")

    if not ext_cnt:
        forge.cprint(forge.CYAN, 0,
            "adjust the product list to specify primary 'ext' extensions to "
            "build")
    else:
        common.build_extensions(module, forge.ext_list, suppress)

    forge.help.append("""\
Free Electron extensions specific help
Environment Variables:
FE_EXT            - build only specific extensions
FE_NO_EXT         - avoid building specific extensions
""")

def get_boost_version(boost_path):
    for pattern in [ "*libboost_thread-gcc??-mt-1*",
                        "libhboost_thread-mt.so.1*" ]:
        boost_lib_path = os.path.join(boost_path, pattern)
        boost_list = glob.glob(boost_lib_path)
        if len(boost_list):
            tokens = boost_list[0].split('-')
            tokenCount = len(tokens)
            if tokenCount > 3:
                compiler_version = tokens[tokenCount-3]

                subtokens = tokens[tokenCount-1].split('.')
                subtokenCount = len(subtokens)
                if subtokenCount:
                    numbers = subtokens[0].split('_')
                    numberCount = len(numbers)
                    if numberCount >= 2:
                        boost_version = numbers[0] + '_' + numbers[1]
                        return boost_version
            elif tokenCount==2:
                # new Houdini hboost style
                subtokens = tokens[tokenCount-1].split('.')
                subtokenCount = len(subtokens)
                if subtokenCount>2:
                    boost_version = subtokens[subtokenCount-3] + '_' + subtokens[subtokenCount-2]

                    return boost_version

    boost_version_h = boost_path + "/boost/version.hpp"
    if os.path.exists(boost_version_h):
        for line in open(boost_version_h).readlines():
            if re.match("#define BOOST_LIB_VERSION.*", line):
                boost_version = line.split()[2].replace('"', '')
                return boost_version

    return ""

def scan_boost_installs():
    boost_installs = utility.env_list("FE_BOOST_INSTALLS")
    boost_sources = utility.safer_eval(os.environ["FE_BOOST_SOURCES"])
    for boost_install in boost_installs:
        path_list = glob.glob(boost_install + '/*')

        for path in path_list:
            if os.path.exists(path + '/boost'):
                boost_version = get_boost_version(path)

                if not boost_version in forge.boost_found:
                    include = os.path.normpath(path)

                    if forge.api == "x86_linux":
                        lib_list = glob.glob(path + "/lib")
                    elif forge.api == "x86_win64":
                        lib_list = glob.glob(path + "/lib64*")
                    else:
                        lib_list = glob.glob(path + "/lib32*")
                    if len(lib_list):
                        lib = os.path.normpath(lib_list[0])

                        boost_source = [ boost_version, "", include, lib, "" ]
                        boost_sources += [ boost_source ]
                        forge.boost_found[boost_version] = True
                        continue

                    # TODO check if we still need this fallback
                    lib_list = glob.glob(path + "/stage/lib")
                    if len(lib_list):
                        lib = os.path.normpath(lib_list[0])

                        boost_source = [ boost_version, "", include, lib, "" ]
                        boost_sources += [ boost_source ]
                        forge.boost_found[boost_version] = True
                        continue


def scan_houdini_installs():
    houdini_alembic = []
    houdini_boost = []
    houdini_tbb = []
    houdini_installs = os.environ["FE_HOUDINI_INSTALLS"]
    houdini_toolkit = os.getenv('HT')
    if houdini_toolkit:
        houdini_installs = os.path.normpath(houdini_toolkit + '/../..')
    else:
        # try to set HT and fix houdini_installs
        pattern = "/opt/hfs[0-9]*"
        if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            pattern = "c:/Program Files/Side Effects Software/Houdini [0-9]*"

        houdini_list = glob.glob(pattern)
        houdini_list.reverse()
        for candidate in houdini_list:
            toolkit = candidate + "/toolkit"
            if os.path.isdir(toolkit):
                toolkit = os.path.normpath(toolkit)
                forge.cprint(forge.CYAN,0,"setting HT to " + toolkit)
                os.environ['HT'] = toolkit
                houdini_installs = os.path.normpath(candidate + '/..')
                break;

    houdini_toolkit = os.getenv('HT')

    # Windows HDK requires MSVCDir
    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        MSVCDir = os.getenv('MSVCDir')
        if not MSVCDir:
            MSVCDir = os.getenv('VCToolsInstallDir')
            if MSVCDir:
                os.environ["MSVCDir"] = MSVCDir
        if not MSVCDir:
            pattern = "c:/Program Files/Microsoft Visual Studio/2*"
            mvs_list = glob.glob(pattern)
            mvs_list.reverse()
            for candidate in mvs_list:
                pattern2 = candidate + "/*/VC/Tools/MSVC/*"
                msvc_list = glob.glob(pattern2)
                if len(msvc_list):
                    MSVCDir = os.path.normpath(msvc_list[-1])
                    forge.cprint(forge.CYAN,0,"setting MSVCDir to " + MSVCDir)
                    os.environ['MSVCDir'] = MSVCDir
                    break

    alembic_sources = utility.safer_eval(os.environ["FE_ALEMBIC_SOURCES"])
    alembic_found = {}
    for alembic_source in alembic_sources:
        alembic_version = alembic_source[0]
        alembic_found[alembic_version] = True

    boost_sources = utility.safer_eval(os.environ["FE_BOOST_SOURCES"])
    for boost_source in boost_sources:
        boost_version = boost_source[0]
        forge.boost_found[boost_version] = True

    tbb_sources = utility.safer_eval(os.environ["FE_TBB_SOURCES"])
    tbb_found = {}
    for tbb_source in tbb_sources:
        tbb_version = tbb_source[0]
        tbb_found[tbb_version] = True

    compiler_found = {}
    compiler_messages = {}
    compiler_note = False

    for alembic_source in alembic_sources:
        alembic_version = alembic_source[0]
        alembic_found[ alembic_version ] = True

    houdini_versions = utility.env_list("FE_HOUDINI_VERSIONS")

    rootFiles = []
    if os.path.isdir(houdini_installs):
        rootFiles = os.listdir(houdini_installs)

    if houdini_versions == [ 'auto' ]:
        houdini_versions = []
        for prefix in [ '', 'hfs', 'Houdini ' ]:
            for suffix in [ '', '_64' ]:
#               pattern = prefix + '1[1-8].*' + suffix
#               houdini_list = glob.glob(houdini_installs + '/' + pattern)

                pattern = rf"{prefix}[0-9]+\.[0-9]+\.[0-9]+{suffix}"
                houdini_list = [ ]
                for rootFile in rootFiles:
                    if re.fullmatch(pattern, rootFile):
                        houdini_list += [ rootFile ]

                for installation in houdini_list:
                    houdini_version = os.path.basename(installation)
                    if houdini_version[:3] == 'hfs':
                        houdini_version = houdini_version[3:]
                    if houdini_version[:8] == 'Houdini ':
                        houdini_version = houdini_version[8:]
                    if houdini_version[-3:] == '_64':
                        houdini_version = houdini_version[:-3]
                    if not houdini_version in houdini_versions:
                        houdini_versions += [ houdini_version ]
        houdini_versions = sorted(houdini_versions)

    for version in houdini_versions:
        for prefix in [ '', 'hfs', 'Houdini ' ]:
            for suffix in [ '', '_64' ]:

                houdini_version = version + suffix
                version_path = houdini_installs + "/" + prefix + houdini_version

                if os.path.exists(version_path + '/toolkit'):
                    vpti = version_path + '/toolkit/include'

                    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
                        vpd = version_path + '/bin'
                    else:
                        vpd = version_path + '/dsolib'

                    compiler_version = ''
                    boost_version = get_boost_version(vpd)

                    tbb_version = ''
                    for line in open(vpti + '/tbb/tbb_stddef.h'):
                        if 'TBB_INTERFACE_VERSION ' in line:
                            tbb_version = line.split(' ')[2].rstrip()
                            break

                    alembic_version = common.get_alembic_version(vpti)

                    compilers = utility.safer_eval(os.environ["FE_COMPILERS"])

                    compiler = ''
                    if compiler_version in compilers:
                        compiler = compilers[compiler_version]

                    if forge.pretty < 2:
                        if compiler_version in compiler_found:
                                compiler_messages[compiler_version][1] += ' ' + houdini_version
                        else:
                            if compiler == '':
                                compiler_note = True
                                compiler_messages[compiler_version] = [forge.CYAN, 'using default \'' + forge.cxx + '\' as "' + compiler_version + '" for Houdini ' + houdini_version]
                            else:
                                compiler_messages[compiler_version] = [forge.CYAN, 'using \'' + compiler + '\' as "' + compiler_version + '" for Houdini ' + houdini_version]

                    compiler_found[ compiler_version ] = True

                    forge.houdini_sources += [[ houdini_version, vpti, vpd, compiler ]]

                    if not alembic_version in alembic_found:
                        houdini_alembic += [[ alembic_version, '', vpti, vpd, compiler ]]
                        alembic_found[ alembic_version ] = True

                    if not boost_version in forge.boost_found:
                        houdini_boost += [[ boost_version, '', vpti, vpd, compiler ]]
                        forge.boost_found[ boost_version ] = houdini_version

                    if int(tbb_version) > 9000 and not tbb_version in tbb_found:
                        houdini_tbb += [[ tbb_version, '', vpti, vpd, compiler ]]
                        tbb_found[ tbb_version ] = houdini_version

    for compiler_message in compiler_messages:
        forge.cprint(compiler_messages[compiler_message][0],0,compiler_messages[compiler_message][1])

    if compiler_note:
        forge.cprint(forge.CYAN,0,'see local.env to edit compiler map')

    alembic_sources += houdini_alembic
    boost_sources += houdini_boost
    tbb_sources += houdini_tbb

    os.environ["FE_ALEMBIC_SOURCES"] = str(alembic_sources)
    os.environ["FE_BOOST_SOURCES"] = str(boost_sources)
    os.environ["FE_TBB_SOURCES"] = str(tbb_sources)

    if len(forge.houdini_sources) == 0 and houdini_toolkit:
        forge.houdini_sources = [[ '',
                os.path.join(houdini_toolkit, 'include'), os.getenv('HDSO') ]]

def scan_maya_installs():
    maya_installs = os.environ["FE_MAYA_INSTALLS"]
    maya_root = os.getenv('MAYA_ROOT')
    if maya_root:
        maya_installs = os.path.normpath(maya_root + '/..') + '/'
    else:
        # try to set MAYA_ROOT and fix maya_installs
        pattern = "/usr/autodesk/maya20*"
        if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            pattern = "c:/Program Files/Autodesk/Maya20*"

        maya_list = glob.glob(pattern)
        maya_list.reverse()

        for candidate in maya_list:
            maya_include = candidate + "/include"
            if os.path.isdir(maya_include):
                maya_root = os.path.normpath(candidate)
                forge.cprint(forge.CYAN,0,"setting MAYA_ROOT to " + candidate)
                os.environ['MAYA_ROOT'] = maya_root
                maya_installs = os.path.normpath(candidate + '/..')
                break;

    maya_alembic = []
    maya_tbb = []

    alembic_sources = utility.safer_eval(os.environ["FE_ALEMBIC_SOURCES"])
    alembic_found = {}
    for alembic_source in alembic_sources:
        alembic_version = alembic_source[0]
        alembic_found[ alembic_version ] = True

    tbb_sources = utility.safer_eval(os.environ["FE_TBB_SOURCES"])
    tbb_found = {}
    for tbb_source in tbb_sources:
        tbb_version = tbb_source[0]
        tbb_found[tbb_version] = True

    compiler_found = {}
    compiler_messages = {}
    compiler_note = False

    maya_versions = utility.env_list("FE_MAYA_VERSIONS")

    if maya_versions == [ 'auto' ]:
        maya_versions = []
        for prefix in [ '', 'Maya', 'maya' ]:
            for suffix in [ '-x64', '_64', '_test_64', '' ]:
                pattern = prefix + '20[12][2-9]*' + suffix
                maya_list = glob.glob(maya_installs + '/' + pattern)
                for installation in maya_list:
                    maya_version = os.path.basename(installation)
                    if maya_version[:4] == 'Maya':
                        maya_version = maya_version[4:]
                    if maya_version[:4] == 'maya':
                        maya_version = maya_version[4:]
                    if maya_version[-4:] == '-x64':
                        maya_version = maya_version[:-4]
                    if maya_version[-3:] == '_64':
                        maya_version = maya_version[:-3]
                    if maya_version[-8:] == '_test_64':
                        maya_version = maya_version[:-8]
                    if not maya_version in maya_versions:
                        maya_versions += [ maya_version ]
        maya_versions = sorted(maya_versions)

    for version in maya_versions:
        for prefix in [ '', 'maya' ]:
            for suffix in [ '-x64', '_64', '_test_64', '' ]:
                maya_version = version + suffix
                version_path = maya_installs + '/' + prefix + maya_version
                if os.path.exists(version_path + '/include'):
                    vpti = version_path + '/include'
                    vpd = version_path + '/lib'

                    tbb_version = ''
                    tbb_stddef = vpti + '/tbb/tbb_stddef.h'
                    if not os.path.exists(tbb_stddef):
                        forge.cprint(forge.YELLOW,0,'missing SDK for Maya ' + maya_version)
                        continue

                    versionSplit = maya_version.split('/')
                    versionSplit = versionSplit[len(versionSplit)-2].split('-')
                    versionSplit = versionSplit[0].split('.')
                    versionSplit = versionSplit[0].split('pr')
                    maya_major = versionSplit[0].replace('maya', '')

                    #TODO check binaries (maybe there are builds with different compilers)
#                   compiler_version = 'gcc44'
#                   if int(maya_major) >= 2018:
#                       compiler_version = 'gcc48'
                    compiler_version = ''

                    compilers = utility.safer_eval(os.environ["FE_COMPILERS"])

                    compiler = ''
                    if compiler_version in compilers:
                        compiler = compilers[compiler_version]

                    if forge.pretty < 2:
                        if compiler_version in compiler_found:
                                compiler_messages[compiler_version][1] += ' ' + maya_version
                        else:
                            if compiler == '':
                                compiler_note = True
                                compiler_messages[compiler_version] = [forge.CYAN, 'using default \'' + forge.cxx + '\' as "' + compiler_version + '" for Maya ' + maya_version]
                            else:
                                compiler_messages[compiler_version] = [forge.CYAN, 'using \'' + compiler + '\' as "' + compiler_version + '" for Maya ' + maya_version]

                    compiler_found[ compiler_version ] = True

                    forge.maya_sources += [[ maya_version, vpti, vpd, compiler ]]

                    for line in open(vpti + '/tbb/tbb_stddef.h'):
                        if 'TBB_INTERFACE_VERSION ' in line:
                            tbb_version = line.split(' ')[2].rstrip()
                            break

                    vpdi = version_path + '/devkit/Alembic/include'
                    vpda = version_path + '/devkit/Alembic/lib'

                    alembic_version = common.get_alembic_version(vpdi)

                    if not alembic_version in alembic_found:
                        maya_alembic += [[ alembic_version, '', vpdi, vpda, compiler ]]
                        alembic_found[ alembic_version ] = True

                    if int(tbb_version) > 9000 and not tbb_version in tbb_found:
                        maya_tbb += [[ tbb_version, '', vpti, vpd, compiler ]]
                        tbb_found[ tbb_version ] = maya_version

    for compiler_message in compiler_messages:
        forge.cprint(compiler_messages[compiler_message][0],0,compiler_messages[compiler_message][1])

    if compiler_note:
        forge.cprint(forge.CYAN,0,'see local.env to edit compiler map')

    alembic_sources += maya_alembic
    tbb_sources += maya_tbb

    os.environ["FE_ALEMBIC_SOURCES"] = str(alembic_sources)
    os.environ["FE_TBB_SOURCES"] = str(tbb_sources)

    if len(forge.maya_sources) == 0 and maya_root:
        forge.maya_sources = [[ '',
                os.path.join(maya_root, 'include'),
                os.path.join(maya_root, 'lib'),
                '' ]]
