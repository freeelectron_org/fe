/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_X_GFX

#define FE_X_FB_DEBUG	FALSE

#define FE_X_EVENTMASK	ExposureMask		| \
						EnterWindowMask		| \
						LeaveWindowMask		| \
						KeyPressMask		| \
						KeyReleaseMask		| \
						ButtonPressMask		| \
						ButtonReleaseMask	| \
						StructureNotifyMask

/*** alternates:    PointerMotionMask/ButtonMotionMask
					StructureNotifyMask/SubstructureNotifyMask */


Result NativeWindow::nativeInitialize(void)
{
	FEASSERT(m_spEventContextI.isValid());
	sp<NativeEventContext> spNativeEventContext=m_spEventContextI;
	FEASSERT(spNativeEventContext.isValid());
	Display *pDisplay=spNativeEventContext->displayHandle();

	m_spEventContextI->threadLock();

	//* NOTE try to fit into 80% of one monitor

	Screen* pScreen=DefaultScreenOfDisplay(pDisplay);

//	const I32 screenWidth=XWidthOfScreen(pScreen);
	const I32 screenHeight=XHeightOfScreen(pScreen);

	const int windowX=0.1*screenHeight;
	const int windowY=0.1*screenHeight;
	const int windowHeight=0.8*screenHeight;
	const int windowWidth=1.2*windowHeight;
	setPosition(windowX,windowY);
	setSize(windowWidth,windowHeight);

	m_spEventContextI->threadUnlock();
	return e_ok;
}

Result NativeWindow::nativeStartup(void)
{
	//* in case something goes wrong
	FE_WINDOW_HANDLE default_handle={ NULL, NULL, 0 };
	setWindowHandle(default_handle);

	FEASSERT(m_spEventContextI.isValid());
	sp<NativeEventContext> spNativeEventContext=m_spEventContextI;
	FEASSERT(spNativeEventContext.isValid());
	Display *pDisplay=spNativeEventContext->displayHandle();

	m_spEventContextI->threadLock();
	const int screenIndex=DefaultScreen(pDisplay);

	Window parent=RootWindow(pDisplay,screenIndex);
	if(!parent)
	{
		feLog("NativeWindow::nativeStartup"
				" RootWindow(pDisplay=%p,screenIndex=%d)=NULL\n",
				pDisplay,screenIndex);
		return e_undefinedFailure;
	}

	//* NOTE presumably powers of 2, up to 64
	const I32 multisample=
			registry()->master()->catalog()->catalogOrDefault<I32>(
			"hint:multisample",0);
	I32 samples=(multisample>0)? pow(2,I32(log2(multisample))): 0;

	I32 m=0;
	int attributes[32];
	attributes[m++]=GLX_RENDER_TYPE;
	attributes[m++]=GLX_RGBA_BIT;
	attributes[m++]=GLX_DRAWABLE_TYPE;
	attributes[m++]=GLX_WINDOW_BIT;
	attributes[m++]=GLX_RED_SIZE;
	attributes[m++]=1;
	attributes[m++]=GLX_GREEN_SIZE;
	attributes[m++]=1;
	attributes[m++]=GLX_BLUE_SIZE;
	attributes[m++]=1;
	attributes[m++]=GLX_ALPHA_SIZE;
	attributes[m++]=1;
	attributes[m++]=GLX_DEPTH_SIZE;
	attributes[m++]=1;
	attributes[m++]=GLX_DOUBLEBUFFER;
	attributes[m++]=True;
	attributes[m]=None;

#if FE_X_FB_DEBUG
	feLog("NativeWindow::nativeStartup Display %p \"%s\" screen %d/%d\n",
			pDisplay,DisplayString(pDisplay),
			screenIndex,ScreenCount(pDisplay));
#endif

	int numfbconfigs=0;
	GLXFBConfig* fbconfigs=
			glXChooseFBConfig(pDisplay,screenIndex,attributes,&numfbconfigs);

#if FE_X_FB_DEBUG
	feLog("NativeWindow::nativeStartup FB configs %d\n",numfbconfigs);
#endif

	I32 bestFbconfigI= -1;
	I32 mostSamplesFound= -1;
	for(I32 i=0;i<numfbconfigs;i++)
	{
#if FE_X_FB_DEBUG
		feLog("NativeWindow::nativeStartup FB config %d/%d\n",
				i,numfbconfigs);
#endif
		GLXFBConfig& rFBConfig=fbconfigs[i];

		XVisualInfo* pVisualInfo=(XVisualInfo *)glXGetVisualFromFBConfig(
				pDisplay,rFBConfig);
		if(!pVisualInfo)
		{
#if FE_X_FB_DEBUG
			feLog("  XVisualInfo is NULL\n");
#endif
			continue;
		}

		XRenderPictFormat* pictFormat=
				XRenderFindVisualFormat(pDisplay,pVisualInfo->visual);
		if(!pictFormat)
		{
#if FE_X_FB_DEBUG
			feLog("  XRenderPictFormat is NULL\n");
#endif
			continue;
		}

		// Check that we have an alpha mask.
		if(pictFormat->direct.alphaMask <= 0)
		{
#if FE_X_FB_DEBUG
			feLog("  no alpha mask\n");
#endif
			continue;
		}

		int sampleBuffersFound;
		int samplesFound;

		glXGetFBConfigAttrib(pDisplay,rFBConfig,
				GLX_SAMPLE_BUFFERS,&sampleBuffersFound);
		glXGetFBConfigAttrib(pDisplay,rFBConfig,
				GLX_SAMPLES,&samplesFound);

		if(bestFbconfigI<0 || (sampleBuffersFound &&
				samplesFound>mostSamplesFound && samplesFound<=samples))
		{
			bestFbconfigI=i;
			mostSamplesFound=samplesFound;
		}
	}

#if FE_X_FB_DEBUG
	feLog("NativeWindow::nativeStartup best FB is %d\n",bestFbconfigI);
#endif

	GLXFBConfig fbconfig(NULL);
	XVisualInfo* pVisualInfo(NULL);

	if(bestFbconfigI>=0)
	{
		fbconfig=fbconfigs[bestFbconfigI];

		pVisualInfo=(XVisualInfo *)glXGetVisualFromFBConfig(pDisplay,fbconfig);

		if(samples!=mostSamplesFound)
		{
			feLog("NativeWindow::nativeStartup"
					" using FB multisample %d instead of %d\n",
					mostSamplesFound,samples);
		}
	}

	if(!pVisualInfo)
	{
		feLog("NativeWindow::nativeStartup"
				" no desired FB found; fallback to simple XVisualInfo\n");

		int attribSingle[]=
		{
			GLX_RGBA,
			GLX_RED_SIZE,	1,
			GLX_GREEN_SIZE,	1,
			GLX_BLUE_SIZE,	1,
			None
		};
		int attribDouble[]=
		{
			GLX_RGBA,
			GLX_RED_SIZE,	1,
			GLX_GREEN_SIZE,	1,
			GLX_BLUE_SIZE,	1,
			GLX_DOUBLEBUFFER,
			None
		};

		pVisualInfo=glXChooseVisual(pDisplay,screenIndex,attribDouble);
#if FE_X_FB_DEBUG
		feLog("DB pVisualInfo %p\n",pVisualInfo);
#endif

		if(!pVisualInfo)
		{
			pVisualInfo=glXChooseVisual(pDisplay,screenIndex,attribSingle);
#if FE_X_FB_DEBUG
			feLog("SB pVisualInfo %p\n",pVisualInfo);
#endif
		}
	}

	if(!pVisualInfo)
	{
		XFree(fbconfigs);
		feX("NativeWindow::nativeStartup"
				" no matching GLXFBConfig or XVisualInfo");
	}

	unsigned long value_mask=0;
	XSetWindowAttributes cwa;

	cwa.border_pixel=0;
	cwa.colormap=XCreateColormap(pDisplay,parent,
			pVisualInfo->visual,AllocNone);

	value_mask=CWColormap|CWBorderPixel;

	//* create the window
	Window win=XCreateWindow(pDisplay,parent,
			m_box[0],m_box[1],			// position
			width(m_box),height(m_box),	// size
			0,							// border width
			pVisualInfo->depth,
			InputOutput,
			pVisualInfo->visual,
			value_mask,&cwa);

//	XFreeColormap(pDisplay,cwa.colormap);

	FE_WINDOW_HANDLE handle={pDisplay, pVisualInfo, win};
	setWindowHandle(handle);

	setPointerMotion(spNativeEventContext->pointerMotion());

	XSizeHints sizeHints;
	XWMHints *wmHints;
	sizeHints.x=m_box[0];
	sizeHints.y=m_box[1];
	sizeHints.width=width(m_box);
	sizeHints.height=height(m_box);
	sizeHints.flags=USSize;

	wmHints=XAllocWMHints();
	wmHints->initial_state=NormalState;
	wmHints->flags=StateHint;

	XClassHint *classHint=XAllocClassHint();
	classHint->res_class=(char*)"FreeElectron Viewer";

	XSetWMProperties(pDisplay, win, NULL, NULL,
					 NULL, 0,
					 &sizeHints,
					 wmHints,
					 classHint);
	XFree(classHint);
	XFree(wmHints);

	//* set client name
	char hostname[128];
	int fail=gethostname(hostname,128);
	FEASSERT(!fail);
	XTextProperty clientName;
	char *stringlist[1];
	stringlist[0]=hostname;
	if(!XStringListToTextProperty(stringlist,1,&clientName))
	{
		feX("Could not allocate space for client name");
	}
	XSetWMClientMachine(pDisplay,win,&clientName);
	XFree((void *)clientName.value);

	//* set pid
	pid_t pid=getpid();
	Atom atom=XInternAtom(pDisplay, "_NET_WM_PID", FALSE);
	FEASSERT(atom!=None);
	XChangeProperty(pDisplay,win,atom,XA_CARDINAL,32,
			PropModeReplace,(unsigned char *)(&pid), 1);

	//* create GLX context
	GLXContext shareList=NULL;
	Bool direct=True;
	if(bestFbconfigI>=0)
	{
		m_glxContext=glXCreateNewContext(pDisplay,fbconfig,GLX_RGBA_TYPE,
				shareList,direct);

		if(m_glxContext==NULL)
		{
			feX("NativeWindow::nativeStartup","glXCreateNewContext failed\n");
		}
	}
	else
	{
		m_glxContext=glXCreateContext(pDisplay,pVisualInfo,
				shareList,direct);
		if(m_glxContext==NULL)
		{
			feX("NativeWindow::nativeStartup","glXCreateContext failed\n");
		}
	}

	XFree(fbconfigs);

	makeCurrent();

	spNativeEventContext->notifyCreate(m_windowHandle);

	//* force window to update now
	XFlush(pDisplay);
//	XSync(pDisplay,FALSE);

	registry()->master()->catalog()->catalog<void*>("xDisplay")=pDisplay;
	registry()->master()->catalog()->catalog<void*>("glxContext")=m_glxContext;

	m_spEventContextI->threadUnlock();

	setCreated(TRUE);
	return e_ok;
}

Result NativeWindow::nativeShutdown(void)
{
	if(created())
	{
		//* NOTE glXDestroyContext sometimes gets stuck after thread lock
		//*	(actually XLockDisplay) when multiple threads have had access

//		m_spEventContextI->threadLock();
		glXDestroyContext(windowHandle()->m_pDisplay,m_glxContext);
		XDestroyWindow(windowHandle()->m_pDisplay,windowHandle()->m_window);
//		m_spEventContextI->threadUnlock();

		XFree(windowHandle()->m_pVisualInfo);
	}
	setCreated(FALSE);
	return e_ok;
}

void NativeWindow::setPointerMotion(BWORD active)
{
	long mask=FE_X_EVENTMASK | (active? PointerMotionMask: ButtonMotionMask);

	//* select which events we want to receive
	m_spEventContextI->threadLock();
	XSelectInput(windowHandle()->m_pDisplay,windowHandle()->m_window,mask);
	m_spEventContextI->threadUnlock();
}

void NativeWindow::setTitle(const String &title)
{
	//* set window title
	XTextProperty windowName;
	char *titles[1];
	titles[0]=(char *)title.c_str();
	if(!XStringListToTextProperty(titles,1,&windowName))
		feLog("Could not allocate space for window title");

	m_spEventContextI->threadLock();
	XSetWMName(windowHandle()->m_pDisplay,windowHandle()->m_window,&windowName);
	m_spEventContextI->threadUnlock();

	XFree((void *)windowName.value);
}

Result NativeWindow::open(const String title)
{
	if(mapped())
	{
		return e_alreadyAvailable;
	}

	startup();

	setTitle(title);

	//* map to screen and raise
	m_spEventContextI->threadLock();
	XMapRaised(windowHandle()->m_pDisplay,windowHandle()->m_window);
	XMoveWindow(windowHandle()->m_pDisplay,windowHandle()->m_window,
			m_box[0],m_box[1]);
	m_spEventContextI->threadUnlock();

	setMapped(TRUE);

	makeCurrent();
	return e_ok;
}

Result NativeWindow::close(void)
{
	m_spEventContextI->threadLock();
	XUnmapWindow(windowHandle()->m_pDisplay,windowHandle()->m_window);
	m_spEventContextI->threadUnlock();

	setMapped(FALSE);
	return e_ok;
}

void NativeWindow::makeCurrent(void)
{
	m_spEventContextI->threadLock();
	Bool success=glXMakeCurrent(windowHandle()->m_pDisplay,
			windowHandle()->m_window,m_glxContext);
	m_spEventContextI->threadUnlock();

	if(!success)
	{
		//* NOTE may already be current on a different thread

		feLog("NativeWindow::makeCurrent glXMakeCurrent failed\n");
		feLog("  glGetError: \"%s\"\n",gluErrorString(glGetError()));
	}
}

void NativeWindow::releaseCurrent(void)
{
	m_spEventContextI->threadLock();
	Bool success=glXMakeCurrent(windowHandle()->m_pDisplay,None,NULL);
	m_spEventContextI->threadUnlock();

	if(!success)
	{
		feLog("NativeWindow::releaseCurrent glXMakeCurrent failed\n");
		feLog(" glGetError: \"%s\"\n",gluErrorString(glGetError()));
	}
}

void NativeWindow::swapBuffers(void)
{
	if(m_glxContext)
	{
		m_spEventContextI->threadLock();
		glXSwapBuffers(windowHandle()->m_pDisplay,windowHandle()->m_window);
		m_spEventContextI->threadUnlock();
	}
}

#endif

} /* namespace ext */
} /* namespace fe */
