/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OperatorRaster_h__
#define __operator_OperatorRaster_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Evaluate some aspect of a number of surfaces

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT RasterOp:
	public OperatorSurfaceCommon,
	public Initialize<RasterOp>
{
	public:
					RasterOp(void)										{}
virtual				~RasterOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual void		handle(Record& a_rSignal);

	private:

		void		saveRaster(String a_savefile);
		void		loadRaster(String a_loadfile);
		sp<ImageI>	createImage(String a_filename);

		sp<SurfaceAccessibleI>	m_spInputAccessible;
		sp<SurfaceAccessibleI>	m_spDriverAccessible;
		sp<SurfaceAccessibleI>	m_spOutputAccessible;

		sp<DrawI>				m_spDrawI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OperatorRaster_h__ */
