/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

#if (FE_COUNTED_MT || FE_COUNTED_TRACK)
FE_DL_PUBLIC RecursiveMutex Counted::ms_counted_mutex;
#endif
#if FE_COUNTED_STORE_TRACKER
FE_DL_PUBLIC Tracker* Counted::ms_pTracker=NULL;
FE_DL_PUBLIC U32 Counted::ms_trackerCount=0;
#endif

} /* namespace */

