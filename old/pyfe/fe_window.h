/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __pyfe_fe_window_h__
#define __pyfe_fe_window_h__

class WindowFS : public FunctionSet<fe::WindowI>
{
	public:
					WindowFS(PyObject *pP) : FunctionSet<fe::WindowI>(pP) {};
		bool		open(const std::string &title);
		void		close(void);
		void		setSize(unsigned int x, unsigned int y);
		void		makeCurrent(void);
		void		clear(float r, float g, float b, float a);
		void		swapBuffers(void);
		PyObject	*getEventContextI(void);
};

class ViewerFS : public FunctionSet<fe::ViewerI>
{
	public:
					ViewerFS(PyObject *pP) : FunctionSet<fe::ViewerI>(pP) {};
		void		bind(PyObject *pyObject);
};

class EventMapFS : public FunctionSet<fe::EventMapI>
{
	public:
					EventMapFS(PyObject *pP) : FunctionSet<fe::EventMapI>(pP){};
		void		bind(PyObject *pyObjectRecord, const LayoutPtr &layoutPtr);
};

class QuickViewerFS : public FunctionSet<fe::QuickViewerI>
{
	public:
					QuickViewerFS(PyObject *pP)
						: FunctionSet<fe::QuickViewerI>(pP) {};
		void		insertDrawHandler(HandlerFS &handlerPtr);
		void		run(unsigned int frames);
};


#endif /* __pyfe_fe_window_h__ */

