/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MayaContext_h__
#define __maya_MayaContext_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief shared FE context for Maya plugins

	Note that this MayaContext is the Maya form of the FE OperatorContext
	singleton and is not related to the Maya MPxContext tool object.

	@ingroup maya
*//***************************************************************************/
class MayaContext: public OperatorContext
{
	public:
				MayaContext(void)
				{
					m_libEnvVar="FE_MAYA_LIBS";
#ifdef FE_MAYA_LIBS
					m_libDefault=FE_STRING(FE_MAYA_LIBS);
#else
					m_libDefault="fexMayaDL";
#endif
				}

virtual			~MayaContext(void)											{}

virtual	void	libraryLoaded(String a_libraryName);
};

inline void MayaContext::libraryLoaded(String a_libraryName)
{
	const String checkCommand="exists "+a_libraryName+"";

	MCommandResult melCommandResult;
	MGlobal::executeCommand(checkCommand.c_str(),melCommandResult);

	int melFound=0;
	melCommandResult.getResult(melFound);

	if(melFound)
	{
		const String initCommand=a_libraryName+"()";

		MString melResult;
		MGlobal::executeCommand(initCommand.c_str(),melResult);
	}
}

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MayaContext_h__ */
