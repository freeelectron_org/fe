/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

using namespace fe;
using namespace fe::ext;

void BindEditOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* NOTE inherits attributes from SurfaceBindOp

	catalogRemove("Threads");
	catalogRemove("AutoThread");
	catalogRemove("Paging");
	catalogRemove("StayAlive");

	catalog<String>("DriverGroup")="";
	catalog<String>("DriverGroup","label")="Driver Group";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","prompt")="LMB picks a point to bind."
			"  Ctrl-LMB rebinds the point to a driver face."
			"  Wheel adjusts drilling depth.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<String>("Driver Surface","label")="Driver Reference Surface";

	//* HACK
	set(m_selectPoint);
	set(m_selectNormal,1.0,0.0,0.0);
	set(m_bindPoint);
	set(m_bindNormal,1.0,0.0,0.0);
}

void BindEditOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

//	feLog("BindEditOp::handle brush %d\n",spDrawBrush.isValid());
	if(spDrawBrush.isValid())
	{
		const Color white(1.0,1.0,0.8,1.0);
		const Color grey(0.8,0.8,1.0,1.0);
		const Color red(0.8,0.0,0.0,1.0);
		const Color green(0.2,1.0,0.2,1.0);
		const Color blue(0.0,0.0,1.0,1.0);
		const Color cyan(0.0,0.8,1.0,1.0);
		const Color magenta(0.8,0.0,0.8,1.0);
		const Color yellow(0.8,0.8,0.0,1.0);

		const SpatialVector up(0.0,1.0,0.0);

		if(m_spInput.isValid())
		{
			m_event.bind(windowEvent(a_rSignal));

//			feLog("%s\n",c_print(m_event));
			WindowEvent::MouseButtons buttons=m_event.mouseButtons();
			const BWORD pressed=(buttons&WindowEvent::e_mbLeft);

			if(!m_event.isSystem())
			{
				m_ctrled=m_event.isModifiedBy(WindowEvent::e_keyControl);
			}

			if(m_event.isMouseWheel())
			{
				const I32 maxDepth=4;
				m_rayDepth+=(m_event.state()>0)? 1: -1;
				if(m_rayDepth<0)
				{
					m_rayDepth=0;
				}
				if(m_rayDepth>maxDepth)
				{
					m_rayDepth=maxDepth;
				}
			}

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			const Real maxDistance=100.0;

			if(!m_ctrled)
			{
				Array< sp<SurfaceI::ImpactI> > impactArray=
						m_spInput->rayImpacts(rRayOrigin,rRayDirection,
						maxDistance,m_rayDepth+1);
				sp<SurfaceTriangles::Impact> spTriImpact;
				if(impactArray.size())
				{
					const I32 max=impactArray.size()-1;
					const I32 index=(m_rayDepth<max)? m_rayDepth: max;
					spTriImpact=impactArray[index];
				}
				if(spTriImpact.isValid())
				{
					const SpatialVector point=nearestVertexPoint(spTriImpact);
					const SpatialVector normal=spTriImpact->normal();

					if(pressed && !m_ctrled)
					{
						m_selectPoint=point;
						m_selectNormal=normal;
						set(m_bindPoint);
					}

					drawPicker(spDrawBrush,point,up,normal,0.2,m_rayDepth,&red);
				}
			}
			else
			{
				Array< sp<SurfaceI::ImpactI> > impactArray=
						m_spDriver->rayImpacts(rRayOrigin,rRayDirection,
						maxDistance,m_rayDepth+1);
				sp<SurfaceTriangles::Impact> spTriImpact;
				if(impactArray.size())
				{
					const I32 max=impactArray.size()-1;
					const I32 index=(m_rayDepth<max)? m_rayDepth: max;
					spTriImpact=impactArray[index];
				}
				if(spTriImpact.isValid())
				{
					const SpatialVector point=spTriImpact->intersection();
					const SpatialVector normal=spTriImpact->normal();

					if(pressed && m_ctrled)
					{
						m_bindPoint=point;
						m_bindNormal=normal;
					}

					drawPicker(spDrawBrush,point,up,normal,
							0.2,m_rayDepth,&green);
					drawImpact(spDrawBrush,spTriImpact,0.08,&cyan);
				}

				sp<DrawableI> spDrawable=m_spDriver;
				if(spDrawable.isValid())
				{
					spDrawable->draw(spDrawBrush,&blue);
				}
			}

			if(magnitudeSquared(m_bindPoint)>1e-3)
			{
				const Real maxDistance= -1.0;
				Array< sp<SurfaceI::ImpactI> > impactArray=
						m_spDriver->nearestPoints(m_bindPoint,maxDistance,8);
				for(U32 m=0;m<impactArray.size();m++)
				{
					drawImpact(spDrawBrush,impactArray[m],0.04,
							m? &grey: &white);
				}
			}
		}

		drawPicker(spDrawBrush,m_selectPoint,up,m_selectNormal,0.1,0,&yellow);
		drawPicker(spDrawBrush,m_bindPoint,up,m_bindNormal,0.1,0,&magenta);

		return;
	}

	if(!access(m_spInput,"Input Surface")) return;

	m_spInput->setRefinement(0);

	const String driverGroup=catalog<String>("DriverGroup");
	if(!access(m_spDriver,"Driver Surface",driverGroup)) return;

	m_spDriver->setRefinement(0);

	return;
}

void BindEditOp::setupState(void)
{
	m_aIndex.populate(m_spScope,"Replace","Index");
	m_aBinding.populate(m_spScope,"Replace","Binding");
	m_aFace.populate(m_spScope,"Replace","Face");
	m_aBarycenter.populate(m_spScope,"Replace","Barycenter");

	m_spEditMap=m_spState->lookupMap(m_aIndex);
}

BWORD BindEditOp::loadState(const String& rBuffer)
{
	BWORD success=OperatorState::loadState(rBuffer);

	m_spEditMap=m_spState->lookupMap(m_aIndex);

	return success;
}
