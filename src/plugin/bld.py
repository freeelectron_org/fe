import sys
forge = sys.modules["forge"]

def setup(module):
    srclist = [ "plugin.pmh",
                "Catalog",
                "CatalogBuffer",
                "CatalogReaderBase",
                "Component",
                "Library",
                "Master",
                "Registry",
                "SingleMaster",
                "StateCatalog",
                "assertPlugin" ]

    deplibs = forge.basiclibs[:]

    lib = module.DLL("fePlugin",srclist)

    forge.deps( ["fePluginLib"], deplibs )

    module.Module('test')

    exe = module.Exe('inspect')
    forge.deps(["inspectExe"], deplibs)
    forge.deps(["inspectExe"], ["fePluginLib"])
