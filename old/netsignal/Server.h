/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_Server_h__
#define __netsignal_Server_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT ServerI:
	public Component,
	public CastableAs<ServerI>
{
	public:
virtual	void	addLayout(sp<Layout> spLayout)								= 0;
virtual	void	start(sp<SignalerI> spSignalerI,
					unsigned short a_port,
					sp<Layout> spPollLayout,
					Socket::Transport transport=Socket::e_tcp,
					String ioPriority="normal")								= 0;
virtual	void	stop(void)													= 0;

};

/** Serves signaling

	@ingroup netsignal

	see @ref network_design
	*/
class BSDServer : public ServerI
{
	public:
		BSDServer(void);
virtual	~BSDServer(void);

		/**		Add a layout.  Only signals of layouts that are added will
				be sent to the Clients. All addLayout calls must be made
				before start() is called. */
virtual	void	addLayout(sp<Layout> spLayout);
		/**		Start the server.  @em server_addr is to specify the
				listening port. @em spPollLayout specifies the layout upon
				which listen polling checks and dead connection cleanup will
				happen.  Note that signal
				transmission is not associated with this polling, but happens
				automatically in a child thread.
				*/
virtual	void	start(sp<SignalerI> spSignalerI, unsigned short a_port,
					sp<Layout> spPollLayout,
					Socket::Transport transport=Socket::e_tcp,
					String ioPriority="normal");
		/**		Stop the server.
				*/
virtual	void	stop(void);

	private:
		sp<SocketHandler>			m_spSocketHandler;
		sp<SignalerI>				m_spSignaler;
		sp<Layout>					m_spPollLayout;
		bool						m_operating;
		std::list<sp<Layout> >		m_layouts;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_Server_h__ */

