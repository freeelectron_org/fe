import sys
forge = sys.modules["forge"]

def setup(module):
    srclist = [ "core.pmh",
                "assertCore",
                "Counted",
                "List",
                "Instance",
                "InstanceMap",
                "Peeker",
                "Poison",
                "PoolAllocator",
                "Profiler",
                "stream",
                "SystemTicker",
                "Tracker",
                "Type",
                "TypeMaster" ]

    deplibs = [ "fePlatformLib",
                "feMemoryLib" ]

    lib = module.DLL("feCore",srclist)

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "feImportMemoryLib" ]

    forge.deps( ["feCoreLib"], deplibs )

    module.Module('test')
