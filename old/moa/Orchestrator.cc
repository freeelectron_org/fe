/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

Orchestrator::Orchestrator(void)
{
}

Orchestrator::~Orchestrator(void)
{
}

void Orchestrator::initialize(void)
{
	m_note_step = note_id(FE_NOTE_STEP);
	m_note_dt_change = note_id(FE_NOTE_DT_CHANGE);
	m_note_compile = note_id(FE_NOTE_COMPILE);
}

void Orchestrator::datasetInitialize(sp<RecordGroup> a_rg_dataset)
{
	rg_dataset = a_rg_dataset;

	m_r_time = m_asTime.simClock(rg_dataset);
}


bool Orchestrator::compile(void)
{
	perform(m_note_compile);
	return true;
}


void Orchestrator::perform(const t_note_id &a_note_id)
{
//fe_fprintf(stderr, "========================= Orchestrator::perform ID %d\n", a_note_id);
	if(a_note_id >= (t_note_id)m_note_array.size()) { return; }

	t_perform_array &performs = m_note_array[a_note_id];
	for(unsigned int i = 0; i < performs.size(); i++)
	{
//fe_fprintf(stderr, "Orchestrator::perform pre %d\n", i);
		performs[i](a_note_id);
//fe_fprintf(stderr, "Orchestrator::perform post %d\n", i);
	}
}

t_note_id Orchestrator::note_id(const char *a_label)
{
	std::map<fe::String, t_note_id>::iterator i_note = m_note_map.find(a_label);

	if(i_note != m_note_map.end())
	{
		return i_note->second;
	}

	t_note_id new_note_id = m_note_array.size();
	m_note_map[a_label] = new_note_id;
	m_note_array.resize(new_note_id+1);

	return new_note_id;
}

t_note_id Orchestrator::connect(t_note_perform a_perform, const char *a_label)
{
	t_note_id tid = note_id(a_label);
	m_note_array[tid].push_back(a_perform);
	return tid;
}

t_note_id Orchestrator::connect(sp<SystemI> a_system, const char *a_label)
{
	return connect(a_system.raw(), a_label);
}


t_note_id Orchestrator::connect(SystemI *a_system, const char *a_label)
{
	t_note_perform fn = std::bind(&SystemI::perform, a_system,
		std::placeholders::_1);
	return connect(fn, a_label);
}


void Orchestrator::append(sp<SystemI> a_system)
{
	if(!a_system.isValid())
	{
		feX("Orchestrator::append", "Invalid System");
		return;
	}

	m_systems.push_back(a_system);

	a_system->connectOrchestrator((sp<OrchestratorI>) this);

}


void Orchestrator::step(const t_step_mode &a_step_mode, t_moa_real a_time)
{
	if(a_step_mode == e_step_dt)
	{
		m_asTime.time(m_r_time) += m_asTime.dt(m_r_time) + a_time;
		perform(m_note_step);
	}
	else if(a_step_mode == e_step_no_more_than)
	{
		t_moa_real time_left = a_time;
		while(time_left >= m_asTime.dt(m_r_time))
		{
			m_asTime.time(m_r_time) += m_asTime.dt(m_r_time);
			perform(m_note_step);
			time_left -= m_asTime.dt(m_r_time);
		}
	}
	else if(a_step_mode == e_step_at_least)
	{
		t_moa_real time_left = a_time;
		while(time_left > 0.0)
		{
			if(m_asTime.dt(m_r_time) > time_left)
			{
				m_asTime.time(m_r_time) += time_left;
				perform(m_note_step);
				break;
			}
			m_asTime.time(m_r_time) += m_asTime.dt(m_r_time);
			perform(m_note_step);
			time_left -= m_asTime.dt(m_r_time);
		}
	}
}

void Orchestrator::setDT(t_moa_real a_dt)
{
	if(a_dt != m_asTime.dt(m_r_time))
	{
		m_asTime.dt(m_r_time) = a_dt;
		perform(m_note_dt_change);
	}
}

t_moa_real Orchestrator::getDT(void)
{
	return m_asTime.dt(m_r_time);
}

t_moa_real Orchestrator::getTime(void)
{
	return m_asTime.time(m_r_time);
}

} /* namespace ext */
} /* namespace fe */
