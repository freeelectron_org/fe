/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

#if FE_2DGL==FE_2D_X_GFX

#define FE_KEY_DEBUG	FALSE
#define FE_KEY_VERBOSE	FALSE
#define FE_KEY_LOG		FALSE

namespace fe
{
namespace ext
{

NativeKeyboard::NativeKeyboard(void):
	m_pDisplay(NULL),
	m_maxButtons(0),
	m_shift(FALSE),
	m_control(FALSE),
	m_alt(FALSE),
	m_capsLock(FALSE)
{
}

void NativeKeyboard::initialize(void)
{
#if FE_KEY_DEBUG
	feLog("NativeKeyboard::initialize\n");
#endif

	Status status=XInitThreads();
	if(!status)
	{
		feLog("NativeKeyboard::initialize XInitThreads failed\n");
	}

	if((m_pDisplay=XOpenDisplay(NULL)) == NULL)
	{
		feLog("NativeKeyboard::initialize OpenDisplay failed\n");

		feX(e_cannotCreate,"NativeKeyboard::initialize()","no X11 display");
		return;
	}

	m_rootWindow=DefaultRootWindow(m_pDisplay);

	int event, error;
	if(!XQueryExtension(m_pDisplay, "XInputExtension",
			&m_xi_opcode, &event, &error))
	{
		feLog("NativeKeyboard::initialize"
				"  Input extension not available.\n");
		m_pDisplay=NULL;

		feX(e_cannotCreate,"NativeKeyboard::initialize()","no X11 display");
		return;
	}

	int deviceid = -1;

	m_eventMask[0].deviceid = (deviceid == -1) ? XIAllDevices : deviceid;
	m_eventMask[0].mask_len = XIMaskLen(XI_LASTEVENT);
	m_eventMask[0].mask =
			(unsigned char*)calloc(m_eventMask[0].mask_len, sizeof(char));

	m_eventMask[1].deviceid = (deviceid == -1) ? XIAllMasterDevices : deviceid;
	m_eventMask[1].mask_len = XIMaskLen(XI_LASTEVENT);
	m_eventMask[1].mask =
			(unsigned char*)calloc(m_eventMask[1].mask_len, sizeof(char));

	XISetMask(m_eventMask[0].mask, XI_KeyPress);
	XISetMask(m_eventMask[0].mask, XI_KeyRelease);

	XISetMask(m_eventMask[1].mask, XI_RawKeyPress);
	XISetMask(m_eventMask[1].mask, XI_RawKeyRelease);

	XISelectEvents(m_pDisplay, m_rootWindow, &m_eventMask[0], 2);
	XSync(m_pDisplay, False);
}

NativeKeyboard::~NativeKeyboard(void)
{
	if(m_pDisplay)
	{
		XCloseDisplay(m_pDisplay);

		free(m_eventMask[1].mask);
		free(m_eventMask[0].mask);
	}
}

void NativeKeyboard::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_hpSignalerI=spSignalerI;

	sp<Scope> spScope=spLayout->scope();
	m_keyEvent.bind(spScope);
	m_keyRecord=m_keyEvent.createRecord();

	m_keyEvent.setSource(WindowEvent::e_sourceKeyboard);
}

void NativeKeyboard::handle(Record &record)
{
	m_event.bind(record);

#if FE_KEY_VERBOSE
	if(m_event.is(WindowEvent::e_sourceKeyboard,WindowEvent::e_itemAny,
			WindowEvent::e_stateAny))
	{
		feLog("NativeKeyboard::handle %s\n",c_print(m_event));
	}
#endif

	if(m_event.isPoll())
	{
#if FE_KEY_DEBUG
		feLog("NativeKeyboard::handle %s\n",c_print(m_event));
#endif

		//* WARNING possibly reentrant
		poll();
	}
}

void NativeKeyboard::poll(void)
{
	if(!m_pDisplay)
	{
		return;
	}

	XLockDisplay(m_pDisplay);

	while(XPending(m_pDisplay))
	{
		XEvent ev;
		XGenericEventCookie *cookie = (XGenericEventCookie*)&ev.xcookie;
		XNextEvent(m_pDisplay, (XEvent*)&ev);

		if(XGetEventData(m_pDisplay, cookie) &&
				cookie->type == GenericEvent &&
				cookie->extension == m_xi_opcode)
		{
#if !FE_KEY_DEBUG
			if(cookie->evtype==XI_RawKeyPress ||
					cookie->evtype==XI_RawKeyRelease)
			{
				XFreeEventData(m_pDisplay, cookie);
				continue;
			}
#endif
			XIDeviceEvent *de = (XIDeviceEvent*)cookie->data;
			KeyCode keycode = de->detail;

			int keysyms_per_keycode;
			KeySym *keysym=XGetKeyboardMapping(m_pDisplay, keycode, 1,
					&keysyms_per_keycode);
			const char* buffer = XKeysymToString(keysym[0]);
			WindowEvent::Item item=WindowEvent::Item(buffer[0]);

//			feLog("NativeKeyboard::poll \"%s\"\n",buffer);

			// e_keyLineFeed?

			//* TODO most non-alphanumeric chars give names, not ascii

			String keyName(buffer);

			XFree(keysym);

			if(keyName=="minus")
			{
				item=WindowEvent::Item('-');
			}
			else if(keyName=="equal")
			{
				item=WindowEvent::Item('=');
			}
			else if(keyName=="backslash")
			{
				item=WindowEvent::Item('\\');
			}
			else if(keyName=="grave")
			{
				item=WindowEvent::Item('`');
			}
			else if(keyName=="bracketleft")
			{
				item=WindowEvent::Item('[');
			}
			else if(keyName=="bracketright")
			{
				item=WindowEvent::Item(']');
			}
			else if(keyName=="semicolon")
			{
				item=WindowEvent::Item(';');
			}
			else if(keyName=="apostrophe")
			{
				item=WindowEvent::Item('\'');
			}
			else if(keyName=="comma")
			{
				item=WindowEvent::Item(',');
			}
			else if(keyName=="period")
			{
				item=WindowEvent::Item('.');
			}
			else if(keyName=="slash")
			{
				item=WindowEvent::Item('/');
			}
			else if(keyName=="BackSpace")
			{
				item=WindowEvent::e_keyBackspace;
			}
			else if(keyName=="Tab")
			{
				item=WindowEvent::e_keyTab;
			}
			else if(keyName=="Return")
			{
				//* NOTE NativeEventContextXWIN converts CR (\r) into LF (\n)
//				item=WindowEvent::e_keyCarriageReturn;
				item=WindowEvent::e_keyLineFeed;
			}
			else if(keyName=="space")
			{
				item=WindowEvent::e_keySpace;
			}
			else if(keyName=="Escape")
			{
				item=WindowEvent::e_keyEscape;
			}
			else if(keyName=="Delete")
			{
				item=WindowEvent::e_keyDelete;
			}
			else if(keyName=="Up")
			{
				item=WindowEvent::e_keyCursorUp;
			}
			else if(keyName=="Down")
			{
				item=WindowEvent::e_keyCursorDown;
			}
			else if(keyName=="Left")
			{
				item=WindowEvent::e_keyCursorLeft;
			}
			else if(keyName=="Right")
			{
				item=WindowEvent::e_keyCursorRight;
			}
			else if(keyName=="Shift_L" || keyName=="Shift_R")
			{
				item=WindowEvent::e_keyShift;
				m_shift=(cookie->evtype==XI_KeyPress);
				XFreeEventData(m_pDisplay, cookie);
				continue;
			}
			else if(keyName=="Alt_L" || keyName=="Alt_R")
			{
				item=WindowEvent::e_keyAlt;
				m_alt=(cookie->evtype==XI_KeyPress);
				XFreeEventData(m_pDisplay, cookie);
				continue;
			}
			else if(keyName=="Control_L" || keyName=="Control_R")
			{
				item=WindowEvent::e_keyControl;
				m_control=(cookie->evtype==XI_KeyPress);
				XFreeEventData(m_pDisplay, cookie);
				continue;
			}
			else if(keyName=="Caps_Lock")
			{
				item=WindowEvent::e_keyCapLock;
				if(cookie->evtype==XI_KeyPress)
				{
					m_capsLock=!m_capsLock;
				}
				XFreeEventData(m_pDisplay, cookie);
				continue;
			}
			else if(keyName.length()>1)
			{
				//* other special key
#if FE_KEY_VERBOSE
				feLog("NativeKeyboard::poll"
						" unconverted special key \"%s\"\n",keyName.c_str());
#endif
				XFreeEventData(m_pDisplay, cookie);
				continue;
			}

			const I32 button = item;
			if(m_shift || m_capsLock)
			{
				item=WindowEvent::Item(toupper(button));
//				feLog("toupper %d '%c' -> %d'%c'\n",
//						button,button,I32(item),I32(item));
			}
			if(m_shift)
			{
//				feLog("SHIFT\n");
				item=WindowEvent::Item(item|WindowEvent::e_keyShift);
			}
			if(m_alt)
			{
//				feLog("ALT\n");
				item=WindowEvent::Item(item|WindowEvent::e_keyAlt);
			}
			if(m_control)
			{
//				feLog("CONTROL\n");
				item=WindowEvent::Item(item|WindowEvent::e_keyControl);
			}
			if(m_capsLock)
			{
//				feLog("CAPSLOCK\n");
				item=WindowEvent::Item(item|WindowEvent::e_keyCapLock);
			}

			const I32 minSize=button+1;
			if(m_maxButtons<minSize)
			{
#if FE_KEY_DEBUG
				feLog("NativeKeyboard::poll resize %d\n",minSize);
#endif

				m_pButtonState.resize(minSize);
				for(I32 index=m_maxButtons;index<minSize;index++)
				{
					m_pButtonState[index]= -1;
				}
				m_maxButtons=minSize;
			}

#if FE_KEY_VERBOSE
			feLog("NativeKeyboard::poll type %d detail %d char %d \"%s\"\n",
					cookie->evtype,de->detail,button,keyName.c_str());
#endif

			I32 value= -1;

			switch(cookie->evtype)
			{
				case XI_KeyPress:
#if FE_KEY_DEBUG
					feLog("NativeKeyboard::poll press\n");
#endif
					value=1;
					break;
				case XI_KeyRelease:
#if FE_KEY_DEBUG
					feLog("NativeKeyboard::poll release\n");
#endif
					value=0;
					break;
#if FE_KEY_DEBUG
				case XI_RawKeyPress:
					feLog("NativeKeyboard::poll raw press\n");
					break;
				case XI_RawKeyRelease:
					feLog("NativeKeyboard::poll raw release\n");
					break;
#endif
			}

			if(value>=0)
			{
				const BWORD repeat=(m_pButtonState[button]>0 && value>0);

				m_pButtonState[button]=value;

				m_keyEvent.setItem(item);
				m_keyEvent.setState(
						repeat? WindowEvent::e_stateRepeat:
						(value? WindowEvent::e_statePress:
						WindowEvent::e_stateRelease));

				sp<SignalerI> spSignalerI(m_hpSignalerI);
				if(spSignalerI.isValid())
				{
#if FE_KEY_LOG
					feLog("NativeKeyboard::poll signal %s\n",
							c_print(m_keyEvent));
#endif
					spSignalerI->signal(m_keyRecord);
				}
			}
		}

		XFreeEventData(m_pDisplay, cookie);
	}

	XUnlockDisplay(m_pDisplay);
}

} /* namespace ext */
} /* namespace fe */

#endif
