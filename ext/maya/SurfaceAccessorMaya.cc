/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MRenderUtil.h"
#include "maya/MFloatMatrix.h"

#define FE_SAM_FORMAT_DEBUG		FALSE
#define FE_SAM_GROUP_DEBUG		FALSE
#define FE_SAM_APPEND_DEBUG		FALSE
#define FE_SAM_APPEND_VERBOSE	FALSE
#define FE_SAM_DETAIL_DEBUG		TRUE
#define FE_SAM_DETAIL_MIN_ID	1000

								//* NOTE 2013 headers appear prototypical
#define FE_MAYA_METADATA		(MAYA_API_VERSION>=201400)

#if FE_MAYA_METADATA
#include "maya/adskDataAssociations.h"
#include "maya/adskDataChannel.h"
#include "maya/adskDataStream.h"
#endif

#define FE_SAM_DETAIL_COMPONENT	MFn::kMeshVertComponent

namespace fe
{
namespace ext
{

String SurfaceAccessorMaya::type(void) const
{
	if(m_attrType.empty())
	{
		String attrType;

		if(m_attribute==SurfaceAccessibleI::e_position ||
				m_attribute==SurfaceAccessibleI::e_normal)
		{
			attrType="vector3";
		}
		else
		{
			const String format=
				const_cast<SurfaceAccessorMaya*>(this)->getBlindFormat(
				m_attrName);

//			feLog("SurfaceAccessorMaya::type format \"%s\"\n",
//					format.c_str());

			attrType=typeOfFormat(format);
		}

//		attrType="vector2";
//		attrType="vector4";

		const_cast<SurfaceAccessorMaya*>(this)->m_attrType=attrType;
	}

//	feLog("SurfaceAccessorMaya::type \"%s\"\n",m_attrType.c_str());

	return m_attrType;
}

//* static
String SurfaceAccessorMaya::typeOfFormat(String a_format)
{
	String attrType;

	if(a_format=="string")
	{
		attrType="string";
	}
	else if(a_format=="int")
	{
		attrType="integer";
	}
	else if(a_format=="bool")
	{
		attrType="boolean";
	}
	else if(a_format=="double")
	{
		attrType="real";
	}

	return attrType;
}

String SurfaceAccessorMaya::getBlindFormat(String a_attrName)
{
	BWORD found=FALSE;

	const I32 blindDataID=getBlindDataID(a_attrName,"",found);
	if(!found)
	{
#if FE_SAM_BLIND_DEBUG
		feLog("SurfaceAccessorMaya::getBlindFormat \"%s\" not found id %d\n",
				a_attrName.c_str(),blindDataID);
#endif

		//* no such blind data in any surface
		return "";
	}

	if(!m_pFnMesh->hasBlindData(componentType(),blindDataID))
	{
#if FE_SAM_BLIND_DEBUG
		feLog("SurfaceAccessorMaya::getBlindFormat \"%s\" not present id %d\n",
				a_attrName.c_str(),blindDataID);
#endif

		//* no such blind data on this surface
		return "";
	}

	MStringArray longNames;
	MStringArray shortNames;
	MStringArray formatNames;
//	MStatus status=
			m_pFnMesh->getBlindDataAttrNames(blindDataID,
			longNames,shortNames,formatNames);

//	feLog("SurfaceAccessorMaya::getBlindFormat len %d\n",formatNames.length());

	const String format=formatNames[0].asChar();

#if FE_SAM_BLIND_DEBUG
		feLog("SurfaceAccessorMaya::getBlindFormat"
				" \"%s\" id %d format \"%s\"\n",
				a_attrName.c_str(),blindDataID,format.c_str());
#endif

	return format;
}

I32 SurfaceAccessorMaya::getBlindDataID(String a_attrName,String a_attrType,
	BWORD& a_rFound)
{
	a_rFound=FALSE;

	const String attrKey=a_attrName+":"+a_attrType;
	I32 blindDataID=m_spMaster->catalog()->catalogOrDefault<I32>(
			"MayaBlindID",attrKey,-1);

#if FE_SAM_BLIND_DEBUG
	feLog("SurfaceAccessorMaya::getBlindDataID \"%s\" %d\n",
			attrKey.c_str(),blindDataID);
#endif

	if(blindDataID>=0)
	{
		//* created already
		a_rFound=TRUE;
		return blindDataID;
	}

#if FE_SAM_BLIND_DEBUG
	feLog("SurfaceAccessorMaya::getBlindDataID looking for \"%s\"\n",
			attrKey.c_str());
#endif

	const String searchName=a_attrName+(a_attrType=="vector3"? "_x": "");

	blindDataID=0x0;
	while(m_pFnMesh->isBlindDataTypeUsed(blindDataID))
	{
		//* search for already existing match

		MStringArray longNames;
		MStringArray shortNames;
		MStringArray formatNames;
//		MStatus status=
				m_pFnMesh->getBlindDataAttrNames(blindDataID,
				longNames,shortNames,formatNames);

#if FE_SAM_BLIND_DEBUG
		feLog("looking for \"%s\" vs blind %d long \"%s\""
				" short \"%s\" format \"%s\"\n",
				searchName.c_str(),
				blindDataID,longNames[0].asChar(),
				shortNames[0].asChar(),formatNames[0].asChar());
#endif

		if(searchName==longNames[0].asChar())
		{
#if FE_SAM_BLIND_DEBUG
			feLog("found %d\n",blindDataID);
#endif
			a_rFound=TRUE;
			return blindDataID;
		}

		blindDataID++;
	}

#if FE_SAM_BLIND_DEBUG
	feLog("new %d\n",blindDataID);
#endif

	return blindDataID;
}

I32 SurfaceAccessorMaya::createBlindDataID(String a_attrName,String a_attrType)
{
	BWORD found=FALSE;

	const String attrKey=a_attrName+":"+a_attrType;
	I32& rBlindDataID=m_spMaster->catalog()->catalog<I32>(
			"MayaBlindID",attrKey,-1);

	rBlindDataID=getBlindDataID(a_attrName,a_attrType,found);

#if FE_SAM_BLIND_DEBUG
	feLog("SurfaceAccessorMaya::createBlindDataID"
			" found %d blindDataID %d\n",found,rBlindDataID);
#endif

	if(found)
	{
		return rBlindDataID;
	}

	MStringArray longNames;
	MStringArray shortNames;
	MStringArray formatNames;

	// format names: int float double boolean string binary

	if(a_attrType=="vector3")
	{
		String name=a_attrName+"_x";
		longNames.append(name.c_str());
		shortNames.append(name.c_str());
		formatNames.append("double");

		name=a_attrName+"_y";
		longNames.append(name.c_str());
		shortNames.append(name.c_str());
		formatNames.append("double");

		name=a_attrName+"_z";
		longNames.append(name.c_str());
		shortNames.append(name.c_str());
		formatNames.append("double");
	}
	else
	{
		longNames.append(a_attrName.c_str());
		shortNames.append(a_attrName.c_str());

		if(a_attrType=="real")
		{
			formatNames.append("double");
		}
		else if(a_attrType=="integer")
		{
			formatNames.append("int");
		}
		else if(a_attrType=="boolean")
		{
			formatNames.append("bool");
		}
		else if(a_attrType=="string")
		{
			formatNames.append("string");
		}
	}

	MStatus status=m_pFnMesh->createBlindDataType(
			rBlindDataID,longNames,shortNames,formatNames);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::createBlindDataID type \"%s\" name \"%s\""
				" createBlindDataType %p failed\n",
				a_attrType.c_str(),a_attrName.c_str(),rBlindDataID);
	}

	return rBlindDataID;
}

SurfaceAccessorMaya::PolyFormat SurfaceAccessorMaya::guessPolyFormat(void) const
{
	MStatus status;

	status=m_pFnMesh->syncObject();
	if(!status)
	{
		feLog("SurfaceAccessorMaya::guessPolyFormat sync failed\n");

		return e_unknown;
	}

#if MAYA_API_VERSION>=201400
	MString errors;
	status=m_pFnMesh->validateMetadata(errors);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::guessPolyFormat"
				" mesh failed validation:\n  \"%s\"\n",
				errors.asChar());

		return e_unknown;
	}
#endif

	const I32 primitiveCount=m_pFnMesh->numPolygons(&status);

	if(!status || primitiveCount<1)
	{
#if FE_SAM_FORMAT_DEBUG
		feLog("SurfaceAccessorMaya::guessPolyFormat no polys -> e_unknown\n");
#endif

		return e_unknown;
	}

#if FE_SAM_FORMAT_DEBUG
	feLog("SurfaceAccessorMaya::guessPolyFormat primitiveCount %d\n",
			primitiveCount);
#endif

	const I32 pointCount=m_pFnMesh->numVertices(&status);

	if(!status || pointCount<1)
	{
#if FE_SAM_FORMAT_DEBUG
		feLog("SurfaceAccessorMaya::guessPolyFormat no points -> e_unknown\n");
#endif

		return e_unknown;
	}

#if FE_SAM_FORMAT_DEBUG
	feLog("SurfaceAccessorMaya::guessPolyFormat pointCount %d\n",
			pointCount);
#endif

	//* NOTE only checking first primitive
	const I32 primitiveIndex=0;

	const I32 vertexCount=
			m_pFnMesh->polygonVertexCount(primitiveIndex,&status);

	if(!status)
	{
#if FE_SAM_FORMAT_DEBUG
		feLog("SurfaceAccessorMaya::guessPolyFormat"
				" unknown count -> e_unknown\n");
#endif

		return e_unknown;
	}

#if FE_SAM_FORMAT_DEBUG
	feLog("SurfaceAccessorMaya::guessPolyFormat vertexCount %d\n",
			vertexCount);
#endif

	if(vertexCount<4 || vertexCount>100 || vertexCount%2)
	{
#if FE_SAM_FORMAT_DEBUG
		feLog("SurfaceAccessorMaya::guessPolyFormat"
				" small, huge, or odd count -> e_closedPoly\n");
#endif

		return e_closedPoly;
	}

	MIntArray vertexList;
	status=m_pFnMesh->getPolygonVertices(primitiveIndex,vertexList);

	MPoint point;
	MPoint twin;

	const I32 halfCount=vertexCount>>1;
	for(I32 vertexIndex=0;vertexIndex<halfCount;vertexIndex++)
	{
		status=m_pFnMesh->getPoint(vertexList[vertexIndex],point);
		status=m_pFnMesh->getPoint(vertexList[vertexCount-1-vertexIndex],twin);

		const SpatialVector p0(point[0],point[1],point[2]);
		const SpatialVector p1(twin[0],twin[1],twin[2]);
		if(magnitudeSquared(p1-p0)>1e-6)
		{
#if FE_SAM_FORMAT_DEBUG
			feLog("SurfaceAccessorMaya::guessPolyFormat"
					" deviant twin -> e_closedPoly\n");
#endif

			return e_closedPoly;
		}
	}

#if FE_SAM_FORMAT_DEBUG
	feLog("SurfaceAccessorMaya::guessPolyFormat twin match -> e_openPoly\n");
#endif

	return e_openPoly;
}

void SurfaceAccessorMaya::cacheShaders(void)
{
	const U32 elementCount=count();
	m_pStringCache=new String[elementCount];
	for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
	{
		m_pStringCache[elementIndex]=".";
	}

	//* NOTE getConnectedShaders only works on actual mesh nodes, not mesh data
	MFnMesh mFnMesh(m_meshNode);

//	const U32 parentCount=
			mFnMesh.parentCount();

//	feLog("SurfaceAccessorMaya::cacheShaders"
//			" parentCount %d elementCount %d\n",
//			parentCount,elementCount);

	//* TODO other elements
	if(m_element==SurfaceAccessibleI::e_primitive)
	{
		const unsigned int instanceNumber=0;
		MObjectArray shaderArray;
		MIntArray shaderOfPolygon;

		MStatus status=mFnMesh.getConnectedShaders(instanceNumber,
				shaderArray,shaderOfPolygon);
		const U32 shaderCount=shaderArray.length();
		const U32 polyCount=shaderOfPolygon.length();

//		feLog("  shaderCount %d polyCount %d\n",shaderCount,polyCount);

		if(status && shaderCount)
		{
			String* shaderNameArray=new String[shaderCount];

			for(U32 polyIndex=0;polyIndex<polyCount;polyIndex++)
			{
				const U32 shaderIndex=shaderOfPolygon[polyIndex];
				String& rShaderName=shaderNameArray[shaderIndex];
				if(rShaderName=="")
				{
					const MObject shaderObject=shaderArray[shaderIndex];
					const MFnDependencyNode node(shaderObject);
					rShaderName=node.name().asChar();
				}

				m_pStringCache[polyIndex]=rShaderName;

//				feLog("  poly %d/%d shader %d/%d \"%s\"\n",
//						polyIndex,polyCount,shaderIndex,shaderCount,
//						rShaderName.c_str());
			}

			delete[] shaderNameArray;
		}
	}
}

void SurfaceAccessorMaya::cacheTexture(String a_textureName)
{
	MFloatArray uCoords;
	MFloatArray vCoords;

	const U32 pointCount=count();
	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector uv=spatialVector(pointIndex);

		uCoords.append(uv[0]);
		vCoords.append(uv[1]);
	}

	//* NOTE camera not really needed for UV sampling
	MDagPath cameraPath;
	M3dView::active3dView().getCamera(cameraPath);
	MMatrix mat=cameraPath.inclusiveMatrix();
	MFloatMatrix cameraMatrix(mat.matrix);

	MFloatVectorArray resultColors;
	MFloatVectorArray resultTransparencies;

	MStatus status=MRenderUtil::sampleShadingNetwork(
			MString(a_textureName.c_str()),
			pointCount,false,false,cameraMatrix,NULL,
			&uCoords,&vCoords,
			NULL,NULL,NULL,NULL,NULL,
			resultColors,resultTransparencies);

	m_pSpatialVectorCache=new SpatialVector[pointCount];
	m_pRealCache=new Real[pointCount];

	if(!status || resultColors.length()<pointCount)
	{
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			m_pSpatialVectorCache[pointIndex]=SpatialVector(0.0,0.0,0.0);
			m_pRealCache[pointIndex]=0.0;
		}

		return;
	}

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const MVector& vector=resultColors[pointIndex];
		m_pSpatialVectorCache[pointIndex]=SpatialVector(
				vector[0],vector[1],vector[2]);

		m_pRealCache[pointIndex]=vector[0];

//		feLog("point %d/%d uv %.6G %.6G color %s"
//				" trans %.6G %.6G %.6G\n",
//				pointIndex,pointCount,
//				uCoords[pointIndex],
//				vCoords[pointIndex],
//				c_print(m_pSpatialVectorCache[pointIndex]),
//				resultTransparencies[pointIndex][0],
//				resultTransparencies[pointIndex][1],
//				resultTransparencies[pointIndex][2]);
	}
}

void SurfaceAccessorMaya::updateGroupList(void)
{
#if FE_SAM_GROUP_DEBUG
	feLog("SurfaceAccessorMaya::updateGroupList \"%s\"\n",m_attrName.c_str());
#endif

	m_groupList.clear();
	if(m_attrName.empty())
	{
		return;
	}
	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup)
	{
		return;
	}

	if(m_attrName==".")
	{
		if(!m_pGroupIt)
		{
			feLog("SurfaceAccessorMaya::updateGroupList NULL group iterator\n");
			return;
		}

		int iteration=0;
		for(m_pGroupIt->reset();!m_pGroupIt->isDone();m_pGroupIt->next())
		{
			const int groupIndex=m_pGroupIt->index();
//			feLog("groupIndex %d %d\n",iteration,groupIndex);

			m_groupList.push_back(groupIndex);

			iteration++;
		}
		return;
	}

	SurfaceAccessibleI::Element storeElement=m_element;

	//* temporary switch to use common methods
	if(m_element==SurfaceAccessibleI::e_pointGroup)
	{
		m_element=SurfaceAccessibleI::e_point;
	}
	else
	{
		m_element=SurfaceAccessibleI::e_primitive;
	}

	//* TODO scan all points/primitives
	//* if group attribute is non-zero, add to group list

	delete[] m_pIntegerCache;
	m_pIntegerCache=NULL;

	const U32 elements=count();
	for(U32 index=0;index<elements;index++)
	{
#if FE_SAM_GROUP_DEBUG
		feLog("SurfaceAccessorMaya::updateGroupList %d/%d int %d\n",
				index,elements,integer(index));
#endif

		if(integer(index)>0)
		{
			m_groupList.push_back(index);
		}
	}

	//* restore original
	m_element=storeElement;

	delete[] m_pIntegerCache;
	m_pIntegerCache=NULL;

#if FE_SAM_GROUP_DEBUG
	feLog("SurfaceAccessorMaya::updateGroupList count %d\n",count());
#endif

	if(!count())
	{
		//* Maya sets
		const unsigned int instanceNumber=0;
		const bool renderableSetsOnly=false;
		MObjectArray sets;
		MObjectArray comps;

		MFnMesh mFnMesh(m_meshNode);
		MStatus status=mFnMesh.getConnectedSetsAndMembers(
				instanceNumber,sets,comps,renderableSetsOnly);

#if FE_SAM_GROUP_DEBUG
		if(!status)
		{
			feLog("SurfaceAccessorMaya::updateGroupList"
					" failed to get sets because \"%s\"\n",
					status.errorString().asChar());
		}
#endif

		const U32 setCount=sets.length();

#if FE_SAM_GROUP_DEBUG
		const U32 compCount=comps.length();
		feLog("SurfaceAccessorMaya::updateGroupList setCount %d compCount %d\n",
				setCount,compCount);
#endif

		for(U32 setIndex=0;setIndex<setCount;setIndex++)
		{
			MObject set=sets[setIndex];
			MObject comp=comps[setIndex];

//			feLog("SurfaceAccessorMaya::updateGroupList %d/%d set %d comp %d\n",
//					setIndex,setCount,!set.isNull(),!comp.isNull());

			if(set.isNull())
			{
				continue;
			}

			MFnSet fnSet(set,&status);
			if(!status)
			{
				feLog("SurfaceAccessorMaya::updateGroupList"
						" failed to create set %d/%d because \"%s\"\n",
						setIndex,setCount,status.errorString().asChar());
				continue;
			}

			const String setName=fnSet.name().asChar();

//			feLog("  name \"%s\"\n",setName.c_str());

			if(setName!=m_attrName)
			{
				continue;
			}

			if(comp.isNull())
			{
				continue;
			}

			MDagPath dagPath;
			status=mFnMesh.getPath(dagPath);

			if(m_element==SurfaceAccessibleI::e_primitiveGroup)
			{
				MItMeshPolygon it(dagPath,comp,&status);
				if(!status)
				{
					feLog("SurfaceAccessorMaya::updateGroupList"
							" failed to create poly iterator because \"%s\"\n",
							status.errorString().asChar());
					continue;
				}

#if FE_SAM_GROUP_DEBUG
				feLog("  primitive group count %d elements:",it.count());
#endif

				for(;!it.isDone();it.next())
				{
					const U32 primitiveIndex=it.index();

#if FE_SAM_GROUP_DEBUG
					feLog("  %d",primitiveIndex);
#endif

					m_groupList.push_back(primitiveIndex);
				}

#if FE_SAM_GROUP_DEBUG
				feLog("\n");
#endif
			}
			else
			{
				MItMeshVertex it(dagPath,comp,&status);
				if(!status)
				{
					feLog("SurfaceAccessorMaya::updateGroupList"
							" failed to create vert iterator because \"%s\"\n",
							status.errorString().asChar());
					continue;
				}

#if FE_SAM_GROUP_DEBUG
				feLog("  point group count %d elements:",it.count());
#endif

				for(;!it.isDone();it.next())
				{
					const U32 pointIndex=it.index();

#if FE_SAM_GROUP_DEBUG
					feLog(" %d",pointIndex);
#endif

					m_groupList.push_back(pointIndex);
				}

#if FE_SAM_GROUP_DEBUG
				feLog("\n");
#endif
			}

			break;
		}
	}
}

//* static
String SurfaceAccessorMaya::readDetailAt(MDataHandle a_meshData,I32 a_id)
{
	MFnGeometryData geoData(a_meshData.asMesh());
	MStatus status;

	MObject singles=geoData.objectGroupComponent(a_id,&status);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::readDetailAt"
				" failed to get components at %d because\n  \"%s\"\n",
				a_id,status.errorString().asChar());
		return "";
	}

	MFnSingleIndexedComponent singleComp(singles);
	MIntArray elements;
	status=singleComp.getElements(elements);

	const int len=elements.length();

#if TRUE
	char* buffer=new char[len];
#else
	//* NOTE add extra char and zero out in case something breaks
	char* buffer=new char[len+1];
	memset(buffer,0,len+1);
#endif

//	feLog("read %d len %d\n",a_id,len);

	for(U32 m=0;m<len;m++)
	{
		const U32 verifyIndex=(elements[m]>>8);
		if(verifyIndex!=m)
		{
			feLog("verify error %d %d:%d\n",a_id,m,verifyIndex);
			return "";
		}

		buffer[m]=(elements[m]&0xFF);

//		feLog("read %d %d:%d %p %d,%d '%c'\n",a_id,m,verifyIndex,elements[m],
//				elements[m]&(~0xFF),buffer[m],buffer[m]? buffer[m]: ' ');
	}

	String result=buffer;
	delete[] buffer;

	return result;
}

//* static
void SurfaceAccessorMaya::writeDetailAt(MDataHandle a_meshData,I32 a_id,
	const String& a_rString)
{
	MObject meshObject=a_meshData.asMesh();
	MFnGeometryData geoData(meshObject);
	MStatus status;

	const char* buffer=a_rString.c_str();
	const I32 len=a_rString.length();

	if(!geoData.hasObjectGroup(a_id))
	{
		status=geoData.addObjectGroup(a_id);
		if(!status)
		{
			feLog("SurfaceAccessorMaya::writeDetailAt"
					"failed to add at %d because\n  \"%s\"\n",
					a_id,status.errorString().asChar());
		}
	}

	MFnSingleIndexedComponent singleComp;
	MObject singles=singleComp.create(FE_SAM_DETAIL_COMPONENT,&status);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::writeDetailAt"
				"failed to create components at %d because\n  \"%s\"\n",
				a_id,status.errorString().asChar());
	}

	for(I32 m=0;m<len;m++)
	{
		const I32 code=(m<<8) + buffer[m];
		singleComp.addElement(code);

//		MIntArray checkElements;
//		status=singleComp.getElements(checkElements);
//
//		feLog("write %d %d/%d %p '%c' check %p\n",
//				a_id,m,checkElements.length(),code,buffer[m],checkElements[m]);
	}
	singleComp.addElement(len<<8);

	status=geoData.setObjectGroupComponent(a_id,singles);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::writeDetailAt"
				"failed to add components because\n  \"%s\"\n",
				status.errorString().asChar());
	}

	//* NOTE needed to propagate changes
	a_meshData.set(meshObject);
}

//* static
void SurfaceAccessorMaya::removeDetailAt(MDataHandle a_meshData,I32 a_id)
{
#if FE_SAM_DETAIL_DEBUG
	feLog("SurfaceAccessorMaya::removeDetailAt %d\n",a_id);
	SurfaceAccessorMaya::dumpDetail(a_meshData);
#endif

	MObject meshObject=a_meshData.asMesh();
	MFnGeometryData geoData(meshObject);
	MStatus status;

	if(geoData.hasObjectGroup(a_id))
	{
#if FE_SAM_DETAIL_DEBUG
		feLog("  has %d\n",a_id);
#endif
		status=geoData.removeObjectGroup(a_id);
		if(!status)
		{
			feLog("SurfaceAccessorMaya::removeDetailAt"
					"failed to remove at %d because\n  \"%s\"\n",
					a_id,status.errorString().asChar());
		}
	}

#if FE_SAM_DETAIL_DEBUG
	SurfaceAccessorMaya::dumpDetail(a_meshData);
#endif

	//* NOTE needed to propagate changes
	a_meshData.set(meshObject);
}

//* static
I32 SurfaceAccessorMaya::findDetailKey(MDataHandle a_meshData,
	const String& a_rKey,BWORD a_createMissing)
{
	MFnGeometryData geoData(a_meshData.asMesh());
	MStatus status;

	const int groupCount=geoData.objectGroupCount(&status);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::findDetailKey"
				" failed to get group count because\n  \"%s\"\n",
				status.errorString().asChar());
	}
	I32 maxGroup= -1;
	for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
	{
		const int groupId=geoData.objectGroup(groupIndex,&status);
		if(groupId<FE_SAM_DETAIL_MIN_ID)
		{
#if FE_SAM_DETAIL_DEBUG
			feLog("SurfaceAccessorMaya::findDetailKey skip %d too low\n",
					groupId);
#endif
			continue;
		}

		MFn::Type mayaType=geoData.objectGroupType(groupId,&status);
		if(!status || mayaType!=FE_SAM_DETAIL_COMPONENT)
		{
#if FE_SAM_DETAIL_DEBUG
			feLog("SurfaceAccessorMaya::findDetailKey skip %d bad type\n",
					groupId);
#endif
			continue;
		}

		if(!(groupIndex%1) && readDetailAt(a_meshData,groupId)==a_rKey)
		{
#if FE_SAM_DETAIL_DEBUG
			feLog("SurfaceAccessorMaya::findDetailKey found %d \"%s\"\n",
					groupId,a_rKey.c_str());
#endif
			return groupId;
		}
		if(maxGroup<groupId)
		{
			maxGroup=groupId;
		}
	}
	maxGroup++;

	if(!a_createMissing)
	{
		return -1;
	}

	if(maxGroup<FE_SAM_DETAIL_MIN_ID)
	{
		maxGroup=FE_SAM_DETAIL_MIN_ID;
	}

#if FE_SAM_DETAIL_DEBUG
	feLog("SurfaceAccessorMaya::findDetailKey create %d \"%s\"\n",
			maxGroup,a_rKey.c_str());
#endif

	writeDetailAt(a_meshData,maxGroup,a_rKey);
	return maxGroup;
}

//* static
void SurfaceAccessorMaya::dumpDetail(MDataHandle a_meshData)
{
	MFnGeometryData geoData(a_meshData.asMesh());
	MStatus status;

	const int groupCount=geoData.objectGroupCount(&status);
	if(!status)
	{
		feLog("SurfaceAccessorMaya::dumpDetail"
				" failed to get group count because\n  \"%s\"\n",
				status.errorString().asChar());
	}
	for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
	{
		const int groupId=geoData.objectGroup(groupIndex,&status);
		if(!status)
		{
			feLog("%d \"%s\"",groupIndex,status.errorString().asChar());
		}
		else if(!geoData.hasObjectGroup(groupId))
		{
			feLog("%d %d MISSING\n",groupIndex,groupId);
		}
		else
		{
			MFn::Type mayaType=geoData.objectGroupType(groupId,&status);
			if(!status)
			{
				feLog("%d %d NO TYPE\n",groupIndex,groupId);
			}
			else if(mayaType!=FE_SAM_DETAIL_COMPONENT)
			{
				feLog("%d %d WRONG TYPE\n",groupIndex,groupId);
			}
			else
			{
				feLog("%d %d \"%s\"\n",groupIndex,groupId,
						readDetailAt(a_meshData,groupId).c_str())
			}
		}
	}
}

void SurfaceAccessorMaya::setDetail(const String& a_rKey,
	const String& a_rValue)
{
#if FE_MAYA_METADATA
	FEASSERT(m_pFnMesh);

	MStatus status;

	adsk::Data::Associations metadata(m_pFnMesh->metadata());

	//* TODO only copy once, on first write per mesh (see also removeDetailFor)
	metadata.makeUnique();
	m_pFnMesh->setMetadata(metadata);

	adsk::Data::Channel channel=metadata.channel(a_rKey.c_str());

	adsk::Data::Stream* pOldStream=channel.dataStream("detail");
	if(pOldStream)
	{
//		BWORD removed=
				channel.removeDataStream("detail");
	}

	adsk::Data::Structure* pStructure=
			adsk::Data::Structure::structureByName("oneString");
	if(!pStructure)
	{
		pStructure=adsk::Data::Structure::create();
		pStructure->setName("oneString");
		pStructure->addMember(adsk::Data::Member::kString,1,"string");
	}

	adsk::Data::Stream stream(*pStructure,"detail");

	channel.setDataStream(stream);

	adsk::Data::Handle handle(*pStructure);

	const std::string value=a_rValue.c_str();
	std::string errors;
	if(handle.fromStr(value,0,errors))
	{
		feLog("SurfaceAccessorMaya::setDetail fromStr \"%s\"\n",
				errors.c_str());
	}

	stream.setElement(0,handle);


	return;
#endif

	const I32 keyId=findDetailKey(m_meshData,a_rKey,TRUE);
	writeDetailAt(m_meshData,keyId+1,a_rValue);

#if FE_SAM_DETAIL_DEBUG
	feLog("SurfaceAccessorMaya::setDetail %d \"%s\" to \"%s\"\n",
			keyId,a_rKey.c_str(),a_rValue.c_str());
	dumpDetail(m_meshData);
#endif
}

String SurfaceAccessorMaya::getDetail(const String& a_rKey)
{
#if FE_MAYA_METADATA
	FEASSERT(m_pFnMesh);

	MStatus status;

	adsk::Data::Associations metadata(m_pFnMesh->metadata());
	adsk::Data::Channel channel=metadata.channel(a_rKey.c_str());

	adsk::Data::Stream* pOldStream=channel.dataStream("detail");
	if(!pOldStream || !pOldStream->hasElement(0))
	{
		return "";
	}

	adsk::Data::Handle handle=pOldStream->element(0);

	return handle.str(0).c_str();
#endif

	const I32 keyId=findDetailKey(m_meshData,a_rKey,FALSE);
	if(keyId<0)
	{
		return "";
	}

	String text=readDetailAt(m_meshData,keyId+1);

#if FE_SAM_DETAIL_DEBUG
	feLog("SurfaceAccessorMaya::getDetail %d \"%s\" as \"%s\"\n",
			keyId,a_rKey.c_str(),text.c_str());
#endif

	return text;
}

//* static
BWORD SurfaceAccessorMaya::removeDetailFor(MDataHandle a_meshData,
	String a_name)
{
#if FE_MAYA_METADATA
	//* TEMP
//	return FALSE;

	MFnMesh* pFnMesh=new MFnMesh(a_meshData.asMesh());
	FEASSERT(pFnMesh);

	MStatus status;

	adsk::Data::Associations metadata(pFnMesh->metadata());

	metadata.makeUnique();
	pFnMesh->setMetadata(metadata);

	return metadata.removeChannel(a_name.c_str());
#endif

	const I32 keyId=findDetailKey(a_meshData,a_name,FALSE);
	if(keyId<0)
	{
		return FALSE;
	}

	//* value, then label
	removeDetailAt(a_meshData,keyId+1);
	removeDetailAt(a_meshData,keyId);

	return TRUE;
}

void SurfaceAccessorMaya::initializeFaceVertArray(void)
{
	const U32 pointCount=m_pFnMesh->numVertices();
	m_faceVertTable.resize(pointCount);

#if FALSE
	MIntArray normalIdCounts;
	MIntArray normalIds;
	m_pFnMesh->getNormalIds(normalIdCounts,normalIds);

	const U32 normalCount=m_pFnMesh->numNormals();

	feLog("SurfaceAccessorMaya::initializeFaceVertArray"
			" pointCount %d normalCount %d\n",
			pointCount,normalCount);

	U32 normalIdIndex=0;

	const U32 faceCount=normalIdCounts.length();
	for(U32 faceIndex=0;faceIndex<faceCount;faceIndex++)
	{
		const U32 vertCount=normalIdCounts[faceIndex];
		for(U32 vertIndex=0;vertIndex<vertCount;vertIndex++)
		{
			const U32 normalIndex=normalIds[normalIdIndex++];
			FEASSERT(normalIndex<pointCount);

			Array<FaceVert>& rFaceVertArray=m_faceVertTable[normalIndex];
			rFaceVertArray.push_back(FaceVert(faceIndex,vertIndex));
		}
	}
#else
	const U32 primitiveCount=m_pFnMesh->numPolygons();
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		MIntArray vertexList;
		MStatus status=m_pFnMesh->getPolygonVertices(
				primitiveIndex,vertexList);

		if(!status)
		{
			feLog("SurfaceAccessorMaya::initializeFaceVertArray failed \"%s\"\n",
					status.errorString().asChar());
		}

		const U32 vertCount=m_pFnMesh->polygonVertexCount(primitiveIndex);

//		feLog("SurfaceAccessorMaya::initializeFaceVertArray"
//				" prim %d/%d sub %d vs %d\n",
//				primitiveIndex,primitiveCount,vertCount,vertexList.length());

		FEASSERT(vertCount==vertexList.length());
		for(U32 subIndex=0;subIndex<vertCount;subIndex++)
		{
			const U32 raw=vertexList[subIndex];
			const U32 pointIndex=logicalVertex(raw);

			Array<FaceVert>& rFaceVertArray=m_faceVertTable[pointIndex];
			rFaceVertArray.push_back(FaceVert(primitiveIndex,subIndex));
		}
	}
#endif
}

//* TODO don't clobber existing data (should append, not replace)
void SurfaceAccessorMaya::append(Array< Array<I32> >& a_rPrimVerts)
{
	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		return;
	}

	const I32 addPrimitiveCount=a_rPrimVerts.size();

#if FE_SAM_APPEND_DEBUG
	feLog("SurfaceAccessorMaya::append addPrimitiveCount %d\n",
			addPrimitiveCount);
#endif

	MStatus status;

	I32 oldPointCount=m_pFnMesh->numVertices(&status);

#if FE_SAM_APPEND_DEBUG
	feLog("SurfaceAccessorMaya::append starting point count %d\n",
			oldPointCount);
#endif

#if FALSE
	FEASSERT(oldPointCount==0);

	//* one polygon at a time
	//* WARNING ignores vertex index data
	for(I32 polyIndex=0;polyIndex<addPrimitiveCount;polyIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[polyIndex];

		const I32 vertCount=rVertList.size();

		append(vertCount,SurfaceAccessibleI::e_closed);

		for(I32 subIndex=0;subIndex<vertCount;subIndex++)
		{
			FEASSERT(rVertList[subIndex]==oldPointCount);
			oldPointCount++;
		}
	}

	return;
#endif

	//* NOTE first read back all existing data, then append new data

	const I32 oldPrimitiveCount=count();
	const I32 newPrimitiveCount=oldPrimitiveCount+addPrimitiveCount;

	int newPointCount=0;
	MIntArray polygonConnects;

	MIntArray polygonCounts;
	polygonCounts.setLength(newPrimitiveCount);

	MFloatPointArray vertexArray;
	vertexArray.setLength(oldPointCount);

	for(I32 primitiveIndex=0;primitiveIndex<oldPrimitiveCount;primitiveIndex++)
	{
		const I32 vertCount=subCount(primitiveIndex);
		polygonCounts[primitiveIndex]=vertCount;

		const I32 connects=polygonConnects.length();
		polygonConnects.setLength(connects+vertCount);

		for(I32 subIndex=0;subIndex<vertCount;subIndex++)
		{
			const I32 pointIndex=integer(primitiveIndex,subIndex);
			if(newPointCount<pointIndex+1)
			{
				newPointCount=pointIndex+1;
			}

			polygonConnects[connects+subIndex]=pointIndex;

			const SpatialVector point=spatialVector(primitiveIndex,subIndex);
			vertexArray.set(pointIndex,point[0],point[1],point[2]);

#if FE_SAM_APPEND_VERBOSE
			feLog("old polygonConnects[%d]=%d\n",connects+subIndex,pointIndex);
#endif
		}
	}

	FEASSERT(newPointCount==oldPointCount);

	for(I32 polyIndex=0;polyIndex<addPrimitiveCount;polyIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[polyIndex];

		const I32 vertCount=rVertList.size();
		polygonCounts[oldPrimitiveCount+polyIndex]=vertCount;

		const I32 connects=polygonConnects.length();
		polygonConnects.setLength(connects+vertCount);

		for(I32 subIndex=0;subIndex<vertCount;subIndex++)
		{
			const I32 pointIndex=rVertList[subIndex];
			if(newPointCount<pointIndex+1)
			{
				newPointCount=pointIndex+1;
			}

			polygonConnects[connects+subIndex]=pointIndex;

#if FE_SAM_APPEND_VERBOSE
			feLog("add polygonConnects[%d]=%d\n",
					connects+subIndex,pointIndex);
#endif
		}
	}

#if FE_SAM_APPEND_VERBOSE
	for(I32 polyIndex=0;polyIndex<newPrimitiveCount;polyIndex++)
	{
		feLog("polygonCounts[%d]=%d\n",polyIndex,polygonCounts[polyIndex]);
	}
#endif

	vertexArray.setLength(newPointCount);

	for(I32 pointIndex=oldPointCount;pointIndex<newPointCount;pointIndex++)
	{
		vertexArray.set(pointIndex,
				0.0,0.1*pointIndex,0.01*(pointIndex*pointIndex));
	}

#if FE_SAM_APPEND_DEBUG
	feLog("SurfaceAccessorMaya::append create total points %d primitives %d\n",
			newPointCount,newPrimitiveCount);
#endif

	m_pFnMesh->setCheckSamePointTwice(false);
#if FALSE
	status=m_pFnMesh->createInPlace(newPointCount,newPrimitiveCount,
			vertexArray,polygonCounts,polygonConnects);
#else
	MFnMeshData meshDataFn;
	MObject newMeshData=meshDataFn.create();

//	MFnMesh newMeshFn;
	MObject mesh=m_pFnMesh->create(newPointCount,newPrimitiveCount,
			vertexArray,polygonCounts,polygonConnects,newMeshData,&status);
	m_meshData.set(newMeshData);
	m_pFnMesh=new MFnMesh(m_meshData.asMesh());

	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=m_spSurfaceAccessibleI;
	spSurfaceAccessibleMaya->setMeshData(m_meshData);
#endif

	if(!status)
	{
		feLog("SurfaceAccessorMaya::append createInPlace failed\n");
	}

	if(vertexCount()!=newPointCount)
	{
		feLog("SurfaceAccessorMaya::append created %d of %d points\n",
				vertexCount(),newPointCount);
	}
	if(m_pFnMesh->numPolygons()!=newPrimitiveCount)
	{
		feLog("SurfaceAccessorMaya::append created %d of %d polygons\n",
				m_pFnMesh->numPolygons(),newPrimitiveCount);
	}
}

} /* namespace ext */
} /* namespace fe */
