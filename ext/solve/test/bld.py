import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    module.summary = []

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexSolveDLLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexThreadDLLib" ]

    tests = [   'xSparseArray',
                'xSparseMatrix',
                'xSpringSlide' ]

    forge.tests += [
        ("xSparseArray.exe",    "",                         None,       None),
        ("xSparseMatrix.exe",   "",                         None,       None),
        ("xSpringSlide.exe",    "100",                      None,       None) ]

    if 'viewer' in forge.modules_confirmed:
        deplibs += [ "fexViewerDLLib" ]

        tests += [  'xWiggle' ]

        forge.tests += [
            ("xWiggle.exe",         "10",                   None,       None) ]
    else:
        module.summary += [ "-viewer" ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)
