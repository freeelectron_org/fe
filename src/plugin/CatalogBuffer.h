/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_CatalogBuffer_h__
#define __core_CatalogBuffer_h__

namespace fe
{

/**************************************************************************//**
	@brief Access fe::Array<> properties without invoking std::vector<>

	@ingroup plugin

	Potentially useful where stdlib headers are inconsistant.

	Buffers are only for short term reading and writing.
	Other accesses can easily change or delete the contents.
*//***************************************************************************/
class FE_DL_EXPORT CatalogBuffer
{
	public:

static	void	resize(sp<Catalog> a_spCatalog,String a_name,String a_property,
						I32 a_size);

static	I32		access(sp<Catalog> a_spCatalog,String a_name,String a_property,
						void*& a_rpBuffer);
};

} /* namespace */

#endif /* __core_CatalogBuffer_h__ */
