/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Type_h__
#define __core_Type_h__

//* NOTE already stored for FE_COUNTED_STORE_TRACKER
#define FE_TYPE_STORE_NAME	(FE_CODEGEN<=FE_DEBUG && !FE_COUNTED_STORE_TRACKER)

namespace fe
{

/**	@brief C++ type_info wrapper

	@ingroup core
	*/
class FE_DL_EXPORT TypeInfo
{
	public:
		class		not_initialized {};

					TypeInfo(void);
					TypeInfo(const fe_type_info &other);
					TypeInfo(const TypeInfo &other);

					template<typename X>
		void		assign(void)	{ operator=(getTypeId<X>()); }

		const		fe_type_info &ref(void) const;

		TypeInfo	&operator=(const TypeInfo &other);
		bool		operator==(const TypeInfo &other) const;
		bool		operator!=(const TypeInfo &other) const;
		bool		operator<(const TypeInfo &other) const;
		bool		operator>(const TypeInfo &other) const;
		bool		operator>=(const TypeInfo &other) const;
		bool		operator<=(const TypeInfo &other) const;

		bool		isValid(void) const;
	private:
		const fe_type_info	*m_pTypeInfo;
};

class FE_DL_EXPORT BaseTypeVector:
	public Counted,
	public CastableAs<BaseTypeVector>
{
	public:
		BaseTypeVector(void)
		{
			m_base = NULL;
			m_size = 0;
		}
virtual	~BaseTypeVector(void)
		{
		}

//virtual	void	*vectorRoot(void) = 0;
//virtual	size_t	itemSize(void) = 0;
virtual	void	resize(unsigned int a_size) {}

		// raw_at cannot be a virtual due to speed issues
		void	*raw_at(unsigned int a_index)
		{
			return (void *)((char *)m_base + m_size*a_index);
		}

#if FE_COUNTED_STORE_TRACKER
const	String	verboseName(void) const
				{	return "BaseTypeVector "+name(); }
#endif

		unsigned int size(void) const { return m_size; }

	protected:
		void			*m_base;
		unsigned int	m_size;
};

/**	@brief A class to associate functionality with run time types.

	@ingroup core

	The primary purpose of this class is to associate functionaility such
	as serialization, construction, and destruction with library/application
	managed instances of C++ types.

	This is done by creating a BaseType, typically by using the Type template
	class through TypeMaster.  The necessary functionality is associated by
	creating a BaseType::Info object and attaching it to the BaseType.

	@code
	class Info32 : public BaseType::Info
	{
	virtual	void	output(std::ostream &ostrm, void *instance, t_serialMode mode)
			{
				U32 u32;
				u32 = htonl(*(U32 *)instance);
				ostrm.write((char *)&u32, sizeof(U32));
			}
	virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
			{
				U32 u32;
				istrm.read((char *)&u32, sizeof(U32));
				*(U32 *)instance = ntohl(u32);
			}
	virtual	IWORD	iosize(void) { return sizeof(U32); }
	};

	sp<BaseType> spT;
	spT = typeMaster->assertType<U32>("unsigned_integer");
	spT->setInfo(new Info32());

	@endcode

	*/
class FE_DL_EXPORT BaseType : public Counted, public ClassSafe<GlobalHolder>
{
	public:
		class FE_DL_PUBLIC FE_CORE_PORT Info:
			virtual public Counted,
			public CastableAs<Info>
		{
			public:
						typedef enum
						{
							e_binary,
							e_ascii
						} t_serialMode;
						Info(void)
						{
#if FE_COUNTED_STORE_TRACKER
							setName("BaseType::Info");
#endif
#if FE_CODEGEN<=FE_DEBUG && FE_OS==FE_LINUX && FE_HEAP_CHECKABLE
							U8 current_stack;
							if((void*)this>(void*)&current_stack)
								feX("fe::BaseType::Info::Info",
										"Info object not on stack");
#endif
							suppressReport();
						}
		virtual			~Info(void)											{}
		virtual	bool	getConstruct(void)				{ return false; }
		virtual	void	construct(void *instance)							{}
		virtual	void	destruct(void *instance)							{}
		virtual	BWORD	copy(void *instance,const void* source)
						{
							if(iosize()==c_implicit)
								return FALSE;
							memcpy(instance,source,iosize());
							return TRUE;
						}
		virtual	IWORD	output(	std::ostream &ostrm, void *instance,
								t_serialMode mode)							= 0;
		virtual	void	input(	std::istream &istrm, void *instance,
								t_serialMode mode)							= 0;
		virtual	String	print(void *instance)			{ return String(); }
		virtual	IWORD	iosize(void)					{ return c_implicit; }
		virtual	FE_UWORD	alignment(void)					{ return 0; }

		/** implicit variable size. avoid. mainly for internal use. */
		static FE_DL_PUBLIC	const	IWORD	c_implicit;
		/** explicit variable size. network byte order 32 bit unsigned
			integer should follow */
		static FE_DL_PUBLIC	const	IWORD	c_explicit;
		static FE_DL_PUBLIC	const	IWORD	c_noascii;
		static FE_DL_PUBLIC	const	IWORD	c_ascii;

#if FE_COUNTED_STORE_TRACKER
	const	String		verboseName(void) const
						{	return "BaseType::Info "+name(); }
#endif
		};
	public:
						BaseType(void);
virtual					~BaseType(void);
virtual	FE_UWORD		size(void) const;
virtual	void			assign(void *lvalue, void *rvalue)					=0;
virtual	bool			equiv(void *a_a, void *a_b)							=0;
virtual	void			setInfo(const sp<Info> &spInfo);
virtual	void			setInfo(Info *pInfo);
virtual	sp<Info>		getInfo(void);

virtual	bool			getConstruct(void) const;
virtual	void			construct(void *instance);
virtual	void			destruct(void *instance);
virtual	BWORD			copy(void *instance,const void* source);
virtual	TypeInfo		&typeinfo(void);
virtual	sp<BaseTypeVector>	createVector(void)								=0;


#if FE_COUNTED_STORE_TRACKER || FE_TYPE_STORE_NAME
const	String			verboseName(void) const	{ return "Type<"+name()+">"; }
#endif

#if FE_TYPE_STORE_NAME
virtual
const	String&			name(void) const			{ return m_storedName; }
		void			setName(const String& a_name)
						{	m_storedName=a_name; }
	private:
		String			m_storedName;
#endif

	protected:
		FE_UWORD		m_size;
		sp<Info>		m_spInfo;
		TypeInfo		m_typeInfo;
};


template <class T>
class FE_DL_EXPORT TypeInfoInitialized : public BaseType::Info
{
	public:
				TypeInfoInitialized(void)
				{
#if FE_COUNTED_STORE_TRACKER
					setName(FE_TYPESTRING(T));
#endif
				}
virtual			~TypeInfoInitialized(void)		{ }
virtual	bool	getConstruct(void)				{ return true; }
virtual	void	construct(void *instance)		{ *((T *)instance)=T(0); }
virtual	IWORD	output(	std::ostream &ostrm, void *instance,
						t_serialMode mode)
				{
					if(mode == e_binary) { return 0; }
					else { return c_noascii; }
				}
virtual	void	input(	std::istream &istrm, void *instance,
						t_serialMode mode)		{ }
virtual	IWORD	iosize(void)					{ return sizeof(T); }
};

template <class T>
class FE_DL_EXPORT TypeInfoConstructable : public BaseType::Info
{
	public:
				TypeInfoConstructable(void)
				{
#if FE_COUNTED_STORE_TRACKER
					setName(FE_TYPESTRING(T));
#endif
				}
virtual			~TypeInfoConstructable(void)	{ }
virtual	bool	getConstruct(void)				{ return true; }
virtual	void	construct(void *instance)		{ new(instance)T; }
virtual	void	destruct(void *instance)		{ ((T *)instance)->~T(); }
virtual	IWORD	output(	std::ostream &ostrm, void *instance,
						t_serialMode mode)
				{
					if(mode == e_binary) { return 0; }
					else { return c_noascii; }
				}
virtual	void	input(	std::istream &istrm, void *instance,
						t_serialMode mode) { }
};

class FE_DL_EXPORT BaseTypeInfoArray:
	virtual public Counted,
	public CastableAs<BaseTypeInfoArray>
{
	public:
				BaseTypeInfoArray(void)										{}
virtual			~BaseTypeInfoArray(void)									{}

				//* changes buffer size
virtual	void	resize(I32 a_size, void *instance)							=0;

				//* set buffer pointer and returns size
virtual	I32		access(void*& a_rpBuffer, void *instance)					=0;
};

//* NOTE T is Array<typename> or interchangeable std::vector<typename>
template <class T>
class FE_DL_EXPORT TypeInfoArray:
	public TypeInfoConstructable<T>,
	public BaseTypeInfoArray
{
	public:
				TypeInfoArray(void)											{}
virtual			~TypeInfoArray(void)										{}

virtual	void	resize(I32 a_size, void *instance)
				{
					if(!instance) return;
					T &v = *(T *)instance;
					v.resize(a_size);
				}

virtual	I32		access(void*& a_rpBuffer, void *instance)
				{
					if(!instance) return -1;
					T &v = *(T *)instance;
					a_rpBuffer=v.data();
					return v.size();
				}
};

template <class T>
class FE_DL_EXPORT TypeInfoMemcpyIO : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	IWORD	iosize(void);
};

#if 0
template <class T>
class FE_DL_EXPORT TypeVector : public BaseTypeVector
{
	public:
		TypeVector(void)
		{
			m_base = NULL;
			m_vector = NULL;
			m_size = sizeof(T);
		}
virtual	~TypeVector(void)
		{
			if(m_vector)
			{
				deallocate(m_vector);
			}
		}

virtual void    resize(unsigned int a_size)
		{
			if(m_vector && a_size == 0)
			{
				deallocate(m_vector);
				m_vector = NULL;
				m_base = NULL;
				return;
			}
			if(!m_vector)
			{
				m_vector = (T *)allocate(a_size*m_size);
			}
			else
			{
				m_vector = (T *)reallocate((void *)m_vector, a_size*m_size);
			}
			m_base = m_vector;
		}

virtual void    *raw_at(unsigned int a_index)
		{
			return (void *)&m_vector[a_index];
		}

		T &at(unsigned int a_index)
		{
			return m_vector[a_index];
		}

	private:
		T				*m_vector;
};
#endif


#if 1
template <class T>
class FE_DL_EXPORT TypeVector:
	public BaseTypeVector,
	public CastableAs< TypeVector<T> >
{
	public:
		TypeVector(void)
		{
			m_base = NULL;
			m_size = sizeof(T);
			//TODO: 1000 seems grossly arbitrary
			m_vector.reserve(1000);
		}
virtual	~TypeVector(void)
		{
#if FE_COUNTED_STORE_TRACKER
			if(m_base)
			{
				deregisterRegion(m_base);
			}
#endif
		}

virtual void    resize(unsigned int a_size)
		{
#if FE_COUNTED_STORE_TRACKER
			if(m_base)
			{
				deregisterRegion(m_base);
			}
#endif

			m_vector.resize(a_size);
			if(a_size > 0)
			{
				m_base = (void *)&(m_vector[0]);
			}

#if FE_COUNTED_STORE_TRACKER
			registerRegion(m_base,a_size*m_size);
#endif
		}

virtual void    *raw_at(unsigned int a_index)
		{
			return (void *)&m_vector[a_index];
		}

		T &at(unsigned int a_index)
		{
			return m_vector[a_index];
		}

#if FE_COUNTED_STORE_TRACKER
const	String	verboseName(void) const		{ return "TypeVector "+name(); }
#endif

	private:
		Array<T>	m_vector;
};


// work around stupid STL decision to specialize std::vector<bool>
template <>
class FE_DL_EXPORT TypeVector<bool> : public BaseTypeVector
{
	public:
		TypeVector(void)
		{
			m_base = NULL;
			m_vector = NULL;
			m_size = sizeof(bool);
		}
virtual	~TypeVector(void)
		{
			if(m_vector)
			{
				deallocate(m_vector);
			}
		}

virtual void    resize(unsigned int a_size)
		{
			if(m_vector && a_size == 0)
			{
				deallocate(m_vector);
				m_vector = NULL;
				m_base = NULL;
				return;
			}
			if(!m_vector)
			{
				m_vector = (bool *)allocate(a_size*m_size);
			}
			else
			{
				m_vector = (bool *)reallocate((void *)m_vector, a_size*m_size);
			}
			m_base = m_vector;
		}

virtual void    *raw_at(unsigned int a_index)
		{
			return (void *)&m_vector[a_index];
		}

		bool &at(unsigned int a_index)
		{
			return m_vector[a_index];
		}

	private:
		bool				*m_vector;
};
#endif

template <typename T>
class FE_DL_EXPORT TypeVector< std::atomic<T> > :
	public BaseTypeVector,
	public CastableAs< TypeVector< std::atomic<T> > >
{


	public:
		TypeVector(void)
		{
			m_base = NULL;
			m_size = sizeof(std::atomic<T>);
			m_count_alloc = 0;
			m_count_used = 0;
			m_vector = NULL;
		}
virtual	~TypeVector(void)
		{
			if(m_vector) { delete [] m_vector; }
		}

virtual void resize(unsigned int a_size)
		{
			if(a_size == 0)
			{
				if(m_vector)
				{
					delete [] m_vector;
					m_vector = NULL;
					m_base = NULL;
					m_count_alloc = 0;
				}
			}
			else if(a_size > m_count_used)
			{
				if(a_size > m_count_alloc)
				{
					unsigned int new_alloc = 1;
					if(m_count_alloc > 0) { new_alloc = m_count_alloc * 2; }
					if(m_count_alloc < a_size) { new_alloc = a_size; }
					std::atomic<T> *from = m_vector;
					m_vector = new std::atomic<T>[new_alloc];
					m_base = m_vector;
					for(unsigned int i = 0; i < m_count_used; i++)
					{
						T intermediary = from[i];
						m_vector[i] = intermediary;
					}
					delete [] from;
					m_count_alloc = new_alloc;
				}
				for(unsigned int i = m_count_used; i < a_size; i++)
				{
					m_vector[i] = T();
				}
			}
			m_count_used = a_size;
			FEASSERT(m_count_used<=m_count_alloc);
		}

virtual void *raw_at(unsigned int a_index)
		{
			return (void *)&m_vector[a_index];
		}

		std::atomic<T> &at(unsigned int a_index)
		{
			FEASSERT(a_index<m_count_alloc);
			FEASSERT(a_index<m_count_used);
			return m_vector[a_index];
		}

	private:
		std::atomic<T>	*m_vector;
		unsigned int m_count_alloc;
		unsigned int m_count_used;
};

/**	@brief A class to associate functionality with run time types.

	@ingroup core

	See BaseType for details.
	*/
template <class T>
class FE_DL_EXPORT Type : public BaseType
{
	public:
						Type(void);
virtual					~Type(void);
virtual		void		assign(void *lvalue, void *rvalue);
virtual		bool		equiv(void *a_a, void *a_b);
static		Type<T>*	create(void);
virtual		sp<BaseTypeVector>	createVector(void);
};

template <typename T>
class FE_DL_EXPORT Type< std::atomic<T> > : public BaseType
{
	public:
						Type(void);
virtual					~Type(void);
virtual		void		assign(void *lvalue, void *rvalue);
virtual		bool		equiv(void *a_a, void *a_b);
static		Type< std::atomic<T> >*	create(void);
virtual		sp<BaseTypeVector>	createVector(void);
};

template <class T>
inline sp<BaseTypeVector> Type<T>::createVector(void)
{
	sp< BaseTypeVector > spBTV(new TypeVector<T>);
	return spBTV;
}

template <>
inline sp<BaseTypeVector> Type<void>::createVector(void)
{
	sp< BaseTypeVector > spBTV(new BaseTypeVector());
	return spBTV;
}


template <class T>
inline Type<T>::Type(void)
{
	m_size=sizeof(T);
	m_typeInfo.assign<T>();
#if FE_COUNTED_STORE_TRACKER || FE_TYPE_STORE_NAME
	setName(m_typeInfo.ref().name());
#endif
}

template <typename T>
inline Type< std::atomic<T> >::Type(void)
{
	m_size=sizeof(std::atomic<T>);
	m_typeInfo.assign< std::atomic<T> >();
#if FE_COUNTED_STORE_TRACKER || FE_TYPE_STORE_NAME
	setName(m_typeInfo.ref().name());
#endif
}

template <class T>
inline Type<T>::~Type(void)
{
}

template <typename T>
inline Type< std::atomic<T> >::~Type(void)
{
}

template <typename T>
inline void Type< std::atomic<T> >::assign(void *lvalue, void *rvalue)
{
	T value =
		*(reinterpret_cast<std::atomic<T> *>(rvalue));
	*(reinterpret_cast<std::atomic<T> *>(lvalue)) = value;
}

template <typename T>
inline sp<BaseTypeVector> Type< std::atomic<T> >::createVector(void)
{
	sp< BaseTypeVector > spBTV(new TypeVector< std::atomic<T> >);
	return spBTV;
}

template <typename T>
inline bool Type< std::atomic<T> >::equiv(void *a_a, void *a_b)
{
	return (*(reinterpret_cast<std::atomic<T> *>(a_a)) == *(reinterpret_cast<std::atomic<T> *>(a_b)));
}

template <class T>
inline void Type<T>::assign(void *lvalue, void *rvalue)
{	*(reinterpret_cast<T *>(lvalue)) = *(reinterpret_cast<T *>(rvalue)); }

template <class T>
inline bool Type<T>::equiv(void *a_a, void *a_b)
{
	return (*(reinterpret_cast<T *>(a_a)) == *(reinterpret_cast<T *>(a_b)));
}

template <class T>
inline Type<T>* Type<T>::create(void)
{
	Type<T>* pType=new Type<T>;
	pType->registerRegion(pType,sizeof(Type<T>));
//	feLog("Type< %s >::create %p %p %p %s\n",
//			FE_TYPESTRING(T).c_str(),
//			&create,pType,pType->typeinfo().ref().name(),
//			pType->typeinfo().ref().name());
	return pType;
}

template <typename T>
inline Type< std::atomic<T> >* Type< std::atomic<T> >::create(void)
{
	Type< std::atomic<T> >* pType=new Type< std::atomic<T> >;
	pType->registerRegion(pType,sizeof(Type< std::atomic<T> >));
	return pType;
}

template<>
inline void Type<void>::assign(void *lvalue, void *rvalue)
{ }

template<>
inline bool Type<void>::equiv(void *a_a, void *a_b)
{
	return true;
}

template<>
inline Type<void>::Type(void)
{
	m_size=0;
	m_typeInfo.assign<void>();
}

inline FE_UWORD BaseType::size(void) const
{
	SAFEGUARDCLASS;
	return m_size;
}

template <class T>
inline IWORD TypeInfoMemcpyIO<T>::output(std::ostream &ostrm, void *instance,
	t_serialMode mode)
{
	if(mode == e_binary)
	{
		ostrm.write((char *)instance, sizeof(T));
		return sizeof(T);
	}
	return c_noascii;
}

template <class T>
inline void TypeInfoMemcpyIO<T>::input(std::istream &ostrm, void *instance,
	t_serialMode mode)
{
	ostrm.read((char *)instance, sizeof(T));
}

template <class T>
inline IWORD TypeInfoMemcpyIO<T>::iosize(void)
{
	return sizeof(T);
}

inline TypeInfo::TypeInfo(void)
{
	m_pTypeInfo = &getTypeId<not_initialized>();
	FEASSERT(m_pTypeInfo);
}

inline TypeInfo::TypeInfo(const fe_type_info &other)
{
	m_pTypeInfo = &other;
	FEASSERT(m_pTypeInfo);
}

inline TypeInfo::TypeInfo(const TypeInfo &other)
{
	m_pTypeInfo = other.m_pTypeInfo;
	FEASSERT(m_pTypeInfo);
}

inline TypeInfo &TypeInfo::operator=(const TypeInfo &other)
{
	if(this != &other)
	{
		m_pTypeInfo = other.m_pTypeInfo;
	}
	FEASSERT(m_pTypeInfo);
	return *this;
}

inline const fe_type_info &TypeInfo::ref(void) const
{
	FEASSERT(m_pTypeInfo);
	return *m_pTypeInfo;
}

inline bool TypeInfo::isValid(void) const
{
	return (bool)(ref() != getTypeId<not_initialized>());
}

#if defined(GCC_HASCLASSVISIBILITY) || FE_RTTI_HOMEBREW == 1
inline bool TypeInfo::operator==(const TypeInfo &other) const
{
	const char* n1=ref().name();
	const char* n2=other.ref().name();
	return n1==n2 || !strcmp(n1,n2);
}

inline bool TypeInfo::operator!=(const TypeInfo &other) const
{
	return !(*this==other);
}

inline bool TypeInfo::operator<(const TypeInfo &other) const
{
	const char* n1=ref().name();
	const char* n2=other.ref().name();
	return strcmp(n1,n2)<0;
}

inline bool TypeInfo::operator>(const TypeInfo &other) const
{
	const char* n1=ref().name();
	const char* n2=other.ref().name();
	return strcmp(n1,n2)>0;
}

inline bool TypeInfo::operator>=(const TypeInfo &other) const
{
	return !(*this<other);
}

inline bool TypeInfo::operator<=(const TypeInfo &other) const
{
	return !(*this>other);
}
#else
inline bool TypeInfo::operator==(const TypeInfo &other) const
{
	return ref() == other.ref();
}

inline bool TypeInfo::operator!=(const TypeInfo &other) const
{
	return ref() != other.ref();
}

inline bool TypeInfo::operator<(const TypeInfo &other) const
{
	return ref().before(other.ref());
}

inline bool TypeInfo::operator>(const TypeInfo &other) const
{
	return other.ref().before(ref());
}

inline bool TypeInfo::operator>=(const TypeInfo &other) const
{
	return !(ref().before(other.ref()));
}

inline bool TypeInfo::operator<=(const TypeInfo &other) const
{
	return !(other.ref().before(ref()));
}
#endif

struct eq_type_info
{
	bool operator()(const TypeInfo &t1, const TypeInfo &t2) const
	{
		return t1 == t2;
	}
};

struct hash_type_info
{
	FE_UWORD operator()(const TypeInfo &t) const
	{
		FE_UWORD hash = 0;
		FE_UWORD len = strlen(t.ref().name());
		for(FE_UWORD i = 0; i < len; i++)
			{ hash = (hash << 3) + t.ref().name()[i]; }
		return hash;
	}
};

template<typename U>
class AutoHashMap< TypeInfo, U >:
	public HashMap< TypeInfo, U, hash_type_info, eq_type_info >				{};

} /* namespace */

#endif /* __core_Type_h__ */
