/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <freeelectron.pmh>

namespace fe
{
namespace ext
{

SurfaceAccessibleGodotMesh::SurfaceAccessibleGodotMesh(void)
{
}

SurfaceAccessibleGodotMesh::~SurfaceAccessibleGodotMesh(void)
{
	reset();
}

Ref<ArrayMesh> SurfaceAccessibleGodotMesh::mesh(void)
{
	if(!m_refMesh.is_valid())
	{
		m_refMesh.instance();
	}
	return m_refMesh;
}

void SurfaceAccessibleGodotMesh::reset(void)
{
	SurfaceAccessibleBase::reset();

	if(m_refMesh.is_valid())
	{
		m_refMesh.instance();
	}
}

void SurfaceAccessibleGodotMesh::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
	feLog("SurfaceAccessibleGodotMesh::attributeSpecs element %d\n",a_element);

	a_rSpecs.clear();

	//* NOTE a_node ignored

	if(!m_refMesh.is_valid())
	{
		return;
	}

	const String elementName=elementLayout(a_element);

	//* TODO

	SurfaceAccessibleI::Spec spec;
	spec.set("P","vector3");
	a_rSpecs.push_back(spec);
}

sp<SurfaceAccessorI> SurfaceAccessibleGodotMesh::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	String a_name,SurfaceAccessibleI::Creation a_create)
{
	//* TODO
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleGodotMesh::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute,
	SurfaceAccessibleI::Creation a_create)
{
	//* TODO
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleGodotMesh::createAccessor(void)
{
	//* TODO
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceI> SurfaceAccessibleGodotMesh::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
	sp<SurfaceI> spSurface=spSurface=registry()->create(
			"SurfaceI.*.*.SurfaceTrianglesGodot");

	if(spSurface.isValid())
	{
		spSurface->setRestrictions(a_restrictions);
	}

	return spSurface;
}

} /* namespace ext */
} /* namespace fe */
