/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_h__
#define __datatool_h__

#include "signal/signal.h"
#include "math/math.h"

#ifdef MODULE_datatool
#define FE_DATATOOL_PORT FE_DL_EXPORT
#else
#define FE_DATATOOL_PORT FE_DL_IMPORT
#endif

#include "datatool/RecordView.h"
#include "datatool/RecordArrayView.h"
#include "datatool/Operator.h"
#include "datatool/StringAccessor.h"

#include "datatool/View.h"
#include "datatool/RecorderI.h"
#include "datatool/ConfigI.h"
#include "datatool/Config.h"
#include "datatool/RecordableI.h"
#include "datatool/Recordable.h"

#include "datatool/datatoolAS.h"

namespace fe
{
namespace ext
{

Library* CreateDatatoolLibrary(sp<Master> spMaster);

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_h__ */
