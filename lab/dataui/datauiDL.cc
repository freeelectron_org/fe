/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "platform/dlCore.cc"

#include "DrawButtons.h"
#include "DrawAttributeLabels.h"
#include "DrawStringEntry.h"
#include "ButtonController.h"
#include "StringEntryController.h"
#include "AttributeController.h"
#include "DataIOController.h"
#include "ExistController.h"
#include "ScopeDebug.h"
#include "RecordGroupDebug.h"
#include "CatalogDebug.h"
#include "MaskMapDebug.h"
#include "Manipulator.h"
#include "RootLocate.h"
#include "PaneLocate.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexDataToolDL"));
	list.append(new String("fexViewerDL"));
	list.append(new String("fexArchitectureDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());
	assertData(spMaster->typeMaster());

	//sp<Scope> spWinScope=spMaster->catalogComponent("Scope","WinScope");
	//sp<Layout> spEventLayout = spWinScope->declare("WindowEvent");
	//spEventLayout->populate(FE_USE("win:data"), "record");

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<DrawButtons>("HandlerI.DrawButtons.dataui.fe");
	pLibrary->add<DrawAttributeLabels>(
			"HandlerI.DrawAttributeLabels.dataui.fe");
	pLibrary->add<DrawStringEntry>("HandlerI.DrawStringEntry.dataui.fe");
	pLibrary->add<ButtonController>("HandlerI.ButtonController.dataui.fe");
	pLibrary->add<StringEntryController>(
			"HandlerI.StringEntryController.dataui.fe");
	pLibrary->add<AttributeController>(
			"HandlerI.AttributeController.dataui.fe");
	pLibrary->add<DataIOController>("HandlerI.DataIOController.dataui.fe");
	pLibrary->add<ScopeDebug>("HandlerI.ScopeDebug.dataui.fe");
	pLibrary->add<RecordGroupDebug>("HandlerI.RecordGroupDebug.dataui.fe");
	pLibrary->add<CatalogDebug>("HandlerI.CatalogDebug.dataui.fe");
	pLibrary->add<MaskMapDebug>("HandlerI.MaskMapDebug.dataui.fe");
	pLibrary->add<Manipulator>("HandlerI.Manipulator.dataui.fe");
	pLibrary->add<RootLocate>("HandlerI.RootLocate.dataui.fe");
	pLibrary->add<PaneLocate>("HandlerI.PaneLocate.dataui.fe");
	pLibrary->add<ExistController>("HandlerI.ExistController.dataui.fe");
	pLibrary->add<AsManipulator>("PopulateI.AsManipulator.dataui.fe");
	pLibrary->add<AsManipulatable>("PopulateI.AsManipulatable.dataui.fe");
	pLibrary->add<AsStringEntry>("PopulateI.AsStringEntry.dataui.fe");
	pLibrary->add<AsRectButton>("PopulateI.AsRectButton.dataui.fe");
	pLibrary->add<AsValueButton>("PopulateI.AsValueButton.dataui.fe");
	pLibrary->add<AsPane>("PopulateI.AsPane.dataui.fe");

	pLibrary->add<DebugWindow>("HandlerI.DebugWindow.dataui.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
