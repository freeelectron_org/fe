import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):
    srcList = [ "SurfaceAccessibleXml",
                "pugixml",
                "xml.pmh",
                "xmlDL" ]

    dll = module.DLL( "fexXmlDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexXmlDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexXmlDL",             None,       None) ]

    module.Module('test')
