import sys
import os
forge = sys.modules["forge"]

def setup(module):
    deplibs = [
        'fexSignalLib' ] + forge.corelibs

    tests = [   'xGriddb'
                ]

    for t in tests:
        exe = module.Exe(t)

        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xGriddb.exe", "",                         None,   None) ]

