if __name__ == '__main__':
    import forge
else:
    import os
    import ctypes
    import shlex
    import platform

    class Context(object):
        '''
        Initial point of contact for FE operations,
        allowing you to load surfaces, access the Master, and create operators.

        Generally, you only want to use one Context, like a singleton.
        '''

        def __init__(self, name = "Context"):
#           print("pyfe Context.init " + name)
            self.objName = name
            self.verbose = False
            self.nameCount = {}

            if platform.system() == 'Darwin':
                terminalDLPath = "libfexTerminalDL.dylib"
            elif platform.system() == 'Windows':
                terminalDLPath = "fexTerminalDL.dll"
            else:
                terminalDLPath = "libfexTerminalDL.so"
            terminalDL = None

            try:
                terminalDL = ctypes.CDLL(terminalDLPath)
            except:
                pass

            if not terminalDL:
                scriptDir = os.path.dirname(os.path.realpath(__file__))
                if platform.system() == 'Windows':
                    os.add_dll_directory(scriptDir)
                else:
                    terminalDLPath = os.path.abspath(os.path.join(scriptDir, terminalDLPath))
                terminalDL = ctypes.CDLL(terminalDLPath)

            if terminalDL:
                self.fe_terminal_execute = terminalDL.fe_terminal_execute

        def __del__(self):
#           print("pyfe Context.del " + self.objName)
            pass

        def setVerbose(self, verbose):
            '''
            Log the internal commands and responses, for debugging purposes.
            '''

            self.verbose = verbose

        def execute(self, command):
            '''
            Send a formatted command to C++ via ctypes.

            This in generally done internally via these pyfe objects,
            so the syntax is not formally described and is subject to change.
            '''

            size = 1024
            result = ctypes.create_string_buffer(size)

            if self.verbose:
                if platform.system() == 'Windows':
                    print("\npyfe    " + command)
                else:
                    print("\n[36mpyfe    " + command + "[0m")

            self.fe_terminal_execute(size,result,command.encode('utf-8'))

            response = result.value.decode('utf-8')

            if response[:6] == "error:":
                if platform.system() == 'Windows':
                    print("pyfe    " + response)
                else:
                    print("[36mpyfe[31m    " + response + "[0m")
            elif self.verbose:
                if platform.system() == 'Windows':
                    print("pyfe    response: " + response)
                else:
                    print("[36mpyfe[34m    response: " + response + "[0m")

            return response

        def name(self):
            '''
            Return the name provided to the constructor.

            Currently, this name has no other use.
            '''

            return self.objName

        def setFrame(self, frame):
            '''
            Set the current frame in time.
            '''

            command = "set frame " + str(frame)
            self.execute(command)

        def manage(self, library):
            '''
            Manually load a C++ module library.

            Generally, the autoload system is supposed to do this for you,
            just as needed.
            '''

#           print("pyfe Context.manage " + library)

            command = "manage " + library
            self.execute(command)

        def unmanage(self, library):
            '''
            Manually drop a C++ module library.

            Generally, you should be able to just leave these modules in memory.
            '''

#           print("pyfe Context.unmanage " + library)

            command = "unmanage " + library
            self.execute(command)

        def load(self, path, name = False, options = ""):
            '''
            Load a surface from file.

            path: filename containing surface
            name: text to use for logging purposes
            options: arbitrary text passed along to the surface handler

            If no name is provided, the path is used as the name.

            Options are generally in a space delimited format,
            with optional assignment, like 'a b c=d'.
            '''

            name = name or path

#           print("pyfe Context.load " + path + " name " + name)

            command = "load %s '%s' '%s'" % (name, path, options)
            result = self.execute(command)

            if result != "loaded":
                return None

            return Surface(self, name)

        def master(self):
            '''
            Return the pyfe.Master in use.
            '''

#           print("pyfe Context.master ")

            return Master(self)

        def create(self, operator, name = False):
            '''
            Create an operator.

            operator: dot delimited implementation spec (or C++ class name)
            name: text to use for logging purposes

            If no name is provided, the operator spec is used as the name.
            The name is used for internal bookkeeping.  It should be unique.

            As a convenience, a C++ operator class name can usually be
            substituted for a proper component implementation spec.
            '''

            name = name or operator

#           print("pyfe Context.create " + operator + " name " + name)

            command = "create " + operator + " " + name
            result = self.execute(command)

            if result != "created":
                return None

            return Operator(self, name)

    class Master(object):
        '''
        Proxy to the C++ singleton Master object.

        The Master should be obtained from the Context.

        Currently, the global Catalog is exposed, allowing you to set values
        which can potentially be read by C++ objects.
        '''

        def __init__(self, context):
#           print("pyfe Master.init ")
            self.context = context

        def __del__(self):
#           print("pyfe Master.del ")
            pass

        def set(self, attribute, prop, value, typename = False):
            '''
            Set a value in the global Catalog.

            attribute: primary Catalog key
            prop: secondary Catalog key (property of an attribute)
            value: what to set the property to (the Python type matters)

            Currently, the following Python types are supported:
                real, integer, boolean, string, list of three reals

            Setting a existing entry to a new type can cause an exception.
            '''

            if not typename:
                if type(value) is float:
                    typename = "real"
                elif type(value) is int:
                    typename = "integer"
                elif type(value) is bool:
                    typename = "boolean"
                elif type(value) is str:
                    typename = "string"
                    value = value.replace('"', '\\"')
                    value = "\"%s\"" % value
                elif type(value) is list and len(value) == 3:
                    typename = "vector3"
                    value = "\"%G %G %G\"" % (value[0], value[1], value[2])
                else:
                    print("Master.set incompatible type " + str(type(value)))
                    return

#           print("pyfe Master.set \"" + attribute + "\" " + typename + " " + str(value))

            command = "set MASTER \"%s\" \"%s\" %s %s" % (attribute, prop, typename, str(value))
            self.context.execute(command)

        def get(self, attribute, prop):
            '''
            Get a value from the global Catalog.

            attribute: primary Catalog key
            prop: secondary Catalog key (property of an attribute)

            Currently, the return value is always represented in a string.
            '''

#           print("pyfe Master.get " + attribute)

            command = "get MASTER \"%s\" \"%s\"" % (attribute, prop)
            return self.context.execute(command)

        def __setitem__(self, attribute, value):
            '''
            Call set() using the square bracket syntax.
            '''
            self.set(attribute, "value", value)

        def __getitem__(self, attribute):
            '''
            Call get() using the square bracket syntax.
            '''
            return self.get(attribute, "value")

    class Operator(object):
        '''
        Surface Operator

        An Operator should be created from the Context.
        '''

        def __init__(self, context, name = "Operator"):
#           print("pyfe Operator.init " + name)
            self.context = context
            self.objName = name

        def __del__(self):
#           print("pyfe Operator.del " + self.objName)

            command = "release " + self.objName
            self.context.execute(command)

        def name(self):
            '''
            Return the name used when the operator was created.

            Unique names should be used.
            '''
            return self.objName

        def operate(self, input0 = None, input1 = None, input2 = None, input3 = None, name = False):
            '''
            Run the operator.

            Up to fours input surfaces may be provided.

            The provided name is applied to the newly created output surface.
            '''
            name = name or (self.objName + ".out")

#           print("pyfe Operator.operate " + name)

            command = "signal " + self.objName + " " + name

            if input0:
#               print("  input0 " + input0.name())
                command += " " + input0.name()
            if input1:
#               print("  input1 " + input1.name())
                command += " " + input1.name()
            if input2:
#               print("  input2 " + input2.name())
                command += " " + input2.name()
            if input3:
#               print("  input3 " + input3.name())
                command += " " + input3.name()

            self.context.execute(command)

            return Surface(self.context, name)

        def set(self, attribute, prop, value, typename = False):
            '''
            Set a value in the operator's parameter Catalog.

            attribute: primary Catalog key
            prop: secondary Catalog key (property of an attribute)
            value: what to set the property to (the Python type matters)

            Currently, the following Python types are supported:
                real, integer, boolean, string, list of three reals

            Setting a existing entry to a new type can cause an exception.
            '''

            if not typename:
                if type(value) is float:
                    typename = "real"
                elif type(value) is int:
                    typename = "integer"
                elif type(value) is bool:
                    typename = "boolean"
                elif type(value) is str:
                    typename = "string"
                    value = value.replace('"', '\\"')
                    value = "\"%s\"" % value
                elif type(value) is list and len(value) == 3:
                    typename = "vector3"
                    value = "\"%G %G %G\"" % (value[0], value[1], value[2])
                else:
                    print("pyfe Operator.set incompatible type " + str(type(value)))
                    return

#           print("pyfe Operator.set \"" + attribute + "\" " + typename + " " + str(value))

            command = "set %s \"%s\" \"%s\" %s %s" % (self.objName, attribute, prop, typename, str(value))
            self.context.execute(command)

        def get(self, attribute, prop):
            '''
            Get a value from the operator's parameter Catalog.

            attribute: primary Catalog key
            prop: secondary Catalog key (property of an attribute)

            Currently, the return value is always represented in a string.
            '''

#           print("pyfe Operator.get " + attribute)

            command = "get %s \"%s\" \"%s\"" % (self.objName, attribute, prop)
            return self.context.execute(command)

        def keys(self):
            '''
            Get a list of all the keys in the operator's parameter Catalog.
            '''

            command = "keys %s" % self.objName
            response = self.context.execute(command)

            result = {}

            entries = shlex.split(response)
            for entry in entries:
                key,keyType = entry.split("=")
                result[key] = keyType

            return result

        def __call__(self, input0 = None, input1 = None, input2 = None, input3 = None, name = False):
            '''
            Call operate() using the paren syntax.
            '''

            return self.operate(input0, input1, input2, input3)

        def __setitem__(self, attribute, value):
            '''
            Call set() using the square bracket syntax.
            '''

            self.set(attribute, "value", value)

        def __getitem__(self, attribute):
            '''
            Call get() using the square bracket syntax.
            '''

            return self.get(attribute, "value")

    class Surface(object):
        '''
        Geometric Surface (Mesh, Curves, Point Cloud, etc)

        An Surface should be loaded using the Context,
        returned from running an operator,
        or be created with clone().
        '''

        def __init__(self, context, name = "Surface"):
#           print("pyfe Surface.init " + name)
            self.context = context
            self.objName = name

            if not name in context.nameCount:
                context.nameCount[name] = 0

            context.nameCount[name]+=1

        def __del__(self):
#           print("pyfe Surface.del " + self.objName)

            self.context.nameCount[self.objName]-=1

            if self.context.nameCount[self.objName] == 0:
                command = "release " + self.objName
                self.context.execute(command)

        def name(self):
            '''
            Return the name used when the surface was created.

            Unique names should be used.
            '''

            return self.objName

        def save(self, path, options = ""):
            '''
            Save the surface to a file.

            path: filename to write surface
            options: arbitrary text passed along to the surface handler

            Options are generally in a space delimited format,
            with optional assignment, like 'a b c=d'.
            '''

#           print("pyfe Surface.save " + path)

            command = "save %s '%s' '%s'" % (self.objName, path, options)
            self.context.execute(command)

        def count(self, element):
            '''
            Return the quantity of a particular element

            Currently, the element can be "point" or "primitive".
            '''

#           print("pyfe Surface.count %s" % element)

            command = "count %s %s" % (self.objName, element)
            return self.context.execute(command)

        def specs(self, element):
            '''
            Return the list of attributes for a particular element

            Currently, the element can be "point" or "primitive".
            '''
            '''
            Return an Accessor to a named attribute for a particular element

            Currently, the element can be "point" or "primitive".

            The special attribute name "<vertices>" can always be used
            for the "primitive" element.
            '''

#           print("pyfe Surface.specs %s" % element)

            command = "specs %s %s" % (self.objName, element)
            result = self.context.execute(command)

            attributeList = result.split(",")
            attributeMap = {}
            for attribute in attributeList:
                if attribute != "":
                    entry = attribute.split()
                    attributeMap[entry[1]] = entry[0]
            return attributeMap;

        def access(self, element, attribute):
            '''
            Return an Accessor to a named attribute for a particular element

            Currently, the element can be "point" or "primitive".

            The special attribute name "<vertices>" can always be used
            for the "primitive" element.
            '''

#           print("pyfe Surface.access %s %s" % (element, attribute))

            command = "access %s %s %s" % (self.objName, element, attribute)
            name = self.context.execute(command)

            return Accessor(self.context, attribute, name)

        def clone(self, name):
            '''
            Return a duplicate of the surface.

            A new unique name should be provided.
            '''

#           print("pyfe Surface.clone to %s" % (name))

            command = "clone %s %s" % (self.objName, name)
            result = self.context.execute(command)

            if result != "cloned":
                return None

            return Surface(self.context, name)

    class Accessor(object):
        '''
        Data handle to a particular attribute of a Surface.

        An Accessor should be obtained from a Surface.
        '''

        def __init__(self, context, attribute, name = "Accessor"):
#           print("pyfe Accessor.init " + name)
            self.context = context
            self.isVertices = (attribute == "<vertices>")
            self.objName = name

            if not name in context.nameCount:
                context.nameCount[name] = 0

            context.nameCount[name]+=1

        def __del__(self):
#           print("pyfe Accessor.del " + self.objName)
            self.context.nameCount[self.objName]-=1

            if self.context.nameCount[self.objName] == 0:
                command = "release " + self.objName
                self.context.execute(command)

        def name(self):
            '''
            Return the name used when the accessor was created.

            A unique name should have been automatically assigned.
            '''
            return self.objName

        def __getitem__(self, index):
            '''
            Return the value at the given index.

            Currently, the return value is always represented in a string.
            '''

#           print("pyfe Accessor.__getitem__ " + str(index))

            command = "get %s %d" % (self.objName, index)
            result = self.context.execute(command)

            if self.isVertices:
                return result.split()

            return result

        def __setitem__(self, index, value):
            '''
            Set the value at the given index.

            The Python type matters.
            '''

#           print("pyfe Accessor.__setitem__ " + str(index) + " " + str(value))

            command = "set %s %d %s" % (self.objName, index, str(value))
            return self.context.execute(command)
