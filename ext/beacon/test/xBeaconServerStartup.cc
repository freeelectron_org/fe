/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <chrono>
#include "beacon/beacon.h"

int main(int argc,char** argv)
{
    fe::sp<fe::Master> spMaster(new fe::Master);
    fe::sp<fe::Registry> spRegistry=spMaster->registry();

    spRegistry->manage("fexBeaconServerDL");
    fe::sp<beacon::BeaconServerStartupI> spBeaconServerStartup(
			spRegistry->create("*.BeaconServerStartup"));

	if(!spBeaconServerStartup.isValid())
    {
        feX(argv[0], "couldn't create Beacon server component");
		return 1;
    }

	if(argc < 2 || !spBeaconServerStartup->startup(argv[1]))
	{
		feLog("Beacon startup failed using file: %s\n", argv[1]);
		return 2;
	}

	bool running = true;

	feLog("Enter q to to stop Beacon\n");
	while(running && spBeaconServerStartup->running())
	{
		char c;
		std::cin >> c;

		if(c == 'q' || c == 'Q')
		{
			running = false;
		}
	}

	spBeaconServerStartup->stop();

	feLog("Beacon server shutdown\n");

	return 0;
}
