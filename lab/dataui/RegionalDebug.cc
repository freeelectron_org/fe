/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "RegionalDebug.h"
#include "RootLocate.h"
#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

RegionalDebug::RegionalDebug(void)
{
	m_offset[0] = 0.0;
	m_offset[1] = 0.0;
}

RegionalDebug::~RegionalDebug(void)
{
}

void RegionalDebug::initialize(void)
{
	// default event mapping
	maskInit("fe_regionaldebug");
	maskDefault("_select",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_statePress));
	maskDefault("_home",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)27 /* esc */,
							WindowEvent::e_statePress));

	cfg<Real>("margin") = 0.0;
	cfg<String>("label") = "";
	cfg<Vector2>("offset")[0] = 0;
	cfg<Vector2>("offset")[1] = 0;
	cfg< sp<Layout> >("draw");
}

void RegionalDebug::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	if(!m_spScope.isValid())
	{
		m_spScope = l_sig->scope();
	}
	m_asSignal.bind(l_sig->scope());
	m_asWindata.bind(l_sig->scope());
	m_asOrtho.bind(l_sig->scope());
}

void RegionalDebug::handle(Record &r_sig)
{
	m_select = maskGet("_select");
	m_home = maskGet("_home");
	m_wev.bind(r_sig);
	if(!maskEnabled(m_wev))
	{
		return;
	}
	BWORD select_or_home=m_wev.is(m_select) || m_wev.is(m_home);
	m_wev.unbind();
	if(select_or_home)
	{
		handleEvent(r_sig);
	}
	else if(r_sig.layout() == cfg< sp<Layout> >("draw"))
	{
		if(!m_drawview.handle(r_sig))
		{
			return;
		}
//		sp<EventContextI> spEC(m_drawview.windowI()->getEventContextI());
		I32 a, d;
		m_drawview.drawI()->font()->fontHeight(&a, &d);
		m_fontHeight = a+d;
		m_height = height(m_drawview.windowI()->geometry());
		m_width = width(m_drawview.windowI()->geometry());
		m_cursor[0] = 0.0;
		m_cursor[1] = m_height-m_fontHeight;
		applyOffset();
		m_lo[0] = 1e8;
		m_lo[1] = 1e8;
		m_hi[0] = -1e8;
		m_hi[1] = -1e8;
		Color white(1.0f,1.0f,1.0f,1.0f);
		String label = cfg<String>("label");
		Real &m_margin = cfg<Real>("margin");
		if(label != "")
		{
			Real margin = m_margin;
			m_margin = 1.0;
			applyOffset();
			drawText(label, white);
			m_margin = margin;
			applyOffset();
			m_offset[1] -= m_fontHeight;
		}
		handleDraw(r_sig);
		if(m_margin > 0.0)
		{
			Box3 box;
			set(box,m_lo[0],m_lo[1],Real(0),
					m_hi[0]-m_lo[0], m_hi[1]-m_lo[1],Real(0));
			m_drawview.drawI()->drawBox(box, white);
		}
	}
}

void RegionalDebug::drawText(const String &a_text, const Color a_color)
{
	Real &m_margin = cfg<Real>("margin");
	Vector2f lo, hi;
	lo[0] = m_cursor[0]+m_offset[0]-m_margin;
	lo[1] = m_cursor[1]+m_offset[1]-m_margin;
	hi[0] = m_cursor[0]+m_offset[0]+textWidth(a_text)+m_margin;
	hi[1] = m_cursor[1]+m_offset[1]+m_fontHeight+m_margin;
	if(lo[0] < m_lo[0]) { m_lo[0] = lo[0]; }
	if(lo[1] < m_lo[1]) { m_lo[1] = lo[1]; }
	if(hi[0] > m_hi[0]) { m_hi[0] = hi[0]; }
	if(hi[1] > m_hi[1]) { m_hi[1] = hi[1]; }
	Vector4 textLoc(m_cursor[0]+m_offset[0],m_cursor[1]+m_offset[1],0.0,0.0);
	m_drawview.drawI()->drawAlignedText(textLoc,a_text,a_color);
}

Real RegionalDebug::textWidth(const String &a_text)
{
//	sp<EventContextI> spEC(m_drawview.windowI()->getEventContextI());
	return m_drawview.drawI()->font()->pixelWidth(a_text);
}

void RegionalDebug::applyOffset(void)
{
	Vector4 c;
	Vector2 &v = cfg<Vector2>("offset");
	RootLocate::convert(c, m_drawview.windowI(), v, v);
	Real &m_margin = cfg<Real>("margin");
	m_offset[0] = c[0] + m_margin;
	m_offset[1] = -c[1] - m_margin;
}

void RegionalDebug::handleEvent(Record &r_sig)
{
	applyOffset();
	if(cfg<String>("label") != "")
	{
		m_offset[1] -= m_fontHeight;
	}
	Record r_event = r_sig;
	if(!m_asSignal.winData.check(r_event))
	{
		feLog("RegionalDebug::handleEvent winData not in event\n");
		return;
	}
	Record r_windata = m_asSignal.winData(r_event);
	if(!r_windata.isValid())
	{
		feLog("RegionalDebug::handleEvent winData invalid\n");
		return;
	}
	if(!m_asWindata.windowI(r_windata).isValid())
	{
		feLog("RegionalDebug::handleEvent winData windowI invalid\n");
		return;
	}
	sp<WindowI> spWindow(m_asWindata.windowI(r_windata));
	Real h = height(spWindow->geometry());
	m_wev.bind(r_event);
	if(m_wev.is(m_home))
	{
		m_wev.unbind();
		handleHome();
		return;
	}

	Real x = m_wev.mouseX();
	Real y = m_wev.mouseY();
	m_wev.unbind();

	if(m_asOrtho.check(r_windata))
	{
		x -= m_asOrtho.center(r_windata)[0];
		y += m_asOrtho.center(r_windata)[1];
	}

	for(int i = 0; i < int(m_regions.size()); i++)
	{
		if(	(x-m_offset[0]		>= m_regions[i]->m_lo[0])				&&
			(x-m_offset[0]		<= m_regions[i]->m_hi[0])				&&
			(h-y-m_offset[1]	>= m_regions[i]->m_lo[1])				&&
			(h-y-m_offset[1]	<= m_regions[i]->m_hi[1]))
		{
			handleRegion(m_regions[i]);
			return;
		}
	}
}

} /* namespace ext */
} /* namespace fe */

