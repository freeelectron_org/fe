/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawView.h"

namespace fe
{
namespace ext
{

DrawView::DrawView(void)
{
}

DrawView::~DrawView(void)
{
}

bool DrawView::handle(Record &r_sig)
{
	sp<Scope> spScope = r_sig.layout()->scope();
	m_asSignal.bind(spScope);
	m_asWindata.bind(spScope);
	m_asColor.bind(spScope);
	if(!m_asSignal.winData.check(r_sig)) { return false; }
	Record r_windata = m_asSignal.winData(r_sig);

	if(!r_windata.isValid()) { return false; }

	if(!m_asWindata.check(r_windata)) { return false; }

	m_spDraw = m_asWindata.drawI(r_windata);

	m_spRG = m_asWindata.group(r_windata);

	if(!m_spRG.isValid())
	{
		return false;
	}

	m_spWindow = m_asWindata.windowI(r_windata);

	return true;
}

const Color &DrawView::getColor(const String &a_name, const Color &a_default)
{
	if(!group().isValid())
	{
		return a_default;
	}
	Record r_color;
	r_color = group()->lookup<String>(m_asColor.name, a_name);
	if(r_color.isValid())
	{
		return m_asColor.rgba(r_color);
	}
	return a_default;
}

} /* namespace ext */
} /* namespace fe */

