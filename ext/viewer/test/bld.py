import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexDataToolDLLib",
                'fexViewerDLLib',
                'fexWindowDLLib' ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib" ]

    tests = [   'xViewer' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xViewer.exe",     "10",                               None,   None),
        ("xViewer.exe",     "fexViewerDL QuickViewerI 3 10",    None,   None)]  # test 3 viewers at once

    if 'console' in forge.modules_confirmed:
        forge.tests += [
            ("xViewer.exe", "fexConsoleDL 10",                  None,   None),
            ("xViewer.exe", "fexConsoleDL \"*.NullViewer\" 10", None,   None) ]
