import os
import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    # NOTE also had netsignal
    return ["operator", "terminal"]

def setup(module):
    module.summary = []

    # NOTE NetworkOp currently requires netsignal module
    build_NetworkOp = ('netsignal' in forge.modules_confirmed)

    # TODO check Windows
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        build_NetworkOp = False

    srcList = [ "oplab.pmh",
                "BindEditOp",
                "CurvaceousOp",
                "FusionOp",
                "GridWrapOp",
                "MultiModOp",
                "NexusOp",
                "PreviewOp",
                "SharpenOp",
                "XRayOp",
                "oplabDL" ]

    module.cppmap = {}

    if build_NetworkOp:
        srcList += [ "NetworkOp" ]

        module.cppmap['networkop'] = ' -DFE_BUILD_NETWORKOP'

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")
    for src in srcList:
            extra = ""
            if re.compile(r'(.*)Op$').match(src):
                with open(manifest, 'a') as outfile:
                    outfile.write('\tspManifest->catalog<String>(\n'+
                            '\t\t\t"OperatorSurfaceI.' + src + '.fe' +
                            extra + '")='+ '"fexOpLabDL";\n');

    dll = module.DLL( "fexOpLabDL", srcList )

    deplibs = forge.corelibs+ [
                "fexDrawDLLib",
                "fexOperateDLLib",
                "fexOperatorDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexGeometryDLLib",
                        "fexNetworkDLLib",
                        "fexThreadDLLib" ]

        if build_NetworkOp:
            deplibs += [ "fexNetSignalDLLib" ]

    if 'graphviz' in forge.modules_confirmed and os.path.exists('/usr/include/graphviz/gvc.h'):
        module.cppmap['meta'] = ' -DFE_USE_GRAPHVIZ'
    else:
        module.summary += [ "-graphviz" ]

    forge.deps( ["fexOpLabDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOpLabDL",                   None,   None) ]

