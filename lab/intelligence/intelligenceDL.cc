/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexElementDL"));
	list.append(new String("fexLuaDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	String path=spMaster->catalog()->catalog<String>("path:media")+
			"/template/intelligence.rg";

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<Attract>("HandlerI.Attract.fe");
	pLibrary->add<Collect>("HandlerI.Collect.fe");
	pLibrary->add<Communicate>("HandlerI.Communicate.fe");
	pLibrary->add<Control>("HandlerI.Control.fe");
	pLibrary->add<Construct>("HandlerI.Construct.fe");
	pLibrary->add<Detonate>("HandlerI.Detonate.fe");
	pLibrary->addSingleton<Director>("DirectorI.Director.fe");
	pLibrary->add<Discard>("HandlerI.Discard.fe");
	pLibrary->add<Dominate>("HandlerI.Dominate.fe");
	pLibrary->add<DrawConstruct>("HandlerI.DrawConstruct.fe");
	pLibrary->add<DrawParticle>("HandlerI.DrawParticle.fe");
	pLibrary->add<DrawRadio>("HandlerI.DrawRadio.fe");
	pLibrary->add<DrawScan>("HandlerI.DrawScan.fe");
	pLibrary->add<DrawTargeting>("HandlerI.DrawTargeting.fe");
	pLibrary->add<DrawThrust>("HandlerI.DrawThrust.fe");
	pLibrary->add<FactionFilter>("FilterI.FactionFilter.fe");
	pLibrary->add<LuaBehavior>("HandlerI.LuaBehavior.fe");
	pLibrary->add<Scan>("HandlerI.Scan.fe");
	pLibrary->add<Shoot>("HandlerI.Shoot.fe");
	pLibrary->add<Sing>("HandlerI.Sing.fe");
	pLibrary->add<Thrust>("HandlerI.Thrust.fe");
	pLibrary->add<Tick>("HandlerI.Tick.fe");

	pLibrary->add<Attractor>("RecordFactoryI.Attractor.fe");
	pLibrary->add<Behavior>("RecordFactoryI.Behavior.fe");
	pLibrary->add<Cannon>("RecordFactoryI.Cannon.fe");
	pLibrary->add<ControlCenter>("RecordFactoryI.ControlCenter.fe");
	pLibrary->add<Controlled>("RecordFactoryI.Controlled.fe");
	pLibrary->add<Crowd>("RecordFactoryI.Crowd.fe");
	pLibrary->add<Foundry>("RecordFactoryI.Foundry.fe");
	pLibrary->add<Matter>("RecordFactoryI.Matter.fe");
	pLibrary->add<Mortal>("RecordFactoryI.Mortal.fe");
	pLibrary->add<Periodic>("RecordFactoryI.Periodic.fe");
	pLibrary->add<Radio>("RecordFactoryI.Radio.fe");
	pLibrary->add<Scanner>("RecordFactoryI.Scanner.fe");
	pLibrary->add<Thruster>("RecordFactoryI.Thruster.fe");

	sp<Scope> spScope=spMaster->catalog()->catalogComponent("Scope","SimScope");
	RecordView::loadRecordGroup(spScope,path);

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
