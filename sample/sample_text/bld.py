import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    srcList = [ "sample_text.pmh",
                "BruteScan",
                "sample_textDL" ]

    dll = module.DLL( "fexSampleTextDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    forge.deps( ["fexSampleTextDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexSampleTextDL",                      None,   None) ]

    module.Module('test')
