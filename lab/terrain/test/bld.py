import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   [
                "fexViewerLib",
                "fexSignalLib",
                "fexDataToolDLLib" ] + forge.corelibs

    tests = [   'xTerrain' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xTerrain.exe","10",                                       None,None),
        ("xSurface.exe","terrain/butte.rg 100",                     None,None),
        ("xSurface.exe","terrain/butte.rg \"*.DrawRayTrace\" 2",    None,None),
        ("xSurface.exe","terrain/letters.rg 100",                   None,None),
        ("xSurface.exe","terrain/letters.rg \"*.DrawRayTrace\" 2",  None,None),
        ("xSurface.exe","terrain/checkerboard.rg 100",              None,None)]
