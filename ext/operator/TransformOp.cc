/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

using namespace fe;
using namespace fe::ext;

void TransformOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("orientation")="world";
	catalog<bool>("orientation","hidden")=true;

	catalog<String>("Brush","prompt")=
			"  Enter toggles pivot/deform."
			"  LMB manipulates."
			"  MMB remanipulates last control."
			"  Shift slows manipulation.";

	catalogRemove("Driver Surface");
}

void TransformOp::handle(Record& a_rSignal)
{
	HammerOp::handle(a_rSignal);
}
