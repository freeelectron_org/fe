/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

namespace fe
{
namespace ext
{

Fork::Fork(void)
{
}

Fork::~Fork(void)
{
}

void Fork::initialize(void)
{
}

void Fork::use(Record &r_fork)
{
	m_fork = r_fork;
}

void Fork::spawn(sp<Layout> &l_fork)
{
	m_spSpawn = l_fork;
}

void Fork::handle(Record& record)
{
	if(m_spSpawn.isValid())
	{
		Record r_spawn = m_spScope->createRecord(m_spSpawn);
		m_spSignaler->signal(r_spawn);
	}
	if(m_fork.isValid())
	{
		m_spSignaler->signal(m_fork);
	}
}

void Fork::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_spSignaler = spSignaler;
	m_spScope = l_sig->scope();
}

} /* namespace ext */
} /* namespace fe */


