/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

//#define FE_INITIALIZE_SEGMENTS

//#define FE_SB_START_COUNT	2
//#define FE_SB_GROW_COUNT(c) ((FE_UWORD)(((c * 1.5)==c)?(c * 1.5):(c + 1)))
#define FE_SB_START_COUNT	1024
#define FE_SB_GROW_COUNT(c) ((FE_UWORD)(((c * 2.0)==c)?(c * 2.0):(c + 1)))

namespace fe
{

SegmentStore::SegmentStore(void)
{
	m_pFreeList = NULL;
	m_pSegment = NULL;

	//* NOTE shouldn't need to do this if it was created by the Registry
	setName("SegmentStore");
}

SegmentStore::~SegmentStore(void)
{
	clear();
}

void SegmentStore::clear(void)
{
	while(m_pSegment)
	{
		Segment *pNext = m_pSegment->m_pNext;
		deallocate((void *)m_pSegment);
#if FE_COUNTED_TRACK
		Counted::deregisterRegion(m_pSegment);
#endif
		m_pSegment = pNext;
	}
}

FE_UWORD SegmentStore::skipSizeSB(void)
{
	return stateBlockBytes();
}

void* SegmentStore::stateBlockOf(U32 index,void* pSegment)
{
	return (void *)( ((FE_UWORD)pSegment) + sizeof(Segment) +
			(stateBlockBytes() * index));
}

// NOTE alignment adjustment only actually matters if some attribute cares
FE_UWORD SegmentStore::stateBlockBytes(void)
{
	FE_UWORD bytes=m_hpLayout->size() + sizeof(SBHeader);
#if FE_MEM_ALIGNMENT
	FE_UWORD skew=bytes%FE_MEM_ALIGNMENT;
	if(skew)
	{
		return bytes+FE_MEM_ALIGNMENT-skew;
	}
#endif
	return bytes;
}

FE_UWORD SegmentStore::segmentBytes(U32 count)
{
	return sizeof(Segment)+count*stateBlockBytes();
}

void *SegmentStore::createSB(void)
{
	SBHeader *pAllocated;

	// check for free list block
	if(m_pFreeList)
	{
		pAllocated = (SBHeader *)m_pFreeList;
		m_pFreeList = m_pFreeList->m_pNext;
		// createSB needs to be fast; no dynamic_cast
		pAllocated->m_pStore = (StoreI *)this;
		initSB(FE_HDR_TO_SB(pAllocated));
		acquireSB(FE_HDR_TO_SB(pAllocated));
		return FE_HDR_TO_SB(pAllocated);
	}

	// make first segment
	if(!m_pSegment)
	{
		U32 segmentSize=segmentBytes(FE_SB_START_COUNT);
		m_pSegment = (Segment *)allocate(segmentSize);
#ifdef FE_INITIALIZE_SEGMENTS
		memset((void *)m_pSegment, 2, segmentSize);
#endif
		m_pSegment->m_allocated = FE_SB_START_COUNT;
		m_pSegment->m_used = 0;
		m_pSegment->m_pNext = NULL;
	}

	FEASSERT(m_pSegment->m_used <= m_pSegment->m_allocated);

	// if segment is full, make new segment
	if(m_pSegment->m_used == m_pSegment->m_allocated)
	{
		U32 segmentSize=segmentBytes(FE_SB_GROW_COUNT(m_pSegment->m_allocated));
		Segment *pSegment = (Segment *)allocate(segmentSize);
		pSegment->m_allocated = FE_SB_GROW_COUNT(m_pSegment->m_allocated);
		pSegment->m_used = 0;
		pSegment->m_pNext = m_pSegment;
		m_pSegment = pSegment;
	}

	FEASSERT(m_pSegment->m_used < m_pSegment->m_allocated);

	pAllocated = (SBHeader *)stateBlockOf(m_pSegment->m_used, m_pSegment);
	m_pSegment->m_used++;
	pAllocated->m_pStore = (StoreI *)this;

#if FE_COUNTED_TRACK
	Counted::registerRegion(pAllocated,stateBlockBytes(),
			"Segment "+m_hpLayout->name());
	Counted::trackReference(pAllocated,fe_cast<Counted>(this),
			"SegmentStore (single)");
#endif

	// set the reference count to 0
	//* TODO clean this up
	if(m_hpLayout.isValid())
	{
		if(m_hpLayout->checkAttribute(m_cntIndex))
		{
			int &count= *((int *)((char *)(FE_HDR_TO_SB(pAllocated)) +
										m_hpLayout->offsetTable()[m_cntIndex]));
			count=0;

//			fe_fprintf(stderr,"acquireSB %p %d %s\n",(U32)this,count,
//					m_hpLayout->name().c_str());
		}
	}

	initSB(FE_HDR_TO_SB(pAllocated));
	acquireSB(FE_HDR_TO_SB(pAllocated));
	return FE_HDR_TO_SB(pAllocated);
}

void *SegmentStore::createSB(FE_UWORD count)
{
	// This version of CreateSB purposely allocates a contiguous set of SBs
	U32 segmentSize=segmentBytes(count);
	Segment *pSegment = (Segment *)allocate(segmentSize);
#ifdef FE_INITIALIZE_SEGMENTS
	memset((void *)m_pSegment, 2, segmentSize);
#endif
	pSegment->m_allocated = count;
	pSegment->m_used = count;
	if(!m_pSegment)
	{
		m_pSegment = pSegment;
		m_pSegment->m_pNext = NULL;
	}
	else
	{
		pSegment->m_pNext = m_pSegment->m_pNext;
		m_pSegment->m_pNext = pSegment;
	}

	// clear all the reference counts and set the depot pointers
	SBHeader *pAllocated=NULL;
	FE_UWORD i;
	if(m_hpLayout.isValid())
	{
		for(i = 0; i < count; i++)
		{
			pAllocated = (SBHeader *)stateBlockOf(i, pSegment);
			pAllocated->m_pStore = (StoreI *)this;

#if FE_COUNTED_TRACK
			Counted::registerRegion(pAllocated,stateBlockBytes(),
					"Segment "+m_hpLayout->name());
			Counted::trackReference(pAllocated,fe_cast<Counted>(this),
					"SegmentStore (set)");
#endif
		}
		if(m_hpLayout->checkAttribute(m_cntIndex))
		{
			for(i = 0; i < count; i++)
			{
				IWORD &count= *((IWORD *)((char *)
					(FE_HDR_TO_SB(stateBlockOf(i,pSegment))) +
					m_hpLayout->offsetTable()[m_cntIndex]));
				count=0;
			}
		}
	}

	for(i = 0; i < count; i++)
	{
		initSB(FE_HDR_TO_SB(pAllocated));
		acquireSB(FE_HDR_TO_SB(stateBlockOf(i,pSegment)));
	}

	return FE_HDR_TO_SB(stateBlockOf(0,pSegment));
}

void SegmentStore::freeSB(void *stateBlock)
{
#if FE_COUNTED_TRACK
	Counted::deregisterRegion(FE_SB_TO_HDR(stateBlock));
#endif

	m_hpLayout->destructAttributes(stateBlock);
	FreeHeader *pFree = (FreeHeader *)(FE_SB_TO_HDR(stateBlock));
	pFree->m_pNext = m_pFreeList;
	m_pFreeList = pFree;
}

bool SegmentStore::queryRC(void *sb, I32 *&pCount)
{
	if(!m_hpLayout->checkAttribute(m_i32Index))
	{
		return false;
	}
	pCount = &(*((I32 *)((char *)(sb)+m_hpLayout->offsetTable()[m_i32Index])));
	return true;
}


void SegmentStore::initSB(void *stateBlock)
{
	I32 *pCount;
	//if(m_aRC.queryAttribute(m_hpLayout, stateBlock, pCount))
	if(queryRC(stateBlock, pCount))
	{
		(*pCount)=0;
	}
}

void SegmentStore::acquireSB(void *stateBlock)
{
	I32 *pCount;
	//if(m_aRC.queryAttribute(m_hpLayout, stateBlock, pCount))
	if(queryRC(stateBlock, pCount))
	{
		(*pCount)++;
#if FE_COUNTED_TRACK
		Counted::acquireRegion(FE_SB_TO_HDR(stateBlock),
				"Segment "+m_hpLayout->name(),*pCount);
#endif
	}
	acquire();
}

void SegmentStore::releaseSB(const RecordSB &r_old)
{
	void *stateBlock = r_old.data();
	I32 *pCount;
	//if(m_aRC.queryAttribute(m_hpLayout, stateBlock, pCount))
	if(queryRC(stateBlock, pCount))
	{
		(*pCount)--;
#if FE_COUNTED_TRACK
		Counted::releaseRegion(FE_SB_TO_HDR(stateBlock),
				"Segment "+m_hpLayout->name(),*pCount);
#endif
		if((*pCount) == 0)
		{
			m_hpLayout->scope()->freeIDNumber(r_old);
			freeSB(stateBlock);
		}
	}
	release();
}

hp<LayoutSB>& SegmentStore::getLayout(void)
{
	return m_hpLayout;
}

void SegmentStore::setLayout(sp<LayoutSB>& rspLayout)
{
	m_hpLayout = rspLayout;
	if(!m_hpLayout->locked())
	{
		feX("SegmentStore::setLayout",
			"can only set locked layouts");
	}
//	feLog("SegmentStore::setLayout %s\n",rspLayout->name().c_str());
	setName(rspLayout->name());
	//m_aRC.setup(m_hpLayout->scope(), FE_USE(":CT"), "integer");
	sp<Scope> scope(m_hpLayout->scope());
	if(!(scope->findAttribute(FE_USE(":CT"),m_i32Index).isValid()))
	{
		scope->support(":CT","integer");
		if(!(scope->findAttribute(":CT",m_i32Index).isValid()))
		{
			feX("SegmentStore::setLayout",
				"accessor init could not find valid index for attribute %s"
				, ":CT");
		}
	}
	scope->findAttribute(FE_USE(":CT"),m_cntIndex);
}

} /* namespace */
