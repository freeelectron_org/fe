/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace fe
{
namespace ext
{

MultiGroupReader::MultiGroupReader(void)
{
	m_initialized = false;
}

MultiGroupReader::~MultiGroupReader(void)
{
}

void MultiGroupReader::initialize(void)
{
}

void MultiGroupReader::handle(Record &a_signal)
{
	sp<RecordGroup> rg_output =
		cfg< sp<RecordGroup> >("output"); // output group


	if(!rg_output.isValid())
	{
		return;
	}

	if(!m_initialized)
	{
		m_initialized = 1;
		try
		{
			int start = atoi(cfg<String>("multi.start").c_str());
			int end = atoi(cfg<String>("multi.end").c_str());

			if(start==0 && end==0)
			{
				end = 1000000;
			}

			bfs::path prefix(cfg<String>("multi.prefix").c_str());
			bfs::path p = prefix.parent_path();
			Regex re_in_file("(.*)\\.([0-9]+)\\.rg");
			bfs::directory_iterator i_end;
			for (bfs::directory_iterator i_dir(p); i_dir!=i_end; i_dir++)
			{
				if(bfs::is_regular_file(i_dir->status()))
				{
					fprintf(stderr, "%s\n", i_dir->string().c_str());
					if(re_in_file.match(i_dir->string().c_str()))
					{
						sp<data::StreamI> spStream(new data::AsciiStream(
							a_signal.layout()->scope()));
						std::string s = re_in_file.result(2);
						int frame = std::atoi(s.c_str());

						if(frame < start || frame > end)
						{
							continue;
						}

						std::ifstream infile(i_dir->string().c_str());

						sp<RecordGroup> rg_read = spStream->input(infile);

						infile.close();

						m_loaded[frame] = rg_read;
					}
				}
			}

			m_i_m_loaded = m_loaded.begin();
		}
		catch(...)
		{
			feLog("could not initialize input from \'%s\'\n",
				cfg<String>("multi.prefix").c_str());
		}
	}

	rg_output->clear();

	if(m_i_m_loaded == m_loaded.end())
	{
		m_i_m_loaded = m_loaded.begin();
	}

	if(m_i_m_loaded != m_loaded.end())
	{
		rg_output->add((m_i_m_loaded->second));
		m_i_m_loaded++;
	}
}

} /* namespace ext */
} /* namespace fe */

