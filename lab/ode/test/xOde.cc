/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"
#include "window/WindowEvent.h"
#include "architecture/architecture.h"
#include "world/world.h"
#include "math/math.h"
#include "ode/ode.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char **env)
{
	try
	{
		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		spMaster->registry()->manage("fexODEDL");

		sp<MechanicsSimI> spSim = spMaster->registry()->create("MechanicsSimI");

		spSim->setGravity(Vector3(0.0,0.0,-9.81));

		sp<BodyI> spBodyA = spSim->createBody();
		spBodyA->setPosition(Vector3(1.0,0.0,1.0));

		sp<BodyI> spBodyB = spSim->createBody();
		spBodyB->setPosition(Vector3(1.0,0.0,-1.0));
		//spBodyB->becomeCylinder(0.0001, 0.1f, 2.0);
		Mass mass;
		mass.cylinder(e_xAxis, 0.0001, 0.1f, 2.0);
		spBodyB->setMass(mass);

		sp<BodyI> spBodyC = spSim->createBody();
		spBodyC->setPosition(Vector3(2.0,0.0,0.0));


		sp<JointI> spJoint_a = spSim->createJoint(e_hinge,
			spBodyA, sp<Body>());
		spJoint_a->setAnchor(Vector3(0.0,0.0,1.0));
		spJoint_a->setAxis(Vector3(0.0,1.0,0.0));

		sp<JointI> spJoint_b = spSim->createJoint(e_hinge,
			spBodyA, spBodyC);
		spJoint_b->setAnchor(Vector3(2.0,0.0,1.0));
		spJoint_b->setAxis(Vector3(0.0,1.0,0.0));

		sp<JointI> spJoint_c = spSim->createJoint(e_hinge,
			spBodyC, spBodyB);
		spJoint_c->setAnchor(Vector3(2.0,0.0,-1.0));
		spJoint_c->setAxis(Vector3(0.0,1.0,0.0));

		sp<JointI> spJoint_d = spSim->createJoint(e_hinge,
			spBodyB, sp<Body>());
		spJoint_d->setAnchor(Vector3(0.0,0.0,-1.0));
		spJoint_d->setAxis(Vector3(0.0,1.0,0.0));

		for(int i = 0; i < 10; i++)
		{
			spSim->step(0.1);

			Vector3 pos;
			spBodyC->getPosition(pos);
			feLog("position %g %g %g\n", pos[0], pos[1], pos[2]);
		}

		fprintf(stderr, "++++++++++++++++++++++++++\n\n");

		sp<Body> body(spBodyC);
		dMatrix3 R;
		dRFrom2Axes(R, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0);
		dBodySetRotation(body->id(), R);
		dVector3 rv;
		dBodyVectorToWorld(body->id(), 1.0, 0.0, 0.0, rv);
		fprintf(stderr, "%f %f %f\n", rv[0], rv[1], rv[2]);

		SpatialTransform xform;
		Vector<3,Real> from(1.0,0.0,0.0);
		Vector<3,Real> to(1.0,1.0,1.0);
		normalize(from);
		normalize(to);
		set(xform, from, to);
		Vector<3,Real> in(1.0,0.0,0.0);
		Vector<3,Real> out;
		transformVector(xform, in, out);
		fprintf(stderr, "%f %f %f\n", out[0], out[1], out[2]);


#if 0
		sp<ApplicationI> spApplication
			= spMaster->registry()->create("ApplicationI.Configured");

		spApplication->setup(argc, argv, env);

		int &run_level = spMaster->catalog<int>("run:level");

		int r;
		spApplication->loop(r);
#endif

		{
#define _I(i,j) I[(i)*4+(j)]
			Mass mass_a;
			Quaternion<Real> q;
			Vector<3,Real> axis(0.0, 1.0, 0.0);
			set(q, 1.5, axis);
			SpatialTransform rot;
			//rotate(rot, 1.5, e_yAxis);
			rot = q;

			fprintf(stderr, "aa\n");
			fprint(stderr, rot);
			fprintf(stderr, "\n");

			mass_a.cylinder(e_xAxis, 1.0, 1.0, 10.0);
			mass_a.translate(SpatialVector(1.0,2.0,3.0));
			mass_a.rotate(rot);
			mass_a.translate(SpatialVector(1.0,2.0,3.0));
			fprintf(stderr, "AAAAAAAA\n");
			fprint(stderr, mass_a.I());
			fprintf(stderr, "\n");
			fprintf(stderr, "CG %f %f %f\n", mass_a.cg()[0], mass_a.cg()[1], mass_a.cg()[2]);


			mass_a.cylinder(e_xAxis, 1.0, 1.0, 10.0);
			dMass mass;
			mass.mass = mass_a.mass();
			for(int i = 0; i < 3; i++)
			{
				mass.c[i] = mass_a.cg()[i];
				for(int j = 0; j < 3; j++)
				{
					mass._I(i,j) = mass_a.I()(i,j);
				}
			}

			dMatrix3 R;
			dRFromAxisAndAngle(R, 0.0, 1.0, 0.0, 1.5);
			rot(0,0) = R[0];
			rot(0,1) = R[1];
			rot(0,2) = R[2];
			rot(1,0) = R[4];
			rot(1,1) = R[5];
			rot(1,2) = R[6];
			rot(2,0) = R[8];
			rot(2,1) = R[9];
			rot(2,2) = R[10];
			fprintf(stderr, "bb\n");
			fprint(stderr, rot);
			fprintf(stderr, "\n");
			dMassTranslate(&mass, 1.0, 2.0, 3.0);
			dMassRotate(&mass, R);
			dMassTranslate(&mass, 1.0, 2.0, 3.0);

			mass_a.mass() = mass.mass;
			for(int i = 0; i < 3; i++)
			{
				mass_a.cg()[i] = mass.c[i];
				for(int j = 0; j < 3; j++)
				{
					mass_a.I()(i,j) = mass._I(i,j);
				}
			}
			fprintf(stderr, "BBBBBBBB\n");
			fprint(stderr, mass_a.I());
			fprintf(stderr, "\n");
			fprintf(stderr, "CG %f %f %f\n", mass_a.cg()[0], mass_a.cg()[1], mass_a.cg()[2]);
		}

	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(std::exception &e)
	{
		fprintf(stderr,"%s\n", e.what());
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

}

