import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexOperatorDLLib",
                "fexDataToolDLLib",
                "fexSignalLib" ]

    # disabled this test due to ms_spMaster not linking right in spite
    # of FE_DL_PUBLIC usage.
    #tests = [ 'xOperator' ]

    #for t in tests:
    #   exe = module.Exe(t)
    #   forge.deps([t + "Exe"], deplibs)

    #forge.tests += [
    #   ("xOperator.exe",       "",                 None,       None) ]


