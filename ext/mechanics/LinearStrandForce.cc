/*	Copyright (C) 2003-2014 Free Electron Organization
	Any use of this software requires a license. */

/** @file */

#include "LinearStrandForce.h"

namespace fe
{
namespace ext
{

LinearStrandForce::LinearStrandForce(sp<SemiImplicit> a_integrator)
{
	m_integrator = a_integrator;
}

LinearStrandForce::~LinearStrandForce(void)
{
}

void LinearStrandForce::clear(void)
{
}

void LinearStrandForce::compile(sp<RecordGroup> rg_input, std::vector<SemiImplicit::Particle> &a_particles, SemiImplicit::CompileMatrix &a_compileMatrix)
{
	AsStrand			asLinearStrand;
	AsForcePoint		asForcePoint;
	AsSolverParticle	asSolverParticle;

	m_elements.clear();
	unsigned int n = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearStrand.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());

		if(asLinearStrand.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				// internal pivot is C, interface pivot is more intuitively B
				WeakRecord &r_A = asLinearStrand.a(spRA,i);
				WeakRecord &r_B = asLinearStrand.c(spRA,i);
				WeakRecord &r_C = asLinearStrand.b(spRA,i);

				if(!asForcePoint.check(r_A)) { continue; }
				if(!asForcePoint.check(r_B)) { continue; }
				if(!asForcePoint.check(r_C)) { continue; }

				n += 1;
			}
		}
	}

	m_elements.resize(n);
	unsigned int ii = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearStrand.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());
		asSolverParticle.bind(spRA->layout()->scope());

		if(asLinearStrand.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Element &element = m_elements[ii];
				//
				// internal pivot is C, interface pivot is more intuitively B
				WeakRecord &r_A = asLinearStrand.a(spRA,i);
				WeakRecord &r_B = asLinearStrand.c(spRA,i);
				WeakRecord &r_C = asLinearStrand.b(spRA,i);

				if(!asForcePoint.check(r_A)) { continue; }
				if(!asForcePoint.check(r_B)) { continue; }
				if(!asForcePoint.check(r_C)) { continue; }

				ii++;

				t_solve_real stiffness = asLinearStrand.stiffness(spRA,i);
				t_solve_real damping = asLinearStrand.damping(spRA,i);

				t_solve_v3 P_AC = asForcePoint.location(r_A)
					- asForcePoint.location(r_C);
				t_solve_v3 P_BC = asForcePoint.location(r_B)
					- asForcePoint.location(r_C);

				t_solve_real LAC = magnitude(P_AC);
				t_solve_real LBC = magnitude(P_BC);
				t_solve_real LACB = LAC + LBC;

				element.m_alpha_A = LBC/LACB;
				element.m_alpha_B = LAC/LACB;
				element.m_alpha_C = -1.0;

				t_solve_real ACBC = LBC * LAC;
				element.m_lambda = stiffness * (LAC + LBC) / (ACBC * ACBC);
				element.m_gamma = damping * (LAC + LBC) / (ACBC * ACBC);

				t_solve_v3 rest_R =
										element.m_alpha_A * asForcePoint.location(r_A)
						+				element.m_alpha_B * asForcePoint.location(r_B)
						+				element.m_alpha_C * asForcePoint.location(r_C);

				element.m_length = magnitude(rest_R);

				t_solve_matrix common;
				setIdentity(common);
				common *= -element.m_lambda;

				element.m_dfdxAA = common * (element.m_alpha_A * element.m_alpha_A);
				element.m_dfdxAB = common * (element.m_alpha_A * element.m_alpha_B);
				element.m_dfdxAC = common * (element.m_alpha_A * element.m_alpha_C);
				element.m_dfdxBB = common * (element.m_alpha_B * element.m_alpha_B);
				element.m_dfdxBC = common * (element.m_alpha_B * element.m_alpha_C);
				element.m_dfdxCC = common * (element.m_alpha_C * element.m_alpha_C);

				setIdentity(common);
				common *= -element.m_gamma;

				element.m_dfdvAA = common * (element.m_alpha_A * element.m_alpha_A);
				element.m_dfdvAB = common * (element.m_alpha_A * element.m_alpha_B);
				element.m_dfdvAC = common * (element.m_alpha_A * element.m_alpha_C);
				element.m_dfdvBB = common * (element.m_alpha_B * element.m_alpha_B);
				element.m_dfdvBC = common * (element.m_alpha_B * element.m_alpha_C);
				element.m_dfdvCC = common * (element.m_alpha_C * element.m_alpha_C);

				int i_A = asSolverParticle.index(r_A);
				int i_B = asSolverParticle.index(r_B);
				int i_C = asSolverParticle.index(r_C);

				element.m_location[0] = &(a_particles[i_A].m_location);
				element.m_location[1] = &(a_particles[i_B].m_location);
				element.m_location[2] = &(a_particles[i_C].m_location);

				element.m_velocity[0] = &(a_particles[i_A].m_velocity);
				element.m_velocity[1] = &(a_particles[i_B].m_velocity);
				element.m_velocity[2] = &(a_particles[i_C].m_velocity);

				element.m_force[0] = &(a_particles[i_A].m_force);
				element.m_force[1] = &(a_particles[i_B].m_force);
				element.m_force[2] = &(a_particles[i_C].m_force);

				a_compileMatrix.entry(i_A, i_A).first
					.push_back(&(element.m_p_dfdxAA));
				a_compileMatrix.entry(i_B, i_B).first
					.push_back(&(element.m_p_dfdxBB));
				a_compileMatrix.entry(i_C, i_C).first
					.push_back(&(element.m_p_dfdxCC));

				if(i_A < i_B)
				{
					a_compileMatrix.entry(i_A, i_B).first
						.push_back(&(element.m_p_dfdxAB));
				}
				else
				{
					a_compileMatrix.entry(i_B, i_A).first
						.push_back(&(element.m_p_dfdxAB));
				}

				if(i_A < i_C)
				{
					a_compileMatrix.entry(i_A, i_C).first
						.push_back(&(element.m_p_dfdxAC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_A).first
						.push_back(&(element.m_p_dfdxAC));
				}

				if(i_B < i_C)
				{
					a_compileMatrix.entry(i_B, i_C).first
						.push_back(&(element.m_p_dfdxBC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_B).first
						.push_back(&(element.m_p_dfdxBC));
				}


				a_compileMatrix.entry(i_A, i_A).second
					.push_back(&(element.m_p_dfdvAA));
				a_compileMatrix.entry(i_B, i_B).second
					.push_back(&(element.m_p_dfdvBB));
				a_compileMatrix.entry(i_C, i_C).second
					.push_back(&(element.m_p_dfdvCC));

				if(i_A < i_B)
				{
					a_compileMatrix.entry(i_A, i_B).second
						.push_back(&(element.m_p_dfdvAB));
				}
				else
				{
					a_compileMatrix.entry(i_B, i_A).second
						.push_back(&(element.m_p_dfdvAB));
				}

				if(i_A < i_C)
				{
					a_compileMatrix.entry(i_A, i_C).second
						.push_back(&(element.m_p_dfdvAC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_A).second
						.push_back(&(element.m_p_dfdvAC));
				}


				if(i_B < i_C)
				{
					a_compileMatrix.entry(i_B, i_C).second
						.push_back(&(element.m_p_dfdvBC));
				}
				else
				{
					a_compileMatrix.entry(i_C, i_B).second
						.push_back(&(element.m_p_dfdvBC));
				}
			}
		}
	}
}

bool LinearStrandForce::validate(void)
{
	return true;
}

void LinearStrandForce::accumulate(void)
{
	unsigned int n = (unsigned int)m_elements.size();
	for(unsigned int i = 0; i < n; i++)
	{
		Element &element = m_elements[i];

		// calculate R
		t_solve_v3 R =		element.m_alpha_A * *(element.m_location[0])
			+				element.m_alpha_B * *(element.m_location[1])
			+				element.m_alpha_C * *(element.m_location[2]);

		if(isZero(R))
		{
			continue;
		}

		// adjust R by rest R
		t_solve_real r = magnitude(R);
		if(isZero(R))
		{
			continue;
		}

		//normalize(R);
		R *= 1.0/r;
		t_solve_v3 Rn = R;

		r -= element.m_length;
		R *= r;

		// calculate dx forces
		*(element.m_force[0]) -= element.m_lambda * element.m_alpha_A * R;
		*(element.m_force[1]) -= element.m_lambda * element.m_alpha_B * R;
		*(element.m_force[2]) -= element.m_lambda * element.m_alpha_C * R;

		// calculate Rp
		t_solve_v3 Rp =		element.m_alpha_A * *(element.m_velocity[0])
			+				element.m_alpha_B * *(element.m_velocity[1])
			+				element.m_alpha_C * *(element.m_velocity[2]);
		Rp = dot(Rp, Rn) * Rn;

		// calculate dv forces
		*(element.m_force[0]) -= element.m_gamma * element.m_alpha_A * Rp;
		*(element.m_force[1]) -= element.m_gamma * element.m_alpha_B * Rp;
		*(element.m_force[2]) -= element.m_gamma * element.m_alpha_C * Rp;

		// the Jacobians
		add(*(element.m_p_dfdxAA), *(element.m_p_dfdxAA), element.m_dfdxAA);
		add(*(element.m_p_dfdxBB), *(element.m_p_dfdxBB), element.m_dfdxBB);
		add(*(element.m_p_dfdxCC), *(element.m_p_dfdxCC), element.m_dfdxCC);
		add(*(element.m_p_dfdxAB), *(element.m_p_dfdxAB), element.m_dfdxAB);
		add(*(element.m_p_dfdxAC), *(element.m_p_dfdxAC), element.m_dfdxAC);
		add(*(element.m_p_dfdxBC), *(element.m_p_dfdxBC), element.m_dfdxBC);

		add(*(element.m_p_dfdvAA), *(element.m_p_dfdvAA), element.m_dfdvAA);
		add(*(element.m_p_dfdvBB), *(element.m_p_dfdvBB), element.m_dfdvBB);
		add(*(element.m_p_dfdvCC), *(element.m_p_dfdvCC), element.m_dfdvCC);
		add(*(element.m_p_dfdvAB), *(element.m_p_dfdvAB), element.m_dfdvAB);
		add(*(element.m_p_dfdvAC), *(element.m_p_dfdvAC), element.m_dfdvAC);
		add(*(element.m_p_dfdvBC), *(element.m_p_dfdvBC), element.m_dfdvBC);
	}
}

} /* namespace */
} /* namespace */


