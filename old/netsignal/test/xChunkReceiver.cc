/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define LISTENQ		5
#define SERV_PORT	7876

//#define CLIENT_ROLE


void ClientRole();
void ServerRole();


//-----------------------------------------------------------------
//							MAIN()
//-----------------------------------------------------------------
int main(void)
{
	fe::UnitTest unitTest;
#ifdef CLIENT_ROLE
	ClientRole();
#else
	ServerRole();
#endif
	return unitTest.failures();
}


//-----------------------------------------------------------------
// ServerRole()
//-----------------------------------------------------------------
void ServerRole()
{
	try
	{
		fe::sp<fe::ext::Socket>		listener(new fe::ext::Socket);
		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;
		fe::ext::SockAddr			client_addr;

		listener->openStream();
		server_addr.setPort(SERV_PORT);
		listener->bind(server_addr);
		listener->listen(LISTENQ);

		connection = listener->accept(client_addr);

		fe::sp<fe::ext::ChunkSpool> spBuffer(new fe::ext::ChunkSpool);
		fe::sp<fe::ext::ChunkReceiver> spReceiver(new fe::ext::ChunkReceiver);

		spReceiver->start(spBuffer, connection);

		while(true)
		{
			fe::sp<fe::ext::Chunk> spChunk;
			spChunk = spBuffer->wait();
			feLogGroup("netsignal","received chunk of size %u\n", spChunk->size());
		}
		spReceiver->stop();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
}

//-----------------------------------------------------------------
// ClientRole()
//-----------------------------------------------------------------
void ClientRole()
{
	try
	{
		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

		server_addr.setPort(SERV_PORT);
		server_addr.setAddress("127.0.0.1");

		connection->openStream();
		connection->connect(server_addr);

		fe::sp<fe::ext::ChunkSpool> spBuffer(new fe::ext::ChunkSpool);
		fe::sp<fe::ext::ChunkReceiver> spReceiver(new fe::ext::ChunkReceiver);

		spReceiver->start(spBuffer, connection);

		while(true)
		{
			fe::sp<fe::ext::Chunk> spChunk;
			spChunk = spBuffer->wait();
			feLogGroup("netsignal","received chunk of size %u\n", spChunk->size());
		}
		spReceiver->stop();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
}



