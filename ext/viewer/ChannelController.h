/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer__ChannelController_h__
#define __viewer__ChannelController_h__

namespace fe
{
namespace ext
{

/**	Channel controller

	@copydoc ChannelController_info
	*/
class FE_DL_EXPORT ChannelController :
	virtual public HandlerI,
	virtual public Mask,
	public Initialize<ChannelController>
{
	public:
				ChannelController(void);
virtual			~ChannelController(void);

		void	initialize(void);

		// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void	handle(Record &r_signal);

	private:
		AsSignal			m_asSignal;
		WindowEvent::Mask	m_key_press;
		WindowEvent::Mask	m_key_release;

};

} /* namespace ext */
} /* namespace fe */

#endif
