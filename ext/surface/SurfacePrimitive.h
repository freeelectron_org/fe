/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfacePrimitive_h__
#define __surface_SurfacePrimitive_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SurfacePrimitive RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfacePrimitive:
	virtual public Recordable,
	public CastableAs<SurfacePrimitive>
{
	public:
		Functor< Array<I32> >		points;
		Functor< I32 >				properties;
		Functor< String >			part;

				SurfacePrimitive(void)			{ setName("SurfacePrimitive"); }
virtual	void	addFunctors(void)
				{
					Recordable::addFunctors();

					add(points,		FE_SPEC("surf:points",
							"Points in a primitive"));
					add(properties,	FE_SPEC("properties",
							"Bitwise properties of a primitive"));
					add(part,		FE_SPEC("part",
							"Part name of a primitive"));
				}
virtual	void	initializeRecord(void)
				{
					Recordable::initializeRecord();

					properties()=0;
				}
virtual	void	finalizeRecord(void)
				{
					Recordable::finalizeRecord();
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfacePrimitive_h__ */


