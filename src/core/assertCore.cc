/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

IWORD InfoU8::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		U8 u8= *(U8 *)instance;
		ostrm.write((char *)&u8, sizeof(U8));
		return sizeof(U8);
	}
	else
	{
		U8 u8= *(U8 *)instance;
		ostrm << std::dec << U16(u8);
		return c_ascii;
	}
}

void InfoU8::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		U8 u8;
		istrm.read((char *)&u8, sizeof(U8));
		*(U8 *)instance = u8;
	}
	else
	{
		U16 u16;
		istrm >> u16;
		*(U8 *)instance = u16;
	}
}

IWORD InfoU16::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		U16 u16 = htons(*(U16 *)instance);
		ostrm.write((char *)&u16, sizeof(U16));
		return sizeof(U16);
	}
	else
	{
		U16 u16= *(U16 *)instance;
		ostrm << std::dec << u16;
		return c_ascii;
	}
}

void InfoU16::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	U16 u16;
	if(mode == e_binary)
	{
		istrm.read((char *)&u16, sizeof(U16));
		*(U16 *)instance = ntohs(u16);
	}
	else
	{
		istrm >> u16;
		*(U16 *)instance = u16;
	}
}

IWORD InfoI16::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		I16 i16 = htons(*(I16 *)instance);
		ostrm.write((char *)&i16, sizeof(I16));
		return sizeof(I16);
	}
	else
	{
		I16 i16= *(I16 *)instance;
		ostrm << std::dec << i16;
		return c_ascii;
	}
}

void InfoI16::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	I16 i16;
	if(mode == e_binary)
	{
		istrm.read((char *)&i16, sizeof(I16));
		*(I16 *)instance = ntohs(i16);
	}
	else
	{
		istrm >> i16;
		*(I16 *)instance = i16;
	}
}

IWORD InfoU32::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		U32 u32 = htonl(*(U32 *)instance);
		ostrm.write((char *)&u32, sizeof(U32));
		return sizeof(U32);
	}
	else
	{
		U32 u32= *(U32 *)instance;
		ostrm << std::dec << u32;
		return c_ascii;
	}
}

void InfoU32::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	U32 u32;
	if(mode == e_binary)
	{
		istrm.read((char *)&u32, sizeof(U32));
		*(U32 *)instance = ntohl(u32);
	}
	else
	{
		istrm >> u32;
		*(U32 *)instance = u32;
	}
}

IWORD InfoI32::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		I32 i32 = htonl(*(I32 *)instance);
		ostrm.write((char *)&i32, sizeof(I32));
		return sizeof(I32);
	}
	else
	{
		I32 i32= *(I32 *)instance;
		ostrm << std::dec << i32;
		return c_ascii;
	}
}

void InfoI32::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	I32 i32;
	if(mode == e_binary)
	{
		istrm.read((char *)&i32, sizeof(I32));
		*(I32 *)instance = ntohl(i32);
	}
	else
	{
		istrm >> i32;
		*(I32 *)instance = i32;
	}
}

String InfoU64::print(void *instance)
{
	U64 value= *(U64 *)instance;
	String string;
	string.sPrintf("%llu",value);
	return string;
}

String InfoI64::print(void *instance)
{
	I64 value= *(I64 *)instance;
	String string;
	string.sPrintf("%lld",value);
	return string;
}

IWORD InfoIWORD::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		IWORD iword = htonl(*(IWORD *)instance);
		ostrm.write((char *)&iword, sizeof(IWORD));
		return sizeof(IWORD);
	}
	else
	{
		IWORD iword= *(IWORD *)instance;
		ostrm << std::dec << iword;
		return c_ascii;
	}
}

void InfoIWORD::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	IWORD iword;
	if(mode == e_binary)
	{
		istrm.read((char *)&iword, sizeof(IWORD));
		*(IWORD *)instance = ntohl(iword);
	}
	else
	{
		istrm >> iword;
		*(IWORD *)instance = iword;
	}
}

// ----------------------------------------------------------------------

String InfoF32::print(void *instance)
{
	F32 f32= *(F32 *)instance;
	String string;
	string.sPrintf("%.6G",f32);
	return string;
}

IWORD InfoF32::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	F32 f32= *(F32 *)instance;
	if(mode == e_binary)
	{
		ostrm.write((char *)&f32, sizeof(F32));
		return sizeof(F32);
	}
	else
	{
		ostrm << std::setprecision(7)<< f32;
		return c_ascii;
	}
}

void InfoF32::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	F32 f32=0.0f;
	if(mode == e_binary)
	{
		istrm.read((char *)&f32, sizeof(F32));
	}
	else
	{
		istrm >> f32;
	}
	*(F32 *)instance = f32;
}

void InfoF32::construct(void *instance)
{
	*(F32 *)instance=0.0f;
}

IWORD InfoF32::iosize(void)
{
	return sizeof(F32);
}

// ----------------------------------------------------------------------

String InfoF64::print(void *instance)
{
	F64 f64= *(F64 *)instance;
	String string;
	string.sPrintf("%.15G",f64);
	return string;
}

IWORD InfoF64::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	F64 f64= *(F64 *)instance;
	if(mode == e_binary)
	{
		ostrm.write((char *)&f64, sizeof(F64));
		return sizeof(F64);
	}
	else
	{
		ostrm << std::setprecision(15)<< f64;
		return c_ascii;
	}
}

void InfoF64::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	F64 f64=0.0f;
	if(mode == e_binary)
	{
		istrm.read((char *)&f64, sizeof(F64));
	}
	else
	{
		istrm >> f64;
	}
	*(F64 *)instance = f64;
}

void InfoF64::construct(void *instance)
{
	*(F64 *)instance=0.0;
}

IWORD InfoF64::iosize(void)
{
	return sizeof(F64);
}

// ----------------------------------------------------------------------

class InfoString : public TypeInfoConstructable<String>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						String string= *(String *)instance;
						I32 n = htonl(string.length());
						ostrm.write((char *)&n, sizeof(I32));
						ostrm.write(string.c_str(), string.length());
						return sizeof(I32) + string.length();
					}
					else
					{
						String string= *(String *)instance;
						string=string.substitute("\"","\\\"");
						ostrm << "\"" << string.c_str() << "\"";
						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						I32 n;
						istrm.read((char *)&n, sizeof(I32));
						n = ntohl(n);
						char buffer[FESTRING_LOCALSIZE];
						char *ptr;
						if(n >= FESTRING_LOCALSIZE)
						{
							ptr = new char[n+1];
						}
						else
						{
							ptr = buffer;
						}
						istrm.read(ptr, n);
						ptr[n] = '\0';
						*(String *)instance = ptr;
						if(ptr != buffer) { delete[] ptr; }
					}
					else
					{
						std::string s;
						char c;
						while(istrm.get(c))
						{
							if(c == '\0')
							{
								break;
							}
							s.push_back(c);
						}
						*(String *)instance = String(s.c_str())
								.maybeUnQuote().substitute("\\\"","\"");
					}
				}
virtual	BWORD	copy(void *instance,const void* source)
				{
					String& rInstance=*(String *)instance;
					String& rSource=*(String *)source;
					rInstance=rSource;
					return TRUE;
				}
virtual	String	print(void *instance)
				{	return *(String *)instance; }
virtual	IWORD	iosize(void)
				{	return BaseType::Info::c_implicit; }
};

typedef std::map<String,String> t_mapStrStr;
class FE_DL_EXPORT InfoStringStringMap:
		public TypeInfoConstructable<t_mapStrStr>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					t_mapStrStr &map = *(t_mapStrStr *)instance;

					if(mode == e_binary)
					{
						U32 bytes=0;
						I32 size = htonl(map.size());
						ostrm.write((char *)&size, sizeof(I32));
						bytes+=sizeof(I32);

						t_mapStrStr::const_iterator it=map.begin();
						while(it!=map.end())
						{
							String key=it->first;
							size = htonl(key.length());
							ostrm.write((char *)&size, sizeof(I32));
							bytes+=sizeof(I32);

							ostrm.write(key.c_str(), key.length());
							bytes+=key.length();

							String value=it->second;
							size = htonl(value.length());
							ostrm.write((char *)&size, sizeof(I32));
							bytes+=sizeof(I32);

							ostrm.write(value.c_str(), value.length());
							bytes+=value.length();

							it++;
						}

						return bytes;
					}
					else
					{
						t_mapStrStr::const_iterator it=map.begin();
						if(it==map.end())
						{
							ostrm << "\"\"";
							return c_ascii;
						}
						ostrm << "\"";
						while(it!=map.end())
						{
							if(it!=map.begin())
							{
								ostrm << "  ";
							}
							ostrm << it->first.maybeQuote("'").c_str();
							ostrm << " ";
							ostrm << it->second.maybeQuote("'").c_str();

							it++;
						}
						ostrm << "\"";

						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					t_mapStrStr &map = *(t_mapStrStr *)instance;

					if(mode == e_binary)
					{
						I32 size;
						istrm.read((char *)&size, sizeof(I32));
						size = ntohl(size);

						for(I32 index=0;index<size;index++)
						{
							String key;
							for(U32 pass=0;pass<2;pass++)
							{
								I32 n;
								istrm.read((char *)&n, sizeof(I32));
								n = ntohl(n);
								char buffer[FESTRING_LOCALSIZE];
								char *ptr;
								if(n >= FESTRING_LOCALSIZE)
								{
									ptr = new char[n+1];
								}
								else
								{
									ptr = buffer;
								}
								istrm.read(ptr, n);
								ptr[n] = '\0';

								if(pass)
								{
									map[key]=ptr;
								}
								else
								{
									key=ptr;
								}
								if(ptr != buffer)
								{
									delete [] ptr;
								}
							}
						}
					}
					else
					{
						std::string s;
						char c;
						while(istrm.get(c))
						{
							if(c == '\0')
							{
								break;
							}
							s.push_back(c);
						}
						String buffer = s.c_str();
						buffer=buffer.prechop("\"").chop("\"");

						while(!buffer.empty())
						{
							String key=buffer.parse("'");
							if(key.empty())
							{
								break;
							}
							String value=buffer.parse("'");
							map[key]=value;
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					t_mapStrStr &map = *(t_mapStrStr *)instance;

					t_mapStrStr::const_iterator it=map.begin();
					while(it!=map.end())
					{
						s.cat("'");
						s.cat(it->first);
						s.cat("' '");
						s.cat(it->second);
						s.cat("'\n");

						it++;
					}

					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

typedef Array<String> t_vecstring;
class FE_DL_EXPORT InfoStringArray:
	public TypeInfoArray<t_vecstring>,
	public CastableAs<InfoStringArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					t_vecstring &v = *(t_vecstring *)instance;
					if(mode == e_binary)
					{
						U32 bytes=0;
						long cnt = (long)v.size();

						I32 n = htonl(cnt);
						ostrm.write((char *)&n, sizeof(I32));
						bytes+=sizeof(I32);

						for(long i = 0; i < cnt; i++)
						{
							String word=v[i];
							I32 len = htonl(word.length());
							ostrm.write((char *)&len, sizeof(I32));
							bytes+=sizeof(I32);

							ostrm.write(word.c_str(), word.length());
							bytes+=word.length();
						}
						return bytes;
					}
					else
					{
						long cnt = (long)v.size();
						if(!cnt)
						{
							ostrm << "\"\"";
							return c_ascii;
						}

						ostrm << "\"";
						for(long i = 0; i < cnt; i++)
						{
							if(i)
							{
								ostrm << " ";
							}
							ostrm << v[i].maybeQuote("'").c_str();
						}
						ostrm << "\"";

						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					t_vecstring &v = *(t_vecstring *)instance;

					if(mode == e_binary)
					{
						I32 cnt=0;
						istrm.read((char *)&cnt, sizeof(I32));
						cnt = ntohl(cnt);

						v.resize(cnt);
						for(I32 i = 0; i < cnt; i++)
						{
							I32 len;
							istrm.read((char *)&len, sizeof(I32));
							len = ntohl(len);
							char buffer[FESTRING_LOCALSIZE];
							char *ptr;
							if(len >= FESTRING_LOCALSIZE)
							{
								ptr = new char[len+1];
							}
							else
							{
								ptr = buffer;
							}
							istrm.read(ptr, len);
							ptr[len] = '\0';

							v[i]=ptr;

							if(ptr != buffer)
							{
								delete[] ptr;
							}
						}
					}
					else
					{
						std::string s;
						char c;
						while(istrm.get(c))
						{
							if(c == '\0')
							{
								break;
							}
							s.push_back(c);
						}
						String buffer = s.c_str();
						buffer=buffer.prechop("\"").chop("\"");

						unsigned int cnt=0;
						while(!buffer.empty())
						{
							String word=buffer.parse("'");
							if(!word.empty())
							{
								v.resize(cnt+1);
								v[cnt++]=word;
							}
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					t_vecstring &v = *(t_vecstring *)instance;
					const I32 count = v.size();
					const I32 max = 32;
					s.sPrintf("%d:", count);
					for(unsigned int i = 0; i < v.size(); i++)
					{
						s.catf(" '%s'", v[i].c_str());
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

typedef std::deque<String> t_dequestring;
class FE_DL_EXPORT InfoStringDeque :
	public TypeInfoConstructable<t_dequestring>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					t_dequestring &v = *(t_dequestring *)instance;
					if(mode == e_binary)
					{
						U32 bytes=0;
						long cnt = (long)v.size();

						I32 n = htonl(cnt);
						ostrm.write((char *)&n, sizeof(I32));
						bytes+=sizeof(I32);

						for(long i = 0; i < cnt; i++)
						{
							String word=v[i];
							I32 len = htonl(word.length());
							ostrm.write((char *)&len, sizeof(I32));
							bytes+=sizeof(I32);

							ostrm.write(word.c_str(), word.length());
							bytes+=word.length();
						}
						return bytes;
					}
					else
					{
						long cnt = (long)v.size();
						if(!cnt)
						{
							ostrm << "\"\"";
							return c_ascii;
						}

						ostrm << "\"";
						for(long i = 0; i < cnt; i++)
						{
							if(i)
							{
								ostrm << " ";
							}
							ostrm << v[i].maybeQuote("'").c_str();
						}
						ostrm << "\"";

						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					t_dequestring &v = *(t_dequestring *)instance;

					if(mode == e_binary)
					{
						I32 cnt=0;
						istrm.read((char *)&cnt, sizeof(I32));
						cnt = ntohl(cnt);

						v.resize(cnt);
						for(I32 i = 0; i < cnt; i++)
						{
							I32 len;
							istrm.read((char *)&len, sizeof(I32));
							len = ntohl(len);
							char buffer[FESTRING_LOCALSIZE];
							char *ptr;
							if(len >= FESTRING_LOCALSIZE)
							{
								ptr = new char[len+1];
							}
							else
							{
								ptr = buffer;
							}
							istrm.read(ptr, len);
							ptr[len] = '\0';

							v[i]=ptr;

							if(ptr != buffer)
							{
								delete[] ptr;
							}
						}
					}
					else
					{
						std::string s;
						char c;
						while(istrm.get(c))
						{
							if(c == '\0')
							{
								break;
							}
							s.push_back(c);
						}
						String buffer = s.c_str();
						buffer=buffer.prechop("\"").chop("\"");

						unsigned int cnt=0;
						while(!buffer.empty())
						{
							String word=buffer.parse("'");
							feLog("@%s@\n",word.c_str());
							if(!word.empty())
							{
								v.resize(cnt+1);
								v[cnt++]=word;
							}
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					t_dequestring &v = *(t_dequestring *)instance;
					const I32 count = v.size();
					const I32 max = 32;
					s.sPrintf("%d:", count);
					for(unsigned int i = 0; i < v.size(); i++)
					{
						s.catf(" '%s'", v[i].c_str());
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

//* NOTE ascii mode uses hex
class FE_DL_EXPORT InfoByteArray:
	public TypeInfoArray< Array<U8> >,
	public CastableAs<InfoByteArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						Array<U8> &v = *(Array<U8> *)instance;
						I32 cnt = (I32)v.size();
						I32 n = htonl(cnt);

						ostrm.write((char *)&n, sizeof(I32));
						ostrm.write((char *)v.data(), cnt);
						return sizeof(I32) + cnt * sizeof(U8);
					}
					else
					{
						Array<U8> &v = *(Array<U8> *)instance;
						I32 cnt = (I32)v.size();
						ostrm << cnt;
						if(cnt)
						{
							ostrm << ",";
						}
						for(I32 i = 0; i < cnt; i++)
						{
							ostrm << std::hex << std::setfill('0')
									<< std::setw(2) << I32(v[i]) << std::dec;
						}
						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						I32 n=0;
						istrm.read((char *)&n, sizeof(I32));
						n = ntohl(n);

						Array<U8> &v = *(Array<U8> *)instance;
						v.resize(n);
						istrm.read((char *)v.data(), n);
					}
					else
					{
						I32 n=0;
						istrm >> n;
						Array<U8> &v = *(Array<U8> *)instance;
						v.resize(n);
						if(n)
						{
							char comma;
							istrm >> comma;
						}
						for(I32 i = 0; i < n; i++)
						{
							char buffer[3];
							istrm >> std::setw(3) >> buffer;

							I32 value=0;
							sscanf(buffer,"%02x",&value);
							v[i] = value;
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					Array<U8> &v = *(Array<U8> *)instance;
					const I32 count = v.size();
					const I32 max = 32;
					s.sPrintf("%d:", count);
					for(I32 i = 0; i < count && i < max; i++)
					{
						s.catf(" %02x", v[i]);
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class FE_DL_EXPORT InfoIntegerArray:
	public TypeInfoArray<t_vecint>,
	public CastableAs<InfoIntegerArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						t_vecint &v = *(t_vecint *)instance;
						long cnt = (long)v.size();
						I32 n = htonl(cnt);

						ostrm.write((char *)&n, sizeof(I32));
						for(long i = 0; i < cnt; i++)
						{
							I32 integer = (I32)v[i];
							ostrm.write((char *)&integer, sizeof(I32));
						}
						return sizeof(I32) + cnt * sizeof(I32);
					}
					else
					{
						t_vecint &v = *(t_vecint *)instance;
						long cnt = (long)v.size();
						ostrm << cnt;
						for(long i = 0; i < cnt; i++)
						{
							ostrm << ",";
							ostrm << v[i];
						}
						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						I32 n=0;
						istrm.read((char *)&n, sizeof(I32));
						n = ntohl(n);

						t_vecint &v = *(t_vecint *)instance;
						v.resize(n);
						for(I32 i = 0; i < n; i++)
						{
							I32 integer;
							istrm.read((char *)&integer, sizeof(I32));
							v[i] = integer;
						}
					}
					else
					{
						I32 n=0;
						istrm >> n;
						t_vecint &v = *(t_vecint *)instance;
						v.resize(n);
						for(I32 i = 0; i < n; i++)
						{
							char comma;
							istrm >> comma;
							I32 integer;
							istrm >> integer;
							v[i] = integer;
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					t_vecint &v = *(t_vecint *)instance;
					const I32 count = v.size();
					const I32 max = 32;
					s.sPrintf("%d:", count);
					for(I32 i = 0; i < count && i < max; i++)
					{
						s.catf(" %d", v[i]);
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class FE_DL_EXPORT InfoUnsignedArray:
	public TypeInfoArray<t_vecuint>,
	public CastableAs<InfoUnsignedArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						t_vecuint &v = *(t_vecuint *)instance;
						long cnt = (long)v.size();
						uint32_t n = htonl(cnt);

						ostrm.write((char *)&n, sizeof(U32));
						for(long i = 0; i < cnt; i++)
						{
							U32 integer = (U32)v[i];
							ostrm.write((char *)&integer, sizeof(U32));
						}
						return sizeof(U32) + cnt * sizeof(U32);
					}
					else
					{
						t_vecuint &v = *(t_vecuint *)instance;
						long cnt = (long)v.size();
						ostrm << cnt;
						for(long i = 0; i < cnt; i++)
						{
							ostrm << ",";
							ostrm << v[i];
						}
						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						uint32_t n=0;
						istrm.read((char *)&n, sizeof(U32));
						n = ntohl(n);

						t_vecuint &v = *(t_vecuint *)instance;
						v.resize(n);
						for(U32 i = 0; i < n; i++)
						{
							U32 integer;
							istrm.read((char *)&integer, sizeof(U32));
							v[i] = integer;
						}
					}
					else
					{
						uint32_t n=0;
						istrm >> n;
						t_vecuint &v = *(t_vecuint *)instance;
						v.resize(n);
						for(U32 i = 0; i < n; i++)
						{
							char comma;
							istrm >> comma;
							U32 integer;
							istrm >> integer;
							v[i] = integer;
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					t_vecuint &v = *(t_vecuint *)instance;
					const U32 count = v.size();
					const U32 max = 32;
					s.sPrintf("%d:", count);
					for(U32 i = 0; i < count && i < max; i++)
					{
						s.catf(" %d", v[i]);
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class FE_DL_EXPORT InfoRealArray:
	public TypeInfoArray<t_vecreal>,
	public CastableAs<InfoRealArray>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						t_vecreal &v = *(t_vecreal *)instance;
						long cnt = (long)v.size();
						I32 n = htonl(cnt);

						ostrm.write((char *)&n, sizeof(I32));
						for(long i = 0; i < cnt; i++)
						{
							F64 f64 = (F64)v[i];
							ostrm.write((char *)&f64, sizeof(F64));
						}
						return sizeof(I32) + cnt * sizeof(F64);
					}
					else
					{
						t_vecreal &v = *(t_vecreal *)instance;
						long cnt = (long)v.size();
						ostrm << cnt;
						for(long i = 0; i < cnt; i++)
						{
							ostrm << ",";
							ostrm << std::setprecision(7)<< v[i];
						}
						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						I32 n=0;
						istrm.read((char *)&n, sizeof(I32));
						n = ntohl(n);

						t_vecreal &v = *(t_vecreal *)instance;
						v.resize(n);
						for(I32 i = 0; i < n; i++)
						{
							F64 f64;
							istrm.read((char *)&f64, sizeof(F64));
							v[i] = f64;
						}
					}
					else
					{
						I32 n=0;
						istrm >> n;
						t_vecreal &v = *(t_vecreal *)instance;
						v.resize(n);
						for(I32 i = 0; i < n; i++)
						{
							char comma;
							istrm >> comma;
							F64 f64;
							istrm >> f64;
							v[i] = f64;
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					t_vecreal &v = *(t_vecreal *)instance;
					const I32 count = v.size();
					const I32 max = 32;
					s.sPrintf("%d:", count);
					for(I32 i = 0; i < count && i < max; i++)
					{
						s.catf(" %f", v[i]);
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};

class FE_DL_EXPORT InfoIntIntArray :
	public TypeInfoConstructable< Array< Array<I32> > >
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					IWORD n = 0;
					const Array< Array<I32> > *pA =
							(Array< Array<I32> > *)instance;
					const I32 cnt=(*pA).size();
					if(mode == e_ascii)
					{
						ostrm << '"' << cnt << ":";
					}
					else
					{
						const I32 n = htonl(cnt);
						ostrm.write((char *)&n, sizeof(I32));
					}
					for(int i = 0; i < cnt; i++)
					{
						const Array<I32>& rArray=(*pA)[i];
						if(i && mode == e_ascii)
						{
							ostrm << ' ';
						}
						n += helpIntArray.output(ostrm, (void *)&rArray, mode);
					}
					if(mode == e_ascii)
					{
						ostrm << '"';
					}
					return (mode == e_ascii)? c_ascii: n;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					I32 cnt(0);
					if(mode == e_ascii)
					{
						char colon;
						istrm >> cnt >> colon;
					}
					else
					{
						istrm.read((char *)&cnt, sizeof(I32));
						cnt = ntohl(cnt);
					}
					Array< Array<I32> > *pA =
							(Array< Array<I32> > *)instance;
					(*pA).resize(cnt);
					for(int i = 0; i < cnt; i++)
					{
						Array<I32>& rArray=(*pA)[i];
						helpIntArray.input(istrm, (void *)&rArray, mode);
					}
				}
virtual	String	print(void *instance)
				{
					const Array< Array<I32> > *pA =
							(Array< Array<I32> > *)instance;
					const I32 cnt=(*pA).size();
					const I32 max = 8;
					String s = "";
					s.sPrintf("%d:", cnt);
					for(I32 i = 0; i < cnt && i < max; i++)
					{
						const Array<I32>& rArray=(*pA)[i];
						s.catf(" <%s>",
								helpIntArray.print((void *)&rArray).c_str());
					}
					if(cnt > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{	return BaseType::Info::c_implicit; }
	private:
		InfoIntegerArray helpIntArray;
};

class FE_DL_EXPORT InfoIntegerSet:
	public TypeInfoConstructable< std::set<I32> >
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						std::set<I32> &set = *(std::set<I32> *)instance;
						long cnt = (long)set.size();
						I32 n = htonl(cnt);

						ostrm.write((char *)&n, sizeof(I32));

						for(I32 integer: set)
						{
							ostrm.write((char *)&integer, sizeof(I32));
						}
						return sizeof(I32) + cnt * sizeof(I32);
					}
					else
					{
						std::set<I32> &set = *(std::set<I32> *)instance;
						long cnt = (long)set.size();
						ostrm << cnt;

						for(I32 integer: set)
						{
							ostrm << ",";
							ostrm << integer;
						}
						return c_ascii;
					}
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						I32 n;
						istrm.read((char *)&n, sizeof(I32));
						n = ntohl(n);

						std::set<I32> &set = *(std::set<I32> *)instance;

						for(I32 i = 0; i < n; i++)
						{
							I32 integer;
							istrm.read((char *)&integer, sizeof(I32));
							set.insert(integer);
						}
					}
					else
					{
						I32 n;
						istrm >> n;
						std::set<I32> &set = *(std::set<I32> *)instance;

						for(I32 i = 0; i < n; i++)
						{
							char comma;
							istrm >> comma;
							I32 integer;
							istrm >> integer;
							set.insert(integer);
						}
					}
				}
virtual	String	print(void *instance)
				{
					String s = "";
					std::set<I32> &set = *(std::set<I32> *)instance;
					const I32 count = set.size();
					const I32 max = 32;
					s.sPrintf("%d:", count);

					I32 printed(0);
					for(I32 integer: set)
					{
						s.catf(" %d", integer);
						if(++printed>max)
						{
							break;
						}
					}
					if(count > max)
					{
						s.cat(" ...");
					}
					return s;
				}
virtual	IWORD	iosize(void)
				{
					return BaseType::Info::c_implicit;
				}
};


String InfoBool::print(void *instance)
{
	bool b= *(bool *)instance;
	String string;
	if(b)
	{
		string.sPrintf("true");
	}
	else
	{
		string.sPrintf("false");
	}
	return string;
}

IWORD InfoBool::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		U8 b;
		if(*(bool *)instance)
		{
			b = 1;
		}
		else
		{
			b = 0;
		}
		ostrm.write((char *)&b, sizeof(U8));
		return sizeof(U8);
	}
	else
	{
		bool b= *(bool *)instance;
		ostrm << b;
		return c_ascii;
	}
}

void InfoBool::input(std::istream &istrm, void *instance, t_serialMode mode)
{
	if(mode == e_binary)
	{
		U8 u8;
		istrm.read((char *)&u8, sizeof(U8));
		*(bool *)instance = (u8==1);
	}
	else
	{
		std::string s;
		istrm >> s;

		if (s == "0" || s == "false" || s == "FALSE" || s == "False")
		{
			*(bool *)instance = false;
		}
		else
		{
			*(bool *)instance = true;
		}
	}
}

void InfoBool::construct(void *instance)
{
	*(bool *)instance=false;
}

IWORD InfoBool::iosize(void)
{
	return sizeof(U8);
}

String InfoVoid::print(void *instance)
{
	String string;
	return string;
}

IWORD InfoVoid::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	return 0;
}

void InfoVoid::input(std::istream &istrm, void *instance, t_serialMode mode)
{
}

void InfoVoid::construct(void *instance)
{
}

IWORD InfoVoid::iosize(void)
{
	return 0;
}

String InfoVoidStar::print(void *instance)
{
	String string;
	return string;
}

IWORD InfoVoidStar::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	return 0;
}

void InfoVoidStar::input(std::istream &istrm, void *instance, t_serialMode mode)
{
}

void InfoVoidStar::construct(void *instance)
{
}

IWORD InfoVoidStar::iosize(void)
{
	return 0;
}

class InfoCounted : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					sp<Counted>* pspC = (sp<Counted>*)instance;
					return (*pspC).isValid()? "<valid>": "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					sp<Counted>* pspC = (sp<Counted>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					sp<Counted>* pspC = (sp<Counted>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)sp<Counted>; }
virtual	void	destruct(void *instance)
				{	((sp<Counted>*)instance)->~sp<Counted>();}
};

void assertCore(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spT;
	sp<BaseType::Info> spInfo;
	spT = spTypeMaster->assertType<U8>("U8");
	spT->setInfo(new InfoU8());
	spT = spTypeMaster->assertType<U16>("U16");
	spT->setInfo(new InfoU16());
	spT = spTypeMaster->assertType<I16>("I16");
	spT->setInfo(new InfoI16());
	spT = spTypeMaster->assertType<U32>("U32");
	spT->setInfo(new InfoU32());
	spT = spTypeMaster->assertType<I32>("I32");
	spT->setInfo(new InfoI32());
	spT = spTypeMaster->assertType<U64>("U64");
	spT->setInfo(new InfoU64());
	spT = spTypeMaster->assertType<I64>("I64");
	spT->setInfo(new InfoI64());
	spT = spTypeMaster->assertType<IWORD>("IWORD");
	spT->setInfo(new InfoIWORD());
	spT = spTypeMaster->assertType<F32>("F32");
	spT->setInfo(new InfoF32());
	spT = spTypeMaster->assertType<F64>("F64");
	spT->setInfo(new InfoF64());
	spT = spTypeMaster->assertType<String>("string");
	spT->setInfo(new InfoString());
	spT = spTypeMaster->assertType<void>("void");
	spT->setInfo(new InfoVoid());
	spT = spTypeMaster->assertType<void*>("voidstar");
	spT->setInfo(new InfoVoidStar());

	spT = spTypeMaster->assertType< std::map<String,String> >(
			"stringstringmap");
	spT->setInfo(new InfoStringStringMap());

	spT = spTypeMaster->assertType< Array<String> >("stringarray");
	spT->setInfo(new InfoStringArray());

	spT = spTypeMaster->assertType< std::deque<String> >("stringqueue");
	spT->setInfo(new InfoStringDeque());

	spT = spTypeMaster->assertType< Array<U8> >("bytearray");
	spT->setInfo(new InfoByteArray());

	spT = spTypeMaster->assertType< Array<I32> >("intarray");
	spT->setInfo(new InfoIntegerArray());

	spT = spTypeMaster->assertType< Array<U32> >("unsignedarray");
	spT->setInfo(new InfoUnsignedArray());

	spT = spTypeMaster->assertType< Array<Real> >("realarray");
	spT->setInfo(new InfoRealArray());

	spT = spTypeMaster->assertType< Array< Array<I32> > >(
			"intintarray");
	spT->setInfo(new InfoIntIntArray());

	spT = spTypeMaster->assertType< std::set<I32> >("intset");
	spT->setInfo(new InfoIntegerSet());

	// platform and language neutral names
	spT = spTypeMaster->assertType<U8>("unsigned char");
	spT = spTypeMaster->assertType<I32>("integer");
	spT = spTypeMaster->assertType< std::atomic<I32> >("atomic_integer");
	spT = spTypeMaster->assertType< std::atomic<Real> >("atomic_real");
	spT = spTypeMaster->assertType<Real>("real");
	spT = spTypeMaster->assertType<bool>("boolean");
	spT->setInfo(new InfoBool());

	// C/C++ type mappings
	spT = spTypeMaster->assertType<int>("int");
	spT = spTypeMaster->assertType<float>("float");
	spT = spTypeMaster->assertType<double>("double");
	spT = spTypeMaster->assertType<bool>("bool");
	spT = spTypeMaster->assertType<unsigned char>("unsigned char");

	spT = spTypeMaster->assertType< sp<Counted> >("Counted");
	spT->setInfo(new InfoCounted());

	// not sure we want this:
	//spT = spTypeMaster->assertType<Instance>("any");
}

} /* namespace */
