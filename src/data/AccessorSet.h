/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_AccessorSet_h__
#define __data_AccessorSet_h__

namespace fe
{

/** @brief Set of accessors

	@ingroup data
*/

class FE_DL_EXPORT AccessorSet : virtual public PopulateI,
		public CastableAs<AccessorSet>
{
	public:
						AccessorSet(void)
						{
							setActive(false);
						}
						AccessorSet(sp<Scope> spScope)
						{
							setActive(false);
							bind(spScope);
						}
virtual					~AccessorSet(void)
						{
							if(m_hpScope.isValid())
							{
								m_hpScope->removeAccessorSet(this);
							}
						}
virtual	FE_UWORD		size(void) const { return m_accessors.size(); }
virtual	BaseAccessor	*operator[](FE_UWORD index)
						{
							return m_accessors[index];
						}

		bool	operator==(const AccessorSet &a_aset) const
		{
			if(!m_hpScope.isValid()) { feX("AccessorSet::==", "invalid left scope"); }
			if(!a_aset.m_hpScope.isValid()) { feX("AccessorSet::==", "invalid right scope"); }

			if(m_hpScope != a_aset.m_hpScope) { return false; }

			return (m_bitset == a_aset.m_bitset);
		}

		bool	operator<(const AccessorSet &a_aset) const
		{
			if(!m_hpScope.isValid()) { feX("AccessorSet::<", "invalid left scope"); }
			if(!a_aset.m_hpScope.isValid()) { feX("AccessorSet::<", "invalid right scope"); }
			if(m_hpScope != a_aset.m_hpScope) { feX("AccessorSet::<", "scope mismatch"); }

			return (m_bitset < a_aset.m_bitset);
		}


		bool	bind(const WeakRecord a_record)
		{
			if(!a_record.isValid()) { return false; }
			bind(a_record.layout()->scope());
			bool rv = check(a_record);
			if(rv)
			{
				m_optional_record = a_record;
				for(unsigned int i = 0; i < m_accessors.size(); i++)
				{
					m_accessors[i]->bind(a_record);
				}
			}
			return rv;
		}
		void	bind(sp<Scope> spScope)
		{
			if(!spScope.isValid())
			{
				feX("AccessorSet::bind",
					"given invalid scope");
			}
			if(m_hpScope==spScope)
			{
				return;
			}
			if(m_hpScope.isValid())
			{
				feX("AccessorSet::bind","changed scope\n");
			}
			m_hpScope = spScope;

			t_bitset bitset(m_hpScope->attributes().size());
			for(unsigned int j = 0; j < size(); j++)
			{
				bitset.set((*this)[j]->index());
			}
			setBitset(bitset);

			m_hpScope->addAccessorSet(this);

			setActive(true);
			initializeAll();
		}
virtual	void	populate(sp<Layout> spLayout)
		{
			if(!spLayout.isValid())
			{
				feX("AccessorSet::populate","has invalid layout\n");
			}
			bind(spLayout->scope());
			for(unsigned int i = 0; i < size(); i++)
			{
				spLayout->populate(*((*this)[i]));
			}
		}
		void	enforceHaving(const AccessorSet &a_other)
		{
			if(!m_hpScope.isValid())
			{
				feX("AccessorSet::populate","unset scope\n");
			}

			Array<String> self_set;
			for(unsigned int i = 0; i < size(); i++)
			{
				self_set.push_back((*this)[i]->name());
			}

			for(unsigned int i = 0; i < a_other.size(); i++)
			{
				m_hpScope->enforce(self_set, a_other.m_accessors[i]->name());
			}
		}
		bool	bindCheck(sp<Layout> spLayout)
		{
			bind(spLayout->scope());
			return m_bitset.is_subset_of(spLayout->bitset());
#if 0
			for(unsigned int i = 0; i < size(); i++)
			{
				if(!((*this)[i]->check(spLayout))) { return false; }
			}
			return true;
#endif
		}
		bool	check(sp<LayoutSB> spLayout)
		{
			return m_bitset.is_subset_of(spLayout->bitset());
		}
		bool	check(sp<LayoutAV> spLayout)
		{
			return m_bitset.is_subset_of(spLayout->bitset());
		}

		bool	bindCheck(sp<RecordArray> spRA)
		{
			bind(spRA->layout()->scope());
			for(unsigned int i = 0; i < size(); i++)
			{
				if(!((*this)[i]->check(spRA))) { return false; }
			}
			return true;
		}
		bool	check(sp<RecordArray> spRA)
		{
//			fe_fprintf(stderr, "CHECK %d\n", m_bitset.is_subset_of(spRA->layout()->bitset()));
			return m_bitset.is_subset_of(spRA->layout()->bitset());
#if 0
			for(unsigned int i = 0; i < size(); i++)
			{
				if(!((*this)[i]->check(spRA)))
				{
					return false;
				}
			}
			return true;
#endif
		}
		bool	check(const WeakRecord &a_record)
		{
			return m_bitset.is_subset_of(a_record.layout()->bitset());
#if 0
			for(unsigned int i = 0; i < size(); i++)
			{
				if(!((*this)[i]->check(a_record)))
				{
					return false;
				}
			}

			return true;
#endif
		}
		bool	check(const Record &a_record)
		{
			return m_bitset.is_subset_of(a_record.layout()->bitset());
#if 0
			for(unsigned int i = 0; i < size(); i++)
			{
				if(!((*this)[i]->check(a_record)))
				{
					return false;
				}
			}

			return true;
#endif
		}
		void	check(const WeakRecord a_record,
					const char *a_annotation)
		{
			for(unsigned int i = 0; i < size(); i++)
			{
				if(!((*this)[i]->check(a_record)))
				{
					feX("AccessorSet::assert",
						"%s | %s | missing %s",
						(a_annotation!=NULL)?a_annotation:"",
						a_record.layout()->name().c_str(),
						(*this)[i]->attribute()->name().c_str());
				}
			}
		}
		void	enforce(const String &a_ifHas)
		{
			for(unsigned int i = 0; i < size(); i++)
			{
				m_hpScope->enforce(a_ifHas, (*this)[i]->attribute()->name());
			}
		}

		void	filter(sp<RecordGroup> rg_output, sp<RecordGroup> rg_input)
		{
			for(RecordGroup::iterator i_in = rg_input->begin();
				i_in != rg_input->end(); i_in++)
			{
				bind((*i_in)->layout()->scope());
				if(check(*i_in))
				{
					for(int i = (*i_in)->length() - 1; i >= 0; i--)
					{
						rg_output->add((*i_in)->getRecord(i));
					}
				}
			}
		}
		hp<Scope>	scope(void) { return m_hpScope; }

		void	filter(std::vector<Record> &a_records, sp<RecordGroup> rg_input)
		{
			for(RecordGroup::iterator i_in = rg_input->begin();
				i_in != rg_input->end(); i_in++)
			{
				bind((*i_in)->layout()->scope());
				if(check(*i_in))
				{
					for(int i = (*i_in)->length() - 1; i >= 0; i--)
					{
						a_records.push_back((*i_in)->getRecord(i));
					}
				}
			}
		}

		void	filter(std::vector<WeakRecord> &a_records, sp<RecordGroup> rg_input)
		{
			for(RecordGroup::iterator i_in = rg_input->begin();
				i_in != rg_input->end(); i_in++)
			{
				bind((*i_in)->layout()->scope());
				if(check(*i_in))
				{
					for(int i = (*i_in)->length() - 1; i >= 0; i--)
					{
						a_records.push_back((*i_in)->getRecord(i));
					}
				}
			}
		}
		const t_bitset	&bitset(void) const { return m_bitset; }
		void			setBitset(const t_bitset &a_bs) { m_bitset = a_bs; }
	public:
		//TODO: these adds are grossly bad/slow at scale.   Fix.
		void		add(BaseAccessor &a_accessor, const String &a_attribute)
					{
						if(m_hpScope.isValid())
						{
							a_accessor.setup(m_hpScope, a_attribute);
							m_accessors.push_back(&a_accessor);
							t_bitset bitset(m_hpScope->attributes().size());
							for(unsigned int j = 0; j < size(); j++)
							{
								bitset.set((*this)[j]->index());
							}
							setBitset(bitset);
						}
						if(m_optional_record.isValid())
						{
							a_accessor.bind(m_optional_record);
						}
					}
		void		add(BaseAccessor &a_accessor)
					{
						if(m_hpScope.isValid())
						{
							m_accessors.push_back(&a_accessor);
							t_bitset bitset(m_hpScope->attributes().size());
							for(unsigned int j = 0; j < size(); j++)
							{
								bitset.set((*this)[j]->index());
							}
							setBitset(bitset);
						}
						if(m_optional_record.isValid())
						{
							a_accessor.bind(m_optional_record);
						}
					}
		void attach(const WeakRecord &a_record)
		{
			for(unsigned int i = 0; i < m_accessors.size(); i++)
			{
				m_accessors[i]->bind(a_record);
			}
		}
	protected:
		hp<Scope>											m_hpScope;
		Array<BaseAccessor*>								m_accessors;
		t_bitset											m_bitset;
		WeakRecord											m_optional_record;
};

} /* namespace */

#endif /* __data_AccessorSet_h__ */

