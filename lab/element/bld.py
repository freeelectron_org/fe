import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool" ]

def setup(module):

    srcList = [ "element.pmh",
                "BruteCollide",
                "Drag",
                "DrawRod",
                "DrawTerrain",
                "Gravity",
                "NewtonEuler",
                "Provot",
                "Swim",
                "Terrain",
                "elementDL" ]

    dll = module.DLL( "fexElementDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib" ]

    forge.deps( ["fexElementDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexElementDL",                 None,       None) ]

#   module.Module( 'test' )
