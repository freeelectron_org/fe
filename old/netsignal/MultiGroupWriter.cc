/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace fe
{
namespace ext
{

MultiGroupWriter::MultiGroupWriter(void)
{
	m_count = 0;
	m_loop = 0;
	m_rate = 1;
	m_initialized = false;
}

MultiGroupWriter::~MultiGroupWriter(void)
{
	fprintf(stderr, "destruction of MultiGroupWriter\n");
}

void MultiGroupWriter::initialize(void)
{
}

void MultiGroupWriter::handle(Record &a_signal)
{
	if(cfg<String>("multi.prefix") != "")
	{
		sp<RecordGroup> rg_input =
			cfg< sp<RecordGroup> >("input"); // input group

		if(!rg_input.isValid())
		{
			return;
		}

		if(!m_initialized)
		{
			bfs::path prefix(cfg<String>("multi.prefix").c_str());
			bfs::path p = prefix.parent_path();

			if(!bfs::exists(p))
			{
				bfs::create_directories<bfs::path>(p);
			}

			m_prefix = prefix.string();

			Instance dummy;
			if(configLookup("multi.loop", dummy))
			{
				m_loop = atoi(cfg<String>("multi.loop").c_str());
			}
			if(configLookup("multi.rate", dummy))
			{
				m_rate = atoi(cfg<String>("multi.rate").c_str());
			}

			m_spScope = a_signal.layout()->scope();

			m_spChunkSpool = new ChunkSpool();
			m_chunkSenderTask.m_spChunkSpool = m_spChunkSpool;
			m_chunkSenderTask.m_pMultiGroupWriter = this;
			m_chunkSenderTask.m_prefix = m_prefix;
			m_chunkSenderTask.m_loop = m_loop;
			m_pThread = new boost::thread(m_chunkSenderTask);

			m_initialized = 1;
		}

		if(!(m_count%m_rate))
		{
			std::ostringstream ost;
			sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));
			spStream->output(ost, rg_input);
			sp<Chunk> spChunk(new Chunk(ost.str()));
			m_spChunkSpool->put(spChunk.abandon());
		}

		m_count++;
	}
}

MultiGroupWriterTask::MultiGroupWriterTask(void)
{
	m_pMultiGroupWriter = NULL;
	m_count = 0;
	m_loop = 0;
}

void MultiGroupWriterTask::operator()()
{
	try
	{
		sp<Chunk> spChunk;
		while(true)
		{
			spChunk = m_spChunkSpool->wait();
			feLogGroup("netsignal","chunk send size %u\n", spChunk->size());
			if(spChunk->size() == 0)
			{
				feX(e_writeFailed,
					"fe::ChunkSenderThread",
					"todo: decide on what to do with zero size chunks");
			}

			String filename;
			filename.sPrintf("%s.%d.rg",m_prefix.c_str(), m_count);

			std::ofstream outfile(filename.c_str());

			outfile.write((const char *)spChunk->buffer(), spChunk->size());

			outfile.close();

			m_count++;
			if(m_loop && (m_count >= m_loop))
			{
				m_count = 0;
			}
		}
	}
	catch(...)
	{
#if FE_CODEGEN<=FE_DEBUG
		exit(1);
#else
		feLogGroup("netsignal",
				"multi writer thread dying due to write failure\n");
#endif
	}
}

} /* namespace ext */
} /* namespace fe */

