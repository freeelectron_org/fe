/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage("fexSDLDL");
		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			sp<HandlerI> spLogger(spRegistry->create("*.LogWindowHandler"));
			if(!spQuickViewerI.isValid() || !spLogger.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->open();

			sp<HandlerI> spJoyHandler=spRegistry->create("JoyI");
			UNIT_TEST(spJoyHandler.isValid());

			if(spJoyHandler.isValid())
			{
				spQuickViewerI->insertEventHandler(spJoyHandler);
			}

			UNIT_TEST(spLogger.isValid());
			if(spLogger.isValid())
			{
				spQuickViewerI->insertEventHandler(spLogger);
			}

			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(6);
	UNIT_RETURN();
}
