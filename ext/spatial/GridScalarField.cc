/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "spatial/spatial.h"

namespace fe
{
namespace ext
{

GridScalarField::GridScalarField(void)
{
	m_volume = 0.0;
}

GridScalarField::~GridScalarField(void)
{
}

void GridScalarField::initialize(void)
{
}

GridScalarField::t_continuum &GridScalarField::continuum(void)
{
	return m_continuum;
}


Real GridScalarField::sample(const SpatialVector &a_location)
{
	SpatialVector location;
	location = a_location;
	return directSample(m_continuum, location);
}

void GridScalarField::gradient(SpatialVector &a_gradient, const SpatialVector &a_location)
{
	SpatialVector grad;
	fe::ext::gradient(m_continuum, grad, a_location);
	a_gradient = grad;
}

void GridScalarField::create(const sp<SpaceI> &a_space, const Vector3i &a_count, Real a_default)
{
	m_continuum.create(a_count, a_default, a_space);
}

void GridScalarField::radialSet(const SpatialVector &a_location, const SpatialVector &a_radius, Real a_value)
{
	fe::ext::radialSet(continuum(), a_location, a_radius, a_value);
}

sp<SpaceI> GridScalarField::space(void)
{
	return m_continuum.space();
}

Real GridScalarField::volume(void)
{
	if(m_volume <= 0.0)
	{
		SpatialVector cellsize;
		continuum().cellsize(cellsize);
		m_volume = cellsize[0] * cellsize[1] * cellsize[2];
	}
	return m_volume;
}

void GridScalarField::set(Real a_value)
{
	continuum().accessUnit().set(a_value);
}

void GridScalarField::set(const SpatialVector &a_location, Real a_value)
{
	t_continuum::t_index index;
	if(!getIndex(index, a_location))
	{
		return;
	}

	Real &value = continuum().accessUnit().access(index);
	value = a_value;
}

void GridScalarField::add(const SpatialVector &a_location, Real a_value)
{
	t_continuum::t_index index;
	if(!getIndex(index, a_location))
	{
		return;
	}

	Real &value = continuum().accessUnit().access(index);
	value += a_value;
}

bool GridScalarField::getIndex(t_continuum::t_index &a_index, const SpatialVector &a_loc)
{
	t_continuum::t_index safe_idx;
	continuum().index(a_index, a_loc);
	safe_idx = a_index;
	continuum().unit().safe(safe_idx);
	if(a_index != safe_idx)
	{
		return false;
	}
	return true;
}


} /* namespace ext */
} /* namespace fe */

