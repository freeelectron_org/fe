/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

#define FE_MMD_DEBUG		FALSE

#define FE_MMD_MAX_ANCHORS	4

//* "select" grips and move with middle mouse
//* hold shift for finer control

//* TODO
//* uv-based vs barycenter
//* smoothing
//* fix pan alpha
//* default parent surface while creating (input, driver, prior deform)
//* move anchor, interactive or with numbers
//* delete/reorder
//* undo/redo
//* paint points
//* choose representation: Matrix, TRS Euler, or TQS Quaternions
//* choose Euler order

using namespace fe;
using namespace fe::ext;

#define FE_MMD_PARAMS	10
static char s_hammer_name[FE_MMD_PARAMS][12]=
{
	"anchorName",
	"parent",
	"weight",
	"radius",
	"power",
	"pivotT",
	"pivotR",
	"deformT",
	"deformR",
	"deformS"
};

static char s_hammer_label[FE_MMD_PARAMS][20]=
{
	"Anchor Name",
	"Parent",
	"Weight Attribute",
	"Radius",
	"Power",
	"Pivot Translate",
	"Pivot Rotate",
	"Deform Translate",
	"Deform Rotate",
	"Deform Scale"
};

void MultiModOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("showAll")=false;
	catalog<String>("showAll","label")="Show All";
	catalog<String>("showAll","page")="Edit";

	String name;
	String label;
	for(U32 anchorIndex=0;anchorIndex<FE_MMD_MAX_ANCHORS;anchorIndex++)
	{
		for(U32 param=0;param<FE_MMD_PARAMS;param++)
		{
			name.sPrintf("%s%d",s_hammer_name[param],anchorIndex);
			label.sPrintf("%s %d",s_hammer_label[param],anchorIndex);

			switch(param)
			{
				case 0:
					catalog<String>(name)="";
					catalog<bool>(name,"joined")=true;
					break;
				case 1:
					catalog<String>(name)="Prior";
					catalog<String>(name,"choice:0")="Driver";
					catalog<String>(name,"choice:1")="Input";
					catalog<String>(name,"choice:2")="Prior";
					catalog<String>(name,"IO")="input output";
					break;
				case 2:
					catalog<String>(name);
					break;
				case 3:
					catalog<Real>(name)=1.0;
					catalog<Real>(name,"high")=10.0;
					catalog<bool>(name,"joined")=true;
					break;
				case 4:
					catalog<Real>(name)=1.0;
					catalog<Real>(name,"high")=2.0;
					break;
				default:
					catalog<SpatialVector>(name)=(param<8)?
							SpatialVector(0.0,0.0,0.0):
							SpatialVector(1.0,1.0,1.0);
					catalog<String>(name,"IO")="input output";
					break;
			}

			catalog<String>(name,"label")=label;
			catalog<bool>(name,"hidden")=false;
			catalog<String>(name,"page")="Edit";
		}
	}

	catalog<bool>("gripLabels")=false;
	catalog<String>("gripLabels","label")="Grip Labels";
	catalog<String>("gripLabels","page")="Display";

	catalog<Real>("anchorScale")=0.05;
	catalog<Real>("anchorScale","high")=0.1;
	catalog<Real>("anchorScale","max")=1.0;
	catalog<String>("anchorScale","label")="Anchor Scale";
	catalog<String>("anchorScale","page")="Display";

	catalog<Real>("axisScale")=0.3;
	catalog<Real>("axisScale","max")=1.0;
	catalog<String>("axisScale","label")="Axis Scale";
	catalog<String>("axisScale","page")="Display";

	catalog<Real>("weightScale")=0.1;
	catalog<Real>("weightScale","max")=1.0;
	catalog<String>("weightScale","label")="Weight Scale";
	catalog<String>("weightScale","page")="Display";

	catalog<Real>("Dimming")=0.6;
	catalog<Real>("Dimming","max")=1.0;
	catalog<String>("Dimming","page")="Display";

	catalog<Real>("Foreshadow")=0.4;
	catalog<Real>("Foreshadow","max")=1.0;
	catalog<String>("Foreshadow","page")="Display";

	catalog<Real>("Aura")=0.5;
	catalog<Real>("Aura","max")=1.0;
	catalog<String>("Aura","page")="Display";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")="Prompt here.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog<bool>("Output Surface","recycle")=true;

	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setPointSize(6.0);
	m_spWireframe->setLineWidth(1.0);

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_foreshadow);
	m_spSolid->setAntialias(TRUE);
//	m_spSolid->setZBuffering(FALSE);
	m_spSolid->setLit(FALSE);

	m_spManipulator=registry()->create("*.TransformManipulator");

	m_locatorList.resize(FE_MMD_MAX_ANCHORS);

	changeMode(e_pickAnchor);
}

void MultiModOp::changeMode(EditMode a_editMode)
{
	m_editMode=a_editMode;

	switch(m_editMode)
	{
		case e_pickAnchor:
			catalog<String>("Brush","prompt")="LMB sets anchor.";
			break;
		case e_manipulator:
			catalog<String>("Brush","prompt")=
					"LMB to manipulate."
					"  Enter to toggle transform and pivot."
					"  Esc to return to anchors.";
			break;
		case e_influence:
			catalog<String>("Brush","prompt")="Influence Prompt.";
			break;
		default:
			catalog<String>("Brush","prompt")="Prompt Error.";
			break;
	}
}

void MultiModOp::handle(Record& a_rSignal)
{
	const BWORD gripLabels=catalog<bool>("gripLabels");
	const Real weightScale=catalog<Real>("weightScale");
	const Real dimming=catalog<Real>("Dimming");
	const Real foreshadow=catalog<Real>("Foreshadow");
	const Real aura=catalog<Real>("Aura");

	const Color black(0.0,0.0,0.0);
	const Color white(1.0,1.0,0.8);
	const Color grey(0.8,0.8,1.0);
	const Color red(0.8,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color cyan(0.0,0.8,1.0);
	const Color magenta(0.8,0.0,0.8);
	const Color yellow(1.0,1.0,0.0);
	const Color orange(1.0,0.5,0.0);
	const Color blank(0.0,0.0,0.0,0.0);
	const Color lightgrey(0.4,0.4,0.4);
	const Color darkgrey(0.2,0.2,0.2);

	const SpatialVector up(0.0,1.0,0.0);

	const U32 resolution=16;

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

#if FE_MMD_DEBUG
	feLog("MultiModOp::handle brush %d brushed %d\n",
			spDrawBrush.isValid(),m_brushed);
#endif

	Real frame=currentFrame(a_rSignal);
	const BWORD frameChanged=(frame!= m_lastFrame);
	const BWORD replaced=(catalog<bool>("Output Surface","replaced"));
	if((frameChanged || replaced) && m_anchorPicked>=0)
	{
		pickAnchor(m_anchorPicked);
	}
	const BWORD paramChanged=(!m_brushed && !replaced && !frameChanged);

	if(m_spManipulator.isValid())
	{
		sp<Catalog> spCatalog=m_spManipulator;
		FEASSERT(spCatalog.isValid());

		spCatalog->catalog<bool>("gripLabels")=gripLabels;
		spCatalog->catalog<Real>("dimming")=dimming;
		spCatalog->catalog<Real>("foreshadow")=foreshadow;
		spCatalog->catalog<Real>("aura")=aura;
	}

	sp<DrawI> spDrawGuide;

	if(!spDrawBrush.isValid())
	{
		accessGuide(spDrawGuide,a_rSignal,e_quiet);

		if(spDrawGuide.isValid())
		{
			spDrawGuide->pushDrawMode(m_spSolid);
		}

		m_brushed=FALSE;
		m_lastFrame=frame;

		//* TODO use
//		const BWORD rebuild=(paramChanged || frameChanged || replaced);

		access(m_spInput,"Input Surface");
		if(!access(m_spDriver,"Driver Surface",e_quiet))
		{
			m_spDriver=m_spInput;
		}
		if(!m_spInput.isValid())
		{
			if(!m_spDriver.isValid())
			{
				catalog<String>("error")=
						"neither input nor driver is searchable";
				return;
			}
			m_spInput=m_spDriver;
		}

		sp<SurfaceAccessorI> spInputPoint;
		if(!access(spInputPoint,"Input Surface",e_point,e_position)) return;

		if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

		sp<SurfaceAccessorI> spOutputPoint;
		if(!access(spOutputPoint,m_spOutputAccessible,
				e_point,e_position)) return;

		sp<SurfaceAccessorI> spOutputNormal;
		access(spOutputNormal,m_spOutputAccessible,
				e_point,e_normal,e_quiet);

		//* reset output to input
		const U32 pointCount=spOutputPoint->count();
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=spInputPoint->spatialVector(pointIndex);
			spOutputPoint->set(pointIndex,point);
		}

		I32 anchorIndex=0;
		while(TRUE)
		{
			if(!m_spAnchorMap->lookup(anchorIndex).isValid())
			{
				break;
			}

			m_locatorList[anchorIndex]=evaluateLocator(anchorIndex);

			const Real radius=anchorRadius(anchorIndex);
			const Real power=anchorPower(anchorIndex);
			const String weightAttr=anchorWeightAttr(anchorIndex);

			sp<SurfaceAccessorI> spInputWeight;
			if(!weightAttr.empty())
			{
				access(spInputWeight,"Input Surface",
						e_point,weightAttr,e_warning);
			}

			const SpatialTransform& rLocator=m_locatorList[anchorIndex];
			const SpatialTransform cat=
					evaluateConcatenation(rLocator,anchorIndex);

//			const SpatialTransform pivot=getParamPivot(anchorIndex);
//			const SpatialTransform context=pivot*rLocator;

//			feLog("locator %d\n%s\n",anchorIndex,c_print(rLocator));
//			feLog("cat\n%s\n",c_print(cat));

			const U32 pointCount=spOutputPoint->count();
			for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				const SpatialVector point=anchorIndex?
						spOutputPoint->spatialVector(pointIndex):
						spInputPoint->spatialVector(pointIndex);

				//* relative anchor locator or pivot context?
				const Real distance=magnitude(point-rLocator.translation());

				const Real faded=1.0-distance/radius;
				if(faded<=0.0)
				{
					continue;
				}

				const Real weight=(spInputWeight.isValid())?
						spInputWeight->real(pointIndex): 1.0;

				const Real ratio=pow(faded,power)*weight;
				if(ratio<=0.0)
				{
					continue;
				}

				SpatialVector transformed;
				transformVector(cat,point,transformed);

				spOutputPoint->set(pointIndex,
						ratio*transformed+(1.0-ratio)*point);

				if(spDrawGuide.isValid() && m_anchorPicked==anchorIndex)
				{
					const SpatialVector norm=(spOutputNormal.isValid())?
							spOutputNormal->spatialVector(pointIndex):
							SpatialVector(0.0,1.0,0.0);

					const Color weightColor(1.0,ratio,0.0,1.0);

					SpatialVector line[2];
					line[0]=point;
					line[1]=line[0]+weightScale*norm;

					spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
							FALSE,&weightColor);
				}
			}

			if(spDrawGuide.isValid())
			{
//				SpatialTransform unscaled=transform;
//				normalizeSafe(unscaled.column(0));
//				normalizeSafe(unscaled.column(1));
//				normalizeSafe(unscaled.column(2));

				spDrawGuide->drawTransformedAxes(rLocator,0.2);
//				spDrawGuide->drawTransformedAxes(pivot*anchor,0.1);
//				spDrawGuide->drawTransformedAxes(unscaled,0.2);
			}

			if(spDrawGuide.isValid() &&
					m_spAnchorMap->lookup(anchorIndex+1).isValid())
			{
				if(m_anchorPicked==anchorIndex+1)
				{
					sp<SurfaceI> spSurface;
					access(spSurface,m_spOutputAccessible);

					spDrawGuide->draw(spSurface,&darkgrey);
				}
				else if(m_anchorPicked<0 || m_anchorPicked==anchorIndex)
				{
					//* TODO check if this is expensive
					sp<SurfaceI> spSurface;
					access(spSurface,m_spOutputAccessible);

					spDrawGuide->draw(spSurface,&lightgrey);
				}
			}

			anchorIndex++;
		}

		if(spDrawGuide.isValid())
		{
			if(m_anchorPicked<=0 && m_spAnchorMap->lookup(0).isValid())
			{
				spDrawGuide->draw(m_spInput,&darkgrey);
			}
			if(m_spDriver!=m_spInput && (m_anchorPicked<0 ||
					anchorParent(m_anchorPicked)=="Driver"))
			{
				spDrawGuide->draw(m_spDriver,&orange);
			}

			spDrawGuide->popDrawMode();
		}

		m_newParent="";
	}
	else
	{
		spDrawBrush->pushDrawMode(m_spSolid);
		spDrawOverlay->pushDrawMode(m_spWireframe);

		m_brushed=TRUE;

		sp<ViewI> spView=spDrawOverlay->view();
		const Box2i viewport=spView->viewport();
		const I32 width=viewport.size()[0];
		const I32 height=viewport.size()[1];

		if(m_spDriver.isValid())
		{
			m_event.bind(windowEvent(a_rSignal));

#if FE_MMD_DEBUG
			feLog("%s\n",c_print(m_event));
#endif

			if(m_event.isKeyPress(WindowEvent::e_keyEscape))
			{
				changeMode(e_pickAnchor);
				m_anchorPicked= -1;
			}

			const Real anchorSize=
					catalog<Real>("anchorScale")*m_spDriver->radius();

			I32 closestAnchor= -1;

			if(m_editMode==e_pickAnchor)
			{
				const Vector2i mouse(m_event.mouseX(),m_event.mouseY());

				const I32 range=16;
				Real minDistance=0.0;

				U32 anchorIndex=0;
				while(TRUE)
				{
					if(!m_spAnchorMap->lookup(anchorIndex).isValid())
					{
						break;
					}

					const SpatialTransform& rLocator=
							m_locatorList[anchorIndex];

					const Color anchorColor(0.8,0.8,0.4,0.5);
					drawAnchor(spDrawBrush,rLocator,anchorSize,
							resolution,anchorColor);

					const String label=anchorLabel(anchorIndex);
					Vector2i projected=spView->project(rLocator.translation()+
							SpatialVector(0.0,2.0*anchorSize,0.0),
							ViewI::e_perspective);

					const BWORD centered=TRUE;
					const U32 border=1;
					drawLabel(spDrawOverlay,projected,
							label,centered,border,white,NULL,&black);

					const Vector2i spot=spView->project(rLocator.translation(),
							ViewI::e_perspective);
					const Real distance=magnitude(spot-mouse);
					if(distance<range &&
							(closestAnchor<0 || distance<minDistance))
					{
						closestAnchor=anchorIndex;
						minDistance=distance;
					}

					anchorIndex++;
				}
			}

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			const WindowEvent::MouseButtons buttons=m_event.mouseButtons();

			sp<SurfaceI> spCreate=m_spDriver;
			if(spCreate==m_spInput)
			{
				access(spCreate,m_spOutputAccessible);
			}

			if(m_editMode==e_pickAnchor)
			{
				SpatialVector intersection;
				set(intersection);
				SpatialVector hitNormal;
				set(hitNormal);

				I32 triIndex= -1;
				SpatialBary barycenter;
				set(barycenter);

				if(closestAnchor>=0)
				{
					Record anchor=m_spAnchorMap->lookup(closestAnchor);
					FEASSERT(anchor.isValid());

					triIndex=m_aTriIndex(anchor);
					barycenter=m_aBarycenter(anchor);
				}
				else
				{
					const Real maxDistance= -1.0;
					sp<SurfaceI::ImpactI> spImpact=spCreate->rayImpact(
							rRayOrigin,rRayDirection,maxDistance);
					if(spImpact.isValid())
					{
						intersection=spImpact->intersection();
						hitNormal=spImpact->normal();
						barycenter=spImpact->barycenter();

						sp<SurfaceSearchable::Impact> spSearchImpact=spImpact;
						if(spSearchImpact.isValid())
						{
							triIndex=spSearchImpact->triangleIndex();
						}

						sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
						if(spTriImpact.isValid())
						{
#if FE_MMD_DEBUG
							const I32 pickIndex=spTriImpact->triangleIndex();
							feLog("MultiModOp::handle triIndex %d pick %d %s\n",
									triIndex,pickIndex,c_print(intersection));
#endif

							drawImpact(spDrawBrush,spTriImpact,0.08,&cyan);
						}
					}
				}

				if(triIndex>=0)
				{
					//* TODO choose inital parent setting
					const SpatialTransform locator=(closestAnchor>=0)?
							m_locatorList[closestAnchor]:
							spCreate->sample(triIndex,barycenter);

					const Color anchorColor(0.5,0.5,1.0,0.5);
					drawAnchor(spDrawBrush,locator,anchorSize,
							resolution,anchorColor);

					if(m_event.isMousePress() &&
							buttons&WindowEvent::e_mbLeft)
					{
						if(closestAnchor>=0)
						{
							pickAnchor(closestAnchor);
						}
						else
						{
							createAnchor(triIndex,barycenter);
						}
						changeMode(e_manipulator);

#if FE_MMD_DEBUG
						feLog("MultiModOp::handle created %d\n",
								m_anchorPicked);
#endif
					}
				}
			}
			else if(m_editMode==e_manipulator)
			{
				SpatialTransform cameraMatrix=spView->camera()->cameraMatrix();
				SpatialTransform cameraTransform;
				invert(cameraTransform,cameraMatrix);

//				const SpatialVector deltaX=cameraTransform.column(0);
//				const SpatialVector deltaY=cameraTransform.column(1);
//				const SpatialVector towardCamera=cameraTransform.column(2);

				FEASSERT(m_spManipulator.isValid());

				const SpatialTransform& rLocator=m_locatorList[m_anchorPicked];

				const Real axisScale=catalog<Real>("axisScale");
				const Real axisPixels=axisScale*height;
				const Real gripScale=
						spView->worldSize(rLocator.translation(),axisPixels);

				sp<Catalog> spCatalog=m_spManipulator;
				FEASSERT(spCatalog.isValid());

				spCatalog->catalog<Real>("anchorScale")=anchorSize;
				spCatalog->catalog<Real>("gripScale")=gripScale;

				m_spManipulator->bindOverlay(spDrawOverlay);
				m_spManipulator->handle(a_rSignal);
				m_spManipulator->draw(spDrawBrush,NULL);

				updateAnchor();
			}
		}

		if(m_anchorPicked>=0)
		{
			const String label=anchorLabel(m_anchorPicked);
			const SpatialVector textPoint(0.5*width,
					height-2*spDrawOverlay->font()->fontHeight(NULL,NULL),1.0);
			drawLabel(spDrawOverlay,textPoint,label,
					TRUE,3,white,NULL,&black);

		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

		//* TODO more selective
		catalog<String>("Brush","cook")="once";

		return;
	}

	if(m_anchorPicked>=0)
	{
		if(paramChanged)
		{
			//* repick to update manipulator
			pickAnchor(m_anchorPicked);
		}
		else
		{
			updateAnchor();
		}
	}

	const BWORD showAll=catalog<bool>("showAll");
	for(I32 anchorIndex=0;anchorIndex<FE_MMD_MAX_ANCHORS;anchorIndex++)
	{
		const BWORD hidden=(!showAll && anchorIndex!=m_anchorPicked);
		String name;
		for(U32 param=0;param<FE_MMD_PARAMS;param++)
		{
			name.sPrintf("%s%d",s_hammer_name[param],anchorIndex);
			catalog<bool>(name,"hidden")=hidden;
		}
	}

	return;
}

SpatialTransform MultiModOp::getParamPivot(I32 a_anchorIndex)
{
	String name;

	name.sPrintf("%s%d",s_hammer_name[5],a_anchorIndex);
	const SpatialVector translation=catalog<SpatialVector>(name);

//	feLog("MultiModOp::getParamPivot translation %s %s\n",
//			name.c_str(),c_print(translation));

	name.sPrintf("%s%d",s_hammer_name[6],a_anchorIndex);
	const SpatialVector eulerAngles=catalog<SpatialVector>(name);

	SpatialTransform result=SpatialEuler(eulerAngles);
	setTranslation(result,translation);

	return result;
}

void MultiModOp::setParamPivot(I32 a_anchorIndex,SpatialTransform a_transform)
{
	String name;

	name.sPrintf("%s%d",s_hammer_name[5],a_anchorIndex);
	catalog<SpatialVector>(name)=a_transform.translation();

//	feLog("MultiModOp::setParamPivot translation %s %s\n",
//			name.c_str(),c_print(a_transform.translation()));

	name.sPrintf("%s%d",s_hammer_name[6],a_anchorIndex);
	catalog<SpatialVector>(name)=SpatialEuler(a_transform);
}

SpatialTransform MultiModOp::getParamDeform(I32 a_anchorIndex)
{
	String name;

	name.sPrintf("%s%d",s_hammer_name[7],a_anchorIndex);
	const SpatialVector translation=catalog<SpatialVector>(name);

	name.sPrintf("%s%d",s_hammer_name[8],a_anchorIndex);
	const SpatialVector eulerAngles=catalog<SpatialVector>(name);

	name.sPrintf("%s%d",s_hammer_name[9],a_anchorIndex);
	const SpatialVector scaling=catalog<SpatialVector>(name);

	SpatialTransform rotation=SpatialEuler(eulerAngles);

	SpatialTransform scaleMatrix;
	setIdentity(scaleMatrix);
	scale(scaleMatrix,scaling);

	SpatialTransform result=scaleMatrix*rotation;
	setTranslation(result,translation);

	return result;
}

void MultiModOp::setParamDeform(I32 a_anchorIndex,SpatialTransform a_transform)
{
	SpatialTransform unscaled=a_transform;
	SpatialVector scaling(
			magnitude(a_transform.column(0)),
			magnitude(a_transform.column(1)),
			magnitude(a_transform.column(2)));

	unscaled.column(0)*=1.0/scaling[0];
	unscaled.column(1)*=1.0/scaling[1];
	unscaled.column(2)*=1.0/scaling[2];

//	feLog("MultiModOp::setParamDeform\n%s\nunscaled\n%s\nscaling %s\n",
//			c_print(a_transform),c_print(unscaled),c_print(scaling));

	String name;

	name.sPrintf("%s%d",s_hammer_name[7],a_anchorIndex);
	catalog<SpatialVector>(name)=a_transform.translation();

	name.sPrintf("%s%d",s_hammer_name[8],a_anchorIndex);
	catalog<SpatialVector>(name)=SpatialEuler(unscaled);

	name.sPrintf("%s%d",s_hammer_name[9],a_anchorIndex);
	catalog<SpatialVector>(name)=scaling;
}

String MultiModOp::anchorLabel(I32 a_anchorIndex)
{
	String anchorName;
	anchorName.sPrintf("anchorName%d",a_anchorIndex);

	String label=catalog<String>(anchorName);
	if(label.empty())
	{
		label.sPrintf("%d",a_anchorIndex);
	}
	return label;
}

String& MultiModOp::anchorParent(I32 a_anchorIndex)
{
	String name;
	name.sPrintf("%s%d",s_hammer_name[1],a_anchorIndex);

	if(!m_newParent.empty() && a_anchorIndex==m_anchorPicked)
	{
		catalog<String>(name)=m_newParent;
		return m_newParent;
	}

	return catalog<String>(name);
}

String MultiModOp::anchorWeightAttr(I32 a_anchorIndex)
{
	String name;
	name.sPrintf("%s%d",s_hammer_name[2],a_anchorIndex);

	return catalog<String>(name);
}

Real MultiModOp::anchorRadius(I32 a_anchorIndex)
{
	String name;
	name.sPrintf("%s%d",s_hammer_name[3],a_anchorIndex);

	return catalog<Real>(name);
}

Real MultiModOp::anchorPower(I32 a_anchorIndex)
{
	String name;
	name.sPrintf("%s%d",s_hammer_name[4],a_anchorIndex);

	return catalog<Real>(name);
}

SpatialTransform MultiModOp::evaluateLocator(I32 a_anchorIndex)
{
	Record anchor=m_spAnchorMap->lookup(a_anchorIndex);

	const I32 triIndex=m_aTriIndex(anchor);
	const SpatialBary barycenter=m_aBarycenter(anchor);

	const String parent=anchorParent(a_anchorIndex);

	SpatialTransform result;
	if(parent=="Prior")
	{
		sp<SurfaceI> spOutput;
		access(spOutput,m_spOutputAccessible);
		result=spOutput->sample(triIndex,barycenter);
	}
	else
	{
		result=(parent=="Driver")?
				m_spDriver->sample(triIndex,barycenter):
				m_spInput->sample(triIndex,barycenter);
	}

//	feLog("MultiModOp::evaluateLocator %d parent \"%s\"\n%s\n",
//			a_anchorIndex,parent.c_str(),c_print(result));

	return result;
}

SpatialTransform MultiModOp::evaluateConcatenation(SpatialTransform a_locator,
	I32 a_anchorIndex)
{
	const SpatialTransform pivot=getParamPivot(a_anchorIndex);
	const SpatialTransform deform=getParamDeform(a_anchorIndex);

	const SpatialTransform context=pivot*a_locator;

	SpatialTransform invContext;
	invert(invContext,context);

	return invContext*deform*context;
}

void MultiModOp::createAnchor(I32 a_triIndex,SpatialBary a_bary)
{
	//* find unique index
	I32 anchorIndex=0;
	while(TRUE)
	{
		if(!m_spAnchorMap->lookup(anchorIndex).isValid())
		{
			break;
		}
		anchorIndex++;
	}

	//* brushes not set up to output params, so defer setting parent
	m_anchorPicked=anchorIndex;
	m_newParent=(m_spDriver!=m_spInput)? "Driver": "Prior";

	Record anchor=m_spScope->createRecord("Anchor");
	m_aIndex(anchor)=anchorIndex;

	m_spState->add(anchor);

	m_aTriIndex(anchor)=a_triIndex;
	m_aBarycenter(anchor)=a_bary;

	m_locatorList[anchorIndex]=evaluateLocator(anchorIndex);

	SpatialTransform identity;
	setIdentity(identity);
	setParamPivot(anchorIndex,identity);
	setParamDeform(anchorIndex,identity);

	pickAnchor(anchorIndex);
}

void MultiModOp::pickAnchor(I32 a_anchorIndex)
{
	//* copy record to manipulator catalog
	m_anchorPicked=a_anchorIndex;

	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	spCatalog->catalog<SpatialTransform>("anchor")=
			m_locatorList[a_anchorIndex];
	spCatalog->catalog<SpatialTransform>("pivot")=
			getParamPivot(a_anchorIndex);
	spCatalog->catalog<SpatialTransform>("transform")=
			getParamDeform(a_anchorIndex);
}

void MultiModOp::updateAnchor(void)
{
	//* copy manipulator to record

	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	Record anchor=m_spAnchorMap->lookup(m_anchorPicked);
	FEASSERT(anchor.isValid());

	setParamPivot(m_anchorPicked,
			spCatalog->catalog<SpatialTransform>("pivot"));
	setParamDeform(m_anchorPicked,
			spCatalog->catalog<SpatialTransform>("transform"));
}

void MultiModOp::setupState(void)
{
/*
	m_aPoint.populate(m_spScope,"Influence","Point");
	m_aWeight.populate(m_spScope,"Influence","Weight");
*/

	m_aIndex.populate(m_spScope,"Anchor","Index");
	m_aTriIndex.populate(m_spScope,"Anchor","TriIndex");
	m_aBarycenter.populate(m_spScope,"Anchor","Barycenter");

	m_spAnchorMap=m_spState->lookupMap(m_aIndex);
}

BWORD MultiModOp::loadState(const String& a_rBuffer)
{
	if(OperatorState::loadState(a_rBuffer))
	{
		m_spAnchorMap=m_spState->lookupMap(m_aIndex);
		return TRUE;
	}

	return FALSE;
}
