import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs = forge.corelibs[:]

    tests = [ 'xAudio' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)
