/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <freetype/freetype.pmh>

extern "C"
{

FE_DL_EXPORT void FreeTypeLocation(void)
{
}

}

namespace fe
{
namespace ext
{

char* shader_read(const char* filename)
{
	FILE * file;
	char * buffer;
	size_t size;

	file = fopen(filename, "rb");
	if(!file)
	{
		feLogError( "Unable to open file \"%s\".\n", filename);
		return 0;
	}

	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);

	buffer = (char *) malloc( (size+1) * sizeof( char *));
	fread(buffer, sizeof(char), size, file);
	buffer[size] = 0;

	fclose(file);
	return buffer;
}

GLuint shader_compile(const char* source,const GLenum type)
{
	GLint compile_status;
	GLuint handle = glCreateShader(type);
	glShaderSource(handle, 1, &source, 0);
	glCompileShader(handle);

	glGetShaderiv(handle, GL_COMPILE_STATUS, &compile_status);
	if(compile_status == GL_FALSE)
	{
		GLchar messages[256];
		glGetShaderInfoLog(handle, sizeof(messages), 0, &messages[0]);
		feLogError( "%s\n", messages);
		exit(EXIT_FAILURE);
	}
	return handle;
}

GLuint shader_load(const char* vert_filename,const char* frag_filename)
{
	if(glCreateProgram==NULL)
	{
		feLog("shader_load glCreateProgram is NULL\n");
		return 0;
	}

	GLuint handle = glCreateProgram();

	if(vert_filename && strlen(vert_filename))
	{
		char *vert_source = shader_read(vert_filename);
		GLuint vert_shader = shader_compile(vert_source, GL_VERTEX_SHADER);
		glAttachShader(handle, vert_shader);
		glDeleteShader(vert_shader);
		free(vert_source);
	}
	if(frag_filename && strlen(frag_filename))
	{
		char *frag_source = shader_read(frag_filename);
		GLuint frag_shader = shader_compile(frag_source, GL_FRAGMENT_SHADER);
		glAttachShader(handle, frag_shader);
		glDeleteShader(frag_shader);
		free(frag_source);
	}

	glLinkProgram(handle);

	GLint link_status;
	glGetProgramiv(handle, GL_LINK_STATUS, &link_status);
	if(link_status == GL_FALSE)
	{
		GLchar messages[256];
		glGetProgramInfoLog(handle, sizeof(messages), 0, &messages[0]);
		feLogError( "%s\n", messages);
		exit(1);
	}

	return handle;
}

FontFreeTypeGL::FontFreeTypeGL(void):
	m_maxWidth(0),
	m_fontAscent(0),
	m_fontDescent(0),
	m_multiplication(0.0),
	m_pAtlas(NULL),
	m_pFont(NULL),
	m_pBuffer(NULL),
	m_shader(0)
{
/*
	amiri-regular.ttf
	fireflysung.ttf
	Liberastika-Regular.ttf
	Lobster-Regular.ttf
	LuckiestGuy.ttf
	OldStandard-Regular.ttf
	SourceCodePro-Regular.ttf
	SourceSansPro-Regular.ttf
	VeraMoBd.ttf
	VeraMoBI.ttf
	VeraMoIt.ttf
	VeraMono.ttf
	Vera.ttf
*/

	const Real dpi=fe::compute_dpi();
	m_multiplication=fe::determine_multiplication(dpi);

	feLog("FontFreeTypeGL::FontFreeTypeGL multiplication %.6G\n",
			m_multiplication);

	m_fontName="SourceCodePro-Regular";
	m_fixedWidth=TRUE;
	m_fontSize=12*m_multiplication;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

	const String loadPath=System::getLoadPath((void*)FreeTypeLocation,TRUE);

#pragma GCC diagnostic pop

/*X
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	m_freetypePath=loadPath.replace("\\\\[^\\\\]*$","")+"\\freetype-gl";
#else
	m_freetypePath=loadPath.replace("/[^/]*$","")+"/freetype-gl";
#endif
*/

	m_freetypePath=loadPath.pathname()+"/freetype-gl";

//	feLog("FontFreeTypeGL::FontFreeTypeGL loadPath \"%s\"\n",
//			loadPath.c_str());
//	feLog("FontFreeTypeGL::FontFreeTypeGL freetype path \"%s\"\n",
//			m_freetypePath.c_str());
}

FontFreeTypeGL::~FontFreeTypeGL(void)
{
	if(m_pBuffer)
	{
		vertex_buffer_delete(m_pBuffer);
	}
	if(m_pFont)
	{
		texture_font_delete(m_pFont);
	}

    glDeleteTextures(1, &m_pAtlas->id);
    m_pAtlas->id = 0;

	if(m_pAtlas)
	{
		texture_atlas_delete(m_pAtlas);
	}
}

void FontFreeTypeGL::initialize(void)
{
	feLog("FontFreeTypeGL::initialize\n");

	GLenum glewError=glewInit();
	if(glewError!=GLEW_OK)
	{
		feLog("FontFreeTypeGL::initialize glewInit failed \"%s\"\n",
				glewGetErrorString(glewError));
	}

	// Texture atlas to store individual glyphs
	m_pAtlas=texture_atlas_new(512,512,1);

	// Build a new vertex buffer (position, texture & color)
	m_pBuffer=vertex_buffer_new("vertex:3f,tex_coord:2f,color:4f");

	// Build a new texture font from its description and size
	m_pFont=texture_font_new_from_file(m_pAtlas,m_fontSize,
			(m_freetypePath+"/fonts/"+m_fontName+".ttf").c_str());

	glGenTextures(1, &m_pAtlas->id);

	m_shader=shader_load(
			(m_freetypePath+"/shaders/v3f-t2f-c4f.vert").c_str(),
			(m_freetypePath+"/shaders/v3f-t2f-c4f.frag").c_str());

	m_maxWidth=10;
	m_fontAscent=0;
	m_fontDescent=0;

	texture_glyph_t* glyph=texture_font_get_glyph(m_pFont,"W");
	if(glyph!=NULL)
	{
		m_maxWidth=glyph->advance_x;
		m_fontAscent=glyph->offset_y;
		m_fontDescent=glyph->height-glyph->offset_y;
	}

	glyph=texture_font_get_glyph(m_pFont,"y");
	if(glyph!=NULL)
	{
		m_fontAscent=fe::maximum(m_fontAscent,glyph->offset_y);
		m_fontDescent=fe::maximum(m_fontDescent,
				I32(glyph->height-glyph->offset_y));
	}

	if(m_fontAscent+m_fontDescent<1)
	{
		m_fontAscent=m_fontSize;
		m_fontDescent=0;
	}

	//* HACK add vertical separation
	m_fontAscent+=1;

	feLog("FontFreeTypeGL::initialize ascent %d descent %d\n",
			m_fontAscent,m_fontDescent);

	char buffer[2];
	buffer[1]=0;
	for(I32 c=32;c<127;c++)
	{
		buffer[0]=c;
		texture_font_get_glyph(m_pFont,buffer);
	}

	glBindTexture(GL_TEXTURE_2D, m_pAtlas->id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m_pAtlas->width, m_pAtlas->height,
			0, GL_RED, GL_UNSIGNED_BYTE, m_pAtlas->data);
}

void FontFreeTypeGL::drawAlignedText(sp<Component> a_spDrawComponent,
	const SpatialVector& a_location,const String a_text,const Color &a_color)
{
//	feLog("FontFreeTypeGL::drawAlignedText loc %s text \"%s\" color %s\n",
//			c_print(a_location),a_text.c_str(),c_print(a_color));

	sp<DrawI> spDrawI=a_spDrawComponent;
	if(spDrawI.isNull())
	{
		feLog("FontFreeTypeGL::drawAlignedText null draw component\n");
		return;
	}

	if(m_shader && !glIsProgram(m_shader))
	{
		feLog("FontFreeTypeGL::drawAlignedText shader %p is"
				" not recognized as a program\n",m_shader);
		return;
	}

	sp<ViewI> spViewI=spDrawI->view();

	const SpatialVector projected=
			(spViewI->projection()==ViewI::e_perspective)?
			spViewI->project(a_location): a_location;

	const Box2i viewport=spViewI->viewport();

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindTexture(GL_TEXTURE_2D, m_pAtlas->id);

	vertex_buffer_clear(m_pBuffer);
	add_text(projected,a_text,a_color);

	Matrix<4,4,float> model;
	Matrix<4,4,float> view;
	Matrix<4,4,float> projection;
	setIdentity(model);
	setIdentity(view);
	setIdentity(projection);

	projection=ortho(
			Real(0),Real(width(viewport)-1),
			Real(0),Real(height(viewport)-1),
			Real(-1),Real(1));

	if(m_shader)
	{
		glUseProgram(m_shader);

		glUniform1i(glGetUniformLocation(m_shader, "texture"),
				0);

		glUniformMatrix4fv(glGetUniformLocation(m_shader, "model"),
				1, 0, model.raw());
		glUniformMatrix4fv(glGetUniformLocation(m_shader, "view"),
				1, 0, view.raw());
		glUniformMatrix4fv(glGetUniformLocation(m_shader, "projection"),
				1, 0, projection.raw());

		vertex_buffer_render(m_pBuffer,GL_TRIANGLES);

		glUseProgram(0);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopAttrib();
}

typedef struct {
	float x, y, z;		// position
	float s, t;			// texture
	float r, g, b, a;	// color
} vertex_t;

void FontFreeTypeGL::add_text(const SpatialVector& a_location,
	const String a_text,const Color &a_color)
{
	vec2 pen={{a_location[0],a_location[1]}};

	float z=a_location[2];

	const float r=a_color[0];
	const float g=a_color[1];
	const float b=a_color[2];
	const float a=a_color[3];

	const char* pBuffer=a_text.c_str();

//	texture_font_load_glyphs(m_pFont,pBuffer);

	const U32 count=a_text.length();
	for(U32 index=0;index<count;index++)
	{
		texture_glyph_t *glyph=texture_font_get_glyph(m_pFont,pBuffer+index);
		if(glyph != NULL)
		{
			float kerning(0);
			if(m_fixedWidth)
			{
				kerning=(m_maxWidth - glyph->advance_x)/2;
			}
			else if(index>0)
			{
				kerning=texture_glyph_get_kerning(glyph,pBuffer+index-1);
			}

			pen.x += kerning;

			const float x0=int(pen.x + glyph->offset_x);
			const float y0=int(pen.y + glyph->offset_y);
			const float x1=int(x0 + glyph->width);
			const float y1=int(y0 - glyph->height);

			const float s0 = glyph->s0;
			const float t0 = glyph->t0;
			const float s1 = glyph->s1;
			const float t1 = glyph->t1;

			const GLuint indices[6] = {0,1,2, 0,2,3};
			const vertex_t vertices[4] = {
					{	x0,y0,z,	s0,t0,	r,g,b,a },
					{	x0,y1,z,	s0,t1,	r,g,b,a },
					{	x1,y1,z,	s1,t1,	r,g,b,a },
					{	x1,y0,z,	s1,t0,	r,g,b,a } };

			vertex_buffer_push_back(m_pBuffer, vertices, 4, indices, 6);

			if(m_fixedWidth)
			{
				pen.x += m_maxWidth-kerning;
			}
			else
			{
				pen.x += glyph->advance_x;
			}
		}
	}
}

I32 FontFreeTypeGL::pixelWidth(String a_string)
{
	const U32 count=a_string.length();
	if(m_fixedWidth)
	{
		return m_maxWidth*count;
	}

	I32 pixelCount=0;

	const char* pBuffer=a_string.c_str();
	for(U32 index=0;index<count;index++)
	{
		texture_glyph_t *glyph=texture_font_get_glyph(m_pFont,pBuffer+index);
		if(glyph != NULL)
		{
			pixelCount+=texture_glyph_get_kerning(glyph,pBuffer+index-1);
			pixelCount+=glyph->advance_x;
		}
	}

//	feLog("FontFreeTypeGL::pixelWidth \"%s\" %d\n",
//			a_string.c_str(),pixelCount);

	return pixelCount;
}

} /* namespace ext */
} /* namespace fe */
