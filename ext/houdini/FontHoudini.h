
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opengl_FontHoudini_h__
#define __opengl_FontHoudini_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini-direct implementations for FontI

	@ingroup opengl
*//***************************************************************************/
class FE_DL_EXPORT FontHoudini: public FontI,
		public Initialize<FontHoudini>
{
	public:

	class Phrase
	{
		public:
			SpatialVector	m_location;
			Color			m_color;
			String			m_text;
	};

				FontHoudini(void);
virtual			~FontHoudini(void);

		void	initialize(void);

virtual	I32		fontHeight(I32* a_pAscent,I32* a_pDescent)
				{
					if(a_pAscent)
					{
						*a_pAscent=m_fontAscent;
					}
					if(a_pDescent)
					{
						*a_pDescent=m_fontDescent;
					}

					return m_fontAscent+m_fontDescent;
				}
virtual	I32		pixelWidth(String a_string);

virtual	void	drawAlignedText(sp<Component> a_spDrawComponent,
					const SpatialVector& a_location,
					const String a_text,const Color &a_color);

static	void	setRender(RE_Render* a_pRender)
				{	ms_pRender=a_pRender; }

static	void	flush(void);

	private:

static	RE_Render*			ms_pRender;

		I32					m_fontAscent;
		I32					m_fontDescent;

		Real				m_multiplication;

static	Array<Phrase>		ms_phraseArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opengl_FontHoudini_h__ */
