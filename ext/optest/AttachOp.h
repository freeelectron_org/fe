/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_AttachOp_h__
#define __optest_AttachOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to attach one surface to another

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT AttachOp:
	public OperatorSurfaceCommon,
	public Initialize<AttachOp>
{
	public:

					AttachOp(void)											{}
virtual				~AttachOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_AttachOp_h__ */
