import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs + [
                "fePlatformLib",
                "feMemoryLib",
                "feCoreLib",
                "fePluginLib",
                "fexMessageDLLib",
                "fexMessageReliableUDPDLLib" ]

    tests = [
            'messageudp_server',
            'messageudp_client',
            'xMessageReliableSender' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xMessageReliableSender.exe",  "", "", None) ]

