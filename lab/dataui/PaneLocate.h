/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_PaneLocate_h__
#define __dataui_PaneLocate_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT PaneLocate :
		virtual public HandlerI
{
	public:
				PaneLocate(void);
virtual			~PaneLocate(void);

virtual void	handle(	Record &r_sig);

	private:

static	void convert(Vector4 &a_v, const Vector4 &a_s,
		const Vector2 &a_start, const Vector2 &a_end);

		AsWidget				m_asWidget;
		AsPane					m_asPane;
		DrawView				m_drawview;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __dataui_PaneLocate_h__ */

