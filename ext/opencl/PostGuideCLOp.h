/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_PostGuideCLOp_h__
#define __opencl_PostGuideCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to tweak output curves based on repositioned guides

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT PostGuideCLOp:
	public OpenCLOp,
	public Initialize<PostGuideCLOp>
{
	public:
				PostGuideCLOp(void)											{}
virtual			~PostGuideCLOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_PostGuideCLOp_h__ */
