import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "solve.pmh",
                "BlockPCG",
                "IncompleteSparse",
                "ExplicitInertial",
                "SemiImplicitInertial",
                "SemiImplicit",
                "SemiImplicit1D",
                "SemiImplicit2D",
                "solveLib"
                ]

    srcList += [ "solveDL" ]

    dll = module.DLL( "fexSolveDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexThreadDLLib" ]


    forge.deps( ["fexSolveDLLib"], deplibs )

#   forge.tests += [
#       ("inspect.exe",     "fexSolveDL",                   None,       None) ]

    module.Module( 'test' )
