/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

// Based on CORAL GPL_String

#ifndef __platform_dualstring_h__
#define __platform_dualstring_h__

namespace fe
{

#if defined(FE_OS) && FE_OS==FE_FREEBSD
#define FESTRING_WIDE		__wchar_t
#define FESTR_MB2WC(x,y,z)	mbstowcs((FESTRING_WIDE*)x,(const char*)y,z)
#define FESTR_WC2MB(x,y,z)	wcstombs((char*)x,(const FESTRING_WIDE*)y,z)
#else
#define FESTRING_WIDE		wchar_t
#define FESTR_MB2WC(x,y,z)	mbstowcs(x,(char*)y,z)
#define FESTR_WC2MB(x,y,z)	wcstombs((char*)x,y,z)
 #endif

#define FESTRING_WIDE_SIZE	sizeof(FESTRING_WIDE)

/**************************************************************************//**
	@brief Automatically reference-counted string container.

	Designed to behave as a native type, like float or int.
	All allocation worries are completely hidden.

	To assign a string just use simple assignments.
	@code
		DualString name("me");
		name="you";
		DualString id=name;
	@endcode

	You can compare strings in a boolean or lexical fashion.
	@code
		if(name!="Bob" && name.compare("M")<0)
			...
	@endcode

	To print a string in printf fashion, cast it to (char*) or use c_str().
	@code
		printf("%s %s\n",(char*)firstname,lastname.c_str());
	@endcode

	Operates in multiple formats with JIT conversion.

	TODO consider fixed local buffer for small strings to avoid new()

	Reference: Stroustrup C++ Ref Man, 2nd ed., pg. 248
*//***************************************************************************/
class DualString
	{
	private:

	class Rep
		{
		public:
			void					newBuffer(U32 size);
			void					deleteBuffer(void);

			U32						m_references;
			FESTRING_U8*			m_pBuffer8;
			FESTRING_WIDE*			m_pBufferWide;
			BWORD					m_stale8;
			BWORD					m_staleWide;
		};

	public:
						/// Construct an empty string.
						DualString(void);

						/// Copy constructor.
						DualString(const DualString &operand);

						/// Construct using a signed byte buffer.
						DualString(const FESTRING_I8 *operand);

						/// Construct using a unsigned byte buffer.
						DualString(const FESTRING_U8 *operand);

						/// Construct using a wide buffer, presumably Unicode.
						DualString(const FESTRING_WIDE *operand);

						/// allow init as 0 in templates
						DualString(I32 operand);

virtual					~DualString(void);

						/** Cast the contents of the 8-bit buffer as signed
							bytes. */
						operator const FESTRING_I8 *(void) const
						{	return raw(); }

						/** Cast the contents of the 8-bit buffer as
							unsigned bytes. */
						operator const FESTRING_U8 *(void) const
						{	return rawU8(); }

						/// Cast the contents of the wide buffer as Unicode.
						operator const FESTRING_WIDE *(void) const
						{	return rawWide(); }

						/// Compare to another DualString.
		DualString			&operator=(const DualString &operand);

						/// Compare to a signed byte buffer (using 8-bit).
		DualString			&operator=(const FESTRING_I8 *operand)
						{ return operator=((const FESTRING_U8 *)operand); };

						/// Compare to an unsigned byte buffer (using 8-bit).
		DualString			&operator=(const FESTRING_U8 *operand);

						/// Compare to an unicode buffer (using wide chars).
		DualString			&operator=(const FESTRING_WIDE *operand);

						/** Standard string compare: returns -1, 0, or 1 if the
							string is alphabetically less than, equal,
							or greater than the operand */
		I32				compare(const DualString &operand) const;

		BWORD			operator<(const DualString &operand) const
						{	return compare(operand) < 0; }

						/** Compares to another string assuming the given string
							is a dot-delimited ordered list containing optional
							substrings.
							Returns true if strings are exactly the same, or if
							they match only up to the length of the operand
							and the operand's terminating zero matches a
							period. */
		BWORD			dotMatch(const DualString &operand) const;

						/// Force all applicable characters to upper case.
		void			forceUppercase(void)	{ forceCase(true); };
						/// Force all applicable characters to lower case.
		void			forceLowercase(void)	{ forceCase(false); };

						/** Return the number of represented characters,
							but not necessarily the size of any buffer. */
		U32				length(void) const;

						/// Populate the string in the manner of sprintf().
		DualString		&sPrintf(const char* fmt, ...);

						/** Populate the string as with SPrintf(), but by
							concatenating the results to the existing string. */
		DualString		&catf(const char* fmt, ...);

						/** Concatenate the strings in a list. */
		DualString		&cat(std::list<DualString> &strList,
								DualString sep = "");

						/** @brief Populate using variable arg format

							@internal */
		DualString		&vsPrintf(const char* fmt,va_list ap);

						/** Return the contents of the 8-bit buffer cast as
							signed bytes. */
const	FESTRING_I8		*raw(void) const	{ return (FESTRING_I8 *)rawU8(); };
						/** Return the contents of the 8-bit buffer cast as
							signed bytes. */
const	FESTRING_I8		*c_str(void) const	{ return (FESTRING_I8 *)rawU8(); };
						/** Return the contents of the 8-bit buffer cast as
							unsigned bytes. */
const	FESTRING_U8		*rawU8(void) const;
						/** Return the contents of the wide buffer cast in
							native unicode format. */
const	FESTRING_WIDE	*rawWide(void) const;

						/** @todo: string serialization currently hacked in.
							only works for 8bit and does too much copying */
		void			output(std::ostream &ostrm) const;
		void			input(std::istream &istrm);

	private:
						/** Returns a new string with a copied underlying
							representation.  Note that the normal operator=()
							uses the same representation.  This method is
							provided in case data from an alternate heap may be
							destroyed.  This is primarily a Win32 DLL issue.
							Note that DualString is already safe across live
							heaps. */
		DualString			copy(void) const;

		void			init(void);

		void			forceCase(BWORD upper);
static	U32				strlenWide(const FESTRING_WIDE *buffer);

		void			confirm8(void);
		void			confirmWide(void);

virtual	Rep				*newRep(void);
virtual	void			deleteRep(void);

		Rep				*m_pRep;
	};


/// Compare two DualString's
inline BWORD operator == (const DualString &s1, const DualString &s2)
{
	if(s1.compare(s2)) { return false; }
	return true;
}

/// Compare a byte buffer to an DualString
inline BWORD operator == (const char *s1, const DualString &s2)
{
	if(((const DualString)s1).compare(s2)) { return false; }
	return true;
}

/// Compare an DualString to a byte buffer
inline BWORD operator == (const DualString &s1, const char *s2)
{
	if(s1.compare((const DualString)s2)) { return false; }
	return true;
}

/// Compare two DualString's (reverse logic)
inline BWORD operator != (const DualString &s1, const DualString &s2)
{
	if(s1.compare(s2)) { return true; }
	return false;
}

/// Compare a byte buffer to an DualString (reverse logic)
inline BWORD operator != (const char *s1, const DualString &s2)
{
	if(((const DualString)s1).compare(s2)) { return true; }
	return false;
}

/// Compare an DualString to a byte buffer (reverse logic)
inline BWORD operator != (const DualString &s1, const char *s2)
{
	if(s1.compare((const DualString)s2)) { return true; }
	return false;
}

inline DualString operator+(const DualString &s1, const DualString &s2)
{
	DualString result=s1;
	return result.catf("%s",s2.c_str());
}

/// Return a string for an FE_ERRNO code.
DualString errorDualString(int errnum);

} /* namespace */

#endif /* __platform_dualstring_h__ */
