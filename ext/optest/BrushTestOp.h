/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_BrushTestOp_h__
#define __optest_BrushTestOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to mouse event and custom drawing

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT BrushTestOp:
	public OperatorSurfaceCommon,
	public Initialize<BrushTestOp>
{
	public:

					BrushTestOp(void):
						m_keepInput(FALSE)									{}
virtual				~BrushTestOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		BWORD			m_keepInput;
		sp<SurfaceI>	m_spInput;
		WindowEvent		m_event;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_BrushTestOp_h__ */
