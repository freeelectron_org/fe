#! /usr/bin/perl

###############################################################################
#	Copyright 1998 Infinite Monkeys Inc.
#	Jason Weber baboon@imonk.com
#	Distribution and use unlimited given this header remains intact.
#
#	subphrase.pl: substitute phrase in one file with phrase in another
#
###############################################################################

# example: subphrase.pl copyright_pattern new_copyright source.h source.cc
#
# copyright_pattern:
#	/\*\*(.|\n)*Copyright([^\*]|\n)*\**/
#
# new_copyright:
#	/***************************************
#		Copyright 1998 such and such
#	***************************************/

if(@ARGV<3)
	{
	print "Usage: subphrase <oldtext_file> <newtext_file> <target_file> ...\n\n";
	die "Substitute pattern in one file with phrase from another.\n\n";
	}

$_=shift(@ARGV);
open(OLDFILE,$_) || die "Cannot open $_: $!";
$oldtext=join("",<OLDFILE>);
chop $oldtext;

$_=shift(@ARGV);
open(NEWFILE,$_) || die "Cannot open $_: $!";
$newtext=join("",<NEWFILE>);
chop $newtext;

while($filename=shift(@ARGV))
	{
	open(FILE,$filename) || die "Cannot open $_: $!";

	$_=join("",<FILE>);

	s/${oldtext}/${newtext}/;

	close(FILE);

	open(FILE,">$filename") || die "Cannot open $_: $!";
	print FILE $_;
	close(FILE);
	}
