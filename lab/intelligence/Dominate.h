/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Dominate_h__
#define __intelligence_Dominate_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Propagate control connections

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Dominate: virtual public HandlerI
{
	public:
				Dominate(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		void	unwire(ControlCenter& controlled,WeakRecord& rOldControlRecord);
		void	wireLike(ControlCenter& controlled,
						ControlCenter& controlCenter,I32 subservience);
		void	wire(ControlCenter& controlled,
						ControlCenter& controlCenter,I32 subservience);

		Accessor<I32>					m_aSubservience;

		Theater							m_theaterRV;
		ControlCenter					m_controlRV1;
		ControlCenter					m_controlRV2;
		RecordArrayView<ControlCenter>	m_controlRAV;
		RecordArrayView<Rod>			m_rodRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Dominate_h__ */
