import sys
forge = sys.modules["forge"]

import os.path

def setup(module):

    deplibs = forge.corelibs[:]
    deplibs += [
        "fexSignalLib",
        "fexWindowLib",
        "fexDataToolDLLib",
        "fexMessageDLLib",
        "fexBeaconServerDLLib",
        "fexBeaconClientDLLib",
    ]

    tests = [   'xBeaconServer',
                'xBeaconServerStartup',
                'xBeaconClient',
                'xBeaconClientStartup',
                'xBeaconTimeSync'
            ]

    for t in tests:
        exe = module.Exe(t)
        if forge.fe_os == "FE_LINUX":
            exe.linkmap = { "stdthread": "-lpthread" }
        forge.deps([t + "Exe"], deplibs)
