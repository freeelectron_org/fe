CREATE TABLE Car (
  id INTEGER UNSIGNED NOT NULL,
  name VARCHAR(255) NULL,
  PRIMARY KEY(id)
);

CREATE TABLE Car_has_Class (
  Car_id INTEGER UNSIGNED NOT NULL,
  Class_name VARCHAR(255) NOT NULL,
  PRIMARY KEY(Car_id, Class_name)
);

CREATE TABLE Class (
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY(name)
);

CREATE TABLE Driver (
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY(name)
);

CREATE TABLE DriverSession (
  Session_id INTEGER UNSIGNED NOT NULL,
  Driver_name VARCHAR(255) NOT NULL,
  Car_id INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(Session_id, Driver_name)
);

CREATE TABLE Lap (
  count INTEGER UNSIGNED NOT NULL,
  DriverSession_Session_id INTEGER UNSIGNED NOT NULL,
  DriverSession_Driver_name VARCHAR(255) NOT NULL,
  elapsed TIME NULL,
  PRIMARY KEY(count, DriverSession_Session_id, DriverSession_Driver_name)
);

CREATE TABLE Session (
  id INTEGER UNSIGNED NOT NULL,
  TrackSession_id INTEGER UNSIGNED NOT NULL,
  kind VARCHAR(255) NOT NULL,
  lapcount INTEGER UNSIGNED NOT NULL,
  start DATETIME NULL,
  PRIMARY KEY(id)
);

CREATE TABLE Split (
  elapsed TIME NOT NULL,
  sector INTEGER UNSIGNED NOT NULL,
  Lap_DriverSession_Session_id INTEGER UNSIGNED NOT NULL,
  Lap_count INTEGER UNSIGNED NOT NULL,
  Lap_DriverSession_Driver_name VARCHAR(255) NOT NULL,
  PRIMARY KEY(elapsed, sector, Lap_DriverSession_Session_id, Lap_count, Lap_DriverSession_Driver_name)
);

CREATE TABLE Track (
  name VARCHAR(255) NOT NULL,
  Venue_name VARCHAR(255) NOT NULL,
  meters INTEGER UNSIGNED NULL,
  PRIMARY KEY(name)
);

CREATE TABLE TrackSession (
  id INTEGER UNSIGNED NOT NULL,
  Track_name VARCHAR(255) NOT NULL,
  start DATETIME NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE Venue (
  name VARCHAR(255) NOT NULL,
  latitude FLOAT NOT NULL,
  longitude FLOAT NOT NULL,
  PRIMARY KEY(name)
);


