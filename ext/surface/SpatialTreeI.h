/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SpatialTreeI_h__
#define __surface_SpatialTreeI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Triangular storage interface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SpatialTreeI:
	virtual public Component,
	public CastableAs<SpatialTreeI>
{
	public:

	class Hit:
		public Counted,
		public CastableAs<Hit>
	{
		public:
								Hit(void)
								{
									setName("Hit");
								}
	virtual
	const	SpatialVector&		intersection(void) const					=0;
	virtual
	const	SpatialVector&		direction(void) const						=0;
	virtual	Real				distance(void) const						=0;
	virtual I32					face(void) const							=0;
	virtual I32					triangleIndex(void) const					=0;
	virtual I32					primitiveIndex(void) const					=0;
	virtual I32					partitionIndex(void) const					=0;
	virtual
	const	SpatialBary&		barycenter(void) const						=0;
	virtual
	const	Vector2&			uv(void) const								=0;
	virtual
	const	SpatialVector&		du(void) const								=0;
	virtual
	const	SpatialVector&		dv(void) const								=0;
	virtual
	const	I32*				pointIndex(void) const						=0;
	virtual
	const	SpatialVector*		vertex(void) const							=0;
	virtual
	const	SpatialVector*		normal(void) const							=0;
	};

virtual	void	setRefinement(U32 a_refinement)								=0;

virtual	void	setAccuracy(SurfaceI::Accuracy a_accuracy)					=0;

virtual	void	populate(
						const Vector3i* a_pElementArray,
						const SpatialVector* a_pVertexArray,
						const SpatialVector* a_pNormalArray,
						const Vector2* a_pUVArray,
						const Color* a_pColorArray,
						const I32* a_pTriangleIndexArray,
						const I32* a_pPointIndexArray,
						const I32* a_pPrimitiveIndexArray,
						const I32* a_pPartitionIndexArray,
						U32 a_primitives,U32 a_vertices,
						const SpatialVector& a_center,F32 a_radius)			=0;
virtual	Real	nearestPoint(const SpatialVector& a_origin,
						Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
						const sp<PartitionI>& a_rspPartition,
						Array< sp<Hit> >& a_rHitArray) const				=0;
virtual	Real	rayImpact(const SpatialVector& a_origin,
						const SpatialVector& a_direction,
						Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
						const sp<PartitionI>& a_rspPartition,
						Array< sp<Hit> >& a_rHitArray) const				=0;

virtual	void	draw(sp<DrawI>,const fe::Color*) const						=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SpatialTreeI_h__ */
