/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#include <float.h>

#define	FE_SS_DEBUG				FALSE
#define	FE_SS_TREE_HIT_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceSearchable::SurfaceSearchable(void):
	m_refinement(0),
	m_accuracy(SurfaceI::e_triangle),
	m_searchable(TRUE),
	m_elements(0),
	m_vertices(0),
	m_optionalArrays(e_arrayNull),
	m_pElementArray(NULL),
	m_pVertexArray(NULL),
	m_pNormalArray(NULL),
	m_pTangentArray(NULL),
	m_pUVArray(NULL),
	m_pUV3Array(NULL),
	m_pColorArray(NULL),
	m_pRadiusArray(NULL),
	m_pTriangleIndexArray(NULL),
	m_pPartitionIndexArray(NULL),
	m_pPointIndexArray(NULL),
	m_pPrimitiveIndexArray(NULL)
{
	m_radius=0.0;
}

SurfaceSearchable::~SurfaceSearchable(void)
{
	deallocate(m_pPartitionIndexArray);
	deallocate(m_pPrimitiveIndexArray);
	deallocate(m_pPointIndexArray);
	deallocate(m_pTriangleIndexArray);
	deallocate(m_pUV3Array);
	deallocate(m_pColorArray);
	deallocate(m_pRadiusArray);
	deallocate(m_pUVArray);
	deallocate(m_pTangentArray);
	deallocate(m_pNormalArray);
	deallocate(m_pVertexArray);
	deallocate(m_pElementArray);
}

void SurfaceSearchable::clear(void)
{
	m_cached=FALSE;
	m_spSearchComponent=NULL;
	m_spSpatialTree=NULL;
	m_spUVTree=NULL;
}

void SurfaceSearchable::resizeArrays(U32 a_elementCount,U32 a_vertexCount)
{
	m_pElementArray=(Vector3i*)reallocate(m_pElementArray,
			a_elementCount*sizeof(Vector3i));

	m_pVertexArray=(SpatialVector*)reallocate(m_pVertexArray,
			a_vertexCount*sizeof(SpatialVector));
	m_pNormalArray=(SpatialVector*)reallocate(m_pNormalArray,
			a_vertexCount*sizeof(SpatialVector));
	m_pTriangleIndexArray=(I32*)reallocate(m_pTriangleIndexArray,
			a_vertexCount*sizeof(I32));
	m_pPointIndexArray=(I32*)reallocate(m_pPointIndexArray,
			a_vertexCount*sizeof(I32));
	m_pPrimitiveIndexArray=(I32*)reallocate(m_pPrimitiveIndexArray,
			a_vertexCount*sizeof(I32));
	m_pPartitionIndexArray=(I32*)reallocate(m_pPartitionIndexArray,
			a_vertexCount*sizeof(I32));

	if(m_optionalArrays&e_arrayColor)
	{
		m_pColorArray=(Color*)reallocate(m_pColorArray,
				a_vertexCount*sizeof(Color));
	}

	if(m_optionalArrays&e_arrayRadius)
	{
		m_pRadiusArray=(Real*)reallocate(m_pRadiusArray,
				a_vertexCount*sizeof(Real));
	}

	if(m_optionalArrays&e_arrayUV)
	{
		m_pUVArray=(Vector2*)reallocate(m_pUVArray,
				a_vertexCount*sizeof(Vector2));
	}
}

void SurfaceSearchable::resizeFor(U32 a_primitiveCount,U32 a_vertexCount,
	U32& a_rElementAllocated,U32& a_rVertexAllocated)
{
	U32 elementDesired=a_rElementAllocated;
#if FALSE
	if(!a_rElementAllocated)
	{
		elementDesired=a_vertexCount*a_primitiveCount/3;
	}
	else if(a_rElementAllocated<a_vertexCount)
	{
		elementDesired=(a_vertexCount*1.2)/3;	//* tweak
	}
#else
	if(elementDesired<a_primitiveCount)
	{
		elementDesired=a_primitiveCount*1.2;	//* tweak
	}
#endif

	U32 vertexDesired=a_rVertexAllocated;
	if(!a_rVertexAllocated)
	{
		vertexDesired=a_vertexCount;
	}
	else if(a_rVertexAllocated<a_vertexCount)
	{
		vertexDesired=a_vertexCount*1.2;		//* tweak
	}

	//* don't shrink here
	if(elementDesired<a_rElementAllocated)
	{
		elementDesired=a_rElementAllocated;
	}
	if(vertexDesired<a_rVertexAllocated)
	{
		vertexDesired=a_rVertexAllocated;
	}

	if(a_rElementAllocated!=elementDesired ||
			a_rVertexAllocated!=vertexDesired)
	{
		resizeArrays(elementDesired,vertexDesired);
		a_rElementAllocated=elementDesired;
		a_rVertexAllocated=vertexDesired;
	}
}


Protectable* SurfaceSearchable::clone(Protectable* pInstance)
{
#if	FE_SS_DEBUG
	feLog("SurfaceSearchable::clone\n");
#endif

	SurfaceSearchable* pSurfaceSearchable= pInstance?
			fe_cast<SurfaceSearchable>(pInstance):
			new SurfaceSearchable();

	checkCache();

	SurfaceSphere::clone(pSurfaceSearchable);

	// TODO copy attributes

	pSurfaceSearchable->m_elements=m_elements;
	pSurfaceSearchable->m_vertices=m_vertices;
	pSurfaceSearchable->resizeArrays(m_elements,m_vertices);

	const U32 sizeElemVector3i=m_elements*sizeof(Vector3i);
	memcpy((void*)pSurfaceSearchable->m_pElementArray,
			m_pElementArray,sizeElemVector3i);

	const U32 sizeVector=m_vertices*sizeof(SpatialVector);
	const U32 sizeVector2=m_vertices*sizeof(Vector2);
	const U32 sizeVectorC=m_vertices*sizeof(Color);
	const U32 sizeI32=m_vertices*sizeof(I32);
	const U32 sizeReal=m_vertices*sizeof(Real);

	memcpy((void*)pSurfaceSearchable->m_pVertexArray,
			m_pVertexArray,sizeVector);
	memcpy((void*)pSurfaceSearchable->m_pNormalArray,
			m_pNormalArray,sizeVector);
	memcpy((void*)pSurfaceSearchable->m_pTriangleIndexArray,
			m_pTriangleIndexArray,sizeI32);
	memcpy((void*)pSurfaceSearchable->m_pPointIndexArray,
			m_pPointIndexArray,sizeI32);
	memcpy((void*)pSurfaceSearchable->m_pPrimitiveIndexArray,
			m_pPrimitiveIndexArray,sizeI32);
	memcpy((void*)pSurfaceSearchable->m_pPartitionIndexArray,
			m_pPartitionIndexArray,sizeI32);

	if(pSurfaceSearchable->m_pUVArray)
	{
		memcpy((void*)pSurfaceSearchable->m_pUVArray,
				m_pUVArray,sizeVector2);
	}
	else
	{
		deallocate(m_pUVArray);
		m_pUVArray=NULL;
	}

	if(pSurfaceSearchable->m_pColorArray)
	{
		memcpy((void*)pSurfaceSearchable->m_pColorArray,
				m_pColorArray,sizeVectorC);
	}
	else
	{
		deallocate(m_pColorArray);
		m_pColorArray=NULL;
	}

	if(pSurfaceSearchable->m_pRadiusArray)
	{
		memcpy((void*)pSurfaceSearchable->m_pRadiusArray,
				m_pRadiusArray,sizeReal);
	}
	else
	{
		deallocate(m_pRadiusArray);
		m_pRadiusArray=NULL;
	}

	if(pSurfaceSearchable->m_pTangentArray)
	{
		memcpy((void*)pSurfaceSearchable->m_pTangentArray,
				m_pTangentArray,sizeVector);
	}
	else
	{
		deallocate(m_pTangentArray);
		m_pTangentArray=NULL;
	}

	return pSurfaceSearchable;
}

void SurfaceSearchable::cache(void)
{
	feLog("SurfaceSearchable::cache\n");

	if(!m_record.isValid())
	{
		feLog("SurfaceSearchable::cache record invalid\n");
		return;
	}

	setOptionalArrays(Arrays(e_arrayColor|e_arrayUV|e_arrayRadius));

	SurfaceSphere::cache();

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_record);

	sp<RecordGroup> spRG=surfaceModelRV.surfacePointRG();
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;

		feLog("next RecordArray\n");

		RecordArrayView<SurfacePoint> surfacePointRAV;
		surfacePointRAV.bind(spRA);
		for(SurfacePoint& surfacePointRV: surfacePointRAV)
		{
			SpatialVector& at=surfacePointRV.at();
			SpatialVector& up=surfacePointRV.up();
//			SpatialVector& uvw=surfacePointRV.uvw();

			resizeArrays(m_elements,m_vertices+1);

//			Vector3i& rPrimitive=m_pElementArray[m_elements];
			SpatialVector& rVertex=m_pVertexArray[m_vertices];
			SpatialVector& rNormal=m_pNormalArray[m_vertices];
			Vector2& rUV=m_pUVArray[m_vertices];
			Color& rColor=m_pColorArray[m_vertices];
			Real& rRadius=m_pRadiusArray[m_vertices];
			I32& rTriangleIndex=m_pTriangleIndexArray[m_vertices];
			I32& rPointIndex=m_pPointIndexArray[m_vertices];
			I32& rPrimitiveIndex=m_pPrimitiveIndexArray[m_vertices];
			I32& rPartitionIndex=m_pPartitionIndexArray[m_vertices];

			//* TODO element array

			rVertex=at;
			rNormal=up;

			//* TODO
			set(rUV);
			set(rColor);
			rRadius=Real(0);
			rTriangleIndex= -1;
			rPointIndex= -1;
			rPrimitiveIndex= -1;
			rPartitionIndex= -1;

			feLog("vertex %d v %s n %s uv %s color %s radius %.6G"
					" triangle %d point %d primitive %d partition %d\n",
					m_vertices,c_print(rVertex),c_print(rNormal),
					c_print(rUV),c_print(rColor),rRadius,
					rTriangleIndex,rPointIndex,
					rPrimitiveIndex,rPartitionIndex);

			m_vertices++;
		}
	}

	if(surfaceModelRV.searchComponent().isValid())
	{
		m_spSearchComponent=surfaceModelRV.searchComponent();
	}
}

void SurfaceSearchable::filterUV(void)
{
	if(!m_pUVArray)
	{
		return;
	}

	for(U32 elementIndex=0;elementIndex<m_elements;elementIndex++)
	{
		const U32 subCount=m_pElementArray[elementIndex][1];
		const BWORD openCurve=m_pElementArray[elementIndex][2];
		if(subCount!=3 || openCurve)
		{
			//* TODO
			continue;
		}

		//* filter UV wrap-around on each triangle

		const U32 pointIndex=m_pElementArray[elementIndex][0];

		FEASSERT(m_pUVArray);
		Vector2& rUV0=m_pUVArray[pointIndex];
		Vector2& rUV1=m_pUVArray[pointIndex+1];
		Vector2& rUV2=m_pUVArray[pointIndex+2];

//			feLog("filter %d/%d  %s  %s  %s\n",pointIndex,m_vertices,
//					c_print(rUV0),c_print(rUV1),c_print(rUV2));

		for(U32 m=0;m<2;m++)
		{
			if(rUV0[m]<0.3 && (rUV1[m]>0.7 || rUV2[m]>0.7))
			{
				rUV0[m]+=1.0;
			}

			if(rUV1[m]<0.3 && (rUV0[m]>0.7 || rUV2[m]>0.7))
			{
				rUV1[m]+=1.0;
			}

			if(rUV2[m]<0.3 && (rUV0[m]>0.7 || rUV1[m]>0.7))
			{
				rUV2[m]+=1.0;
			}
		}

//		feLog("filtered %s  %s  %s\n",
//				c_print(rUV0),c_print(rUV1),c_print(rUV2));
	}
}

void SurfaceSearchable::calcTangents(void)
{
	if(!m_pUVArray)
	{
		return;
	}
	if(m_pTangentArray)
	{
		//* keep provided tangents
		return;
	}

	m_pTangentArray=(SpatialVector*)reallocate(m_pTangentArray,
			m_vertices*sizeof(SpatialVector));

	BWORD hasTangents=FALSE;

	for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
	{
		set(m_pTangentArray[vertexIndex]);
	}

	for(U32 elementIndex=0;elementIndex<m_elements;elementIndex++)
	{
		const U32 subCount=m_pElementArray[elementIndex][1];
		const BWORD openCurve=m_pElementArray[elementIndex][2];
		if(subCount!=3 || openCurve)
		{
			//* TODO other cases

			continue;
		}

		const U32 pointIndex=m_pElementArray[elementIndex][0];

		const SpatialVector& rVertex0=m_pVertexArray[pointIndex];
		const SpatialVector& rVertex1=m_pVertexArray[pointIndex+1];
		const SpatialVector& rVertex2=m_pVertexArray[pointIndex+2];

		const SpatialVector& rNormal0=m_pNormalArray[pointIndex];
		const SpatialVector& rNormal1=m_pNormalArray[pointIndex+1];
		const SpatialVector& rNormal2=m_pNormalArray[pointIndex+2];

		const Vector2& rUV0=m_pUVArray[pointIndex];
		const Vector2& rUV1=m_pUVArray[pointIndex+1];
		const Vector2& rUV2=m_pUVArray[pointIndex+2];

		SpatialVector du;
		SpatialVector dv;

		triangleDuDv(du,dv,rVertex0,rVertex1,rVertex2,
				rUV0,rUV1,rUV2);

		SpatialVector tangent;
		tangentFromDuDvN(tangent,du,dv,rNormal0);
		m_pTangentArray[pointIndex]+=tangent;
		tangentFromDuDvN(tangent,du,dv,rNormal1);
		m_pTangentArray[pointIndex+1]+=tangent;
		tangentFromDuDvN(tangent,du,dv,rNormal2);
		m_pTangentArray[pointIndex+2]+=tangent;

		hasTangents=TRUE;
	}

	if(hasTangents)
	{
		for(U32 vertexIndex=0;vertexIndex<m_vertices;vertexIndex++)
		{
			normalizeSafe(m_pTangentArray[vertexIndex]);
		}
	}
	else
	{
		deallocate(m_pTangentArray);
		m_pTangentArray=NULL;
	}
}

void SurfaceSearchable::prepareForSample(void)
{
	SurfaceBase::prepareForSample();

	filterUV();
	calcTangents();
}

void SurfaceSearchable::prepareForSearch(void)
{
	checkCache();

	if(m_spSpatialTree.isValid())
	{
		return;
	}

#if	FE_SS_DEBUG
	feLog("SurfaceSearchable::prepareForSearch"
			" m_elements=%d m_vertices=%d tree valid=%d\n",
			m_elements,m_vertices,m_spSpatialTree.isValid());
#endif

	if(!m_elements)
	{
		feX(e_cannotCreate,"SurfaceSearchable::prepareForSearch","no elements");
	}
	if(!m_vertices)
	{
		feX(e_cannotCreate,"SurfaceSearchable::prepareForSearch","no vertices");
	}
	if(m_spSearchComponent.isValid())
	{
		m_spSpatialTree=m_spSearchComponent;
	}
	if(!m_spSpatialTree.isValid() && !m_searchName.empty())
	{
		m_spSpatialTree=create(m_searchName);
	}
	if(!m_spSpatialTree.isValid())
	{
		m_spSpatialTree=create("SpatialTreeI.default");
	}
	if(!m_spSpatialTree.isValid())
	{
		return;
	}

#if	FE_SS_DEBUG
	feLog("SurfaceSearchable::prepareForSearch building spatial tree\n");
#endif

	filterUV();
	calcTangents();

	m_spSpatialTree->setRefinement(m_refinement);
	m_spSpatialTree->setAccuracy(m_accuracy);
	m_spSpatialTree->populate(m_pElementArray,m_pVertexArray,m_pNormalArray,
			m_pUVArray,m_pColorArray,m_pTriangleIndexArray,m_pPointIndexArray,
			m_pPrimitiveIndexArray,m_pPartitionIndexArray,
			m_elements,m_vertices,center(),radius());

#if	FE_SS_DEBUG
	feLog("SurfaceSearchable::prepareForSearch built spatial tree\n");
#endif
}

void SurfaceSearchable::prepareForUVSearch(void)
{
	checkCache();

	if(!m_pUVArray)
	{
		feX(e_cannotFind,"SurfaceSearchable::prepareForUVSearch","no UVs");
		return;
	}

	prepareForSearch();

	if(m_spUVTree.isValid())
	{
		return;
	}

#if	FE_SS_DEBUG
	feLog("SurfaceSearchable::prepareForUVSearch"
			" m_elements=%d m_vertices=%d tree valid=%d\n",
			m_elements,m_vertices,m_spUVTree.isValid());
#endif

	if(!m_elements || !m_vertices)
	{
		return;
	}
	if(!m_spUVTree.isValid())
	{
		m_spUVTree=create("SpatialTreeI.QuadTree");
	}
	if(!m_spUVTree.isValid())
	{
		return;
	}

#if	FE_SS_DEBUG
	feLog("SurfaceSearchable::prepareForSearchByUV building spatial tree\n");
#endif

	m_pUV3Array=(SpatialVector*)reallocate(m_pUV3Array,
			m_vertices*sizeof(SpatialVector));
	for(U32 index=0;index<m_vertices;index++)
	{
		m_pUV3Array[index]=m_pUVArray[index];

#if	FE_SS_DEBUG
		feLog("  prep %d triangle %d point %d primitive %d partition %d"
				" uv %s  %s\n",
				index,
				m_pTriangleIndexArray[index],
				m_pPointIndexArray[index],
				m_pPrimitiveIndexArray[index],
				m_pPartitionIndexArray[index],
				c_print(m_pUVArray[index]),
				c_print(m_pUV3Array[index]))
#endif
	}

	m_spUVTree->setRefinement(0);
	m_spUVTree->populate(m_pElementArray,m_pUV3Array,m_pNormalArray,
			m_pUVArray,m_pColorArray,m_pTriangleIndexArray,m_pPointIndexArray,
			m_pPrimitiveIndexArray,m_pPartitionIndexArray,
			m_elements,m_vertices,SpatialVector(0.0,0.0,0.0),0.0);
}

SpatialTransform SurfaceSearchable::sample(Vector2 a_uv) const
{
//	feLog("SurfaceSearchable::sample uv %s\n",c_print(a_uv));

	sp<SurfaceSearchable::ImpactI> spImpact=nearestPoint(a_uv);
	if(spImpact.isNull())
	{
		feLog("SurfaceSearchable::sample spImpact invalid\n");
		SpatialTransform result;
		setIdentity(result);
		return result;
	}

	const I32 tri=spImpact->triangleIndex();
	const SpatialBary barycenter=spImpact->barycenter();

//	feLog("SurfaceSearchable::sample uv %s tri %d bary %s\n",
//			c_print(a_uv),tri,c_print(barycenter));

//~	return reinterpret_cast<const SurfaceBase* const>(this)->sample(
//~			tri,barycenter);
	return SurfaceBase::sample(tri,barycenter);
}

void SurfaceSearchable::Impact::copy(sp<SpatialTreeI::Hit>& a_rspHit)
{
	setUV(a_rspHit->uv());
	setDu(a_rspHit->du());
	setDv(a_rspHit->dv());
	setTangent(SpatialVector(0,0,0));
	setBarycenter(a_rspHit->barycenter());
	setTriangleIndex(a_rspHit->triangleIndex());
	setPrimitiveIndex(a_rspHit->primitiveIndex());
	setNearestSubIndex(-1);
	setPartitionIndex(a_rspHit->partitionIndex());
	setDirection(a_rspHit->direction());
	setDistance(a_rspHit->distance());
	setIntersectionLocal(a_rspHit->intersection());

	const I32* pPointIndex=a_rspHit->pointIndex();
	setPointIndex0(pPointIndex[0]);
	setPointIndex1(pPointIndex[1]);

	const SpatialVector* pVertex=a_rspHit->vertex();
	setVertex0(pVertex[0]);
	setVertex1(pVertex[1]);

	const SpatialVector* pNormal=a_rspHit->normal();
	setNormal0(pNormal[0]);
	setNormal1(pNormal[1]);

	//* should be done in resolveImpact
//	const SpatialVector* normalArray=a_rspHit->normal();
//	const SpatialVector weightedNormal=fe::location(a_rspHit->barycenter(),
//			normalArray[0],normalArray[1],normalArray[2]);
//	setNormalLocal(unitSafe(weightedNormal));
}

Array< sp<SurfaceI::ImpactI> > SurfaceSearchable::nearestPoints(
		const Vector2& a_uv,Real a_maxDistance,U32 a_hitLimit) const
{
#if FE_SS_DEBUG
	feLog("SurfaceSearchable::nearestPoints(%s) verts %d tree %d\n",
			c_print(a_uv),m_vertices,m_spUVTree.isValid());
#endif

	Array< sp<SurfaceI::ImpactI> > impactArray;

	if(!m_elements || !m_vertices || !m_spUVTree.isValid())
	{
		return impactArray;
	}

	SpatialVector origin(a_uv);
	Array< sp<SpatialTreeI::Hit> > hitArray;
	const BWORD anyHit=FALSE;
	m_spUVTree->nearestPoint(origin,a_maxDistance,anyHit,a_hitLimit,
			m_spPartition,hitArray);
	const U32 hitCount=hitArray.size();
	if(!hitCount)
	{
#if FE_SS_DEBUG
		feLog("SurfaceSearchable::nearestPoints(%s) no hits\n",
				m_spUVTree.isValid());
#endif
		return impactArray;
	}

	impactArray.resize(hitCount);

	for(U32 m=0;m<hitCount;m++)
	{
		sp<SpatialTreeI::Hit>& rspHit=hitArray[m];
		sp<Impact> spImpact=m_pImpactPool->get();
		spImpact->copy(rspHit);
		spImpact->setSurface(this);
		spImpact->setOrigin(origin);
		impactArray[m]=spImpact;
	}

	return impactArray;
}

sp<SurfaceSearchable::ImpactI> SurfaceSearchable::nearestPoint(
		const Vector2& a_uv) const
{
	if(!m_elements || !m_vertices || !m_spUVTree.isValid())
	{
		feLog("SurfaceSearchable::nearestPoint(%s) verts %d tree %d\n",
				c_print(a_uv),m_vertices,m_spUVTree.isValid());

		return sp<ImpactI>(NULL);
	}

	SpatialVector origin(a_uv);
	const Real maxDistance= -1.0;
	const U32 hitLimit=1;
	const BWORD anyHit=FALSE;
	Array< sp<SpatialTreeI::Hit> > hitArray;
	m_spUVTree->nearestPoint(origin,maxDistance,anyHit,hitLimit,
			m_spPartition,hitArray);
	const U32 hitCount=hitArray.size();
	if(!hitCount)
	{
		return sp<ImpactI>(NULL);
	}

	sp<SpatialTreeI::Hit>& rspHit=hitArray[0];

	sp<Impact> spImpact=m_pImpactPool->get();
	spImpact->copy(rspHit);
	spImpact->setSurface(this);
	spImpact->setOrigin(origin);

	//* TODO reset vertices to original locations, not UV substitutes

	return spImpact;
}

Array< sp<SurfaceI::ImpactI> > SurfaceSearchable::nearestPoints(
		const SpatialVector& a_origin,Real a_maxDistance,U32 a_hitLimit,
		sp<PartitionI> a_spPartition) const
{
	Array< sp<SurfaceI::ImpactI> > impactArray;

	if(!m_elements || !m_vertices || !m_spSpatialTree.isValid())
	{
		return impactArray;
	}

	Array< sp<SpatialTreeI::Hit> > hitArray;
	const BWORD anyHit=FALSE;
	m_spSpatialTree->nearestPoint(a_origin,a_maxDistance,anyHit,a_hitLimit,
			a_spPartition.isValid()? a_spPartition: m_spPartition,hitArray);
	const U32 hitCount=hitArray.size();
	if(!hitCount)
	{
#if FE_SS_DEBUG
		feLog("SurfaceSearchable::nearestPoints no hits\n");
#endif
		return impactArray;
	}

	impactArray.resize(hitCount);

	for(U32 m=0;m<hitCount;m++)
	{
		sp<SpatialTreeI::Hit>& rspHit=hitArray[m];
		sp<Impact> spImpact=m_pImpactPool->get();
		spImpact->copy(rspHit);
		spImpact->setSurface(this);
		spImpact->setOrigin(a_origin);
		impactArray[m]=spImpact;
	}

	return impactArray;
}

sp<SurfaceSearchable::ImpactI> SurfaceSearchable::nearestPoint(
		const SpatialVector& a_origin,Real a_maxDistance,BWORD a_anyHit) const
{
#if FE_SS_DEBUG
	feLog("SurfaceSearchable::nearestPoint elements %d vertices %d tree %d\n",
			m_elements,m_vertices,m_spSpatialTree.isValid());
#endif

	if(!m_elements || !m_vertices || !m_spSpatialTree.isValid())
	{
		return sp<ImpactI>(NULL);
	}

	Array< sp<SpatialTreeI::Hit> > hitArray;
	const U32 hitLimit=1;	//* TODO param multi-weighting
	m_spSpatialTree->nearestPoint(a_origin,a_maxDistance,a_anyHit,hitLimit,
			m_spPartition,hitArray);
	const U32 hitCount=hitArray.size();
	if(!hitCount)
	{
#if FE_SS_DEBUG
		feLog("SurfaceSearchable::nearestPoint no hits\n");
#endif
		return sp<ImpactI>(NULL);
	}

	sp<Impact> spImpact0=m_pImpactPool->get();
	spImpact0->copy(hitArray[0]);
	spImpact0->setSurface(this);
	spImpact0->setOrigin(a_origin);

#if FE_SS_TREE_HIT_DEBUG
	feLog("\n");
	for(U32 m=0;m<hitCount;m++)
	{
		sp<SpatialTreeI::Hit>& rspHit=hitArray[m];
		feLog("Hit %d/%d dist %.6G  inter %s\n", m,hitCount,
				rspHit->distance(),c_print(rspHit->intersection()));
		feLog("  face %d triangleIndex %d bary %s\n",rspHit->face(),
				rspHit->triangleIndex(),c_print(rspHit->barycenter()));
		for(U32 n=0;n<3;n++)
		{
			feLog("  v%d %s n %s\n",n,c_print(rspHit->vertex()[n]),
					c_print(rspHit->normal()[n]));
/*
			FEASSERT(fabs(magnitude(rspHit->vertex()[n]))<1e9);
			FEASSERT(fabs(magnitude(rspHit->normal()[n]))<1.1);
			if(fabs(magnitude(rspHit->vertex()[n]))>1e9 ||
					fabs(magnitude(rspHit->normal()[n]))>1.1)
			{
				exit(1);
			}
*/
		}
	}
#endif

	return spImpact0;
	//* TODO clean up blending is still needed

	Real hitWeight[hitLimit];
	hitWeight[0]=1.0;

#if TRUE
	//* simply closest
	const Real sum=1.0;
	for(U32 m=1;m<hitCount;m++)
	{
		hitWeight[m]=0.0;
	}
#endif

	// TODO pick single best hit
	U32 bestHit=0;
	Real bestScore=0.0;
	for(U32 m=0;m<hitCount;m++)
	{
		const sp<SpatialTreeI::Hit>& rspHit=hitArray[m];
		Real thisScore=0.0;
		const SpatialVector diff=a_origin-rspHit->intersection();
		const Real mag=magnitude(diff);
		if(mag<1e-6)
		{
			thisScore=1.0;
		}
		else
		{
			const SpatialVector to=diff/mag;
			const SpatialVector norm=unitSafe(location(rspHit->barycenter(),
					rspHit->normal()[0],rspHit->normal()[1],
					rspHit->normal()[2]));
			const Real to_dot=dot(to,norm);
			thisScore=(to_dot>0.0)? to_dot: 0.0;
#if FE_SS_TREE_HIT_DEBUG
			feLog("  to %s\n",c_print(to));
#endif
		}
		if(!m)
		{
			bestScore=thisScore;
		}
		else if(thisScore>bestScore)
		{
			bestScore=thisScore;
			bestHit=m;
		}
#if FE_SS_TREE_HIT_DEBUG
		feLog("Score %d/%d %.6G best %.6G\n",
				m,hitCount,thisScore,bestScore);
#endif
	}
	//* single weighted
	for(U32 m=0;m<hitCount;m++)
	{
		hitWeight[m]=0.0;
	}
	hitWeight[bestHit]=1.0;

	sp<SpatialTreeI::Hit>& rspHit=hitArray[bestHit];
	sp<Impact> spImpact=m_pImpactPool->get();
	spImpact->copy(rspHit);
	spImpact->setSurface(this);
	spImpact->setOrigin(a_origin);

#if FALSE
	//* reevaluate barycenter based on normals
//	const SpatialVector to=unit(a_origin-rspHit->intersection());
	const SpatialVector norm=unitSafe(location(rspHit->barycenter(),
			rspHit->normal()[0],rspHit->normal()[1],rspHit->normal()[2]));
	SpatialBary barynormal;
	barynormal.solve(rspHit->normal()[0],rspHit->normal()[1],
			rspHit->normal()[2],norm);
	spImpact->setBarycenter(barynormal);
	const SpatialVector intersection=location(barynormal,
			rspHit->vertex()[0],rspHit->vertex()[1],rspHit->vertex()[2]);
	const SpatialVector toward=intersection-a_origin;
	const Real range=magnitude(toward);
	spImpact->setDistance(range);
	spImpact->setDirection(range>0.0? toward/range: toward);
#endif

	if(hitCount>1)
	{
		SpatialVector intersection;
		SpatialVector normal;
		set(intersection);
		set(normal);
		for(U32 m=0;m<hitCount;m++)
		{
			const Real fraction=hitWeight[m]/sum;
			intersection+=fraction*hitArray[m]->intersection();

			const SpatialVector* norms=hitArray[m]->normal();
			const SpatialBary& bary=hitArray[m]->barycenter();
			normal+=fraction*location(bary,norms[0],norms[1],norms[2]);
		}
		normalizeSafe(normal);
		const SpatialVector to=intersection-a_origin;
		const Real mag=magnitude(to);
		spImpact->setDistance(mag);
		spImpact->setDirection(mag>1e-6? to/mag: to);

#if FE_SS_TREE_HIT_DEBUG
		feLog("Blend %.6G %s\n",
				spImpact->distance(),c_print(intersection));
#endif
	}

/*
	feLog("dist %.6G dir %s\n",spImpact->distance(),
			c_print(spImpact->direction()));
	feLog("normal0 %s\n",c_print(spImpact->normal0()));
	feLog("normal1 %s\n",c_print(spImpact->normal1()));
	feLog("normal2 %s\n",c_print(spImpact->normal2()));
*/

	return spImpact;
}

Array< sp<SurfaceI::ImpactI> > SurfaceSearchable::rayImpacts(
		const SpatialVector& a_origin,const SpatialVector& a_direction,
		Real a_maxDistance,U32 a_hitLimit) const
{
	Array< sp<SurfaceI::ImpactI> > impactArray;

	if(!m_elements || !m_vertices || !m_spSpatialTree.isValid())
	{
		return impactArray;
	}

	Array< sp<SpatialTreeI::Hit> > hitArray;
	const Real distance=m_spSpatialTree->rayImpact(a_origin,a_direction,
			a_maxDistance,FALSE,a_hitLimit,m_spPartition,hitArray);
	const U32 hitCount=hitArray.size();

	if(!hitCount || (a_maxDistance>0.0 && distance>a_maxDistance))
	{
		return impactArray;
	}

	impactArray.resize(hitCount);
	for(U32 m=0;m<hitCount;m++)
	{
		sp<SpatialTreeI::Hit>& rspHit=hitArray[m];
		sp<Impact> spImpact=m_pImpactPool->get();
		spImpact->copy(rspHit);
		spImpact->setSurface(this);
		spImpact->setOrigin(a_origin);
		spImpact->setDirection(a_direction);
		impactArray[m]=spImpact;
	}
	return impactArray;
}

sp<SurfaceSearchable::ImpactI> SurfaceSearchable::rayImpact(
		const SpatialVector& a_origin,const SpatialVector& a_direction,
		Real a_maxDistance,BWORD a_anyHit) const
{
	if(!m_elements || !m_vertices || !m_spSpatialTree.isValid())
	{
		return sp<ImpactI>(NULL);
	}

	const U32 hitLimit=1;
	Array< sp<SpatialTreeI::Hit> > hitArray;
	const Real distance=m_spSpatialTree->rayImpact(a_origin,a_direction,
			a_maxDistance,a_anyHit,hitLimit,m_spPartition,hitArray);
	const U32 hitCount=hitArray.size();
	if(!hitCount || (a_maxDistance>0.0 && distance>a_maxDistance))
	{
		return sp<ImpactI>(NULL);
	}

	sp<SpatialTreeI::Hit>& rspHit=hitArray[0];

	sp<Impact> spImpact=m_pImpactPool->get();
	spImpact->copy(rspHit);
	spImpact->setSurface(this);
	spImpact->setOrigin(a_origin);
	spImpact->setDirection(a_direction);
	return spImpact;
}

void SurfaceSearchable::calcBoundingSphere(void)
{
	if(!m_vertices)
	{
		set(m_center);
		m_radius=0.0;
		return;
	}

#if FALSE
	//* by center of "mass"
	m_center=m_pVertexArray[0];
	for(U32 m=1;m<m_vertices;m++)
	{
		m_center+=m_pVertexArray[m];
	}
	m_center*=1.0/m_vertices;
#else
	//* by bounding box (not optimal, but quick)
	SpatialVector min=m_pVertexArray[0];
	SpatialVector max=min;
	for(U32 m=1;m<m_vertices;m++)
	{
		const SpatialVector& rVertex=m_pVertexArray[m];
		for(U32 n=0;n<3;n++)
		{
			if(min[n]>rVertex[n])
			{
				min[n]=rVertex[n];
			}
			if(max[n]<rVertex[n])
			{
				max[n]=rVertex[n];
			}
		}
	}
	m_center=0.5*(min+max);
#endif

	Real max2=0.0;
	for(U32 m=0;m<m_vertices;m++)
	{
		const SpatialVector radial=m_pVertexArray[m]-m_center;
		const Real dist2=magnitudeSquared(radial);
		if(max2<dist2)
		{
			max2=dist2;
		}
	}
	m_radius=sqrt(max2);

#if FE_SS_DEBUG
	feLog("SurfaceSearchable::calcBoundingSphere"
			" vertices %d center %s radius %.6G\n",
			m_vertices,c_print(m_center),m_radius);
#endif

	const I32 partCount=partitionCount();
//	feLog("SurfaceSearchable::calcBoundingSphere partCount %d\n",partCount);

	if(partCount)
	{
		Array<SpatialVector> partMin(partCount);
		Array<SpatialVector> partMax(partCount);

		for(I32 partIndex=0;partIndex<partCount;partIndex++)
		{
			set(partMin[partIndex],FLT_MAX,FLT_MAX,FLT_MAX);
			set(partMax[partIndex],-FLT_MAX,-FLT_MAX,-FLT_MAX);
		}

		for(U32 m=0;m<m_vertices;m++)
		{
			const I32 partIndex=m_pPartitionIndexArray[m];
			SpatialVector& rPartMin=partMin[partIndex];
			SpatialVector& rPartMax=partMax[partIndex];

			const SpatialVector& rVertex=m_pVertexArray[m];

			for(U32 n=0;n<3;n++)
			{
				if(rPartMin[n]>rVertex[n])
				{
					rPartMin[n]=rVertex[n];
				}
				if(rPartMax[n]<rVertex[n])
				{
					rPartMax[n]=rVertex[n];
				}
			}
		}

		m_partitionSphere.resize(partCount);

		for(I32 partIndex=0;partIndex<partCount;partIndex++)
		{
			m_partitionSphere[partIndex]=
					0.5*(partMin[partIndex]+partMax[partIndex]);
		}

		partMin.clear();
		partMax.clear();

		Array<Real> partMax2(partCount,0.0);
		for(U32 m=0;m<m_vertices;m++)
		{
			const I32 partIndex=m_pPartitionIndexArray[m];
			Real& rPartMax2=partMax2[partIndex];

			const SpatialVector radial=m_pVertexArray[m]-
					SpatialVector(m_partitionSphere[partIndex]);
			const Real dist2=magnitudeSquared(radial);
			if(rPartMax2<dist2)
			{
				rPartMax2=dist2;
			}
		}

		for(I32 partIndex=0;partIndex<partCount;partIndex++)
		{
			m_partitionSphere[partIndex][3]=sqrt(partMax2[partIndex]);

//			feLog("  part %d sphere %s\n",partIndex,
//					c_print(m_partitionSphere[partIndex]));
		}
	}
	else
	{
		m_partitionSphere.clear();
	}
}

void SurfaceSearchable::partitionWith(String a_attributeName)
{
	if(m_partitionAttr==a_attributeName)
	{
		return;
	}

	clear();

	m_partitionAttr=a_attributeName;

	if(a_attributeName.empty())
	{
		m_spPartition=NULL;
		return;
	}
	if(m_spPartition.isNull())
	{
		m_spPartition=sp<PartitionI>(new PartitionSearchable());
	}
}

Vector4 SurfaceSearchable::partitionSphere(U32 a_index) const
{
	if(a_index>=m_partitionSphere.size())
	{
		return SurfaceBase::partitionSphere(a_index);
	}

	return m_partitionSphere[a_index];
}

sp<PartitionI> SurfaceSearchable::createPartition(void)
{
#if FALSE
	//* duplicate list of partition names

	sp<PartitionI> spPartition(new PartitionSearchable());
	if(m_spPartition.isValid())
	{
		const I32 count=m_spPartition->partitionCount();
		for(I32 m=0;m<count;m++)
		{
			spPartition->lookup(m_spPartition->partitionName(m));
		}
	}
#else
	sp<PartitionI> spPartition=m_spPartition.isValid()?
			m_spPartition->clone(): sp<PartitionI>(NULL);
#endif

	return spPartition;
}

sp<PartitionI> SurfaceSearchable::PartitionSearchable::clone(void) const
{
	sp<PartitionSearchable> spClone(new PartitionSearchable());

	spClone->m_stringMap=m_stringMap;
	spClone->m_filterString=m_filterString;
	spClone->m_filterMethod=m_filterMethod;
	spClone->m_filterArray=m_filterArray;
	spClone->m_matchCount=m_matchCount;
	spClone->m_tweaked=m_tweaked;

	return spClone;
}

String SurfaceSearchable::PartitionSearchable::partitionName(U32 a_index) const
{
	for(std::map<String,I32>::const_iterator it=m_stringMap.begin();
			it!=m_stringMap.end(); it++)
	{
		if(it->second == I32(a_index))
		{
			return it->first;
		}
	}
	return "";
}

I32 SurfaceSearchable::PartitionSearchable::lookup(String a_partitionString)
{
	if(m_stringMap.find(a_partitionString)==m_stringMap.end())
	{
		const I32 partitionIndex=partitionCount();

		m_stringMap[a_partitionString]=partitionIndex;

		if(m_tweaked)
		{
			feLog("SurfaceSearchable::PartitionSearchable::lookup"
					" tweaked partition list can not be reevaluated\n");
		}

		const BWORD matched=(!m_tweaked &&
				((m_filterString==".*" ||
				a_partitionString.match(m_filterString))==
				(m_filterMethod==PartitionI::e_matchRegex)));

		m_filterArray.resize(partitionIndex+1);
		m_filterArray[partitionIndex]=matched;
		m_matchCount+=matched;
	}

	return m_stringMap[a_partitionString];
}

I32 SurfaceSearchable::PartitionSearchable::select(String a_filterString,
	PartitionI::FilterMethod a_filterMethod)
{
	if(!m_tweaked && m_filterString==a_filterString &&
			m_filterMethod==a_filterMethod)
	{
		return m_matchCount;
	}

	m_tweaked=FALSE;
	m_filterString=a_filterString;
	m_filterMethod=a_filterMethod;

	m_filterArray.resize(m_stringMap.size());

	if(a_filterString=="")
	{
		for(std::map<String,I32>::const_iterator it=m_stringMap.begin();
				it!=m_stringMap.end(); it++)
		{
			m_filterArray[it->second]=FALSE;
		}

		m_matchCount=0;
	}
	else if(a_filterString==".*")
	{
		for(std::map<String,I32>::const_iterator it=m_stringMap.begin();
				it!=m_stringMap.end(); it++)
		{
			m_filterArray[it->second]=TRUE;
		}

		m_matchCount=m_stringMap.size();
	}
	else
	{
		m_matchCount=0;
		for(std::map<String,I32>::const_iterator it=m_stringMap.begin();
				it!=m_stringMap.end(); it++)
		{
			const BWORD matched=(it->first.match(m_filterString)==
					(m_filterMethod==PartitionI::e_matchRegex));
			m_filterArray[it->second]=matched;
			m_matchCount+=matched;
		}
	}

	return m_matchCount;
}

BWORD SurfaceSearchable::PartitionSearchable::add(String a_string)
{
	if(m_stringMap.find(a_string)!=m_stringMap.end())
	{
		I32& rMatched=m_filterArray[m_stringMap[a_string]];
		if(!rMatched)
		{
			rMatched=TRUE;
			m_tweaked=TRUE;
			m_filterString="";
			m_filterMethod=PartitionI::e_matchRegex;
			m_matchCount++;
			return TRUE;
		}
	}

	return FALSE;
}

BWORD SurfaceSearchable::PartitionSearchable::remove(String a_string)
{
	if(m_stringMap.find(a_string)!=m_stringMap.end())
	{
		I32& rMatched=m_filterArray[m_stringMap[a_string]];
		if(rMatched)
		{
			rMatched=FALSE;
			m_tweaked=TRUE;
			m_filterString="";
			m_filterMethod=PartitionI::e_matchRegex;
			m_matchCount--;
			return TRUE;
		}
	}

	return FALSE;
}

} /* namespace ext */
} /* namespace fe */
