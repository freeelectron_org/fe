/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_CFO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void CurveFollowOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<SpatialVector>("fromPoint")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("fromPoint","label")="From Point";
	catalog<String>("fromPoint","hint")=
			"Location of object targeting the curve.";

	catalog<Real>("lookAhead")=0.01;
	catalog<Real>("lookAhead","low")=-0.1;
	catalog<Real>("lookAhead","high")=0.1;
	catalog<Real>("lookAhead","min")=-1.0;
	catalog<Real>("lookAhead","max")=1.0;
	catalog<String>("lookAhead","label")="Look Ahead";
	catalog<String>("lookAhead","hint")=
			"Advance target from nearest point on curve, in world units.";

	catalog<I32>("subSteps")=32;
	catalog<I32>("subSteps","high")=100;
	catalog<I32>("subSteps","max")=10000;
	catalog<String>("subSteps","label")="Substeps";
	catalog<String>("subSteps","hint")=
			"Sample points along path to target.";

	catalog<I32>("stepRadius")=2;
	catalog<I32>("stepRadius","high")=10;
	catalog<I32>("stepRadius","max")=100;
	catalog<String>("stepRadius","label")="Step Radius";
	catalog<String>("stepRadius","hint")=
			"Sample points outward to compute curvature.";

	catalog<Real>("smoothCurvature")=0.0;
	catalog<Real>("smoothCurvature","min")=0.0;
	catalog<Real>("smoothCurvature","max")=1.0;
	catalog<String>("smoothCurvature","label")="Smooth Curvature";

	catalog<SpatialVector>("target")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("target","IO")="output";
	catalog<String>("target","label")="Target";

	catalog<Real>("curvatureMin")=0.0;
	catalog<Real>("curvatureMin","min")=-1e6;
	catalog<Real>("curvatureMin","max")=1e6;
	catalog<String>("curvatureMin","IO")="output";
	catalog<String>("curvatureMin","label")="Minimum Curvature";

	catalog<Real>("curvatureMax")=0.0;
	catalog<Real>("curvatureMax","min")=-1e6;
	catalog<Real>("curvatureMax","max")=1e6;
	catalog<String>("curvatureMax","IO")="output";
	catalog<String>("curvatureMax","label")="Maximum Curvature";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";

	catalog< sp<Component> >("Input Curve");

	//* no auto-copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="";

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_solid);
	m_spSolid->setBackfaceCulling(FALSE);
	m_spSolid->setLineWidth(1.0);

	m_spOverlay=new DrawMode();
	m_spOverlay->setDrawStyle(DrawMode::e_solid);
	m_spOverlay->setBackfaceCulling(FALSE);
	m_spOverlay->setLineWidth(1.0);
	m_spOverlay->setLit(FALSE);
}

void CurveFollowOp::handle(Record& a_rSignal)
{
#if FE_CFO_DEBUG
	feLog("CurveFollowOp::handle\n");
#endif

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		const Color black(0.0,0.0,0.0,1.0);
		const Color white(1.0,1.0,1.0,1.0);

		spDrawBrush->pushDrawMode(m_spSolid);
		spDrawOverlay->pushDrawMode(m_spOverlay);

		sp<ViewI> spView=spDrawOverlay->view();

		const I32 count=m_feedbackArray.size();
		for(I32 index=0;index<count;index++)
		{
			Feedback& rFeedback=m_feedbackArray[index];

			const SpatialVector projected=
					spView->project(rFeedback.m_location,
					ViewI::e_perspective);

			const U32 margin=1;
			drawLabel(spDrawOverlay,projected,
					rFeedback.m_text.c_str(),
					FALSE,margin,white,NULL,&black);
		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

		return;
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,
			e_point,e_position)) return;

	if(catalogOrDefault<bool>("Input Curve","replaced",true))
	{
//		feLog("CurveFollowOp::handle input replaced\n");
		m_spInputAccessible=NULL;
		m_spCurve=NULL;
	}

	if(m_spInputAccessible.isNull())
	{
		if(!access(m_spInputAccessible,"Input Curve")) return;

		m_spCurve=NULL;
	}

	if(m_spCurve.isNull())
	{
		if(!access(m_spCurve,m_spInputAccessible)) return;
	}

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal,e_warning);

	const SpatialVector fromPoint=catalog<SpatialVector>("fromPoint");
	const Real lookAhead=catalog<Real>("lookAhead");

	SpatialVector intersection(0,0,0);
	Vector2 uv(0,0);

	sp<SurfaceI::ImpactI> spImpact=m_spCurve->nearestPoint(fromPoint);
	if(spImpact.isValid())
	{
		intersection=spImpact->intersection();
		uv=spImpact->uv();
	}

	const I32 subSteps=catalog<I32>("subSteps");

	//* TODO param
	const BWORD worldUnits(TRUE);

	Real targetU(0);
	Real incU(0);

	if(worldUnits)
	{
		//* lookAhead as world units
		Real distance(0.0);
		targetU=uv[0];
		SpatialVector lastPoint(intersection);

		for(I32 stage=0;stage<4;stage++)
		{
			if(interrupted())
			{
				break;
			}

			const Real uvStep=pow(0.1,stage+1);
			for(I32 index=0;index<=10;index++)
			{
				if(interrupted())
				{
					break;
				}

				const Real newTargetU=fmod(targetU+uvStep,1.0);
				const SpatialVector point=
						m_spCurve->samplePoint(Vector2(newTargetU,uv[1]));

				const Real newDistance=distance+magnitude(point-lastPoint);
//				feLog("uvStep %.6G newTargetU %.6G newDistance %.6G point %s\n",
//						uvStep,newTargetU,newDistance,c_print(point));
				if(newDistance>lookAhead)
				{
					break;
				}

				targetU=newTargetU;
				distance=newDistance;
				lastPoint=point;
			}
		}

		const Real lookAheadU=fmod(targetU-uv[0]+1.0,1.0);
		incU=lookAheadU/subSteps;

//		feLog("uv[0] %.6G lookAhead %.6G"
//				" targetU %.6G lookAheadU %.6G incU %.6G\n",
//				uv[0],lookAhead,targetU,lookAheadU,incU);
	}
	else
	{
		//* lookAhead as delta U
		targetU=fmod(uv[0]+lookAhead+1.0,1.0);
		incU=lookAhead/subSteps;
	}

	const SpatialVector target=m_spCurve->samplePoint(Vector2(targetU,uv[1]));

	const I32 stepRadius=catalog<I32>("stepRadius");
	const Real smoothCurvature=catalog<Real>("smoothCurvature");

	const Real smoothKeep=0.999*pow(smoothCurvature,0.5);
	const Real smoothKeep1=1.0-smoothKeep;
	Real smoothedCurvature=0.0;
	Real curvatureMin(0);
	Real curvatureMax(0);

	if(fabs(incU)<1e-6)
	{
		m_feedbackArray.resize(0);
	}
	else
	{
		const I32 sampleCount=subSteps-2*stepRadius;
		m_feedbackArray.resize(sampleCount);

		for(I32 sample=0;sample<sampleCount;sample++)
		{
			if(interrupted())
			{
				break;
			}

			targetU=fmod(uv[0]+(sample)*incU+1.0,1.0);
			const SpatialVector point0=
					m_spCurve->samplePoint(Vector2(targetU,uv[1]));

			targetU=fmod(uv[0]+(sample+stepRadius)*incU+1.0,1.0);
			const SpatialVector point1=
					m_spCurve->samplePoint(Vector2(targetU,uv[1]));

			targetU=fmod(uv[0]+(sample+2*stepRadius)*incU+1.0,1.0);
			const SpatialVector point2=
					m_spCurve->samplePoint(Vector2(targetU,uv[1]));

			const SpatialVector v01=point1-point0;
			const SpatialVector v12=point2-point1;
			const Real stepLength=0.5*(magnitude(v01)+magnitude(v12));
			const Real angle=asin(cross(unitSafe(v01),unitSafe(v12))[2]);

			const Real curvature=tan(angle)/stepLength;
			smoothedCurvature=
					smoothKeep*smoothedCurvature+
					smoothKeep1*curvature;

			if(curvatureMin>smoothedCurvature)
			{
				curvatureMin=smoothedCurvature;
			}
			if(curvatureMax<smoothedCurvature)
			{
				curvatureMax=smoothedCurvature;
			}

//			feLog("sample %d/%d curvature %.6G smoothedCurvature %.6G\n",
//					sample,sampleCount,curvature,smoothedCurvature);

			Feedback& rFeedback=m_feedbackArray[sample];
			rFeedback.m_location=point1;
			rFeedback.m_text.sPrintf("%.4f",curvature);

			spDrawI->drawPoints(&point1,NULL,1,FALSE,NULL);
		}
	}

#if FE_CFO_DEBUG
	feLog("CurveFollowOp::handle uv %s\n",c_print(uv));
	feLog("CurveFollowOp::handle intersection %s\n",c_print(intersection));
	feLog("CurveFollowOp::handle target %s\n",c_print(target));
#endif

	spDrawI->drawPoints(&target,NULL,1,FALSE,NULL);

	catalog<SpatialVector>("target")=target;
	catalog<Real>("curvatureMin")=curvatureMin;
	catalog<Real>("curvatureMax")=curvatureMax;

	if(spDrawGuide.isValid())
	{
		const Color red(1.0,0.0,0.0,1.0);
		const Color yellow(1.0,1.0,0.0,1.0);
		const Color blue(0.5,0.5,1.0,1.0);

		SpatialVector line[2];

		line[0]=fromPoint;
		line[1]=fromPoint+SpatialVector(0.0,0.0,4.0);

		spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,false,&blue);

		line[0]=intersection;
		line[1]=intersection+SpatialVector(0.0,0.0,4.0);

		spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,false,&red);

		line[0]=target;
		line[1]=target+SpatialVector(0.0,0.0,4.0);

		spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,false,&yellow);
	}

	String summary;
//	summary.sPrintf("%s to %s\n",
//			c_print(intersection),c_print(intersection));
	summary.sPrintf("min %.4f max %.4f\n",curvatureMin,curvatureMax);
	catalog<String>("summary")=summary;

#if FE_CFO_DEBUG
	feLog("CurveFollowOp::handle done\n");
#endif
}
