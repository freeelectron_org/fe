/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceAttrPromoteOp_h__
#define __operator_SurfaceAttrPromoteOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Project attributes to another rate

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAttrPromoteOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceAttrPromoteOp>
{
	public:
				SurfaceAttrPromoteOp(void)									{}
virtual			~SurfaceAttrPromoteOp(void)									{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:

		enum	AttrType
				{
					e_none,
					e_string,
					e_integer,
					e_real,
					e_spatialVector
				};

		void	copyAttribute(AttrType a_attrType,
						sp<SurfaceAccessorI>& a_rspSourceAttr,
						I32 a_sourceIndex,I32 a_sourceSubIndex,
						sp<SurfaceAccessorI>& a_rspDestAttr,
						I32 a_destIndex,I32 a_destSubIndex);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceAttrPromoteOp_h__ */
