import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                'fexDataToolDLLib',
                'fexWindowLib' ]

    tests = [   'xKey' ]

    for t in tests:
        exe = module.Exe(t)

        forge.deps([t + "Exe"], deplibs)
