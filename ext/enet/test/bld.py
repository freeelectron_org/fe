import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexNetworkDLLib",
                        "fexEnetDLLib" ]

    tests = [   'xEnetRawClient',
                'xEnetRawServer' ]

    for t in tests:
        exe = module.Exe(t)

        exe.linkmap = {}
        if forge.fe_os == "FE_LINUX":
            exe.linkmap["enet_libs"] = "-lenet"
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            exe.linkmap["enet_libs"] = "enet.lib"

        forge.deps([t + "Exe"], deplibs)
