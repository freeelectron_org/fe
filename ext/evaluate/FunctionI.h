/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __evaluator_FunctionI_h__
#define __evaluator_FunctionI_h__

namespace fe
{
namespace ext
{

/// A bundling of src/data type into a concrete notion
class FE_DL_EXPORT Aggregate : public Counted, CastableAs<Aggregate>
{
	public:
		Aggregate(void);
virtual	~Aggregate(void);

		void	bakeTypes(sp< AccessorSet > a_as, sp< Layout > a_layout)
		{
			m_spLayout = a_layout;
			m_spAS = a_as;
			m_spAS->populate(a_layout);
			bakeTypes();
		}
		void	bakeTypes(void)
		{
			unsigned int i_as = m_spAS->size();
			m_types.resize(i_as);
			m_BAs.resize(i_as);
			for(unsigned int i_a = 0; i_a < i_as ; i_a++)
			{
				m_BAs[i_a] = (*(m_spAS))[i_a];
				m_types[i_a] = m_BAs[i_a]->attribute()->type();
			}
		}

		void	assign(Record &r_dst, const Record &r_src)
		{
			unsigned int i_as = m_types.size();
			for(unsigned int i_a = 0; i_a < i_as ; i_a++)
			{
				BaseAccessor *pBA = m_BAs[i_a];
				void *dst = r_dst.rawAttribute(pBA->index());
				void *src = r_src.rawAttribute(pBA->index());
				m_types[i_a]->assign(dst, src);
			}
		}

		void asUnion(sp< Aggregate > a_other)
		{
			m_types.clear();
			m_BAs.clear();

			unsigned int i_as = a_other->m_spAS->size();
			for(unsigned int i_a = 0; i_a < i_as ; i_a++)
			{
				BaseAccessor *pBA = a_other->m_BAs[i_a];
				if( m_spLayout->checkAttribute(pBA->index()) )
				{
					m_BAs.push_back(pBA);
					m_types.push_back(pBA->attribute()->type());
				}
			}
		}

		sp< Layout >					m_spLayout;

	private:
		sp< AccessorSet >				m_spAS;
		t_stdvector< sp<BaseType> >		m_types;
		t_stdvector< BaseAccessor * >	m_BAs;
};


/// Interface to add functions to an Evaluator
class FE_DL_EXPORT FunctionI: virtual public Component,
	public CastableAs<FunctionI>
{
	public:
virtual	bool			compile(sp<RecordGroup>, Record &,
							t_stdvector<WeakRecord> &, t_stdstring &)		= 0;
virtual	void			eval(Record &, t_stdvector<WeakRecord> &)			= 0;
virtual	sp<Aggregate>	&returnType(void)									= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif

