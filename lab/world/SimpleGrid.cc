/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "SimpleGrid.h"

namespace fe
{
namespace ext
{

SimpleGrid::SimpleGrid(void)
{
	set(m_thick[0],(Real)0.0,	(Real)-10.0,	(Real)0.0);
	set(m_thick[1],(Real)0.0,	(Real)0.0,	(Real)0.0);
	set(m_thick[2],(Real)0.0,	(Real)1.0,	(Real)0.0);
	set(m_thick[3],(Real)0.0,	(Real)10.0,	(Real)0.0);

	set(m_thick[4],(Real)-10.0,	(Real)0.0,	(Real)0.0);
	set(m_thick[5],(Real)0.0,	(Real)0.0,	(Real)0.0);
	set(m_thick[6],(Real)1.0,	(Real)0.0,	(Real)0.0);
	set(m_thick[7],(Real)10.0,	(Real)0.0,	(Real)0.0);

	set(m_x[0],(Real)0.0,	(Real)0.0,	(Real)0.0);
	set(m_x[1],(Real)1.0,	(Real)0.0,	(Real)0.0);

	set(m_y[0],(Real)0.0,	(Real)0.0,	(Real)0.0);
	set(m_y[1],(Real)0.0,	(Real)1.0,	(Real)0.0);

	set(m_z[0],(Real)0.0,	(Real)0.0,	(Real)0.0);
	set(m_z[1],(Real)0.0,	(Real)0.0,	(Real)1.0);

	Real f = -10.0;
	for(int i = 0; i < 10; i++)
	{
		set(m_thin[4*i+0], f,		(Real)-10.0,	(Real)0.0);
		set(m_thin[4*i+1], f,		(Real)10.0,	(Real)0.0);
		set(m_thin[4*i+2], (Real)-10.0,	f,		(Real)0.0);
		set(m_thin[4*i+3], (Real)10.0,	f,		(Real)0.0);
		f += 1.0;
	}

	f += 1.0;
	for(int i = 10; i < 20; i++)
	{
		set(m_thin[4*i+0], f,		(Real)-10.0,	(Real)0.0);
		set(m_thin[4*i+1], f,		(Real)10.0,	(Real)0.0);
		set(m_thin[4*i+2], (Real)-10.0,	f,		(Real)0.0);
		set(m_thin[4*i+3], (Real)10.0,	f,		(Real)0.0);
		f += 1.0;
	}

	m_spThickMode = new DrawMode();
	m_spThickMode->setAntialias(false);
	m_spThickMode->setDrawStyle(DrawMode::e_wireframe);
	m_spThickMode->setLineWidth(3.0);

	m_spThinMode = new DrawMode();
	m_spThinMode->setAntialias(false);
	m_spThinMode->setDrawStyle(DrawMode::e_wireframe);
	m_spThinMode->setLineWidth(1.5);
}

SimpleGrid::~SimpleGrid(void)
{
}

void SimpleGrid::handle(fe::Record &r_sig)
{
	m_asSignal.bind(r_sig.layout()->scope());
	m_asWindata.bind(r_sig.layout()->scope());
	if(m_asSignal.winData.check(r_sig))
	{
		Color grey(0.5,0.5,0.5,1.0);
		Color red(1.0,0.0,0.0,1.0);
		Color green(0.0,1.0,0.0,1.0);
		Color blue(0.0,0.0,1.0,1.0);
		sp<DrawI> spDraw(m_asWindata.drawI(m_asSignal.winData(r_sig)));

		SpatialVector offset = cfg<SpatialVector>("offset");

		for(unsigned int i = 0; i < 80; i++)
		{
			m_x_thin[i] = m_thin[i] + offset;
		}
		for(unsigned int i = 0; i < 8; i++)
		{
			m_x_thick[i] = m_thick[i] + offset;
		}
		for(unsigned int i = 0; i < 2; i++)
		{
			m_x_x[i] = m_x[i] + offset;
			m_x_y[i] = m_y[i] + offset;
			m_x_z[i] = m_z[i] + offset;
		}

#if 1
		spDraw->pushDrawMode(m_spThickMode);
		spDraw->drawLines(m_x_thick, NULL, 8, DrawI::e_discrete, false, &grey);
		spDraw->popDrawMode();
#endif

#if 1
		spDraw->pushDrawMode(m_spThinMode);
		spDraw->drawLines(m_x_thin, NULL, 80, DrawI::e_discrete, false, &grey);
		spDraw->popDrawMode();
#endif

		spDraw->pushDrawMode(m_spThickMode);
		spDraw->drawLines(m_x_x, NULL, 2, DrawI::e_discrete, false, &red);
		spDraw->drawLines(m_x_y, NULL, 2, DrawI::e_discrete, false, &green);
		spDraw->drawLines(m_x_z, NULL, 2, DrawI::e_discrete, false, &blue);
		spDraw->popDrawMode();
	}
}

} /* namespace ext */
} /* namespace fe */

