/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_Gravity_h__
#define __world_Gravity_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "solve/solve.h"
namespace fe
{
namespace ext
{
/** Universal Gravity Force

	@copydoc Gravity_info
	*/
class FE_DL_EXPORT Gravity :
	public Initialize<Gravity>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				Gravity(void);
virtual			~Gravity(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsParticle			m_asParticle;
		AsSolverParticle	m_asSolverParticle;
		AsAccumulate		m_asAccumulate;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __world_Gravity_h__ */

