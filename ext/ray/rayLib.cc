/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ray/ray.pmh>


namespace fe
{
namespace ext
{

Library* CreateRayLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->setName("default_ray");
	pLibrary->add<DrawRayTrace>("DrawI.DrawRayTrace.fe");

	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
