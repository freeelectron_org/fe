/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_SurfaceTrianglesOpenCL_h__
#define __opencl_SurfaceTrianglesOpenCL_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief OpenCL augmentation of SurfaceTrianglesAccessible

	@ingroup opencl

*//***************************************************************************/
class FE_DL_EXPORT SurfaceTrianglesOpenCL:
	public SurfaceTrianglesAccessible,
	public CastableAs<SurfaceTrianglesOpenCL>
{
	public:

					SurfaceTrianglesOpenCL(void):
						m_replicated(FALSE)									{}
virtual				~SurfaceTrianglesOpenCL(void)							{}

virtual	void		prepareForSearch(void);

	protected:

virtual	void		cache(void);

virtual	void		drawInternal(BWORD a_transformed,
							const SpatialTransform* a_pTransform,
							sp<DrawI> a_spDrawI,
							const fe::Color* a_pColor,
							sp<DrawBufferI> a_spDrawBuffer,
							sp<PartitionI> a_spPartition) const;

	private:

		BWORD	m_replicated;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_SurfaceTrianglesOpenCL_h__ */


