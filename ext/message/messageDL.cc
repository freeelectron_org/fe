/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "message/message.pmh"
#include "platform/dlCore.cc"
#include "message/NetUtil.h"
#include "message/MessageUDP.h"

using namespace fe;
using namespace ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String *> &list)
{
}

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<MessageUDP>("MessageI.MessageUDP.fe");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	NetUtil &nu = NetUtil::Instance();
	FE_MAYBE_UNUSED(nu);
}

}
