/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_memory_h__
#define __platform_memory_h__

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
//#define FE_EXPORT_MEM_VIA_FUNC
#else
#define FE_EXPORT_MEM_VIA_FUNC
#endif

#if FE_CODEGEN<=FE_DEBUG
#define FE_CHECK_NEW		TRUE
#else
#define FE_CHECK_NEW		FALSE
#endif

//* HACK something with amd64, multi-processor, or a bug causes malloc on stack
//* or maybe multi-threaded stacks can reside on either side of the heap
#if defined(__x86_64__) || defined(__aarch64__)
#define	FE_HEAP_CHECKABLE	FALSE
#else
#define	FE_HEAP_CHECKABLE	FE_CHECK_NEW
#endif

// isn't required to be the same as FE_CHECK_NEW
#if FE_CODEGEN<=FE_DEBUG
#define FE_CHECK_HEAP		FE_HEAP_CHECKABLE
#else
#define FE_CHECK_HEAP		FALSE
#endif

#define FE_LOCK_MEMORY (FE_CHECK_HEAP || FE_CHECK_NEW)

#ifndef FE_MEM_ALIGNMENT
#if FE_SSE!=0
#define FE_MEM_ALIGNMENT	16
#else
#define FE_MEM_ALIGNMENT	0
#endif
#endif

//* delete per-class mutex with last instance of class
//* not doing this can cause a python crash shutting down model_view
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64 || defined(__clang__)
#define FE_SAFE_COUNTED_MUTEX	FALSE
#else
#define FE_SAFE_COUNTED_MUTEX	TRUE
#endif

namespace fe
{

extern "C"
{

typedef void* ( FE_CDECL allocateFunction )( FE_UWORD byteCount );
typedef void ( FE_CDECL deallocateFunction )( void* pMemory );
typedef void* ( FE_CDECL reallocateFunction )( void* pMemory, FE_UWORD byteCount );
typedef void ( FE_CDECL printFunction )( const char* pAscii );

} /* extern "C" */

#if FE_COMPILER==FE_DMC
extern allocateFunction*				gs_pAllocateFunction;
extern deallocateFunction*				gs_pDeallocateFunction;
extern reallocateFunction*				gs_pReallocateFunction;
extern printFunction*					gs_pPrintFunction;
extern void*							gs_pHeapBase;
extern I32								gs_newCheck;
#else
FE_MEM_PORT	extern allocateFunction*	gs_pAllocateFunction;
FE_MEM_PORT	extern deallocateFunction*	gs_pDeallocateFunction;
FE_MEM_PORT	extern reallocateFunction*	gs_pReallocateFunction;
FE_MEM_PORT	extern printFunction*		gs_pPrintFunction;
FE_MEM_PORT	extern void*				gs_pHeapBase;
FE_MEM_PORT extern I32					gs_newCheck;
#endif

FE_MEM_PORT void * FE_CDECL ex_allocate(FE_UWORD byteCount);
FE_MEM_PORT void * FE_CDECL ex_reallocate(void *pMemory, FE_UWORD byteCount);
FE_MEM_PORT void FE_CDECL ex_deallocate(void *pMemory);

extern "C"
{

inline
void* allocate( FE_UWORD byteCount )
{
#ifdef FE_ALLOC_RAW
	return malloc(byteCount);
#endif
#ifdef FE_EXPORT_MEM_VIA_FUNC
	return ex_allocate(byteCount);
#else
#if FE_CHECK_NEW
	gs_pHeapBase=(void*)0x1;
#endif
	return gs_pAllocateFunction( byteCount );
#endif
}


inline
void deallocate( void* pMemory )
{
//	assert( gs_pDeallocateFunction );
#ifdef FE_ALLOC_RAW
	free(pMemory);
	return;
#endif

#ifdef FE_EXPORT_MEM_VIA_FUNC
	ex_deallocate(pMemory);
#else
	gs_pDeallocateFunction( pMemory );
#endif
}

inline
void* reallocate( void* pMemory, FE_UWORD byteCount )
{
#ifdef FE_ALLOC_RAW
	return realloc(pMemory, byteCount);
#endif
#ifdef FE_EXPORT_MEM_VIA_FUNC
	return ex_reallocate(pMemory, byteCount);
#else
	return gs_pReallocateFunction( pMemory, byteCount );
#endif
}

} /* extern "C" */

/**	@brief Basic memory management
	*/
class FE_DL_PUBLIC Memory
{
	public:
static	bool	isHeap(void* pPtr);
static	bool	isHeapOrCannotTell(void* pPtr);
static	bool	newWorks(void);

template<class T>
static	T*		instantiate(void);

#if FE_SAFE_COUNTED_MUTEX
static	void	markForDeath(Mutex*& a_rpMutex,I32* a_pCount);
static	void	markForDeath(RecursiveMutex*& a_rpMutex,I32* a_pCount);
static	void	clearDeadPool(void);
#endif

#if FE_LOCK_MEMORY
#if FE_SAFE_COUNTED_MUTEX
static	I32					ms_mutexCount;
static	RecursiveMutex*		ms_pMutex;
#else
static	RecursiveMutex		ms_mutex;
#endif
#endif
	private:

#if FE_SAFE_COUNTED_MUTEX
	class CountedMutex
	{
		public:
				CountedMutex(Mutex*& a_rpMutex,I32* a_pCount)
				{
					m_ppMutex= &a_rpMutex;
					m_pCount=a_pCount;
				}
				CountedMutex(RecursiveMutex*& a_rpMutex,I32* a_pCount)
				{
					m_ppMutex= reinterpret_cast<Mutex**>(&a_rpMutex);
					m_pCount=a_pCount;
				}
		Mutex**	m_ppMutex;
		I32*	m_pCount;
	};

static	std::vector<CountedMutex>	ms_deadPool;
#endif
};

template<class T>
inline	T* Memory::instantiate(void)
{
	void* ptr=fe::gs_pAllocateFunction(sizeof(T));
	return new(ptr) T();
}

#if !FE_CHECK_HEAP
inline bool Memory::isHeap(void* pPtr)
{
	return false;
}
#elif FE_OS==FE_LINUX
inline bool Memory::isHeap(void* pPtr)
{
	U8 current_stack;
//	fe_printf("Memory::isHeap %p<%p\n",pPtr,(void*)&current_stack);
	return pPtr < (void*)&current_stack;
}
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
inline bool Memory::isHeap(void* pPtr)
{
/* TODO try
	HANDLE heap=GetProcessHeap();
	return HeapValidate(heap,0x0,pPtr);
*/

	// what was wrong with this one?
//	return _CrtIsValidHeapPointer(pPtr);

	// not 64-bit compatible
	return pPtr >= gs_pHeapBase;
}
#elif FE_OS==FE_OSX
inline bool Memory::isHeap(void* pPtr)
{
	U8 current_stack;
//	fe_printf("Memory::isHeap %p<%p\n",pPtr,(void*)&current_stack);
	return pPtr < (void*)&current_stack;
}
#else
#error Memory::isHeap not implemented
#endif

inline bool Memory::newWorks(void)
{
#if FE_CHECK_NEW
	if(!gs_newCheck)
	{
		char* byte=new char();
		delete byte;
		gs_newCheck=gs_pHeapBase? 1: -1;
	}
	return gs_newCheck>0;
#else
	return false;
#endif
}

inline bool Memory::isHeapOrCannotTell(void* pPtr)
{
#if FE_CHECK_HEAP
	return !newWorks() || Memory::isHeap(pPtr);
#else
	return true;
#endif
}

FE_DL_EXPORT void FE_CDECL setAllocateFunction(allocateFunction *p_fn);
FE_DL_EXPORT void FE_CDECL setDeallocateFunction(deallocateFunction *p_fn);
FE_DL_EXPORT void FE_CDECL setPrintFunction(printFunction *p_fn);

} /* namespace */

#if FALSE
inline void* _cdecl operator new( size_t byteCount )
{	return fe::allocate( byteCount ); }
inline void* _cdecl operator new[]( size_t byteCount )
{	return operator new( byteCount ); }

inline void _cdecl operator delete( void* pMemory )
{	fe::deallocate( pMemory ); }
inline void _cdecl operator delete[]( void* pMemory )
{	operator delete( pMemory ); }
#endif

//* NOTE ISO C++1z does not allow dynamic exception specifications
#if (FE_COMPILER==FE_GNU && __GNUC__>6) || (defined(__clang__) && FE_CPLUSPLUS >= 201703L)
#define FE_NO_DYNAMIC_EXCEPTIONS 1
#else
#define FE_NO_DYNAMIC_EXCEPTIONS 0
#endif

#ifndef FE_USE_HOST_NEW
#if !defined(__clang__)
void* FE_CDECL operator new( size_t byteCount );
void* FE_CDECL operator new[]( size_t byteCount );
void FE_CDECL operator delete( void* pMemory );
void FE_CDECL operator delete[]( void* pMemory );
#elif FE_CPLUSPLUS >= 201703L
void* FE_CDECL operator new( size_t byteCount ) noexcept(false);
void* FE_CDECL operator new[]( size_t byteCount ) noexcept(false);
void FE_CDECL operator delete( void* pMemory ) throw ();
void FE_CDECL operator delete[]( void* pMemory ) throw ();
#else
void* FE_CDECL operator new( size_t byteCount ) throw (std::bad_alloc);
void* FE_CDECL operator new[]( size_t byteCount ) throw (std::bad_alloc);
void FE_CDECL operator delete( void* pMemory ) throw ();
void FE_CDECL operator delete[]( void* pMemory ) throw ();
#endif
#endif

#endif /* __platform_memory_h__ */

