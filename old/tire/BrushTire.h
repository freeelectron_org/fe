/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_BrushTire_h__
#define __tire_BrushTire_h__

namespace fe
{
namespace ext
{

/// @brief Real Time Brush Tire
class AsBrushTire
	: public AsTire, public Initialize<AsBrushTire>
{
	public:
		AsConstruct(AsBrushTire);
		void initialize(void)
		{
		}
};


/// @brief Brush Tire Model
class AsBrushTireModel
	: public AccessorSet, public Initialize<AsBrushTireModel>
{
	public:
		AsConstruct(AsBrushTireModel);
		void initialize(void)
		{
			add(stiffness,			FE_USE("tire:z:stiffness"));
			add(damping,			FE_USE("tire:z:damping"));
			add(camberStiffness,	FE_USE("brush:camber_stiffness"));
			add(brakingStiffness,	FE_USE("brush:braking_stiffness"));
			add(corneringStiffness,	FE_USE("brush:cornering_stiffness"));
			add(selfAligningStiffness,
									FE_USE("brush:self_aligning_stiffness"));
			add(latInitialLoadStiffnessScale,
									FE_USE("brush:lat_init_load_stiff_scale"));
			add(longInitialLoadStiffnessScale,
									FE_USE("brush:long_init_load_stiff_scale"));
			add(designLoad,			FE_USE("brush:design_load"));
			add(slipSpeedFade,		FE_USE("brush:slip_speed_fade"));
			add(sensSlope,			FE_USE("brush:load_sensitivy_slope"));
			add(finalSens,			FE_USE("brush:load_sensitivy_final"));
			add(finalLoad,			FE_USE("brush:load_sensitivy_final_load"));
			add(tireHalfGripSpeed,	FE_USE("brush:half_grip_speed"));
			add(latGrip,			FE_USE("brush:lat_grip"));
			add(longGrip,			FE_USE("brush:long_grip"));
			add(slideGrip,			FE_USE("brush:slide_grip"));

		}
		Accessor<Real>			stiffness;
		Accessor<Real>			damping;
		Accessor<Real>			camberStiffness;
		Accessor<Real>			brakingStiffness;
		Accessor<Real>			corneringStiffness;
		Accessor<Real>			selfAligningStiffness;
		Accessor<Real>			latInitialLoadStiffnessScale;
		Accessor<Real>			longInitialLoadStiffnessScale;
		Accessor<Real>			designLoad;
		Accessor< t_moa_v2 >		slipSpeedFade;
		Accessor<Real>			sensSlope;
		Accessor<Real>			finalSens;
		Accessor<Real>			finalLoad;
		Accessor<Real>			tireHalfGripSpeed;
		Accessor<Real>			latGrip;
		Accessor<Real>			longGrip;
		Accessor<Real>			slideGrip;
};


class TireLoadSens
{
	t_moa_real mA, mB, mC, mD;
	t_moa_real mMaxLoad;	// Maximum load for which this is curve is valid

	public:
		void Init( t_moa_real zeroSlope, t_moa_real finalSens,
			t_moa_real finalLoad );

		t_moa_real Sensitivity( const t_moa_real tireLoad ) const
		{
			const t_moa_real x = (tireLoad > mMaxLoad) ? mMaxLoad : tireLoad;
			return (mD + (x * (mC + (x * (mB + (x * mA))))));
		}
};

/// @brief Brush Tire Model
class BrushTire
	: virtual public TireI,
	public CastableAs<BrushTire>
{
	public:
				BrushTire(void);
virtual			~BrushTire(void);

				/** Integrate a time step. */
		void	step(t_moa_real a_dt);

				/** Compile internal structure.   This should be done
						before any time stepping. */
		bool	compile(Record r_tire, Record r_config,
					sp<RecordGroup> a_rg_dataset);

		/// @name	Dynamic State Accessors
		/// @{
		void		setVelocity(		const t_moa_v3		&a_velocity);
		void		setAngularVelocity(	const t_moa_v3		&a_ang_velocity);
		void		setContact(			const t_moa_real	a_radius,
										const t_moa_real	a_inclination);

		void		setExternalFactor(	const t_moa_real	a_factor);
		void		setFrame(		const t_moa_v3	&a_pos,
									const t_moa_v3 &a_x,
									const t_moa_v3 &a_y,
									const t_moa_v3 &a_z);
		const t_moa_v3		&getForce(void);
		const t_moa_v3		&getMoment(void);
		const t_moa_v3		&getVelocity(void);
		const t_moa_v3		&getAngularVelocity(void);
		const t_moa_real	&getRadius(void) { return m_radius; }

		t_moa_real	&contactRadius(void) { return m_contact_radius; }
		t_moa_real &inclination(void) { return m_inclination; }
		/// @}


		/// @name	Set Initial Parameters and Properties
		/// @{
		void	setGeometry(	t_moa_real a_radius,
								t_moa_real	a_width);
		void	setFrictionCoefficient(
							const t_moa_v2 &a_friction_coefficient);
		void	setMode(unsigned int a_mode) { m_mode = a_mode; }
		/// @}


	private:
		void		printf(FILE *fp);
		Record	m_r_parent;
		void	initialize(void);
		unsigned int m_mode;
		struct t_contact
		{
			struct t_velocities
			{
				t_moa_real fHubVelocity;
				t_moa_real fSinSlipAngle;
				t_moa_real fCosSlipAngle;

				t_moa_v2 vHubVelocity_cf;
				t_moa_v2 vTreadSlippage_cf;
			};

			t_velocities Velocities;
			t_moa_real fSinCamber;
		};

		struct t_internal
		{
			t_moa_real fMyx;
			t_moa_real fMyy;
			t_moa_real fMySlide;

			//Carcass stiffness
			t_moa_real fCx;	//Braking stiffness
			t_moa_real fCy;	//Cornering stiffness;
			t_moa_real fCz;	//Self Alignment stiffness

			t_moa_real fCCamberRelative; //Relative camber stiffness

			t_moa_real fMaxSlideSpeedFade;
			t_moa_real fMaxSlideSpeed;

			t_moa_real fLateralViscoGrip;
			t_moa_real fLateralViscoSlope;
		};

		t_internal			m_internal;
		t_contact			m_Contact;

		// dynamic state
		t_moa_v3			m_force;
		t_moa_v3			m_moment;
		t_moa_v3			m_velocity;
		t_moa_v3			m_angular_velocity;
		t_moa_real			m_contact_radius;
		t_moa_real			m_inclination;

		// force space spring
		t_moa_v3			m_force_filtered;
		t_moa_v3			m_force_v;
		t_moa_real			m_stiffness_filter;
		t_moa_real			m_damping_filter;

		// geometry
		t_moa_real			m_radius;
		t_moa_real			m_width;

		// parameters
		t_moa_real			m_deflectionStiffness;
		t_moa_real			m_deflectionDamping;
		t_moa_real			m_brakingStiffness;
		t_moa_real			m_corneringStiffness;
		t_moa_real			m_selfAligningStiffness;
		t_moa_real			m_latInitialLoadStiffnessScale;
		t_moa_real			m_longInitialLoadStiffnessScale;
		t_moa_real			m_designLoad;
		t_moa_v2			m_slipSpeedFade;
		t_moa_real			m_sensSlope;
		t_moa_real			m_finalSens;
		t_moa_real			m_finalLoad;
		t_moa_real			m_tireHalfGripSpeed;
		t_moa_real			m_latGrip;
		t_moa_real			m_longGrip;
		t_moa_real			m_slideGrip;

		t_moa_real			m_externalFactor;

		TireLoadSens		m_loadSensitivity;

		t_moa_xform		m_frame;

#ifdef FE_ENABLE_CHANNEL_LOGGING
		t_moa_real							m_et;
		sp< ChannelLog <t_moa_v2> >		m_log;
#endif

	private:
		void GetForce(			const t_contact& rContact,
								const t_moa_real fNormalForce,
								const t_moa_real fGroundSpeed,
								t_moa_v3* pvForce_cf,
								t_moa_real* pfPneumaticTrail,
								t_moa_real* pfAdhesiveWeight);
		void GetHighSpeedForce(	const t_contact& rContact,
								const t_moa_real fNormalForce,
								t_moa_v3* pvForce_cf,
								t_moa_real* pfPneumaticTrail,
								t_moa_real* pfAdhesiveWeight);
		void GetLowSpeedForce(	const t_contact& rContact,
								const t_moa_real fNormalForce,
								t_moa_v3* pvForce_cf,
								t_moa_real* pfAdhesiveWeight) const;

		t_moa_real GetTreadSlideGripFactor(	const t_contact& rContact,
												t_moa_real fLambda) const;
};

/// @brief Brush Tire Model System
class FE_DL_EXPORT BrushTireSystem :
	virtual public Stepper,
	public Initialize<BrushTireSystem>
{
	public:
				BrushTireSystem(void);
virtual			~BrushTireSystem(void);
		void	initialize(void);

virtual	void			compile(const t_note_id &a_note_id);
virtual	void			step(t_moa_real a_dt);
		void			step(t_moa_real a_dt, unsigned int i_tire);
		//void			step(t_moa_real a_dt, sp<BrushTire> spTire);
	private:
		sp<RecordGroup>					rg_dataset;
		sp<Scope>						m_spScope;
		//std::vector< sp<BrushTire> >	m_tires;
		std::vector< Record >				m_r_tires;
		AsTireLive								m_asTireLive;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_BrushTire_h__ */

