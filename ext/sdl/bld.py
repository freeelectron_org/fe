import os
import sys
forge = sys.modules["forge"]

forge.sdl_auto_detected = 0
forge.sdl_version = 2

def prerequisites():
    return [ "datatool", "image" ]

def setup(module):
    srcList = [ "sdl.pmh",
                "ContextSDL",
                "ImageSDL",
                "sdlDL" ]

    # not in DarwinPorts, for some reason
    if forge.fe_os != "FE_OSX":
        srcList += [ "JoySDL" ]

    dll = module.DLL( "fexSDLDL", srcList )

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexImageDLLib" ]
#               "fexWindowLib",
#               "fexWindowDLLib" ]

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    module.cppmap = {}
    module.cppmap["sdl"] = " -DFE_SDL=%d" % forge.sdl_version
    forge.includemap["sdl"] = "/usr/include/SDL2"

    if forge.sdl_version == 1:
        if forge.fe_os == "FE_LINUX":
            dll.linkmap["imagelibs"] = "-lSDL -lSDL_image"
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            dll.linkmap["imagelibs"] = "SDL.lib"
        elif forge.fe_os == 'FE_OSX':
            dll.linkmap["imagelibs"] = "-framework SDL -framework SDL_image"
    else:
        if forge.fe_os == "FE_LINUX":
            dll.linkmap["imagelibs"] = "-lSDL2"
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            if forge.codegen == 'debug':
                dll.linkmap["imagelibs"] = "SDL2-staticd.lib"
            else:
                dll.linkmap["imagelibs"] = "SDL2-static.lib"

            dll.linkmap["imagelibs"] += " Version.lib Imm32.lib Setupapi.lib"
        elif forge.fe_os == 'FE_OSX':
#           dll.linkmap["imagelibs"] = "-framework SDL2"
            dll.linkmap["imagelibs"] = "-lSDL2main -lSDL2 -Wl,-framework,Cocoa"

    forge.deps( ["fexSDLDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexSDLDL",                     None,   None) ]

    if 'viewer' in forge.modules_confirmed:
        forge.tests += [
            ("xImage.exe",  "fexSDLDL 10",                  None,   None) ]

        if 'ray' in forge.modules_confirmed:
            forge.tests += [
                ("xRay.exe",    "fexSDLDL 30",                  None,   None) ]

        if forge.media:
            forge.tests += [ ("xImage.exe",
                "fexSDLDL " + forge.media + "/image/SDL_button.bmp 10",
                                                                None,   None) ]

    module.Module('test')

def auto(module):
    if forge.sdl_version == 1:
        test_file = """
        #include <SDL/SDL.h>
        #include <SDL/SDL_image.h>
        int main(void)
        {
            SDL_Init(SDL_INIT_NOPARACHUTE);
            return 0;
        }
        \n"""
    else:
        test_file = """
        #define SDL_MAIN_HANDLED
        #include <SDL2/SDL.h>
        int main(void)
        {
            SDL_Init(SDL_INIT_NOPARACHUTE);
            return 0;
        }
        \n"""

    sdl_linkmap = {}
    sdl_includemap = {}
    sdl_includemap["sdl"] = "/usr/include/SDL2"

    if forge.fe_os == "FE_LINUX":
        sdl_linkmap["imagelibs"] = "-lSDL2"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            sdl_linkmap["imagelibs"] = "SDL2-staticd.lib"
        else:
            sdl_linkmap["imagelibs"] = "SDL2-static.lib"

        sdl_linkmap["imagelibs"] += " Version.lib Imm32.lib Setupapi.lib"
    elif forge.fe_os == 'FE_OSX':
        sdl_linkmap["imagelibs"] = "-lSDL2main -lSDL2 -Wl,-framework,Cocoa"

    if forge.cctest(test_file, linkmap = sdl_linkmap, includemap = sdl_includemap):
#       forge.cprint(forge.RED,1,'(system install not found)', eol=None)
#       return 'abort'
        return 'build'

#   forge.cprint(forge.CYAN,0,'(system install)', eol=None)
    forge.sdl_auto_detected = 1

    return None
