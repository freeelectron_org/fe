/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __image_ImageRaw_h__
#define __image_ImageRaw_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Image handling using raw buffers

	@ingroup image

	Currently, no file IO is supported.
	A raw byte dump might be added later.
*//***************************************************************************/
class FE_DL_EXPORT ImageRaw: public ImageCommon
{
	public:
				ImageRaw(void);
virtual			~ImageRaw(void);

				//* As ImageI
virtual	I32		createSelect(void);
virtual void	select(I32 id);
virtual I32		selected(void) const	{ return m_selected; }
virtual	void	unload(I32 id);

virtual	void	setFormat(ImageI::Format format);
virtual	ImageI::Format	format(void) const
				{	return m_selected<I32(m_buffers.size())?
							m_buffers[m_selected].m_format: ImageI::e_none; }

virtual	void	resize(U32 width,U32 height,U32 depth);
virtual	void	replaceRegion(U32 x,U32 y,U32 z,
						U32 width,U32 height,U32 depth,void* data);

virtual	U32		width(void) const
				{	return m_selected<I32(m_buffers.size())?
							m_buffers[m_selected].m_width: 0; }
virtual	U32		height(void) const
				{	return m_selected<I32(m_buffers.size())?
							m_buffers[m_selected].m_height: 0; }
virtual	U32		depth(void) const
				{	return m_selected<I32(m_buffers.size())?
							m_buffers[m_selected].m_depth: 0; }
virtual	void*	raw(void) const
				{	return m_selected<I32(m_buffers.size())?
							m_buffers[m_selected].m_pData: NULL; }

	private:
	class Buffer
	{
		public:

					Buffer(void):
						m_created(FALSE),
						m_pData(NULL)
					{ reset(); }
					~Buffer(void)
					{
						if(m_pData)
						{
							deallocate(m_pData);
						}
					}
			void	reset(void)
					{
						if(m_pData)
						{
							deallocate(m_pData);
						}
						m_pData=NULL;
						m_width=0;
						m_height=0;
						m_depth=0;
						m_format=ImageI::e_none;
					}

			BWORD			m_created;
			U32				m_width;
			U32				m_height;
			U32				m_depth;
			ImageI::Format	m_format;
			void*			m_pData;
	};

		Array<ImageRaw::Buffer>	m_buffers;
		I32						m_selected;

static
const	U32	m_bytes[4];
};

} /* namespace ext */
} /* namespace fe */

#endif /* __image_ImageRaw_h__ */
