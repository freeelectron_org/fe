/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_SocketHandler_h__
#define __netsignal_SocketHandler_h__
namespace fe
{
namespace ext
{

/**	Socket handler

	@copydoc SocketHandler_info
	*/
class SocketHandler :
	virtual public HandlerI
{
	public:
				SocketHandler(SockAddr server_addr,
						Socket::Transport transport=Socket::e_tcp,
						String ioPriority="normal");
virtual			~SocketHandler(void);

virtual	void	handle(Record &signal);
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);

virtual	void	addSenderLayout(sp<Layout> spLayout);

	enum
	{
		e_end_op					=	0,
		e_create_signal_sender		=	1,
		e_create_signal_receiver	=	2
	};

	struct SenderUndoStruct
	{
		sp<HandlerI>			m_spSignalSender;
		sp<ChunkSender>		m_spChunkSender;
		std::list<sp<Layout> >	m_layouts;
	};

	struct ReceiverUndoStruct
	{
		sp<HandlerI>			m_spSignalReceiver;
		sp<ChunkReceiver>		m_spChunkReceiver;
	};

	private:
		void	createSignalSender(sp<Socket> spSocket);
		void	createSignalReceiver(sp<Socket> spSocket);
		void	stop(void);
		void	assertScope(sp<Scope> spScope);
		void	cleanDeadConnections(void);

	private:
		sp<SocketSpool>					m_spSocketSpool;
		SockAddr						m_serverAddr;
		Socket::Transport				m_sockTransportType;
		String							m_ioPriority;
		sp<SignalerI>					m_spSignaler;
		sp<Scope>						m_spScope;
		sp<Layout>						m_spLayout;
		sp<Listener>					m_spListener;
		std::list<sp<Layout> >			m_layouts;
		std::list<SenderUndoStruct>		m_senders;
		std::list<ReceiverUndoStruct>	m_receivers;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __netsignal_SocketHandler_h__ */
