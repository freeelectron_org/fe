/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

using namespace fe;
using namespace fe::ext;

void SpineFitOp::initialize(void)
{
	catalog<bool>("FragmentInput")=false;
	catalog<String>("FragmentInput","label")="Fragment Input";
	catalog<bool>("FragmentInput","joined")=true;
	catalog<String>("FragmentInput","hint")=
			"Divide input into groups of primitives.";

	catalog<String>("FragmentAttr")="fragment";
	catalog<String>("FragmentAttr","label")="Attribute";
	catalog<String>("FragmentAttr","enabler")="FragmentInput";
	catalog<String>("FragmentAttr","hint")=
			"Primitive string attribute on input used to"
			" gather primitives with the same string.";

	catalog<String>("Facing Attribute")="N";
	catalog<String>("Facing Attribute","hint")=
			"Name of vector attribute to put on output curves"
			" describing the twist orientation.";

	catalog<I32>("Samples")=5;
	catalog<I32>("Samples","min")=2;
	catalog<I32>("Samples","high")=10;
	catalog<I32>("Samples","max")=100;
	catalog<String>("Samples","hint")="Number of points along each curve.";

	catalog<I32>("Refinement")=0;
	catalog<I32>("Refinement","high")=4;
	catalog<I32>("Refinement","max")=8;
	catalog<String>("Refinement","hint")="Maximum level of subdivision."
			"  Refinement may be adaptive and non-uniform.";

	catalog<bool>("Fast Snap")=false;
	catalog<String>("Fast Snap","hint")=
			"Force sample vertices to colocate with existing surface vertices."
			"  This may be faster but probably much less evenly placed.";

	catalog<bool>("Debug Surface")=false;
	catalog<String>("Debug Surface","hint")=
			"Draws lines over the output showing the surface refinement."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<bool>("Debug Facing")=false;
	catalog<String>("Debug Facing","hint")=
			"Draws lines over the output showing the facing direction."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";

	m_spDebugGroup=new DrawMode();
	m_spDebugGroup->setGroup("debug");
}

void SpineFitOp::handle(Record& a_rSignal)
{
//	feLog("SpineFitOp::handle\n");

	const BWORD fragmentInput=catalog<bool>("FragmentInput");
	const String fragmentAttr=catalog<String>("FragmentAttr");
	const String facingName=catalog<String>("Facing Attribute");
	const I32 samples=catalog<I32>("Samples");
	const I32 refinement=catalog<I32>("Refinement");
	const BWORD fastSnap=catalog<bool>("Fast Snap");
	const BWORD debugSurface=catalog<bool>("Debug Surface");
	const BWORD debugFacing=catalog<bool>("Debug Facing");

	if(samples<2)
	{
		feLog("SpineFitOp::handle"
				" at least two samples required\n");
		catalog<String>("error")="samples must be >= 2";
		return;
	}

	sp<SurfaceAccessibleI> spOutputCurves;
	if(!accessOutput(spOutputCurves,a_rSignal)) return;

	sp<DrawI> spDrawOutput;
	if(!accessDraw(spDrawOutput,a_rSignal)) return;

	sp<DrawI> spDrawDebug;
	if(!accessGuide(spDrawDebug,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputCurves,e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spAccessorFacing;
	access(spAccessorFacing,spOutputCurves,e_point,facingName,e_warning);

	BWORD surfaceWarned=FALSE;

	sp<SurfaceAccessorI> spInputFragment;
	sp<SurfaceAccessorI> spOutputFragment;
	sp<SurfaceI> spSurfaceI;
	if(fragmentInput)
	{
		if(!access(spInputFragment,spInputAccessible,
				e_primitive,fragmentAttr)) return;

		if(!access(spOutputFragment,spOutputCurves,
				e_primitive,fragmentAttr,e_error,e_createMissing)) return;

		if(!access(spSurfaceI,spInputAccessible)) return;
		spSurfaceI->setRefinement(0);	//* TODO allow refinement
		spSurfaceI->partitionWith(fragmentAttr);
		spSurfaceI->prepareForSearch();
	}

	const U32 primitiveCount=spInputVertices->count();

	const U32 fragmentCount=fragmentInput?
			spSurfaceI->partitionCount(): spInputVertices->count();
	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		SpatialVector* pCurve=new SpatialVector[samples];

		SpatialVector base;
		SpatialVector tip;
		set(base);
		set(tip);
		Real maxDist=0.0;

		String fragmentName;

		if(fragmentInput)
		{
			fragmentName=spSurfaceI->partitionName(fragmentIndex);
			spSurfaceI->setPartitionFilter(fragmentName);

			U32 found=0;
			for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				const String oneFragment=
						spInputFragment->string(primitiveIndex);
				if(oneFragment!=fragmentName)
				{
					continue;
				}
				const U32 subCount=spInputVertices->subCount(primitiveIndex);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=spInputVertices->spatialVector(
							primitiveIndex,subIndex);

					if(found)
					{
						const Real distance=magnitude(point-base);
						if(maxDist<distance)
						{
							maxDist=distance;
							tip=point;
						}
					}
					else
					{
						base=point;
						tip=point;
					}

					found++;
				}
			}
		}
		else
		{
			if(!access(spSurfaceI,spInputAccessible,e_warning,fragmentIndex))
			{
				feLog("SpineFitOp::handle"
						" input mesh would not provide SurfaceI for %d\n",
						fragmentIndex);
				continue;
			}
			spSurfaceI->setRefinement(refinement);

			const U32 subCount=spInputVertices->subCount(fragmentIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const SpatialVector point=
						spInputVertices->spatialVector(fragmentIndex,subIndex);

				if(subIndex)
				{
					const Real distance=magnitude(point-base);
					if(maxDist<distance)
					{
						maxDist=distance;
						tip=point;
					}
				}
				else
				{
					base=point;
					tip=point;
				}
			}
		}

		pCurve[0]=base;
		pCurve[samples-1]=tip;

		for(I32 sample=1;sample<samples-1;sample++)
		{
			SpatialVector& rOutputPoint=pCurve[sample];

			rOutputPoint=base+(tip-base)*(sample/(samples-1.0));

			if(fastSnap)
			{
				//* brute force, on vertex only
				Real minDist= -1.0;
				SpatialVector best=base;

				const U32 elementCount=fragmentInput? primitiveCount: 1;
				for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					if(fragmentInput)
					{
						const String oneFragment=
								spInputFragment->string(elementIndex);
						if(oneFragment!=fragmentName)
						{
							continue;
						}
					}

					const U32 primitiveIndex=
							fragmentInput? elementIndex: fragmentIndex;
					const U32 subCount=
							spInputVertices->subCount(primitiveIndex);
					for(U32 subIndex=0;subIndex<subCount;subIndex++)
					{
						const SpatialVector point=
								spInputVertices->spatialVector(
								primitiveIndex,subIndex);

						const Real distance=magnitude(point-rOutputPoint);
						if(minDist<0.0 || minDist>distance)
						{
							minDist=distance;
							best=point;
						}
					}
				}
				rOutputPoint=best;
			}
			else
			{
				try
				{
					if(spSurfaceI->radius()>1e-6)
					{
						sp<SurfaceI::ImpactI> spImpact=
								spSurfaceI->nearestPoint(rOutputPoint);
						if(spImpact.isValid())
						{
							rOutputPoint=spImpact->intersection();
						}
					}
					else
					{
						if(!surfaceWarned)
						{
							catalog<String>("warning")+=
									"unsearchable input surface";
							surfaceWarned=TRUE;
						}
					}
				}
				catch(const std::exception& e)
				{
					if(!surfaceWarned)
					{
						catalog<String>("warning")+=
								"nearest point search failed: " +
								String(e.what())+";";
						surfaceWarned=TRUE;
					}
				}
			}
		}

		spDrawOutput->drawLines(pCurve,NULL,samples,DrawI::e_strip,false,NULL);

		if(fragmentInput)
		{
			spOutputFragment->set(fragmentIndex,fragmentName);
		}

		spDrawDebug->pushDrawMode(m_spDebugGroup);

		if(spAccessorFacing.isValid() && samples>2)
		{
			const Color green(0.0,1.0,0.0,1.0);
			SpatialVector line[2];

			const I32 mid=samples>>1;
			const SpatialVector midpoint=pCurve[mid];

			const SpatialVector side=cross(midpoint-base,tip-midpoint);
			const SpatialVector facing=cross(side,tip-base);
			const Real mag=magnitude(facing);
			if(mag!=0.0)
			{
				const SpatialVector unitFacing=facing*(1.0/mag);

				//* TODO should be able to get a specific dir for each point
				for(I32 sample=0;sample<samples;sample++)
				{
					const U32 vertexIndex=
							spOutputVertices->integer(fragmentIndex,sample);
					spAccessorFacing->set(vertexIndex,unitFacing);

					if(debugFacing)
					{
						line[0]=pCurve[sample];
						line[1]=line[0]+facing;
						spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,
								false,&green);
					}
				}
			}
		}

		delete[] pCurve;

		if(debugSurface)
		{
			sp<DrawableI> spDrawable=spSurfaceI;
			if(spDrawable.isValid())
			{
				spDrawable->draw(spDrawDebug,NULL);
			}
		}

		spDrawDebug->popDrawMode();
	}

//	feLog("SpineFitOp::handle done\n");
}
