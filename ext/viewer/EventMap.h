/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_EventMap_h__
#define __viewer_EventMap_h__

namespace fe
{
namespace ext
{

typedef std::list<sp<Layout> >				t_layout_list;
typedef std::map<I32, t_layout_list >		t_state_layout;
typedef std::map<I32, t_state_layout >		t_item_state;
typedef std::map<I32, t_item_state >		t_source_item;

/**	Maps WindowEvent signals to Layouts.

	If a signal matches a bound WindowEvent pattern a record of
	the corresponding Layout is created and signaled.  If the layout
	has an AsSignal::event attribute, then that attribute is set to the
	matched WindowEvent.

	@copydoc EventMap_info
	*/
class EventMap :
	public ChainSignaler,
	virtual public HandlerI,
	virtual public EventMapI,
	public Initialize<EventMap>
{
	public:
				EventMap(void);
virtual			~EventMap(void);
virtual	void	initialize(void);

virtual void	handleBind(	fe::sp<SignalerI> spSignalerI,
						fe::sp<Layout> spLayout);
virtual void	handle(	fe::Record &record);
virtual	void	bind(	WindowEvent::Source source,
						WindowEvent::Item item,
						WindowEvent::State state,
						sp<Layout> resultSignalLayout);

	private:
		void	handleSource(WindowEvent &wev, t_source_item &sourceMap);
		void	handleItem(WindowEvent &wev, t_item_state &itemMap);
		void	handleState(WindowEvent &wev, t_state_layout &stateMap);
		void	fireSignal(WindowEvent &wev, t_layout_list &layouts);
	private:
		t_source_item		m_sourceMap;
		sp<Scope>			m_spScope;
		hp<SignalerI>		m_spSignaler;
		AsSignal			m_asSignal;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_EventMap_h__ */

