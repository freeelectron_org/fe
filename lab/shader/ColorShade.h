/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shader_ColorShade_h__
#define __shader_ColorShade_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Simple test shader to set color

	@ingroup shader
*//***************************************************************************/
class FE_DL_EXPORT ColorShade:
	public ShaderCommon,
	public Initialize<ColorShade>
{
	public:

				ColorShade(void)											{}
virtual			~ColorShade(void)											{}

		void	initialize(void);

				//* As ShaderI
virtual	void	update(void);
virtual	void	evaluate(sp<ShaderVariablesI> a_spShaderVariablesI) const;

	private:
		I32		m_channelColor;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shader_ColorShade_h__ */
