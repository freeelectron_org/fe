/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_TreeLevel_h__
#define __vegetation_TreeLevel_h__

FE_ATTRIBUTE("veg:form",		"Part of tree, Stick or Leaf");
FE_ATTRIBUTE("veg:level",		"Depth in hierarchy");
FE_ATTRIBUTE("veg:length",		"Curve length over all segments");
FE_ATTRIBUTE("veg:bare",		"Fraction of length stripped of forks");
FE_ATTRIBUTE("veg:valleybase",	"Minimum fraction of length at base");
FE_ATTRIBUTE("veg:valleytip",	"Minimum fraction of length at tip");
FE_ATTRIBUTE("veg:peakalong",	"Point along parent for peak child length");
FE_ATTRIBUTE("veg:powerbase",	"Exponent applied before peak");
FE_ATTRIBUTE("veg:powertip",	"Exponent applied after peak");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief TreeLevel RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT TreeLevel: virtual public RecordView
{
	public:
		Functor<String>		form;
		Functor<I32>		level;
		Functor<F32>		length;
		Functor<F32>		bare;
		Functor<F32>		valleybase;
		Functor<F32>		valleytip;
		Functor<F32>		peakalong;
		Functor<F32>		powerbase;
		Functor<F32>		powertip;

				TreeLevel(void)		{ setName("TreeLevel"); }
virtual	void	addFunctors(void)
				{
					add(form,		FE_USE("veg:form"));
					add(level,		FE_USE("veg:level"));
					add(length,		FE_USE("veg:length"));
					add(bare,		FE_USE("veg:bare"));
					add(valleybase,	FE_USE("veg:valleybase"));
					add(valleytip,	FE_USE("veg:valleytip"));
					add(peakalong,	FE_USE("veg:peakalong"));
					add(powerbase,	FE_USE("veg:powerbase"));
					add(powertip,	FE_USE("veg:powertip"));
				}
virtual	void	initializeRecord(void)
				{
					level()=0;
					length()=1.0f;
					bare()=0.0f;
					valleybase()=0.0f;
					valleytip()=0.0f;
					peakalong()=0.0f;
					powerbase()=1.0f;
					powertip()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_TreeLevel_h__ */


