/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

namespace fe
{
namespace ext
{

void ContractOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("contraction")=0.0;
	catalog<Real>("contraction","high")=1.0;
	catalog<String>("contraction","label")="Contraction";
	catalog<String>("contraction","hint")=
			"Peak of curve length reduction.";

	catalog< sp<Component> >("Input Curves");

	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";
	catalog<String>("Output Curves","copy")="Input Curves";
	catalog<bool>("Output Curves","clobber")=true;
}

void ContractOp::handle(Record& a_rSignal)
{
	const Real contraction=catalog<Real>("contraction");

//	sp<SurfaceAccessibleI> spInputAccessible;
//	if(!access(spInputAccessible,"Input Curves")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

//	spOutputAccessible->copy(spInputAccessible);

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	U32 noiseSeed=0;

	I32 curveCount=0;

	const I32 primitiveCount=spOutputVertices->count();

	I32 maxCount=0;
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(maxCount<subCount)
		{
			maxCount=subCount;
		}
	}

	SpatialVector* pointArray=new SpatialVector[maxCount];

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		const Real shrink=
				contraction*randomRealReentrant(&noiseSeed,0.0,1.0);

		Real curveLength=Real(0);
		SpatialVector lastPoint=
				spOutputVertices->spatialVector(primitiveIndex,0);
		pointArray[0]=lastPoint;

		for(I32 subIndex=1;subIndex<subCount;subIndex++)
		{
			const SpatialVector point=
					spOutputVertices->spatialVector(primitiveIndex,subIndex);
			const Real length=magnitude(point-lastPoint);

			pointArray[subIndex]=point;
			curveLength+=length;

			lastPoint=point;
		}

		if(curveLength<=Real(0))
		{
			continue;
		}

		const Real reducedLength=(1.0-shrink)*curveLength;

		//* TODO option for faster linear interpolation

		ConvergentSpline convergentSpline;
		convergentSpline.configure(subCount,pointArray,NULL);

		//* NOTE not affecting first
		for(I32 subIndex=1;subIndex<subCount;subIndex++)
		{
			const Real unitDistance=subIndex/Real(subCount-1);

			const SpatialVector contracted=
					convergentSpline.solve(unitDistance*reducedLength);

			spOutputVertices->set(primitiveIndex,subIndex,contracted);
		}

		curveCount++;
	}

	delete[] pointArray;

	String summary;
	summary.sPrintf("%d curve%s\n",curveCount,curveCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
