#include "FEMorphGen.h"

//* NOTE turn off 'Use GPU for computing morph targets' in project settings

/***
always works:
editor - no/many morphs, add morph, replace added morph
package - no morphs, add morph (TODO test replace added morph)
editor/package - no morphs, add two like morphs at once (use second as target)

works with project setting of GPU morphs turned off:
package - many morphs, add morph (fails with Weights error on GPU)
package - many morphs, replace pre-existing morph (fails with no error on GPU)

never works, CPU or GPU morphs:
editor - many morphs, replace pre-existing morph (fails with no error given)
*/

AFEMorphGen::AFEMorphGen(const FObjectInitializer& ObjectInitializer):
	AFEImportOp(ObjectInitializer)
{
	feLog("AFEMorphGen::AFEMorphGen\n");

	m_fsTargetName="MorphGen";

	m_fsVertexMapfile="./vertexMap.json";
}

bool AFEMorphGen::UpdateConfiguration(fe::String a_configuration)
{
	feLog("AFEMorphGen::UpdateConfiguration\n");

	if(!AFEImportOp::UpdateConfiguration(a_configuration))
	{
		return false;
	}

	if(!m_pSkeletalMesh)
	{
		feLog("AFEMorphGen::UpdateConfiguration SkeletalMesh is NULL\n");
		return false;
	}

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feLog("AFEMorphGen::UpdateConfiguration spCatalog is NULL\n");
		return false;
	}

	fe::sp<fe::ext::SurfaceAccessibleI> spNewOutput=
			spCatalog->catalog< fe::sp<fe::Component> >("Output Surface");
	if(m_spOutputAccessible!=spNewOutput)
	{
		feLog("AFEMorphGen::UpdateConfiguration output replaced\n");
		m_spOutputAccessible=spNewOutput;
	}

	fe::sp<fe::ext::SurfaceAccessorI> spDeltaPoint=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_point,
			"deltaP",
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spDeltaPoint.isNull())
	{
		feLog("AFEMorphGen::UpdateConfiguration no deltaP access\n");
		return false;
	}

	fe::sp<fe::ext::SurfaceAccessorI> spDeltaNormal=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_point,
			"deltaN",
			fe::ext::SurfaceAccessibleI::e_refuseMissing);
	if(spDeltaNormal.isNull())
	{
		feLog("AFEMorphGen::UpdateConfiguration no deltaN access\n");
		return false;
	}

	const I32 pointCount=spDeltaPoint->count();

	feLog("AFEMorphGen::UpdateConfiguration copying %d deltas\n",pointCount);

	const int32 LODIndex(0);

	//* view section numbers by right clicking meshes in editor asset preview
	TArray<int32> sectionIndices;

#if WITH_EDITOR
	const I32 importedVertexCount=m_pSkeletalMesh->GetNumImportedVertices();
	feLog("AFEMorphGen::UpdateConfiguration"
			" SkeletalMesh has %d imported vertices\n",importedVertexCount);

	const FSkeletalMeshModel* pSkeletalMeshModel=
			m_pSkeletalMesh->GetImportedModel();
	const FSkeletalMeshLODModel& rSkeletalMeshLODModel=
			pSkeletalMeshModel->LODModels[LODIndex];
	const TArray<int32>& rVertexMap=rSkeletalMeshLODModel.MeshToImportVertexMap;
	const I32 vertexCount=rVertexMap.Num();
	feLog("AFEMorphGen::UpdateConfiguration"
			" vertex map size %d\n",vertexCount);
	I32 maxIndex(0);
	for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
	{
		const I32 pointIndex=rVertexMap[vertexIndex];
		if(maxIndex<pointIndex)
		{
			maxIndex=pointIndex;
		}
	}
	feLog("AFEMorphGen::UpdateConfiguration"
			" max index %d\n",maxIndex);

	//* NOTE activate all sections
	const I32 sectionCount=rSkeletalMeshLODModel.Sections.Num();
	for(I32 sectionIndex=0;sectionIndex<sectionCount;sectionIndex++)
	{
		sectionIndices.Add(sectionIndex);
	}
#else
	//* TODO share with FEImportOp and ExportVertexMap
	const FString fsContentDir=FPaths::ProjectContentDir();
	const fe::String contentDir=TCHAR_TO_ANSI(*fsContentDir);
	fe::String filename(TCHAR_TO_ANSI(*m_fsVertexMapfile));
	//* if filename starts with "./" or ".\", prepend content dir
	if(filename.prechop("./") != filename ||
		filename.prechop(".\\") != filename)
	{
		filename=contentDir+"/"+filename;
	}

	fe::sp<fe::Master> spMaster=m_spSingleMaster->master();
	fe::sp<fe::Registry> spRegistry=spMaster->registry();

	fe::sp<fe::Catalog> spCatalogIn=spMaster->createCatalog("Morph Data");

	fe::sp<fe::CatalogReaderI> spCatalogReader=
			spRegistry->create("CatalogReaderI.*.*.json");
	if(spCatalogReader.isNull())
	{
		feLog("AFEMorphGen::UpdateConfiguration"
				" failed to create CatalogReaderI\n");
		return false;
	}

	feLog("AFEMorphGen::UpdateConfiguration loading \"%s\"\n",filename.c_str());
	spCatalogReader->load(filename,spCatalogIn);

	I32* pSectionIndices(NULL);
	const I32 sectionCount=fe::CatalogBuffer::access(spCatalogIn,
			"sectionIndices","value",(void*&)pSectionIndices);
	if(!pSectionIndices)
	{
		feLog("AFEMorphGen::UpdateConfiguration"
				" failed to access sectionIndices buffer\n");
		return false;
	}

	for(I32 sectionIndex=0;sectionIndex<sectionCount;sectionIndex++)
	{
		sectionIndices.Add(pSectionIndices[sectionIndex]);
	}

	I32* pIndexMap(NULL);
	const I32 vertexCount=fe::CatalogBuffer::access(spCatalogIn,
			"vertexMap","value",(void*&)pIndexMap);
	if(!pIndexMap)
	{
		feLog("AFEMorphGen::UpdateConfiguration"
				" failed to access vertexMap buffer\n");
		return false;
	}

	TArray<int32> rVertexMap;
	for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
	{
		const int32 index=pIndexMap[vertexIndex];
		if(index>=0)
		{
			rVertexMap.Add(index);
		}
	}
#endif

	//* NOTE see ApplyMorphBlend() (CPU version) in
	//* Engine/Source/Runtime/Engine/Private/SkeletalRenderCPUSkin.cpp

	//* NOTE compare to UMorphTarget::PopulateDeltas() in
	//* Engine/Source/Runtime/Engine/Classes/Animation/Private/MorphTools.cpp

	feLog("AFEMorphGen::UpdateConfiguration sectionCount %d\n",sectionCount);

	//* TODO for multiple levels
	FMorphTargetLODModel morphTargetLODModel;
	morphTargetLODModel.bGeneratedByEngine=false;
	morphTargetLODModel.NumBaseMeshVerts=vertexCount;
	morphTargetLODModel.SectionIndices=sectionIndices;

	I32 flipCount(0);

	FMorphTargetDelta morphTargetDelta;
	for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
	{
		const I32 pointIndex=rVertexMap[vertexIndex];

		if(pointIndex>=pointCount)
		{
			continue;
		}

		const fe::SpatialVector deltaP=spDeltaPoint->spatialVector(pointIndex);
		const fe::SpatialVector deltaN=spDeltaNormal->spatialVector(pointIndex);

		if(vertexIndex<8)
		feLog("vt %d/%d pt %d/%d dP %s dN %s\n",vertexIndex,vertexCount,
				pointIndex,pointCount,c_print(deltaP),c_print(deltaN));

		//* DCC		Y up X left Z forward
		//* Unreal	Z up X left Y forward

		morphTargetDelta.SourceIdx=vertexIndex;
		morphTargetDelta.PositionDelta.Set(deltaP[0],deltaP[2],deltaP[1]);
		morphTargetDelta.TangentZDelta.Set(deltaN[0],deltaN[2],deltaN[1]);

		morphTargetLODModel.Vertices.Add(morphTargetDelta);

		if(fe::magnitudeSquared(deltaN)>fe::Real(2))
		{
			flipCount++;
		}
	}

	if(flipCount)
	{
		feLog("AFEMorphGen::UpdateConfiguration"
				" %d/%d normals may have flipped\n",
				flipCount,vertexCount);
	}

	morphTargetLODModel.NumVertices=morphTargetLODModel.Vertices.Num();

	//* TEMP print existing morph targets
#if TRUE
	feLog("AFEMorphGen::UpdateConfiguration check for existing morphs\n");
	const TMap<FName,int32>& morphTargetIndexMap=
			m_pSkeletalMesh->GetMorphTargetIndexMap();
	TArray<FName> morphTargetKeys;
	morphTargetIndexMap.GetKeys(morphTargetKeys);
	I32 index(0);
	for(FName& key: morphTargetKeys)
	{
		feLog("  %d \"%s\"\n",index++,TCHAR_TO_ANSI(*key.ToString()));
	}
#endif

	const FString morphName=m_fsTargetName;
	UMorphTarget* pMorphTarget=
			m_pSkeletalMesh->FindMorphTarget((FName)*morphName);
	if(pMorphTarget)
	{
		feLog("AFEMorphGen::UpdateConfiguration existing morph target %p\n",
				pMorphTarget);

		pMorphTarget->Rename();
		pMorphTarget->MarkAsGarbage();
		m_pSkeletalMesh->UnregisterMorphTarget(pMorphTarget);
		pMorphTarget=NULL;
	}
	if(!pMorphTarget)
	{
		pMorphTarget=NewObject<UMorphTarget>(m_pSkeletalMesh,*morphName);
		feLog("AFEMorphGen::UpdateConfiguration new morph target %p\n",
				pMorphTarget);
	}
	pMorphTarget->BaseSkelMesh=m_pSkeletalMesh;
	pMorphTarget->GetMorphLODModels().Empty();
	pMorphTarget->GetMorphLODModels().Add(morphTargetLODModel);

#if WITH_EDITOR
	//* Engine/SkeletalMesh.h:
	//* "Force the creation of a new GUID use to build
	//* the derive data cache key.
	//* Next time a build happen the whole skeletal mesh will be rebuild.
	//* Use this when you change stuff not in the skeletal mesh ddc key,
	//* like the geometry (import, re-import)
	//* Every big data should not be in the ddc key and should
	//* use this function, because its slow to create a key with big data."
	//* `NECESSITY CONFIRMED'
	m_pSkeletalMesh->InvalidateDeriveDataCacheGUID();
#endif

	feLog("AFEMorphGen::UpdateConfiguration registering morph target\n");

	const bool bInvalidateRenderData(true);
	if(!m_pSkeletalMesh->RegisterMorphTarget(
			pMorphTarget,bInvalidateRenderData))
	{
		feLog("AFEMorphGen::UpdateConfiguration"
				" RegisterMorphTarget failed\n");
	}

#if !WITH_EDITOR
	feLog("AFEMorphGen::UpdateConfiguration [not editor] init resources\n");

	FSkeletalMeshLODRenderData& rSkeletalMeshLODRenderData=
			m_pSkeletalMesh->GetResourceForRendering()->LODRenderData[LODIndex];

#if !FE_UNREAL_5_2_PLUS
	//* NOTE fixed in UE 5.2
	//* `Possible Unreal Bug'
	//* in Engine/Source/Runtime/Engine/Private/SkeletalMeshLODRenderData.cpp
	//* FSkeletalMeshLODRenderData::InitResources(),
	//* see check(RenderSection.DuplicatedVerticesBuffer.DupVertData.Num());
	//* Note that ReleaseResources() only does this check if WITH_EDITOR.

	//* NOTE dodge assertion in FSkeletalMeshLODRenderData::InitResources()
	//* by resizing that duplicated vertex resource to non-zero.
	//* WARNING This is a hack, not a solution.
	for(FSkelMeshRenderSection& RenderSection:
			rSkeletalMeshLODRenderData.RenderSections)
	{
		//* Rendering/SkeletalMeshLODRenderData.h:
		//* "Index Buffer containting all duplicated vertices in the section
		//* and a buffer containing which indices into the index buffer
		//* are relevant per vertex"
		FDuplicatedVerticesBuffer& rDuplicatedVerticesBuffer=
				RenderSection.DuplicatedVerticesBuffer;

		//* NOTE FDuplicatedVerticesBuffer::Init does this for zero NumVertices.
		//* The exact number doesn't appear to matter, just not zero.
		RenderSection.DuplicatedVerticesBuffer.DupVertData.ResizeBuffer(1);

		//* only really needed for FSkeletalMeshRenderData::InitResources
		RenderSection.DuplicatedVerticesBuffer.DupVertIndexData.ResizeBuffer(1);
	}
#endif

	const bool bNeedsVertexColors(false);

	//* Either way seems to work.
#if FALSE
	//* Call FSkeletalMeshLODRenderData::InitResources() on all levels.
	m_pSkeletalMesh->GetResourceForRendering()->ReleaseResources();
	m_pSkeletalMesh->GetResourceForRendering()->InitResources(
			bNeedsVertexColors,
			m_pSkeletalMesh->GetMorphTargets(),m_pSkeletalMesh);
#else
	//* Call BeginInitResources/etc on a variety of affected buffers.
	//* `NECESSITY CONFIRMED'
	TArray<UMorphTarget*> morphTargets;
	morphTargets.Add(pMorphTarget);
	rSkeletalMeshLODRenderData.InitResources(bNeedsVertexColors,LODIndex,
//			m_pSkeletalMesh->GetMorphTargets(),
			morphTargets,
			m_pSkeletalMesh);
#endif

#endif

	feLog("AFEMorphGen::UpdateConfiguration complete\n");

	return true;
}

bool AFEMorphGen::ExportVertexMap(void)
{
	feLog("AFEMorphGen::ExportVertexMap\n");

	if(!m_pSkeletalMesh)
	{
		feLog("AFEMorphGen::ExportVertexMap SkeletalMesh is NULL\n");
		return false;
	}

#if WITH_EDITOR
	const I32 importedVertexCount=m_pSkeletalMesh->GetNumImportedVertices();
	feLog("AFEMorphGen::ExportVertexMap"
			" SkeletalMesh has %d imported vertices\n",importedVertexCount);

	const int32 LODIndex(0);
	const FSkeletalMeshModel* pSkeletalMeshModel=
			m_pSkeletalMesh->GetImportedModel();
	const FSkeletalMeshLODModel& rSkeletalMeshLODModel=
			pSkeletalMeshModel->LODModels[LODIndex];
	const I32 sectionCount=rSkeletalMeshLODModel.Sections.Num();
	const TArray<int32>& rVertexMap=rSkeletalMeshLODModel.MeshToImportVertexMap;
	const I32 vertexCount=rVertexMap.Num();
	feLog("AFEMorphGen::ExportVertexMap"
			" vertex map size %d\n",vertexCount);

	//* TODO share with FEImportOp
	const FString fsContentDir=FPaths::ProjectContentDir();
	const fe::String contentDir=TCHAR_TO_ANSI(*fsContentDir);
	fe::String filename(TCHAR_TO_ANSI(*m_fsVertexMapfile));
	//* if filename starts with "./" or ".\", prepend content dir
	if(filename.prechop("./") != filename ||
		filename.prechop(".\\") != filename)
	{
		filename=contentDir+"/"+filename;
	}

	fe::System::createParentDirectories(filename);

	fe::sp<fe::Master> spMaster=m_spSingleMaster->master();
	fe::sp<fe::Registry> spRegistry=spMaster->registry();

	fe::sp<fe::Catalog> spCatalogOut=spMaster->createCatalog("Morph Data");

	spCatalogOut->catalogSet("sectionIndices","value","intarray","");
	fe::CatalogBuffer::resize(spCatalogOut,"sectionIndices","value",
			sectionCount);
	I32* pSectionIndices(NULL);
	I32 bufferCount=fe::CatalogBuffer::access(spCatalogOut,
			"sectionIndices","value",(void*&)pSectionIndices);
	if(!pSectionIndices)
	{
		feLog("AFEMorphGen::ExportVertexMap"
				" failed to create sectionIndices buffer\n");
		return false;
	}
	if(bufferCount!=sectionCount)
	{
		feLog("AFEMorphGen::ExportVertexMap"
				" sectionIndices buffer size %d/%d\n",bufferCount,vertexCount);
		return false;
	}

	for(I32 sectionIndex=0;sectionIndex<sectionCount;sectionIndex++)
	{
		pSectionIndices[sectionIndex]=sectionIndex;
	}

	spCatalogOut->catalogSet("vertexMap","value","intarray","");
	fe::CatalogBuffer::resize(spCatalogOut,"vertexMap","value",vertexCount);
	I32* pIndexMap(NULL);
	bufferCount=fe::CatalogBuffer::access(spCatalogOut,
			"vertexMap","value",(void*&)pIndexMap);
	if(!pIndexMap)
	{
		feLog("AFEMorphGen::ExportVertexMap"
				" failed to create vertexMap buffer\n");
		return false;
	}
	if(bufferCount!=vertexCount)
	{
		feLog("AFEMorphGen::ExportVertexMap"
				" vertexMap buffer size %d/%d\n",bufferCount,vertexCount);
		return false;
	}

	for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
	{
		pIndexMap[vertexIndex]=rVertexMap[vertexIndex];
	}

	fe::sp<fe::CatalogWriterI> spCatalogWriter=
			spRegistry->create("CatalogWriterI.*.*.json");
	if(spCatalogWriter.isNull())
	{
		feLog("AFEMorphGen::ExportVertexMap"
				" failed to create CatalogWriterI\n");
		return false;
	}

	feLog("AFEMorphGen::ExportVertexMap saving \"%s\"\n",filename.c_str());
	spCatalogWriter->save(filename,spCatalogOut);

	feLog("AFEMorphGen::ExportVertexMap done\n");

	return true;
#else
	return false;
#endif
}
