/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

class Serializable
{
public:
    virtual String serialize(void) const { return ""; };
    virtual void deserialize(String input) {};

    virtual bool operator==(const Serializable &other) const
    {
        if (typeid(*this) != typeid(other))
            return false;

        // Slow but all-encompasing comparison.
        return serialize() == other.serialize();
    };
};

template <class T>
class FE_DL_EXPORT InfoSerializable : public BaseType::Info
{
public:
    IWORD output(std::ostream &ostrm, void *instance, t_serialMode mode) override;
    void input(std::istream &istrm, void *instance, t_serialMode mode) override;
    String print(void *instance) override;
    void construct(void *instance) override;
    IWORD iosize(void) override;
};

template <class T>
class FE_DL_EXPORT InfoSp : public BaseType::Info
{
public:
    IWORD output(std::ostream &ostrm, void *instance, t_serialMode mode) override
    {
        if (mode == e_binary)
        {
            // For proper implementation we need protobuf.
            feX("Binary serialization not implemented.");
        }
        else
        {
            return c_ascii;
        }

    };
    void input(std::istream &istrm, void *instance, t_serialMode mode) override
    {
        if (mode == e_binary)
        {
            // For proper implementation we need protobuf.
            feX("Binary serialization not implemented.");
        }
    };
    String print(void *instance) override { return ""; };
    void construct(void *instance) override {};
    IWORD iosize(void) override { return 0; };
};

template <class T>
class FE_DL_EXPORT InfoSerializableArray : public BaseType::Info
{
public:
    IWORD output(std::ostream &ostrm, void *instance, t_serialMode mode) override;
    void input(std::istream &istrm, void *instance, t_serialMode mode) override;
    String print(void *instance) override;
    void construct(void *instance) override;
    IWORD iosize(void) override;

private:
    InfoIWORD helpIWORD;
};

template <class T>
IWORD InfoSerializableArray<T>::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
    if (mode == e_binary)
    {
        // For proper implementation we need protobuf.
        feX("Binary serialization not implemented.");
    }
    else
    {
        // Cast to array pointer so we can iterate it.
        Array<T> *pArray = (Array<T> *)instance;

        // Write array size so it can be read correctly.
        IWORD size = pArray->size();
        helpIWORD.output(ostrm, &size, t_serialMode::e_ascii);
        // Write array size delimiter.
        ostrm << "|";

        for (unsigned int i = 0; i < size; i++)
        {
            // Serialize to string.
            String output = pArray->at(i).serialize();

            // Write string with quotes and null terminate it.
            ostrm << "\"" << output.c_str() << "\"";// << '\0';
        }

        return c_ascii;
    }
};

template <class T>
void InfoSerializableArray<T>::input(std::istream &istrm, void *instance, t_serialMode mode)
{
    if (mode == e_binary)
    {
        // For proper implementation we need protobuf.
        feX("Binary serialization not implemented.");
    }
    else
    {
        // Cast to array and empty it.
        Array<T> *pArray = (Array<T> *)instance;
        pArray->empty();

        // Read array size so we can read in the correct number of elements.
        IWORD size;
        helpIWORD.input(istrm, &size, t_serialMode::e_ascii);
        // Skip array size delimter.
        istrm.ignore(1);

        // Read in specified number of array elements.
        for (unsigned int i = 0; i < size; i++)
        {
            // Read chars into string until we hit null character.
            std::string str;
            char c;
            while (istrm.get(c))
            {
                if (c == '\0')
                {
                    break;
                }
                str.push_back(c);
            }

            // Remove quotes.
            String input(str.c_str());
            input = input.parse();

            // Deserialize string.
            T item;
            item.deserialize(input);

            // Add it to array.
            pArray->push_back(item);
        }
    }
};

template <class T>
String InfoSerializableArray<T>::print(void *instance)
{
    Array<T> *pArray = (Array<T> *)instance;
    IWORD size = pArray->size();

    String s = "";

    for (unsigned int i = 0; i < size; i++)
    {
        s.cat(" <%s>", pArray->at(i).serialize().c_str());
    }
    return s;
};

template <class T>
IWORD InfoSerializableArray<T>::iosize(void)
{
    return sizeof(T);
};

template <class T>
void InfoSerializableArray<T>::construct(void *instance){};

template <class T>
IWORD InfoSerializable<T>::output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
    if (mode == e_binary)
    {
        // Code below is not reliable for network transmission. It only works if the
        // encoding/decoding code is compiled the same as it's sensitive to byte size, endianness,
        // and memory layout. For proper implementation we need protobuf.
        feX("Binary serialization not implemented.");

        // // Cast data to bytes.
        // char *buffer = reinterpret_cast<char *>(instance);
        // // Write bytes.
        // ostrm.write(buffer, iosize());
        // return iosize();
    }
    else
    {
        // Serialize to string.
        String output = ((T *)instance)->serialize();

        // Write string with quotes.
        ostrm << "\"" << output.c_str() << "\"";

        return c_ascii;
    }
};

template <class T>
void InfoSerializable<T>::input(std::istream &istrm, void *instance, t_serialMode mode)
{
    if (mode == e_binary)
    {
        // Code below is not reliable for network transmission. It only works if the
        // encoding/decoding code is compiled the same as it's sensitive to byte size, endianness,
        // and memory layout. For proper implementation we need protobuf.
        feX("Binary serialization not implemented.");

        // // Create byte buffer to read data into.
        // char buffer[iosize()];
        // // Read in bytes.
        // istrm.read(buffer, iosize());
        // // Cast the binary data to the target class and write it to the pointer target.
        // *(T *)instance = *reinterpret_cast<T *>(buffer);
    }
    else
    {
        // Read chars into string until we hit null character.
        std::string str;
        char c;
        while (istrm.get(c))
        {
            if (c == '\0')
            {
                break;
            }
            str.push_back(c);
        }

        // Remove quotes.
        String input(str.c_str());
        input = input.parse();

        // Deserialize string.
        ((T *)instance)->deserialize(input);
    }
};

template <class T>
String InfoSerializable<T>::print(void *instance)
{
    String s = "";
    s.cat(" <%s>", ((T *)instance)->serialize().c_str());
    return s;
};

template <class T>
IWORD InfoSerializable<T>::iosize(void)
{
    return sizeof(T);
};

template <class T>
void InfoSerializable<T>::construct(void *instance){};

} // namespace ext
} // namespace fe
