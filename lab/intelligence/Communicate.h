/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Communicate_h__
#define __intelligence_Communicate_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Manipulate connections between radios

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Communicate: public HandlerI
{
	public:
				Communicate(void)												{}

				//* as HandlerI
virtual void	handle(Record& record);


	private:
		void	merge(Record& rNeighborhood1,Record& rNeighborhood2);
		void	fill(WeakRecord rRadio,WeakRecord rNeighborhood);
		void	partition(Record& rNeighborhood);

		Accessor<SpatialVector>	m_aLocation;
		Accessor<String>		m_aFaction;

		Theater					m_theaterRV;
		Crowd					m_crowdRV1;
		Crowd					m_crowdRV2;
		RecordArrayView<Radio>	m_radioRAV;
		RecordArrayView<Radio>	m_radioRAV2;
		RecordArrayView<Radio>	m_radioRAV3;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Communicate_h__ */
