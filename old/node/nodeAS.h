/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __node_nodeAS_h__
#define __node_nodeAS_h__

#include "spatial/spatial.h"

namespace fe
{

class AsNode
	: public AccessorSet, public Initialize<AsNode>
{
	public:
		AsNode(void){}
		void initialize(void)
		{
			add(name,		FE_SPEC("node:name",	"node name"));
			add(content,	FE_SPEC("node:content",	"node container"));
		}
		/// name
		Accessor<String>					name;
		/// container
		Accessor<sp<RecordGroup> >			content;
};

} /* namespace */

#endif /* __node_nodeAS_h__ */

