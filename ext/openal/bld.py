import sys
import os
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "openal.pmh",
                "AudioAL",
                "ListenerAL",
                "VoiceAL",
                "freealut",
                "openalDL" ]

    dll = module.DLL( "fexOpenALDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs += [ "feMathLib" ]

    module.includemap = {}
    module.includemap['freealut'] = os.path.join(module.modPath,'freealut','include')

    dll.linkmap = {}

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["openal"] = " -lopenal"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        dll.linkmap["openal"] = " OpenAL32.lib"
    elif forge.fe_os == 'FE_OSX':
        dll.linkmap["openal"] = "-framework OpenAL"

    forge.deps( ["fexOpenALDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexOpenALDL",                      None,   None) ]

    if forge.media:
        forge.tests += [
            ("xAudio.exe",  "fexOpenALDL",                  None,   None) ]

def auto(module):
    test_file = """
#include "fe/config.h"
#include "platform/define.h"
#include "openal/openal_core.h"

#if !defined(ALUT_API_MAJOR_VERSION) || ALUT_API_MAJOR_VERSION<1
#error
#endif

int main(void)
{
//  alutInitWithoutContext(nullptr,nullptr);
    alGetError();
    return(0);
}
    \n"""

    includemap = {}
    linkmap = {}

    includemap['freealut'] = os.path.join(module.modPath,'freealut','include')

    if forge.fe_os == "FE_LINUX":
        linkmap["openal"] = " -lopenal"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        linkmap["openal"] = " OpenAL32.lib"
    elif forge.fe_os == 'FE_OSX':
        linkmap["openal"] = "-framework OpenAL"

    return forge.cctest(test_file, linkmap, includemap)
