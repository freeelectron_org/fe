/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terminal/terminal.pmh>

#define	FE_TMD_DEBUG		FALSE
#define	FE_TMD_TICKER		FALSE

namespace fe
{
namespace ext
{

TerminalDispatch::TerminalDispatch(void)
{
#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::TerminalDispatch\n");
#endif

#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(TerminalDispatch),"TerminalDispatch");
#endif
}

TerminalDispatch::~TerminalDispatch(void)
{
#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::~TerminalDispatch\n");
#endif

#if FE_COUNTED_TRACK
	Counted::deregisterRegion(this);
#endif
}

bool TerminalDispatch::call(const String &a_name,Array<Instance>& a_argv)
{
#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::call ordered \"%s\" %d\n",
			a_name.c_str(),a_argv.size());
#endif

#if FE_TMD_TICKER
	const U32 tick0=fe::systemTick();
#endif

	//* NOTE a_argv[0] is for output

	const I32 argc=a_argv.size();

#if	FE_TMD_DEBUG
	for(I32 index=0;index<argc;index++)
	{
		feLog("  %d/%d \"%s\"\n",index,argc,
				a_argv[index].data()? c_print(a_argv[index]): "NULL");
	}
#endif

	InstanceMap instanceMap;

	sp<Catalog> spCatalog=m_terminalNode.catalog();

	if(spCatalog.isValid() && argc>0)
	{
		const String outputKey=spCatalog->catalog<String>("output:0");
		instanceMap[outputKey]=a_argv[0];

		for(I32 index=1;index<argc;index++)
		{
			Instance& rInstance=a_argv[index];

			const I32 channel=index-1;

			String lookup;
			lookup.sPrintf("input:%d",channel);

			const String inputKey=spCatalog->catalog<String>(lookup);
			if(inputKey.empty())
			{
				feLog("TerminalDispatch::call no \"%s\" expected for \"%s\"\n",
						lookup.c_str(),spCatalog->name().c_str());
			}

			FEASSERT(!inputKey.empty());

			instanceMap[inputKey]=rInstance;
		}
	}

	if(!call(a_name,instanceMap))
	{
		return false;
	}

	if(spCatalog.isValid())
	{
		const String outputKey=spCatalog->catalog<String>("output:0");
		a_argv[0]=instanceMap[outputKey];
	}

#if FE_TMD_TICKER
	const U32 tick1=fe::systemTick();

	const U32 tickDiff01=fe::SystemTicker::tickDifference(tick0,tick1);

	const fe::Real msPerTick=1e-3*fe::SystemTicker::microsecondsPerTick();
	const fe::Real ms01=tickDiff01*msPerTick;

	feLog("TerminalDispatch::call ordered  %.2f ms (includes mapped)\n",ms01);
#endif

	return true;
}

bool TerminalDispatch::call(const String &a_name,InstanceMap& a_argv)
{
	const Real frame=a_argv.exists("frame")?
			Real(a_argv["frame"].cast<double>()): Real(0);

#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::call map \"%s\" %d frame %.6G dispatch %p\n",
			a_name.c_str(),a_argv.size(),frame,this);
#endif

#if FE_TMD_TICKER
	const U32 tick0=fe::systemTick();
#endif

	sp<Catalog> spCatalog=m_terminalNode.catalog();

	const String outputKey=spCatalog.isValid()?
			spCatalog->catalog<String>("output:0"): String();

	//* TODO global 'allow clobber' variable

	BWORD recycle=FALSE;
	BWORD clobber=FALSE;
	String copy;

	if(spCatalog.isValid())
	{
		copy=spCatalog->catalogOrDefault<String>(outputKey,"copy","");
		clobber=spCatalog->catalogOrDefault<bool>(outputKey,"clobber",false);
		recycle=spCatalog->catalogOrDefault<bool>(outputKey,"recycle",false);

#if FE_TMD_DEBUG
		feLog("  output \"%s\" copies \"%s\" recycle %d\n",
				outputKey.c_str(),copy.c_str(),recycle);
#endif

		I32 inputIndex=0;
		for(InstanceMap::iterator it=a_argv.begin();it!=a_argv.end();it++)
		{
			const String key=it->first;
			Instance& rInstance=it->second;

#if	FE_TMD_DEBUG
			feLog("  key \"%s\"\n",key.c_str());
#endif

			if(key==outputKey || !rInstance.is<Record>())
			{
				continue;
			}

			sp<SurfaceAccessibleI> spInputAccessible;

			BWORD replaced=TRUE;

			if(rInstance.data())
			{
				Record record=rInstance.cast<Record>();

				if(m_accessorAccessible.scope()==record.layout()->scope() &&
						m_accessorAccessible.check(record))
				{
#if	FE_TMD_DEBUG
					feLog("    as Component\n");
#endif
					spInputAccessible=m_accessorAccessible(record);

					if(I32(m_lastInputs.size())<=inputIndex)
					{
						m_lastInputs.resize(inputIndex+1);
					}
					if(m_lastInputs[inputIndex]==spInputAccessible)
					{
						replaced=FALSE;
					}
					else
					{
						m_lastInputs[inputIndex]=spInputAccessible;
					}
				}
				else
				{
					feLog("TerminalDispatch::call map"
							" has incompatible instance\n");
				}
			}

			spCatalog->catalog< sp<Component> >(key)=spInputAccessible;
			spCatalog->catalog<bool>(key,"replaced")=replaced;

#if	FE_TMD_DEBUG
			feLog("    points %d primitives %d replaced %d\n",
					spInputAccessible->count(SurfaceAccessibleI::e_point),
					spInputAccessible->count(SurfaceAccessibleI::e_primitive),
					replaced);
#endif

			inputIndex=0;
		}
	}

	//* TODO respect recycle

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(spCatalog.isValid() && !copy.empty())
	{
		sp<SurfaceAccessibleI> spSourceAccessible=
				spCatalog->catalog< sp<Component> >(copy);

		if(clobber)
		{
#if	FE_TMD_DEBUG
			feLog("  CLOBBER INPUT \"%s\"\n",copy.c_str());
#endif
			spOutputAccessible=spSourceAccessible;
		}
		else if(spSourceAccessible.isValid())
		{
#if	FE_TMD_DEBUG
			feLog("  CLONE INPUT  \"%s\"\n",copy.c_str());
			feLog("    of \"%s\"\n",
					spSourceAccessible->factoryName().c_str());
#endif
			sp<Protectable> spProtectable(spSourceAccessible->clone());
			spOutputAccessible=spProtectable;
		}

		FEASSERT(spOutputAccessible.isValid());

		spOutputAccessible->setName(spSourceAccessible->name());
//		spOutputAccessible->setName(outputName);

		spCatalog->catalog< sp<Component> >(outputKey)=spOutputAccessible;
	}

	if(spOutputAccessible.isNull())
	{
		spOutputAccessible=spCatalog->catalog< sp<Component> >(outputKey);
		if(spOutputAccessible.isNull())
		{
#if	FE_TMD_DEBUG
			feLog("  CREATE NEW OUTPUT\n");
#endif

			//* TODO more selective
			const BWORD quiet=TRUE;
			spOutputAccessible=
					registry()->create("*.SurfaceAccessibleOpenCL",quiet);

			if(spOutputAccessible.isNull())
			{
				spOutputAccessible=
						registry()->create("*.SurfaceAccessibleRecord");

				if(spOutputAccessible.isValid())
				{
					//* independent attributes per mesh
					sp<Scope> spScope=registry()->create("Scope");
					spScope->setLocking(FALSE);

					spOutputAccessible->bind(spScope);
					spOutputAccessible->clear();
				}
			}

			if(spCatalog.isValid())
			{
				spCatalog->catalog< sp<Component> >(outputKey)=
						spOutputAccessible;
			}
		}
#if	FE_TMD_DEBUG
		else
		{
			feLog("  REUSE PREVIOUS OUTPUT (%d references)\n",
					spOutputAccessible->Counted::count());
		}
#endif

		if(spOutputAccessible.isValid())
		{
#if	FE_TMD_DEBUG
			feLog("    at %p of \"%s\"\n",spOutputAccessible.raw(),
					spOutputAccessible->factoryName().c_str());
			feLog("  CLEAR\n");
#endif
			spOutputAccessible->clear();
		}
	}

#if FE_TMD_TICKER
	const U32 tick1=fe::systemTick();
#endif

#if	FE_TMD_DEBUG
	feLog("  COOK\n");
#endif

#if	FE_TMD_DEBUG
	feLog("  PRECOOK points %d primitives %d\n",
			spOutputAccessible->count(SurfaceAccessibleI::e_point),
			spOutputAccessible->count(SurfaceAccessibleI::e_primitive));
#endif

	m_terminalNode.cook(frame);

#if	FE_TMD_DEBUG
	feLog("  COOKED points %d primitives %d\n",
			spOutputAccessible->count(SurfaceAccessibleI::e_point),
			spOutputAccessible->count(SurfaceAccessibleI::e_primitive));
#endif

	if(spCatalog.isValid() && a_argv.size()>0)
	{
		m_outputRecord=m_spScope->createRecord(m_spSurfaceLayout);
		m_accessorAccessible(m_outputRecord)=spOutputAccessible;

		const sp<TypeMaster>& spTM = registry()->master()->typeMaster();
		a_argv[outputKey].set<Record>(spTM,m_outputRecord);
	}

#if FE_TMD_TICKER
	const U32 tick2=fe::systemTick();

	const U32 tickDiff01=fe::SystemTicker::tickDifference(tick0,tick1);
	const U32 tickDiff12=fe::SystemTicker::tickDifference(tick1,tick2);

	const fe::Real msPerTick=1e-3*fe::SystemTicker::microsecondsPerTick();
	const fe::Real ms01=tickDiff01*msPerTick;
	const fe::Real ms12=tickDiff12*msPerTick;

	feLog("TerminalDispatch::call map prep %.2f ms\n",ms01);
	feLog("TerminalDispatch::call map cook %.2f ms\n",ms12);
#endif

#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::call \"%s\" done\n",a_name.c_str());
#endif

	return true;
}

bool TerminalDispatch::configLookup(const String &a_key,Instance &a_instance)
{
#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::configLookup \"%s\"\n",a_key.c_str());
#endif

	if(a_key=="_scope_")
	{
		const sp<TypeMaster>& spTM = registry()->master()->typeMaster();
		a_instance.create< sp<Scope> >(spTM);

#if	FE_TMD_DEBUG
		feLog("  token scope\n");
#endif

		return true;
	}

	sp<Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
#if	FE_TMD_DEBUG
		feLog("  no catalog\n");
#endif
		return false;
	}

	if(!spCatalog->catalogLookup(a_key,a_instance))
	{
#if	FE_TMD_DEBUG
		feLog("  key not found\n");
#endif
		return false;
	}

#if	FE_TMD_DEBUG
	feLog("  catalog entry \"%s\"\n",c_print(a_instance));
#endif

	return true;
}

void TerminalDispatch::configSet(const String &a_key, Instance &a_instance)
{
#if	FE_TMD_DEBUG
	feLog("TerminalDispatch::configSet \"%s\" \"%s\"\n",
			a_key.c_str(),c_print(a_instance));
#endif

	if(a_key=="_scope_")
	{
		m_spScope=a_instance.cast< sp<Scope> >();

		m_spSurfaceLayout=m_spScope->declare("surface");
		m_spScope->support("accessible","Component");
		m_spScope->populate("surface","accessible");
		m_accessorAccessible.setup(m_spScope,"accessible");

		return;
	}

	if(a_key=="_implementation_")
	{
		if(m_spScope.isNull())
		{
			feLog("TerminalDispatch::configSet implementation without scope\n");
			return;
		}

		const String implementation=a_instance.cast<String>();
		const String name=implementation;

		m_terminalNode.setupOperator(m_spScope,implementation,name);

		return;
	}

	sp<Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feLog("  no catalog\n");
		return;
	}

	spCatalog->catalogInstance(a_key)=a_instance;
}

} /* namespace ext */
} /* namespace fe */
