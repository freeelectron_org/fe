/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		sp<TypeMaster> spTypeMaster(new TypeMaster());
		assertCore(spTypeMaster);
		sp<Allocator> spAllocator(new BasicAllocator());
		sp<Scope> spScope(new Scope(spTypeMaster, spAllocator));

		sp<Layout> spLayout=spScope->declare("sack");
		spScope->support("quantity", "integer");
		spScope->populate("sack", "quantity");
		spScope->populate("sack", FE_USE(":SN"));

		Accessor<I32> aSN;
		aSN.setup(spScope, FE_USE(":SN"), "integer");
		Accessor<I32> aQuantity;
		aQuantity.setup(spScope, "quantity", "integer");

		feLog("\n>>>>> single record\n");

		Record first=spScope->createRecord(spLayout);
		feLog("aSN(first)=%d\n",aSN(first));
		Record second=spScope->createRecord(spLayout);
		feLog("aSN(second)=%d\n",aSN(second));

		Record record=spScope->createRecord(spLayout);
		WeakRecord weakRecord=record;

		aQuantity(record)=7;

		I32 recordSN=aSN(record);
		feLog("aSN(record)=%d\n",recordSN);
		I32 weakSN=aSN(weakRecord);
		feLog("aSN(weakRecord)=%d\n",weakSN);

		BWORD recordValid=record.isValid();
		feLog("record.isValid()=%d\n",recordValid);
		BWORD weakValid=weakRecord.isValid();
		feLog("weakRecord.isValid()=%d\n",weakValid);

		UNIT_TEST(recordValid);
		UNIT_TEST(weakValid);

		I32 recordInt=aQuantity(record);
		feLog("aQuantity(record)=%d\n",recordInt);
		I32 weakInt=aQuantity(weakRecord);
		feLog("aQuantity(weakRecord)=%d\n",weakInt);

		UNIT_TEST(recordInt==7);
		UNIT_TEST(weakInt==7);

		feLog(">>>>> clear record\n");
		record=Record();

		recordValid=record.isValid();
		feLog("record.isValid()=%d\n",recordValid);
		weakValid=weakRecord.isValid();
		feLog("weakRecord.isValid()=%d\n",weakValid);

		UNIT_TEST(!recordValid);
		UNIT_TEST(!weakValid);

		record=spScope->createRecord(spLayout);
		aQuantity(record)=5;


		feLog("\n>>>>> add record to group\n");

		sp<RecordGroup> spRG(new RecordGroup);
		spRG->setWeak(TRUE);
		spRG->add(record);
		U32 count=spRG->count();
		feLog("RG count=%d\n",count);
		U32 size=spRG->size();
		feLog("RG size=%d\n",size);

		UNIT_TEST(count==1);
		UNIT_TEST(size==1);

		RecordGroup::iterator it=spRG->begin();
		sp<RecordArray> spRA(*it);
		Record r=spRA->getRecord(0);
		recordValid=r.isValid();
		feLog("RG RA [0] isValid=%d\n",recordValid);

		UNIT_TEST(recordValid);

		r=Record();

		feLog(">>>>> clear record\n");
		record=Record();

		count=spRG->count();
		feLog("RG count=%d\n",count);
		size=spRG->size();
		feLog("RG size=%d\n",size);

		UNIT_TEST(count==1);
		UNIT_TEST(size==1);

		it=spRG->begin();
		spRA=*it;
		r=spRA->getRecord(0);
		recordValid=r.isValid();
		feLog("RG RA [0] isValid=%d\n",recordValid);

		UNIT_TEST(!recordValid);

		spRG->prune();

		count=spRG->count();
		feLog("RG count=%d\n",count);
		size=spRG->size();
		feLog("RG size=%d\n",size);

		UNIT_TEST(count==0);
//		UNIT_TEST(size==0);

		r=Record();

		completed=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(14);
	UNIT_RETURN();
}

