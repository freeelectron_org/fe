/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_InfluenceOp_h__
#define __operator_InfluenceOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Aggregate point influences

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT InfluenceOp:
	public OperatorSurfaceCommon,
	public Initialize<InfluenceOp>
{
	public:

					InfluenceOp(void)										{}
virtual				~InfluenceOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_InfluenceOp_h__ */
