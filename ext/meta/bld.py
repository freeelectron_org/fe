import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["surface", "window"]

def setup(module):
    srcList = [ "meta.pmh",
                "MetaBrush",
                "MetaGraph",
                "MetaPlugin",
                "OperatorContext",
                "metaDL" ]

    dll = module.DLL( "fexMetaDL", srcList )

    deplibs = forge.corelibs+ [
                "fexDrawDLLib",
                "fexSurfaceDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexMetaDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexMetaDL",                    None,   None) ]
