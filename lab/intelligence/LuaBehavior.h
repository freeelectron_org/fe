/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_LuaBehavior_h__
#define __intelligence_LuaBehavior_h__

#include "lua/LuaI.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Load a Lua script and run it when signaled

	@ingroup intelligence

	A behavior can get bound to a ControlCenter's self-signaler.
	The signal is the ControlCenter record itself.

	A LuaBehavior is potentially created as an attribute of a general
	Behavior, when it is deemed to be the appropriate pluggable component
	by the Behavior configuration.

	As a RecordableI, a LuaBehavior can receive a Behavior in a bind.
	From this, the LuaBehavior can derive pertinent configuration.
*//***************************************************************************/
class FE_DL_EXPORT LuaBehavior: virtual public HandlerI,
		virtual public RecordableI
{
	public:
				LuaBehavior(void)											{}

				//* as HandlerI
virtual void	handle(Record& record);

				//* as RecordableI
virtual void	bind(Record& record)	{ m_recorded=record; }

	private:

		WeakRecord				m_recorded;
		sp<LuaI>				m_spLuaI;

		ControlCenter			m_controlRV;
		Behavior				m_behaviorRV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_LuaBehavior_h__ */
