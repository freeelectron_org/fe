import sys
forge = sys.modules["forge"]

def setup(module):

    srcList = [ "SimpleGrid",
                "DrawAtoms",
                "DrawPairs",
                "DrawPoints",
                "DrawVectorField",
                "DrawPick",
                "ExplicitInertial",
                "miscDL" ]

    dll = module.DLL( "fexMiscDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolLib",
                "fexViewerLib",
                "fexWindowDLLib",
                "fexSpatialDLLib"
                ]

    forge.deps( ["fexMiscDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexMiscDL",                    None,       None) ]

