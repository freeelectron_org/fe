/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_PipelineI_h__
#define __architecture_PipelineI_h__
namespace fe
{
namespace ext
{

/**	application framework interface
	*/
class FE_DL_EXPORT PipelineI
	: virtual public Component
{
	public:
virtual	void	attach(	sp<ApplicationI> a_application,
						sp<SignalerI> a_signaler,
						sp<SequencerI> a_sequencer,
						sp<Layout> l_hb,
						sp<RecordGroup> rg_input)							= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_PipelineI_h__ */

