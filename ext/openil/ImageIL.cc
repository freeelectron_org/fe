/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <openil/openil.pmh>

namespace fe
{
namespace ext
{

ImageIL::ImageIL(void):
	m_selected(-1)
{
	ilInit();
	iluInit();
//	ilutInit();
//	ilutRenderer(ILUT_OPENGL);

	iluImageParameter(ILU_FILTER,ILU_SCALE_BELL);	//* arbitrary
	ilEnable(IL_FILE_OVERWRITE);
//	ilEnable(IL_ORIGIN_SET);
//	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
}

ImageIL::~ImageIL(void)
{
}

I32 ImageIL::createSelect(void)
{
	ILuint id;
	ilGenImages(1,&id);
	ilBindImage(id);

	m_selected=id;
	incrementSerial();
	return id;
}

I32 ImageIL::loadSelect(String filename)
{
	ILuint id;
	ilGenImages(1,&id);
	ilBindImage(id);
	if(!ilLoadImage((ILconst_string)filename.c_str()))
	{
		feLog("ImageIL::loadSelect failed to load \"%s\"\n",
				filename.c_str());

		ilDeleteImages(1,(ILuint*)&id);
		return -1;
	}

	m_selected=id;
	incrementSerial();
	return id;
}

I32 ImageIL::interpretSelect(String a_source)
{
	//* would this ever make sense?
	return interpretSelect(const_cast<char*>(a_source.c_str()),
			a_source.length());
}

I32 ImageIL::interpretSelect(void* data,U32 size)
{
	ILuint id;
	ilGenImages(1,&id);
	ilBindImage(id);
	if(!ilLoadL(IL_TYPE_UNKNOWN,data,size))
	{
		ilDeleteImages(1,(ILuint*)&id);
		return -1;
	}

	m_selected=id;
	incrementSerial();
	return id;
}

BWORD ImageIL::save(String filename)
{
	if(m_selected<0)
	{
		return FALSE;
	}

	ilBindImage((ILuint)m_selected);
	ILboolean success=ilSaveImage((ILconst_string)filename.c_str());
	return success!=0;
}

void ImageIL::select(I32 id)
{
	m_selected=id;
}

void ImageIL::unload(I32 id)
{
	ilDeleteImages(1,(ILuint*)&id);
}

void ImageIL::setFormat(ImageI::Format format)
{
	if(m_selected<0)
	{
		return;
	}

	ilBindImage((ILuint)m_selected);

	ILenum formatid=IL_COLOUR_INDEX;
	switch(format)
	{
		case e_rgb:
			formatid=IL_RGB;
			break;
		case e_rgba:
			formatid=IL_RGBA;
			break;
		default:
			break;
	}

//	feLog("ImageIL::setFormat %d %d %d %d %d %d\n",width(),height(),depth(),
//			bppID(formatid),formatid,typeID(formatid));

	ILboolean success=ilTexImage(width(),height(),depth(),
			bppID(formatid),formatid,typeID(formatid),NULL);

	if(!success)
	{
		feX("ImageIL::setFormat","ilTexImage failed");
	}

	incrementSerial();
}

void ImageIL::resize(U32 width,U32 height,U32 depth)
{
	if(m_selected<0)
	{
		return;
	}

	ilBindImage((ILuint)m_selected);

#ifdef __x86_64__

#if FE_CODEGEN<=FE_DEBUG
	feLog("ImageIL::resize"
			" WARNING, x86_64 OpenIL workaround discards old image\n");
#endif

//	feLog("ImageIL::resize %d %d %d %d %d %d\n",width,height,depth,
//			bppID(formatID()),formatID(),typeID(formatID()));

	ILboolean success=ilTexImage(width,height,depth,
			bppID(formatID()),formatID(),typeID(formatID()),NULL);

	if(!success)
	{
		feX("ImageIL::resize","ilTexImage failed");
	}
#else
	iluScale((ILuint)width,(ILuint)height,(ILuint)depth);
#endif

	incrementSerial();
}

void ImageIL::replaceRegion(U32 x,U32 y,U32 z,
		U32 width,U32 height,U32 depth,void* data)
{
	if(m_selected<0)
	{
		return;
	}

	ilBindImage((ILuint)m_selected);
	ilSetPixels(x,y,z,width,height,depth,formatID(),typeID(formatID()),data);

	incrementSerial();
}

ILenum ImageIL::formatID(void) const
{
	if(m_selected<0)
	{
		return 0;
	}

	ilBindImage((ILuint)m_selected);
	return ilGetInteger(IL_IMAGE_FORMAT);
}

ILenum ImageIL::typeID(ILenum formatid) const
{
	switch(formatid)
	{
		case IL_COLOUR_INDEX:
			return IL_UNSIGNED_SHORT;
	}
	return IL_UNSIGNED_BYTE;
}

ILubyte ImageIL::bppID(ILenum formatid) const
{
	switch(formatid)
	{
		case IL_COLOUR_INDEX:
			return 2;
		case IL_RGB:
			return 3;
		case IL_RGBA:
			return 4;
	}
	return 0;
}

ImageI::Format ImageIL::format(void) const
{
	switch(formatID())
	{
		case IL_COLOUR_INDEX:
			return e_colorindex;
		case IL_RGB:
			return e_rgb;
		case IL_RGBA:
			return e_rgba;
	}
	return e_none;
}

U32 ImageIL::width(void) const
{
	if(m_selected<0)
	{
		return 0;
	}

	ilBindImage((ILuint)m_selected);
	return ilGetInteger(IL_IMAGE_WIDTH);
}

U32 ImageIL::height(void) const
{
	if(m_selected<0)
	{
		return 0;
	}

	ilBindImage((ILuint)m_selected);
	return ilGetInteger(IL_IMAGE_HEIGHT);
}

U32 ImageIL::depth(void) const
{
	if(m_selected<0)
	{
		return 0;
	}

	ilBindImage((ILuint)m_selected);
	return ilGetInteger(IL_IMAGE_DEPTH);
}

void* ImageIL::raw(void) const
{
//	feLog("ImageIL::raw %d %d %p\n",
//			ilGetInteger(IL_ACTIVE_IMAGE),
//			ilGetInteger(IL_CUR_IMAGE),
//			ilGetData());

	if(m_selected<0)
	{
		return NULL;
	}

	ilBindImage((ILuint)m_selected);
	return ilGetData();
}

} /* namespace ext */
} /* namespace fe */
