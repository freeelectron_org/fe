/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_ChunkReceiver_h__
#define __netsignal_ChunkReceiver_h__
namespace fe
{
namespace ext
{

class ChunkReceiver;

class FE_DL_EXPORT ChunkReceiverTask: public Thread::Functor
{
	public:
						ChunkReceiverTask(void);
virtual	void			operate(void);

		sp<ChunkSpool>		m_spChunkSpool;
		sp<Socket>			m_spSocket;
		ChunkReceiver		*m_pChunkReceiver;

		Array< sp<Chunk> >	m_spChunkPool;
		I32					m_poolIndex;
};

class FE_DL_EXPORT ChunkReceiver : public Component
{
	public:
				ChunkReceiver(void);
virtual			~ChunkReceiver(void);

virtual	void	start(sp<ChunkSpool> spChunkSpool, sp<Socket> spSocket,
					String ioPriority="normal");
virtual	void	stop(void);
virtual	bool	live(void);

virtual	void	setLive(bool setting);

		void	setPolling(sp<SignalerI> a_spSignaler,
						sp<Layout> a_spPollLayout);
		void	poll(void);

	private:
		Thread*				m_pChunkReceiverThread;
		ChunkReceiverTask	m_chunkReceiverTask;
		bool				m_operating;
		Poison				m_poison;
		bool				m_live;
		Mutex				m_liveMutex;
		sp<SignalerI>		m_spSignaler;
		Record				m_poll;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_ChunkReceiver_h__ */

