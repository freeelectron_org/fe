/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SDO_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceDeltaOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("deltaPAttr")="deltaP";
	catalog<String>("deltaPAttr","label")="Delta P Attribute";
	catalog<String>("deltaPAttr","hint")=
			"Name of vector point attribute to write"
			" the difference in position.";

	catalog<String>("deltaNAttr")="deltaN";
	catalog<String>("deltaNAttr","label")="Delta N Attribute";
	catalog<String>("deltaNAttr","hint")=
			"Name of vector point attribute to write"
			" the difference in normal.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");
}

void SurfaceDeltaOp::handle(Record& a_rSignal)
{
#if FE_SDO_DEBUG
	feLog("SurfaceDeltaOp::handle\n");
#endif
	const String deltaPAttr=catalog<String>("deltaPAttr");
	const String deltaNAttr=catalog<String>("deltaNAttr");

	catalog<String>("summary")="OFF";

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spRefAccessible;
	if(!access(spRefAccessible,"Reference Surface")) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputNormal;
	if(!access(spOutputNormal,spOutputAccessible,e_point,e_normal)) return;

	sp<SurfaceAccessorI> spDeltaPoint;
	if(!access(spDeltaPoint,spOutputAccessible,e_point,deltaPAttr)) return;

	sp<SurfaceAccessorI> spDeltaNormal;
	if(!access(spDeltaNormal,spOutputAccessible,e_point,deltaNAttr)) return;

	sp<SurfaceAccessorI> spRefPoint;
	if(!access(spRefPoint,spRefAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spRefNormal;
	if(!access(spRefNormal,spRefAccessible,e_point,e_normal)) return;

	const I32 outputPointCount=spOutputPoint->count();
	const I32 refPointCount=spRefPoint->count();

	const I32 pointCount=fe::minimum(outputPointCount,refPointCount);
	const I32 maxCount=fe::maximum(outputPointCount,refPointCount);

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector outputPoint=
				spOutputPoint->spatialVector(pointIndex);
		const SpatialVector outputNormal=
				spOutputNormal->spatialVector(pointIndex);

		const SpatialVector refPoint=spRefPoint->spatialVector(pointIndex);
		const SpatialVector refNormal=spRefNormal->spatialVector(pointIndex);

		const SpatialVector deltaPoint=outputPoint-refPoint;
		const SpatialVector deltaNormal=outputNormal-refNormal;

		spDeltaPoint->set(pointIndex,deltaPoint);
		spDeltaNormal->set(pointIndex,deltaNormal);
	}

	SpatialVector zero(0,0,0);
	for(I32 pointIndex=pointCount;pointIndex<outputPointCount;pointIndex++)
	{
		spDeltaPoint->set(pointIndex,zero);
		spDeltaNormal->set(pointIndex,zero);
	}

	String summary;
	if(pointCount==maxCount)
	{
		summary.sPrintf("%d points",pointCount);
	}
	else
	{
		summary.sPrintf("%d/%d points",pointCount,maxCount);
	}
	catalog<String>("summary")=summary;

#if FE_SDO_DEBUG
	feLog("SurfaceDeltaOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
