/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "UnitThrust.h"
#include "UnitRepulse.h"
#include "FormDrag.h"
#include "OpaqueDrag.h"
#include "Jitter.h"
#include "SimpleNav.h"
#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
{
}

FE_DL_EXPORT fe::Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<UnitThrust>("HandlerI.UnitThrust.mobility.fe");
	pLibrary->add<UnitRepulse>("HandlerI.UnitRepulse.mobility.fe");
	pLibrary->add<Jitter>("HandlerI.Jitter.mobility.fe");
	pLibrary->add<SimpleNav>("HandlerI.SimpleNav.mobility.fe");
	pLibrary->add<FormDrag>("HandlerI.FormDrag.mobility.fe");
	pLibrary->add<OpaqueDrag>("HandlerI.OpaqueDrag.mobility.fe");
	pLibrary->add<AsMobile>("PopulateI.AsMobile.mobility.fe");

	return pLibrary;
}

}
