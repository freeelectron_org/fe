/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_EXC_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void ExcarnateOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	catalog<String>("Group");
	catalog<String>("Group","label")="Point Group";
	catalog<String>("Group","suggest")="pointGroups";

	catalog<String>("startFrom")="rootPoint";
	catalog<String>("startFrom","label")="Start From";
	catalog<String>("startFrom","choice:0")="rootPoint";
	catalog<String>("startFrom","label:0")="Closest to Root";
	catalog<String>("startFrom","choice:1")="lowest";
	catalog<String>("startFrom","label:1")="Lowest Index";
	catalog<String>("startFrom","hint")=
			"Selection method of remaining points to start each curve.";

	catalog<SpatialVector>("RootPoint");
	catalog<String>("RootPoint","label")="Root";
	catalog<String>("RootPoint","hint")=
			"Approximate starting location for first point";

	catalog<bool>("Degenerate")=true;

	catalog<I32>("Reductions")=1;
	catalog<I32>("Reductions","high")=10;
	catalog<I32>("Reductions","max")=100;

	catalog<bool>("Expand")=false;

	catalog<Real>("Fusion")=0.01;
	catalog<Real>("Fusion","high")=1.0;
	catalog<String>("Fusion","enabler")="Expand";

	catalog<bool>("WriteSurface")=false;
	catalog<String>("WriteSurface","label")="Output Degenerate Surface";

	catalog<bool>("WriteLines")=true;
	catalog<String>("WriteLines","label")="Output Skeleton Lines";

	catalog<String>("pointAttributes");
	catalog<String>("pointAttributes","label")="Copy Point Attributes";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void ExcarnateOp::handle(Record& a_rSignal)
{
	String& rSummary=catalog<String>("summary");
	rSummary="";

	const SpatialVector rootPoint=catalog<SpatialVector>("RootPoint");
	const I32 reductions=catalog<I32>("Reductions");
	const Real fusion=catalog<Real>("Fusion");
	const bool expand=catalog<bool>("Expand");
	const bool degenerate=catalog<bool>("Degenerate");
	const bool writeSurface=catalog<bool>("WriteSurface");
	const bool writeLines=catalog<bool>("WriteLines");

	const String group=catalog<String>("Group");
	const BWORD grouped=(!group.empty());

	const String startFrom=catalog<String>("startFrom");
	const BWORD startFromRoot=(startFrom=="rootPoint");

	//* will create lines as new geometry
	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	sp<SurfaceAccessibleI> spSurfaceInput;
	if(!access(spSurfaceInput,"Input Surface")) return;

	sp<SurfaceAccessibleI> spSurfaceOutput;
	if(!accessOutput(spSurfaceOutput,a_rSignal)) return;

	if(writeSurface)
	{
		const SpatialTransform xform=SpatialTransform::identity();
		spSurfaceOutput->append(spSurfaceInput,&xform);
	}

	sp<SurfaceAccessorI> spInputPrimitives;
	if(!access(spInputPrimitives,spSurfaceInput,
			e_primitive,e_vertices)) return;
	const U32 primitiveCount=spInputPrimitives->count();
	if(!primitiveCount)
	{
		feLog("ExcarnateOp::handle primitive count is zero\n");
		catalog<String>("error")="primitives required";
		return;
	}

	sp<SurfaceAccessorI> spInputPoint;
	sp<SurfaceAccessorI> spOutputPoint;
	if(grouped)
	{
		if(!access(spInputPoint,spSurfaceInput,e_pointGroup,group)) return;
		if(!access(spOutputPoint,spSurfaceOutput,e_pointGroup,group)) return;
	}
	else
	{
		if(!access(spInputPoint,spSurfaceInput,e_point,e_position)) return;
		if(!access(spOutputPoint,spSurfaceOutput,e_point,e_position)) return;
	}

	const U32 groupCount=spInputPoint->count();
	if(!groupCount)
	{
		feLog("ExcarnateOp::handle point count is zero\n");
		catalog<String>("error")="points required";
		return;
	}

	Array<SpatialVector> inputBuffer(groupCount);
	Array<SpatialVector> outputBuffer(groupCount);

	for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
	{
		outputBuffer[groupIndex]=spInputPoint->spatialVector(groupIndex);
	}

	if(!degenerate)
	{
		for(I32 pass=0;pass<reductions;pass++)
		{
			//* TODO double buffer
			for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
			{
				inputBuffer[groupIndex]=outputBuffer[groupIndex];
			}
			for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
			{
				const SpatialVector point=inputBuffer[groupIndex];

				const I32 pointIndex=
						grouped? spInputPoint->integer(groupIndex): groupIndex;

				SpatialVector tweakSum=point;
				Real tweakWeight=1.0;

				for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
						primitiveIndex++)
				{
					const U32 subCount=
							spInputPrimitives->subCount(primitiveIndex);
					U32 subMatch;
					for(subMatch=0;subMatch<subCount;subMatch++)
					{
						if(spInputPrimitives->integer(
								primitiveIndex,subMatch)==pointIndex)
						{
							break;
						}
					}
					if(subMatch==subCount)
					{
						continue;
					}
					for(U32 subIndex=0;subIndex<subCount;subIndex++)
					{
						if(subIndex==subMatch)
						{
							continue;
						}
						const U32 otherIndex=spInputPrimitives->integer(
								primitiveIndex,subIndex);

						tweakSum+=inputBuffer[otherIndex];
						tweakWeight+=1.0;
					}
				}

				outputBuffer[groupIndex]=tweakSum/tweakWeight;
			}
		}
	}

	Array<I32> masterIndex(groupCount);
	Array<I32> resolved(groupCount);
	for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
	{
		masterIndex[groupIndex]= -1;
		resolved[groupIndex]=FALSE;
	}

	if(degenerate)
	{
		Array<U32> weightCount(groupCount);
		for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			weightCount[groupIndex]=1;
		}

		for(I32 pass=0;pass<reductions;pass++)
		{
			for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				if(interrupted())
				{
					pass=reductions;
					break;
				}

				const U32 subCount=spInputPrimitives->subCount(primitiveIndex);
				if(subCount<3 || subCount>4)
				{
					continue;
				}

#if FE_EXC_DEBUG
				String text;
				text.sPrintf("prim %d pts",primitiveIndex);
#endif

				U32 vertIndex[4];
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					U32 pointIndex=spInputPrimitives->integer(
							primitiveIndex,subIndex);

#if FE_EXC_DEBUG
					String text2;
					text2.sPrintf(" %d",pointIndex);
					text+=text2;
#endif

					while(masterIndex[pointIndex]>=0)
					{
						pointIndex=masterIndex[pointIndex];

#if FE_EXC_DEBUG
						text2.sPrintf("(%d)",pointIndex);
						text+=text2;
#endif
					}
					vertIndex[subIndex]=pointIndex;
				}
#if FE_EXC_DEBUG
				feLog("%s\n",text.c_str());
#endif

				U32 remaining=0;
				U32 mergeIndex0=0;
				U32 mergeIndex1=0;
				Real leastMag2= -1.0;
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const U32 check0=vertIndex[subIndex];
					const U32 check1=vertIndex[(subIndex+1)%subCount];
					if(check0==check1)
					{
						continue;
					}

					const Real mag2=magnitudeSquared(
							outputBuffer[check1]-outputBuffer[check0]);
					if(!remaining || leastMag2>mag2)
					{
						leastMag2=mag2;
						mergeIndex0=check0;
						mergeIndex1=check1;
					}
					remaining++;
				}
				if(remaining<3)
				{
					continue;
				}
				U32& rWeightCount0=weightCount[mergeIndex0];
				U32& rWeightCount1=weightCount[mergeIndex1];
				const SpatialVector sum=rWeightCount0*outputBuffer[mergeIndex0]+
						rWeightCount1*outputBuffer[mergeIndex1];

				rWeightCount0+=rWeightCount1;
				rWeightCount1=0;
				outputBuffer[mergeIndex0]=sum/rWeightCount0;

#if FE_EXC_DEBUG
				feLog("merge %d into %d\n",mergeIndex1,mergeIndex0);
#endif

				masterIndex[mergeIndex1]=mergeIndex0;
				for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
				{
					if(masterIndex[groupIndex]==I32(mergeIndex1))
					{
#if FE_EXC_DEBUG
						feLog("remaster %d to %d\n",groupIndex,mergeIndex0);
#endif
						masterIndex[groupIndex]=mergeIndex0;
					}
				}
			}
		}
		for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			const I32 master=masterIndex[groupIndex];
			if(master>=0)
			{
				outputBuffer[groupIndex]=outputBuffer[master];
				resolved[groupIndex]=TRUE;
			}
		}
	}

	if(expand)
	{
		const Real fusion2=fusion*fusion;

		Array<U32> mergeList(groupCount);

		for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			if(resolved[groupIndex])
			{
				continue;
			}

			const SpatialVector point=outputBuffer[groupIndex];
			SpatialVector result=spInputPoint->spatialVector(groupIndex);
			U32 mergeCount=0;

			for(U32 groupIndex2=groupIndex+1;
					groupIndex2<groupCount;groupIndex2++)
			{
				if(resolved[groupIndex2])
				{
					continue;
				}

				const SpatialVector otherPoint=outputBuffer[groupIndex2];
				if(magnitudeSquared(otherPoint-point)<fusion2)
				{
					result+=spInputPoint->spatialVector(groupIndex2);
					mergeList[mergeCount++]=groupIndex2;
					resolved[groupIndex2]=TRUE;
				}
			}
			result*=1.0/Real(mergeCount+1);
			outputBuffer[groupIndex]=result;
			for(U32 mergeIndex=0;mergeIndex<mergeCount;mergeIndex++)
			{
				const U32 redundant=mergeList[mergeIndex];
				set(outputBuffer[redundant]);
			}
		}
	}

	if(writeSurface)
	{
		for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			spOutputPoint->set(groupIndex,outputBuffer[groupIndex]);
		}
	}

	if(!writeLines)
	{
		return;
	}

	SpatialVector* pLines=new SpatialVector[groupCount];
	I32* pLineMaster=new I32[groupCount];

	m_edgeMap.clear();
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const U32 subCount=spInputPrimitives->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}
		I32 pointIndexA=spInputPrimitives->integer(
				primitiveIndex,0);
		while(masterIndex[pointIndexA]>=0)
		{
			pointIndexA=masterIndex[pointIndexA];
		}

		U32 subIndex;
		for(subIndex=1;subIndex<=subCount;subIndex++)
		{
			I32 pointIndexB=spInputPrimitives->integer(
					primitiveIndex,subIndex%subCount);
			while(masterIndex[pointIndexB]>=0)
			{
				pointIndexB=masterIndex[pointIndexB];
			}

			if(pointIndexA!=pointIndexB)
			{
				const U64 edgeKey=(pointIndexA>pointIndexB)?
						(U64(pointIndexA)<<32)+pointIndexB:
						(U64(pointIndexB)<<32)+pointIndexA;

				Edge& rEdge=m_edgeMap[edgeKey];
				rEdge.m_faceCount++;
			}

			pointIndexA=pointIndexB;
		}
	}

	Array<I32> valence(groupCount,0);
	for(std::map<U64,Edge>::iterator it=m_edgeMap.begin();
			it!=m_edgeMap.end();it++)
	{
		if(interrupted())
		{
			break;
		}

		const U64 combo=it->first;
		const I32 pointIndexA=combo>>32;
		const I32 pointIndexB=combo&0xFFFFFFFF;

		valence[pointIndexA]++;
		valence[pointIndexB]++;
	}

	I32 currentIndex= -1;
	Real best2=0.0;
	for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
	{
		if(resolved[groupIndex])
		{
			continue;
		}
		if(valence[groupIndex]>1)
		{
			continue;
		}
		if(!startFromRoot)
		{
			currentIndex=groupIndex;
			break;
		}

		const SpatialVector point=outputBuffer[groupIndex];
		const Real distance2=magnitudeSquared(point-rootPoint);
		if(currentIndex<0 || best2>distance2)
		{
			currentIndex=groupIndex;
			best2=distance2;
		}
	}

	I32 boneCount=0;
	I32 skeletonCount=0;
	I32 outputPointCount=0;

	while(currentIndex<I32(groupCount))
	{
#if FE_EXC_DEBUG
		feLog("start %d\n",currentIndex);
#endif

		resolved[currentIndex]=true;

		pLines[0]=outputBuffer[currentIndex];
		pLineMaster[0]=currentIndex;
		U32 vertCount=1;

		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
				primitiveIndex++)
		{
			if(interrupted())
			{
				currentIndex=groupCount;
				break;
			}

			const U32 subCount=spInputPrimitives->subCount(primitiveIndex);
			U32 subMatch;
			for(subMatch=0;subMatch<subCount;subMatch++)
			{
				I32 vertIndex=spInputPrimitives->integer(
						primitiveIndex,subMatch);
				while(masterIndex[vertIndex]>=0)
				{
					vertIndex=masterIndex[vertIndex];
				}
				if(vertIndex==currentIndex)
				{
					break;
				}
			}
			if(subMatch==subCount)
			{
				continue;
			}
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				if(subIndex==subMatch)
				{
					continue;
				}

				U32 nextIndex=spInputPrimitives->integer(
						primitiveIndex,subIndex);
				while(masterIndex[nextIndex]>=0)
				{
					nextIndex=masterIndex[nextIndex];
				}

				if(resolved[nextIndex])
				{
					continue;
				}

#if FE_EXC_DEBUG
				feLog("  seg %d -> %d\n",currentIndex,nextIndex);
#endif

				//* add segment
				pLines[vertCount]=outputBuffer[nextIndex];
				pLineMaster[vertCount]=nextIndex;
				vertCount++;

				currentIndex=nextIndex;
				resolved[currentIndex]=true;

				//* restart from 0
				primitiveIndex=0;
				break;
			}
		}
		if(vertCount>1)
		{
			spDrawI->drawLines(pLines,NULL,vertCount,
					DrawI::e_strip,false,NULL);

			boneCount+=vertCount-1;
			skeletonCount++;

			String pointAttributes=catalog<String>("pointAttributes");
			String pointAttribute;
			while(!(pointAttribute=pointAttributes.parse("\""," ")).empty())
			{
				sp<SurfaceAccessorI> spInputAttr;
				if(!access(spInputAttr,spSurfaceInput,e_point,pointAttribute))
				{
					continue;
				}
				sp<SurfaceAccessorI> spOutputAttr;
				if(!access(spOutputAttr,spSurfaceOutput,e_point,pointAttribute))
				{
					continue;
				}

				const String attrType=spInputAttr->type();

				if(attrType=="string")
				{
					for(U32 vertIndex=0;vertIndex<vertCount;vertIndex++)
					{
						spOutputAttr->set(outputPointCount+vertIndex,
								spInputAttr->string(
								pLineMaster[vertIndex]));
					}
				}
				else if(attrType=="integer")
				{
					for(U32 vertIndex=0;vertIndex<vertCount;vertIndex++)
					{
						spOutputAttr->set(outputPointCount+vertIndex,
								spInputAttr->integer(
								pLineMaster[vertIndex]));
					}
				}
				else if(attrType=="real")
				{
					for(U32 vertIndex=0;vertIndex<vertCount;vertIndex++)
					{
						spOutputAttr->set(outputPointCount+vertIndex,
								spInputAttr->real(
								pLineMaster[vertIndex]));
					}
				}
				else if(attrType=="vector3")
				{
					for(U32 vertIndex=0;vertIndex<vertCount;vertIndex++)
					{
						spOutputAttr->set(outputPointCount+vertIndex,
								spInputAttr->spatialVector(
								pLineMaster[vertIndex]));
					}
				}
			}

			outputPointCount+=vertCount;
		}
		best2= -1.0;
		for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			if(resolved[groupIndex])
			{
				continue;
			}
			if(valence[groupIndex]>1)
			{
				continue;
			}
			if(!startFromRoot)
			{
				currentIndex=groupIndex;
				best2=0.0;
				break;
			}

			const SpatialVector point=outputBuffer[groupIndex];
			const Real distance2=magnitudeSquared(point-rootPoint);
			if(best2<0.0 || best2>distance2)
			{
				currentIndex=groupIndex;
				best2=distance2;
			}
		}
		if(best2<0.0)
		{
			break;
		}
	}

	delete[] pLineMaster;
	delete[] pLines;

	rSummary.sPrintf("%s%d bones",group.empty()? "": (group+" ").c_str(),
			boneCount);

	if(skeletonCount!=1)
	{
		String text;
		text.sPrintf(" in %d sections",skeletonCount);

		rSummary+=text;
	}
}
