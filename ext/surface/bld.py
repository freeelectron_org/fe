import sys
forge = sys.modules["forge"]

def prerequisites():
    return [    "datatool",
                "geometry",
                "thread" ]

def setup(module):
    srcList = [ "FlatTree",
                "MapTree",
                "QuadTree",
                "SpatialTreeBase",
                "SurfaceAccessibleBase",
                "SurfaceAccessibleCatalog",
                "SurfaceAccessibleJoint",
                "SurfaceAccessibleRecord",
                "SurfaceAccessibleObj",
                "SurfaceAccessibleXyz",
                "SurfaceAccessorBase",
                "SurfaceAccessorCached",
                "SurfaceAccessorCatalog",
                "SurfaceAccessorRecord",
                "SurfaceBase",
                "SurfaceMRP",
                "SurfaceCurves",
                "SurfaceCurvesAccessible",
                "SurfaceCylinder",
                "SurfaceDisk",
                "SurfaceOBJ",
                "SurfacePlane",
                "SurfaceSearchable",
                "SurfaceSphere",
                "SurfaceSTL",
                "SurfaceTriangles",
                "SurfaceTrianglesAccessible",
                "surface.pmh",
                "surfaceDL" ]

    dll = module.DLL( "fexSurfaceDL", srcList )

    deplibs = forge.corelibs + [
                "fexSignalLib",
                "fexGeometryDLLib",
                "fexDataToolDLLib",
                "fexThreadDLLib"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "fexDrawDLLib" ]

    forge.deps( ["fexSurfaceDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexSurfaceDL",                     None,   None) ]

    module.Module('test')
