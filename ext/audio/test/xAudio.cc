/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "audio/audio.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	if(argc<2)
	{
		feLog("%s: No lib specified\nTry %s <DL>\n",argv[0],argv[0]);
		return 1;
	}

	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage(argv[1]);
		UNIT_TEST(successful(result));

		{
			sp<ListenerI> spListenerI(spRegistry->create("ListenerI"));
			sp<VoiceI> spVoiceI(spRegistry->create("VoiceI"));
			sp<VoiceI> spVoiceI2(spRegistry->create("VoiceI"));
			if(!spListenerI.isValid() ||
					!spVoiceI.isValid() || !spVoiceI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			sp<AudioI> spAudioI(spRegistry->create("AudioI"));
			if(!spAudioI.isValid())
			{
				feX(argv[0], "couldn't create spAudioI");
			}

			String wavPath=
				spMaster->catalog()->catalog<String>("path:media")+"/wav";

			String path=wavPath+"/pop.wav";
			result=spAudioI->load("pop",path);
			UNIT_TEST(successful(result));

			path=wavPath+"/door_creak1.wav";
			result=spAudioI->load("door",path);
			UNIT_TEST(successful(result));

			spAudioI=NULL;

			spVoiceI->queue("pop");
			spVoiceI->setVolume(0.1f);
			spVoiceI->loop(TRUE);
			spVoiceI->play();

			spVoiceI2->queue("door");
			spVoiceI2->setVolume(1.0f);
			spVoiceI2->loop(TRUE);
			spVoiceI2->play();

			for(U32 m=0;m<100;m++)
			{
				F32 angle=fe::pi*m/50.0f;
				SpatialVector location(sinf(angle),0.0f,cosf(angle));
				feLog("%d %s\n",m,print(location).c_str());
				spVoiceI->setLocation(location);
				milliSleep(10);
			}

			spVoiceI->stop();
			spVoiceI2->stop();
			feLog("done\n");
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
