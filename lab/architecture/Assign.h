/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_Assign_h__
#define __architecture_Assign_h__

#include <datatool/datatool.h>
namespace fe
{
namespace ext
{

/**	Assign the value of one attribute to another attribute

	@copydoc Assign_info
	*/
class Assign :
	public Initialize<Assign>,
	public Config,
	virtual public AssignI
{
	public:
				Assign(void);
virtual			~Assign(void);
		void	initialize(void);

				// AS AssignI
virtual	void	set(const String &a_lhs, const String &a_rhs);

				// AS HandlerI
virtual	void	handle(Record& r_sig);


	private:
		class Assignment
		{
			public:
				VoidAccessor	m_lhs;
				VoidAccessor	m_rhs;
		};
		std::list<Assignment>	m_assignments;


};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_Assign_h__ */
