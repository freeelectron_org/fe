Using the NetHost {#nethost}
==================

Accessing the NetHost {#nethost_access}
======================

fe::ext::NetHost is handled as a singleton.
First access will create the single instance.
Further access will return the same instance.

Releasing a reference to the NetHost will not destroy it,
unless that was the last reference.
If a new instance is created once again,
the previous NetHost contents will not be restored.

To access the NetHost, potentially creating it,
use its static `create()` call and place the result in a smart pointer.

```cpp
sp<NetHost> spNetHost(NetHost::create());
```

Setting up Autoload {#nethost_autoload}
===================

Often, the first thing to do with the NetHost is to set up
module auto-loading.
If you later try to create a component of a type for which there
is not currently a factory,
the autoload subsystem will scan a manifest for a module that
can provide a compatible implementation and then load it automatically.

```cpp
sp<Master> spMaster=spNetHost->master();
sp<Registry> spRegistry=spMaster->registry();

Result result=spRegistry->manage("feAutoLoadDL");
```

The fe::Master is [almost always] also a singleton that holds a lot
a general data for the process.
The fe::Registry manages loadable modules and creates components.

Next Step {#nethost_host_next}
=========

A common use of the NetHost to to create spaces,
each in the form of an fe::StateCatalog.

```cpp
sp<StateCatalog> spStateCatalog=spNetHost->accessSpace("world");
```

For more, see @ref nethost_state_catalog.
