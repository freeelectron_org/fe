/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <katana/katana.pmh>

#include "platform/dlCore.cc"

#include "FnAttribute/client/FnAttribute.cpp"
#include "FnAttribute/client/FnAttributeUtils.cpp"
#include "FnAttribute/client/FnDataBuilder.cpp"

#include "FnGeolib/op/FnGeolibOp.cpp"
#include "FnGeolib/op/FnGeolibSetupInterface.cpp"
#include "FnGeolib/op/FnGeolibCookInterface.cpp"
#include "FnGeolib/op/FnGeolibCookInterfaceUtils.cpp"
#include "FnGeolib/util/Path.cpp"
#include "FnGeolib/util/PowerNap.cpp"

#include "FnGeolibServices/client/FnGeolibCookInterfaceUtilsService.cpp"

#include "FnPluginManager/client/FnPluginManager.cpp"

#include "FnPluginSystem/FnPlugin.cpp"

#include "pystring/pystring.cpp"

using namespace fe;
using namespace fe::ext;

DEFINE_GEOLIBOP_PLUGIN(KatanaGeo)

void registerPlugins()
{
	feLog("Free Electron for Katana\n");

	//* NOTE same class reuses same structure, but registering does a deep copy
	//* the name string may need to be persistent (probably pointer copy)
	REGISTER_PLUGIN(KatanaGeo,"FurOp",0,1);
	REGISTER_PLUGIN(KatanaGeo,"ImportOp",0,1);
	REGISTER_PLUGIN(KatanaGeo,"TreeOp",0,1);
}

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	feLog("katanaDL ListDependencies()\n");

	list.append(new fe::String("fexTerminalDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	feLog("katanaDL CreateLibrary()\n");

	Library *pLibrary = Memory::instantiate<Library>();
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	feLog("katanaDL InitializeLibrary()\n");
}

}
