/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

#define	FE_NOTE_DRAW "draw"
#define	FE_NOTE_ADDED_VIEWPORT "added_viewport"
#define	FE_NOTE_REMOVED_VIEWPORT "removed_viewport"

#include "moa/moa.h"
#include "viewer/viewer.h"
#include "Serializable.h"

#include <thread>
#if FE_CPLUSPLUS >= 201703L
    #include <shared_mutex> // C++17
    #include <mutex>
#endif

#include "AsViewport.h"

#include "ThreadedViewerI.h"
#include "ViewerSystem.h"

#include "systems/OriginViewerSystem.h"

