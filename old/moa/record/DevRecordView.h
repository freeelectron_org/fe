/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Development version of a Record View.
 *
 * Not finalized, work in proggress.
 */
class DevRecordView :
	public AccessorSet
	// virtual public RecordFactoryI
{
public:
	DevRecordView() {};

	/**	Create a Record using all the attributes.
	 *
	 * The Record is automatically bound.
	 */
	Record createRecord()
	{
		if(!m_hpScope.isValid())
		{
			feX("RecordView::createRecord",
					"\"%s\" has invalid scope\n",
					name().c_str());
		}

		Record newrecord = m_hpScope->createRecord(getLayout());
		if(!layout.isValid())
		{
			feX("RecordView::createRecord",
					"\"%s\" failed to create layout\n",
					name().c_str());
		}
		if(!newrecord.isValid())
		{
			feX("RecordView::createRecord",
					"\"%s\" failed to create record\n",
					name().c_str());
		}

		record = newrecord;

		bind(record);
		// initializeRecord();
		return newrecord;
	};

	WeakRecord getRecord() const
	{
		return record;
	};

	/**
	 * Access a Layout of all the attributes.
	 */
	sp<Layout> getLayout()
	{
		if(!m_hpScope.isValid())
		{
			feLog("RecordView::layout \"%s\" invalid scope\n",
					name().c_str());
			return sp<Layout>(NULL);
		}

		if(!layout.isValid())
		{
			layout = createLayout();
		}

		return layout;
	};

protected:

private:
	DevRecordView(const DevRecordView& rRecordView): AccessorSet() {};

	sp<Layout> createLayout()
	{
		if(!m_hpScope.isValid())
		{
			feX("AccessorArray::createLayout",
					"\"%s\" has invalid scope\n",
					name().c_str());
		}

		sp<Layout> spLayout = m_hpScope->declare(name());
		if(!spLayout.isValid())
		{
			feX("AccessorArray::createLayout",
					"\"%s\" failed to create layout\n",
					name().c_str());
		}

		populate(spLayout);
		return spLayout;
	}

	hp<Layout> layout;
	WeakRecord record;
};

inline BWORD operator==(const DevRecordView& rRecordView1,
		const DevRecordView& rRecordView2)
{
	return rRecordView1.getRecord()==rRecordView2.getRecord();
}

inline BWORD operator!=(const DevRecordView& rRecordView1,
		const DevRecordView& rRecordView2)
{
	return rRecordView1.getRecord()!=rRecordView2.getRecord();
}

} /* namespace ext */
} /* namespace fe */
