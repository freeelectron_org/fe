/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Provot_h__
#define __element_Provot_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Apply fixed rod lengths

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Provot: virtual public HandlerI
{
	public:
				Provot(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Arena					m_arenaRV;
		Particle				m_particleRV;
		RecordArrayView<Rod>	m_rodRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Provot_h__ */
