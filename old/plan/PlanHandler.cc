/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "plan/PlanHandler.h"

namespace fe
{

PlanHandler::PlanHandler(void)
{
}

PlanHandler::~PlanHandler(void)
{
}

void PlanHandler::initialize(void)
{
	dispatch<sp<Layout> >("method");
	dispatch<sp<Component> >("method");
}

void PlanHandler::handle(Record &r_sig)
{
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		sp<Scope> spScope = spRA->layout()->scope();
		m_asPlan.bind(spScope);
		sp<RecordGroup> spRemoval(new RecordGroup());

		if(m_asPlan.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_object = spRA->getRecord(i);

				spRemoval->clear();
				sp<RecordGroup> rg_plan = m_asPlan.group(r_object);
				if(!rg_plan.isValid())
				{
					continue;
				}
				for(RecordGroup::iterator i_plan = rg_plan->begin();
					i_plan != rg_plan->end(); i_plan++)
				{
					sp<RecordArray> ra_plan(*i_plan);
					t_map::iterator i_method = m_map.find(ra_plan->layout());
					if(i_method != m_map.end())
					{
						for(int i_op = 0; i_op < ra_plan->length(); i_op++)
						{
							Record r_op = ra_plan->getRecord(i_op);
							if(i_method->second->method(
								r_object,
								r_op,
								rg_plan))
							{
								spRemoval->add(r_op);
							}
						}
					}
				}

				for(RecordGroup::iterator i_rm = spRemoval->begin();
					i_rm != spRemoval->end(); i_rm++)
				{
					sp<RecordArray> ra_rm(*i_rm);
					for(int i_rm = 0; i_rm < ra_rm->length(); i_rm++)
					{
						Record r_rm = ra_rm->getRecord(i_rm);
						rg_plan->remove(r_rm);
					}
				}

			}
		}
	}
}

bool PlanHandler::call(const String &a_name, std::vector<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("method", "[layout] [method]"))
	{
		sp<Layout> spLayout = a_argv[0].cast<sp<Layout> >();
		sp<MethodI> spMethod(a_argv[1].cast<sp<Component> >());
		if(spMethod.isValid())
		{
			method(spLayout, spMethod);
		}
	}

	return true;
}

void PlanHandler::method(sp<Layout> spLayout, sp<MethodI> spMethod)
{
	m_map[spLayout] = spMethod;
}

} /* namespace */

