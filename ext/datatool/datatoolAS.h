/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_datatoolAS_h__
#define __datatool_datatoolAS_h__

namespace fe
{
namespace ext
{

/// possible attributes of a top level signal
class AsSignal
	: public AccessorSet, public Initialize<AsSignal>
{
	public:
		void initialize(void)
		{
			add(winData,		FE_USE("win:data"));
			//add(appData,		FE_USE("app:data"));
			//add(simData,		FE_USE("sim:data"));
			add(event,			FE_USE("evt:record"));
		}
		/// window data record
		Accessor<Record>		winData;
		/// application data record
		//Accessor<Record>		appData;
		/// simulation data record
		//Accessor<Record>		simData;
		/// event record
		Accessor<Record>		event;
};

/// window data
class AsWindata
	: public AccessorSet, public Initialize<AsWindata>
{
	public:
		void initialize(void)
		{
			add(windowI,	FE_USE("win:component"));
			add(drawI,		FE_USE("win:draw"));
			add(group,		FE_USE("win:group"));
		}
		/// reference to window component
		Accessor<sp<Component> >	windowI;
		/// window DrawI component
		Accessor<sp<Component> >	drawI;
		/// groups for records associated with window
		Accessor<sp<RecordGroup> >	group;
};

/// Group Operator
class AsOperator
	: public AccessorSet, public Initialize<AsOperator>
{
	public:
		void initialize(void)
		{
			add(input,				FE_USE("op:input"));
			add(output,				FE_USE("op:output"));
		}
		/// input group
		Accessor<sp<RecordGroup> >	input;
		/// output group
		Accessor<sp<RecordGroup> >	output;
};

/// Time-based Operator
class AsTemporal
	: public AccessorSet, public Initialize<AsTemporal>
{
	public:
		void initialize(void)
		{
			add(time,				FE_USE("sim:time"));
			add(timestep,			FE_USE("sim:timestep"));
		}
		/// input group
		Accessor<Real>	time;
		/// output group
		Accessor<Real>	timestep;
};

/// Signal to Initialize
class AsInit
	: public AccessorSet, public Initialize<AsInit>
{
	public:
		void initialize(void)
		{
			add(is,				FE_USE("app:init"));
		}
		/// 
		Accessor<void>	is;
};

/// Pair of records
class AsPair
	: public AccessorSet, public Initialize<AsPair>
{
	public:
		AsPair(void){}
		void initialize(void)
		{
			add(left,		FE_USE("pair:left"));
			add(right,		FE_USE("pair:right"));
		}
		///	One record in a pair of records
		Accessor<WeakRecord>	left;
		///	One record in a pair of records
		Accessor<WeakRecord>	right;
};

class AsProximity
	: public AccessorSet, public Initialize<AsProximity>
{
	public:
		AsProximity(void){}
		void initialize(void)
		{
			// these are different than for pair since they are not weak
			add(left,		FE_USE("prox:left"));
			add(right,		FE_USE("prox:right"));
		}
		Accessor<WeakRecord>	left;
		Accessor<WeakRecord>	right;
};

/// Named Records
class AsNamed
	: public AccessorSet, public Initialize<AsNamed>
{
	public:
		AsNamed(void){}
		void initialize(void)
		{
			add(name,		FE_USE("name"));
		}
		///	string name
		Accessor<String>	name;

		void filterMap(std::map<String, Record> &a_records, sp<RecordGroup> rg_input)
		{
			for (RecordGroup::iterator i_in = rg_input->begin();
				 i_in != rg_input->end(); i_in++)
			{
				bind((*i_in)->layout()->scope());
				if (check(*i_in))
				{
					for (int i = (*i_in)->length() - 1; i >= 0; i--)
					{
						Record r = (*i_in)->getRecord(i);
						a_records[name(r)] = r;
					}
				}
			}
		}
};

class AsGroup
	: public AccessorSet, public Initialize<AsGroup>
{
	public:
		AsGroup(void){}
		void initialize(void)
		{
			add(group,		FE_USE("group"));
		}
		///	record group
		Accessor< sp<RecordGroup> >	group;
};



} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_datatoolAS_h__ */

