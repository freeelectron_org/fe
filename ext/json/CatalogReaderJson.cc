/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#define FE_CRJ_DEBUG	FALSE

namespace fe
{
namespace ext
{

//* TODO Json::Reader is deprecated; try Json::CharReaderBuilder

BWORD CatalogReaderJson::load(String a_filename, sp<Catalog> a_spCatalog)
{
#if FE_CRJ_DEBUG
	feLog("CatalogReaderJson::load \"%s\"\n",a_filename.c_str());
#endif

	std::ifstream istrm(a_filename.c_str());
	if(!istrm.is_open())
	{
		feLog("CatalogReaderJson::load failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	m_spJsonRoot=sp<JsonValue>(new JsonValue());

	Json::Reader reader;
	reader.parse(istrm,m_spJsonRoot->value());

	for(Json::Value::iterator it=m_spJsonRoot->value().begin();
			it!=m_spJsonRoot->value().end();it++)
	{
		const String keyName=it.name().c_str();

		const Json::Value jsonEntry= *it;
		const String keyType=jsonEntry["type"].asCString();
		const String keyValue=jsonEntry["value"].asCString();

#if FE_CRJ_DEBUG
		feLog("CatalogReaderYaml::load "
				" key \"%s\" type \"%s\" value \"%s\"\n",
				keyName.c_str(),keyType.c_str(),keyValue.c_str());
#endif
		addInstance(a_spCatalog,keyName,"value",keyType,keyValue);

		for(Json::Value::const_iterator it2=jsonEntry.begin();
				it2!=jsonEntry.end();it2++)
		{
			const String propertyName=it2.name().c_str();
			if(propertyName=="type" || propertyName=="value")
			{
				continue;
			}

			const Json::Value jsonEntry2= *it2;
			const String propertyType=jsonEntry2["type"].asCString();
			const String propertyValue=jsonEntry2["value"].asCString();

#if FE_CRJ_DEBUG
			feLog("CatalogReaderYaml::load "
					"   property \"%s\" type \"%s\" value \"%s\"\n",
					propertyName.c_str(),propertyType.c_str(),
					propertyValue.c_str());
#endif
			addInstance(a_spCatalog,keyName,propertyName,
					propertyType,propertyValue);
		}
	}

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
