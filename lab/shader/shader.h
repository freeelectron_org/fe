/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shader_h__
#define __shader_h__

#include "shade/shade.h"

#endif /* __shader_h__ */
