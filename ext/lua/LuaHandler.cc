/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

namespace fe
{
namespace ext
{

LuaHandler::LuaHandler(void)
{
	m_pLuaState = NULL;
	m_name = "unnamed";
}

LuaHandler::~LuaHandler(void)
{
	flush();
#if FE_COUNTED_TRACK
	U32 size=m_trackArray.size();
	for(U32 m=0;m<size;m++)
	{
		Counted::deregisterRegion(m_trackArray[m]);
	}
#endif
	if(m_pLuaState) { lua_close(m_pLuaState); }
}

void LuaHandler::initialize(void)
{
	m_pLuaState = lua_newstate(lua_alloc, NULL);

#if FALSE
	luaL_Reg lualibs[] =
	{
		{ "base",       luaopen_base },
		{ "table",      luaopen_table },
		{ "io",         luaopen_io },
		{ "string",     luaopen_string },
		{ "math",       luaopen_math },
		{ "debug",      luaopen_debug },
		{ "loadlib",    luaopen_package },
		{ NULL,         NULL }
	};

	luaL_Reg *lib;

	for(lib = lualibs; lib->func != NULL; lib++)
	{
		lua_pushcfunction(m_pLuaState, lib->func);
		lua_pushstring(m_pLuaState, lib->name);
		lua_call(m_pLuaState, 1, 0);
		lua_settop(m_pLuaState, 0);
	}
#else
	luaL_openlibs(m_pLuaState);
#endif

	m_spContext = new LuaContext(registry()->master(), m_pLuaState);

	sp<TypeMaster> spTM=registry()->master()->typeMaster();

	dispatch<String>("loadString");

	dispatch<String>("setRecord");
	dispatch<Record>("setRecord");
}

void LuaHandler::alias(String a_aliasName,String a_trueName)
{
	if(m_spContext.isValid())
	{
		m_spContext->alias(a_aliasName,a_trueName);
	}
}

bool LuaHandler::preloadString(const String &text)
{
	m_preload=text;
	return true;
}

Result LuaHandler::loadFile(const String &filename)
{
	m_name = filename;
	std::ifstream file;
	file.open(filename.c_str(),std::ifstream::in|std::ifstream::binary);
	if(!file)
	{
		feLog("LuaHandler::loadFile open failed on \"%s\"\n",
				filename.c_str());
		return e_invalidFile;
	}

	file.seekg(0, std::ios::end);
	int length = file.tellg();
	file.seekg(0, std::ios::beg);

	char *buffer = new char [length+1];
	memset((void *)buffer, 0, length+1); // make valgrind happy

	file.read(buffer,length);
	if(!file)
	{
		feLog("LuaHandler::loadFile read %d/%d bytes, now at byte %d\n",
				length,file.gcount(),I32(file.tellg()));

		if(file.eof())
		{
			feLog("LuaHandler::loadFile read caused eofbit\n");
		}
		else if(file.fail())
		{
			feLog("LuaHandler::loadFile read caused failbit\n");
		}
		else if(file.bad())
		{
			feLog("LuaHandler::loadFile read caused badbit\n");
		}
		else if(file.good())
		{
			feLog("LuaHandler::loadFile read failed, but reported goodbit\n");
		}

		delete[] buffer;
		file.close();

		return e_readFailed;
	}

	m_chunk = buffer;

	delete[] buffer;
	file.close();

	return compile();
}

Result LuaHandler::loadString(const String &text)
{
	m_name = "loaded string";
	m_chunk = text;
	return compile();
}

Result LuaHandler::compile(void)
{
	m_source=m_preload+m_chunk;

	int result;
	result = luaL_loadbuffer(m_pLuaState, m_source.c_str(),
			m_source.length(), m_source.c_str());
	if(result != 0)
	{
		String error=lua_tostring(m_pLuaState, -1);
		feLog("LUA syntax error: %s \n", error.c_str());

// [string "function createPoint()..."]:68: syntax error near 'error'

		//* [string "function createPoint()..."]
		error.parse("",":");

		const String lineText=error.parse("",":");
		const String message=error.parse("",":").prechop(" ");

		const I32 lineProgram=atoi(lineText.c_str());

		const I32 linesPreload=m_preload.lines();
		const I32 linesChunk=m_chunk.lines();

		const I32 lineChunk=lineProgram-linesPreload;

		const String red("[31m");
		const String yellow("[33m");
		const String cyan("[36m");
		const String plain("[0m");

		feLog("%sLuaHandler::compile error on line %d%s\n",
				red.c_str(),lineChunk,plain.c_str());
		feLog("%s  \"%s\"%s\n",
				red.c_str(),message.c_str(),plain.c_str());

		const I32 linesAside(3);
		for(I32 offset=-linesAside;offset<=linesAside;offset++)
		{
			const I32 lineOffset=lineChunk+offset;
			if(lineOffset>=1 && lineOffset<=linesChunk)
			{
				feLog("%s%-8d%s%s\n",(offset? cyan: yellow).c_str(),
						lineOffset,m_chunk.line(lineOffset-1).c_str(),
						plain.c_str());
			}
		}

		return e_unsolvable;
	}

	lua_setglobal(m_pLuaState, m_name.c_str());

	return e_ok;
}

bool LuaHandler::execute(const String &a_fnName)
{
	m_spContext->setPreamble(m_preload.lines());

	lua_pushcfunction(m_pLuaState, LuaContext::debug);

	lua_getglobal(m_pLuaState, a_fnName.c_str());

	if(lua_isnil(m_pLuaState, -1))
	{
		lua_pop(m_pLuaState, 1);
		lua_pop(m_pLuaState, 1);
		return false;
	}
	int result = lua_pcall(m_pLuaState, 0, 0, -2);
	if(result)
	{
		String err = lua_tostring(m_pLuaState, -1);
		lua_pop(m_pLuaState, 1);

		//* NOTE avoid % formatting
		feLog("%s",err.c_str());

		return false;
	}
	lua_pop(m_pLuaState, 1);
	return true;
}

Record LuaHandler::getRecord(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaRecordObject *pObj = (LuaRecordObject *)lua_touserdata(m_pLuaState, -1);
	lua_pop(m_pLuaState, 1);

	Record record=pObj->data();
	return record;
}

String LuaHandler::getString(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	const String value=lua_tostring(m_pLuaState, -1);
	lua_pop(m_pLuaState, 1);

	return value;
}

Real LuaHandler::getReal(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	const Real value=(Real)lua_tonumber(m_pLuaState, -1);
	lua_pop(m_pLuaState, 1);

	return value;
}

Vector2f LuaHandler::getVector2f(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaVector2fObject *pObj =
			(LuaVector2fObject *)lua_touserdata(m_pLuaState, 1);
	if(!pObj)
	{
		feLog("LuaHandler::getVector2f no key \"%s\"\n",key.c_str());
		return Vector2f(0,0);
	}
	const Vector2f vector2f(*pObj->data());
	lua_pop(m_pLuaState, 1);

	return vector2f;
}

Vector2d LuaHandler::getVector2d(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaVector2dObject *pObj =
			(LuaVector2dObject *)lua_touserdata(m_pLuaState, 1);
	if(!pObj)
	{
		feLog("LuaHandler::getVector2d no key \"%s\"\n",key.c_str());
		return Vector2d(0,0);
	}
	const Vector2d vector2d(*pObj->data());
	lua_pop(m_pLuaState, 1);

	return vector2d;
}

Vector3f LuaHandler::getVector3f(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaVector3fObject *pObj =
			(LuaVector3fObject *)lua_touserdata(m_pLuaState, 1);
	if(!pObj)
	{
		feLog("LuaHandler::getVector3f no key \"%s\"\n",key.c_str());
		return Vector3f(0,0,0);
	}
	const Vector3f vector3f(*pObj->data());
	lua_pop(m_pLuaState, 1);

	return vector3f;
}

Vector3d LuaHandler::getVector3d(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaVector3dObject *pObj =
			(LuaVector3dObject *)lua_touserdata(m_pLuaState, 1);
	if(!pObj)
	{
		feLog("LuaHandler::getVector3d no key \"%s\"\n",key.c_str());
		return Vector3d(0,0,0);
	}
	const Vector3d vector3d(*pObj->data());
	lua_pop(m_pLuaState, 1);

	return vector3d;
}

Vector4f LuaHandler::getVector4f(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaVector4fObject *pObj =
			(LuaVector4fObject *)lua_touserdata(m_pLuaState, 1);
	if(!pObj)
	{
		feLog("LuaHandler::getVector4f no key \"%s\"\n",key.c_str());
		return Vector4f(0,0,0,0);
	}
	const Vector4f vector4f(*pObj->data());
	lua_pop(m_pLuaState, 1);

	return vector4f;
}

Vector4d LuaHandler::getVector4d(const String &key)
{
	lua_getglobal(m_pLuaState, key.c_str());
	LuaVector4dObject *pObj =
			(LuaVector4dObject *)lua_touserdata(m_pLuaState, 1);
	if(!pObj)
	{
		feLog("LuaHandler::getVector4d no key \"%s\"\n",key.c_str());
		return Vector4(0,0,0,0);
	}
	const Vector4d vector4d(*pObj->data());
	lua_pop(m_pLuaState, 1);

	return vector4d;
}

void LuaHandler::set(const String &key, Record r_value)
{
	t_lua_record::push(m_spContext.raw(), r_value);
	lua_setglobal(m_pLuaState, key.c_str());

// TODO restore for RecordAV
#if FALSE //FE_COUNTED_TRACK
	Record& record=r_value;
	if(record.isValid())
	{
		void* data=record.data();
		U32 size=record.layout()->size();

		Counted::registerRegion(data,size,
				"LuaHandler:Record "+record.layout()->name());
		Counted::trackReference(data,fe_cast<Counted>(this),
				"LuaHandler");

		m_trackArray.push_back(data);
	}
#endif
}

void LuaHandler::set(const String &key, sp<RecordArray> ra_value)
{
	t_lua_array::push(m_spContext.raw(), ra_value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, const String &value)
{
	lua_pushstring(m_pLuaState, value.c_str());
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, const Real &value)
{
	lua_pushnumber(m_pLuaState, value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, Vector2f &value)
{
	t_lua_vector2f::push(m_spContext.raw(), &value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, Vector2d &value)
{
	t_lua_vector2d::push(m_spContext.raw(), &value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, Vector3f &value)
{
	t_lua_vector3f::push(m_spContext.raw(), &value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, Vector3d &value)
{
	t_lua_vector3d::push(m_spContext.raw(), &value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, Vector4f &value)
{
	t_lua_vector4f::push(m_spContext.raw(), &value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, Vector4d &value)
{
	t_lua_vector4d::push(m_spContext.raw(), &value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, sp<Layout> l_value)
{
	t_lua_layout::push(m_spContext.raw(), l_value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, sp<Component> spValue)
{
	lua_rawgeti(m_pLuaState, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
	lua_pushstring(m_pLuaState, key.c_str());
	LuaComponentObject::pushComponent(m_pLuaState, spValue);
	lua_settable(m_pLuaState, -3);
	lua_pop(m_pLuaState, 1);

	//lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, sp<Scope> spValue)
{
	t_lua_scope::push(m_spContext.raw(), spValue);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaHandler::set(const String &key, sp<StateCatalog> spValue)
{
	t_lua_statecatalog::push(m_spContext.raw(), spValue);
	lua_setglobal(m_pLuaState, key.c_str());
}

bool LuaHandler::execute(void)
{
	m_spContext->setPreamble(m_preload.lines());

	lua_pushcfunction(m_pLuaState, LuaContext::debug);

	lua_getglobal(m_pLuaState, m_name.c_str());

	int result = lua_pcall(m_pLuaState, 0, 0, -2);
	if(result)
	{
		String err = lua_tostring(m_pLuaState, -1);
		lua_pop(m_pLuaState, 1);
		feLog(err.c_str());
		return false;
	}
	lua_pop(m_pLuaState, 1);

#if 0
	// force garbage collection
	lua_setgcthreshold(m_pLuaState,0);
#endif

	return true;
}

void LuaHandler::flush(void)
{
	lua_gc(m_pLuaState,LUA_GCCOLLECT,0);
}

void LuaHandler::handle(Record& r_sig)
{
	set("r_sig", r_sig);
	execute();
}

bool LuaHandler::call(const String &a_name, Array<Instance>& a_argv)
{
	if(a_name == FE_DISPATCH("loadString", "[string]"))
	{
		String text = a_argv[0].cast<String>();
		if(!loadString(text))
		{
			feX("LuaHandler::call",
				"lua script error");
		}
	}

	if(a_name == FE_DISPATCH("setRecord", "[name] [record]"))
	{
		String name = a_argv[0].cast<String>();
		Record record = a_argv[1].cast<Record>();
		set(name, record);
	}

	return true;
}

} /* namespace ext */
} /* namespace fe */
