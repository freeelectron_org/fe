/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

/*** TODO
	lock group selection edges
	smooth custom attributes
*/

void SmoothOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("primitiveGroup");
	catalog<String>("primitiveGroup","label")="Primitive Group";
	catalog<String>("primitiveGroup","suggest")="primitiveGroups";
	catalog<String>("primitiveGroup","hint")=
			"Group in Input Surface that limits which"
			" primitives are affected.";

	catalog<String>("lockPointGroup");
	catalog<String>("lockPointGroup","label")="Lock Point Group";
	catalog<String>("lockPointGroup","suggest")="pointGroups";
	catalog<String>("lockPointGroup","hint")=
			"Group in Input Surface for which"
			" points are not affected.";

	catalog<Real>("smoothing")=1.0;
	catalog<Real>("smoothing","high")=1.0;
	catalog<Real>("smoothing","max")=10.0;
	catalog<String>("smoothing","label")="Smoothing";
	catalog<String>("smoothing","hint")=
			"Amount of smoothing to apply per iteration.";

	catalog<I32>("iterations")=1;
	catalog<I32>("iterations","high")=10;
	catalog<I32>("iterations","max")=1e3;
	catalog<String>("iterations","label")="Iterations";
	catalog<String>("iterations","hint")=
			"Number of times to apply smoothing.";

	catalog<Real>("resistSliding")=0.0;
	catalog<Real>("resistSliding","max")=1.0;
	catalog<String>("resistSliding","label")="Resist Sliding";
	catalog<String>("resistSliding","hint")=
			"Try not to move points tangent to their normals.";

	catalog<Real>("resistShrinkage")=0.0;
	catalog<Real>("resistShrinkage","max")=1.0;
	catalog<String>("resistShrinkage","label")="Resist Shrinkage";
	catalog<String>("resistShrinkage","hint")=
			"Try not to move points along their normals.";

	catalog<Real>("conservation")=0.0;
	catalog<Real>("conservation","max")=1.0;
	catalog<String>("conservation","label")="Conservation";
	catalog<String>("conservation","hint")=
			"Also apply reverse change, divided among neighbors.";

	catalog<bool>("lockBoundaries")=false;
	catalog<String>("lockBoundaries","label")="Lock Boundaries";
	catalog<String>("lockBoundaries","hint")=
			"Do not affect points on an outside edge.";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<String>("Output Surface","iterate")="Output Surfaces";

	m_spDots=new DrawMode();
	m_spDots->setDrawStyle(DrawMode::e_solid);
	m_spDots->setPointSize(8.0);
}

void SmoothOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		const I32 dotCount=m_locationArray.size();

		spDrawBrush->pushDrawMode(m_spDots);

		//* TODO pull towards camera a little, out of surface
		spDrawBrush->drawPoints(m_locationArray.data(),NULL,
				dotCount,TRUE,m_colorArray.data());

		spDrawBrush->popDrawMode();

		return;
	}

	const String primitiveGroup=catalog<String>("primitiveGroup");
	const BWORD primitiveGrouped=(!primitiveGroup.empty());
	const String lockPointGroup=catalog<String>("lockPointGroup");
	const BWORD lockPointGrouped=(!lockPointGroup.empty());
	const Real smoothing=catalog<Real>("smoothing");
	const I32 iterations=catalog<I32>("iterations");
	const Real resistSliding=catalog<Real>("resistSliding");
	const Real resistShrinkage=catalog<Real>("resistShrinkage");
	const Real conservation=catalog<Real>("conservation");
	const BWORD lockBoundaries=catalog<bool>("lockBoundaries");

	String& rSummary=catalog<String>("summary");
	rSummary="";

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputNormal;
	if(resistSliding>0.0 || resistShrinkage>0.0)
	{
		if(!access(spInputNormal,spInputAccessible,
				e_point,e_normal,e_warning))
		{
			catalog<String>("warning")="missing point normals;";
		}
	}

	sp<SurfaceAccessorI> spInputPrimGroup;
	if(primitiveGrouped)
	{
		if(!access(spInputPrimGroup,spInputAccessible,
				e_primitiveGroup,primitiveGroup)) return;
	}

	sp<SurfaceAccessorI> spLockPointGroup;
	if(lockPointGrouped)
	{
		if(!access(spLockPointGroup,spInputAccessible,
				e_pointGroup,lockPointGroup)) return;
	}

	const U32 pointCount=spOutputPoint->count();
	const U32 primitiveCount=spInputVertices->count();
	const U32 memberCount=primitiveGrouped?
			(spInputPrimGroup.isValid()? spInputPrimGroup->count(): 0):
			primitiveCount;

	if(!memberCount)
	{
		feLog("SmoothOp::handle primitive count is zero\n");

		if(primitiveGrouped)
		{
			catalog<String>("warning")="no primitives in input group;";

			rSummary="EMPTY group "+primitiveGroup;
		}
		else
		{
			catalog<String>("warning")="no primitives in input surface;";

			rSummary="NO PRIMITIVES";
		}
		return;
	}

	//* NOTE polygon surfaces only
	//* TODO curves and meshes

	Array< std::set<I32> > pointFaces;
	Array< std::set<I32> > pointNeighbors;
	determineNeighborhood(spInputAccessible,pointFaces,pointNeighbors);

	std::set<I32> inGroup;
	std::set<I32> inLockGroup;

	SpatialVector* pointBuffer[2];
	pointBuffer[0]=new SpatialVector[pointCount];
	pointBuffer[1]=new SpatialVector[pointCount];

	I32 readIndex=0;
	SpatialVector* readBuffer=pointBuffer[0];
	SpatialVector* writeBuffer=pointBuffer[1];

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		readBuffer[pointIndex]=spOutputPoint->spatialVector(pointIndex);
	}

	if(spLockPointGroup.isValid())
	{
		const I32 lockCount=spLockPointGroup->count();
		for(I32 lockIndex=0;lockIndex<lockCount;lockIndex++)
		{
			const I32 pointIndex=spLockPointGroup->integer(lockIndex);
			inLockGroup.insert(pointIndex);
		}
	}

	if(spInputPrimGroup.isValid())
	{
		for(U32 memberIndex=0;memberIndex<memberCount;memberIndex++)
		{
			const U32 subCount=spInputPrimGroup->subCount(memberIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 primitiveIndex=spInputPrimGroup->integer(memberIndex);
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);
				inGroup.insert(pointIndex);
			}
		}
	}
	else
	{
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const U32 subCount=spInputVertices->subCount(primitiveIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);
				inGroup.insert(pointIndex);
			}
		}
	}

	const Real conservationKeep=1.0-0.5*conservation;
	const Real conservationGive= -0.5*conservation;

	for(I32 iteration=0;iteration<iterations;iteration++)
	{
		memcpy((void*)writeBuffer,(void*)readBuffer,
				pointCount*sizeof(SpatialVector));

		//* TODO iterate on inGroup
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			if(inGroup.find(pointIndex)==inGroup.end())
			{
				continue;
			}

			std::set<I32>& rFaces=pointFaces[pointIndex];
			std::set<I32>& rNeighbors=pointNeighbors[pointIndex];

			const BWORD isBoundary=(rNeighbors.size()>rFaces.size());
			if(lockBoundaries && isBoundary)
			{
				continue;
			}

			if(inLockGroup.find(pointIndex)!=inLockGroup.end())
			{
				continue;
			}

			SpatialVector sum(0,0,0);
			Real weight(0);

			const I32 neighborCount=rNeighbors.size();
			if(neighborCount<2)
			{
				sum=readBuffer[pointIndex];
				weight=Real(1);
			}

			for(std::set<I32>::iterator it=rNeighbors.begin();
					it!=rNeighbors.end();it++)
			{
				const I32 otherIndex= *it;

				sum+=readBuffer[otherIndex];
				weight+=Real(1);
			}

			SpatialVector change=sum/Real(weight)-readBuffer[pointIndex];

			if(spInputNormal.isValid())
			{
				const SpatialVector norm=
						spInputNormal->spatialVector(pointIndex);
				const SpatialVector perpendicular=norm*dot(change,norm);
				const SpatialVector sliding=change-perpendicular;

				change=sliding*(Real(1)-resistSliding)+
						perpendicular*(Real(1)-resistShrinkage);
			}

			change*=fabs(smoothing);

			if(conservation>Real(0))
			{
				const SpatialVector reverse=
						conservationGive*change/Real(neighborCount);

				for(std::set<I32>::iterator it=rNeighbors.begin();
						it!=rNeighbors.end();it++)
				{
					const I32 otherIndex= *it;

					writeBuffer[otherIndex]+=reverse;
				}
			}

			writeBuffer[pointIndex]+=conservationKeep*change;
		}

		readIndex=!readIndex;
		readBuffer=pointBuffer[readIndex];
		writeBuffer=pointBuffer[!readIndex];
	}

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		spOutputPoint->set(pointIndex,readBuffer[pointIndex]);
	}

	m_locationArray.resize(inGroup.size());
	m_colorArray.resize(inGroup.size());
	I32 dotIndex(0);
	for(std::set<I32>::iterator it=inGroup.begin();it!=inGroup.end();it++)
	{
		const I32 pointIndex= *it;


		m_locationArray[dotIndex]=readBuffer[pointIndex];
		set(m_colorArray[dotIndex],1.0,1.0,0.0,1.0);

		dotIndex++;
	}

	delete[] pointBuffer[1];
	delete[] pointBuffer[0];

	rSummary.sPrintf("%d primitives",memberCount);

	if(primitiveGrouped)
	{
		String text;
		text.sPrintf(" in group %s",primitiveGroup.c_str());
		rSummary+=text;
	}

}

} /* namespace ext */
} /* namespace fe */
