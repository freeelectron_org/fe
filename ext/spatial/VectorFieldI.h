/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_VectorField_h__
#define __spatial_VectorField_h__

namespace fe
{
namespace ext
{

/** Vector Field */
class FE_DL_EXPORT VectorFieldI : virtual public Component
{
	public:
virtual	bool	sample(	SpatialVector &a_result,
						const SpatialVector &a_location)					= 0;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_VectorField_h__ */

