/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __scene_h__
#define __scene_h__

#include "fe/plugin.h"

#include "scene/SceneNodeI.h"

#endif /* __scene_h__ */
