/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_JunkOp_h__
#define __optest_JunkOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Generate bad data

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT JunkOp:
	public OperatorSurfaceCommon,
	public Initialize<JunkOp>
{
	public:

					JunkOp(void)											{}
virtual				~JunkOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_JunkOp_h__ */
