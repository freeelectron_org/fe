/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperatorSurfaceCommon_h__
#define __operate_OperatorSurfaceCommon_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Partial Generic SurfaceI Implemention

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperatorSurfaceCommon:
	public OperateCommon,
	public CastableAs<OperatorSurfaceCommon>,
	virtual public OperatorSurfaceI
{
	public:
				//* duplicates from SurfaceAccessibleI
		enum	Element
				{
					e_point=SurfaceAccessibleI::e_point,
					e_pointGroup=SurfaceAccessibleI::e_pointGroup,
					e_vertex=SurfaceAccessibleI::e_vertex,
					e_primitive=SurfaceAccessibleI::e_primitive,
					e_primitiveGroup=SurfaceAccessibleI::e_primitiveGroup,
					e_detail=SurfaceAccessibleI::e_detail
				};
		enum	Attribute
				{
					e_generic=SurfaceAccessibleI::e_generic,
					e_position=SurfaceAccessibleI::e_position,
					e_normal=SurfaceAccessibleI::e_normal,
					e_uv=SurfaceAccessibleI::e_uv,
					e_color=SurfaceAccessibleI::e_color,
					e_vertices=SurfaceAccessibleI::e_vertices,
					e_properties=SurfaceAccessibleI::e_properties
				};
		enum	Properties
				{
					e_openCurve=SurfaceAccessibleI::e_openCurve,
					e_countU=SurfaceAccessibleI::e_countU,
					e_countV=SurfaceAccessibleI::e_countV,
					e_wrappedU=SurfaceAccessibleI::e_wrappedU,
					e_wrappedV=SurfaceAccessibleI::e_wrappedV,
					e_depth=SurfaceAccessibleI::e_depth
				};
		enum	Message
				{
					e_quiet=SurfaceAccessibleI::e_quiet,
					e_warning=SurfaceAccessibleI::e_warning,
					e_error=SurfaceAccessibleI::e_error
				};
		enum	Creation
				{
					e_refuseMissing=SurfaceAccessibleI::e_refuseMissing,
					e_createMissing=SurfaceAccessibleI::e_createMissing
				};
		enum	AtomicChange
				{
					e_pointsOnly=
							SurfaceAccessibleI::e_pointsOnly,
					e_pointsOfPrimitives=
							SurfaceAccessibleI::e_pointsOfPrimitives,
					e_primitivesOnly=
							SurfaceAccessibleI::e_primitivesOnly,
					e_primitivesWithPoints=
							SurfaceAccessibleI::e_primitivesWithPoints
				};

							OperatorSurfaceCommon(void):
								m_pPlugin(NULL)
							{
								m_spThreadingState=new
										SurfaceAccessibleI::ThreadingState();
							}
virtual						~OperatorSurfaceCommon(void)				{}

							//* As HandlerI
virtual	void				handleBind(sp<SignalerI> a_spSignalerI,
									sp<Layout> a_spLayout);

							//* As OperatorSurfaceI
virtual	BWORD				saveState(String& rBuffer)		{ return FALSE; }
virtual	BWORD				loadState(const String& rBuffer){ return FALSE; }

virtual	BWORD				undo(String a_change,sp<Counted> a_spCounted);
virtual	BWORD				redo(String a_change,sp<Counted> a_spCounted);

virtual	void				setPlugin(OperatorPlugin* a_pPlugin)
							{	m_pPlugin=a_pPlugin; }

		void				dirty(BWORD a_aggressive)
							{	if(m_pPlugin) m_pPlugin->dirty(a_aggressive); }

		void				setSharingName(String a_name)
							{	m_sharingName=a_name; }
		String				sharingName(void)
							{	return m_sharingName; }
		void				changeSharing(BWORD a_include);
		void				selectNode(String a_selectName);
		void				select(void)
							{	if(m_pPlugin) m_pPlugin->select(); }

		BWORD				interrupted(void)
							{	return m_pPlugin?
										m_pPlugin->interrupted(): FALSE; }

							//* As OperatorSurfaceCommon
		Real				startFrame(Record& a_rSignal)
							{
								Real* pFrame=m_aStartFrame.queryAttribute(
										a_rSignal);
								return pFrame? *pFrame: -1.0;
							}
		Real				endFrame(Record& a_rSignal)
							{
								Real* pFrame=m_aEndFrame.queryAttribute(
										a_rSignal);
								return pFrame? *pFrame: -1.0;
							}
		Real				currentFrame(Record& a_rSignal)
							{
								Real* pFrame=m_aFrame.queryAttribute(a_rSignal);
								return pFrame? *pFrame: -1.0;
							}
		void				setCurrentFrame(Record& a_rSignal,Real a_frame)
							{
								Real* pReal=m_aFrame.queryAttribute(a_rSignal);
								if(pReal)
								{
									*pReal=a_frame;
								}
							}
		Real				currentTime(Record& a_rSignal)
							{
								Real* pTime=m_aTime.queryAttribute(a_rSignal);
								return pTime? *pTime: -1.0;
							}
		sp<Component>		surfaceOutput(Record& a_rSignal)
							{
								sp<Component>* pspComponent=
										m_aSurfaceOutput.queryAttribute(
										a_rSignal);
								if(pspComponent)
								{
									return *pspComponent;
								}
								return sp<Component>(NULL);
							}
		void				setSurfaceOutput(Record& a_rSignal,
									sp<Component> a_spComponent)
							{
								sp<Component>* pspComponent=
										m_aSurfaceOutput.queryAttribute(
										a_rSignal);
								if(pspComponent)
								{
									*pspComponent=a_spComponent;
								}
							}
		sp<Component>		outputDraw(Record& a_rSignal)
							{
								sp<Component>* pspComponent=
										m_aDrawOutput.queryAttribute(a_rSignal);
								if(pspComponent)
								{
									return *pspComponent;
								}
								return sp<Component>(NULL);
							}
		sp<Component>		guideDraw(Record& a_rSignal)
							{
								sp<Component>* pspComponent=
										m_aDrawGuide.queryAttribute(a_rSignal);
								if(pspComponent)
								{
									return *pspComponent;
								}
								return sp<Component>(NULL);
							}
		sp<Component>		brushDraw(Record& a_rSignal)
							{
								sp<Component>* pspComponent=
										m_aDrawBrush.queryAttribute(a_rSignal);
								if(pspComponent)
								{
									return *pspComponent;
								}
								return sp<Component>(NULL);
							}
		sp<Component>		brushOverlayDraw(Record& a_rSignal)
							{
								sp<Component>* pspComponent=m_aDrawBrushOverlay
										.queryAttribute(a_rSignal);
								if(pspComponent)
								{
									return *pspComponent;
								}
								return sp<Component>(NULL);
							}
		SpatialTransform	cameraTransform(Record& a_rSignal)
							{
								SpatialTransform* pSpatialTransform=
										m_aCameraTransform.queryAttribute(
										a_rSignal);
								if(pSpatialTransform)
								{
									return *pSpatialTransform;
								}
								return SpatialTransform::identity();
							}
		SpatialVector		rayOrigin(Record& a_rSignal)
							{
								SpatialVector* pSpatialVector=
										m_aRayOrigin.queryAttribute(a_rSignal);
								if(pSpatialVector)
								{
									return *pSpatialVector;
								}
								return SpatialVector(0.0,0.0,0.0);
							}
		SpatialVector		rayDirection(Record& a_rSignal)
							{
								SpatialVector* pSpatialVector=
										m_aRayDirection.queryAttribute(
										a_rSignal);
								if(pSpatialVector)
								{
									return *pSpatialVector;
								}
								return SpatialVector(0.0,0.0,0.0);
							}

							//* boilerplate reduction methods
		BWORD				accessDraw(sp<DrawI>& a_rspDraw,Record& a_rSignal,
									Message a_message=e_error)
							{
								a_rspDraw=outputDraw(a_rSignal);
								if(a_rspDraw.isValid())
								{
									return TRUE;
								}
								reportDraw("Output",a_message);
								return FALSE;
							}
		BWORD				accessGuide(sp<DrawI>& a_rspDraw,Record& a_rSignal,
									Message a_message=e_error)
							{
								a_rspDraw=guideDraw(a_rSignal);
								if(a_rspDraw.isValid())
								{
									return TRUE;
								}
								reportDraw("Guide",a_message);
								return FALSE;
							}
		BWORD				accessBrush(sp<DrawI>& a_rspDraw,Record& a_rSignal,
									Message a_message=e_error)
							{
								a_rspDraw=brushDraw(a_rSignal);
								if(a_rspDraw.isValid())
								{
									return TRUE;
								}
								reportDraw("Brush",a_message);
								return FALSE;
							}
		BWORD				accessBrushOverlay(sp<DrawI>& a_rspDraw,
									Record& a_rSignal,
									Message a_message=e_error)
							{
								a_rspDraw=brushOverlayDraw(a_rSignal);
								if(a_rspDraw.isValid())
								{
									return TRUE;
								}
								reportDraw("BrushOverlay",a_message);
								return FALSE;
							}
		BWORD				accessOutput(
									sp<SurfaceAccessibleI>& a_rspAccessible,
									Record& a_rSignal,Message a_message=e_error)
							{
								a_rspAccessible=surfaceOutput(a_rSignal);
								if(a_rspAccessible.isValid() &&
										a_rspAccessible->isBound())
								{
									a_rspAccessible->bind(m_spThreadingState);
									m_spSurfaceOutput=a_rspAccessible;
									return TRUE;
								}
								a_rspAccessible=NULL;
								reportAccessible("<output>",a_message);
								return FALSE;
							}
		BWORD				accessOutput(sp<SurfaceAccessorI>& a_rspAccessor,
									Record& a_rSignal,
									Element a_element,Attribute a_attribute,
									Message a_message=e_error,
									Creation a_create=e_createMissing)
							{
								sp<SurfaceAccessibleI> spAccessible;
								if(!accessOutput(spAccessible,
										a_rSignal,a_message))
								{
									a_rspAccessor=NULL;
									return FALSE;
								}

								spAccessible->bind(m_spThreadingState);
								return access(a_rspAccessor,spAccessible,
										a_element,a_attribute,
										a_message,a_create);
							}
		BWORD				accessOutput(sp<SurfaceAccessorI>& a_rspAccessor,
									Record& a_rSignal,
									Element a_element,String a_attrName,
									Message a_message=e_error,
									Creation a_create=e_createMissing)
							{
								sp<SurfaceAccessibleI> spAccessible;
								if(!accessOutput(spAccessible,
										a_rSignal,a_message))
								{
									a_rspAccessor=NULL;
									return FALSE;
								}

								spAccessible->bind(m_spThreadingState);
								return access(a_rspAccessor,spAccessible,
										a_element,a_attrName,
										a_message,a_create);
							}
		BWORD				accessOutput(sp<SurfaceI>& a_rspSurface,
									Record& a_rSignal,
									String a_primitiveGroup,
									Message a_message=e_error,
									I32 a_subIndex= -1)
							{
								sp<SurfaceAccessibleI> spAccessible=
										surfaceOutput(a_rSignal);
								if(spAccessible.isNull())
								{
									a_rspSurface=NULL;
									return FALSE;
								}
								spAccessible->bind(m_spThreadingState);
								return access(a_rspSurface,spAccessible,
										a_primitiveGroup,a_message,a_subIndex);
							}
		BWORD				accessOutput(sp<SurfaceI>& a_rspSurface,
									Record& a_rSignal,
									Message a_message=e_error,
									I32 a_subIndex= -1)
							{
								return accessOutput(a_rspSurface,a_rSignal,"",
										a_message,a_subIndex);
							}
virtual	I32					discardPattern(
									sp<SurfaceAccessibleI>& a_spAccessible,
									Element a_element,String a_pattern)
							{
								return a_spAccessible->discardPattern(
										SurfaceAccessibleI::Element(a_element),
										a_pattern);
							}
virtual	BWORD				discard(sp<SurfaceAccessibleI>& a_spAccessible,
									Element a_element,String a_attrName)
							{
								return a_spAccessible->discard(
										SurfaceAccessibleI::Element(a_element),
										a_attrName);
							}
virtual	BWORD				discard(sp<SurfaceAccessibleI>& a_spAccessible,
									Element a_element,Attribute a_attribute)
							{
								return a_spAccessible->discard(
										SurfaceAccessibleI::Element(a_element),
										SurfaceAccessibleI::Attribute(
										a_attribute));
							}
virtual	I32					deleteElements(
									sp<SurfaceAccessibleI>& a_spAccessible,
									Element a_element,String a_groupString,
									BWORD a_retainGroups=FALSE)
							{
								return a_spAccessible->deleteElements(
										SurfaceAccessibleI::Element(a_element),
										a_groupString,a_retainGroups);
							}
virtual	I32					deleteElements(
									sp<SurfaceAccessibleI>& a_spAccessible,
									Element a_element,std::set<I32>& a_rIntSet,
									BWORD a_retainGroups=FALSE)
							{
								return a_spAccessible->deleteElements(
										SurfaceAccessibleI::Element(a_element),
										a_rIntSet,a_retainGroups);
							}

		BWORD				access(sp<SurfaceI>& a_rspSurface,
									sp<SurfaceAccessibleI>& a_rspAccessible,
									String a_primitiveGroup,
									Message a_message=e_error,
									I32 a_subIndex= -1)
							{
								a_rspSurface=a_subIndex<0?
										a_rspAccessible->surface(
										a_primitiveGroup):
										a_rspAccessible->subSurface(
										a_subIndex,a_primitiveGroup);
								if(a_rspSurface.isValid())
								{
									return TRUE;
								}
								reportSurface(a_rspAccessible->name(),
										a_message);
								return FALSE;
							}
		BWORD				access(sp<SurfaceI>& a_rspSurface,
									sp<SurfaceAccessibleI>& a_rspAccessible,
									Message a_message=e_error,
									I32 a_subIndex= -1)
							{
								return access(a_rspSurface,a_rspAccessible,"",
										a_message,a_subIndex);
							}
		BWORD				access(sp<SurfaceI>& a_rspSurface,
									String a_key,String a_primitiveGroup,
									Message a_message=e_error,
									I32 a_subIndex= -1)
							{
								sp<SurfaceAccessibleI> spAccessible;
								if(!access(spAccessible,a_key,a_message))
								{
									a_rspSurface=NULL;
									return FALSE;
								}

								return access(a_rspSurface,spAccessible,
										a_primitiveGroup,a_message,a_subIndex);
							}
		BWORD				access(sp<SurfaceI>& a_rspSurface,
									String a_key,Message a_message=e_error,
									I32 a_subIndex= -1)
							{
								return access(a_rspSurface,a_key,"",
										a_message,a_subIndex);
							}
		BWORD				access(sp<SurfaceAccessibleI>& a_rspAccessible,
									String a_key,Message a_message=e_error)
							{
								a_rspAccessible=catalog< sp<Component> >(a_key);
								if(a_rspAccessible.isValid() &&
										a_rspAccessible->isBound())
								{
									return TRUE;
								}
								a_rspAccessible=NULL;
								reportAccessible(a_key,a_message);
								return FALSE;
							}
		BWORD				access(sp<SurfaceAccessorI>& a_rspAccessor,
									sp<SurfaceAccessibleI>& a_rspAccessible,
									String a_nodeName,
									Element a_element,Attribute a_attribute,
									Message a_message=e_error,
									Creation a_create=e_createMissing)
							{
								const BWORD writable=isOutput(a_rspAccessible);
								a_rspAccessor=a_rspAccessible->accessor(
										a_nodeName,
										SurfaceAccessibleI::Element(a_element),
										SurfaceAccessibleI::Attribute(
										a_attribute),
										writable?
										SurfaceAccessibleI::Creation(a_create):
										SurfaceAccessibleI::e_refuseMissing,
										writable?
										SurfaceAccessibleI::e_readWrite:
										SurfaceAccessibleI::e_readOnly);
								if(a_rspAccessor.isValid())
								{
									return TRUE;
								}
								const String attrName=attributeText(a_attribute);
								reportAccessor(a_rspAccessible->name(),
										a_element,attrName,a_message);
								return FALSE;
							}
		BWORD				access(sp<SurfaceAccessorI>& a_rspAccessor,
									sp<SurfaceAccessibleI>& a_rspAccessible,
									String a_nodeName,
									Element a_element,String a_attrName,
									Message a_message=e_error,
									Creation a_create=e_createMissing)
							{
								const BWORD writable=isOutput(a_rspAccessible);
								a_rspAccessor=a_rspAccessible->accessor(
										a_nodeName,
										SurfaceAccessibleI::Element(a_element),
										a_attrName,
										writable?
										SurfaceAccessibleI::Creation(a_create):
										SurfaceAccessibleI::e_refuseMissing,
										writable?
										SurfaceAccessibleI::e_readWrite:
										SurfaceAccessibleI::e_readOnly);
								if(a_rspAccessor.isValid())
								{
									return TRUE;
								}
								reportAccessor(a_rspAccessible->name(),
										a_element,
										"\""+a_attrName+"\"",a_message);
								return FALSE;
							}
		BWORD				access(sp<SurfaceAccessorI>& a_rspAccessor,
									sp<SurfaceAccessibleI>& a_rspAccessible,
									Element a_element,Attribute a_attribute,
									Message a_message=e_error,
									Creation a_create=e_createMissing)
							{
								return access(a_rspAccessor,a_rspAccessible,
										"",a_element,a_attribute,
										a_message,a_create);
							}
		BWORD				access(sp<SurfaceAccessorI>& a_rspAccessor,
									sp<SurfaceAccessibleI>& a_rspAccessible,
									Element a_element,String a_attrName,
									Message a_message=e_error,
									Creation a_create=e_createMissing)
							{
								return access(a_rspAccessor,a_rspAccessible,
										"",a_element,a_attrName,
										a_message,a_create);
							}
		BWORD				access(sp<SurfaceAccessorI>& a_rspAccessor,
									String a_key,
									Element a_element,Attribute a_attribute,
									Message a_message=e_error)
							{
								sp<SurfaceAccessibleI> spAccessible;
								if(!access(spAccessible,a_key,a_message))
								{
									a_rspAccessor=NULL;
									return FALSE;
								}

								return access(a_rspAccessor,spAccessible,
										a_element,a_attribute,
										a_message,e_refuseMissing);
							}
		BWORD				access(sp<SurfaceAccessorI>& a_rspAccessor,
									String a_key,
									Element a_element,String a_attrName,
									Message a_message=e_error)
							{
								sp<SurfaceAccessibleI> spAccessible;
								if(!access(spAccessible,a_key,a_message))
								{
									a_rspAccessor=NULL;
									return FALSE;
								}

								return access(a_rspAccessor,spAccessible,
										a_element,a_attrName,
										a_message,e_refuseMissing);
							}

		BWORD				isOutput(
									sp<SurfaceAccessibleI>& a_rspAccessible)
							{
								return (a_rspAccessible==m_spSurfaceOutput);
							}
		void				setOutput(
									sp<SurfaceAccessibleI>& a_rspAccessible)
							{
								m_spSurfaceOutput=a_rspAccessible;
							}

		String				generateSignature(
								sp<SurfaceAccessibleI>& a_rspAccessibleI);

static	String				elementText(Element a_element)
							{
								switch(a_element)
								{
									case e_point:
										return "Point";
									case e_pointGroup:
										return "PointGroup";
									case e_vertex:
										return "Vertex";
									case e_primitive:
										return "Primitive";
									case e_primitiveGroup:
										return "PrimitiveGroup";
									case e_detail:
										return "Detail";
									default:
										return "<bad element>";
								}
							}
static	String				elementText(SurfaceAccessibleI::Element a_element)
							{	return elementText(Element(a_element)); }

static	String				attributeText(Attribute a_attribute)
							{
								switch(a_attribute)
								{
									case e_generic:
										return "Generic";
									case e_position:
										return "Position";
									case e_normal:
										return "Normal";
									case e_uv:
										return "UVW";
									case e_color:
										return "Color";
									case e_vertices:
										return "Vertices";
									case e_properties:
										return "Properties";
									default:
										return "<bad attribute>";
								}
							}
static	String				attributeText(
									SurfaceAccessibleI::Attribute a_attribute)
							{	return attributeText(Attribute(a_attribute)); }

	protected:

		BWORD				determineNeighborhood(
									sp<SurfaceAccessibleI>& a_rspAccessible,
									Array< std::set<I32> >& a_rPointFaces,
									Array< std::set<I32> >& a_rPointNeighbors);

		sp<Catalog>			duplicateCatalog(void);

virtual	BWORD				anticipate(String a_change,
									sp<Counted> a_spCounted);
virtual	BWORD				anticipate(String a_change)
							{	return anticipate(a_change,
									sp<Counted>(NULL)); }
virtual	BWORD				resolve(String a_change,
									sp<Counted> a_spCounted);
virtual	BWORD				resolve(String a_change)
							{	return resolve(a_change,
										sp<Counted>(NULL)); }
virtual	BWORD				resolve(void)
							{	return resolve("",sp<Counted>(NULL)); }
virtual	BWORD				chronicle(String a_change,
									sp<Counted> a_spOldCounted,
									sp<Counted> a_spNewCounted)
							{	return m_pPlugin? m_pPlugin->chronicle(
										a_change,a_spOldCounted,
										a_spNewCounted): FALSE; }
virtual	BWORD				chronicle(String a_change)
							{	return chronicle(a_change,sp<Counted>(NULL),
										sp<Counted>(NULL)); }

		sp<SurfaceAccessibleI::ThreadingState>	m_spThreadingState;

	private:
		void				report(String a_text,Message a_message)
							{
#if FE_CODEGEN<=FE_DEBUG
								feLog("%s %s\n",name().c_str(),a_text.c_str());
#endif
								catalog<String>(a_message==e_error?
										"error": "warning")+=a_text+"; ";
							}
		void				reportDraw(String a_drawName,Message a_message)
							{
								if(a_message==e_quiet)
								{
									return;
								}
								String text;
								text.sPrintf("failed to access drawing"
										" interface \"%s\"",a_drawName.c_str());
								report(text,a_message);
							}
		void				reportSurface(String a_key,Message a_message)
							{
								if(a_message==e_quiet)
								{
									return;
								}
								String text;
								text.sPrintf("failed to access \"%s\""
										" (as searchable)",a_key.c_str());
								report(text,a_message);
							}
		void				reportAccessible(String a_key,Message a_message)
							{
								if(a_message==e_quiet)
								{
									return;
								}
								String text;
								if(a_key=="<output>")
								{
									text.sPrintf(
											"failed to access general output");
								}
								else
								{
									text.sPrintf("failed to access general"
											" input \"%s\"",a_key.c_str());
								}
								report(text,a_message);
							}
		void				reportAccessor(String a_key,Element a_element,
									String a_attrName,Message a_message)
							{
								if(a_message==e_quiet)
								{
									return;
								}
								String elementName=elementText(a_element);
								String text;
								text.sPrintf("failed to access %s %s on %s",
										elementName.c_str(),
										a_attrName.c_str(),
										a_key.c_str()[0]=='<'? a_key.c_str():
										("\""+a_key+"\"").c_str());
								report(text,a_message);

//								feX("inaccessible element");
							}

		OperatorPlugin*							m_pPlugin;

		hp<SurfaceAccessibleI>					m_spSurfaceOutput;

		Accessor< Real >						m_aStartFrame;
		Accessor< Real >						m_aEndFrame;
		Accessor< Real >						m_aFrame;
		Accessor< Real >						m_aTime;
		Accessor< sp<Component> >				m_aSurfaceOutput;
		Accessor< sp<Component> >				m_aDrawOutput;
		Accessor< sp<Component> >				m_aDrawGuide;
		Accessor< sp<Component> >				m_aDrawBrush;
		Accessor< sp<Component> >				m_aDrawBrushOverlay;
		Accessor<SpatialTransform>				m_aCameraTransform;
		Accessor<SpatialVector>					m_aRayOrigin;
		Accessor<SpatialVector>					m_aRayDirection;

		String									m_sharingName;

		String									m_anticipation;
		sp<Counted>								m_spOldCounted;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperatorSurfaceCommon_h__ */
