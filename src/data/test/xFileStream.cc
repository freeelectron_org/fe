/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

int main(int argc, char *argv[])
{
	UnitTest unitTest;

	try
	{
		sp<Master> spMaster(new Master);
		sp<TypeMaster> spTypeMaster(spMaster->typeMaster());
		assertCore(spTypeMaster);

		Peeker peeker;
		spTypeMaster->peek(peeker);
		feLog("\nTypeMaster:%s\n",peeker.output().c_str());

		spMaster->registry()->manage("feDataDL");

		sp<Scope> spScope(spMaster->registry()->create("Scope"));
		spScope->setName("default_scope");

		sp<data::FileStreamI> spFileStream(
			spMaster->registry()->create("FileStreamI.Ascii"));
		spFileStream->bind(spScope);
		sp<RecordGroup> rg_test =
			spFileStream->input("test.rg");


		sp<Scope> spAScope(spMaster->registry()->create("Scope"));
		spAScope->setName("scope_A");
		sp<Scope> spBScope(spMaster->registry()->create("Scope"));
		spBScope->setName("scope_B");


		Accessor<int>		a(spAScope,"a");
		Accessor<Record>	r(spAScope,"r");
		Accessor<int>		b(spBScope,"b");

		sp<Layout>	l_A = spAScope->declare("layout_A");
		sp<Layout>	l_B = spBScope->declare("layout_B");

		l_A->populate(a);
		l_A->populate(r);

		l_B->populate(b);

		Record r_A = spAScope->createRecord(l_A);
		Record r_A2 = spAScope->createRecord(l_A);
		Record r_B = spBScope->createRecord(l_B);

		a(r_A) = 0;
		a(r_A2) = 0;
		r(r_A) = r_B;
		b(r_B) = 0;

		sp<RecordGroup> rg_A(new RecordGroup());

		rg_A->add(r_A);

		spFileStream->bind(spAScope);
		spFileStream->output("test_out.rg", rg_A);

		feLog("\nTypeMaster:%s\n",peeker.output().c_str());
	}
	catch(Exception &e)
	{
		e.log();
		return 1;
	}
	catch(...)
	{
		feLog("unhandled exception\n");
		return 1;
	}

	return unitTest.failures();

}

