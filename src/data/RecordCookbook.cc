/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#include <sstream>

#define FE_COOKBOOK_DEBUG	FALSE

namespace fe
{

RecordCookbook::RecordCookbook(void):
	m_name("RecordCookBook")
{
	setName(m_name);
}

RecordCookbook::~RecordCookbook(void)
{
	clear();
}

void RecordCookbook::initialize(void)
{
	sp<Registry> spRegistry=registry();
	registerDependency(spRegistry);
}

void RecordCookbook::registerDependency(sp<Registry> a_spRegistry)
{
	if(a_spRegistry.isValid())
	{
		a_spRegistry->addDependent(sp<Counted>(
				new RecordCookbook::Clear(sp<RecordCookbook>(this))));
	}
}

void RecordCookbook::clear(void)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::clear\n");
#endif

	m_recipeMap.clear();
}

void RecordCookbook::Recipe::setup(String name,
	sp<RecordCookbook> spRecordCookBook)
{
	m_name=name;
	m_hpRecordCookbook=spRecordCookBook;
}

sp<RecordCookbook::Recipe> RecordCookbook::lookup(String recipeName)
{
	sp<Recipe> spRecipe=m_recipeMap[recipeName];
	if(!spRecipe.isValid())
	{
		m_recipeMap[recipeName]=new Recipe();
		m_recipeMap[recipeName]->setup(recipeName,sp<RecordCookbook>(this));
#if FE_COUNTED_TRACK
		m_recipeMap[recipeName]->trackReference(&m_recipeMap[recipeName],
				"RecordCookbook::lookup StringRecipeMap");
#endif
		spRecipe=m_recipeMap[recipeName];
	}
	return spRecipe;
}

void RecordCookbook::findLayoutList(sp<Scope> spScope,String recipeName,
		Array<String>& rLayoutList)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::findLayoutList %s\n",recipeName.c_str());
#endif

	sp<Recipe> spRecipe=lookup(recipeName);
	if(spRecipe.isValid())
	{
		spRecipe->findLayoutList(spScope,rLayoutList);
	}
}

void RecordCookbook::derive(String recipeName,String parentName)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::derive(%s,%s)\n",recipeName.c_str(),
			parentName.c_str());
#endif

	sp<Recipe> spRecipe=lookup(recipeName);
	if(spRecipe.isValid())
	{
		spRecipe->derive(parentName);
	}

	spRecipe=lookup(parentName);
	if(spRecipe.isValid())
	{
		spRecipe->adopt(recipeName);
	}
}

void RecordCookbook::add(String recipeName,String attributeName,String value)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::add(%s,%s,\"%s\")\n",recipeName.c_str(),
			attributeName.c_str(),value.c_str());
#endif

	sp<Recipe> spRecipe=lookup(recipeName);
	if(spRecipe.isValid())
	{
		spRecipe->add(attributeName,value);
	}
}

void RecordCookbook::cook(sp<Scope> spScope,String recipeName,Record& rRecord)
{
	cook_internal(spScope,recipeName,rRecord,TRUE,NULL);
}

void RecordCookbook::cook_internal(sp<Scope> spScope,String recipeName,
		Record& rRecord,BWORD try_cache,StringIngredientMap* pNewCacheMap)
{
	if(!rRecord.isValid())
	{
		feLog("RecordCookbook::cook invalid Record\n");
		return;
	}
	sp<Layout> spLayout=rRecord.layout();
	if(!spLayout.isValid())
	{
		feLog("RecordCookbook::cook invalid Layout\n");
		return;
	}

#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::cook \"%s\" as \"%s\"\n",
			spLayout->name().c_str(),recipeName.c_str());
#endif

	sp<Recipe> spRecipe=m_recipeMap[recipeName];
	if(spRecipe.isValid())
	{
		spRecipe->cook(spScope,rRecord,try_cache,pNewCacheMap);
	}
}

void RecordCookbook::spoil(String recipeName)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::spoil(%s)\n",recipeName.c_str());
#endif

	sp<Recipe> spRecipe=lookup(recipeName);
	if(spRecipe.isValid())
	{
		spRecipe->spoil();
	}
}

void RecordCookbook::Recipe::derive(String parentName)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::Recipe::derive(%s)\n",parentName.c_str());
#endif

	m_prerequisites.push_back(parentName);
	spoil();
}

void RecordCookbook::Recipe::adopt(String childName)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::Recipe::adopt(%s)\n",childName.c_str());
#endif

	m_dependents.push_back(childName);
}

void RecordCookbook::Recipe::spoil(void)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::Recipe::spoil \"%s\"\n",m_name.c_str());
#endif

	m_cacheMap.clear();

	U32 count=m_dependents.size();
	for(U32 m=0;m<count;m++)
	{
		//* TODO cycle protection

		m_hpRecordCookbook->spoil(m_dependents[m]);
	}
}

void RecordCookbook::Recipe::findLayoutList(sp<Scope> spScope,
		Array<String>& rLayoutList)
{
	if(spScope->lookupLayout(m_name).isValid())
	{
#if FE_COOKBOOK_DEBUG
		feLog("RecordCookbook::Recipe::findLayoutList"
				" layout \"%s\" exists in Scope \"%s\"\n",
				m_name.c_str(),spScope->name().c_str());
#endif
		rLayoutList.push_back(m_name);
		return;
	}
	else
	{
		U32 count=m_prerequisites.size();
#if FE_COOKBOOK_DEBUG
		feLog("RecordCookbook::Recipe::findLayoutList count=%d\n",count);
#endif
		for(U32 m=0;m<count;m++)
		{
			//* TODO cycle protection

#if FE_COOKBOOK_DEBUG
			feLog("RecordCookbook::Recipe::findLayoutList"
					" \"%s\" check \"%s\"\n",
					m_name.c_str(),m_prerequisites[m].c_str());
#endif

			m_hpRecordCookbook->findLayoutList(spScope,
					m_prerequisites[m],rLayoutList);
		}
	}
}

void RecordCookbook::Recipe::add(String attributeName,String value)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::Recipe::add(%s,\"%s\")\n",
			attributeName.c_str(),value.c_str());
#endif

	m_attributeMap[attributeName]=value;
	spoil();
}

void RecordCookbook::Recipe::cook(sp<Scope> spScope,Record& rRecord,
		BWORD try_cache,StringIngredientMap* pNewCacheMap)
{
#if FE_COOKBOOK_DEBUG
	feLog("RecordCookbook::Recipe::cook\n");
#endif
	StringIngredientMap& cacheMap=m_cacheMap[spScope];

	if(try_cache && cacheMap.size())
	{
		//* use cache
		StringIngredientMap::iterator it;
		for(it=cacheMap.begin();it!=cacheMap.end();it++)
		{
			Ingredient& value=it->second;

#if FE_COOKBOOK_DEBUG
			String attr_name=it->first;
			feLog("RecordCookbook::Recipe::cook template \"%s\""
					" %s:%p,%p,%p %p\n",m_name.c_str(),attr_name.c_str(),
					value.m_locator,value.type()->size(),
					value.data(),*(char *)value.data());
#endif

			BWORD success=value.type()->copy(
					(char*)rRecord.rawAttribute(value.m_locator),
					value.data());
			if(!success)
			{
				String attr_name=it->first;
				feLog("RecordCookbook::Recipe::cook copy failed \"%s\""
						" %s:%p,%p,%p %p\n",m_name.c_str(),attr_name.c_str(),
						value.m_locator,value.type()->size(),
						value.data(),*(char *)value.data());
			}
		}

		return;
	}

	if(!pNewCacheMap)
	{
		pNewCacheMap=&cacheMap;
	}

	//* prerequisites recipes
	U32 count=m_prerequisites.size();
	for(U32 m=0;m<count;m++)
	{
		//* TODO cycle protection

#if FE_COOKBOOK_DEBUG
		feLog("RecordCookbook::Recipe::cook prerequisite \"%s\"\n",
				m_prerequisites[m].c_str());
#endif

		m_hpRecordCookbook->cook_internal(spScope,m_prerequisites[m],rRecord,
				FALSE,pNewCacheMap);
	}

	//* this recipe
	StringStringMap::iterator it;
	for(it=m_attributeMap.begin();it!=m_attributeMap.end();it++)
	{
		String attr_name=it->first;
		String& value=it->second;

		//* parse like AsciiReader

		FE_UWORD index=0;
		sp<Attribute>& rspAttribute=spScope->findAttribute(attr_name,index);
		if(!rspAttribute.isValid())
		{
			feX("RecordCookbook::Recipe::cook",
					"attribute '%s' not found in scope for '%s'",
					attr_name.c_str(),m_name.c_str());
		}

		bool has=rRecord.layout()->checkAttribute(index);
		if(!has)
		{
			feX("RecordCookbook::Recipe::cook",
					"offsetTable[%d] for '%s' in '%s' invalid",
					index,attr_name.c_str(),m_name.c_str());
		}

		void *instance = has? rRecord.rawAttribute(index): NULL;
		sp<BaseType> spBT = rspAttribute->type();

		if(spBT->getInfo().isValid())
		{
			std::string s(value.c_str());
			std::istringstream istrm(s);
			spBT->getInfo()->input(istrm,instance,BaseType::Info::e_ascii);

			BWORD success=(*pNewCacheMap)[attr_name].set(index,spBT,
					(char*)instance);
			if(!success)
			{
				feLog("RecordCookbook::Recipe::cook"
						" offsetTable[%d] for \"%s\" in \"%s\" failed to set\n",
						index,attr_name.c_str(),m_name.c_str());
			}
		}
		else
		{
			feX("RecordCookbook::Recipe::cook","invalid type");
		}
	}
}

RecordCookbook::Ingredient::~Ingredient(void)
{
	char* block=(char*)data();
	if(block)
	{
#if FE_COUNTED_TRACK
		type()->untrackReference(this);
#endif
		type()->destruct(block);
		delete[] block;
	}
}

BWORD RecordCookbook::Ingredient::set(FE_UWORD a_locator,sp<BaseType>& a_rspBT,
		void* a_pData)
{
	char* block=(char*)data();
	if(block)
	{
#if FE_COUNTED_TRACK
		a_rspBT->untrackReference(this);
#endif
		a_rspBT->destruct(block);
		delete[] block;
	}

#if FE_COUNTED_TRACK
	a_rspBT->trackReference(this,"RecordCookbook::Ingredient::set");
#endif
	block=new char[a_rspBT->size()];

	a_rspBT->construct(block);
	BWORD success=a_rspBT->copy(block,a_pData);
	if(!success)
	{
		feLog("RecordCookbook::Ingredient::set copy failed\n");
	}

	Instance::set(block,a_rspBT,NULL);
	m_locator=a_locator;
	return success;
}

} /* namespace */
