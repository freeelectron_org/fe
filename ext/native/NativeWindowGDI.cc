/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

#define	NW_SOFTWARE_RENDER	FALSE

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_GDI

HINSTANCE gs_hinstDLL;
NativeWindow* pLastCreatedWindow;	//* TODO

Result NativeWindow::nativeInitialize(void)
{
	return e_ok;
}

Result NativeWindow::nativeStartup(void)
{
#if FE_WINDOW_EVENT_DEBUG
	feLog("NativeWindow::nativeStartup()\n");
#endif

	String title("NativeWindow");
	String windowClass("NativeWindowClass");
	HINSTANCE hInstance=gs_hinstDLL;

	feLog("NativeWindow::nativeStartup hInstance=%p\n",hInstance);

	pLastCreatedWindow=this;

#if FALSE
	m_windowHandle = CreateWindowExA(
			dwexstyle,      // extended style
			title.Raw8(),	// registered class name
			title.Raw8(),	// window name
			dwstyle,        // style
			ix,dy-iy-sy,    // position
			sx,sy,          // size
			parent_hwnd,    // parent
			(HMENU) NULL,   // menu
			EW_HINSTANCE,   // app instance handle
			(LPVOID) NULL); // pointer to window creation data
#else
	m_windowHandle=CreateWindowA(
			windowClass.c_str(),
			title.c_str(),
			WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			20,20,						// pos
			width(m_box),height(m_box),	// size
			NULL,NULL,
			hInstance,
			NULL);
#endif

	if(m_windowHandle==NULL)
	{
		const DWORD errorCode=GetLastError();
		feLog("NativeWindow::nativeStartup CreateWindowA failed code=%d\n",
				errorCode);
		return e_refused;
	}

	sp<NativeEventContext> spNativeEventContext=m_spEventContextI;
	spNativeEventContext->acquire();
	void *pEventContext=spNativeEventContext.raw();

	// since SetWindowLong() returns the previous (which could be 0)
	// or 0 on failure, you have to do it twice to see if it worked
	SetWindowLongPtr(m_windowHandle,0,(LONG_PTR)pEventContext);
	if(!SetWindowLongPtr(m_windowHandle,0,(LONG_PTR)pEventContext))
	{
		feLog("NativeWindow::nativeStartup SetWindowLong failed code=%d\n",
				GetLastError());
		return e_refused;
	}

	HDC hdc=GetDC(m_windowHandle);
	setDCPixelFormat(hdc);
	m_hglrc=wglCreateContext(hdc);
	if(m_hglrc==NULL)
	{
		feX("NativeWindow::open","wglCreateContext failed code=%d",
				GetLastError());
	}

	makeCurrent();

	GLenum glewError=glewInit();
	if(glewError!=GLEW_OK)
	{
		feX("NativeWindow::open","glewInit failed code=%d",glewError);
	}

	feLog("using GLEW %s\n",glewGetString(GLEW_VERSION));

	spNativeEventContext->notifyCreate(m_windowHandle);

	setCreated(TRUE);
	return e_ok;
}

Result NativeWindow::nativeShutdown(void)
{
	if(created())
	{
		wglDeleteContext(m_hglrc);
		DestroyWindow(m_windowHandle);

		sp<NativeEventContext> spNativeEventContext=m_spEventContextI;
		spNativeEventContext->release();
	}
	setCreated(FALSE);
	return e_ok;
}

void NativeWindow::setPointerMotion(BWORD active)
{
}

void NativeWindow::setTitle(const String &title)
{
	if(!SetWindowTextA(m_windowHandle,title.c_str()))
	{
		feX("NativeWindow::setTitle","could not set title");
	}
}

Result NativeWindow::open(const String title)
{
#if FE_WINDOW_EVENT_DEBUG
	feLog("NativeWindow::open(\"%s\")\n",title.c_str());
#endif

	startup();

	setTitle(title);

	ShowWindow(m_windowHandle,SW_SHOW);
	UpdateWindow(m_windowHandle);
	setMapped(TRUE);

	makeCurrent();
	return e_ok;
}

Result NativeWindow::close(void)
{
	setMapped(FALSE);
	return e_ok;
}

void NativeWindow::makeCurrent(void)
{
	HDC hdc=GetDC(m_windowHandle);
	wglMakeCurrent(hdc,m_hglrc);
}

void NativeWindow::releaseCurrent(void)
{
	HDC hdc=GetDC(m_windowHandle);
	wglMakeCurrent(hdc,NULL);
}

void NativeWindow::swapBuffers(void)
{
	HDC hdc=GetDC(m_windowHandle);
	SwapBuffers(hdc);
}

void NativeWindow::setDCPixelFormat(HDC hdc)
	{
	static PIXELFORMATDESCRIPTOR pfd=
		{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,						// version
		PFD_DRAW_TO_WINDOW|
		PFD_SUPPORT_OPENGL|
		PFD_DOUBLEBUFFER,
#if FALSE
		PFD_TYPE_COLORINDEX,
		8,						// color buffer depth
#else
		PFD_TYPE_RGBA,
		24,						// color buffer depth
#endif
		0,0,0,0,0,0,			// ignored
		0,0,					// ignored
		0,0,0,0,0,				// ignored
		32,						// Z buffer depth
		0,						// ignored
		0,						// ignored
		PFD_MAIN_PLANE,
		0,						// ignored
		0,0,0					// ignored
		};

#if NW_SOFTWARE_RENDER
	BWORD softwareRender=TRUE;
	U32 depth=24;
	int nPixelFormat=EnumPixelFormats(hdc,depth,softwareRender);
#else
	int nPixelFormat=ChoosePixelFormat(hdc,&pfd);
#endif

	if(!nPixelFormat)
		feLog("could not choose pixel format\n");

	if(!SetPixelFormat(hdc, nPixelFormat, &pfd))
		feLog("could not set pixel format\n");

	if(!DescribePixelFormat(hdc,nPixelFormat,
			sizeof(PIXELFORMATDESCRIPTOR),&pfd))
		feLog("could not get pixel format\n");
	}

#endif

} /* namespace ext */
} /* namespace fe */
