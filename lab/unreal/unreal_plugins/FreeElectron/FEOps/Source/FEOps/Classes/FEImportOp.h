#pragma once

#include "FEOperator.h"

//* must be last include
#include "FEImportOp.generated.h"

UCLASS(Blueprintable,BlueprintType)
class AFEImportOp: public AFEOperator
{
        GENERATED_UCLASS_BODY()

	protected:

virtual	void	OperatorInit(void) override;
virtual	void	OperatorUpdate(void) override;
virtual	void	UpdateMesh(void) override;

virtual	bool	UpdateConfiguration(fe::String a_configuration);

        UPROPERTY(EditAnywhere, Category="Operator",
				DisplayName="Import Filename")
		FString		m_fsFilename;

        UPROPERTY(EditAnywhere, Category="Operator",
				DisplayName="Model Filename")
		FString		m_fsModelfile;

        UPROPERTY(EditAnywhere, Category="Operator",
				DisplayName="Model Node")
		FString		m_fsModelnode;

		UFUNCTION(BlueprintCallable, Category="Operator")
		bool	Configure(FString configuration);

		fe::String	m_configuration;
};
