/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

WindowController::WindowController(void)
{
	m_init = false;
}

WindowController::~WindowController(void)
{
}

void WindowController::initialize(void)
{
	maskInit("fe_window");
	maskDefault("fe_window_quit",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)113 /* 'q' */,
							WindowEvent::e_statePress));

	maskDefault("fe_window_pause",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)112 /* 'p' */,
							WindowEvent::e_statePress));
}

void WindowController::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	if(!m_init)
	{
		m_init = true;
		sp<Scope> spScope = l_sig->scope();
		m_asOrtho.bind(spScope);
		m_asSignal.bind(spScope);
		m_asSel.bind(spScope);
	}
}

void WindowController::handle(Record &r_sig)
{
	Record r_event = r_sig;
	Record r_windata =m_asSignal.winData(r_event, "WindowController");

	WindowEvent wev;
	wev.bind(r_event);

	if(!maskEnabled(wev)) { return; }

	m_quit = maskGet("fe_window_quit");
	m_pause = maskGet("fe_window_pause");
	m_orthoZoom = maskGet("fe_window_orthoZoom");
	m_orthoZoomUp = maskGet("fe_window_orthoZoomUp");
	m_orthoZoomDown = maskGet("fe_window_orthoZoomDown");
	m_orthoCenter = maskGet("fe_window_orthoCenter");
	m_orthoCenterUp = maskGet("fe_window_orthoCenterUp");
	m_orthoCenterDown = maskGet("fe_window_orthoCenterDown");
	m_orthoCenterLeft = maskGet("fe_window_orthoCenterLeft");
	m_orthoCenterRight = maskGet("fe_window_orthoCenterRight");
	m_orthoRecenter = maskGet("fe_window_orthoRecenter");



	if(!r_windata.isValid()) { return; }

	int &run_level = registry()->master()->catalog()->catalog<int>("run:level");

	if(wev.is(m_quit))
	{
		run_level = 0;
	}

	if(wev.is(m_pause))
	{
		if(run_level == 2)
		{
			run_level = 1;
		}
		else
		{
			run_level = 2;
		}
	}

	if(wev.is(m_orthoZoom))
	{
		if(wev.state() > 0)
		{
			m_asOrtho.zoom(r_windata) /= 1.1;
		}
		else
		{
			m_asOrtho.zoom(r_windata) *= 1.1;
		}
	}

	if(wev.is(m_orthoZoomUp))
	{
		if(!m_asOrtho.zoom.check(r_windata)) { return; }
		m_asOrtho.zoom(r_windata) *= 2.0;
	}

	if(wev.is(m_orthoZoomDown))
	{
		if(!m_asOrtho.zoom.check(r_windata)) { return; }
		m_asOrtho.zoom(r_windata) /= 2.0;
	}

	if(wev.is(m_orthoCenter))
	{
		m_asOrtho.center(r_windata)[0] +=
			m_asSel.end(r_windata)[0] - m_asSel.prev(r_windata)[0];
		m_asOrtho.center(r_windata)[1] -=
			m_asSel.end(r_windata)[1] - m_asSel.prev(r_windata)[1];
	}

	if(wev.is(m_orthoRecenter))
	{
		m_asOrtho.center(r_windata) = Vector2(0.0,0.0);
	}

	if(wev.is(m_orthoCenterUp))
	{
		if(!m_asOrtho.center.check(r_windata)){ return; }
		m_asOrtho.center(r_windata)[1] += 1.0;
	}

	if(wev.is(m_orthoCenterDown))
	{
		if(!m_asOrtho.center.check(r_windata)) { return; }
		m_asOrtho.center(r_windata)[1] -= 1.0;
	}

	if(wev.is(m_orthoCenterLeft))
	{
		if(!m_asOrtho.center.check(r_windata)) { return; }
		m_asOrtho.center(r_windata)[0] -= 1.0;
	}

	if(wev.is(m_orthoCenterRight))
	{
		if(!m_asOrtho.center.check(r_windata)) { return; }
		m_asOrtho.center(r_windata)[0] += 1.0;
	}

}


} /* namespace ext */
} /* namespace fe */

