import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    tests = [   'xEigen' ]

    for t in tests:
        exe = module.Exe(t)

        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xEigen.exe",      "",             None,       None) ]
