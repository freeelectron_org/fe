import sys
import os
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):
    module.summary = []

    opensubdiv_include = os.environ["FE_OPENSUBDIV_INCLUDE"]
    opensubdiv_lib = os.environ["FE_OPENSUBDIV_LIB"]

    # TODO find the native installed version file presumably being used
    version = "/"

    opensubdiv_version_h = opensubdiv_include + "/opensubdiv/version.h"
    if os.path.exists(opensubdiv_version_h):
        for line in open(opensubdiv_version_h).readlines():
            if re.match("#define OPENSUBDIV_VERSION .*", line):
                version = line.split()[2][1:].rstrip()

    if version != "":
        module.summary += [ version ]

    srcList = [ "SurfaceAccessibleOsd",
                "SurfaceAccessorOsd",
                "opensubdiv.pmh",
                "opensubdivDL" ]

    dll = module.DLL( "fexOpenSubdivDL", srcList )

    module.includemap = {}
    module.includemap['opensubdiv'] = opensubdiv_include

    dll.linkmap = {}

    deplibs = forge.corelibs+ [
                "fexSurfaceDLLib" ]

    if forge.fe_os == "FE_LINUX":
        dll.linkmap['opensubdiv'] = "-Wl,-rpath='" + opensubdiv_lib + "'"
        dll.linkmap['opensubdiv'] += ' -L' + opensubdiv_lib + ' -losdCPU'
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        deplibs += [    "fexDataToolLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib" ]

        dll.linkmap["opensubdiv"] = '/LIBPATH:"' + opensubdiv_lib + '"'
        dll.linkmap["opensubdiv"] += " osdCPU.lib"

    forge.deps( ["fexOpenSubdivDLLib"], deplibs )

def auto(module):
    if os.getenv("FE_MS_RT") == "MT":
        return 'TODO Windows MT'

    if forge.codegen == "debug" and (forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64"):
        return "TODO Windows debug"

    opensubdiv_include = os.environ["FE_OPENSUBDIV_INCLUDE"]

    test_file = """
#include <opensubdiv/far/topologyDescriptor.h>

int main(void)
{
    return(0);
}
    \n"""

    # TODO also configurable (as above)
    forge.includemap['opensubdiv'] = opensubdiv_include

    result = forge.cctest(test_file)

    forge.includemap.pop('opensubdiv', None)

    return result
