/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <solve/solve.pmh>

#include "ExplicitInertial.h"

namespace fe
{
namespace ext
{

ExplicitInertial::ExplicitInertial(void)
{
	m_mode = e_verlet;
}

ExplicitInertial::~ExplicitInertial(void)
{
}

void ExplicitInertial::initialize(void)
{
}

void ExplicitInertial::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_hpSignaler = spSignaler;
	m_spScope = l_sig->scope();

	m_asParticle.bind(m_spScope);
	m_asForcePoint.bind(m_spScope);
	m_asTemporal.bind(m_spScope);
	m_asAccumulate.bind(m_spScope);
	m_asClear.bind(m_spScope);
	m_asUpdate.bind(m_spScope);

	if(m_mode == e_rk2)
	{
		m_asRK2.bind(m_spScope);
		m_asForcePoint.enforceHaving(m_asRK2);
	}

}

void ExplicitInertial::accumulate(sp<RecordGroup> rg_input, Real a_timestep)
{
	// TODO: architect substepping
	// TODO: move this clear to a handler called on l_accumulate
	// clear the force accumulators
	// Note that this is on the more general force point, not just particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asForcePoint.force(spRA, i) = zeroVector;
			}
		}
	}

	r_clear = cfg<Record>("clear");

	m_asTemporal.timestep(r_clear) = a_timestep;

	if(!r_clear.isValid())
	{
		feX("ExplicitInertial::handle",
			"invalid clear signal record");
	}
	m_hpSignaler->signal(r_clear);

	r_accum = cfg<Record>("accumulate");

	m_asTemporal.timestep(r_accum) = a_timestep;

	if(!r_accum.isValid())
	{
		feX("ExplicitInertial::handle",
			"invalid accumulate signal record");
	}
	m_hpSignaler->signal(r_accum);
}

void ExplicitInertial::handle(Record &r_sig)
{
	feLog("ExplicitInertial\n");

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	Real timestep = m_asTemporal.timestep(r_sig);

	if(m_mode == e_euler)
	{
		accumulate(rg_input, timestep);

		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{

					SpatialVector v = m_asParticle.velocity(spRA, i);
					v *= timestep;
					m_asParticle.location(spRA, i) += v;

					SpatialVector f = m_asParticle.force(spRA, i);
					f *= timestep/m_asParticle.mass(spRA, i);
					m_asParticle.velocity(spRA, i) += f;
				}
			}
		}
	}

	if(m_mode == e_verlet)
	{
		accumulate(rg_input, timestep);

		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					SpatialVector f = m_asParticle.force(spRA, i);
					f *= 0.5*timestep/m_asParticle.mass(spRA, i);

					SpatialVector v = m_asParticle.velocity(spRA, i);
					v *= timestep;
					v += timestep*f;

					m_asParticle.location(spRA, i) += v;

					m_asParticle.velocity(spRA, i) += f;
				}
			}
		}

		accumulate(rg_input, timestep);

		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					SpatialVector f = m_asParticle.force(spRA, i);
					f *= 0.5*timestep/m_asParticle.mass(spRA, i);

					m_asParticle.velocity(spRA, i) += f;
				}
			}
		}
	}

	if(m_mode == e_foreback)
	{
		accumulate(rg_input, timestep);

		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					SpatialVector f = m_asParticle.force(spRA, i);
					f *= timestep/m_asParticle.mass(spRA, i);
					m_asParticle.velocity(spRA, i) += f;

					SpatialVector v = m_asParticle.velocity(spRA, i);
					v *= timestep;
					m_asParticle.location(spRA, i) += v;
				}
			}
		}
	}

	if(m_mode == e_rk2)
	{
		// modified euler is alpha == 0.5;
		Real alpha = 0.5;
		Real kappa = 1.0 / (2.0 * alpha);

		accumulate(rg_input, timestep);

		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					m_asRK2.l0(spRA, i) = m_asParticle.location(spRA, i);
					m_asParticle.location(spRA, i) +=
						kappa * timestep * m_asParticle.velocity(spRA, i);

					SpatialVector f =
						m_asParticle.force(spRA, i)/m_asParticle.mass(spRA, i);
					m_asRK2.f0(spRA, i) = f;
					m_asRK2.v0(spRA, i) = m_asParticle.velocity(spRA, i);
					m_asParticle.velocity(spRA, i) += kappa * timestep * f;

				}
			}
		}

		accumulate(rg_input, timestep);

		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{


					SpatialVector f =
						m_asParticle.force(spRA, i)/m_asParticle.mass(spRA, i);
					f = (1.0 - alpha) * f +
						alpha * m_asRK2.f0(spRA, i);

					m_asParticle.velocity(spRA, i) =
						m_asRK2.v0(spRA, i) + timestep * f;

					m_asParticle.location(spRA, i) =
						m_asRK2.l0(spRA, i) +
						timestep * m_asParticle.velocity(spRA, i);
				}
			}
		}

	}

	r_update = cfg<Record>("update");

	m_asTemporal.timestep(r_update) = timestep;

	if(!r_update.isValid())
	{
		feX("ExplicitInertial::handle",
			"invalid update signal record");
	}
	m_hpSignaler->signal(r_update);
}


} /* namespace ext */
} /* namespace fe */


