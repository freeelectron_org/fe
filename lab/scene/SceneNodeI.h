/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __scene_SceneNodeI_h__
#define __scene_SceneNodeI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Scene graph node

	@ingroup scene
*//***************************************************************************/
class FE_DL_EXPORT SceneNodeI: virtual public Component
{
	public:
virtual	BWORD			load(String a_filename)								=0;
virtual	U32				childCount(void) const								=0;
virtual	sp<SceneNodeI>	child(U32 a_index) const							=0;

virtual	sp<SceneNodeI>	createChild(void)									=0;
virtual	BWORD			append(sp<SceneNodeI> a_spChild)					=0;
virtual	BWORD			remove(sp<SceneNodeI> a_spChild)					=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __scene_SceneNodeI_h__ */
