/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define SERV_PORT	7876

const char *txt = "hello Roborace!";

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		feLogGroup("netsignal","test UDP echo client\n");

		fe::ext::Socket::startup();
		fe::sp<fe::ext::Socket>		_socket(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

		_socket->openStream(fe::ext::Socket::e_udp);     // Goes first.    
        server_addr.setPort(SERV_PORT);
		server_addr.setAddress("127.0.0.1");
		_socket->connect(server_addr);

		for(int i =0;i<400;++i)
		{
			// Packet header
			long size = htonl(strlen(txt)+1);
			_socket->write((void *)&size, 4);

			// Packet payload
			_socket->write((void *)txt,strlen(txt)+1);
		}
		_socket->close();

	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}
