/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "mechanics.pmh"

#if 0
#include "DrawMechanics.h"
#include "NaiveTireHandler.h"
#include "MechanicsHandler.h"
#include "LinearSpringHandler.h"
#include "LinearSpringDeltaHandler.h"
#include "LinearBendHandler.h"
#include "LinearStrandHandler.h"
#include "StrandTwistHandler.h"
#include "LocatedFrameHandler.h"
#include "EulerFrameHandler.h"
#include "PhysicalFrameHandler.h"
#include "FrameConstraintHandler.h"
#include "ContactHandler.h"
#include "BarycentricPointHandler.h"
#include "PointThrustHandler.h"
#include "SphereSphereCollisionHandler.h"
#include "SPHHandler.h"
#endif

namespace fe
{

Library* CreateMechanicsLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->setName("default_mechanics");

#if 0
	pLibrary->add<DrawMechanics>("HandlerI.DrawMechanics.fe");
	pLibrary->add<NaiveTireHandler>("HandlerI.Tire.Naive.fe");
	pLibrary->add<MechanicsHandler>("HandlerI.Mechanics.fe");
	pLibrary->add<LinearSpringHandler>("HandlerI.LinearSpring.fe");
	pLibrary->add<LinearSpringDeltaHandler>("HandlerI.LinearSpringDelta.fe");
	pLibrary->add<LinearBendHandler>("HandlerI.LinearBend.fe");
	pLibrary->add<LinearStrandHandler>("HandlerI.LinearStrand.fe");
	pLibrary->add<StrandTwistHandler>("HandlerI.StrandTwist.fe");
	pLibrary->add<LocatedFrameHandler>("HandlerI.LocatedFrame.fe");
	pLibrary->add<EulerFrameHandler>("HandlerI.EulerFrame.fe");
	pLibrary->add<PhysicalFrameHandler>("HandlerI.PhysicalFrame.fe");
	pLibrary->add<FrameConstraintHandler>("HandlerI.FrameConstraint.fe");
	pLibrary->add<ContactHandler>("HandlerI.Contact.fe");
	pLibrary->add<ContactRV>("RecordFactoryI.Contact.fe");
	pLibrary->add<BarycentricPointHandler>("HandlerI.BarycentricPoint.fe");
	pLibrary->add<PointThrustHandler>("HandlerI.PointThrust.fe");
	pLibrary->add<SphereSphereCollisionHandler>("HandlerI.SphereSphereCollision.fe");
	pLibrary->add<SPHHandler>("HandlerI.SPH.fe");
#endif

	return pLibrary;
}

}

