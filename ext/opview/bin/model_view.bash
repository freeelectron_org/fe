#! /bin/bash

if ( [[ $# == 0 || $1 == "-h" || $1 == "--help" ]] ) then
	echo "Usage: $0 [-frame FRAME] [-frames START END] [-zup] [-options 'OPTION=\"STRING\"...'] FILES..."
	exit
fi

# wake monitor
#xset dpms force on

if ( [[ ${CODEGEN} == "" ]] ) then
    export CODEGEN=optimize
fi

if ( [[ ${PYTHON} == "" ]] ) then
    export PYTHON=/usr/bin/python3
fi

fullpath=`readlink -f "$0"`

fe_root=`dirname "$fullpath"`/../..
if [ ! -d "${fe_root}/lib" ]; then
	fe_root=$fe_root/..
fi

libs="${fe_root}/lib/x86_64_linux_${CODEGEN}"

#export FE_VERBOSE=all
export FE_EXPOSURE=all
#export PYTHONPATH=${PYTHONPATH}:${fe_root}/ext/terminal
export PYTHONPATH=${PYTHONPATH}:${libs}

#export

# NOTE can break jpeg
#houdini="/opt/hfs13.0.621"
#houdini="/opt/hfs15.0.416"
#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${houdini}/dsolib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HDSO}

maya="/usr/autodesk/maya2015-x64"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${maya}/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${maya}/plug-ins/xgen/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${fe_root}/lib/x86_64_linux_${CODEGEN}

script="${libs}/model_view.py"

if ( [[ $# > 0 && $1 == "-vgs" ]] ) then

	shift
	# generate suppressions
	valgrind								\
		--leak-check=yes					\
		--leak-resolution=low				\
		--show-reachable=yes				\
		--num-callers=50					\
		--suppressions=test/test.supp		\
		--gen-suppressions=yes				\
		$PYTHON $script "$@"

elif ( [[ $# > 0 && $1 == "-vg" ]] ) then

	shift
	# valgrind check
	valgrind								\
		--leak-check=yes					\
		--leak-resolution=low				\
		--show-reachable=yes				\
		--num-callers=50					\
		--suppressions=test/test.supp		\
		--trace-children=yes				\
		$PYTHON $script "$@"

elif ( [[ $# > 0 && $1 == "-vgdb" ]] ) then

	shift
	# valgrind check
	valgrind								\
		--leak-check=yes					\
		--leak-resolution=low				\
		--show-reachable=yes				\
		--num-callers=50					\
		--suppressions=test/test.supp		\
		--vgdb=yes							\
		--vgdb-error=0						\
		$PYTHON $script "$@"

elif ( [[ $# > 0 && $1 == "-hg" ]] ) then

	shift
	# helgrind check
	valgrind								\
		--tool=helgrind						\
		$PYTHON $script "$@"

elif ( [[ $# > 0 && $1 == "-g" ]] ) then

	shift
	gdb --args $PYTHON $script "$@"

else

	$PYTHON "$script" "$@"

fi
