import string
import re
import os
import sys
import shutil
import copy
import filecmp
import platform
import subprocess

import utility

forge = sys.modules["forge"]
sys.path.append(forge.rootPath)
clog = forge.clog
clog.context("import processing of root.py")

import common

config = {}

# NOTE local.env is read in forge.py

###############################################################################
#### GENERAL ##################################################################
config['EXTENSIONS'] = []

if forge.codegen == 'validate':
    config['FE_CODEGEN'] = 'FE_VALIDATE'
    config['_REENTRANT'] = None

if forge.codegen == 'debug':
    config['FE_CODEGEN'] = 'FE_DEBUG'
    config['_REENTRANT'] = None

if forge.codegen == 'profile':
    config['FE_CODEGEN'] = 'FE_PROFILE'

if forge.codegen == 'fast':
    config['FE_CODEGEN'] = 'FE_OPTIMIZE'

if forge.codegen == 'optimize':
    config['FE_CODEGEN'] = 'FE_OPTIMIZE'

forge.basiclibs = [ "fePlatformLib",
                    "feMemoryLib",
                    "feCoreLib" ]

if forge.api == "x86_win32" or forge.api == "x86_win64":
    forge.basiclibs += [ "feImportMemoryLib" ]

forge.corelibs = forge.basiclibs + [
                    "fePluginLib",
                    "feMathLib",
                    "feDataLib" ]

forge.libRoot = os.getenv('FE_LIB')
if not forge.libRoot:
    forge.libRoot=forge.rootPath

forge.objRoot = os.getenv('FE_OBJ')
if not forge.objRoot:
    forge.objRoot=forge.libRoot

config['FE_DOUBLE_REAL'] = '0'
if "FE_DOUBLE_REAL" in os.environ:
    config['FE_DOUBLE_REAL'] = os.environ["FE_DOUBLE_REAL"]

if forge.api == "x86_win32" or forge.api == "x86_win64":
    config['FE_MS_RT'] = 'MD'
    if "FE_MS_RT" in os.environ:
        config['FE_MS_RT'] = os.environ["FE_MS_RT"]
    forge.cprint(forge.CYAN,0,'using MS runtime mode ' + config['FE_MS_RT'])
else:
    config['FE_MS_RT'] = 'NONE'

config['FE_USE_SSE'] = '0'
if "FE_USE_SSE" in forge.fe_config:
    config['FE_USE_SSE'] = forge.fe_config['FE_USE_SSE']

config['FE_FORGE_CXX'] = '"%s"' % forge.cxx

config['FE_USE_VISIBILITY'] = '0'
if "FE_USE_VISIBILITY" in os.environ:
    config['FE_USE_VISIBILITY'] = os.environ["FE_USE_VISIBILITY"]

config['FE_MT'] = '1'
if "FE_MT" in os.environ:
    config['FE_MT'] = os.environ["FE_MT"]

config['FE_USE_PRINTF'] = '1'
if "FE_USE_PRINTF" in os.environ:
    config['FE_USE_PRINTF'] = os.environ["FE_USE_PRINTF"]

config['FE_RTTI'] = '1'
if "FE_RTTI" in os.environ:
    config['FE_RTTI'] = os.environ["FE_RTTI"]
forge.rtti = int(config['FE_RTTI'])

config['FE_TYPEID'] = config['FE_RTTI']
if int(config['FE_TYPEID']) and "FE_TYPEID" in os.environ:
    config['FE_TYPEID'] = os.environ["FE_TYPEID"]

config['FE_RTTI_HOMEBREW'] = '1'
if "FE_RTTI_HOMEBREW" in os.environ and forge.rtti:
    config['FE_RTTI_HOMEBREW'] = os.environ["FE_RTTI_HOMEBREW"]

if forge.pretty < 2:
    if not int(config['FE_RTTI_HOMEBREW']):
        if not forge.rtti:
            forge.cprint(forge.RED,0,'incorrectly configured RTTI')
        else:
            forge.cprint(forge.CYAN,0,'using compiler based RTTI')
    elif not forge.rtti:
        forge.cprint(forge.CYAN,0,'using homebrew RTTI without compiler support')
    else:
        forge.cprint(forge.CYAN,0,
                'using homebrew RTTI with compiler support left on')
    if int(config['FE_TYPEID']):
        forge.cprint(forge.CYAN,0,'using compiler based typeid')
    else:
        forge.cprint(forge.CYAN,0,'using signature type id')

config['FE_USE_TEMPLATE_STATIC'] = '0'
if forge.compiler_brand != 'clang' and "FE_USE_TEMPLATE_STATIC" in os.environ:
    use_template_static = os.environ["FE_USE_TEMPLATE_STATIC"]
    if use_template_static:
        if forge.api != "x86_win32" and forge.api != "x86_win64":
            config['FE_USE_TEMPLATE_STATIC'] = os.environ["FE_USE_TEMPLATE_STATIC"]
        elif forge.pretty < 2:
            forge.cprint(forge.YELLOW,0,'FE_USE_TEMPLATE_STATIC not allowed on Windows')

forge.alignment = None
if "FE_MEM_ALIGNMENT" in os.environ:
    forge.alignment = os.getenv('FE_MEM_ALIGNMENT')
    forge.cppmap['alignment'] = '-DFE_MEM_ALIGNMENT=' + forge.alignment
    forge.cprint(forge.YELLOW,0,'memory alignment set to ' + forge.alignment)

#pcre_check = os.popen('pcre-config --version','r')
#pcre_version = pcre_check.read()
#pcre_check.close()
#if pcre_version == "":
#   pcre_version = 0
#   forge.cprint(forge.CYAN,0,'PCRE version undetermined')
#else:
#   pcre_version = float(pcre_version)
#   forge.cprint(forge.CYAN,0,'PCRE version ' + str(pcre_version))
#
#if "FE_BOOST_REGEX" in os.environ and int(os.environ["FE_BOOST_REGEX"]):
#   forge.cprint(forge.CYAN,0,'using boost regular expressions')
#   forge.fe_re = 'FE_RE_BOOST'
#elif os.path.exists('/usr/include/pcrecpp.h') and pcre_version >= 0:
#   forge.cprint(forge.CYAN,0,'using PCRE regular expressions')
#   forge.fe_re = 'FE_RE_PCRE'
#else:
#   forge.cprint(forge.CYAN,0,'using no regular expressions')
#   forge.fe_re = 'FE_RE_NONE'

###############################################################################
#### PLATFORMS ################################################################
common.platform_setup()

if forge.cpp_11:
    config['FE_UNORDERED_MAP'] = None
    if forge.pretty < 2:
        forge.cprint(forge.CYAN,0,'using unordered_map')

elif forge.api[-6:] == "_linux" or forge.api[-4:] == "_osx":
    config['GNU_STL_HASH_MAP'] = None
    if forge.pretty < 2:
        forge.cprint(forge.YELLOW,0,'using hash_map')

elif forge.fe_os == "FE_CYGWIN":
    config['GNU_STL_HASH_MAP'] = None

elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
    pass

###############################################################################
#### COMPILERS ################################################################
common.compiler_setup()

config['FE_OS'] = forge.fe_os
config['FE_OSVER'] = forge.fe_osver
config['FE_COMPILER'] = forge.fe_compiler
config['FE_2DGL'] = forge.fe_gl2d
config['FE_3DGL'] = forge.fe_gl3d
config['FE_HW'] = forge.fe_hw
#config['FE_RE'] = forge.fe_re

#forge.libPath = os.path.join(forge.libRoot, 'lib', forge.apiCodegen)
forge.libPath = os.environ["FE_LIB_PATH"]

forge.objPath = os.path.join(forge.objRoot, 'obj', forge.apiCodegen)
forge.binPath = os.path.join(forge.rootPath, 'bin', forge.api)
forge.srcPath = os.path.join(forge.rootPath, 'src')

#forge.includemap["master_headers"] = "fe"

forge.includemap["root"] = forge.srcPath

###############################################################################
#### BOOST ####################################################################
#common.setup_boost()

############################################################################
# print project build header
#forge.cprint(forge.CYAN,0,'compiler = ' + forge.fe_compiler)

if forge.pretty < 2:
    forge.cprint(forge.CYAN,0,'Python ' + sys.version)

print
forge.cprint(forge.YELLOW,1,'Free Electron')

forge.cprint(forge.CYAN,0,'building ' + forge.apiCodegen)

###############################################################################
#### LOCAL TWEAKS #############################################################

# using os.environ["FE_WINDOWS_LOCAL"]
if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
    windows_local = os.environ["FE_WINDOWS_LOCAL"]

    # Boost
    # check for highest version of each
#   boost_install = None
#   if forge.fe_os == 'FE_WIN64':
#       ex_boost = re.compile("^boost_[0-9_]*x64$")
#   else:
#       ex_boost = re.compile("^boost_[0-9_]*$")
#   if os.path.exists(windows_local):
#       dir_list = sorted(os.listdir(windows_local),reverse=True)
#       for entry in dir_list:
#           if boost_install == None and ex_boost.match(entry):
#               boost_candidate = windows_local + "/" + entry
#               if os.path.exists(boost_candidate + "/boost/version.hpp"):
#                   boost_install = boost_candidate
#                   print("using %s" % boost_install)
    # for dynamic_bitset, tokenizer, and exception/diagnostic_information
#   if boost_install:
#       forge.includemap['boost'] = boost_install
        # header only, libs not needed
#           forge.linkmap['boost'] = "/LIBPATH:" + boost_install + "/lib"

    # search for GLEW
    if False:
        # check for highest version of each
        glew_install = None
        forge.glew_dll = None

        glew_install = "ext/glew/glew-2.1.0"

#       ex_glew = re.compile("^glew-")
#       if os.path.exists(windows_local):
#           dir_list = sorted(os.listdir(windows_local),reverse=True)
#           for entry in dir_list:
#               if glew_install == None and ex_glew.match(entry):
#                   glew_candidate = windows_local + "/" + entry
#                   if os.path.exists(glew_candidate + "/include/GL/glew.h"):
#                       glew_install = glew_candidate
#                       print("using %s" % glew_install)

        if glew_install:
            glewLib = glew_install + "/lib/"
            glewBin = glew_install + "/bin/"
            if forge.codegen == 'debug':
                glewLib += "Debug/"
                glewBin += "Debug/"
            else:
                glewLib += "Release/"
                glewBin += "Release/"
            if forge.fe_os == "FE_WIN64":
                glewLib += "x64"
                glewBin += "x64"
            else:
                glewLib += "Win32"
                glewBin += "Win32"
#           if forge.codegen == 'debug':
#               forge.glew_dll = glewBin + "/glew32d.dll"
#           else:
#               forge.glew_dll = glewBin + "/glew32.dll"
            forge.includemap['glew'] = glew_install + "/include"
            forge.linkmap['glew'] = "/LIBPATH:" + glewLib

    # -------------------------------------------------------------------------
    # VCPKG detection/configuration
    # see https://github.com/Microsoft/vcpkg/

    fe_vcpkg_install = os.path.normpath(os.environ['FE_VCPKG_INSTALL'])
    fe_vcpkg_triplet_active = os.environ['FE_VCPKG_TRIPLET_ACTIVE']

    vcpkg = ""

#   OLD version
#   runtime = "-md" if os.environ["FE_MS_RT"] == "MD" else ""
#   if forge.fe_os == "FE_WIN32":
#       vcpkg = os.path.join(fe_vcpkg_install, "installed", "x86-windows-static" + runtime)
#   elif forge.fe_os == "FE_WIN64":
#       vcpkg = os.path.join(fe_vcpkg_install, "installed", "x64-windows-static" + runtime)

    host = fe_vcpkg_triplet_active[:3]
    compatible = {
        'FE_WIN32': 'x86',
        'FE_WIN64': 'x64',
    }
    if (host != compatible.get(forge.fe_os, host)):
        clog.error(
            "Incompatibility detected between OS declaration "
            f"({forge.fe_os}) and the active vcpkg triplet "
            f"({fe_vcpkg_triplet_active})"
        )

    vcpkg = fe_vcpkg_install

    if os.path.exists(vcpkg):
        forge.vcpkg_include = os.path.join(vcpkg, "include")
        if not os.path.exists(forge.vcpkg_include):
            clog.warning(
                f"vcpkg include doesn't exist: '{forge.vcpkg_include}'")
            forge.vcpkg_include = ""

        if forge.codegen == 'debug':
            forge.vcpkg_lib = os.path.join(vcpkg, "debug", "lib")
        else:
            forge.vcpkg_lib = os.path.join(vcpkg, "lib")
        if not os.path.exists(forge.vcpkg_lib):
            clog.warning(f"vcpkg lib doesn't exist: '{forge.vcpkg_lib}'")
            forge.vcpkg_lib = ""
    else:
        clog.warning(f"vcpkg install doesn't exist: '{vcpkg}'")

    if forge.vcpkg_include and forge.vcpkg_lib:
        forge.includemap["vcpkg"] = forge.vcpkg_include
        forge.linkmap["vcpkg"] = "/LIBPATH:" + forge.vcpkg_lib
        clog.info(f"using vcpkg bundle: {vcpkg}")
    else:
        clog.warning("not using vcpkg")

    # end VCPKG stuff
    # -------------------------------------------------------------------------

thread_order = utility.env_list("FE_THREAD_ORDER")
mutex_order = utility.env_list("FE_MUTEX_ORDER")
regex_order = utility.env_list("FE_REGEX_ORDER")

forge.cppmap['thread_order'] = '-DFE_THREAD_ORDER=' + ':'.join(thread_order)
forge.cppmap['mutex_order'] = '-DFE_MUTEX_ORDER=' + ':'.join(mutex_order)
forge.cppmap['regex_order'] = '-DFE_REGEX_ORDER=' + ':'.join(regex_order)

# LD_LIBRARY_PATH
def prepend_to_ld_lib_path(ld_lib_path):
    old_ld_lib_path = os.getenv('LD_LIBRARY_PATH')
    if old_ld_lib_path:
        ld_lib_path += ':' + old_ld_lib_path
    os.environ['LD_LIBRARY_PATH'] = ld_lib_path

prepend_to_ld_lib_path(forge.libPath)

# PYTHONPATH
old_pythonpath = os.getenv('PYTHONPATH')
pythonpath = forge.libPath
if old_pythonpath:
    pythonpath += ':' + old_pythonpath
os.environ['PYTHONPATH'] = pythonpath

# INCLUDEPATH
if 'INCLUDEPATH' in os.environ:
    forge.cprint(forge.CYAN,0,'INCLUDEPATH:')
    include_path = os.environ['INCLUDEPATH'].split(':')
    for include in include_path:
        forge.cprint(forge.CYAN,0,'  ' + include)
        forge.includemap['~_' + include] = include
#else:
#   forge.cprint(forge.CYAN,0,'INCLUDEPATH not set')

# BOOST VERSION
#forge.cprint(forge.CYAN,0,'BOOST_VERSION: ' + forge.boost_version)

# set HASH_MAP_IS_MAP to force ext::hash_map to be std::map
if os.getenv('HASH_MAP_IS_MAP'):
    config['HASH_MAP_IS_MAP'] = None
    forge.cprint(forge.CYAN,0,'HASH_MAP_IS_MAP set')
# automatically done for win32
if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
    config['HASH_MAP_IS_MAP'] = None

############################################################################
test_file = """
#include <string.h>
int main(void)
{
    char s[] = "hello";
    memrchr((void *)s, 'e', 5);
    return (0);
}
\n"""

result = forge.cctest(test_file)

if result:
    forge.cprint(forge.YELLOW,1,"memrchr() not available")
else:
    config['HAS_MEMRCHR'] = None
    forge.cprint(forge.CYAN,1,"memrchr() available")

############################################################################
# config.h
#configname=os.path.join(forge.srcPath, 'fe','config.h')
config_dir = os.path.join(forge.libPath, 'fe')
if not os.path.exists(config_dir):
    os.makedirs(config_dir)
configname=os.path.join(config_dir, 'config.h')
forge.includemap['headers_in_lib'] = os.path.join(forge.libPath)
config_header = forge.FileTarget(configname)
config_header.AddDep(forge.targetRegistry['ROOT_BUILD_FILE'])
forge.targetRegistry['root'].AddDep(config_header)
def config_text():
    txt = ""
    txt += "/* Free Electron Configuration File */\n"
    keylist = sorted(list(config.keys()))
    for define in keylist:
        txt += '\n#ifndef %s\n' % (define)
        if config[define] == None:
            txt += '#define %s\n' % (define)
        else:
            txt += '#define %-30s %s\n' % (define, config[define])
        txt += '#endif\n'
    return txt

def generate_config(target):
    forge.cprint(forge.WHITE,0,'generating config file')
    f = open(target.name, "w")
    txt = config_text()
    f.write(txt)
    f.close()

# if it does not exist at all, just build it.  Dependencies do not generate
# off of the header files to not existent headers, so we need to force it here
if not os.path.exists(configname):
    generate_config(config_header)
# if it changed content, build it
else:
    fp = open(configname)
    txt0 = fp.read()
    fp.close()
    txt1 = config_text()
    if txt0 != txt1:
        generate_config(config_header)

#configname="(.*)config.h"
configname = os.path.join(forge.srcPath,'fe','config.h')
forge.defaultContext.AppendRule(configname, generate_config)

############################################################################
# attribute_define.h
#attrtxtname=os.path.join(forge.rootPath, 'doc', 'attribute_names.txt')
#attrtxt = forge.FileTarget(attrtxtname)
#attrdefname=os.path.join(config_dir, 'attribute_define.h')
#attrdef_header = forge.FileTarget(attrdefname)
#attrdef_header.AddDep(attrtxt)
#forge.targetRegistry['root'].AddDep(attrdef_header)
#forge.targetRegistry['root'].AddDep(attrtxt)
#def generate_attrdef(target):
#   forge.cprint(forge.WHITE,0,'generating attribute_define file')
#   attrdef_cmd = os.path.join(forge.rootPath, 'doc', 'attribute_define.py')
#   attrdef_cmd += ' %s %s' % (attrtxtname, attrdefname)
#   os.system(attrdef_cmd)

#if not os.path.exists(attrdefname):
#   generate_attrdef(attrdef_header)

#attrdefnm="(.*)attribute_define.h"
#forge.defaultContext.AppendRule(attrdefnm, generate_attrdef)


############################################################################

def python_build_name():
    v = sys.version_info
    return platform.platform() + "-" + str(v[0]) + "." + str(v[1])

# 20130928 jweber deactivated
if False:# forge.fe_os == "FE_LINUX":
    forge.cprint(forge.CYAN,0,'effective LD_LIBRARY_PATH:')
    ld_lib_path = os.environ['LD_LIBRARY_PATH'].split(':')
    forge.linkmap['LD_LIBRARY_PATH'] = ''
    for l in ld_lib_path:
        forge.linkmap['LD_LIBRARY_PATH'] += ' -L' + l
        if os.path.exists(l):
            forge.cprint(forge.CYAN,0,'  ' + l)
        else:
            forge.cprint(forge.RED,0,'  ' + l + '  [does not exist]')

def scrub_tree(arg,dirname,names):
    # removals
    extensions = [ 'pyc' ]
    for ext in extensions:
        for filename in names:
            if re.compile(r'.*\.%s$' % (ext)).match(filename):
                fullname = os.path.join(dirname,filename)
                forge.cprint(forge.YELLOW,0,'removing: '+ fullname)
                os.remove(fullname)
    # warnings
    extensions = [ 'swp' ]
    for ext in extensions:
        for filename in names:
            if re.compile(r'.*\.%s$' % (ext)).match(filename):
                fullname = os.path.join(dirname,filename)
                forge.cprint(forge.YELLOW,0,'warning: '+ fullname)


class ScrubTarget(forge.Target):
    def __init__(self,str):
            forge.Target.__init__(self,str)
            self.help = """\
Scrub transient files in a sledge hammer fashion (the 'clean' target
does so in a more surgical fashion.)

However, this is probably more analagous to 'make clean', so if that is what
you really want then this is your target.
"""

    def Build(self, reference_time):
        directories = [ forge.objPath, forge.libPath ]
        for dir in directories:
            if os.path.exists(dir) and os.path.isdir(dir):
                forge.cprint(forge.RED,0,'removing ' + dir)
                shutil.rmtree(dir)

        forge.cprint(forge.RED,0,'removing .pyc files')
        os.walk(forge.rootPath,scrub_tree,None)

        fullname = os.path.join(forge.rootPath,'test','log.txt')
        if os.path.exists(fullname):
            forge.cprint(forge.YELLOW,0,'removing: '+ fullname)
            os.remove(fullname)

        fullname = os.path.join(forge.srcPath,'fe','config.h')
        if os.path.exists(fullname):
            forge.cprint(forge.YELLOW,0,'removing: '+ fullname)
            os.remove(fullname)

        cwd = os.getcwd()
        os.chdir(forge.rootPath)
        os.system('svn status')
        forge.cprint(forge.WHITE,1,'x86_win32:')
        os.system('svn propget x86_win32 .')
        forge.cprint(forge.WHITE,1,'x86_linux:')
        os.system('svn propget x86_linux .')
        os.chdir(cwd)

scrubTarget = ScrubTarget("scrub")
forge.targetRegistry['scrub'] = scrubTarget

fe_help = """\
Free Electron specific help

FE Build Environment Variables:
FE_ROOT               - root directory of FE distribution
FE_LIB                - specify a particular directory for lib files
FE_OBJ                - specify a particular directory for object files
HASH_MAP_IS_MAP       - use std::map in place of hash_map
HASH_MAP_IS_MAP       - use std::map in place of hash_map
BOOST_INCLUDE         - location of the boost header directory
BOOST_LIB             - location of the boost header directory
BOOST_STRUCTURE       - (install or bin-stage)
BOOST_VERSION         - boost version (such as 1_31)
FE_CC                 - compiler
FE_UNIT               - enable automatic unit tests
FE_USE_SSE            - turn on SSE optimizations, if available
FE_USE_PCH            - turn on pre-compiled headers, if available
FE_E_BACKTRACE        - at runtime, backtrace FE exception throw (default 0)
FE_UNCAUGHT_BACKTRACE - at runtime, backtrace seg fault (default 1)
FE_SEGV_BACKTRACE     - at runtime, backtrace seg fault (default 1)
FE_E_ATTACH           - at runtime, attach to debugger on segfault (default 0)
FE_E_ASSERT           - causes an assert in place of an exception
"""

forge.help.append(fe_help)


def dummy_execute(command,name):
    print(command)

class PretendTarget(forge.Target):
    def __init__(self,str):
        forge.Target.__init__(self,str)
        self.help = """\
Print out commands to be executed, but do not actually execute them.
"""
    def Build(self, reference_time):
        forge.execute = dummy_execute

pretendTarget = PretendTarget("pretend")
forge.targetRegistry['pretend'] = pretendTarget

def dot_build(self, reference_time):
    if self.buildNum >= forge.buildNum:
        return self.LastBuild()
    self.buildNum = forge.buildNum

    for dep in self.deps:
        (s_h,s_t) = os.path.split(self.name)
        (d_h,d_t) = os.path.split(dep.name)
        forge.dotfile.write('"%s" -> "%s"\n' % (s_t,d_t))
        if forge.recursionDepth < 0 or forge.recursionLevel < forge.recursionDepth:
            forge.recursionLevel += 1
            dep.Build(reference_time)

class DotFile:
    def __init__(self):
        self.file = file('out.dot', "w")
        self.file.write('digraph {\n')
    def __del__(self):
        self.file.write('}\n')
        self.file.close()
        import os
        os.system("dot -Tps out.dot -o out.ps")
    def write(self, s):
        self.file.write(s)

class DotTarget(forge.Target):
    def __init__(self,str):
        forge.Target.__init__(self,str)
        self.help = """\
Generate dep.dot, a graph of build dependencies
"""
    def Build(self, reference_time):
        forge.dotfile = DotFile()
        forge.Target.Build = dot_build

dotTarget = DotTarget("dot")
forge.targetRegistry['dot'] = dotTarget

def copy_headers(arg,dirname,names):
    (base_dir, include_dir) = arg
    re_compile = re.compile(r'.*\.(h|pmh)$')
    rel_path = forge.RelativePath(base_dir,dirname)
    if not rel_path:
        return
    target_dir = os.path.join(include_dir, rel_path)
    if not os.path.exists(target_dir):
        try:
            os.makedirs(target_dir)
        except OSError:
            print("failed creating " + target_dir)
    for filename in names:
        if re_compile.match(filename):
            src = os.path.join(dirname, filename)
            dst = os.path.join(target_dir, filename)
            print("%s -> %s" % (src,dst))
            shutil.copyfile(src,dst)

class IncludeTarget(forge.Target):
    def __init__(self,str):
        forge.Target.__init__(self,str)
        self.help = """\
Extract header files to include directory
"""
    def Build(self, reference_time):
        include_dir = os.path.join(forge.rootPath, 'include')
        for src in ['src', 'ext']:
            src_path = os.path.join(forge.rootPath, src)
            os.walk(src_path,copy_headers,(src_path, include_dir))

includeTarget = IncludeTarget("include")
forge.targetRegistry['include'] = includeTarget


############################################################################
# xfig Image conversion support
def search_file(filename):
    found = 0
    os_path = os.getenv('PATH')
    if not os_path:
        os_path = os.defpath
    paths = os_path.split(os.pathsep)
    for path in paths:
        if os.path.exists(os.path.join(path, filename)):
            found = 1
            break
    if found:
        return os.path.abspath(os.path.join(path, filename))
    else:
        return None

def fig2jpg(target):
    if not forge.fig2dev:
        return
    forge.cprint(forge.WHITE,0,'jpg: fig2dev %s' % forge.RelativePath(forge.rootPath,target.name))
    prog = search_file('fig2dev')
    if prog:
        forge.confirm_path(target)
        command = "%s -L jpeg -S 4 -q 100 %s > %s" % (prog, target.deps[0].name,target.name)
        os.system(command)
    else:
        forge.cprint(forge.RED,0,'fig2dev not found')

def fig2eps(target):
    if not forge.fig2dev:
        return
    forge.cprint(forge.WHITE,0,'eps: fig2dev %s' % forge.RelativePath(forge.rootPath,target.name))
    prog = search_file('fig2dev')
    if prog:
        forge.confirm_path(target)
        command = "%s -L eps %s > %s" % (prog, target.deps[0].name,target.name)
        os.system(command)
    else:
        forge.cprint(forge.RED,0,'fig2dev not found')

def Module_Image(self, name, silent=False):
    if not forge.fig2dev:
        if not silent:
            forge.color_on(0, forge.YELLOW)
            sys.stdout.write('not generating images\n')
            forge.color_off()
        return
    src_name = os.path.join(self.modPath,name + ".fig")
    jpg_name = os.path.join(forge.rootPath,'doc','image',name + ".jpg")
    eps_name = os.path.join(forge.rootPath,'doc','image',name + ".eps")
    src = forge.FileTarget(src_name)
    jpg = forge.FileTarget(jpg_name)
    eps = forge.FileTarget(eps_name)
    jpg.AddDep(src)
    eps.AddDep(src)
    self.AddDep(jpg)
    self.AddDep(eps)
    dox = forge.targetRegistry['documentation']
    dox.AddDep(jpg)
    dox.AddDep(eps)
    return (jpg, eps)

forge.defaultContext.AppendRule(r"(.*)\.(jpg)", fig2jpg)
forge.defaultContext.AppendRule(r"(.*)\.(eps)", fig2eps)

forge.Module.Image = Module_Image

##############################################################################
# prettify stl output
def stl_gcc_decrypt(raw):
    return raw

    command = 'perl ' + os.path.join(forge.rootPath,'bin','gSTLFilt.pl')

    # TODO: refactor for python 3 idioms if ever resurrected.
    p = subprocess.Popen(forge.cleanup_command(command), shell=True, bufsize=-1,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    p.stdin.write(raw)
    p.stdin.close()
    cooked = p.stdout.read()
    n = p.stdout.close()

    return cooked.rstrip()

def colorize_filenames(raw):
    re_c = re.compile(r'^(|.*\s)(\S+\.(cc|h|c|pmh)):(\d*)(.*)$')
    lines = str(raw).split('\n')
    cooked = ''
    for l in lines:
        m = re_c.match(l)
        if m:
            cooked += m.group(1)
            cooked += forge.ansi(forge.BOLD)
            cooked += forge.ansi(forge.CYAN)
            cooked += m.group(2)
            cooked += forge.ansi(forge.NORMAL) + ':' + forge.ansi(forge.WHITE)
            cooked += m.group(4)
            cooked += forge.ansi(forge.NORMAL)
            cooked += m.group(5)
            cooked += '\n'
        else:
            cooked += l + '\n'
    return cooked.rstrip()

def colorize_errors(raw):
    error = forge.ansi(forge.BOLD) + forge.ansi(forge.RED) + 'error' + forge.ansi(forge.NORMAL)
    cooked = raw.replace('error', error)
    return cooked

def colorize_warnings(raw):
    warning = forge.ansi(forge.BOLD) + forge.ansi(forge.YELLOW) + 'warning' + forge.ansi(forge.NORMAL)
    cooked = raw.replace('warning', warning)
    return cooked

if forge.fe_os == "FE_LINUX":
    # gSTLFilt.pl breaks on gcc 3.4
    if forge.cxx == "g++":
        forge.outputContext.AppendRule(r"(.*)\.(obj|o|gch)", stl_gcc_decrypt)
    forge.outputContext.AppendRule(r"(.*)\.(obj|o|gch)", colorize_filenames)
    forge.outputContext.AppendRule(r"(.*)\.(obj|o|gch)", colorize_errors)
    forge.outputContext.AppendRule(r"(.*)\.(obj|o|gch)", colorize_warnings)

##############################################################################

def bld_exists(rel_path):
    return os.path.exists(os.path.join(forge.rootPath, rel_path, 'bld.py'))

modset_list = []
modset_root_list = forge.modset_roots.split(':')
for modset_root in modset_root_list:
    modset_root_path = os.path.join(forge.rootPath, modset_root)
    if os.path.isdir(modset_root_path):
        bld_py_path = os.path.join(modset_root_path, 'bld.py')
        if os.path.exists(bld_py_path):
            # as explicit path
            modset_list.append(modset_root)
        else:
            # as parent path
            dir_list = sorted(os.listdir(modset_root_path))
            for modset in dir_list:
                if not modset in modset_list:
                    modset_path = os.path.join(modset_root_path, modset)
                    if os.path.isdir(modset_path):
                        bld_py_path = os.path.join(modset_path, 'bld.py')
                        if os.path.exists(bld_py_path):
                            if modset_root == ".":
                                modset_list.append(modset)
                            else:
                                modset_list.append(os.path.join(modset_root, modset))

modset_first = [ "src", "ext", "lab" ]
modset_first.reverse()
for modset_early in modset_first:
    for modset_path in modset_list:
        modset = os.path.basename(modset_path)
        if modset == modset_early:
            modset_list.remove(modset_path)
            modset_list.insert(0, modset_path)

modset_last = [ "test", "doc" ]
for modset_late in modset_last:
    for modset_path in modset_list:
        modset = os.path.basename(modset_path)
        if modset == modset_late:
            modset_list.remove(modset_path)
            modset_list.append(modset_path)

forge.tests = []

ext_string = os.getenv('FE_EXT')
if ext_string == None:
    ext_string = forge.extensions
if ext_string == None:
    extPath = os.path.join(forge.rootPath,'ext')
    ext_string = ":".join(sorted(os.listdir(extPath)))

if ext_string == "":
    candidates = []
else:
    candidates = ext_string.split(':')

# filter candidates to evident paths on disk
forge.modules_found = []
for modset_path in modset_list:
    modset_root = os.path.dirname(modset_path)
    modset = os.path.basename(modset_path)
    if bld_exists(modset_path):
        for candidate in candidates:
            if not candidate in forge.modules_found:
                if bld_exists(os.path.join(modset_root, modset, candidate)):
                    forge.modules_found.append(candidate)

missing = []
for candidate in candidates:
    if not candidate in forge.modules_found:
        missing += [ candidate ]

forge.cprint(forge.CYAN,0,'producing "' + forge.build_product + '"')

if len(missing):
    forge.color_on(0, forge.RED)
    sys.stdout.write("missing modules ")
    forge.color_on(0, forge.YELLOW)
    sys.stdout.write(" ".join(missing) + "\n")
    forge.color_off()

    print("FE_EXTENSIONS=" + ext_string)

forge.dot_modules_confirmed = []
forge.modules_confirmed = []
forge.modules_created = []
forge.modules_failed = []
forge.module_dictionary = {}

forge.media = None
mediaPath = '../media'
if os.path.exists(mediaPath):
    forge.media = mediaPath
    if forge.pretty < 2:
        forge.cprint(forge.CYAN, 0, f"using media path '{forge.media}'")
elif forge.pretty < 2:
    forge.cprint(forge.YELLOW, 0, 'not using media', eol=None)
    forge.cprint(forge.CYAN, 0, ' (not found)')

# Check if we should do a hotreload build
forge.hotreload_manifest = forge.HotReloadManifest()
if forge.ShouldHotReload():
    forge.cprint(forge.MAGENTA, 0,
        "hotreload build detected (building dll's only)")
    forge.hotreload_mode = True
else:
    forge.hotreload_mode = False
    if forge.hotreload_manifest.Exists():
        forge.cprint(forge.MAGENTA, 0, "cleaning up hotreload files")
        forge.hotreload_manifest.Cleanup()

total = len(modset_list)
for i, modset_path in enumerate(modset_list, start=1):

    modset = os.path.basename(modset_path)

    clog.context(f"setting up modset: {modset} ({i}/{total})")

    if bld_exists(modset_path):
        forge.setup(None, modset_path)

# don't trigger rebuild on autoload unless manifests changed
manifests = os.path.join(forge.rootPath, 'src/autoload/manifests.cc')
manifests_new = os.path.join(forge.rootPath, 'src/autoload/manifests_new.cc')
if not os.path.exists(manifests) or not filecmp.cmp(manifests_new,manifests):
    shutil.copyfile(manifests_new,manifests)
os.remove(manifests_new)

# shouldn't this be in test/bld.py?
if bld_exists('test'):
    test_module = forge.setup(None, "test")
    if test_module and "FE_UNIT" in os.environ and os.environ["FE_UNIT"] == "1":
        forge.dep(test_module, "unit")

# is this still something?
if bld_exists('app'):
    lab_module = forge.setup(None, "app")

if len(forge.modules_failed):
    forge.cprint(forge.RED, 1,
        "Modules Failed ({cnt}): {names}".format(
            cnt=len(forge.modules_failed),
            names=' '.join(forge.modules_failed),
        )
    )

    if os.environ["FE_REQUIRE_ALL_MODULES"] != "0":
        sys.exit(1)
