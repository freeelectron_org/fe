import sys
import os
import re

import utility
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def setup(module):
    module.summary = []

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")

    fbx_sources = utility.safer_eval(os.environ["FE_FBX_SOURCES"])

    for fbx_source in reversed(sorted(fbx_sources)):
        fbx_source_version = fbx_source[0]
#       fbx_source_suffix = fbx_source[1]
        fbx_source_inc = fbx_source[2]
        fbx_source_libs = fbx_source[3]

        if not os.path.isdir(fbx_source_libs):
            module.summary += [ "-" + fbx_source_version ]
            continue

        compilers = os.listdir(fbx_source_libs)

        if len(compilers)<1:
            module.summary += [ "-" + fbx_source_version ]
            continue

        fbx_source_libs = fbx_source_libs + "/" + compilers[0]

        if forge.api == "x86_win32":
            fbx_source_libs = fbx_source_libs + "/x86"
        else:
            fbx_source_libs = fbx_source_libs + "/x64"

        if forge.codegen == "debug":
            fbx_source_libs = fbx_source_libs + "/debug"
        else:
            fbx_source_libs = fbx_source_libs + "/release"

        if not os.path.exists(fbx_source_inc) or not os.path.exists(fbx_source_libs):
            module.summary += [ "-" + fbx_source_version ]
            continue

        if fbx_source_version == "":
            module.summary += [ "/" ]
        else:
            module.summary += [ fbx_source_version ]

        srcList = [ "SurfaceAccessibleFbx",
                    "fbx.pmh",
                    "fbxDL" ]

        variant = fbx_source_version
        if variant != "":
            variantPath = module.modPath + '/' + variant
            if os.path.lexists(variantPath) == 0:
                os.symlink('.', variantPath)

            srcListVariant = []
            for src in srcList:
                srcListVariant += [ variant + '/' + src ]
            srcList = srcListVariant

        fbx_version = variant
        fbx_version_path = fbx_source_inc + '/FBX/FbxCoreAbstract/Foundation.h'
        if os.path.exists(fbx_version_path):
            for line in open(fbx_version_path):
                if 'ALEMBIC_LIBRARY_VERSION ' in line:
                    fbx_version = line.split(' ')[2].rstrip()
                    break

        if fbx_version != variant:
            module.summary += [ "=" + fbx_version ]

        dllname = "fexFbxDL" + variant

        with open(manifest, 'a') as outfile:
            suffix = ""
            if variant != "":
                suffix = "." + variant
            outfile.write('\tspManifest->catalog<String>(\n'+
                    '\t\t\t"SurfaceAccessibleI.SurfaceAccessibleFbx.fe.fbx'+
                    suffix+'")=\n'+
                    '\t\t\t"fexFbxDL'+variant+'";\n');

        dll = module.DLL( dllname, srcList )

        for src in srcList:
            srcTarget = module.FindObjTargetForSrc(src)
            srcTarget.includemap = { 'fbx' : fbx_source_inc }

        dll.linkmap = { "fbx" : "" }

        if forge.fe_os == "FE_LINUX":
            dll.linkmap["fbx"] += " -Wl,-rpath='" + fbx_source_libs + "/'"
            dll.linkmap["fbx"] += " -L" + fbx_source_libs
            dll.linkmap["fbx"] += " -lfbxsdk"
            dll.linkmap["fbx"] += " -lxml2"
        elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
            dll.linkmap["fbx"] += '"/LIBPATH:' + fbx_source_libs + '"'

            runtime = os.environ["FE_MS_RT"]
            if runtime == "MD":
                dll.linkmap["fbx"] += " libfbxsdk-md.lib"
                dll.linkmap["fbx"] += " libxml2-md.lib"
                dll.linkmap["fbx"] += " zlib-md.lib"
            elif runtime == "MT":
                dll.linkmap["fbx"] += " libfbxsdk-mt.lib"
                dll.linkmap["fbx"] += " libxml2-mt.lib"
                dll.linkmap["fbx"] += " zlib-mt.lib"

        deplibs = forge.corelibs + [
                    "feMathLib",
                    "fexSurfaceDLLib" ]

        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            deplibs += [    "fexSignalLib",
                            "fexDataToolDLLib",
                            "fexDrawDLLib",
                            "fexGeometryDLLib",
                            "fexThreadDLLib" ]

        forge.deps( [ dllname + "Lib" ], deplibs )

        forge.tests += [
            ("inspect.exe",     dllname,                None,       None) ]

    module.Module('test')
