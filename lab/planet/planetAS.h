/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __planet_planetAS_h__
#define __planet_planetAS_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT AsPlanet :
	public AccessorSet,
	public Initialize<AsPlanet>
{
	public:
		void initialize(void)
		{
			add(organics,		FE_SPEC("planet:organics", "planet organics"));
			add(surface,		FE_SPEC("planet:surface", "planet surface"));
			add(atmosphere,		FE_SPEC("planet:atmosphere", "planet atmosphere"));
		}

		Accessor< Record >	organics;
		Accessor< Record >	surface;
		Accessor< Record >	atmosphere;
};


// TODO: deprecate this flora approach.  break up into the cycle approach
class FE_DL_EXPORT AsFlora :
	public AccessorSet,
	public Initialize<AsFlora>
{
	public:
		void initialize(void)
		{
			add(home,			FE_SPEC("planet:home", "home planet or domain"));
			add(location,		FE_USE("spc:at"));
			add(radius,			FE_USE("bnd:radius"));
			add(maxRadius,		FE_USE("bnd:maxradius"));
			add(age,			FE_USE("planet:age"));
			add(lifespan,		FE_USE("planet:lifespan"));
			add(reprospan,		FE_USE("planet:reprospan"));
			add(repronext,		FE_USE("planet:repronext"));
		}

		Accessor< Record >			home;
		Accessor< SpatialVector >	location;
		Accessor< Real >			radius;
		Accessor< Real >			maxRadius;
		Accessor< Real >			age;
		Accessor< Real >			lifespan;
		Accessor< Real >			reprospan;
		Accessor< Real >			repronext;
};

class FE_DL_EXPORT AsLife :
	public AccessorSet,
	public Initialize<AsLife>
{
	public:
		void initialize(void)
		{
			add(home,			FE_SPEC("planet:home", "home planet or domain"));
			add(location,		FE_USE("spc:at"));
			add(age,			FE_USE("life:age"));
			add(lifespan,		FE_USE("life:lifespan"));
			add(reprospan,		FE_USE("life:reprospan"));
			add(repronext,		FE_USE("life:repronext"));
		}

		Accessor< Record >			home;
		Accessor< SpatialVector >	location;
		Accessor< Real >			radius;
		Accessor< Real >			maxRadius;
		Accessor< Real >			age;
		Accessor< Real >			lifespan;
		Accessor< Real >			reprospan;
		Accessor< Real >			repronext;
};

#if 0
class FE_DL_EXPORT AsPhotoSynthesis :
	public AccessorSet,
	public Initialize<AsPhotoSynthesis>
{
	public:
		void initialize(void)
		{
			add(o2absorp,		FE_USE("life:o2absorp"));
			add(co2absorp,		FE_USE("life:co2absorp"));
			add(h2oabsorp,		FE_USE("life:h20absorp"));
			add(co2,			FE_USE("life:co2"));
			add(o2,				FE_USE("life:o2"));
			add(h20,			FE_USE("life:h2o"));
			add(carbs,			FE_USE("life:carbs"));
			add(photons,		FE_USE("life:photons"));
			add(photosyntheff,	FE_USE("life:photosyntheff"));
		}
};
#endif



} /* namespace ext */
} /* namespace fe */

#endif /* __planet_planetAS_h__ */

