/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawConstruct_h__
#define __intelligence_DrawConstruct_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw construction info

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawConstruct: public DrawShape, public HandlerI
{
	public:
				DrawConstruct(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater						m_theaterRV;
		Matter						m_matterRV2;
		RecordArrayView<Foundry>	m_foundryRAV;
		RecordArrayView<Matter>		m_matterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawConstruct_h__ */
