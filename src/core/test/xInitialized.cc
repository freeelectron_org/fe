/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

static U32 gs_count=0;

class B:
	virtual public Counted,
	virtual public Initialized,
	public Initialize<B>
{
	public:

				B(void)							{ feLog("B\n"); }
virtual			~B(void)						{ feLog("~B\n"); }
		void	initialize(void)
				{	feLog("initialize B\n");
					gs_count+=1; }
};

class C:
	virtual public Counted,
	virtual public Initialized
{
	public:
				C(void)							{ feLog("C\n"); }
virtual			~C(void)						{ feLog("~C\n"); }
		void	initialize(void)
				{	feLog("initialize C\n");
					gs_count+=10; }
};

class D:
	public B,
	public C,
	public Initialize<D>
{
	public:
				D(void)							{ feLog("D\n"); }
virtual			~D(void)						{ feLog("~D\n"); }
		void	initialize(void)
				{	feLog("initialize D\n");
					gs_count+=100; }
};

int main(int argc,char** argv,char** env)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		feLog("construct D\n");
		D d;
		d.initialize();		//* just calls for D
		feLog("-\n");
		d.initializeAll();	//* calls for B and D, not C

		feLog("gs_count %d\n",gs_count);
		UNIT_TEST(gs_count==201);

		completed=TRUE;
	}
	catch(fe::Exception &e)	{ e.log(); }
	catch(...)				{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(2);
	UNIT_RETURN();
}
