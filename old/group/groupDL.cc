/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "group/group.h"
#include "platform/dlCore.cc"

extern "C"
{

FE_DL_EXPORT fe::Library* CreateLibrary(fe::sp<fe::Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<fe::group::Projector>("HandlerI.Projector.fex");
	pLibrary->add<fe::group::Insert>("HandlerI.Insert.fex");
	return pLibrary;
}

} /* extern */
