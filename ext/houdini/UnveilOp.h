/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_UnveilOp_h__
#define __houdini_UnveilOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Extract FE surface primitive into native Houdini data

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT UnveilOp:
	public OperatorSurfaceCommon,
	public Initialize<UnveilOp>
{
	public:

					UnveilOp(void)											{}
virtual				~UnveilOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_UnveilOp_h__ */
