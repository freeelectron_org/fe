/*	Copyright (C) 2003-2014 Free Electron Organization
	Any use of this software requires a license. */

/** @file */

#ifndef __mechanics_LinearBendForce_h__
#define __mechanics_LinearBendForce_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "solve/solve.h"
#include "solve/SemiImplicit.h"
#include "mechanicsAS.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT LinearBendForce : public SemiImplicit::Force,
	public CastableAs<LinearBendForce>
{
	public:
			LinearBendForce(sp<SemiImplicit> a_integrator);
virtual		~LinearBendForce(void);
virtual		void clear(void);
virtual		void accumulate(void);
virtual		bool validate(void);

virtual		void compile(	sp<RecordGroup> rg_input,
							std::vector<SemiImplicit::Particle> &a_particles,
							SemiImplicit::CompileMatrix &a_compileMatrix);

	private:
		class FE_DL_EXPORT Element
		{
			public:
				t_solve_real							m_length;
				t_solve_real							m_alpha_A;
				t_solve_real							m_alpha_B;
				t_solve_real							m_alpha_C;
				t_solve_real							m_alpha_D;
				t_solve_real							m_lambda;
				t_solve_real							m_gamma;

				t_solve_v3					*m_location[4];
				t_solve_v3					*m_velocity[4];
				t_solve_v3					*m_force[4];

				t_solve_matrix					m_dfdxAA;
				t_solve_matrix					m_dfdxAB;
				t_solve_matrix					m_dfdxAC;
				t_solve_matrix					m_dfdxAD;
				t_solve_matrix					m_dfdxBB;
				t_solve_matrix					m_dfdxBC;
				t_solve_matrix					m_dfdxBD;
				t_solve_matrix					m_dfdxCC;
				t_solve_matrix					m_dfdxCD;
				t_solve_matrix					m_dfdxDD;

				t_solve_matrix					m_dfdvAA;
				t_solve_matrix					m_dfdvAB;
				t_solve_matrix					m_dfdvAC;
				t_solve_matrix					m_dfdvAD;
				t_solve_matrix					m_dfdvBB;
				t_solve_matrix					m_dfdvBC;
				t_solve_matrix					m_dfdvBD;
				t_solve_matrix					m_dfdvCC;
				t_solve_matrix					m_dfdvCD;
				t_solve_matrix					m_dfdvDD;


				t_solve_matrix					*m_p_dfdxAA;
				t_solve_matrix					*m_p_dfdxAB;
				t_solve_matrix					*m_p_dfdxAC;
				t_solve_matrix					*m_p_dfdxAD;
				t_solve_matrix					*m_p_dfdxBB;
				t_solve_matrix					*m_p_dfdxBC;
				t_solve_matrix					*m_p_dfdxBD;
				t_solve_matrix					*m_p_dfdxCC;
				t_solve_matrix					*m_p_dfdxCD;
				t_solve_matrix					*m_p_dfdxDD;

				t_solve_matrix					*m_p_dfdvAA;
				t_solve_matrix					*m_p_dfdvAB;
				t_solve_matrix					*m_p_dfdvAC;
				t_solve_matrix					*m_p_dfdvAD;
				t_solve_matrix					*m_p_dfdvBB;
				t_solve_matrix					*m_p_dfdvBC;
				t_solve_matrix					*m_p_dfdvBD;
				t_solve_matrix					*m_p_dfdvCC;
				t_solve_matrix					*m_p_dfdvCD;
				t_solve_matrix					*m_p_dfdvDD;
		};

		hp<SemiImplicit>		m_integrator;

		std::vector<Element>	m_elements;
};

} /* namespace */
} /* namespace */


#endif /* __mechanics_LinearBendForce_h__ */
