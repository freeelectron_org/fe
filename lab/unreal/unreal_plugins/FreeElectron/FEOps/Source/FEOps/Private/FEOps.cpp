#include "IFEOps.h"

#if PLATFORM_LINUX
const char* ue4_module_options = "linux_global_symbols";
#endif

class FFEOps : public IFEOps
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

		fe::sp<fe::SingleMaster>			m_spSingleMaster;
};

IMPLEMENT_MODULE( FFEOps, FEOps )

void FFEOps::StartupModule()
{
	// This code will execute after your module is loaded into memory
	// (but after global variables are initialized, of course.)

	feLog("FFEOps::StartupModule\n");

	m_spSingleMaster=UnrealGetSingleMaster();
}

void FFEOps::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.
	// For modules that support dynamic reloading,
	// we call this function before unloading the module.

	feLog("FFEOps::ShutdownModule\n");

	m_spSingleMaster=NULL;
}
