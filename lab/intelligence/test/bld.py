import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   [
                "fexSignalLib",
                "fexNetworkDLLib",
                "fexViewerLib",
                "fexDataToolDLLib"
                ] + forge.corelibs

    if forge.codegen == 'profile':
        deplibs += [
                "feDataDLLib",
                "fexArchitectureDLLib",
                "fexConsoleDLLib",
                "fexDataToolDLLib",
                "fexDrawDLLib",
                "fexElementDLLib",
                "fexIntelligenceDLLib",
                "fexLuaDLLib",
                "fexNativeWindowDLLib",
                "fexOpenGLDLLib",
                "fexShapeDLLib",
                "fexTerrainDLLib"
                ]

    tests = [ 'xFloat' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xFloat.exe",      "10",                           None,       None) ]
