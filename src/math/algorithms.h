/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_algorithms_h__
#define __math_algorithms_h__

namespace fe
{

FE_DL_PUBLIC
BWORD intersectRaySphere(	Real *a_collisionDistance,
							const SpatialVector &a_rayRoot,
							const SpatialVector &a_rayDirection,
							const SpatialVector &a_sphereCenter,
							const Real &a_sphereRadius);


FE_DL_PUBLIC
BWORD intersectLinePlane(	SpatialVector &a_intersection,
							BWORD a_loBound, BWORD a_hiBound,
							const SpatialVector &a_lineLo,
							const SpatialVector &a_lineHi,
							const SpatialVector &a_pointOnPlane,
							const SpatialVector &a_planeNormal);


FE_DL_PUBLIC
BWORD intersectRayPlane(		SpatialVector &a_intersection,
							const SpatialVector &a_lineLo,
							const SpatialVector &a_lineHi,
							const SpatialVector &a_pointOnPlane,
							const SpatialVector &a_planeNormal);


FE_DL_PUBLIC
BWORD intersectLineTriangle(	SpatialVector &a_intersection,
							BWORD a_loBound, BWORD a_hiBound,
							const SpatialVector &a_lineLo,
							const SpatialVector &a_lineHi,
							const SpatialVector &a_triA,
							const SpatialVector &a_triB,
							const SpatialVector &a_triC,
							const SpatialVector &a_triNormal);


FE_DL_PUBLIC
BWORD intersectRayTriangle(	SpatialVector &a_intersection,
							const SpatialVector &a_lineLo,
							const SpatialVector &a_lineHi,
							const SpatialVector &a_triA,
							const SpatialVector &a_triB,
							const SpatialVector &a_triC);

FE_DL_PUBLIC
BWORD intersectTrianglePlane(SpatialVector &a_intersectionA,
							SpatialVector &a_intersectionB,
							const SpatialVector &a_triA,
							const SpatialVector &a_triB,
							const SpatialVector &a_triC,
							const SpatialVector &a_pointOnPlane,
							const SpatialVector &a_planeNormal);

FE_DL_PUBLIC
BWORD pointInTriangle(		const SpatialVector &a_point,
							const SpatialVector &a_triA,
							const SpatialVector &a_triB,
							const SpatialVector &a_triC);

FE_DL_PUBLIC
Vector2 uvDelta(			const Vector2 &a_uv0,
							const Vector2 &a_uv1);

FE_DL_PUBLIC
BWORD triangleDuDv(			Vector<3,Real> &a_du,
							Vector<3,Real> &a_dv,
							const Vector<3,Real> &a_point0,
							const Vector<3,Real> &a_point1,
							const Vector<3,Real> &a_point2,
							const Vector2 &a_uv0,
							const Vector2 &a_uv1,
							const Vector2 &a_uv2);

FE_DL_PUBLIC
BWORD tangentFromDuDvN(		Vector<3,Real> &a_tangent,
							const Vector<3,Real> &a_du,
							const Vector<3,Real> &a_dv,
							const Vector<3,Real> &a_normal);

FE_DL_PUBLIC
void randomRealSeed(		const U32 &a_seed);

FE_DL_PUBLIC
Real randomReal(			const Real &a_min,
							const Real &a_max);

FE_DL_PUBLIC
Real randomRealReentrant(	U32* a_pSeed,
							const Real &a_min,
							const Real &a_max);


} /* namespace */

#endif /* __math_algorithms_h__ */

