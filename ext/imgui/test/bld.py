import sys
forge = sys.modules["forge"]

import os.path

def setup(module):

    deplibs = forge.corelibs + [
            "fexImguiDLLib",
            "fexViewerDLLib",
            "fexWindowLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexSignalLib",
                        "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexOperateDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib" ]

    tests = [   'xImguiCatalog',
                'xImguiRecord' ]

    forge.tests += [
        ("xImguiCatalog.exe",   "ext/imgui/test/catalog.json 60",   None,None) ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)
