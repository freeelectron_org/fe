/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opengl/opengl.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexDrawDL"));
#ifdef FE_GL_OPENCL
	list.append(new String("fexOpenCLDL"));
#endif
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<DrawOpenGL>("DrawI.DrawOpenGL.fe");
	pLibrary->add<FontOpenGL>("FontI.FontOpenGL.fe");
	pLibrary->add<ViewOpenGL>("ViewI.ViewOpenGL.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	//* NOTE this is not what the manifest says
	//* Once OpenGL gets loaded, it takes priority.
	spLibrary->registry()->prioritize("DrawI.DrawOpenGL.fe",1);
	spLibrary->registry()->prioritize("ViewI.ViewOpenGL.fe",1);
}

}
