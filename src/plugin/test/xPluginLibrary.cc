/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

#include "platform/dlCore.cc"

#include "xInterface.h"

class MyComponent : public MyComponentI, public fe::CastableAs<MyComponent>
{
	public:
				MyComponent(void)											{}
virtual			~MyComponent(void)											{}
virtual	int		test(int i)			{ return i*i; }
};

extern "C"
{

FE_DL_EXPORT fe::Library *CreateLibrary(fe::sp<fe::Master>)
{
	fe::Library *pLibrary = fe::Memory::instantiate<fe::Library>();
	pLibrary->add<MyComponent>("MyComponentI.MyComponent.test");
	pLibrary->add<MyComponent>("MyComponentI.MyComponent.test");
	return pLibrary;
}

}
