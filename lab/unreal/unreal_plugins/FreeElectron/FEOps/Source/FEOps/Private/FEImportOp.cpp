#include "FEImportOp.h"

AFEImportOp::AFEImportOp(const FObjectInitializer& ObjectInitializer):
	AFEOperator(ObjectInitializer)
{
	feLog("AFEImportOp::AFEImportOp\n");

	m_componentName="ImportOp";
	m_nodeName="ImportActor";

	m_fsFilename=
			"/home/symhost/sym/proj/gitlab/fe/test/import.lua";

	m_fsModelfile=
			"/home/symhost/sym/proj/gitlab/fe/model/skin.rg";

	m_fsModelnode="";
}

void AFEImportOp::OperatorInit(void)
{
	feLog("AFEImportOp::OperatorInit \"%s\" \"%s\"\n",
			m_componentName.c_str(),m_nodeName.c_str());

	fe::sp<fe::Master> spMaster=m_spSingleMaster->master();
	fe::sp<fe::Registry> spRegistry=spMaster->registry();

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feX(fe::e_invalidPointer,"AFEImportOp::OperatorInit",
				"spCatalog is NULL");
	}

	spCatalog->catalog<bool>("replaceOutput")=TRUE;
	spCatalog->catalog<bool>("cacheFrames")=FALSE;
}

void AFEImportOp::OperatorUpdate(void)
{
	feLog("AFEImportOp::OperatorUpdate \"%s\"\n",m_nodeName.c_str());

//	feLog("AFEImportOp::OperatorUpdate pwd\"%s\"\n",
//			fe::System::getCurrentWorkingDirectory().c_str());

	const FString fsContentDir=FPaths::ProjectContentDir();
	const fe::String contentDir=TCHAR_TO_ANSI(*fsContentDir);

	const fe::String modelfile(TCHAR_TO_ANSI(*m_fsModelfile));
	const fe::String modelnode(TCHAR_TO_ANSI(*m_fsModelnode));

	fe::String filename(TCHAR_TO_ANSI(*m_fsFilename));

	//* if filename starts with "./" or ".\", prepend content dir
	if(filename.prechop("./") != filename ||
		filename.prechop(".\\") != filename)
	{
		filename=contentDir+"/"+filename;
	}

//	feLog("AFEImportOp::OperatorUpdate configuration\n%s\n",
//			m_configuration.c_str());

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();

	spCatalog->catalog<fe::String>("loadfile")=filename;

	fe::String options;
	options.sPrintf( "workpath=\"%s\" modelfile=\"%s\" modelnode=\"%s\"",
			contentDir.c_str(),modelfile.c_str(),modelnode.c_str());

	spCatalog->catalog<fe::String>("options")=options;

	spCatalog->catalog<fe::String>("configuration")=m_configuration;

	feLog("AFEImportOp::OperatorUpdate filename \"%s\"\n",filename.c_str());
	feLog("AFEImportOp::OperatorUpdate modelfile \"%s\"\n",modelfile.c_str());
	feLog("AFEImportOp::OperatorUpdate modelnode \"%s\"\n",modelnode.c_str());
	feLog("AFEImportOp::OperatorUpdate workpath \"%s\"\n",contentDir.c_str());
	feLog("AFEImportOp::OperatorUpdate options \"%s\"\n",options.c_str());
}

void AFEImportOp::UpdateMesh(void)
{
	//* HACK no mesh
	return;

	AFEOperator::UpdateMesh();
}

bool AFEImportOp::Configure(FString configuration)
{
	feLog("AFEImportOp::Configure\n");
	return UpdateConfiguration(TCHAR_TO_ANSI(*configuration));
}

bool AFEImportOp::UpdateConfiguration(fe::String a_configuration)
{
	feLog("AFEImportOp::UpdateConfiguration\n");

	m_configuration=a_configuration;

	//* HACK copied from beginning of UpdateMesh()

	if(m_spOutputAccessible.isNull())
	{
		OperatorCreate();
		OperatorInit();
	}

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();
	if(spCatalog.isNull())
	{
		feLog("AFEImportOp::UpdateConfiguration spCatalog is NULL\n");
		return false;
	}

	OperatorUpdate();

	feLog("AFEImportOp::UpdateConfiguration COOK\n");
	m_terminalNode.cook(fe::Real(0));

	feLog("AFEImportOp::UpdateConfiguration cook summary: \"%s\"\n",
			spCatalog->catalogOrDefault<fe::String>("summary","").c_str());

	return true;
}
