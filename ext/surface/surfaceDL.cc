/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexDataToolDL"));
	list.append(new String("fexShapeDL"));
	list.append(new String("fexGeometryDL"));
	list.append(new String("fexThreadDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	String path=spMaster->catalog()->catalog<String>(
			"path:media")+"/template/surface.rg";

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<OcTree>("SpatialTreeI.default.fe");
	pLibrary->add<OcTree>("SpatialTreeI.OcTree.fe");
	pLibrary->add<MapTree>("SpatialTreeI.MapTree.fe");
	pLibrary->add<QuadTree>("SpatialTreeI.QuadTree.fe");
	pLibrary->add<FlatTree>("SpatialTreeI.FlatTree.fe");

	pLibrary->add<SurfaceSphere>("SurfaceI.SurfaceSphere.fe");
	pLibrary->add<SurfaceCurves>("SurfaceI.SurfaceCurves.fe");
	pLibrary->add<SurfaceCurvesAccessible>(
			"SurfaceI.SurfaceCurvesAccessible.fe.SurfaceCurvesAccessible");
	pLibrary->add<SurfaceDisk>("SurfaceI.SurfaceDisk.fe");
	pLibrary->add<SurfacePlane>("SurfaceI.SurfacePlane.fe");
	pLibrary->add<SurfaceCylinder>("SurfaceI.SurfaceCylinder.fe");
	pLibrary->add<SurfaceTriangles>("SurfaceI.SurfaceTriangles.fe");
	pLibrary->add<SurfaceTrianglesAccessible>(
			"SurfaceI.SurfaceTrianglesAccessible.fe.SurfaceTrianglesAccessible");
	pLibrary->add<SurfaceMRP>("SurfaceI.SurfaceMRP.fe");
	pLibrary->add<SurfaceOBJ>("SurfaceI.SurfaceOBJ.fe");
	pLibrary->add<SurfaceSTL>("SurfaceI.SurfaceSTL.fe");

	pLibrary->add<SurfaceAccessibleBase>(
			"SurfaceAccessibleI.SurfaceAccessibleBase.fe");
	pLibrary->add<SurfaceAccessibleRecord>(
			"SurfaceAccessibleI.SurfaceAccessibleRecord.fe.rg");
	pLibrary->add<SurfaceAccessibleRecord>(
			"SurfaceAccessibleI.SurfaceAccessibleRecord.fe.obj");
	pLibrary->add<SurfaceAccessibleRecord>(
			"SurfaceAccessibleI.SurfaceAccessibleRecord.fe.stl");

	pLibrary->add<SurfaceAccessibleCatalog>(
			"SurfaceAccessibleI.SurfaceAccessibleCatalog.fe.json");
	pLibrary->add<SurfaceAccessibleCatalog>(
			"SurfaceAccessibleI.SurfaceAccessibleCatalog.fe.yaml");

	pLibrary->add<SurfaceAccessibleJoint>(
			"SurfaceAccessibleI.SurfaceAccessibleJoint.fe");
	pLibrary->add<SurfaceAccessibleObj>(
			"SurfaceAccessibleI.SurfaceAccessibleObj.fe.obj");
	pLibrary->add<SurfaceAccessibleXyz>(
			"SurfaceAccessibleI.SurfaceAccessibleObj.fe.xyz");

	pLibrary->add<SurfaceAccessorCached>(
			"SurfaceAccessorI.SurfaceAccessorCached.fe");
	pLibrary->add<SurfaceAccessorCatalog>(
			"SurfaceAccessorI.SurfaceAccessorCatalog.fe");

	pLibrary->add<Material>("RecordFactoryI.Material.fe");
	pLibrary->add<Raster>("RecordFactoryI.Raster.fe");
	pLibrary->add<SurfaceFile>("RecordFactoryI.SurfaceFile.fe");
	pLibrary->add<SurfaceModel>("RecordFactoryI.SurfaceModel.fe");
	pLibrary->add<SurfacePoint>("RecordFactoryI.SurfacePoint.fe");
	pLibrary->add<SurfacePrimitive>("RecordFactoryI.SurfacePrimitive.fe");
	pLibrary->add<SurfaceTransform>("RecordFactoryI.SurfaceTransform.fe");

	sp<Scope> spScope=spMaster->catalog()->catalogComponent("Scope","SimScope");
	RecordView::loadRecordGroup(spScope,path);

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	//* NOTE facilitates generic message for unknown file formats
	spLibrary->registry()->prioritize(
			"SurfaceAccessibleI.SurfaceAccessibleBase.fe",1);

	spLibrary->registry()->prioritize(
			"SurfaceAccessibleI.SurfaceAccessibleRecord.fe.obj",-1);

	spLibrary->registry()->prioritize(
			"SurfaceAccessibleI.SurfaceAccessibleRecord.fe.stl",-1);
}

}


