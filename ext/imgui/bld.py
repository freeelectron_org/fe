import sys
import os
forge = sys.modules["forge"]

def prerequisites():
    return ["viewer"]

def setup(module):

    srcList = [ "ImguiHandler",
                "ImguiHandlerCatalog",
                "ImguiHandlerRecord",
                "Oscilloscope",
                "imguiDL",
                "imgui.pmh",
                "imgui_all" ]


    dll = module.DLL( "fexImguiDL", srcList )

    deplibs = forge.corelibs[:]

    deplibs += [ "fexOperateDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexSignalLib",
                        "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib",
                        "fexViewerDLLib",
                        "fexWindowLib" ]

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    module.includemap = {}
    module.includemap["imgui"] = os.path.join(module.modPath,'imgui')

    forge.deps( ["fexImguiDLLib"], deplibs )

    module.Module('test')

def auto(module):
    test_file = """
#include "imgui/imgui/imgui.h"

int main(void)
{
    return(0);
}
    \n"""

    result = forge.cctest(test_file)

    return result
