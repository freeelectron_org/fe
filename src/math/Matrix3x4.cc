/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

namespace fe
{

BWORD makeFrame(SpatialTransform &a_frame,
	const SpatialVector &a_0,
	const SpatialVector &a_1, const SpatialVector &a_2,
	const SpatialVector &a_offset)
{
	SpatialVector direction = a_1 - a_0;
	SpatialVector up = a_2 - a_0;

	normalize(direction);
	normalize(up);

	SpatialVector side = cross(up, direction);
	if(isZero(side))
	{
		return FALSE;
	}
	normalize(side);
	up = cross(direction, side);
	if(isZero(up))
	{
		return FALSE;
	}
	normalize(up);

	a_frame.direction() = direction;
	a_frame.left() = side;
	a_frame.up() = up;
	a_frame.translation() = a_0;


	SpatialTransform offset_xform;
	setIdentity(offset_xform);
	translate(offset_xform, a_offset);
	a_frame *= offset_xform;

	return TRUE;
}

BWORD makeFrame(SpatialTransform &a_frame,
	const SpatialVector &a_0,
	const SpatialVector &a_1, const SpatialVector &a_2,
	const SpatialTransform &a_transform)
{
	SpatialVector direction = a_1 - a_0;
	SpatialVector up = a_2 - a_0;

	normalize(direction);
	normalize(up);

	SpatialVector side = cross(up, direction);
	if(isZero(side))
	{
		return FALSE;
	}
	normalize(side);
	up = cross(direction, side);
	if(isZero(up))
	{
		return FALSE;
	}
	normalize(up);

	a_frame.direction() = direction;
	a_frame.left() = side;
	a_frame.up() = up;
	a_frame.translation() = a_0;

	a_frame *= a_transform;
	return TRUE;
}

BWORD makeFrame(SpatialTransform &a_frame,
	const SpatialVector &a_location,
	const SpatialVector &a_direction, const SpatialVector &a_up)
{
	SpatialVector side = cross(a_up, a_direction);
	if(isZero(side))
	{
		return FALSE;
	}
	normalize(side);
	SpatialVector up = cross(a_direction, side);
	if(isZero(up))
	{
		return FALSE;
	}
	normalize(up);

	a_frame.direction() = a_direction;
	a_frame.left() = side;
	a_frame.up() = up;
	a_frame.translation() = a_location;
	return TRUE;
}

BWORD makeFrameCameraZ(SpatialTransform &a_frame,
	const SpatialVector &a_location,
	const SpatialVector &a_yDir, const SpatialVector &a_cameraZ)
{
	const SpatialVector zDir = -a_cameraZ;
	SpatialVector xDir = cross(a_yDir, zDir);
	if(isZero(xDir))
	{
		return FALSE;
	}
	normalize(xDir);
	SpatialVector yDir = cross(zDir, xDir);
	if(isZero(yDir))
	{
		return FALSE;
	}
	normalize(yDir);

	a_frame.direction()=xDir;
	a_frame.left()=yDir;
	a_frame.up()=zDir;
	a_frame.translation()=a_location;
	return TRUE;
}

BWORD makeFrameTangentX(SpatialTransform &a_frame,
	const SpatialVector &a_location,
	const SpatialVector &a_tangentX, const SpatialVector &a_normalY)
{
	SpatialVector sideZ = cross(a_tangentX,a_normalY);
	if(isZero(sideZ))
	{
		return FALSE;
	}
	normalize(sideZ);
	SpatialVector normalY = cross(sideZ, a_tangentX);
	if(isZero(normalY))
	{
		return FALSE;
	}
	normalize(normalY);

	a_frame.direction() = a_tangentX;
	a_frame.left() = normalY;
	a_frame.up() = sideZ;
	a_frame.translation() = a_location;
	return TRUE;
}

BWORD makeFrameNormalY(SpatialTransform &a_frame,
	const SpatialVector &a_location,
	const SpatialVector &a_tangentX, const SpatialVector &a_normalY)
{
	SpatialVector sideZ;
	cross(sideZ,a_tangentX,a_normalY);

	SpatialVector tangentX;
	cross(tangentX,a_normalY,sideZ);

	if(isZero(sideZ) || isZero(tangentX))
	{
		return FALSE;
	}

	a_frame.direction() = unit(tangentX);
	a_frame.left() = a_normalY;
	a_frame.up() = unit(sideZ);
	a_frame.translation() = a_location;
	return TRUE;
}

BWORD makeFrameNormalZ(SpatialTransform &a_frame,
	const SpatialVector &a_location,
	const SpatialVector &a_tangentX, const SpatialVector &a_normalZ)
{
	SpatialVector sideY;
	cross(sideY,a_normalZ,a_tangentX);

	SpatialVector tangentX;
	cross(tangentX,sideY,a_normalZ);

	if(isZero(sideY) || isZero(tangentX))
	{
		return FALSE;
	}

	a_frame.direction() = unit(tangentX);
	a_frame.left() = unit(sideY);
	a_frame.up() = a_normalZ;
	a_frame.translation() = a_location;
	return TRUE;
}

} // namespace
