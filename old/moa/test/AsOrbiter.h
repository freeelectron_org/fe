/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Test-only.
 *
 * Accessor Set for objects that should orbit stuff.
 */
class AsOrbiter :
    public fe::AccessorSet,
    public fe::Initialize<AsOrbiter>
{
public:
    void initialize()
    {
        add(transform, FE_USE("transform"));
        add(orbitAngle, FE_USE("orbitAngle"));
        add(orbitPoint, FE_USE("orbitPoint"));
        add(orbitRadius, FE_USE("orbitRadius"));
    };

    /** Object transform to modify. */
    fe::Accessor<fe::SpatialTransform> transform;
    /** Current orbit angle. */
    fe::Accessor<fe::Real> orbitAngle;
    /** Point to orbit about. */
    fe::Accessor<fe::SpatialVector> orbitPoint;
    /** Radius of orbit. */
    fe::Accessor<fe::Real> orbitRadius;
};

} /* namespace ext */
} /* namespace fe */
