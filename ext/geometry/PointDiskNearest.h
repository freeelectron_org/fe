/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_PointDiskNearest_h__
#define __geometry_PointDiskNearest_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find point nearest to a circular solid

	@ingroup geometry
*//***************************************************************************/
template <typename T>
class PointDiskNearest
{
	public:
static	T	solve(const Vector<3,T>& center,const Vector<3,T>& facing,
					const T radius,const Vector<3,T>& origin,
					Vector<3,T>& direction,Vector<3,T>& intersection);
};

template <typename T>
inline T PointDiskNearest<T>::solve(const Vector<3,T>& center,
		const Vector<3,T>& facing,T radius,
		const Vector<3,T>& origin,Vector<3,T>& direction,
		Vector<3,T>& intersection)
{
//	feLog("center %s facing %s radius %.6G\n",
//			c_print(center),c_print(facing),radius);
//	feLog("origin: %s\n",c_print(origin));
//	feLog("direction: %s\n",c_print(direction));

	const Vector<3,T> to_origin=origin-center;
	const T displacement=dot(facing,to_origin);
	intersection=origin-displacement*facing;
	const Vector<3,T> radial=intersection-center;
	const T outward2=magnitudeSquared(radial);
//	feLog("displacement %.6G radial %s\n",displacement,c_print(radial));
//	feLog("intersection: %s sq %.6G vs %.6G\n",c_print(intersection),
//			outward2,radius*radius);

	if(outward2<=radius*radius)
	{
		direction= -facing;
		return displacement;
	}

	intersection=center+radius/sqrt(outward2)*radial;
	direction=intersection-origin;
	T mag=magnitude(direction);
	direction*=T(1)/mag;
	return mag;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_PointDiskNearest_h__ */


