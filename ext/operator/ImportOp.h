/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_ImportOp_h__
#define __operator_ImportOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Load a surface from an arbitrary file

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT ImportOp:
	public OperatorSurfaceCommon,
	public Initialize<ImportOp>
{
	public:

					ImportOp(void)											{}
virtual				~ImportOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	protected:

		String		find(String a_filename);

		sp<SurfaceAccessibleI>					m_spLoadAccessible;

	private:
		String									m_loadedFile;
		String									m_loadedString;
		String									m_loadedPaths;
		String									m_loadedPathsDelimiter;
		String									m_loadedFormat;
		String									m_loadedFilter;
		String									m_loadedSpec;
		String									m_loadedOptions;
		String									m_loadedConfiguration;

		std::map< I32, sp<SurfaceAccessibleI> >	m_frameCache;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_ImportOp_h__ */
