/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SBS_TUBE_DEBUG		FALSE

namespace fe
{
namespace ext
{

//* TODO threshold
SurfaceI::Containment SurfaceBase::containment(
	const SpatialVector& a_origin) const
{
	const Real maxDistance= -1.0;
	sp<SurfaceI::ImpactI> spImpact=nearestPoint(a_origin,maxDistance);
	if(!spImpact.isValid())
	{
		return SurfaceI::e_unknown;
	}

	const SpatialVector intersection=spImpact->intersection();
	const SpatialVector outward=spImpact->normal();
	const SpatialVector toward=intersection-a_origin;

	//* no need to normalize vs zero
	return (dot(outward,toward)<0.0)? SurfaceI::e_outside: SurfaceI::e_inside;
}


sp<SurfaceI::ImpactI> SurfaceBase::shootRay(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,BWORD a_anyHit,
	sp<SurfaceI::ImpactI> a_spLastImpact)
{
	const Real tiny=1e-3;
	const SpatialVector tweakedOrigin=a_origin-tiny*a_direction;
	const Real tweakedDistance=a_maxDistance+2.0*tiny;


	sp<SurfaceTriangles::Impact> spTriImpact=a_spLastImpact;
	if(spTriImpact.isValid())
	{
		FEASSERT(a_anyHit);

		Barycenter<Real> barycenter;
		const Real distance=RayTriangleIntersect<Real>::solve(
				spTriImpact->vertex0(),
				spTriImpact->vertex1(),
				spTriImpact->vertex2(),
				tweakedOrigin,a_direction,barycenter);
		if(distance>=0.0 && distance<=tweakedDistance)
		{
			return a_spLastImpact;
		}
	}
	else
	{
		return rayImpact(tweakedOrigin,a_direction,tweakedDistance,a_anyHit);
	}

	return sp<SurfaceI::ImpactI>();
}

sp<SurfaceI::ImpactI> SurfaceBase::tubeImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,
	Real a_radius0,Real a_radius1,BWORD a_anyHit,sp<DrawI> a_spDrawI,
	sp<SurfaceI::ImpactI> a_spLastImpact)
{
	sp<SurfaceI::ImpactI> spResult;

	sp<SurfaceTriangles::Impact> spTriImpact;
	if(a_anyHit)
	{
		spTriImpact=a_spLastImpact;
	}

	const U32 spineCount=(a_radius0>0.0 || a_radius1>0.0)? 6: 0;	//* tweak

#if FE_SBS_TUBE_DEBUG
	feLog("SurfaceBase::tubeImpact %s dir %s dist %.6G"
			" radius %.6G %.6G any %d\n",
			c_print(a_origin),c_print(a_direction),
			a_maxDistance,a_radius0,a_radius1,a_anyHit);
#endif

	const U32 startPass=spTriImpact.isNull();
	for(U32 pass=startPass;pass<2;pass++)
	{
		if(pass)
		{
			spTriImpact=NULL;

			if(spineCount)
			{
				//* midpoint sphere check
				const Real halfDistance=0.5*a_maxDistance;
				const SpatialVector midpoint=a_origin+halfDistance*a_direction;
				const Real range=halfDistance+
						((a_radius0>a_radius1)? a_radius0: a_radius1);

				sp<SurfaceI::ImpactI> spSphereImpact=
						nearestPoint(midpoint,range,a_anyHit);

				if(a_spDrawI.isValid())
				{
					const Color magenta(1.0,0.0,1.0,1.0);
					const Color cyan(0.0,1.0,1.0,1.0);

					drawHit(a_spDrawI,midpoint,range,
							spSphereImpact.isValid()? magenta: cyan);
				}

				if(spSphereImpact.isNull())
				{
#if FE_SBS_TUBE_DEBUG
					feLog("SurfaceBase::tubeImpact midpoint sphere miss\n");
#endif
					return spSphereImpact;
				}
			}
		}

		//* centerline ray
		spResult=shootRay(a_origin,a_direction,a_maxDistance,
				a_anyHit,spTriImpact);

		if(a_spDrawI.isValid())
		{
			const Color red(0.7,0.0,0.0,1.0);
			const Color lightred(1.0,0.5,0.5,1.0);
			const Color greenish(0.2,0.4,0.0,1.0);
			const Color lightgreenish(0.5,1.0,0.0,1.0);

			SpatialVector line[2];
			line[0]=a_origin;
			line[1]=a_origin+a_direction*a_maxDistance;

			a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,false,
					(spResult.isValid() && spResult->distance()>0.0)?
					(pass? &lightred: &red):
					(pass? &lightgreenish: &greenish));
		}

		if(a_anyHit && spResult.isValid())
		{
#if FE_SBS_TUBE_DEBUG
			feLog("SurfaceBase::tubeImpact anyHit centerline pass %d\n",pass);
#endif
			return spResult;
		}

		if(!spineCount)
		{
			continue;
		}

		//* tip sphere
		const SpatialVector tipPoint=a_origin+a_maxDistance*a_direction;
		sp<SurfaceI::ImpactI> spTipImpact;

		if(pass)
		{
			sp<SurfaceI::ImpactI> spSphereImpact=
					nearestPoint(tipPoint,a_radius1,a_anyHit);
			if(spSphereImpact.isValid())
			{
				spTipImpact=spSphereImpact;
			}
		}
		else
		{
			Barycenter<Real> barycenter;
			SpatialVector direction;
			SpatialVector intersection;
			const Real distance=PointTriangleNearest<Real>::solve(
					spTriImpact->vertex0(),
					spTriImpact->vertex1(),
					spTriImpact->vertex2(),
					tipPoint,direction,intersection,barycenter);
			if(distance>=0.0 && distance<=a_radius1)
			{
				spTipImpact=a_spLastImpact;
			}
		}

		if(a_spDrawI.isValid())
		{
			const Color magenta(0.7,0.0,0.7,1.0);
			const Color cyan(0.0,0.7,0.7,1.0);
			const Color lightmagenta(1.0,0.7,1.0,1.0);
			const Color lightcyan(0.7,1.0,1.0,1.0);

			const Real drawRadius=a_radius1*
					((!startPass && pass)? 0.90: 1.0);

			drawHit(a_spDrawI,tipPoint,drawRadius,
					spTipImpact.isValid()?
					(pass? lightmagenta: magenta):
					(pass? lightcyan: cyan));
		}

		if(a_anyHit && spTipImpact.isValid())
		{
			return spTipImpact;
		}

		const SpatialVector different(
				a_direction[1],a_direction[2],-a_direction[0]);

		SpatialVector alignment;
		cross(alignment,a_direction,different);

#if FE_SBS_TUBE_DEBUG
		feLog("\ncast %s\n",c_print(a_origin));
		feLog("dir %s\n",c_print(a_direction));
		if(spResult.isValid())
		{
			feLog("dist %.6G\n",spResult->distance());
		}
#endif

		for(U32 m=0;m<spineCount;m++)
		{
			SpatialQuaternion spin(2.0*M_PI*m/Real(spineCount),a_direction);
			SpatialVector axis;
			rotateVector(spin,alignment,axis);

#if TRUE
			const SpatialVector reorigin=a_origin+a_radius0*axis;

			//* TODO some of this may not be changing
			const SpatialVector endpoint=
					a_origin+a_maxDistance*a_direction+a_radius1*axis;
			const SpatialVector delta=endpoint-reorigin;
			const Real redistance=magnitude(delta);
			if(redistance<=0.0)
			{
				break;
			}
			SpatialVector redirection=(1.0/redistance)*delta;
#else
			//* cone only
			const SpatialVector reorigin=a_origin;
			const Real redistance=a_maxDistance;
			const Real coneAngle=atan2(a_radius1,a_maxDistance);
			SpatialQuaternion splay(coneAngle,axis);
			SpatialVector redirection;
			rotateVector(splay,a_direction,redirection);
#endif

#if FE_SBS_TUBE_DEBUG
			feLog("%d axis %s\n",m,c_print(axis));
			feLog("redir %s\n",c_print(redirection));
#endif

			sp<SurfaceI::ImpactI> spCandidate=shootRay(reorigin,redirection,
					redistance,a_anyHit,spTriImpact);
#if FE_SBS_TUBE_DEBUG
			if(spCandidate.isValid())
			{
				feLog("dist %.6G\n",spCandidate->distance());
			}
#endif
			if(a_spDrawI.isValid())
			{
				const Real scaledDistance=redistance>0.0? redistance: 1.0;

				const Color blue(0.0,0.0,0.7,1.0);
				const Color lightblue(0.5,0.5,1.0,1.0);
				const Color orange(0.7,0.3,0.0,1.0);
				const Color lightorange(1.0,0.7,0.2,1.0);

				SpatialVector line[2];
				line[0]=reorigin;
				line[1]=reorigin+redirection*scaledDistance;
				a_spDrawI->drawLines(line,NULL,2,
						DrawI::e_discrete,false,
						(spCandidate.isValid() && spCandidate->distance()>0.0)?
						(pass? &lightorange: &orange):
						(pass? &lightblue: &blue));
			}

			if(!spCandidate.isValid() || spCandidate->distance()<0.0)
			{
				continue;
			}
			if(a_anyHit)
			{
#if FE_SBS_TUBE_DEBUG
				feLog("SurfaceBase::tubeImpact anyHit side pass %d\n",pass);
#endif
				return spCandidate;
			}

			if(!spResult.isValid() || spResult->distance()<0.0 ||
					spCandidate->distance()<spResult->distance())
			{
				spResult=spCandidate;
#if FE_SBS_TUBE_DEBUG
				feLog("REPLACE\n");
#endif
			}
		}
	}
#if FE_SBS_TUBE_DEBUG
	feLog("SurfaceBase::tubeImpact result valid %d\n",spResult.isValid());
#endif
	return spResult;
}

sp<SurfaceI::ImpactI> SurfaceBase::coneImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,
	Real a_coneAngle, sp<DrawI> a_spDrawI)
{
	const Real radius=a_maxDistance*tan(a_coneAngle);
	sp<SurfaceI::ImpactI> a_spLastImpact;	//* TODO

	return tubeImpact(a_origin,a_direction,a_maxDistance,
			0.0,radius,FALSE,a_spDrawI,a_spLastImpact);
}

sp<SurfaceI::ImpactI> SurfaceBase::capsuleImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,
	Real a_radius,BWORD a_anyHit,sp<DrawI> a_spDrawI,
	sp<SurfaceI::ImpactI> a_spLastImpact)
{
	//* TODO cone caps

	return tubeImpact(a_origin,a_direction,a_maxDistance,
			a_radius,a_radius,a_anyHit,a_spDrawI,a_spLastImpact);
}

sp<SurfaceI::ImpactI> SurfaceBase::coneImpact(
	const SpatialTransform& a_transform,
	const SpatialVector& a_origin,const SpatialVector& a_direction,
	Real a_maxDistance,Real a_coneAngle,sp<DrawI> a_spDrawI)
{
	const SpatialVector invOrigin=inverseTransformVector(a_transform,a_origin);
	const SpatialVector invDirection=
			inverseRotateVector(a_transform,a_direction);
	sp<Impact> spImpact=coneImpact(invOrigin,invDirection,a_maxDistance,
			a_coneAngle,a_spDrawI);
	if(spImpact.isValid())
	{
		spImpact->setTransform(a_transform);
	}
	return spImpact;
}

sp<SurfaceI::ImpactI> SurfaceBase::capsuleImpact(
	const SpatialTransform& a_transform,
	const SpatialVector& a_origin,const SpatialVector& a_direction,
	Real a_maxDistance,Real a_radius,BWORD a_anyHit,sp<DrawI> a_spDrawI,
	sp<SurfaceI::ImpactI> a_spLastImpact)
{
	const SpatialVector invOrigin=inverseTransformVector(a_transform,a_origin);
	const SpatialVector invDirection=
			inverseRotateVector(a_transform,a_direction);
	sp<Impact> spImpact=capsuleImpact(invOrigin,invDirection,a_maxDistance,
			a_radius,a_anyHit,a_spDrawI,a_spLastImpact);
	if(spImpact.isValid())
	{
		spImpact->setTransform(a_transform);
	}
	return spImpact;
}

//* static
void SurfaceBase::drawHit(sp<DrawI> a_spDrawI,SpatialVector a_center,
	Real a_radius,Color a_color)
{
	const Real crossScale=0.2;

	SpatialVector line[4];
	line[0]=a_center-crossScale*SpatialVector(a_radius,0.0,0.0);
	line[1]=a_center+crossScale*SpatialVector(a_radius,0.0,0.0);
	line[2]=a_center-crossScale*SpatialVector(0.0,0.0,a_radius);
	line[3]=a_center+crossScale*SpatialVector(0.0,0.0,a_radius);

	a_spDrawI->drawLines(line,NULL,4,DrawI::e_discrete,false,&a_color);

	SpatialTransform transform;
	setIdentity(transform);
	rotate(transform,90.0*degToRad,e_xAxis);
	setTranslation(transform,a_center);

	SpatialVector scale(a_radius,a_radius,a_radius);

	a_spDrawI->drawCircle(transform,&scale,a_color);
}

} /* namespace ext */
} /* namespace fe */