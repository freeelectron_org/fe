/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __enet_EnetCatalog_h__
#define __enet_EnetCatalog_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief ConnectedCatalog over Enet

	@ingroup Enet
*//***************************************************************************/
class FE_DL_EXPORT EnetCatalog:
	public ConnectedCatalog,
	public Initialize<EnetCatalog>
{
	public:

				EnetCatalog(void);
virtual			~EnetCatalog(void);

		void	initialize(void);

	protected:

virtual	Result	disconnect(void) override;

virtual	Result	connectAsServer(String a_address,U16 a_port) override;
virtual	Result	connectAsClient(String a_address,U16 a_port) override;

	private:

		void	broadcastSelect(String a_name,String a_property,
					String a_message,
					I32 a_includeCount,const String* a_pIncludes,
					I32 a_excludeCount,const String* a_pExcludes,
					const U8* a_pRawBytes=NULL,I32 a_byteCount=0) override;

		void	sendString(String a_textD);
		void	sendBytes(const U8* a_pByteBlock,const I32 a_byteCountD);

	class MessageBuffer
	{
		public:
						MessageBuffer(void) { m_currentOffset = 0; }

			void		AddString(const String &str);
			void		GetString(String &str);

			void		AddBytes(const U8 *data, const int size);
			I32			GetBytes(U8 *data, const int size);

			void		AddArray(const Array<U8> &data);
			I32			GetArray(Array<U8> &data);

			const U8*	Data(void)		{ return m_data.data(); }
			Array<U8>&	DataArray(void)	{ return m_data; }
			I32			Size(void)		{ return m_data.size(); }

			void		Reset();

		private:
			int			m_currentOffset;
			Array<U8>	m_data;
	};

	class FE_DL_EXPORT ReceiverTask: public Thread::Functor
	{
		public:
					ReceiverTask(void):
						m_terminate(FALSE) {}
	virtual	void	operate(void);

			I32		readString(String& a_rString);
			I32		readBytes(Array<U8>& a_rByteArray);
			I32		readIdentity(Identity& a_identity);

			EnetCatalog*	m_pEnetCatalog;
			void*			m_pEnetConnection;
			BWORD			m_terminate;

		private:
			I32		readBuffer(Array<U8>& a_pMsg);

			MessageBuffer	m_receiveMessage;
	};

		MessageBuffer	m_message;
		Thread*			m_pReceiverThread;
		ReceiverTask	m_receiverTask;

		void*			m_pEnetConnection;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __enet_EnetCatalog_h__ */
