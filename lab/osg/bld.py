import sys
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "osg.pmh",
                "ContextOSG",
                "SceneNodeOSG",
                "SurfaceOSG",
                "osgDL" ]

    dll = module.DLL( "fexOSGDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexSolveDLLib",
                "fexSurfaceDLLib" ]

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["scenelibs"] = "-losg -losgDB -losgUtil"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            dll.linkmap["scenelibs"] = "osgd.lib osgDBd.lib osgUtild.lib"
            dll.linkmap["scenelibs"] += " OpenThreadsd.lib"
        else:
            dll.linkmap["scenelibs"] = "osg.lib osgDB.lib osgUtil.lib"
            dll.linkmap["scenelibs"] += " OpenThreads.lib"

    forge.deps( ["fexOSGDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexOSGDL",         None,   None),
        ("xSurface.exe",    "strip1 10",        None,   None) ]

#   module.Module('test')

def auto(module):
    test_file = """
#include <osg/Node>

int main(void)
{
    return(0);
}
    \n"""

    scenemap = { "gfxlibs": forge.gfxlibs }
    if forge.fe_os == "FE_LINUX":
        scenemap["scenelibs"] = "-losg -losgDB -losgUtil"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == 'debug':
            scenemap["scenelibs"] = "osgd.lib osgDBd.lib osgUtild.lib"
            scenemap["scenelibs"] += " OpenThreadsd.lib"
        else:
            scenemap["scenelibs"] = "osg.lib osgDB.lib osgUtil.lib"
            scenemap["scenelibs"] += " OpenThreads.lib"

    return forge.cctest(test_file, scenemap)
