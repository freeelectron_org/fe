import os
import sys
import re
import utility
forge = sys.modules["forge"]

def prerequisites():
    return []

def setup(module):

    srcList = [ "CatalogReaderYaml",
                "CatalogWriterYaml",
                "ManifestReaderYaml",
                "yaml.pmh",
                "yamlDL" ]

    # add every "yaml-cpp/src/*.cpp"
    srcList += utility.find_all(module.modPath,"yaml-cpp/src",r'(.*)\.cpp')

    dll = module.DLL( "fexYamlDL", srcList )

    module.cppmap = {}
    module.cppmap["yaml"] = "-DYAML_CPP_STATIC_DEFINE"

    module.includemap = {}
    module.includemap["yaml"] = "ext/yaml/yaml-cpp/include"

    deplibs = forge.corelibs[:]

    forge.deps( ["fexYamlDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexYamlDL",                None,       None) ]

    module.Module('test')
