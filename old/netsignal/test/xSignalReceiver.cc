/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>	// direct access to ChainSignaler

#include <netsignal/netsignal.pmh>

#define LISTENQ		5
#define SERV_PORT	7876

class AHandler:
	virtual public fe::ext::HandlerI
{
	public:
		AHandler(void){}
virtual	~AHandler(void){}
virtual	void	handle(fe::Record &signal)
		{
			feLogGroup("netsignal","a: %d\n", a(signal));
		}
virtual	void	handleBind(fe::sp<fe::ext::SignalerI> spSignalerI,
						fe::sp<fe::Layout> spLayout)
		{
			//spLayout->lock();
			a.setup(spLayout->scope(), "a");
			if(!a.check(spLayout))
			{
				throw fe::Exception("cannot register for signals w/o \'a\'");
			}
		}
	private:
		fe::Accessor<int>	a;
};

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Scope> spScope(new fe::Scope(*spMaster.raw()));

		fe::Accessor<int>	a(spScope,"a");
		fe::sp<fe::Layout>	spSignal = spScope->declare("SIGNAL");
		spSignal->populate(a);

		fe::sp<fe::Layout>	spBeat = spScope->declare("BEAT");
		fe::Record beat = spScope->createRecord(spBeat);

		fe::sp<fe::ext::SignalerI> spSignalerI(new fe::ext::ChainSignaler());

		fe::sp<fe::ext::HandlerI> spHandlerI(new AHandler);
		spSignalerI->insert(spHandlerI, spSignal);

		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

		server_addr.setPort(SERV_PORT);
		server_addr.setAddress("127.0.0.1");

		connection->openStream();
		connection->connect(server_addr);

		fe::sp<fe::ext::ChunkSpool> spChunkSpool(new fe::ext::ChunkSpool);

		fe::sp<fe::ext::ChunkReceiver> spChunkReceiver(new fe::ext::ChunkReceiver);

		fe::sp<fe::ext::HandlerI> spReceiver(
			new fe::ext::SignalReceiver(spScope, spChunkSpool));

		spSignalerI->insert(spReceiver, spBeat);

		spChunkReceiver->start(spChunkSpool, connection);

		while(true)
		{
			fe::milliSleep(2000);
			spSignalerI->signal(beat);
		}

		spChunkReceiver->stop();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}
