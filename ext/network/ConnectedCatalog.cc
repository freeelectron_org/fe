/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "network/network.pmh"

#include <thread>

#define FE_CTC_DEBUG			FALSE
#define FE_CTC_VERBOSE			FALSE

#define FE_CTC_THREAD_CONNECT	TRUE	//* wait for connection on side thread
#define FE_CTC_PERSISTENT		TRUE	//* keep going with no clients left

namespace fe
{
namespace ext
{

ConnectedCatalog::ConnectedCatalog(void):
	m_identityCount(0),
	m_lockOnUpdate(TRUE),
	m_connectionResult(e_notInitialized),
	m_pConnectionThread(NULL),
	m_pServiceThread(NULL),
	m_signalsQueued(0),
	m_started(false),
	m_connected(false),
	m_aborting(false),
	m_disconnecting(false),
	m_timerLimit(1.0),
	m_tickerLimit(1.0)
{
	setConnectionTimeout(4.0);

	timerRestart();
	tickerRestart();
}

ConnectedCatalog::~ConnectedCatalog(void)
{
	//* NOTE auto-running stop() here is unlikely to be safe

//	joinThreads();
	stop();
}

void ConnectedCatalog::initialize(void)
{
	catalog<bool>("net:broadcasting")=false;

	catalog<bool>("net:implementation","local")=true;
	catalog<bool>("net:role","local")=true;
	catalog<bool>("net:address","local")=true;
	catalog<bool>("net:port","local")=true;

	catalog<bool>("net:ioPriority","local")=true;

	//* tcp, udp
	catalog<bool>("net:transport","local")=true;

	//* block on unconnected flush: true or false
	catalog<bool>("net:connectionBlock","local")=true;
}

BWORD ConnectedCatalog::useBinaryForType(String a_type) const
{
	return (a_type=="bytearray" || a_type=="record");
}

void ConnectedCatalog::stall(void) const
{
	minimumSleep(100000);
}

void ConnectedCatalog::microPause(I32 a_microSeconds) const
{
	std::this_thread::sleep_for(std::chrono::microseconds(a_microSeconds));
}

void ConnectedCatalog::yield(void) const
{
//	std::this_thread::yield();
	microPause(10);
}

void ConnectedCatalog::serviceYield(void) const
{
	microPause(10000);
}

void ConnectedCatalog::setConnectionTimeout(Real a_seconds)
{
	m_timerLimit=a_seconds;
	m_tickerLimit=0.5*a_seconds;
}

void ConnectedCatalog::timerRestart(void)
{
	m_timerStart=std::chrono::steady_clock::now();
}

Real ConnectedCatalog::timerSeconds(void) const
{
	std::chrono::steady_clock::time_point timerNow=
			std::chrono::steady_clock::now();

	return std::chrono::duration<float>(timerNow-m_timerStart).count();
}

BWORD ConnectedCatalog::timerReached(void) const
{
	return (timerSeconds()>=m_timerLimit);
}

void ConnectedCatalog::tickerRestart(void)
{
	m_tickerStart=std::chrono::steady_clock::now();
}

Real ConnectedCatalog::tickerSeconds(void) const
{
	std::chrono::steady_clock::time_point tickerNow=
			std::chrono::steady_clock::now();

	return std::chrono::duration<float>(tickerNow-m_tickerStart).count();
}

BWORD ConnectedCatalog::tickerReached(void) const
{
	return (tickerSeconds()>=m_tickerLimit);
}

Result ConnectedCatalog::configure(String a_line)
{
	String location=a_line.parse();
//	feLog("location \"%s\" %d\n",location.c_str());

	const String address=location.parse("\"",":");
	const String portText=location.parse("\"",":");
	const I32 port=portText.integer();

//	feLog("address \"%s\"\n",address.c_str());
//	feLog("port \"%s\" %d\n",portText.c_str(),port);

	if(!address.empty())
	{
		catalog<String>("net:address")=address;
	}

	if(port>0)
	{
		catalog<I32>("net:port")=port;
	}

	String token;
	while(!(token=a_line.parse()).empty())
	{
//		feLog("ConnectedCatalog::configure \"%s\"\n",token.c_str());

		const String netKey=token.parse("\"","=");
		const String value=token.parse("\"","=");

		if(!(netKey.empty() || value.empty()))
		{
			catalog<String>("net:"+netKey)=value;
		}
	}

	catalogDump();
	return e_ok;
}

Result ConnectedCatalog::start(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::start\n");
#endif

	Mutex::Guard guard(m_startStopMutex);

	if(started())
	{
		return e_notNeeded;
	}

	m_started=true;

	m_serviceTask.m_pConnectedCatalog=this;
	m_pServiceThread=new Thread(&m_serviceTask);

#if FE_CTC_THREAD_CONNECT
	m_connectionTask.m_pConnectedCatalog=this;
	m_pConnectionThread=new Thread(&m_connectionTask);
	m_connectionResult=e_ok;
#else
	m_connectionResult=connect();
#endif

	return m_connectionResult;
}

void ConnectedCatalog::joinThreads(void)
{
	m_aborting=true;

	//* NOTE thread should already have exited right after connection
	if(m_pConnectionThread)
	{
		if(m_pConnectionThread->joinable())
		{
			m_pConnectionThread->join();
		}
		delete m_pConnectionThread;
		m_pConnectionThread=NULL;
	}

	if(m_pServiceThread)
	{
		if(m_pServiceThread->joinable())
		{
			m_pServiceThread->join();
		}
		delete m_pServiceThread;
		m_pServiceThread=NULL;
		m_serviceTask.m_pConnectedCatalog=NULL;
	}
}

Result ConnectedCatalog::stop(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::stop %p\n",this);
#endif

	Mutex::Guard guard(m_startStopMutex);

	if(!started())
	{
#if FE_CTC_DEBUG
		feLog("ConnectedCatalog::stop %p not started\n",this);
#endif
		return e_notNeeded;
	}

	joinThreads();

#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::stop disconnect\n");
#endif

	Result result=disconnect();

	m_started=false;
	return result;
}

Result ConnectedCatalog::waitForConnection(void)
{
	while(!connected())
	{
		stall();
	}

	return e_ok;
}

Result ConnectedCatalog::connect(void)
{
	if(connected())
	{
#if FE_CTC_DEBUG
		feLog("ConnectedCatalog::connect not needed\n");
#endif
		return e_notNeeded;
	}

	try
	{
		const BWORD isClient=
				(catalogOrDefault<String>("net:role","client")=="client");
		const String address=
				catalogOrDefault<String>("net:address","127.0.0.1");
		const I32 port=catalogOrDefault<I32>("net:port",7890);

		if(isClient)
		{
			return connectAsClient(address,port);
		}
		return connectAsServer(address,port);
	}
	catch(Exception &e)
	{
		feLog("ConnectedCatalog::connect failed: %s\n",
				e.getMessage().c_str());
		return e.getResult();
	}

	return e_undefinedFailure;
}

Result ConnectedCatalog::connectAsServer(String a_address,U16 a_port)
{
	m_role="server";

	while(!m_identityCount)
	{
		if(aborting())
		{
#if FE_CTC_DEBUG
			feLog("ConnectedCatalog::connectAsServer"
					" abort without connection\n");
#endif
			return e_aborted;
		}

		heartbeat();

#if FE_CTC_VERBOSE
		feLog("ConnectedCatalog::connectAsServer waiting for connection\n");
#endif

		stall();
	}

	safeLock();

	catalog<bool>("net:broadcasting")=true;
	broadcastUpdate("net:broadcasting","value");
	m_connected=true;

	safeUnlock();

	return e_ok;
}

Result ConnectedCatalog::connectAsClient(String a_address,U16 a_port)
{
	feLog("ConnectedCatalog::connectAsClient \"%s\" %d\n",
			a_address.c_str(),a_port);

	m_role="client";

	heartbeat();

	//* NOTE need to send something to introduce this client
	safeLock();
	broadcastMessage("net:greeting","new client");
	safeUnlock();

	bool broadcasting=false;
	while(!successful(
			justGetState<bool>("net:broadcasting","value",broadcasting)) ||
			!broadcasting)
	{
		if(aborting())
		{
#if FE_CTC_DEBUG
			feLog("ConnectedCatalog::connectAsClient"
					" abort without connection\n");
#endif
			return e_aborted;
		}

		heartbeat();

#if FE_CTC_VERBOSE
		feLog("ConnectedCatalog::connectAsClient waiting for confirmation\n");
#endif

		stall();
	}

	justGetState<String>("net:id","value",m_clientID);
	feLog("ConnectedCatalog::connectAsClient net:id \"%s\"\n",
			m_clientID.c_str());

	safeLock();

	m_listenKey="net:listening["+m_clientID+"]";
	catalog<bool>(m_listenKey)=true;
	broadcastUpdate(m_listenKey,"value");

	safeUnlock();

	m_connected=true;

#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::connectAsClient connected\n");
#endif
	return e_ok;
}

Result ConnectedCatalog::disconnect(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::disconnect\n");
#endif

	if(!connected())
	{
#if FE_CTC_DEBUG
		feLog("ConnectedCatalog::disconnect not needed\n");
#endif
		return e_notNeeded;
	}

	sendNotifications();

	m_connected=false;

	I32 waitCount=32;

	if(m_role == "server")
	{
//		safeLock();

		catalog<bool>("net:broadcasting")=false;
		broadcastUpdate("net:broadcasting","value");

//		safeUnlock();

		while(m_identityCount)
		{
			heartbeat();

			feLog("ConnectedCatalog::disconnect"
					" waiting for zero clients (%d remain)\n",
					m_identityCount);

			stall();

			if(--waitCount<1)
			{
				feLog("ConnectedCatalog::disconnect"
						" giving up waiting for zero clients\n");
				break;
			}
		}

		if(waitCount)
		{
			feLog("ConnectedCatalog::disconnect confirmed zero clients\n");
		}
	}

	if(m_role == "client")
	{
//		safeLock();

		catalog<bool>(m_listenKey)=false;
		broadcastUpdate(m_listenKey,"value");

//		safeUnlock();
	}

	return e_ok;
}

Result ConnectedCatalog::dropConnection(void)
{
	feLog("ConnectedCatalog::dropConnection\n");

	catalog<bool>("net:broadcasting")=false;

	if(m_role == "client")
	{
		catalog<bool>(m_listenKey)=false;
	}

	m_connected=false;
	m_started=false;

	//* TODO should we resolve threads, like with joinThreads()?

	return e_ok;
}

void ConnectedCatalog::broadcastUpdate(String a_name,String a_property)
{
	broadcastSelect(a_name,a_property,"",0,NULL,0,NULL);
}

void ConnectedCatalog::broadcastMessage(String a_name,String a_message)
{
	broadcastSelect(a_name,FE_STATECATALOG_MESSAGE_PROPERTY,a_message,
			0,NULL,0,NULL);
}

void ConnectedCatalog::broadcastTick(void)
{
	broadcastSelect("net:tick","","",0,NULL,0,NULL);
}

void ConnectedCatalog::queueRepeat(Command a_command,
	String a_name,String a_property,String a_message,String a_source,
	U8* a_pRawBytes,I32 a_byteCount)
{
//	feLog("ConnectedCatalog::queueRepeat \"%s\" \"%s\" \"%s\" \"%s\" %p %d\n",
//			a_source.c_str(),a_name.c_str(),a_property.c_str(),
//			a_message.c_str(),a_pRawBytes,a_byteCount);

	Array<KeyProperty>& rRepeatQueue=m_repeatQueueMap[a_source];

	const I32 reflectionCount=rRepeatQueue.size();
	I32 index=0;
	for(;index<reflectionCount;index++)
	{
		KeyProperty& rKeyProperty=rRepeatQueue[index];

		sp<KeyProperty::Bytes>& rspBytes=rKeyProperty.m_spBytes;
		const U8* pRawBytes=rspBytes.isValid()? rspBytes->m_pRawBytes: NULL;

		if(		rKeyProperty.m_command==a_command &&
				rKeyProperty.m_key==a_name &&
				rKeyProperty.m_property==a_property &&
				rKeyProperty.m_message==a_message &&
				pRawBytes==a_pRawBytes &&
				rKeyProperty.m_byteCount==a_byteCount)
		{
			break;
		}
	}
	if(index<reflectionCount)
	{
		//* repeat already pending
		return;
	}
	if(index==reflectionCount)
	{
//		feLog("ConnectedCatalog::queueRepeat expanding queue to %d\n",
//				reflectionCount+1);
		rRepeatQueue.resize(reflectionCount+1);
	}

	rRepeatQueue[index].set(a_command,a_name,a_property,a_message,a_source,
			a_pRawBytes,a_byteCount);

	if(a_command==e_signal)
	{
		m_signalsQueued++;
	}
}

Result ConnectedCatalog::catchUpAsNeeded(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::catchUpAsNeeded\n");
#endif

	if(m_role!="server")
	{
		return e_ok;
	}

	for(I32 identityIndex=0;identityIndex<m_identityCount;identityIndex++)
	{
		Identity& rIdentity=m_identityArray[identityIndex];
		if(!rIdentity.m_initiated)
		{
			catchUp(rIdentity);
			rIdentity.m_initiated=TRUE;
		}
	}
	return e_ok;
}

Result ConnectedCatalog::catchUp(Identity& a_rIdentity)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::catchUp client \"%s\"\n",
			a_rIdentity.m_text.c_str());
#endif

	catalog<String>("net:id")=a_rIdentity.m_text;
	broadcastSelect("net:id","value","",1,&a_rIdentity.m_text,0,NULL);
	catalogRemove("net:id");

	I32 catchupCount=0;

	Array<String> keys;
	catalogKeys(keys);

	const I32 keyCount=keys.size();
	for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String& key=keys[keyIndex];

		const BWORD isSignal=catalogOrDefault<bool>(key,"signal",false);
		if(isSignal)
		{
			continue;
		}

		if(catalogOrDefault<bool>(key,"broadcasted",false))
		{
#if FE_CTC_DEBUG
			feLog("ConnectedCatalog::catchUp \"%s\"\n",key.c_str());
#endif

			const BWORD isNet=(!strncmp(key.c_str(),"net:",4));

			Array<String> properties;
			catalogProperties(key,properties);

			const I32 propCount=properties.size();
			for(I32 propIndex=0;propIndex<propCount;propIndex++)
			{
				const String& property=properties[propIndex];

				if(property == "local" || property == "broadcasted" ||
						property == "message")
				{
					continue;
				}

				broadcastSelect(key,property,"",1,&a_rIdentity.m_text,0,NULL);

				//* NOTE "net:*" are not included in a flush
				if(!isNet)
				{
					catchupCount++;
				}
			}
		}
	}

	feLog("ConnectedCatalog::catchUp catchupCount %d\n",catchupCount);

	if(catchupCount)
	{
		catalog<I32>("net:flushed")=catchupCount;
		broadcastSelect("net:flushed","value","",1,&a_rIdentity.m_text,0,NULL);
	}

	return e_ok;
}

Result ConnectedCatalog::flushRepeats(BWORD a_signalsOnly)
{
#if FE_CNC_VERBOSE
	feLog("ConnectedCatalog::flushRepeats signalsOnly %d\n",a_signalsOnly);
#endif

	//* send repeats
	for(std::map<String, Array<KeyProperty> >::iterator it=
			m_repeatQueueMap.begin();it!=m_repeatQueueMap.end();it++)
	{
		const String& source=it->first;
		Array<KeyProperty>& rRepeatQueue=it->second;

		const I32 reflectionCount=rRepeatQueue.size();
		I32 repeatCount=0;
		for(I32 index=0;index<reflectionCount;index++)
		{
			const KeyProperty& keyProperty=rRepeatQueue[index];
			const BWORD isSignal=(keyProperty.m_command==e_signal);

			if(isSignal)
			{
				m_signalsQueued--;
			}
			else if(a_signalsOnly)
			{
				continue;
			}

			const String& key=keyProperty.m_key;
			const String entrySource=keyProperty.m_source;

#if FE_CNC_VERBOSE
			feLog("ConnectedCatalog::flushRepeats"
					" source \"%s\" key \"%s\"\n",
					entrySource.c_str(),key.c_str());
#endif

			if(source!=entrySource)
			{
#if FE_CNC_VERBOSE
				feLog("ConnectedCatalog::flushRepeats"
						" mismatched source \"%s\" -> \"%s\"\n",
						source.c_str(),entrySource.c_str());
#endif
			}

			const String& property=keyProperty.m_property;
			const String& message=keyProperty.m_message;
			const U8* pRawBytes=keyProperty.m_spBytes.isValid()?
					keyProperty.m_spBytes->m_pRawBytes: NULL;
			const I32 byteCount=keyProperty.m_byteCount;

#if FE_CNC_VERBOSE
			feLog("ConnectedCatalog::flushRepeats signalsOnly %d"
					" repeat \"%s\" from \"%s\"\n",
					a_signalsOnly,key.c_str(),source.c_str());
#endif

			broadcastSelect(key,property,message,0,NULL,1,&source,
					pRawBytes,byteCount);

			if(!isSignal)
			{
				repeatCount++;
			}
		}
		if(repeatCount)
		{
#if FE_CNC_VERBOSE
			feLog("ConnectedCatalog::flushRepeats repeatCount %d\n",
					repeatCount);
#endif
			catalog<I32>("net:flushed")=repeatCount;
			broadcastSelect("net:flushed","value","",0,NULL,1,&source);
		}

		rRepeatQueue.clear();
	}

	return e_ok;
}

Result ConnectedCatalog::flush(void)
{
#if FALSE
	Result result=StateCatalog::flush();
	if(failure(result))
	{
		return result;
	}
#endif

	const String connectionBlock=
			catalogOrDefault<String>("net:connectionBlock","false");

#if FE_CTC_VERBOSE
	feLog("ConnectedCatalog::flush connected %d connectionBlock \"%s\"\n",
			connected(),connectionBlock.c_str());
#endif

	if(connectionBlock=="true")
	{
		while(!connected())
		{
			stall();
		}
	}
	else if(!connected())
	{
#if FE_CTC_VERBOSE
		feLog("ConnectedCatalog::flush ignored while not connected\n");
#endif
		return e_notInitialized;
	}

	safeLock();

	flushRepeats(FALSE);

	I32 flushed=0;

	const I32 changeCount=m_flushKeys.size();
	for(I32 changeIndex=0;changeIndex<changeCount;changeIndex++)
	{
		const KeyProperty& rKeyProperty=m_flushKeys[changeIndex];
		if(!catalogOrDefault<bool>(rKeyProperty.m_key,"local",false))
		{
			broadcastUpdate(rKeyProperty.m_key,rKeyProperty.m_property);

			//* internal "net:" messages are not counted
			if(strncmp(rKeyProperty.m_key.c_str(),"net:",4))
			{
				flushed++;
			}
		}
	}

	const I32 messageCount=m_flushMessages.size();
	for(I32 messageIndex=0;messageIndex<messageCount;messageIndex++)
	{
		const KeyMessage& rKeyMessage=m_flushMessages[messageIndex];
		if(!catalogOrDefault<bool>(rKeyMessage.m_key,"local",false))
		{
			broadcastMessage(rKeyMessage.m_key,rKeyMessage.m_message);

			//* internal "net:" messages are not counted
			if(strncmp(rKeyMessage.m_key.c_str(),"net:",4))
			{
				flushed++;
			}
		}
	}

	if(!flushed)
	{
		safeUnlock();
		return e_ok;
	}

#if FALSE
	static BWORD firstFlush=TRUE;
	if(firstFlush)
	{
		for(I32 keyIndex=0;keyIndex<changeCount;keyIndex++)
		{
			const KeyProperty& rKeyProperty=m_flushKeys[keyIndex];
			feLog("flush \"%s\" \"%s\"\n",
					rKeyProperty.m_key.c_str(),
					rKeyProperty.m_property.c_str());
		}
	}
	firstFlush=FALSE;
#endif

#if FE_CTC_VERBOSE
	if(flushed!=2)
	{
		feLog("  flushed %d flushCount %d serial %d\n",
				flushed,flushCount(),serial());
	}
#endif

	catalog<I32>("net:flushed")=flushed;
	broadcastUpdate("net:flushed","value");
	m_flushKeys.clear();
	m_flushMessages.clear();

	safeUnlock();
	return e_ok;
}

void ConnectedCatalog::heartbeat(void)
{
	if(connected() && disconnecting())
	{
#if FE_CTC_DEBUG
		feLog("ConnectedCatalog::heartbeat disconnecting\n");
#endif

		m_disconnecting=false;
		disconnect();
	}
}

Result ConnectedCatalog::preGet(String a_name,String a_property) const
{
	Result result=StateCatalog::preGet(a_name,a_property);
	if(failure(result))
	{
		return result;
	}

	if(catalogOrDefault<bool>(a_name,"local",false))
	{
		return e_ok;
	}

	return e_ok;
}

Result ConnectedCatalog::postSet(String a_name,String a_property)
{
	incrementSerial();

	if(catalogOrDefault<bool>(a_name,"local",false))
	{
		return e_ok;
	}

//	feLog("ConnectedCatalog::postSet \"%s\" \"%s\"\n",
//			a_name.c_str(),a_property.c_str());

	const BWORD isSignal=catalogOrDefault<bool>(a_name,"signal",false);

	if(isSignal)
	{
		const String message=catalogValue(a_name,a_property);
		if(message.empty())
		{
			return e_refused;
		}

		broadcastMessage(a_name,message);
		return e_ok;
	}

	if(cataloged(a_name,a_property) &&
			a_property==FE_STATECATALOG_MESSAGE_PROPERTY)
	{
		const String message=catalogValue(a_name,a_property);
		if(message.empty())
		{
			return e_refused;
		}
		m_flushMessages.push_back(KeyMessage(a_name,message));
		return e_ok;
	}

	//* don't add redundant pairs
	const I32 keyCount=m_flushKeys.size();
	for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const KeyProperty& rKeyProperty=m_flushKeys[keyIndex];
		if(rKeyProperty.m_key==a_name &&
				rKeyProperty.m_property==a_property)
		{
			return e_alreadyAvailable;
		}
	}

	m_flushKeys.push_back(KeyProperty(e_update,a_name,a_property));
	return e_ok;
}

Result ConnectedCatalog::waitForUpdate(I32& a_rFlushCount,I32& a_rSpins,
	volatile BWORD& a_rKeepWaiting,I32 a_microSleep)
{
	a_rSpins=0;

	I32 current=flushCount();
	while(connected() && current<=a_rFlushCount)
	{
		if(!a_rKeepWaiting)
		{
			return e_aborted;
		}

		//* busy wait without locking
		current=flushCount();
		a_rSpins++;

		if(a_microSleep)
		{
			microPause(a_microSleep);
		}
	}
	a_rFlushCount=current;
	return e_ok;
}

Result ConnectedCatalog::lockAfterUpdate(I32& a_rFlushCount,I32& a_rSpins,
	volatile BWORD& a_rKeepWaiting,I32 a_microSleep)
{
	Result result=
			waitForUpdate(a_rFlushCount,a_rSpins,a_rKeepWaiting,a_microSleep);
	if(failure(result))
	{
		return result;
	}

	safeLockShared();
	a_rFlushCount=flushCount();
	return e_ok;
}

void ConnectedCatalog::update(Command a_command,String a_source,
	String a_key,String a_property,
	String a_type,String a_text,const U8* a_pRawBytes,I32 a_byteCount)
{
#if FALSE
	const String commandString=
			(a_command==e_update)? "update":
			((a_command==e_signal)? "signal":
			((a_command==e_remove)? "remove": "unknown" ));
	feLog("ConnectedCatalog::update \"%s\" '%s' \"%s\" \"%s\" \"%s\" \"%s\"\n",
			a_source.c_str(),commandString.c_str(),
			a_key.c_str(),a_property.c_str(),
			a_type.c_str(),a_text.c_str());
#endif

	if(a_command==e_unknown)
	{
		feLog("ConnectedCatalog::update"
				" unknown command on key \"%s\" property \"%s\"\n",
				a_key.c_str(),a_property.c_str());
		return;
	}

	if(a_command==e_update && a_type=="")
	{
		feLog("ConnectedCatalog::update"
				" update without type on key \"%s\" property \"%s\"\n",
				a_key.c_str(),a_property.c_str());
		return;
	}

	//* send signals immediately
	if(a_command==e_signal)
	{
		if(m_lockOnUpdate)
		{
			safeLock();
		}

		catalog<bool>(a_key,"signal")=true;

		updateState(
				a_key,
				a_property,
				a_type,
				a_text);

		if(!a_source.empty())
		{
			catalog<String>(a_key,"source")=a_source;

			const BWORD isMessage=
					(a_property==FE_STATECATALOG_MESSAGE_PROPERTY);
			queueRepeat(a_command,a_key,a_property,
					isMessage? a_text: String(),a_source,NULL,0);
		}

		//* should we call incrementSerial()?

		if(m_lockOnUpdate)
		{
			safeUnlock();
		}

		sendNotifications();

		//* TODO remove signal after notifications sent

		return;
	}

	//* TODO on server, differentiate flush per source

	if(a_key=="net:flushed")
	{
		if(m_lockOnUpdate)
		{
			safeLock();
		}

		const I32 flushed=atoi(a_text.c_str());
		I32 changeCount=m_changes.size();
		BWORD allowUpdate=TRUE;
		BWORD changed=FALSE;

		I32 changeMatches=0;
		for(I32 changeIndex=0;changeIndex<changeCount;changeIndex++)
		{
			const Change& change=m_changes[changeIndex];
			const String& source=change.m_source;
#if FALSE
			feLog("CHANGE %d/%d \"%s\" \"%s\" \"%s\" \"%s\"\n",
					changeIndex,changeCount,
					source.c_str(),a_source.c_str(),
					change.m_key.c_str(),change.m_property.c_str());
#endif
			if(source==a_source)
			{
				changeMatches++;
			}
		}

		if(flushed!=changeMatches)
		{
			feLog("ConnectedCatalog::update"
					" flushed %d changeMatches %d/%d\n",
					flushed,changeMatches,changeCount);
			allowUpdate=FALSE;
		}

//		feLog("flushed %d changeMatches %d changeCount %d\n",
//				flushed,changeMatches,changeCount);

		for(I32 changeIndex=0;changeIndex<changeCount;)
		{
			const Change& change=m_changes[changeIndex];

			const String& source=change.m_source;
			if(source!=a_source)
			{
				changeIndex++;
				continue;
			}

			const Command command=change.m_command;
			const String& key=change.m_key;
			const String& property=change.m_property;

			const BWORD isRemoval=(command==e_remove);
			const BWORD isMessage=(property==FE_STATECATALOG_MESSAGE_PROPERTY);

			if(allowUpdate || isRemoval || isMessage)
			{
				if(isRemoval)
				{
					clearState(
							key,
							property);
				}
				else if(change.m_pRawBytes && change.m_byteCount)
				{
					updateState(
							key,
							property,
							change.m_type,
							change.m_pRawBytes,
							change.m_byteCount);
				}
				else
				{
					updateState(
							key,
							property,
							change.m_type,
							change.m_text);
				}
				changed=TRUE;

				if(!source.empty())
				{
					catalog<String>(key,"source")=source;
				}

				if(key=="frame")
				{
					static I32 lastFrame=0;
					const I32 frame=catalog<I32>("frame");
					if(frame && frame!=lastFrame+1)
					{
						feLog("ConnectedCatalog::update"
								" frame %d -> %d\n",
								lastFrame,frame);
					}
					lastFrame=frame;
				}
			}
			else
			{
				feLog("  \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",
						source.c_str(),
						key.c_str(),
						change.m_property.c_str(),
						change.m_type.c_str(),
						change.m_text.c_str());
			}

			if(!source.empty())
			{
				queueRepeat(command,key,property,
						isMessage? change.m_text: String(),change.m_source,
						change.m_pRawBytes,change.m_byteCount);
			}
			else if(change.m_pRawBytes)
			{
				free(change.m_pRawBytes);
				m_changes[changeIndex].m_pRawBytes=NULL;
			}

			//* copy last element to this slot and shrink array by one
			changeCount--;
			if(changeIndex<changeCount)
			{
				m_changes[changeIndex]=m_changes[changeCount];
			}
		}

		if(changed)
		{
			incrementSerial();
			incrementFlushCount();
		}

		if(changeCount)
		{
			m_changes.resize(changeCount);
		}
		else
		{
			m_changes.clear();
		}

		if(m_lockOnUpdate)
		{
			safeUnlock();
		}

		sendNotifications();
		return;
	}
	else if(strncmp(a_key.c_str(),"net:",4))
	{
		Change change;
		change.m_command=a_command;
		change.m_source=a_source;
		change.m_key=a_key;
		change.m_property=a_property;
		change.m_type=a_type;
		change.m_text=a_text;
		if(a_pRawBytes && a_byteCount)
		{
			change.m_byteCount=a_byteCount;
			change.m_pRawBytes=(U8*)malloc(a_byteCount);
			memcpy(change.m_pRawBytes,a_pRawBytes,a_byteCount);
		}
		m_changes.push_back(change);
		return;
	}

	//* "net:*" keys only

	const BWORD isTick=(a_key=="net:tick");

	if(m_lockOnUpdate)
	{
		safeLock();
	}

	if(a_command==e_remove)
	{
		catalogRemove(a_key,a_property);
	}
	else
	{
		catalogSet(a_key,a_property,a_type,a_text);
	}

	if(!isTick)
	{
		incrementSerial();
		incrementFlushCount();
	}

	if(role() == "server" && !strncmp(a_key.c_str(),"net:listening",13))
	{
		I32 identityIndex=0;
		while(identityIndex<m_identityCount)
		{
			Identity& rIdentity=m_identityArray[identityIndex];
			const String listenKey("net:listening["+rIdentity.m_text+"]");

			if(!catalogOrDefault<bool>(listenKey,true))
			{
				feLog("ConnectedCatalog::update"
						" remove identity \"%s\"\n",rIdentity.m_text.c_str());

				removeIdentity(identityIndex);

				//* other identity can assume that index
				continue;
			}
			identityIndex++;
		}
	}

	if(connected())
	{
		if(role() == "client" && !catalog<bool>("net:broadcasting"))
		{
			requestDisconnect();
		}
	}

	if(!disconnecting())
	{
		catchUpAsNeeded();
	}

	if(m_lockOnUpdate)
	{
		safeUnlock();
	}
}

Result ConnectedCatalog::removeIdentity(I32 a_index)
{
	if(a_index<m_identityCount-1)
	{
		m_identityArray[a_index]=m_identityArray[m_identityCount-1];
	}
	m_identityArray[m_identityCount-1].reset();	//* just to be sure

	m_identityCount--;

#if !FE_CTC_PERSISTENT
		//* kill the server if no clients left
		if(!m_identityCount && connected())
		{
			requestDisconnect();
		}
#endif

	return e_ok;
}

Result ConnectedCatalog::removeIdentity(const Identity& a_rIdentity)
{
	for(I32 identityIndex=0;identityIndex<m_identityCount;identityIndex++)
	{
		if(a_rIdentity==m_identityArray[identityIndex])
		{
			Result result=removeIdentity(identityIndex);
			if(failure(result))
			{
				return result;
			}

			return e_ok;
		}
	}

	return e_notNeeded;
}

ConnectedCatalog::ConnectionTask::ConnectionTask(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ConnectionTask::ConnectionTask()\n");
#endif
}

ConnectedCatalog::ConnectionTask::~ConnectionTask(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ConnectionTask::~ConnectionTask()\n");
#endif
}

void ConnectedCatalog::ConnectionTask::operate(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ConnectionTask::operate connect\n");
#endif

	m_pConnectedCatalog->m_connectionResult=m_pConnectedCatalog->connect();

#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ConnectionTask::operate done\n");
#endif
}

ConnectedCatalog::ServiceTask::ServiceTask(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ServiceTask::ServiceTask()\n");
#endif
}

ConnectedCatalog::ServiceTask::~ServiceTask(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ServiceTask::~ServiceTask()\n");
#endif
}

void ConnectedCatalog::ServiceTask::operate(void)
{
#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ServiceTask::operate\n");
#endif

	while(TRUE)
	{
		if(!m_pConnectedCatalog)
		{
			feLog("ConnectedCatalog::ServiceTask::operate"
					" ConnectedCatalog disappeared\n");
			break;
		}

		if(m_pConnectedCatalog->aborting())
		{
#if FE_CTC_DEBUG
			feLog("ConnectedCatalog::ServiceTask::operate aborting\n");
#endif
			break;
		}

		if(m_pConnectedCatalog->connected())
		{
			m_pConnectedCatalog->service();
		}

		m_pConnectedCatalog->serviceYield();
	}

#if FE_CTC_DEBUG
	feLog("ConnectedCatalog::ServiceTask::operate done\n");
#endif
}

void ConnectedCatalog::service(void)
{
	if(role()=="server")
	{
//		sendNotifications();

		const int signalsQueued=m_signalsQueued.load(
				std::memory_order_relaxed);

#if FE_CTC_VERBOSE
		feLog("ConnectedCatalog::ServiceTask::operate signalsQueued %d\n",
				signalsQueued);
#endif

		if(signalsQueued)
		{
//			feLog("ConnectedCatalog::ServiceTask::operate flush signals\n");

			safeLock();
			flushRepeats(TRUE);
			safeUnlock();
		}

		if(tickerReached())
		{
			safeLock();
			broadcastTick();
			safeUnlock();

			tickerRestart();
		}
	}
}

} /* namespace ext */
} /* namespace fe */
