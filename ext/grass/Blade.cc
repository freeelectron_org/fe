/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

#define FE_BLADE_DEBUG				FALSE
#define FE_BLADE_COLLISION_DEBUG	FALSE
#define FE_BLADE_BOW_DEBUG			FALSE
#define FE_BLADE_BUCKLE_DEBUG		FALSE
#define FE_BLADE_XFORM_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

// NOTE dx and dv actually refer to angle and angular velocity,
// but we keep the familiar notation

Blade::Blade(void)
{
	//* current using uniform properties
	m_tension=1e4;
	m_tensionRandom=0.0;
	m_drag=1e2;
	m_dragRandom=0.0;
	m_bend=0.0;
	m_mass=1e0;
	m_h=1.0/60.0;
	m_t=0.0;
	m_grabbing=FALSE;
	m_grabBias=0.0;
	m_grabBiasRandom=0.0;
	m_collisionGap=0.0;
	m_buckling=FALSE;
	m_breakAngle=1.0;
	m_breakAngleRandom=0.0;
	m_elasticAngle=0.1;
	m_elasticAngleRandom=0.0;
	m_popAngle=0.8;
	m_popAngleRandom=0.0;
	m_recoveryAngle=0.01;
	m_recoveryAngleRandom=0.0;

	m_restoration=0.0;
	m_restorationRandom=0.0;

	m_wind=0.0;
	m_turbulence=0.0;

	populate();
}

void Blade::populate(void)
{
//	0	4	8	12
//	1	5	9	13
//	2	6	10	14
//	3	7	11	15

//	-2	1	0	0
//	1	-2	1	0   * k  (or c)
//	0	1	-2	1
//	0	0	1	-1

// non-symetrical?
//	-1	0	0	0
//	-1	-1	0	0   * restoration
//	-1	-1	-1	0
//	-1	-1	-1	-1

	set(m_dfdx);
	m_dfdx[0]= -2.0;
	m_dfdx[1]=1.0;

	m_dfdx[4]=1.0;
	m_dfdx[5]= -2.0;
	m_dfdx[6]=1.0;

	m_dfdx[9]=1.0;
	m_dfdx[10]= -2.0;
	m_dfdx[11]=1.0;

	m_dfdx[14]=1.0;
	m_dfdx[15]= -1.0;

	//* NOTE not accounting for randomness in tension, drag, and restoration

	Matrix<4,4,Real> dfdv=m_dfdx;
	m_dfdx*=m_tension;
	dfdv*=m_drag;

	m_dfdx[0]-=m_restoration;
	m_dfdx[1]-=m_restoration;
	m_dfdx[2]-=m_restoration;
	m_dfdx[3]-=m_restoration;
	m_dfdx[5]-=m_restoration;
	m_dfdx[6]-=m_restoration;
	m_dfdx[7]-=m_restoration;
	m_dfdx[10]-=m_restoration;
	m_dfdx[11]-=m_restoration;
	m_dfdx[15]-=m_restoration;

	Matrix<4,4,Real> id;
	setIdentity(id);
	for(U32 m=0;m<4;m++)
	{
		id(m,m)*=4-m;
	}
	Matrix<4,4,Real> A=m_mass*id-m_h*dfdv-m_h*m_h*m_dfdx;
	jad_invert(m_invA,A);

#if FE_BLADE_DEBUG
	Matrix<4,4,Real> check=A*m_invA;

	feLog("m_h %.6G\n",m_h);
	feLog("m_dfdx %s\n",c_print(m_dfdx));
	feLog("dfdv %s\n",c_print(dfdv));
	feLog("A %s\n",c_print(A));
	feLog("invA %s\n",c_print(m_invA));
	feLog("check %s\n",c_print(check));
#endif

	m_hdfdx=m_h*m_dfdx;

	if(m_spCollider.isValid())
	{
		m_spCollider->prepareForSearch();
	}
}

void Blade::computeAngles(U32 a_index, Vector4& a_rAngle,
	const SpatialVector* a_pDelta)
{
	for(U32 m=0;m<4;m++)
	{
		// projected length onto ground
		const Real radialLength=sqrtf(a_pDelta[m][0]*a_pDelta[m][0]+
				a_pDelta[m][2]*a_pDelta[m][2]);

		SpatialVector projected(a_pDelta[m][0],0.0,a_pDelta[m][2]);
		const Real projectedLength=magnitude(projected);
		Real tandot=1.0;
		if(projectedLength>0.0)
		{
			tandot=dot(m_tangent[a_index],
					unitSafe(SpatialVector(a_pDelta[m][0],0.0,a_pDelta[m][2])));
		}
		a_rAngle[m]=atan2f(tandot*radialLength,a_pDelta[m][1]);
	}
}

void Blade::storeAsRest(U32 a_index)
{
	const U32 index5=a_index*5;

	//* TODO resize in larger grain
	if(m_restAngle.size()<a_index+1)
	{
		m_restAngle.resize(a_index+1);
	}
	if(m_plasticAngle.size()<a_index+1)
	{
		m_plasticAngle.resize(a_index+1);
	}
	if(m_tangentBias.size()<a_index+1)
	{
		m_tangentBias.resize(a_index+1);
	}
	if(m_kink.size()<a_index+1)
	{
		m_kink.resize(a_index+1);
	}
	if(m_kinkForward.size()<a_index+1)
	{
		m_kinkForward.resize(a_index+1);
	}
	if(m_length.size()<a_index+1)
	{
		m_length.resize(a_index+1);
	}
	if(m_y.size()<a_index+1)
	{
		m_y.resize(a_index+1);
	}
	if(m_restLength.size()<a_index+1)
	{
		m_restLength.resize(a_index+1);
	}
	if(m_contact.size()<a_index+1)
	{
		m_contact.resize(a_index+1);
	}
	if(m_location.size()<index5+5)
	{
		m_location.resize(index5+5);
	}
	if(m_locationDef.size()<index5+5)
	{
		m_locationDef.resize(index5+5);
	}
	if(!m_facing.size())
	{
		m_restFacing.resize(0);
	}
	else if(m_restFacing.size()<index5+5)
	{
		m_restFacing.resize(index5+5);
	}

	//* TODO clean up duplication from step()

	const SpatialVector& rLocationRef0=m_locationRef[index5];
	const SpatialVector& rLocationRef1=m_locationRef[index5+1];
	const SpatialVector& rLocationRef2=m_locationRef[index5+2];
	const SpatialVector& rLocationRef3=m_locationRef[index5+3];
	const SpatialVector& rLocationRef4=m_locationRef[index5+4];

	SpatialVector& rLocation0=m_location[index5];
	SpatialVector& rLocation1=m_location[index5+1];
	SpatialVector& rLocation2=m_location[index5+2];
	SpatialVector& rLocation3=m_location[index5+3];
	SpatialVector& rLocation4=m_location[index5+4];

	I32 face= -1;
	if(m_spDriver.isValid() && m_bindFace.size() && m_bindBary.size())
	{
		face=m_bindFace[a_index];
	}
	const BWORD localized=(face>=0 && m_spDeformed.isValid());
	if(localized)
	{
		const SpatialBary barycenter=m_bindBary[a_index];

		const SpatialTransform xformDriver=m_spDriver->sample(face,barycenter);
		SpatialTransform invDriver;
		invert(invDriver,xformDriver);

		transformVector(invDriver,rLocationRef0,rLocation0);
		transformVector(invDriver,rLocationRef1,rLocation1);
		transformVector(invDriver,rLocationRef2,rLocation2);
		transformVector(invDriver,rLocationRef3,rLocation3);
		transformVector(invDriver,rLocationRef4,rLocation4);

#if FE_BLADE_XFORM_DEBUG
		feLog("Blade::storeAsRest %d face %d bary %s\n",
				a_index,face,c_print(barycenter));
		feLog("Blade::storeAsRest xformDriver\n%s\n",c_print(xformDriver));
		feLog("Blade::storeAsRest invDriver\n%s\n",c_print(invDriver));
		feLog("ref0 %s  %s\n",c_print(rLocationRef0),c_print(rLocation0));
		feLog("ref1 %s  %s\n",c_print(rLocationRef1),c_print(rLocation1));
#endif

		// HACK
		SpatialVector& rTangent=m_tangent[a_index];
		rotateVector(invDriver,rTangent,rTangent);

		//* adjust tangent to be perpendicular to normal
		rTangent[1] = 0.0;
		normalizeSafe(rTangent);

		if(m_facing.size())
		{
			rotateVector(invDriver,m_facing[index5],m_facing[index5]);
			rotateVector(invDriver,m_facing[index5+1],m_facing[index5+1]);
			rotateVector(invDriver,m_facing[index5+2],m_facing[index5+2]);
			rotateVector(invDriver,m_facing[index5+3],m_facing[index5+3]);
			rotateVector(invDriver,m_facing[index5+4],m_facing[index5+4]);
		}
	}
	else
	{
		rLocation0=rLocationRef0;
		rLocation1=rLocationRef1;
		rLocation2=rLocationRef2;
		rLocation3=rLocationRef3;
		rLocation4=rLocationRef4;
	}

	if(m_facing.size())
	{
		m_restFacing[index5]=m_facing[index5];
		m_restFacing[index5+1]=m_facing[index5+1];
		m_restFacing[index5+2]=m_facing[index5+2];
		m_restFacing[index5+3]=m_facing[index5+3];
		m_restFacing[index5+4]=m_facing[index5+4];
	}

	// delta segment
	SpatialVector delta[4];
	delta[0]=rLocation1-rLocation0;
	delta[1]=rLocation2-rLocation1;
	delta[2]=rLocation3-rLocation2;
	delta[3]=rLocation4-rLocation3;

	Vector4 angle;
	computeAngles(a_index,angle,delta);

	//* rest angles are relative
	Vector4& rRest=m_restAngle[a_index];
	rRest[0]=angle[0];
	rRest[1]=angle[1]-angle[0];
	rRest[2]=angle[2]-angle[1];
	rRest[3]=angle[3]-angle[2];

	m_plasticAngle[a_index]=rRest;

	m_tangentBias[a_index]=0.0;
	m_kink[a_index]= -1;

	//* segment length
	Vector4& rLength=m_length[a_index];
	rLength[0]=sqrtf(delta[0][0]*delta[0][0]+
			delta[0][1]*delta[0][1]+delta[0][2]*delta[0][2]);
	rLength[1]=sqrtf(delta[1][0]*delta[1][0]+
			delta[1][1]*delta[1][1]+delta[1][2]*delta[1][2]);
	rLength[2]=sqrtf(delta[2][0]*delta[2][0]+
			delta[2][1]*delta[2][1]+delta[2][2]*delta[2][2]);
	rLength[3]=sqrtf(delta[3][0]*delta[3][0]+
			delta[3][1]*delta[3][1]+delta[3][2]*delta[3][2]);

	m_restLength[a_index]=magnitude(rLocation4-rLocation0);

	set(m_y[a_index]);
}

void Blade::step(U32 a_index)
{
	const Color grey(0.3,0.3,0.3,1.0);
	const Color red(1.0,0.0,0.0,1.0);
	const Color pink(1.0,0.5,0.5,1.0);
	const Color green(0.0,1.0,0.0,1.0);
	const Color blue(0.0,0.0,1.0,1.0);
	const Color cyan(0.0,1.0,1.0,1.0);
	const Color magenta(1.0,0.0,1.0,1.0);
	const Color yellow(1.0,1.0,0.3,1.0);
	const Color darkyellow(0.8,0.8,0.1,1.0);

	Real radius=0.0;
	SpatialVector center;
	set(center);
	if(m_spCollider.isValid())
	{
		radius=m_spCollider->radius();
		center=m_spCollider->center();
	}

	const U32 index4=a_index*4;
	const U32 index5=a_index*5;

	SpatialVector from;
	Vector4 push;	// external force
	set(push);

	SpatialVector& rLocation0=m_location[index5];
	SpatialVector& rLocation1=m_location[index5+1];
	SpatialVector& rLocation2=m_location[index5+2];
	SpatialVector& rLocation3=m_location[index5+3];
	SpatialVector& rLocation4=m_location[index5+4];

	I32 face= -1;
	if(m_spDriver.isValid() && m_bindFace.size() && m_bindBary.size())
	{
		face=m_bindFace[a_index];
	}
	const BWORD localized=(face>=0 && m_spDeformed.isValid());
	SpatialTransform xformDriver;
	SpatialTransform xformDeformed;
	SpatialTransform invDeformed;
	if(localized)
	{
		const SpatialBary barycenter=m_bindBary[a_index];
		xformDriver=m_spDriver->sample(face,barycenter);
		xformDeformed=m_spDeformed->sample(face,barycenter);
		invert(invDeformed,xformDeformed);

#if FE_BLADE_XFORM_DEBUG
		feLog("Blade::step xformDriver\n%s\n",c_print(xformDriver));
		feLog("Blade::step xformDeformed\n%s\n",c_print(xformDeformed));
		feLog("Blade::step invDeformed\n%s\n",c_print(invDeformed));
#endif

		transformVector(invDeformed,center,center);
	}
	else if(m_spDrawDebug.isValid())
	{
		setIdentity(xformDeformed);
	}

	//* delta segment
	SpatialVector delta[4];
	delta[0]=rLocation1-rLocation0;
	delta[1]=rLocation2-rLocation1;
	delta[2]=rLocation3-rLocation2;
	delta[3]=rLocation4-rLocation3;

	const SpatialVector& rTangent=m_tangent[a_index];

	//* segment length
	const Vector4& rLength=m_length[a_index];

	//* NOTE root force ignored

	Vector4 tangentForce;
	if(m_forceIn.size())
	{
		SpatialVector localForceIn[4];
		if(localized)
		{
			rotateVector(invDeformed,m_forceIn[index5+1],
					localForceIn[0]);
			rotateVector(invDeformed,m_forceIn[index5+2],
					localForceIn[1]);
			rotateVector(invDeformed,m_forceIn[index5+3],
					localForceIn[2]);
			rotateVector(invDeformed,m_forceIn[index5+4],
					localForceIn[3]);
		}
		else
		{
			localForceIn[0]=m_forceIn[index5+1];
			localForceIn[1]=m_forceIn[index5+2];
			localForceIn[2]=m_forceIn[index5+3];
			localForceIn[3]=m_forceIn[index5+4];
		}

		set(tangentForce,
				dot(rTangent,localForceIn[0]),
				dot(rTangent,localForceIn[1]),
				dot(rTangent,localForceIn[2]),
				dot(rTangent,localForceIn[3]));
	}
	else
	{
		set(tangentForce);
	}

	if(m_velocityIn.size())
	{
		SpatialVector localVelocityIn[4];
		if(localized)
		{
			rotateVector(invDeformed,m_velocityIn[index5+1],
					localVelocityIn[0]);
			rotateVector(invDeformed,m_velocityIn[index5+2],
					localVelocityIn[1]);
			rotateVector(invDeformed,m_velocityIn[index5+3],
					localVelocityIn[2]);
			rotateVector(invDeformed,m_velocityIn[index5+4],
					localVelocityIn[3]);
		}
		else
		{
			localVelocityIn[0]=m_velocityIn[index5+1];
			localVelocityIn[1]=m_velocityIn[index5+2];
			localVelocityIn[2]=m_velocityIn[index5+3];
			localVelocityIn[3]=m_velocityIn[index5+4];
		}

		//* side is cross of normal and tangent
		const SpatialVector side(rTangent[2],0.0,-rTangent[0]);

		SpatialVector crossVelocity[4];
		crossVelocity[0]=cross(delta[0],localVelocityIn[0]);
		crossVelocity[1]=cross(delta[1],localVelocityIn[1]);
		crossVelocity[2]=cross(delta[2],localVelocityIn[2]);
		crossVelocity[3]=cross(delta[3],localVelocityIn[3]);

		Vector4 velocityForce;
		set(velocityForce,
				dot(side,crossVelocity[0]),
				dot(side,crossVelocity[1]),
				dot(side,crossVelocity[2]),
				dot(side,crossVelocity[3]));

		tangentForce+=velocityForce;

		if(m_spDrawDebug.isValid())
		{
			SpatialVector line[2];
			line[0]=m_location[index5];
			line[1]=line[0]+rTangent;
			m_spDrawDebug->drawTransformedLines(xformDeformed,NULL,
					line,NULL,2,DrawI::e_discrete,false,&magenta);

			for(I32 m=0;m<4;m++)
			{
				line[0]=m_location[index5+m+1];
				line[1]=line[0]+1e-3*crossVelocity[m];
				m_spDrawDebug->drawTransformedLines(xformDeformed,NULL,
						line,NULL,2,DrawI::e_discrete,false,&cyan);
			}
		}
	}

#if FALSE
	from=rLocation0-center;
	Real dist2=magnitudeSquared(from);
	if(dist2<radius*radius)
	{
		const Real alignment=dot(rTangent,unitSafe(from));
		setAll(push,5e2*(radius-sqrt(dist2))*alignment);
	}
#endif

	Real& rTangentBias=m_tangentBias[a_index];
	I32& rKink=m_kink[a_index];
	I32& rKinkForward=m_kinkForward[a_index];

	//* TODO pre-compute
	Vector4 coneAngle(
			atan(m_collisionGap/rLength[0]),
			atan(m_collisionGap/rLength[1]),
			atan(m_collisionGap/rLength[2]),
			atan(m_collisionGap/rLength[3]));
	const Real maxConeAngle=0.3;	//* TODO param
	for(I32 m=0;m<4;m++)
	{
		if(coneAngle[m]>maxConeAngle)
		{
			coneAngle[m]=maxConeAngle;
		}
	}

	Vector4 a;		// angle
	computeAngles(a_index,a,delta);

	//* simple wind in positive X direction
	if(m_wind>0.0 || m_turbulence>0.0)
	{
		const Real locX=rLocation0[0];
		const Real locZ=rLocation0[2];
		const Real wind=rTangent[0]*
				(m_wind+m_turbulence*sin(locX+m_t)*sin(locZ+m_t));
		push+=Vector4(wind,wind,wind,wind);
	}

	push*=Vector4(cos(a[0]),cos(a[1]),cos(a[2]),cos(a[3]));

	Vector4 w;		// angular velocity
	set(w,m_velocity[index4],
			m_velocity[index4+1],
			m_velocity[index4+2],
			m_velocity[index4+3]);

	Vector4 plasticCurrent=m_plasticAngle[a_index]+
			m_bend*Vector4(1.0,1.0,1.0,1.0);

	Vector4 restAbsolute=
			m_restAngle[a_index]+m_bend*Vector4(1.0,1.0,1.0,1.0);
	restAbsolute[1]+=restAbsolute[0];
	restAbsolute[2]+=restAbsolute[1];
	restAbsolute[3]+=restAbsolute[2];

	const U32 seed=0;	//* TODO param
	U32 oneSeed=seed+a_index;
	const Real tension=m_tension+
			m_tensionRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real drag=m_drag+m_dragRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real restore=m_restoration+
			m_restorationRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real grabBias=m_grabBias+
			m_grabBiasRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real breakAngle=m_breakAngle+
			m_breakAngleRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real elasticAngle=m_elasticAngle+
			m_elasticAngleRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real popAngle=m_popAngle+
			m_popAngleRandom*randomRealReentrant(&oneSeed,-1.0,1.0);
	const Real recoveryAngle=m_recoveryAngle+
			m_recoveryAngleRandom*randomRealReentrant(&oneSeed,-1.0,1.0);

	//* TODO
	const Vector4 tensionRamp=tension*Vector4(1.0,1.0,1.0,1.0);

	Vector4& y=m_y[a_index];

	Vector4 f;		// force
	set(f, -tensionRamp[0]*(a[0]-plasticCurrent[0])
			-drag*(w[0])+
			tensionRamp[0]*(a[1]-a[0]-plasticCurrent[1])+drag*(w[1]-w[0])-
			restore*(a[0]-restAbsolute[0]),

			-tensionRamp[1]*(a[1]-a[0]-y[0]-plasticCurrent[1])
			-drag*(w[1]-w[0])+
			tensionRamp[1]*(a[2]-a[1]-plasticCurrent[2])+drag*(w[2]-w[1])-
			restore*(a[1]-restAbsolute[1]),

			-tensionRamp[2]*(a[2]-a[1]-y[1]-plasticCurrent[2])
			-drag*(w[2]-w[1])+
			tensionRamp[2]*(a[3]-a[2]-plasticCurrent[3])+drag*(w[3]-w[2])-
			restore*(a[2]-restAbsolute[2]),

			-tensionRamp[3]*(a[3]-a[2]-y[2]-plasticCurrent[3])
			-drag*(w[3]-w[2])-
			restore*(a[3]-restAbsolute[3]));

	Contact& rContact=m_contact[a_index];

	//* no wind while grabbing
	//* TODO fix jumpiness near edges
	if(!rContact.active())
	{
		f+=push;
		f+=tangentForce;
	}

	//* TODO do we need to use the last correction
	set(y);

	const Real totalForce=fabs(f[0])+fabs(f[1])+fabs(f[2])+fabs(f[3]);

	Vector4 b=m_h*(f+m_hdfdx*w+m_dfdx*y);
	Vector4 dv=m_invA*b;

#if FE_BLADE_DEBUG
	feLog("m_h %.6G\n",m_h);
	feLog("tangent %s\n",c_print(rTangent));
	feLog("push %s\n",c_print(push));
	feLog("tangentForce %s\n",c_print(tangentForce));
	feLog("plasticCurrent %s\n",c_print(plasticCurrent));
	feLog("restAbsolute %s\n",c_print(restAbsolute));
	feLog("f %s\n",c_print(f));
	feLog("b %s\n",c_print(b));
	feLog("dv %s\n",c_print(dv));
	feLog("length %s\n",c_print(rLength));
	feLog("a %s before\n",c_print(a));
	feLog("w %s\n",c_print(w));
#endif

	w+=dv;
	a+=m_h*w+y;
	set(y);

	//* limit angles
	const Real limit=3.0;
	for(U32 m=0;m<4;m++)
	{
		if(a[m]>limit)
		{
			a[m]=limit;
		}
		else if(a[m]< -limit)
		{
			a[m]= -limit;
		}
	}

	//* TODO work this out properly
	const Real partX=rTangent[0];
	const Real partZ=rTangent[2];

	BWORD relocated=FALSE;
	for(I32 pass=0;pass<1+relocated;pass++)
	{
		const Real lsa0=rLength[0]*sin(a[0]);
		rLocation1=rLocation0+
				SpatialVector(lsa0*partX,rLength[0]*cosf(a[0]),lsa0*partZ);

		const Real lsa1=rLength[1]*sin(a[1]);
		rLocation2=rLocation1+
				SpatialVector(lsa1*partX,rLength[1]*cosf(a[1]),lsa1*partZ);

		const Real lsa2=rLength[2]*sin(a[2]);
		rLocation3=rLocation2+
				SpatialVector(lsa2*partX,rLength[2]*cosf(a[2]),lsa2*partZ);

		const Real lsa3=rLength[3]*sin(a[3]);
		rLocation4=rLocation3+
				SpatialVector(lsa3*partX,rLength[3]*cosf(a[3]),lsa3*partZ);

		if(pass)
		{
			break;
		}

		if(m_spDrawDebug.isValid())
		{
			m_spDrawDebug->drawTransformedLines(xformDeformed,NULL,
					&m_location[index5],NULL,5,DrawI::e_strip,false,&grey);
		}

		if(m_spCollider.isNull())
		{
			break;
		}

		//* collision displacement
		for(U32 m=0;m<4;m++)
		{
			const Real extendedRadius=radius+m_collisionGap;
			const SpatialVector& base=m_location[index5+m];

			const SpatialVector toCenter=center-base;
			const Real reach=extendedRadius+rLength[m];
			if(magnitudeSquared(toCenter)<reach*reach)
			{
				BWORD applyCorrection=FALSE;
				SpatialVector correction;
				SpatialVector intersection;
				SpatialVector normal(0.0,1.0,0.0);

#if FE_BLADE_COLLISION_DEBUG
				feLog("%d angle %.6G\n",m,a[m]);
#endif

				if(m_method==e_boundingSphere)
				{
					//* point line-segment distance
					const Real sam=sinf(a[m]);
					const SpatialVector span(partX*sam,cosf(a[m]),partZ*sam);
					Real along=dot(toCenter,span)/rLength[m];
					if(along<0.0f)
					{
						from=base-center;
					}
					else if(along>1.0f)
					{
						from=base+rLength[m]*span-center;
					}
					else
					{
						SpatialVector projection=base+along*rLength[m]*span;
						from=projection-center;
					}

					Real distance=magnitude(from);
					if(distance>0.0f && distance<extendedRadius)
					{
						applyCorrection=TRUE;
						normal=from/distance;
						correction=normal*(extendedRadius-distance);

						//* for tip contact
						intersection=center+
								extendedRadius*unitSafe(rLocation4-center);
					}
#if FE_BLADE_COLLISION_DEBUG
					feLog("dist %.6G along %.6G\n",distance,along);
#endif
				}
				else if(m_method==e_nearestPoint)
				{
					const Real maxDistance= -1.0;
					SpatialVector point=m_location[index5+m+1];
					sp<SurfaceI::ImpactI> spImpact=localized?
							m_spCollider->nearestPoint(invDeformed,
							point,maxDistance):
							m_spCollider->nearestPoint(point,maxDistance);
					if(spImpact.isValid())
					{
						normal=spImpact->normal();
						intersection=spImpact->intersection()+
								m_collisionGap*normal;
						SpatialVector proposed=intersection-point;
						const Real outward=dot(proposed,normal);
#if FE_BLADE_COLLISION_DEBUG
						feLog("outward %.6G\n",outward);
#endif
						if(outward>0.0)
						{
							applyCorrection=TRUE;
							correction=outward*normal;
						}

						if(m_spDrawDebug.isValid())
						{
							SpatialVector line[2];
							line[0]=point;
							line[1]=spImpact->intersection();
							m_spDrawDebug->drawTransformedLines(
									xformDeformed,NULL,
									line,NULL,2,
									DrawI::e_discrete,false,&yellow);

							line[0]=spImpact->intersection();
							line[1]=line[0]+normal;
							m_spDrawDebug->drawTransformedLines(
									xformDeformed,NULL,
									line,NULL,2,
									DrawI::e_discrete,false,&pink);
						}
					}
				}
				else if(m_method==e_rayCastOneStep)
				{
					const BWORD anyHit=FALSE;
					const SpatialVector point=m_location[index5+m];
					const SpatialVector direction=
							unitSafe(m_location[index5+m+1]-point);
					sp<SurfaceI::ImpactI> spImpact=localized?
							m_spCollider->rayImpact(invDeformed,
							point,direction,rLength[m],anyHit):
							m_spCollider->rayImpact(
							point,direction,rLength[m],anyHit);
					if(spImpact.isValid())
					{
						normal=spImpact->normal();
						const Real normdot=dot(normal,direction);

#if FE_BLADE_COLLISION_DEBUG
						feLog("point %s\n",c_print(point));
						feLog("direction %s\n",c_print(direction));
						feLog("normdot %.6G distance %.6G vs %.6G\n",
								normdot,spImpact->distance(),rLength[m]);
#endif

						if(normdot>0.0 || spImpact->distance()<rLength[m])
						{
							intersection=spImpact->intersection();

							const SpatialVector point2=m_location[index5+m+1];
							SpatialVector side;
							cross(side,direction,rTangent);
							SpatialVector direction2;
							cross(direction2,side,direction);
							normalize(direction2);
							const Real normdot2=dot(normal,direction2);
							if(normdot2<0.0)
							{
								direction2= -direction2;
							}

#if FE_BLADE_COLLISION_DEBUG
							feLog("tangent %s\n",c_print(rTangent));
							feLog("side %s\n",c_print(side));
							feLog("normdot2 %.6G\n",normdot2);
							feLog("direction2 %s\n",c_print(direction2));
#endif

							sp<SurfaceI::ImpactI> spImpact2=localized?
									m_spCollider->rayImpact(invDeformed,
									point2,direction2,rLength[m],anyHit):
									m_spCollider->rayImpact(
									point2,direction2,rLength[m],anyHit);
							if(spImpact2.isValid())
							{
								applyCorrection=TRUE;
								correction=direction2*spImpact2->distance();

								//* replace
								intersection=spImpact2->intersection()+
										m_collisionGap*normal;
							}

							if(m_spDrawDebug.isValid())
							{
								SpatialVector line[2];

								if(spImpact2.isValid())
								{
									line[0]=point2;
									line[1]=spImpact2->intersection();
									m_spDrawDebug->drawTransformedLines(
											xformDeformed,NULL,
											line,NULL,2,
											DrawI::e_discrete,false,&yellow);

									line[0]=spImpact2->intersection();
									line[1]=point2+direction2;
									m_spDrawDebug->drawTransformedLines(
											xformDeformed,NULL,line,NULL,2,
											DrawI::e_discrete,false,
											&darkyellow);
								}
								else
								{
									line[0]=point2;
									line[1]=point2+direction2;
											m_spDrawDebug->drawTransformedLines(
											xformDeformed,NULL,line,NULL,2,
											DrawI::e_discrete,false,
											&darkyellow);
								}

								line[0]=spImpact->intersection();
								line[1]=line[0]+normal;
								m_spDrawDebug->drawTransformedLines(
										xformDeformed,NULL,line,NULL,2,
										DrawI::e_discrete,false,&pink);
							}
						}
						else if(m_spDrawDebug.isValid())
						{
							intersection=spImpact->intersection();

							SpatialVector line[2];
							line[0]=point+direction*rLength[m];
							line[1]=spImpact->intersection();
							m_spDrawDebug->drawTransformedLines(
									xformDeformed,NULL,line,NULL,2,
									DrawI::e_discrete,false,&darkyellow);

							line[0]=intersection;
							line[1]=intersection+normal;
							m_spDrawDebug->drawTransformedLines(
									xformDeformed,NULL,line,NULL,2,
									DrawI::e_discrete,false,&pink);
						}
					}
				}
				else if(m_method==e_rayCastHalvsies)
				{
					const SpatialVector point=m_location[index5+m];
					SpatialVector direction=
							unitSafe(m_location[index5+m+1]-point);
					const SpatialVector origDirection=direction;
					const Real extendedLength=rLength[m]+m_collisionGap;

					sp<SurfaceI::ImpactI> spImpact=localized?
							m_spCollider->coneImpact(invDeformed,
							point,direction,extendedLength,
							coneAngle[m],m_spDrawDebug):
							m_spCollider->coneImpact(
							point,direction,extendedLength,
							coneAngle[m],m_spDrawDebug);
					if(spImpact.isValid())
					{
//						intersection=point+distance*direction;

						intersection=spImpact->intersection();

						Real response=1.0;
						if(m_collisionGap>0.0)
						{
//							const SpatialVector diff=intersection-point;
//							const Real along=dot(direction,diff);
//							const SpatialVector away=diff-along*direction;
//							const Real aside=magnitude(away);
//							const Real fullAside=
//									m_collisionGap*along/rLength[m];
//							response-=aside/fullAside;

							const Real distance=spImpact->distance();
							if(distance>rLength[m])
							{
								response*=1.0-(distance-rLength[m])/
										m_collisionGap;
							}
						}

						normal=spImpact->normal();
						const Real tandot=dot(rTangent,normal)+rTangentBias;

						SpatialVector lower=direction;
						SpatialVector upper;
						if(tandot>0.0)
						{
							upper=rTangent;
							rTangentBias=1.0;
						}
						else
						{
							upper=-rTangent;
							rTangentBias= -1.0;
						}

						spImpact=localized?
								m_spCollider->coneImpact(invDeformed,
								point,upper,extendedLength,
								coneAngle[m],m_spDrawDebug):
								m_spCollider->coneImpact(
								point,upper,extendedLength,
								coneAngle[m],m_spDrawDebug);
						BWORD hit=(spImpact.isValid());

						if(hit)
						{
							direction=upper;
//							intersection=spImpact->intersection();
						}

						if(m_spDrawDebug.isValid())
						{
							SpatialVector line[2];
							line[0]=point;
							line[1]=point+direction*extendedLength;
							m_spDrawDebug->drawTransformedLines(
									xformDeformed,NULL,line,NULL,2,
									DrawI::e_discrete,false,
									hit? &darkyellow: &yellow);
						}

						if(!hit)
						{
							for(I32 n=0;n<m_halfingSteps;n++)
							{
								direction=unitSafe(lower+upper);
								spImpact=localized?
										m_spCollider->coneImpact(invDeformed,
										point,direction,extendedLength,
										coneAngle[m],m_spDrawDebug):
										m_spCollider->coneImpact(
										point,direction,extendedLength,
										coneAngle[m],m_spDrawDebug);
								hit=FALSE;
								if(spImpact.isValid())
								{
									hit=TRUE;
//									intersection=spImpact->intersection();
									lower=direction;
								}
								else
								{
									upper=direction;
								}

								if(m_spDrawDebug.isValid())
								{
									SpatialVector line[2];
									line[0]=point;
									line[1]=point+direction*extendedLength;
									m_spDrawDebug->drawTransformedLines(
											xformDeformed,NULL,line,NULL,2,
											DrawI::e_discrete,false,
											hit? &pink: &green);
								}

								direction=upper;
							}
						}
						if(response<1.0)
						{
							direction=unitSafe(origDirection+
									response*(direction-origDirection));
						}

						const SpatialVector to=point+rLength[m]*direction;

						applyCorrection=TRUE;
						correction=to-m_location[index5+m+1];
					}
					else
					{
						//* no hit -> decay bias
						rTangentBias*=0.8;
					}
				}

				if(applyCorrection)
				{
					delta[m]=m_location[index5+1+m]-m_location[index5+m];

					const SpatialVector d_mod=delta[m]+correction;
					const Real r_mod=sqrtf(d_mod[0]*d_mod[0]+d_mod[2]*d_mod[2]);
					const Real tandot=dot(rTangent,
							unitSafe(SpatialVector(d_mod[0],0.0,d_mod[2])));
					const Real a_mod=atan2f(tandot*r_mod,d_mod[1]);

					y[m]=a_mod-a[m];
					w[m]=0.0;

#if FE_BLADE_COLLISION_DEBUG
					feLog("  correction %s\n",c_print(correction));
					feLog("  delta[m] %s\n",c_print(delta[m]));
					feLog("  normal %s\n",c_print(normal));
					feLog("  d_mod %s\n",c_print(d_mod));
					feLog("  r_mod %.6G tandot %.6G\n",r_mod,tandot);
					feLog("  angle %.6G y[m] %.6G\n",a_mod,y[m]);
#endif
				}

				//* tip contact
				if(!m_grabbing && m==3)
				{
					if(rContact.active())
					{
						rContact.clear();
					}
				}
				else if(m==3)
				{
					//* TODO margin
					const Real maxDist=m_restLength[a_index];

					Real dotImpact=2.0;
					if(rContact.active())
					{
						dotImpact=dot(unitSafe(rLocation4-rLocation0),
								rContact.normal());
#if FE_BLADE_COLLISION_DEBUG
						feLog("dotImpact %.6G normal %s\n",
								dotImpact,c_print(rContact.normal()));
#endif
						if(dotImpact<grabBias)
						{
							const Real currentLength2=magnitudeSquared(
									center+rContact.offset()-rLocation0);
							if(currentLength2 > maxDist*maxDist)
							{
#if FE_BLADE_BOW_DEBUG
								feLog("contact %d far %.6G vs %.6G\n",a_index,
										sqrtf(currentLength2),maxDist);
#endif
								dotImpact=2.0;
							}
						}
					}
					else if(applyCorrection)
					{
						dotImpact=dot(unitSafe(rLocation4-rLocation0),normal);
					}

					const Real gripForce=0.1*tensionRamp[m];	//* TODO param
					if(dotImpact<grabBias)
					{
						if(!rContact.active() && totalForce<gripForce)
						{
							const Real proposedLength2=magnitudeSquared(
									intersection-rLocation0);
							if(proposedLength2 < maxDist*maxDist)
							{
								//* TODO should be inv transform, not just delta
								rContact.setOffset(intersection-center);
								rContact.setNormal(normal);
#if FE_BLADE_BOW_DEBUG
								feLog("contact %d set offset %s\n normal %s\n",
										a_index,c_print(rContact.offset()),
										c_print(rContact.normal()));
								feLog("  center %s\n intersection %s\n",
										c_print(center),c_print(intersection));
								feLog("  totalForce %.6G\n",totalForce);
#endif
							}
							else
							{
#if FE_BLADE_BOW_DEBUG
								feLog("contact %d too far %.6G vs %.6G\n",
										a_index,
										sqrtf(proposedLength2),maxDist);
#endif
							}

						}
					}
					else if(rContact.active())
					{
#if FE_BLADE_BOW_DEBUG
						feLog("contact %d release %s dotImpact %.6G\n",
								a_index,c_print(rContact.offset()),dotImpact);
#endif
						rContact.clear();
					}
				}
				if(applyCorrection)
				{
					relocated=TRUE;
				}
			}
			else if(m==3 && rContact.active())
			{
#if FE_BLADE_BOW_DEBUG
				feLog("contact %d clear %s\n",
						a_index,c_print(rContact.offset()));
#endif
				rContact.clear();
			}
			if(relocated)
			{
				//* TODO are we really going to use y back in the implicit?

				const Real a_mod=a[m]+y[m];
				const Real lsam=rLength[m]*sin(a_mod);
				m_location[index5+1+m]=m_location[index5+m]+
						SpatialVector(lsam*partX,rLength[m]*cosf(a_mod),
						lsam*partZ);
			}
		}
		a+=y;

#if FE_BLADE_DEBUG
		feLog("a %s after\n",c_print(a));
		feLog("w %s\n",c_print(w));
#endif

		//* tip-constrained bowing
		if(rContact.active())
		{
			const SpatialVector target=center+rContact.offset();
			const SpatialVector span=target-rLocation0;
			const Real rise=span[1];
			const Real tandot=dot(rTangent,
					unitSafe(SpatialVector(span[0],0.0,span[2])));
			const Real tangential=tandot*sqrtf(span[0]*span[0]+span[2]*span[2]);
			const Real rotation=atan2f(tangential,rise);

			const Real distance=sqrtf(rise*rise+tangential*tangential);
			const Real ratio0=(rLength[0]>0.0)? 0.25*distance/rLength[0]: 1.0;
			const Real radians0=(ratio0<1.0)? acos(ratio0): 0.0;

			Real rounding=0.4-0.2*radians0;	//* tweak
			if(rounding<0.0)
			{
				rounding=0.0;
			}

			a[0]= -(1.0+rounding)*radians0;

			const Real dist0=rLength[0]*cos(a[0]);
			const Real ratio1=
					(rLength[1]>0.0)? (0.5*distance-dist0)/rLength[1]: 1.0;
			const Real radians1=(ratio1<1.0)? acos(ratio1): 0.0;

			a[1]= -radians1;
			a[2]= -a[1];
			a[3]= -a[0];

/*
			feLog("target %s\n",c_print(target));
			feLog(" span %s\n",c_print(span));
			feLog(" tandot %.6G tangential %.6G rotation %.6G\n",
					tandot,tangential,rotation);
			feLog(" distance %.6G ratio0 %.6G radians0 %.6G rounding %.6G\n",
					distance,ratio0,radians0,rounding);
			feLog(" ratio1 %.6G radians1 %.6G\n",ratio1,radians1);
			feLog(" length %s\n",c_print(rLength));
			feLog(" a %s\n",c_print(a));
*/

			a+=rotation*Vector4(1.0,1.0,1.0,1.0);

			set(w);
		}
	}

	Vector4& rRestOriginal=m_restAngle[a_index];
	Vector4& rRestPlastic=m_plasticAngle[a_index];
	if(m_buckling && !rContact.active())
	{
		//* TODO account for m_bend
		const Vector4 relativeAngle=a-Vector4(0.0,a[0],a[1],a[2]);
#if FE_BLADE_BUCKLE_DEBUG
		feLog("Blade::step check %d angle %s\n rel %s\n"
				" restOrig %s\n restPlas %s\n",
				rKink,c_print(a),c_print(relativeAngle),
				c_print(rRestOriginal),c_print(rRestPlastic));
#endif
		I32 minIndex=rKink;
		I32 maxIndex=rKink;
		if(rKink<0)
		{
			minIndex=0;
			maxIndex=0;
			for(U32 m=1;m<4;m++)
			{
				if(relativeAngle[minIndex]>relativeAngle[m])
				{
					minIndex=m;
				}
				if(relativeAngle[maxIndex]<relativeAngle[m])
				{
					maxIndex=m;
				}
			}
		}
		if((rKink<0 &&
				relativeAngle[maxIndex]>rRestOriginal[maxIndex]+breakAngle) ||
				(rKink>=0 && rKinkForward &&
				relativeAngle[maxIndex]>rRestPlastic[maxIndex]+elasticAngle))
		{
			rKink=maxIndex;
			rKinkForward=TRUE;
#if FE_BLADE_BUCKLE_DEBUG
			feLog("Blade::step buckle %d forward %.6G > %.6G + %.6G (%.6G)\n",
					rKink,relativeAngle[rKink],
					rRestOriginal[rKink],breakAngle,
					rRestPlastic[rKink]);
			feLog(" a %s\n",c_print(a));
#endif
			//* buckle forward
			rRestPlastic=rRestOriginal;
//			rRestPlastic[rKink]=
//					rRestOriginal[rKink]+breakAngle-elasticAngle;
			rRestPlastic[rKink]=
					relativeAngle[rKink]-elasticAngle;
		}
		else if((rKink<0 &&
				relativeAngle[minIndex]<rRestOriginal[minIndex]-breakAngle) ||
				(rKink>=0 && !rKinkForward &&
				relativeAngle[minIndex]<rRestPlastic[minIndex]-elasticAngle))
		{
			rKink=minIndex;
			rKinkForward=FALSE;
#if FE_BLADE_BUCKLE_DEBUG
			feLog("Blade::step buckle %d back %.6G < %.6G - %.6G (%.6G)\n",
					rKink,relativeAngle[rKink],
					rRestOriginal[rKink],breakAngle,
					rRestPlastic[rKink]);
			feLog(" a %s\n",c_print(a));
#endif
			//* buckle back
			rRestPlastic=rRestOriginal;
//			rRestPlastic[rKink]=
//					rRestOriginal[rKink]-breakAngle+elasticAngle;
			rRestPlastic[rKink]=
					relativeAngle[rKink]+elasticAngle;
		}
		else if(rKink>=0 && rKinkForward)
		{
			if(relativeAngle[rKink]<rRestOriginal[rKink]+popAngle)
			{
#if FE_BLADE_BUCKLE_DEBUG
				feLog("Blade::step pop %d back %.6G < %.6G + %.6G (%.6G)\n",
						rKink,relativeAngle[rKink],
						rRestOriginal[rKink],popAngle,
						rRestPlastic[rKink]);
				feLog(" a %s\n",c_print(a));
#endif
				//* pop back
				rRestPlastic=rRestOriginal;
				rKink= -1;
			}
			else if(rRestPlastic[rKink]>rRestOriginal[rKink]+recoveryAngle)
			{
#if FE_BLADE_BUCKLE_DEBUG
				feLog("Blade::step ease %d back %.6G >= %.6G + %.6G (%.6G)\n",
						rKink,relativeAngle[rKink],
						rRestOriginal[rKink],recoveryAngle,
						rRestPlastic[rKink]);
				feLog(" a %s\n",c_print(a));
#endif
				//* ease back
				rRestPlastic[rKink]-=recoveryAngle;
			}
		}
		else if(rKink>=0 && !rKinkForward)
		{
			if(relativeAngle[rKink]>rRestOriginal[rKink]-popAngle)
			{
#if FE_BLADE_BUCKLE_DEBUG
				feLog("Blade::step pop %d forward %.6G > %.6G - %.6G (%.6G)\n",
						rKink,relativeAngle[rKink],
						rRestOriginal[rKink],popAngle,
						rRestPlastic[rKink]);
				feLog(" a %s\n",c_print(a));
#endif
				//* pop forward
				rRestPlastic=rRestOriginal;
				rKink= -1;
			}
			else if(rRestPlastic[rKink]<rRestOriginal[rKink]-recoveryAngle)
			{
#if FE_BLADE_BUCKLE_DEBUG
				feLog("Blade::step ease %d forward %.6G <= %.6G - %.6G (%.6G)\n",
						rKink,relativeAngle[rKink],
						rRestOriginal[rKink],recoveryAngle,
						rRestPlastic[rKink]);
				feLog(" a %s\n",c_print(a));
#endif
				//* ease forward
				rRestPlastic[rKink]+=recoveryAngle;
			}
		}
	}
	else
	{
#if FE_BLADE_BUCKLE_DEBUG
		feLog("Blade::step clear %d\n",rKink);
		feLog(" a %s\n",c_print(a));
#endif
		rRestPlastic=rRestOriginal;
		rKink= -1;
	}

	//* update facing vectors (except root)
	SpatialVector yAxis(0.0,1.0,0.0);
	SpatialVector about;
	cross(about,yAxis,rTangent);

	if(m_facing.size())
	{
		SpatialQuaternion rotation;
		set(rotation,a[0]-restAbsolute[0],about);
		rotateVector(rotation,m_restFacing[index5+1],m_facing[index5+1]);
		set(rotation,a[1]-restAbsolute[1],about);
		rotateVector(rotation,m_restFacing[index5+2],m_facing[index5+2]);
		set(rotation,a[2]-restAbsolute[2],about);
		rotateVector(rotation,m_restFacing[index5+3],m_facing[index5+3]);
		set(rotation,a[3]-restAbsolute[3],about);
		rotateVector(rotation,m_restFacing[index5+4],m_facing[index5+4]);

		if(m_spDrawDebug.isValid())
		{
			SpatialVector line[2];

			for(U32 m=0;m<5;m++)
			{
				line[0]=m_location[index5+m];
				line[1]=line[0]+m_facing[index5+m];
				m_spDrawDebug->drawTransformedLines(xformDeformed,NULL,
						line,NULL,2,DrawI::e_discrete,false,&red);
			}
		}
	}

#if FALSE
	if(relocated)
	{
		const SpatialVector radial=rLocation0-center;
		const SpatialVector target=center+radius*unitSafe(radial);

		InvKineCCD::solve(m_location,index5,5,target);
	}
#endif

	m_velocity[index4]=w[0];
	m_velocity[index4+1]=w[1];
	m_velocity[index4+2]=w[2];
	m_velocity[index4+3]=w[3];

	if(localized)
	{
		transformVector(xformDeformed,rLocation0,m_locationDef[index5]);
		transformVector(xformDeformed,rLocation1,m_locationDef[index5+1]);
		transformVector(xformDeformed,rLocation2,m_locationDef[index5+2]);
		transformVector(xformDeformed,rLocation3,m_locationDef[index5+3]);
		transformVector(xformDeformed,rLocation4,m_locationDef[index5+4]);

#if FE_BLADE_XFORM_DEBUG
		feLog("loc0 %s  %s\n",c_print(rLocation0),
				c_print(m_locationDef[index5]));
		feLog("loc1 %s  %s\n",c_print(rLocation1),
				c_print(m_locationDef[index5+1]));
#endif
	}
	else
	{
		m_locationDef[index5]=rLocation0;
		m_locationDef[index5+1]=rLocation1;
		m_locationDef[index5+2]=rLocation2;
		m_locationDef[index5+3]=rLocation3;
		m_locationDef[index5+4]=rLocation4;
	}
}
