/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_MultiModOp_h__
#define __oplab_MultiModOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to alter regions of a surface

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT MultiModOp:
	public OperatorState,
	public Initialize<MultiModOp>
{
	public:

							MultiModOp(void):
								m_editMode(e_pickAnchor),
								m_lastFrame(0.0),
								m_brushed(FALSE),
								m_anchorPicked(-1)							{}

virtual						~MultiModOp(void)								{}

		void				initialize(void);

							//* As HandlerI
virtual	void				handle(Record& a_rSignal);

	protected:

virtual	void				setupState(void);
virtual	BWORD				loadState(const String& a_rBuffer);

		SpatialTransform	getParamPivot(I32 a_anchorIndex);
		void				setParamPivot(I32 a_anchorIndex,
								SpatialTransform a_transform);
		SpatialTransform	getParamDeform(I32 a_anchorIndex);
		void				setParamDeform(I32 a_anchorIndex,
								SpatialTransform a_transform);

		String				anchorLabel(I32 a_anchorIndex);
		String&				anchorParent(I32 a_anchorIndex);
		String				anchorWeightAttr(I32 a_anchorIndex);
		Real				anchorRadius(I32 a_anchorIndex);
		Real				anchorPower(I32 a_anchorIndex);
		SpatialTransform	evaluateLocator(I32 a_anchorIndex);
		SpatialTransform	evaluateConcatenation(SpatialTransform a_locator,
								I32 a_anchorIndex);
		void				createAnchor(I32 a_triIndex,SpatialBary a_bary);
		void				pickAnchor(I32 a_anchorIndex);
		void				updateAnchor(void);

	private:

		enum		EditMode
					{
						e_pickAnchor,
						e_createAnchor,
						e_manipulator,
						e_influence
					};

		void			changeMode(EditMode a_editMode);

		EditMode						m_editMode;

		sp<ManipulatorI>				m_spManipulator;
		sp<SurfaceAccessibleI>			m_spOutputAccessible;
		sp<SurfaceI>					m_spInput;
		sp<SurfaceI>					m_spDriver;
		sp<SurfaceI>					m_spParent;
		WindowEvent						m_event;

		sp<DrawMode>					m_spWireframe;
		sp<DrawMode>					m_spSolid;

		sp< RecordMap<I32> >			m_spAnchorMap;
		Accessor<I32>					m_aTriIndex;
		Accessor<Vector3>				m_aBarycenter;

		Array<SpatialTransform>			m_locatorList;
		Real							m_lastFrame;
		BWORD							m_brushed;
		I32								m_anchorPicked;
		String							m_newParent;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_MultiModOp_h__ */
