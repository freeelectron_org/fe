/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_vertStart,__global int* a_vertCount,
	__global float4* a_position2,
	__global float4* a_normal2,
	__global int* a_vertStart2,__global int* a_vertCount2,
	__global float4* a_fromPosition2,
	__global float4* a_fromNormal2,
	__global int4* a_guideIndices,
	__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];
	if(subCount<1)
	{
		return;
	}

	int primitiveIndex2=a_guideIndices[primitiveIndex].x;
	if(primitiveIndex2<0)
	{
		return;
	}

	int subStart2=a_vertStart2[primitiveIndex2];
	int subCount2=a_vertCount2[primitiveIndex2];

	float scaleCount=convert_float(subCount2-1)/convert_float(subCount-1);

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		int pointIndex=subStart+subIndex;
		float4 input=a_position[pointIndex];

		float floatIndex2=convert_float(subIndex)*scaleCount;
		int subIndex2A=convert_int(floatIndex2);
		float between=floatIndex2-convert_float(subIndex2A);

		int subIndex2B=subIndex2A+1;
		if(subIndex2A>subCount2-1)
		{
			subIndex2A=subCount2-2;
			subIndex2B=subCount2-1;
			between=1.0f;
		}

		int pointIndex2A=subStart2+subIndex2A;
		int pointIndex2B=subStart2+subIndex2B;

		float4 fromPointA=a_fromPosition2[pointIndex2A];
		float4 toPointA=a_position2[pointIndex2A];

		float4 fromNormalA=a_fromNormal2[pointIndex2A];
		float4 toNormalA=a_normal2[pointIndex2A];

		float4 fromPointB=a_fromPosition2[pointIndex2B];
		float4 toPointB=a_position2[pointIndex2B];

		float4 fromTangent=normalize(fromPointB-fromPointA);
		float4 toTangent=normalize(toPointB-toPointA);

		float16 fromXform=makeFrameNormalY(fromPointA,fromTangent,fromNormalA);
		float16 toXform=makeFrameNormalY(toPointA,toTangent,toNormalA);

		float16 fromInv=invertMatrix(fromXform);

		float4 relative=transformVector(fromInv,input);

		float4 output=transformVector(toXform,relative);

		a_position[pointIndex]=output;

//		a_position[pointIndex]=input+
//				(1.0f-between)*(toPointA-fromPointA)+
//				between*(toPointB-fromPointB);
	}
}
