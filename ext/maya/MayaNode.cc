/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#include <maya/MFnPlugin.h>

#define FE_MN_COMPOUND_TEST		FALSE
#define FE_MN_POPULATE_DEBUG	FALSE
#define FE_MN_CATALOG_DEBUG		FALSE
#define FE_MN_ARRAY_DEBUG		FALSE
#define FE_MN_PAINT_DEBUG		FALSE
#define FE_MN_COMPUTE_DEBUG		FALSE
#define FE_MN_IO_DEBUG			FALSE

// TODO may sure this is a unique name
static const char* feMayaDefaultOutputSurface = "Output Surface";

/*	ESC interrupt
	MComputation computation;
	computation.beginComputation();
	for (int i= 1; i<1000; i++) {
		Computation();  // Some expensive operation
		if(computation.isInterruptRequested()) break ;
	}
	computation.endComputation();
*/

namespace fe
{
namespace ext
{

I32 mayaDiscoverOperators(MObject a_object)
{
	//* NOTE std::regex crashes, so use boost::regex (same with Houdini)
	fe::Regex::confirm("fexBoostRegex");

	MayaContext mayaContext;

#if FE_MN_POPULATE_DEBUG
	feLog("mayaDiscoverOperators master=%p\n",
		mayaContext.master().raw());
#endif

	sp<Registry> spRegistry=mayaContext.master()->registry();
	mayaContext.loadLibraries();

	MStatus status;

	MFnPlugin plugin(a_object,"freeelectron.org",
			System::buildDate().c_str(),"Any");

	status=plugin.registerContextCommand(
			"MayaBrushContext", &MayaBrush::Command::creator);
	if(!status)
	{
		status.perror("registerContextCommand");
		return status;
	}

	status=plugin.registerNode("MayaBrushManip",MayaBrushManip::m_id,
			&MayaBrushManip::creator,&MayaBrushManip::initialize,
			MPxNode::kManipContainer);
	if(!status)
	{
		feLog("fe::ext::mayaDiscoverOperators"
				" error registering MayaBrushManip node");
	}

	const String checkCommand="exists fe_init";

	MCommandResult melCommandResult;
	MGlobal::executeCommand(checkCommand.c_str(),melCommandResult);

	int melFound=0;
	melCommandResult.getResult(melFound);

	if(!melFound)
	{
		feLog("fe::ext::mayaDiscoverOperators"
				" failed to find FE mel scripts (check your paths)\n");
	}

	const String initCommand="fe_init(\""+System::buildDate()+"\")";

	MString melResult;
	MGlobal::executeCommand(initCommand.c_str(),melResult);

	const char* exposure=getenv("FE_EXPOSURE");
	const BWORD showPrototype=exposure &&
			(exposure==String("prototype") || exposure==String("all"));

	int count=0;

	Array<String> available;
	spRegistry->listAvailable("OperatorSurfaceI",available);
	U32 candidates=available.size();
	for(U32 index=0;index<candidates;index++)
	{
#if FE_MN_POPULATE_DEBUG
		feLog("checking %d/%d \"%s\"\n",
				index,candidates,available[index].c_str());
#endif

		//* ignore operators tagged as prototype
		if(!showPrototype && available[index].dotMatch("*.*.*.prototype"))
		{
#if FE_CODEGEN<=FE_DEBUG
			feLog("[MayaNode] mayaDiscoverOperators"
					" hiding prototype operator:\n"
					"  %s\n",available[index].c_str());
#endif
			continue;
		}

		sp<Component> spComponent=
				spRegistry->create(available[index]);
		if(!spComponent.isValid())
		{
			feLog("mayaDiscoverOperators"
					" failed to create Component \"%s\"\n",
					available[index].c_str());
			continue;
		}

		sp<OperatorSurfaceI> spOperatorSurfaceI=spComponent;
		if(!spOperatorSurfaceI.isValid())
		{
			feLog("mayaDiscoverOperators"
					" Component is not an OperatorSurfaceI \"%s\"\n",
					available[index].c_str());
			continue;
		}

		String implementation=available[index];
		char* buffer1=strchr(const_cast<char*>(available[index].c_str()),'.');
		if(buffer1)
		{
			buffer1++;
			const char* buffer2=strchr(buffer1,'.');
			if(buffer2>buffer1)
			{
				implementation.sPrintf("%.*s",int(buffer2-buffer1),buffer1);
			}
			else
			{
				implementation=buffer1;
			}
		}

		const U32 uniqueId=(0xfe0fe)+count;	//* WARNING could collide

#if FE_MN_POPULATE_DEBUG
		feLog("mayaDiscoverOperators implementation \"%s\" id %p\n",
				implementation.c_str(),uniqueId);
#endif

		//* TODO
		const String rootUrl="http://freeelectron.org/op/";

		const char* name=strdup(implementation.c_str());
		const char* url=strdup(("\\\""+rootUrl+implementation+"\\\"").c_str());

		fe::ext::MayaNode::preRegister(uniqueId,name,available[index]);
		MStatus status=plugin.registerNode(
				name,
				MTypeId(uniqueId),
				fe::ext::MayaNode::create,
				fe::ext::MayaNode::initialize,
				MPxNode::kDeformerNode);
		if(!status)
		{
			status.perror("registerNode");
		}
		fe::ext::MayaNode::postRegister();

		if(melFound)
		{
			plugin.addMenuItem(
					implementation.c_str(),
					"fe_help_menu",
					"launch -web",
					url);

			plugin.addMenuItem(
					implementation.c_str(),
					"fe_create_menu",
					"createNode",
					name);
		}

		count++;
	}

	return count;
}

} /* namespace ext */
} /* namespace fe */

//~extern "C"
//~{

//* NOTE returning MStatus is not "C"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

//* discovery point for Maya
FE_DL_EXPORT MStatus initializePlugin(MObject a_object)
{
#if FE_MN_POPULATE_DEBUG
	feLog("fexMayaDL ::initializePlugin\n");
#endif

//	I32 count=
			fe::ext::mayaDiscoverOperators(a_object);

#if FE_MN_POPULATE_DEBUG
	feLog("fexMayaDL ::initializePlugin done\n");
#endif

	return MS::kSuccess;
}

FE_DL_EXPORT MStatus uninitializePlugin(MObject a_object)
{
#if FE_MN_POPULATE_DEBUG
	feLog("fexMayaDL ::uninitializePlugin\n");
#endif

	MGlobal::executeCommand("deleteUI -m fe_menu");

	MFnPlugin plugin(a_object);

	MStatus status = plugin.deregisterContextCommand("MayaBrushContext");
	if (!status) {
		MGlobal::displayError("deregisterContextCommand");
		return status;
	}

	//* TODO proper
/*
	MStatus status=plugin.deregisterNode(fe::ext::MayaNode::ms_id);
	if(!status)
	{
		status.perror("deregisterNode");
	}
*/

#if FE_MN_POPULATE_DEBUG
	feLog("fexMayaDL ::uninitializePlugin done\n");
#endif

	return MS::kSuccess;
}

#pragma clang diagnostic pop

//* TODO Is this unneeded in Maya?
#if FALSE
FE_DL_EXPORT void HoudiniDSOInit(UT_DSOInfo& dsoinfo)
{
	//* force Houdini to load with RTLD_GLOBAL (needed for dynamic_cast<>)
	dsoinfo.loadGlobal=true;
}
#endif

//~}

namespace fe
{
namespace ext
{

I32	MayaNode::ms_registerLock=0;
String MayaNode::ms_registerName;
MObject	MayaNode::ms_compoundBehavior;
std::map<String,MayaNode::Registration> MayaNode::ms_mayaRegistry;
std::map<String,MayaNode::Brush> MayaNode::ms_brushMap;
std::map<String,MayaNode::Brush> MayaNode::ms_shapeMap;

MayaNode::MayaNode(void):
	m_paintSize(0),
	m_bound(FALSE)
{
#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::MayaNode master=%p\n",m_mayaContext.master().raw());
#endif

	sp<Registry> spRegistry=m_mayaContext.master()->registry();
	m_mayaContext.loadLibraries();

	m_spDrawOutput=spRegistry->create("DrawI.MayaDraw");
//	feLog("MayaNode::MayaNode m_spDrawOutput valid=%d\n",
//			m_spDrawOutput.isValid());

	//* NOTE Houdini guides are not direct to OpenGL like this
//	m_spDrawGuideChain=spRegistry->create("DrawI.DrawOpenGL");

	setupScope(m_mayaContext.scope());

#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(MayaNode),"MayaNode");
#endif

//	feLog("MayaNode::MayaNode done\n");
}

void MayaNode::postConstructor(void)
{
	precompute(0.0);
	updateComponents(TRUE,TRUE);
}

MayaNode::~MayaNode(void)
{
#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::~MayaNode \"%s\" \"%s\" shape \"%s\"\n",
			m_typeName.c_str(),m_nodeName.c_str(),m_shapeNode.c_str());
#endif

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(spCatalog.isValid() && spCatalog->cataloged("Brush"))
	{
		ms_brushMap[m_nodeName].m_pMayaNode=NULL;
		ms_shapeMap[m_shapeNode].m_pMayaNode=NULL;
	}

	m_spOriginalOutput=NULL;
	m_originalInputArray.clear();

	clearReferences();

#if FE_COUNTED_TRACK
	Counted::deregisterRegion(this);
#endif
}

void MayaNode::preRegister(U32 a_uniqueId,String a_name,
		String a_implementation)
{
#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode::preRegister\n");
#endif

	FEASSERT(ms_registerLock==0);
	ms_registerLock++;
	ms_registerName=a_name;

	Registration& rRegistration=ms_mayaRegistry[a_name];
	rRegistration.m_autoOutput=FALSE;
	rRegistration.m_uniqueId=a_uniqueId;
	rRegistration.m_implementation=a_implementation;
}

void MayaNode::postRegister(void)
{
#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode::postRegister\n");
#endif

	FEASSERT(ms_registerLock==1);

	ms_registerName="";
	ms_registerLock--;
}

MStatus MayaNode::initialize(void)
{
#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode::initialize %s %d\n",
			ms_registerName.c_str(),ms_registerLock);
#endif

	populateOperator(ms_registerName);

#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode::initialize done\n");
#endif

	return MS::kSuccess;
}

void* MayaNode::create(void)
{
//	feLog("MayaNode::create\n");
	return new MayaNode();
}

void MayaNode::populateOperator(String a_name)
{
	MayaContext mayaContext;

#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode:populateOperator master=%p\n",
		mayaContext.master().raw());
#endif

	sp<Registry> spRegistry=mayaContext.master()->registry();

	Registration& rRegistration=ms_mayaRegistry[a_name];
	String implementation=rRegistration.m_implementation;
	std::map<String,Parameter>& rParameterMap=rRegistration.m_parameterMap;
	std::map<String,String>& rCleanMap=rRegistration.m_cleanMap;

#if FE_MN_POPULATE_DEBUG
	feLog("registration \"%s\" \"%s\"\n",
			a_name.c_str(),implementation.c_str());
#endif

	sp<OperatorSurfaceI> spOperatorSurfaceI=
			spRegistry->create(implementation);
	if(!spOperatorSurfaceI.isValid())
	{
		feLog("MayaNode:populateOperator"
				" Component is not an OperatorSurfaceI \"%s\"\n",
				implementation.c_str());
		return;
	}

	//* tried to move built-in attributes to a compound
	MStatus status=MS::kSuccess;
#if FE_MN_COMPOUND_TEST
//	MFnAttribute fnCaching;
//	fnCaching.setObject(caching);
//	fnCaching.setHidden(true);

//	MFnAttribute fnState;
//	fnState.setObject(state);
//	fnState.setHidden(true);

	MFnCompoundAttribute fnBehavior;
	ms_compoundBehavior=fnBehavior.create("NodeBehavior","behavior",&status);
	status=fnBehavior.setNiceNameOverride("Node Behavior");
	FEASSERT(status);
//	status=fnBehavior.addChild(caching);
	FEASSERT(status);
//	status=fnBehavior.addChild(state);
	FEASSERT(status);
#endif

	Array<String> keys;

	sp<Catalog> spCatalog=spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("MayaNode:populateOperator"
				" Component is not a Catalog \"%s\"\n",
				implementation.c_str());

		//* TODO keep this from crashing later
	}
	else
	{
		spCatalog->catalogKeys(keys);
	}

	U32 number=keys.size();
#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode::populateOperator keys %d\n",number);
#endif

	U32 outputs=0;
	for(U32 m=0;m<number;m++)
	{
		Instance instance;
		if(!spCatalog->catalogLookup(keys[m],instance))
		{
			continue;
		}
		if(instance.is< sp<Component> >())
		{
			char* keyname=strdup(keys[m].c_str());
			String io=spCatalog->catalogOrDefault<String>(keyname,"IO","input");
			if(io=="output")
			{
				outputs++;
			}
		}
	}

	if(!outputs)
	{
#if FE_MN_POPULATE_DEBUG
		feLog("MayaNode::populateOperator no output surface specified\n");
#endif

		//* refresh key list
		keys.clear();
		rRegistration.m_autoOutput=TRUE;

		if(spCatalog.isValid())
		{

			spCatalog->catalog< sp<Component> >(feMayaDefaultOutputSurface);
			spCatalog->catalog<String>(feMayaDefaultOutputSurface,"IO")=
					"output";

			spCatalog->catalogKeys(keys);
			number=keys.size();
		}
	}

	//* state
	if(true)
	{
		const char* stateName="fe_ascii";
		MFnStringData fnStringData;
		MObject defaultString=fnStringData.create(strdup(""));
		MFnTypedAttribute stateAttr;
		rParameterMap[stateName].m_object=stateAttr.create(stateName,stateName,
				MFnData::kString,defaultString);
		rCleanMap[stateName]=stateName;
		stateAttr.setKeyable(false);
		stateAttr.setStorable(true);
		stateAttr.setHidden(true);
		status=addAttribute(rParameterMap[stateName].m_object);
		FEASSERT(status);
	}

	//* local space matrix
	if(true)
	{
		const char* geomName="geomMatrix";
		MFnMatrixAttribute matrixAttr;
		rParameterMap[geomName].m_object=matrixAttr.create(geomName,geomName,
				MFnMatrixAttribute::kFloat);
		rCleanMap[geomName]=geomName;
		matrixAttr.setNiceNameOverride("Geom Matrix");
		matrixAttr.setKeyable(false);
		matrixAttr.setStorable(true);
		matrixAttr.setHidden(true);
		matrixAttr.setArray(true);
		matrixAttr.setUsesArrayDataBuilder(true);
		status=addAttribute(rParameterMap[geomName].m_object);
		FEASSERT(status);
	}

	for(U32 m=0;m<number;m++)
	{
#if FE_MN_POPULATE_DEBUG
		feLog("MayaNode::populateOperator key %d: \"%s\"\n",
				m,keys[m].c_str());
#endif

		if(keys[m]=="label" || keys[m]=="icon" || keys[m]=="url" ||
				keys[m]=="precook" || keys[m]=="summary")
		{
			spCatalog->catalog<bool>(keys[m],"internal")=true;
		}
		if(spCatalog->catalogOrDefault<bool>(keys[m],"internal",false))
		{
			continue;
		}

		Instance instance;
		bool success=spCatalog->catalogLookup(keys[m],instance);
		if(!success)
		{
			feLog("MayaNode::populateOperator \"%s\" invalid Instance \"%s\"\n",
					implementation.c_str(),keys[m].c_str());
			continue;
		}

		char* cleanname=strdup(keys[m].substitute(" ","_").c_str());
		char* keyname=strdup(keys[m].c_str());
		String io=spCatalog->catalogOrDefault<String>(keyname,"IO","input");
		rCleanMap[cleanname]=keyname;

		const BWORD isArrayType=
				(instance.is< Array<String> >() ||
				instance.is< Array<Real> >() ||
				instance.is< Array<I32> >() ||
				instance.is< Array<SpatialVector> >() ||
				instance.is< Array< sp<Component> > >());

		const BWORD isArray=(isArrayType ||
				spCatalog->catalogOrDefault<bool>(
				keyname,"array",false));

		if(instance.is< sp<Component> >() ||
				instance.is< Array< sp<Component> > >())
		{
			//* see meshOpNode.cpp

			String implementation=
					spCatalog->catalogOrDefault<String>(keyname,
					"implementation",
					"SurfaceAccessibleI.SurfaceAccessibleMaya");

			MFnData::Type dataType=MFnMeshData::kMesh;

			if(implementation=="curve")
			{
				dataType=MFnNurbsCurveData::kNurbsCurve;
				implementation="SurfaceAccessibleI";
			}

			if(implementation=="SurfaceAccessibleI" || implementation==
					"SurfaceAccessibleI.SurfaceAccessibleMaya")
			{
				MFnTypedAttribute typedAttr;
				rParameterMap[keyname].m_object=typedAttr.create(
						cleanname,cleanname,dataType);

				if(isArray)
				{
					typedAttr.setArray(true);
					typedAttr.setUsesArrayDataBuilder(true);
				}

				const String label=spCatalog->catalogOrDefault<String>(
						keyname,"label",keyname);
				typedAttr.setNiceNameOverride(label.c_str());

				if(io=="input")
				{
					if(!isArrayType)
					{
						U32 channel=rRegistration.m_inputChannelArray.size();
						rRegistration.m_inputChannelArray.resize(channel+1);
						rRegistration.m_inputChannelArray[channel]=keyname;
					}

					typedAttr.setStorable(true);
					typedAttr.setHidden(true);
				}
				else
				{
					if(!isArrayType)
					{
						U32 channel=rRegistration.m_outputChannelArray.size();
						rRegistration.m_outputChannelArray.resize(channel+1);
						rRegistration.m_outputChannelArray[channel]=keyname;
					}

					typedAttr.setStorable(false);
					typedAttr.setWritable(false);
				}
//				MStatus status=
						addAttribute(rParameterMap[keyname].m_object);
			}
			else if(implementation=="RampI")
			{
				const String defaultRamp=
						spCatalog->catalogOrDefault<String>(
						keyname,"default","");
				if(defaultRamp=="descending")
				{
					//* TODO
				}

				rParameterMap[keyname].m_object=
						MRampAttribute::createCurveRamp(cleanname,cleanname);
//				MStatus status=
						addAttribute(rParameterMap[keyname].m_object);
			}
//~			else if(implementation=="DrawI")
//~			{
//~				addBrushName(a_name);
//~			}
		}
		else
		{
			MFnAttribute attribute;

			if(instance.is<String>())
			{
				const char* value=strdup(instance.cast<String>().c_str());

				if(!spCatalog->catalogOrDefault<String>(
						keyname,"choice:0","").empty())
				{
					short defaultValue=0;

					U32 choiceCount=0;
					while(TRUE)
					{
						String keyChoice;
						keyChoice.sPrintf("choice:%d",choiceCount);
						const String nameChoice=
								spCatalog->catalogOrDefault<String>(
								keyname,keyChoice,"");
						if(nameChoice.empty())
						{
							break;
						}
						if(nameChoice==value)
						{
							defaultValue=choiceCount;
						}
						choiceCount++;
					}

					MFnEnumAttribute enumAttr;
					rParameterMap[keyname].m_object=
							enumAttr.create(cleanname,cleanname,defaultValue);
					attribute.setObject(enumAttr.object());

					for(U32 choice=0;choice<choiceCount;choice++)
					{
						String keyChoice;
						keyChoice.sPrintf("choice:%d",choice);
						const String nameChoice=
								spCatalog->catalogOrDefault<String>(
								keyname,keyChoice,"<error>");
						keyChoice.sPrintf("label:%d",choice);
						const String labelString=
								spCatalog->catalogOrDefault<String>(
								keyname,keyChoice,nameChoice);
#if	FE_MN_POPULATE_DEBUG
						const char* cleanChoice=strdup(
								nameChoice.substitute(" ","_").c_str());
						feLog("  choice %d/%d \"%s\" \"%s\"\n",
								choice,choiceCount,
								cleanChoice,labelString.c_str());
#endif
						MString fieldString=labelString.c_str();
						enumAttr.addField(fieldString,choice);
					}
				}
				else
				{
					MFnStringData fnStringData;
					MObject defaultString=fnStringData.create(value);
					MFnTypedAttribute typedAttr;
					rParameterMap[keyname].m_object=
							typedAttr.create(cleanname,cleanname,
							MFnData::kString,defaultString);
					attribute.setObject(typedAttr.object());
				}
			}
			else if(instance.is< Array<String> >())
			{
				MFnTypedAttribute typedAttr;
				rParameterMap[keyname].m_object=
						typedAttr.create(cleanname,cleanname,
						MFnData::kString,MObject::kNullObj,&status);

				attribute.setObject(typedAttr.object());
			}
			else
			{
				MFnNumericAttribute numAttr;

				if(instance.is<bool>())
				{
					bool value=instance.cast<bool>();

					rParameterMap[keyname].m_object=
							numAttr.create(cleanname,cleanname,
							MFnNumericData::kBoolean,value);
				}
				else if(instance.is<Real>() ||
						instance.is< Array<Real> >())
				{
					Real value=instance.is<Real>()? instance.cast<Real>(): 0.0;

					Real min=spCatalog->catalogOrDefault<Real>(keyname,
							"min",0.0);
					Real max=spCatalog->catalogOrDefault<Real>(keyname,
							"max",1.0);
					Real low=spCatalog->catalogOrDefault<Real>(keyname,
							"low",min);
					Real high=spCatalog->catalogOrDefault<Real>(keyname,
							"high",max);

					min=(min>low)? low: min;
					max=(max<high)? high: max;

					rParameterMap[keyname].m_object=
							numAttr.create(cleanname,cleanname,
							MFnNumericData::kDouble);
					numAttr.setSoftMin(low);
					numAttr.setSoftMax(high);
					numAttr.setMin(min);
					numAttr.setMax(max);
					numAttr.setDefault(double(value));

					const BWORD paintable=(instance.is<Real>() &&
							spCatalog->catalogOrDefault<bool>(
							keyname,"paintable",false));
					if(paintable)
					{
						const String paintableName=String(cleanname)+"Painting";
						const String paintListName=paintableName+"List";
						char* paintname=strdup(paintableName.c_str());
						char* listname=strdup(paintListName.c_str());

#if FALSE
						MFnDoubleArrayData doubleArrayData;
						MObject doubleArrayObject=
								doubleArrayData.create(MDoubleArray(30,1.0));

						MFnTypedAttribute paintAttr;
						MObject objectPaint=
								paintAttr.create(paintname,paintname,
								MFnData::kDoubleArray,doubleArrayObject);
#else
						MFnTypedAttribute paintAttr;
						MObject objectPaint=
								paintAttr.create(paintname,paintname,
								MFnData::kDoubleArray);
#endif
						paintAttr.setHidden(true);
						paintAttr.setStorable(true);
//						paintAttr.setUsesArrayDataBuilder(true);

						MFnCompoundAttribute compoundAttr;
						MObject objectPaintList=
								compoundAttr.create(listname,listname);
						compoundAttr.addChild(objectPaint);
						compoundAttr.setHidden(true);
						compoundAttr.setStorable(true);
//						compoundAttr.setArray(true);
//						compoundAttr.setUsesArrayDataBuilder(true);

//						MStatus status=
								addAttribute(objectPaintList);

						rParameterMap[keyname].m_objectPaint=objectPaint;
						rParameterMap[keyname].m_objectPaintList=
								objectPaintList;

						String command="makePaintable"
								" -attrType doubleArray"
								" "+a_name+" "+paintableName;
#if FE_MN_POPULATE_DEBUG
						feLog("MayaNode::populateOperator MEL \"%s\"\n",
								command.c_str());
#endif

						//* making the call causes paint changes to be ignored
						MGlobal::executeCommandOnIdle(MString(command.c_str()));
					}

					if(keys[m]=="envelope" || keys[m]=="Envelope")
					{
						rParameterMap[keyname].m_object=
								MPxDeformerNode::envelope;
						continue;
					}
				}
				else if(instance.is<I32>() ||
						instance.is< Array<I32> >())
				{
					I32 value=instance.is<I32>()? instance.cast<I32>(): 0;

					I32 min=spCatalog->catalogOrDefault<I32>(keyname,
							"min",0);
					I32 max=spCatalog->catalogOrDefault<I32>(keyname,
							"max",10);
					I32 low=spCatalog->catalogOrDefault<I32>(keyname,
							"low",min);
					I32 high=spCatalog->catalogOrDefault<I32>(keyname,
							"high",max);

					min=(min>low)? low: min;
					max=(max<high)? high: max;

					rParameterMap[keyname].m_object=
							numAttr.create(cleanname,cleanname,
							MFnNumericData::kInt);
					numAttr.setSoftMin(low);
					numAttr.setSoftMax(high);
					numAttr.setMin(min);
					numAttr.setMax(max);
					numAttr.setDefault(value);

				}
				else if(instance.is<SpatialVector>() ||
						instance.is< Array<SpatialVector> >())
				{
					rParameterMap[keyname].m_object=
							numAttr.create(cleanname,cleanname,
							MFnNumericData::k3Double);

					if(instance.is<SpatialVector>())
					{
						SpatialVector& value=instance.cast<SpatialVector>();

						numAttr.setDefault((double)value[0],(double)value[1],
								(double)value[2]);
					}
					else
					{
						numAttr.setDefault(0.0,0.0,0.0);
					}
				}
				else if(instance.is<Vector4>())
				{
					Vector4& value=instance.cast<Vector4>();

					rParameterMap[keyname].m_object=
							numAttr.create(cleanname,cleanname,
							MFnNumericData::k4Double);
					numAttr.setDefault((double)value[0],(double)value[1],
							(double)value[2],(double)value[3]);
				}

				attribute.setObject(numAttr.object());
			}

			const String label=spCatalog->catalogOrDefault<String>(
					keyname,"label",keyname);
			attribute.setNiceNameOverride(label.c_str());

			//* NOTE 'category' doesn't seem to affect anything
			//* This seems to be handled by "layouts" in a custom AE template.
			const String page=spCatalog->catalogOrDefault<String>(
					keyname,"page","");
			if(!page.empty())
			{
				attribute.addToCategory(page.c_str());
			}

			if(io=="output")
			{
				attribute.setKeyable(false);
				attribute.setStorable(false);
			}
			else
			{
				attribute.setKeyable(true);
				attribute.setStorable(true);
			}

			if(isArray)
			{
				attribute.setArray(true);
				attribute.setUsesArrayDataBuilder(true);
			}

			if(spCatalog->catalogOrDefault<bool>(keyname,"hidden",false))
			{
				attribute.setHidden(true);
			}

			const String suggest=
					spCatalog->catalogOrDefault<String>(keyname,"suggest","");
			if(suggest=="filename")
			{
				//* NOTE auto-substitution of relative paths
//				attribute.setUsedAsFilename(true);
			}
			else if(suggest=="color")
			{
				attribute.setUsedAsColor(true);
			}

#if FE_MN_COMPOUND_TEST
			MStatus status=fnBehavior.addChild(rParameterMap[keyname].m_object);
#else
//			MStatus status=
					addAttribute(rParameterMap[keyname].m_object);
#endif
		}
	}

#if FE_MN_COMPOUND_TEST
	status=addAttribute(ms_compoundBehavior);
	FEASSERT(status);
#endif

	//* Currently every output is dependent on every input.
	//* We could get more specific.
	std::map<String,Parameter>::const_iterator it=rParameterMap.begin();
	while(it!=rParameterMap.end())
	{
		const String& key=it->first;
		String io=spCatalog->catalogOrDefault<String>(key,"IO","input");
		if(io=="output")
		{
			const Parameter& parameter=it->second;

			for(U32 pass=0;pass<2;pass++)
			{
				const MObject& rParamOutput=
						pass? MPxDeformerNode::outputGeom: parameter.m_object;

				std::map<String,Parameter>::const_iterator it2=
						rParameterMap.begin();
				while(it2!=rParameterMap.end())
				{
					const String& key2=it2->first;
					String io2=spCatalog->catalogOrDefault<String>(
							key2,"IO","input");
					if(io2!="output")
					{
						const Parameter& parameter2=it2->second;
						const MObject& rParamInput=parameter2.m_object;

						MStatus status=
								attributeAffects(rParamInput,rParamOutput);
						if(!status)
						{
							feLog("MayaNode::populateOperator"
									" attributeAffects %s -> %s failed\n",
									key2.c_str(),key.c_str());
						}

						const MObject& rParamPaint=parameter2.m_objectPaint;
						if(!rParamPaint.isNull())
						{
							status=attributeAffects(rParamPaint,rParamOutput);
							if(!status)
							{
								feLog("MayaNode::populateOperator"
										" attributeAffects %s (Paint)"
										" -> %s failed\n",
										key2.c_str(),key.c_str());
							}
						}

						const MObject& rParamPaintList=
								parameter2.m_objectPaintList;
						if(!rParamPaintList.isNull())
						{
							status=attributeAffects(
									rParamPaintList,rParamOutput);
							if(!status)
							{
								feLog("MayaNode::populateOperator"
										" attributeAffects %s (PaintList)"
										" -> %s failed\n",
										key2.c_str(),key.c_str());
							}
						}
					}
					it2++;
				}
			}
		}
		it++;
	}

#if FE_MN_POPULATE_DEBUG
	feLog("MayaNode::populateOperator done\n");
#endif
}

String MayaNode::evalParamString(String a_key,Real a_time)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(spCatalog.isNull())
	{
		return "";
	}

	const MString value=spCatalog->catalog<String>(a_key).c_str();
	const MString expanded=value.expandEnvironmentVariablesAndTilde();
	return expanded.asChar();
}

void MayaNode::updateCatalog(MDataBlock& a_rData,Real a_time,BWORD a_doInput)
{
#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::updateCatalog doInput=%d\n",a_doInput);
#endif

	if(!m_spOperatorSurfaceI.isValid())
	{
		feLog("MayaNode::updateCatalog invalid m_spOperatorSurfaceI\n");
		return;
	}

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("MayaNode:updateCatalog operator is not a Catalog\n");
		return;
	}

	Registration& rRegistration=ms_mayaRegistry[m_typeName];
	const std::map<String,Parameter>& rParameterMap=
			rRegistration.m_parameterMap;

	for(std::map<String,Parameter>::const_iterator it=rParameterMap.begin();
			it!=rParameterMap.end();it++)
	{
		const String& key=it->first;
		const Parameter& parameter=it->second;
		const MObject& rObject=parameter.m_object;
		const MObject& rObjectPaint=parameter.m_objectPaint;
		const MObject& rObjectPaintList=parameter.m_objectPaintList;

#if FE_MN_CATALOG_DEBUG
		feLog("MayaNode::updateCatalog \"%s\"\n",key.c_str());
#endif

		if(key=="fe_ascii" || key=="geomMatrix")
		{
			continue;
		}

		Instance instance;
		bool success=spCatalog->catalogLookup(key,instance);
		if(!success)
		{
			feLog("MayaNode::updateCatalog \"%s\" invalid Instance \"%s\"\n",
					m_nodeName.c_str(),key.c_str());
			continue;
		}

		String io=spCatalog->catalogOrDefault<String>(key,"IO","input");
		const BWORD isInput=(io=="input");

		if(!a_doInput)
		{
			const String enabler=
					spCatalog->catalogOrDefault<String>(key,"enabler","");
			if(!enabler.empty())
			{
				const BWORD enabled=evalParamCondition(enabler,a_time);

				if(isInput)
				{
					//* locking
					MPlug plug(thisMObject(),rObject);
					const BWORD locked=plug.isLocked();
					if(locked==enabled)
					{
						plug.setLocked(!enabled);
					}
				}

#if FALSE
				//* dimming
				String editorCommand="editorTemplate -dimControl "+
						m_nodeName+" "+key+(enabled? " false": " true");

				feLog("%s\n",editorCommand.c_str());

				MGlobal::executeCommandOnIdle(editorCommand.c_str(),true);
#endif
			}
		}

		if(io!="input output" && isInput!=a_doInput)
		{
#if FE_MN_CATALOG_DEBUG
			feLog("MayaNode::updateCatalog skip \"%s\"\n",io.c_str());
#endif
			continue;
		}

		if(instance.is<String>())
		{
			MStatus status;
			if(a_doInput)
			{
				MDataHandle handleIn=a_rData.inputValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					if(handleIn.type()==MFnData::kString)
					{
						MString value=handleIn.asString();

						spCatalog->catalog<String>(key,"expression")=
								value.asChar();

						MString evaluated=
								value.expandEnvironmentVariablesAndTilde();

#if FE_MN_CATALOG_DEBUG
						feLog("MayaNode::updateCatalog"
								" got string \"%s\"\n  expanded \"%s\"\n",
								spCatalog->catalog<String>(key).c_str(),
								evaluated.asChar());
#endif

						const String suggest=spCatalog->catalogOrDefault<String>(
								key,"suggest","");
						if(suggest=="filename")
						{
							MFileObject mFileObject;
							mFileObject.setRawFullName(evaluated);

							I32 len=evaluated.numChars();
							const BWORD hadSlash=
									(len && evaluated.rindex('/')==len-1);

#if FE_MN_CATALOG_DEBUG
							feLog("  filename raw resolved \"%s\"\n",
									evaluated.asChar());
#endif

							//* resolve relative to the Maya project path
							evaluated=mFileObject.resolvedFullName();

							//* discard appended slash
							if(!hadSlash)
							{
								len=evaluated.numChars();

								if(len && evaluated.rindex('/')==len-1)
								{
									evaluated=evaluated.substring(0,len-2);
								}
							}

#if FE_MN_CATALOG_DEBUG
							feLog("  filename resolved \"%s\"\n",
									evaluated.asChar());
#endif
						}

						spCatalog->catalog<String>(key)=evaluated.asChar();
					}
					else
					{
						String keyChoice;
						keyChoice.sPrintf("choice:%d",handleIn.asShort());
						spCatalog->catalog<String>(key)=
							spCatalog->catalogOrDefault<String>(
							key,keyChoice,"<error>");
					}
				}
			}
			else
			{
				MDataHandle handleOut=a_rData.outputValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					const String value=spCatalog->catalog<String>(key);

					if(handleOut.type()==MFnData::kString)
					{
						handleOut.setString(MString(value.c_str()));

#if FE_MN_CATALOG_DEBUG
						feLog("MayaNode::updateCatalog now string \"%s\"\n",
								value.c_str());
#endif
					}
					else
					{
						//* TODO keep a map
						U32 choiceCount=0;
						while(TRUE)
						{
							String keyChoice;
							keyChoice.sPrintf("choice:%d",choiceCount);
							const String nameChoice=
									spCatalog->catalogOrDefault<String>(
									key,keyChoice,"");
							if(nameChoice.empty())
							{
								break;
							}
							if(nameChoice==value)
							{
								handleOut.setShort(short(choiceCount));
								break;
							}
							choiceCount++;
						}
					}
				}
			}
		}
		else if(instance.is<bool>())
		{
			bool& rValue=spCatalog->catalog<bool>(key);

			MStatus status;
			if(a_doInput)
			{
				MDataHandle handleIn=a_rData.inputValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					rValue=handleIn.asBool();
				}
			}
			else
			{
				MDataHandle handleOut=a_rData.outputValue(rObject,&status);
				handleOut.setBool(rValue);
			}
#if FE_MN_CATALOG_DEBUG
			feLog("MayaNode::updateCatalog now bool %d\n",rValue);
#endif

		}
		else if(instance.is<Real>())
		{
			Real& rValue=spCatalog->catalog<Real>(key);

			MStatus status;

			if(key=="envelope" || key=="Envelope")
			{
				if(a_doInput)
				{
					MDataHandle handleIn=a_rData.inputValue(rObject,&status);
					if(status==MS::kSuccess)
					{
						rValue=handleIn.asFloat();
					}
				}
				else
				{
					MDataHandle handleOut=a_rData.outputValue(rObject,&status);
					handleOut.setFloat(float(rValue));
				}
			}
			else
			{
				if(a_doInput)
				{
					MDataHandle handleIn=a_rData.inputValue(rObject,&status);
					if(status==MS::kSuccess)
					{
						rValue=handleIn.asDouble();
					}
				}
				else
				{
					MDataHandle handleOut=a_rData.outputValue(rObject,&status);
					handleOut.setDouble(double(rValue));
				}
			}

#if FE_MN_CATALOG_DEBUG
			feLog("MayaNode::updateCatalog now Real %.6G\n",rValue);
#endif

			const BWORD paintable=spCatalog->catalogOrDefault<bool>(
					key,"paintable",false);

#if FE_MN_CATALOG_DEBUG
			feLog("MayaNode::updateCatalog paintable %d\n",paintable);
#endif

			if(paintable)
			{
				String rPaintableName=spCatalog->catalog<String>(
						key,"paintableName");
				if(rPaintableName.empty())
				{
					rPaintableName=key.substitute(" ","_")+"Painting";
				}

				Array<Real>& rPainting=
						spCatalog->catalog< Array<Real> >(key,"painting");

				if(a_doInput)
				{
#if FE_MN_PAINT_DEBUG
					feLog("MayaNode::updateCatalog"
							" input painting size %d ---- %s\n",
							m_paintSize,m_nodeName.c_str());
#endif

#if FALSE
					MArrayDataHandle listIn=
							a_rData.inputArrayValue(rObjectPaintList,&status);
					if(status!=MS::kSuccess)
					{
						feLog("MayaNode::updateCatalog input failed to"
								" get handle to painting list %s\n",
								status.errorString().asChar());
					}

					U32 listCount=listIn.elementCount(&status);
					feLog("MayaNode::updateCatalog input painting"
							" listCount %d\n",listCount);
#else
					MDataHandle listIn=
							a_rData.inputValue(rObjectPaintList,&status);
					if(status!=MS::kSuccess)
					{
						feLog("MayaNode::updateCatalog input failed to"
								" get handle to painting list %s\n",
								status.errorString().asChar());
					}

					U32 listCount=1;
//					feLog("MayaNode::updateCatalog input painting"
//							" listCount %d\n",listCount);
#endif

					for(U32 m=0;m<listCount;m++)
					{
#if FALSE
						status=listIn.jumpToArrayElement(m);
						if(status!=MS::kSuccess)
						{
							feLog("MayaNode::updateCatalog input painting"
									" failed to jump to %d/%d %s\n",
									m,listCount,status.errorString().asChar());
						}
						const U32 index=listIn.elementIndex();
						MDataHandle elementHandle=listIn.inputValue(&status);
						if(status!=MS::kSuccess)
						{
							feLog("MayaNode::updateCatalog input painting"
									" failed to get handle %d/%d %s\n",
									m,listCount,status.errorString().asChar());
						}

						MDataHandle childHandle=
								elementHandle.child(rObjectPaint);
#else
						MDataHandle childHandle=
								listIn.child(rObjectPaint);
#endif
						MObject childData=childHandle.data();
						if(childData.isNull())
						{
#if FE_MN_PAINT_DEBUG
							feLog("MayaNode::updateCatalog input painting"
									" child data null\n");
#endif

							if(m_paintSize)
							{
#if FE_MN_PAINT_DEBUG
								feLog("MayaNode::updateCatalog input painting"
										" doubleArrayData create %d\n",
										m_paintSize);
#endif
								MFnDoubleArrayData doubleArrayData;
								MObject doubleArrayObject=
										doubleArrayData.create(
										MDoubleArray(m_paintSize,1.0));

								childHandle.set(doubleArrayObject);
								childData=childHandle.data();
							}
						}

						MFnDoubleArrayData doubleArrayData(childData,&status);
						if(status!=MS::kSuccess)
						{
#if FE_MN_PAINT_DEBUG
							feLog("MayaNode::updateCatalog input painting"
									" failed to bind doubleArrayData %s\n",
									status.errorString().asChar());
#endif
						}
						else
						{
							U32 length=doubleArrayData.length(&status);
							if(status!=MS::kSuccess)
							{
								feLog("MayaNode::updateCatalog input painting"
										" failed to get doubleArrayData"
										" length %s\n",
										status.errorString().asChar());
							}
#if FE_MN_PAINT_DEBUG
							feLog("MayaNode::updateCatalog input painting"
									" doubleArrayData length %d\n",length);
#endif

							if(!m_paintSize && length)
							{
#if FE_MN_PAINT_DEBUG
								feLog("MayaNode::updateCatalog input painting"
										" doubleArrayData reload %d\n",length);
#endif
								m_paintSize=length;
							}

							const U32 size=rPainting.size();
							if(size!=length)
							{
								rPainting.resize(length);
							}

							for(U32 m=0;m<length;m++)
							{
								rPainting[m]=doubleArrayData[m];
							}
						}
					}
				}
			}
		}
		else if(instance.is<I32>())
		{
			I32& rValue=spCatalog->catalog<I32>(key);

			MStatus status;
			if(a_doInput)
			{
				MDataHandle handleIn=a_rData.inputValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					rValue=handleIn.asInt();
				}
			}
			else
			{
				MDataHandle handleOut=a_rData.outputValue(rObject,&status);
				handleOut.setInt(rValue);
			}
#if FE_MN_CATALOG_DEBUG
			feLog("MayaNode::updateCatalog now I32 %d\n",rValue);
#endif
		}
		else if(instance.is<SpatialVector>())
		{
			SpatialVector& rVector=spCatalog->catalog<SpatialVector>(key);

			MStatus status;
			if(a_doInput)
			{
				MDataHandle handleIn=a_rData.inputValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					double3& rValue=handleIn.asDouble3();
					set(rVector,rValue[0],rValue[1],rValue[2]);
				}
			}
			else
			{
				MDataHandle handleOut=a_rData.outputValue(rObject,&status);
				handleOut.set3Double(rVector[0],rVector[1],rVector[2]);
			}

#if FE_MN_CATALOG_DEBUG
			feLog("MayaNode::updateCatalog now SpatialVector %s\n",
					c_print(rVector));
#endif
		}
		else if(instance.is<Vector4>())
		{
			// TODO four floats is oddly supported
		}
		else if(instance.is< Array<String> >() ||
				instance.is< Array<I32> >() ||
				instance.is< Array<Real> >() ||
				instance.is< Array<SpatialVector> >())
		{
			const BWORD isString=instance.is< Array<String> >();
			const BWORD isInt=instance.is< Array<I32> >();
			const BWORD isReal=instance.is< Array<Real> >();
			const BWORD isVector=instance.is< Array<SpatialVector> >();

			Array<String>* pStringArray=NULL;
			Array<I32>* pIntArray=NULL;
			Array<Real>* pRealArray=NULL;
			Array<SpatialVector>* pVectorArray=NULL;

			if(isString)
			{
				pStringArray=&spCatalog->catalog< Array<String> >(key);
			}
			else if(isInt)
			{
				pIntArray=&spCatalog->catalog< Array<I32> >(key);
			}
			else if(isReal)
			{
				pRealArray=&spCatalog->catalog< Array<Real> >(key);
			}
			else if(isVector)
			{
				pVectorArray=
						&spCatalog->catalog< Array<SpatialVector> >(key);
			}

			MStatus status;
			if(a_doInput)
			{
				MArrayDataHandle arrayData=
						a_rData.outputArrayValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					I32 length=arrayData.elementCount();

					arrayData.jumpToArrayElement(0);

					if(isString)
					{
						pStringArray->resize(length);
					}
					else if(isInt)
					{
						pIntArray->resize(length);
					}
					else if(isReal)
					{
						pRealArray->resize(length);
					}
					else if(isVector)
					{
						pVectorArray->resize(length);
					}

					for(I32 m=0;m<length;m++)
					{
						const I32 elementIndex=arrayData.elementIndex();
						if(!elementIndex)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("input array %d/%d ZERO\n",m,length);
#endif
							arrayData.next();
							continue;
						}

						MDataHandle handleIn=arrayData.inputValue(&status);
						if(status==MS::kSuccess)
						{
							if(length<elementIndex)
							{
								length=elementIndex;

								if(isString)
								{
									pStringArray->resize(length);
								}
								else if(isInt)
								{
									pIntArray->resize(length);
								}
								else if(isReal)
								{
									pRealArray->resize(length);
								}
								else if(isVector)
								{
									pVectorArray->resize(length);
								}
							}

							if(isString)
							{
								(*pStringArray)[elementIndex-1]=
										handleIn.asString().asChar();

#if FE_MN_ARRAY_DEBUG
								feLog("input string %d/%d %d \"%s\"\n",
										m,length,elementIndex,
										(*pStringArray)[elementIndex-1]
										.c_str());
#endif
							}
							else if(isInt)
							{
								(*pIntArray)[elementIndex-1]=
										handleIn.asInt();

#if FE_MN_ARRAY_DEBUG
								feLog("input int %d/%d %d %d\n",
										m,length,elementIndex,
										(*pIntArray)[elementIndex-1]
										.c_str());
#endif
							}
							else if(isReal)
							{
								(*pRealArray)[elementIndex-1]=
										handleIn.asDouble();

#if FE_MN_ARRAY_DEBUG
								feLog("input real %d/%d %d %.6G\n",
										m,length,elementIndex,
										(*pRealArray)[elementIndex-1]
										.c_str());
#endif
							}
							else if(isVector)
							{
								double3& rValue=handleIn.asDouble3();
								set((*pVectorArray)[elementIndex-1],
										rValue[0],rValue[1],rValue[2]);

#if FE_MN_ARRAY_DEBUG
								feLog("input vector %d/%d %d %s\n",
										m,length,elementIndex,
										c_print(
										(*pVectorArray)[elementIndex-1]));
#endif
							}
						}

						arrayData.next();
					}
				}
			}
			else
			{
				MArrayDataHandle arrayData=
						a_rData.outputArrayValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					I32 length=0;

					if(isString)
					{
						length=pStringArray->size();
					}
					else if(isInt)
					{
						length=pIntArray->size();
					}
					else if(isReal)
					{
						length=pRealArray->size();
					}
					else if(isVector)
					{
						length=pVectorArray->size();
					}

#if FE_MN_ARRAY_DEBUG
					feLog("output length %d\n",length);
#endif

					MArrayDataBuilder builder=arrayData.builder();
					I32 count=builder.elementCount();
					for(I32 m=0;m<count;m++)
					{
						arrayData.jumpToArrayElement(m);
						const I32 elementIndex=arrayData.elementIndex();

#if FE_MN_ARRAY_DEBUG
						feLog("output check %d/%d %d\n",
								m,count,elementIndex);
#endif

						if(elementIndex>length)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("  rm %d\n",elementIndex);
#endif
							builder.removeElement(elementIndex);
							m--;
							count--;
						}
					}

#if FE_MN_ARRAY_DEBUG
					const I32 precount=
#endif
							builder.elementCount();
#if FE_MN_ARRAY_DEBUG
					feLog("output string precount %d\n",precount);
#endif

					for(I32 m=0;m<length;m++)
					{
#if FE_MN_ARRAY_DEBUG
						feLog("  add %d\n",m+1);
#endif
						builder.addElement(m+1);
					}

					//* push changes back to array
					arrayData.set(builder);
					arrayData.setAllClean();

					for(I32 m=0;m<length;m++)
					{
						arrayData.jumpToArrayElement(m);
						const I32 elementIndex=arrayData.elementIndex();
						if(!elementIndex)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output %d/%d ZERO\n",m,length);
#endif
							continue;
						}
						if(elementIndex>length)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output %d/%d EXCESSIVE %d\n",
									m,length,elementIndex);
#endif
							continue;
						}

						if(isString)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output string %d/%d %d \"%s\"\n",
									m,length,elementIndex,
									(*pStringArray)[elementIndex-1].c_str());
#endif

							MDataHandle handleOut=arrayData.outputValue();
							handleOut.setString(
									(*pStringArray)[elementIndex-1].c_str());
						}
						else if(isInt)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output int %d/%d %d %d\n",
									m,length,elementIndex,
									(*pIntArray)[elementIndex-1]);
#endif

							MDataHandle handleOut=arrayData.outputValue();
							handleOut.setInt(
									(*pIntArray)[elementIndex-1]);
						}
						else if(isReal)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output real %d/%d %d %.6G\n",
									m,length,elementIndex,
									(*pRealArray)[elementIndex-1]);
#endif

							MDataHandle handleOut=arrayData.outputValue();
							handleOut.setDouble(
									(*pRealArray)[elementIndex-1]);
						}
						else if(isVector)
						{
							SpatialVector& rVector=
								(*pVectorArray)[elementIndex-1];

#if FE_MN_ARRAY_DEBUG
							feLog("output vector %d/%d %d %s\n",
									m,length,elementIndex,
									c_print(rVector);
#endif


							MDataHandle handleOut=arrayData.outputValue();
							handleOut.set3Double(
									rVector[0],rVector[1],rVector[2]);
						}
					}
				}
			}
		}
		else if(instance.is< Array<Real> >())
		{
			Array<Real>& rRealArray=
					spCatalog->catalog< Array<Real> >(key);

			MStatus status;
			if(a_doInput)
			{
				MArrayDataHandle arrayData=
						a_rData.outputArrayValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					I32 length=arrayData.elementCount();

					arrayData.jumpToArrayElement(0);

#if FE_MN_ARRAY_DEBUG
					feLog("input real resize %d\n",length);
#endif
					rRealArray.resize(length);

					for(I32 m=0;m<length;m++)
					{
						const I32 elementIndex=arrayData.elementIndex();
						if(!elementIndex)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("input real %d/%d ZERO\n",m,length);
#endif
							arrayData.next();
							continue;
						}

						MDataHandle handleIn=arrayData.inputValue(&status);
						if(status==MS::kSuccess)
						{
							if(length<elementIndex)
							{
								length=elementIndex;
								rRealArray.resize(length);
							}

							rRealArray[elementIndex-1]=handleIn.asDouble();

#if FE_MN_ARRAY_DEBUG
							feLog("input real %d/%d %d %.3f\n",
									m,length,elementIndex,
									rRealArray[elementIndex-1]);
#endif
						}

						arrayData.next();
					}
				}
			}
			else
			{
				MArrayDataHandle arrayData=
						a_rData.outputArrayValue(rObject,&status);
				if(status==MS::kSuccess)
				{
					const I32 length=rRealArray.size();
#if FE_MN_ARRAY_DEBUG
					feLog("output real length %d\n",length);
#endif

					MArrayDataBuilder builder=arrayData.builder();
					I32 count=builder.elementCount();
					for(I32 m=0;m<count;m++)
					{
						arrayData.jumpToArrayElement(m);
						const I32 elementIndex=arrayData.elementIndex();

#if FE_MN_ARRAY_DEBUG
						feLog("output real check %d/%d %d\n",
								m,count,elementIndex);
#endif

						if(elementIndex>length)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("  rm %d\n",elementIndex);
#endif
							builder.removeElement(elementIndex);
							m--;
							count--;
						}
					}

#if FE_MN_ARRAY_DEBUG
					const I32 precount=
#endif
							builder.elementCount();
#if FE_MN_ARRAY_DEBUG
					feLog("output real precount %d\n",precount);
#endif

					for(I32 m=0;m<length;m++)
					{
#if FE_MN_ARRAY_DEBUG
						feLog("  add %d\n",m+1);
#endif
						builder.addElement(m+1);
					}

#if FE_MN_ARRAY_DEBUG
					const I32 recount=
#endif
							builder.elementCount();
#if FE_MN_ARRAY_DEBUG
					feLog("output real recount %d\n",recount);
#endif

					//* push changes back to array
					arrayData.set(builder);
					arrayData.setAllClean();

					for(I32 m=0;m<length;m++)
					{
						arrayData.jumpToArrayElement(m);

						const I32 elementIndex=arrayData.elementIndex();
						if(!elementIndex)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output real %d/%d ZERO\n",m,length);
#endif
							continue;
						}
						if(elementIndex>length)
						{
#if FE_MN_ARRAY_DEBUG
							feLog("output real %d/%d EXCESSIVE %d\n",
									m,length,elementIndex);
#endif
							continue;
						}

#if FE_MN_ARRAY_DEBUG
						feLog("output real %d/%d %d %.3f\n",
								m,length,elementIndex,
								rRealArray[elementIndex-1]);
#endif

						MDataHandle handleOut=arrayData.outputValue();
						handleOut.setDouble(rRealArray[elementIndex-1]);
					}
				}
			}
		}
		else if(instance.is< sp<Component> >())
		{
			//* meshes handled in updateComponents()

			sp<Component>& rspComponent=instance.cast< sp<Component> >();
			sp<MayaRamp> spRamp=rspComponent;
			if(spRamp.isValid())
			{
				MStatus status;
				MRampAttribute curveAttribute(thisMObject(),rObject,&status);
				FEASSERT(status);

				spRamp->setRamp(curveAttribute);
			}
		}
	}

	updateComponents(FALSE,a_doInput);

#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::updateCatalog done\n");
#endif
}

void MayaNode::updateComponents(BWORD a_constructing,BWORD a_doInput)
{
#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::updateComponents \"%s\" \"%s\" constructing %d doInput %d\n",
			m_typeName.c_str(),m_nodeName.c_str(),a_constructing,a_doInput);
#endif

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("MayaNode:updateComponents operator is not a Catalog\n");
		return;
	}

	Registration& rRegistration=ms_mayaRegistry[m_typeName];

	BWORD autocopyInput=FALSE;
	if(rRegistration.m_autoOutput)
	{
#if FE_MN_CATALOG_DEBUG
		feLog("MayaNode::updateComponents auto-output\n");
#endif

		spCatalog->catalog< sp<Component> >(feMayaDefaultOutputSurface);
		spCatalog->catalog<String>(feMayaDefaultOutputSurface,"IO")="output";
		autocopyInput=TRUE;
	}

	sp<Registry> spRegistry=m_mayaContext.master()->registry();

	Array<String> keys;
	spCatalog->catalogKeys(keys);

	const U32 number=keys.size();
	for(U32 m=0;m<number;m++)
	{
		if(keys[m]=="geomMatrix")
		{
			continue;
		}

		Instance instance;
		bool success=spCatalog->catalogLookup(keys[m],instance);
		if(!success)
		{
			feLog("MayaNode::updateComponents \"%s\" invalid Instance \"%s\"\n",
					m_nodeName.c_str(),keys[m].c_str());
			continue;
		}

		char* keyname=strdup(keys[m].c_str());

#if FE_MN_CATALOG_DEBUG
		char* cleanname=strdup(keys[m].substitute(" ","_").c_str());
#endif

		if(instance.is< sp<Component> >())
		{
			sp<Component>& rspComponent=instance.cast< sp<Component> >();

			if(!rspComponent.isValid())
			{
				String implementation=spCatalog->catalogOrDefault<String>(
						keyname,"implementation",
						"SurfaceAccessibleI.SurfaceAccessibleMaya");
				if(implementation=="curve")
				{
					implementation="SurfaceAccessibleI";
				}
				if(!implementation.empty())
				{
					if(implementation=="SurfaceAccessibleI")
					{
						implementation=
								"SurfaceAccessibleI.SurfaceAccessibleMaya";
					}
					if(implementation=="DrawI")
					{
						implementation=(keyname==String("Brush"))?
								"DrawI.DrawCached": "DrawI.MayaDraw";
					}
					if(implementation=="RampI")
					{
						implementation="RampI.MayaRamp";
					}
					if(implementation=="OperatorGraphI")
					{
						implementation="OperatorGraphI.MayaGraph";
					}
					if(!a_constructing ||
							implementation.dotMatch("SurfaceAccessibleI"))
					{
						rspComponent=spRegistry->create(implementation);
						rspComponent->setName(
								String(typeName().asChar())+" "+keyname);
					}
				}
				sp<DrawI> spDrawI=rspComponent;
				if(spDrawI.isValid())
				{
					if(keyname==String("Brush"))
					{
						MayaBrush::staticSetup();

						m_spDrawBrush=spDrawI;

						m_spDrawBrushOverlay=spRegistry->create(implementation);
						m_spDrawBrushOverlay->setName(
								String(typeName().asChar())+
								" DrawBrushOverlay");

						setupBrush(m_mayaContext.scope());

						//* NOTE not thread safe
#if FE_MN_CATALOG_DEBUG
						feLog("brush for \"%s\"\n",m_nodeName.c_str());
#endif
						ms_brushMap[m_nodeName].m_pMayaNode=this;
					}
				}
				sp<SurfaceAccessibleI> spSurfaceAccessibleI=rspComponent;
				if(spSurfaceAccessibleI.isValid())
				{
					const String iterate=
							spCatalog->catalogOrDefault<String>(
							keyname,"iterate","");
					if(!iterate.empty())
					{
						spCatalog->catalog<String>(
								iterate,"iterates")=keyname;
					}

					String io=spCatalog->catalogOrDefault<String>(
							keyname,"IO","input");
					if(io=="input")
					{
						I32 channel=m_surfaceInputArray.size();
#if FE_MN_CATALOG_DEBUG
						feLog("MayaNode::updateComponents"
								" adding surface input %d \"%s\" \"%s\"\n",
								channel,cleanname,keyname);
#endif
						spCatalog->catalog<I32>(keyname,"channel")=channel;
						m_originalInputArray.push_back(spSurfaceAccessibleI);
						m_surfaceInputArray.push_back(spSurfaceAccessibleI);

						if(!iterate.empty())
						{
							spCatalog->catalog<I32>(
									iterate,"channel")=channel;
						}

						if(autocopyInput)
						{
							spCatalog->catalog<String>(
									feMayaDefaultOutputSurface,"copy")=keyname;
							autocopyInput=FALSE;
						}
					}
					else
					{
#if FE_MN_CATALOG_DEBUG
						feLog("MayaNode::updateComponents"
								" adding surface output \"%s\" \"%s\"\n",
								cleanname,keyname);
#endif
						m_spOriginalOutput=spSurfaceAccessibleI;
						m_spSurfaceOutput=spSurfaceAccessibleI;

						// HACK
						m_nameOutput=keyname;
					}
				}
			}

			if(!a_doInput)
			{
				sp<SurfaceAccessibleI> spSurfaceAccessibleI=rspComponent;
				if(spSurfaceAccessibleI.isValid())
				{
					String io=spCatalog->catalogOrDefault<String>(
							keyname,"IO","input");
					if(io=="input")
					{
						spCatalog->catalog<bool>(keyname,"replaced")=FALSE;
					}
				}
			}
		}
		else if(instance.is< Array< sp<Component> > >())
		{
			Array< sp<Component> >& rspArray=
					instance.cast< Array< sp<Component> > >();

			//* TODO
		}
	}

#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::updateComponents done\n");
#endif
}

void MayaNode::updateComponentArrays(I32 a_iterateIndex,I32 a_iterateCount)
{
#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::updateComponentArrays %d/%d\n",
			a_iterateIndex,a_iterateCount);
#endif

	FEASSERT(a_iterateIndex<a_iterateCount);

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("MayaNode:updateComponentArrays operator is not a Catalog\n");
		return;
	}

	Registration& rRegistration=ms_mayaRegistry[m_typeName];

	sp<Registry> spRegistry=m_mayaContext.master()->registry();

	Array<String> keys;
	spCatalog->catalogKeys(keys);

	const U32 number=keys.size();
	for(U32 m=0;m<number;m++)
	{
		if(keys[m]=="geomMatrix")
		{
			continue;
		}

		Instance instance;
		bool success=spCatalog->catalogLookup(keys[m],instance);
		if(!success)
		{
			feLog("MayaNode::updateComponentArrays"
					" \"%s\" invalid Instance \"%s\"\n",
					m_nodeName.c_str(),keys[m].c_str());
			continue;
		}

		char* keyname=strdup(keys[m].c_str());

#if FE_MN_CATALOG_DEBUG
		char* cleanname=strdup(keys[m].substitute(" ","_").c_str());
#endif

		if(instance.is< Array< sp<Component> > >())
		{
			Array< sp<Component> >& rspArray=
					instance.cast< Array< sp<Component> > >();

			if(rspArray.size()!=a_iterateCount)
			{
				rspArray.resize(a_iterateCount);
			}

			const String io=spCatalog->catalogOrDefault<String>(
					keyname,"IO","input");
			const String iterates=spCatalog->catalogOrDefault<String>(
					keyname,"iterates","");

			if(a_iterateIndex>=0)
			{
				sp<Component>& rspComponent=rspArray[a_iterateIndex];

				if(!rspComponent.isValid())
				{
					rspComponent=spRegistry->create(
								"SurfaceAccessibleI.SurfaceAccessibleMaya");
					FEASSERT(rspComponent.isValid());
					rspComponent->setName(m_nodeName+" "+keyname);

					sp<SurfaceAccessibleI> spSurfaceAccessibleI=rspComponent;
					if(spSurfaceAccessibleI.isValid())
					{
						spCatalog->catalog< sp<Component> >(iterates)=
								spSurfaceAccessibleI;

						if(io=="input")
						{
							const I32 channel=
									spCatalog->catalog<I32>(keyname,"channel");

							m_surfaceInputArray[channel]=spSurfaceAccessibleI;
						}
						else
						{
							m_spSurfaceOutput=spSurfaceAccessibleI;
						}
					}
				}
			}
			else
			{
				//* restore originals

				if(!iterates.empty())
				{
					sp<Component>& rspComponent=
							spCatalog->catalog< sp<Component> >(iterates);

					sp<SurfaceAccessibleI> spSurfaceAccessibleI=rspComponent;
					if(spSurfaceAccessibleI.isValid())
					{
						if(io=="input")
						{
							const I32 channel=
									spCatalog->catalog<I32>(keyname,"channel");

							rspComponent=m_originalInputArray[channel];
							m_surfaceInputArray[channel]=rspComponent;
						}
						else
						{
							rspComponent=m_spOriginalOutput;
							m_spSurfaceOutput=rspComponent;
						}
					}
				}
			}
		}
	}

#if FE_MN_CATALOG_DEBUG
	feLog("MayaNode::updateComponentArrays done\n");
#endif
}

void MayaNode::dirty(BWORD a_aggressive)
{
	bool displayEnabled=false;

#if FE_MN_COMPUTE_DEBUG
	feLog("MayaNode::dirty \"%s\"\n",m_nodeName.c_str());
	displayEnabled=true;
#endif

	MGlobal::executeCommandOnIdle(("dgdirty "+m_nodeName).c_str(),
			displayEnabled);
}

BWORD MayaNode::interrupted(void)
{
	return m_computation.isInterruptRequested();
}

void MayaNode::precompute(Real a_time)
{
	m_typeName=typeName().asChar();
	m_nodeName=name().asChar();

#if FE_MN_COMPUTE_DEBUG
	feLog("MayaNode::precompute \"%s\" \"%s\"\n",
			m_nodeName.c_str(),m_typeName.c_str());
#endif

	// TODO check maya node state (normal, passthrough, etc)

	Registration& rRegistration=ms_mayaRegistry[m_typeName];
	m_implementation=rRegistration.m_implementation;

#if FE_MN_COMPUTE_DEBUG
	const U32 uniqueId=rRegistration.m_uniqueId;

	feLog("  name \"%s\"\n",m_typeName.c_str());
	feLog("  unique ID %p\n",uniqueId);
	feLog("  implementation \"%s\"\n",m_implementation.c_str());
#endif

	std::map<String,Parameter>& rParameterMap=rRegistration.m_parameterMap;

	if(!m_spOperatorSurfaceI.isValid())
	{
		sp<Registry> spRegistry=m_mayaContext.master()->registry();
		m_spOperatorSurfaceI=spRegistry->create(m_implementation);
		m_spOperatorSurfaceI->setName(m_nodeName+" "+m_implementation);

#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::precompute m_spOperatorSurfaceI \"%s\" valid=%d\n",
				m_implementation.c_str(),m_spOperatorSurfaceI.isValid());
#endif

		if(m_spOperatorSurfaceI.isValid())
		{
			m_spOperatorSurfaceI->setPlugin(this);
		}

		m_bound=FALSE;
	}
}

//* TODO only components
MStatus MayaNode::setDependentsDirty(const MPlug &a_rPlugBeingDirtied,
	MPlugArray& a_rAffectedPlugs)
{
	Registration& rRegistration=ms_mayaRegistry[m_typeName];

	const String plug=a_rPlugBeingDirtied.name().asChar();

#if FALSE
	const String clean=plug.replace(".*\\.","");
#else
	String clean;
	String buffer=plug;
	String token;
	while(!(token=buffer.parse("\"",".")).empty())
	{
		clean=token;
	}
#endif
	const String key=rRegistration.m_cleanMap[clean];

//	feLog("MayaNode::setDependentsDirty \"%s\" \"%s\" \"%s\"\n",
//			plug.c_str(),clean.c_str(),key.c_str());

	if(key.empty())
	{
#if FE_CODEGEN<=FE_DEBUG
//		feLog("MayaNode::setDependentsDirty no key for \"%s\" clean \"%s\"\n",
//				plug.c_str(),clean.c_str());
#endif
	}
	else
	{
		sp<Catalog> spCatalog=m_spOperatorSurfaceI;
		spCatalog->catalog<bool>(key,"replaced")=TRUE;
	}

	return MPxNode::setDependentsDirty(a_rPlugBeingDirtied,a_rAffectedPlugs);
}

MStatus MayaNode::compute(const MPlug& a_rPlug,MDataBlock& a_rData)
{
	MStatus status=MS::kSuccess;

	MDataHandle stateData=a_rData.outputValue(state,&status);
	if(!status)
	{
		feLog("MayaNode::compute no state data (fail)\n");
		return MStatus::kFailure;
	}
	const I32 nodeState=stateData.asShort();

	const String plugName(a_rPlug.name().asChar());

	//* 0 = Normal, 1 = Bypass
	if(nodeState==1)
	{
		MPlug inPlug(thisMObject(),MPxDeformerNode::input);
		inPlug.selectAncestorLogicalIndex(
				a_rPlug.logicalIndex(),MPxDeformerNode::input);

		MDataHandle inputData=a_rData.inputValue(inPlug,&status);
		if(status)
		{
			MDataHandle outputData=a_rData.outputValue(a_rPlug,&status);
			if(!status)
			{
				feLog("MayaNode::compute bypass failed to access output\n");
				return MStatus::kFailure;
			}

			outputData.set(inputData.asMesh());
		}

#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::compute plug \"%s\" bypassed\n",plugName.c_str());
#endif

		return MStatus::kSuccess;
	}

	const double startFrame=MAnimControl::minTime().value();
	const double endFrame=MAnimControl::maxTime().value();
	const double frame=MAnimControl::currentTime().value();
	const double time=frame;

#if FE_MN_COMPUTE_DEBUG
	feLog("MayaNode::compute start %.6G end %.6G frame %.6G plug \"%s\"\n",
			startFrame,endFrame,frame,plugName.c_str());
#endif

	const I32 plugNameLength=plugName.length();
	if(plugNameLength>3 && plugName.c_str()[plugNameLength-1]==']')
	{
		String plugParam;
		String buffer(plugName);
		String token;
		while(!(token=buffer.parse("\"",".")).empty())
		{
			plugParam=token;
		}

		const String plugVar=plugParam.parse("\"","[");

#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::compute plug param var \"%s\"\n",plugVar.c_str());
#endif

		if(plugVar!="outputGeometry")
		{
#if FE_MN_COMPUTE_DEBUG
			feLog("MayaNode::compute skip general plug with []\n",
					plugName.c_str());
#endif

			return MStatus::kSuccess;
		}
	}

	m_aTime(m_cookSignal)=time;
	m_aFrame(m_cookSignal)=frame;
	m_aStartFrame(m_cookSignal)=startFrame;
	m_aEndFrame(m_cookSignal)=endFrame;

	precompute(time);

	if(!m_bound)
	{
		if(m_spOperatorSurfaceI.isValid())
		{
			updateCatalog(a_rData,time,TRUE);

			m_lastName=m_implementation;
			insertHandler(m_spOperatorSurfaceI);
			m_bound=TRUE;

			restoreState();
		}
	}
	else
	{
		updateCatalog(a_rData,time,TRUE);
	}

	//* HACK icon test
//	MFnDependencyNode depNode(thisMObject());
//	depNode.setIcon("... /FE_20.png");

	Registration& rRegistration=ms_mayaRegistry[m_typeName];
	std::map<String,Parameter>& rParameterMap=rRegistration.m_parameterMap;

	if(m_spOperatorSurfaceI.isValid())
	{
		m_spOperatorSurfaceI->setName(m_nodeName);

		sp<SurfaceAccessibleMaya> spSurfaceOutput=m_spSurfaceOutput;
		if(spSurfaceOutput.isNull())
		{
			sp<Registry> spRegistry=m_mayaContext.master()->registry();
			spSurfaceOutput=spRegistry->create(
					"SurfaceAccessibleI.SurfaceAccessibleMaya");
			if(spSurfaceOutput.isNull())
			{
				feLog("MayaNode::compute failed to create output on \"%s\"\n",
						m_nodeName.c_str());
				return MStatus::kFailure;
			}
			spSurfaceOutput->setName(m_nodeName+" output");
		}

		if(m_spDrawGuideCached.isValid())
		{
			m_spDrawGuideCached->clearInput();
		}

		sp<Catalog> spCatalog=m_spOperatorSurfaceI;

		U32 iterateCount=0;
		BWORD originalConnected=FALSE;
		I32 channel=0;
		if(!m_nameOutput.empty())
		{
			const String copy=spCatalog->catalogOrDefault<String>(m_nameOutput,
					"copy","");
			channel= -1;

			// TODO multiple outputs with individual channels
			if(!copy.empty())
			{
				channel=spCatalog->catalogOrDefault<I32>(copy,"channel",-1);

				const String copyIterate=
						spCatalog->catalogOrDefault<String>(copy,"iterate","");
				if(!copyIterate.empty())
				{
					MObject paramItMesh=
							rParameterMap[copyIterate].m_object;

					MStatus status=MS::kSuccess;

					MDataHandle iterateData=
							a_rData.inputValue(paramItMesh,&status);
					FEASSERT(status);

					MArrayDataHandle arrayData(iterateData);
					iterateCount=arrayData.elementCount();

					MObject paramInMesh=rParameterMap[copy].m_object;
					MPlug plug(thisMObject(),paramInMesh);

					MPlugArray connections;
//					const bool found=
							plug.connectedTo(connections,true,false);

					originalConnected=(connections.length()>0);
				}
			}
		}

		//* only used on Maya-specific nodes
		const BWORD outputIsArray=spCatalog->catalogOrDefault<bool>(
					feMayaDefaultOutputSurface,"array",false);

		spCatalog->catalog<bool>(
				feMayaDefaultOutputSurface,"replaced")=true;

		m_computation.beginComputation();

		BWORD isDeformer=FALSE;

		const I32 iterations=iterateCount? originalConnected+iterateCount: 1;
		for(I32 iteration=0;iteration<iterations && !interrupted();iteration++)
		{
			const I32 iterateIndex=
					iterateCount? iteration-originalConnected: -1;

#if FE_MN_COMPUTE_DEBUG
			feLog("MayaNode::compute iteration %d/%d iterate %d/%d array %d\n",
						iteration,iterations,iterateIndex,iterateCount,
						outputIsArray);
#endif

			updateComponentArrays(iterateIndex,iterateCount);

			spCatalog->catalog<I32>(
					feMayaDefaultOutputSurface,"iteration")=iteration;
			spCatalog->catalog<I32>(
					feMayaDefaultOutputSurface,"iterations")=iterations;

			BWORD useTransform=FALSE;
			SpatialTransform transform;

			MDataHandle channelData;

			U32 surfaces=m_surfaceInputArray.size();
			FEASSERT(surfaces==rRegistration.m_inputChannelArray.size());
			for(int surface=0;surface<surfaces;surface++)
			{
				sp<SurfaceAccessibleMaya> spSurfaceInput=
						m_surfaceInputArray[surface];

				const String inputName=
						rRegistration.m_inputChannelArray[surface];
				const String paramName=(iterateIndex>=0)?
						spCatalog->catalogOrDefault<String>(
						inputName,"iterate",inputName): inputName;

				MObject paramInMesh=rParameterMap[paramName].m_object;

//				feLog("input %d/%d \"%s\" using \"%s\"\n",
//						surface,surfaces,inputName.c_str(),paramName.c_str());

				MStatus status=MS::kSuccess;

				MDataHandle inputData;

				//* NOTE check for alternate input as deformer
				if(!surface)
				{
					MPlug inPlug(thisMObject(),MPxDeformerNode::input);
					inPlug.selectAncestorLogicalIndex(
							a_rPlug.logicalIndex(),MPxDeformerNode::input);

					MDataHandle meshData2=a_rData.inputValue(inPlug,&status);
					if(status)
					{
//						feLog("using deformer input\n");

						inputData=meshData2.child(inputGeom);

						if(inputData.type() != MFnData::kMesh)
						{
							feLog("MayaNode::compute incorrect deformer"
									" input geometry type (fail)\n");
							m_computation.endComputation();
							return MStatus::kFailure;
						}

						MDataHandle inputGroup=inputData.child(groupId);
						const unsigned int groupID=inputGroup.asLong();

						//* TODO delete group later
						MItGeometry* pGroupIt=
								new MItGeometry(inputData,groupID,false);
						spSurfaceInput->setGroupIt(pGroupIt);

//						feLog("%d group %d\n",a_rPlug.logicalIndex(),groupID);

#if FALSE
						int weightIndex=0;
						for(;!pGroupIt->isDone();pGroupIt->next())
						{
							const int groupIndex=pGroupIt->index();
							const Real weight=weightValue(a_rData,
									a_rPlug.logicalIndex(),groupIndex);
							feLog("iter %d %d %.6G\n",
									weightIndex,groupIndex,weight);
							weightIndex++;
						}
#endif

						paramInMesh=MPxDeformerNode::input;
						isDeformer=TRUE;
					}
					else
					{
						spSurfaceInput->setGroupIt(NULL);
					}
				}

				if(surface || !isDeformer)
				{
					const MDataHandle meshData=
							a_rData.inputValue(paramInMesh,&status);
					FEASSERT(status);

					inputData=meshData;

					if(paramName != inputName)
					{
						MArrayDataHandle arrayData(meshData);

						const U32 arrayCount=arrayData.elementCount(&status);

						FEASSERT(status);
						FEASSERT(arrayCount==iterateCount);

						if(iterateIndex<arrayCount)
						{
							status=arrayData.jumpToArrayElement(iterateIndex);
							FEASSERT(status);

//							const U32 atIndex=
									arrayData.elementIndex(&status);
							FEASSERT(status);

							inputData=arrayData.outputValue();
						}
						else if(iterateIndex==arrayCount)
						{
							feLog("MayaNode::compute"
									" node \"%s\" mesh array \"%s\""
									" only has %d of %d expected inputs\n",
									m_nodeName.c_str(),paramName.c_str(),
									arrayCount,iterateCount);
						}
					}
				}

				if(!surface)
				{
					const Parameter& parameter=rParameterMap["geomMatrix"];
					const MObject& rObject=parameter.m_object;

					MArrayDataHandle arrayData=
							a_rData.inputValue(rObject,&status);

					const U32 arrayCount=arrayData.elementCount(&status);
					FEASSERT(status);

					U32 useIndex=0;
					if(isDeformer)
					{
						//* TODO check
						useIndex=a_rPlug.logicalIndex();
					}
					else
					{
						useIndex=(iterateIndex>=0)? iterateIndex: 0;
					}

					if(useIndex<arrayCount)
					{
						status=arrayData.jumpToArrayElement(useIndex);
						FEASSERT(status);

//						const U32 atIndex=
								arrayData.elementIndex(&status);
						FEASSERT(status);

						MDataHandle matrixData=
							arrayData.outputValue(&status);

						if(status==MS::kSuccess)
						{
							const MFloatMatrix geomMatrix=
									matrixData.asFloatMatrix();

							set(transform,geomMatrix[0]);

//							feLog("\"%s\" geomMatrix\n%s\n",
//									m_nodeName.c_str(),c_print(transform));

							if(	transform.column(0)[0]!=1.0 ||
								transform.column(0)[1]!=0.0 ||
								transform.column(0)[2]!=0.0 ||
								transform.column(1)[0]!=0.0 ||
								transform.column(1)[1]!=1.0 ||
								transform.column(1)[2]!=0.0 ||
								transform.column(2)[0]!=0.0 ||
								transform.column(2)[1]!=0.0 ||
								transform.column(2)[2]!=1.0 ||
								transform.column(3)[0]!=0.0 ||
								transform.column(3)[1]!=0.0 ||
								transform.column(3)[2]!=0.0)
							{
//								feLog("  not identity\n");
								useTransform=TRUE;
							}
						}
					}
					else if(iterateIndex==arrayCount)
					{
						feLog("MayaNode::compute"
								" node \"%s\" geomMatrix"
								" expected element %d with only %d found\n",
								m_nodeName.c_str(),
								useIndex,arrayCount);
					}

				}

				if(!surface && useTransform)
				{
					spSurfaceInput->setTransform(transform);
				}
				else
				{
					spSurfaceInput->clearTransform();
				}

				spSurfaceInput->setMeshData(inputData);

				if(surface==channel)
				{
					channelData=inputData;

					spCatalog->catalog<bool>(
							feMayaDefaultOutputSurface,"replaced")=
							spCatalog->catalogOrDefault<bool>(
							inputName,"replaced",true);

#if FE_MN_COMPUTE_DEBUG
					feLog("input channel %d replaced %d\n",channel,
							spCatalog->catalog<bool>(
							feMayaDefaultOutputSurface,"replaced"));
#endif
				}

				MPlug plug(thisMObject(),paramInMesh);

				MPlugArray connections;
//				const bool found=
						plug.connectedTo(connections,true,false);
//				feLog("input connections %d\n",connections.length());

				if(connections.length()>0)
				{
					MObject meshNode=connections[0].node();
					spSurfaceInput->setMeshNode(meshNode);

#if FALSE
					MFnMesh meshFn(meshNode);
					const U32 parentCount=meshFn.parentCount();
					feLog("MayaNode::compute input %d \"%s\" parentCount %d\n",
							surface,meshFn.name().asChar(),parentCount);

					const unsigned int instanceNumber=0;
					MObjectArray shaderArray;
					MIntArray shaderOfPolygon;

					MStatus status=meshFn.getConnectedShaders(instanceNumber,
							shaderArray,shaderOfPolygon);
					const U32 shaderCount=shaderArray.length();
					const U32 polyCount=shaderOfPolygon.length();

					feLog("  shaderCount %d polyCount %d\n",
							shaderCount,polyCount);
#endif
				}
				else
				{
					spSurfaceInput->setMeshNode(MObject());
				}

#if FALSE
				// HACK alternate attribute test
				sp<SurfaceAccessorI> spAccessor=spSurfaceInput->accessor(
						SurfaceAccessibleI::e_point,"milliIndex");
				FEASSERT(spAccessor.isValid());

				if(spAccessor.isValid())
				{
					const I32 pointCount=spAccessor->count();
					for(I32 index=0;index<pointCount;index++)
					{
//						feLog("milli %d/%d\n",index,spAccessor->count());
						spAccessor->set(index,0,Real(0.001*index));
					}
				}
#endif
			}

			String outputName=rRegistration.m_outputChannelArray[0];
			const String paramName=(iterateIndex>=0)?
					spCatalog->catalog<String>(outputName,"iterate"):
					outputName;
			MObject paramOutMesh=rParameterMap[paramName].m_object;
			MPlug plug(thisMObject(),paramOutMesh);

			MStatus status=MS::kSuccess;

			MDataHandle meshData;
			if(isDeformer)
			{
				meshData=a_rData.outputValue(a_rPlug,&status);
				plug=a_rPlug;
			}
			else
			{
				meshData=a_rData.outputValue(paramOutMesh,&status);
			}
			FEASSERT(status);

			MDataHandle outputData;
			if(!outputIsArray)
			{
				if(iterateIndex>=0)
				{
					MArrayDataHandle arrayData(meshData);

					const U32 arrayCount=arrayData.elementCount(&status);

					FEASSERT(status);
					FEASSERT(arrayCount==iterateCount);
					FEASSERT(iterateIndex<arrayCount);

					status=arrayData.jumpToArrayElement(iterateIndex);
					FEASSERT(status);

					outputData=arrayData.outputValue();
				}
				else
				{
					outputData=meshData;
				}
			}

			if(channel>=0 && channel<surfaces)
			{
#if FE_MN_COMPUTE_DEBUG
				feLog("MayaNode::compute output=input replaced %d\n",
						spCatalog->catalog<bool>(
						feMayaDefaultOutputSurface,"replaced"));
#endif

				if(channelData.asMesh().isNull())
				{
#if FE_MN_COMPUTE_DEBUG
					//* can happen with redundant outputs
					feLog("MayaNode::compute"
							" \"%s\" channel %d data NULL (fail)\n",
							m_nodeName.c_str(),channel);
#endif
					m_computation.endComputation();
					return MStatus::kFailure;
				}

				if(spCatalog->catalog<bool>(
						feMayaDefaultOutputSurface,"replaced"))
				{
					MFnMeshData meshDataFn;
					MObject newMeshData=meshDataFn.create();

					MFnMesh newMeshFn;
					newMeshFn.copy(channelData.asMesh(),newMeshData,&status);
					if(!status)
					{
						feLog("MayaNode::compute"
								" \"%s\" channel %d mesh copy failed: %s\n",
								m_nodeName.c_str(),channel,
								status.errorString().asChar());
						m_computation.endComputation();
						return status;
					}

					outputData.set(newMeshData);

					const U32 vertices=newMeshFn.numVertices();
					if(m_paintSize!=vertices)
					{
						m_paintSize=vertices;

						//* second update
						updateCatalog(a_rData,time,TRUE);
					}

					spSurfaceOutput->setMeshData(outputData);
				}

#if TRUE
				//* TODO maybe don't run mel for every node

				String findCommand="fe_find_shape(\""+m_nodeName+"\")";

				MString melResult;
				MGlobal::executeCommand(findCommand.c_str(),melResult);
//				feLog("MayaNode::compute fe_find_shape -> \"%s\"\n",
//						melResult.asChar());

				MSelectionList selection;
				selection.add(melResult);

				MObject meshNode;
				selection.getDependNode(0,meshNode);

				spSurfaceOutput->setMeshNode(meshNode);

				m_shapeNode=melResult.asChar();

				if(spCatalog->cataloged("Brush"))
				{
					ms_shapeMap[m_shapeNode].m_pMayaNode=this;
				}
#else

				MPlugArray connections;
				const bool found=plug.connectedTo(connections,false,true);
//				feLog("output connections %d\n",connections.length());

				if(connections.length()>0)
				{
					MObject meshNode=connections[0].node();
					spSurfaceOutput->setMeshNode(meshNode);
				}
#endif
			}
			else
			{
				// erase all prior geometry

#if FE_MN_COMPUTE_DEBUG
				feLog("MayaNode::compute output reset\n");
#endif
				MPlug firstPlug=plug;

				if(outputIsArray)
				{
					firstPlug=plug.elementByLogicalIndex(0);

					MArrayDataHandle arrayData=
							a_rData.outputArrayValue(paramOutMesh,&status);
					spSurfaceOutput->setMeshArrayData(arrayData);
				}
				else
				{
					MFnMeshData meshDataFn;
					MObject newMeshData=meshDataFn.create();

					int numVertices=0;
					int numPolygons=0;
					MFloatPointArray vertexArray;
					MIntArray polygonCounts;
					MIntArray polygonConnects;

					status=MS::kSuccess;
					MFnMesh newMeshFn;
					MObject mesh=newMeshFn.create(numVertices,numPolygons,
							vertexArray,polygonCounts,polygonConnects,
							newMeshData,&status);
					if(!status)
					{
						feLog("MayaNode::compute create failed: %s\n",
								status.errorString().asChar());
					}

					outputData.set(newMeshData);

					spSurfaceOutput->setMeshData(outputData);
				}

				MPlugArray connections;
//				const bool found=
						firstPlug.connectedTo(connections,false,true);
//				feLog("output connections %d\n",connections.length());

				MObject meshNode;
				if(connections.length()>0)
				{
					meshNode=connections[0].node();
				}
				if(meshNode==MObject::kNullObj)
				{
					//* if nothing connected to element 0, try whole array
					plug.connectedTo(connections,false,true);
					if(connections.length()>0)
					{
						meshNode=connections[0].node();
					}
				}
				spSurfaceOutput->setMeshNode(meshNode);

				m_paintSize=0;
			}

			if(useTransform)
			{
				spSurfaceOutput->setTransform(transform);
			}
			else
			{
				spSurfaceOutput->clearTransform();
			}

			if(!outputIsArray)
			{
				sp<MayaDraw> spMayaDraw=m_spDrawOutput;
				if(spMayaDraw.isValid())
				{
#if FE_MN_COMPUTE_DEBUG
					feLog("MayaNode::compute set draw mesh\n");
#endif
					spMayaDraw->setMeshData(outputData);
				}
			}

			m_aSurfaceOutput(m_cookSignal)=spSurfaceOutput;

			try
			{
#if FE_MN_COMPUTE_DEBUG
				feLog("MayaNode::compute signaling\n");
#endif
				m_spSignalerI->signal(m_cookSignal);
#if FE_MN_COMPUTE_DEBUG
				feLog("MayaNode::compute signaled\n");
#endif
			}
			catch(const Exception& e)
			{
				spCatalog->catalog<String>("error")+=
						"FE EXCEPTION: "+print(e);
			}
			catch(const std::exception& std_e)
			{
				spCatalog->catalog<String>("error")+=
						"stdlib EXCEPTION: "+String(std_e.what());
			}
#ifdef FE_BOOST_EXCEPTIONS
			catch(const boost::exception& boost_e)
			{
				spCatalog->catalog<String>("error")+=
						"boost EXCEPTION: "+
						String(boost::diagnostic_information(boost_e).c_str());
			}
#endif
			catch(...)
			{
				spCatalog->catalog<String>("error")+=
						"UNRECOGNIZED EXCEPTION";
			}

//			outputData.copy(spSurfaceOutput->meshData());
		}

		m_computation.endComputation();

		//* TODO draw Guide data

		updateCatalog(a_rData,time,FALSE);
	}

	m_lastTime=time;

/*	TODO MGlobal::
	static void			displayInfo( const MString & theMessage );
	static void			displayWarning( const MString & theWarning );
	static void			displayError( const MString & theError );
*/

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	status=spCatalog->catalogOrDefault<String>("error","").empty()?
			MS::kSuccess: MS::kFailure;

	relayErrors();

#if FE_MN_COMPUTE_DEBUG
	feLog("MayaNode::compute done\n");
#endif

	return status;
}

void MayaNode::relayErrors(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;

	String messageString=spCatalog->catalogOrDefault<String>("message","");
	if(!messageString.empty())
	{
		MGlobal::displayInfo((m_nodeName+": "+messageString).c_str());
		spCatalog->catalog<String>("message")="";

#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::compute message string \"%s\"\n",
				messageString.c_str());
#endif
	}

	String warningString=spCatalog->catalogOrDefault<String>("warning","");
	if(!warningString.empty())
	{
		MGlobal::displayWarning((m_nodeName+": "+warningString).c_str());
		spCatalog->catalog<String>("warning")="";

#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::compute warning string \"%s\"\n",
				warningString.c_str());
#endif
	}

	String errorString=spCatalog->catalogOrDefault<String>("error","");
	if(!errorString.empty())
	{
		MGlobal::displayError((m_nodeName+": "+errorString).c_str());
		spCatalog->catalog<String>("error")="";

#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::compute error string \"%s\"\n",
				errorString.c_str());
#endif
	}
}

void MayaNode::brush(WindowEvent& a_rEvent,
	const SpatialVector& a_rRayOrigin,const SpatialVector& a_rRayDirection)
{
#if FE_MN_COMPUTE_DEBUG
	feLog("MayaNode::brush valid %d event %s\n",
			m_spDrawBrush.isValid(),c_print(a_rEvent));
	feLog(" ray %s dir %s\n",c_print(a_rRayOrigin),c_print(a_rRayDirection));
#endif

	if(!m_spDrawBrush.isValid())
	{
		return;
	}

	if(a_rEvent.isNull())
	{
#if FE_MN_COMPUTE_DEBUG
		feLog("MayaNode::brush null event\n");
#endif
		return;
	}


	m_spDrawBrush->clearInput();
	m_spDrawBrushOverlay->clearInput();

	m_aWindowEvent(m_brushSignal)=a_rEvent.record();
	m_aRayOrigin(m_brushSignal)=a_rRayOrigin;
	m_aRayDirection(m_brushSignal)=a_rRayDirection;

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;

	try
	{
		m_spSignalerI->signal(m_brushSignal);
	}
	catch(const Exception& e)
	{
		spCatalog->catalog<String>("error")+=
				"FE EXCEPTION: "+print(e);
	}
	catch(const std::exception& std_e)
	{
		spCatalog->catalog<String>("error")+=
				"stdlib EXCEPTION: "+String(std_e.what());
	}
#ifdef FE_BOOST_EXCEPTIONS
	catch(const boost::exception& boost_e)
	{
		spCatalog->catalog<String>("error")+=
				"boost EXCEPTION: "+
				String(boost::diagnostic_information(boost_e).c_str());
	}
#endif
	catch(...)
	{
		spCatalog->catalog<String>("error")+=
				"UNRECOGNIZED EXCEPTION";
	}

	relayErrors();
}

//* TODO move to MetaPlugin
const String MayaNode::prompt(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		return String();
	}

	const String promptString=
			spCatalog->catalogOrDefault<String>("Brush","prompt","");

	return promptString;
}

MStatus MayaNode::shouldSave(const MPlug& a_rPlug,bool& a_rResult)
{
	const String plugName(a_rPlug.name().asChar());

	if(plugName.dotMatch("*.fe_ascii"))
	{
		String buffer;
		if(m_spOperatorSurfaceI.isValid() &&
				m_spOperatorSurfaceI->saveState(buffer))
		{

			Registration& rRegistration=ms_mayaRegistry[m_typeName];
			std::map<String,Parameter>& rParameterMap=
					rRegistration.m_parameterMap;
			const Parameter& parameter=rParameterMap["fe_ascii"];
			const MObject& rObject=parameter.m_object;

			MDataBlock data=forceCache();
			MStatus status;
			MDataHandle handleIn=data.inputValue(rObject,&status);
			if(status==MS::kSuccess)
			{
#if FE_MN_IO_DEBUG
				feLog("MayaNode::shouldSave \"%s\" \"%s\" -> pack state\n",
						m_typeName.c_str(),plugName.c_str());
				feLog("\"%s\"\n",buffer.c_str());
#endif

				handleIn.set(MString(buffer.c_str()));
			}
		}
	}

	MStatus status=MPxNode::shouldSave(a_rPlug,a_rResult);
	return status;
}

void MayaNode::restoreState(void)
{
	if(!m_spOperatorSurfaceI.isValid())
	{
		return;
	}

	Registration& rRegistration=ms_mayaRegistry[m_typeName];
	std::map<String,Parameter>& rParameterMap=
			rRegistration.m_parameterMap;
	const Parameter& parameter=rParameterMap["fe_ascii"];
	const MObject& rObject=parameter.m_object;

	MDataBlock data=forceCache();
	MStatus status;
	MDataHandle handleIn=data.inputValue(rObject,&status);
	if(status==MS::kSuccess)
	{
		const String buffer(handleIn.asString().asChar());
		if(!buffer.empty())
		{
#if FE_MN_IO_DEBUG
			feLog("MayaNode::restoreState \"%s\" \"%s\" -> unpack state:\n",
					m_typeName.c_str(),m_nodeName.c_str());
			feLog("\"%s\"\n",buffer.c_str());
#endif

			handleIn.set(MString(""));

			if(!m_spOperatorSurfaceI->loadState(buffer))
			{
				feLog("MayaNode::restoreState loadState failed\n");
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
