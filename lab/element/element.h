/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_h__
#define __element_h__

#include "shape/shape.h"

#include "element/Modifier.h"

#include "element/Explosion.h"
#include "element/Particle.h"
#include "element/Proxy.h"
#include "element/Rod.h"
#include "element/Arena.h"

#endif /* __element_h__ */
