/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <datatool/datatool.pmh>


namespace fe
{
namespace ext
{

Library* CreateDatatoolLibrary(sp<Master> spMaster)
{
	assertDataTool(spMaster->typeMaster());

	String path=spMaster->catalog()->catalog<String>(
			"path:media")+"/template/datatool.rg";

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->setName("default_datatool");

	pLibrary->add<AsSignal>("PopulateI.AsSignal.fe");
	pLibrary->add<AsWindata>("PopulateI.AsWindata.fe");
	pLibrary->add<AsOperator>("PopulateI.AsOperator.fe");
	pLibrary->add<AsPair>("PopulateI.AsPair.fe");
	pLibrary->add<AsNamed>("PopulateI.AsNamed.fe");

	pLibrary->add<RecordPlayer>("HandlerI.RecordPlayer.fe");
	pLibrary->add<RecordRecorder>("RecorderI.RecordRecorder.fe");

	pLibrary->add<Recordable>("RecordFactoryI.Recordable.fe");

	sp<Scope> spScope=spMaster->catalog()->catalogComponent("Scope","SimScope");
	FEASSERT(spScope.isValid());
	RecordView::loadRecordGroup(spScope,path);

	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
