/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_QuickViewerI_h__
#define __viewer_QuickViewerI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Self-configuring Viewer

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT QuickViewerI:
	virtual public Component,
	public CastableAs<QuickViewerI>
{
	public:
						/// Adopts a DrawI
virtual	void			bind(sp<DrawI> spDrawI)								=0;

						/// Returns the current DrawI
virtual	sp<DrawI>		getDrawI(void) const								=0;

						/// Returns the internally-created WindowI
virtual	sp<WindowI>		getWindowI(void) const								=0;

						/// Returns the current CameraControllerI
virtual	sp<CameraControllerI>	getCameraControllerI(void) const			=0;

						/// Returns the center of attention
virtual	SpatialVector	interestPoint(void) const							=0;

						/// Returns the secondary point
virtual	SpatialVector	auxilliaryPoint(void) const							=0;

						/// Add an extra handler for the core heartbeat
virtual	void			insertBeatHandler(sp<HandlerI> spHandlerI)			=0;

						/// Add an extra handler for raw event signals
virtual	void			insertEventHandler(sp<HandlerI> spEventHandler)		=0;

						/// Specify a handler for render signals
virtual void			insertDrawHandler(sp<HandlerI> spHandlerI)			=0;

						/// @brief Map the viewer to the screen
virtual	void			open(void)											=0;

virtual	void			reopen(void)										=0;

						/** @brief Run the viewer for a specified number
							of frames

							The window will be opened automatically.

							For unlimited frames, specify zero. */
virtual	void			run(U32 frames)										=0;

						/// Returns frames per seconds
virtual Real			frameRate(void) const								=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_QuickViewerI_h__ */
