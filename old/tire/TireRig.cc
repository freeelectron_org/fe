/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

void RigSystem::Rig::makeWishbone(RigSystem &a_system, t_moa_real a_stiffness,
	t_moa_real a_damping, t_moa_real a_load)
{
	AsParticle asParticle;
	AsBounded asBounded;
	AsColored asColored;
	AsForcePoint asForcePoint;
	AsLinearSpringExtended asLinearSpring;

	t_moa_real strut_stiffness = 1.0e10;
	t_moa_real strut_damping = 1.0e7;
	t_moa_real spring_stiffness = a_stiffness;
	t_moa_real spring_damping = a_damping;
	t_moa_real load = a_load;

	sp<Scope> spScope = a_system.m_spScope;
	sp<RecordGroup> rg_rig(new RecordGroup());

	sp<Layout> l_particle = spScope->declare(FE_L_RIG_PARTICLE);
	asParticle.populate(l_particle);
	asBounded.populate(l_particle);
	asColored.populate(l_particle);

	sp<Layout> l_constraint = spScope->declare(FE_L_RIG_CONSTRAINT);
	asForcePoint.populate(l_constraint);
	asBounded.populate(l_constraint);
	asColored.populate(l_constraint);

	sp<Layout> l_spring = spScope->declare(FE_L_RIG_SPRING);
	asLinearSpring.populate(l_spring);

	Record r_p = spScope->createRecord(l_particle);
	rg_rig->add(r_p);
	asParticle.location(r_p) = SpatialVector(0.0, 0.0, 0.0);
	asParticle.velocity(r_p) = SpatialVector(0.0,0.0,0.0);
	asParticle.mass(r_p) = load;
	asBounded.radius(r_p) = 0.1;
	asColored.color(r_p) = Color(1.0,1.0,1.0);

	// wishbone attach A
	Record r_wishbone_attach_a = spScope->createRecord(l_constraint);
	rg_rig->add(r_wishbone_attach_a);
	asParticle.location(r_wishbone_attach_a) = SpatialVector(0.2, 0.5, 0.0);
	asBounded.radius(r_wishbone_attach_a) = 0.01;
	asColored.color(r_wishbone_attach_a) = Color(0.0,0.0,0.0);

	// wishbone attach B
	Record r_wishbone_attach_b = spScope->createRecord(l_constraint);
	rg_rig->add(r_wishbone_attach_b);
	asParticle.location(r_wishbone_attach_b) = SpatialVector(-0.2, 0.5, 0.0);
	asBounded.radius(r_wishbone_attach_b) = 0.01;
	asColored.color(r_wishbone_attach_b) = Color(0.0,0.0,0.0);

	// wishbone strut A
	Record r_wishbone_strut_a = spScope->createRecord(l_spring);
	rg_rig->add(r_wishbone_strut_a);
	asLinearSpring.left(r_wishbone_strut_a) = r_p;
	asLinearSpring.right(r_wishbone_strut_a) = r_wishbone_attach_a;
	asLinearSpring.stiffness(r_wishbone_strut_a) = strut_stiffness;
	asLinearSpring.damping(r_wishbone_strut_a) = strut_damping;
	asLinearSpring.length(r_wishbone_strut_a) = -1.0;
	asLinearSpring.flags(r_wishbone_strut_a) = 0;

	// wishbone strut B
	Record r_wishbone_strut_b = spScope->createRecord(l_spring);
	rg_rig->add(r_wishbone_strut_b);
	asLinearSpring.left(r_wishbone_strut_b) = r_p;
	asLinearSpring.right(r_wishbone_strut_b) = r_wishbone_attach_b;
	asLinearSpring.stiffness(r_wishbone_strut_b) = strut_stiffness;
	asLinearSpring.damping(r_wishbone_strut_b) = strut_damping;
	asLinearSpring.length(r_wishbone_strut_b) = -1.0;
	asLinearSpring.flags(r_wishbone_strut_b) = 0;

	// spring root
	Record r_spring_root = spScope->createRecord(l_constraint);
	rg_rig->add(r_spring_root);
	asParticle.location(r_spring_root) = SpatialVector(0.0, 0.5, 0.5);
	asBounded.radius(r_spring_root) = 0.01;
	asColored.color(r_spring_root) = Color(0.0,0.0,0.0);

	// contact root
	Record r_contact_root = spScope->createRecord(l_constraint);
	rg_rig->add(r_contact_root);
	asParticle.location(r_contact_root) = SpatialVector(0.0, 0.0, 0.0);
	asBounded.radius(r_contact_root) = 0.001;
	asColored.color(r_contact_root) = Color(1.0,0.0,0.0);

	// main spring
	Record r_main_spring = spScope->createRecord(l_spring);
	rg_rig->add(r_main_spring);
	asLinearSpring.left(r_main_spring) = r_p;
	asLinearSpring.right(r_main_spring) = r_spring_root;
	asLinearSpring.stiffness(r_main_spring) = spring_stiffness;
	asLinearSpring.damping(r_main_spring) = spring_damping;
	asLinearSpring.length(r_main_spring) = -1.0;
	asLinearSpring.flags(r_main_spring) = 0;

	// contact spring
	Record r_contact_spring = spScope->createRecord(l_spring);
	rg_rig->add(r_contact_spring);
	asLinearSpring.left(r_contact_spring) = r_p;
	asLinearSpring.right(r_contact_spring) = r_contact_root;
	asLinearSpring.stiffness(r_contact_spring) = 0.0 * 100000.0;
	asLinearSpring.damping(r_contact_spring) = 0.0e3;
	asLinearSpring.length(r_contact_spring) = -1.0;
	asLinearSpring.flags(r_contact_spring) = 0;

	m_spSemiImplicit->compile(rg_rig);
	std::vector<SemiImplicit::Particle> &particles =
		m_spSemiImplicit->particles();

	AsSolverParticle asSolverParticle;
	asSolverParticle.bind(spScope);

	unsigned int i_p;
	i_p = asSolverParticle.index(r_p);
	m_spTireForce->bind(&particles[i_p]);
	m_particle = &particles[i_p];

	AsRig asRig(spScope);
	asRig.structure(m_r_parent) = rg_rig;
}

void RigSystem::compile(const t_note_id &a_note_id)
{
	m_spScope = m_rg_dataset->scope();

	if(!m_spScope.isValid()) { return; }

	AsRig asRig(m_rg_dataset->scope());
	m_asRig.bind(m_rg_dataset->scope());

	std::vector<Record>	rigs;
	asRig.filter(rigs, m_rg_dataset);

	enforce<AsRig,AsRigLive>(m_rg_dataset);

	for(unsigned int i_rig = 0; i_rig < rigs.size(); i_rig++)
	{
		Record r_rig = rigs[i_rig];
		Rig rig;
		rig.m_spSemiImplicit = new SemiImplicit();;
		rig.m_spSemiImplicit->initialize(m_spScope);

		sp<SemiImplicit::Force> spForce;
		spForce = new LinearSpringForce(rig.m_spSemiImplicit);
		rig.m_spSemiImplicit->addForce(spForce, false);

		rig.m_spTireForce = new TireRigForce(rig.m_spSemiImplicit);
		spForce = rig.m_spTireForce;
		rig.m_spSemiImplicit->addForce(spForce, false);

		rig.m_particle = NULL;
		rig.m_r_parent = r_rig;

		// If we loaded a rig, use it, otherwise generate a wishbone
		if(asRig.structure(r_rig).isValid())
		{
			AsSolverParticle asSolverParticle;
			asSolverParticle.bind(m_spScope);

			rig.m_spSemiImplicit->compile(asRig.structure(r_rig));
			std::vector<SemiImplicit::Particle> &particles =
				rig.m_spSemiImplicit->particles();

			FEASSERT(asRig.root(r_rig).isValid());
			if(asRig.root(r_rig).isValid())
			{
				if(asSolverParticle.check(asRig.root(r_rig)))
				{
					asSolverParticle.bind(asRig.root(r_rig));
					rig.m_spTireForce->bind(&particles[asSolverParticle.index]);
					rig.m_particle = &particles[asSolverParticle.index];
				}
			}
		}

		if(!rig.m_particle)
		{
			rig.makeWishbone(*this,0.0,1.0e4,10.0); // 10kg mass
		}

		AsForcePoint asForcePoint;
		asForcePoint.bind(m_spScope);
		asForcePoint.filter(rig.m_forceParticles, asRig.structure(r_rig));

		std::vector<SemiImplicit::Particle> &ps =
			rig.m_spSemiImplicit->particles();

		rig.m_spTireForces.resize(rig.m_forceParticles.size());
		for(unsigned int i = 0; i < rig.m_forceParticles.size(); i++)
		{
			AsSolverParticle asSolverParticle;
			asSolverParticle.bind(m_spScope);
			rig.m_spTireForces[i] = new TireRigForce(rig.m_spSemiImplicit);
			spForce = rig.m_spTireForces[i];
			rig.m_spSemiImplicit->addForce(spForce, false);
			rig.m_spTireForces[i]->bind(&ps[asSolverParticle.index(
				rig.m_forceParticles[i])]);
			rig.m_spTireForces[i]->set(t_moa_v3(0.0,0.0,0.0));
		}

		rig.m_spTireForce->set(t_moa_v3(0.0,0.0,0.0));

		m_rigs.push_back(rig);
	}
}

void RigSystem::step(t_moa_real a_dt)
{
	t_solve_v3 dummy;

	AsForcePoint asForcePoint;
	asForcePoint.bind(m_spScope);

	for(unsigned i_rig = 0; i_rig < m_rigs.size(); i_rig++)
	{
		Rig &rig = m_rigs[i_rig];
		// load
		m_asRig.force_accum(rig.m_r_parent).add
			(t_moa_v3(0.0,0.0,-m_asRig.load(rig.m_r_parent)));
		t_moa_v3 F;
		m_asRig.force_accum(rig.m_r_parent).read(F);
		rig.m_spTireForce->set(F);

		for(unsigned int i = 0; i < rig.m_forceParticles.size(); i++)
		{
			rig.m_spTireForces[i]->set(
				asForcePoint.force(rig.m_forceParticles[i]));
		}

		m_asRig.force(rig.m_r_parent) = t_moa_v3(0.0,0.0,0.0);
		m_asRig.moment(rig.m_r_parent) = t_moa_v3(0.0,0.0,0.0);
		t_moa_v3 root = rig.m_spTireForce->location();
		for(unsigned int i = 0; i < rig.m_forceParticles.size(); i++)
		{
			m_asRig.force(rig.m_r_parent) +=
				asForcePoint.force(rig.m_forceParticles[i]);
			t_moa_v3 arm = asForcePoint.location(rig.m_forceParticles[i])-root;
			t_moa_v3 M;
			cross3(M, arm, asForcePoint.force(rig.m_forceParticles[i]));
			m_asRig.moment(rig.m_r_parent) += M;
		}

		rig.m_spSemiImplicit->step(a_dt, dummy);

		if(m_asRig.solve_extract(rig.m_r_parent))
		{
			rig.m_spSemiImplicit->extract(m_rg_dataset);
		}

		for(unsigned int i = 0; i < rig.m_forceParticles.size(); i++)
		{
			asForcePoint.force(rig.m_forceParticles[i]) = t_moa_v3(0.0,0.0,0.0);
		}

		m_asRig.location(rig.m_r_parent) = rig.m_spTireForce->location();
		m_asRig.pnt_velocity(rig.m_r_parent) = rig.m_spTireForce->velocity();

		// add in signal from non-particle mounts
		t_moa_v3 T(0.0,0.0,0.0);
		m_asRig.force_accum(rig.m_r_parent).read(T);
		m_asRig.force(rig.m_r_parent) += T;
		m_asRig.moment_accum(rig.m_r_parent).read(T);
		m_asRig.moment(rig.m_r_parent) += T;

		// clear accumulation
		m_asRig.force_accum(rig.m_r_parent).clear();
		m_asRig.moment_accum(rig.m_r_parent).clear();
	}
}

void applyFMToPoints(t_moa_v3 &F, t_moa_v3 &M, t_moa_v3 &P,
	fe::ext::AsForcePoint &a_asPoint,
	fe::WeakRecord &r_p0, fe::WeakRecord &r_p1, fe::WeakRecord &r_p2)
{
	t_moa_v3 A = a_asPoint.location(r_p0) - P;

	M[0] += - F[2] * A[1] + F[1] * A[2];
	M[1] +=   F[2] * A[0] - F[0] * A[2];
	M[2] += - F[1] * A[0] + F[0] * A[1];

	a_asPoint.force(r_p0) += F;

	t_moa_v3 p0 = a_asPoint.location(r_p0);
	t_moa_v3 p1 = a_asPoint.location(r_p1);
	t_moa_v3 p2 = a_asPoint.location(r_p2);

	t_moa_v3 F0(0.0,0.0,0.0);
	t_moa_v3 F1(0.0,0.0,0.0);
	t_moa_v3 F2(0.0,0.0,0.0);

	t_moa_xform frame;
	t_moa_xform relative;
	setIdentity(relative);
	bool frame_made = makeFrame(frame,
		p0,
		p1,
		p2,
		relative);

	t_moa_xform frame_inv;
	invert(frame_inv, frame);
	t_moa_v3 r0 = fe::transformVector<3, t_moa_real>(frame_inv, p0);
	t_moa_v3 r1 = fe::transformVector<3, t_moa_real>(frame_inv, p1);
	t_moa_v3 r2 = fe::transformVector<3, t_moa_real>(frame_inv, p2);
	t_moa_v3 frameM = fe::rotateVector<3, t_moa_real>(frame_inv,M);

	// M0
	t_moa_real arm = r2[2];
	F2[1] -= frameM[0] / arm;
	F0[1] += frameM[0] / arm;
	t_moa_real residue = (frameM[0] / arm) * r2[0];
	arm = r1[0];
	F1[1] += residue / arm;
	F0[1] -= residue / arm;

	// M1
	arm = r1[0];
	F1[2] -= frameM[1] / arm;
	F0[2] += frameM[1] / arm;

	// M2
	arm = r1[0];
	F1[1] += frameM[2] / arm;
	F0[1] -= frameM[2] / arm;

	F0 = fe::rotateVector<3, t_moa_real>(frame, F0);
	F1 = fe::rotateVector<3, t_moa_real>(frame, F1);
	F2 = fe::rotateVector<3, t_moa_real>(frame, F2);

	a_asPoint.force(r_p0) += F0;
	a_asPoint.force(r_p1) += F1;
	a_asPoint.force(r_p2) += F2;
}

} /* namespace ext */
} /* namespace fe */
