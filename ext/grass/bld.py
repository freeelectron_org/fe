import sys
import re
forge = sys.modules["forge"]

def prerequisites():
    return ["operator"]

def setup(module):

    srcList = [ "grass.pmh",
                "BenderOp",
                "Blade",
                "BladeOp",
                "ClumpOp",
                "ContractOp",
                "CurlOp",
                "DodgeOp",
                "FlatnessOp",
                "KinkOp",
                "ScatterOp",
                "SpineFitOp",
                "TwistBindOp",
                "TwistWrapOp",
                "WavyOp",
                "grassDL" ]

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")
    for src in srcList:
            if re.compile(r'(.*)Op$').match(src):
                with open(manifest, 'a') as outfile:
                    outfile.write('\tspManifest->catalog<String>(\n'+
                            '\t\t\t"OperatorSurfaceI.' + src + '.fe")='+
                            '"fexGrassDL";\n');

    dll = module.DLL( "fexGrassDL", srcList )

    deplibs = forge.corelibs + [
                "fexSignalLib",
                "fexGeometryDLLib",
                "fexOperatorDLLib",
                "fexDataToolDLLib",
                "fexWindowLib"  ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexOperateDLLib",
                        "fexSurfaceDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexGrassDLLib"], deplibs )

    forge.tests += [
            ("inspect.exe",     "fexGrassDL",               None,       None) ]

    module.Module( 'test' )
