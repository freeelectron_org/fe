/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_ChainSignaler_h__
#define __signal_ChainSignaler_h__

//#if FE_CODEGEN<FE_OPTIMIZE
//#ifndef FE_CHAINSIGNALER_PROFILING
//#define FE_CHAINSIGNALER_PROFILING
//#endif
//#endif

namespace fe
{
namespace ext
{

/**	@brief SignalerI that calls registered HandlerI instances in order

	@ingroup signal

	If a signal has an <int> attribute named _TTL and its value is greater
	than zero, it is decremented by 1.  If it is zero the handlers are not
	called (the signal is in effect killed).  This is useful in breaking cycles.
	*/
class FE_DL_EXPORT ChainSignaler:
		virtual public SignalerI,
		public ObjectSafe<ChainSignaler>,
		public CastableAs<ChainSignaler>
{
	public:
					ChainSignaler(void);
virtual				~ChainSignaler(void);

virtual	void		signal(Record &signal);
virtual	void		insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
						IWORD clue = -1);
virtual	void		remove(sp<HandlerI> spHandlerI, sp<Layout> spLayout);
virtual	bool		contains(sp<HandlerI> spHandlerI, sp<Layout> spLayout);
virtual	void		peek(Peeker &peeker);
#ifdef MODAL_SIGNALER
virtual	void		setMode(const String &aMode, bool aIsActive);
virtual	bool		getMode(const String &aMode);
virtual	void		insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
						const String &aMode, IWORD clue = -1);
#endif

	protected:

		class Entry
		{
			public:
								Entry(void);
	virtual						~Entry(void);
				sp<Component>	m_component;
#ifdef MODAL_SIGNALER
				unsigned int	m_mode;
#endif
		};

		typedef std::map< sp<Layout>, List< Entry* > >	t_entrymap;

virtual	void		signalEntries(Record &signal, t_entrymap *a_entryMap);

		void		signalHandler(Record &signal,
							const sp<HandlerI> &spHandler);

	private:

		t_entrymap										m_entryMap;
		Accessor<int>									m_aTTL;
#ifdef MODAL_SIGNALER
		Array<bool>										m_modeSwitch;
		std::map<String, unsigned int>					m_modes;
#endif

#ifdef FE_CHAINSIGNALER_PROFILING
		class Profile
		{
			public:
				Profile(void)
				{
					m_count = 0;
					m_ms = 0;
					m_us = 0;
				}
				FE_UWORD	m_count;
				FE_UWORD	m_ms;
				FE_UWORD	m_us;
		};
		std::map< sp<Layout>, Profile >						m_layoutProfiles;
		std::map< sp<Layout>, std::map<String, Profile> >	m_componentProfiles;

		SystemTicker* m_pTicker;
#endif
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_ChainSignaler_h__ */

