/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_JoyI_h__
#define __window_JoyI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Joystick event generator

	@ingroup window

	For Linux, you may need to be in group 'input' to access controllers.
*//***************************************************************************/
class FE_DL_EXPORT JoyI:
	virtual public HandlerI,
	public CastableAs<JoyI>
{
	public:

		typedef enum
		{
			e_none=				0x00,
			e_stick=			0x01,
			e_wheel=			0x02,
			e_pedals=			0x03,
		} ControllerType;

virtual	ControllerType	getControllerType(I32 a_joystickIndex)		=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __window_JoyI_h__ */
