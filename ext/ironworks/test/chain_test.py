#! /usr/bin/python

import pyfe.context
import math

fe = pyfe.context.Context()
fe.manage("fexOperatorDL")
fe.manage("fexIronWorksDL")
fe.manage("fexOpLabDL")

linkSurface = fe.load("torus.geo")

surfaceNormalOp = fe.create("SurfaceNormalOp")
normalSurface = surfaceNormalOp(linkSurface)

curveCreateOp = fe.create("CurveCreateOp")
curveCreateOp["segments"] = 18
curveCreateOp["length"] = 0.7
curveSurface = curveCreateOp()

lengthCorrectOp = fe.create("LengthCorrectOp")
lengthCorrectOp["Iterations"] = int(curveCreateOp["segments"])
lengthCorrectOp["Root Lock"] = True
lengthCorrectOp["Temporal"] = True
lengthCorrectOp["Response"] = 0.01

chainOp = fe.create("ChainOp")
chainOp["linkScale"] = [1.0, 0.6, 0.5]
chainOp["pitchVary"] = 5.0
chainOp["yawVary"] = float(chainOp["pitchVary"])
chainOp["spiralVary"] = 4.0 * float(chainOp["pitchVary"])
chainOp["maxCount"] = int(0.95*float(curveCreateOp["segments"])*
        float(curveCreateOp["length"]))

surfaceViewerOp = fe.create("SurfaceViewerOp")

for frame in range(0, 108000):
    fe.setFrame(frame)

    lengthCorrectOp["temporalOffset"] = [
            0.15*math.sin(frame/8)*math.sin(frame/19), -0.2, 1.0/(frame+1)]

    correctedSurface = lengthCorrectOp(curveSurface, curveSurface)

    chainSurface = chainOp(correctedSurface, normalSurface)
#   chainSurface.save("chain." + str(frame) + ".geo")

    surfaceViewerOp(chainSurface, normalSurface, correctedSurface)
