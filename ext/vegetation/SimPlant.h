/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_SimPlant_h__
#define __vegetation_SimPlant_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Move all plants

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT SimPlant: virtual public HandlerI,
	virtual public WorkI
{
	public:
							SimPlant(void)										{}

							//* as HandlerI
virtual void				handle(Record &a_forestRecord);

							//* As WorkI
virtual	WorkI::Threading	threading(void)
							{	return WorkI::Threading(NULL); }
virtual	void				setThreading(WorkI::Threading a_threading)		{}
virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange);
virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange,
									String a_stage)
							{	run(a_id,a_spSpannedRange); }

	private:
		void	update(Plant& a_rPlantRV);

		Forest					m_forestRV;
		Plant					m_plantRV;
		RecordArrayView<Plant>	m_plantRAV;
		PlantSeed				m_seedRV;

	class PlantWorker: public Thread::Functor
	{
		public:
							PlantWorker(sp< JobQueue<I32> > a_spJobQueue,
									U32 a_id,String a_stage):
								m_id(a_id),
								m_hpJobQueue(a_spJobQueue)					{}

	virtual	void			operate(void);

			void			setObject(sp<Counted> spCounted)
							{	m_hpSimPlant=sp<Component>(spCounted); }

		private:
			U32					m_id;
			hp< JobQueue<I32> >	m_hpJobQueue;
			hp<SimPlant>		m_hpSimPlant;
	};

		sp< Gang<PlantWorker,I32> >	m_spGang;
		I32							m_bundleSize;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_SimPlant_h__ */
