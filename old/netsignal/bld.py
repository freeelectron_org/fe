import os
import sys
import string
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool", "network" ]

def setup(module):
    # the library
    srclist = [ "netsignal.pmh",
                "Chunk",
                "ChunkSender",
                "ChunkReceiver",
                "Client",
                "Listener",
                "NetworkCatalog",
                "Server",
                "SignalReceiver",
                "SignalSender",
                "Socket",
                "SocketHandler",
                "netsignalLib"
                ]

    if forge.filesystem:
        srclist += [
                "MultiGroupReader",
                "MultiGroupWriter",
                ]

    lib = module.Lib("fexNetSignal",srclist)

    liblist = [ "fePlatformLib",
                "feMemoryLib",
                "feCoreLib",
                "fePluginLib",
                "fexSignalLib",
                "fexNetworkDLLib",
                "fexNetSignalLib",
                "fexDataToolDLLib",
                "feDataLib"
                ]


    # the plugin
    dlsrclist = [
        "netsignalDL"
        ]

    dll = module.DLL( "fexNetSignalDL", dlsrclist)
    forge.deps( ["fexNetSignalDLLib"], liblist )

    forge.tests += [
        ("inspect.exe",     "fexNetSignalDL",               None,       None) ]

    module.Module('test')

    module.Image("network")
    module.Image("netstack")
    module.Image("network_overview")

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        implNetCat = "*.NetworkCatalog"
    else:
        implNetCat = "'*.NetworkCatalog'"

    forge.tests += [
        ("xSignalServer.exe",   implNetCat + " udp 7892",   "",
        ("xSignalClient.exe",   implNetCat + " udp 7892",   "", None)) ]

    forge.tests += [
        ("xSignalServer.exe",   implNetCat + " tcp 7893",   "",
        ("xSignalClient.exe",   implNetCat + " tcp 7893",   "", None)) ]
