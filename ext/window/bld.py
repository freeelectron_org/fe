import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "datatool" ]

def setup(module):
    # the library
    srcList = [ "window.pmh",
                "windowAS",
                "WindowEvent",
                "LogWindowHandler",
                "ConsoleKeys",
                "windowLib"
                ]

    lib = module.Lib("fexWindow",srcList)

    deplibs =   forge.corelibs + [ ]

    forge.deps( ["fexWindowLib"], deplibs )

    # the plugin
    srcList = [ "window.pmh", "windowDL" ]

    dll = module.DLL( "fexWindowDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexWindowLib",
                "fexDataToolDLLib" ]

    forge.deps( ["fexWindowDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexWindowDL",                  None,       None) ]

    module.Image("window", silent=True)

    module.Module( 'test' )
