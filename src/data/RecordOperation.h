/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordOperation_h__
#define __data_RecordOperation_h__

namespace fe
{

typedef std::list<BaseAccessor> FilterList;

/**	@brief An operation on a record.

	@ingroup data

	This is used by RecordGroup::all()

	The common use case is to derive from RecordOperation and
	override operator()(const Record &record).
	*/
class RecordOperation
{
	public:
						RecordOperation(sp<Scope> &spScope);
virtual					~RecordOperation(void);
		/** Override this function to implement the record operation. */
virtual	void			operator()(const Record &record)					=0;
		void			operator()(sp<RecordGroup> &spRG);
		FilterList		&filters(void);
		void			addFilter(const BaseAccessor &filter);
		void			setup(BaseAccessor &accessor, const String &attribute);

	private:
						RecordOperation(void);
		FilterList		m_filters;
		sp<Scope>		m_spScope;
};

inline RecordOperation::RecordOperation(void)
{
	/* NOOP */
}

inline RecordOperation::RecordOperation(sp<Scope> &spScope)
{
	m_spScope = spScope;
}

inline RecordOperation::~RecordOperation(void)
{
	/* NOOP */
}

inline FilterList &RecordOperation::filters(void)
{
	return m_filters;
}

inline void RecordOperation::addFilter(const BaseAccessor &filter)
{
	m_filters.push_back(filter);
}

inline void RecordOperation::setup(BaseAccessor &accessor,
	const String &attribute)
{
	if(!m_spScope.isValid())
		feX("RecordOperation::setup","invalid scope");
	accessor.setup(m_spScope, attribute);
	addFilter(accessor);
}

inline void RecordOperation::operator()(sp<RecordGroup> &spRG)
{
	spRG->all(*this);
}

} /* namespace */


#endif /* __data_RecordOperation_h__ */
