import sys
import re
forge = sys.modules["forge"]

import os.path

def prerequisites():
    return [    "operate" ]

def setup(module):
    module.summary = []

    srcList = [ "DrawPlant",
                "LimberOp",
                "PlantSketch",
                "SimPlant",
                "Tree",
                "TreeOp",
                "vegetation.pmh",
                "vegetationDL" ]

    manifest = module.modPath + '/manifest.cc'
    with open(manifest, 'w') as outfile:
        outfile.write("")
    for src in srcList:
            if re.compile(r'(.*)Op$').match(src):
                with open(manifest, 'a') as outfile:
                    outfile.write('\tspManifest->catalog<String>(\n'+
                            '\t\t\t"OperatorSurfaceI.' + src + '.fe")='+
                            '"fexVegetationDL";\n');

    deplibs = forge.corelibs + [
                "fexOperateDLLib",
                "fexSurfaceDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexThreadDLLib"]

        if 'viewer' in forge.modules_confirmed:
            deplibs += [    "fexViewerDLLib" ]

    module.cppmap = {}

    if 'solve' in forge.modules_confirmed:
        module.cppmap['solve'] = ' -DFE_VEG_SOLVE'


        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            deplibs += [    "fexSolveDLLib" ]
    else:
        module.summary += [ "-solve" ]

    dll = module.DLL( "fexVegetationDL", srcList )

    forge.deps( ["fexVegetationDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexVegetationDL",              None,       None) ]

    if 'viewer' in forge.modules_confirmed and os.path.exists('ext/vegetation/test'):
        module.cppmap['solve'] = ' -DFE_VEG_VIEWER'
        module.Module( 'test' )
    else:
        module.summary += [ "-viewer" ]
