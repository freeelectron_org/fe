/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "SimpleGrid.h"
#include "platform/dlCore.cc"
#include "DrawPick.h"
#include "DrawPairs.h"
#include "DrawAtoms.h"
#include "DrawPoints.h"
#include "DrawVectorField.h"
#include "ExplicitInertial.h"
#include "misc.pmh"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
{
}

FE_DL_EXPORT fe::Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<SimpleGrid>("HandlerI.SimpleGrid.misc.fe");
	pLibrary->add<DrawPick>("HandlerI.DrawPick.misc.fe");
	pLibrary->add<DrawAtoms>("HandlerI.DrawAtoms.misc.fe");
	pLibrary->add<DrawPairs>("HandlerI.DrawPairs.misc.fe");
	pLibrary->add<DrawPoints>("HandlerI.DrawPoints.misc.fe");
	pLibrary->add<DrawVectorField>("HandlerI.DrawVectorField.misc.fe");
	pLibrary->add<ExplicitInertial>("HandlerI.ExplicitInertial.misc.fe");

	return pLibrary;
}

}
