/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <meta/meta.pmh>

using namespace fe;
using namespace fe::ext;

MetaPlugin::MetaPlugin(void):
	m_lastTime(0.0f)
{
}

void MetaPlugin::clearReferences(void)
{
	//* the derived object holds the Master, so free up things beforehand

	m_spOperatorSurfaceI=NULL;
	m_spSignalerI=NULL;
	m_spCookLayout=NULL;
	m_spBrushLayout=NULL;
	m_spDrawOutput=NULL;
	m_spDrawGuideChain=NULL;
	m_spDrawGuideCached=NULL;
	m_spDrawBrush=NULL;
	m_spDrawBrushOverlay=NULL;
	m_spSurfaceOutput=NULL;

	m_surfaceInputArray.clear();

	m_cookSignal=Record();
	m_brushSignal=Record();
}

MetaPlugin::~MetaPlugin(void)
{
}

void MetaPlugin::setupScope(sp<Scope> a_spScope)
{
	//* TODO general m_spDrawGuideCached->view() shouldn't provoke OpenGL

	if(a_spScope.isNull())
	{
		feLog("MetaPlugin::setupScope null scope\n");
		return;
	}

	sp<Registry> spRegistry=a_spScope->registry();

	m_spDrawGuideCached=spRegistry->create("DrawI.DrawCached");

	if(m_spDrawGuideCached.isValid())
	{
		m_spDrawGuideCached->setName("DrawGuideCached");
		m_spDrawGuideCached->view()->setProjection(ViewI::e_current);
	}

	sp<DrawCached> spDrawCached=m_spDrawGuideCached;
	if(spDrawCached.isValid())
	{
		spDrawCached->setDrawChain(m_spDrawGuideChain);
	}

	m_spSignalerI=spRegistry->create("SignalerI");
	if(m_spSignalerI.isValid())
	{
		m_spSignalerI->setName("MetaPlugin.SignalerI");
	}

	m_aFrame.setup(a_spScope,"Frame","real");
	m_aStartFrame.setup(a_spScope,"StartFrame","real");
	m_aEndFrame.setup(a_spScope,"EndFrame","real");
	m_aTime.setup(a_spScope,"Time","real");
	m_aSurfaceOutput.setup(a_spScope,"SurfaceOutput","component");
	m_aDrawOutput.setup(a_spScope,"DrawI","component");
	m_aDrawGuide.setup(a_spScope,"DrawGuide","component");
	m_aDrawBrush.setup(a_spScope,"DrawBrush","component");
	m_aDrawBrushOverlay.setup(a_spScope,"DrawBrushOverlay","component");
	m_aWindowEvent.setup(a_spScope,"WindowEvent","record");
	m_aCameraTransform.setup(a_spScope,"CameraTransform","transform");
	m_aRayOrigin.setup(a_spScope,"RayOrigin","spatial_vector");
	m_aRayDirection.setup(a_spScope,"RayDirection","spatial_vector");

	m_spCookLayout = a_spScope->declare("cook");
	m_spCookLayout->populate(m_aFrame);
	m_spCookLayout->populate(m_aStartFrame);
	m_spCookLayout->populate(m_aEndFrame);
	m_spCookLayout->populate(m_aTime);
	m_spCookLayout->populate(m_aSurfaceOutput);
	m_spCookLayout->populate(m_aDrawOutput);
	m_spCookLayout->populate(m_aDrawGuide);
	m_cookSignal = a_spScope->createRecord(m_spCookLayout);
	m_aDrawOutput(m_cookSignal)=m_spDrawOutput;
	m_aDrawGuide(m_cookSignal)=m_spDrawGuideCached;
}

void MetaPlugin::setupBrush(sp<Scope> a_spScope)
{
	m_spBrushLayout = a_spScope->declare("brush");
	m_spBrushLayout->populate(m_aDrawBrush);
	m_spBrushLayout->populate(m_aDrawBrushOverlay);
	m_spBrushLayout->populate(m_aWindowEvent);
	m_spBrushLayout->populate(m_aCameraTransform);
	m_spBrushLayout->populate(m_aRayOrigin);
	m_spBrushLayout->populate(m_aRayDirection);

	m_brushSignal=a_spScope->createRecord(m_spBrushLayout);

	m_aDrawBrush(m_brushSignal)=m_spDrawBrush;
	m_aDrawBrushOverlay(m_brushSignal)=m_spDrawBrushOverlay;

	sp<Registry> spRegistry=a_spScope->registry();

	sp<ViewI> spViewBrush=spRegistry->create("*.ViewCommon");
	spViewBrush->setName("DrawBrush ViewCommon");
	spViewBrush->setProjection(ViewI::e_perspective);
	m_spDrawBrush->setView(spViewBrush);

	sp<ViewI> spViewBrushOverlay=spRegistry->create("*.ViewCommon");
	spViewBrushOverlay->setName("DrawBrushOverlay ViewCommon");
	spViewBrushOverlay->setProjection(ViewI::e_ortho);
	spViewBrushOverlay->camera()->setRasterSpace(TRUE);
	m_spDrawBrushOverlay->setView(spViewBrushOverlay);
}

void MetaPlugin::insertHandler(sp<HandlerI> a_spHandlerI)
{
	if(m_spSignalerI.isValid())
	{
		m_spSignalerI->insert(a_spHandlerI,m_spCookLayout);
		if(m_spBrushLayout.isValid())
		{
			m_spSignalerI->insert(a_spHandlerI,m_spBrushLayout);
		}
	}
}

void MetaPlugin::removeHandler(sp<HandlerI> a_spHandlerI)
{
	if(m_spSignalerI.isValid())
	{
		m_spSignalerI->remove(a_spHandlerI,m_spCookLayout);
		if(m_spBrushLayout.isValid())
		{
			m_spSignalerI->remove(a_spHandlerI,m_spBrushLayout);
		}
	}
}

bool MetaPlugin::evalParamBoolean(String a_key,Real a_time)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(spCatalog.isNull())
	{
		return false;
	}
	return spCatalog->catalog<bool>(a_key);
}

I32 MetaPlugin::evalParamInteger(String a_key,Real a_time)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(spCatalog.isNull())
	{
		return 0;
	}
	return spCatalog->catalog<I32>(a_key);
}

String MetaPlugin::evalParamString(String a_key,Real a_time)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(spCatalog.isNull())
	{
		return "";
	}
	return spCatalog->catalog<String>(a_key);
}

//* TODO so many more operations
BWORD MetaPlugin::evalParamCondition(String a_conditions,Real a_time)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("MetaPlugin:evalParamCondition operator is not a Catalog\n");
		return FALSE;
	}

	String buffer=a_conditions;
	BWORD enabled=FALSE;

	String phrase;
	//* phrase || phrase
	while(!enabled && !(phrase=buffer.parse("\"","|")).empty())
	{
		enabled=TRUE;

		String condition;
		//* condition && condition
		while(enabled && !(condition=phrase.parse("\"","&")).empty())
		{
			String chopped;
			while((chopped=condition.prechop(" "))!=condition)
			{
				condition=chopped;
			}
			while((chopped=condition.chop(" "))!=condition)
			{
				condition=chopped;
			}

			//* key==desired
			String key=condition.parse("\"","=");
			String desired=condition.parse("\"","=");

			//* !key  or  !key==desired
			const String prechopped=key.prechop("!");
			const bool negated=(key!=prechopped);
			if(negated)
			{
				key=prechopped;
			}

			if(spCatalog->catalogInstance(key).is<bool>())
			{
				const bool evaluated=evalParamBoolean(key,a_time);

				if(desired.empty())
				{
					if(evaluated==negated)
					{
						enabled=FALSE;
					}
				}
				else
				{
					const bool desiredBool=atoi(desired.c_str());

					if((evaluated==desiredBool) == negated)
					{
						enabled=FALSE;
					}
				}
			}

			if(spCatalog->catalogInstance(key).is<I32>())
			{
				const I32 evaluated=evalParamInteger(key,a_time);

				if(desired.empty())
				{
					if(evaluated==negated)
					{
						enabled=FALSE;
					}
				}
				else
				{
					const I32 desiredInt=atoi(desired.c_str());

					if((evaluated==desiredInt) == negated)
					{
						enabled=FALSE;
					}
				}
			}

			if(spCatalog->catalogInstance(key).is<String>())
			{
				const String evaluated=evalParamString(key,a_time);

				if(!desired.empty())
				{
					if((evaluated==desired) == negated)
					{
						enabled=FALSE;
					}
				}
				else if(evaluated.empty() != negated)
				{
					enabled=FALSE;
				}
			}
		}
	}

	return enabled;
}
