/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

#include <datatool/datatool.h>

namespace fe
{
namespace ext
{

PlayerPipeline::PlayerPipeline(void)
{
}

PlayerPipeline::~PlayerPipeline(void)
{
}

void PlayerPipeline::initialize(void)
{
}

void PlayerPipeline::attach(
	sp<ApplicationI> a_application,
	sp<SignalerI> a_signaler,
	sp<SequencerI> a_sequencer,
	sp<Layout> l_hb,
	sp<RecordGroup> a_rg_input)
{
	sp<Scope> spSimScope(registry()->create("Scope"));
	spSimScope->setName("PlayerScope");


	sp<RecordGroup> &rg_dataset =
		registry()->master()->catalog()->catalog< sp<RecordGroup> >(
			FE_CAT_SPEC("WorldDataSet", "dataset RG"));
	if(!rg_dataset.isValid())
	{
		rg_dataset = new RecordGroup();
	}

	sp<Config> spConfig;

#if 1
	sp<Component> spMultiReader = create("HandlerI.MultiGroupReader");
	spMultiReader->adjoin(a_signaler);
	a_signaler->insert(spMultiReader, l_sim_hb);
	spConfig = spMultiReader;
	spConfig->cfg< sp<RecordGroup> >("output") = rg_dataset;
#endif

#if 0
	if(cfg<String>("dataset") != "")
	{
		sp<data::FileStreamI> spFileStream(
			registry()->create("FileStreamI.Ascii"));
		spFileStream->bind(spSimScope);
		sp<RecordGroup> rg_input =
			spFileStream->input(cfg<String>("dataset"));
		rg_dataset->add(rg_input);
	}
#endif
}

} /* namespace ext */
} /* namespace fe */

