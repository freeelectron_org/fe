/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;



class MyAccessorSet : public AccessorSet, public Initialize<MyAccessorSet>
{
	public:
		MyAccessorSet(void){}
		void initialize(void)
		{
			add(aA, "a");
			add(aB, "b");
			add(aC, "c");
		}
		Accessor<int>	aA;
		Accessor<int>	aB;
		Accessor<int>	aC;
		Accessor<int>	aD;
};

int main(int argc,char** argv)
{
	UnitTest unitTest;

	try
	{
		int limit=1000;
		if(argc>1)
		{
			limit=atoi(argv[1]);
		}
		feLog("limit = %d\n",limit);

		Peeker peek;

		// setup
		sp<Master> spMaster(new Master());
		sp<Scope> spScope(new Scope(*spMaster.raw()));

		Accessor<String>	aName(spScope,"name");
		Accessor<I32>		aAge(spScope,"age");

		// accessors
		Accessor<float>	x(spScope,"x");
		Accessor<float>	y(spScope,"y");
		Accessor<float>	z(spScope,"z");
		Accessor<int>	a(spScope,"a");
		Accessor<int>	b(spScope,"b");
		Accessor<int>	c(spScope,"c");
		Accessor<void>	v(spScope,"v");
		Accessor<double> dbl(spScope,"dbl");

		Accessor<int>	scratch(spScope,":SCRATCH");

		// dependencies
		spScope->enforce("b", "a");
		spScope->enforce("y", "b");
		spScope->share("", "b", "c");
		//spScope->within("", "x", "dbl", sizeof(float));

		// layouts
		sp<Layout>	spI = spScope->declare("I");
		sp<Layout>	spJ = spScope->declare("J");
		spI->populate(y);
		spI->populate(c);
		spJ->populate(z);
		spJ->populate(v);
		spJ->populate(dbl);
		//spI->populate(scratch);
		//spJ->populate(scratch);


		// record group
		sp<RecordGroup> spRG(new RecordGroup());
		Array<Record> records;

		MyAccessorSet myAccessorSet;
		myAccessorSet.bind(spScope);
		myAccessorSet.populate(spJ);

		for(int i = 0; i < limit; i++)
		{
			Record r;
			r = spScope->createRecord(spI);
			spRG->add(r);
			records.push_back(r);

			a(r) = 30;
			myAccessorSet.aA(r) = 10;
			myAccessorSet.aB(r) = 20;

			r = spScope->createRecord(spJ);
			myAccessorSet.aA(r) = 40;
			myAccessorSet.aB(r) = 50;
			spRG->add(r);
			spRG->add(r);
			records.push_back(r);
		}

		SystemTicker ticker("RG tests");
		ticker.log();
		int cnt = 0;
		for(int i = records.size()-1; i >= 0; i--)
		{
			bool b = spRG->find(records[i]);
			if(b)
			{
				cnt++;
			}
		}
		ticker.log("find");

		int rgcnt = 0;
		for(RecordGroup::iterator i_rg = spRG->begin();
			i_rg != spRG->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			rgcnt += spRA->length();
		}
		feLog("cnt %d %d\n", cnt, spRG->count());

		sp<RecordGroup> rg_set(new RecordGroup());
		sp<RecordGroup> rg_subset(new RecordGroup());
		Array<Record> rvec;
		for(int i = 0; i < 10*limit; i++)
		{
			Record r_i = spScope->createRecord(spI);
			Record r_j = spScope->createRecord(spJ);

			rvec.push_back(r_i);
			rvec.push_back(r_j);
		}

		ticker.log();
		for(int i = rvec.size()-1 ; i >=0 ; i--)
		{
			rg_set->add(rvec[i]);
		}
		ticker.log("add");

		for(int i = 0; i < limit; i++)
		{
			int j = (int)(((float)rvec.size())*((float)rand()/
					(float)RAND_MAX));
			rg_subset->add(rvec[j]);
		}

		rvec.clear();

		feLog("set count:    %d\n", rg_set->count());
		feLog("subset count: %d\n", rg_subset->count());

		ticker.log();
		rg_set->remove(rg_subset);
		ticker.log("removal");

		feLog("set count:    %d\n", rg_set->count());
		feLog("subset count: %d\n", rg_subset->count());

		ticker.log();
		rg_set->remove(rg_subset);
		ticker.log("removal");

		feLog("set count:    %d\n", rg_set->count());
		feLog("subset count: %d\n", rg_subset->count());

		peek(*spScope);
		feLog(peek.output().c_str());
//		spScope->shutdown();
	}
	catch(Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

