/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

Real StratumBowl::height(Real x,Real y) const
{
	return offset()+scale()*(x*x+y*y);
}

SpatialVector StratumBowl::normal(Real x,Real y) const
{
	const Real c=2.0f*scale();
	return fe::unit(SpatialVector(-c*x,-c*y,1.0f));
}

} /* namespace ext */
} /* namespace fe */