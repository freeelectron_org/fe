/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __arnold_ArnoldContext_h__
#define __arnold_ArnoldContext_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief shared FE context for Arnold plugins

	@ingroup arnold
*//***************************************************************************/
class ArnoldContext: public OperatorContext
{
	public:
				ArnoldContext(void)
				{
					m_libEnvVar="FE_ARNOLD_LIBS";
#ifdef FE_ARNOLD_LIBS
					m_libDefault=FE_STRING(FE_ARNOLD_LIBS);
#else
					m_libDefault="fexArnoldDL";
#endif

				}
virtual			~ArnoldContext(void)										{}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __arnold_ArnoldContext_h__ */
