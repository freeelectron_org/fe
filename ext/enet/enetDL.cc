#include <enet/enetComs.pmh>
#include "enet/EnetConnection.h"
#include "enet/EnetCatalog.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<EnetCatalog>("ConnectedCatalog.EnetCatalog.fe");
	pLibrary->add<EnetConnection>("ConnectionI.EnetConnection.fe");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	Mutex::confirm("fexStdThread");
}

}

