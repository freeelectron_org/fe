import sys
forge = sys.modules["forge"]

def setup(module):
    deplibs =   forge.corelibs + [
                "fexViewerLib",
                'fexSignalLib',
                "fexDataToolDLLib",
                'fexSpatialDLLib' ]

    tests = [   'xSpatial' ]

    for t in tests:
        exe = module.Exe(t)
        exe.linkmap = { "gfxlibs": forge.gfxlibs }

        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xSpatial.exe",    "10",                           None,       None) ]
