import os
import sys
import re
import shutil
forge = sys.modules["forge"]

def prerequisites():
    return ["terminal"]

def setup(module):
    module.summary = []

    katanaArch = ""
    katanaMajor = ""
    katanaMinor = ""
    katanaFix = ""

    katana_include = os.environ["FE_KATANA_INCLUDE"]
    katana_lib = os.environ["FE_KATANA_LIB"]

    for line in open(katana_include + "/FnAPI/FnAPI.h").readlines():
        if re.match("#define KATANA_VERSION_MAJOR.*", line):
            katanaMajor = line.split()[2].rstrip()
        if re.match("#define KATANA_VERSION_MINOR.*", line):
            katanaMinor = line.split()[2].rstrip()
        if re.match("#define KATANA_VERSION_RELEASE.*", line):
            katanaRelease = line.split()[2].rstrip()

    version = katanaMajor + "." + katanaMinor + "." + katanaRelease
    if version != "...":
        module.summary += [ version ]

    srcList = [ "KatanaGeo",
                "katana.pmh",
                "katanaDL" ]

    dll = module.DLL( "fexKatanaDL", srcList )

    module.cppmap = {}
    module.cppmap['woe_conditionally'] = ""

    module.includemap = {}
    module.includemap['katana'] = katana_include
    module.includemap['katana_src'] = katana_include + "/../src"

    dll.linkmap = {}
    dll.linkmap["katana"] = ""
    dll.linkmap['katana'] += " -Wl,-rpath='$ORIGIN/../../'"
    dll.linkmap['katana'] += " -shared"

    deplibs = forge.corelibs + [
                "fexSurfaceDLLib",
                "fexTerminalDLLib"  ]

    forge.deps( ["fexKatanaDLLib"], deplibs )

    katanaPath = os.path.join(forge.libPath, 'katana')
    katanaLibsPath = os.path.join(katanaPath, 'Libs')
    katanaPluginsPath = os.path.join(katanaPath, 'Plugins')
    if os.path.isdir(katanaPath) == 0:
        os.makedirs(katanaPath, 0o755)
    if os.path.isdir(katanaLibsPath) == 0:
        os.makedirs(katanaLibsPath, 0o755)
    if os.path.isdir(katanaPluginsPath) == 0:
        os.makedirs(katanaPluginsPath, 0o755)

    dest = os.path.join(katanaLibsPath, 'libfexKatanaDL.so')
    if os.path.lexists(dest) == 0:
        os.symlink("../../libfexKatanaDL.so", dest)

    from_rfp_py = os.path.join(forge.rootPath, "lab", "katana", "RegisterFreeElectron.py")
    to_rfp_py = os.path.join(katanaPluginsPath, "RegisterFreeElectron.py")
    shutil.copyfile(from_rfp_py, to_rfp_py)

def auto(module):
    katana_include = os.environ["FE_KATANA_INCLUDE"]
    katana_lib = os.environ["FE_KATANA_LIB"]

    if katana_include == '' or katana_lib == '':
        return 'unset'

    test_file = """
#include <FnAttribute/FnAttribute.h>

int main(void)
{
    return(0);
}
    \n"""

    forge.includemap['katana'] = katana_include

#   forge.linkmap['katana'] = "-Wl,-rpath='" + katana_lib + "'"
#   forge.linkmap['katana'] = '-L' + katana_lib + ' -lai'

    result = forge.cctest(test_file)

    forge.includemap.pop('katana', None)
    forge.linkmap.pop('katana', None)

    return result
