/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "flight/flight.pmh"

namespace fe
{
namespace ext
{

DroneInput::DroneInput(void):
	m_throttle(0.0),
	m_yaw(0.0),
	m_pitch(0.0),
	m_bank(0.0)
{
};

DroneInput::~DroneInput(void)
{
};

void DroneInput::initialize(void)
{
}

void DroneInput::handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout)
{
}

void DroneInput::handle(Record& a_signal)
{
//	feLog("DroneInput::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	m_event.bind(a_signal.layout()->scope());
	if(m_event.check(a_signal))
	{
		handleEvent(a_signal);
		return;
	}

	if(!m_asCatalog.bind(a_signal))
	{
		return;
	}

	sp<Catalog>& rspStepCatalog=m_asCatalog.catalog(a_signal);

	rspStepCatalog->catalog<Real>("input.throttle")=m_throttle;
	rspStepCatalog->catalog<Real>("input.yaw")=m_yaw;
	rspStepCatalog->catalog<Real>("input.pitch")=m_pitch;
	rspStepCatalog->catalog<Real>("input.bank")=m_bank;
}

void DroneInput::handleEvent(Record& record)
{
	m_event.bind(record);

	if(m_event.isPoll())
	{
		return;
	}

//	feLog("DroneInput::handleEvent %s\n",c_print(m_event));

	if(m_event.source() == WindowEvent::Source::e_sourceJoy0)
	{
		const Real unitState=m_event.state()/32768.0;

		if(m_event.item()==WindowEvent::Item::e_joyLeftStickX)
		{
			m_yaw= -unitState;
		}
		if(m_event.item()==WindowEvent::Item::e_joyLeftStickY)
		{
			m_throttle=unitState;
		}
		if(m_event.item()==WindowEvent::Item::e_joyRightStickX)
		{
			m_bank=unitState;
		}
		if(m_event.item()==WindowEvent::Item::e_joyRightStickY)
		{
			m_pitch= -unitState;
		}
	}
}

} /* namespace ext */
} /* namespace fe */
