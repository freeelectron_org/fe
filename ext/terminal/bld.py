import sys
import os
import shutil
import re
import utility
forge = sys.modules["forge"]

def prerequisites():
    return [    "meta",
                "operator" ]

def setup(module):

    srcList = [ "terminal.pmh",
                "Terminal",
                "TerminalDispatch",
                "TerminalDraw",
                "TerminalNode",
                "terminalDL" ]

    dll = module.DLL( "fexTerminalDL", srcList )

    deplibs =   forge.corelibs + [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexDrawDLLib",
                "fexMetaDLLib",
                "fexSurfaceDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexGeometryDLLib",
                        "fexThreadDLLib" ]

    forge.deps( ["fexTerminalDLLib"], deplibs )

    module.Module('test')

    utility.copy_all(module.modPath + "/bin", forge.libPath)
    utility.copy_files(module.modPath + "/test/*_test.py", forge.libPath)

    pyfePath = forge.libPath + '/pyfe'
    if os.path.lexists(pyfePath) == 0:
        os.symlink(".", pyfePath)
