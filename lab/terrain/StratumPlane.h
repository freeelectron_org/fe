/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumPlane_h__
#define __terrain_StratumPlane_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Flat Elevation

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumPlane: virtual public StratumBase
{
	public:
						StratumPlane(void):
								m_time(0.0f)								{}
virtual					~StratumPlane(void)									{}

						//* As StratumI
virtual	void			setTime(Real time)				{ m_time=time; }

virtual Real			height(Real x,Real y) const		{ return offset(); }
virtual SpatialVector	normal(Real x,Real y) const
						{	return SpatialVector(0.0f,0.0f,1.0f); }

virtual	void			sample(Real startx,Real starty,Real incx,Real incy,
								U32 vertx,U32 verty,
								SpatialVector* pVertex,SpatialVector* pNormal);

	protected:
		Real			m_time;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumPlane_h__ */
