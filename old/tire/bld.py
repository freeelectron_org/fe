import sys
forge = sys.modules["forge"]

def prerequisites():
    return [ "moa", "evaluate", "mechanics", "viewer_system" ]

def setup(module):
    srcList = [ "tire.pmh",
                "BrushTire",
                "LinearTire",
                "StickTire",
                "BlendTire",
                "SurfaceGroundSystem",
                "PointViewerSystem",
                "TireISystem",
                "TireRig",
                "SoftSystem",
                "tireDL" ]

    dll = module.DLL( "fexTireDL", srcList )

    # dll.linkmap = { "tbb": ' -ltbb' }

    deplibs = forge.corelibs + [    "feMathLib",
                                    "fexEvaluateDLLib",
                                    "fexMechanicsDLLib",
                                    "fexMoaLib",
                                    "fexViewerSystemDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexMoaDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib" ]

    forge.deps( ["fexTireDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexTireDL",                None,   None) ]

    module.Module('test')
