/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

#define FE_THREAD_DEBUG	FALSE

namespace fe
{

Thread::Thread(void):
	m_pRawThread(NULL)
{
	confirm();

	if(gs_fnThreadDefaultInit)
	{
		m_pRawThread=gs_fnThreadDefaultInit();
	}

	FEASSERT(m_pRawThread);
}

Thread::Thread(Functor* a_pFunctor):
	m_pRawThread(NULL)
{
	confirm();

	if(gs_fnThreadInit)
	{
		m_pRawThread=gs_fnThreadInit(a_pFunctor);
	}

	FEASSERT(m_pRawThread);
}

Thread::Thread(void* a_pRawThread):
	m_pRawThread(a_pRawThread)
{
	FEASSERT(m_pRawThread);
}

Thread::~Thread(void)
{
	if(m_pRawThread)
	{
		if(gs_fnThreadFinish)
		{
			gs_fnThreadFinish(m_pRawThread);
		}
		m_pRawThread=NULL;
	}
}

BWORD Thread::config(String a_property,String a_value)
{
	if(!gs_fnThreadConfig)
	{
		return FALSE;
	}

	return gs_fnThreadConfig(m_pRawThread,a_property,a_value);
}

BWORD Thread::interrupt(void)
{
	if(!gs_fnThreadInterrupt)
	{
		return FALSE;
	}

	gs_fnThreadInterrupt(m_pRawThread);
	return TRUE;
}

BWORD Thread::join(void)
{
	if(!gs_fnThreadJoin)
	{
		return FALSE;
	}

	gs_fnThreadJoin(m_pRawThread);
	return TRUE;
}

BWORD Thread::joinable(void)
{
	if(!gs_fnThreadJoinable)
	{
		return FALSE;
	}

	return gs_fnThreadJoinable(m_pRawThread);
}

//* static
BWORD Thread::interruptionPoint(void)
{
	if(!gs_fnThreadInterruption)
	{
		return FALSE;
	}

	gs_fnThreadInterruption();
	return TRUE;
}

//* static
I32 Thread::hardwareConcurrency(void)
{
	confirm();

	if(!gs_fnThreadConcurrency)
	{
		return 1;
	}

	return gs_fnThreadConcurrency();
}


Thread::Functor::Functor(void):
	m_pOriginal(this)
{
#if FE_THREAD_DEBUG
	fe_printf("Thread::Functor::Functor %p %p\n",this,m_pOriginal);
#endif
}

Thread::Functor::~Functor(void)
{
#if FE_THREAD_DEBUG
	fe_printf("Thread::Functor::~Functor %p %p\n",this,m_pOriginal);
#endif

	FEASSERT(m_pOriginal);

	m_pOriginal=NULL;
}

void Thread::Functor::operator()(void)
{
#if FE_THREAD_DEBUG
	fe_printf("Thread::Functor::operator() %p %p\n",this,m_pOriginal);
#endif

	FEASSERT(m_pOriginal);

	//* TODO check if these tests are meaningful for boost

#if FALSE
	if(abs(m_pOriginal-this)>0xFFFF)
	{
		fe_printf("Thread::Functor::operator() %p %p CORRUPTED\n",
				this,m_pOriginal);
	}
	else
#endif
	{
#if FE_THREAD_DEBUG
		if(m_pOriginal!=this)
#else
		if(m_pOriginal!=this && System::getVerbose()=="all")
#endif
		{
#if FE_CODEGEN<=FE_DEBUG
//			fe_printf("Thread::Functor::operator() %p %p CHANGED\n",
//					this,m_pOriginal);
#endif
		}

		m_pOriginal->operate();
	}

#if FE_THREAD_DEBUG
	fe_printf("Thread::Functor::operator() %p %p DONE\n",this,m_pOriginal);
#endif
}

void Thread::Functor::operate(void)
{
	fe_printf("Thread::Functor::operate doing nothing as base class\n");
}

Thread::Group::Group(void):
	m_pRawThreadGroup(NULL)
{
#if FE_THREAD_DEBUG
	fe_printf("Thread::Group::Group %p raw %p\n",
			this,m_pRawThreadGroup);
#endif

	confirm();

#if FE_THREAD_DEBUG
	fe_printf("Thread::Group::Group %p gs_fnThreadGroupInit %p %p\n",
			this,&gs_fnThreadGroupInit,gs_fnThreadGroupInit);
#endif

	if(gs_fnThreadGroupInit)
	{
		m_pRawThreadGroup=gs_fnThreadGroupInit();
	}

#if FE_THREAD_DEBUG
	fe_printf("Thread::Group::Group %p raw %p DONE\n",
			this,m_pRawThreadGroup);
#endif
}

Thread::Group::~Group(void)
{
#if FE_THREAD_DEBUG
	fe_printf("Thread::Group::~Group %p raw %p\n",
			this,m_pRawThreadGroup);
#endif

	if(gs_fnThreadGroupFinish)
	{
		gs_fnThreadGroupFinish(m_pRawThreadGroup);
	}

#if FE_THREAD_DEBUG
	fe_printf("Thread::Group::~Group %p raw %p DONE\n",
			this,m_pRawThreadGroup);
#endif
}

Thread* Thread::Group::createThread(Functor* a_pFunctor)
{
	if(gs_fnThreadGroupCreate)
	{
#if FE_THREAD_DEBUG
		fe_printf("Thread::Group::createThread %p raw %p functor %p\n",
				this,m_pRawThreadGroup,a_pFunctor);
#endif

		void* pRawThread=gs_fnThreadGroupCreate(m_pRawThreadGroup,a_pFunctor);

#if FE_THREAD_DEBUG
		fe_printf("Thread::Group::createThread %p raw %p functor %p DONE\n",
				this,m_pRawThreadGroup,a_pFunctor);
#endif

		return new Thread(pRawThread);
	}
	return NULL;
}

BWORD Thread::Group::joinAll(void)
{
	if(gs_fnThreadGroupJoinAll)
	{
#if FE_THREAD_DEBUG
		fe_printf("Thread::Group::joinAll %p raw %p\n",
				this,m_pRawThreadGroup);
#endif

		gs_fnThreadGroupJoinAll(m_pRawThreadGroup);

#if FE_THREAD_DEBUG
		fe_printf("Thread::Group::joinAll %p raw %p DONE\n",
				this,m_pRawThreadGroup);
#endif

		return TRUE;
	}

	return FALSE;
}

#ifdef _OPENMP
extern "C"
{
bool omp_thread_init(void);
}
#endif

//* static
BWORD Thread::confirm(void)
{
#if FE_THREAD_DEBUG
	fe_printf("Thread::confirm\n");
#endif

	if(gs_fnThreadInit)
	{
		return TRUE;
	}
	if(gs_threadChecked)
	{
		return FALSE;
	}
	gs_threadChecked=TRUE;

	const String verbose=System::getVerbose();

	Mutex::confirm();

	//* try using same lib as mutexes
	const String mutexSupport=Mutex::support();
	String nameOrder=mutexSupport;
	if(!nameOrder.empty())
	{
		nameOrder+=":";
	}

	nameOrder+=FE_STRING(FE_THREAD_ORDER);

	gs_pThreadLoader=new DL_Loader();
	FEASSERT(gs_pThreadLoader);

	BWORD success=FALSE;
	const BWORD adaptname=TRUE;

	String name;
	while(!nameOrder.empty())
	{
		while(!success && !(name=nameOrder.parse("\"",":")).empty())
		{
//			fe_printf("open %s\n",name.c_str());

			if(name=="omp")
			{
#ifdef _OPENMP
				if(verbose=="all")
				{
					fe_printf("%susing OpenMP (for threads)\n",
							PrefixLog::prefix().c_str());
				}
				gs_threadSupport=name;

				return omp_thread_init();
#endif
			}

			success=gs_pThreadLoader->openDL(name,adaptname);
		}

		if(!success)
		{
			if(verbose!="none")
			{
				fe_printf("%sno thread support\n",
						PrefixLog::prefix().c_str());
			}
			delete gs_pThreadLoader;
			gs_pThreadLoader=NULL;
			return FALSE;
		}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

		bool (*thread_init)(void);
		thread_init=(bool (*)(void))gs_pThreadLoader->findDLFunction("thread_init");

#pragma GCC diagnostic pop

		if(!thread_init)
		{
			if(verbose!="none")
			{
				fe_fprintf(stderr,"%sthread_init not found for %s\n",
						PrefixLog::prefix().c_str(),name.c_str());
			}
		}
		else if(thread_init())
		{
			if(verbose=="all")
			{
				fe_printf("%s%s %s (for threading)\n",
						PrefixLog::prefix().c_str(),
						(name==mutexSupport)? "reusing": "using",
						name.c_str());
			}

			gs_threadSupport=name;

			return TRUE;
		}
		else
		{
#if FE_THREAD_DEBUG
			fe_fprintf(stderr,"%sthread_init failed for %s\n",
					PrefixLog::prefix().c_str(),name.c_str());
#endif
		}

		success=FALSE;
	}

	delete gs_pThreadLoader;
	gs_pThreadLoader=NULL;
	return FALSE;
}

//* static
BWORD Thread::clearFunctionPointers(void)
{
	Thread::replaceDefaultInitFunction(NULL);
	Thread::replaceInitFunction(NULL);
	Thread::replaceConfigFunction(NULL);
	Thread::replaceInterruptFunction(NULL);
	Thread::replaceJoinFunction(NULL);
	Thread::replaceJoinableFunction(NULL);
	Thread::replaceFinishFunction(NULL);
	Thread::replaceInterruptionFunction(NULL);
	Thread::replaceConcurrencyFunction(NULL);

	Thread::replaceGroupInitFunction(NULL);
	Thread::replaceGroupCreateFunction(NULL);
	Thread::replaceGroupJoinAllFunction(NULL);
	Thread::replaceGroupFinishFunction(NULL);

	return TRUE;
}

//* static
BWORD Thread::dismiss(void)
{
	if(System::getVerbose()=="all")
	{
		fe_printf("%sdismissing thread support\n",
				PrefixLog::prefix().c_str());
	}

	clearFunctionPointers();

	if(gs_pThreadLoader)
	{
		gs_pThreadLoader->closeDL();
	}
	gs_pThreadLoader=NULL;

	gs_threadChecked=FALSE;

	return TRUE;
}

} /* namespace */

