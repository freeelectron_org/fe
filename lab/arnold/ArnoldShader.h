/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __arnold_ArnoldShader_h__
#define __arnold_ArnoldShader_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Arnold shader

	@ingroup arnold
*//***************************************************************************/
class ArnoldShader
{
	public:

								ArnoldShader(void);
virtual							~ArnoldShader(void);

		void					setShader(sp<ShaderI> a_spShaderI)
								{	m_spShaderI=a_spShaderI; }
		sp<ShaderI>				shader(void)
								{	return m_spShaderI; }

	private:
		sp<ShaderI>				m_spShaderI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __arnold_ArnoldShader_h__ */
