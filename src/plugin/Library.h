/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_Library_h__
#define __plugin_Library_h__

namespace fe
{

/**************************************************************************//**
	@brief Interface into a dynamic library to access its factories

	@ingroup plugin

	Each dynamic library should contain a single code block exposing
	component implementations it can create.  For example,

	@code
	extern "C"
	{

	FE_DL_EXPORT fe::Library *CreateLibrary(fe::sp<fe::Master>)
	{
		fe::Library *pLibrary = new fe::Library();
		pLibrary->add<MyComponent>("my.component");
		pLibrary->addSingleton<MySingleton>("my.singleton");
		return pLibrary;
	}

	}
	@endcode

	This code block will create the neccessary factories.
	It is not optional since omitting it makes the library useless
	(you can set up automatic dynamic linking in your build).

	Another code block can be optionally used to automatically load
	any number of other libraries prior to the given library.  For example,
	@code
	extern "C"
	{

	FE_DL_EXPORT void ListDependencies(fe::List<fe::String*>& list)
	{
		list.append(new fe::String("otherDL"));
		list.append(new fe::String("anotherDL"));
	}

	}
	@endcode

	If this block is omitted, no dependency preloading is performed.

	By convention, these two blocks are provided together in a file named
	like moduleDL.cc, one for each module.
*//***************************************************************************/
class FE_DL_EXPORT Library : public Counted, public ObjectSafe<Library>
{
	/**********************************************************************//**
		@brief Type-nonspecific base class for factories
	*//***********************************************************************/
	class BaseFactory: public Counted
	{
		public:
							BaseFactory(const String& name):
									m_name(name)
							{	m_className.sPrintf("Factory<%s>",name.c_str());
								suppressReport(); }
	virtual					~BaseFactory(void)								{}

							/// @brief Return the factory's type's name
	const	String			&name(void) const				{ return m_name; }

							/// @brief Return the factory's templated name
	const	String			verboseName(void) const		{ return m_className; }

							/** @brief Instantiate a component

								The implementation type is the business of the
								derived class.

								@internal */
	virtual	sp<Component>	create(Library *pLibrary,
								I32 a_factoryIndex) const					=0;

		private:
			String			m_name;
			String			m_className;
	};

	/**********************************************************************//**
		@brief Type-specific factory template
	*//***********************************************************************/
	template <typename T>
	class Factory: public BaseFactory
	{
		public:
							Factory(const String& name): BaseFactory(name)	{}
	virtual					~Factory(void)									{}

							//* VC7 can't seem to handle defining this outside
							//* of the class declaration.
							/** @brief Instantiate a component

								@internal */
	virtual	sp<Component>	create(Library* pLibrary,
								I32 a_factoryIndex) const
							{
//								feLog("fe::Library::Factory<T>::create()"
//										" \"%s\"\n",this->name().c_str());
								sp<Component> spComponent(Library::create<T>(
										this->name(),pLibrary,a_factoryIndex));
//								feLog("  \"%s\"=%p\n",this->name().c_str(),
//										(U32)spComponent.raw());
#if FALSE
								if(!spComponent.isValid())
								{
									feX("fe::Library::Factory<T>::create",
											"%s is not a valid fe::Component",
											this->name().c_str());
								}
#endif
								return spComponent;
							}
	};

	public:
							Library(void);
virtual						~Library(void);

							/** @brief Create a named factory for the given
								type and add it to the library's collection */
							template <typename T>
		void				add(const String &name)
							{	addFactory(new Factory<T>(name),FALSE); }

							/** @brief Create a named factory for the given
								type, but always return the same instance,
								created on the first request for the Component.

								Note that this is just "single instance",
								without an inherent global access
								(often inferred from the term "Singleton").

								Limiting a Component to only a single instance
								should only [rarely] be used to reflect a
								strictly singular notion of an abstracted
								system. */
							template <typename T>
		void				addSingleton(const String &name)
							{	addFactory(new Factory<T>(name),TRUE); }

							/// @brief Return the library name
const	String&				name(void) const			{	return m_name; }

							/** @brief Append the names of the available
								factories to the given array */
		void				populateManifest(Array<String> &names) const;

							/** @brief Return number of references adjusted
								for singletons

								Once this drops to 1 plus the number of
								factories, there are no outstanding
								references outside of the Library and the
								singletons it has instantiated. */
		U32					adjustedReferences(void) const;

							/** @brief Release all instantiated singletons

								@internal

								This command is intended to be part of
								an internal shutdown procedure.
								If any of the singletons was still otherwise
								referenced and an attempt is made to create the
								singleton again, a new independent instance
								of the singleton will be created. */
		void				releaseSingletons(void);

		I32					strayCount(void)	{ return m_strayCount; }
		I32					adjustStrayCount(I32 inc)
							{	return m_strayCount+=inc; }

							/** @brief Create component using the known index

								@internal */
		sp<Component>		create(U32 factoryIndex);

							///	@internal
		String				factoryName(U32 factoryIndex);

							///	@internal
		hp<Registry>		registry(void) const;
							///	@internal
		void				setRegistry(sp<Registry> spRegistry);

							///	@internal
		sp<Library>			getChainLibrary(void) const
							{	return m_spChainLibrary; }
							/** @brief Enforce FIFO by referencing prev lib
								on stack

								@internal */
		void				setChainLibrary(sp<Library>& rspLibrary)
							{	m_spChainLibrary=rspLibrary; }

							/// for use in static lib scenario
		void				setName(const char *a_name)
							{	m_name = a_name; }

							///	@internal
		DL_Loader*			getLoader(void) const		{ return m_pLoader; }
							///	@internal
		void				setLoader(DL_Loader* pLoader)
							{
								m_pLoader=pLoader;
								if(m_pLoader)
								{
									m_name=m_pLoader->name().basename()
											.chop(".so").chop(".lib")
											.prechop("lib");
								}
							}

							/** @brief Instantiate and initialize a component
								without using a factory

								This is useful for unit tests that don't want
								to utilize a proper library.
								It is not intended for general use.

								You have to explicitly provide the class
								type as the template argument. */
							template<typename T>
static	sp<Component>		create(String name,Library* pLibrary=NULL,
									I32 a_factoryIndex= -1)
							{
								try
								{
									T* pT=new T();
									sp<Component> spComponent(pT);
									initComponent(spComponent,pT,sizeof(T),
											name,pLibrary,a_factoryIndex);
									return spComponent;
								}
								catch(fe::Exception &e)
								{
									e.log();
									if(e.getResult()==e_cannotCreate)
									{
#if FE_CODEGEN<=FE_DEBUG
										feLog("Library::create"
												" cannot create \"%s\"\n",
												name.c_str());
#endif
									}
									else
									{
										throw;
									}
								}
								catch(...)
								{
									feLog("Library::create"
											" uncaught exception\n");
									throw;
								}

								return sp<Component>();
							}

	private:

static	void				initComponent(sp<Component>& rspComponent,
									void* ptr,U32 size,String name,
									Library* pLibrary,I32 a_factoryIndex);

	class FactoryData
	{
		public:
			sp<BaseFactory>			m_spFactory;
			U32						m_singleton;
			sp<Component>			m_spSingleton;
	};

		Array<FactoryData>			m_factories;

		sp<Library>					m_spChainLibrary;
		DL_Loader*					m_pLoader;
		I32							m_strayCount;

		//* don't hold a reference, Registry manages the Library
		Registry*					m_pRegistry;
		String						m_name;

		void				addFactory(BaseFactory *pFactory,BWORD singleton);
};


} /* namespace */

#endif /* __plugin_Library_h__ */
