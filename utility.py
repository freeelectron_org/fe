# TEMP: for minimal python2 behavior (enough to complain and quit).
# NOTE: __future__ imports must occur before any other expression.
from __future__ import print_function

import os
import sys
import re
import glob
import shutil
import threading
import getpass
import collections
import configparser
import traceback
import fnmatch
import platform
import importlib.util

SUCCESS = 0
FAIL = 1

# -----------------------------------------------------------------------------
# Python Version checking and enforcement.

PYTHON_VERSION_MIN = collections.OrderedDict([
    ('major', 3),
    ('minor', 8),
    ('micro', 18),
])

def PythonVersionSafeguard():

    verkeys = PYTHON_VERSION_MIN.keys()
    actualver = sys.version_info
    desiredver = '.'.join([str(v) for v in PYTHON_VERSION_MIN.values()])
    for actual, desired in [
        (getattr(actualver, k), PYTHON_VERSION_MIN[k]) for k in verkeys
    ]:
        if actual > desired:
            break
        elif actual < desired:
            raise EnvironmentError(
                "Insufficient python version detected: {}. " \
                    "Must be {} or newer.".format(sys.version, desiredver)
            )

# Conduct python version guard as early as possible.
try:
    PythonVersionSafeguard()
except Exception as e:
    print("[ERROR]", e, file=sys.stderr)
    sys.exit(FAIL)

# -----------------------------------------------------------------------------
# All of this section is related to legacy logging/print capabilities.

coloramaColor = 0
wconioColor = 0

if sys.platform == "win32":
    try:
        import colorama
        colorama.init()
        coloramaColor = 1
    except ImportError:
        print("\ninstall the package colorama to get color output on Win32")

#   try:
#       import WConio
#       wconioColor = 1
#   except ImportError:
#       print("\ninstall the package WConio to get color output on Win32")

# color support
if sys.platform == "win32" and wconioColor == 1:
    RED     = WConio.RED
    GREEN   = WConio.GREEN
    YELLOW  = WConio.YELLOW
    BLUE    = WConio.BLUE
    MAGENTA = WConio.MAGENTA
    CYAN    = WConio.CYAN
    WHITE   = WConio.WHITE
else:
    RED     = 31
    GREEN   = 32
    YELLOW  = 33
    BLUE    = 34
    MAGENTA = 35
    CYAN    = 36
    WHITE   = 37
    NORMAL  = 0
    BOLD    = 1

def ansi(c):
    return "[%dm" % c

def color_on(bold, hue):
    if wconioColor and sys.platform == "win32":
        if wconioColor == 1:
            WConio.textcolor(hue)
            if bold == -1:
                # NOTE appears to be the same as normvideo()
                WConio.lowvideo()
            elif bold == 1:
                WConio.highvideo()
            else:
                WConio.normvideo()
    elif coloramaColor or sys.platform != "win32":
        if bold == -1:
            bold = 2
        sys.stdout.write( r"[%dm[%dm" % (bold,hue) )

def dim_on():
    if wconioColor and sys.platform == "win32":
        if wconioColor == 1:
            WConio.lowvideo()
    elif coloramaColor or sys.platform != "win32":
        sys.stdout.write( r"[%dm" % (2) )

def color_off():
    if wconioColor and sys.platform == "win32":
        if wconioColor == 1:
            WConio.normvideo()
    elif coloramaColor or sys.platform != "win32":
        sys.stdout.write( "[0m" )
        sys.stdout.flush()

print_lock = threading.Lock()
def cprint(color, bold, string, eol='\n'):
    print_lock.acquire()
    color_on(bold,color)
    if eol:
        string = string + eol
    sys.stdout.write(string)
    color_off()
    print_lock.release()

class clogger():

    LEVEL_MAX = sys.maxsize

    VALUE_MAP = {
        'critical': 0,
        'exception': 1,
        'error': 2,
        'warning': 3,
        'stdout': 4,
        'info': 5,
        'context': 6,
        'debug': 7,
        None: 0, # Used as a default, for any spurious keywords.
    }

    PRETTY_MAP = {
        0: 7, # corresponds roughly to 'ugly' (debug?) in forge
        1: 5, # corresponds to 'info' in forge
        2: 4, # has no direct mapping in forge; is between ugly and "<info"
        3: 3, # used inconsistently in forge (as >2, i.e. ">info")
        4: 2, # not used in forge at all, except implicitly (>2).
        5: 1, # not used in forge at all, except implicitly (>2).
        6: 0, # not used in forge at all, except implicitly (>2).
        None: 0,
    }

    # -------------------------------------------------------------------------
    # Most of these class-internal helper things can go away, with clever use
    # of dynamic locals.

    LCRT = VALUE_MAP['critical']
    LEXC = VALUE_MAP['exception']
    LERR = VALUE_MAP['error']
    LWRN = VALUE_MAP['warning']
    LSTD = VALUE_MAP['stdout']
    LINF = VALUE_MAP['info']
    LCTX = VALUE_MAP['context']
    LDBG = VALUE_MAP['debug']
    LDFT = VALUE_MAP[None]

    COLOR_MAP = {
        LCRT: RED,
        LEXC: RED,
        LERR: RED,
        LWRN: YELLOW,
        LSTD: WHITE,
        LINF: CYAN,
        LCTX: BLUE,
        LDBG: WHITE,
        None: WHITE,
    }

    CCRT = COLOR_MAP[LCRT]
    CEXC = COLOR_MAP[LEXC]
    CERR = COLOR_MAP[LERR]
    CWRN = COLOR_MAP[LWRN]
    CSTD = COLOR_MAP[LSTD]
    CINF = COLOR_MAP[LINF]
    CCTX = COLOR_MAP[LCTX]
    CDBG = COLOR_MAP[LDBG]
    CDFT = COLOR_MAP[None]

    # most recently created (do we expect only one?)
    instance = None
    # -------------------------------------------------------------------------

    def __init__(self):
        self.level = self.LDBG
        clogger.instance = self

    def __get_level_pretty(self):
        # Honestly, this is probably not useful. The conversion space isn't
        # meaningful given how pretty is used. We're getting rid of pretty
        # eventually so this isn't worth anything more clever. It's just here
        # for symmetry.
        return round(self.LDBG-self.LDBG*self.level/self.LDBG)

    def __set_level_pretty(self, pretty):
        self.level = self.PRETTY_MAP.get(pretty, self.PRETTY_MAP[None])
        return pretty

    pretty = property(__get_level_pretty, __set_level_pretty)

    # A decorator for use in another module (forge), to provide automated
    # sensitivity to runtime pretty value.
    # The 'lf' argument is expected to be the host module's locals() function
    # object. Execution of that is deferred to the body of the internal
    # wrapper, so that it happens with every invocation, ensuring the value is
    # accurate at time-of-call.
    def pretty_inject(lf):
        def outer(func):
            def wrapper(*args, **kwargs):
                tokens = lf()
                if 'pretty' in tokens:
                    kwargs['pretty'] = tokens['pretty']
                return func(*args, **kwargs)
            return wrapper
        return outer

    # A decorator that converts any named log function into a generic, with the
    # associated level value getting involved.
    def log_level(func):
        def wrapper(self, *args, **kwargs):
            lvl = self.VALUE_MAP[func.__name__]
            kwargs['color'] = kwargs.get(
                'color', self.COLOR_MAP.get(lvl, self.CDFT))
            self.log(lvl, *args, **kwargs)
            return
        return wrapper

    # A function for replacement of cprint() calls. It can be used directly,
    # and the pretty argument provided explicitly. Or it can be decorated in
    # the host module with the pretty_inject decorator, to allow for pretty to
    # be omitted as an argument but still participate in filtering, thus making
    # it an exact 1:1 drop-in replacement for cprint() calls.
    def plog(self, lvl, color, bold, string, *, eol='\n', pretty=LEVEL_MAX):
        '''
        A shim helper to accommodate existing cprint() calls in general.
        The lvl provided should be intended for comparison directly with a
        pretty value.
        If pretty is not provided, output occurs.
        '''

        # Notice how the comparison is inverted, since for the pretty numerical
        # range, greater means LESS verbose (compared to the quasi-standard for
        # logging).
        if lvl >= pretty:
            cprint(color, bold, string, eol=eol)

    def log(self, lvl, string, *, color=CDFT, bold=0, eol='\n'):
        if lvl <= self.level:
            cprint(color, bold, string, eol=eol)

    @log_level
    def critical(self, *args, **kwargs):
        pass

    @log_level
    def error(self, *args, **kwargs):
        pass

    @log_level
    def warning(self, *args, **kwargs):
        pass

    @log_level
    def info(self, *args, **kwargs):
        pass

    @log_level
    def context(self, *args, **kwargs):
        pass

    @log_level
    def debug(self, *args, **kwargs):
        pass

    def exception(self, exc, limit=2, *, level=LEXC, **kwargs):
        '''
        exc is an exception instance. Must be a subclass of BaseException.
        kwargs must conform to the expectations of traceback.format_exception.

        limit is the same arg as in the traceback functions, determining the
        maximum number of trace entries in the stack that can be included in
        the output.
        '''

        # Ensuring that deprecated python versions (3.12>ver>=3.5) still work.
        # Once we're requiring >=3.12, we can simplify this call format to
        # eliminate the then-redundant arguments.
        out = traceback.format_exception(exc, exc, exc.__traceback__, **kwargs)
        # out is a list of newline-terminated strings (which may internally
        # contain additional newline chars).

        color = self.COLOR_MAP.get(level, self.CDFT)
        self.log(level, ''.join(out), color=color)

        return

# End of legacy logging/print stuff.
# -----------------------------------------------------------------------------

def copy_files(fromPattern,toDir):
    filenames = glob.glob(fromPattern)
    for filename in filenames:
        shutil.copy(filename,toDir)

# -------------------------------------------------------------------------
def known_compiler(brand):
    return brand == "g++" or brand == "clang" or brand == "VS" or brand == "icc"

# -------------------------------------------------------------------------
def file_has_include(filename, header):

    header = header.replace("\\", "/")

    for line in open(filename):
        if "#include" in line and header in line:
            return True

    return False

# -------------------------------------------------------------------------
def determine_target_architecture():
    arch = "undefined"
    os_name = platform.system().lower()
    if os_name == 'windows':
        arch = os.getenv('VSCMD_ARG_TGT_ARCH')
        if arch == None:
            arch = "unset"
    return arch

# -------------------------------------------------------------------------
# Target api value map.
HOST_API_MAP = {
    '':                 'x86',
    'i686':             'x86',
    'i386':             'x86',
    'amd64':            'x86',
    'power macintosh':  'ppc',
}

def determine_api():
    hw = platform.machine().lower()
    os_name = platform.system().lower()

    # convert hardware identifiers to standardized values.
    api = HOST_API_MAP.get(hw, hw.lower())

    if os_name == 'windows':
#       (release, version, csd, ptype) = platform.win32_ver()

#       env_path = os.getenv('Path')
#       first = env_path.find("BIN\\amd64")
#       if first >= 0:
#           os_name = 'win64'
#       else:
#           os_name = 'win32'

        env_target = determine_target_architecture()
        if env_target == "x64":
            os_name = 'win64'
        else:
            if env_target != "x86":
                if env_target == None:
                    cprint(RED,1,'VSCMD_ARG_TGT_ARCH not set')
                else:
                    cprint(RED,1,'VSCMD_ARG_TGT_ARCH not recognized')
                print('presuming win32 (x86)')

            os_name = 'win32'

    if os_name == 'darwin':
        os_name = 'osx'

    api = api + '_' + os_name

#   cprint(CYAN,0,"API = " + api)

    return api

# -------------------------------------------------------------------------
# Compact API provides an api string with less cruft. It only shows things that
# aren't the default on your hardware.

def compact_api():
    compact_api = ""

    if platform.system().lower() == 'windows':

        env_target = os.getenv('VSCMD_ARG_TGT_ARCH')
        if env_target != "x64":
            compact_api += "win32"

    return compact_api

# -------------------------------------------------------------------------
def import_file_as_module(name, filepath, lazy=False, register=True,
        tolerant=True):
    '''
    name: the module's string name, if it were to be registered into
        sys.modules (i.e. `import <name>`). The name is used in some
        logging output, but only matters beyond this function call if
        register=True.
    filepath: location of the source python file to import. Must have a
        suffix of .py or .pyc.
    lazy: whether to use a delayed execution handler (LazyLoader) for the
        module object.
        If False (the default), execution of the module occurs in this
        function prior to return, and any exception that might occur is
        logged but is not re-raised. The return value would be None in the
        case of an error during execution.
        If True, this function will return with the module being available
        but not yet actually having endured import processing by the
        interpreter. Execution would occur on the first reference into the
        module's namespace (accessing an attribute).
        Note that a lazy loading module can hide even invalid source file
        location values - that is, the file doesn't exist but that is
        not exposed as an error until an eventual attempt to execute the
        import of the module. If when calling this function the caller
        desires to know at that moment whether the source is valid and
        capable of successful import, then lazy=False should be used. This
        would also mean the full import execution occurs within the call,
        regardless of the scope or nature of its execution impact.
        Modules are encouraged to not do anything during their import that
        is sensitive to time or external sequences for this general reason
        (pythonic). Any such sensitivities should be deferred to some
        notion of a module setup action.
    register: whether to add the resulting module to sys.modules, using the
        provided name parameter (i.e. sys.modules[<name>]).
    tolerant: whether to treat errors as fail conditions.
        If True (the default), any errors encountered will be logged, but
        no exceptions will be raised.
        If False, any errors countered will cause an exception to occur and
        escape out of the function call.
    returns: the module object. Value is None if the module's loader
        execution failed. If lazy=False, None would be indicative of an
        inability to successfully run the import-time code in the module
        source file.
    '''

    # ---------------------------------------------------------------------
    # Internal book-keeping. This tracks how many times a given file is
    # imported. The count is included in the first log line below. The
    # stats themselves could also be accessed outside of this function, by
    # referencing the function attribute (.stats), which is a dictionary
    # whose keys are absolute filepaths.

    if not hasattr(import_file_as_module, 'stats'):
        import_file_as_module.stats = {}

    fpabs = os.path.abspath(filepath)
    import_file_as_module.stats[fpabs] = \
        import_file_as_module.stats.get(fpabs, 0) + 1
    icnt = import_file_as_module.stats[fpabs]
    # ---------------------------------------------------------------------

    clog = clogger.instance
    if not clog:
        clog = clogger()

    clog.debug(f"importing ({icnt}) {filepath} as {name}")

    errmsg = "ERROR: failure loading {fp} as a module ({name})."

    # ---------------------------------------------------------------------
    # Step 1: create the import spec object.
    #
    # This can fail, but likely only if the file name portion of filepath
    # indicates an incompatible filetype via its suffix. The acceptable
    # suffix values are .py and .pyc. Note that a spec object will be
    # created even if file does not yet exist, but the filename is still
    # checked against the suffix criteria.

    try:
        spec = importlib.util.spec_from_file_location(name, filepath)
        if not spec:
            raise ValueError("spec_from_file_location() failed")
    except Exception as e:
        clog.error(errmsg.format(fp=filepath, name=name))
        clog.error("Unable to create import spec (bad filename?)")
        clog.exception(e)
        if not tolerant:
            raise e
        return None

    if lazy:
        spec.loader = importlib.util.LazyLoader(spec.loader)

    # ---------------------------------------------------------------------
    # Step 2: create the import module object.

    try:
        mod = importlib.util.module_from_spec(spec)
        if not mod:
            raise ValueError("module_from_spec() failed")
    except Exception as e:
        clog.error(errmsg.format(fp=filepath, name=name))
        clog.error("Unable to create import module from spec.")
        clog.exception(e)
        if not tolerant:
            raise e
        return None

    # ---------------------------------------------------------------------
    # Step 3: Have the loader execute the module.
    #
    # Note that if we swapped in a LazyLoader, it still does an execution,
    # but not the FULL import processing of the code in the file.

    try:
        spec.loader.exec_module(mod)
    except Exception as e:
        clog.error(errmsg.format(fp=filepath, name=name))
        clog.error("Unable to execute import module's loader.")
        clog.exception(e)
        if not tolerant:
            raise e
        return None

    # ---------------------------------------------------------------------
    # If all 3 steps succeeded, then we optionally officially record the
    # module with its provided name, and return it.

    if register:
        sys.modules[name] = mod

    return mod

# -------------------------------------------------------------------------
RE_MODULE_FILE = re.compile(r'(.+)\.py')

def import_from(name, modPath):

    # This entire function feels like a convenience that potentially
    # creates more liability than it's worth, and should probably be
    # eliminated. There are major ambiguities and assumptions about module
    # name. It would seem better to change the calling contexts to better
    # understand their own circumstances, and to call
    # import_file_as_module() directly.

    # Handle name with or without .py suffix.

    fn = name

    m = RE_MODULE_FILE.match(name)
    if m:
        name = m.group(1)
    else:
        fn = f'{name}.py'

    # Load as a module. Note that we're skipping the module registration as
    # that alleviates needing to ensure a unique name for it.
    mod = import_file_as_module(
        name, os.path.join(modPath, fn), register=False)

    return mod

# -----------------------------------------------------------------------------
# TEMP: this is an interim shim helper until we can properly support env
# ingestion, as part of a layered cfg system.

def cfg_as_bool(val, name=None, default=None):
    '''!
    This will convert the provided value to a logical boolean based on a
    string:bool mapping consistent with cli/env "standards".
    '0/false/no/off' = False
    '1/true/yes/on' = True

    You can also just provide an actual boolean, indiscriminately, and it'll
    pass through here with logical success.

    The value is compared case-insensitively.
    The actual mapping is deferred to configparser. See its `BOOLEAN_STATES`
    documentation for further details.

    @param val Any 'raw', as-is, configuration value. Often string or
    string-like, as would be obtained directly from os.environ, but could be
    anything from anywhere that is intended to be used as an endorsed
    boolean-proxy value, subjected to a known mapping to True or False.

    @param name (optional) The associated name of the configuration option. If
    provided, will be referenced in any logging and aid in identification.

    @param default (optional) A substitute value to use if the provided value
    cannot be converted (unambiguously) into a boolean. If no (!=None) default
    is provided, then anything incompatible about val will result in an
    exception. If substitution to a default would occur, but the provided
    default value itself is not compatibly a boolean, then an unhandled
    exception is thrown. In other words, this function handles failures in
    converting val, if a default is provided, but it makes no attempt to handle
    anything wrong with a default.
    '''

    ALIASES = configparser.ConfigParser.BOOLEAN_STATES

    # We'll supplement ConfigParser.BOOLEAN_STATES with some extra mappings, to
    # allow lazy success for some `val` values, and allow calling contexts to
    # just call this function in a more indiscriminate fashion (not having to
    # triage `val`, in order to call a function whose purpose is to triage
    # `val`).

    extra_aliases = {
        True: True,
        False: False,
    }

    exc = None
    keyval = val

    if hasattr(keyval, 'lower'):
        keyval = keyval.lower()
    try:
        bval = ALIASES[keyval] if keyval in ALIASES else extra_aliases[keyval]
    except KeyError as e:
        msg = f"'{val}'. Not a recognized boolean alias"
        exc = e
    finally:
        if exc:
            if name:
                msg = f"(name='{name}') " + msg

            msg = f"invalid configuration value: {msg}"

            if default is None:
                cprint(RED, 1, f"[ERROR] {msg}")
                raise exc
            else:
                # Using isinstance(), which should never be done frivolously,
                # as it is very non-pythonic in most circumstances. Here,
                # however, we're being extremely specific about the nature of
                # `default` that is allowable: it has to actually be a boolean.
                # Since nearly every type in python can be implicitly converted
                # to a bool (sometimes unintuitively), we opt to be very
                # stringent with default in order to not have gotten this far
                # only to subvert the original purpose of the function.
                if not isinstance(default, type(True)):
                    msg += f", and the provided default ({default}) is not " \
                        "unambiguously a boolean."
                    cprint(RED, 1, f"[ERROR] {msg}")
                    raise exc

                bval = bool(default)

                msg += f", substituting default of '{default}'"
                cprint(YELLOW, 0, f"[WARNING] {msg}")

    return bval

def env(name, default=None, require=False, asBool=False):
    '''!
    Extract an environment variable out of os.environ.

    @param name (required) the key to use to find the variable in os.environ.

    The remaining parameters affect additional ways that the extraction
    behavior can occur.

    @param default (optional) A substitute value to use if the variable name
    is not currently in os.environ.

    @param require (optional) Whether to treat the absence of the variable
    name in os.environ as a fail condition. If False (default behavior), then
    the function will return None if no `default` value is provided, or
    `default` itself if it is. If True, then if the variable name is not in
    os.environ, an exception is raised.

    @asBool (option) Whether to convert any extracted value into a boolean,
    using an established mapping (see `cfg_as_bool()` function, which
    accomplishes this).

    In all circumstances, a return value is provided if tolerable conditions
    (according to parameterization) exist, or an exception is raised. Thus, the
    function can be used in assignment statements or within other logical
    expressions.
    '''

    if name in os.environ:
        bval = os.environ[name]
    else:
        bval = None

        msg = f"env option not found: '{name}'"

        if require:
            cprint(RED, 1, f"[ERROR] {msg}")
            raise ValueError(msg)

        # Otherwise, it's ok that it's missing; we'll consider substitution.
        if default is not None:
            bval = default
            msg += f", substituting default of '{default}'"

        # Might end up choosing to distinguish between INFO and WARNING for
        # missing vars, based on params.
        cprint(YELLOW, 0, f"[WARNING] {msg}")

    if asBool:
        bval = cfg_as_bool(bval, name=name, default=default)

    return bval

# -----------------------------------------------------------------------------

# return list of "subPath/match"
def find_all(rootPath,subPath,pattern):
    result = []
    sourceFiles = os.listdir(os.path.join(rootPath,subPath))
    for sourceFile in sourceFiles:
        if re.compile(pattern).match(sourceFile):
            result += [ os.path.join(subPath,sourceFile) ]
    return result

def copy_all(fromDir,toDir):
    filenames = os.listdir(fromDir)
    for filename in filenames:
        pathSource = os.path.join(fromDir, filename)
        pathDest = os.path.join(toDir, filename)
        if os.path.isdir(pathSource):
            if not os.path.exists(pathDest):
                os.mkdir(pathDest)
            copy_all(pathSource,pathDest)
        else:
            shutil.copyfile(pathSource, pathDest)
            shutil.copymode(pathSource, pathDest)

def findModsetFiles(rootPath, basename, modset_root_list):
    dot_file = re.compile(r"^\.[^.].*$")
    local_parent_path = []
    for modset_root in modset_root_list:
        modset_root_path = os.path.join(rootPath, modset_root)
        if not os.path.exists(modset_root_path):
            continue
        if modset_root != ".":
            modset_local_path = os.path.join(modset_root_path, basename)
            if os.path.exists(modset_local_path):
                local_parent_path.append(modset_root_path)
        dir_list = sorted(os.listdir(modset_root_path))
        for entry in dir_list:
            if dot_file.match(entry):
                continue
            modset_subdir_path = os.path.join(modset_root_path, entry)
            modset_subdir_local = os.path.join(modset_subdir_path, basename)
            if os.path.exists(modset_subdir_local):
                local_parent_path.append(modset_subdir_path)
    return local_parent_path

def readLocalEnvFiles(rootPath='', auto_env=None):

    if auto_env is None:
        auto_env = {}

    toolenv = findTools(auto_env)

    env_files = ["default.env", "local.env"]

    # update toolenv
    readEnvFiles(env_files, toolenv, os.environ)

    default_env = os.path.join(rootPath, "default.env")
    fe_env = os.path.join(rootPath, "..", "fe.env")
    local_env = os.path.join(rootPath, "local.env")
    local_env_dist = os.path.join(rootPath, "local.env.dist")

    if not os.path.exists(local_env_dist):
        cprint(YELLOW,1,"missing %s" % local_env_dist)
        sys.exit(1)
    elif not os.path.exists(local_env):
        cprint(CYAN,1,'copying local.env.dist to non-existent local.env')
        shutil.copy(local_env_dist, local_env)
    if not os.path.exists(local_env):
        cprint(RED,1,"missing %s" % local_env)
        sys.exit(1)

    env_files = [default_env]

    if os.path.exists(fe_env):
        env_files += [fe_env]
        cprint(CYAN,0,'using ../fe.env')
    else:
        cprint(CYAN,0,'not using ../fe.env (not found)')

    env_files += [local_env]

    # get a reference to a freshly updated toolenv
    env = readEnvFiles(env_files, toolenv, os.environ)

    if "FE_MODSET_ROOTS" in os.environ:
        modset_roots = os.environ["FE_MODSET_ROOTS"]
    elif "FE_MODSET_ROOTS" in env:
        modset_roots = env["FE_MODSET_ROOTS"]
    else:
        modset_roots = "."

    modset_list = []
    modset_root_list = modset_roots.split(':')

    env_files = [default_env]
    if os.path.exists(fe_env):
        env_files += [fe_env]

    for env_name in ["default.env", "local.env"]:
        if env_name == "local.env":
            env_files += [local_env]
        local_parent_path = findModsetFiles(rootPath, env_name, modset_root_list)
        for local_parent in local_parent_path:
            extra_env = os.path.join(local_parent, env_name)
            env_files += [extra_env]

    # NOTE readEnvFiles() overlays each env file on top of previous ones,
    # excluding keys in os.environ

    # get a reference to a freshly updated toolenv
    env = readEnvFiles(env_files, toolenv, os.environ)

    return (env, modset_roots)

def applyLocalEnvFiles(rootPath='', auto_env=None):
    '''
    A convenience function that takes the results of readLocalEnvFiles() and
    injects them into os.environ.  Any key collision with os.environ results in
    a warning, but the value is updated regardless.
    '''

    if auto_env is None:
        auto_env = {}

    (env, modset_roots) = readLocalEnvFiles(rootPath, auto_env)

    for key in env:
        if key in os.environ:
            print("WARNING: key collision with os.environ '{}'".format(key))
        os.environ[key] = env[key]

    return (env, modset_roots)

def findTools(env=None):
    '''
    Returns env updated with any key:value pairs matching discovered tool
    locations.  If env is not provided, a new dict is used and returned.  Note
    that some tools might have an implicit default location that is provided if
    no explicit discovery occurs.

    Currently the search includes the following tools (and their associated
    variable names):
    - None
    '''

    if env is None:
        env = {}

    # The function stub remains as it is called in an appropriate spot in the
    # cfg establishment sequence where if we do have any findable tools, this
    # is where that discovery should still occur.
    return env

def interpretVarReplacement(text:str) -> (str, str, str, int):
    '''
    Returns (key, pattern, fill, limit) for a bash-style parameter expansion.
    The limit is 0 for 'key//pattern/fill' (replace all) and 1 for
    'key/pattern/fill' (replace first).
    https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
    '''
    tokens = text.split("/")
    count = len(tokens)
    if count<2:
        return text, None, None, 1
    if tokens[1]=="":
        # key/ or key//...
        if count<3:
            # key/
            return text, None, None, 1
        if count<4:
            # key//pattern
            return tokens[0], tokens[2], None, 0
        # key//pattern/fill
        return tokens[0], tokens[2], tokens[3], 0

    # key/pattern or key/pattern/fill
    return tokens[0], tokens[1], (tokens[2] if count>2 else ""), 1

#*  potential non-standard conditional (:: here is otherwise undefined syntax)
#*  ${VAR::==value:result_true:result_false}
#*  where == could also be !=, >, >=, <, <=
#*  omitting :result_false results in "" on False
#*
#*  FE_VCPKG_TRIPLET_ACTIVE = "x64-windows-static${FE_MS_RT_T::==MD:-md}"

#*  potential non-standard map
#*  VAR = ${OTHER:["AB":"ab","CD":"cd"]}

def resolveEnvVar(variable:str, env=None, authoritative_env=None):
    '''
    Returns the value of an environment variable in env, expanding with
    substitutions as necessary, potentially recursively.
    Any key that does not exists in authoritative_env is updated in env.
    Substitutions use authoritative_value as an original basis,
    and thereafter the env itself.
    '''
    value = env[variable]

    # replace ${OTHER_VAR}
    while True:
        subs = re.findall(r'\${([^}]*)}', value)
        if not len(subs):
            break
        for sub in subs:
            key, pattern, fill, limit = interpretVarReplacement(sub)
            if pattern:
                resolveEnvVar(key, env, authoritative_env)

            if key in authoritative_env:
                replace = authoritative_env[key]
            elif key in env:
                replace = env[key]
            else:
                # unknown variable replace with nothing
                replace = ""

            if pattern:
                re_pattern = fnmatch.translate(pattern)
                replace = re.sub(re_pattern, fill, replace, count=limit)

            value = value.replace("${" + sub + "}", replace)

    # Seems like this filtering already occurred in readEnvFile().
    if variable not in authoritative_env:
        env[variable] = value

    return value

def readEnvFiles(env_files, env=None, authoritative_env=None):
    '''
    Returns the updated env (or a new dict if env is not specified) that
    contains key:value pairs from all files in the env_files list. Any key that
    exists in authoritative_env is omitted. Also, substitutions occur along the
    way, using authoritative_value as an original basis, and thereafter the env
    itself.
    '''

    for filename in env_files:
        env = readEnvFile(filename, env, authoritative_env)

    for variable in env.keys():
        value = resolveEnvVar(variable, env, authoritative_env)

    tmp_keys = [key for key in env.keys() if key.startswith("_TMP_")]
    for key in tmp_keys:
        env.pop(key)

    return env

env_serial = 0
def readEnvFile(filename, env=None, authoritative_env=None):
    '''
    Returns a copy of env (or a new dict) that has accumulated the key:value
    pairs in the specified file.  Any key that exists in authoritative_env is
    omitted.  Also, authoritative_env is used as the source of values for any
    substitution expression detected in the value in the file.
    '''
    global env_serial

    # Ensure we don't modify or return the param dict.
    env = env.copy() if env else {}

    # TODO is this ok to allow spaces in value without quotes?

    if not os.path.exists(filename):
        print("WARNING: readEnvFile() could not find '{}'".format(filename))
        return env

    with open(filename) as envFile:
        while True:
            line = envFile.readline()
            if line == "":
                break
            multiline = (line[-2:-1] == '\\')
            entry = line.split('#')[0].strip().rstrip("\\").strip()

            while(multiline):
                nextLine = envFile.readline()
                if nextLine == "":
                    break
                multiline = (nextLine[-2:-1] == '\\')
                nextEntry = nextLine.split('#')[0].strip().rstrip("\\").strip()
                if nextEntry != "":
                    entry = entry + nextEntry

            tokens = entry.split('=')
            if len(tokens) != 2:
                continue

            variable = tokens[0].strip()
            value = tokens[1].strip()

            # potentially remove quotes
            value = re.sub(r'"([^"]*)"',r'\1',value)

            subs = re.findall(r'\${([^}]*)}', value)

            # fill in self-references as they are read
            for sub in subs:
                key, pattern, fill, limit = interpretVarReplacement(sub)

                if key != variable:
                    continue

                if pattern:
                    temp = "_TMP_%06d" % env_serial
                    env_serial += 1
                    env[temp] = env[key] if key in env else ""

                    if limit:
                        replace = "${%s/%s/%s}" % (temp, pattern, fill)
                    else:
                        replace = "${%s//%s/%s}" % (temp, pattern, fill)
                else:
                    if key in authoritative_env:
                        replace = authoritative_env[key]
                    elif key in env:
                        replace = env[key]
                    else:
                        continue
                value = value.replace("${" + sub + "}", replace)

            if variable not in authoritative_env:
                env[variable] = value

    return env

def env_list(name):
    if not name in os.environ:
        return []
    result = os.environ[name].split(':')
    result = list(filter(None, result))     # remove empty entries
    return result

# NOTE ast.literal_eval() is only single object (can't do "['a']+['b']")
def safer_eval(expression):

    # far from bullet proof
    # https://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html
    return eval(expression, {'__builtins__':{}})

# -----------------------------------------------------------------------------
# Convenience tuple for user credentials.
# Any field set to False will be ignored by the credential supply logic,
# meaning that you can ask for only the field(s) you care about.
Credential = collections.namedtuple('Credential',
    ['user', 'password'],
    defaults=[None, None],
)

def getUserCredentials(creds=None):

    if creds is None:
        creds = Credential()

    # Ignore the pathological case (in which the caller wants nothing):
    if all(v is False for v in creds):
        return creds

    cprint(YELLOW,0,"Enter server credentials")

    user, password = creds.user, creds.password

    if user != False:
        # Default user can be obtained implicitly from the system.
        default_user = getpass.getuser()

        user = input("Username [%s]: " % default_user)
        if user == '':
            user = default_user

    if password != False:
        password = getpass.getpass()

    # Supply new credential values, preserving anything else in the tuple.
    creds = creds._replace(user=user, password=password)

    return creds

class Atomic():
    def __init__(self, value=0):
        self.value = int(value)
        self.lock = threading.Lock()

    def increment(self, delta=1):
        with self.lock:
#           print("Atomic %d -> %d" % (self.value, self.value+delta))
            self.value += int(delta)
            return self.value

    def decrement(self, delta=1):
        return self.increment(-delta)

    def exchange(self, replacement):
        with self.lock:
            previous = self.value
            self.value = replacement
        return previous

    def current(self):
        with self.lock:
            return self.value

def replace_symlink_on_ms():
    # https://stackoverflow.com/questions/6260149/os-symlink-support-in-windows
    if os.name == "nt":
        def symlink_ms(source, link_name):
            import ctypes
            csl = ctypes.windll.kernel32.CreateSymbolicLinkW
            csl.argtypes = (ctypes.c_wchar_p, ctypes.c_wchar_p, ctypes.c_uint32)
            csl.restype = ctypes.c_ubyte
            flags = 2 + (1 if os.path.isdir(source) else 0)
            try:
                if csl(link_name.replace('/', '\\'), source.replace('/', '\\'), flags) == 0:
                    raise ctypes.WinError()
            except:
                print("CreateSymbolicLinkW FAILED")
                pass
        os.symlink = symlink_ms
