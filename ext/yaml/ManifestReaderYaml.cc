/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <yaml/yaml.pmh>

#define FE_MRY_VERBOSE		FALSE
#define FE_MRY_RAW_PRIORITY	FALSE

namespace fe
{
namespace ext
{

/*** examples

# each entry's value can be a dictionary, value, or omitted
# nameless value can be an string lib name or integer priority
# default lib name is same as base filename, without .yaml
# default priority is 0
components:
  - "HandlerI.LuaBind.fe": fexLuaDL
  - "LuaI.LuaHandler.fe":
      priority: 1
  - "LuaI.LuaIterate.fe"
      lib: fexLuaDL
      priority: 0
  - "SurfaceAccessibleI.SurfaceAccessibleLua.fe.lua"

# A raw priority shorthand was considered, but is not currently permitted.
components:
  - "LuaI.LuaHandler.fe": 1		# unsupported

*/

BWORD ManifestReaderYaml::load(String a_filename)
{
#if FE_MRY_VERBOSE
	feLog("ManifestReaderYaml::load \"%s\"\n",a_filename.c_str());
#endif

	const String libName=a_filename.basename().chop(".yaml");

	YAML::Node yamlRoot;

	try
	{
		std::ifstream infile(a_filename.c_str());
		if(!infile)
		{
			feLog("ManifestReaderYaml::load could not open \"%s\"\n",
					a_filename.c_str());
			return FALSE;
		}

		std::stringstream buffer;
		buffer << infile.rdbuf();

		const String text(buffer.str().c_str());

		yamlRoot=YAML::Load(text.c_str());
	}
	catch(Exception &e)
	{
		feLog("ManifestReaderYaml::load"
				" caught exception loading \"%s\"\n",
				a_filename.c_str());
		e.log();
		return FALSE;
	}
	catch(...)
	{
		feLog("ManifestReaderYaml::load"
				" uncaught exception loading \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	if(!yamlRoot.IsMap())
	{
		feLog("ManifestReaderYaml::load no root map\n");
		return FALSE;
	}

	//* pass: components=0 types=1
	for(I32 pass=0;pass<2;pass++)
	{
		sp<Catalog> spManifest=
				pass? registry()->typeManifest(): registry()->manifest();
		const String nodeName(pass? "types": "components");

		const YAML::Node listNode=yamlRoot[nodeName.c_str()];
		if(listNode)
		{
			if(!listNode.IsSequence())
			{
				feLog("ManifestReaderYaml::load '%s' is not a sequence\n",
						nodeName.c_str());
				continue;
			}

			const I32 entryCount=listNode.size();
			for(I32 entryIndex=0;entryIndex<entryCount;
					entryIndex++)
			{
				//* type node or component node, depending on pass
				const YAML::Node entryNode=listNode[entryIndex];

				if(entryNode.IsSequence())
				{
#if FE_MRY_VERBOSE
					feLog("  %d/%d sequence\n");
#endif
					continue;
				}

				if(entryNode.IsMap())
				{
					YAML::const_iterator it=entryNode.begin();
					if(it==entryNode.end())
					{
#if FE_MRY_VERBOSE
						feLog("   NO ZERO\n");
#endif
						continue;
					}

					const YAML::Node keyNode=it->first;
					if(!keyNode.IsDefined() || keyNode.IsNull())
					{
#if FE_MRY_VERBOSE
						feLog("    NOT VALID\n");
#endif
						continue;
					}

					const String entryName(
							keyNode.as<std::string>().c_str());
#if FE_MRY_VERBOSE
					feLog("  %d/%d map \"%s\"\n",
							entryIndex,entryCount,
							entryName.c_str());
#endif
					const YAML::Node valueNode=it->second;
					if(!valueNode.IsMap())
					{
						const String value(
								valueNode.as<std::string>().c_str());
#if FE_MRY_VERBOSE
						feLog("    value \"%s\"\n",value.c_str());
#endif

#if FE_MRY_RAW_PRIORITY
						const I32 priority=value.integer();

#if FE_MRY_VERBOSE
						feLog("    priority %d\n",priority);
#endif

						if(priority || value=="0")
						{
							spManifest->catalog<I32>(
									entryName,"priority")=priority;
						}
						else
#endif
						{
							spManifest->catalog<String>(
									entryName)=value;
						}
						continue;
					}

					String mapLibName(libName);
					const YAML::Node libNode=valueNode["lib"];
					if(libNode)
					{
						mapLibName=libNode.as<std::string>().c_str();
#if FE_MRY_VERBOSE
						feLog("    lib \"%s\"\n",mapLibName.c_str());
#endif
					}

					spManifest->catalog<String>(entryName)=
							mapLibName;

					const YAML::Node priorityNode=valueNode["priority"];
					if(priorityNode)
					{
						const I32 priority=priorityNode.as<int>();
						spManifest->catalog<I32>(
								entryName,"priority")=priority;
#if FE_MRY_VERBOSE
						feLog("    priority %d\n",priority);
#endif
					}

					continue;
				}

				const String entryName(
						entryNode.as<std::string>().c_str());
#if FE_MRY_VERBOSE
				feLog("  %d/%d raw \"%s\"\n",entryIndex,entryCount,
						entryName.c_str());
#endif
				spManifest->catalog<String>(entryName)=
						libName;
			}
		}
	}

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
