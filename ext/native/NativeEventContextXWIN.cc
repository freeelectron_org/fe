/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

#define	FE_NEC_LOCK_DEBUG	FALSE

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_X_GFX

I32 gettid(void)
{
#ifdef __i386__
	return (long int)syscall(224);
#else
	return (long int)syscall(186);
#endif
}

int (*originalXErrorHandler)(Display*,XErrorEvent*);
int (*originalXIOErrorHandler)(Display*);

int xErrorHandler(Display *pDisplay,XErrorEvent *error_event)
{
	const U32 messagelen=256;
	char buffer[messagelen],function[messagelen];
	char request_number[16];

	sprintf(request_number,"%d",error_event->request_code);
	sprintf(buffer,"UNKNOWN");
	XGetErrorDatabaseText(pDisplay,"XRequest",
								request_number,buffer,function,messagelen);

	feLogError("non-fatal X error as follows, pDisplay %p"
						" op %d:%d \"%s\" serial %u error %d\n",
			(void*)pDisplay,error_event->request_code,error_event->minor_code,
			function,(I32)error_event->serial,error_event->error_code);

	XGetErrorText(pDisplay,error_event->error_code,buffer,messagelen);
	feLogError("  %s\n",buffer);

	return FALSE;
}

int xIOErrorHandler(Display *pDisplay)
{
	feLogError("fatal IO Error on pDisplay %p\n",(void*)pDisplay);
	FEASSERT(0);

	originalXIOErrorHandler(pDisplay);

	feLogError("original X IO handler should not have returned\n");

	// original should have terminated before this
	feX(e_impossible,"fe::xIOErrorHandler","failed to terminate");
	return FALSE;
}

void NativeEventContext::nativeStartup(void)
{
	m_pFontStruct=NULL;
	m_threadLocks=0;
	m_threadID= -1;

	Status status=XInitThreads();
	if(!status)
	{
		feLog("XInitThreads failed\n");
	}

	//* open display
	if((m_pDisplayHandle=XOpenDisplay(NULL)) == NULL)
	{
		feX("NativeEventContext::nativeStartup",
				" cannot connect to X server");
	}

	m_atom[0]=XInternAtom(m_pDisplayHandle,"WM_DELETE_WINDOW",TRUE);
	m_atom[1]=XInternAtom(m_pDisplayHandle,"_WM_QUIT_APP",TRUE);

	originalXIOErrorHandler=XSetIOErrorHandler(xIOErrorHandler);
	originalXErrorHandler=XSetErrorHandler(xErrorHandler);

	const Real dpi=fe::compute_dpi();
	m_multiplication=fe::determine_multiplication(dpi);

	if(m_multiplication>Real(1))
	{
		feLog("NativeEventContext::nativeStartup"
			" adjusting for %.6G DPI display\n",dpi);
	}
}

void NativeEventContext::nativeShutdown(void)
{
	if(m_pFontStruct)
	{
		XFreeFont(m_pDisplayHandle,m_pFontStruct);
		m_pFontStruct=NULL;
	}
	if(m_pDisplayHandle)
	{
		XCloseDisplay(m_pDisplayHandle);
		m_pDisplayHandle=NULL;
	}
}

void NativeEventContext::notifyCreate(FE_WINDOW_HANDLE &windowHandle)
{
	if( !XSetWMProtocols(m_pDisplayHandle,windowHandle.m_window,m_atom,2) )
	{
		feLog("NativeEventContext: could not XSetWMProtocols()");
	}

//~	if(!m_fontBase)
//~	{
//~		m_fontBase=loadFont(windowHandle);
//~	}

	//* each context must do this
//~	useLoadedFont(windowHandle);

//	feLog("NativeEventContext::notifyCreate() fontbase=%d\n",m_fontBase);
}

void NativeEventContext::setPointerMotion(BWORD active)
{
	m_pointerMotion=active;

	NativeWindow* pNativeWindow;
	List<WindowI*>::Iterator iterator(m_windows);
	iterator.toHead();
	while((pNativeWindow=fe_cast<NativeWindow>(*iterator++))
			!= NULL)
	{
		pNativeWindow->setPointerMotion(active);
	}
}

void NativeEventContext::threadLock(void)
{
	const I32 tid=gettid();
#if FE_NEC_LOCK_DEBUG
	feLog("NativeEventContext::threadLock   %d %d? locking\n",
			tid,m_threadLocks);
#endif
	XLockDisplay(m_pDisplayHandle);
	FEASSERT(m_threadLocks>=0);
	FEASSERT(m_threadLocks==0 || m_threadID==tid);
	m_threadLocks++;
	m_threadID=tid;
#if FE_NEC_LOCK_DEBUG
	feLog("NativeEventContext::threadLock   %d %d  locked\n",
			tid,m_threadLocks);
#endif
}
void NativeEventContext::threadUnlock(void)
{
	const I32 tid=gettid();
#if FE_NEC_LOCK_DEBUG
	feLog("NativeEventContext::threadUnlock %d %d  unlocking\n",
			tid,m_threadLocks);
#endif
	FEASSERT(m_threadLocks>0 && m_threadID==tid);
	m_threadLocks--;
	XUnlockDisplay(m_pDisplayHandle);
#if FE_NEC_LOCK_DEBUG
	feLog("NativeEventContext::threadUnlock %d %d? unlocked\n",
			tid,m_threadLocks);
#endif
}

//* \todo re-entering Loop will reset event state (mousebuttons)
void NativeEventContext::loop(BWORD dropout)
{
	//* store window of last event as first guess of which window has next event
	FE_STATIC_BAD NativeWindow *pNativeWindow=NULL;

	Display *pDisplay=m_pDisplayHandle;

	m_endloop=false;
	BWORD done=false;
	while(!done && !m_endloop)
	{
		WindowEvent::MouseButtons mb=m_event.mouseButtons();
		m_event.reset();
		m_event.setMouseButtons(mb);

		threadLock();
		if(XPending(pDisplay))
		{
			XEvent report;
			BWORD press=FALSE;

			XNextEvent(pDisplay,&report);

//			feLog("window %p\n",report.xany.window);
			if(!pNativeWindow)
				pNativeWindow=fe_cast<NativeWindow>(m_windows.head());
			if(pNativeWindow &&
					pNativeWindow->windowHandle()->m_window!=report.xany.window)
			{
				//* find WindowHandle that matches the XEvent's window
				ListCore::Context context;
				m_windows.toHead(context);
				while((pNativeWindow=fe_cast<NativeWindow>(
						m_windows.postIncrement(context))) != NULL)
				{
//					feLog("\tvs %p (%p)\n",
//							pNativeWindow->WindowHandle()->m_window,
//							pNativeWindow->WindowHandle());
					if(pNativeWindow->windowHandle()->m_window==
							report.xany.window)
						break;
				}
			}

			if(!pNativeWindow)
			{
				threadUnlock();
				continue;
			}

			FE_WINDOW_HANDLE* pWindowHandle=pNativeWindow->windowHandle();
			Window window=pWindowHandle->m_window;

			switch (report.type)
			{
				case Expose:
				{
					Window rootwin=0;
					int x,y;
					unsigned int sx,sy,border,depth;

					if(XGetGeometry(pDisplay,window,
							&rootwin,&x,&y,&sx,&sy,
							&border,&depth) == False )
						feLog("NativeEventContext could not get window"
								" geometry from XGetGeometry()\n");

#if FE_WINDOW_EVENT_DEBUG
					feLog("X Expose size=%d,%d\n",sx,sy);
#endif

					m_event.setSIS(WindowEvent::e_sourceSystem,
							WindowEvent::e_itemExpose,(WindowEvent::State)sx);
					m_event.setState2((WindowEvent::State)sy);
				}
					break;

				case EnterNotify:
				{
					m_event.setSIS(WindowEvent::e_sourceSystem,
							WindowEvent::e_itemEnter,WindowEvent::e_stateNull);
				}
					break;

				case LeaveNotify:
				{
					m_event.setSIS(WindowEvent::e_sourceSystem,
							WindowEvent::e_itemExit,WindowEvent::e_stateNull);
				}
					break;

				case ConfigureNotify:
				{
					Window rootwin=0;
					int x,y;
					unsigned int sx=0,sy=0,border,depth;
					if(XGetGeometry(pDisplay,window,
							&rootwin,&x,&y,&sx,&sy,&border,&depth)==False)
					{
						feLog("Could not get window geometry from"
															" XGetGeometry()");
					}

					if(pNativeWindow)
					{
						Window child;
						XTranslateCoordinates(pDisplay,window,rootwin,0,0,
								&x,&y,&child);

						pNativeWindow->setPosition(x,y);
						pNativeWindow->setSize(sx,sy);
					}

					m_event.setSIS(WindowEvent::e_sourceSystem,
							WindowEvent::e_itemResize,(WindowEvent::State)sx);
					m_event.setState2((WindowEvent::State)sy);
				}
					break;

				case ClientMessage:
				{
					U32 do_delete = FALSE;
					U32 quit_app = FALSE;
					switch(report.xclient.format)
					{
						case 8:
							if((Atom)(report.xclient.data.b[0]) == m_atom[0])
								do_delete = TRUE;
							break;
						case 16:
							if((Atom)(report.xclient.data.s[0]) == m_atom[0])
								do_delete = TRUE;
							break;
						case 32:
							if((Atom)(report.xclient.data.l[0]) == m_atom[0])
								do_delete = TRUE;
							if((Atom)(report.xclient.data.l[0]) == m_atom[1])
							{
								do_delete = TRUE;
								quit_app = TRUE;
							}
							break;
						default:
							feLog("ClientMessage: invalid X event format");
							break;
					}
					if(do_delete)
					{
						if(quit_app)
							m_event.setSIS(WindowEvent::e_sourceSystem,
									WindowEvent::e_itemQuit,
									WindowEvent::e_stateNull);
						else
							m_event.setSIS(WindowEvent::e_sourceSystem,
									WindowEvent::e_itemCloseWindow,
									WindowEvent::e_stateNull);
					}
					else
						m_event.setSource(WindowEvent::e_sourceNull);
				}
					break;

				case ButtonPress:
					press=TRUE;
#if FE_CPLUSPLUS >= 201103L
					[[fallthrough]];
#endif
				case ButtonRelease:
					m_event.setSource(WindowEvent::e_sourceMouseButton);
					m_event.setState(press?
							WindowEvent::e_statePress:
							WindowEvent::e_stateRelease);

					m_event.setMousePosition(report.xbutton.x,report.xbutton.y);

					switch(report.xbutton.button)
					{
						case Button1:
							m_event.setItem(WindowEvent::e_itemLeft);
							m_event.setMouseButtons((WindowEvent::MouseButtons)(
									(m_event.mouseButtons()&6) | (press)));
							break;

						case Button2:
							m_event.setItem(WindowEvent::e_itemMiddle);
							m_event.setMouseButtons((WindowEvent::MouseButtons)(
									(m_event.mouseButtons()&5) | (press<<1)
									));
							break;

						case Button3:
							m_event.setItem(WindowEvent::e_itemRight);
							m_event.setMouseButtons((WindowEvent::MouseButtons)(
									(m_event.mouseButtons()&3) | (press<<2)
									));
							break;

						case Button4:
							m_event.setItem(WindowEvent::e_itemWheel);
							m_event.setState((WindowEvent::State)120);
							break;

						case Button5:
							m_event.setItem(WindowEvent::e_itemWheel);
							m_event.setState((WindowEvent::State)-120);
							break;

						default:
							feLog("release without button\n");
							break;
					}

#if FE_WINDOW_EVENT_DEBUG
					feLog("X %s %d mouse=%d,%d\n",press?"Press":"Release",
											m_event.mouseButtons(),
											report.xbutton.x,report.xbutton.y);
#endif
					break;

				case KeyPress:
				case KeyRelease:
				{
					KeySym keysym;
					XComposeStatus compose;
					const U32 maxmaplength=10;
					char buffer[maxmaplength];
					const long modifier=report.xkey.state;

					m_event.setSource(WindowEvent::e_sourceKeyboard);
					m_event.setState( (report.type==KeyPress)?
							WindowEvent::e_statePress:
							WindowEvent::e_stateRelease);

					m_event.setMousePosition(report.xkey.x,report.xkey.y);

					const long count=XLookupString(&(report.xkey),buffer,
							maxmaplength, &keysym, &compose);

#if FE_WINDOW_EVENT_DEBUG
					const long keycode=report.xkey.keycode;
					feLog("X %s keycode=%p state=%p count=%d keysym=%p\n",
							(report.type==KeyPress)? "KeyPress": "KeyRelease",
							keycode, report.xkey.state, count, keysym);
#endif

					if(count>0 && keysym>=XK_space && keysym<=XK_asciitilde)
					{
						// consolidated ascii
						m_event.setItem((WindowEvent::Item)buffer[0]);
					}
					else
					{
						switch(keysym)
						{
							case XK_Left:
								m_event.setItem(WindowEvent::e_keyCursorLeft);
								break;

							case XK_Up:
								m_event.setItem(WindowEvent::e_keyCursorUp);
								break;

							case XK_Right:
								m_event.setItem(WindowEvent::e_keyCursorRight);
								break;

							case XK_Down:
								m_event.setItem(WindowEvent::e_keyCursorDown);
								break;

							case XK_ISO_Left_Tab:
								m_event.setItem(WindowEvent::e_keyTab);
								break;

							default:
								if( count>0 && keysym >= XK_BackSpace &&
												keysym <= XK_Delete)
								{
									// scattered ascii (ascii | 0xFF00)
									m_event.setItem(
											(WindowEvent::Item)buffer[0]);
								}
								else
								{
									m_event.setSource(
											WindowEvent::e_sourceNull);
								}

								break;
						}
					}

					// convert CR (\r) into LF (\n)
					if(m_event.item()==13)
						m_event.setItem((WindowEvent::Item)10);

					// shift, caplock, and control modifiers
					WindowEvent::Item item=m_event.item();

					U32 ctrl_add='a'-1;
					if(modifier&ShiftMask)
					{
						item=WindowEvent::Item(item|WindowEvent::e_keyShift);
						ctrl_add='A'-1;
					}
					if(modifier&LockMask)
					{
						ctrl_add='A'-1;
						item=WindowEvent::Item(item|WindowEvent::e_keyCapLock);
					}
					if(modifier&ControlMask)
					{
						item=WindowEvent::Item(item+ctrl_add);
						item=WindowEvent::Item(item|WindowEvent::e_keyControl);
					}
					if(modifier&Mod1Mask)
					{
						item=WindowEvent::Item(item|WindowEvent::e_keyAlt);
					}

					m_event.setItem(item);
				}
					break;

				case MotionNotify:
				{
					m_event.setMousePosition(report.xmotion.x,report.xmotion.y);

					// redetermine state of mouse buttons
					unsigned int button_state=report.xmotion.state;

					U32 mouseflags=0x0;
					mouseflags|=(button_state&Button1Mask)? 1: 0;
					mouseflags|=(button_state&Button2Mask)? 2: 0;
					mouseflags|=(button_state&Button3Mask)? 4: 0;

					m_event.setMouseButtons(
							WindowEvent::MouseButtons(mouseflags));

#if FE_WINDOW_EVENT_DEBUG
					feLog("MotionNotify %p\n",m_event.mouseButtons());
#endif

					if(m_event.mouseButtons())
						m_event.setSIS(WindowEvent::e_sourceMousePosition,
								WindowEvent::e_itemDrag,
								WindowEvent::e_stateRepeat);
					else if(m_pointerMotion)
					{
						m_event.setSIS(WindowEvent::e_sourceMousePosition,
								WindowEvent::e_itemMove,
								WindowEvent::e_stateRepeat);
					}
					else
					{
						m_event.setSource(WindowEvent::e_sourceNull);
					}

					break;
				}
			}

			threadUnlock();

			if(report.type==ButtonPress || report.type==ButtonRelease ||
					report.type==MotionNotify)
			{
				WindowEvent::Item item=m_event.item();

				long modifier;
				if(report.type==MotionNotify)
					modifier=report.xmotion.state;
				else
					modifier=report.xbutton.state;

				if(modifier&ShiftMask)
					item=WindowEvent::Item(item|WindowEvent::e_keyShift);
				if(modifier&LockMask)
					item=WindowEvent::Item(item|WindowEvent::e_keyCapLock);
				if(modifier&ControlMask)
					item=WindowEvent::Item(item|WindowEvent::e_keyControl);
				if(modifier&Mod1Mask)
					item=WindowEvent::Item(item|WindowEvent::e_keyAlt);

				//* Mod1Mask = left/right alt
				//* Mod2Mask = num lock
				//* Mod3Mask = left window
				//* Mod4Mask = right window
				//* Mod5Mask = Mode?

				m_event.setItem(item);
			}

			if(m_event.source())
			{
				broadcastEvent(pWindowHandle);
			}
		}
		else
		{
			threadUnlock();

			//* TODO send idle to main signaler (anonymous of window)
			m_event.setSIS(WindowEvent::e_sourceSystem,WindowEvent::e_itemIdle,
					WindowEvent::e_stateRepeat);
			if(pNativeWindow)
				broadcastEvent(pNativeWindow->windowHandle());

			if(dropout)
				done=true;
		}
	}
}

void NativeEventContext::accumulateTime(U32 exclusive,U32 inclusive)
{
	const U32 update_ms=300;

	m_time_excl+=exclusive;
	m_time_incl+=inclusive;
	m_frames++;

	if(inclusive==0 || m_time_incl>update_ms)
	{
		m_fps_excl=m_time_excl? m_frames/(m_time_excl*0.001f): 0.0f;
		m_fps_incl=inclusive? m_frames/(m_time_incl*0.001f): 0.0f;

		m_time_incl=0;
		m_time_excl=0;
		m_frames=0;
	}
}

U32 NativeEventContext::systemTimer(void)
{
	//	return timeGetTime();
	return 0;
}

#if FALSE
U32 NativeEventContext::loadFont(FE_WINDOW_HANDLE &windowHandle)
{
	threadLock();

	if(m_pFontStruct)
	{
		XFreeFont(m_pDisplayHandle,m_pFontStruct);
	}

	m_pFontStruct=fe::loadX11Font(m_pDisplayHandle,m_multiplication);

	threadUnlock();

	if(!m_pFontStruct)
	{
		feLog("NativeEventContext::loadFont(): could not load a font\n");
		return 0;
	}

	U32 lists=m_pFontStruct->max_char_or_byte2+1;
	U32 fontBase=glGenLists(lists);
	if(!fontBase)
	{
		feLog("NativeEventContext::loadFont():"
				" Out of display lists (requested %d) glGetError=%d\n",
				lists,glGetError());
		threadLock();
		XFreeFont(m_pDisplayHandle,m_pFontStruct);
		threadUnlock();
		m_pFontStruct=NULL;
		return 0;
	}

	calcFontHeight(windowHandle);

	return fontBase;
}

void NativeEventContext::calcFontHeight(FE_WINDOW_HANDLE &windowHandle)
{
	int direction;			/* direction hint */

	XCharStruct overall;	/* overall char structure */

	//* quick test of sample string "Qy," (should get high and low extents)
	XTextExtents(m_pFontStruct,"Qy,",
			3,&direction,&m_fontAscent,&m_fontDescent,&overall);

//	m_fontDescent-=1;	//* X thinks different than we do

//	feLog("NativeEventContext::calcFontHeight ascent=%d descent=%d\n",
//			m_fontAscent,m_fontDescent);
}

I32 NativeEventContext::pixelWidth(String string)
{
	if(!m_pFontStruct)
	{
		return 0;
	}
	return XTextWidth(m_pFontStruct,string.c_str(),string.length());
}

void NativeEventContext::useLoadedFont(FE_WINDOW_HANDLE &windowHandle)
{
	if(!m_pFontStruct)
	{
		feLog("NativeEventContext::useLoadedFont(): font not valid\n");
		return;
	}

	Font fontid=m_pFontStruct->fid;
	U32 first=m_pFontStruct->min_char_or_byte2;
	U32 last=m_pFontStruct->max_char_or_byte2;

	glXUseXFont(fontid,first,last-first+1,m_fontBase+first);
}
#endif

#endif

} /* namespace ext */
} /* namespace fe */
