/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __godot_SurfaceAccessibleGodotMesh_h__
#define __godot_SurfaceAccessibleGodotMesh_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Godot ArrayMesh Surface Binding

	@ingroup godot
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleGodotMesh:
	public SurfaceAccessibleBase
{
	public:
								SurfaceAccessibleGodotMesh(void);
virtual							~SurfaceAccessibleGodotMesh(void);

								//* as SurfaceAccessibleI
virtual	BWORD					isBound(void)
								{	return mesh().is_valid(); }

								using SurfaceAccessibleBase::bind;

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create);

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

								//* Godot specific
		Ref<ArrayMesh>			mesh(void);
		void					setMesh(Ref<ArrayMesh> a_refMesh)
								{	m_refMesh=a_refMesh; }

	protected:
virtual	void					reset(void);

	private:
		sp<SurfaceAccessorI>	createAccessor(void);

		Ref<ArrayMesh>			m_refMesh;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __godot_SurfaceAccessibleGodotMesh_h__ */
