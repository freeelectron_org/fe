/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ode/ode.pmh>

#include "platform/dlCore.cc"
#include "fe/data.h"

#include "ContextODE.h"
#include "World.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> master)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->addSingleton<ContextODE>("Component.ContextODE.fe");
	pLibrary->add<World>("MechanicsSimI.World.ode.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
