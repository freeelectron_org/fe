/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_VideoRecorder_h__
#define __viewer_VideoRecorder_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Saves a snapshot for every handle call that time advances

	@ingroup viewer

	As RecorderI, the pathname is the directory name and the integer limit
	sets the number of cycled files.
	The scalar for findNameFor() indicates the desired time to find a file for.
*//***************************************************************************/
class FE_DL_EXPORT VideoRecorder:
		virtual public HandlerI,
		public Initialize<VideoRecorder>,
		virtual public RecorderI
{
	public:
				VideoRecorder(void):
					m_basename("test/video"),
					m_index(0),
					m_ringSize(20)
				{	m_timeStamp.resize(m_ringSize); }

		void	initialize(void);

				//* as HandlerI
virtual void	handle(Record &record);

				//* as RecorderI
virtual	void	configure(String pathname,U32 limit);
virtual	String	findNameFor(Real scalar);

	private:
		String						m_basename;
		U32							m_index;
		U32							m_ringSize;
		std::vector<Real>			m_timeStamp;

		Accessor< sp<Component> >	m_aDrawI;
		AsTemporal					m_asTemporal;
		AsViewer					m_asViewer;
		sp<ImageI>					m_spImageI;
};

inline void VideoRecorder::configure(String pathname,U32 limit)
{
	m_basename=pathname;
	m_ringSize=limit;
	m_timeStamp.resize(m_ringSize);
}

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_VideoRecorder_h__ */

