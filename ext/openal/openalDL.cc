/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <openal/openal.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("feDataDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->addSingleton<AudioAL>("AudioI.AudioAL.fe");

	pLibrary->addSingleton<ListenerAL>("ListenerI.ListenerAL.fe");

	pLibrary->add<VoiceAL>("VoiceI.VoiceAL.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
