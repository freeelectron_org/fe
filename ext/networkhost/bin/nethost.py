if __name__ == '__main__':
    print("nethost doesn't run directly")
    exit()

import os
import ctypes
import ctypes.util
import numpy

fp_nethost_initialized = False
fp_nethost_acquire = None
fp_nethost_release = None
fp_nethost_access_space = None
fp_nethost_space_start = None
fp_nethost_space_stop = None
fp_nethost_space_wait_for_connection = None
fp_nethost_space_flush = None
fp_nethost_space_lock = None
fp_nethost_space_unlock = None
fp_nethost_space_wait_for_update = None
fp_nethost_space_lock_after_update = None
fp_nethost_space_set_state_bools = None
fp_nethost_space_get_state_bools = None
fp_nethost_space_set_state_ints = None
fp_nethost_space_get_state_ints = None
fp_nethost_space_set_state_floats = None
fp_nethost_space_get_state_floats = None
fp_nethost_space_set_state_string = None
fp_nethost_space_get_state_string = None
fp_nethost_space_set_state_bytes = None
fp_nethost_space_get_state_bytes = None
fp_nethost_space_send_message_string = None
fp_nethost_space_next_message_string = None
fp_nethost_snapshot_acquire = None
fp_nethost_snapshot_release = None
fp_nethost_snapshot_get_serial = None
fp_nethost_snapshot_get_flush_count = None
fp_nethost_snapshot_get_state_bools = None
fp_nethost_snapshot_get_state_ints = None
fp_nethost_snapshot_get_state_string = None
fp_nethost_snapshot_get_state_floats = None
fp_nethost_snapshot_get_state_bytes = None
fp_nethost_free_string = None

class NetHost:
    def __init__(self):
        global fp_nethost_initialized
        global fp_nethost_acquire
        global fp_nethost_release
        global fp_nethost_access_space
        global fp_nethost_space_start
        global fp_nethost_space_stop
        global fp_nethost_space_wait_for_connection
        global fp_nethost_space_flush
        global fp_nethost_space_lock
        global fp_nethost_space_unlock
        global fp_nethost_space_wait_for_update
        global fp_nethost_space_lock_after_update
        global fp_nethost_space_set_state_bools
        global fp_nethost_space_get_state_bools
        global fp_nethost_space_set_state_ints
        global fp_nethost_space_get_state_ints
        global fp_nethost_space_set_state_floats
        global fp_nethost_space_get_state_floats
        global fp_nethost_space_set_state_string
        global fp_nethost_space_get_state_string
        global fp_nethost_space_set_state_bytes
        global fp_nethost_space_get_state_bytes
        global fp_nethost_space_send_message_string
        global fp_nethost_space_next_message_string
        global fp_nethost_snapshot_acquire
        global fp_nethost_snapshot_release
        global fp_nethost_snapshot_get_serial
        global fp_nethost_snapshot_get_flush_count
        global fp_nethost_snapshot_get_state_bools
        global fp_nethost_snapshot_get_state_ints
        global fp_nethost_snapshot_get_state_string
        global fp_nethost_snapshot_get_state_floats
        global fp_nethost_snapshot_get_state_bytes
        global fp_nethost_free_string

        if not fp_nethost_initialized:

            # NOTE look in cwd, this file's path, and then this file's parent
            # TODO rethink how to find binaries

            netHostDLFile = "libfexNetworkHostDL.so"
            netHostDLPath = netHostDLFile
            netHostDL = None

            try:
                netHostDL = ctypes.CDLL(netHostDLPath)
            except:
                pass

            if not netHostDL:
                try:
                    scriptDir = os.path.dirname(os.path.realpath(__file__))
                    netHostDLPath = os.path.abspath(os.path.join(scriptDir, netHostDLFile))
                    netHostDL = ctypes.CDLL(netHostDLPath)
                except:
                    pass

            if not netHostDL:
                try:
                    scriptDir = os.path.dirname(scriptDir)
                    netHostDLPath = os.path.abspath(os.path.join(scriptDir, netHostDLFile))

                    netHostDL = ctypes.CDLL(netHostDLPath)
                except:
                    pass

            if not netHostDL:
                print("failed to load fexNetworkHostDL")
                return

            fp_nethost_acquire = netHostDL.fe_nethost_acquire
            fp_nethost_release = netHostDL.fe_nethost_release

            fp_nethost_access_space = netHostDL.fe_nethost_access_space
            fp_nethost_space_start = netHostDL.fe_nethost_space_start
            fp_nethost_space_stop = netHostDL.fe_nethost_space_stop
            fp_nethost_space_wait_for_connection = netHostDL.fe_nethost_space_wait_for_connection

            fp_nethost_space_flush = netHostDL.fe_nethost_space_flush

            fp_nethost_space_lock = netHostDL.fe_nethost_space_lock
            fp_nethost_space_unlock = netHostDL.fe_nethost_space_unlock

            fp_nethost_space_wait_for_update = netHostDL.fe_nethost_space_wait_for_update
            fp_nethost_space_lock_after_update = netHostDL.fe_nethost_space_lock_after_update

            fp_nethost_space_set_state_bools = netHostDL.fe_nethost_space_set_state_bools
            fp_nethost_space_get_state_bools = netHostDL.fe_nethost_space_get_state_bools

            fp_nethost_space_set_state_ints = netHostDL.fe_nethost_space_set_state_ints
            fp_nethost_space_get_state_ints = netHostDL.fe_nethost_space_get_state_ints

            fp_nethost_space_set_state_floats = netHostDL.fe_nethost_space_set_state_floats
            fp_nethost_space_get_state_floats = netHostDL.fe_nethost_space_get_state_floats

            fp_nethost_space_set_state_string = netHostDL.fe_nethost_space_set_state_string
            fp_nethost_space_get_state_string = netHostDL.fe_nethost_space_get_state_string
            fp_nethost_space_get_state_string.restype = ctypes.c_void_p

            fp_nethost_space_set_state_bytes = netHostDL.fe_nethost_space_set_state_bytes
            fp_nethost_space_get_state_bytes = netHostDL.fe_nethost_space_get_state_bytes
            fp_nethost_space_get_state_bytes.restype = ctypes.c_void_p

            fp_nethost_space_send_message_string = netHostDL.fe_nethost_space_send_message_string
            fp_nethost_space_next_message_string = netHostDL.fe_nethost_space_next_message_string
            fp_nethost_space_next_message_string.restype = ctypes.c_void_p

            fp_nethost_snapshot_acquire = netHostDL.fe_nethost_snapshot_acquire
            fp_nethost_snapshot_release = netHostDL.fe_nethost_snapshot_release
            fp_nethost_snapshot_get_serial = netHostDL.fe_nethost_snapshot_get_serial
            fp_nethost_snapshot_get_flush_count = netHostDL.fe_nethost_snapshot_get_flush_count

            fp_nethost_snapshot_get_state_bools = netHostDL.fe_nethost_snapshot_get_state_bools

            fp_nethost_snapshot_get_state_ints = netHostDL.fe_nethost_snapshot_get_state_ints

            fp_nethost_snapshot_get_state_floats = netHostDL.fe_nethost_snapshot_get_state_floats
            fp_nethost_snapshot_get_state_bytes = netHostDL.fe_nethost_snapshot_get_state_bytes
            fp_nethost_snapshot_get_state_bytes.restype = ctypes.c_void_p

            fp_nethost_snapshot_get_state_string = netHostDL.fe_nethost_snapshot_get_state_string
            fp_nethost_snapshot_get_state_string.restype = ctypes.c_void_p

            fp_nethost_free_string = netHostDL.fe_nethost_free_string

            fp_nethost_initialized = True

        fp_nethost_acquire()

    def __del__(self):
        fp_nethost_release()

    def accessSpace(self, name):
        spaceIndex = fp_nethost_access_space(name.encode('utf-8'),"*.ZeroCatalog".encode('utf-8'))
        return Space(spaceIndex,name)

class Space:
    class ByteBuffer:
            # expects ctypes.Structure OR ptr and length
        def __init__(self, obj=None, ptr=0, length=0):
            if obj:
                ptr = ctypes.cast(ctypes.pointer(obj),
                        ctypes.POINTER(ctypes.c_ubyte))
                length = ctypes.sizeof(obj)
                data = [ptr[i] for i in range(length)]
            else:
                data = [ptr[i] for i in range(length)]
            self.array = (ctypes.c_ubyte*length)(*data)

            self.ptr = ctypes.cast(self.array, ctypes.POINTER(ctypes.c_ubyte))
            self.length = length

        def __del__(self):
            pass

        def castTo(self, typeobj):
            return ctypes.cast(self.ptr, ctypes.POINTER(typeobj)).contents

    class Atomic:
        def __init__(self, space, flushCount=None):
            self.space = space
            if flushCount == None:
                self.space.lock()
            else:
                (success, self.flushCount, self.spins) = self.space.lockAfterUpdate(flushCount, spins)

        def __del__(self):
            self.space.unlock()

        def setStateProperty(self, key, prop, value):
            return self.space.setStateProperty(key, prop, value)

        def getStateProperty(self, key, prop, default):
            return self.space.getStateProperty(key, prop, default, safe=False)

        def setState(self, key, value):
            return self.space.setStateProperty(key, "value", value)

        def getState(self, key, default):
            return self.space.getStateProperty(key, "value", default, safe=False)

    def atomic(self, flushCount=None):
        if flushCount == None:
            return Space.Atomic(self)
        else:
            atomic = Space.Atomic(self, flushCount)
            return (atomic, atomic.flushCount, atomic.spins)

    class Snapshot:
        def __init__(self, space):
            self.snapshotIndex = fp_nethost_snapshot_acquire(space.spaceIndex)
            self.snapshotName = space.spaceName
#           print("Snapshot __init__ '%s' %d as %d" % (space.spaceName, space.spaceIndex, self.snapshotIndex))

        def __del__(self):
#           print("Snapshot __del__ '%s' %d" % (self.snapshotName, self.snapshotIndex))
            fp_nethost_snapshot_release(self.snapshotIndex)

        def serial(self):
            return fp_nethost_snapshot_get_serial(self.snapshotIndex)

        def flushCount(self):
            return fp_nethost_snapshot_get_flush_count(self.snapshotIndex)

        def getStateProperty(self, key, prop, default):
#           print("Snapshot.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\"" % (self.snapshotIndex, self.snapshotName, key, prop, str(default)))

            bkey = key.encode('utf-8')
            bprop = prop.encode('utf-8')

            if type(default) is bool:
                c_array = (ctypes.c_bool * 1)(default)
                success = fp_nethost_snapshot_get_state_bools(
                        self.snapshotIndex,
                        bkey, bprop, c_array, 1)
                return (success, c_array[0])
            elif type(default) is int:
                c_array = (ctypes.c_int * 1)(default)
                success = fp_nethost_snapshot_get_state_ints(
                        self.snapshotIndex,
                        bkey, bprop, c_array, 1)
                return (success, c_array[0])
            elif type(default) is str:
                ptr = fp_nethost_snapshot_get_state_string(
                        self.snapshotIndex, bkey, bprop)
                success = (ptr != None)
                if success:
                    c_array = ctypes.cast(ptr,ctypes.c_char_p)
                    text = str(c_array.value.decode('utf-8'))
                    fp_nethost_free_string(c_array);
                else:
                    text = default

#               print("Snapshot.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" %d \"%s\"" % (self.snapshotIndex, self.snapshotName, key, prop, str(default), type(default), success, text))
                return (success, text)
            elif type(default) is float:
                c_array = (ctypes.c_float * 1)(default)
                success = fp_nethost_snapshot_get_state_floats(
                        self.snapshotIndex,
                        bkey, bprop, c_array, 1)
                return (success, c_array[0])
            elif type(default) is numpy.ndarray:
                py_array = []
                for element in default.flat:
                    py_array.append(element)
                c_array = (ctypes.c_float * default.size)(*py_array)
                success = fp_nethost_snapshot_get_state_floats(
                        self.snapshotIndex,
                        bkey, bprop, c_array, default.size)
                for index in range(default.size):
                    default.flat[index] = c_array[index]

                return (success, default)
            elif isinstance(default,Space.ByteBuffer):
                pBytesRead = ctypes.pointer(ctypes.c_int(0))
                ptr = fp_nethost_snapshot_get_state_bytes(
                        self.snapshotIndex, bkey, bprop, pBytesRead)
                success = (ptr != None)
                bytesRead = pBytesRead.contents.value
                if success:
                    c_array = ctypes.cast(ptr, ctypes.POINTER(ctypes.c_ubyte))
                    result = Space.ByteBuffer(ptr=c_array,length=bytesRead)
                    fp_nethost_free_string(c_array);
                else:
                    result = default

#               print("Snapshot.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" %d \"%s\" %d" % (self.snapshotIndex, self.snapshotName, key, prop, str(default), type(default), success, result, bytesRead))
                return (success, result)
            else:
                print("Snapshot.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" unknown type %s" % (self.snapshotIndex, self.snapshotName, key, prop, str(default), type(default)))

        def getState(self, key, default):
            return self.getStateProperty(key, "value", default)

    def snapshot(self):
        return Space.Snapshot(self)

    def __init__(self, spaceIndex, name):
#       print("Space.init %d \"%s\"" % (spaceIndex, name))
        self.spaceIndex = spaceIndex
        self.spaceName = name

    def start(self):
#       print("Space.start %d \"%s\"" % (self.spaceIndex, self.spaceName))
        return fp_nethost_space_start(self.spaceIndex)

    def stop(self):
#       print("Space.stop %d \"%s\"" % (self.spaceIndex, self.spaceName))
        return fp_nethost_space_stop(self.spaceIndex)

    def waitForConnection(self):
#       print("Space.start %d \"%s\"" % (self.spaceIndex, self.spaceName))
        return fp_nethost_space_wait_for_connection(self.spaceIndex)

    def flush(self):
#       print("Space.flush %d \"%s\"" % (self.spaceIndex, self.spaceName))
        return fp_nethost_space_flush(self.spaceIndex)

    def lock(self):
#       print("Space.lock %d \"%s\"" % (self.spaceIndex, self.spaceName))
        return fp_nethost_space_lock(self.spaceIndex)

    def unlock(self):
#       print("Space.unlock %d \"%s\"" % (self.spaceIndex, self.spaceName))
        return fp_nethost_space_unlock(self.spaceIndex)

    def waitForUpdate(self, flushCount):
#       print("Space.waitForUpdate %d \"%s\" %d %d" % (self.spaceIndex, self.spaceName, flushCount))
        rSerial = ctypes.c_int(flushCount)
        rSpins = ctypes.c_int(0)
        success = fp_nethost_space_wait_for_update(self.spaceIndex, ctypes.byref(rSerial), ctypes.byref(rSpins))
        return (success, rSerial.value, rSpins.value)

    def lockAfterUpdate(self, flushCount):
#       print("Space.lockAfterUpdate %d \"%s\" %d %d" % (self.spaceIndex, self.spaceName, flushCount))
        rSerial = ctypes.c_int(flushCount)
        rSpins = ctypes.c_int(0)
        success = fp_nethost_space_lock_after_update(self.spaceIndex, ctypes.byref(rSerial), ctypes.byref(rSpins))
        return (success, rSerial.value, rSpins.value)

    def setStateProperty(self, key, prop, value):
#       print("Space.setStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\"" % (self.spaceIndex, self.spaceName, key, prop, str(value)))

        bkey = key.encode('utf-8')
        bprop = prop.encode('utf-8')

        if type(value) is bool:
            c_array = (ctypes.c_bool * 1)(value)
            return fp_nethost_space_set_state_bools(self.spaceIndex,
                    bkey, bprop, c_array, 1)
        elif type(value) is int:
            c_array = (ctypes.c_int * 1)(value)
            return fp_nethost_space_set_state_ints(self.spaceIndex,
                    bkey, bprop, c_array, 1)
        elif type(value) is str:
            return fp_nethost_space_set_state_string(self.spaceIndex,
                    bkey, bprop, value.encode('utf-8'))
        elif type(value) is float:
            c_array = (ctypes.c_float * 1)(value)
            return fp_nethost_space_set_state_floats(self.spaceIndex,
                    bkey, bprop, c_array, 1)
        elif type(value) is numpy.ndarray:
            py_array = []
            for element in value.flat:
                py_array.append(element)
            c_array = (ctypes.c_float * value.size)(*py_array)
            return fp_nethost_space_set_state_floats(self.spaceIndex,
                    bkey, bprop, c_array, value.size)
        elif isinstance(value,Space.ByteBuffer):
            return fp_nethost_space_set_state_bytes(self.spaceIndex,
                    bkey, bprop, value.array, value.length)
        else:
            print("Space.setStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" unknown type %s" % (self.spaceIndex, self.spaceName, key, prop, str(value), type(value)))

    def getStateProperty(self, key, prop, default, safe=True):
#       print("Space.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\"" % (self.spaceIndex, self.spaceName, key, prop, str(default)))

        bkey = key.encode('utf-8')
        bprop = prop.encode('utf-8')

        if type(default) is bool:
            c_array = (ctypes.c_bool * 1)(default)
            success = fp_nethost_space_get_state_bools(self.spaceIndex,
                    bkey, bprop, c_array, 1, safe)
            return (success, c_array[0])
        elif type(default) is int:
            c_array = (ctypes.c_int * 1)(default)
            success = fp_nethost_space_get_state_ints(self.spaceIndex,
                    bkey, bprop, c_array, 1, safe)
            return (success, c_array[0])
        elif type(default) is str:
            ptr = fp_nethost_space_get_state_string(self.spaceIndex, bkey, bprop, safe)
            success = (ptr != None)
            if success:
                c_array = ctypes.cast(ptr,ctypes.c_char_p)
                text = str(c_array.value.decode('utf-8'))
                fp_nethost_free_string(c_array);
            else:
                text = default

#           print("Space.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" %d \"%s\"" % (self.spaceIndex, self.spaceName, key, prop, str(default), type(default), success, text))
            return (success, text)
        elif type(default) is float:
            c_array = (ctypes.c_float * 1)(default)
            success = fp_nethost_space_get_state_floats(self.spaceIndex,
                    bkey, bprop, c_array, 1, safe)
            return (success, c_array[0])
        elif type(default) is numpy.ndarray:
            py_array = []
            for element in default.flat:
                py_array.append(element)
            c_array = (ctypes.c_float * default.size)(*py_array)
            success = fp_nethost_space_get_state_floats(self.spaceIndex,
                    bkey, bprop, c_array, default.size, safe)
            for index in range(default.size):
                default.flat[index] = c_array[index]

            return (success, default)
        elif isinstance(default,Space.ByteBuffer):
            pBytesRead = ctypes.pointer(ctypes.c_int(0))
            ptr = fp_nethost_space_get_state_bytes(
                    self.spaceIndex, bkey, bprop, safe, pBytesRead)
            success = (ptr != None)
            bytesRead = pBytesRead.contents.value
            if success:
                c_array = ctypes.cast(ptr, ctypes.POINTER(ctypes.c_ubyte))
                result = Space.ByteBuffer(ptr=c_array,length=bytesRead)
                fp_nethost_free_string(c_array);
            else:
                result = default

#               print("Snapshot.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" %d \"%s\" %d" % (self.snapshotIndex, self.snapshotName, key, prop, str(default), type(default), success, result, bytesRead))
            return (success, result)
        else:
            print("Space.getStateProperty %d \"%s\" \"%s\" \"%s\" \"%s\" unknown type %s" % (self.spaceIndex, self.spaceName, key, prop, str(default), type(default)))

    def setState(self, key, value):
        return self.setStateProperty(key, "value", value)

    def getState(self, key, default, safe=True):
        return self.getStateProperty(key, "value", default, safe)

    def sendMessage(self, key, value):
#       print("Space.sendMessage %d \"%s\" \"%s\" \"%s\" \"%s\"" % (self.spaceIndex, self.spaceName, key, str(value)))

        bkey = key.encode('utf-8')

        if type(value) is str:
            return fp_nethost_space_send_message_string(self.spaceIndex,
                    bkey, value.encode('utf-8'))
        else:
            print("Space.sendMessage %d \"%s\" \"%s\" \"%s\" \"%s\" unknown type %s" % (self.spaceIndex, self.spaceName, key, str(value), type(value)))

    def nextMessage(self, key, default=""):
#       print("Space.nextMessage %d \"%s\" \"%s\" \"%s\"" % (self.spaceIndex, self.spaceName, key, str(default)))

        bkey = key.encode('utf-8')

        if type(default) is str:
            ptr = fp_nethost_space_next_message_string(self.spaceIndex, bkey)
            success = (ptr != None)
            if success:
                c_array = ctypes.cast(ptr,ctypes.c_char_p)
                text = str(c_array.value.decode('utf-8'))
                fp_nethost_free_string(c_array);
            else:
                text = default

#           print("Space.nextMessage %d \"%s\" \"%s\" \"%s\" \"%s\" %d \"%s\"" % (self.spaceIndex, self.spaceName, key, str(default), type(default), success, text))
            return (success, text)
        else:
            print("Space.nextMessage %d \"%s\" \"%s\" \"%s\" \"%s\" unknown type %s" % (self.spaceIndex, self.spaceName, key, str(default), type(default)))
