INFO 5
ATTRIBUTE A real =1.0 F64 @other_scope
ATTRIBUTE B string
ATTRIBUTE C real =1.0 F64 
ATTRIBUTE R record @other_scope

LAYOUT L0
	C
	B

LAYOUT L1
	@other_scope
	A
	R

DEFAULTGROUP 1

RECORD 1 L0
	C	1.0
	B	"hello"

RECORD 2 L1
	A	2.1
	R 1

END

