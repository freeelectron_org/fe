/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oiio/oiio.pmh>

namespace fe
{
namespace ext
{

ImageOIIO::ImageOIIO(void):
	m_selected(-1)
{
}

ImageOIIO::~ImageOIIO(void)
{
	//* delete all buffers
	const U32 bufferCount=m_bufferList.size();
	for(U32 bufferIndex=0;bufferIndex<bufferCount;bufferIndex++)
	{
		delete m_bufferList[bufferIndex];
	}
	m_bufferList.clear();
}

I32 ImageOIIO::createSelect(void)
{
	m_bufferList.push_back(new OIIO::ImageBuf());
	m_selected=m_bufferList.size()-1;
	incrementSerial();
	return m_selected;
}

I32 ImageOIIO::loadSelect(String filename)
{
	m_bufferList.push_back(new OIIO::ImageBuf(filename.c_str()));
	m_selected=m_bufferList.size()-1;

	if(!m_bufferList[m_selected]->read(0,true))
	{
		//* HACK debian 9 has broken geterror() symbols
//		feLog("ImageOIIO::loadSelect id %d read failed because \"%s\"\n",
//				m_selected,m_bufferList[m_selected]->geterror().c_str());
		feLog("ImageOIIO::loadSelect id %d read failed\n",m_selected);

		delete m_bufferList[m_selected];
		m_bufferList.pop_back();
		m_selected= -1;
	}
	else
	{
		feLog("ImageOIIO::loadSelect id %d valid %d\n",
				m_selected,m_bufferList[m_selected]->pixels_valid());
	}

	incrementSerial();
	return m_selected;
}

I32 ImageOIIO::interpretSelect(String a_source)
{
	//* would this ever make sense?
	return interpretSelect(const_cast<char*>(a_source.c_str()),
			a_source.length());
}

I32 ImageOIIO::interpretSelect(void* data,U32 size)
{
	return 0;
}

BWORD ImageOIIO::save(String filename)
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return FALSE;
	}

#if OIIO_VERSION > 10304
	return m_bufferList[m_selected]->write(filename.c_str());
#else
	return m_bufferList[m_selected]->save(filename.c_str());
#endif
}

void ImageOIIO::select(I32 id)
{
	if(id<I32(m_bufferList.size()))
	{
		m_selected=id;
	}
}

void ImageOIIO::unload(I32 id)
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return;
	}
	delete m_bufferList[m_selected];
	m_bufferList[m_selected]=NULL;

	//* reclaim ids
	I32 lastId=m_bufferList.size()-1;
	while(lastId>0)
	{
		if(m_bufferList[lastId])
		{
			return;
		}
		m_bufferList.pop_back();
		lastId--;
	}
}

void ImageOIIO::setFormat(ImageI::Format a_format)
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return;
	}

	U32 channels=1;
	switch(a_format)
	{
		case e_rgba:
			channels=4;
			break;
		case e_rgb:
			channels=3;
			break;
		case e_colorindex:
			channels=1;
			break;
		case e_none:
			channels=0;	//* what will this do?
			break;
	}

	const OIIO::TypeDesc fmt(OIIO::TypeDesc::UINT8,
			OIIO::TypeDesc::AGGREGATE(channels));

	FEASSERT(m_bufferList[m_selected]);

	OIIO::ImageSpec spec=m_bufferList[m_selected]->spec();
	spec.set_format(fmt);

    const std::string& imageName=m_bufferList[m_selected]->name();
	m_bufferList[m_selected]->reset(imageName,spec);
	incrementSerial();
}

ImageI::Format ImageOIIO::format(void) const
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return e_none;
	}
	const OIIO::ImageSpec& spec=m_bufferList[m_selected]->spec();
	const OIIO::TypeDesc fmt=spec.format;

//	const size_t basesize=fmt.basesize();
	const size_t numelements=fmt.numelements();

	//* TODO check if float, int, etc

	switch(numelements)
	{
		case 4:
			return e_rgba;
		case 3:
			return e_rgb;
		case 1:
			return e_colorindex;
		default:
			;
	}

	return e_none;
}

void ImageOIIO::resize(U32 width,U32 height,U32 depth)
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return;
	}

	ImageI::Format imageFormat=format();

	U32 channels=1;
	switch(imageFormat)
	{
		case e_rgba:
			channels=4;
			break;
		case e_rgb:
			channels=3;
			break;
		case e_colorindex:
			channels=1;
			break;
		case e_none:
			channels=0;	//* what will this do?
			break;
	}

	const OIIO::TypeDesc fmt=OIIO::TypeDesc::UINT8;	//* TODO
	OIIO::ImageSpec spec(width,height,channels,fmt);

	FEASSERT(m_bufferList[m_selected]);
    const std::string& imageName=m_bufferList[m_selected]->name();
	m_bufferList[m_selected]->reset(imageName,spec);
	incrementSerial();
}

void ImageOIIO::replaceRegion(U32 x,U32 y,U32 z,
		U32 width,U32 height,U32 depth,void* data)
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return;
	}

#if TRUE
	//* one pixel at a time (probably slow)

	OIIO::ImageBuf* pBuffer=m_bufferList[m_selected];
	const U8* bytes=(U8*)data;
	const U32 channels=3;	//* TODO

	float value[4];

	for(U32 y=0;y<height;y++)
	{
		const U32 yy=y*height;
		for(U32 x=0;x<width;x++)
		{
			const U32 xx=(yy+x)*depth;
			for(U32 z=0;z<depth;z++)
			{
				const U32 index=xx+z;
				const U32 byte=index*channels;

				value[0]=bytes[byte]/255.0;
				value[1]=bytes[byte+1]/255.0;
				value[2]=bytes[byte+2]/255.0;

				pBuffer->setpixel(index,value,3);
			}
		}
	}
#else
	OIIO::ROI roi;	//* TODO

	for(OIIO::ImageBuf::Iterator<U8> it(*m_bufferList[m_selected],roi);
			!it.done();it++)
	{
		if(!it.exists())
		{
			continue;
		}

		//* TODO
	}
#endif
	incrementSerial();
}

U32 ImageOIIO::width(void) const
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return 0;
	}
	const OIIO::ImageSpec& spec=m_bufferList[m_selected]->spec();
	return spec.width;
}

U32 ImageOIIO::height(void) const
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return 0;
	}
	const OIIO::ImageSpec& spec=m_bufferList[m_selected]->spec();
	return spec.height;
}

U32 ImageOIIO::depth(void) const
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return 0;
	}
	const OIIO::ImageSpec& spec=m_bufferList[m_selected]->spec();
	return spec.depth;
}

void* ImageOIIO::raw(void) const
{
	if(m_selected<0 || m_selected>=I32(m_bufferList.size()))
	{
		return NULL;
	}

	return m_bufferList[m_selected]->localpixels();
}

} /* namespace ext */
} /* namespace fe */
