/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_MBR_EVENT_DEBUG		FALSE
#define FE_MBR_MANIP_DEBUG		FALSE
#define FE_MBR_CALLBACK_DEBUG	FALSE

namespace fe
{
namespace ext
{

MHWRender::MUIDrawManager* MayaBrush::ms_pMUIDrawManager=NULL;
MayaBrush* MayaBrush::ms_pMayaBrush=NULL;
MayaNode* MayaBrush::ms_pCurrentNode=NULL;
BWORD MayaBrush::ms_mayaBrushCurrent=FALSE;

//* can an actual callback be 0?
MCallbackId MayaBrush::ms_commandCallback=0;
MCallbackId MayaBrush::ms_idleCallback=0;

MTypeId MayaBrushManip::m_id=0xfe1fe;

MayaBrushManip::MayaBrushManip(void)
{
#if FE_MBR_MANIP_DEBUG
	feLog("MayaBrushManip::MayaBrushManip\n");
#endif
}

MayaBrushManip::~MayaBrushManip(void)
{
#if FE_MBR_MANIP_DEBUG
	feLog("MayaBrushManip::~MayaBrushManip\n");
#endif
}

//* static
void* MayaBrushManip::creator(void)
{
	return new MayaBrushManip();
}

//* static
MStatus MayaBrushManip::initialize(void)
{
#if FE_MBR_MANIP_DEBUG
	feLog("MayaBrushManip::initialize\n");
#endif
	return MPxManipContainer::initialize();
}

MStatus MayaBrushManip::connectToDependNode(const MObject &node)
{
#if FE_MBR_MANIP_DEBUG
	feLog("MayaBrushManip::connectToDependNode\n");
#endif
	finishAddingManips();
	return MPxManipContainer::connectToDependNode(node);
}

MStatus MayaBrushManip::createChildren(void)
{
#if FE_MBR_MANIP_DEBUG
	feLog("MayaBrushManip::createChildren\n");
#endif
	return MStatus::kSuccess;
}

void MayaBrushManip::preDrawUI(const M3dView &mayaView)
{
#if FE_MBR_MANIP_DEBUG
	feLog("\nMayaBrushManip::preDrawUI\n");
#endif
}

void MayaBrushManip::drawUI(MHWRender::MUIDrawManager& drawManager,
					const MHWRender::MFrameContext& frameContext) const
{
#if FE_MBR_MANIP_DEBUG
	feLog("\nMayaBrushManip::drawUI %p %p\n",
			&drawManager,&frameContext);
#endif

	drawManager.beginDrawable();

	MayaBrush::ms_pMUIDrawManager=&drawManager;

	if(MayaBrush::ms_pMayaBrush)
	{
		MayaBrush::ms_pMayaBrush->brush();
		MayaBrush::ms_pMayaBrush->redraw();
	}

	drawManager.endDrawable();
}

void MayaBrushManip::draw(M3dView &mayaView,const MDagPath &path,
	M3dView::DisplayStyle style,M3dView::DisplayStatus status)
{
#if FE_MBR_MANIP_DEBUG
	feLog("\nMayaBrushManip::draw\n");
#endif

	MPxManipContainer::draw(mayaView,path,style,status);

	if(MayaBrush::ms_pMayaBrush)
	{
		MayaBrush::ms_pMayaBrush->brush();
		MayaBrush::ms_pMayaBrush->redraw();
	}
}

class MayaBrushCommand: public MPxContextCommand
{
public:
					MayaBrushCommand(void)									{}
virtual	MPxContext*	makeObj(void)	{ return new MayaBrush(); }
static	void*		creator(void)	{ return new MayaBrushCommand(); }
};

/*
MayaBrushContext MayaBrushContext1;
setToolTo MayaBrushContext1;

currentCtx
*/

static void eventCB(void* data)
{
	const String eventName((char*)data);

#if FE_MBR_CALLBACK_DEBUG
	if(eventName!="idleVeryLow")
	{
		feLog("eventCB \"%s\"\n",eventName.c_str());
	}
#endif

	if(eventName=="PostToolChanged")
	{
		MString currentCtx;
		MGlobal::executeCommand("currentCtx",currentCtx);
		String currentName=currentCtx.asChar();

#if FE_MBR_CALLBACK_DEBUG
		feLog("  -> \"%s\"\n",currentName.c_str());
#endif

		MayaBrush::ms_mayaBrushCurrent=(currentName=="MayaBrushCommon");
		MayaBrush::idleAdjust();

#if FE_MBR_CALLBACK_DEBUG
		feLog("eventCB \"%s\" done\n",eventName.c_str());
#endif
		return;
	}

	if(eventName=="PreFileNewOrOpened")
	{
#if FE_MBR_CALLBACK_DEBUG
		feLog("eventCB ms_pMayaBrush %p\n",MayaBrush::ms_pMayaBrush);
#endif

		FEASSERT(MayaBrush::ms_pMayaBrush);
		if(MayaBrush::ms_pMayaBrush)
		{
			sp<Master> spMaster=MayaBrush::ms_pMayaBrush->master();
			if(spMaster.isValid())
			{
				spMaster->catalog()->catalogRemove("MayaBlindID");
			}
		}
		MayaBrush::ms_pCurrentNode=NULL;

#if FE_MBR_CALLBACK_DEBUG
		feLog("eventCB \"%s\" done\n",eventName.c_str());
#endif
		return;
	}
}

static void idleCB(void* data)
{
	//* TODO can we remove this now, replaced with doPtrMoved?
	return;

	MStatus status;
	M3dView mayaView=M3dView::active3dView(&status);
	FEASSERT(status);

	int windowX(0);
	int windowY(0);

#if FE_2DGL==FE_2D_X_GFX
	Window root_return;
	Window child_return;
	int root_x_return;
	int root_y_return;
	unsigned int mask_return;
//	Bool result=
			XQueryPointer(mayaView.display(),mayaView.window(),
			&root_return,&child_return,&root_x_return,&root_y_return,
			&windowX,&windowY,&mask_return);
#elif FE_2DGL==FE_2D_GDI
	//* TODO
#endif

	const I32 width=mayaView.portWidth();
	const I32 height=mayaView.portHeight();

	if(windowX>=0 && windowX<width &&
			windowY>=0 && windowY<height)
	{
		const I32 mouseX=windowX;
		const I32 mouseY=height-1-windowY;

		static I32 s_mouseX=0;
		static I32 s_mouseY=0;
		if(s_mouseX!=mouseX || s_mouseY!=mouseY)
		{
			s_mouseX=mouseX;
			s_mouseY=mouseY;

#if FALSE
			feLog("\nroot %p child %p root %d %d win %d %d mask %p\n",
					root_return,child_return,mask_return,
					root_x_return,root_y_return,windowX,windowY,
					mask_return);

			feLog("idle mouse %d %d viewport %d %d\n",
					mouseX,mouseY,width,height);
#endif

			if(MayaBrush::ms_pMayaBrush)
			{
				MayaBrush::ms_pMayaBrush->doMove(mouseX,mouseY);
			}
			return;
		}
	}

	milliSleep(1);
}

//* NOTE this gets *ALL* mel
static void commandCB(const MString& string,void *data)
{
	//* make the initial check simple and fast
	const char* buffer=string.asChar();
	const U32 length=strlen(buffer);
	if(length<18 || strncmp(&buffer[length-18],"AEnodeNameField",15))
	{
		return;
	}

	MayaNode* pPriorNode=MayaBrush::ms_pCurrentNode;

	String command(string.asChar());
//	feLog("commmandCB: %s",command.c_str());
	command.parse();
	command.parse();
	command.parse();
	const String nodeName=command.parse();

#if FE_MBR_CALLBACK_DEBUG
	feLog("commmandCB: Attribute Edit \"%s\"\n",nodeName.c_str());
#endif

	//* search for best-associated MayaNode
	std::map<String,MayaNode::Brush>::const_iterator it=
			MayaNode::ms_brushMap.find(nodeName);
	if(it!=MayaNode::ms_brushMap.end())
	{
#if FE_MBR_CALLBACK_DEBUG
		feLog("  brush found\n");
#endif

		MayaBrush::ms_pCurrentNode=it->second.m_pMayaNode;
	}
	else
	{
		String pickName;
		String buffer=nodeName;
		String token;
		while(!(token=buffer.parse("\"","|")).empty())
		{
			pickName=token;
		}

#if FE_MBR_CALLBACK_DEBUG
		feLog("  try \"%s\"\n", pickName.c_str());
#endif

		std::map<String,MayaNode::Brush>::const_iterator it2=
				MayaNode::ms_shapeMap.find(pickName);
		if(it2!=MayaNode::ms_shapeMap.end())
		{
			MayaBrush::ms_pCurrentNode=it2->second.m_pMayaNode;

#if FE_MBR_CALLBACK_DEBUG
			feLog("  current %p\n",MayaBrush::ms_pCurrentNode);
			feLog("  mapped to \"%s\"\n",
					MayaBrush::ms_pCurrentNode?
					MayaBrush::ms_pCurrentNode->name().asChar(): "<NULL>");
#endif
		}
		else
		{
			String findCommand="fe_find_shape(\""+nodeName+"\")";

			MString melResult;
			MGlobal::executeCommand(findCommand.c_str(),melResult);

			pickName=melResult.asChar();

#if FE_MBR_CALLBACK_DEBUG
			feLog("  also try \"%s\"\n", pickName.c_str());
#endif

			std::map<String,MayaNode::Brush>::const_iterator it3=
					MayaNode::ms_shapeMap.find(pickName);
			if(it3!=MayaNode::ms_shapeMap.end())
			{
				MayaBrush::ms_pCurrentNode=it3->second.m_pMayaNode;

#if FE_MBR_CALLBACK_DEBUG
				feLog("  mapped to \"%s\"\n",
						MayaBrush::ms_pCurrentNode?
						MayaBrush::ms_pCurrentNode->name().asChar(): "<NULL>");
#endif
			}
			else
			{
				MayaBrush::ms_pCurrentNode=NULL;
			}
		}
	}

	MString response;
	MGlobal::executeCommand(
			("global string $fe_current_node = \""+ nodeName+"\"").c_str(),
			response);

	MayaBrush::idleAdjust();

	if(MayaBrush::ms_pCurrentNode!=pPriorNode)
	{
		MayaBrush::provokeRedraw();
	}
}

//* static
void MayaBrush::idleAdjust(void)
{
	const BWORD desired=(ms_mayaBrushCurrent && ms_pCurrentNode);

#if FE_MBR_CALLBACK_DEBUG
	feLog("MayaBrush::idleAdjust callback %p desired %d\n",
			ms_idleCallback,desired);
#endif

	if(desired && !ms_idleCallback)
	{
		MStatus status=MStatus::kSuccess;
		ms_idleCallback=MEventMessage::addEventCallback("idle",idleCB,
				NULL,&status);
		FEASSERT(status);
	}
	else if(!desired && ms_idleCallback)
	{
		MStatus status=MMessage::removeCallback(ms_idleCallback);
		FEASSERT(status);
		ms_idleCallback=0;
	}
}

//* static
void MayaBrush::staticSetup(void)
{
	if(MGlobal::mayaState()!=MGlobal::kInteractive)
	{
		return;
	}

	if(ms_commandCallback)
	{
		//* already set up
		return;
	}

	MStatus status=MStatus::kSuccess;

	//* TODO remove callback before unloading
	ms_commandCallback=MCommandMessage::addCommandCallback(
			commandCB,NULL,&status);
	FEASSERT(status);

#if FE_MBR_CALLBACK_DEBUG
	feLog("MayaBrush::staticSetup commandCallback %p\n",ms_commandCallback);
#endif

//	MCallbackId cid = MEventMessage::addEventCallback(
//			"PostToolChanged",eventCB,(void*)"PostToolChanged",&status);

	//* see mel command help on 'scriptjob' for descriptions
	MStringArray eventNames;
	MEventMessage::getEventNames(eventNames);
	const U32 nameCount=eventNames.length();
	for(U32 m=0;m<nameCount;m++)
	{
#if FE_MBR_CALLBACK_DEBUG
		feLog("  event %d \"%s\"\n",m,eventNames[m].asChar());
#endif

		const char* eventName=eventNames[m].asChar();
		if(eventName!=String("idle") && eventName!=String("idleHigh"))
		{
			MEventMessage::addEventCallback(eventName,eventCB,
					(void*)eventName,&status);
			FEASSERT(status);
		}
	}

	MGlobal::executeCommand("MayaBrushContext MayaBrushCommon");
}

MayaBrush::MayaBrush(void):
	m_mouseX(0),
	m_mouseY(0)
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::MayaBrush\n");
#endif
	setImage("FE_240.svg",MPxContext::kImage1);
	setTitleString("FE Tool");

	m_pOperatorContext=new MayaContext();
	sp<Scope> spScope=m_pOperatorContext->scope();
	initEvent(spScope);

	FEASSERT(!ms_pMayaBrush);
	ms_pMayaBrush=this;
}

MayaBrush::~MayaBrush(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::~MayaBrush\n");
#endif
}

void MayaBrush::toolOnSetup(MEvent &mayaEvent)
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::toolOnSetup\n");
#endif

	updateManipulators(this);
	MStatus status;
	m_updateCallback=MModelMessage::addCallback(
			MModelMessage::kActiveListModified,updateManipulators,this,&status);
	if(!status)
	{
		feLog("MayaBrush::toolOnSetup"
				" addCallback failed for updateManipulators\n");
	}
}

void MayaBrush::toolOffCleanup()
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::toolOffCleanup\n");
#endif

	MStatus status=MModelMessage::removeCallback(m_updateCallback);
	if(!status)
	{
		feLog("MayaBrush::toolOffCleanup"
				" removeCallback failed for updateManipulators\n");
	}

	MPxContext::toolOffCleanup();
}

void MayaBrush::updateManipulators(void* m_pData)
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::updateManipulators\n");
#endif

	MayaBrush* pMayaBrush=(MayaBrush*) m_pData;
	pMayaBrush->deleteManipulators();

	MString manipName("MayaBrushManip");
	MObject manipObject;
	MayaBrushManip* pMayaBrushManip=(MayaBrushManip *)
			MayaBrushManip::newManipulator(manipName,manipObject);

	if(!pMayaBrushManip)
	{
		feLog("MayaBrush::updateManipulators error creating manipulator\n");
		return;
	}

	pMayaBrush->addManipulator(manipObject);

#if FALSE
	feLog("MayaBrush::updateManipulators current %p\n",ms_pCurrentNode);

	if(!ms_pCurrentNode ||
			!pMayaBrushManip->connectToDependNode(ms_pCurrentNode->thisMObject()))
	{
		feLog("MayaBrush::updateManipulators"
				" error connecting manipulator\n");
	}
#endif
}

void MayaBrush::provokeRedraw(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::provokeRedraw\n");
#endif
	MStatus status;
	M3dView mayaView=M3dView::active3dView(&status);
	FEASSERT(status);
	mayaView.refresh(true);

	if(!ms_pCurrentNode)
	{
		return;
	}

	MGlobal::executeCommand("dgdirty "+ms_pCurrentNode->name());
}

MStatus MayaBrush::doEnterRegion(MEvent &mayaEvent)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doEnterRegion\n");
#endif

	MayaNode* pMayaNode=dynamic_cast<MayaNode*>(ms_pCurrentNode);
	const String prompt=pMayaNode? pMayaNode->prompt(): "";

	setHelpString(prompt.c_str());

	return MStatus::kSuccess;
}

MStatus MayaBrush::doPress(MEvent& mayaEvent,
	MHWRender::MUIDrawManager& drawManager,
	const MHWRender::MFrameContext& frameContext)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doPress\n");
#endif
	interpretMouse(mayaEvent,WindowEvent::e_statePress);

	provokeRedraw();
	return MStatus::kSuccess;
}

MStatus MayaBrush::doRelease(MEvent& mayaEvent,
	MHWRender::MUIDrawManager& drawManager,
	const MHWRender::MFrameContext& frameContext)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doRelease\n");
#endif
	interpretMouse(mayaEvent,WindowEvent::e_stateRelease);

	provokeRedraw();
	return MStatus::kSuccess;
}

MStatus MayaBrush::doDrag(MEvent& mayaEvent,
	MHWRender::MUIDrawManager& drawManager,
	const MHWRender::MFrameContext& frameContext)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doDrag\n");
#endif

	interpretMouse(mayaEvent,WindowEvent::e_stateRepeat);

	provokeRedraw();
	return MStatus::kSuccess;
}

MStatus MayaBrush::doHold(MEvent& mayaEvent,
	MHWRender::MUIDrawManager& drawManager,
	const MHWRender::MFrameContext& frameContext)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doHold\n");
#endif

	return MStatus::kSuccess;
}

MStatus MayaBrush::doPtrMoved(MEvent& mayaEvent,
	MHWRender::MUIDrawManager& drawManager,
	const MHWRender::MFrameContext& frameContext)
{
	short mouseX;
	short mouseY;
	mayaEvent.getPosition(mouseX,mouseY);

#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doPtrMoved %d %d\n",mouseX,mouseY);
#endif

	doMove(mouseX,mouseY);

	return MStatus::kSuccess;
}

MStatus MayaBrush::drawFeedback(
	MHWRender::MUIDrawManager& drawManager,
	const MHWRender::MFrameContext& frameContext)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::drawFeedback\n");
#endif

	return MStatus::kSuccess;
}

void MayaBrush::doMove(I32 a_mouseX,I32 a_mouseY)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::doMove %d %d\n",a_mouseX,a_mouseY);
#endif
	m_event.reset();
	m_event.setSIS(WindowEvent::e_sourceMousePosition,
			WindowEvent::Item(WindowEvent::e_itemMove),
			WindowEvent::e_stateRepeat);
	m_event.setMousePosition(a_mouseX,a_mouseY);

	m_mouseX=a_mouseX;
	m_mouseY=a_mouseY;

//	brush();
	provokeRedraw();
}

void MayaBrush::deleteAction(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::deleteAction\n");
#endif
	m_event.reset();
	m_event.setSIS(WindowEvent::e_sourceKeyboard,
			WindowEvent::e_keyDelete,WindowEvent::e_statePress);

//	brush();
	provokeRedraw();
}

void MayaBrush::completeAction(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::completeAction\n");
#endif

	//* NOTE X11 seems to use line feed, but Houdini sends carriage return

	m_event.reset();
	m_event.setSIS(WindowEvent::e_sourceKeyboard,
			WindowEvent::e_keyCarriageReturn,WindowEvent::e_statePress);

//	brush();
	provokeRedraw();
}

void MayaBrush::abortAction(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::abortAction\n");
#endif
	m_event.reset();
	m_event.setSIS(WindowEvent::e_sourceKeyboard,
			WindowEvent::e_keyEscape,WindowEvent::e_statePress);

//	brush();
	provokeRedraw();
}

bool MayaBrush::processNumericalInput(const MDoubleArray& rValues,
								const MIntArray& rFlags,bool isAbsolute)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::processNumericalInput\n");
#endif
	return false;
}

void MayaBrush::interpretMouse(MEvent& mayaEvent,WindowEvent::State state)
{
#if FE_MBR_EVENT_DEBUG
	feLog("\nMayaBrush::interpretMouse %p node %p\n",this,ms_pCurrentNode);
#endif

	if(!ms_pCurrentNode)
	{
		return;
	}

	WindowEvent::Item modifiers=WindowEvent::e_itemNull;
	if(mayaEvent.isModifierShift())
	{
		modifiers=WindowEvent::Item(modifiers|WindowEvent::e_keyShift);
	}
	if(mayaEvent.isModifierControl())
	{
		modifiers=WindowEvent::Item(modifiers|WindowEvent::e_keyControl);
	}

	MStatus status;
	MEvent::MouseButtonType buttonType=mayaEvent.mouseButton(&status);
	if(!status)
	{
		feLog("MayaBrush::interpretMouse failed to query mouse button\n");
		return;
	}

	if(state==WindowEvent::e_stateRepeat)
	{
		m_event.setSIS(WindowEvent::e_sourceMousePosition,
				WindowEvent::Item(WindowEvent::e_itemDrag|modifiers),
				WindowEvent::e_stateRepeat);
	}
	else if(buttonType==MEvent::kLeftMouse)
	{
		m_event.setSIS(WindowEvent::e_sourceMouseButton,
				WindowEvent::Item(WindowEvent::e_itemLeft|modifiers),
				state);
		m_event.setMouseButtons(state==WindowEvent::e_statePress?
				WindowEvent::e_mbLeft: WindowEvent::e_mbNone);
	}
	else if(buttonType==MEvent::kMiddleMouse)
	{
		m_event.setSIS(WindowEvent::e_sourceMouseButton,
				WindowEvent::Item(WindowEvent::e_itemMiddle|modifiers),
				state);
		m_event.setMouseButtons(state==WindowEvent::e_statePress?
				WindowEvent::e_mbMiddle: WindowEvent::e_mbNone);
	}

	short mouseX;
	short mouseY;
	mayaEvent.getPosition(mouseX,mouseY);
	m_event.setMousePosition(mouseX,mouseY);

	m_mouseX=mouseX;
	m_mouseY=mouseY;

//	brush();
}

void MayaBrush::brush(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::brush %p node %p\n",this,ms_pCurrentNode);
#endif

	if(!ms_pCurrentNode)
	{
		return;
	}

//	MGlobal::executeCommand("dgdirty "+ms_pCurrentNode->name());

	//* TODO should only need to do once
	initDrawInterfaces(ms_pCurrentNode);

	MStatus status;
	M3dView mayaView=M3dView::active3dView(&status);
	FEASSERT(status);

	status=mayaView.updateViewingParameters();

	set(m_viewport,0,0,mayaView.portWidth(),mayaView.portHeight());

	MMatrix projectionMatrix;
	status=mayaView.projectionMatrix(projectionMatrix);
	FEASSERT(status);

	Matrix<4,4,Real> perspective;
	set(perspective,projectionMatrix.matrix[0]);

	MMatrix modelViewMatrix;
	status=mayaView.modelViewMatrix(modelViewMatrix);
	FEASSERT(status);

	SpatialTransform modelView;
	set(modelView,modelViewMatrix.matrix[0]);

	Real fovy=0.0;
	Real aspect=0.0;
	Real nearplane=0.0;
	Real farplane=0.0;
	decomposePerspective(perspective,fovy,aspect,nearplane,farplane);

#if FALSE
	feLog("projection\n%s\n",c_print(perspective));
	feLog("modelView\n%s\n",c_print(modelView));
	feLog("viewport %d %d\n",mayaView.portWidth(),mayaView.portHeight());
	feLog("fovy %.6G aspect %.6G near %.6G far %.6G\n",
			fovy,aspect,nearplane,farplane);

//	const Matrix<4,4,Real> proj4x4=fe::perspective(fovy,aspect,
//			nearplane,farplane);
//	feLog("recombined\n%s\n",c_print(proj4x4));
#endif

	m_projection=perspective;

	sp<CameraI> spCameraPerspective=m_spDrawPerspective->view()->camera();
	sp<CameraI> spCameraOrtho=m_spDrawOrtho->view()->camera();

	m_spDrawPerspective->view()->setViewport(m_viewport);
	m_spDrawOrtho->view()->setViewport(m_viewport);

	spCameraPerspective->setCameraMatrix(modelView);
	spCameraPerspective->setFov(Vector2(0.0,fovy));
	spCameraPerspective->setPlanes(nearplane,farplane);

	spCameraOrtho->setCameraMatrix(modelView);
	spCameraOrtho->setFov(Vector2(0.0,fovy));
	spCameraOrtho->setPlanes(nearplane,farplane);

	MPoint worldPt;
	MVector worldDir;
	status=mayaView.viewToWorld(m_mouseX,m_mouseY,worldPt,worldDir);
	FEASSERT(status);

	const SpatialVector rayOrigin(worldPt[0],worldPt[1],worldPt[2]);
	const SpatialVector rayDirection(worldDir[0],worldDir[1],worldDir[2]);

#if FALSE
	feLog("rayOrigin %s rayDirection %s\n",
			c_print(rayOrigin),c_print(rayDirection));
#endif

	if(m_event.isNull())
	{
		//* presumably a camera change
		m_event.setSIS(WindowEvent::e_sourceSystem,
				WindowEvent::Item(WindowEvent::e_itemExpose),
				WindowEvent::e_stateNull);
	}

#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::brush %s\n",c_print(m_event));
#endif
	ms_pCurrentNode->brush(m_event,rayOrigin,rayDirection);

	//* no repeats
	const WindowEvent::MouseButtons buttons=m_event.mouseButtons();
	const I32 mouseX=m_event.mouseX();
	const I32 mouseY=m_event.mouseY();
	m_event.reset();
	m_event.setMouseButtons(buttons);
	m_event.setMouseX(mouseX);
	m_event.setMouseY(mouseY);
}

void MayaBrush::redraw(void)
{
#if FE_MBR_EVENT_DEBUG
	feLog("MayaBrush::redraw %p node %p\n",this,ms_pCurrentNode);
#endif

	if(!ms_pCurrentNode)
	{
		return;
	}

	if(!ms_pMUIDrawManager)
	{
		feLog("MayaBrush::redraw MUIDrawManager is NULL\n");
	}

	if(m_spDrawOpenGL.isNull() && m_spMaster.isValid())
	{
		m_spDrawOpenGL=m_spMaster->registry()->create("DrawI.DrawMaya");

		if(m_spDrawOpenGL.isValid())
		{
			m_spDrawOpenGL->setName("MayaBrush DrawMaya");
		}

		setupDrawOpenGL();

		//* only try to create once
		m_spMaster=NULL;
	}

	sp<DrawMaya> spDrawMaya=m_spDrawOpenGL;
	if(spDrawMaya.isValid())
	{
		spDrawMaya->setDrawManager(ms_pMUIDrawManager);
	}
	else
	{
		feLog("MayaBrush::redraw DrawMaya invalid\n");
	}

	MStatus status;
	M3dView mayaView=M3dView::active3dView(&status);
	FEASSERT(status);

	draw();
}

} /* namespace ext */
} /* namespace fe */
