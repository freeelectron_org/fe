/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OpenSubdivOp_h__
#define __operator_OpenSubdivOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Apply subdivision using OpenSubdiv

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT OpenSubdivOp:
	public OperatorSurfaceCommon,
	public Initialize<OpenSubdivOp>
{
	public:

					OpenSubdivOp(void)											{}
virtual				~OpenSubdivOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OpenSubdivOp_h__ */
