/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

namespace fe
{
namespace ext
{

void *lua_alloc(void *ud, void *ptr, size_t osize, size_t nsize)
{
	//* not used
	(void)ud;
	(void)osize;

	if(nsize == 0)
	{
//		feLog("lua_alloc free %p %p %d %d\n",ud,ptr,osize,nsize);
//		fe::deallocate(ptr);
		free(ptr);
		return NULL;
	}

//	feLog("lua_alloc realloc %p %p %d %d\n",ud,ptr,osize,nsize);
//	return fe::reallocate(ptr, nsize);
	return realloc(ptr, nsize);
}

// https://stackoverflow.com/questions/4508119/redirecting-redefining-print-for-embedded-lua
extern "C"
{
static int lua_print(lua_State* L)
{
    int nargs = lua_gettop(L);

	String message;

    for (int i=1; i <= nargs; i++)
	{
        if (lua_isstring(L, i))
		{
			message+=(String)lua_tostring(L,i);
        }
        else
		{
			/* Do something with non-strings if you like */
        }
    }

	if(!message.empty())
	{
		feLogGroup("lua_script",(message+"\n").c_str());
	}

    return 0;
}
}

static const struct luaL_Reg lua_printlib[]=
{
	{ "print", lua_print },
	{ NULL, NULL } /* end of array */
};

void lua_replace_print(lua_State *a_pLuaState)
{
	lua_getglobal(a_pLuaState, "_G");

#if FALSE
	// for Lua versions < 5.2
	luaL_register(a_pLuaState, NULL, lua_printlib);
#else
	// for Lua versions 5.2 or greater
	luaL_setfuncs(a_pLuaState, lua_printlib, 0);
#endif

	lua_pop(a_pLuaState, 1);

#if FE_USE_PRINTF
	PrefaceLog* pPrefaceLog=new PrefaceLog();

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	pPrefaceLog->setPreface("lua     ");
#else
	pPrefaceLog->setPreface("[35mlua   [0m  ");
#endif

	feLogger()->setLog("lua_preface",pPrefaceLog);
	feLogger()->bind("lua_script", "lua_preface");
	feLogger()->antibind("lua_script", "prefix");
	feLogger()->antibind("lua_script", "win32");
#endif
}

LuaContext::LuaContext(sp<Master> spMaster, lua_State *pLuaState):
		m_name("LuaContext"),
		m_preamble(0)
{
	if(!spMaster.isValid())
	{
		feX("LuaContext::LuaContext",
			"Master not valid");
	}

	registerRegion(this,sizeof(*this));

	m_hpMaster = spMaster;
	sp<TypeMaster> spTM = spMaster->typeMaster();
	m_pLuaState = pLuaState;

	lua_replace_print(m_pLuaState);

	m_spBooleanType = spTM->lookupType<bool>();
	m_spIntegerType = spTM->lookupType<int>();
	m_spFloatType = spTM->lookupType<float>();
	m_spDoubleType = spTM->lookupType<double>();
	m_spStringType = spTM->lookupType<String>();
	m_spRecordType = spTM->lookupType<Record>();
	m_spGroupType = spTM->lookupType<sp<RecordGroup> >();
	m_spArrayType = spTM->lookupType<sp<RecordArray> >();
	m_spComponentType = spTM->lookupType<sp<Component> >();
	m_spIntArrayType = spTM->lookupType< Array<I32> >();
	m_spVector2fType = spTM->lookupType<Vector2f>();
	m_spVector2dType = spTM->lookupType<Vector2d>();
	m_spVector3fType = spTM->lookupType<Vector3f>();
	m_spVector3dType = spTM->lookupType<Vector3d>();
	m_spVector4fType = spTM->lookupType<Vector4f>();
	m_spVector4dType = spTM->lookupType<Vector4d>();
	m_spTransformType = spTM->lookupType<SpatialTransform>();
	m_spStateCatalogType = spTM->lookupType< sp<StateCatalog> >();
	m_spScopeType = spTM->lookupType<sp<Scope> >();
	m_spLayoutType = spTM->lookupType<sp<Layout> >();

	m_luaTypes.resize(e_size);

	m_luaTypes[e_record] =
		new LuaRecordType(this,"mt_record");
	m_luaTypes[e_record]->putMetaTable();

	m_luaTypes[e_layout] =
		new LuaLayoutType(this,"mt_layout");
	m_luaTypes[e_layout]->putMetaTable();

	m_luaTypes[e_scope] =
		new LuaScopeType(this,"mt_scope");
	m_luaTypes[e_scope]->putMetaTable();

	m_luaTypes[e_array] =
		new LuaArrayType(this,"mt_array");
	m_luaTypes[e_array]->putMetaTable();

	m_luaTypes[e_arrayit] =
		new LuaArrayItType(this,"mt_arrayit");
	m_luaTypes[e_arrayit]->putMetaTable();

	m_luaTypes[e_group] =
		new LuaGroupType(this,"mt_group");
	m_luaTypes[e_group]->putMetaTable();

	m_luaTypes[e_groupit] =
		new LuaGroupItType(this,"mt_groupit");
	m_luaTypes[e_groupit]->putMetaTable();

	m_luaTypes[e_component] =
		new LuaComponentType(this,"mt_component");
	m_luaTypes[e_component]->putMetaTable();

	m_luaTypes[e_intarray] =
		new LuaIntArrayType(this,"mt_intarray");
	m_luaTypes[e_intarray]->putMetaTable();

	m_luaTypes[e_intarrayit] =
		new LuaIntArrayItType(this,"mt_intarrayit");
	m_luaTypes[e_intarrayit]->putMetaTable();

	m_luaTypes[e_vector] =
		new LuaVectorType(this,"mt_vector");
	m_luaTypes[e_vector]->putMetaTable();

	m_luaTypes[e_transform] =
		new LuaTransformType(this,"mt_transform");
	m_luaTypes[e_transform]->putMetaTable();

	m_luaTypes[e_statecatalog] =
		new LuaStateCatalogType(this,"mt_statecatalog");
	m_luaTypes[e_statecatalog]->putMetaTable();

	m_luaTypes[e_vector4f] =
		new LuaVector4fType(this,"mt_vector4f");
	m_luaTypes[e_vector4f]->putMetaTable();

	m_luaTypes[e_vector4d] =
		new LuaVector4dType(this,"mt_vector4d");
	m_luaTypes[e_vector4d]->putMetaTable();

	m_luaTypes[e_vector3f] =
		new LuaVector3fType(this,"mt_vector3f");
	m_luaTypes[e_vector3f]->putMetaTable();

	m_luaTypes[e_vector3d] =
		new LuaVector3dType(this,"mt_vector3d");
	m_luaTypes[e_vector3d]->putMetaTable();

	m_luaTypes[e_vector2f] =
		new LuaVector2fType(this,"mt_vector2f");
	m_luaTypes[e_vector2f]->putMetaTable();

	m_luaTypes[e_vector2d] =
		new LuaVector2dType(this,"mt_vector2d");
	m_luaTypes[e_vector2d]->putMetaTable();

	lua_rawgeti(m_pLuaState, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
	lua_pushstring(m_pLuaState, "fe_master");
	lua_pushlightuserdata(m_pLuaState, spMaster.raw());
	lua_settable(m_pLuaState, -3);
	lua_pop(pLuaState, 1);

	lua_rawgeti(m_pLuaState, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
	lua_pushstring(m_pLuaState, "fe_context");
	lua_pushlightuserdata(m_pLuaState, this);
	lua_settable(m_pLuaState, -3);
	lua_pop(pLuaState, 1);

	setWindowEventEnums();

	lua_register(m_pLuaState, "manage",				manage);
	lua_register(m_pLuaState, "catalog",			catalog);
	lua_register(m_pLuaState, "create",				create);
	lua_register(m_pLuaState, "createRecordArray",	createRecordArray);
	lua_register(m_pLuaState, "createScope",		createScope);
	lua_register(m_pLuaState, "componentScope",		createScopeComponent);
	lua_register(m_pLuaState, "b_or",				b_or);
	lua_register(m_pLuaState, "b_and",				b_and);
	lua_register(m_pLuaState, "createRecordGroup",	createRecordGroup);
	lua_register(m_pLuaState, "createRecord",		createRecord);
	lua_register(m_pLuaState, "Vector3",			createVector3);
}

LuaContext::~LuaContext(void)
{
//	feLog("~LuaContext\n");

#if FE_COUNTED_TRACK
	U32 size=m_trackArray.size();
	for(U32 m=0;m<size;m++)
	{
		deregisterRegion(m_trackArray[m]);
	}
#endif
}

void LuaContext::set(const char *tbl, const char *name, double value)
{
	lua_getglobal(m_pLuaState, tbl);
	if(!lua_istable(m_pLuaState, -1))
	{
		lua_pop(m_pLuaState, 1);

		lua_rawgeti(m_pLuaState, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
		lua_pushstring(m_pLuaState, tbl);
		lua_newtable(m_pLuaState);
		lua_settable(m_pLuaState, -3);
		lua_pop(m_pLuaState, 1);

		lua_getglobal(m_pLuaState, tbl);
	}

	lua_rawgeti(m_pLuaState, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
	lua_pushstring(m_pLuaState, name);
	lua_pushnumber(m_pLuaState, value);
	lua_settable(m_pLuaState, -3);
	lua_pop(m_pLuaState, 1);

	lua_pop(m_pLuaState, 1);
}


sp<LuaBaseType> LuaContext::lookup(FE_UWORD tid)
{
	return m_luaTypes[tid];
}

lua_State *LuaContext::state(void)
{
	return m_pLuaState;
}


void LuaContext::pushInstance(Instance &instance)
{
	if(instance.type() == m_spBooleanType)
	{
		lua_pushnumber(m_pLuaState, (lua_Number)(instance.cast<bool>()));
	}
	else if(instance.type() == m_spIntegerType)
	{
		lua_pushnumber(m_pLuaState, (lua_Number)(instance.cast<int>()));
	}
	else if(instance.type() == m_spFloatType)
	{
		lua_pushnumber(m_pLuaState, (lua_Number)(instance.cast<float>()));
	}
	else if(instance.type() == m_spDoubleType)
	{
		lua_pushnumber(m_pLuaState, (lua_Number)(instance.cast<double>()));
	}
	else if(instance.type() == m_spStringType)
	{
		lua_pushstring(m_pLuaState, instance.cast<String>().c_str());
	}
	else if(instance.type() == m_spRecordType)
	{
		t_lua_record::push(this, instance.cast<Record>());

// TODO restore for RecordAV
#if FALSE //FE_COUNTED_TRACK
		Record& record=instance.cast<Record>();
		if(record.isValid())
		{
			void* data=record.data();
			U32 size=record.layout()->size();

			Counted::registerRegion(data,size,
					"LuaContext:Record "+record.layout()->name());
			Counted::trackReference(data,fe_cast<Counted>(this),
					"LuaContext");

			m_trackArray.push_back(data);
		}
#endif
	}
	else if(instance.type() == m_spGroupType)
	{
		t_lua_group::push(this, instance.cast<sp<RecordGroup> >());
	}
	else if(instance.type() == m_spArrayType)
	{
		t_lua_array::push(this, instance.cast<sp<RecordArray> >());
	}
	else if(instance.type() == m_spComponentType)
	{
		LuaComponentObject::pushComponent(state(),
			instance.cast<sp<Component> >());
	}
	else if(instance.type() == m_spScopeType)
	{
		t_lua_scope::push(this, instance.cast<sp<Scope> >());
	}
	else if(instance.type() == m_spLayoutType)
	{
		t_lua_layout::push(this, instance.cast<sp<Layout> >());
	}
	else if(instance.type() == m_spIntArrayType)
	{
		t_lua_intarray::push(this, &instance.cast< Array<I32> >());
	}
	else if(instance.type() == m_spVector2fType)
	{
		t_lua_vector::push(this, instance.cast<Vector2f>().raw());
	}
	else if(instance.type() == m_spVector2dType)
	{
		t_lua_vector2d::push(this, &instance.cast<Vector2d>());
	}
	else if(instance.type() == m_spVector3fType)
	{
		t_lua_vector3f::push(this, &instance.cast<Vector3f>());
	}
	else if(instance.type() == m_spVector3dType)
	{
		t_lua_vector3d::push(this, &instance.cast<Vector3d>());
	}
	else if(instance.type() == m_spVector4fType)
	{
		t_lua_vector4f::push(this,&instance.cast<Vector4f>());
	}
	else if(instance.type() == m_spVector4dType)
	{
		t_lua_vector4d::push(this,&instance.cast<Vector4d>());
	}
	else if(instance.type() == m_spTransformType)
	{
		t_lua_transform::push(this,&instance.cast<SpatialTransform>());
	}
	else if(instance.type() == m_spStateCatalogType)
	{
		t_lua_statecatalog::push(this,instance.cast< sp<StateCatalog> >());
	}
	else
	{
		sp<TypeMaster> spTM = m_hpMaster->typeMaster();
		String errstr;
		errstr.sPrintf("attempt to push unsupported type \"%s\"",
			spTM->reverseLookup(instance.type()).c_str());
		error(m_pLuaState, errstr.c_str());
	}
}

Instance LuaContext::popCreateInstance(void)
{
	Instance instance;
	int t = lua_type(m_pLuaState, -1);
	if(t == LUA_TNUMBER)
	{
		instance.create<Real>(typeMaster());
		instance.cast<Real>() = (Real)lua_tonumber(m_pLuaState, -1);
		lua_pop(m_pLuaState, 1);
	}
	else if(t == LUA_TSTRING)
	{
		instance.create<String>(typeMaster());
		instance.cast<String>() = (String)lua_tostring(m_pLuaState, -1);
		lua_pop(m_pLuaState, 1);
	}
	else
	{
	}

	return instance;
}

void LuaContext::popInstance(Instance &instance, bool create)
{
	int t = lua_type(m_pLuaState, -1);
	bool mismatch = false;

	if(!instance.type().isValid() && create)
	{
		if(t == LUA_TNUMBER)
		{
			instance.create<double>(typeMaster());
		}

		if(t == LUA_TSTRING)
		{
			instance.create<String>(typeMaster());
		}

	}

	if(instance.type() == m_spBooleanType)
	{
		if(t == LUA_TBOOLEAN)
		{
			instance.cast<bool>() = (bool)lua_toboolean(m_pLuaState, -1);
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spIntegerType)
	{
		if(t == LUA_TNUMBER || t == LUA_TSTRING)
		{
			instance.cast<int>() = (int)lua_tonumber(m_pLuaState, -1);
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spFloatType)
	{
		if(t == LUA_TNUMBER || t == LUA_TSTRING)
		{
			instance.cast<float>() = (float)lua_tonumber(m_pLuaState, -1);
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spDoubleType)
	{
		if(t == LUA_TNUMBER || t == LUA_TSTRING)
		{
			instance.cast<double>() = (double)lua_tonumber(m_pLuaState, -1);
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spStringType)
	{
		if(t == LUA_TNUMBER || t == LUA_TSTRING)
		{
			instance.cast<String>() = lua_tostring(m_pLuaState, -1);
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spRecordType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_record])
			{
				t_lua_record *pLR = (t_lua_record *)pV;
				instance.cast<Record>() = pLR->data();

// TODO restore for RecordAV
#if FALSE //FE_COUNTED_TRACK
				Record& record=instance.cast<Record>();
				if(record.isValid())
				{
					feLog("LuaContext dereg\n");
					Counted::deregisterRegion(record.data());
				}
#endif
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			Record empty;
			instance.cast<Record>() = empty;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spGroupType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_group])
			{
				t_lua_group *pLR = (t_lua_group *)pV;
				instance.cast<sp<RecordGroup> >() = pLR->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<sp<RecordGroup> >() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spArrayType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_array])
			{
				t_lua_array *pLR = (t_lua_array *)pV;
				instance.cast<sp<RecordArray> >() = pLR->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<sp<RecordArray> >() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spComponentType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_component])
			{
				t_lua_component *pLR = (t_lua_component *)pV;
				instance.cast<sp<Component> >() = pLR->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<sp<Component> >() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spScopeType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_scope])
			{
				t_lua_scope *pLR = (t_lua_scope *)pV;
				instance.cast<sp<Scope> >() = pLR->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<sp<Scope> >() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spLayoutType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_layout])
			{
				t_lua_layout *pLR = (t_lua_layout *)pV;
				instance.cast<sp<Layout> >() = pLR->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<sp<Layout> >() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spIntArrayType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_intarray])
			{
				t_lua_intarray *pLR = (t_lua_intarray *)pV;
				instance.cast< Array<I32> >() = *pLR->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast< Array<I32> >().clear();
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spStateCatalogType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_statecatalog])
			{
				t_lua_statecatalog *pLV = (t_lua_statecatalog *)pV;
				instance.cast< sp<StateCatalog> >() = pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast< sp<StateCatalog> >() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spTransformType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_transform])
			{
				t_lua_transform *pLV = (t_lua_transform *)pV;
				instance.cast<SpatialTransform>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<SpatialTransform>() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spVector4dType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_vector4d])
			{
				t_lua_vector4d *pLV = (t_lua_vector4d *)pV;
				instance.cast<Vector4d>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<Vector4d>() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spVector4fType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_vector4f])
			{
				t_lua_vector4f *pLV = (t_lua_vector4f *)pV;
				instance.cast<Vector4f>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<Vector4f>() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spVector3dType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_vector3d])
			{
				t_lua_vector3d *pLV = (t_lua_vector3d *)pV;
				instance.cast<Vector3d>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<Vector3f>() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spVector3fType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_vector3f])
			{
				t_lua_vector3f *pLV = (t_lua_vector3f *)pV;
				instance.cast<Vector3f>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<Vector3f>() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spVector2dType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_vector2d])
			{
				t_lua_vector2d *pLV = (t_lua_vector2d *)pV;
				instance.cast<Vector2d>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<Vector2d>() = NULL;
		}
		else { mismatch = true; }
	}
	else if(instance.type() == m_spVector2fType)
	{
		if(t == LUA_TUSERDATA)
		{
			void *pV = lua_touserdata(m_pLuaState, -1);
			if(((LuaBaseObject *)pV)->type() == m_luaTypes[e_vector2f])
			{
				t_lua_vector2f *pLV = (t_lua_vector2f *)pV;
				instance.cast<Vector2f>() = *pLV->data();
			}
			else { mismatch = true; }
		}
		else if(t == LUA_TNIL)
		{
			instance.cast<Vector2f>() = NULL;
		}
		else { mismatch = true; }
	}
	else
	{
		sp<TypeMaster> spTM = m_hpMaster->typeMaster();
		String errstr;
		errstr.sPrintf("lua->fe attempt to pop unsupported type \"%s\"",
				spTM->reverseLookup(instance.type()).c_str());
		error(m_pLuaState, errstr.c_str());
	}

	if(mismatch)
	{
		sp<TypeMaster> spTM = m_hpMaster->typeMaster();
		String errstr;
		errstr.sPrintf("lua->fe type mismatch \'%s\'",
			spTM->reverseLookup(instance.type()).c_str());
		error(m_pLuaState, errstr.c_str());
	}

	lua_pop(m_pLuaState, 1);
}

int LuaContext::createRecordGroup(lua_State *pLuaState)
{
	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	sp<RecordGroup> spRG(new RecordGroup());
	t_lua_group::push(pContext, spRG);

	return 1;
}

int LuaContext::createRecord(lua_State *pLuaState)
{
	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	Record record;
	t_lua_record::push(pContext, record);

	return 1;
}

int LuaContext::createVector3(lua_State *pLuaState)
{
	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	//* WARNING leak
	Vector3f* pVector3f=new Vector3f(-1,-1,-1);

	int argc = lua_gettop(pLuaState);
	for(int i = 0; i < argc && i < 3; i++)
	{
		(*pVector3f)[i] = lua_tonumber(pLuaState, i+1);
	}

	t_lua_vector3f::push(pContext, pVector3f, TRUE);

	return 1;
}

int LuaContext::createRecordArray(lua_State *pLuaState)
{
	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	sp<RecordArray> spRA(new RecordArray());
	t_lua_array::push(pContext, spRA);

	return 1;
}

int LuaContext::createScope(lua_State *pLuaState)
{
	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	sp<Scope> spScope(new Scope(*(pContext->master())));
	t_lua_scope::push(pContext, spScope);

	return 1;
}

int LuaContext::createScopeComponent(lua_State *pLuaState)
{
	t_lua_scope *pObj = (t_lua_scope *)lua_touserdata(pLuaState, -1);
	if(!pObj)
	{
		return 0;
	}
	lua_pop(pLuaState, 1);

	lua_getglobal(pLuaState, "fe_context");
//	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	sp<Component> spComponent = pObj->data();
	if(!spComponent.isValid())
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaComponentObject::pushComponent(pLuaState,spComponent);

	return 1;
}

void LuaContext::error(lua_State *pLuaState, const char *message)
{
	lua_pushstring(pLuaState, message);
	lua_error(pLuaState);
}

int LuaContext::debug(lua_State *pLuaState)
{
	String err = "[31m[1mLua execution failed: ";
	err.cat(lua_tostring(pLuaState, -1));
	err.cat("[0m\n");

	lua_pop(pLuaState, 1);

	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	const I32 preamble=pContext->m_preamble;

	int level = 0;
	lua_Debug ar;

	String message;
	while(lua_getstack(pLuaState, level, &ar))
	{
		lua_getinfo(pLuaState, "nSl", &ar);

		// name
		String name = "---";
		if(ar.name) { name = ar.name; }

		// namewhat
		String namewhat = "unknown";
		if(ar.namewhat) { namewhat = ar.namewhat; }

		// source
		String source = ar.source;

		// what
		String what = ar.what;

		if(ar.currentline >= 0)
		{
			message.sPrintf(
				"lua: [35m%-45s [0mline:[33m%5d [34m%-10s[0m",
				name.c_str(),
				ar.currentline-preamble,
				what.c_str()
				);
			err.cat(message);
			err.cat("\n");
		}

		const I32 linesAside(3);
		I32 lineNumber=1;
		String buffer=source;
		while(!buffer.empty())
		{
			const String line=buffer.parse("\"","\n",TRUE);

			if(abs(lineNumber-ar.currentline)<=linesAside && line!="=[C]")
			{
				const BWORD highlight=(lineNumber==ar.currentline);

				message.sPrintf("%s%-8d%s%s\n",
						highlight? "[33m": "[36m",
						lineNumber-preamble,line.c_str(),
						highlight? "[0m": "");
				err.cat(message);
			}
			lineNumber++;
		}

		level += 1;
	}

	err=err.substitute("%","%%");

	lua_pushstring(pLuaState, err.c_str());
	return 1;
}

int LuaContext::manage(lua_State *pLuaState)
{
	String libname = lua_tostring(pLuaState, -1);
	lua_pop(pLuaState, 1);

	lua_getglobal(pLuaState, "fe_master");
	sp<Master> spMaster((Master *)(lua_touserdata(pLuaState,-1)));

	if(!successful(spMaster->registry()->manage(libname)))
	{
		lua_pushboolean(pLuaState, 0);
		return 1;
	}
	lua_pushboolean(pLuaState, 1);
	return 1;
}

int LuaContext::create(lua_State *pLuaState)
{
	String componentName = lua_tostring(pLuaState, -1);
	lua_pop(pLuaState, 1);
	if(componentName == "")
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	sp<Component> spComponent =
		pContext->master()->registry()->create(componentName);
	if(!spComponent.isValid())
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaComponentObject::pushComponent(pLuaState,spComponent);

	return 1;
}

int LuaContext::catalog(lua_State *pLuaState)
{
	String interface;
	sp<Component> spC;
	if(lua_isstring(pLuaState, 1))
	{
		interface = lua_tostring(pLuaState, 1);
		if(interface == "")
		{
			lua_pushnil(pLuaState);
			return 1;
		}
	}
	else
	{
		spC = component<Component>(pLuaState, 1);
		if(!spC.isValid())
		{
			lua_pushnil(pLuaState);
			return 1;
		}
	}

	String label = lua_tostring(pLuaState, 2);
	if(label == "")
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	lua_getglobal(pLuaState, "fe_context");
	LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));

	sp<Component> spComponent;
	if(!spC.isValid())
	{
		spComponent = pContext->master()->catalog()->catalogComponent(
				interface, label);
	}
	else
	{
		spComponent = spC;
		pContext->master()->catalog()->catalogComponent(spC, label);
	}
	if(!spComponent.isValid())
	{
		lua_pushnil(pLuaState);
		return 1;
	}

	LuaComponentObject::pushComponent(pLuaState,spComponent);

	return 1;
}

int LuaContext::b_or(lua_State *pLuaState)
{
	FE_UWORD val = 0;
	int argc = lua_gettop(pLuaState);
	for(int i = 0; i < argc; i++)
	{
		val |= (int)lua_tonumber(pLuaState, i+1);
	}
	lua_pushnumber(pLuaState, val);
	return 1;
}

int LuaContext::b_and(lua_State *pLuaState)
{
	FE_UWORD val = ~0;
	int argc = lua_gettop(pLuaState);
	for(int i = 0; i < argc; i++)
	{
		val &= (int)lua_tonumber(pLuaState, i+1);
	}
	lua_pushnumber(pLuaState, val);
	return 1;
}

#define FEWEV FE_EVENT_LAYOUT

void LuaContext::setWindowEventEnums(void)
{
	set(FEWEV,"sourceNull",				WindowEvent::e_sourceNull);
	set(FEWEV,"sourceMousePosition",	WindowEvent::e_sourceMousePosition);
	set(FEWEV,"sourceMouseButton",		WindowEvent::e_sourceMouseButton);
	set(FEWEV,"sourceKeyboard",			WindowEvent::e_sourceKeyboard);
	set(FEWEV,"sourceSystem",			WindowEvent::e_sourceSystem);
	set(FEWEV,"sourceUser",				WindowEvent::e_sourceUser);
	set(FEWEV,"sourceAny",				WindowEvent::e_sourceAny);

	set(FEWEV,"itemNull",				WindowEvent::e_itemNull);
	set(FEWEV,"itemAny",				WindowEvent::e_itemAny);

	set(FEWEV,"itemMove",				WindowEvent::e_itemMove);
	set(FEWEV,"itemDrag",				WindowEvent::e_itemDrag);

	set(FEWEV,"itemLeft",				WindowEvent::e_itemLeft);
	set(FEWEV,"itemMiddle",				WindowEvent::e_itemMiddle);
	set(FEWEV,"itemRight",				WindowEvent::e_itemRight);
	set(FEWEV,"itemWheel",				WindowEvent::e_itemWheel);

	set(FEWEV,"keyBackspace",			WindowEvent::e_keyBackspace);
	set(FEWEV,"keyEscape",				WindowEvent::e_keyEscape);
	set(FEWEV,"keyDelete",				WindowEvent::e_keyDelete);
	set(FEWEV,"keyAsciiMask",			WindowEvent::e_keyAsciiMask);
	set(FEWEV,"keyCursor",				WindowEvent::e_keyCursor);
	set(FEWEV,"keyCursorUp",			WindowEvent::e_keyCursorUp);
	set(FEWEV,"keyCursorDown",			WindowEvent::e_keyCursorDown);
	set(FEWEV,"keyCursorLeft",			WindowEvent::e_keyCursorLeft);
	set(FEWEV,"keyCursorRight",			WindowEvent::e_keyCursorRight);
	set(FEWEV,"keyCursorMask",			WindowEvent::e_keyCursorMask);
	set(FEWEV,"keyShift",				WindowEvent::e_keyShift);
	set(FEWEV,"keyCapLock",				WindowEvent::e_keyCapLock);
	set(FEWEV,"keyControl",				WindowEvent::e_keyControl);
	set(FEWEV,"keyUsed",				WindowEvent::e_keyUsed);

	set(FEWEV,"itemIdle",				WindowEvent::e_itemIdle);
	set(FEWEV,"itemResize",				WindowEvent::e_itemResize);
	set(FEWEV,"itemExpose",				WindowEvent::e_itemExpose);
	set(FEWEV,"itemCloseWindow",		WindowEvent::e_itemCloseWindow);
	set(FEWEV,"itemQuit",				WindowEvent::e_itemQuit);

	set(FEWEV,"windowClear",			WindowEvent::e_windowClear);
	set(FEWEV,"windowSwapBuffers",		WindowEvent::e_windowSwapBuffers);

	set(FEWEV,"stateNull",				WindowEvent::e_stateNull);
	set(FEWEV,"statePress",				WindowEvent::e_statePress);
	set(FEWEV,"stateRelease",			WindowEvent::e_stateRelease);
	set(FEWEV,"stateRepeat",			WindowEvent::e_stateRepeat);
	set(FEWEV,"stateAny",				WindowEvent::e_stateAny);

	set(FEWEV,"mbNone",					WindowEvent::e_mbNone);
	set(FEWEV,"mbLeft",					WindowEvent::e_mbLeft);
	set(FEWEV,"mbMiddle",				WindowEvent::e_mbMiddle);
	set(FEWEV,"mbRight",				WindowEvent::e_mbRight);
	set(FEWEV,"mbAll",					WindowEvent::e_mbAll);
	set(FEWEV,"mbAny",					WindowEvent::e_mbAny);
}

} /* namespace ext */
} /* namespace fe */
