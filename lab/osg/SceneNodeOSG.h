/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __osg_SceneNodeOSG_h__
#define __osg_SceneNodeOSG_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Object hierarchy using OpenSceneGraph

	@ingroup osg

	http://www.openscenegraph.org
*//***************************************************************************/
class SceneNodeOSG: virtual public SceneNodeI
{
	public:
						SceneNodeOSG(void);
virtual					~SceneNodeOSG(void);

						//* As SceneI
virtual	BWORD			load(String a_filename);
virtual	U32				childCount(void) const;
virtual	sp<SceneNodeI>	child(U32 a_index) const;

virtual	sp<SceneNodeI>	createChild(void);
virtual	BWORD			append(sp<SceneNodeI> a_spChild);
virtual	BWORD			remove(sp<SceneNodeI> a_spChild);

		osg::ref_ptr<osg::Node>	node(void)	{ return m_refNode; }

	private:
		osg::ref_ptr<osg::Node>		m_refNode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __openil_SceneNodeOSG_h__ */
