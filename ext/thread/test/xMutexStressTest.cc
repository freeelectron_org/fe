/** @file */

#include "fe/data.h"
#include <chrono>
#include <mutex>

using namespace fe;

#define SLEEP_MILLISECONDS		1
#define TEST_DURATION_SECONDS	1

#define MIN_THREADS				3
#define MAX_THREADS				11

std::mutex gs_mutex;

/**
 * Task and Thread container.
 */
template<class Task>
struct TaskThread
{
	Task* m_task;
	Thread* m_thread;

	TaskThread()
	{
		m_task = new Task;
		m_thread = new Thread(m_task);
	};

	TaskThread(const TaskThread& other)
	{
		m_task = other.m_task;
		m_thread = other.m_thread;
	};

	void stop()
	{
		m_task->m_poison.start();
		m_thread->join();
		delete m_thread;
	};
};

/**
 * Thread task that rapidly acquires a mutex lock.
 *
 * This task is designed to test the thread integrity mutexes.
 */
class MutexSmashingTask: public Thread::Functor
{
public:
	void operate(void) override
	{
		// Thread loop.
		while(!m_poison.active())
		{
			gs_mutex.lock();

			milliSleep(SLEEP_MILLISECONDS);

			gs_mutex.unlock();

			Thread::interruptionPoint();
		}

		m_poison.stop();
	};

	/** Poison to kill the thread */
	Poison m_poison;
};

/**
 * This thread task sweeps the number of MutexSmashingTask threads
 * up and down.
 */
class ThreadSweepingTask: public Thread::Functor
{
public:
	void operate(void) override
	{
		m_threads.reserve(MAX_THREADS);

		// We start off with 3 threads.
		for(unsigned int i = 0; i < 3; i++)
		{
			m_threads.emplace_back();
		}

		// Thread loop.
		while(!m_poison.active())
		{
			milliSleep(50);

			if(m_rateOfChange > 0)
			{
				m_threads.emplace_back();
			}
			else
			{
				m_threads.front().stop();
				m_threads.erase(m_threads.begin());
			}

			if(m_threads.size() >= MAX_THREADS ||
					m_threads.size() <= MIN_THREADS)
			{
				m_rateOfChange *= -1;
			}

			feLog("Thread count: %u\n", m_threads.size());
		}

		m_poison.stop();

		for(auto thread: m_threads)
		{
			thread.stop();
		}
	};

	/** Poison to kill the thread */
	Poison m_poison;

protected:
	std::vector< TaskThread<MutexSmashingTask> > m_threads;

	int m_rateOfChange = 1;
};

int main(int argc, char **argv)
{
	UnitTest unitTest;

    sp<Master> spMaster(new Master);
	sp<Registry> spRegistry = spMaster->registry();

    spRegistry->manage("feAutoLoadDL");
    spRegistry->manage("fexStdThread");

	try
	{
		unitTest(Mutex::confirm("fexStdThread"));

		// Start thread sweeper.
		TaskThread<ThreadSweepingTask> thread;

		std::chrono::steady_clock::time_point startTime =
				std::chrono::steady_clock::now();
		while(std::chrono::steady_clock::now() - startTime <
				std::chrono::seconds{TEST_DURATION_SECONDS})
		{
			gs_mutex.lock();
			feLog("Main thread acquired lock\n");

			unitTest(true);

			gs_mutex.unlock();
			feLog("Main thread released lock\n");

			milliSleep(100);
		}

		// Kill thread sweeper.
		thread.stop();
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	return unitTest.failures();
}
