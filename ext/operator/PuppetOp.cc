/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_PPO_DEBUG		FALSE
#define FE_PPO_ARRAY_DEBUG	FALSE

#define FE_PPO_OFFSET		FALSE

using namespace fe;
using namespace fe::ext;

void PuppetOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* TODO
	catalogRemove("IdAttr");

	//* page Edit

	catalog<Real>("envelope")=1.0;
	catalog<String>("envelope","label")="Envelope";
	catalog<String>("envelope","page")="Edit";

	//* pose is alternative reference
	catalog<bool>("poseJoints")=true;
	catalog<String>("poseJoints","label")="Use Custom Rest Pose";
	catalog<String>("poseJoints","page")="Edit";
	catalog<bool>("poseJoints","joined")=true;

	//* how much the pose changes the reference
	catalog<Real>("poseRatio")=1.0;
	catalog<String>("poseRatio","label")="Repose Applied";
	catalog<String>("poseRatio","enabler")="poseJoints";
	catalog<String>("poseRatio","page")="Edit";

	catalog<bool>("pinRootPos")=true;
	catalog<String>("pinRootPos","label")="Pin Root Position";
	catalog<String>("pinRootPos","page")="Edit";
	catalog<bool>("pinRootPos","joined")=true;

	catalog<bool>("pinRootRot")=true;
	catalog<String>("pinRootRot","label")="Pin Root Rotation";
	catalog<String>("pinRootRot","page")="Edit";

	catalog<bool>("bendJoints")=false;
	catalog<String>("bendJoints","label")="Allow Bending";
	catalog<String>("bendJoints","page")="Edit";
	catalog<bool>("bendJoints","joined")=true;

	catalog<Real>("bendRatio")=1.0;
	catalog<String>("bendRatio","label")="Bend Applied";
	catalog<String>("bendRatio","enabler")="bendJoints";
	catalog<String>("bendRatio","page")="Edit";

	catalog<bool>("resizeJoints")=true;
	catalog<String>("resizeJoints","label")="Allow Resizing";
	catalog<String>("resizeJoints","page")="Edit";
	catalog<bool>("resizeJoints","joined")=true;

	catalog<Real>("resizeRatio")=1.0;
	catalog<String>("resizeRatio","label")="Resize Applied";
	catalog<String>("resizeRatio","enabler")="resizeJoints";
	catalog<String>("resizeRatio","page")="Edit";

	catalog<bool>("translationControl")=false;
	catalog<String>("translationControl","label")=
			"Independent Translation Controls";
	catalog<String>("translationControl","page")="Edit";

	catalog<bool>("shiftJoints")=false;
	catalog<String>("shiftJoints","label")="Allow Shifting";
	catalog<String>("shiftJoints","enabler")="translationControl";
	catalog<String>("shiftJoints","page")="Edit";
	catalog<bool>("shiftJoints","joined")=true;

	catalog<Real>("shiftRatio")=1.0;
	catalog<String>("shiftRatio","label")="Shift Applied";
	catalog<String>("shiftRatio","enabler")=
			"translationControl && shiftJoints";
	catalog<String>("shiftRatio","page")="Edit";

	catalog<bool>("stretchJoints")=true;
	catalog<String>("stretchJoints","label")="Allow Stretching";
	catalog<String>("stretchJoints","enabler")="translationControl";
	catalog<String>("stretchJoints","page")="Edit";
	catalog<bool>("stretchJoints","joined")=true;

	catalog<Real>("stretchRatio")=1.0;
	catalog<String>("stretchRatio","label")="Stretch Applied";
	catalog<String>("stretchRatio","enabler")=
			"translationControl && stretchJoints";
	catalog<String>("stretchRatio","page")="Edit";

#if FE_PPO_OFFSET
	catalog<bool>("offsetJoints")=false;
	catalog<String>("offsetJoints","label")="Offset";
	catalog<String>("offsetJoints","page")="Edit";
	catalog<bool>("offsetJoints","joined")=true;

	catalog<Real>("offsetRatio")=1.0;
	catalog<String>("offsetRatio","label")="Scale";
	catalog<String>("offsetRatio","enabler")="offsetJoints";
	catalog<String>("offsetRatio","page")="Edit";
#endif

	//* page Display

	catalog<String>("drawForm")="pyramid";
	catalog<String>("drawForm","label")="Form";
	catalog<String>("drawForm","choice:0")="pyramid";
	catalog<String>("drawForm","label:0")="Pyramid Lines";
	catalog<String>("drawForm","choice:1")="teardrop";
	catalog<String>("drawForm","label:1")="Tear Drop";
	catalog<String>("drawForm","choice:2")="bone";
	catalog<String>("drawForm","label:2")="Bone";
	catalog<String>("drawForm","page")="Display";
	catalog<bool>("drawForm","joined")=true;

	catalog<Real>("drawRadius")=1.0;
	catalog<Real>("drawRadius","high")=1.0;
	catalog<Real>("drawRadius","max")=1e6;
	catalog<String>("drawRadius","label")="Radius";
	catalog<String>("drawRadius","page")="Display";

	catalog<bool>("drawNames")=false;
	catalog<String>("drawNames","label")="Draw Names";
	catalog<String>("drawNames","page")="Display";

	catalog<SpatialVector>("jointColor")=SpatialVector(0.0,1.0,0.0);
	catalog<String>("jointColor","label")="Default Color";
	catalog<String>("jointColor","suggest")="color";
	catalog<String>("jointColor","page")="Display";

	catalog<bool>("recolor")=false;
	catalog<String>("recolor","label")="Replace Color";
	catalog<bool>("recolor","joined")=true;
	catalog<String>("recolor","page")="Display";

	catalog<bool>("enhanceColor")=false;
	catalog<String>("enhanceColor","label")="Enhance Color";
	catalog<String>("enhanceColor","page")="Display";

	catalog<String>("pickName");
	catalog<String>("pickName","page")="Dynamic";
	catalog<String>("pickName","IO")="output";

	catalog< Array<String> >("jointName");
	catalog<String>("jointName","page")="Dynamic";
	catalog<String>("jointName","IO")="output";
	catalog<bool>("jointName","hidden")=true;

	catalog< Array<SpatialVector> >("jointShift");
	catalog<String>("jointShift","page")="Dynamic";
	catalog<String>("jointShift","IO")="input output";
	catalog<bool>("jointShift","hidden")=true;

	catalog< Array<SpatialVector> >("jointStretch");
	catalog<String>("jointStretch","page")="Dynamic";
	catalog<String>("jointStretch","IO")="input output";
	catalog<bool>("jointStretch","hidden")=true;

	catalog< Array<SpatialVector> >("jointBend");
	catalog<String>("jointBend","page")="Dynamic";
	catalog<String>("jointBend","IO")="input output";
	catalog<bool>("jointBend","hidden")=true;

	catalog< Array<SpatialVector> >("jointResize");
	catalog<String>("jointResize","page")="Dynamic";
	catalog<String>("jointResize","IO")="input output";
	catalog<bool>("jointResize","hidden")=true;

	catalog< Array<SpatialVector> >("jointRescale");
	catalog<String>("jointRescale","page")="Dynamic";
	catalog<String>("jointRescale","IO")="input output";
	catalog<bool>("jointRescale","hidden")=true;

	catalog< Array<SpatialVector> >("jointForward");
	catalog<String>("jointForward","page")="Dynamic";
	catalog<String>("jointForward","IO")="input output";
	catalog<bool>("jointForward","hidden")=true;

	catalog< Array<SpatialVector> >("jointBackward");
	catalog<String>("jointBackward","page")="Dynamic";
	catalog<String>("jointBackward","IO")="input output";
	catalog<bool>("jointBackward","hidden")=true;

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")=
			"LMB joint to select."
			"  LMB bar to adjust."
			"  ESC deselects."
			"  Enter shows all."
			"  DEL resets joint (all reset if all shown).";

	catalog< sp<Component> >("Input Joints");

	catalog< sp<Component> >("Pose Joints");
	catalog<bool>("Pose Joints","optional")=true;

	catalog< sp<Component> >("Offset Joints");
	catalog<bool>("Offset Joints","optional")=true;

	catalog<String>("Output Surface","copy")="Input Joints";

	m_spWireframe=new DrawMode();
//	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setDrawStyle(DrawMode::e_foreshadow);

	m_spThick=new DrawMode();
	m_spThick->copy(m_spWireframe);
	m_spThick->setLineWidth(2.0);

	m_spSolid=new DrawMode();
	m_spSolid->copy(m_spWireframe);
	m_spSolid->setDrawStyle(DrawMode::e_foreshadow);
	m_spSolid->setLit(FALSE);
	m_spSolid->setBackfaceCulling(FALSE);

	m_spOverlay=new DrawMode();
	m_spOverlay->copy(m_spWireframe);
	m_spOverlay->setDrawStyle(DrawMode::e_solid);
	m_spOverlay->setLit(FALSE);
	m_spOverlay->setBackfaceCulling(FALSE);

	m_spManipulator=registry()->create("*.ValueManipulator");
	if(m_spManipulator.isValid())
	{
		sp<Catalog> spCatalog=m_spManipulator;
		if(spCatalog.isNull())
		{
			m_spManipulator=NULL;
		}
		else
		{
			const Color red(1.0,0.0,0.0);
			const Color green(0.0,1.0,0.0);
			const Color blue(0.0,0.0,1.0);
			const Color grey(0.5,0.5,0.5);

			addParam("shiftX","Shift X",1.0,0.0,1.0,red);
			addParam("shiftY","Shift Y",1.0,0.0,1.0,green);
			addParam("shiftZ","Shift Z",1.0,0.0,1.0,blue);

			addParam("stretchX","Stretch X",1.0,0.0,1.0,red);
			addParam("stretchY","Stretch Y",1.0,0.0,1.0,green);
			addParam("stretchZ","Stretch Z",1.0,0.0,1.0,blue);

			addParam("bendX","Bend X",1.0,0.0,1.0,red);
			addParam("bendY","Bend Y",1.0,0.0,1.0,green);
			addParam("bendZ","Bend Z",1.0,0.0,1.0,blue);

			addParam("resizeX","Resize X",1.0,0.0,1.0,red);
			addParam("resizeY","Resize Y",1.0,0.0,1.0,green);
			addParam("resizeZ","Resize Z",1.0,0.0,1.0,blue);

			addParam("rescaleX","Rescale X",1.0,0.0,1.0,red);
			addParam("rescaleY","Rescale Y",1.0,0.0,1.0,green);
			addParam("rescaleZ","Rescale Z",1.0,0.0,1.0,blue);

			addParam("forwardX","Forward X",180.0,0.0,180.0,red);
			addParam("backwardX","Backward X",180.0,0.0,180.0,red);

			addParam("forwardY","Forward Y",180.0,0.0,180.0,green);
			addParam("backwardY","Backward Y",180.0,0.0,180.0,green);

			addParam("forwardZ","Forward Z",180.0,0.0,180.0,blue);
			addParam("backwardZ","Backward Z",180.0,0.0,180.0,blue);

			spCatalog->catalog<bool>("gripLabels")=TRUE;
			spCatalog->catalog<Real>("dimming")=0.5;
		}
	}
}

void PuppetOp::addParam(String a_key,String a_label,Real a_value,
	Real a_min,Real a_max,Color a_color)
{
	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	spCatalog->catalog<Real>(a_key)=a_value;
	spCatalog->catalog<Real>(a_key,"min")=a_min;
	spCatalog->catalog<Real>(a_key,"max")=a_max;
	spCatalog->catalog<String>(a_key,"label")=a_label;
	spCatalog->catalog<SpatialVector>(a_key,"color")=a_color;
}

void PuppetOp::handle(Record& a_rSignal)
{
#if FE_PPO_DEBUG
	feLog("PuppetOp::handle\n");
#endif

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Joints")) return;

	sp<SurfaceAccessorI> spInputName;
	if(!access(spInputName,spInputAccessible,e_primitive,"name")) return;

	sp<SurfaceAccessorI> spInputParent;
	if(!access(spInputParent,spInputAccessible,e_primitive,"parent")) return;

	sp<SurfaceAccessorI> spInputColor;
	if(!access(spInputColor,spInputAccessible,e_point,e_color)) return;

	const U32 primitiveCount=spInputName->count();

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

#if FE_PPO_DEBUG
	feLog("PuppetOp::handle brush %d brushed %d\n",
			spDrawBrush.isValid(),m_brushed);
#endif

	if(spDrawBrush.isValid())
	{
		sp<SurfaceAccessorI> spOutputVertices;
		if(m_spOutputAccessible.isNull() ||
				!access(spOutputVertices,m_spOutputAccessible,
				e_primitive,e_vertices)) return;

		spDrawBrush->pushDrawMode(m_spWireframe);
		spDrawOverlay->pushDrawMode(m_spOverlay);

		sp<ViewI> spView=spDrawOverlay->view();

		m_brushed=TRUE;

		const Color black(0.0,0.0,0.0);
		const Color white(1.0,1.0,0.8);
		const Color grey(0.8,0.8,1.0);
		const Color red(0.8,0.0,0.0);
		const Color green(0.0,0.5,0.0);
		const Color blue(0.0,0.0,1.0);
		const Color cyan(0.0,1.0,1.0);
		const Color magenta(0.8,0.0,0.8);
		const Color yellow(1.0,1.0,0.0);
		const Color sparkle(1.0,1.0,0.6);
		const Color orange(1.0,0.5,0.0);
		const Color blank(0.0,0.0,0.0,0.0);

		m_event.bind(windowEvent(a_rSignal));

#if FE_PPO_DEBUG
		feLog("%s\n",c_print(m_event));
#endif

		if(m_event.isKeyPress(WindowEvent::e_keyCarriageReturn))
		{
			m_showAll=!m_showAll;
		}

		if(m_event.isKeyPress(WindowEvent::e_keyEscape))
		{
			m_pickName="";
		}

		if(m_event.isKeyPress(WindowEvent::e_keyDelete))
		{
			if(m_showAll)
			{
				deleteJoints();
			}
			else
			{
				deleteJoint(m_pickName);
			}

			m_pickName="";
			m_showAll=FALSE;
		}

		const Vector2i mouse(m_event.mouseX(),m_event.mouseY());

		std::map< String, Array<String> > childMap;
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const String jointName=spInputName->string(primitiveIndex);
			const String parentName=spInputParent->string(primitiveIndex);

			childMap[parentName].push_back(jointName);
		}

		const Real nearForward=64.0;
		const Real nearForward2=nearForward*nearForward;

		Real nearDist2= -1.0;
		String nearName;

		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
				primitiveIndex++)
		{
			const String jointName=spInputName->string(primitiveIndex);

			Array<String>& rChildList=childMap[jointName];
			const U32 childCount=rChildList.size();

			const U32 checkCount=childCount? childCount: 1;
			for(U32 checkIndex=0;checkIndex<checkCount;checkIndex++)
			{
				const String otherName=
						childCount? rChildList[checkIndex]: jointName;

				const SpatialTransform& xform=m_animMap[jointName];
				const SpatialTransform& xformOther=m_animMap[otherName];

				const SpatialVector midpoint=
						0.5*(xform.translation()+xformOther.translation());

				const Vector2i projected=
						spView->project(midpoint,ViewI::e_perspective);

				const Real dist2=magnitudeSquared(projected-mouse);
				if(dist2>nearForward2)
				{
					continue;
				}

				if(nearDist2<0.0 || nearDist2>dist2)
				{
					nearDist2=dist2;
					nearName=jointName;
				}
			}
		}

		if(!m_dragging && m_event.isMouseRelease(WindowEvent::e_itemLeft))
		{
			m_pickName=nearName;

			if(m_spManipulator.isValid())
			{
				sp<Catalog> spCatalog=m_spManipulator;
				FEASSERT(spCatalog.isValid());

				Record jointRecord=lookupJoint(m_pickName);

				spCatalog->catalog<String>("selection")=m_pickName;
				spCatalog->catalog<bool>("show")=true;

				SpatialVector shift=m_aShift(jointRecord);
				SpatialVector stretch=m_aStretch(jointRecord);
				SpatialVector bend=m_aBend(jointRecord);
				SpatialVector resize=m_aBend(jointRecord);
				SpatialVector rescale=m_aRescale(jointRecord);
				SpatialVector forward=m_aForward(jointRecord);
				SpatialVector backward=m_aBackward(jointRecord);

				spCatalog->catalog<Real>("shiftX")=shift[0];
				spCatalog->catalog<Real>("shiftY")=shift[1];
				spCatalog->catalog<Real>("shiftZ")=shift[2];

				spCatalog->catalog<Real>("stretchX")=stretch[0];
				spCatalog->catalog<Real>("stretchY")=stretch[1];
				spCatalog->catalog<Real>("stretchZ")=stretch[2];

				spCatalog->catalog<Real>("bendX")=bend[0];
				spCatalog->catalog<Real>("bendY")=bend[1];
				spCatalog->catalog<Real>("bendZ")=bend[2];

				spCatalog->catalog<Real>("resizeX")=resize[0];
				spCatalog->catalog<Real>("resizeY")=resize[1];
				spCatalog->catalog<Real>("resizeZ")=resize[2];

				spCatalog->catalog<Real>("rescaleX")=rescale[0];
				spCatalog->catalog<Real>("rescaleY")=rescale[1];
				spCatalog->catalog<Real>("rescaleZ")=rescale[2];

				spCatalog->catalog<Real>("forwardX")=forward[0];
				spCatalog->catalog<Real>("forwardY")=forward[1];
				spCatalog->catalog<Real>("forwardZ")=forward[2];

				spCatalog->catalog<Real>("backwardX")=backward[0];
				spCatalog->catalog<Real>("backwardY")=backward[1];
				spCatalog->catalog<Real>("backwardZ")=backward[2];
			}
		}

		m_dragging=(m_event.isMouseDrag() && m_event.mouseButtons());

		const bool recolor=catalog<bool>("recolor");
		const bool enhanceColor=catalog<bool>("enhanceColor");
		const Color jointColor=catalog<SpatialVector>("jointColor");
		const Real drawRadius=catalog<Real>("drawRadius");

		const U32 resolution=16;

		SpatialVector pointArray[resolution];
		SpatialVector normalArray[resolution];
		Real radiusArray[resolution];
		Color colorArray[resolution];

		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const String jointName=spInputName->string(primitiveIndex);

			Color color[3];
			color[0]=jointColor;

			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount && !recolor)
			{
				const I32 pointIndex0=
						spOutputVertices->integer(primitiveIndex,0);

				color[0]=spInputColor->spatialVector(pointIndex0);
			}

			color[1]=enhanceColor? red: color[0];
			color[2]=enhanceColor? blue: color[0];

			const BWORD bright=(jointName==m_pickName) ||
					(!m_dragging && jointName==nearName);

			color[0]=makeColor(color[0],bright);
			color[1]=makeColor(color[1],bright);
			color[2]=makeColor(color[2],bright);

			const SpatialVector point0=m_animMap[jointName].translation();
			const SpatialVector point1=point0+
					drawRadius*m_animMap[jointName].column(0);
			const SpatialVector point2=point0+
					drawRadius*m_animMap[jointName].column(2);
			const SpatialVector norm=m_animMap[jointName].column(1);

			SpatialVector corner[5];
			corner[0]=point1;
			corner[1]=2.0*point0-point2;
			corner[2]=2.0*point0-point1;
			corner[3]=point2;
			corner[4]=point1;

			SpatialVector midpoint(0.0,0.0,0.0);
			SpatialVector endpoint(0.0,0.0,0.0);

			Array<String>& rChildList=childMap[jointName];
			const U32 childCount=rChildList.size();
			for(U32 childIndex=0;childIndex<childCount;childIndex++)
			{
				const String childName=rChildList[childIndex];

				const SpatialTransform& xformChild=m_animMap[childName];

				const SpatialVector& childPoint0=xformChild.translation();

				midpoint+=0.5*(childPoint0+point0);
				endpoint+=childPoint0;

				drawJoint(spDrawBrush,childPoint0,corner,color);
			}

			if(childCount)
			{
				const Real fraction=1.0/Real(childCount);
				midpoint*=fraction;
				endpoint*=fraction;
			}
			else
			{
				Real length=0.0;
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=spOutputVertices->spatialVector(
							primitiveIndex,subIndex);
					const Real along=dot(norm,point-point0);
					if(length<along)
					{
						length=along;
					}
				}

				const SpatialVector segment=length*norm;
				midpoint=point0+0.5*segment;
				endpoint=point0+segment;

				drawJoint(spDrawBrush,endpoint,corner,color);
			}

			const BWORD drawNames=catalog<bool>("drawNames");

			if(drawNames)
			{
				const U32 margin=1;
				const Vector2i projected=
						spView->project(midpoint,ViewI::e_perspective);
				drawLabel(spDrawOverlay,projected,"  "+jointName,
						FALSE,margin,white,NULL,&blue);
			}

			SpatialVector line[2];

			line[0]=point0;
			line[1]=endpoint;

			spDrawBrush->pushDrawMode(m_spThick);

			const Real dotRadius=6;
			if(m_showAll || jointName==m_pickName)
			{
				if(jointName==m_pickName)
				{
					drawDot(spView,spDrawOverlay,midpoint,dotRadius,yellow);

					if(childCount<2)
					{
						spDrawBrush->drawLines(line,NULL,2,DrawI::e_strip,
								false,&yellow);
					}
				}

				spDrawBrush->pushDrawMode(m_spSolid);

				Record jointRecord=lookupJoint(jointName);

				const SpatialTransform& xform=m_animMap[jointName];

				for(U32 axis=0;axis<3;axis++)
				{
					const Real jointBend=lookupBend(jointRecord,axis);
					const Real jointForward=
							lookupForward(jointRecord,axis)*degToRad;
					const Real jointBackward=
							-lookupBackward(jointRecord,axis)*degToRad;

					//* TODO draw stretch, resize, rescale
//					const Real jointStretch=lookupStretch(jointRecord,axis);
//					const Real jointResize=
//							lookupResize(jointRecord,axis);
//					const Real jointRescale=
//							lookupRescale(jointRecord,axis);

					for(U32 pass=0;pass<2;pass++)
					{
						const Real freescale=pass? jointBend: 1.0;

						for(U32 m=0;m<resolution;m++)
						{
							const Real fraction=m/(resolution-1.0);

							const Real start=jointBackward*freescale;
							const Real length=
									(jointForward-jointBackward)*freescale;

							SpatialTransform spun=xform;
							rotate(spun,(start+length*fraction),Axis(axis));

							set(pointArray[m]);
							pointArray[m][(axis==1)? 0: 1]=4.0;
							transformVector(spun,pointArray[m],pointArray[m]);

							set(normalArray[m]);
							normalArray[m][axis]=1.0;
							transformVector(spun,normalArray[m],normalArray[m]);

							radiusArray[m]=pass? 0.2: 0.1;

							set(colorArray[m]);
							colorArray[m][axis]=pass? 1.0: 0.5;
							colorArray[m][3]=0.3;
						}

						const BWORD multicolor=TRUE;
						const OperateCommon::CurveMode curveMode=
								OperateCommon::e_tube;
						const SpatialVector towardCamera(0.0,0.0,1.0);

						drawTube(spDrawBrush,pointArray,normalArray,radiusArray,
								resolution,curveMode,multicolor,colorArray,
								towardCamera);
					}
				}

				spDrawBrush->popDrawMode();
			}
			else if(!m_dragging && jointName==nearName)
			{
				drawDot(spView,spDrawOverlay,midpoint,dotRadius,cyan);

				if(childCount<2)
				{
					spDrawBrush->drawLines(line,NULL,2,DrawI::e_strip,
							false,&cyan);
				}
			}
			else if(drawNames)
			{
				drawDot(spView,spDrawOverlay,midpoint,dotRadius,magenta);
			}

			spDrawBrush->popDrawMode();
		}

		if(m_pickName.empty())
		{
			if(m_spManipulator.isValid())
			{
				sp<Catalog> spCatalog=m_spManipulator;
				FEASSERT(spCatalog.isValid());

				spCatalog->catalog<String>("selection")="";
				spCatalog->catalog<bool>("show")=false;
			}
		}
		else if(m_spManipulator.isValid())
		{
			sp<Catalog> spCatalog=m_spManipulator;
			FEASSERT(spCatalog.isValid());

			Record jointRecord=lookupJoint(m_pickName);

			m_aShift(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("shiftX"),
					spCatalog->catalog<Real>("shiftY"),
					spCatalog->catalog<Real>("shiftZ"));

			m_aStretch(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("stretchX"),
					spCatalog->catalog<Real>("stretchY"),
					spCatalog->catalog<Real>("stretchZ"));

			m_aBend(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("bendX"),
					spCatalog->catalog<Real>("bendY"),
					spCatalog->catalog<Real>("bendZ"));

			m_aResize(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("resizeX"),
					spCatalog->catalog<Real>("resizeY"),
					spCatalog->catalog<Real>("resizeZ"));

			m_aRescale(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("rescaleX"),
					spCatalog->catalog<Real>("rescaleY"),
					spCatalog->catalog<Real>("rescaleZ"));

			m_aForward(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("forwardX"),
					spCatalog->catalog<Real>("forwardY"),
					spCatalog->catalog<Real>("forwardZ"));

			m_aBackward(jointRecord)=SpatialVector(
					spCatalog->catalog<Real>("backwardX"),
					spCatalog->catalog<Real>("backwardY"),
					spCatalog->catalog<Real>("backwardZ"));
		}

		if(m_spManipulator.isValid())
		{
			m_spManipulator->bindOverlay(spDrawOverlay);
			m_spManipulator->handle(a_rSignal);
			m_spManipulator->draw(spDrawBrush,NULL);
		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

		return;
	}

	const Real envelope=catalog<Real>("envelope");

	const BWORD pinRootPos=catalog<bool>("pinRootPos");
	const BWORD pinRootRot=catalog<bool>("pinRootRot");

	const BWORD bendJoints=catalog<bool>("bendJoints");
	const Real bendRatio=
			bendJoints? envelope*catalog<Real>("bendRatio"): 0.0;

	const BWORD resizeJoints=catalog<bool>("resizeJoints");
	const Real resizeRatio=
			resizeJoints? envelope*catalog<Real>("resizeRatio"): 0.0;

	const BWORD translationControl=catalog<bool>("translationControl");
	const BWORD shiftJoints=catalog<bool>("shiftJoints");
	const Real shiftRatio=(!translationControl)? bendRatio:
			(shiftJoints? envelope*catalog<Real>("shiftRatio"): 0.0);

	const BWORD stretchJoints=catalog<bool>("stretchJoints");
	const Real stretchRatio=(!translationControl)? resizeRatio:
			(stretchJoints? envelope*catalog<Real>("stretchRatio"): 0.0);

	const Real poseRatio=
			catalog<bool>("poseJoints")? catalog<Real>("poseRatio"): 0.0;

	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputNormal;
	if(!access(spOutputNormal,m_spOutputAccessible,
			e_primitive,e_normal)) return;

	sp<SurfaceAccessorI> spOutputAnimX;
	if(!access(spOutputAnimX,m_spOutputAccessible,e_primitive,"animX")) return;

	sp<SurfaceAccessorI> spOutputAnimY;
	if(!access(spOutputAnimY,m_spOutputAccessible,e_primitive,"animY")) return;

	sp<SurfaceAccessorI> spOutputAnimZ;
	if(!access(spOutputAnimZ,m_spOutputAccessible,e_primitive,"animZ")) return;

	sp<SurfaceAccessorI> spOutputAnimT;
	if(!access(spOutputAnimT,m_spOutputAccessible,e_primitive,"animT")) return;

	sp<SurfaceAccessorI> spOutputAnimS;
	if(!access(spOutputAnimS,m_spOutputAccessible,e_primitive,"animS")) return;

	sp<SurfaceAccessorI> spOutputRefX;
	if(!access(spOutputRefX,m_spOutputAccessible,e_primitive,"refX")) return;

	sp<SurfaceAccessorI> spOutputRefY;
	if(!access(spOutputRefY,m_spOutputAccessible,e_primitive,"refY")) return;

	sp<SurfaceAccessorI> spOutputRefZ;
	if(!access(spOutputRefZ,m_spOutputAccessible,e_primitive,"refZ")) return;

	sp<SurfaceAccessorI> spOutputRefT;
	if(!access(spOutputRefT,m_spOutputAccessible,e_primitive,"refT")) return;

	//* input joint arrays
	Array<String>& rJointNameArray=
			catalog< Array<String> >("jointName");

	Array<SpatialVector>& rJointShiftArray=
			catalog< Array<SpatialVector> >("jointShift");

	Array<SpatialVector>& rJointStretchArray=
			catalog< Array<SpatialVector> >("jointStretch");

	Array<SpatialVector>& rJointBendArray=
			catalog< Array<SpatialVector> >("jointBend");

	Array<SpatialVector>& rJointResizeArray=
			catalog< Array<SpatialVector> >("jointResize");

	Array<SpatialVector>& rJointRescaleArray=
			catalog< Array<SpatialVector> >("jointRescale");

	Array<SpatialVector>& rJointForwardArray=
			catalog< Array<SpatialVector> >("jointForward");

	Array<SpatialVector>& rJointBackwardArray=
			catalog< Array<SpatialVector> >("jointBackward");

	m_posePrimitiveMap.clear();
	m_animPrimitiveMap.clear();

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const String jointName=spInputName->string(primitiveIndex);

		m_animPrimitiveMap[jointName]=primitiveIndex;

		const U32 nameCount=rJointNameArray.size();
		const U32 shiftCount=rJointShiftArray.size();
		const U32 stretchCount=rJointStretchArray.size();
		const U32 bendCount=rJointBendArray.size();
		const U32 backwardCount=rJointBackwardArray.size();
		const U32 forwardCount=rJointForwardArray.size();
		const U32 minCount=
				fe::minimum(nameCount,
				fe::minimum(shiftCount,
				fe::minimum(stretchCount,
				fe::minimum(bendCount,
				fe::minimum(backwardCount,forwardCount)))));

		for(U32 nameIndex=0;nameIndex<minCount;nameIndex++)
		{
			if(rJointNameArray[nameIndex]==jointName)
			{
				Record jointRecord=lookupJoint(jointName);
				m_aShift(jointRecord)=rJointShiftArray[nameIndex];
				m_aStretch(jointRecord)=rJointStretchArray[nameIndex];
				m_aBend(jointRecord)=rJointBendArray[nameIndex];
				m_aResize(jointRecord)=rJointResizeArray[nameIndex];
				m_aRescale(jointRecord)=rJointRescaleArray[nameIndex];
				m_aForward(jointRecord)=rJointForwardArray[nameIndex];
				m_aBackward(jointRecord)=rJointBackwardArray[nameIndex];

#if FE_PPO_ARRAY_DEBUG
				feLog("PuppetOp::handle array input %d/%d \"%s\" %s\n",
						primitiveIndex,primitiveCount,
						jointName.c_str(),
						c_print(m_aBend(jointRecord)));
#endif
			}
		}
	}

	sp<SurfaceAccessibleI> spPoseAccessible;
	sp<SurfaceAccessorI> spPoseAnimX;
	sp<SurfaceAccessorI> spPoseAnimY;
	sp<SurfaceAccessorI> spPoseAnimZ;
	sp<SurfaceAccessorI> spPoseAnimT;
	if(poseRatio>0.0)
	{
		access(spPoseAccessible,"Pose Joints",e_quiet);

		if(spPoseAccessible.isValid())
		{
			sp<SurfaceAccessorI> spPoseName;
			if(!access(spPoseName,spPoseAccessible,
					e_primitive,"name")) return;

			if(!access(spPoseAnimX,spPoseAccessible,
					e_primitive,"animX")) return;

			if(!access(spPoseAnimY,spPoseAccessible,
					e_primitive,"animY")) return;

			if(!access(spPoseAnimZ,spPoseAccessible,
					e_primitive,"animZ")) return;

			if(!access(spPoseAnimT,spPoseAccessible,
					e_primitive,"animT")) return;

			const U32 posePrimitiveCount=spPoseName->count();
			for(U32 posePrimitiveIndex=0;posePrimitiveIndex<posePrimitiveCount;
					posePrimitiveIndex++)
			{
				const String jointName=spPoseName->string(posePrimitiveIndex);

				m_posePrimitiveMap[jointName]=posePrimitiveIndex;
			}
		}
		else
		{
			catalog<String>("warning")+="no pose input;";
		}
	}

#if FE_PPO_OFFSET
	const Real offsetRatio=
			catalog<bool>("offsetJoints")? catalog<Real>("offsetRatio"): 0.0;
	m_offsetPrimitiveMap.clear();

	sp<SurfaceAccessibleI> spOffsetAccessible;
	sp<SurfaceAccessorI> spOffsetAnimX;
	sp<SurfaceAccessorI> spOffsetAnimY;
	sp<SurfaceAccessorI> spOffsetAnimZ;
	sp<SurfaceAccessorI> spOffsetAnimT;
	if(offsetRatio>0.0)
	{
		access(spOffsetAccessible,"Offset Joints",e_quiet);

		if(spOffsetAccessible.isValid())
		{
			sp<SurfaceAccessorI> spOffsetName;
			if(!access(spOffsetName,spOffsetAccessible,
					e_primitive,"name")) return;

			if(!access(spOffsetAnimX,spOffsetAccessible,
					e_primitive,"animX")) return;

			if(!access(spOffsetAnimY,spOffsetAccessible,
					e_primitive,"animY")) return;

			if(!access(spOffsetAnimZ,spOffsetAccessible,
					e_primitive,"animZ")) return;

			if(!access(spOffsetAnimT,spOffsetAccessible,
					e_primitive,"animT")) return;

			const U32 offsetPrimitiveCount=spOffsetName->count();
			for(U32 offsetPrimitiveIndex=0;
					offsetPrimitiveIndex<offsetPrimitiveCount;
					offsetPrimitiveIndex++)
			{
				const String jointName=
						spOffsetName->string(offsetPrimitiveIndex);

				m_offsetPrimitiveMap[jointName]=offsetPrimitiveIndex;
			}
		}
		else
		{
			catalog<String>("warning")+="no offset input;";
		}
	}
#endif

	m_animMap.clear();
	m_origMap.clear();

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const String jointName=spInputName->string(primitiveIndex);

		const String parentName=spInputParent->string(primitiveIndex);
		const BWORD hasParent=(m_animMap.find(parentName)!=m_animMap.end());

		Record jointRecord=lookupJoint(jointName);
		Record parentRecord=
				parentName.empty()? Record(): lookupJoint(parentName);

		const SpatialVector jointForward(
				lookupForward(jointRecord,0)*degToRad,
				lookupForward(jointRecord,1)*degToRad,
				lookupForward(jointRecord,2)*degToRad);

		const SpatialVector jointBackward(
				-lookupBackward(jointRecord,0)*degToRad,
				-lookupBackward(jointRecord,1)*degToRad,
				-lookupBackward(jointRecord,2)*degToRad);

		SpatialVector oneBend(0.0,0.0,0.0);
		SpatialVector oneResize(0.0,0.0,0.0);
		SpatialVector oneShift(0.0,0.0,0.0);
		SpatialVector oneStretch(0.0,0.0,0.0);

		SpatialVector oneRescale(1.0,1.0,1.0);
		if(parentRecord.isValid())
		{
			set(oneRescale,
				lookupRescale(parentRecord,0),
				lookupRescale(parentRecord,1),
				lookupRescale(parentRecord,2));
		}
		for(U32 m=0;m<3;m++)
		{
			if(oneRescale[m]<=0.0)
			{
				oneRescale[m]=1.0;
			}
		}

		if(bendJoints && (hasParent || !pinRootRot))
		{
			const SpatialVector jointBend(
					lookupBend(jointRecord,0),
					lookupBend(jointRecord,1),
					lookupBend(jointRecord,2));

			oneBend=jointBend*bendRatio;
		}

		if(resizeJoints)
		{
			const SpatialVector jointResize=parentRecord.isValid()?
					SpatialVector(
					lookupResize(parentRecord,0),
					lookupResize(parentRecord,1),
					lookupResize(parentRecord,2)):
					SpatialVector(1.0,1.0,1.0);

			oneResize=jointResize*resizeRatio;
		}

		if(shiftJoints && (hasParent || !pinRootPos))
		{
			const SpatialVector jointShift(
					lookupShift(jointRecord,0),
					lookupShift(jointRecord,1),
					lookupShift(jointRecord,2));

			oneShift=jointShift*shiftRatio;
		}

		if(stretchJoints && (hasParent || !pinRootPos))
		{
			const SpatialVector jointStretch(
					lookupStretch(jointRecord,0),
					lookupStretch(jointRecord,1),
					lookupStretch(jointRecord,2));

			oneStretch=jointStretch*stretchRatio;
		}

		SpatialTransform xformLocalRef;
		SpatialQuaternion quatLocalRef;
		if(spOutputRefX.isValid())
		{
			xformLocalRef.column(0)=spOutputRefX->spatialVector(primitiveIndex);
			xformLocalRef.column(1)=spOutputRefY->spatialVector(primitiveIndex);
			xformLocalRef.column(2)=spOutputRefZ->spatialVector(primitiveIndex);
			xformLocalRef.column(3)=spOutputRefT->spatialVector(primitiveIndex);

			quatLocalRef=xformLocalRef;
		}
		else
		{
			setIdentity(xformLocalRef);
			set(quatLocalRef);
		}

		SpatialTransform xformLocal;
		xformLocal.column(0)=spOutputAnimX->spatialVector(primitiveIndex);
		xformLocal.column(1)=spOutputAnimY->spatialVector(primitiveIndex);
		xformLocal.column(2)=spOutputAnimZ->spatialVector(primitiveIndex);
		xformLocal.column(3)=spOutputAnimT->spatialVector(primitiveIndex);

		const U32 parentPrimitiveIndex=m_animPrimitiveMap[parentName];
		const SpatialVector scaleLocal=parentName.empty()?
				SpatialVector(1.0,1.0,1.0):
				spOutputAnimS->spatialVector(parentPrimitiveIndex);

#if FE_PPO_DEBUG
		const SpatialVector scaleLocalNext=
				spOutputAnimS->spatialVector(primitiveIndex);

		feLog("PuppetOp::handle"
				" input %d/%d \"%s\" parent %d \"%s\"\n",
				primitiveIndex,primitiveCount,jointName.c_str(),
				parentPrimitiveIndex,parentName.c_str());
		feLog("  scale %s next %s\n",
				c_print(scaleLocal),c_print(scaleLocalNext));
		feLog("  oneBend %s oneResize %s oneRescale %s\n",
				c_print(oneBend),c_print(oneResize),c_print(oneRescale));
		feLog("  oneShift %s oneStretch %s\n",
				c_print(oneShift),c_print(oneStretch));
		feLog("  xformLocalRef:\n%s\n",c_print(xformLocalRef));
		feLog("  xformLocal:\n%s\n",c_print(xformLocal));
#endif

		if(spPoseAccessible.isValid())
		{
			const U32 posePrimitiveIndex=m_posePrimitiveMap[jointName];

			SpatialTransform xformPose;
			xformPose.column(0)=spPoseAnimX->spatialVector(posePrimitiveIndex);
			xformPose.column(1)=spPoseAnimY->spatialVector(posePrimitiveIndex);
			xformPose.column(2)=spPoseAnimZ->spatialVector(posePrimitiveIndex);
			xformPose.column(3)=spPoseAnimT->spatialVector(posePrimitiveIndex);

			if(poseRatio<1.0)
			{
				SpatialTransform invLocalRef;
				invert(invLocalRef,xformLocalRef);

				const SpatialQuaternion quatToPose=xformPose*invLocalRef;

				Real radians;
				SpatialVector axis;
				quatToPose.computeAngleAxis(radians,axis);
				const SpatialQuaternion quatToPartial(poseRatio*radians,axis);

				SpatialTransform xformToPartial=SpatialTransform(quatToPartial);
				xformToPartial.translation();

				xformLocalRef=xformToPartial*xformLocalRef;
			}
			else
			{
				xformLocalRef=xformPose;
			}

			quatLocalRef=xformLocalRef;
		}

#if FE_PPO_OFFSET
		SpatialQuaternion quatOffset(0.0,0.0,0.0,1.0);
		if(spOffsetAccessible.isValid())
		{
			const U32 offsetPrimitiveIndex=m_offsetPrimitiveMap[jointName];

			SpatialTransform xformOffset;
			xformOffset.column(0)=
					spOffsetAnimX->spatialVector(offsetPrimitiveIndex);
			xformOffset.column(1)=
					spOffsetAnimY->spatialVector(offsetPrimitiveIndex);
			xformOffset.column(2)=
					spOffsetAnimZ->spatialVector(offsetPrimitiveIndex);
			xformOffset.column(3)=
					spOffsetAnimT->spatialVector(offsetPrimitiveIndex);

			SpatialTransform invLocalRef;
			invert(invLocalRef,xformLocalRef);

			quatOffset=xformOffset*invLocalRef;
		}
#endif

		SpatialQuaternion quatLocal=xformLocal;

		//* min max
		const Real deg180=179.999*degToRad;
		const BWORD constrained=
				(-jointBackward[0]<deg180 || -jointBackward[1]<deg180 ||
				-jointBackward[2]<deg180 || jointForward[0]<deg180 ||
				jointForward[1]<deg180 || jointForward[2]<deg180);
		const BWORD affectTranslation=
				(oneShift[0]<1.0 || oneShift[1]<1.0 || oneShift[2]<1.0 ||
				oneStretch[0]<1.0 || oneStretch[1]<1.0 || oneStretch[2]<1.0);

		SpatialVector displaceBlended;
		SpatialQuaternion quatBlended;
		if(constrained || affectTranslation||
				oneBend[0]<1.0 || oneBend[1]<1.0 || oneBend[2]<1.0 ||
				oneResize[0]<1.0 || oneResize[1]<1.0 || oneResize[2]<1.0 ||
				oneRescale[0]<1.0 || oneRescale[1]<1.0 || oneRescale[2]<1.0)
		{
			if(!constrained &&
					fabs(oneBend[0]-oneBend[1])<1e-6 &&
					fabs(oneBend[0]-oneBend[2])<1e-6)
			{
				//* avoid Euler
				slerp(quatBlended,quatLocalRef,quatLocal,oneBend[0]);
			}
			else
			{
				const SpatialQuaternion quatDelta=
						inverse(quatLocalRef)*quatLocal;

				SpatialEuler euler=SpatialTransform(quatDelta);
				for(U32 m=0;m<3;m++)
				{
					euler[m]*=oneBend[m];

					if(euler[m]<jointBackward[m])
					{
						euler[m]=jointBackward[m];
					}
					if(euler[m]>jointForward[m])
					{
						euler[m]=jointForward[m];
					}
				}

				const SpatialQuaternion quatPartial=SpatialTransform(euler);

				quatBlended=quatLocalRef*quatPartial;
			}

			SpatialVector resizedAnimT;
			SpatialVector resizedRefT;

			//* TODO resize scaleLocal and store back in AnimS

			for(U32 m=0;m<3;m++)
			{
				const Real resize=oneResize[m]*scaleLocal[m]+(1.0-oneResize[m]);

				const Real animT=xformLocal.translation()[m];
				const Real unscaledAnimT=animT/scaleLocal[m];
				resizedAnimT[m]=unscaledAnimT*resize;

				const Real refT=xformLocalRef.translation()[m];
				resizedRefT[m]=refT*resize;
			}

			if(affectTranslation)
			{
				const Real refMag=magnitude(resizedRefT);
				const Real animMag=magnitude(resizedAnimT);

				//* TODO separate axes, aligned to ref dir
				const Real stretchMag=refMag+(animMag-refMag)*oneStretch[1];

				SpatialVector refDir=unitSafe(resizedRefT);

				SpatialQuaternion rotation;
				set(rotation,refDir,unitSafe(resizedAnimT));

				//* TODO conditionally separate axes as Euler
				rotation=angularlyScaled(rotation,oneShift[0]);

				SpatialVector stretchT;
				rotateVector(rotation,refDir,stretchT);
				stretchT*=stretchMag;

				for(U32 m=0;m<3;m++)
				{
					displaceBlended[m]=oneRescale[m]*stretchT[m];
				}
			}
			else
			{
				SpatialVector resizedDiffT=resizedAnimT-resizedRefT;

				for(U32 m=0;m<3;m++)
				{
					displaceBlended[m]=oneRescale[m]*
							(resizedRefT[m]+oneStretch[m]*resizedDiffT[m]);
				}
			}
		}
		else
		{
			quatBlended=quatLocal;
			displaceBlended=xformLocal.translation();
		}

#if FE_PPO_OFFSET
		if(offsetRatio>0.0)
		{
			Real radiansOff;
			SpatialVector axisOff;
			quatOffset.computeAngleAxis(radiansOff,axisOff);
			const SpatialQuaternion quatPartialOff(
					offsetRatio*radiansOff,axisOff);

			quatBlended=quatBlended*quatPartialOff;
		}
#endif

		SpatialTransform xformBlended(quatBlended);
		xformBlended.translation()=displaceBlended;

#if FE_PPO_DEBUG
		feLog("  blended:\n%s\n",c_print(xformBlended));
#endif

		spOutputAnimX->set(primitiveIndex,xformBlended.column(0));
		spOutputAnimY->set(primitiveIndex,xformBlended.column(1));
		spOutputAnimZ->set(primitiveIndex,xformBlended.column(2));
		spOutputAnimT->set(primitiveIndex,xformBlended.column(3));

		spOutputRefX->set(primitiveIndex,xformLocalRef.column(0));
		spOutputRefY->set(primitiveIndex,xformLocalRef.column(1));
		spOutputRefZ->set(primitiveIndex,xformLocalRef.column(2));
		spOutputRefT->set(primitiveIndex,xformLocalRef.column(3));

		//* update visible triangle

		SpatialTransform xformParent;
		SpatialTransform xformParentOrig;
		if(hasParent)
		{
			xformParent=m_animMap[parentName];
			xformParentOrig=m_origMap[parentName];
		}
		else
		{
			setIdentity(xformParent);
			setIdentity(xformParentOrig);
		}

		const SpatialTransform xformWorld=xformBlended*xformParent;
		m_animMap[jointName]=xformWorld;

		const SpatialTransform xformWorldOrig=xformLocal*xformParentOrig;
		m_origMap[jointName]=xformWorldOrig;

		SpatialTransform invWorldOrig;
		invert(invWorldOrig,xformWorldOrig);

		const SpatialTransform conversion=invWorldOrig*xformWorld;

		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const SpatialVector point=
					spOutputVertices->spatialVector(primitiveIndex,subIndex);

			SpatialVector transformed;
			transformVector(conversion,point,transformed);

			spOutputVertices->set(primitiveIndex,subIndex,transformed);
		}

		spOutputNormal->set(primitiveIndex,xformBlended.column(2));
	}

	//* output joint arrays
	rJointNameArray.clear();
	rJointShiftArray.clear();
	rJointStretchArray.clear();
	rJointBendArray.clear();
	rJointForwardArray.clear();
	rJointBackwardArray.clear();

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const String jointName=spInputName->string(primitiveIndex);
		Record jointRecord=lookupJoint(jointName);
		const SpatialVector shift=m_aShift(jointRecord);
		const SpatialVector stretch=m_aStretch(jointRecord);
		const SpatialVector bend=m_aBend(jointRecord);
		const SpatialVector resize=m_aResize(jointRecord);
		const SpatialVector rescale=m_aRescale(jointRecord);
		const SpatialVector forward=m_aForward(jointRecord);
		const SpatialVector backward=m_aBackward(jointRecord);

#if FE_PPO_ARRAY_DEBUG
		feLog("PuppetOp::handle array output %d/%d \"%s\" %s\n",
				primitiveIndex,primitiveCount,
				jointName.c_str(),c_print(bend));
#endif

		rJointNameArray.push_back(jointName);
		rJointShiftArray.push_back(shift);
		rJointStretchArray.push_back(stretch);
		rJointBendArray.push_back(bend);
		rJointResizeArray.push_back(resize);
		rJointRescaleArray.push_back(rescale);
		rJointForwardArray.push_back(forward);
		rJointBackwardArray.push_back(backward);
	}

	catalog<String>("pickName")=m_pickName;

#if FE_PPO_DEBUG
	feLog("PuppetOp::handle done\n");
#endif
}

Color PuppetOp::makeColor(Color a_color,BWORD a_bright)
{
	if(!a_bright)
	{
		return a_color;
	}

	Color result=a_color*2.0;
	for(U32 m=0;m<3;m++)
	{
		if(result[m]>1.0)
		{
			result[m]=1.0;
		}
		if(result[m]<0.5)
		{
			result[m]=0.5;
		}
	}
	result[3]=a_color[3];

	return result;
}

void PuppetOp::drawJoint(sp<DrawI>& a_rspDrawI,const SpatialVector& a_rTip,
	const SpatialVector* a_pCorner,const Color* a_pColor)
{
	FEASSERT(a_pCorner);
	FEASSERT(a_pColor);

	//* tweak
	const U32 resolution=64;
	const Real alpha=0.2;

	const String drawForm=catalog<String>("drawForm");

	if(drawForm=="pyramid")
	{
		SpatialVector line[2];
		Color color;

		line[0]=a_rTip;

		line[1]=a_pCorner[0];
		color=a_pColor[1];
		color[3]=alpha;
		a_rspDrawI->drawLines(line,NULL,2,DrawI::e_strip,
				false,&color);

		line[1]=a_pCorner[3];
		color=a_pColor[2];
		color[3]=alpha;
		a_rspDrawI->drawLines(line,NULL,2,DrawI::e_strip,
				false,&color);

		line[1]=a_pCorner[1];
		color=a_pColor[0];
		color[3]=alpha;
		a_rspDrawI->drawLines(line,NULL,2,DrawI::e_strip,
				false,&color);

		line[1]=a_pCorner[2];
		a_rspDrawI->drawLines(line,NULL,2,DrawI::e_strip,
				false,&color);

		a_rspDrawI->drawLines(a_pCorner,NULL,5,DrawI::e_strip,
				false,&color);

		return;
	}

	const Real drawRadius=catalog<Real>("drawRadius");

	const BWORD boneForm=(drawForm=="bone");

	SpatialVector pointArray[resolution];
	SpatialVector normalArray[resolution];
	Real radiusArray[resolution];
	Color colorArray[resolution];

	const SpatialVector base=0.5*(a_pCorner[0]+a_pCorner[2]);
	const SpatialVector segment=a_rTip-base;
	const Real segLength=magnitude(segment);
	const SpatialVector unitSegment=segment*(segLength>0.0? 1.0/segLength: 1.0);

	const Real radius=fe::minimum(Real(0.25*segLength),drawRadius);
	const Real radius2=2.0*radius;

	//* twist doesn't matter?
	const SpatialVector norm(unitSegment[1],unitSegment[2],unitSegment[0]);

	for(U32 m=0;m<resolution;m++)
	{
		const Real fraction=m/(resolution-1.0);

		Real flame=0.0;
		if(boneForm)
		{
			const Real along=segLength*fraction;
			if(along<radius2)
			{
				flame=sin(fe::pi*along/radius2);
			}
			else if(along>segLength-radius2)
			{
				flame=sin(fe::pi*(segLength-along)/radius2);
			}

			if(along>radius && along<segLength-radius)
			{
				flame=fe::maximum(flame,Real(0.4));	//* tweak
			}
		}
		else
		{
			flame=flameProfile(0.3,fraction);
		}

		pointArray[m]=base+fraction*segment;

		normalArray[m]=norm;

		radiusArray[m]=radius*flame;

		colorArray[m]=a_pColor[0]*(0.2+0.8*pow(flame,0.5));
		colorArray[m][3]=alpha;
	}

	const BWORD multicolor=TRUE;
	const OperateCommon::CurveMode curveMode=
			OperateCommon::e_tube;
	const SpatialVector towardCamera(0.0,0.0,1.0);

	a_rspDrawI->pushDrawMode(m_spSolid);
	drawTube(a_rspDrawI,pointArray,normalArray,radiusArray,
			resolution,curveMode,multicolor,colorArray,
			towardCamera);
	a_rspDrawI->popDrawMode();
}

Record PuppetOp::lookupJoint(String a_jointName)
{
	Record jointRecord=m_spJointMap->lookup(a_jointName);
	if(jointRecord.isValid())
	{
		return jointRecord;
	}

	//* create new one
	jointRecord=m_spScope->createRecord("Joint");

	m_aName(jointRecord)=a_jointName;
	m_aShift(jointRecord)=SpatialVector(1.0,1.0,1.0);
	m_aStretch(jointRecord)=SpatialVector(1.0,1.0,1.0);
	m_aBend(jointRecord)=SpatialVector(1.0,1.0,1.0);
	m_aResize(jointRecord)=SpatialVector(1.0,1.0,1.0);
	m_aRescale(jointRecord)=SpatialVector(1.0,1.0,1.0);
	m_aForward(jointRecord)=SpatialVector(180.0,180.0,180.0);
	m_aBackward(jointRecord)=SpatialVector(180.0,180.0,180.0);

	m_spState->add(jointRecord);

	return jointRecord;
}

void PuppetOp::deleteJoint(String a_jointName)
{
	if(a_jointName.empty())
	{
		return;
	}

	Record jointRecord=m_spJointMap->lookup(a_jointName);
	if(jointRecord.isValid())
	{
		m_spState->remove(jointRecord);
	}
}

void PuppetOp::deleteJoints(void)
{
	m_spState->clear();
}

Real PuppetOp::lookupShift(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aShift.check(a_record))
	{
		return 1.0;
	}
	return m_aShift(a_record)[a_axis];
}

Real PuppetOp::lookupStretch(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aStretch.check(a_record))
	{
		return 1.0;
	}
	return m_aStretch(a_record)[a_axis];
}

Real PuppetOp::lookupBend(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aBend.check(a_record))
	{
		return 1.0;
	}
	return m_aBend(a_record)[a_axis];
}

Real PuppetOp::lookupResize(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aResize.check(a_record))
	{
		return 1.0;
	}
	return m_aResize(a_record)[a_axis];
}

Real PuppetOp::lookupRescale(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aRescale.check(a_record))
	{
		return 1.0;
	}
	return m_aRescale(a_record)[a_axis];
}

Real PuppetOp::lookupForward(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aForward.check(a_record))
	{
		return 180.0;
	}
	return m_aForward(a_record)[a_axis];
}

Real PuppetOp::lookupBackward(Record a_record,U32 a_axis)
{
	FEASSERT(a_axis<3);
	if(!m_aBackward.check(a_record))
	{
		return 180.0;
	}
	return m_aBackward(a_record)[a_axis];
}

void PuppetOp::setupState(void)
{
	//* esc: clear selection?
	//* return: toggle displace/rotate
	//* delete: reset joint
	//* left: bend
	//* middle: max

	m_aName.populate(m_spScope,"Joint","Name");

	m_aShift.populate(m_spScope,"Joint","Shift");
	m_aStretch.populate(m_spScope,"Joint","Stretch");
	m_aBend.populate(m_spScope,"Joint","Bend");
	m_aResize.populate(m_spScope,"Joint","Resize");
	m_aRescale.populate(m_spScope,"Joint","Rescale");
	m_aForward.populate(m_spScope,"Joint","Forward");
	m_aBackward.populate(m_spScope,"Joint","Backward");

	//* default values, in case some attributes are missing
	m_spScope->cookbook()->add("Joint","Shift","1 1 1");
	m_spScope->cookbook()->add("Joint","Stretch","1 1 1");
	m_spScope->cookbook()->add("Joint","Bend","1 1 1");
	m_spScope->cookbook()->add("Joint","Resize","1 1 1");
	m_spScope->cookbook()->add("Joint","Rescale","1 1 1");
	m_spScope->cookbook()->add("Joint","Forward","180 180 180");
	m_spScope->cookbook()->add("Joint","Backward","180 180 180");

	m_spJointMap=m_spState->lookupMap(m_aName);
}

BWORD PuppetOp::loadState(const String& a_rBuffer)
{
	if(OperatorState::loadState(a_rBuffer))
	{
		m_spJointMap=m_spState->lookupMap(m_aName);
		return TRUE;
	}

	return FALSE;
}
