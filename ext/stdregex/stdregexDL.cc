/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <stdregex/stdregex.pmh>

#include "platform/dlCore.cc"

#define FE_REGEX_DEBUG	FALSE

namespace fe
{
namespace ext
{

void* stdregex_init(const char* a_pattern)
{
	std::basic_regex<char>* expression=new std::basic_regex<char>(a_pattern);

#if FE_REGEX_DEBUG
	feLogError("stdregex_init \"%s\" -> %p\n",a_pattern,expression);
#endif

	return expression;
}

void* stdregex_match(void* a_expression,const char* a_candidate)
{
#if FE_REGEX_DEBUG
	feLogError("stdregex_match %p \"%s\"\n",a_expression,a_candidate);
#endif

	Array<std::string>* pStringArray=new Array<std::string>(1);
	std::basic_regex<char>& re=*(std::basic_regex<char>*)a_expression;
	if(!std::regex_match(a_candidate,re))
	{
#if FE_REGEX_DEBUG
		feLogError("stdregex_match %p failed\n",a_expression);
#endif

		delete pStringArray;
		return NULL;
	}

#if FE_REGEX_DEBUG
	feLogError("stdregex_match %p result %p\n",a_expression,pStringArray);
#endif

	return pStringArray;
}

void* stdregex_search(void* a_expression,const char* a_candidate)
{
#if FE_REGEX_DEBUG
	feLogError("stdregex_search %p \"%s\"\n",a_expression,a_candidate);
#endif

	Array<std::string>* pStringArray=new Array<std::string>(1);
	std::basic_regex<char>& re=*(std::basic_regex<char>*)a_expression;
	if(!std::regex_search(a_candidate,re))
	{
#if FE_REGEX_DEBUG
		feLogError("stdregex_search %p failed\n",a_expression);
#endif

		delete pStringArray;
		return NULL;
	}

#if FE_REGEX_DEBUG
	feLogError("stdregex_search %p result %p\n",a_expression,pStringArray);
#endif

	return pStringArray;
}

void* stdregex_replace(void* a_expression,const char* a_candidate,
		const char* a_replacement)
{
	//* there is also Replace() for single replacement

	Array<std::string>* pStringArray=new Array<std::string>(1);
	std::basic_regex<char>& re=*(std::basic_regex<char>*)a_expression;

#if FE_REGEX_DEBUG
	// REMOVED because std::basic_regex::pattern() isn't a thing
	// feLogError("stdregex_replace %p \"%s\" \"%s\" -> \"%s\"\n",
	//		a_expression,a_candidate,re.pattern().c_str(),a_replacement);
#endif

	std::string candidate=a_candidate;
	std::string replacement=a_replacement;
	std::string replaced;
	try
	{
		replaced=std::regex_replace(candidate,re,replacement);
	}
	catch(std::regex_error& e)
	{
		delete pStringArray;
		return NULL;
	}

#if FE_REGEX_DEBUG
	feLogError("stdregex_replace result \"%s\" \n",replaced.c_str());
#endif

	(*pStringArray)[0]=replaced;

	return pStringArray;
}

const char* stdregex_result(void* a_result,U32 a_index)
{
#if FE_REGEX_DEBUG
	feLogError("stdregex_result %p %d\n",a_result,a_index);
#endif

	Array<std::string>* pStringArray=(Array<std::string>*)a_result;
	return (*pStringArray)[a_index].c_str();
}

void stdregex_finish(void* a_expression)
{
#if FE_REGEX_DEBUG
	feLogError("stdregex_finish %p\n",a_expression);
#endif

	std::basic_regex<char>* expression=(std::basic_regex<char>*)a_expression;
	delete expression;
}

void stdregex_release(void* a_result)
{
#if FE_REGEX_DEBUG
	feLogError("stdregex_release %p\n",a_result);
#endif

	Array<std::string>* pStringArray=(Array<std::string>*)a_result;
	delete pStringArray;
}

extern "C"
{

FE_DL_EXPORT bool regex_init(void)
{
#if FE_REGEX_DEBUG
	feLogError("stdregexDL regex_init\n");
#endif

//	feLogError("  using stdlib regex\n");

	Regex::replaceInitFunction(stdregex_init);
	Regex::replaceMatchFunction(stdregex_match);
	Regex::replaceSearchFunction(stdregex_search);
	Regex::replaceReplaceFunction(stdregex_replace);
	Regex::replaceResultFunction(stdregex_result);
	Regex::replaceFinishFunction(stdregex_finish);
	Regex::replaceReleaseFunction(stdregex_release);

#if FE_REGEX_DEBUG
	feLogError("stdregexDL regex_init done\n");
#endif

	return true;
}

}

} /* namespace ext */
} /* namespace fe */
