import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs[:]

#   deplibs += [    "fexFlightDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [ "fexSignalLib" ]

    tests = [   'xDroneServer' ]

    for t in tests:
        exe = module.Exe(t)

        forge.deps([t + "Exe"], deplibs)

#   forge.tests += [
#       ("xDroneServer.exe",    "test/drone_mission.yaml",  None,   None) ]
