/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_Executor_h__
#define __architecture_Executor_h__

#include <datatool/datatool.h>

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Executor RecordView

	@ingroup architecture
*//***************************************************************************/
class FE_DL_EXPORT Executor:
	virtual public Recordable,
	virtual public RecordableI,
	virtual public ApplicationI
{
	public:
		Functor< Array<String> >			pluginList;
		Functor< std::map<String,String> >	catalogMap;
		Functor<String>						pipeline;

				Executor(void);
virtual			~Executor(void);

virtual	void	addFunctors(void)
				{
					Recordable::addFunctors();

					add(pluginList,		FE_SPEC("plug:list",
							"array of plugin dynamic libraries to preload"));
					add(catalogMap,		FE_SPEC("cat:map",
							"store of catalog entry pairs"));
					add(pipeline,	FE_SPEC("arch:pipe",
							"architecture pipeline filename"));
				}
virtual	void	initializeRecord(void)
				{
					Recordable::initializeRecord();
				}
virtual	void	finalizeRecord(void)
				{
					Recordable::finalizeRecord();
				}

				//* as RecordableI

				using Recordable::bind;

virtual void	bind(Record& record);

				//* as ApplicationI
virtual	void	setup(int a_argc, char **a_argv, char** a_env);
virtual	bool	step(int &a_returnCode);
virtual	bool	loop(int &a_returnCode);

		void	resolve(void);

	protected:
		sp<RecordGroup>	m_spPipeline;
		sp<SignalerI>	m_spSignalerI;
		sp<Layout>		m_spHeartbeat;
		Record			m_heartbeat;

	private:
		void	populateSignaler(void);
		void	loadPlugins(void);
		void	restoreCatalog(void);
		void	storeCatalog(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_Executor_h__ */
