import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexThreadDLLib",
                "fexSurfaceDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDataToolDLLib",
                        "fexDrawDLLib",
                        "fexGeometryDLLib",
                        "fexJsonDLLib" ]

    tests = [   'xJsonGeo',
                'xJsonRecord',
                'xCatalogWriterJson',
                'xStringFilterJson' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xCatalogWriterJson.exe",  "output/test/json/xCatalogWriterJson.json", None,None),
        ("xStringFilterJson.exe",   "ext/json/test/filter.json",    None,None) ]

#   forge.tests += [
#       ("xJsonGeo.exe",            "cross.geo",                    None,None) ]

    if forge.media:
        forge.tests += [
            ("xJsonRecord.exe",     "model/skin.rg",                None,None) ]
