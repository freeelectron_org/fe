/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

namespace fe
{
namespace ext
{

Library *CreateNetSignalLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->setName("default_netsignal");
	pLibrary->add<BSDServer>("ServerI.BSDServer.fe");
	pLibrary->add<BSDClient>("ClientI.BSDClient.fe");
	pLibrary->add<NetworkCatalog>("ConnectedCatalog.NetworkCatalog.fe");

#if FE_BOOST_FILESYSTEM
	pLibrary->add<MultiGroupWriter>("HandlerI.MultiGroupWriter.fe");
	pLibrary->add<MultiGroupReader>("HandlerI.MultiGroupReader.fe");
#endif

	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
