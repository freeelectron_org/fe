/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void BruteCollide::handle(Record& record)
{
	const F32 gain=200.0f;

	m_arenaRV.bind(record);
	const F32 deltaT=m_arenaRV.deltaT();

	sp<RecordGroup> spRG=m_arenaRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_particleRAV.bind(spRA);
		if(m_particleRAV.recordView().mass.check(spRA))
		{
			for(I32 index=0;index<spRA->length();index++)
			{
				Particle& particleRV=m_particleRAV[index];
				const F32 radius1=particleRV.radius();
				const SpatialVector& location1=particleRV.location();

				I32 start=index+1;
				for(RecordGroup::iterator it2=it;it2!=spRG->end();it2++)
				{
					sp<RecordArray> spRA2= *it2;
					m_particleRAV2.bind(spRA2);
					if(m_particleRAV2.recordView().mass.check(spRA2))
					{
						for(I32 index2=start;index2<spRA2->length();index2++)
						{
							Particle& particleRV2=m_particleRAV2[index2];

							const F32 radius2=particleRV2.radius();
							const SpatialVector& location2=
									particleRV2.location();

							const SpatialVector difference=location2-location1;
							const F32 minimum=radius1+radius2;
							const F32 distance2=magnitudeSquared(difference);
							if(minimum*minimum<distance2)
								continue;

							const F32 distance=sqrtf(distance2);
							if(distance>0.1f*minimum)
							{
								const SpatialVector penalty=difference*
										(gain*(1.0f-minimum/distance));

								particleRV.force()+=penalty;
								particleRV2.force()-=penalty;
							}
							else
							{
								feLog("BruteCollide::handle"
										" negligible distance %.6G"
										" \"%s\" vs. \"%s\"\n"
										"\t\"%s\" vs. \"%s\"\n",distance,
										particleRV.record().layout()->
										name().c_str(),
										particleRV2.record().layout()->
										name().c_str(),
										print(location1).c_str(),
										print(location2).c_str());

								const SpatialVector penalty=
										SpatialVector(1.0f,0.0f,0.0f)*gain;

								particleRV.force()+=penalty;
								particleRV2.force()-=penalty;
							}
#if FALSE
							feLog("****\n");
							particleRV.dump();
							particleRV2.dump();
#endif
						}
					}
					start=0;
				}
			}
		}
		//* NOTE considers ALL mass particles (no distance pre-filter)
		//* TODO use negative attractor (using Spherical scan)
		m_explosionRAV.bind(spRA);
		if(m_explosionRAV.recordView().radialForce.check(spRA))
		{
			for(Explosion& explosionRV: m_explosionRAV)
			{
				const F32 expansion=1.2;	//* TODO param
				const F32 minForce=100.0f;

				const SpatialVector& location1=
						explosionRV.location();
				Real& radius=explosionRV.radius();
				F32& force=explosionRV.radialForce();

				//* push
				for(RecordGroup::iterator it2=spRG->begin();
						it2!=spRG->end();it2++)
				{
					sp<RecordArray> spRA2= *it2;
					m_particleRAV2.bind(spRA2);
					if(m_particleRAV2.recordView().mass.check(spRA2))
					{
						for(Particle& particleRV2: m_particleRAV2)
						{
							const SpatialVector& location2=
									particleRV2.location();

							const SpatialVector radial(location2-location1);
							const F32 distance2=magnitudeSquared(radial);
							if(distance2>1e-6f && distance2<radius*radius)
							{
								const F32 distance=sqrtf(distance2);
								particleRV2.force()+=radial*(force/distance);
							}
						}
					}
				}

				if(deltaT>1e-3f)
				{
					//* expand
					//* TODO use deltaT
					radius*=expansion;
					force/=expansion*expansion*expansion;
					if(force<minForce)
					{
//						feLog("expansion %.6G %.6G\n",force,radius);
						Record fizzled=explosionRV.record();
						spRG->remove(fizzled);
					}
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
