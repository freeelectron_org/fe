/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceWrapOp_h__
#define __operator_SurfaceWrapOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to update an attached surface to follow a driving surface

	@ingroup operator

	"Edgy" options are based on "Poly Translation Frames" from
	DreamWorks Animation.

	https://research.dreamworks.com/wp-content/uploads/2018/07/Deformation_Affector_Frame_DWA_2013.pdf

*//***************************************************************************/
class FE_DL_EXPORT SurfaceWrapOp:
	public SurfaceBindOp,
	public Initialize<SurfaceWrapOp>,
	public ObjectSafe<SurfaceWrapOp>
{
	public:

					SurfaceWrapOp(void):
							m_brushed(FALSE),
						m_lastFrame(-1.0),
						m_lastSubdiv(-1),
						m_lastBindToPoints(-1),
						m_lastEdgyDepth(-1),
						m_forceRecook(FALSE),
						m_selectIndex(-1)									{}
virtual				~SurfaceWrapOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange);

	class FE_DL_EXPORT Feedback
	{
		public:
									Feedback(void)							{}
									Feedback(Real a_weight,
										sp<SurfaceI::ImpactI> a_spImpact):
										m_weight(a_weight),
										m_spImpact(a_spImpact)				{}

			Real					weight(void) const	{ return m_weight; }
			sp<SurfaceI::ImpactI>	impact(void) const	{ return m_spImpact; }

		private:
			Real					m_weight;
			sp<SurfaceI::ImpactI>	m_spImpact;
	};

	private:

		Real	calculateWeight(Real a_distance,Real a_peakDistance,
						Real a_distanceBias,Real a_distanceScale,
						Real a_minimum);

		sp<SurfaceAccessibleI>					m_spInputAccessible;
		sp<SurfaceAccessorI>					m_spFragmentAccessor;

		sp<SurfaceI>							m_spDeformed;
		sp<SurfaceI>							m_spRefEdged;
		sp<SurfaceI>							m_spDefEdged;
		sp<SurfaceI>							m_spOutput;

		sp<DrawI>								m_spDrawDebug;
		sp<DrawMode>							m_spDebugGroup;
		sp<DrawMode>							m_spOverlay;
		sp<DrawMode>							m_spBrush;
		sp<DrawMode>							m_spWireframe;

		BWORD									m_brushed;
		Real									m_lastFrame;
		I32										m_lastSubdiv;
		I32										m_lastBindToPoints;
		I32										m_lastEdgyDepth;
		I32										m_forceRecook;
		WindowEvent								m_event;
		I32										m_selectIndex;
		Array<Feedback>							m_feedback;
		BWORD									m_spread;
		I32										m_bindCount;
		I32										m_samples;
		String									m_locatorPrefix;
		I32										m_deformedFaceOffset;
		I32										m_outputElementOffset;

		BWORD									m_makeCache;
		Array< Array<I32> >						m_faceCaches;
		Array< Array<SpatialBary> >				m_baryCaches;
		Array< Array<SpatialTransform> >		m_driverSampleCaches;

		//* TODO store in Surface, read from Impact
		Array<Real>								m_curveLengthRef;
		Array<Real>								m_curveLengthDef;

		sp<Profiler>				m_spProfiler;
		sp<Profiler::Profile>		m_spProfileAccess;
		sp<Profiler::Profile>		m_spProfileDriver;
		sp<Profiler::Profile>		m_spProfileDeformed;
		sp<Profiler::Profile>		m_spProfilePrepare;
		sp<Profiler::Profile>		m_spProfileEdgy;
		sp<Profiler::Profile>		m_spProfileMoreAccess;
		sp<Profiler::Profile>		m_spProfileCache;
		sp<Profiler::Profile>		m_spProfileRun;
		sp<Profiler::Profile>		m_spProfileFinish;
		sp<Profiler::Profile>		m_spProfilePrep;
		sp<Profiler::Profile>		m_spProfilePreload;
		sp<Profiler::Profile>		m_spProfileIterate;
		sp<Profiler::Profile>		m_spProfileFragment;
		sp<Profiler::Profile>		m_spProfileFilter;
		sp<Profiler::Profile>		m_spProfileSpread;
		sp<Profiler::Profile>		m_spProfileDistance;
		sp<Profiler::Profile>		m_spProfileLoop;
		sp<Profiler::Profile>		m_spProfileBary;
		sp<Profiler::Profile>		m_spProfileSample;
		sp<Profiler::Profile>		m_spProfileInvert;
		sp<Profiler::Profile>		m_spProfileWeight;
		sp<Profiler::Profile>		m_spProfileTable;
		sp<Profiler::Profile>		m_spProfileTransfer;
		sp<Profiler::Profile>		m_spProfileResolve;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceWrapOp_h__ */
