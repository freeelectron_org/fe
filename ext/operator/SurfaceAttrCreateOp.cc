/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SAC_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAttrCreateOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("name")="attribute";
	catalog<String>("name","label")="Name";
	catalog<String>("name","hint")=
			"Name of attribute to create.";

	catalog<String>("class")="point";
	catalog<String>("class","choice:0")="point";
	catalog<String>("class","label:0")="Point";
	catalog<String>("class","choice:1")="vertex";
	catalog<String>("class","label:1")="Vertex";
	catalog<String>("class","choice:2")="primitive";
	catalog<String>("class","label:2")="Primitive";
	catalog<String>("class","choice:3")="detail";
	catalog<String>("class","label:3")="Detail";
	catalog<String>("class","label")="Class";
	catalog<String>("class","hint")=
			"Rate of attribute to create.";

	catalog<String>("type")="real";
	catalog<String>("type","choice:0")="real";
	catalog<String>("type","label:0")="Real";
	catalog<String>("type","choice:1")="integer";
	catalog<String>("type","label:1")="Integer";
	catalog<String>("type","choice:2")="vector";
	catalog<String>("type","label:2")="Vector";
	catalog<String>("type","choice:3")="string";
	catalog<String>("type","label:3")="String";
	catalog<String>("type","label")="Type";
	catalog<String>("type","hint")=
			"Data type of attribute to create.";

	catalog<String>("default");
	catalog<String>("default","label")="Default";
	catalog<String>("default","hint")=
			"The default value used for the new attribute,"
			" if it doesn't already exist and 'Write Values' is not used."
			"  The string will be converted to the chosen type.";

	catalog<bool>("writeValues")=false;
	catalog<String>("writeValues","label")="Write Values";
	catalog<bool>("writeValues","joined")=true;
	catalog<String>("writeValues","hint")=
			"Replace values if attribute already exists.";

	catalog<String>("value");
	catalog<String>("value","label")="Value";
	catalog<String>("value","enabler")="writeValues";
	catalog<String>("value","hint")=
			"The value used for the new attribute,"
			" if 'Write Values' is used."
			"  The string will be converted to the chosen type.";

	catalog<bool>("rampValue")=false;
	catalog<String>("rampValue","label")="Ramp";
	catalog<bool>("rampValue","joined")=true;
	catalog<String>("rampValue","hint")=
			"For real, apply a increasing values,"
			" starting from the regular entered value.";

	catalog<String>("rampTo");
	catalog<String>("rampTo","label")="To";
	catalog<String>("rampTo","enabler")="rampValue";
	catalog<bool>("rampTo","joined")=true;
	catalog<String>("rampValue","hint")=
			"For real, with 'Ramp' on, ramp up to this value.";

	catalog<Real>("rampGamma")=1.0;
	catalog<String>("rampGamma","label")="Gamma";
	catalog<String>("rampGamma","enabler")="rampValue";
	catalog<String>("rampValue","hint")=
			"When ramping, make the curve nonlinear by deviating away from 1.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void SurfaceAttrCreateOp::handle(Record& a_rSignal)
{
#if FE_SAC_DEBUG
	feLog("SurfaceAttrCreateOp::handle\n");
#endif

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	const String attrName=catalog<String>("name");
	catalog<String>("summary")=attrName;

	if(attrName.empty())
	{
		feLog("SurfaceAttrCreateOp::handle no name\n");
		return;
	}
	const String attrClass=catalog<String>("class");
	const String attrType=catalog<String>("type");
	const String& attrDefault=catalog<String>("default");

	const BWORD attrWriteValues=catalog<bool>("writeValues");
	const String& attrValue=catalog<String>("value");

	const BWORD rampValue=catalog<bool>("rampValue");
	const String& rampTo=catalog<String>("rampTo");
	const Real rampGamma=catalog<Real>("rampGamma");

#if FE_SAC_DEBUG
	feLog("SurfaceAttrCreateOp::handle"
			" \"%s\" \"%s\" \"%s\" \"%s\" %d \"%s\"\n",
			attrClass.c_str(),attrType.c_str(),attrName.c_str(),
			attrDefault.c_str(),attrWriteValues,attrValue.c_str());
#endif

	Element element;
	if(attrClass=="point")
	{
		element=e_point;
	}
	else if(attrClass=="vertex")
	{
		element=e_vertex;
	}
	else if(attrClass=="primitive")
	{
		element=e_primitive;
	}
	else if(attrClass=="detail")
	{
		element=e_detail;
	}
	else
	{
		feLog("SurfaceAttrCreateOp::handle unsupported class \"%s\"\n",
				attrClass.c_str());
		return;
	}

	const BWORD subCounted=(element==e_vertex);

	sp<SurfaceAccessorI> spOutputElement;
	access(spOutputElement,spOutputAccessible,element,
			attrName,e_quiet,e_refuseMissing);
	if(spOutputElement.isValid())
	{
		if(!attrWriteValues)
		{
			//* don't clobber existing data
			return;
		}
	}
	else
	{
		//* create new attribute
		if(!access(spOutputElement,spOutputAccessible,
				element,attrName)) return;
	}

	const String& attrSet=(attrWriteValues)? attrValue: attrDefault;

	const I32 elementCount=spOutputElement->count();

#if FE_SAC_DEBUG
	feLog("SurfaceAttrCreateOp::handle elementCount=%d\n",elementCount);
#endif

	if(attrType=="string")
	{
		for(I32 index=0;index<elementCount;index++)
		{
			const I32 subCount=subCounted?
					spOutputElement->subCount(index): 1;
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
#if FE_SAC_DEBUG
				feLog("SurfaceAttrCreateOp::handle set %d %d \"%s\"\n",
						index,subIndex,attrSet.c_str());
#endif
				spOutputElement->set(index,subIndex,attrSet);
			}
		}
	}
	else if(attrType=="integer")
	{
		const I32 value=atoi(attrSet.c_str());
		for(I32 index=0;index<elementCount;index++)
		{
			const I32 subCount=subCounted?
					spOutputElement->subCount(index): 1;
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
#if FE_SAC_DEBUG
				feLog("SurfaceAttrCreateOp::handle set %d %d %d\n",
						index,subIndex,value);
#endif
				spOutputElement->set(index,subIndex,value);
			}
		}
	}
	else if(attrType=="real")
	{
		const Real value=atof(attrSet.c_str());

		if(rampValue)
		{
			const Real rampRange=atof(rampTo.c_str())-value;

			for(I32 index=0;index<elementCount;index++)
			{
				const Real ramped=value+
						rampRange*pow((index/Real(elementCount-1)),rampGamma);

				const I32 subCount=subCounted?
						spOutputElement->subCount(index): 1;
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
#if FE_SAC_DEBUG
					feLog("SurfaceAttrCreateOp::handle set %d %d %.6G\n",
							index,subIndex,ramped);
#endif

					spOutputElement->set(index,subIndex,ramped);
				}
			}
		}
		else
		{
			for(I32 index=0;index<elementCount;index++)
			{
				const I32 subCount=subCounted?
						spOutputElement->subCount(index): 1;
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
#if FE_SAC_DEBUG
					feLog("SurfaceAttrCreateOp::handle set %d %d %.6G\n",
							index,subIndex,value);
#endif
					spOutputElement->set(index,subIndex,value);
				}
			}
		}
	}
	else if(attrType=="vector")
	{
		double value0(0);
		double value1(0);
		double value2(0);

		sscanf(attrSet.c_str(),"%lf %lf %lf",&value0,&value1,&value2);

		const SpatialVector defaultVector(value0,value1,value2);

		for(I32 index=0;index<elementCount;index++)
		{
			const I32 subCount=subCounted?
					spOutputElement->subCount(index): 1;
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
#if FE_SAC_DEBUG
				feLog("SurfaceAttrCreateOp::handle set %d %d %s\n",
						index,subIndex,c_print(defaultVector));
#endif

				spOutputElement->set(index,subIndex,defaultVector);
			}
		}
	}

#if FE_SAC_DEBUG
	feLog("SurfaceAttrCreateOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
