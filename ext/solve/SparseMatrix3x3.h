/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __SparseMatrix3x3_h__
#define __SparseMatrix3x3_h__

namespace fe
{
namespace ext
{

template <typename T>
class FE_DL_EXPORT SparseMatrix3x3 : public Component,
	public CastableAs< SparseMatrix3x3<T> >
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;

virtual	~SparseMatrix3x3(void) {}
virtual	void multiply(std::vector<t_v3> &a_b,
			const std::vector<t_v3> &a_x) const							= 0;
virtual	void scale(t_real a_scale)										= 0;
};

/// Vector Map Rows (vector of row maps)
class FE_DL_EXPORT FullVMR : public SparseMatrix3x3<Real>
{
	public:
		FullVMR(void);
virtual	~FullVMR(void);

virtual	void multiply(std::vector<SpatialVector> &a_b,
			const std::vector<SpatialVector> &a_x) const;
virtual	void scale(Real a_scale);

		typedef std::map<unsigned int, SpatialMatrix> t_row;

		void			clear(void);
		void			setRows(unsigned int a_count);
		unsigned int	rows(void);
		t_row			&row(unsigned int a_index);
		void			zero(void);

		SpatialMatrix	&block(unsigned int a_i, unsigned int a_j);

	protected:
		std::vector<t_row>	m_rows;

};

/// Upper Triangular Vector Map Rows (vector of row maps)
class FE_DL_EXPORT UpperTriangularVMR : public FullVMR
{
	public:
		UpperTriangularVMR(void) {}
virtual	~UpperTriangularVMR(void) {}

virtual	void multiply(std::vector<SpatialVector> &a_b,
			const std::vector<SpatialVector> &a_x) const;
	private:

};

template <typename T>
class FE_DL_EXPORT CRS
{
	public:
		CRS(void){}
virtual	~CRS(void){}

		std::vector<T>		m_values;
		std::vector<int>	m_colind;
		std::vector<int>	m_rowptr;
		unsigned int		m_r;
};

template <typename T>
class FE_DL_EXPORT FullBCRS : public SparseMatrix3x3<T>
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;
		FullBCRS(void);
virtual	~FullBCRS(void);


virtual	void multiply(std::vector<t_v3> &a_b,
			const std::vector<t_v3> &a_x) const;
virtual	void scale(t_real a_scale);

		void				clear(void);
		void				startrow(void);
		void				done(void);
		t_matrix		&insert(unsigned int a_columnIndex,
								const t_matrix &a_block);

		void				dump(void) const;

		void				write(const String &a_filename);
		void				read(const String &a_filename);

		void				writeDense(const String &a_filename);
		void				readDense(const String &a_filename, unsigned int a_M, unsigned int a_N);

		void				writeStructure(const String &a_filename) const;

		void				sparse(const sp<FullBCRS> &a_other);
		void				project(const sp<FullBCRS> &a_other);

		void				write(CRS<T> &aCRS) const;
		void				read(const CRS<T> &aCRS);


	public:
		std::vector<t_matrix>	&blocks(void) { return m_blocks; }
		std::vector<unsigned int>	&colind(void) { return m_colind; }
		std::vector<unsigned int>	&rowptr(void) { return m_rowptr; }

	protected:
		std::vector<t_matrix>			m_blocks;
		std::vector<unsigned int>		m_colind;
		std::vector<unsigned int>		m_rowptr;
		unsigned int					m_r;
		t_v3							m_zeroVector;
};

template <typename T>
class FE_DL_EXPORT UpperTriangularBCRS : public FullBCRS<T>
{
	public:
typedef T					t_real;
typedef Vector<3, t_real>	t_v3;
typedef Matrix<3,3,t_real>	t_matrix;
		UpperTriangularBCRS(void){}
virtual	~UpperTriangularBCRS(void){}

virtual	void multiply(std::vector<t_v3> &a_b,
			const std::vector<t_v3> &a_x) const;

		void incompleteSqrt(void);
		void backSub(std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b);
		void transposeForeSub(std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b);
		void backSolve(std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b);

};



template <typename T>
inline FullBCRS<T>::FullBCRS(void)
{
	m_r = 0;
	m_zeroVector = t_v3(0.0,0.0,0.0);
}

template <typename T>
inline FullBCRS<T>::~FullBCRS(void)
{
}

template <typename T>
inline void FullBCRS<T>::clear(void)
{
	m_blocks.clear();
	m_colind.clear();
	m_rowptr.clear();
	m_r = 0;
}

template <typename T>
inline void FullBCRS<T>::startrow(void)
{
	m_rowptr.push_back(m_r);
}

template <typename T>
inline void FullBCRS<T>::done(void)
{
	m_rowptr.push_back(m_r);
}

template <typename T>
inline Matrix<3,3,T> &FullBCRS<T>::insert(unsigned int a_columnIndex, const t_matrix &a_block)
{
	m_blocks.push_back(a_block);
	m_colind.push_back(a_columnIndex);
	m_r++;
	return m_blocks.back();
}

template <typename T>
inline void FullBCRS<T>::multiply(std::vector<t_v3> &a_b, const std::vector<t_v3> &a_x) const
{
	unsigned int n = m_rowptr.size() - 1;
	for(unsigned int i = 0; i < n; i++)
	{
		a_b[i] = m_zeroVector;
		for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++)
		{
			a_b[i] += fe::multiply(m_blocks[k], a_x[m_colind[k]]);
		}
	}
}

template <typename T>
inline void FullBCRS<T>::dump(void) const
{
	t_matrix offdiagonal_sum;
	setAll(offdiagonal_sum, 0.0);
	unsigned int n = m_rowptr.size() - 1;
	for(unsigned int i = 0; i < n; i++)
	{
		for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++)
		{
			if(m_colind[k] != i) // diagonal
			{
				offdiagonal_sum = offdiagonal_sum + m_blocks[k];
			}
		}
	}

	String s;
	s = print(offdiagonal_sum);
	feLogGroup("BCRS", "off diagonal\n%s\n", s.c_str());
}

template <typename T>
inline void FullBCRS<T>::writeStructure(const String &a_filename) const
{
	FILE *fp = fopen(a_filename.c_str(), "w");
	if(!fp)
	{
		feLog("WARNING: FullBCRS::writeStructure failed\n");
		return;
	}
	t_matrix zero;
	setAll(zero, 0.0);
	unsigned int n = m_rowptr.size() - 1;
	for(unsigned int i = 0; i < n; i++)
	{
		unsigned int j = 0;
		for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++)
		{
			while(m_colind[k] > j)
			{
				fe_fprintf(fp, "-");
				j++;
			}
			if(m_blocks[k] == zero)
			{
				fe_fprintf(fp, "0");
			}
			else
			{
				fe_fprintf(fp, "*");
			}
			j++;
		}
		while(j<n)
		{
			fe_fprintf(fp, "-");
			j++;
		}
		fe_fprintf(fp, "\n");
	}
	fclose(fp);
}

template <typename T>
inline void FullBCRS<T>::write(const String &a_filename)
{
	FILE *fp = fopen(a_filename.c_str(), "w");
	if(!fp)
	{
		feLog("WARNING: FullBCRS::write failed\n");
		return;
	}
	fe_fprintf(fp, "%d\n", (int)m_blocks.size());
	for(unsigned int i = 0; i < m_blocks.size(); i++)
	{
		fe_fprintf(fp, "%g %g %g\n", m_blocks[i](0,0), m_blocks[i](0,1), m_blocks[i](0,2));
		fe_fprintf(fp, "%g %g %g\n", m_blocks[i](1,0), m_blocks[i](1,1), m_blocks[i](1,2));
		fe_fprintf(fp, "%g %g %g\n", m_blocks[i](2,0), m_blocks[i](2,1), m_blocks[i](2,2));
	}
	fe_fprintf(fp, "%d\n", (int)m_rowptr.size());
	for(unsigned int i = 0; i < m_rowptr.size(); i++)
	{
		fe_fprintf(fp, "%d ", m_rowptr[i]);
	}
	fe_fprintf(fp, "\n");

	fe_fprintf(fp, "%d\n", (int)m_colind.size());
	for(unsigned int i = 0; i < m_colind.size(); i++)
	{
		fe_fprintf(fp, "%d ", m_colind[i]);
	}
	fe_fprintf(fp, "\n");

	fe_fprintf(fp, "%d\n", m_r);
	fclose(fp);
}

template <typename T>
inline void FullBCRS<T>::read(const String &a_filename)
{
	std::ifstream infile(a_filename.c_str());
	int m;
	infile >> m;
	m_blocks.resize(m);
	for(int i = 0; i < m; i++)
	{
		infile >> m_blocks[i](0,0);
		infile >> m_blocks[i](0,1);
		infile >> m_blocks[i](0,2);
		infile >> m_blocks[i](1,0);
		infile >> m_blocks[i](1,1);
		infile >> m_blocks[i](1,2);
		infile >> m_blocks[i](2,0);
		infile >> m_blocks[i](2,1);
		infile >> m_blocks[i](2,2);
	}

	infile >> m;
	m_rowptr.resize(m);
	for(int i = 0; i < m; i++)
	{
		infile >> m_rowptr[i];
	}

	infile >> m;
	m_colind.resize(m);
	for(int i = 0; i < m; i++)
	{
		infile >> m_colind[i];
	}

	infile >> m_r;

	infile.close();
}

template <typename T>
inline void FullBCRS<T>::writeDense(const String &a_filename)
{
	FILE *fp = fopen(a_filename.c_str(), "w");
	if(!fp)
	{
		feLog("WARNING: FullBCRS::write failed\n");
		return;
	}
	unsigned int n = m_rowptr.size() - 1;

	std::vector< std::vector<double> > dense_matrix;
	dense_matrix.resize(n*3);
	for(unsigned int i = 0; i < n*3; i++)
	{
		dense_matrix[i].resize(n*3);
	}

#define FMT "  % 10.8e	% 10.8e  % 10.8e"
#if 0
	for(unsigned int i = 0; i < n; i++)
	{
		for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++)
		{
			unsigned int j = m_colind[k];

			for(unsigned int ii = 0; ii < 3; ii++)
			{
				for(unsigned int jj = 0; jj < 3; jj++)
				{
					unsigned int m = i*3 + ii;
					unsigned int n = j*3 + jj;
					dense_matrix[m][n] = m_blocks[k](ii,jj);
					dense_matrix[n][m] = dense_matrix[m][n];
				}
			}
		}
	}

	for(unsigned int i = 0; i < n*3; i++)
	{
		for(unsigned int j = 0; j < n*3; j++)
		{
			fe_fprintf(fp, "%+10.8e ", dense_matrix[i][j]);
		}
		fe_fprintf(fp, "\n");
	}
#endif

#if 1
	for(unsigned int i = 0; i < n; i++)
	{
		for(unsigned int r = 0; r < 3; r++)
		{
			unsigned int m = 0;
			for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++)
			{
				unsigned int c = m_colind[k];
				while(m < c)
				{
					m++;
					fe_fprintf(fp, FMT, 0.0, 0.0, 0.0);
				}
				m++;
				fe_fprintf(fp, FMT, m_blocks[k](r,0), m_blocks[k](r,1),  m_blocks[k](r,2));
			}
			while(m<n)
			{
				m++;
				fe_fprintf(fp, FMT, 0.0, 0.0, 0.0);
			}
			fe_fprintf(fp, "\n");
		}
	}
#endif

	fclose(fp);
}

template <typename T>
inline void FullBCRS<T>::readDense(const String &a_filename, unsigned int a_M, unsigned int a_N)
{
	m_blocks.clear();
	m_colind.clear();
	m_rowptr.clear();
	for(unsigned int i = 0; i < a_M; i++)
	{
		m_rowptr.push_back(m_colind.size());
		for(unsigned int j = 0; j < a_N; j++)
		{
			if(j>=i)
			{
				m_colind.push_back(j);
			}
		}
	}
	m_rowptr.push_back(m_colind.size());
	m_r = m_colind.size();
	m_blocks.resize(m_r);

	std::ifstream infile(a_filename.c_str());
	unsigned int k = 0;
	for(unsigned int i = 0; i < a_M; i++)
	{
		unsigned int kk = k;
		for(unsigned int ii = 0; ii < 3; ii++)
		{
			k = kk;
			for(unsigned int j = 0; j < a_N; j++)
			{
				for(unsigned int jj = 0; jj < 3; jj++)
				{
					if(j>=i)
					{
						infile >> m_blocks[k](ii,jj);
					}
					else
					{
						double dbl;
						infile >> dbl;
					}
				}
				if(j>=i){k++;}
			}
		}
	}

	infile.close();
}

template <typename T>
inline void FullBCRS<T>::write(CRS<T> &aCRS) const
{
	unsigned int n = m_rowptr.size() - 1;

	aCRS.m_values.resize(m_blocks.size() * 9);
	aCRS.m_colind.resize(m_blocks.size() * 9);
	aCRS.m_rowptr.resize(n*3 + 1);

	unsigned int ii = 0;
	for(unsigned int i = 0; i < n; i++) // block rows
	{
		aCRS.m_rowptr[i] = ii;
		for(unsigned int r = 0; r < 3; r++) // unit rows
		{
			unsigned int m = i*3 + r;
			for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++) // blocks
			{
				unsigned int c = m_colind[k];
				aCRS.m_colind[ii + 0] = c*3 + 0;
				aCRS.m_colind[ii + 1] = c*3 + 1;
				aCRS.m_colind[ii + 2] = c*3 + 2;

				aCRS.m_values[ii + 0] = m_blocks[k](r, 0);
				aCRS.m_values[ii + 1] = m_blocks[k](r, 1);
				aCRS.m_values[ii + 2] = m_blocks[k](r, 2);

				ii += 3;
			}
		}
	}
	aCRS.m_rowptr[n] = ii;
	assert(ii == aCRS.m_values.size());


#if 0
	for(unsigned int i = 0; i < n; i++)
	{
		for(unsigned int r = 0; r < 3; r++)
		{
			unsigned int m = 0;
			for(unsigned int k = m_rowptr[i]; k < m_rowptr[i+1]; k++)
			{
				unsigned int c = m_colind[k];
				while(m < c)
				{
					m++;
					fe_fprintf(fp, FMT, 0.0, 0.0, 0.0);
				}
				m++;
				fe_fprintf(fp, FMT, m_blocks[k](r,0), m_blocks[k](r,1),  m_blocks[k](r,2));
			}
			while(m<n)
			{
				m++;
				fe_fprintf(fp, FMT, 0.0, 0.0, 0.0);
			}
			fe_fprintf(fp, "\n");
		}
	}
#endif

}

template <typename T>
inline void FullBCRS<T>::read(const CRS<T> &aCRS)
{
}


template <typename T>
inline void FullBCRS<T>::scale(t_real a_scale)
{
	unsigned int n = m_blocks.size();
	for(unsigned int i = 0; i < n; i++)
	{
		m_blocks[i] *= a_scale;
	}
}

template <typename T>
inline void UpperTriangularBCRS<T>::multiply(std::vector<t_v3> &a_b, const std::vector<t_v3> &a_x) const
{
	unsigned int n = FullBCRS<T>::m_rowptr.size() - 1;
	for(unsigned int i = 0; i < n; i++)
	{
		a_b[i] = FullBCRS<T>::m_zeroVector;
	}
	for(unsigned int i = 0; i < n; i++)
	{
		unsigned int k = FullBCRS<T>::m_rowptr[i];
		a_b[i] += fe::multiply(FullBCRS<T>::m_blocks[k], a_x[i]);
		for(k=k+1; k < FullBCRS<T>::m_rowptr[i+1]; k++)
		{
			a_b[i] += fe::multiply(FullBCRS<T>::m_blocks[k], a_x[FullBCRS<T>::m_colind[k]]);
			a_b[FullBCRS<T>::m_colind[k]] += fe::transposeMultiply(FullBCRS<T>::m_blocks[k], a_x[i]);
		}
	}
}

template <typename T>
inline void just_upper(Matrix<3,3,T> &a_m)
{
	a_m(1,0) = 0.0;
	a_m(2,0) = 0.0;
	a_m(2,1) = 0.0;
}


template <typename T>
inline void FullBCRS<T>::sparse(const sp<FullBCRS> &a_other)
{
	m_blocks.clear();
	m_colind.clear();
	m_rowptr.clear();
	m_r = 0;
	t_matrix zero;
	setAll(zero, 0.0);
	unsigned int n = a_other->m_rowptr.size()-1;
	for(unsigned int i = 0; i < n; i++)
	{
		startrow();
		int d = a_other->m_rowptr[i];
		for(int k=d; k < a_other->m_rowptr[i+1]; k++)
		{
			int c = a_other->m_colind[k];
			if(!(zero == a_other->m_blocks[k]))
			{
				insert(c, a_other->m_blocks[k]);
			}
		}
	}
	done();
}

template <typename T>
inline void FullBCRS<T>::project(const sp<FullBCRS> &a_other)
{
	unsigned int n = a_other->m_rowptr.size()-1;
	for(unsigned int i = 0; i < n; i++)
	{
		int d = a_other->m_rowptr[i];
		int d_self = m_rowptr[i];
		for(int k=d; k < a_other->m_rowptr[i+1]; k++)
		{
			int c = a_other->m_colind[k];
			for(int k_self=d_self; k_self < m_rowptr[i+1]; k_self++)
			{
				int c_self = m_colind[k_self];
				if(c_self == c)
				{
					m_blocks[k_self] = a_other->m_blocks[k];
				}
			}
		}
	}
}

template <typename T>
inline void UpperTriangularBCRS<T>::backSub(std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b)
{
#if 0
t_matrix zero;
setAll(zero, 0.0);
#endif
	unsigned int n = FullBCRS<T>::m_rowptr.size()-1;
//fe_fprintf(stderr, "root %d %d\n", n-1, m_colind[m_rowptr[n-1]]);
	fe::backSub(FullBCRS<T>::m_blocks[FullBCRS<T>::m_rowptr[n-1]], a_x[n-1], a_b[n-1]);
	t_v3 tmp;

	for(int i = n-2; i >= 0; i--)
	{
//fe_fprintf(stderr, "bs i %d\n", i);
		a_x[i] = a_b[i];
		int d = FullBCRS<T>::m_rowptr[i];
		for(unsigned int k=d+1; k < FullBCRS<T>::m_rowptr[i+1]; k++)
		{
			int c = FullBCRS<T>::m_colind[k];
			fe::multiply(tmp, FullBCRS<T>::m_blocks[k], a_x[c]);
			a_x[i] -= tmp;
#if 0
if(! (zero == m_blocks[k]))
{
fe_fprintf(stderr, "bs c %d -- %d <- T %d\n", c, i, k);
fprint(stderr, m_blocks[k]);
fe_fprintf(stderr, "\n");
}
#endif
			//a_x[i] -= fe::transposeMultiply(m_blocks[k], a_x[c]);
		}
		fe::backSub(FullBCRS<T>::m_blocks[d], a_x[i], a_x[i]);
//fprint(stderr, m_blocks[d]);
//fe_fprintf(stderr, "\n");
	}
}

// operates on upper tri as if it is a transpose of the upper and is lower
template <typename T>
inline void UpperTriangularBCRS<T>::transposeForeSub(std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b)
{
//t_matrix zero;
//setAll(zero, 0.0);
	unsigned int n = FullBCRS<T>::m_rowptr.size()-1;
	std::vector<unsigned int> ptr = FullBCRS<T>::m_rowptr;
	fe::transposeForeSub(FullBCRS<T>::m_blocks[FullBCRS<T>::m_rowptr[0]], a_x[0], a_b[0]);
	t_v3 tmp;

	for(unsigned int i = 1; i < n; i++)
	{
//fe_fprintf(stderr, "fs i %d\n", i);
		a_x[i] = a_b[i];
		for(unsigned int m = 0; m < i; m++)
		{
			unsigned int c = FullBCRS<T>::m_colind[ptr[m]];
			while(c < i && ptr[m] < FullBCRS<T>::m_rowptr[m+1]-1)
			{
				ptr[m]++;
				c = FullBCRS<T>::m_colind[ptr[m]];
			}
			if(c == i)
			{
//if(! (zero == m_blocks[ptr[m]]))
//{
//fe_fprintf(stderr, "fs m %d -- i %d <- T ptr[m] %d m_rowptr[m+1] %d\n", m, i, ptr[m], m_rowptr[m+1]);
//fprint(stderr, m_blocks[ptr[m]]);
//fe_fprintf(stderr, "\n");
//}
				//fe::multiply(tmp, m_blocks[ptr[m]], a_x[m]);
				//a_x[i] -= tmp;
				a_x[i] -= fe::transposeMultiply(FullBCRS<T>::m_blocks[ptr[m]], a_x[m]);
			}
		}
//fe_fprintf(stderr, "fs m_rowptr[%d] %d col %d\n", i, m_rowptr[i], m_colind[m_rowptr[i]]);
		fe::transposeForeSub(FullBCRS<T>::m_blocks[FullBCRS<T>::m_rowptr[i]], a_x[i], a_x[i]);
//fprint(stderr, m_blocks[m_rowptr[i]]);
//fe_fprintf(stderr, "\n");
	}
}

template <typename T>
inline void UpperTriangularBCRS<T>::backSolve(std::vector<t_v3> &a_x, const std::vector<t_v3> &a_b)
{
	std::vector<t_v3> y(a_x.size());

	transposeForeSub(y, a_b);
	backSub(a_x, y);
}

#if 0
inline bool nanCheck(const t_matrix &aM, const char *aMsg)
{
	for(unsigned int i = 0; i < 3; i++)
	{
		for(unsigned int j = 0; j < 3; j++)
		{
			if(isnan(aM(i,j)))
			{
				fe_fprintf(stderr, "M: %s\n", aMsg);
				exit(101);
				return false;
			}
		}
	}
	return true;
}

inline bool nanCheck(const t_v3 &aV, const char *aMsg)
{
	for(unsigned int i = 0; i < 3; i++)
	{
		if(isnan(aV[i]))
		{
			fe_fprintf(stderr, "V: %s\n", aMsg);
			exit(101);
			return false;
		}
	}
	return true;
}
#endif

template <typename T>
inline void UpperTriangularBCRS<T>::incompleteSqrt(void)
{
	unsigned int d;
	unsigned int n = FullBCRS<T>::m_rowptr.size()-1;
//fe_fprintf(stderr, "%d rows\n", n);
	for(unsigned int k = 0; k < n-1; k++)
	{
		d = FullBCRS<T>::m_rowptr[k];
		t_matrix z;
		just_upper(FullBCRS<T>::m_blocks[d]);
//fe_fprintf(stderr, "WA %d attempt (row %d)\n", d, k);
		//t_matrix tz = m_blocks[d] + transpose(m_blocks[d]);
		//fprint(stderr, m_blocks[d]); fe_fprintf(stderr, "\n");
		//nanCheck(m_blocks[d], "pre squareroot");
		squareroot(FullBCRS<T>::m_blocks[d], FullBCRS<T>::m_blocks[d]);
		//nanCheck(m_blocks[d], "post squareroot");
		z = FullBCRS<T>::m_blocks[d];
		//fprint(stderr, m_blocks[d]); fe_fprintf(stderr, "\n");

		just_upper(z);

		//t_v3 sv(0.2,0.5,0.7);
		//t_v3 x, y;

		t_matrix inv_z;
		invert(inv_z, z);

#if 0
//fprint(stderr, tz); fe_fprintf(stderr, "\n");
		t_matrix inv_tz;
		invert(inv_tz, tz);
		invert(tz, inv_tz);
//fprint(stderr, tz); fe_fprintf(stderr, "\n");
//fprint(stderr, inv_tz); fe_fprintf(stderr, "\n");
		fe::multiply(x, inv_tz, sv);
		//fe_fprintf(stderr, "via inv %f %f %f\n", x[0], x[1], x[2]);
		t_v3 x_sv;
		fe::multiply(x_sv, tz, x);
		//fe_fprintf(stderr, "via inv back to sv %f %f %f\n", x_sv[0], x_sv[1], x_sv[2]);

		//fe::backSub(z, x, sv);
		squareroot(z, tz);
		t_matrix re_tz;
		fe::multiply(re_tz, transpose(z), z);
//fprint(stderr, re_tz); fe_fprintf(stderr, "\n");
		//setTranspose(z);
		fe::transposeForeSub(z, y, sv);
		fe::backSub(z, x, y);
		//fe::transposeForeSub(z, x, y);
		//fe_fprintf(stderr, "via bs  %f %f %f\n", x[0], x[1], x[2]);
		fe::multiply(x_sv, tz, x);
		//fe_fprintf(stderr, "via bs back to sv %f %f %f\n", x_sv[0], x_sv[1], x_sv[2]);
//fe_fprintf(stderr, "-----------------------------------------------\n");
#endif

		for(unsigned int i=d+1; i < FullBCRS<T>::m_rowptr[k+1]; i++)
		{
//fe_fprintf(stderr, "WB %d\n", i);
			//fe::multiply(m_blocks[i], inv_z, m_blocks[i]);
			//fprint(stderr, m_blocks[i]); fe_fprintf(stderr, "\n");
			//fprint(stderr, inv_z); fe_fprintf(stderr, "\n");

			//nanCheck(m_blocks[i], "pre multiply A");
			//nanCheck(inv_z, "pre multiply inv_z");
			fe::multiply(z, FullBCRS<T>::m_blocks[i], inv_z);
			setTranspose(z);

			//fe::multiply(m_blocks[i], inv_z, m_blocks[i]);
			//setTranspose(m_blocks[i]);

			//fprint(stderr, m_blocks[i]); fe_fprintf(stderr, "\n");
			//fprint(stderr, z); fe_fprintf(stderr, "\n");
			FullBCRS<T>::m_blocks[i] = z;
		}

		for(unsigned int i=d+1; i < FullBCRS<T>::m_rowptr[k+1]; i++)
		{
//fe_fprintf(stderr, "i %d\n", i);
			z = transpose(FullBCRS<T>::m_blocks[i]);
			//z = m_blocks[i];
			unsigned int h = FullBCRS<T>::m_colind[i];
			unsigned int g = i;

			for(unsigned int j = FullBCRS<T>::m_rowptr[h]; j < FullBCRS<T>::m_rowptr[h+1]; j++)
			{
//fe_fprintf(stderr, "j %d   (%d < %d)   (%d <= %d)\n", j, g, m_rowptr[k+1], m_colind[g+1], m_colind[j]);
				for(; g < FullBCRS<T>::m_rowptr[k+1] && FullBCRS<T>::m_colind[g] <= FullBCRS<T>::m_colind[j]; g++)
				{
//fe_fprintf(stderr, "		g (%d < %d)   (%d <= %d)\n", g, m_rowptr[k+1], m_colind[g+1], m_colind[j]);
					if (FullBCRS<T>::m_colind[g] == FullBCRS<T>::m_colind[j])
					{
//fe_fprintf(stderr, "WC[%d,%d] %d h: %d col: %d\n", i, g, j, h, m_colind[g]);
						t_matrix t;
						//nanCheck(m_blocks[i], "pre multiply B");
						fe::multiply(t, z, FullBCRS<T>::m_blocks[g]);
						//nanCheck(m_blocks[i], "post multiply BA");
						//nanCheck(z, "post multiply BA");
						//nanCheck(t, "post multiply BA");
						setTranspose(t);
						subtract(FullBCRS<T>::m_blocks[j], FullBCRS<T>::m_blocks[j], t);
						//fprint(stderr, m_blocks[j]); fe_fprintf(stderr, "\n");
					}
				}
			}
		}
	}
	d = FullBCRS<T>::m_rowptr[n-1];
//fe_fprintf(stderr, "==------------------------ pre %d %d\n", d, m_blocks.size());
	t_matrix tmp;
	just_upper(FullBCRS<T>::m_blocks[d]);
	//fprint(stderr, m_blocks[d]);
	//fe_fprintf(stderr, "\n");
	squareroot(tmp, FullBCRS<T>::m_blocks[d]);
//fe_fprintf(stderr, "==------------------------\n");
	//fprint(stderr, tmp);
	//fe_fprintf(stderr, "\n");
	FullBCRS<T>::m_blocks[d] = tmp;
//fe_fprintf(stderr, "==------------------------ post\n");
}


inline FullVMR::FullVMR(void)
{
}

inline FullVMR::~FullVMR(void)
{
}

inline void UpperTriangularVMR::multiply(std::vector<t_v3> &a_b, const std::vector<t_v3> &a_x) const
{
	unsigned int n = a_b.size();
	for(unsigned int i = 0; i < n; i++)
	{
		a_b[i] = zeroVector;
	}

	unsigned int r,c;
	n = m_rows.size();
	for(unsigned int i = 0; i < n; i++)
	{
		t_row::const_iterator i_row = m_rows[i].begin();
		r = i;
		const SpatialMatrix &block = (*i_row).second;
		assert((*i_row).first == r);
		a_b[r] += fe::multiply(block, a_x[r]);
		i_row++;

		for(;i_row != m_rows[i].end(); i_row++)
		{
			const SpatialMatrix &block = (*i_row).second;
			c = (*i_row).first;

			a_b[r] += fe::multiply(block, a_x[c]);
			a_b[c] += fe::transposeMultiply(block, a_x[r]);
		}
	}
}

inline void FullVMR::multiply(std::vector<t_v3> &a_b, const std::vector<t_v3> &a_x) const
{
	unsigned int r,c;
	unsigned int n = m_rows.size();
	for(unsigned int i = 0; i < n; i++)
	{
		a_b[i] = zeroVector;
		r = i;
		for(t_row::const_iterator i_row = m_rows[i].begin();
			i_row != m_rows[i].end(); i_row++)
		{
			const SpatialMatrix &block = (*i_row).second;
			c = (*i_row).first;
			a_b[r] += fe::multiply(block, a_x[c]);
		}
	}
}

inline void FullVMR::zero(void)
{
	unsigned int n = m_rows.size();
	for(unsigned int i = 0; i < n; i++)
	{
		for(t_row::iterator i_row = m_rows[i].begin();
			i_row != m_rows[i].end(); i_row++)
		{
			SpatialMatrix &block = (*i_row).second;
			setAll(block, 0.0);
		}
	}
}

inline void FullVMR::scale(t_real a_scale)
{
	unsigned int n = m_rows.size();
	for(unsigned int i = 0; i < n; i++)
	{
		for(t_row::iterator i_row = m_rows[i].begin();
			i_row != m_rows[i].end(); i_row++)
		{
			SpatialMatrix &block = (*i_row).second;
			fe::multiply(block, a_scale);
		}
	}
}

inline void FullVMR::clear(void)
{
	m_rows.clear();
}

inline void FullVMR::setRows(unsigned int a_count)
{
	m_rows.resize(a_count);
}

inline unsigned int FullVMR::rows(void)
{
	return m_rows.size();
}

inline UpperTriangularVMR::t_row &FullVMR::row(unsigned int a_index)
{
	return m_rows[a_index];
}

inline SpatialMatrix &FullVMR::block(unsigned int a_i, unsigned int a_j)
{
	//assert(a_i < m_rows.size());
	t_row::iterator i_row = m_rows[a_i].find(a_j);
	if(i_row != m_rows[a_i].end())
	{
		return i_row->second;
	}
	SpatialMatrix &m = m_rows[a_i][a_j];
	setAll(m, 0.0);
	return m;
}


} /* namespace ext */
} /* namespace fe */

#endif /* __SparseMatrix3x3_h__ */

