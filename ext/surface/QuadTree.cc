/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

// TODO verify all allocation has matching deletes

#define FE_QT_VERBOSE			FALSE
#define FE_QT_DEBUG				FALSE

#define FE_QT_MT_DEBUG			FALSE
#define FE_QT_MT_LEVEL			1

#define FE_QT_DRAW_NORMALS		FALSE
#define FE_QT_DRAW_SPHERES		FALSE
#define FE_QT_DRAW_SUBDIV		TRUE
#define FE_QT_RAY_SUBDIV		TRUE	//* TODO param
#define FE_QT_DRAW_TEXT			FALSE
#define FE_QT_CLUMP_NEARBY		FALSE
#define FE_QT_FAST_BOUNDS		TRUE

#define FE_QT_LOCKING			TRUE
#define FE_QT_SUBDIV_AGE		TRUE
#define FE_QT_SUBDIV_AGE_MAX	2

#define FE_QT_CHECK				FALSE	// (FE_CODEGEN<=FE_DEBUG)

namespace fe
{
namespace ext
{

QuadTree::QuadTree(void):
	m_divisions(2,2,1),
	m_capacity(32),
	m_subdivMax(0),
	m_ageStamp(0.0),
	m_timeStamp(0.0)
{
//	feLog("QuadTree::QuadTree\n");

	m_spRootNode=new Node;
	m_spRootNode->setName("QuadTree::Node root");

	m_spWireframe=new DrawMode;

	String value;
	if(System::getEnvironmentVariable("FE_SUBDIV", value))
	{
		m_subdivMax=atoi(value.c_str());
	}
}

QuadTree::~QuadTree(void)
{
	if(m_spRootNode.isValid())
	{
		deallocate(m_spRootNode->m_pKeyArray);
		m_spRootNode->m_pKeyArray=NULL;
	}
}

//* TODO use the mutable flag
void QuadTree::lockSafe(void)
{
#if FE_QT_LOCKING
	if(m_subdivMax>0)
	{
		safeLockShared();
	}
#endif
}

void QuadTree::unlockSafe(void)
{
#if FE_QT_LOCKING
	if(m_subdivMax>0)
	{
		safeUnlockShared();
	}
#endif
}

//* static
void QuadTree::expandBox(SpatialVector& a_rMin,SpatialVector& a_rMax,
	const SpatialVector& a_rLowPoint,const SpatialVector& a_rHighPoint)
{
	if(a_rMin[0]>a_rLowPoint[0])
	{
		a_rMin[0]=a_rLowPoint[0];
	}
	if(a_rMin[1]>a_rLowPoint[1])
	{
		a_rMin[1]=a_rLowPoint[1];
	}
	if(a_rMin[2]>a_rLowPoint[2])
	{
		a_rMin[2]=a_rLowPoint[2];
	}
	if(a_rMax[0]<a_rHighPoint[0])
	{
		a_rMax[0]=a_rHighPoint[0];
	}
	if(a_rMax[1]<a_rHighPoint[1])
	{
		a_rMax[1]=a_rHighPoint[1];
	}
	if(a_rMax[2]<a_rHighPoint[2])
	{
		a_rMax[2]=a_rHighPoint[2];
	}
}

void QuadTree::populate(
	const Vector3i* a_pElementArray,
	const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,
	const Vector2* a_pUVArray,
	const Color* a_pColorArray,
	const I32* a_pTriangleIndexArray,
	const I32* a_pPointIndexArray,
	const I32* a_pPrimitiveIndexArray,
	const I32* a_pPartitionIndexArray,
	U32 a_primitives,U32 a_vertices,
	const SpatialVector& a_center,F32 a_radius)
{
	if(!a_vertices)
	{
		feX(e_cannotCreate,"QuadTree::populate","no vertices");
	}

	//* TODO cache a copy?
	m_pElementArray=a_pElementArray;
	m_pVertexArray=a_pVertexArray;
	m_pNormalArray=a_pNormalArray;
	m_pUVArray=a_pUVArray;
	m_pColorArray=a_pColorArray;
	m_pTriangleIndexArray=a_pTriangleIndexArray;
	m_pPointIndexArray=a_pPointIndexArray;
	m_pPrimitiveIndexArray=a_pPrimitiveIndexArray;
	m_pPartitionIndexArray=a_pPartitionIndexArray;
	m_vertices=a_vertices;
	m_primitives=a_primitives;

	m_ballCount=m_primitives;

#if FE_QT_CHECK
	for(U32 index=0;index<a_vertices;index++)
	{
		//* NOTE test with std::numeric_limits<>::quiet_NaN()

		if(FE_INVALID_SCALAR(m_pVertexArray[index][0]) ||
				FE_INVALID_SCALAR(m_pVertexArray[index][1]) ||
				FE_INVALID_SCALAR(m_pVertexArray[index][2]))
		{
			feX(e_corrupt,"QuadTree::cacheSpheres","NaN in vertex %d/%d",
					index,a_vertices);
		}
		if(FE_INVALID_SCALAR(m_pNormalArray[index][0]) ||
				FE_INVALID_SCALAR(m_pNormalArray[index][1]) ||
				FE_INVALID_SCALAR(m_pNormalArray[index][2]))
		{
			feX(e_corrupt,"QuadTree::cacheSpheres","NaN in normal %d/%d",
					index,a_vertices);
		}
		if(m_pUVArray &&
				(FE_INVALID_SCALAR(m_pUVArray[index][0]) ||
				FE_INVALID_SCALAR(m_pUVArray[index][1])))
		{
			feX(e_corrupt,"QuadTree::cacheSpheres","NaN in UV %d/%d",
					index,a_vertices);
		}
	}
#endif

	cacheSpheres(a_center,a_radius);
	buildGraph();
}

void QuadTree::cacheSpheres(const SpatialVector& a_center,F32 a_radius)
{
	m_spRootNode->m_pKeyArray=(Key*)reallocate(m_spRootNode->m_pKeyArray,
			m_ballCount*sizeof(Key));

	m_pBallArray=(Ball*)reallocate(m_pBallArray,m_ballCount*sizeof(Ball));
	const F32 radius2=a_radius*a_radius;

#if FE_QT_VERBOSE
		feLog("QuadTree::cacheSpheres count %d filter radius %.6G\n",
				m_ballCount,a_radius);
#endif

	U32 keyCount=0;
	for(U32 sphereIndex=0;sphereIndex<m_ballCount;sphereIndex++)
	{
		SpatialVector center;
		Real radius;
		const U32 startIndex=m_pElementArray[sphereIndex][0];
		const U32 subCount=m_pElementArray[sphereIndex][1];

		FEASSERT(startIndex+subCount<=m_vertices);

		if(!subCount)
		{
			set(center);
			radius=0.0;
			continue;
		}

#if FE_QT_FAST_BOUNDS
		SpatialVector min=m_pVertexArray[startIndex];
		SpatialVector max=min;
		for(U32 subIndex=1;subIndex<subCount;subIndex++)
		{
			const SpatialVector& rPoint=m_pVertexArray[startIndex+subIndex];
			expandBox(min,max,rPoint,rPoint);
		}

		const Real rx=0.5*(max[0]-min[0]);
		const Real ry=0.5*(max[1]-min[1]);
		const Real rz=0.5*(max[2]-min[2]);

		center=0.5*(min+max);
		radius=sqrt(rx*rx+ry*ry+rz*rz);
#else
		BoundingSphere<Real>::solve(&m_pVertexArray[startIndex],
				subCount,center,radius);
#endif

#if FE_QT_DEBUG
		feLog("sphere %2d key %2d center %s radius %.6G (%.6G vs %.6G)\n",
				sphereIndex,keyCount,
				c_print(center),radius,
				magnitudeSquared(center),radius2);
#endif

		if(a_radius>0.0 && magnitudeSquared(center-a_center)>radius2)
		{
			continue;
		}

		Ball& rBall=m_pBallArray[keyCount];
		rBall.m_faceIndex=sphereIndex;
		rBall.m_triangleIndex=
				m_pTriangleIndexArray? m_pTriangleIndexArray[startIndex]: -1;
		rBall.m_primitiveIndex=
				m_pPrimitiveIndexArray? m_pPrimitiveIndexArray[startIndex]: -1;
		rBall.m_partitionIndex=
				m_pPartitionIndexArray? m_pPartitionIndexArray[startIndex]: -1;

		Vector4& rSphere=rBall.m_sphere;
		rSphere=center;
		rSphere[3]=radius;

		Key& rKey=m_spRootNode->m_pKeyArray[keyCount];

		rKey.m_ballIndex=keyCount;
		rKey.m_center=center;

		keyCount++;

#if FALSE
		for(U32 n=0;n<subCount;n++)
		{
			feLog("  %s\n",c_print(m_pVertexArray[startIndex+n]));
		}
		feLog("QuadTree::cacheSpheres %d %s\n",
				sphereIndex,c_print(m_pBallArray[sphereIndex].m_sphere));
#endif
	}
	m_ballCount=keyCount;
	m_pBallArray=(Ball*)reallocate(m_pBallArray,m_ballCount*sizeof(Ball));

	m_spRootNode->m_keyCount=m_ballCount;
	m_spRootNode->m_pKeyArray=(Key*)reallocate(m_spRootNode->m_pKeyArray,
			m_ballCount*sizeof(Key));
}

// TODO deletes
void QuadTree::buildGraph(void)
{
#if FE_QT_DEBUG
	if(m_vertices<10000)
	{
		for(U32 m=0;m<m_vertices;m++)
		{
			feLog("vertex %2d %s  %s\n",m,c_print(m_pVertexArray[m]),
					c_print(m_pNormalArray[m]));
		}
	}
#endif

#if FE_QT_CHECK
	for(U32 primitiveIndex=0;primitiveIndex<m_primitives;primitiveIndex++)
	{
		const U32 startIndex=m_pElementArray[primitiveIndex][0];
		const U32 subCount=m_pElementArray[primitiveIndex][1];

		for(U32 n=0;n<subCount-1;n++)
		{
			for(U32 p=n+1;p<subCount-1;p++)
			{
				if(magnitudeSquared(m_pVertexArray[startIndex+n]-
						m_pVertexArray[startIndex+p])<1e-6)
				{
					feLog("QuadTree::buildGraph"
							" face with coincident vertices"
							" %d %d\n  %s  %s\n",
							startIndex+n,startIndex+p,
							c_print(m_pVertexArray[startIndex+n]),
							c_print(m_pVertexArray[startIndex+p]));
				}
			}
		}
	}
#endif

#if FE_QT_MT_LEVEL>0
	m_spRootNode->m_hpQuadTree=hp<QuadTree>(this);
#endif

	m_verticesUsed=0;
	for(U32 primitiveIndex=0;primitiveIndex<m_primitives;primitiveIndex++)
	{
		const U32 subCount=m_pElementArray[primitiveIndex][1];
		m_verticesUsed+=subCount;
	}

	SpatialVector* pVertexScratch=NULL;

	if(m_verticesUsed==m_vertices)
	{
#if !FE_QT_FAST_BOUNDS
		pVertexScratch=new SpatialVector[m_verticesUsed+1];
#endif

		m_spRootNode->calcBounds(m_pVertexArray,m_vertices);
	}
	else
	{
		pVertexScratch=new SpatialVector[m_verticesUsed+1];

		U32 vertexIndex=0;
		for(U32 primitiveIndex=0;primitiveIndex<m_primitives;primitiveIndex++)
		{
			U32 startIndex=m_pElementArray[primitiveIndex][0];
			const U32 subCount=m_pElementArray[primitiveIndex][1];
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				pVertexScratch[vertexIndex++]=m_pVertexArray[startIndex++];
			}
		}

		m_spRootNode->calcBounds(pVertexScratch,m_verticesUsed);

#if FE_QT_FAST_BOUNDS
		delete[] pVertexScratch;
		pVertexScratch=NULL;
#endif
	}

	//* TODO for FE_QT_MT_LEVEL>1, need to have deferred calcBounds near root

#if FE_QT_MT_LEVEL>0
	m_spGang=new Gang<Worker,I32>();
#endif

	m_spRootNode->refine(m_pElementArray,m_pVertexArray,m_pNormalArray,
			pVertexScratch,m_divisions,m_pBallArray,m_capacity);

	if(m_spGang.isValid())
	{
		const I32 limit=pow(m_divisions[0]*m_divisions[1]*m_divisions[2],
				FE_QT_MT_LEVEL);

		const I32 threadCount=fe::minimum(limit,Thread::hardwareConcurrency());

		m_spGang->start(sp<Counted>(this),threadCount);

		if(m_spGang->workers())
		{
			m_spGang->finish();
		}
	}

	delete[] pVertexScratch;
}

void QuadTree::Node::calcBounds(
	const SpatialVector* a_pVertexArray,
	const U32 a_vertexCount)
{
#if FE_QT_FAST_BOUNDS
	if(!a_vertexCount)
	{
		return;
	}

	m_boundMin=a_pVertexArray[0];
	m_boundMax=m_boundMin;

	for(U32 m=1;m<a_vertexCount;m++)
	{
		const SpatialVector& rPoint=a_pVertexArray[m];
		QuadTree::expandBox(m_boundMin,m_boundMax,rPoint,rPoint);
	}

	const Real rx=0.5*(m_boundMax[0]-m_boundMin[0]);
	const Real ry=0.5*(m_boundMax[1]-m_boundMin[1]);
	const Real rz=0.5*(m_boundMax[2]-m_boundMin[2]);

	m_boundSphere=0.5*(m_boundMin+m_boundMax);
	m_boundSphere[3]=sqrt(rx*rx+ry*ry+rz*rz);
#else
	SpatialVector center;
	Real radius;
	BoundingSphere<Real>::solve(a_pVertexArray,a_vertexCount,center,radius);
	m_boundSphere=center;
	m_boundSphere[3]=radius;
#endif

#if FE_QT_DEBUG
	feLog("QuadTree::Node::calcBounds %d points %s\n",
			a_vertexCount,c_print(m_boundSphere));
#endif
}

void QuadTree::Node::calcBounds(
	const Vector3i* a_pElementArray,
	const SpatialVector* a_pVertexArray,
	SpatialVector* a_pVertexScratch,
	const Ball* a_pBallArray)
{
#if FE_QT_FAST_BOUNDS
	BWORD childFound=FALSE;

	Array< sp<DAGNode::Connection> >& rChildConnections=childConnections();
	const I32 childCount=childConnectionCount();
	for(I32 childIndex=0;childIndex<childCount;childIndex++)
	{
		sp<DAGNode::Connection>& rspChildConnection=
				rChildConnections[childIndex];
		if(rspChildConnection.isValid())
		{
			sp<BaseNode> spChild=rspChildConnection->child();
			if(spChild.isNull())
			{
				continue;
			}

			const SpatialVector& rOtherMin=spChild->m_boundMin;
			const SpatialVector& rOtherMax=spChild->m_boundMax;

			if(!childFound)
			{
				childFound=TRUE;
				m_boundMin=rOtherMin;
				m_boundMax=rOtherMax;
				continue;
			}

			QuadTree::expandBox(m_boundMin,m_boundMax,rOtherMin,rOtherMax);
		}
	}

	if(!childFound)
	{
		for(U32 m=0;m<m_keyCount;m++)
		{
			const U32 faceIndex=face(m,a_pBallArray);
			const U32 startIndex=a_pElementArray[faceIndex][0];
			const U32 subCount=a_pElementArray[faceIndex][1];

			if(!m)
			{
				m_boundMin=a_pVertexArray[startIndex];
				m_boundMax=m_boundMin;
			}
			for(U32 subIndex=(!m);subIndex<subCount;subIndex++)
			{
				const SpatialVector& rPoint=a_pVertexArray[startIndex+subIndex];
				QuadTree::expandBox(m_boundMin,m_boundMax,rPoint,rPoint);
			}
		}
	}

	const Real rx=0.5*(m_boundMax[0]-m_boundMin[0]);
	const Real ry=0.5*(m_boundMax[1]-m_boundMin[1]);
	const Real rz=0.5*(m_boundMax[2]-m_boundMin[2]);

	m_boundSphere=0.5*(m_boundMin+m_boundMax);
	m_boundSphere[3]=sqrt(rx*rx+ry*ry+rz*rz);
#else
	//* tight
	U32 vertexCount=0;
	for(U32 m=0;m<m_keyCount;m++)
	{
		const U32 faceIndex=face(m,a_pBallArray);
		const U32 startIndex=a_pElementArray[faceIndex][0];
		const U32 subCount=a_pElementArray[faceIndex][1];
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			a_pVertexScratch[vertexCount++]=a_pVertexArray[startIndex+subIndex];
		}
	}

	calcBounds(a_pVertexScratch,vertexCount);
#endif
}

void QuadTree::Node::refine(
	const Vector3i* a_pElementArray,
	const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,
	SpatialVector* a_pVertexScratch,const Vector3u& a_divisions,
	const Ball* a_pBallArray,U32 a_capacity)
{
#if FE_QT_DEBUG
	feLog("refine %p %d\n",this,m_keyCount);
	for(U32 m=0;m<m_keyCount;m++)
	{
		const U32 faceIndex=m_pKeyArray[m].m_faceIndex;
		const U32 ballIndex=m_pKeyArray[m].m_ballIndex;
		feLog(" %d:%.3f %d",
				faceIndex,m_pKeyArray[m].m_sortValue,ballIndex);
		const Vector4& rVector=a_pBallArray[ballIndex];
		feLog(" (%.3f,%.3f)",
				rVector[0],rVector[1]);
	}
	feLog("\n");
#endif

	if(m_keyCount<=a_capacity)
	{
#if FALSE
		//* TEMP testing
		const U32 maxLevel=4;
		subdivideAll(a_pElementArray,a_pVertexArray,a_pNormalArray,
				1,maxLevel);
#endif

		return;
	}

	FEASSERT(a_divisions[0]>0);
	FEASSERT(a_divisions[0]<3);	// TEMP
	FEASSERT(a_divisions[1]>0);
	FEASSERT(a_divisions[1]<3);	// TEMP
	FEASSERT(a_divisions[2]>0);
	FEASSERT(a_divisions[2]<3);	// TEMP

	//* `X'
	const U32 divisionsX=a_divisions[0];
	Key** ppKeysX=new Key*[divisionsX];
	U32* pKeyCountX=new U32[divisionsX];
//~	Key* ppKeysX[divisionsX];
//~	U32 pKeyCountX[divisionsX];

	if(divisionsX>1)
	{
		split(0,m_pKeyArray,m_keyCount,
				divisionsX,ppKeysX,pKeyCountX);
	}
	else
	{
		ppKeysX[0]=m_pKeyArray;
		pKeyCountX[0]=m_keyCount;
	}

#if FE_QT_DEBUG
	for(U32 n=0;n<divisionsX;n++)
	{
		feLog("refine X %d/%d %p %d\n",n,divisionsX,this,pKeyCountX[n]);
		for(U32 m=0;m<pKeyCountX[n];m++)
		{
			const U32 faceIndex=ppKeysX[n][m].m_faceIndex;
			const U32 ballIndex=ppKeysX[n][m].m_ballIndex;
			feLog(" %d:%.3f %d",
					faceIndex,ppKeysX[n][m].m_sortValue,ballIndex);
			const Vector4& rVector=a_pBallArray[ballIndex];
			feLog(" (%.3f,%.3f)",
					rVector[0],rVector[1]);
		}
	feLog("\n");
	}
#endif

	//* `Y'
	const U32 divisionsY=a_divisions[1];
	const U32 totalY=divisionsX*divisionsY;
	Key** ppKeysY=new Key*[totalY];
	U32* pKeyCountY=new U32[totalY];
//~	Key* ppKeysY[totalY];
//~	U32 pKeyCountY[totalY];
	if(divisionsY>1)
	{
		for(U32 m=0;m<divisionsX;m++)
		{
			split(1,ppKeysX[m],pKeyCountX[m],
					divisionsY,&ppKeysY[m*divisionsY],
					&pKeyCountY[m*divisionsY]);
		}
	}
	else
	{
		for(U32 m=0;m<divisionsX;m++)
		{
			ppKeysY[m]=ppKeysX[m];
			pKeyCountY[m]=pKeyCountX[m];
		}
	}

	delete[] pKeyCountX;
	delete[] ppKeysX;

#if FE_QT_DEBUG
	for(U32 n=0;n<divisionsY;n++)
	{
		feLog("refine Y %d/%d %p %d\n",n,divisionsY,this,pKeyCountY[n]);
		for(U32 m=0;m<pKeyCountY[n];m++)
		{
			const U32 faceIndex=ppKeysY[n][m].m_faceIndex;
			const U32 ballIndex=ppKeysY[n][m].m_ballIndex;
			feLog(" %d:%.3f %d",
					faceIndex,ppKeysY[n][m].m_sortValue,ballIndex);
			const Vector4& rVector=a_pBallArray[ballIndex];
			feLog(" (%.3f,%.3f)",
					rVector[0],rVector[1]);
		}
	feLog("\n");
	}
#endif

	//* `Z'
	const U32 divisionsZ=a_divisions[2];
	const U32 totalZ=divisionsX*divisionsY*divisionsZ;
	Key** ppKeysZ=new Key*[totalZ];
	U32* pKeyCountZ=new U32[totalZ];
//~	Key* ppKeysZ[totalZ];
//~	U32 pKeyCountZ[totalZ];
	if(divisionsZ>1)
	{
		for(U32 m=0;m<totalY;m++)
		{
			split(2,ppKeysY[m],pKeyCountY[m],
					divisionsZ,&ppKeysZ[m*divisionsZ],
					&pKeyCountZ[m*divisionsZ]);
		}
	}
	else
	{
		for(U32 m=0;m<totalY;m++)
		{
			ppKeysZ[m]=ppKeysY[m];
			pKeyCountZ[m]=pKeyCountY[m];
		}
	}

	delete[] pKeyCountY;
	delete[] ppKeysY;

#if FE_QT_DEBUG
	for(U32 n=0;n<divisionsZ;n++)
	{
		feLog("refine Z %d/%d %p %d\n",n,divisionsZ,this,pKeyCountZ[n]);
		for(U32 m=0;m<pKeyCountZ[n];m++)
		{
			const U32 faceIndex=ppKeysZ[n][m].m_faceIndex;
			const U32 ballIndex=ppKeysZ[n][m].m_ballIndex;
			feLog(" %d:%.3f %d",
					faceIndex,ppKeysZ[n][m].m_sortValue,ballIndex);
			const Vector4& rVector=a_pBallArray[ballIndex];
			feLog(" (%.3f,%.3f)",
					rVector[0],rVector[1]);
		}
	feLog("\n");
	}
#endif

	//* if all keys fall into one branch, stop splitting
	U32 loner=FALSE;
	for(U32 m=0;m<totalZ;m++)
	{
		if(pKeyCountZ[m]==m_keyCount)
		{
			loner=TRUE;
			break;
		}
	}

	if(!loner)
	{
		for(U32 m=0;m<totalZ;m++)
		{
			if(pKeyCountZ[m])
			{
				sp<QuadTree> spQuadTree=m_hpQuadTree;

				sp<Node> spChild(new Node);
				spChild->m_hpQuadTree=spQuadTree;
				spChild->m_level=m_level+1;
				spChild->m_pKeyArray=ppKeysZ[m];
				spChild->m_keyCount=pKeyCountZ[m];
				spChild->attachTo(sp<Node>(this));

				if(spQuadTree.isValid() && I32(m_level)==FE_QT_MT_LEVEL-1)
				{
					spQuadTree->addJob(spChild);
				}
				else
				{
					spChild->refine(a_pElementArray,a_pVertexArray,
							a_pNormalArray,a_pVertexScratch,
							a_divisions,a_pBallArray,a_capacity);

					spChild->calcBounds(a_pElementArray,
							a_pVertexArray,a_pVertexScratch,a_pBallArray);
				}
			}
		}
	}

	delete[] pKeyCountZ;
	delete[] ppKeysZ;
}

void QuadTree::addJob(sp<Node> a_spNode)
{
//	safeLock();	//* write lock

	const I32 jobIndex=m_jobArray.size();
	m_jobArray.resize(jobIndex+1);

	Job& rJob=m_jobArray[jobIndex];

	rJob.m_spNode=a_spNode;

//	safeUnlock();

#if FE_QT_MT_DEBUG
	feLog("QuadTree::addJob gang %d post %d\n",m_spGang.isValid(),jobIndex);
#endif

	if(m_spGang.isValid())
	{
		m_spGang->post(jobIndex);
	}
}

const QuadTree::Job QuadTree::lookupJob(I32 a_index)
{
//	safeLockShared();	//* read-only lock

	Job job=m_jobArray[a_index];

//	safeUnlockShared();

	return job;
}

void QuadTree::Worker::operate(void)
{
#if FE_QT_MT_DEBUG
	const U32 thread=m_id+1;

	feLog("QuadTree::Worker::operate()"
			" %p Thread %d starting\n",this,thread);
#endif

	I32 jobIndex=0;
	while(m_spJobQueue->take(jobIndex))
	{
#if FE_QT_MT_DEBUG
		feLog("QuadTree::Worker::operate()"
				" %p Thread %d Worker %d\n",
				this,thread,jobIndex);
#endif

		const Job job=m_spQuadTree->lookupJob(jobIndex);

		sp<Node> spNode=job.m_spNode;
		if(spNode.isValid())
		{
			SpatialVector* pVertexScratch=NULL;

#if !FE_QT_FAST_BOUNDS
			pVertexScratch=new SpatialVector[m_spQuadTree->m_verticesUsed+1];
#endif

			spNode->refine(
					m_spQuadTree->m_pElementArray,
					m_spQuadTree->m_pVertexArray,
					m_spQuadTree->m_pNormalArray,
					pVertexScratch,
					m_spQuadTree->m_divisions,
					m_spQuadTree->m_pBallArray,
					m_spQuadTree->m_capacity);

			spNode->calcBounds(
					m_spQuadTree->m_pElementArray,
					m_spQuadTree->m_pVertexArray,
					pVertexScratch,
					m_spQuadTree->m_pBallArray);

			delete[] pVertexScratch;
		}

#if FE_QT_MT_DEBUG
		feLog("QuadTree::Worker::operate()"
				" %p Thread %d Worker %d done\n",
				this,thread,jobIndex);
#endif
	}

#if FE_QT_MT_DEBUG
	feLog("QuadTree::Worker::operate()"
			" %p Thread %d finishing\n",this,thread);
#endif
}

// TODO currently assumes divisions==2
void QuadTree::Node::split(U32 a_axis,
	Key*& a_rpKey,U32& a_rKeyCount,
	U32 divisions,Key** a_rppKey,U32* a_rpKeyCount)
{
	FEASSERT(divisions==2);
#if FE_QT_DEBUG
	feLog("QuadTree::Node::split %d by %d\n",
			a_rKeyCount,divisions);
#endif

//~	for(U32 m=0;m<a_rKeyCount;m++)
//~	{
//~		a_rpKey[m].m_sortValue=a_pBallArray[ballIndex(m)].m_sphere[a_axis];
//~	}

	U32 median=a_rKeyCount/2;

	//* full sort
//~	qsort(a_rpKey,a_rKeyCount,sizeof(Key),&Key::compare);

	//* only sort enough so that the "median" is near the halfway point
	//* perhaps 6x faster on 5M triangles (for the full tree generation)
	median=quickSelect(a_rpKey,a_rKeyCount,median,a_axis);

#if FE_QT_CLUMP_NEARBY
	if(median)
	{
		//* try to keep a little of existing organization
		while(median<a_rKeyCount &&
				a_rpKey[median].m_sortValue-
				a_rpKey[median-1].m_sortValue<1e-3)
		{
			median++;
		}
	}
#endif

#if FE_QT_DEBUG
	feLog("QuadTree::Node::split lvl %d: %d at %d axis %d\n",
			m_level,a_rKeyCount,median,a_axis);
#endif

	a_rpKeyCount[0]=median;
	a_rpKeyCount[1]=a_rKeyCount-a_rpKeyCount[0];

	a_rppKey[0]=a_rpKey;
	a_rppKey[1]= &a_rpKey[a_rpKeyCount[0]];

//~	a_rppKey[0]=new Key[a_rpKeyCount[0]];
//~	a_rppKey[1]=new Key[a_rpKeyCount[1]];
//~
//~	for(U32 m=0;m<a_rpKeyCount[0];m++)
//~	{
//~		a_rppKey[0][m]=a_rpKey[m];
//~	}
//~	for(U32 m=0;m<a_rpKeyCount[1];m++)
//~	{
//~		a_rppKey[1][m]=a_rpKey[a_rpKeyCount[0]+m];
//~	}
//~
//~	delete[] a_rpKey;
//~	a_rpKey=NULL;
//~	a_rKeyCount=0;
}

void QuadTree::BaseNode::calcFrames(
	SpatialTransform& a_rTransform1,SpatialTransform& a_rTransform2,
	const SpatialVector& a_rVertex1,const SpatialVector& a_rNormal1,
	const SpatialVector& a_rVertex2,const SpatialVector& a_rNormal2)
{
	const SpatialVector direction=unit(a_rVertex2-a_rVertex1);
	a_rTransform1.left()=unit(cross(a_rNormal1,direction));
	a_rTransform2.left()=unit(cross(a_rNormal2,direction));
	a_rTransform1.direction()=cross(a_rTransform1.left(),a_rNormal1);
	a_rTransform2.direction()=cross(a_rTransform2.left(),a_rNormal2);
	a_rTransform1.up()=a_rNormal1;
	a_rTransform2.up()=a_rNormal2;
	a_rTransform1.translation()=a_rVertex1;
	a_rTransform2.translation()=a_rVertex2;

#if FALSE
	feLog("direction\n%s\n",c_print(direction));
	feLog("frame1\n%s\n",c_print(a_rTransform1));
	feLog("frame2\n%s\n",c_print(a_rTransform2));
#endif
}

void QuadTree::Node::subdivideOne(
	const Vector3i* a_pElementArray,const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,const Vector2* a_pUVArray,
	U32 a_triangleIndex,U32 a_level,U32 a_maxLevel,U32 a_timeStamp)
{
	subdivide(a_pElementArray,a_pVertexArray,a_pNormalArray,a_pUVArray,
			&m_pKeyArray[a_triangleIndex],a_triangleIndex,1,
			a_level,a_maxLevel,a_timeStamp);
}

void QuadTree::Node::subdivideAll(
	const Vector3i* a_pElementArray,const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,const Vector2* a_pUVArray,
	U32 a_level,U32 a_maxLevel,U32 a_timeStamp)
{
	subdivide(a_pElementArray,a_pVertexArray,a_pNormalArray,a_pUVArray,
			m_pKeyArray,0,m_keyCount,
			a_level,a_maxLevel,a_timeStamp);
}

void QuadTree::SubNode::subdivideOne(
	const Vector3i* a_pElementArray,const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,const Vector2* a_pUVArray,
	U32 a_triangleIndex,U32 a_level,U32 a_maxLevel,U32 a_timeStamp)
{
	Key pKey[1];
//	pKey[0].m_faceIndex=a_triangleIndex;		TODO redo
	subdivide(m_pSubdivPrimitiveArray,m_pSubdivVertexArray,
			m_pSubdivNormalArray,m_pSubdivUVArray,
			pKey,a_triangleIndex,1,a_level,a_maxLevel,a_timeStamp);
}

void QuadTree::SubNode::subdivideAll(
	const Vector3i* a_pElementArray,const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,const Vector2* a_pUVArray,
	U32 a_level,U32 a_maxLevel,U32 a_timeStamp)
{
	U32 keyCount=faceCount();
	Key* pKey=new Key[keyCount];
	for(U32 m=0;m<keyCount;m++)
	{
//		pKey[m].m_faceIndex=m;					TODO redo
	}
	subdivide(m_pSubdivPrimitiveArray,m_pSubdivVertexArray,
			m_pSubdivNormalArray,m_pSubdivUVArray,
			pKey,0,keyCount,a_level,a_maxLevel,a_timeStamp);
	delete[] pKey;
}

void QuadTree::BaseNode::subdivide(
	const Vector3i* a_pElementArray,const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,const Vector2* a_pUVArray,
	const Key* a_pKey,U32 a_keyIndex,U32 a_keyCount,
	U32 a_level,U32 a_maxLevel,U32 a_timeStamp)
{
#if FE_QT_DEBUG
	feLog("QuadTree::BaseNode::subdivide"
			" a_keyIndex=%d a_keyCount=%d a_level=%d\n",
			a_keyIndex,a_keyCount,a_level);
#endif

	for(U32 m=0;m<a_keyCount;m++)
	{
		const U32 serial=a_keyIndex+m;
		BWORD found=FALSE;

		//* check for preexisting matching sibling
		Array< sp<DAGNode::Connection> >& rChildConnections=
				childConnections();
		const I32 childCount=childConnectionCount();
		for(I32 childIndex=0;childIndex<childCount;childIndex++)
		{
			sp<DAGNode::Connection> spSibling=
					rChildConnections[childIndex];
			if(spSibling.isNull())
			{
				continue;
			}

			sp<QuadTree::SubNode> spSubNode=spSibling->child();
#if FE_QT_DEBUG
			if(spSubNode.isValid())
			{
				feLog("  existing %d\n",spSubNode->m_serial);
			}
#endif
			if(spSubNode.isValid() && spSubNode->m_serial==serial)
			{
				spSubNode->m_timeStamp=a_timeStamp;
				found=TRUE;
				break;
			}
		}
		if(found)
		{
#if FE_QT_DEBUG
			feLog("QuadTree::BaseNode::subdivide found %d\n",serial);
#endif
			continue;
		}

		sp<SubNode> spChild(new SubNode);
		spChild->attachTo(sp<DAGNode>(this));
		spChild->m_level=m_level+1;
		spChild->m_timeStamp=a_timeStamp;
		spChild->m_serial=serial;
		spChild->m_subdivCount=12;
		spChild->m_subdivLevel=a_level;

		spChild->m_pSubdivPrimitiveArray=
				new Vector3i[spChild->m_subdivCount/3];
		spChild->m_pSubdivVertexArray=
				new SpatialVector[spChild->m_subdivCount];
		spChild->m_pSubdivNormalArray=
				new SpatialVector[spChild->m_subdivCount];
		spChild->m_pSubdivUVArray=
				new Vector2[spChild->m_subdivCount];

		// TODO redo
//		const U32 startIndex=a_pElementArray[a_pKey[m].m_faceIndex][0];
		const U32 startIndex=0;

		const SpatialVector& v0=a_pVertexArray[startIndex];
		const SpatialVector& v1=a_pVertexArray[startIndex+1];
		const SpatialVector& v2=a_pVertexArray[startIndex+2];

		const SpatialVector& n0=a_pNormalArray[startIndex];
		const SpatialVector& n1=a_pNormalArray[startIndex+1];
		const SpatialVector& n2=a_pNormalArray[startIndex+2];

		Vector2 uv0;
		Vector2 uv1;
		Vector2 uv2;
		if(a_pUVArray)
		{
			uv0=a_pUVArray[startIndex];
			uv1=a_pUVArray[startIndex+1];
			uv2=a_pUVArray[startIndex+2];
		}
		else
		{
			set(uv0);
			set(uv1);
			set(uv2);
		}

		if(isZero(v1-v0) || isZero(v2-v0) || isZero(v2-v1))
		{
			continue;
		}

#if FE_QT_DEBUG
		feLog("key %d/%d face %d startIndex %d\n",m,a_keyCount,
				a_pKey[m].m_faceIndex,startIndex);
		feLog("v0 %s  %s\n",c_print(v0),c_print(n0));
		feLog("v1 %s  %s\n",c_print(v1),c_print(n1));
		feLog("v2 %s  %s\n",c_print(v2),c_print(n2));
#endif

#if FALSE
		// linear
		const SpatialVector mid01=0.5*(v0+v1);
		const SpatialVector mid12=0.5*(v1+v2);
		const SpatialVector mid20=0.5*(v2+v0);
#endif

#if FALSE
		// MatrixSqrt
		MatrixSqrt<SpatialTransform> matrixSqrt;
		SpatialTransform frame0;
		SpatialTransform frame1;
		calcFrames(frame0,frame1,v0,n0,v1,n1);
		SpatialTransform inv0;
		invert(inv0,frame0);
		SpatialTransform delta=inv0*frame1;
		SpatialTransform partial;
		matrixSqrt.solve(partial,delta);
		SpatialTransform mid=frame0*partial;
		SpatialVector mid01=mid.translation();
		SpatialVector norm01=mid.up();

		FEASSERT(magnitude(norm01)>0.99);
		FEASSERT(magnitude(norm01)<1.01);

		calcFrames(frame0,frame1,v1,n1,v2,n2);
		invert(inv0,frame0);
		delta=inv0*frame1;
		matrixSqrt.solve(partial,delta);
		mid=frame0*partial;
		SpatialVector mid12=mid.translation();
		SpatialVector norm12=mid.up();

		FEASSERT(magnitude(norm12)>0.99);
		FEASSERT(magnitude(norm12)<1.01);

		calcFrames(frame0,frame1,v2,n2,v0,n0);
		invert(inv0,frame0);
		delta=inv0*frame1;
		matrixSqrt.solve(partial,delta);
		mid=frame0*partial;
		SpatialVector mid20=mid.translation();
		SpatialVector norm20=mid.up();

		FEASSERT(magnitude(norm20)>0.99);
		FEASSERT(magnitude(norm20)<1.01);
#else
		TrianglePN<Real> trianglePN;
		trianglePN.configure(v0,v1,v2,n0,n1,n2);

		const SpatialVector mid01=
				trianglePN.midpoint(TrianglePN<Real>::e_v1v2);
		const SpatialVector mid12=
				trianglePN.midpoint(TrianglePN<Real>::e_v2v3);
		const SpatialVector mid20=
				trianglePN.midpoint(TrianglePN<Real>::e_v3v1);

		const SpatialVector norm01=
				trianglePN.midnormal(TrianglePN<Real>::e_v1v2);
		const SpatialVector norm12=
				trianglePN.midnormal(TrianglePN<Real>::e_v2v3);
		const SpatialVector norm20=
				trianglePN.midnormal(TrianglePN<Real>::e_v3v1);
#endif

		const Vector2 uv01=0.5*(uv0+uv1);
		const Vector2 uv12=0.5*(uv1+uv2);
		const Vector2 uv20=0.5*(uv2+uv0);

		spChild->m_pSubdivPrimitiveArray[0]=Vector3i(0,3,FALSE);
		spChild->m_pSubdivPrimitiveArray[1]=Vector3i(3,3,FALSE);
		spChild->m_pSubdivPrimitiveArray[2]=Vector3i(6,3,FALSE);
		spChild->m_pSubdivPrimitiveArray[3]=Vector3i(9,3,FALSE);

		spChild->m_pSubdivVertexArray[0]=v0;
		spChild->m_pSubdivVertexArray[1]=mid01;
		spChild->m_pSubdivVertexArray[2]=mid20;

		spChild->m_pSubdivVertexArray[3]=mid01;
		spChild->m_pSubdivVertexArray[4]=mid12;
		spChild->m_pSubdivVertexArray[5]=mid20;

		spChild->m_pSubdivVertexArray[6]=mid01;
		spChild->m_pSubdivVertexArray[7]=v1;
		spChild->m_pSubdivVertexArray[8]=mid12;

		spChild->m_pSubdivVertexArray[9]=mid12;
		spChild->m_pSubdivVertexArray[10]=v2;
		spChild->m_pSubdivVertexArray[11]=mid20;

		spChild->m_pSubdivNormalArray[0]=n0;
		spChild->m_pSubdivNormalArray[1]=norm01;
		spChild->m_pSubdivNormalArray[2]=norm20;

		spChild->m_pSubdivNormalArray[3]=norm01;
		spChild->m_pSubdivNormalArray[4]=norm12;
		spChild->m_pSubdivNormalArray[5]=norm20;

		spChild->m_pSubdivNormalArray[6]=norm01;
		spChild->m_pSubdivNormalArray[7]=n1;
		spChild->m_pSubdivNormalArray[8]=norm12;

		spChild->m_pSubdivNormalArray[9]=norm12;
		spChild->m_pSubdivNormalArray[10]=n2;
		spChild->m_pSubdivNormalArray[11]=norm20;

		spChild->m_pSubdivUVArray[0]=uv0;
		spChild->m_pSubdivUVArray[1]=uv01;
		spChild->m_pSubdivUVArray[2]=uv20;

		spChild->m_pSubdivUVArray[3]=uv01;
		spChild->m_pSubdivUVArray[4]=uv12;
		spChild->m_pSubdivUVArray[5]=uv20;

		spChild->m_pSubdivUVArray[6]=uv01;
		spChild->m_pSubdivUVArray[7]=uv1;
		spChild->m_pSubdivUVArray[8]=uv12;

		spChild->m_pSubdivUVArray[9]=uv12;
		spChild->m_pSubdivUVArray[10]=uv2;
		spChild->m_pSubdivUVArray[11]=uv20;

		SpatialVector center;
		Real radius;
		BoundingSphere<Real>::solve(spChild->m_pSubdivVertexArray,
				spChild->m_subdivCount,center,radius);
		spChild->m_boundSphere=center;
		spChild->m_boundSphere[3]=radius;

#if FE_QT_DEBUG
		feLog("QuadTree::BaseNode::subdivide %s\n",
				c_print(spChild->m_boundSphere));
#endif

		if(a_level<a_maxLevel)
		{
			U32 keyCount=4;
			Key* pKey=new Key[keyCount];
			for(U32 n=0;n<keyCount;n++)
			{
//				pKey[n].m_faceIndex=m*12+n;		TODO redo
			}
			spChild->subdivide(
					spChild->m_pSubdivPrimitiveArray,
					spChild->m_pSubdivVertexArray,
					spChild->m_pSubdivNormalArray,
					spChild->m_pSubdivUVArray,
					pKey,0,keyCount,
					a_level+1,a_maxLevel,a_timeStamp);
			delete[] pKey;
		}
	}
}

int QuadTree::Key::compare(const void* a_pVoid1,const void* a_pVoid2)
{
#if FE_CODEGEN<=FE_DEBUG
	const Key* pKey1=static_cast<const Key*>(a_pVoid1);
	const Key* pKey2=static_cast<const Key*>(a_pVoid2);
	FEASSERT(pKey1);
	FEASSERT(pKey2);
//	FEASSERT(pKey1->m_faceIndex!=pKey2->m_faceIndex);
#endif

	//* TODO redo
//~	return (pKey1->m_sortValue==pKey2->m_sortValue)? 0:
//~			(pKey1->m_sortValue>pKey2->m_sortValue)? 1: -1;
	return 0;
}

int QuadTree::Child::compare(const void* a_pVoid1,const void* a_pVoid2)
{
	const Child* pChild1=static_cast<const Child*>(a_pVoid1);
	const Child* pChild2=static_cast<const Child*>(a_pVoid2);
	FEASSERT(pChild1);
	FEASSERT(pChild2);
	return (pChild1->m_distance==pChild2->m_distance)? 0:
			(pChild1->m_distance>pChild2->m_distance)? 1: -1;
}

Real QuadTree::rayImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,BWORD a_anyHit,
	U32 a_hitLimit,const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray) const
{
	if(!a_hitLimit ||
			(a_rspPartition.isValid() && !a_rspPartition->matchable()))
	{
		return 0.0;
	}

//	feLog("QuadTree::rayImpact\n");
#if FE_QT_RAY_SUBDIV
	const BWORD actMutable=TRUE;
	const U32 subdivMax=m_subdivMax;
#else
	const BWORD actMutable=FALSE;
	const U32 subdivMax=0;
#endif
	const BWORD directional=TRUE;

	const_cast<QuadTree*>(this)->prune();

	const_cast<QuadTree*>(this)->lockSafe();

	Real distance=nearestTo(actMutable,directional,a_origin,a_direction,
			a_maxDistance,a_anyHit,a_hitLimit,
			a_rspPartition,a_rHitArray,subdivMax);

	const_cast<QuadTree*>(this)->unlockSafe();

	return distance;
}

Real QuadTree::nearestPoint(const SpatialVector& a_origin,
	Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
	const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray) const
{
	if(!a_hitLimit ||
			(a_rspPartition.isValid() && !a_rspPartition->matchable()))
	{
		return 0.0;
	}

	const BWORD actMutable=TRUE;
	const BWORD directional=FALSE;
	const SpatialVector direction(0.0,0.0,0.0);

	const_cast<QuadTree*>(this)->prune();

	const_cast<QuadTree*>(this)->lockSafe();

	Real distance=nearestTo(actMutable,directional,a_origin,direction,
			a_maxDistance,FALSE,a_hitLimit,
			a_rspPartition,a_rHitArray,m_subdivMax);

	const_cast<QuadTree*>(this)->unlockSafe();

	return distance;
}

Real QuadTree::nearestTo(BWORD a_mutable,BWORD a_directional,
	const SpatialVector& a_origin,const SpatialVector& a_direction,
	Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
	const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray,U32 a_subdivMax) const
{
#if FE_QT_VERBOSE
	feLog("\nQuadTree::nearestTo point %s\n",c_print(a_origin));
	if(a_directional)
	{
		feLog("  along ray %s\n",c_print(a_direction));
	}
#endif

	const_cast<QuadTree*>(this)->prune();

	if(!a_hitLimit)
	{
		return 0.0;
	}

	const_cast<QuadTree*>(this)->lockSafe();

	m_spRootNode->nearestTo(a_mutable,a_directional,a_origin,
			unitSafe(a_direction),a_maxDistance,a_anyHit,a_hitLimit,
			a_rspPartition,a_rHitArray,m_hitPool,a_subdivMax,m_accuracy,
			m_pElementArray,m_pPointIndexArray,m_pVertexArray,m_pNormalArray,
			m_pUVArray,m_pColorArray,m_pBallArray,m_timeStamp);

	const_cast<QuadTree*>(this)->unlockSafe();

#if FE_QT_VERBOSE
	const U32 hitCount=a_rHitArray.size();
	feLog("  hit count %d\n",hitCount);
	for(U32 m=0;m<hitCount;m++)
	{
		sp<SpatialTreeI::Hit>& rspHit=a_rHitArray[m];
		feLog("QuadTree::nearestTo hit %d/%d dist %.6G at %s\n",
				m,hitCount,rspHit->distance(),c_print(rspHit->intersection()));
		feLog("  bary %s\n",c_print(rspHit->barycenter()));
	}
#endif

	return a_rHitArray.size()? a_rHitArray[0]->distance(): 0.0;
}

Real QuadTree::BaseNode::separation(const SpatialVector& a_origin) const
{
	SpatialVector direction;
	const SpatialVector center=m_boundSphere;
	const Real radius=m_boundSphere[3];
	Real distance=
			PointSphereNearest<Real>::solve(center,radius,a_origin,direction);

	// balance disparate sizes: outside sphere is absolute; inside is a ratio
	if(distance<0.0)
	{
		distance/=radius;
	}
	return distance;
}

void QuadTree::BaseNode::nearestTo(BWORD a_mutable,BWORD a_directional,
	const SpatialVector& a_origin,const SpatialVector& a_direction,
	Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
	const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray,
	CountedPool<Hit>& a_rHitPool,
	U32 a_subdivMax,SurfaceI::Accuracy a_accuracy,
	const Vector3i* a_pElementArray,
	const I32* a_pPointIndexArray,const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,const Vector2* a_pUVArray,
	const Color* a_pColorArray,const Ball* a_pBallArray,
	U32 a_timeStamp) const
{
	FEASSERT(a_hitLimit);

	U32 hits=a_rHitArray.size();
	Real margin_distance=(hits && hits==a_hitLimit)?
			a_rHitArray[hits-1]->distance(): a_maxDistance;

#if FE_QT_DEBUG
	feLog("QuadTree::BaseNode::nearestTo %p level %d\n",this,m_level);
	if(!m_level)
	{
		feLog("\n");
	}
	feLog("nearestTo lvl %d tri %d child %d  %s margin %.6G\n",
			m_level,faceCount(),childConnectionCount(),
			c_print(m_boundSphere),margin_distance);
#endif

#if FE_QT_DRAW_TEXT
	if(a_mutable)
	{
		const_cast<QuadTree::BaseNode*>(this)->m_debugText.sPrintf("%.1f",gap);
	}
#endif

	//* check sphere of entire node
	if(margin_distance>=0.0)
	{
		const SpatialVector center=m_boundSphere;
		const Real radius=m_boundSphere[3];

		Real checkDistance;
		SpatialVector checkOrigin;
		if(a_directional)
		{
			checkDistance=0.5*margin_distance;
			checkOrigin=a_origin+checkDistance*a_direction;
		}
		else
		{
			checkDistance=margin_distance;
			checkOrigin=a_origin;
		}

		const Real gapSquared=magnitudeSquared(checkOrigin-center);
		const Real versus=checkDistance+radius;

		if(gapSquared>=versus*versus)
		{
#if FE_QT_DEBUGm_face
			feLog("node reject by distance %.6G vs %.6G\n",
					sqrtf(gapSquared)-radius,margin_distance);
#endif
			return;
		}
	}

	if(a_directional)
	{
		const SpatialVector center=m_boundSphere;
		const Real radius=m_boundSphere[3];
		const Real distance=RaySphereIntersect<Real>::solve(center,radius,
				a_origin-radius*a_direction,a_direction);

		if(distance<0.0f)
		{
#if FE_QT_DEBUG
			feLog("node reject by miss\n");
#endif
			return;
		}
	}

	U32 subdivChildren=0;
	if(a_subdivMax>0)
	{
		const Array< sp<DAGNode::Connection> >& rChildConnections=
				childConnections();
		const I32 childCount=childConnectionCount();
		for(I32 childIndex=0;childIndex<childCount;childIndex++)
		{
			const sp<DAGNode::Connection>& rspChildConnection=
					rChildConnections[childIndex];
			if(rspChildConnection.isNull())
			{
				continue;
			}
			if(rspChildConnection->child().is<QuadTree::SubNode>())
			{
				subdivChildren++;
			}
		}
	}

	// HACK const
	sp<BaseNode> spBaseNode(const_cast<QuadTree::BaseNode*>(this));

	sp<SubNode> spSubNode(spBaseNode);
	if(spSubNode.isValid() && a_mutable)
	{
		spSubNode->m_timeStamp=a_timeStamp;
	}

	Real gap=0.0;
	BWORD gapped=FALSE;

#if FE_QT_DRAW_TEXT
	if(a_mutable)
	{
		if(!gapped)
		{
			gap=separation(a_origin);
			gapped=TRUE;
		}
		const_cast<QuadTree::BaseNode*>(this)->m_debugText.sPrintf("%.1fp",gap);
	}
#endif

#if FE_QT_DRAW_TEXT
	Real local_best= -1.0;
#endif

	// descendents are presumably better representations of the same data
	U32 childCount=childConnectionCount();
	if(!childCount || subdivChildren)
	{
		const U32 subdivLevel=spSubNode.isValid()? spSubNode->m_subdivLevel: 0;
		BWORD wouldSubdivide=(a_mutable && subdivLevel<a_subdivMax);

		const U32 faces=faceCount();
		SpatialVector direction=a_direction;

		//* check each key
		for(U32 m=0;m<faces;m++)
		{
			if(a_rspPartition.isValid())
			{
				const I32 ball=ballIndex(m);
				const I32 partitionIndex=ball>=0?
						a_pBallArray[ball].m_partitionIndex: -1;

				if(!a_rspPartition->match(partitionIndex))
				{
#if FE_QT_DEBUG
					feLog("partition miss %d \"%s\"\n",partitionIndex,
							a_rspPartition->partitionName(
							partitionIndex).c_str());
#endif
					continue;
				}
			}

//			feLog("QuadTree::BaseNode::nearestTo array %p index %d\n",
//					a_pBallArray,m);

			const I32 faceIndex=face(m,a_pBallArray);
			FEASSERT(faceIndex>=0);

			const I32 subCount=a_pElementArray[faceIndex][1];
			const BWORD openCurve=a_pElementArray[faceIndex][2];

			if(subCount<1)
			{
#if FE_QT_DEBUG
				feLog("subCount %d\n",subCount);
#endif
				continue;
			}

			const I32* pPointIndexArray=facePointIndex(a_pBallArray,
					a_pElementArray,a_pPointIndexArray,m);
			const SpatialVector* pVertexArray=faceVertex(a_pBallArray,
					a_pElementArray,a_pVertexArray,m);
			const SpatialVector* pNormalArray=faceNormal(a_pBallArray,
					a_pElementArray,a_pNormalArray,m);
			const Vector2* pUVArray=faceUV(a_pBallArray,
					a_pElementArray,a_pUVArray,m);
			const Color* pColorArray=faceColor(a_pBallArray,
					a_pElementArray,a_pColorArray,m);

			//* TODO fix all the [2] access that presumes a grain of 3

			const Vector4 sphereVector=sphereData(m,a_pBallArray);
//			feLog("QuadTree::BaseNode::nearestTo sphere %s\n",
//					c_print(sphereVector));

			const SpatialVector center=sphereVector;
			const Real radius=sphereVector[3];

#if FE_QT_DEBUG
			feLog("sphere %s vert %s | %s | %s\n",
					c_print(sphereVector),
					c_print(pVertexArray[0]),
					c_print(pVertexArray[1]),
					c_print(pVertexArray[2]));
#endif

			SpatialVector intersection=a_origin;
			Real distance=0.0;

			if(radius>0.0)
			{
				if(a_directional)
				{
					distance=RaySphereIntersect<Real>::solve(center,
							radius,a_origin-radius*a_direction,a_direction);

					if(distance<0.0f)
					{
#if FE_QT_DEBUG
						feLog("skip %d dist %.6G\n",m,distance);
						feLog("  origin %s dir %s\n",
								c_print(a_origin),c_print(a_direction));
#endif
						continue;
					}

					intersection=a_origin+distance*a_direction;
				}
				else if(margin_distance>=0.0)
				{
					if(!PointSphereNearest<Real>::within(
							center,radius,a_origin,margin_distance))
					{
#if FE_QT_DEBUG
						feLog("skip %d  %s\n",m,c_print(sphereVector));
#endif
						continue;
					}
				}
			}

			SpatialBary barycenter(0.0,1.0,0.0);

			if(a_accuracy>SurfaceI::e_sphere)
			{
				distance= -1.0;

				if(subCount==1)
				{
					if(a_directional)
					{
						const Real centerDistance=
								magnitude(pVertexArray[0]-a_origin);
						const Real pointRadius=
								0.04+0.02*centerDistance;	//* tweak

						distance=RaySphereIntersect<Real>::solve(
							pVertexArray[0],pointRadius,
							a_origin,direction);
					}
					else
					{
						distance=magnitude(pVertexArray[0]-a_origin);
					}

					set(barycenter,0.0,1.0);
				}
				else if(openCurve)
				{
#if FALSE
					//* HACK endpoint only
					Real along=1.0;
					distance=magnitude(pVertexArray[subCount-1]-a_origin);
#else
					Real along=0.0;
					if(a_directional)
					{
						const Real radiusAt0=0.04;	//* tweak
						const Real radiusAt1=0.042;	//* tweak

						distance=RayCurveIntersect<Real>::solve(
								pVertexArray,subCount,radiusAt0,radiusAt1,
								a_origin,direction,along,intersection);
					}
					else
					{
						const Real tubeRadius=0.0;	//* TODO

						distance=PointCurveNearest<Real>::solve(
								pVertexArray,subCount,tubeRadius,
								a_origin,direction,along,intersection);
					}

#endif
					barycenter=SpatialBary(along,1.0-along);
				}
				else if(subCount==3)
				{
					if(a_directional)
					{
						distance=RayTriangleIntersect<Real>::solve(
								pVertexArray[0],
								pVertexArray[1],
								pVertexArray[2],
								a_origin,direction,barycenter);
						intersection=a_origin+direction*distance;
					}
					else
					{
						distance=PointTriangleNearest<Real>::solve(
								pVertexArray[0],
								pVertexArray[1],
								pVertexArray[2],
								a_origin,direction,intersection,barycenter);
					}
				}
			}
			else if(!a_directional)
			{
				const SpatialVector toOrigin=a_origin-center;
				const Real mag=magnitude(toOrigin);
				if(mag>radius)
				{
					distance=mag-radius;
					intersection=center+toOrigin*(radius/mag);
				}
			}

#if FE_QT_DEBUG
			feLog("tri %d sphere %s dist %.6G vs %.6G\n",
					m,c_print(sphereVector),distance,margin_distance);
#endif

#if FE_QT_DRAW_TEXT
			if(a_mutable && distance>=0 &&
					(local_best<0 || distance<local_best))
			{
				if(!gapped)
				{
					gap=separation(a_origin);
					gapped=TRUE;
				}

				local_best=distance;
				const_cast<QuadTree::BaseNode*>(this)->m_debugText.sPrintf(
						"%.1f %.1f",gap, local_best);
			}
#endif

			if(wouldSubdivide)
			{
				if(!gapped)
				{
					gap=separation(a_origin);
					gapped=TRUE;
				}

				// blend face distance with sphere separation
				const Real contraction=0.5;	//* TWEAK

				const Real contracted=contraction*distance+
						(1.0-contraction)*(gap<0.0? 0.0: gap);
				if(contracted>=0 &&
						(margin_distance<0 || contracted<margin_distance))
				{
					spBaseNode->subdivideOne(a_pElementArray,a_pVertexArray,
							a_pNormalArray,a_pUVArray,
							m,subdivLevel+1,subdivLevel+1,a_timeStamp);
				}
				continue;
			}

			if(distance>=0 && (margin_distance<0 || distance<margin_distance))
			{
#if FE_QT_DEBUG
				feLog("BETTER\n");
				feLog("QuadTree::BaseNode::nearestTo %s  %s  %s\n",
						c_print(pVertexArray[0]),
						c_print(pVertexArray[1]),
						c_print(pVertexArray[2]));
#endif

				Vector2 uv;
				SpatialVector du;
				SpatialVector dv;
				Color color;

				if(!pUVArray)
				{
					set(uv);
					set(du);
					set(dv);
				}
				else if(openCurve)
				{
					if(subCount==1)
					{
						uv=pUVArray[0];
						set(du);
						set(dv);
					}
					else
					{
						//* TODO use real UV values vs fraction along curve

						set(uv,barycenter[0],0.0);
						set(dv);

						const I32 subIndex=0.999999*(subCount-1)*barycenter[0];
						du=unitSafe(pUVArray[subIndex+1]-pUVArray[subIndex]);
					}
				}
				else
				{
					uv=location(barycenter,pUVArray[0],pUVArray[1],pUVArray[2]);

					triangleDuDv(du,dv,
							pVertexArray[0],pVertexArray[1],pVertexArray[2],
							pUVArray[0],pUVArray[1],pUVArray[2]);
				}

				const I32* pPointIndexTriple=pPointIndexArray;
				const SpatialVector* pVertexTriple=pVertexArray;
				const SpatialVector* pNormalTriple=pNormalArray;

				I32 pointIndexBuffer[3];
				SpatialVector vertexBuffer[3];
				SpatialVector normalBuffer[3];

				if(openCurve)
				{
					if(subCount==1)
					{
						pointIndexBuffer[0]=pPointIndexArray[0];
						pointIndexBuffer[1]=pPointIndexArray[0];
						vertexBuffer[0]=pVertexArray[0];
						vertexBuffer[1]=pVertexArray[0];
						normalBuffer[0]=pNormalArray[0];
						normalBuffer[1]=pNormalArray[0];
					}
					else
					{
						const I32 subIndex=0.999999*(subCount-1)*barycenter[0];

						pointIndexBuffer[0]=pPointIndexArray[subIndex];
						pointIndexBuffer[1]=pPointIndexArray[subIndex+1];
						vertexBuffer[0]=pVertexArray[subIndex];
						vertexBuffer[1]=pVertexArray[subIndex+1];
						normalBuffer[0]=pNormalArray[subIndex];
						normalBuffer[1]=pNormalArray[subIndex+1];
					}

					pointIndexBuffer[2]= -1;
					vertexBuffer[2]=vertexBuffer[1];
					normalBuffer[2]=normalBuffer[1];
					pPointIndexTriple=pointIndexBuffer;
					pVertexTriple=vertexBuffer;
					pNormalTriple=normalBuffer;
				}

				if(pColorArray)
				{
					color=location(barycenter,pColorArray[0],
							pColorArray[1],pColorArray[2]);
				}
				else
				{
					set(color,1.0,1.0,1.0);
				}

//				feLog("QuadTree bary %s uv0 %s uv1 %s uv2 %s uv %s\n",
//						c_print(barycenter),c_print(pUVArray[0]),
//						c_print(pUVArray[1]),c_print(pUVArray[2]),
//						c_print(uv));
//				feLog("  v0 %s v1 %s v2 %s inter %s\n",
//						c_print(pVertexArray[0]),c_print(pVertexArray[1]),
//						c_print(pVertexArray[2]),c_print(intersection));

				const I32 ball=ballIndex(m);
				const I32 faceIndex=face(m,a_pBallArray);
				const I32 triangleIndex=ball>=0?
						a_pBallArray[ball].m_triangleIndex: -1;
				const I32 primitiveIndex=ball>=0?
						a_pBallArray[ball].m_primitiveIndex: -1;
				const I32 partitionIndex=ball>=0?
						a_pBallArray[ball].m_partitionIndex: -1;

				addHit(a_hitLimit,a_rHitArray,a_rHitPool,
						distance,direction,intersection,
						faceIndex,triangleIndex,primitiveIndex,partitionIndex,
						barycenter,uv,du,dv,color,subCount,
						pPointIndexTriple,pVertexTriple,pNormalTriple);

				//* recalc
				hits=a_rHitArray.size();
				margin_distance=(hits && hits==a_hitLimit)?
						a_rHitArray[hits-1]->distance(): a_maxDistance;
			}
		}
	}

	const Array< sp<DAGNode::Connection> >& rChildConnections=
			childConnections();
	childCount=childConnectionCount();
	if(childCount)
	{
		QuadTree::Child* pChildOrder=new QuadTree::Child[childCount];
		U32 checkCount=0;

		for(U32 childIndex=0;childIndex<childCount;childIndex++)
		{
			const sp<DAGNode::Connection>& rspChildConnection=
					rChildConnections[childIndex];

			if(!rspChildConnection.isValid())
			{
				break;
			}

			FEASSERT(checkCount<childCount);
			FEASSERT(childCount==childConnectionCount());
//			FEASSERT(rspChildConnection.raw()==
//					const_cast<QuadTree::BaseNode*>(this)
//					->childConnection(checkCount).raw());

			const sp<DAGNode>& rspChild=rspChildConnection->child();

#if FALSE
			sp<SubNode> spSubNode(rspChild);
			if(spSubNode.isValid())
			{
#if FE_QT_DEBUG
				feLog("nearestTo serial %d\n",spSubNode->m_serial);
#endif

//				spSubNode->m_timeStamp=a_timeStamp;
			}
#endif

			QuadTree::Child& rChild=pChildOrder[checkCount];

			rChild.m_spNode=rspChild;
			if(!rChild.m_spNode.isValid())
			{
#if FE_QT_DEBUG
				feLog("nearestTo not BaseNode\n");
#endif
				continue;
			}

			rChild.m_distance=rChild.m_spNode->separation(a_origin);
			if(margin_distance>=0.0 && rChild.m_distance>margin_distance)
			{
#if FE_QT_DEBUG
				feLog("nearestTo lvl %d filter child %d/%d %.6G vs %.6G\n",
						m_level,checkCount,childCount,
						rChild.m_distance,margin_distance);
#endif
				continue;
			}

			checkCount++;
		}

#if FE_QT_DEBUG
		feLog("childCount %d checkCount %d\n",childCount,checkCount);
#endif

		if(checkCount==2)
		{
			if(pChildOrder[0].m_distance>pChildOrder[1].m_distance)
			{
				QuadTree::Child childTemp=pChildOrder[0];
				pChildOrder[0]=pChildOrder[1];
				pChildOrder[1]=childTemp;
			}
		}
		else if(checkCount)
		{
			qsort(pChildOrder,checkCount,sizeof(QuadTree::Child),
					&QuadTree::Child::compare);
		}

		for(U32 m=0;m<checkCount;m++)
		{
#if FE_QT_DEBUG
			feLog("nearestTo lvl %d filtered child %d/%d\n",
					m_level,m,checkCount);
#endif

			if(a_anyHit && a_rHitArray.size()>=a_hitLimit)
			{
				break;
			}

			pChildOrder[m].m_spNode->nearestTo(a_mutable,a_directional,
					a_origin,a_direction,a_maxDistance,a_anyHit,a_hitLimit,
					a_rspPartition,a_rHitArray,a_rHitPool,a_subdivMax,
					a_accuracy,a_pElementArray,a_pPointIndexArray,
					a_pVertexArray,a_pNormalArray,a_pUVArray,a_pColorArray,
					a_pBallArray,a_timeStamp);
		}
		delete[] pChildOrder;
	}
}

#if FALSE
void QuadTree::BaseNode::rayImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray)
{
	Real best_distance= -1.0f;

/*
	for(U32 m=0;m<m_vertices;m+=m_grain)
	{
		const Vector4& rSphere=m_pBallArray[m/m_grain].m_sphere;
		const SpatialVector center=rSphere;
		const Real radius=rSphere[3];
		Real distance=RaySphereIntersect<Real>::solve(center,radius,
				a_origin-radius*a_direction,a_direction);
		if(distance<0.0f)
		{
			continue;
		}
		distance-=radius;

		Vector2 uv;
		distance=RayTriangleIntersect<Real>::solve(
				m_pVertexArray[m],
				m_pVertexArray[m+1],
				m_pVertexArray[m+2],
				a_origin,a_direction,uv[0],uv[1]);
		if(distance>=0.0 && (best_distance<0.0f ||
				distance<best_distance))
		{
			sp<Hit> spHit=m_hitPool.get();
			a_rHitArray.resize(1);
			a_rHitArray[0]=spHit;

			best_distance=distance;
			spHit->m_pVertex[0]=m_pVertexArray[m];
			spHit->m_pVertex[1]=m_pVertexArray[m+1];
			spHit->m_pVertex[2]=m_pVertexArray[m+2];
			spHit->m_pNormal[0]=m_pNormalArray[m];
			spHit->m_pNormal[1]=m_pNormalArray[m+1];
			spHit->m_pNormal[2]=m_pNormalArray[m+2];
			spHit->m_barycenter=uv;
			spHit->m_intersection=a_origin+distance*a_direction;
			spHit->m_direction=a_direction;
			spHit->m_distance=distance;
		}
	}
*/

	for(U32 m=0;m<m_vertices;m+=m_grain)
	{
		Vector2 uv;
		Real distance=RayTriangleIntersect<Real>::solve(
				m_pVertexArray[m],
				m_pVertexArray[m+1],
				m_pVertexArray[m+2],
				a_origin,a_direction,uv[0],uv[1]);
		if(distance>=0.0 && (best_distance<0.0f ||
				distance<best_distance))
		{
			sp<Hit> spHit=m_hitPool.get();
			a_rHitArray.resize(1);
			a_rHitArray[0]=spHit;

			best_distance=distance;
			spHit->m_pVertex[0]=m_pVertexArray[m];
			spHit->m_pVertex[1]=m_pVertexArray[m+1];
			spHit->m_pVertex[2]=m_pVertexArray[m+2];
			spHit->m_pNormal[0]=m_pNormalArray[m];
			spHit->m_pNormal[1]=m_pNormalArray[m+1];
			spHit->m_pNormal[2]=m_pNormalArray[m+2];
			spHit->m_barycenter=uv;
			spHit->m_intersection=a_origin+distance*a_direction;
			spHit->m_direction=a_direction;
			spHit->m_distance=distance;
		}
	}

	return best_distance;
}
#endif

void QuadTree::age(void)
{
	m_ageStamp++;
}

void QuadTree::prune(void)
{
#if FE_QT_DEBUG
	feLog("QuadTree::prune %.6G vs %.6G\n",m_timeStamp,m_ageStamp);
#endif

	if(m_timeStamp<m_ageStamp)
	{
#if FE_QT_DEBUG
		feLog("QuadTree::prune lock\n");
#endif
		lockSafe();

		//* if still less
		if(m_timeStamp<m_ageStamp)
		{
			m_spRootNode->prune(m_timeStamp);
			m_timeStamp=m_ageStamp;
		}

		unlockSafe();
	}
}

void QuadTree::BaseNode::prune(U32 a_timeStamp)
{
#if FE_QT_DEBUG
	feLog("QuadTree::BaseNode::prune\n");
#endif

	const U32 maxAge=FE_QT_SUBDIV_AGE_MAX;

	const Array< sp<DAGNode::Connection> >& rChildConnections=
			childConnections();
	const U32 childCount=childConnectionCount();
	if(childCount)
	{
		for(U32 childIndex=0;childIndex<childCount;childIndex++)
		{
			const sp<DAGNode::Connection>& rspChildConnection=
					rChildConnections[childIndex];

			if(!rspChildConnection.isValid())
			{
				break;
			}

			sp<DAGNode> spChild=rspChildConnection->child();
			sp<QuadTree::BaseNode> spBaseNode(spChild);
			spBaseNode->prune(a_timeStamp);

			sp<SubNode> spSubNode(spChild);
#if FE_QT_DEBUG
			if(spSubNode.isValid())
			{
				feLog("QuadTree::BaseNode::prune %d %d  %s\n",
						spSubNode->m_timeStamp,a_timeStamp,
						c_print(spSubNode->m_boundSphere));
			}
#endif
			if(spSubNode.isValid() &&
					a_timeStamp-spSubNode->m_timeStamp>maxAge)
			{
#if FE_QT_DEBUG
				feLog("kill %p\n",spSubNode.raw());
#endif
				spSubNode->detach();
			}
		}
	}
}

void QuadTree::draw(sp<DrawI> a_spDrawI,const Color* a_pColor) const
{
	FEASSERT(a_spDrawI->drawMode().isValid());

	m_spWireframe->copy(a_spDrawI->drawMode());
	m_spWireframe->setDrawStyle(DrawMode::e_outline);

	a_spDrawI->pushDrawMode(m_spWireframe);

	const_cast<QuadTree*>(this)->lockSafe();

	m_spRootNode->draw(a_spDrawI,m_pElementArray,m_pVertexArray,
			m_pNormalArray,a_pColor,0);

	const_cast<QuadTree*>(this)->unlockSafe();

	a_spDrawI->popDrawMode();

#if FE_QT_SUBDIV_AGE
	// HACK wrong place
	QuadTree* pThis=const_cast<QuadTree*>(this);
	pThis->age();
#endif
}

void QuadTree::BaseNode::draw(sp<DrawI> a_spDrawI,
	const Vector3i* a_pElementArray,
	const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,
	const Color* a_pColor,U32 a_level) const
{
	Color white(1.0f,1.0f,1.0f);
	Color red(1.0f,0.5f,0.5f);
	Color autocolor(0.0f,0.0f,0.0f);
	autocolor[a_level%3]=a_level<3? 0.5f: 1.0f;

	SpatialTransform transform;
	setIdentity(transform);
	translate(transform,m_boundSphere);
	const Real radius=m_boundSphere[3];
	const SpatialVector scale(radius,radius,radius);

#if FE_QT_DRAW_SPHERES
	a_spDrawI->drawSphere(transform,&scale,a_pColor? *a_pColor: autocolor);
//	a_spDrawI->drawCircle(transform,&scale,a_pColor? *a_pColor: autocolor);
#endif

#if FE_QT_DRAW_NORMALS || FE_QT_DRAW_TEXT
	const QuadTree::BaseNode* pBaseNode=
			fe_cast<QuadTree::BaseNode>(this);
	if(pBaseNode)
	{
		const Real normalScale=1.0;
		SpatialVector pLines[6];
		for(U32 m=0;m<pBaseNode->faceCount();m++)
		{
			const SpatialVector* verts=
					pBaseNode->faceVertex(a_pElementArray,a_pVertexArray,m);
			const SpatialVector* norms=
					pBaseNode->faceNormal(a_pElementArray,a_pNormalArray,m);
			const Vector2* uvs=
					pBaseNode->faceUV(a_pElementArray,a_pUVArray,m);

			pLines[0]=verts[0];
			pLines[1]=verts[0]+norms[0]*normalScale;
			pLines[2]=verts[1];
			pLines[3]=verts[1]+norms[1]*normalScale;
			pLines[4]=verts[2];
			pLines[5]=verts[2]+norms[2]*normalScale;

			if(FE_QT_DRAW_NORMALS)
			{
				const SpatialVector* pNormals=NULL;
				const BWORD multicolor=FALSE;
				a_spDrawI->drawLines(pLines,pNormals,6,DrawI::e_discrete,
						multicolor,&red);
			}

			if(FE_QT_DRAW_TEXT)
			{
				const SpatialVector center=
						0.4*verts[0]+0.3*verts[1]+0.3*verts[2];
				a_spDrawI->drawAlignedText(center,m_debugText,white);
			}
		}
	}
#endif

	const Array< sp<DAGNode::Connection> >& rChildConnections=
			childConnections();
	const U32 childCount=childConnectionCount();
	if(childCount)
	{
		for(U32 childIndex=0;childIndex<childCount;childIndex++)
		{
			const sp<DAGNode::Connection>& rspChildConnection=
					rChildConnections[childIndex];

			if(!rspChildConnection.isValid())
			{
				break;
			}

			sp<DAGNode> spChild=rspChildConnection->child();
#if FE_QT_DRAW_SUBDIV
			sp<SubNode> spSubNode(spChild);
			if(spSubNode.isValid() && spSubNode->m_subdivCount>2)
			{
//				feLog("draw %d %d\n",a_level,spSubNode->m_subdivCount);
//				for(U32 m=0;m<3;m++)
//				{
//					feLog("  %s\n",c_print(spSubNode->m_pSubdivVertexArray[m]));
//				}
				const Vector2* pTexture=NULL;
				const BWORD multicolor=FALSE;
				a_spDrawI->drawTriangles(spSubNode->m_pSubdivVertexArray,
						spSubNode->m_pSubdivNormalArray,
						pTexture,spSubNode->m_subdivCount,
						DrawI::e_discrete,multicolor,
						a_pColor? a_pColor: &autocolor);
			}
#endif

			sp<QuadTree::BaseNode> spBaseNode(spChild);
			spBaseNode->draw(a_spDrawI,a_pElementArray,a_pVertexArray,
					a_pNormalArray,a_pColor,a_level+1);
		}
	}
}

//* static
U32 QuadTree::Node::quickSelect(Key*& a_rpKey,U32& a_keyCount,
	U32 a_selectIndex,U32 a_axis)
{
//	feLog("quickSelect %d/%d *********************\n",
//			a_selectIndex,a_keyCount);

	if(!a_keyCount)
	{
		return 0;
	}

	return select(a_rpKey,a_keyCount,
			0,a_keyCount-1,a_selectIndex,a_axis);
}

//* NOTE https://en.wikipedia.org/wiki/Quickselect

//* static
U32 QuadTree::Node::select(Key*& a_rpKey,U32 a_count,
	U32 a_left,U32 a_right,U32 a_selectIndex,U32 a_axis)
{
	while(TRUE)
	{
		if(a_left==a_right)
		{
			return a_left;
		}

		//* NOTE try not to waste time on a central cluster
		if(a_left>a_count/4 && a_right<a_count*3/4)
		{
			const Real tolerance=0.1;	//* TODO

			BWORD done=TRUE;
			Real min=a_rpKey[a_left].m_center[a_axis];
			Real max=min;
			for(U32 index=a_left+1;index<=a_right;index++)
			{
				const Real value=a_rpKey[index].m_center[a_axis];
				if(min>value)
				{
					min=value;
				}
				if(max<value)
				{
					max=value;
				}
				if(max-min>tolerance)
				{
//					feLog("select min %.6G max %.6G diff %.6G\n",
//							min,max,max-min);
					done=FALSE;
					break;
				}
			}
			if(done)
			{
//				feLog("select %d %d min %.6G max %.6G diff %.6G QUIT\n",
//						a_left,a_right,min,max,max-min);
				return fe::maximum(a_left,fe::minimum(a_selectIndex,a_right));
			}
		}

		//* select some pivotIndex between a_left and right
//		const U32 tryPivotIndex=a_left+rand()%(a_right-a_left+1);
		const U32 tryPivotIndex=(a_left+a_right)/2;

		const U32 pivotIndex=partition(a_rpKey,a_count,
				a_left,a_right,tryPivotIndex,a_axis);

		if(a_selectIndex==pivotIndex)
		{
			return a_selectIndex;
		}
		else if(a_selectIndex<pivotIndex)
		{
			a_right=pivotIndex-1;
		}
		else
		{
			a_left=pivotIndex+1;
		}
	}
}

//* static
U32 QuadTree::Node::partition(Key*& a_rpKey,
	U32 a_count,U32 a_left,U32 a_right,U32 a_pivotIndex,U32 a_axis)
{
	//* move pivot to end
	const Key pivotKey=a_rpKey[a_pivotIndex];
	a_rpKey[a_pivotIndex]=a_rpKey[a_right];
	a_rpKey[a_right]=pivotKey;

	const Real pivotValue=pivotKey.m_center[a_axis];

	U32 storeIndex=a_left;
	for(U32 index=a_left;index<a_right;index++)
	{
		if(a_rpKey[index].m_center[a_axis]<pivotValue)
		{
			if(index!=storeIndex)
			{
				const Key storeValue=a_rpKey[storeIndex];
				a_rpKey[storeIndex]=a_rpKey[index];
				a_rpKey[index]=storeValue;
			}
			storeIndex++;
		}
	}

	//* move pivot to its final place
	const Key rightKey=a_rpKey[a_right];
	a_rpKey[a_right]=a_rpKey[storeIndex];
	a_rpKey[storeIndex]=rightKey;

	return storeIndex;
}

} /* namespace ext */
} /* namespace fe */
