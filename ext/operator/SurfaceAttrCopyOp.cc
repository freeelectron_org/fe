/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SACP_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAttrCopyOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* Point
	catalog<bool>("PointPosition")=false;
	catalog<String>("PointPosition","label")="Position";
	catalog<String>("PointPosition","page")="Point";

	catalog<bool>("PointNormal")=false;
	catalog<String>("PointNormal","label")="Normal";
	catalog<String>("PointNormal","page")="Point";

	catalog<String>("PointAttributes")="";
	catalog<String>("PointAttributes","label")="Attributes";
	catalog<String>("PointAttributes","page")="Point";
	catalog<String>("PointAttributes","hint")=
		"Space delimited list of attributes to copy."
		"  Any token containing an equals sign indicates"
		" a name conversion of the form 'to=from'.";

	//* Primitive
	catalog<bool>("PrimitiveNormal")=false;
	catalog<String>("PrimitiveNormal","label")="Normal";
	catalog<String>("PrimitiveNormal","page")="Primitive";

	catalog<String>("PrimitiveAttributes")="";
	catalog<String>("PrimitiveAttributes","label")="Attributes";
	catalog<String>("PrimitiveAttributes","page")="Primitive";
	catalog<String>("PrimitiveAttributes","hint")=
			catalog<String>("PointAttributes","hint");

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Source Surface");
	catalog<bool>("Source Surface","optional")=TRUE;
}

void SurfaceAttrCopyOp::handle(Record& a_rSignal)
{
#if FE_SACP_DEBUG
	feLog("SurfaceAttrCopyOp::handle\n");
#endif

	catalog<String>("summary")="<failed>";

	const BWORD pointPosition=catalog<bool>("PointPosition");
	const BWORD pointNormal=catalog<bool>("PointNormal");
	const String pointAttributes=catalog<String>("PointAttributes");

	const BWORD primitiveNormal=catalog<bool>("PrimitiveNormal");
	const String primitiveAttributes=catalog<String>("PrimitiveAttributes");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spSourceAccessible;
	if(!access(spSourceAccessible,"Source Surface",e_quiet))
	{
		spSourceAccessible=spOutputAccessible;
	}

	const I32 classCount=2;
	for(I32 classIndex=0;classIndex<classCount;classIndex++)
	{
		const Element element=classIndex? e_primitive: e_point;

		BWORD needPosition=(!classIndex && pointPosition);
		BWORD needNormal=classIndex? primitiveNormal: pointNormal;
		String attributes=classIndex? primitiveAttributes: pointAttributes;

		while(TRUE)
		{
			sp<SurfaceAccessorI> spOutputAccessor;
			sp<SurfaceAccessorI> spSourceAccessor;

			if(needPosition)
			{
				needPosition=FALSE;

				if(!access(spOutputAccessor,spOutputAccessible,
						element,e_position)) continue;
				if(!access(spSourceAccessor,spSourceAccessible,
						element,e_position)) continue;

#if FE_SACP_DEBUG
				feLog("copy position\n");
#endif
			}
			else if(needNormal)
			{
				needNormal=FALSE;

				if(!access(spOutputAccessor,spOutputAccessible,
						element,e_normal,e_warning)) continue;
				if(!access(spSourceAccessor,spSourceAccessible,
						element,e_normal,e_warning)) continue;

#if FE_SACP_DEBUG
				feLog("copy normal\n");
#endif
			}
			else
			{
				String token=attributes.parse();
				if(token.empty())
				{
					break;
				}

				const String toAttr=token.parse("","=");
				const String fromAttr=token.empty()? toAttr: token.prechop("=");

				if(!access(spOutputAccessor,spOutputAccessible,
						element,toAttr,e_warning)) continue;
				if(!access(spSourceAccessor,spSourceAccessible,
						element,fromAttr,e_warning)) continue;

#if FE_SACP_DEBUG
				feLog("copy \"%s\" to \"%s\"\n",
						fromAttr.c_str(),toAttr.c_str());
#endif
			}

			I32 elementCount=spOutputAccessor->count();
			const I32 sourceCount=spSourceAccessor->count();

#if FE_SACP_DEBUG
			feLog("elementCount %d sourceCount %d\n",
					elementCount,sourceCount);
#endif

			if(elementCount>sourceCount)
			{
				elementCount=sourceCount;
			}

			const String attrType=spSourceAccessor->type();

#if FE_SACP_DEBUG
			feLog("type \"%s\"\n",attrType.c_str());
#endif

			if(attrType=="string")
			{
				for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					if(interrupted())
					{
						classIndex=classCount;
						break;
					}

					spOutputAccessor->set(elementIndex,
							spSourceAccessor->string(elementIndex));
				}
			}
			else if(attrType=="integer")
			{
				for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					if(interrupted())
					{
						classIndex=classCount;
						break;
					}

					spOutputAccessor->set(elementIndex,
							spSourceAccessor->integer(elementIndex));
				}
			}
			else if(attrType=="real")
			{
				for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					if(interrupted())
					{
						classIndex=classCount;
						break;
					}

					spOutputAccessor->set(elementIndex,
							spSourceAccessor->real(elementIndex));
				}
			}
			else if(attrType=="vector3")
			{
				for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					if(interrupted())
					{
						classIndex=classCount;
						break;
					}

					spOutputAccessor->set(elementIndex,
							spSourceAccessor->spatialVector(elementIndex));
				}
			}
		}
	}

	String summary=pointPosition? "P": "";
	if(pointNormal)
	{
		summary+=(summary.length()? " ": "")+String("N");
	}
	if(!pointAttributes.empty())
	{
		summary+=(summary.length()? " ": "")+pointAttributes;
	}
	if(!primitiveAttributes.empty())
	{
		summary+=(summary.length()? " ": "")+primitiveAttributes;
	}
	catalog<String>("summary")=summary;

#if FE_SACP_DEBUG
	feLog("SurfaceAttrCopyOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
