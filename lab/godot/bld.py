import string
import os
import subprocess
import re
import sys
forge = sys.modules["forge"]

def prerequisites():
    return ["surface"]

def locate_godot():
    confirmed_build = None
    confirmed_cpp = None

    godot_build = os.environ["FE_GODOT_BUILD"]
    godot_cpp = os.environ["FE_GODOT_CPP"]

    if os.path.exists(godot_build):
        confirmed_build = godot_build

    if os.path.exists(godot_cpp):
        confirmed_cpp = godot_cpp

    windows_local = os.environ["FE_WINDOWS_LOCAL"]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        if not confirmed_build:
            local_build = windows_local + "/godot"
            if os.path.exists(local_build):
                confirmed_build = local_build
        if not confirmed_cpp:
            local_cpp = windows_local + "/godot-cpp"
            if os.path.exists(local_cpp):
                confirmed_cpp = local_cpp

        if confirmed_build:
            confirmed_build = confirmed_build.replace("/","\\")
        if confirmed_cpp:
            confirmed_cpp = confirmed_cpp.replace("/","\\")

    return (confirmed_build, confirmed_cpp)

def setup(module):
    module.summary = []

    (confirmed_build, confirmed_cpp) = locate_godot()

    version_gen = os.path.join(confirmed_build, "core", "version_generated.gen.h")
    if os.path.exists(version_gen):
        godotMajor = ""
        godotMinor = ""

        for line in open(version_gen).readlines():
            if re.match("#define VERSION_MAJOR.*", line):
                godotMajor = line.split()[2].rstrip()
            if re.match("#define VERSION_MINOR.*", line):
                godotMinor = line.split()[2].rstrip()

        version = godotMajor + "." + godotMinor
        if version != ".":
            module.summary += [ version ]

    # NOTE build native after Godot modules
    native = module.Module('native')
    module.RemoveDep(native)
    native.AddDep(module)

    # godot_modules/*

#   if os.path.isdir(confirmed_build):
#       engine_link = os.path.join(confirmed_build, "link")
#       if not os.path.lexists(engine_link):
#           os.makedirs(engine_link)
#
#       if os.path.isdir(engine_link):
#           engine_link_fe = os.path.join(engine_link, "fe")
#           root_parent = os.path.abspath(os.path.join(forge.rootPath, os.pardir))
#
#           if os.path.lexists(engine_link_fe):
#               if os.path.islink(engine_link_fe):
#                   os.remove(engine_link_fe)
#               else:
#                   os.rmdir(engine_link_fe)
#           if not os.path.lexists(engine_link_fe):
#               os.symlink(root_parent, engine_link_fe)
#       else:
#           module.summary += [ "bad_link_path" ]

    godot_dynamic = True
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        godot_dynamic = False

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        build_string = ".windows"
    else:
        build_string = ".x11"
    if forge.codegen != 'debug':
        build_string += ".opt"
    build_string += ".tools.64"

    godot_app = os.path.join(confirmed_build, "bin", "godot" + build_string)
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        if godot_dynamic:
            godot_app += ".dll"
        else:
            godot_app += ".lib"
    delete_module_libs = not os.path.exists(godot_app)

    py_template_filename = module.modPath + "/SCsub_common.py"
    f = open(py_template_filename,'r')
    auto_data = f.read()
    f.close()
    fe_path = os.path.dirname(os.getcwd())
    fe_root = os.environ["FE_ROOT"]
    fe_lib_path = os.environ["FE_LIB_PATH"]
    fe_binary_suffix = os.environ["FE_BINARY_SUFFIX"]
    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        fe_path = fe_path.replace("\\","/")
        fe_root = fe_root.replace("\\","/")
        fe_lib_path = fe_lib_path.replace("\\","/")
    auto_data = auto_data.replace("${FE_PATH}",fe_path)
    auto_data = auto_data.replace("${FE_ROOT}",fe_root)
    auto_data = auto_data.replace("${FE_LIB_PATH}",fe_lib_path)
    auto_data = auto_data.replace("${FE_BINARY_SUFFIX}",fe_binary_suffix)

#   print("")
#   print("fe_path '%s'" % fe_path)
#   print("fe_root '%s'" % fe_root)
#   print("fe_lib_path '%s'" % fe_lib_path)
#   print("fe_binary_suffix '%s'" % fe_binary_suffix)

    forge.godot_module_targets = {}

    modset_root_list = forge.modset_roots.split(':')
    for modset_root in modset_root_list:
        modset_root_path = os.path.join(forge.rootPath, modset_root)
        if os.path.isdir(modset_root_path):
            dir_list = os.listdir(modset_root_path)
            for modset in dir_list:
                modset_path = os.path.join(modset_root_path, modset)
                if os.path.isdir(modset_path):
                    for moduleName in forge.modules_found:
                        module_modules = os.path.join(modset_path, moduleName, "godot_modules")
                        if os.path.isdir(module_modules):
                            modules_list = os.listdir(module_modules)
                            for module_modules_module in modules_list:
                                src_module = os.path.join(module_modules, module_modules_module)
                                if not os.path.isdir(src_module):
                                    continue

                                if godot_dynamic:
                                    fe_tools = module_modules_module
                                else:
                                    fe_tools = "modules"

                                fe_tools += build_string

                                if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
                                    if godot_dynamic:
                                        fe_tools = fe_tools + ".dll"
                                    else:
                                        fe_tools = fe_tools + ".lib"
                                else:
                                    fe_tools = "lib" + fe_tools + ".so"

                                if godot_dynamic:
                                    engine_tools = os.path.join(confirmed_build, "bin", fe_tools)
                                else:
                                    engine_tools = os.path.join(confirmed_build, "modules", fe_tools)

                                engine_module = os.path.join(confirmed_build, "modules", module_modules_module)

                                if delete_module_libs and os.path.lexists(engine_tools):
                                    os.remove(engine_tools)

                                if os.path.isdir(confirmed_build):
                                    if os.path.lexists(engine_module):
                                        if os.path.islink(engine_module):
                                            os.remove(engine_module)
                                        else:
                                            os.rmdir(engine_module)
                                    if not os.path.lexists(engine_module):
                                        os.symlink(src_module, engine_module)

                                # WARNING since these are named the same, only one is used
                                # currently, this works because they are all identical
                                py_auto_filename = "SCsub_auto.py"
                                py_auto_path = os.path.join(src_module, py_auto_filename)
                                py_auto_file = open(py_auto_path,"w")
                                py_auto_file.write(auto_data)

                                if not os.path.isdir(engine_module):
                                    module.summary += [ "-" + module_modules_module ]
                                else:
                                    module.summary += [ module_modules_module ]

                                    target_exists = False

                                    soTarget = forge.LookupTarget(engine_tools)
                                    if soTarget:
                                        target_exists = True
                                    else:
                                        soTarget = forge.FileTarget(engine_tools)

                                    forge.godot_module_targets[module_modules_module] = soTarget

                                    # trigger on any module source change
                                    src_pattern = re.compile(r'.*\.(h|cc|py|pmh)$')
                                    dir_list = os.listdir(src_module)
                                    for filename in dir_list:
                                        if filename == py_auto_filename:
                                            # otherwise, rebuilds every time
                                            continue
                                        if src_pattern.match(filename) or filename == "SCsub":
                                            soTarget.AddDep(forge.FileTarget(os.path.join(src_module, filename)))

                                    if not godot_dynamic and target_exists:
                                        continue

                                    soTarget.AddDep(forge.targetRegistry["lab.godot.buildfile"])

                                    # build the GDNative lib first
#                                   soTarget.AddDep(forge.targetRegistry["fexGodotNativeDLLib"])

                                    # need the FE libs used by the freeelectron Godot module
                                    soTarget.AddDep(forge.targetRegistry["fexTerminalDLLib"])


                                    soTarget.AppendOverrideRule(".*", generate_module)

                                    module.AddDep(soTarget)

def generate_module(target):
    (confirmed_build, confirmed_cpp) = locate_godot()

    forge.cprint(forge.WHITE,0,'generating Godot module')
#   forge.cprint(forge.GREEN,0,"for %s" % target.name)

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        command = "copy lib\\" + forge.apiCodegen + "\\*.dll "
        command += confirmed_build + "\\bin"

        forge.color_on(0, forge.BLUE)
        sys.stdout.write(command + "\n")
        forge.color_off()
        result = os.system(command)

    forge.confirm_path(target)

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        drive = confirmed_build.split(":")[0]
        command = drive + ": && "
        command += "cd " + confirmed_build + " && "
#       command += "set GODOT_BUILD=../../&& "  # no space after path
        command += "scons platform=windows"
    else:
        command = "cd " + confirmed_build + ";"
#       command += "GODOT_BUILD=" + confirmed_build + " "
        command += "scons platform=x11"

    if forge.codegen == 'debug':
        command += " target=debug"
    else:
        command += " target=release_debug"

    import multiprocessing

    command += " -j " + str(multiprocessing.cpu_count())

    forge.color_on(0, forge.BLUE)
    sys.stdout.write(command + "\n")
    forge.color_off()
    result = os.system(command)

    # if build succeeded, update time stamp on result
    if result == 0:
        if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
            command = "copy /b " + target.name + "+,, " + target.name
        else:
            command = "touch " + target.name

        forge.color_on(0, forge.BLUE)
        sys.stdout.write(command + "\n")
        forge.color_off()
        result = os.system(command)

    return result

def auto(module):
    (confirmed_build, confirmed_cpp) = locate_godot()

    if not confirmed_build or not confirmed_cpp:
        return 'not found'

    version_h = os.path.join(confirmed_build, "core", "version.h")
    if not os.path.exists(version_h):
        return 'missing'

    # TODO x86 also
    if forge.fe_os == 'FE_WIN32':
        return 'x64 only'

    # NOTE skipping build test
    return None

    # NOTE a generated header doesn't exist if Godot was never built

    test_file = """
#include <Godot.hpp>

int main(void)
{
    return(0);
}
    \n"""

    std_orig = forge.cppmap.get('std', "")
    forge.cppmap['std'] = forge.use_std("c++17")

    forge.includemap['godot_headers'] = confirmed_build + "/modules/gdnative/include"
    forge.includemap['godot_cpp'] = confirmed_cpp + "/include"
    forge.includemap['godot_core'] = confirmed_cpp + "/include/core"
    forge.includemap['godot_gen'] = confirmed_cpp + "/include/gen"

    result = forge.cctest(test_file)

    forge.cppmap['std'] = std_orig

    forge.includemap.pop('godot_headers', None)
    forge.includemap.pop('godot_cpp', None)
    forge.includemap.pop('godot_core', None)
    forge.includemap.pop('godot_gen', None)
    return result
