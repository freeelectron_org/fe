/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "geometry/geometry.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char** envp)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		const Real p0=0.0f;
		const Real p1=2.0f;
		const Real p2=3.0f;
		const Real p3=5.0f;

		for(Real t=0.0f;t<=1.0f;t+=0.05)
		{
			Real p=Spline<Real>::Cardinal2D(t,p0,p1,p2,p3);
			Real d1=Spline<Real>::Cardinal2D_d1(t,p0,p1,p2,p3);
			Real d2=Spline<Real>::Cardinal2D_d2(t,p0,p1,p2,p3);
			feLog("%.6G: %.6G %.6G %.6G\n",t,p,d1,d2);
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}

