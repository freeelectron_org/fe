/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __pyfe_internal_h__
#define __pyfe_internal_h__

namespace pyfe
{

PyObject *createMaster(void);
static void Master_dealloc(PyObject* self);

static PyTypeObject Master_T =
{
	PyObject_HEAD_INIT(NULL)
	0,
	"pyfeMaster",
	sizeof(Master),
	0,
	pyfe::Master_dealloc, /*tp_dealloc*/
	0,/*tp_print*/
	0,/*tp_getattr*/
	0,/*tp_setattr*/
	0,/*tp_compare*/
	0,/*tp_repr*/
	0,/*tp_as_number*/
	0,/*tp_as_sequence*/
	0,/*tp_as_mapping*/
	0,/*tp_hash */
};

static void Record_dealloc(PyObject* self);
static PyObject *Record_getattr(PyObject *self, char *name);
static int Record_setattr(PyObject *self, char *name, PyObject *value);

static PyTypeObject Record_T =
{
	PyObject_HEAD_INIT(NULL)
	0,
	"pyfeRecord",
	sizeof(Record),
	0,
	Record_dealloc, /*tp_dealloc*/
	0,/*tp_print*/
	Record_getattr,/*tp_getattr*/
	Record_setattr,/*tp_setattr*/
	0,/*tp_compare*/
	0,/*tp_repr*/
	0,/*tp_as_number*/
	0,/*tp_as_sequence*/
	0,/*tp_as_mapping*/
	0,/*tp_hash */
};

static void RecordArray_dealloc(PyObject* self);
static int RecordArray_len(PyObject* self);
static PyObject *RecordArray_getitem(PyObject* self, int index);
static PyObject *RecordArray_getslice(PyObject* self, int start, int end);
static int RecordArray_setitem(PyObject* self, int index, PyObject* obj);
static PyObject *RecordArray_getattr(PyObject *self, char *name);

static PySequenceMethods RecordArray_as_sequence =
{
	RecordArray_len,/* __len__ */
	0,/* __add__ */
	0,/* __mul__ */
	RecordArray_getitem,/* __getitem__ */
	RecordArray_getslice,/* __getslice__ */
	RecordArray_setitem,/* __setitem__ */
	0,/* __setslice__ */
};

static PyTypeObject RecordArray_T =
{
	PyObject_HEAD_INIT(NULL)
	0,
	"pyfeRecordArray",
	sizeof(RecordArray),
	0,
	RecordArray_dealloc, /*tp_dealloc*/
	0,/*tp_print*/
	RecordArray_getattr,/*tp_getattr*/
	0,/*tp_setattr*/
	0,/*tp_compare*/
	0,/*tp_repr*/
	0,/*tp_as_number*/
	&RecordArray_as_sequence,/*tp_as_sequence*/
	0,/*tp_as_mapping*/
	0,/*tp_hash */
};

static void Instance_dealloc(PyObject* self);
static PyTypeObject Instance_T =
{
	PyObject_HEAD_INIT(NULL)
	0,
	"pyfeInstance",
	sizeof(Instance),
	0,
	Instance_dealloc, /*tp_dealloc*/
	0,/*tp_print*/
	0,/*tp_getattr*/
	0,/*tp_setattr*/
	0,/*tp_compare*/
	0,/*tp_repr*/
	0,/*tp_as_number*/
	0,/*tp_as_sequence*/
	0,/*tp_as_mapping*/
	0,/*tp_hash */
};


static void Component_dealloc(PyObject* self);
static PyTypeObject Component_T =
{
	PyObject_HEAD_INIT(NULL)
	0,
	"pyfeComponent",
	sizeof(Component),
	0,
	Component_dealloc, /*tp_dealloc*/
	0,/*tp_print*/
	0,/*tp_getattr*/
	0,/*tp_setattr*/
	0,/*tp_compare*/
	0,/*tp_repr*/
	0,/*tp_as_number*/
	0,/*tp_as_sequence*/
	0,/*tp_as_mapping*/
	0,/*tp_hash */
};


static PyObject *registry_manage(PyObject* self, PyObject* args);
static PyObject *registry_create(PyObject* self, PyObject* args);
static PyObject *registry_prune(PyObject* self, PyObject* args);
static PyMethodDef PyFeMethods[] =
{
	{	"create", registry_create, 1 },
	{	"manage", registry_manage, 1 },
	{	"prune", registry_prune, 1 },
	{	NULL, NULL}
};

static PyObject *Record_check(PyObject* self, PyObject* args);
static PyObject *Record_isValid(PyObject* self, PyObject* args);
static PyMethodDef Record_Methods[] =
{
	{	"check", Record_check, 1 },
	{	"isValid", Record_isValid, 1 },
	{	NULL, NULL}
};

static PyObject *RecordArray_check(PyObject* self, PyObject* args);
static PyObject *RecordArray_add(PyObject* self, PyObject* args);
static PyMethodDef RecordArray_Methods[] =
{
	{	"check", RecordArray_check, 1 },
	{	"add", RecordArray_add, 1 },
	{	NULL, NULL}
};

} /* namespace */

#endif /* __pyfe_internal_h__ */
