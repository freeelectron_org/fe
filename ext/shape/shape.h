/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_h__
#define __shape_h__

#include "datatool/datatool.h"
#include "math/math.h"

#ifdef MODULE_shape
#define FE_SHAPE_PORT FE_DL_EXPORT
#else
#define FE_SHAPE_PORT FE_DL_IMPORT
#endif

#include "shape/FilterI.h"
#include "shape/RampI.h"
#include "shape/SelectorI.h"
#include "shape/StringFilterI.h"

#include "shape/shapeAS.h"
#include "shape/Sphere.h"
#include "shape/Disk.h"
#include "shape/Cylinder.h"

#endif /* __shape_h__ */
