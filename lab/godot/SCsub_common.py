def get_fe_path():
    return "${FE_PATH}"

def get_fe_root():
    return "${FE_ROOT}"

def get_fe_lib_path():
    return "${FE_LIB_PATH}"

def get_fe_binary_suffix():
    return "${FE_BINARY_SUFFIX}"

def platform_is_windows():
    import platform

    # os_name is like "Windows-10"
    os_name = platform.platform(terse=True)
    return (os_name.split("-")[0]=="Windows")

#def get_fe_build(env):
#   import os
#
#   debug_lib = env["target"] == "debug"
#   if platform_is_windows():
#       if debug_lib:
#           build = "x86_win64_debug"
#       else:
#           build = "x86_win64_optimize"
#   else:
#       if debug_lib:
#           build = "x86_64_linux_debug"
#       else:
#           build = "x86_64_linux_optimize"
#   return build

def add_lib(name, env, src_list):
    print("Adding %s" % name)

    import os
    import platform

    # os_name is like "Windows-10"
    os_name = platform.platform(terse=True)
    is_windows = platform_is_windows()

#   profile_lib = os.getenv("CODEGEN") == "profile"
    profile_lib = False

    shared_lib = not is_windows

    if not is_windows:
#       env.Append(CPPFLAGS=["-D_GLIBCXX_USE_CXX11_ABI=0"])

        if profile_lib:
            env.Append(CPPFLAGS=["-pg"])
            env.Append(LINKFLAGS=["-pg"])
    else:
        env.Append(CPPFLAGS=["/bigobj"])
        env.Append(CPPFLAGS=["/Zc:__cplusplus"])

    env_lib = env.Clone()

    if shared_lib:

        if not is_windows:
            env_lib.Append(CXXFLAGS='-fPIC')
            env_lib['LIBS'] = []

        shared_lib = env_lib.SharedLibrary(target="#bin/" + name, source=src_list)

        if is_windows:
            shared_lib_shim = shared_lib[0].name.split('.', 1)[0]
        else:
            shared_lib_shim = shared_lib[0].name.rsplit('.', 1)[0]

        env.Append(LIBS=[shared_lib_shim])
        env.Append(LIBPATH=['#bin'])
    else:
        env_lib.add_source_files(env.modules_sources, src_list)

#   build = get_fe_build(env)
#
#   fe_path = get_fe_path()
#
#   env_lib.Append(CPPPATH=".")
#   env_lib.Append(CPPPATH=fe_path + "/base/src")
#   env_lib.Append(CPPPATH=fe_path + "/base/ext")
#   env_lib.Append(CPPPATH=fe_path + "/base/lab")
#   env_lib.Append(CPPPATH=fe_path + "/base/lib/" + build)
#
#   env.Append(LIBPATH=fe_path + "/base/lib/" + build)

    fe_root = get_fe_root()
    fe_lib_path = get_fe_lib_path()

    env_lib.Append(CPPPATH=".")
    env_lib.Append(CPPPATH=fe_root + "/src")
    env_lib.Append(CPPPATH=fe_root + "/ext")
    env_lib.Append(CPPPATH=fe_root + "/lab")
    env_lib.Append(CPPPATH=fe_lib_path)

    env.Append(LIBPATH=fe_lib_path)

    return env_lib

def append_libs(env, lib_list):
    fe_binary_suffix = get_fe_binary_suffix()

    for index in range(len(lib_list)):
        lib_list[index] = lib_list[index] + fe_binary_suffix
        if platform_is_windows():
            lib_list[index] = lib_list[index] + ".lib"

    if platform_is_windows():
        env.Append(LINKFLAGS=lib_list)
    else:
        env.Append(LIBS=lib_list)
