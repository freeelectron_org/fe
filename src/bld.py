import sys
import os
forge = sys.modules["forge"]

def setup(module):
    manifests_new = os.path.join(forge.rootPath, 'src/autoload/manifests_new.cc')
    with open(manifests_new, 'w') as outfile:
        outfile.write("\t// this file is auto-generated\n\n")

    module_list =   [
                    'platform',
                    'memory',
                    'core',
                    'plugin',
                    'data',
                    'math',
                    'autoload'
                    ]

    for i in module_list:
        child = module.Module(i)
        if child:
            child.AddManifest()
            child.AddYaml()
