/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleRecord_h__
#define __surface_SurfaceAccessibleRecord_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Record Surface Binding

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleRecord:
	public ObjectSafeShared<SurfaceAccessibleRecord>,
	virtual public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleRecord>
{
	public:
								SurfaceAccessibleRecord(void);
virtual							~SurfaceAccessibleRecord(void);

								//* As Protectable
virtual	Protectable*			clone(Protectable* pInstance=NULL);

								//* as SurfaceAccessibleI
virtual	void					bind(Instance a_instance)
								{
									Record record=a_instance.cast<Record>();
									if(record.isValid())
									{
										sp<Scope> spScope=
												record.layout()->scope();
										bind(spScope);
									}
									bind(record);
								}
virtual	BWORD					isBound(void)
								{	return m_weakRecord.isValid(); }

								using SurfaceAccessibleBase::count;

virtual	I32						count(String a_nodeName,
										SurfaceAccessibleI::Element
										a_element) const;

virtual	void					bind(sp<Scope> a_spScope)
								{	m_spScope=a_spScope;
									reset(); }

								using SurfaceAccessibleBase::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::save;

virtual	BWORD					save(String a_filename,
										sp<Catalog> a_spSettings);

virtual	void					bind(sp<SurfaceAccessibleI::ThreadingState>
										a_spThreadingState)
								{	m_spThreadingState=a_spThreadingState; }

virtual	void					groupNames(Array<String>& a_rNameArray,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

virtual void					require(SurfaceAccessibleI::Element a_element,
										String a_name,String a_typeName);

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,
										Creation a_create,Writable a_writable);

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create,
										Writable a_writable);

								using SurfaceAccessibleBase::surface;

virtual	sp<SurfaceI>			surface(String a_group,
										SurfaceI::Restrictions a_restrictions);

								using SurfaceAccessibleBase::subSurface;

virtual	sp<SurfaceI>			subSurface(U32 a_subIndex,String a_group,
										SurfaceI::Restrictions a_restrictions);

								using SurfaceAccessibleBase::copy;

virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible);
virtual	void					copy(sp<SurfaceAccessibleI>
										a_spSurfaceAccessible,
										sp<FilterI> a_spFilter);

virtual	sp<MultiGroup>			generateGroup(
										SurfaceAccessibleI::Element a_element,
										String a_groupString);

								//* as SurfaceAccessibleRecord
		void					bind(Record& a_rRecord);
		Record					record(void)
								{	return m_weakRecord; }

		void					bindLike(sp<SurfaceAccessibleRecord>& a_rspOther)
								{
									if(a_rspOther.isValid())
									{
										m_spThreadingState=
												a_rspOther->m_spThreadingState;
										m_paging=a_rspOther->m_paging;
										m_spScope=a_rspOther->m_spScope;
										m_spRecordGroup=
												a_rspOther->m_spRecordGroup;
										m_weakRecord=a_rspOther->m_weakRecord;
									}
								}
virtual	sp<Component>			payload(void)
								{	return m_spForeignAccessible; }

		void					setDiscontiguous(
									SurfaceAccessibleI::Element a_element);

	protected:
virtual	void					reset(void);

virtual	void					ensureBinding(void);

		void					tuneRecordGroup(void);

		sp<Scope>					m_spScope;
		sp<RecordGroup>				m_spRecordGroup;
		WeakRecord					m_weakRecord;
		sp<SurfaceAccessibleI>		m_spForeignAccessible;

		Accessor<String>			m_groupNameAccessor;
		Accessor< std::set<I32> >	m_groupSetAccessor;

		BWORD						m_contiguous[e_elementEnums];
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleRecord_h__ */
