/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

//*	derived from Houndini sample: PrimBase_Base

#ifndef __houdini_HoudiniPrimBase_h__
#define __houdini_HoudiniPrimBase_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini GEO_Primitive with unneccesary pure virtuals filled in

	@ingroup houdini
*//***************************************************************************/
class HoudiniPrimBase: public GEO_PrimNull
{
	public:
								HoudiniPrimBase(GA_Detail* d,
									GA_Offset offset=GA_INVALID_OFFSET):
									GEO_PrimNull(d, offset)					{}

								HoudiniPrimBase(const GA_MergeMap& map,
									GA_Detail& detail,
									GA_Offset offset,
									const HoudiniPrimBase& src):
									GEO_PrimNull(&detail,offset)			{}

virtual							~HoudiniPrimBase(void)						{}

/*								provided by GEO_PrimNull:

virtual	bool					isDegenerate(void) const
								{	return FALSE; }
virtual	int						getBBox(UT_BoundingBox* bbox) const
								{	return 0; }
virtual	void					reverse(void)								{}
virtual	UT_Vector3				computeNormal(void) const
								{	return UT_Vector3(0.0,1.0,0.0); }
virtual	void					copyPrimitive(const GEO_Primitive* src)		{}
virtual	void					copyUnwiredForMerge(const GA_Primitive *src,
									const GA_MergeMap& map)					{}
virtual	GA_Size					getVertexCount() const
								{	return 0; }
virtual	GA_Offset				getVertexOffset(GA_Size index) const
								{	return 0; }
virtual	int						detachPoints(GA_PointGroup &grp)
								{	return 0; }
virtual	GA_DereferenceStatus	dereferencePoint(GA_Offset point,
									bool dry_run=false)
								{	return isDegenerate()?
											GA_DEREFERENCE_DEGENERATE:
											GA_DEREFERENCE_FAIL; }
virtual	GA_DereferenceStatus	dereferencePoints(
									const GA_RangeMemberQuery &pt_q,
									bool dry_run=false)
								{	return isDegenerate()?
											GA_DEREFERENCE_DEGENERATE:
											GA_DEREFERENCE_FAIL; }
virtual
const	GA_PrimitiveJSON*		getJSON() const
								{	return NULL; }
virtual	void					swapVertexOffsets(const GA_Defragment& defrag)
								{}
virtual	bool					evaluatePointRefMap(GA_Offset result_vtx,
									GA_AttributeRefMap& hlist,
									fpreal u,fpreal v,uint du,uint dv) const
								{	return false; }
*/

virtual	int64					getMemoryUsage() const
								{	return 0; }

virtual	void					countMemory(UT_MemoryCounter& counter) const
								{}

virtual	void					normal(NormalComp& output) const			{}

virtual	GEO_Primitive*			convert(GEO_ConvertParms &parms,
									GA_PointGroup *usedpts=0)
								{	return NULL; }

virtual	GEO_Primitive*			convertNew(GEO_ConvertParms &parms)
								{	return NULL; }

};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniPrimBase_h__ */
