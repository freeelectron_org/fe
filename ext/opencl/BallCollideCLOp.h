/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_BallCollideCLOp_h__
#define __opencl_BallCollideCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to move curve outside of a sphere

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT BallCollideCLOp:
	public OpenCLOp,
	public Initialize<BallCollideCLOp>
{
	public:
				BallCollideCLOp(void)										{}
virtual			~BallCollideCLOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_BallCollideCLOp_h__ */
