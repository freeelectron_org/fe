/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "flight/flight.pmh"

#define FE_SAD_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceAccessorDrone::SurfaceAccessorDrone(void)
{
	setName("SurfaceAccessorDrone");
}

BWORD SurfaceAccessorDrone::bind(SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute)
{
	m_attribute=a_attribute;

	String name;
	switch(a_attribute)
	{
		case SurfaceAccessibleI::e_generic:
		case SurfaceAccessibleI::e_position:
			name="P";
			break;
		case SurfaceAccessibleI::e_normal:
			name="N";
			break;
		case SurfaceAccessibleI::e_uv:
			name="uv";
			break;
		case SurfaceAccessibleI::e_color:
			name="Cd";
			break;
		case SurfaceAccessibleI::e_vertices:
			m_attrName="vertices";
			FEASSERT(a_element==SurfaceAccessibleI::e_primitive);
			break;
		case SurfaceAccessibleI::e_properties:
			m_attrName="properties";
			FEASSERT(a_element==SurfaceAccessibleI::e_primitive);
			break;
	}
	return bindInternal(a_element,name);
}

U32 SurfaceAccessorDrone::count(void) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=
			SurfaceAccessibleI::e_vertices &&
			m_attribute!=
			SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
		case SurfaceAccessibleI::e_vertex:
			return 0;
		case SurfaceAccessibleI::e_primitive:
		{
			sp<SurfaceDrone> spSurfaceDrone(m_spSurfaceI);
			if(spSurfaceDrone.isNull())
			{
				return 0;
			}
			return 1;
		}
		case SurfaceAccessibleI::e_detail:
			return 1;
		default:
			;
	}

	return 0;
}

U32 SurfaceAccessorDrone::subCount(U32 a_index) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=SurfaceAccessibleI::e_vertices &&
			m_attribute!=SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	return 1;
}

String SurfaceAccessorDrone::string(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return 0;
	}

	if(m_element==SurfaceAccessibleI::e_primitive && m_attrName=="type")
	{
		return "Mesh";
	}

	if(m_element==SurfaceAccessibleI::e_primitive && m_attrName=="name")
	{
		if(!a_index)
		{
			return "";
		}
		sp<SurfaceDrone> spSurfaceDrone(m_spSurfaceI);
		if(spSurfaceDrone.isNull())
		{
			return String();
		}
		return "drone";
	}

	return String();
}


BWORD SurfaceAccessorDrone::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
#if FE_SAD_DEBUG
	feLog("SurfaceAccessorDrone::bindInternal %s \"%s\" \"%s\"\n",
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			SurfaceAccessibleBase::attributeString(m_attribute).c_str(),
			a_name.c_str());
#endif

	m_element=SurfaceAccessibleI::e_point;
	if(a_element<0 && a_element>5)
	{
		a_element=SurfaceAccessibleI::e_point;
	}

	m_element=a_element;
	m_attrName=a_name;

	if(m_element==SurfaceAccessibleI::e_primitive)
	{
		if(m_attrName=="type" || m_attrName=="name")
		{
			return TRUE;
		}
	}

	return FALSE;
}

} /* namespace ext */
} /* namespace fe */
