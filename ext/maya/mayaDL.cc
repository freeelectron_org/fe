/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<fe::String*>& list)
{
	list.append(new fe::String("fexMetaDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> a_spMaster)
{
	assertMaya(a_spMaster->typeMaster());

	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->add<CurveCombineOp>("OperatorSurfaceI.CurveCombineOp.fe");
	pLibrary->add<CurveSeparateOp>("OperatorSurfaceI.CurveSeparateOp.fe");
	pLibrary->add<DrawMaya>("DrawI.DrawMaya.fe");
	pLibrary->add<FontMaya>("FontI.FontMaya.fe");
	pLibrary->add<JointCombineOp>("OperatorSurfaceI.JointCombineOp.fe");
	pLibrary->add<JointSeparateOp>("OperatorSurfaceI.JointSeparateOp.fe");
	pLibrary->add<MayaDraw>("DrawI.MayaDraw.fe");
	pLibrary->add<MayaGraph>("OperatorGraphI.MayaGraph.fe");
	pLibrary->add<MayaRamp>("RampI.MayaRamp.fe");
	pLibrary->add<MeshSeparateOp>("OperatorSurfaceI.MeshSeparateOp.fe");
	pLibrary->add<SurfaceAccessibleMaya>(
			"SurfaceAccessibleI.SurfaceAccessibleMaya.fe");
	pLibrary->add<SurfaceCurvesMaya>("SurfaceI.SurfaceCurvesMaya.fe");
	pLibrary->add<SurfaceTrianglesMaya>("SurfaceI.SurfaceTrianglesMaya.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	spLibrary->registry()->prioritize("DrawI.DrawMaya.fe",2);
}

}
