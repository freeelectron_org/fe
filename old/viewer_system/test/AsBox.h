/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Test-only.
 *
 * Accessor Set for a simple box record.
 */
class AsBox :
    public AsNamed,
    public Initialize<AsBox>
{
public:
    void initialize()
    {
        add(transform, FE_USE("transform"));
        add(size, FE_USE("size"));
    }

    /** Transform of box. */
    Accessor<SpatialTransform> transform;
    /** Size of box. */
    Accessor<SpatialVector> size;
};

} /* namespace ext */
} /* namespace fe */
