/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_CurlCLOp_h__
#define __opencl_CurlCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to spiral output curves around input curves

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT CurlCLOp:
	public OpenCLOp,
	public Initialize<CurlCLOp>
{
	public:
				CurlCLOp(void)												{}
virtual			~CurlCLOp(void)												{}

		void	initialize(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_CurlCLOp_h__ */
