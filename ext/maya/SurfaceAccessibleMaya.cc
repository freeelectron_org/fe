/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_SACM_INSTANCE_DEBUG	FALSE
#define FE_SACM_GROUP_DEBUG		FALSE

namespace fe
{
namespace ext
{

void SurfaceAccessibleMaya::reset(void)
{
	SurfaceAccessibleBase::reset();

	//* TODO create empty mesh, like in MayaNode
}

BWORD SurfaceAccessibleMaya::discard(SurfaceAccessibleI::Element a_element,
	String a_name)
{
	//* TODO any element

	return SurfaceAccessorMaya::removeDetailFor(m_meshData,a_name);
}

void SurfaceAccessibleMaya::groupNames(
	Array<String>& a_rGroupNames,
	SurfaceAccessibleI::Element a_element) const
{
	a_rGroupNames.clear();

	//* bool blind data
	I32 blindDataID=0x0;
	while(m_pFnMesh->isBlindDataTypeUsed(blindDataID))
	{
			const MFn::Type component=
					SurfaceAccessorMaya::componentTypeOfElement(a_element);
		if(!m_pFnMesh->hasBlindData(component,blindDataID))
		{
			blindDataID++;
			continue;
		}

		MStringArray longNames;
		MStringArray shortNames;
		MStringArray formatNames;
//		MStatus status=
				m_pFnMesh->getBlindDataAttrNames(blindDataID,
				longNames,shortNames,formatNames);

		const String format=formatNames[0].asChar();
		if(format!="bool")
		{
			blindDataID++;
			continue;
		}

		const String attrName=longNames[0].asChar();

#if FE_SACM_GROUP_DEBUG
		feLog("SurfaceAccessibleMaya::groupNames blind bool %d name \"%s\"\n",
				blindDataID,attrName.c_str());
#endif

		a_rGroupNames.push_back(attrName);

		blindDataID++;
	}

	//* Maya sets
	const unsigned int instanceNumber=0;
	const bool renderableSetsOnly=false;
	MObjectArray sets;
	MObjectArray comps;

	MFnMesh mFnMesh(m_meshNode);
	MStatus status=mFnMesh.getConnectedSetsAndMembers(
			instanceNumber,sets,comps,renderableSetsOnly);
	if(!status)
	{
		feLog("SurfaceAccessibleMaya::groupNames"
				" failed to get sets because \"%s\"\n",
				status.errorString().asChar());
	}

	const U32 setCount=sets.length();

#if FE_SACM_GROUP_DEBUG
	const U32 compCount=comps.length();
	feLog("SurfaceAccessibleMaya::groupNames setCount %d compCount %d\n",
			setCount,compCount);
#endif

	for(U32 setIndex=0;setIndex<setCount;setIndex++)
	{
		MObject set=sets[setIndex];
		MObject comp=comps[setIndex];

#if FE_SACM_GROUP_DEBUG
		feLog("SurfaceAccessibleMaya::groupNames %d/%d set %d comp %d\n",
				setIndex,setCount,!set.isNull(),!comp.isNull());
#endif

		if(set.isNull())
		{
			continue;
		}

		MFnSet fnSet(set,&status);
		if(!status)
		{
			feLog("SurfaceAccessibleMaya::groupNames"
					" failed to create set %d/%d because \"%s\"\n",
					setIndex,setCount,status.errorString().asChar());
			continue;
		}

		const String setName=fnSet.name().asChar();

		//* TODO filter out non-sets like shaders

#if FE_SACM_GROUP_DEBUG
		feLog("  \"%s\"\n",setName.c_str());
#endif

		a_rGroupNames.push_back(setName);
	}
}

void SurfaceAccessibleMaya::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
	a_rSpecs.clear();

	//* NOTE a_node ignored

	if(a_element==SurfaceAccessibleI::e_point)
	{
		SurfaceAccessibleI::Spec spec;

		spec.set("P","vector3");
		a_rSpecs.push_back(spec);

		spec.set("N","vector3");
		a_rSpecs.push_back(spec);
	}

	I32 blindDataID=0x0;
	while(m_pFnMesh->isBlindDataTypeUsed(blindDataID))
	{
			const MFn::Type component=
					SurfaceAccessorMaya::componentTypeOfElement(a_element);
		if(!m_pFnMesh->hasBlindData(component,blindDataID))
		{
			blindDataID++;
			continue;
		}

		MStringArray longNames;
		MStringArray shortNames;
		MStringArray formatNames;
//		MStatus status=
				m_pFnMesh->getBlindDataAttrNames(blindDataID,
				longNames,shortNames,formatNames);

//		feLog("SurfaceAccessibleMaya::attributeSpecs blind %d long \"%s\""
//				" short \"%s\" format \"%s\"\n",
//				blindDataID,longNames[0].asChar(),
//				shortNames[0].asChar(),formatNames[0].asChar());

		const String attrName=longNames[0].asChar();
		const String attrType=
				SurfaceAccessorMaya::typeOfFormat(formatNames[0].asChar());

		SurfaceAccessibleI::Spec spec;
		spec.set(attrName,attrType);

		a_rSpecs.push_back(spec);

		blindDataID++;
	}
}

sp<SurfaceAccessorI> SurfaceAccessibleMaya::accessor(String a_node,
		SurfaceAccessibleI::Element a_element,String a_name,
		SurfaceAccessibleI::Creation a_create,
		SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorMaya> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleMaya::accessor(String a_node,
		SurfaceAccessibleI::Element a_element,
		SurfaceAccessibleI::Attribute a_attribute,
		SurfaceAccessibleI::Creation a_create,
		SurfaceAccessibleI::Writable a_writable)
{
	if(a_attribute==SurfaceAccessibleI::e_normal &&
			threading()==SurfaceAccessibleI::e_multiThread)
	{
		//* normal writing not threadsafe, ever
		return sp<SurfaceAccessorI>(NULL);
	}

	sp<SurfaceAccessorMaya> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

//* TODO can we remove this and just use the one from SurfaceAccessibleBase?
void SurfaceAccessibleMaya::append_not(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	const SpatialTransform* a_pTransform)
{
//	feLog("SurfaceAccessibleMaya::append\n");

	if(!a_spSurfaceAccessibleI.isValid())
	{
		feLog("SurfaceAccessibleMaya::append invalid mesh to append\n");
		return;
	}

	MFnMesh* pFnMeshOut=new MFnMesh(m_meshData.asMesh());

	MFnMesh* pFnMeshIn=NULL;
	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya(a_spSurfaceAccessibleI);
	if(spSurfaceAccessibleMaya.isValid())
	{
		pFnMeshIn=new MFnMesh(spSurfaceAccessibleMaya->meshData().asMesh());
	}

	sp<SurfaceAccessorI> spAccessor=a_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices);
	FEASSERT(spAccessor.isValid());

	MStatus status=MS::kSuccess;

	MIntArray polyBlindIds;
	MIntArray vertBlindIds;
	if(pFnMeshIn)
	{
		status = pFnMeshIn->getBlindDataTypes(
				MFn::kMeshPolygonComponent,polyBlindIds);
		status = pFnMeshIn->getBlindDataTypes(
				MFn::kMeshVertComponent,vertBlindIds);
	}

	I32 primitiveCount=spAccessor->count();
	for(I32 primitive=0;primitive<primitiveCount;primitive++)
	{
		//* positions
		const U32 faceVertCount=pFnMeshIn?
				pFnMeshIn->polygonVertexCount(primitive):
				spAccessor->subCount(primitive);

		MPointArray vertexArray;
		vertexArray.setLength(faceVertCount);

		for(I32 faceVertIndex=0;faceVertIndex<faceVertCount;faceVertIndex++)
		{
			SpatialVector point;
			if(pFnMeshIn)
			{
				MPoint mpoint;
//				MStatus status=
						pFnMeshIn->getPoint(faceVertIndex,mpoint);
				set(point,mpoint[0],mpoint[1],mpoint[2]);
			}
			else
			{
				point=spAccessor->spatialVector(primitive,faceVertIndex);
			}
			SpatialVector transformed=point;
			if(a_pTransform)
			{
				transformVector(*a_pTransform,point,transformed);
			}
			vertexArray.set(faceVertIndex,
					transformed[0],transformed[1],transformed[2]);
		}

		pFnMeshOut->addPolygon(vertexArray,false,0.0,MObject::kNullObj,NULL);
		const I32 primitiveOut=pFnMeshOut->numPolygons()-1;

		//* normals
		//* TODO allow indirect normal copy
		if(pFnMeshIn)
		{
			MFloatVectorArray normalArray;
			status=pFnMeshIn->getFaceVertexNormals(primitive,normalArray);

			const I32 norms=normalArray.length();
			for(I32 normIndex=0;normIndex<norms;normIndex++)
			{
				MFloatVector& rVector=normalArray[normIndex];
				MVector norm(rVector[0],rVector[1],rVector[2]);
				status=pFnMeshOut->setFaceVertexNormal(norm,
						primitive,normIndex);
			}
		}

		//* blind data
		if(pFnMeshIn)
		{
			for(U32 blindIndex=0;blindIndex<polyBlindIds.length();blindIndex++)
			{
				const I32 blindDataId=polyBlindIds[blindIndex];

				// TODO move name lookup out of primitive loop
				MStringArray longNames;
				MStringArray shortNames;
				MStringArray formatNames;
				status=pFnMeshIn->getBlindDataAttrNames(blindDataId,
						longNames,shortNames,formatNames);
				String typeName=formatNames[0].asChar();

				if(typeName=="double")
				{
					double data;
					pFnMeshIn->getDoubleBlindData(primitive,
							MFn::kMeshPolygonComponent,
							blindDataId,longNames[0],data);
					pFnMeshOut->setDoubleBlindData(primitiveOut,
							MFn::kMeshPolygonComponent,
							blindDataId,longNames[0],data);
				}
				else if(typeName=="float")
				{
					float data;
					pFnMeshIn->getFloatBlindData(primitive,
							MFn::kMeshPolygonComponent,
							blindDataId,longNames[0],data);
					pFnMeshOut->setFloatBlindData(primitiveOut,
							MFn::kMeshPolygonComponent,
							blindDataId,longNames[0],data);
				}
				else if(typeName=="int")
				{
					int data;
					pFnMeshIn->getIntBlindData(primitive,
							MFn::kMeshPolygonComponent,
							blindDataId,longNames[0],data);
					pFnMeshOut->setIntBlindData(primitiveOut,
							MFn::kMeshPolygonComponent,
							blindDataId,longNames[0],data);
				}
			}
		}
		//* TODO MFn::kMeshVertComponent

		//* TODO connectivity, uvs
	}

	delete pFnMeshIn;
	delete pFnMeshOut;
}

void SurfaceAccessibleMaya::instance(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	const Array<SpatialTransform>& a_rTransformArray)
{
#if FE_SACM_INSTANCE_DEBUG
	feLog("SurfaceAccessibleMaya::instance\n");
#endif

	if(!a_spSurfaceAccessibleI.isValid())
	{
		feLog("SurfaceAccessibleMaya::instance invalid mesh to append\n");
		return;
	}

	MFnMesh* pFnMeshIn=NULL;
	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya(a_spSurfaceAccessibleI);
	if(spSurfaceAccessibleMaya.isValid())
	{
		pFnMeshIn=new MFnMesh(spSurfaceAccessibleMaya->meshData().asMesh());
	}

	sp<SurfaceAccessorI> spAccessor=a_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices);
	FEASSERT(spAccessor.isValid());

	MStatus status=MS::kSuccess;

	//* pre-count
	I32 pointCountPre=0;
	I32 primitiveCountPre=0;
	const I32 primitiveCount=spAccessor->count();
	for(I32 primitive=0;primitive<primitiveCount;primitive++)
	{
		//* positions
		const I32 faceVertCount=pFnMeshIn?
				pFnMeshIn->polygonVertexCount(primitive):
				spAccessor->subCount(primitive);
		pointCountPre+=faceVertCount;
	}
	primitiveCountPre+=primitiveCount;

	const I32 instanceCount=a_rTransformArray.size();
	pointCountPre*=instanceCount;
	primitiveCountPre*=instanceCount;

#if FE_SACM_INSTANCE_DEBUG
	feLog("SurfaceAccessibleMaya::instance"
			" pointCountPre %d primitiveCountPre %d\n",
			pointCountPre, primitiveCountPre);
#endif

	//* Beware of Maya Arrays: index may not be first argument
	MIntArray perPrimitiveCountOut(primitiveCountPre);
	MIntArray primitiveConnectOut(pointCountPre);
	MFloatPointArray pointArrayOut(pointCountPre);
	I32 pointCountOut=0;
	I32 primitiveCountOut=0;

	//* populate arrays
	for(I32 instance=0;instance<instanceCount;instance++)
	{
		const I32 primitiveCount=spAccessor->count();
		for(I32 primitive=0;primitive<primitiveCount;primitive++)
		{
			//* positions
			const I32 faceVertCount=pFnMeshIn?
					pFnMeshIn->polygonVertexCount(primitive):
					spAccessor->subCount(primitive);

			for(I32 faceVertIndex=0;faceVertIndex<faceVertCount;faceVertIndex++)
			{
				SpatialVector point;
				if(pFnMeshIn)
				{
					MPoint mpoint;
//					MStatus status=
							pFnMeshIn->getPoint(faceVertIndex,mpoint);
					set(point,mpoint[0],mpoint[1],mpoint[2]);
				}
				else
				{
					point=spAccessor->spatialVector(primitive,faceVertIndex);
				}
				SpatialVector transformed;
				transformVector(a_rTransformArray[instance],point,transformed);

				pointArrayOut.set(pointCountOut,
						transformed[0],transformed[1],transformed[2]);

				//* NOTE presuming no shared vertices
				//* TODO support shared vertices
				primitiveConnectOut.set(pointCountOut,pointCountOut);

				pointCountOut++;
			}
			perPrimitiveCountOut.set(faceVertCount,primitiveCountOut);

			primitiveCountOut++;
		}
	}

#if FE_SACM_INSTANCE_DEBUG
	feLog("SurfaceAccessibleMaya::instance"
			" pointCountOut %d primitiveCountOut %d\n",
			pointCountOut, primitiveCountOut);
#endif

#if FALSE
	I32 pointArrayOutLen=pointArrayOut.length();
	for(I32 m=0;m<pointArrayOutLen;m++)
	{
		feLog("pointArrayOut %d/%d %.6G %.6G %.6G\n",
				m,pointArrayOutLen,
				pointArrayOut[m][0],pointArrayOut[m][1],pointArrayOut[m][2]);
	}

	I32 perPrimitiveCountOutLen=perPrimitiveCountOut.length();
	for(I32 m=0;m<perPrimitiveCountOutLen;m++)
	{
		feLog("perPrimitiveCountOut %d/%d %d\n",
				m,perPrimitiveCountOutLen,perPrimitiveCountOut[m]);
	}

	I32 primitiveConnectOutLen=primitiveConnectOut.length();
	for(I32 m=0;m<primitiveConnectOutLen;m++)
	{
		feLog("primitiveConnectOut %d/%d %d\n",
				m,primitiveConnectOutLen,primitiveConnectOut[m]);
	}
#endif

#if FE_SACM_INSTANCE_DEBUG
	feLog("SurfaceAccessibleMaya::instance create\n");
#endif

	MFnMeshData fnMeshDataOut;
	MObject objectMeshDataOut=fnMeshDataOut.create();
	MFnMesh fnMeshOut;
	MObject objectMeshOut=fnMeshOut.create(pointCountOut,primitiveCountOut,
			pointArrayOut,perPrimitiveCountOut,primitiveConnectOut,
			objectMeshDataOut,&status);
	if(status)
	{
		m_meshData.set(objectMeshDataOut);
	}
	else
	{
		feX("SurfaceAccessibleMaya::instance", "create failed: %s\n",
				status.errorString().asChar());
	}
	MFnMesh* pFnMeshOut=new MFnMesh(m_meshData.asMesh());

#if FE_SACM_INSTANCE_DEBUG
	feLog("SurfaceAccessibleMaya::instance copy blind data\n");
#endif

	//* blind data
	if(pFnMeshIn)
	{
		MIntArray polyBlindIds;
		MIntArray vertBlindIds;
		status = pFnMeshIn->getBlindDataTypes(
				MFn::kMeshPolygonComponent,polyBlindIds);
		status = pFnMeshIn->getBlindDataTypes(
				MFn::kMeshVertComponent,vertBlindIds);

		for(U32 blindIndex=0;blindIndex<polyBlindIds.length();blindIndex++)
		{
			const I32 blindDataId=polyBlindIds[blindIndex];

			MStringArray longNames;
			MStringArray shortNames;
			MStringArray formatNames;
			status=pFnMeshIn->getBlindDataAttrNames(blindDataId,
					longNames,shortNames,formatNames);
			String typeName=formatNames[0].asChar();

			//* TODO cache all the input blind data

			for(I32 instance=0;instance<instanceCount;instance++)
			{
				const I32 primitiveOffset=instance*primitiveCount;
				for(I32 primitive=0;primitive<primitiveCount;primitive++)
				{
					const I32 primitiveOut=primitiveOffset+primitive;

					if(typeName=="double")
					{
						double data;
						pFnMeshIn->getDoubleBlindData(primitive,
								MFn::kMeshPolygonComponent,
								blindDataId,longNames[0],data);
						pFnMeshOut->setDoubleBlindData(primitiveOut,
								MFn::kMeshPolygonComponent,
								blindDataId,longNames[0],data);
					}
					else if(typeName=="float")
					{
						float data;
						pFnMeshIn->getFloatBlindData(primitive,
								MFn::kMeshPolygonComponent,
								blindDataId,longNames[0],data);
						pFnMeshOut->setFloatBlindData(primitiveOut,
								MFn::kMeshPolygonComponent,
								blindDataId,longNames[0],data);
					}
					else if(typeName=="int")
					{
						int data;
						pFnMeshIn->getIntBlindData(primitive,
								MFn::kMeshPolygonComponent,
								blindDataId,longNames[0],data);
						pFnMeshOut->setIntBlindData(primitiveOut,
								MFn::kMeshPolygonComponent,
								blindDataId,longNames[0],data);
					}
				}
			}
		}
	}

	//* TODO normals, connectivity, uvs

	delete pFnMeshIn;

#if FE_SACM_INSTANCE_DEBUG
	feLog("SurfaceAccessibleMaya::instance done\n");
#endif
}

//* http://www.vfxoverflow.com/questions/combine-meshes-into-one-with-maya-api
//* (reformatted)
#if 1==0
	U32 numInstances;

	// verts for the combined outputMesh

	// connectivity and UV coord info of a source object
	// fill these in using MFnMesh methods
	U32 numVertices;
	MIntArray countArray;
	MIntArray connectArray;
	MFloatArray uArray;
	MFloatArray vArray;
	MIntArray uvCountArray;
	MIntArray uvIdArray;

	MFloatPointArray vertexArray;

	MStatus status;

	// COUNTS
	U32 index=0;
	U32 nCounts=countArray.length();
	MIntArray outFaceCountArray;
	outFaceCountArray.setLength(nCounts * numInstances);
	for(U32 i=0;i<numInstances;i++)
	{
		for(U32 j=0;j<nCounts;j++)
		{
			outFaceCountArray.set(countArray[j],index);
			index++;
		}
	}

	// CONNECTS
	index=0;
	U32 numConnects=(connectArray.length());
	MIntArray outConnectivityArray;
	outConnectivityArray.setLength(numConnects * numInstances);
	for(U32 i=0;i<numInstances;i++)
	{
		U32 startIndex=i*numVertices;
		for(U32 j=0;j<numConnects;j++)
		{
			outConnectivityArray.set((startIndex + connectArray[j]) ,index);
			index++;
		}
	}

	// CREATE MESH
	MFnMeshData fnC;
	MObject m_outGeom=fnC.create(&status);
	MFnMesh fnM;
	fnM.create(vertexArray.length(),outFaceCountArray.length(),
			vertexArray,outFaceCountArray,outConnectivityArray,
			m_outGeom,&status);

	// ASSIGN UVS
	U32 nUVCounts=uvCountArray.length();
	MIntArray outUvCountArray;
	MIntArray outUvIdArray;
	if(nUVCounts == nCounts) {

		// number of agents
		for(U32 i=0;i<numInstances;i++)
		{
			// number of polygons in one agent
			for(U32 j=0;j<nUVCounts;j++)
			{
				outUvCountArray.append(uvCountArray[j]);
			}
		}

		U32 nUVIds=(uvIdArray.length());
		for(U32 i=0;i<numInstances;i++)
			{
			for(U32 j=0;j<nUVIds;j++)
			{
				outUvIdArray.append(uvIdArray[j]);
			}
		}
		status=fnM.setUVs(uArray,vArray);
		status=fnM.assignUVs(outUvCountArray,outUvIdArray);
	}
	else
	{
		cerr << "nUVCounts counts don't match nCounts: " << nUVCounts
				<< " " << nCounts << endl;
	}
#endif

sp<SurfaceI> SurfaceAccessibleMaya::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
	if(m_meshData.asMesh().isNull())
	{
		return sp<SurfaceI>(NULL);
	}

	safeLock();

	sp<SurfaceI> spResult=m_spSurfaceI;

	if(spResult.isNull())
	{
		sp<SurfaceAccessorI> spAccessor=accessor(
				e_primitive,e_properties,e_refuseMissing);
		BWORD openCurve=spAccessor.isValid()?
				spAccessor->integer(0,e_openCurve): FALSE;

//		feLog("SurfaceAccessibleMaya::surface group \"%s\" open %d\n",
//				a_group.c_str(),openCurve);

		//* HACK
		sp<SurfaceAccessorI> spVertices=
				this->accessor(e_primitive,e_vertices,e_refuseMissing);
		const I32 subCount=spVertices.isValid()? spVertices->subCount(0): 0;
		if(subCount>4)
		{
			openCurve=TRUE;
		}

		//* NOTE no Maya point cloud

		if(!openCurve && !(a_restrictions&SurfaceI::e_excludePolygons))
		{
			//* all triangles (presumed)
			spResult=registry()->create("SurfaceI.SurfaceTrianglesMaya");
			if(spResult.isValid())
			{
				sp<SurfaceTrianglesMaya> spSurface=spResult;
				if(spSurface.isValid())
				{
					spSurface->setMeshData(m_meshData);
					spSurface->setGroup(a_group);

					if(m_useTransform)
					{
						spSurface->setTransform(m_transform);
					}
					else
					{
						spSurface->clearTransform();
					}
				}
			}
		}
		else if(openCurve && !(a_restrictions&SurfaceI::e_excludeCurves))
		{
			//* all curves (presuming all or none are open)
			spResult=registry()->create("SurfaceI.SurfaceCurvesMaya");
			if(spResult.isValid())
			{
				sp<SurfaceCurvesMaya> spSurface=spResult;
				if(spSurface.isValid())
				{
					spSurface->setMeshData(m_meshData);
					spSurface->setGroup(a_group);

					if(m_useTransform)
					{
						spSurface->setTransform(m_transform);
					}
					else
					{
						spSurface->clearTransform();
					}
				}
			}
		}
	}

	if(spResult.isValid())
	{
		spResult->setRestrictions(a_restrictions);
	}

	safeUnlock();

	//* don't cache if it can change
	//* TODO determine if it is input or output
	if(FALSE)
	{
		m_spSurfaceI=spResult;
	}

	return spResult;
}

} /* namespace ext */
} /* namespace fe */
