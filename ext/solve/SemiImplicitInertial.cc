/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <solve/solve.pmh>

#include "SemiImplicitInertial.h"

namespace fe
{
namespace ext
{

void write(const std::vector<SpatialVector> &a_v, const std::string &a_filename)
{
	FILE *fp = fopen(a_filename.c_str(), "w");

	fe_fprintf(fp, "%d\n", (int)a_v.size());
	for(unsigned int i = 0; i < a_v.size(); i++)
	{
		fe_fprintf(fp, "%g %g %g\n", a_v[i][0], a_v[i][1], a_v[i][2]);
	}

	fclose(fp);
}

SemiImplicitInertial::SemiImplicitInertial(void)
{
	m_lhs = new UpperTriangularBCRS<Real>();
	//m_dfdx = new UpperTriangularVMR();
	//m_dfdv = new UpperTriangularVMR();
	m_ratio = 1.0;
	m_subdivcnt = 0;
	m_subdivsz = 10;
	m_subdivmult = 1.1;

	// kind of lively and mostly stable
	//m_dxImplicitness = 0.8;
	//m_dvImplicitness = 1.0;
	//m_dv2dxRatio = 1.0;

	// full stability
	m_dxImplicitness = 1.0;
	m_dvImplicitness = 1.0;
	m_dv2dxRatio = 1.0;

	// force a little on-the-edge instability to iron out recovery
	//m_dxImplicitness = 0.5;
	//m_dvImplicitness = 0.8;
	//m_dv2dxRatio = 1.0;

	// almost explicit
	//m_dxImplicitness = 0.1;
	//m_dvImplicitness = 0.1;
	//m_dv2dxRatio = 1.0;


	// sph
	//m_dxImplicitness = 0.0;
	//m_dvImplicitness = 0.0;
	//m_dv2dxRatio = 1.0;

}

SemiImplicitInertial::~SemiImplicitInertial(void)
{
}

void SemiImplicitInertial::initialize(void)
{
}

void SemiImplicitInertial::handleBind(sp<SignalerI> spSignaler,
	sp<Layout> l_sig)
{
	m_hpSignaler = spSignaler;
	m_spScope = l_sig->scope();

	m_asParticle.bind(m_spScope);
	m_asTemporal.bind(m_spScope);
	m_asValidate.bind(m_spScope);
	m_asAccumulate.bind(m_spScope);
	m_asClear.bind(m_spScope);
	m_asUpdate.bind(m_spScope);
	m_asForcePoint.bind(m_spScope);
	m_asSolverParticle.bind(m_spScope);
	m_asBodyPoint.bind(m_spScope);
	m_asComponent.bind(m_spScope);
	m_asForceFilter.bind(m_spScope);
	m_asLineConstrained.bind(m_spScope);
	m_asPlaneConstrained.bind(m_spScope);

	m_asForcePoint.enforceHaving(m_asSolverParticle);
}

void SemiImplicitInertial::clear(sp<RecordGroup> rg_input, Real a_timestep)
{
	SystemTicker ticker("clear");

	r_clear = cfg<Record>("clear");

	m_asTemporal.timestep(r_clear) = a_timestep;

	if(!r_clear.isValid())
	{
		feX("ExplicitInertial::handle",
			"invalid clear signal record");
	}
	m_asClear.dfdx(r_clear) = m_dfdx;
	m_asClear.dfdv(r_clear) = m_dfdv;
	ticker.log("clr prep");
	m_hpSignaler->signal(r_clear);
	ticker.log("clr");



	// TODO: move this clear to a handler called on l_accumulate
	// clear the force accumulators
	// Note that this is on the more general force point, not just particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asForcePoint.force(spRA, i) = zeroVector;
			}
		}
		if(m_asSolverParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asSolverParticle.externalForce(spRA, i) = zeroVector;
			}
		}
	}
	ticker.log("clear");

	// TODO: maybe pool or something instead of hard clear
	//m_dfdx->clear();
	//m_dfdv->clear();

}

void SemiImplicitInertial::accumulate(sp<RecordGroup> rg_input, Real a_timestep)
{
	SystemTicker ticker("accumulate");
	r_accum = cfg<Record>("accumulate");

	m_asTemporal.timestep(r_accum) = a_timestep;

	if(!r_accum.isValid())
	{
		feX("ExplicitInertial::handle",
			"invalid accumulate signal record");
	}
	m_asAccumulate.dfdx(r_accum) = m_dfdx;
	m_asAccumulate.dfdv(r_accum) = m_dfdv;
	ticker.log("accum prep");
	m_hpSignaler->signal(r_accum);
	ticker.log("accum");
}

bool SemiImplicitInertial::validate(sp<RecordGroup> rg_input, Real a_timestep)
{
	SystemTicker ticker("validate");
	r_validate = cfg<Record>("validate");

	m_asTemporal.timestep(r_validate) = a_timestep;

	if(!r_validate.isValid())
	{
		feX("SemiImplicitInertial::handle",
			"invalid validate signal record");
	}
	m_asValidate.valid(r_validate) = true;
	ticker.log("validate prep");
	m_hpSignaler->signal(r_validate);
	ticker.log("validate");

	if(m_asValidate.valid(r_validate) == false)
	{
		return false;
	}
	return true;
}

void SemiImplicitInertial::handle(Record &r_sig)
{
	SystemTicker ticker("SemiImplicit handle");
	ticker.log("begin");
	if(m_subdivcnt > 0)
	{
		m_subdivcnt--;
		if(!m_subdivcnt)
		{
			m_ratio *= m_subdivmult;
			if(m_ratio >= 1.0)
			{
				m_ratio = 1.0;
			}
			else
			{
				m_subdivcnt = m_subdivsz;
			}
		}
	}
	Real sim_ratio = m_ratio;

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group


	SpatialVector totalConstraintForce(0.0,0.0,0.0);
	std::vector<Record> r_particles;
	Real h = m_asTemporal.timestep(r_sig) * sim_ratio;
	step(rg_input, h, r_particles, totalConstraintForce);
	ticker.log("step");

	r_update = cfg<Record>("update");
	if(!r_update.isValid())
	{
		feX("SemiImplicitInertial::handle",
			"invalid update signal record");
	}
	m_asTemporal.timestep(r_update) = h;
	m_hpSignaler->signal(r_update);

#if 1
	bool valid = validate(rg_input, h);
	ticker.log("validate");

	Real eh = m_asTemporal.timestep(r_sig) * (1.0 - sim_ratio);
	if(!valid)
	{
		sim_ratio = 0.0;
		m_ratio *= 0.5;
		m_subdivcnt = m_subdivsz;
		eh = m_asTemporal.timestep(r_sig) * (1.0 - sim_ratio);
	}

	if(sim_ratio != 1.0)
	{
		// TODO: maybe do angular vel in this rigid body mode too, although if
		//       this happens enough for it to matter, we have bigger problems
		SpatialVector average_vel(0.0,0.0,0.0);
		SpatialVector total_force(-totalConstraintForce);
		Real total_mass = 0.0;
		unsigned long n = 0;
		for(unsigned int i = 0; i < r_particles.size(); i++)
		{
			if(!m_asParticle.check(r_particles[i])) { continue; }
			average_vel += m_asSolverParticle.prevVelocity(r_particles[i]);
			total_force += m_asSolverParticle.externalForce(r_particles[i]);
			total_mass += m_asParticle.mass(r_particles[i]);
			n++;
		}
		average_vel *= 1.0 / (Real)n;

		for(unsigned int i = 0; i < r_particles.size(); i++)
		{
			if(!m_asParticle.check(r_particles[i])) { continue; }
			SpatialVector dx = m_asParticle.location(r_particles[i]) -
				m_asSolverParticle.prevLocation(r_particles[i]);
			dx *= sim_ratio;
			m_asParticle.location(r_particles[i]) =
				m_asSolverParticle.prevLocation(r_particles[i]) + dx;

			SpatialVector fdv = total_force/total_mass * eh;

			SpatialVector dv = m_asParticle.velocity(r_particles[i]) -
				m_asSolverParticle.prevVelocity(r_particles[i]);
			dv *= sim_ratio;
			m_asParticle.velocity(r_particles[i]) =
				m_asSolverParticle.prevVelocity(r_particles[i]) + dv + fdv;

			m_asParticle.location(r_particles[i]) +=
				eh *
				(average_vel  + fdv);
		}
	}
#endif
}

void SemiImplicitInertial::step(sp<RecordGroup> rg_input, Real a_timestep, std::vector<Record> &r_particles, SpatialVector &a_totalConstraintForce)
{
	unsigned long ms0 = m_ticker.timeMS();
	unsigned long t0 = systemTick();
	FE_MAYBE_UNUSED(ms0);
	FE_MAYBE_UNUSED(t0);

	feLog("SemiImplicitInertial\n");
	SystemTicker ticker("SemiImplicit step");
	ticker.log("begin");

	// get the matching dfdx and dfdv
	Cache &cache = m_cache[rg_input];
	if(!cache.m_dfdx.isValid())
	{
		cache.m_dfdx = new UpperTriangularVMR();
	}
	if(!cache.m_dfdv.isValid())
	{
		cache.m_dfdv = new UpperTriangularVMR();
	}

	m_dfdx = cache.m_dfdx;
	m_dfdv = cache.m_dfdv;

	Real h = a_timestep;
	clear(rg_input, h);


	std::vector<unsigned int> massless_ids;
	unsigned int index = 0;

#if 0
	std::map<String, sp<RecordArray> > RAs;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		RAs[spRA->layout()->name()] = spRA;
	}

	for(std::map<String, sp<RecordArray> >::iterator i_RA = RAs.begin();
		i_RA != RAs.end(); i_RA++)
	{
		sp<RecordArray> spRA = i_RA->second;
fe_fprintf(stderr, "%s\n", spRA->layout()->name().c_str());
		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);
				r_particles.push_back(r_particlLogan Thomase);
				m_asSolverParticle.index(r_particle) = index;
fe_fprintf(stderr, "input %d x %f %f %f v %f %f %f \n", i, m_asParticle.location(r_particle)[0], m_asParticle.location(r_particle)[1], m_asParticle.location(r_particle)[2], m_asParticle.velocity(r_particle)[0], m_asParticle.velocity(r_particle)[1], m_asParticle.velocity(r_particle)[2]);
				index++;
			}
		}
	}
#endif

#if 1
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);
				r_particles.push_back(r_particle);
				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}
#endif
	unsigned int n = r_particles.size();
	assert(n == index);


//fe_fprintf(stderr, "******** SYSTEM SIZE %u  TIMESTEP %f\n", n, h);

	m_preconditioner.diagonal().resize(n);
	m_rhs.resize(n);
	m_dv.resize(n);
	m_tmp.resize(n);

	if(m_dfdx->rows() == n)
	{
		m_dfdx->zero();
	}
	else
	{
		m_dfdx->clear();
		m_dfdx->setRows(n);
	}

	if(m_dfdv->rows() == n)
	{
		m_dfdv->zero();
	}
	else
	{
		m_dfdv->clear();
		m_dfdv->setRows(n);
	}

	ticker.log("prep");
#if 0
FILE *fpa = fopen("olddfdx", "a");
for(unsigned int i = 0; i < n; i++)
{
fe_fprintf(fpa, "L %f %f %f V %f %f %f\n", m_asParticle.location(r_particles[i])[0], m_asParticle.location(r_particles[i])[1], m_asParticle.location(r_particles[i])[2], m_asParticle.velocity(r_particles[i])[0], m_asParticle.velocity(r_particles[i])[1], m_asParticle.velocity(r_particles[i])[2]);
}
fclose(fpa);
#endif

	// accumulate after everything is wired up
	accumulate(rg_input, h);

	ticker.log("accumulate");

	Real h_sqr = h * h;
#if 0
for(unsigned int i = 0; i < m_rhs.size(); i++)
{
	fe_fprintf(stderr, "RHS A %d %f %f %f\n", i, m_rhs[i][0], m_rhs[i][1], m_rhs[i][2]);
}
#endif

	for(unsigned int i = 0; i < n; i++)
	{
		m_rhs[i] = zeroVector;
		m_tmp[i] = h_sqr * m_dvImplicitness * m_asParticle.velocity(r_particles[i]);
		m_dv[i] = zeroVector;
		m_dfdx->block(i,i); // force the diagonal to exist
	}
#if 0
for(unsigned int i = 0; i < m_rhs.size(); i++)
{
	fe_fprintf(stderr, "RHS B %d %f %f %f\n", i, m_rhs[i][0], m_rhs[i][1], m_rhs[i][2]);
	fe_fprintf(stderr, "tmp %d %f %f %f\n", i, m_tmp[i][0], m_tmp[i][1], m_tmp[i][2]);
	fe_fprintf(stderr, "v %d %f %f %f\n", i, m_asParticle.velocity(r_particles[i])[0], m_asParticle.velocity(r_particles[i])[1], m_asParticle.velocity(r_particles[i])[2]);
}
#endif

	Real dx_dv_impl = m_dxImplicitness * m_dvImplicitness;

	ticker.log("pre lhs");

	// build LHS
	m_lhs->clear();
	SpatialMatrix lhs_mat;
	for(unsigned int i = 0; i < n; i++)
	{
		m_lhs->startrow();

		FullVMR::t_row &dfdx_row = m_dfdx->row(i);
		FullVMR::t_row &dfdv_row = m_dfdv->row(i);

		// assumes there are no dfdvs w/o a corresponding dfdx
		for(FullVMR::t_row::iterator i_dfdx = dfdx_row.begin();
			i_dfdx != dfdx_row.end(); i_dfdx++)
		{
			unsigned int j = (*i_dfdx).first;

			assert(j >= i); // assert upper triangular

			// -h * dv_impl * df/dv
			FullVMR::t_row::iterator i_dfdv = dfdv_row.find(j);
			if(i_dfdv != dfdv_row.end())
			{
				lhs_mat = -h * m_dvImplicitness * (*i_dfdv).second;
			}
			else
			{
				setAll(lhs_mat, 0.0);
			}

			// -h^2 * dv_impl * dx_impl * df/dx
			SpatialMatrix &dfdx = (*i_dfdx).second;
			//lhs_mat -= h_sqr * dfdx;
			subtract(lhs_mat, lhs_mat, h_sqr * dx_dv_impl * dfdx);
#if 0
FILE *fp = fopen("olddfdx", "a");
String ss;
ss = print(dfdx);
fe_fprintf(fp, "DFDX%s\n", ss.c_str());
ss = print((*i_dfdv).second);
fe_fprintf(fp, "DFDV%s\n", ss.c_str());
fclose(fp);
#endif

			// M (on diagonal only) and block jacobi preconditioner
			if(i == j)
			{
				if(m_asParticle.check(r_particles[i]))
				{
					Real mass = m_asParticle.mass(r_particles[i]);
					lhs_mat(0,0) += mass;
					lhs_mat(1,1) += mass;
					lhs_mat(2,2) += mass;

					//invert(m_preconditioner.diagonal()[i], lhs_mat);
#if 1
					if(!inverted(m_preconditioner.diagonal()[i], lhs_mat))
					{
						setIdentity(m_preconditioner.diagonal()[i]);
					}
#endif
				}
				else
				{
					setIdentity(m_preconditioner.diagonal()[i]);
					massless_ids.push_back(i);
				}
			}

			m_lhs->insert(j, lhs_mat);
		}
	}
	m_lhs->done();

	ticker.log("lhs");

	//m_lhs->dump();


	// RHS
	m_dfdx->multiply(m_rhs, m_tmp);

	for(unsigned int i = 0; i < n; i++)
	{
		SpatialVector &f0 = m_asParticle.force(r_particles[i]);
		m_rhs[i] += h * f0;
	}

	ticker.log("rhs");

	std::vector<FilterConstraint<Real> > filters(massless_ids.size());
	for(unsigned int i = 0; i < massless_ids.size(); i++)
	{
		filters[i].makePointConstraint();
		filters[i].setIndex(massless_ids[i]);
	}

	for(unsigned int i = 0; i < n; i++)
	{
		if(m_asLineConstrained.check(r_particles[i]))
		{
			filters.resize(filters.size()+1);
			filters.back().makeLineConstraint(
				m_asLineConstrained.direction(r_particles[i]));
			filters.back().setIndex(i);
		}
		else if(m_asPlaneConstrained.check(r_particles[i]))
		{
			filters.resize(filters.size()+1);
#if 1
			filters.back().makePlaneConstraint(
				m_asPlaneConstrained.normal(r_particles[i]));
#endif
			//filters.back().makePointConstraint();
			filters.back().setIndex(i);
		}
	}

#if 0
	// apply filters to preconditioner
	for(unsigned int i = 0; i < filters.size(); i++)
	{
		FilterConstraint &filter = filters[i];
		SpatialMatrix tmp, I, S, P;
		invert(P, m_preconditioner.diagonal()[filter.index()]);
		setIdentity(I);
		S = filter.filter();

		tmp = S * P + (I - S);
		invert(m_preconditioner.diagonal()[filter.index()], tmp);
	}
#endif

	// dump LHS to file for whatever reason
	m_lhs->write("lhs_dump");
	write(m_rhs, "rhs_dump");

	ticker.log("constraints");

	bool rv = m_solver.solve(m_dv, m_lhs, m_rhs, m_preconditioner, filters);
	if(!rv)
	{
		//feX("SemiImplicitInertial::handler", "solver failed");
		feLog("WARNING: solver failed\n");
	}

	ticker.log("solve");

	const std::vector<SpatialVector> &residual = m_solver.residual();

#if 0
FILE *fp = fopen("olddfdx", "a");
for(unsigned int i = 0; i < n; i++)
{
fe_fprintf(fp, "DV %d %f %f %f -- V %f %f %f\n", i, m_dv[i][0], m_dv[i][1], m_dv[i][2], m_asParticle.velocity(r_particles[i])[0], m_asParticle.velocity(r_particles[i])[1], m_asParticle.velocity(r_particles[i])[2]);
fe_fprintf(fp, "L %f %f %f\n", m_asParticle.location(r_particles[i])[0], m_asParticle.location(r_particles[i])[1], m_asParticle.location(r_particles[i])[2]);
fe_fprintf(fp, "RHS %f %f %f\n", m_rhs[i][0], m_rhs[i][1], m_rhs[i][2]);
fe_fprintf(fp, "TMP %f %f %f\n", m_tmp[i][0], m_tmp[i][1], m_tmp[i][2]);
}
fclose(fp);
#endif

	// apply solve
	Real total_mass = 0.0;
	for(unsigned int i = 0; i < n; i++)
	{
		Record &r_particle = r_particles[i];

		//if(m_asParticle.check(r_particle))
		{
			SpatialVector v0 = m_asParticle.velocity(r_particle);
			SpatialVector v1 = v0 + m_dv[i];

			//SpatialVector x1 = m_asParticle.location(r_particle) + (h * v1);
			SpatialVector x1 = m_asParticle.location(r_particle) +
				h * (v0 + m_dv2dxRatio * m_dv[i]);

			m_asSolverParticle.prevVelocity(r_particle) =
				m_asParticle.velocity(r_particle);
			m_asSolverParticle.prevLocation(r_particle) =
				m_asParticle.location(r_particle);
			m_asParticle.velocity(r_particle) = v1;
			m_asParticle.location(r_particle) = x1;
		}
		if(m_asParticle.check(r_particle))
		{
			total_mass += m_asParticle.mass(r_particle);
		}
	}

	ticker.log("apply");
#if 0
fp = fopen("olddfdx", "a");
for(unsigned int i = 0; i < n; i++)
{
fe_fprintf(fp, "V %d %f %f %f\n", i, m_asParticle.velocity(r_particles[i])[0], m_asParticle.velocity(r_particles[i])[1], m_asParticle.velocity(r_particles[i])[2]);
}
fclose(fp);
#endif

	a_totalConstraintForce = zeroVector;
	if(h > 0.0)
	{
		for(unsigned int i = 0; i < filters.size(); i++)
		{
			a_totalConstraintForce += residual[filters[i].index()] / h;
		}
	}

	//validate(rg_input, h, r_particles, total_constraint_force);

	ticker.log("validate");

#if 1
	for(unsigned int i = 0; i < filters.size(); i++)
	{
		Record &r_particle = r_particles[filters[i].index()];

#if 1
		if(m_asBodyPoint.check(r_particle))
		{
			Record r_body = m_asBodyPoint.body(r_particle);
			if(!r_body.isValid())
			{
				continue;
			}
			sp<MassI> spBody = m_asComponent.component(r_body);
			if(!spBody.isValid())
			{
				continue;
			}

			Mass mass;
			spBody->getMass(mass);

			m_asForcePoint.force(r_particle) = /*1.0 * mass.mass() **/ residual[filters[i].index()] / h;
//fe_fprintf(stderr, "mass %f\n", mass.mass());
		}
#endif
//fe_fprintf(stderr, "R %f %f %f\n", m_asForcePoint.force(r_particle)[0], m_asForcePoint.force(r_particle)[1], m_asForcePoint.force(r_particle)[2]);

		if(m_asForceFilter.check(r_particle))
		{

			m_asForcePoint.force(r_particle) += m_asForceFilter.impulse(r_particle);
			m_asForceFilter.impulse(r_particle) = zeroVector;
			Real mag = magnitude(m_asForcePoint.force(r_particle));
			if(mag > m_asForceFilter.cap(r_particle))
			{
				SpatialVector f = m_asForcePoint.force(r_particle);
				m_asForcePoint.force(r_particle) *= m_asForceFilter.cap(r_particle)/mag;
				m_asForceFilter.impulse(r_particle) += m_asForceFilter.bleed(r_particle) * (f-m_asForcePoint.force(r_particle));
			}
		}
	}
	ticker.log("body points");
#endif
	ticker.log("end");

	unsigned long t1 = systemTick();
	unsigned long ms1 = m_ticker.timeMS();
	FE_MAYBE_UNUSED(t1);
	FE_MAYBE_UNUSED(ms1);

	//fe_fprintf(stderr, "STEP %lu %lu\n", t1-t0, ms1-ms0);
}



} /* namespace ext */
} /* namespace fe */

