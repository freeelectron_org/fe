/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Scan::handle(Record& record)
{
	if(!m_proxyRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_proxyRV.bind(spScope);
		m_observationRV.bind(spScope);
		m_cylinderRV.bind(spScope);

		m_spSelectorI=spScope->registry()->create("SelectorI");
		m_spFilterI=spScope->registry()->create("*.FactionFilter");

		m_selectOp.bind(spScope);
		m_selectOp.createRecord();
	}

	m_theaterRV.bind(record);

	const I32 ms=I32(m_theaterRV.time()*1e3f);
	const I32 maxLife=2000;		//* TODO param

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

//	feLog("\n");

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_scannerRAV.bind(spRA);

		if(m_scannerRAV.recordView().nearest.check(spRA))
		{
			for(Scanner& scannerRV: m_scannerRAV)
			{
				sp<RecordGroup>& rspSelection=scannerRV.selection();
				if(!rspSelection.isValid())
				{
					rspSelection=new RecordGroup;
				}

				Record& observation=scannerRV.purview();
				Record& shape=m_observationRV.shape(observation);

				const SpatialVector& location=scannerRV.location();
				m_cylinderRV.location(shape)=location;

				const F32 deltaT=m_theaterRV.deltaT();

				//* if really a cylinder
				if(deltaT>1e-3f && m_cylinderRV.span.check(shape))
				{
					SpatialVector& span=m_cylinderRV.span(shape);
					const SpatialVector original=unit(span);
					SpatialQuaternion rotation;

					if(scannerRV.tracking())
					{
						set(rotation,original,
								unit(scannerRV.track()-location));
					}
					else
					{
						set(rotation);
					}

					//* auto-spin
					rotation*=scannerRV.autoRotate();

					//* accelerate, constrained with predictive deceleration
					scannerRV.rotVelocity()=stepVelocity(
							scannerRV.rotVelocity(),rotation,
							deltaT,scannerRV.maxRotVelocity(),
							scannerRV.maxRotAcceleration());

					rotateVector(
							angularlyScaled(scannerRV.rotVelocity(),deltaT),
							span,span);
				}

				//* look
				m_spFilterI->configure(scannerRV.record());
				m_spSelectorI->configure(shape,m_spFilterI);

				m_selectOp.input()=spRG;
				m_selectOp.output()=rspSelection;
				sp<HandlerI> spHandlerI=m_spSelectorI;
				FEASSERT(spHandlerI.isValid());

				Record record=m_selectOp.record();
				spHandlerI->handle(record);

				WeakRecord closest=m_selectOp.choice();

				Record& nearest=scannerRV.nearest();
				if(closest.isValid())
				{
					SpatialVector target=scannerRV.location(closest);
					if(!nearest.isValid())
					{
						nearest=m_proxyRV.createRecord();
					}
					scannerRV.location(nearest)=target;
					m_proxyRV.inceptionMS(nearest)=ms;
				}
				else if(nearest.isValid())
				{
					const I32 life=ms-m_proxyRV.inceptionMS(nearest);
					if(life>maxLife)
					{
						//* expire
						nearest=Record();
					}
				}
			}
		}
		else if(m_scannerRAV.recordView().purview.check(spRA))
		{
			for(Scanner& scannerRV: m_scannerRAV)
			{
				Record& observation=scannerRV.purview();
				Record& shape=m_observationRV.shape(observation);
				scannerRV.location(shape)=scannerRV.location();
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
