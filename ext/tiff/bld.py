import sys
import os
import re
forge = sys.modules["forge"]

def setup(module):

    version = ""

    tiffvers_h = "/usr/include/tiffvers.h"
    if not os.path.exists(tiffvers_h):
        tiffvers_h = "/usr/include/x86_64-linux-gnu/tiffvers.h"
        if not os.path.exists(tiffvers_h):
            tiffvers_h = ""

    if tiffvers_h != "":
        for line in open(tiffvers_h).readlines():
            if re.match("#define TIFFLIB_VERSION.*", line):
                version = line.split()[2].rstrip()

    if version != "":
        sys.stdout.write(" " + version)

    srcList = [ "tiff.pmh",
                "ImageTiff",
                "tiffDL" ]

    dll = module.DLL( "fexTiffDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib",
                "fexImageDLLib" ]

#   dll.linkmap = { "gfxlibs": forge.gfxlibs }
    dll.linkmap = {}

    if forge.fe_os == "FE_LINUX":
        dll.linkmap["tifflibs"] = "-ltiff"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":

        if forge.codegen == "debug":
            dll.linkmap["tifflibs"] = "tiffd.lib"
            dll.linkmap["tifflibs"] += " zlibd.lib"
        else:
            dll.linkmap["tifflibs"] = "tiff.lib"
            dll.linkmap["tifflibs"] += " zlib.lib"

        dll.linkmap["tifflibs"] += " jpeg.lib"
        dll.linkmap["tifflibs"] += " lzma.lib"

    forge.deps( ["fexTiffDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexTiffDL",                        None,   None) ]

#   module.Module('test')

def auto(module):
    test_file = """
#include <tiff.h>

int main(void)
{
    return(0);
}
    \n"""

    ilmap = { "gfxlibs": forge.gfxlibs }
    if forge.fe_os == "FE_LINUX":
        ilmap["tifflibs"] = "-ltiff"
    elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        if forge.codegen == "debug":
            ilmap["tifflibs"] = "tiffd.lib"
        else:
            ilmap["tifflibs"] = "tiff.lib"

    return forge.cctest(test_file, ilmap)
