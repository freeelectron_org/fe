/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconServer_h__
#define __BeaconServer_h__

namespace beacon
{

// Time without a keep alive message until the node is considered alive
#define DEAD_TIME (10000)

struct NodeEntry
{
	uint8_t id;					// Unique ID per node
	uint32_t ipAddress;			// IP address of the none
	uint16_t replyPort;			// Requested port it will listen to the reponces from the beacon
	int64_t lastHeardFromTime;	// Last time the Beacon heard from the node

	NodeEntry(const uint8_t _id,
			const uint32_t _ipdAddress,
			const uint16_t _replyPort,
			const int64_t _lastHeardFromTime)
	{
		id = _id;
		ipAddress = _ipdAddress;
		replyPort = _replyPort;
		lastHeardFromTime = _lastHeardFromTime;
	}
};

/***************************************************************************//**
	@brief Node registration and simulation time syncronization

	@ingroup beacon
*//****************************************************************************/
class FE_DL_EXPORT BeaconServer : virtual public BeaconServerStartupI
{
public:
	BeaconServer();
	virtual ~BeaconServer();

	virtual bool startup(const char *fileName) { return true; }
	virtual bool start(uint16_t beaconPort, uint16_t gdPort) override;
	virtual void stop() override;
	virtual bool running() override;

	void getList(std::list<NodeEntry> &registeredNodes);

protected:
	GlobalDictionary m_globalDictionary;

private:
	bool m_initialized;
	std::atomic<bool> m_done;
	std::atomic<bool> m_running;
	uint8_t m_id;							// Next ID to be assigned
	std::thread* m_registeringThread;
	fe::sp<fe::SingleMaster> m_spSingleMaster;
	fe::sp<fe::ext::MessageI> m_recMsgSystem;
	fe::sp<fe::ext::MessageI> m_sendMsgSystem;
	std::list<NodeEntry> m_registeredNodes;
	std::mutex m_nodeListMutex;
	fe::ext::Messagegram m_listMsg;
	fe::ext::Messagegram m_timeMsg;
	fe::ext::Messagegram m_respMsg;

	void registrationThread();

	void add(const uint32_t ipAddress,
			 const uint16_t replyPort);
	void remove(const uint32_t ipAddress);
	void remove(const uint8_t id);
	void keepAlive(const uint32_t id);

	void sendTimeResponse(const int64_t time,
						  const int64_t estBeaconTime,
						  const uint16_t port);
	void sendKeepAliveResponse(const uint16_t port);
	void sendUpdatedList();
	void sendListMsgToAll(const fe::ext::Messagegram &msg);

	void checkForDeadNodes();
	int64_t getCurrentTime();

	void displayList();
	void clearList();
};

}  // namespace beacon

#endif // __BeaconServer_h__
