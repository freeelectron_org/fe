/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SAR_DEBUG			FALSE
#define FE_SAR_CLONE_DEBUG		FALSE
#define FE_SAR_COPY_DEBUG		FALSE
#define FE_SAR_COPY_TICKER		FALSE
#define FE_SAR_SPEC_DEBUG		FALSE
#define FE_SAR_GROUP_DEBUG		FALSE

namespace fe
{
namespace ext
{

SurfaceAccessibleRecord::SurfaceAccessibleRecord(void)
{
	for(I32 element=0;element<SurfaceAccessibleI::e_elementEnums;element++)
	{
		m_contiguous[element]=TRUE;
	}
}

SurfaceAccessibleRecord::~SurfaceAccessibleRecord(void)
{
}

void SurfaceAccessibleRecord::setDiscontiguous(
		SurfaceAccessibleI::Element a_element)
{
	m_contiguous[a_element]=FALSE;
}

void SurfaceAccessibleRecord::reset(void)
{
	SurfaceAccessibleBase::reset();

	if(m_spScope.isValid())
	{
		SurfaceModel surfaceModelRV;
		surfaceModelRV.bind(m_spScope);
		m_weakRecord=surfaceModelRV.createRecord();

		FEASSERT(m_weakRecord.isValid());

		m_spRecordGroup=new RecordGroup();
		tuneRecordGroup();

		m_spRecordGroup->add(m_weakRecord);

		m_groupNameAccessor.setup(m_spScope,"surf:groupName","string");
		m_groupSetAccessor.setup(m_spScope,"surf:indexSet","intset");
	}
	else
	{
		m_weakRecord=Record();
		m_spRecordGroup=NULL;
	}

	m_spForeignAccessible=NULL;
}

void SurfaceAccessibleRecord::ensureBinding(void)
{
	if(m_weakRecord.isValid())
	{
		return;
	}

	sp<Scope> spScope=registry()->create("Scope");
	bind(spScope);
}

I32 SurfaceAccessibleRecord::count(String a_nodeName,
	SurfaceAccessibleI::Element a_element) const
{
	if(m_spForeignAccessible.isValid())
	{
		return m_spForeignAccessible->count(a_nodeName,a_element);
	}

	return SurfaceAccessibleBase::count(a_nodeName,a_element);
}

void SurfaceAccessibleRecord::tuneRecordGroup(void)
{
	ensureBinding();

	m_spScope->setLocking(FALSE);

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_weakRecord);

	//* don't rearrange arrays during removal
	surfaceModelRV.surfacePointRG()->maintainOrder(true);
	surfaceModelRV.surfacePointSetRG()->maintainOrder(true);
	surfaceModelRV.surfaceVertexRG()->maintainOrder(true);
	surfaceModelRV.surfacePrimitiveRG()->maintainOrder(true);
	surfaceModelRV.surfacePrimitiveSetRG()->maintainOrder(true);
	surfaceModelRV.surfaceDetailRG()->maintainOrder(true);

	//* don't rebuild an exclusive mapping during removal
	surfaceModelRV.surfacePointRG()->enableDuplicates(true);
	surfaceModelRV.surfacePointSetRG()->enableDuplicates(true);
	surfaceModelRV.surfaceVertexRG()->enableDuplicates(true);
	surfaceModelRV.surfacePrimitiveRG()->enableDuplicates(true);
	surfaceModelRV.surfacePrimitiveSetRG()->enableDuplicates(true);
	surfaceModelRV.surfaceDetailRG()->enableDuplicates(true);
}

void SurfaceAccessibleRecord::bind(Record& a_rRecord)
{
	if(!a_rRecord.isValid())
	{
		reset();
		return;
	}

	m_spScope=a_rRecord.layout()->scope();

	//* check if just a payload surface (not SurfaceAccessibleRecord)
	sp<Layout> spSurfaceLayout=m_spScope->declare("surface");
	m_spScope->support("accessible","Component");
	m_spScope->populate("surface","accessible");

	Accessor< sp<Component> > accessorAccessible;
	accessorAccessible.setup(m_spScope,"accessible");

	m_groupNameAccessor.setup(m_spScope,"surf:groupName","string");
	m_groupSetAccessor.setup(m_spScope,"surf:indexSet","intset");

	if(accessorAccessible.check(a_rRecord))
	{
		reset();

#if TRUE
		m_spForeignAccessible=accessorAccessible(a_rRecord);
#else
		copy(accessorAccessible(a_rRecord));
		save("foreign.rg");
#endif

//		feLog("SurfaceAccessibleRecord::bind foreign surface %p \"%s\"\n",
//				m_spForeignAccessible.raw(),
//				m_spForeignAccessible->name().c_str());
	}
	else
	{
		m_weakRecord=a_rRecord;

		m_spRecordGroup=new RecordGroup();
		tuneRecordGroup();

		m_spRecordGroup->add(m_weakRecord);

		m_spForeignAccessible=NULL;
	}
}

//* TODO use SurfaceAccessibleRecord::copy()
Protectable* SurfaceAccessibleRecord::clone(Protectable* pInstance)
{
#if	FE_SAR_CLONE_DEBUG
	feLog("SurfaceAccessibleRecord::clone\n");

//X	const U32 us1=systemMicroseconds();
#endif

	if(!m_spScope.isValid())
	{
		feLog("SurfaceAccessibleRecord::clone no Scope\n");
		return NULL;
	}

	FEASSERT(m_spScope.isValid());

	SurfaceAccessibleRecord* pSurfaceAccessibleRecord= pInstance?
			fe_cast<SurfaceAccessibleRecord>(pInstance):
			new SurfaceAccessibleRecord();

	pSurfaceAccessibleRecord->setLibrary(library());
	pSurfaceAccessibleRecord->setFactoryIndex(factoryIndex());

	//* NOTE would be risky to share scope

	sp<Scope> spScope=registry()->create("Scope");
	pSurfaceAccessibleRecord->bind(spScope);

	pSurfaceAccessibleRecord->copy(sp<SurfaceAccessibleI>(this));
	return pSurfaceAccessibleRecord;

/*X
	sp<RecordGroup>& rspRecordGroup=pSurfaceAccessibleRecord->m_spRecordGroup;

#if	FE_SAR_CLONE_DEBUG
	const U32 us2=systemMicroseconds();
#endif

	// why is direct cloning slower?
//	Cloner cloner(m_spScope);
//	rspRecordGroup=cloner.clone(m_spRecordGroup);

	sp<data::StreamI> spOutputStream=
			sp<data::StreamI>(new data::BinaryStream(m_spScope));

	std::stringstream stringStream;
	spOutputStream->output(stringStream,m_spRecordGroup);

#if	FE_SAR_CLONE_DEBUG
	const U32 us3=systemMicroseconds();
#endif

	sp<data::StreamI> spInputStream=
			sp<data::StreamI>(new data::BinaryStream(spScope));
	rspRecordGroup=spInputStream->input(stringStream);

#if	FE_SAR_CLONE_DEBUG
	const U32 us4=systemMicroseconds();
#endif

	// update surface with new Record
	RecordGroup::iterator it=rspRecordGroup->begin();
	sp<RecordArray> spRA= *it;
	Record record=spRA->getRecord(0);
	pSurfaceAccessibleRecord->bind(record);

	FEASSERT(pSurfaceAccessibleRecord->m_spScope.isValid());

	pSurfaceAccessibleRecord->tuneRecordGroup();

#if	FE_SAR_CLONE_DEBUG
	const U32 us5=systemMicroseconds();

	feLog("SurfaceAccessibleRecord::clone us %d %d %d %d -> %d\n",
			us2-us1,us3-us2,us4-us3,us5-us4,us5-us1);
#endif

	return pSurfaceAccessibleRecord;
*/
}

BWORD SurfaceAccessibleRecord::load(String a_filename,sp<Catalog> a_spSettings)
{
#if	FE_SAR_DEBUG
	feLog("SurfaceAccessibleRecord::load \"%s\"\n",a_filename.c_str());
#endif

	BWORD binary=FALSE;

	if(a_spSettings.isValid())
	{
		const String options=a_spSettings->catalog<String>("options");
		binary=options.match(".*\\bbinary\\b.*");
	}

	ensureBinding();

	FEASSERT(m_spScope.isValid());

	if(a_filename.chop(".obj")!=a_filename)
	{
		String buffer;
		buffer+="DEFAULTGROUP 1\n";
		buffer+="RECORD Surface0 SurfaceOBJ\n";
		buffer+="	surf:filename \""+a_filename+"\"\n";
		buffer+="END\n";

		m_spRecordGroup=
				RecordView::loadRecordGroupFromBuffer(m_spScope,buffer,FALSE);
	}
	else if(a_filename.chop(".stl")!=a_filename)
	{
		String buffer;
		buffer+="DEFAULTGROUP 1\n";
		buffer+="RECORD Surface0 SurfaceSTL\n";
		buffer+="	surf:filename \""+a_filename+"\"\n";
		buffer+="END\n";

		m_spRecordGroup=
				RecordView::loadRecordGroupFromBuffer(m_spScope,buffer,FALSE);
	}
	else
	{
		m_spRecordGroup=
				RecordView::loadRecordGroup(m_spScope,a_filename,binary);
	}

	if(m_spRecordGroup.isNull())
	{
		feLog("SurfaceAccessibleRecord::load NULL RecordGroup\n");
		return FALSE;
	}

	RecordGroup::iterator it=m_spRecordGroup->begin();
	sp<RecordArray> spRA= *it;

	FEASSERT(spRA.isValid());
	m_weakRecord=spRA->getRecord(0);

	tuneRecordGroup();

	outlineCreateDefault();

	return TRUE;
}

BWORD SurfaceAccessibleRecord::save(String a_filename,sp<Catalog> a_spSettings)
{
#if	FE_SAR_DEBUG
	feLog("SurfaceAccessibleRecord::save\n");
#endif

	if(m_spForeignAccessible.isValid())
	{
		feLog("SurfaceAccessibleRecord::save"
				" surface with foreign payload of \"%s\" not saved to \"%s\"\n",
				m_spForeignAccessible->factoryName().c_str(),
				a_filename.c_str());
		return FALSE;
	}

	if(m_spScope.isNull() || m_spRecordGroup.isNull())
	{
		feLog("SurfaceAccessibleRecord::save non data\n");
		return FALSE;
	}

	FEASSERT(m_spScope.isValid());
	FEASSERT(m_spRecordGroup.isValid());

	const String options=a_spSettings.isValid()?
			a_spSettings->catalog<String>("options"): "";
	const BWORD binary=options.match(".*\\bbinary\\b.*");

	RecordView::saveRecordGroup(m_spScope,m_spRecordGroup,a_filename,binary);
	return TRUE;
}

void SurfaceAccessibleRecord::groupNames(Array<String>& a_rNameArray,
	SurfaceAccessibleI::Element a_element) const
{
	a_rNameArray.clear();

	if(!m_weakRecord.isValid())
	{
		return;
	}

	if(a_element!=SurfaceAccessibleI::e_pointGroup &&
			a_element!=SurfaceAccessibleI::e_primitiveGroup)
	{
		return;
	}

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_weakRecord);

	sp<RecordGroup> spRG=(a_element==SurfaceAccessibleI::e_pointGroup)?
			surfaceModelRV.surfacePointSetRG():
			surfaceModelRV.surfacePrimitiveSetRG();

	RecordGroup::iterator it=spRG->begin();
	if(it==spRG->end())
	{
		return;
	}

	sp<RecordArray> spRA= *it;
	const I32 length=spRA->length();
	for(I32 index=0;index<length;index++)
	{
		Record record=spRA->getRecord(index);
		const String groupName=m_groupNameAccessor(record);
		a_rNameArray.push_back(groupName);
	}
}

void SurfaceAccessibleRecord::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
	a_rSpecs.clear();

	//* NOTE a_node ignored

	if(m_spForeignAccessible.isValid())
	{
		return m_spForeignAccessible->attributeSpecs(a_rSpecs,a_element);
	}

	if(!m_weakRecord.isValid())
	{
		return;
	}

#if FE_SAR_SPEC_DEBUG
	feLog("SurfaceAccessibleRecord::attributeSpecs element %s\n",
			elementLayout(a_element).c_str());
#endif

	if(a_element==SurfaceAccessibleI::e_pointGroup ||
			a_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		Array<String> nameArray;
		groupNames(nameArray,a_element);

		for(String groupName: nameArray)
		{
			if(groupName.empty())
			{
				feLog("SurfaceAccessibleRecord::attributeSpecs"
						" element %s contains empty group name\n",
						elementLayout(a_element).c_str());
				continue;
			}
			SurfaceAccessibleI::Spec spec;
			spec.set(groupName,"integer");

			a_rSpecs.push_back(spec);
		}
		return;
	}

	SurfacePoint surfaceElementRV;

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_weakRecord);

	sp<RecordGroup> spRG=surfaceModelRV.surfacePointRG();
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			spRG=surfaceModelRV.surfacePointRG();
			break;
		case SurfaceAccessibleI::e_vertex:
			spRG=surfaceModelRV.surfaceVertexRG();
			break;
		case SurfaceAccessibleI::e_primitive:
			spRG=surfaceModelRV.surfacePrimitiveRG();
			break;
		case SurfaceAccessibleI::e_detail:
			spRG=surfaceModelRV.surfaceDetailRG();
			break;
		default:
			return;
	}

	if(spRG.isNull() || !spRG->count())
	{
#if FE_SAR_SPEC_DEBUG
		feLog("SurfaceAccessibleRecord::attributeSpecs element %s no data\n",
				elementLayout(a_element).c_str());
#endif
		return;
	}

	RecordGroup::iterator it=spRG->begin();
	sp<RecordArray> spRecordArray= *it;
	if(spRecordArray.isNull())
	{
		return;
	}

	if(a_element==SurfaceAccessibleI::e_point)
	{
		surfaceElementRV.bind(spRecordArray->getWeakRecord(0));
	}

#if FALSE
	String layoutName=elementLayout(a_element);
	sp<Layout> spElementLayout=m_spScope->lookupLayout(layoutName);
	if(spElementLayout.isNull())
	{
		spElementLayout=spRecordArray->layout();
		layoutName=spElementLayout->name();
	}
#else
	sp<Layout> spElementLayout=spRecordArray->layout();
	const String layoutName=spElementLayout->name();
#endif

	sp<TypeMaster> spTypeMaster=registry()->master()->typeMaster();

#if FE_SAR_SPEC_DEBUG
	feLog("SurfaceAccessibleRecord::attributeSpecs layout %s %p\n",
			layoutName.c_str(),spElementLayout.raw());
#endif

	if(spElementLayout.isValid())
	{
		const U32 attrCount=spElementLayout->attributeCount();

#if FE_SAR_SPEC_DEBUG
		feLog("SurfaceAccessibleRecord::attributeSpecs attrCount %d\n",
				attrCount);
#endif

		for(U32 attrIndex=0;attrIndex<attrCount;attrIndex++)
		{
			sp<fe::Attribute> spAttribute=spElementLayout->attribute(attrIndex);
			const String attrName=spAttribute->name();

			if(attrName==":CT" || attrName==":SN")
			{
				continue;
			}

			if(a_element==SurfaceAccessibleI::e_point)
			{
				//* TODO check other attributes
				if(attrName=="P" &&
						!surfaceElementRV.at.check())
				{
					//* NOTE probably merits an exception
					continue;
				}
				if(attrName=="N" &&
						!surfaceElementRV.up.check())
				{
					continue;
				}
				if(attrName=="uvw" &&
						!surfaceElementRV.uvw.check())
				{
					continue;
				}
				if(attrName=="Cd" &&
						!surfaceElementRV.color.check())
				{
					continue;
				}
			}

			const String attrTypeName=spTypeMaster->lookupBaseType(
					spAttribute->type());
			String typeName=attrTypeName;	//* string, color

			if(attrTypeName=="I32")
			{
				typeName="integer";
			}
			else if(attrTypeName=="F32")
			{
				typeName="real";
			}
			else if(attrTypeName=="vector3f")
			{
				typeName="vector3";
			}

#if FE_SAR_SPEC_DEBUG
			feLog("SurfaceAccessibleRecord::attributeSpecs %d/%d %s %s %s\n",
					attrIndex,attrCount,
					attrTypeName.c_str(),typeName.c_str(),attrName.c_str());
#endif

			SurfaceAccessibleI::Spec spec;
			spec.set(commonName(attrName),typeName);

			a_rSpecs.push_back(spec);
		}
	}
}

void SurfaceAccessibleRecord::require(SurfaceAccessibleI::Element a_element,
	String a_attrName,String a_attrTypeName)
{
	ensureBinding();

	const String layoutName=elementLayout(a_element);

	m_spScope->declare(layoutName);
	m_spScope->support(a_attrName,a_attrTypeName);
	m_spScope->populate(layoutName,a_attrName);
}

sp<SurfaceAccessorI> SurfaceAccessibleRecord::accessor(
	String a_node,Element a_element,String a_name,
	Creation a_create,Writable a_writable)
{
#if FE_SAR_DEBUG
	feLog("SurfaceAccessibleRecord::accessor"
			" element %d attr \"%s\" create %d\n",
			a_element,a_name.c_str(),a_create);
#endif

	if(m_spForeignAccessible.isValid())
	{
#if FE_SAR_DEBUG
		feLog("SurfaceAccessibleRecord::accessor foreign\n");
#endif

		return m_spForeignAccessible->accessor(a_node,a_element,
				a_name,a_create,a_writable);
	}

	if(!m_weakRecord.isValid())
	{
		feX(e_notInitialized,"no data");
	}

	FEASSERT(m_weakRecord.isValid());

	sp<SurfaceAccessorRecord> spAccessor(new SurfaceAccessorRecord);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->bind(m_weakRecord);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
#if FE_SAR_DEBUG
			feLog("SurfaceAccessibleRecord::accessor spAccessor %p\n",
					spAccessor.raw());
#endif

			return spAccessor;
		}
	}

#if FE_SAR_DEBUG
	feLog("SurfaceAccessibleRecord::accessor"
			" failed element %d attr \"%s\" create %d\n",
			a_element,a_name.c_str(),a_create);
#endif

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleRecord::accessor(
	String a_node,Element a_element,Attribute a_attribute,
	Creation a_create,Writable a_writable)
{
#if FE_SAR_DEBUG
	feLog("SurfaceAccessibleRecord::accessor"
			" element %d attr %d create %d\n",
			a_element,a_attribute,a_create);
#endif

	if(m_spForeignAccessible.isValid())
	{
#if FE_SAR_DEBUG
		feLog("SurfaceAccessibleRecord::accessor foreign\n");
#endif

		return m_spForeignAccessible->accessor(a_node,a_element,
				a_attribute,a_create,a_writable);
	}

	if(!m_weakRecord.isValid())
	{
		feX(e_notInitialized,"no data");
	}

	FEASSERT(m_weakRecord.isValid());

	sp<SurfaceAccessorRecord> spAccessor(new SurfaceAccessorRecord);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);

		FEASSERT(m_weakRecord.isValid());

		spAccessor->bind(m_weakRecord);

		FEASSERT(m_weakRecord.isValid());

		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		FEASSERT(m_weakRecord.isValid());

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			FEASSERT(m_weakRecord.isValid());

#if FE_SAR_DEBUG
			feLog("SurfaceAccessibleRecord::accessor spAccessor %p\n",
					spAccessor.raw());
#endif

			return spAccessor;
		}

#if FE_SAR_DEBUG
		feLog("SurfaceAccessibleRecord::accessor"
				" failed element %d attr %d create %d\n",
				a_element,a_attribute,a_create);
#endif
	}

	FEASSERT(m_weakRecord.isValid());

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceI> SurfaceAccessibleRecord::surface(String a_group,
	SurfaceI::Restrictions a_restrictions)
{
#if	FE_SAR_DEBUG
	feLog("SurfaceAccessibleRecord::surface(\"%s\")\n",a_group.c_str());
#endif

	if(m_spForeignAccessible.isValid())
	{
		feLog("SurfaceAccessibleRecord::surface foreign delegation\n");

		return m_spForeignAccessible->surface(a_group,a_restrictions);
	}

	//* NOTE some RecordGroup files create their own SurfaceI component
	if(m_weakRecord.isValid())
	{
		SurfaceModel surfaceModelRV;
		surfaceModelRV.bind(m_weakRecord);

		cp<SurfaceI> cpSurfaceI=surfaceModelRV.component();

		if(cpSurfaceI.isValid())
		{
			return cpSurfaceI;
		}
	}

	return SurfaceAccessibleBase::surface(a_group,a_restrictions);
}

sp<SurfaceI> SurfaceAccessibleRecord::subSurface(U32 a_subIndex,String a_group,
	SurfaceI::Restrictions a_restrictions)
{
	//* TODO

	return sp<SurfaceI>(NULL);
}

void SurfaceAccessibleRecord::copy(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible)
{
	reset();

	sp<SurfaceAccessibleRecord> spSurfaceAccessibleRecord(
			a_spSurfaceAccessible);
	if(spSurfaceAccessibleRecord.isValid() &&
			spSurfaceAccessibleRecord->m_spForeignAccessible.isValid())
	{
		//* NOTE not really a deep copy
		m_spForeignAccessible=spSurfaceAccessibleRecord->m_spForeignAccessible;
		return;
	}

#if FE_SAR_COPY_DEBUG
	feLog("SurfaceAccessibleRecord::copy %p direct %d\n",
			a_spSurfaceAccessible.raw(),
			spSurfaceAccessibleRecord.isValid());
#endif

#if FE_SAR_COPY_TICKER
	const U32 tickStart=systemTick();
	U32 tickSumA(0);
	U32 tickSumB(0);
#endif

	if(spSurfaceAccessibleRecord.isNull())
	{
		append(a_spSurfaceAccessible,NULL);
	}
	else
	{
		FEASSERT(m_spScope.isValid());

		SurfaceModel sourceRV;
		sourceRV.bind(spSurfaceAccessibleRecord->m_weakRecord);

		SurfaceModel destRV;
		destRV.bind(m_weakRecord);

		for(I32 element=0;element<SurfaceAccessibleI::e_elementEnums;element++)
		{
			sp<RecordGroup> spSourceRG;
			sp<RecordGroup> spDestRG;

			switch(element)
			{
				case e_point:
						spSourceRG=sourceRV.surfacePointRG();
						spDestRG=destRV.surfacePointRG();
					break;
				case e_pointGroup:
						spSourceRG=sourceRV.surfacePointSetRG();
						spDestRG=destRV.surfacePointSetRG();
					break;
				case e_vertex:
						spSourceRG=sourceRV.surfaceVertexRG();
						spDestRG=destRV.surfaceVertexRG();
					break;
				case e_primitive:
						spSourceRG=sourceRV.surfacePrimitiveRG();
						spDestRG=destRV.surfacePrimitiveRG();
					break;
				case e_primitiveGroup:
						spSourceRG=sourceRV.surfacePrimitiveSetRG();
						spDestRG=destRV.surfacePrimitiveSetRG();
					break;
				case e_detail:
						spSourceRG=sourceRV.surfaceDetailRG();
						spDestRG=destRV.surfaceDetailRG();
					break;
				default:
					continue;
			}

#if FE_SAR_COPY_DEBUG
			feLog("  PASS %d \"%s\"\n",element,elementLayout(
					SurfaceAccessibleI::Element(element)).c_str());
#endif

			RecordGroup::iterator it=spSourceRG->begin();
			if(it==spSourceRG->end())
			{
#if FE_SAR_COPY_DEBUG
				feLog("    no source RA\n");
#endif
				continue;
			}

			sp<RecordArray> spSourceRA= *it;
			sp<LayoutAV> spSourceLayout=spSourceRA->layout();
			const I32 recordCount=spSourceRA->length();
			if(!recordCount)
			{
#if FE_SAR_COPY_DEBUG
				feLog("    no records in RA\n");
#endif
				continue;
			}

			const BWORD contiguous=m_contiguous[element];
#if FE_SAR_COPY_DEBUG
				feLog("    contiguous %d\n",contiguous);
#endif

			const String layoutName=spSourceLayout->name();
			sp<LayoutAV> spDestLayout=m_spScope->declare(layoutName);

			const I32 attributeCount=spSourceLayout->attributeCount();
			for(I32 attributeIndex=0;attributeIndex<attributeCount;
					attributeIndex++)
			{
				sp<fe::Attribute> spSourceAttribute=
						spSourceLayout->attribute(attributeIndex);
				const String attrName=spSourceAttribute->name();

				if(attrName==":CT" || attrName==":SN")
				{
					continue;
				}

				const String attrTypeName=
						spSourceAttribute->typeMaster()->lookupBaseType(
						spSourceAttribute->type());

#if FE_SAR_COPY_DEBUG
				feLog("    add Attribute '%s' \"%s\"\n",
						attrTypeName.c_str(),attrName.c_str());
#endif

				m_spScope->support(attrName,attrTypeName);
				m_spScope->populate(layoutName,attrName);
			}

#if FE_SAR_COPY_TICKER
			const U32 tickStartA=systemTick();
#endif

			//* add first Record to create RecordArray
			spDestRG->add(m_spScope->createRecord(spDestLayout));

			it=spDestRG->begin();
			if(it==spDestRG->end())
			{
#if FE_SAR_COPY_DEBUG
				feLog("    no dest RA\n");
#endif
				continue;
			}

			sp<RecordArray> spDestRA= *it;

			//* add remaining Records
			spDestRA->addCreateCovert(recordCount-1);

#if FE_SAR_COPY_TICKER
			const U32 tickEndA=systemTick();
			const U32 tickStartB=tickEndA;
#endif

			for(I32 attributeIndex=0;attributeIndex<attributeCount;
					attributeIndex++)
			{
				sp<fe::Attribute> spSourceAttribute=
						spSourceLayout->attribute(attributeIndex);
				const String attrName=spSourceAttribute->name();

				if(attrName==":CT" || attrName==":SN")
				{
					continue;
				}

				const String attrTypeName=
						spSourceAttribute->typeMaster()->lookupBaseType(
						spSourceAttribute->type());

#if FE_SAR_COPY_DEBUG
				feLog("    copy Attribute \"%s\"\n",attrName.c_str());
#endif

				if(spSourceAttribute->isCloneable())
				{
					const FE_UWORD sourceLocator=
							spSurfaceAccessibleRecord->m_spScope
							->attributeIndex(spSourceAttribute);

					FE_UWORD destLocator(0);
					if(!m_spScope->findAttribute(
							attrName,destLocator).isValid())
					{
#if FE_SAR_COPY_DEBUG
						feLog("      no dest locator\n");
#endif
						continue;
					}

#if FE_SAR_COPY_DEBUG
					feLog("      locator %d %d\n",sourceLocator,destLocator);
#endif

					sp<BaseType> spBT=spSourceAttribute->type();
					const I32 iosize=spBT->getInfo()->iosize();
					const I32 typesize=spBT->size();

#if FE_SAR_COPY_DEBUG
					feLog("      copy \"%s\" iosize %d typesize %d\n",
							attrName.c_str(),iosize,typesize);
#endif

					U8* pDestData=
							(U8*)spDestLayout->baseOfAV(destLocator);

					if(contiguous)
					{
						//* NOTE not safe if any Record has ever been removed
						//* Layouts won't line up

						U8* pSourceData=
								(U8*)spSourceLayout->baseOfAV(sourceLocator);

						if(iosize==BaseType::Info::c_implicit)
						{
							for(I32 recordIndex=0;recordIndex<recordCount;
									recordIndex++)
							{
								const U32 offset=recordIndex*typesize;
								spBT->assign(
										pDestData+offset,
										pSourceData+offset);
							}
						}
						else
						{
							memcpy(pDestData,pSourceData,recordCount*iosize);
						}
					}
					else
					{
						for(I32 recordIndex=0;recordIndex<recordCount;
								recordIndex++)
						{
							Record sourceRecord=
									spSourceRA->getRecord(recordIndex);
//							Record destRecord=
//									spDestRA->getRecord(recordIndex);

//							spBT->assign(
//									destRecord.rawAttribute(destLocator),
//									sourceRecord.rawAttribute(sourceLocator));

							spBT->assign(
									pDestData+recordIndex*typesize,
									sourceRecord.rawAttribute(sourceLocator));
						}
					}
				}
			}
#if FE_SAR_COPY_TICKER
			const U32 tickEndB=systemTick();

			tickSumA+=SystemTicker::tickDifference(tickStartA,tickEndA);
			tickSumB+=SystemTicker::tickDifference(tickStartB,tickEndB);
#endif
		}

//		save("copy.rg");
	}

#if FE_SAR_COPY_TICKER
	const U32 tickEnd=systemTick();
	const U32 tickDiff=SystemTicker::tickDifference(tickStart,tickEnd);
	const Real ms=1e-3*tickDiff*SystemTicker::microsecondsPerTick();
	feLog("SurfaceAccessibleRecord::copy took %.6G ms\n",ms);

	const Real msA=1e-3*tickSumA*SystemTicker::microsecondsPerTick();
	const Real msB=1e-3*tickSumB*SystemTicker::microsecondsPerTick();
	feLog("SurfaceAccessibleRecord::copy create %.6G ms  value %.6G ms\n",
			msA,msB);
#endif
}

void SurfaceAccessibleRecord::copy(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible,sp<FilterI> a_spFilter)
{
	reset();

	sp<SurfaceAccessibleRecord> spSurfaceAccessibleRecord(
			a_spSurfaceAccessible);
	if(spSurfaceAccessibleRecord.isValid() &&
			spSurfaceAccessibleRecord->m_spForeignAccessible.isValid())
	{

		//* NOTE not really a deep copy
		m_spForeignAccessible=spSurfaceAccessibleRecord->m_spForeignAccessible;
		return;
	}

	if(a_spFilter.isNull())
	{
		copy(a_spSurfaceAccessible);
		return;
	}

	append(a_spSurfaceAccessible,NULL,a_spFilter);
}

sp<SurfaceAccessibleBase::MultiGroup> SurfaceAccessibleRecord::generateGroup(
	SurfaceAccessibleI::Element a_element,
	String a_groupString)
{
#if FE_SAR_GROUP_DEBUG
	feLog("SurfaceAccessibleRecord::generateGroup %s \"%s\"\n",
			elementLayout(a_element).c_str(),a_groupString.c_str());
#endif

	if(!m_weakRecord.isValid())
	{
		feX(e_notInitialized,"no data");
	}

	sp<MultiGroup> spMultiGroup=
			sp<MultiGroup>(new MultiGroup());

	const String depletedString=
			addGroupRanges(spMultiGroup,a_groupString);
//	const BWORD depleted=(depletedString!=a_groupString);

	SurfaceModel surfaceModelRV;
	surfaceModelRV.bind(m_weakRecord);

	sp<RecordGroup> spRG=(a_element==SurfaceAccessibleI::e_pointGroup)?
			surfaceModelRV.surfacePointSetRG():
			surfaceModelRV.surfacePrimitiveSetRG();

	RecordGroup::iterator it=spRG->begin();
	if(it==spRG->end())
	{
		return spMultiGroup;
	}

	sp<RecordArray> spRA= *it;

	Array<String> patternArray;
	String buffer=depletedString;
	while(!buffer.empty())
	{
		patternArray.push_back(buffer.parse());
	}
	const U32 patternCount=patternArray.size();

	const U32 nameCount=spRA->length();
	for(U32 nameIndex=0;nameIndex<nameCount;nameIndex++)
	{
		Record record=spRA->getRecord(nameIndex);
		const String groupName=m_groupNameAccessor(record);

#if FE_SAR_GROUP_DEBUG
		feLog("  groupName %d/%d \"%s\"\n",
				nameIndex,nameCount,groupName.c_str());
#endif

		for(U32 patternIndex=0;patternIndex<patternCount;patternIndex++)
		{
			const String pattern=patternArray[patternIndex];

#if FE_SAR_GROUP_DEBUG
			feLog("  pattern %d/%d \"%s\"\n",
					patternIndex,patternCount,pattern.c_str());
#endif

			if(!groupName.match(pattern))
			{
				continue;
			}

#if FE_SAR_GROUP_DEBUG
			feLog("  MATCH\n");
#endif

			const std::set<I32>& rGroupSet=m_groupSetAccessor(record);
			for(I32 entry: rGroupSet)
			{
				spMultiGroup->insert(entry);
			}
		}
	}

	return spMultiGroup;
}

} /* namespace ext */
} /* namespace fe */
