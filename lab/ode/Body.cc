/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ode/ode.pmh>

namespace fe
{
namespace ext
{


Body::Body(dWorldID a_world_id)
{
	m_id = dBodyCreate(a_world_id);
}

Body::~Body(void)
{
	dBodyDestroy(m_id);
}

void Body::setPosition(const Vector3 &a_pos)
{
	dBodySetPosition(m_id, a_pos[0], a_pos[1], a_pos[2]);
}

void Body::getRotation(SpatialTransform &a_rotation)
{
	const dReal *r = dBodyGetRotation(m_id);
	a_rotation(0,0) = r[0];
	a_rotation(0,1) = r[1];
	a_rotation(0,2) = r[2];
	a_rotation(1,0) = r[4];
	a_rotation(1,1) = r[5];
	a_rotation(1,2) = r[6];
	a_rotation(2,0) = r[8];
	a_rotation(2,1) = r[9];
	a_rotation(2,2) = r[10];
}

void Body::setRotation(const SpatialTransform &a_rotation)
{
	dMatrix3 r;
	r[0] = a_rotation(0,0);
	r[1] = a_rotation(0,1);
	r[2] = a_rotation(0,2);
	r[4] = a_rotation(1,0);
	r[5] = a_rotation(1,1);
	r[6] = a_rotation(1,2);
	r[8] = a_rotation(2,0);
	r[9] = a_rotation(2,1);
	r[10] = a_rotation(2,2);
	dBodySetRotation(m_id, r);
}

void Body::getPosition(Vector3 &a_pos)
{
	const dReal *p = dBodyGetPosition(m_id);
	a_pos[0] = p[0];
	a_pos[1] = p[1];
	a_pos[2] = p[2];

	SpatialTransform xform;
	getRotation(xform);

	SpatialVector displ;
	transformVector(xform, m_offset, displ);
	a_pos -= displ;
}

void Body::applyForce(const SpatialVector &a_force, const SpatialVector &a_pos)
{
	dBodyAddForceAtPos(m_id, a_force[0], a_force[1], a_force[2],
		a_pos[0], a_pos[1], a_pos[2]);
}

void Body::applyTorque(const SpatialVector &a_torque)
{
	dBodyAddTorque(m_id, a_torque[0], a_torque[1], a_torque[2]);
}

void Body::getVelocity(SpatialVector &a_velocity, const SpatialVector &a_pos)
{
	dVector3 result;
	dBodyGetPointVel(m_id, a_pos[0], a_pos[1], a_pos[2], result);
	a_velocity[0] = result[0];
	a_velocity[1] = result[1];
	a_velocity[2] = result[2];
}

void Body::getAngularVelocity(SpatialVector &a_velocity)
{
	const dReal *result = dBodyGetAngularVel(m_id);
	a_velocity[0] = result[0];
	a_velocity[1] = result[1];
	a_velocity[2] = result[2];
}

void Body::translate(const SpatialVector &a_displ)
{
	dMass mass;
	dBodyGetMass(m_id, &mass);
	dMassTranslate (&mass, a_displ[0], a_displ[1], a_displ[2]);
	dBodySetMass(m_id, &mass);
}

#define _I(i,j) I[(i)*4+(j)]

void Body::setMass(const Mass &a_mass)
{
	SpatialTransform xform;
	getRotation(xform);

	Mass m = a_mass;

	SpatialVector displ;
	transformVector(xform, a_mass.getCg(), displ);

	m.translate(-a_mass.getCg());

	SpatialVector pos;
	getPosition(pos);
	pos += displ;
	setPosition(pos);
	m_offset = a_mass.getCg();

	dMass mass;
	mass.mass = m.getMass();
	for(int i = 0; i < 3; i++)
	{
		mass.c[i] = m.getCg()[i];
		for(int j = 0; j < 3; j++)
		{
			mass._I(i,j) = m.getI()(i,j);
		}
	}
	if(!dMassCheck(&mass))
	{
		feX("mass failure\n");
	}
	dBodySetMass(m_id, &mass);

}

void Body::getMass(Mass &a_mass)
{
	dMass mass;
	dBodyGetMass(m_id, &mass);
	a_mass.mass() = mass.mass;
	for(int i = 0; i < 3; i++)
	{
		a_mass.cg()[i] = mass.c[i];
		for(int j = 0; j < 3; j++)
		{
			a_mass.I()(i,j) = mass._I(i,j);
		}
	}

	a_mass.translate(m_offset);
}

} /* namespace ext */
} /* namespace fe */

