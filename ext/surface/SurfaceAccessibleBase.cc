/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SAB_DEBUG		FALSE
#define FE_SAB_COPY_DEBUG	FALSE
#define FE_SAB_COPY_TICKER	FALSE
#define FE_SAB_DELETE_DEBUG	FALSE

#define	FE_SAB_LOCK_CHECK	TRUE
#define	FE_SAB_LOCK_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceAccessibleBase::SurfaceAccessibleBase(void):
	m_locker(-1)
{
	reset();
}

SurfaceAccessibleBase::~SurfaceAccessibleBase(void)
{
	FEASSERT(m_locker== -1);
}

void SurfaceAccessibleBase::reset(void)
{
	m_paging=FALSE;
}

void SurfaceAccessibleBase::lock(I64 a_id)
{
#if FE_SAB_LOCK_CHECK
#if FE_SAB_LOCK_DEBUG
	feLog("SurfaceAccessibleBase::lock %p vs %p locking \"%s\"\n",
			a_id,m_locker,name().c_str());
#endif

	FEASSERT(a_id!=m_locker);

	if(a_id==m_locker)
	{
		feLog("SurfaceAccessibleBase::lock"
				" refusing to deadlock \"%s\" with %p\n",
				name().c_str(),a_id);
		return;
	}
#endif

	safeLock();

#if FE_SAB_LOCK_CHECK
#if FE_SAB_LOCK_DEBUG
	feLog("SurfaceAccessibleBase::lock %p vs %p locked \"%s\"\n",
			a_id,m_locker,name().c_str());
#endif

	FEASSERT(m_locker== -1);
	m_locker=a_id;
#endif
}

void SurfaceAccessibleBase::unlock(I64 a_id)
{
#if FE_SAB_LOCK_CHECK
#if FE_SAB_LOCK_DEBUG
	feLog("SurfaceAccessibleBase::unlock %p vs %p unlocking \"%s\"\n",
			a_id,m_locker,name().c_str());
#endif

	FEASSERT(m_locker==a_id);

	if(m_locker== -1)
	{
		feLog("SurfaceAccessibleBase::unlock"
				" unexpected unlock of \"%s\" with %p\n",
				name().c_str(),a_id);
		return;
	}
	else if(a_id!=m_locker)
	{
		feLog("SurfaceAccessibleBase::unlock"
				" refusing to unlock \"%s\" with %p when locked with %p\n",
				name().c_str(),a_id,m_locker);
		return;
	}

	m_locker= -1;
#endif

	safeUnlock();

#if FE_SAB_LOCK_CHECK
#if FE_SAB_LOCK_DEBUG
	feLog("SurfaceAccessibleBase::unlock %p vs %p unlocked \"%s\"\n",
			a_id,m_locker,name().c_str());
#endif
#endif
}

BWORD SurfaceAccessibleBase::load(String a_filename,sp<Catalog> a_spSettings)
{
	feLog("SurfaceAccessibleBase::load no support available for \"%s\"\n",
			a_filename.c_str());
	return FALSE;
}

void SurfaceAccessibleBase::instance(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible,
	const Array<SpatialTransform>& a_rTransformArray)
{
//~	const U32 transformCount=a_rTransformArray.size();
//~	for(U32 transformIndex=0;transformIndex<transformCount;transformIndex++)
//~	{
//~		append(a_spSurfaceAccessible,&a_rTransformArray[transformIndex]);
//~	}

	append(a_spSurfaceAccessible,"",a_rTransformArray,sp<FilterI>(NULL));
}

void SurfaceAccessibleBase::append(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible,
	const SpatialTransform* a_pTransform,sp<FilterI> a_spFilter)
{
	Array<SpatialTransform> transformArray;
	if(a_pTransform)
	{
		transformArray.resize(1);
		transformArray[0]= *a_pTransform;
	}

	append(a_spSurfaceAccessible,"",transformArray,a_spFilter);
}

//* TODO fix for Maya, or provide Maya version with filtering
void SurfaceAccessibleBase::append(
	sp<SurfaceAccessibleI> a_spSurfaceAccessible,
	String a_nodeName,
	const Array<SpatialTransform>& a_rTransformArray,
	sp<FilterI> a_spFilter)
{
#if FE_SAB_COPY_DEBUG
	feLog("SurfaceAccessibleBase::append \"%s\"\n",a_nodeName.c_str());
#endif

#if FE_SAB_COPY_TICKER
	const U32 tickStart=systemTick();
#endif

	sp<SurfaceAccessorI> spInputPosition=a_spSurfaceAccessible->accessor(
			a_nodeName,e_point,e_position,e_refuseMissing);

	sp<SurfaceAccessorI> spOutputPosition=
			accessor("",e_point,e_position,e_createMissing,e_readWrite);

	sp<SurfaceAccessorI> spInputVertices=a_spSurfaceAccessible->accessor(
			a_nodeName,e_primitive,e_vertices,e_refuseMissing);

	sp<SurfaceAccessorI> spOutputVertices=
			accessor("",e_primitive,e_vertices,e_createMissing,e_readWrite);

	sp<SurfaceAccessorI> spInputProperties=a_spSurfaceAccessible->accessor(
			a_nodeName,e_primitive,e_properties,e_refuseMissing);

	sp<SurfaceAccessorI> spOutputProperties=
			accessor("",e_primitive,e_properties,e_createMissing,e_readWrite);

	const I32 classCount(6);
	I32 offset[classCount];

	Array<Spec> elementSpecs[classCount];
	for(U32 elementPass=0;elementPass<classCount;elementPass++)
	{
		SurfaceAccessibleI::Element element;

		switch(elementPass)
		{
			case 0:
				element=e_point;
				break;
			case 1:
				element=e_vertex;
				break;
			case 2:
				element=e_primitive;
				break;
			case 3:
				element=e_detail;
				break;
			case 4:
				element=e_pointGroup;
				break;
			case 5:
				element=e_primitiveGroup;
				break;
			default:
				;
		}

#if FALSE
		sp<SurfaceAccessorI> spOutputGeneric=accessor(element,e_generic);
		offset[elementPass]=
				spOutputGeneric.isValid()? spOutputGeneric->count(): 0;
#else
		offset[elementPass]=count(element);
#endif
	}

	const I32 transformCount=a_rTransformArray.size();
	const I32 instanceCount=transformCount? transformCount: 1;
	I32 pointCount(0);

	const U32 filterCount=a_spFilter.isValid()?
			a_spFilter->filterCount():
			(spInputVertices.isValid()? spInputVertices->count(): 0);

	//* point indices for each vertex per primitive
	Array< Array<I32> > primVerts;
	primVerts.resize(filterCount);

	if(spInputVertices.isValid())
	{
		const I32 offsetIndex=offset[0];

		for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const U32 primitiveIndex=a_spFilter.isValid()?
					a_spFilter->filter(filterIndex): filterIndex;

			Array<I32>& rVertList=primVerts[primitiveIndex];

			const U32 subCount=spInputVertices->subCount(primitiveIndex);

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);

				rVertList.push_back(offsetIndex+pointIndex);

				if(pointCount<pointIndex+1)
				{
					pointCount=pointIndex+1;
				}
			}
		}
	}

	if(!pointCount)
	{
		//* presume point cloud without primitives

		pointCount=
				spInputPosition.isValid()? spInputPosition->count(): 0;
	}

#if FE_SAB_COPY_DEBUG
	feLog("SurfaceAccessibleBase::append input pointCount %d\n",
			pointCount);
#endif

	if(spOutputPosition.isValid())
	{
		spOutputPosition->append(pointCount*instanceCount);
	}

	if(spOutputVertices.isValid() && filterCount)
	{
		if(!pointCount)
		{
			const I32 addCount=filterCount*instanceCount;

			//* primitives without points
			for(I32 addIndex=0;addIndex<addCount;addIndex++)
			{
				spOutputVertices->append();
			}
		}
		else
		{
			//* WARNING Maya ignores any point append and adds them here
			//* this is different than the Houdini version

			for(I32 instanceIndex=0;instanceIndex<instanceCount;instanceIndex++)
			{
				spOutputVertices->append(primVerts);

				if(instanceIndex!=instanceCount-1)
				{
					const I32 primitiveCount=primVerts.size();
					for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
							primitiveIndex++)
					{
						Array<I32>& rVertList=primVerts[primitiveIndex];
						const I32 subCount=rVertList.size();
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							rVertList[subIndex]+=pointCount;
						}
					}
				}
			}
		}
	}

	for(U32 elementPass=0;elementPass<classCount;elementPass++)
	{
		const I32 offsetIndex=offset[elementPass];

		SurfaceAccessibleI::Element element;

		switch(elementPass)
		{
			case 0:
				element=e_point;
				break;
			case 1:
				element=e_vertex;
				break;
			case 2:
				element=e_primitive;
				break;
			case 3:
				element=e_detail;
				break;
			case 4:
				element=e_pointGroup;
				break;
			case 5:
				element=e_primitiveGroup;
				break;
			default:
				;
		}

		const U32 elementCount=
				a_spSurfaceAccessible->count(a_nodeName,element);

		Array<Spec> existingSpecs;
		attributeSpecs(existingSpecs,"",element);

		const U32 existingSpecCount=existingSpecs.size();

		Array<Spec> incomingSpecs;
		a_spSurfaceAccessible->attributeSpecs(incomingSpecs,a_nodeName,element);

		const U32 incomingSpecCount=incomingSpecs.size();

#if FE_SAB_COPY_DEBUG
		feLog("SurfaceAccessibleBase::append"
				" '%s' elementCount %d offsetIndex %d incomingSpecCount %d\n",
				elementLayout(element).c_str(),elementCount,
				offsetIndex,incomingSpecCount);
#endif

		for(U32 specIndex=0;specIndex<incomingSpecCount;specIndex++)
		{
			const Spec& spec=incomingSpecs[specIndex];
			const String& specName=spec.name();
			const String& specType=spec.typeName();

			if(specType.empty() || specName.empty())
			{
				feLog("SurfaceAccessibleBase::append"
						" illegal spec \"%s\" \"%s\"\n",
						specType.c_str(),specName.c_str());
				continue;
			}

			sp<SurfaceAccessorI> spInputAccessor=
					a_spSurfaceAccessible->accessor(
					a_nodeName,element,specName,e_refuseMissing);

			sp<SurfaceAccessorI> spOutputAccessor=
					accessor("",element,specName,e_createMissing,e_readWrite);

//			feLog("SurfaceAccessibleBase::append"
//					" \"%s\" spOutputAccessor %p\n",
//					name().c_str(),spOutputAccessor.raw());

			if(spInputAccessor.isNull())
			{
				feLog("SurfaceAccessibleBase::append"
						" \"%s\" \"%s\" input accessor failure\n",
						specType.c_str(),specName.c_str());
				continue;
			}

			if(spOutputAccessor.isNull())
			{
				feLog("SurfaceAccessibleBase::append"
						"   \"%s\" \"%s\" output accessor failure\n",
						specType.c_str(),specName.c_str());
				continue;
			}

#if FE_SAB_COPY_DEBUG
			feLog("SurfaceAccessibleBase::append"
					"   \"%s\" \"%s\" count %d -> %d\n",
					specType.c_str(),specName.c_str(),elementCount,
					count(element));
#endif

			if(!specsContain(existingSpecs,spec))
			{
				setAttributeToDefault(element,0,offsetIndex,spec);
			}

			for(I32 instanceIndex=0;instanceIndex<instanceCount;instanceIndex++)
			{
				const SpatialTransform* pTransform=transformCount?
						&a_rTransformArray[instanceIndex]: NULL;

				const I32 instanceOffset=(element==e_detail)? 0:
						offsetIndex+elementCount*instanceIndex;

				//* NOTE for element e_vertex, need to loop through subCount()

				if(element==e_pointGroup || element==e_primitiveGroup)
				{
					SurfaceAccessibleI::Element domain=
							element==e_pointGroup? e_point: e_primitive;
					const U32 domainCount=
							a_spSurfaceAccessible->count(a_nodeName,domain);
					const I32 instanceOffsetDomain=
							offsetIndex+domainCount*instanceIndex;

					const U32 entryCount=spInputAccessor->count();
					for(U32 entryIndex=0;entryIndex<entryCount;entryIndex++)
					{
						spOutputAccessor->append(instanceOffsetDomain+
								spInputAccessor->integer(entryIndex));
					}
				}
				else if(specType=="vector3" || specType=="color")
				{
					for(U32 elementIndex=0;elementIndex<elementCount;
							elementIndex++)
					{
						const I32 subCount=(element==e_vertex)?
								spInputAccessor->subCount(elementIndex): 1;

						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							SpatialVector vector3=
									spInputAccessor->spatialVector(
									elementIndex,subIndex);

							if(pTransform)
							{
								if(specName=="P")
								{
									transformVector(*pTransform,
											vector3,vector3);
								}
								else if(specName=="N")
								{
									rotateVector(*pTransform,vector3,vector3);
								}
								//* more?
							}

							spOutputAccessor->set(instanceOffset+elementIndex,
									subIndex,vector3);
						}
					}
				}
				else if(specType=="real")
				{
					for(U32 elementIndex=0;elementIndex<elementCount;
							elementIndex++)
					{
						const I32 subCount=(element==e_vertex)?
								spInputAccessor->subCount(elementIndex): 1;

						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							spOutputAccessor->set(
									instanceOffset+elementIndex,subIndex,
									spInputAccessor->real(
									elementIndex,subIndex));
						}
					}
				}
				else if(specType=="integer")
				{
					for(U32 elementIndex=0;elementIndex<elementCount;
							elementIndex++)
					{
						const I32 subCount=(element==e_vertex)?
								spInputAccessor->subCount(elementIndex): 1;

						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							spOutputAccessor->set(
									instanceOffset+elementIndex,subIndex,
									spInputAccessor->integer(
									elementIndex,subIndex));
						}
					}
				}
				else if(specType=="string")
				{
					for(U32 elementIndex=0;elementIndex<elementCount;
							elementIndex++)
					{
						const I32 subCount=(element==e_vertex)?
								spInputAccessor->subCount(elementIndex): 1;

						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							spOutputAccessor->set(
									instanceOffset+elementIndex,subIndex,
									spInputAccessor->string(
									elementIndex,subIndex));
						}
					}
				}
			}
		}

		for(U32 specIndex=0;specIndex<existingSpecCount;specIndex++)
		{
			const Spec& spec=existingSpecs[specIndex];
			if(specsContain(incomingSpecs,spec))
			{
				continue;
			}

#if FE_SAB_COPY_DEBUG
			feLog("SurfaceAccessibleBase::append"
					" setting to default because source"
					" doesn't contain \"%s\" \"%s\" \"%s\"\n",
					elementLayout(element).c_str(),
					spec.typeName().c_str(),
					spec.name().c_str());

			const U32 specCount=incomingSpecs.size();
			for(U32 specIndex=0;specIndex<specCount;specIndex++)
			{
				feLog("  \"%s\" \"%s\"\n",
						incomingSpecs[specIndex].typeName().c_str(),
						incomingSpecs[specIndex].name().c_str());
			}
#endif

			setAttributeToDefault(element,offsetIndex,
					elementCount*instanceCount,spec);
		}

#if FE_SAB_COPY_DEBUG
		feLog("SurfaceAccessibleBase::append"
				" output element '%s' elementCount %d\n",
				elementLayout(element).c_str(),count(element));
#endif
	}

	if(spInputVertices.isValid() && spOutputVertices.isValid() &&
			spInputProperties.isValid() && spOutputProperties.isValid())
	{
		const I32 offsetIndex=offset[2];

		const U32 filterCount=a_spFilter.isValid()?
				a_spFilter->filterCount(): spInputVertices->count();

		for(I32 instanceIndex=0;instanceIndex<instanceCount;instanceIndex++)
		{
			const I32 instanceOffset=
					offsetIndex+filterCount*instanceIndex;

			for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const U32 primitiveIndex=a_spFilter.isValid()?
						a_spFilter->filter(filterIndex): filterIndex;

				const I32 openCurve=
						spInputProperties->integer(primitiveIndex,
						SurfaceAccessibleI::e_openCurve);
				spOutputProperties->set(instanceOffset+filterIndex,
						SurfaceAccessibleI::e_openCurve,openCurve);
			}
		}
	}

#if FE_SAB_COPY_TICKER
	const U32 tickEnd=systemTick();
	const U32 tickDiff=SystemTicker::tickDifference(tickStart,tickEnd);
	const Real ms=1e-3*tickDiff*SystemTicker::microsecondsPerTick();
	feLog("SurfaceAccessibleBase::append took %.6G ms\n",ms);
#endif

#if FE_SAB_COPY_DEBUG
	feLog("SurfaceAccessibleBase::append"
			" new pointCount %d primitiveCount %d\n",
			count(e_point),count(e_primitive));
#endif
}

void SurfaceAccessibleBase::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
//~	if(a_node.empty())
//~	{
//~		attributeSpecs(a_rSpecs,a_element);
//~		return;
//~	}
	feLog("SurfaceAccessibleBase::attributeSpecs"
			" no elements in base class\n");

	a_rSpecs.clear();
}

I32 SurfaceAccessibleBase::count(String a_nodeName,
	SurfaceAccessibleI::Element a_element) const
{
//	feLog("SurfaceAccessibleBase::count element %d\n",a_element);

	switch(a_element)
	{
		case e_point:
		{
			sp<SurfaceAccessorI> spAccessor=
					const_cast<SurfaceAccessibleBase*>(this)->accessor(
					a_nodeName,e_point,e_position,e_refuseMissing);

			if(spAccessor.isNull())
			{
//				feLog("SurfaceAccessibleBase::count point accessor null\n");
				return 0;
			}

			return spAccessor->count();
		}
		case e_vertex:
		case e_primitive:
		{
			sp<SurfaceAccessorI> spAccessor=
					const_cast<SurfaceAccessibleBase*>(this)->accessor(
					a_nodeName,e_primitive,e_vertices,e_refuseMissing);

			if(spAccessor.isNull())
			{
//				feLog("SurfaceAccessibleBase::count primitive accessor null\n");
				return 0;
			}

			return spAccessor->count();
		}
		case e_detail:
			return 1;
		default:
			;
	}

	return 0;
}

I32 SurfaceAccessibleBase::discardPattern(
	SurfaceAccessibleI::Element a_element,String a_pattern)
{
//	feLog("SurfaceAccessibleBase::discardPattern '%s' \"%s\"\n",
//			elementLayout(a_element).c_str(),a_pattern.c_str());

	I32 discardCount(0);

	Array<String> patternArray;
	String buffer=a_pattern;
	while(!buffer.empty())
	{
		patternArray.push_back(buffer.parse());
	}

	const U32 patternCount=patternArray.size();

	Array<String> nameArray;
	if(a_element==e_pointGroup || a_element==e_primitiveGroup)
	{
		groupNames(nameArray,a_element);
	}
	else
	{
		Array<SurfaceAccessibleI::Spec> specs;
		attributeSpecs(specs,a_element);

		const I32 specCount=specs.size();
		for(I32 specIndex=0;specIndex<specCount;specIndex++)
		{
			const SurfaceAccessibleI::Spec& rSpec=specs[specIndex];

			const String attrName=rSpec.name();

			nameArray.push_back(attrName);
		}
	}

	const U32 nameCount=nameArray.size();
	for(U32 nameIndex=0;nameIndex<nameCount;nameIndex++)
	{
		const String existingName=nameArray[nameIndex];

//		feLog("  existingName %d/%d \"%s\"\n",
//				nameIndex,nameCount,existingName.c_str());

		for(U32 patternIndex=0;patternIndex<patternCount;patternIndex++)
		{
			const String pattern=patternArray[patternIndex];

//			feLog("  pattern %d/%d \"%s\"\n",
//					patternIndex,patternCount,pattern.c_str());

			if(!existingName.match(pattern))
			{
				continue;
			}

//			feLog("  MATCH\n");

			discardCount+=discard(a_element,existingName);
		}
	}

	return discardCount;
}

BWORD SurfaceAccessibleBase::discard(
	SurfaceAccessibleI::Element a_element,String a_name)
{
	feLog("SurfaceAccessibleBase::discard not implemented in derived class\n");
	return FALSE;
}

BWORD SurfaceAccessibleBase::discard(SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute)
{
	return discard(a_element,attributeString(a_attribute));
}

I32 SurfaceAccessibleBase::deleteElements(
	SurfaceAccessibleI::Element a_element,String a_groupString,
	BWORD a_retainGroups)
{
#if FE_SAB_DELETE_DEBUG
	feLog("SurfaceAccessibleBase::deleteElements '%s' \"%s\" retain %d\n",
			elementLayout(a_element).c_str(),a_groupString.c_str(),
			a_retainGroups);
#endif

	if(a_groupString.empty())
	{
		return 0;
	}

	sp<MultiGroup> spMultiGroup=generateGroup(a_element,a_groupString);

	return deleteElements(a_element,spMultiGroup,a_retainGroups);
}

I32 SurfaceAccessibleBase::deleteElements(
	SurfaceAccessibleI::Element a_element,sp<MultiGroup> a_spMultiGroup,
	BWORD a_retainGroups)
{
	if(a_spMultiGroup.isNull())
	{
		feLog("SurfaceAccessibleBase::deleteElements MultiGroup NULL\n");
		return 0;
	}

	sp<SurfaceAccessorI> spSurfacePoints=
			accessor("",e_point,e_position,e_refuseMissing,e_readWrite);
	const I32 pointCount=spSurfacePoints.isValid()?
			spSurfacePoints->count(): 0;

	sp<SurfaceAccessorI> spSurfaceVertices=
			accessor("",e_primitive,e_vertices,e_refuseMissing,e_readWrite);
	const I32 primitiveCount=spSurfaceVertices.isValid()?
			spSurfaceVertices->count(): 0;

	I32 deleteCount(0);

	if(a_element==e_pointGroup)
	{
		Array<I32> remap(pointCount);
		I32 keepPointCount(0);
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			remap[pointIndex]=
					(a_spMultiGroup->contains(pointIndex)==a_retainGroups)?
					keepPointCount++: -1;
		}

		Array< Array<I32> > primVerts(primitiveCount);

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
				primitiveIndex++)
		{
			const I32 subCount=spSurfaceVertices->subCount(primitiveIndex);

			Array<I32>& rVertArray=primVerts[primitiveIndex];
			rVertArray.resize(subCount);

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spSurfaceVertices->integer(primitiveIndex,subIndex);
				const I32 newIndex=remap[pointIndex];

				if(newIndex<0)
				{
					rVertArray.resize(0);
					break;
				}

				rVertArray[subIndex]=remap[pointIndex];
			}
		}

		for(I32 pointIndex=pointCount-1;pointIndex>=0;pointIndex--)
		{
			if(remap[pointIndex]<0)
			{
#if FE_SAB_DELETE_DEBUG
				feLog("SurfaceAccessibleBase::deleteElements"
						" remove point %d\n",pointIndex);
#endif

				spSurfacePoints->remove(pointIndex);
				deleteCount++;
			}
		}

#if FE_SAB_DELETE_DEBUG
		feLog("SurfaceAccessibleBase::deleteElements"
				" points kept %d have %d\n",
				keepPointCount,spSurfacePoints->count());
#endif

		for(I32 primitiveIndex=primitiveCount-1;primitiveIndex>=0;
				primitiveIndex--)
		{
			Array<I32>& rVertArray=primVerts[primitiveIndex];

			const I32 subCount=rVertArray.size();
			if(!subCount)
			{
#if FE_SAB_DELETE_DEBUG
				feLog("SurfaceAccessibleBase::deleteElements"
						" remove affected primitive %d\n",
						primitiveIndex);
#endif

				spSurfaceVertices->remove(primitiveIndex);
				deleteCount++;
				continue;
			}

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 newIndex=rVertArray[subIndex];

#if FALSE
				const I32 pointIndex=
						spSurfaceVertices->integer(primitiveIndex,subIndex);
				feLog("%d/%d %d/%d %d -> %d\n",primitiveIndex,primitiveCount,
						subIndex,subCount,pointIndex,newIndex);
#endif

				spSurfaceVertices->set(primitiveIndex,subIndex,newIndex);
			}
		}
	}

	if(a_element==e_primitiveGroup)
	{
		Array<I32> valence(pointCount,0);

		for(I32 primitiveIndex=primitiveCount-1;primitiveIndex>=0;
				primitiveIndex--)
		{
			if(a_spMultiGroup->contains(primitiveIndex)!=a_retainGroups)
			{
#if FE_SAB_DELETE_DEBUG
				feLog("SurfaceAccessibleBase::deleteElements"
						" remove primitive %d\n",primitiveIndex);
#endif

				spSurfaceVertices->remove(primitiveIndex);
				deleteCount++;
			}
			else
			{
				const I32 subCount=spSurfaceVertices->subCount(primitiveIndex);
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=spSurfaceVertices->integer(
							primitiveIndex,subIndex);
					valence[pointIndex]++;
				}
			}
		}

		//* TODO remove orphaned points
		for(I32 pointIndex=pointCount-1;pointIndex>=0;pointIndex--)
		{
			if(valence[pointIndex]<1)
			{
#if FE_SAB_DELETE_DEBUG
				feLog("SurfaceAccessibleBase::deleteElements"
						" remove affected point %d\n",pointIndex);
#endif

				spSurfacePoints->remove(pointIndex);
				deleteCount++;
			}
		}

	}

	return deleteCount;
}

sp<SurfaceI> SurfaceAccessibleBase::surface(String a_group,
		SurfaceI::Restrictions a_restrictions)
{
//	feLog("SurfaceAccessibleBase::surface(\"%s\")\n",a_group.c_str());

	//* TODO select form
	//* TODO group
	sp<SurfaceI> spSurface;

	FEASSERT(registry().isValid());

	if(registry().isValid())
	{
		sp<SurfaceAccessorI> spSurfacePoints=
				accessor(e_point,e_position,e_refuseMissing);
		const I32 pointCount=spSurfacePoints.isValid()?
				spSurfacePoints->count(): 0;

		sp<SurfaceAccessorI> spSurfaceVertices=
				accessor(e_primitive,e_vertices,e_refuseMissing);
		const I32 primitiveCount=spSurfaceVertices.isValid()?
				spSurfaceVertices->count(): 0;

		const BWORD pointCloud=(pointCount && !primitiveCount);

		sp<SurfaceAccessorI> spSurfaceProperties=
				accessor(e_primitive,e_properties,e_refuseMissing);

		const BWORD openCurve=(spSurfaceProperties.isValid() &&
				spSurfaceProperties->count() &&
				spSurfaceProperties->integer(0,
				SurfaceAccessibleI::e_openCurve));

		//* TODO be better about being points, not curves,
		//* if both e_excludeCurves and e_excludePolygons are set

		if(pointCloud ||
				(openCurve && !(a_restrictions&SurfaceI::e_excludeCurves)) ||
				(a_restrictions&SurfaceI::e_excludePolygons))
		{
			spSurface=registry()->create(
					"SurfaceI.*.*.SurfaceCurvesAccessible");

			sp<SurfaceCurvesAccessible> spSurfaceCurvesAccessible=
					spSurface;
			if(spSurfaceCurvesAccessible.isValid())
			{
				spSurfaceCurvesAccessible->bind(
						sp<SurfaceAccessibleI>(this));
			}
		}
		else
		{
			spSurface=registry()->create(
					"SurfaceI.*.*.SurfaceTrianglesAccessible");

			sp<SurfaceTrianglesAccessible> spSurfaceTrianglesAccessible=
					spSurface;
			if(spSurfaceTrianglesAccessible.isValid())
			{
				spSurfaceTrianglesAccessible->bind(
						sp<SurfaceAccessibleI>(this));
			}
		}
	}
	else
	{
		feLog("SurfaceAccessibleBase::surface registry is NULL\n");
	}

	if(spSurface.isValid())
	{
		spSurface->setRestrictions(a_restrictions);
	}
	else
	{
		feLog("SurfaceAccessibleBase::surface failed to access surface\n");
	}

	return spSurface;
}

sp<SurfaceI> SurfaceAccessibleBase::subSurface(U32 a_subIndex,
	String a_group,SurfaceI::Restrictions a_restrictions)
{
	//* TODO

	return sp<SurfaceI>(NULL);
}

sp<SpannedRange> SurfaceAccessibleBase::atomize(
	AtomicChange a_atomicChange,String a_group,U32 a_desiredCount)
{
#if FE_SAB_DEBUG
	feLog("SurfaceAccessibleBase::atomize\n");
#endif

	if(a_atomicChange==e_pointsOfPrimitives)
	{
		const BWORD checkPages=TRUE;
		return atomizeConnectivity(a_atomicChange,a_group,
				a_desiredCount,checkPages);
	}

	//* fallback to non-atomic

	const BWORD asPoints=(a_atomicChange==e_pointsOnly);

#if FALSE
	const Element element=asPoints? e_point: e_primitive;
	sp<SurfaceAccessorI> spAccessor=accessor(element,e_generic);
	const I32 elementCount=spAccessor.isValid()? spAccessor->count(): 0;
#else
	const I32 elementCount=count(asPoints? e_point: e_primitive);
#endif

	sp<SpannedRange> spSpannedRange(new SpannedRange());

	//* use everything

	if(a_group.empty())
	{
		if(elementCount>0)
		{
			spSpannedRange->nonAtomic().add(0,elementCount-1);
		}
		return spSpannedRange;
	}

	//* use group

//	feLog("SurfaceAccessibleBase::atomize group \"%s\"\n",a_group.c_str());

	SpannedRange::MultiSpan& rMultiSpan=spSpannedRange->nonAtomic();

#if FALSE
	I32 spanStart= -1;
	for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
	{
		const I32 elementPage=
				asPoints? pointPage(elementIndex): primitivePage(elementIndex);
		if(spanStart<0 && elementPage>=0)
		{
			//* begin span
			spanStart=elementIndex;
		}
		else if(spanStart>=0 && elementPage<0)
		{
			//* complete span
			rMultiSpan.add(spanStart,elementIndex-1);
			spanStart=-1;
		}
	}

	//* complete last span
	if(spanStart>=0)
	{
		rMultiSpan.add(spanStart,elementCount-1);

	}
#else
	const Element groupElement=asPoints? e_pointGroup: e_primitiveGroup;

	sp<SurfaceAccessorI> spElementGroup=accessor(groupElement,a_group);

	spElementGroup->appendGroupSpans(rMultiSpan);
#endif

	return spSpannedRange;
}

sp<SpannedRange> SurfaceAccessibleBase::atomizeConnectivity(
	AtomicChange a_atomicChange,String a_group,U32 a_desiredCount,
	BWORD a_checkPages)
{
	if(a_checkPages)
	{
		preparePaging(a_atomicChange,a_group);
	}

	sp<SpannedRange> spSpannedRange(new SpannedRange());

	sp<SurfaceAccessorI> spPoints=accessor(e_point,e_position);
	sp<SurfaceAccessorI> spVertices=accessor(e_primitive,e_vertices);

	const U32 pointCount=spPoints->count();
	const U32 primitiveCount=spVertices->count();

	Array<U32> pointPatch(pointCount,0);
	Array<I32> primitivePatch(primitiveCount,0);

	Array<U32> pagePatch(primitiveCount+1,0);	//* pages <= prims+1
	Array<U32> histogram(primitiveCount+1,0);	//* patches <= prims+1

	U32 maxPatch=0;
	U32 maxPage=0;

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(a_checkPages && primitivePage(primitiveIndex)<0)
		{
			FEASSERT(primitiveIndex<primitiveCount);
			primitivePatch[primitiveIndex]= -1;
			continue;
		}

		U32 lowPatch=0;
		U32 lowPage=0;
		BWORD multiPatch=FALSE;
		BWORD zeroPatches=FALSE;
		BWORD multiPage=FALSE;

		const U32 subCount=spVertices->subCount(primitiveIndex);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const U32 pointIndex=spVertices->integer(primitiveIndex,subIndex);
			FEASSERT(pointIndex<pointCount);
			U32& rPatch=pointPatch[pointIndex];
			FEASSERT(rPatch<=maxPatch);

			const I32 page=a_checkPages? pointPage(pointIndex): 0;
			if(page>0)
			{
				FEASSERT(page<=I32(primitiveCount));
				const U32 existingPatch=pagePatch[page];
				FEASSERT(existingPatch<=maxPatch);

				if(existingPatch)
				{
					rPatch=existingPatch;
				}
				if(!lowPage)
				{
					lowPage=page;
				}
				else if(I32(lowPage)!=page)
				{
					multiPage=TRUE;
					if(I32(lowPage)>page)
					{
						lowPage=page;
					}
				}
			}
			if(rPatch)
			{
				if(!lowPatch)
				{
					lowPatch=rPatch;
				}
				else if(lowPatch!=rPatch)
				{
					multiPatch=TRUE;
					if(lowPatch>rPatch)
					{
						lowPatch=rPatch;
					}
				}
			}
			else
			{
				zeroPatches=TRUE;
			}
		}

		if(multiPage)
		{
			//* leave for postAtomic
			lowPatch=0;
		}
		else if(multiPatch)
		{
//			feLog("multiPatch\n");

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 pointIndex=
						spVertices->integer(primitiveIndex,subIndex);
				FEASSERT(pointIndex<pointCount);
				const U32 patch=pointPatch[pointIndex];

//				feLog("prim %d sub %d/%d patch %d\n",
//						primitiveIndex,subIndex,subCount,patch);

				if(patch<1)
				{
//					feLog("assign patch %d to lowPatch %d\n",patch,lowPatch);

					pointPatch[pointIndex]=lowPatch;
				}
				else if(patch!=lowPatch)
				{
//					feLog("connected patch %d != lowPatch %d\n",patch,lowPatch);

					for(U32 pointOther=0;pointOther<pointCount;pointOther++)
					{
						FEASSERT(pointOther<pointCount);
						U32& rOtherPatch=pointPatch[pointOther];

						if(rOtherPatch==patch)
						{
//							feLog("%d merge patch %d into %d\n",
//									pointIndex,rOtherPatch,lowPatch);

							//* merge all 'patch' into 'lowPatch'
							rOtherPatch=lowPatch;
						}
						else if(rOtherPatch==maxPatch)
						{
//							feLog("%d shift patch %d into %d\n",
//									pointIndex,rOtherPatch,patch);

							//* shift all 'maxPatch' into emptied 'patch'
							rOtherPatch=patch;
						}
					}

					for(U32 primitiveOther=0;primitiveOther<primitiveIndex;
							primitiveOther++)
					{
						I32& rOtherPrim=primitivePatch[primitiveOther];
						primitivePatch[primitiveIndex]=lowPatch;
						if(rOtherPrim==I32(patch))
						{
//							feLog("%d merge patch %d into %d\n",
//									primitiveOther,rOtherPrim,lowPatch);

							//* merge all 'patch' into 'lowPatch'
							rOtherPrim=lowPatch;
						}
						else if(rOtherPrim==I32(maxPatch))
						{
//							feLog("%d shift prim %d into %d\n",
//									primitiveOther,rOtherPrim,patch);

							//* shift all 'maxPatch' into emptied 'patch'
							rOtherPrim=patch;
						}
					}

					histogram[lowPatch]+=histogram[patch];
					histogram[patch]=histogram[maxPatch];

					FEASSERT(maxPatch);
					maxPatch--;

//					feLog("maxPatch -> %d\n",maxPatch);
				}
			}
		}
		else if(!lowPatch)
		{
			maxPatch++;
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 pointIndex=
						spVertices->integer(primitiveIndex,subIndex);
				FEASSERT(pointIndex<pointCount);
				pointPatch[pointIndex]=maxPatch;
			}
			lowPatch=maxPatch;
		}
		else if(zeroPatches)
		{
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 pointIndex=
						spVertices->integer(primitiveIndex,subIndex);
				FEASSERT(pointIndex<pointCount);
				pointPatch[pointIndex]=lowPatch;
			}
		}

//		feLog("primitive %d/%d lowPatch %d/%d\n",
//				primitiveIndex,primitiveCount,lowPatch,maxPatch);

		FEASSERT(lowPatch<=maxPatch);

		FEASSERT(primitiveIndex<primitiveCount);
		primitivePatch[primitiveIndex]=lowPatch;

		FEASSERT(lowPatch<=primitiveCount);
		histogram[lowPatch]+=subCount;

		if(lowPatch)
		{
			FEASSERT(lowPage<=primitiveCount);
			pagePatch[lowPage]=lowPatch;
		}
		if(maxPage<lowPage)
		{
			maxPage=lowPage;
		}
	}

#if FE_SAB_DEBUG
	feLog("SurfaceAccessibleBase::atomizeConnectivity maxPage %d\n",maxPage);
	for(U32 pageIndex=0;pageIndex<=maxPage;pageIndex++)
	{
		FEASSERT(pageIndex<=primitiveCount);
		feLog("page %d uses patch %d\n",pageIndex,pagePatch[pageIndex]);
	}
	feLog("SurfaceAccessibleBase::atomizeConnectivity maxPatch %d\n",maxPatch);
	for(U32 patchIndex=0;patchIndex<=maxPatch;patchIndex++)
	{
		FEASSERT(patchIndex<=primitiveCount);
		feLog("patch %d has %d points\n",patchIndex,histogram[patchIndex]);
	}
#endif

	const U32 desiredSize=a_desiredCount? primitiveCount/a_desiredCount: 1;

	Array<U32> patchAtom(maxPatch+1,0);

	I32 maxAtom=1;
	U32 atomSize=0;
	for(U32 patchIndex=1;patchIndex<=maxPatch;patchIndex++)
	{
		if(atomSize>=desiredSize)
		{
			maxAtom++;
			atomSize=0;
		}
		FEASSERT(patchIndex<=maxPatch);
		patchAtom[patchIndex]=maxAtom;

		FEASSERT(patchIndex<=primitiveCount);
		atomSize+=histogram[patchIndex];
#if FE_SAB_DEBUG
		feLog("patch %d atom %d\n",patchIndex,maxAtom);
#endif
	}

	Array<SpannedRange::MultiSpan*> multiSpans(maxAtom+1,NULL);
	I32 spanAtom= -1;
	I32 spanStart= -1;
#if FE_SAB_DEBUG
	feLog("first %d from %d\n",spanAtom,spanStart);
#endif
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		FEASSERT(primitiveIndex<primitiveCount);
		const I32 patchIndex=primitivePatch[primitiveIndex];

		FEASSERT(patchIndex<0 || patchIndex<=I32(maxPatch));
		const I32 atomIndex=patchIndex>=0? patchAtom[patchIndex]: -1;

		FEASSERT(atomIndex<=maxAtom);
		FEASSERT(spanAtom<=maxAtom);
		if(atomIndex!=spanAtom)
		{
			//* complete existing span
			SpannedRange::MultiSpan* pMultiSpan=NULL;
			if(spanAtom>=0)
			{
				FEASSERT(spanAtom<=maxAtom);
				pMultiSpan=multiSpans[spanAtom];
				if(!pMultiSpan)
				{
					pMultiSpan=spanAtom? &spSpannedRange->addAtomic():
							&spSpannedRange->postAtomic();
					multiSpans[spanAtom]=pMultiSpan;
				}
			}
			if(spanStart>=0)
			{
#if FE_SAB_DEBUG
				feLog("complete %d span %d %d\n",
						spanAtom,spanStart,primitiveIndex-1);
#endif
				pMultiSpan->add(spanStart,primitiveIndex-1);
				spanStart= -1;
			}

			//* start another span
			spanAtom=atomIndex;
			if(atomIndex>=0)
			{
				spanStart=primitiveIndex;

#if FE_SAB_DEBUG
				feLog("start %d from %d\n",spanAtom,spanStart);
#endif
			}
		}
	}

	//* complete last span
	SpannedRange::MultiSpan* pMultiSpan=NULL;
	if(spanStart>=0)
	{
		FEASSERT(spanAtom<=maxAtom);
		pMultiSpan=multiSpans[spanAtom];
		if(!pMultiSpan)
		{
			pMultiSpan=spanAtom? &spSpannedRange->addAtomic():
					&spSpannedRange->postAtomic();
			multiSpans[spanAtom]=pMultiSpan;
		}
		pMultiSpan->add(spanStart,primitiveCount-1);
	}

#if FE_SAB_DEBUG
	feLog("brief: %s\n",spSpannedRange->brief().c_str());
	feLog("dump:\n%s\n",spSpannedRange->dump().c_str());
#endif

	return spSpannedRange;
}

//* static
String SurfaceAccessibleBase::elementLayout(
	SurfaceAccessibleI::Element a_element)
{
	switch(a_element)
	{
		case e_point:
			return "pt";
		case e_pointGroup:
			return "ptgrp";
		case e_vertex:
			return "vtx";
		case e_primitive:
			return "prim";
		case e_primitiveGroup:
			return "primgrp";
		case e_detail:
			return "detail";
		default:
			;
	}

	return "";
}

//* static
String SurfaceAccessibleBase::attributeString(
	SurfaceAccessibleI::Attribute a_attribute)
{
	String name;
	switch(a_attribute)
	{
		case SurfaceAccessibleI::e_generic:
			return "generic";
		case SurfaceAccessibleI::e_position:
			return "P";
		case SurfaceAccessibleI::e_normal:
			return "N";
		case SurfaceAccessibleI::e_uv:
			return "uv";
		case SurfaceAccessibleI::e_color:
			return "Cd";
		case SurfaceAccessibleI::e_vertices:
			return "vertices";
		case SurfaceAccessibleI::e_properties:
			return "properties";
		default:
			;
	}

	return "undefined";
}

//* static
String SurfaceAccessibleBase::commonName(String a_attrName)
{
	if(a_attrName=="spc:at")
	{
		return "P";
	}
	else if(a_attrName=="spc:up")
	{
		return "N";
	}
	else if(a_attrName=="surf:uvw")
	{
		return "uv";
	}
	else if(a_attrName=="surf:color")
	{
		return "Cd";
	}

	return a_attrName;
}

//* static
String SurfaceAccessibleBase::attributeName(String a_specName)
{
	if(a_specName=="P")
	{
		return "spc:at";
	}
	else if(a_specName=="N")
	{
		return "spc:up";
	}
	else if(a_specName=="uv")
	{
		return "surf:uvw";
	}
	else if(a_specName=="Cd")
	{
		return "surf:color";
	}

	return a_specName;
}

void SurfaceAccessibleBase::outlineClear(void)
{
	m_outline.clear();
}

void SurfaceAccessibleBase::outlineAppend(String a_line)
{
	const I32 outlineCount=m_outline.size();
	m_outline.resize(outlineCount+1);
	m_outline[outlineCount]=a_line;
}

BWORD SurfaceAccessibleBase::copyOutline(Array<String>& a_rStringArray) const
{
	const I32 outlineCount=m_outline.size();
	a_rStringArray.resize(outlineCount);

	for(I32 outlineIndex=0;outlineIndex<outlineCount;outlineIndex++)
	{
		a_rStringArray[outlineIndex]=m_outline[outlineIndex];
	}

	return TRUE;
}

void SurfaceAccessibleBase::outlineCreateDefault(void)
{
	const String nameAttr=findNameAttribute();

	sp<SurfaceAccessorI> spPrimitiveName=accessor(e_primitive,nameAttr);
	if(spPrimitiveName.isValid())
	{
		std::set<String> nameSet;

		const I32 primitiveCount=spPrimitiveName->count();
		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
				primitiveIndex++)
		{
			const String name=spPrimitiveName->string(primitiveIndex);
			if(nameSet.find(name)==nameSet.end())
			{
				nameSet.insert(name);
			}
		}

		for(std::set<String>::iterator it=nameSet.begin();
				it!=nameSet.end();it++)
		{
			const String name= *it;
			outlineAppend(name);
		}
	}
}

String SurfaceAccessibleBase::findNameAttribute(void)
{
	String partAttr;

	Array<SurfaceAccessibleI::Spec> specs;
	attributeSpecs(specs,SurfaceAccessibleI::e_primitive);

	const I32 specCount=specs.size();
	for(I32 specIndex=0;specIndex<specCount;specIndex++)
	{
		const SurfaceAccessibleI::Spec& rSpec=specs[specIndex];

		const String attrName=rSpec.name();
		if(attrName!="name" && attrName!="part")
		{
			continue;
		}

		const String attrType=rSpec.typeName();
		if(attrType!="string")
		{
			continue;
		}

		String valueString;

		sp<SurfaceAccessorI> spSurfaceAccessorI=accessor(e_primitive,attrName,
				SurfaceAccessibleI::e_refuseMissing);

		if(spSurfaceAccessorI.isValid())
		{
			partAttr=attrName;

			//* priority
			if(attrName=="name")
			{
				break;
			}
		}
	}

	return partAttr;
}

void SurfaceAccessibleBase::setAttributeToDefault(Element a_element,
	I32 a_start,I32 a_count,const Spec& a_rSpec)
{
	const String& specName=a_rSpec.name();
	const String& specType=a_rSpec.typeName();

	sp<SurfaceAccessorI> spOutputAccessor=accessor("",a_element,specName,
			e_createMissing,e_readWrite);
	const I32 limit=a_start+a_count;

	if(specType=="vector3" || specType=="color")
	{
		for(I32 elementIndex=a_start;elementIndex<limit;elementIndex++)
		{
			const I32 subCount=(a_element==e_vertex)?
					spOutputAccessor->subCount(elementIndex): 1;

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				spOutputAccessor->set(elementIndex,subIndex,
						SpatialVector(0,0,0));
			}
		}
	}
	else if(specType=="real")
	{
		for(I32 elementIndex=a_start;elementIndex<limit;elementIndex++)
		{
			const I32 subCount=(a_element==e_vertex)?
					spOutputAccessor->subCount(elementIndex): 1;

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				spOutputAccessor->set(elementIndex,subIndex,Real(0));
			}
		}
	}
	else if(specType=="integer")
	{
		for(I32 elementIndex=a_start;elementIndex<limit;elementIndex++)
		{
			const I32 subCount=(a_element==e_vertex)?
					spOutputAccessor->subCount(elementIndex): 1;

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				spOutputAccessor->set(elementIndex,subIndex,0);
			}
		}
	}
	else if(specType=="string")
	{
		for(I32 elementIndex=a_start;elementIndex<limit;elementIndex++)
		{
			const I32 subCount=(a_element==e_vertex)?
					spOutputAccessor->subCount(elementIndex): 1;

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				spOutputAccessor->set(elementIndex,subIndex,"");
			}
		}
	}
}

//* static
BWORD SurfaceAccessibleBase::specsContain(
	const Array<Spec>& a_rSpecs,const Spec& a_rSpec)
{
	const U32 specCount=a_rSpecs.size();
	for(U32 specIndex=0;specIndex<specCount;specIndex++)
	{
		if(a_rSpecs[specIndex]==a_rSpec)
		{
			return TRUE;
		}
	}
	return FALSE;
}

I32 SurfaceAccessibleBase::MultiGroup::at(I32 a_position)
{
	const I32 limit=count();

//	feLog("SurfaceAccessibleBase::MultiGroup::at %d -> %d of %d\n",
//			m_currentPosition,a_position,limit);

	if(m_currentPosition>a_position &&
			m_currentPosition-a_position > m_currentPosition/2)
	{
//		feLog("  ->0\n");
		m_iterator=m_indices.begin();
		m_currentPosition=0;
	}
	else if(a_position>m_currentPosition &&
			a_position-m_currentPosition > (limit-m_currentPosition)/2)
	{
//		feLog("  ->%d\n",limit-1);
		m_iterator= --m_indices.end();
		m_currentPosition=limit-1;
	}
	while(a_position<m_currentPosition)
	{
//		feLog("  --\n");
		m_iterator--;
		m_currentPosition--;
	}
	while(a_position>m_currentPosition && m_currentPosition<limit)
	{
//		feLog("  ++\n");
		m_iterator++;
		m_currentPosition++;
	}

//	feLog("  =%d\n",*m_iterator);
	return *m_iterator;
}

sp<SurfaceAccessibleBase::MultiGroup> SurfaceAccessibleBase::generateGroup(
	SurfaceAccessibleI::Element a_element,
	String a_groupString)
{
	feLog("SurfaceAccessibleBase::generateGroup"
			" not implemented in derived class\n");
	return sp<MultiGroup>(NULL);
}

String SurfaceAccessibleBase::addGroupRanges(
	sp<SurfaceAccessibleBase::MultiGroup> a_spMultiGroup,String a_groupString)
{
	String depleted;

	String buffer=a_groupString;
	while(!buffer.empty())
	{
		String pattern=buffer.parse();

//		feLog("  pattern \"%s\"\n",pattern.c_str());

		if(pattern.empty())
		{
			continue;
		}

		const char first=pattern.c_str()[0];
		if(first>='0' && first<='9')
		{
			const I32 first=atoi(pattern.parse("\"","-").c_str());
			const I32 last=fe::maximum(first,
					atoi(pattern.parse("\"","-").c_str()));

//			feLog("  parsed %d to %d\n",first,last);

			a_spMultiGroup->insert(first,last);
		}
		else
		{
			depleted+=(depleted.empty()? "": " ") + pattern;
		}
	}

	return depleted;
}

BWORD SurfaceAccessibleBase::removeGroup(SurfaceAccessibleI::Element a_element,
	String a_groupString)
{
	std::map<String, sp<MultiGroup> >& rGroupMap=groupMap(a_element);

	if(rGroupMap.find(a_groupString)==rGroupMap.end())
	{
		return FALSE;
	}

	rGroupMap.erase(a_groupString);
	{
		return TRUE;
	}
}

BWORD SurfaceAccessibleBase::hasGroup(SurfaceAccessibleI::Element a_element,
	String a_groupString)
{
	std::map<String, sp<MultiGroup> >& rGroupMap=groupMap(a_element);

	return (rGroupMap.find(a_groupString)!=rGroupMap.end());
}

sp<SurfaceAccessibleBase::MultiGroup> SurfaceAccessibleBase::group(
	SurfaceAccessibleI::Element a_element,String a_groupString)
{
	std::map<String, sp<MultiGroup> >& rGroupMap=groupMap(a_element);

	const BWORD exists=(rGroupMap.find(a_groupString)!=rGroupMap.end());
	if(exists)
	{
		return rGroupMap[a_groupString];
	}

	sp<MultiGroup> spMultiGroup=generateGroup(a_element,a_groupString);

	rGroupMap[a_groupString]=spMultiGroup;
	return spMultiGroup;
}

std::map<String, sp<SurfaceAccessibleBase::MultiGroup> >&
	SurfaceAccessibleBase::groupMap(SurfaceAccessibleI::Element a_element)
{
	return (a_element==SurfaceAccessibleI::e_pointGroup)?
			m_pointGroupMap: m_primitiveGroupMap;
}

} /* namespace ext */
} /* namespace fe */
