/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_Geodesic_h__
#define __geometry_Geodesic_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief calculate surface distance between points

	@ingroup geometry

	https://github.com/zishun/geodesic_matlab

*//***************************************************************************/
class FE_DL_EXPORT Geodesic
{
	public:
	class FE_DL_EXPORT Mesh: public Counted
	{
		public:

						Mesh(void);
	virtual				~Mesh(void);

			void		populate(Array<F64>& a_rPoints,Array<U32>& a_rFaces);
			void*		rawMesh(void);

		private:
			void		clear(void);

			void*		m_pMesh;
	};

					Geodesic(void);
					~Geodesic(void);

		sp<Mesh>	createMesh(Array<F64>& a_rPoints,Array<U32>& a_rFaces);

		void		setMesh(sp<Mesh> a_spMesh);
		sp<Mesh>	mesh(void);

		void		setSource(I32 a_sourceFaceIndex,
							SpatialVector a_sourcePosition);
		void		setMaxDistance(Real a_maxDistance);

		Real		distanceTo(I32 a_targetFaceIndex,
						SpatialVector a_targetPosition);

	private:

		void		clear(void);

		sp<Mesh>		m_spMesh;
		void*			m_pAlgorithm;

		I32				m_sourceFaceIndex;
		SpatialVector	m_sourcePosition;
		Real			m_maxDistance;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_Geodesic_h__ */

