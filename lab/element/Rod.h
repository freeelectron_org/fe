/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Rod_h__
#define __element_Rod_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Rod RecordView

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Rod: public RecordView
{
	public:
		Functor<F32>				length;
		Functor<Record>				particle1;
		Functor<Record>				particle2;
		Functor<WeakRecord>			pattern;
		Functor<I32>				serial;

				Rod(void)			{ setName("Rod"); }
virtual	void	addFunctors(void)
				{
					add(length,		FE_SPEC("pair:distance",
							"Separation of two Records"));
					add(particle1,	FE_SPEC("pair:left",
							"First Record of a pair"));
					add(particle2,	FE_SPEC("pair:right",
							"Second Record of a pair"));
					add(pattern,	FE_USE("comp:pattern"));
					add(serial,		FE_USE(":SN"));
				}
virtual	void	initializeRecord(void)
				{
					length()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Rod_h__ */
