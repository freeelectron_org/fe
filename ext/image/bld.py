import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    srcList = [ "image.pmh",
                "ImageCommon",
                "ImageRaw",
                "imageDL" ]

    dll = module.DLL( "fexImageDL", srcList )

    deplibs = forge.basiclibs + [
                "fePluginLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "feMathLib" ]

    forge.deps( ["fexImageDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe", "fexImageDL",                       None,   None) ]

    module.Module('test')
