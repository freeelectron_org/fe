/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

void SoftSystem::compile(const t_note_id &a_note_id)
{
	m_spScope = m_rg_dataset->scope();

	if(!m_spScope.isValid()) { return; }

	m_asSoft.bind(m_rg_dataset->scope());

	std::vector<Record>	softs;
	m_asSoft.filter(softs, m_rg_dataset);

	for(unsigned int i_soft = 0; i_soft < softs.size(); i_soft++)
	{
		Record r_soft = softs[i_soft];
		Soft soft;
		soft.m_spSemiImplicit = new SemiImplicit();;
		soft.m_spSemiImplicit->initialize(m_spScope);
		soft.m_spSemiImplicit->setGravity(t_solve_v3(0.0,0.0,-9.81));

		sp<SemiImplicit::Force> spForce;

		spForce = new LinearSpringForce(soft.m_spSemiImplicit);
		soft.m_spSemiImplicit->addForce(spForce, false);

		spForce = new LinearBendForce(soft.m_spSemiImplicit);
		soft.m_spSemiImplicit->addForce(spForce, false);

		spForce = new LinearStrandForce(soft.m_spSemiImplicit);
		soft.m_spSemiImplicit->addForce(spForce, false);

		soft.m_r_parent = r_soft;

		if(m_asSoft.structure(r_soft).isValid())
		{
			AsSolverParticle asSolverParticle;
			asSolverParticle.bind(m_spScope);

			soft.m_spSemiImplicit->compile(m_asSoft.structure(r_soft));
		}

		AsForcePoint asForcePoint;
		asForcePoint.bind(m_spScope);
		asForcePoint.filter(soft.m_forceParticles, m_asSoft.structure(r_soft));

		std::vector<SemiImplicit::Particle> &ps =
			soft.m_spSemiImplicit->particles();

		soft.m_spExternalForces.resize(soft.m_forceParticles.size());
		for(unsigned int i = 0; i < soft.m_forceParticles.size(); i++)
		{
			AsSolverParticle asSolverParticle;
			asSolverParticle.bind(m_spScope);
			soft.m_spExternalForces[i] =
				new ExternalForce(soft.m_spSemiImplicit);
			spForce = soft.m_spExternalForces[i];
			soft.m_spSemiImplicit->addForce(spForce, false);
			soft.m_spExternalForces[i]->bind(
				&ps[asSolverParticle.index(soft.m_forceParticles[i])]);
			soft.m_spExternalForces[i]->set(t_moa_v3(0.0,0.0,0.0));
		}

		m_softs.push_back(soft);
	}
}

void SoftSystem::step(t_moa_real a_dt)
{
	t_solve_v3 dummy;

	AsForcePoint asForcePoint;
	asForcePoint.bind(m_spScope);

	for(unsigned i_soft = 0; i_soft < m_softs.size(); i_soft++)
	{
		Soft &soft = m_softs[i_soft];

		for(unsigned int i = 0; i < soft.m_forceParticles.size(); i++)
		{
			soft.m_spExternalForces[i]->set(
				asForcePoint.force(soft.m_forceParticles[i]));
		}

		soft.m_spSemiImplicit->step(a_dt, dummy);

		if(m_asSoft.solve_extract(soft.m_r_parent))
		{
			soft.m_spSemiImplicit->extract(m_asSoft.structure(soft.m_r_parent));
		}

		for(unsigned int i = 0; i < soft.m_forceParticles.size(); i++)
		{
			asForcePoint.force(soft.m_forceParticles[i])=t_moa_v3(0.0,0.0,0.0);
		}
	}
}

} /* namespace ext */
} /* namespace fe */


