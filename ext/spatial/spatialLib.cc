/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"
#include "signal/signal.h"
#include "spatial/spatial.h"
#include "Proximity.h"
#include "ProxBrute.h"
#include "ProxMultiGrid.h"
#include "ProxSweep.h"
#include "ProxHash.h"
#include "Flatten.h"

namespace fe
{
namespace ext
{

Library* CreateSpatialLibrary(sp<Master>)
{
	Library *pLibrary = Memory::instantiate<Library>();
	pLibrary->setName("default_spatial");
	pLibrary->add<GridScalarField>("ScalarFieldI.Grid.fe");
	pLibrary->add<GridVectorField>("VectorFieldI.Grid.fe");
	pLibrary->add<Proximity>("HandlerI.Proximity.spatial.fe");
	pLibrary->add<ProxBrute>("ProxI.Brute.spatial.fe");
	pLibrary->add<ProxMultiGrid>("ProxI.MultiGrid.spatial.fe");
	pLibrary->add<ProxSweep>("ProxI.Sweep.spatial.fe");
	pLibrary->add<ProxHash>("ProxI.Hash.spatial.fe");
	pLibrary->add<AffineSpace>("SpaceI.AffineSpace.spatial.fe");
	pLibrary->add<Flatten>("HandlerI.Flatten.spatial.fe");
	return pLibrary;
}

} /* namespace ext */
} /* namespace fe */
