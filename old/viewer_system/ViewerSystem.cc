/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer_system/viewer_system.pmh"

namespace fe
{
namespace ext
{

void ViewerSystem::perform(const t_note_id &a_note_id)
{
	if(a_note_id == m_drawNote)
    {
        #if FE_CPLUSPLUS >= 201703L
			std::shared_lock<std::shared_mutex> lock(m_viewportRecordsMutex, std::try_to_lock); // C++17
            if(lock)
        #else
			if(m_viewportRecordsMutex.tryLockReadOnly())
        #endif
        {
            // Find the viewport for the current thread.
			auto viewportRecord = m_viewportRecords.find(std::this_thread::get_id());

			if(viewportRecord != m_viewportRecords.end())
            {
                // Perform draw calls.
                draw(viewportRecord->second);
            }

            #if FE_CPLUSPLUS < 201703L
				m_viewportRecordsMutex.unlockReadOnly();
            #endif
        }
    }
}

void ViewerSystem::updateViewports(const t_note_id &note_id)
{
	m_viewportAccessorSet.bind(m_dataset->scope());

    // Find all viewport records.
    std::vector<fe::Record> viewportsUnmappedArray;
	m_viewportAccessorSet.filter(viewportsUnmappedArray, m_dataset);

    // Map viewports by their thread id.
    #if FE_CPLUSPLUS >= 201703L
		std::unique_lock<std::shared_mutex> lock(m_viewportRecordsMutex);
    #else
		m_viewportRecordsMutex.lock();
    #endif
    {
		m_viewportRecords.clear();

        for (fe::Record viewportRecord : viewportsUnmappedArray)
        {
			m_viewportRecords[m_viewportAccessorSet.threadId(viewportRecord)] = viewportRecord;
        }
    }
    #if FE_CPLUSPLUS < 201703L
		m_viewportRecordsMutex.unlock();
    #endif
}

} /* namespace ext */
} /* namespace fe */
