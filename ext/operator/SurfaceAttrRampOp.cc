/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define FE_SARO_DEBUG		FALSE

#include "operator/operator.pmh"

using namespace fe;
using namespace fe::ext;

void SurfaceAttrRampOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("name")="attribute";
	catalog<String>("name","label")="Name";

	catalog<I32>("sampleCount")=8;
	catalog<I32>("sampleCount","min")=1;
	catalog<I32>("sampleCount","high")=100;
	catalog<I32>("sampleCount","max")=1e3;
	catalog<String>("sampleCount","label")="Samples";

	catalog<Real>("max")=1.0;
	catalog<Real>("max","low")= -10.0;
	catalog<Real>("max","high")=10.0;
	catalog<Real>("max","min")= -1e9;
	catalog<Real>("max","max")=1e9;
	catalog<String>("max","label")="Scale to Max";

	catalog< sp<Component> >("ramp");
	catalog<String>("ramp","implementation")="RampI";
	catalog<String>("ramp","default")="one";
	catalog<String>("ramp","label")="Ramp";

	catalog< sp<Component> >("Input Surface");
}

void SurfaceAttrRampOp::handle(Record& a_rSignal)
{
#if FE_SARO_DEBUG
	feLog("SurfaceAttrRampOp::handle\n");
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="";

	const String attrName=catalog<String>("name");
	if(attrName.empty())
	{
		feLog("SurfaceAttrCreateOp::handle no name\n");
		return;
	}

	sp<RampI> spRamp=catalog< sp<Component> >("ramp");
	if(spRamp.isNull())
	{
		feLog("SurfaceAttrCreateOp::handle no ramp tool\n");
		return;
	}

	const I32 sampleCount=catalog<I32>("sampleCount");
	const Real max=catalog<Real>("max");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputSamples;
	if(!access(spOutputSamples,spOutputAccessible,
			e_detail,attrName)) return;

	spOutputSamples->append(0,sampleCount);

	for(I32 sampleIndex=0;sampleIndex<sampleCount;sampleIndex++)
	{
		const Real t=sampleIndex/Real(sampleCount-1);

		const Real sample=spRamp->eval(t);

		spOutputSamples->set(0,sampleIndex,max*sample);
	}

	rSummary.sPrintf("%s[%d] to %.6G",attrName.c_str(),sampleCount,max);

#if FE_SARO_DEBUG
	feLog("SurfaceAttrRampOp::handle done\n");
#endif
}
