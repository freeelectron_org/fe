import os
import sys
forge = sys.modules["forge"]

def setup(module):
    srcList = [ "ode.pmh",
                "ContextODE",
                "Body",
                "Joint",
                "World",
                "odeDL" ]

    dll = module.DLL( "fexODEDL", srcList )

    deplibs = forge.corelibs+ [
                "odeLib",
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexMechanicsDLLib",
                "fexSolveDLLib" ]

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    #if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        #dll.linkmap['physics'] = "ode.lib"
    #else:
        #dll.linkmap['physics'] = "-lode"
        #pass

    forge.deps( ["fexODEDLLib"], deplibs )

    module.includemap = { 'ode' : os.path.join(module.modPath,'ode','include') }

    ode = module.Module( 'ode' )
    module.AddDep(ode)

    module.Module('test')

def was_auto(module):
    test_file = """
#include <ode/ode.h>

int main(void)
{
    return(0);
}
    \n"""

    odemap = { "gfxlibs": forge.gfxlibs }
    if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
        odemap["odelibs"] = "ode.lib"
    else:
        odemap["odelibs"] = "-lode"

    return forge.cctest(test_file, odemap)
