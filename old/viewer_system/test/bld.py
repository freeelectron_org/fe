import sys
forge = sys.modules["forge"]

def setup(module):

    setupTestDL(module)

    exe = module.Exe("xViewerSystemTest", [
        "xViewerSystemTest"
    ])

    deplibs = forge.corelibs + [
        "fexEvaluateDLLib",
        "fexMoaLib",
        "fexViewerSystemDLLib"
    ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexMechanicsDLLib",
                "fexMoaDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib" ]

    forge.deps(["xViewerSystemTestExe"], deplibs)

#   forge.tests += [
#       ("xViewerSystemTest.exe",       "", "", None) ]

def setupTestDL(module):

    dll = module.DLL("fexViewerSystemTestDL", [
        "ViewerSystemTestDL"
    ])

    deplibs = forge.corelibs + [
        # This is only needed because we are compiling our own ViewerSystem
        # implementation for this test. You don't need to link the lib to use
        # a ViewerSystem, only to compile a new one.
        "fexViewerSystemDLLib",
    ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [
                "fexMechanicsDLLib",
                "fexMoaDLLib",
                "fexThreadDLLib",
                "fexSolveDLLib" ]

    forge.deps(["fexViewerSystemTestDLLib"], deplibs)
