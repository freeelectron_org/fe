import sys
forge = sys.modules["forge"]

def setup(module):

    deplibs =   forge.corelibs + [
                "fexDrawDLLib",
                "fexThreadDLLib",
                "fexSurfaceDLLib",
                "fexDataToolDLLib" ]

    tests = [ 'xFbx' ]

    for t in tests:
        exe = module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

#   forge.tests += [
#       ("xFbx.exe",    "something.fbx",                None,       None) ]
