/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_HGO_DEBUG				FALSE
#define FE_HGO_FRAGMENT_VERBOSE		FALSE
#define FE_HGO_FRAGMENT_CONDITION	\
		(m_selectIndex>=0 && fragmentIndex==m_fragmentOfPoint[m_selectIndex])
#define FE_HGO_POINT_VERBOSE		FALSE
#define FE_HGO_POINT_CONDITION		\
		(m_spDrawGuide.isValid() && pointIndex==m_selectIndex)
#define FE_HGO_PROFILE				(FE_CODEGEN==FE_PROFILE)
#define FE_HGO_PROFILE_RUN			FALSE

//* TODO reset pivots when driver surface or pivot params change

//* TODO short arms are too strong of influence during softness

using namespace fe;
using namespace fe::ext;

void HingeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("Threads","page")="Config";

	catalog<String>("AutoThread","page")="Config";

	catalog<String>("Paging","page")="Config";

	catalog<String>("StayAlive","page")="Config";

	catalog<String>("Work","page")="Config";

	//* page Config

	catalog<bool>("fragment")=false;
	catalog<String>("fragment","label")="Fragment";
	catalog<String>("fragment","page")="Config";
	catalog<bool>("fragment","joined")=true;
	catalog<String>("fragment","hint")=
			"Associate collections of primitives which have"
			" matching a fragment attribute."
			"  Each fragment is considered an independent hinge."
			"  Without fragmentation,"
			" the entire input is treated as a single hinge.";

	catalog<String>("fragmentAttr")="part";
	catalog<String>("fragmentAttr","label")="By Attr";
	catalog<String>("fragmentAttr","page")="Config";
	catalog<String>("fragmentAttr","enabler")="fragment";
	catalog<bool>("fragmentAttr","joined")=true;
	catalog<String>("fragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives.";

	catalog<bool>("fragmentCollide")=false;
	catalog<String>("fragmentCollide","label")="Inter-Collide";
	catalog<String>("fragmentCollide","page")="Config";
	catalog<String>("fragmentCollide","enabler")="fragment";
	catalog<String>("fragmentCollide","hint")=
			"Check for collisions with other fragments.";

	catalog<bool>("partition")=false;
	catalog<String>("partition","label")="Partition Driver";
	catalog<String>("partition","page")="Config";
	catalog<bool>("partition","joined")=true;
	catalog<String>("partition","hint")=
			"Associate collections of driver primitives which have"
			" matching a partition attribute.";

	catalog<String>("partitionAttr")="part";
	catalog<String>("partitionAttr","label")="By Attr";
	catalog<String>("partitionAttr","page")="Config";
	catalog<String>("partitionAttr","enabler")="partition";
	catalog<String>("partitionAttr","hint")=
			"Name of driver string attribute describing membership"
			" in a collection of primitives.";

	catalog<String>("restrictions")="";
	catalog<String>("restrictions","label")="Restrictions";
	catalog<String>("restrictions","page")="Config";
	catalog<String>("restrictions","enabler")="partition";
	catalog<String>("restrictions","hint")=
			"List of space delimited rules controlling which"
			" collider partitions affect specific input fragments."
			"  Each rule is described in the form"
			" driver_part=\"collider_part1 collider_part2\"."
			"  The driver part for each fragment is found"
			" using a nearest point search on the driver."
			"  Quotes are only needed if more than one collider part"
			" is in a list."
			"  A driver part name listed alone, without '=value',"
			" matches only a like named collider part.";

	//* page Pivot

	catalog<String>("pivotMethod")="manual";
	catalog<String>("pivotMethod","label")="Pivot Method";
	catalog<String>("pivotMethod","choice:0")="manual";
	catalog<String>("pivotMethod","label:0")="Manual";
	catalog<String>("pivotMethod","choice:1")="du";
	catalog<String>("pivotMethod","label:1")="About Driver Du";
	catalog<String>("pivotMethod","choice:2")="normal";
	catalog<String>("pivotMethod","label:2")="Cross Driver Normal";
	catalog<bool>("pivotMethod","joined")=true;
	catalog<String>("pivotMethod","page")="Pivot";
	catalog<String>("pivotMethod","hint")=
			"Chooses how the pivot axis is determined."
			"  Manual method uses face, barycenter, and offset values"
			" explicitly specified in parameters."
			"  The pivot axis is the normalized difference"
			" of point B from point A."
			"  The longitudinal twist axis is from the midpoint"
			" of the pivot axis to point C."
			"  The Driver Du method simply samples the du value"
			" from the driver surface."
			"  The Cross Driver Normal method samples the normal"
			" from the driver surface and also makes a guess"
			" at a longitudinal axis for each hinge element."
			" The pivot axes comes from the cross product of these vectors,";

	catalog<bool>("reversePivot")=false;
	catalog<String>("reversePivot","label")="Reverse";
	catalog<String>("reversePivot","page")="Pivot";
	catalog<String>("reversePivot","hint")=
			"For every pivot axis, however it is determined,"
			" this blindly flips the direction.";

	catalog<I32>("faceA")=0;
	catalog<I32>("faceA","high")=100;
	catalog<I32>("faceA","max")=INT_MAX;
	catalog<String>("faceA","label")="Face A";
	catalog<bool>("faceA","joined")=true;
	catalog<String>("faceA","page")="Pivot";
	catalog<String>("faceA","hint")=
			"For the manual method, this is a face on the driver"
			" used to specify the first end of the pivot axis.";

	catalog<SpatialVector>("baryA")=SpatialVector(1.0,0.0,0.0);
	catalog<String>("baryA","label")="Barycenter A";
	catalog<String>("baryA","page")="Pivot";
	catalog<String>("baryA","hint")=
			"For the manual method, this is a barycentric coordinate on Face A"
			" for the first end of the pivot axis.";

	catalog<SpatialVector>("offsetA");
	catalog<String>("offsetA","label")="Offset A";
	catalog<String>("offsetA","page")="Pivot";
	catalog<String>("offsetA","hint")=
			"For the manual method, this displaces the first end"
			" of the pivot axis away from Face A, in relative space.";

	catalog<I32>("faceB")=0;
	catalog<I32>("faceB","high")=100;
	catalog<I32>("faceB","max")=INT_MAX;
	catalog<String>("faceB","label")="Face B";
	catalog<bool>("faceB","joined")=true;
	catalog<String>("faceB","page")="Pivot";
	catalog<String>("faceB","hint")=
			"For the manual method, this is a face on the driver"
			" used to specify the second end of the pivot axis.";

	catalog<SpatialVector>("baryB")=SpatialVector(1.0,0.0,0.0);
	catalog<String>("baryB","label")="Barycenter B";
	catalog<String>("baryB","page")="Pivot";
	catalog<String>("baryB","hint")=
			"For the manual method, this is a barycentric coordinate on Face B"
			" for the second end of the pivot axis.";

	catalog<SpatialVector>("offsetB");
	catalog<String>("offsetB","label")="Offset B";
	catalog<String>("offsetB","page")="Pivot";
	catalog<String>("offsetB","hint")=
			"For the manual method, this displaces the second end"
			" of the pivot axis away from Face B, in relative space.";

	catalog<I32>("faceC")=0;
	catalog<I32>("faceC","high")=100;
	catalog<I32>("faceC","max")=INT_MAX;
	catalog<String>("faceC","label")="Face C";
	catalog<bool>("faceC","joined")=true;
	catalog<String>("faceC","page")="Pivot";
	catalog<String>("faceC","hint")=
			"For the manual method, this is a face on the driver"
			" used to specify the second end of the twist axis.";

	catalog<SpatialVector>("baryC")=SpatialVector(1.0,0.0,0.0);
	catalog<String>("baryC","label")="Barycenter C";
	catalog<String>("baryC","page")="Pivot";
	catalog<String>("baryB","hint")=
			"For the manual method, this is a barycentric coordinate on Face C"
			" for the second end of the twist axis.";

	catalog<SpatialVector>("offsetC");
	catalog<String>("offsetC","label")="Offset C";
	catalog<String>("offsetC","page")="Pivot";
	catalog<String>("offsetC","hint")=
			"For the manual method, this displaces the second end"
			" of the twist axis away from Face C, in relative space.";

	//* page Behavior

	catalog<I32>("passes")=1;
	catalog<I32>("passes","min")=1;
	catalog<I32>("passes","high")=10;
	catalog<I32>("passes","max")=1000;
	catalog<String>("passes","label")="Passes";
	catalog<String>("passes","page")="Behavior";
	catalog<bool>("passes","joined")=true;
	catalog<String>("passes","hint")=
			"The number of iterations towards a solution.";

	catalog<Real>("rate")=1.0;
	catalog<Real>("rate","max")=1.0;
	catalog<String>("rate","label")="Rate";
	catalog<String>("rate","page")="Behavior";
	catalog<String>("rate","hint")=
			"The fraction of each result applied for each iteration."
			"  A reduced rate may possibly counter an instability,"
			" but it will more likely just cause an incomplete result.";

	catalog<String>("collideMethod")="nearest";
	catalog<String>("collideMethod","label")="Collision Method";
	catalog<String>("collideMethod","choice:0")="ray";
	catalog<String>("collideMethod","label:0")="Ray Cast";
	catalog<String>("collideMethod","choice:1")="nearest";
	catalog<String>("collideMethod","label:1")="Nearest Point";
	catalog<String>("collideMethod","page")="Behavior";
	catalog<bool>("collideMethod","joined")=true;
	catalog<String>("collideMethod","hint")=
			"Type a search used to find potentially intersecting surfaces.";

	catalog<I32>("hitLimit")=8;
	catalog<String>("hitLimit","label")="Hit Limit";
	catalog<I32>("hitLimit","min")=1;
	catalog<I32>("hitLimit","high")=16;
	catalog<I32>("hitLimit","max")=100;
	catalog<String>("hitLimit","page")="Behavior";
	catalog<bool>("hitLimit","joined")=true;
	catalog<String>("hitLimit","hint")=
			"Maximum number of contacts found when searching for collisions.";

	catalog<bool>("dynamic")=true;
	catalog<String>("dynamic","label")="Dynamic";
	catalog<String>("dynamic","page")="Behavior";
	catalog<String>("dynamic","hint")=
			"Rebuild the search tree for each iteration."
			"  Otherwise, the contacts found for the first iteration are reused,"
			" with their locations updated.";

	catalog<Real>("angleLimit")=180.0;
	catalog<Real>("angleLimit","high")=360.0;
	catalog<String>("angleLimit","label")="Angle Limit";
	catalog<String>("angleLimit","page")="Behavior";
	catalog<String>("angleLimit","hint")=
			"Stops the axial rotation from exceeding this limit, in degrees.";

	catalog<Real>("threshold")=0.0;
	catalog<Real>("threshold","min")= -1e6;
	catalog<Real>("threshold","low")=0.0;
	catalog<Real>("threshold","high")=1.0;
	catalog<Real>("threshold","max")=1e6;
	catalog<String>("threshold","label")="Threshold";
	catalog<String>("threshold","page")="Behavior";
	catalog<bool>("threshold","joined")=true;
	catalog<String>("threshold","hint")=
			"The minimum distance desired between"
			" potentially colliding surfaces.";

	catalog<Real>("thresholdFactor")=0.0;
	catalog<Real>("thresholdFactor","high")=0.1;
	catalog<Real>("thresholdFactor","max")=1.0;
	catalog<String>("thresholdFactor","label")="Factor";
	catalog<String>("thresholdFactor","page")="Behavior";
	catalog<String>("thresholdFactor","hint")=
			"For fragments, the fragment length scaled by this factor"
			" is added to the threshold.";

	catalog<Real>("deadRadius")=0.0;
	catalog<Real>("deadRadius","high")=10.0;
	catalog<Real>("deadRadius","max")=1e6;
	catalog<String>("deadRadius","label")="Dead Radius";
	catalog<bool>("deadRadius","joined")=true;
	catalog<String>("deadRadius","page")="Behavior";
	catalog<String>("deadRadius","hint")=
			"Distance from base for which collisions are ignored.";

	catalog<Real>("deadRadiusFactor")=0.0;
	catalog<Real>("deadRadiusFactor","high")=1.0;
	catalog<Real>("deadRadiusFactor","max")=1.0;
	catalog<String>("deadRadiusFactor","label")="Factor";
	catalog<String>("deadRadiusFactor","page")="Behavior";
	catalog<String>("deadRadiusFactor","hint")=
			"For fragments, the fragment length scaled by this factor"
			" is added to the dead radius.";

	catalog<Real>("liveRadius")=0.0;
	catalog<Real>("liveRadius","high")=10.0;
	catalog<Real>("liveRadius","max")=1e6;
	catalog<String>("liveRadius","label")="Live Radius";
	catalog<bool>("liveRadius","joined")=true;
	catalog<String>("liveRadius","page")="Behavior";
	catalog<String>("liveRadius","hint")=
			"If larger than Dead Radius,"
			" this ramps up the collision response up to this distance."
			"  The default of zero always has no ramping,"
			" so everything beyond the Dead Radius is fully responsive.";

	catalog<Real>("liveRadiusFactor")=0.0;
	catalog<Real>("liveRadiusFactor","high")=1.0;
	catalog<Real>("liveRadiusFactor","max")=1.0;
	catalog<String>("liveRadiusFactor","label")="Factor";
	catalog<String>("liveRadiusFactor","page")="Behavior";
	catalog<String>("liveRadiusFactor","hint")=
			"For fragments, the fragment length scaled by this factor"
			" is added to the live radius.";

	catalog<bool>("Temporal")=false;
	catalog<String>("Temporal","page")="Behavior";
	catalog<bool>("Temporal","joined")=true;
	catalog<String>("Temporal","hint")=
			"Tries to retain state of the prior frame."
			"  If the prior frame was not the last evaluated,"
			" the node may choose not to use temporal data.";

	catalog<Real>("Response")=1.0;
	catalog<String>("Response","enabler")="Temporal";
	catalog<String>("Response","page")="Behavior";
	catalog<String>("Response","hint")=
			"When temporal, this defines how quickly the surface returns"
			" to its original shaped when released.";

	catalog<bool>("twist")=false;
	catalog<String>("twist","label")="Twist";
	catalog<bool>("twist","joined")=true;
	catalog<String>("twist","page")="Behavior";
	catalog<String>("twist","hint")=
			"Allow each hinge to also rotate around its longitudinal axis.";

	catalog<Real>("twistiness")=1.0;
	catalog<Real>("twistiness","high")=1.0;
	catalog<Real>("twistiness","max")=1e6;
	catalog<String>("twistiness","label")="Twistiness";
	catalog<String>("twistiness","page")="Behavior";
	catalog<String>("twistiness","hint")=
			"Freedom of each hinge around its longitudinal axis.";

	catalog<bool>("soft")=false;
	catalog<String>("soft","label")="Soft";
	catalog<bool>("soft","joined")=true;
	catalog<String>("soft","page")="Behavior";
	catalog<String>("soft","hint")=
			"Allow each vertex to rotate somewhat independently.";

	catalog<Real>("softness")=180.0;
	catalog<Real>("softness","high")=180.0;
	catalog<Real>("softness","max")=1e6;
	catalog<String>("softness","label")="Softness";
	catalog<String>("softness","page")="Behavior";
	catalog<String>("softness","hint")=
			"Maximum angle difference between adjacent vertices,"
			" in degrees, scaled by edge length over fragment length.";

	catalog<bool>("conform")=false;
	catalog<String>("conform","label")="Conform";
	catalog<bool>("conform","joined")=true;
	catalog<String>("conform","page")="Behavior";
	catalog<String>("conform","hint")=
			"Pull individual rotations of vertices about their pivot axis"
			" towards the average of their hinge.";

	catalog<Real>("conformity")=0.5;
	catalog<Real>("conformity","max")=1.0;
	catalog<String>("conformity","label")="Conformity";
	catalog<String>("conformity","page")="Behavior";
	catalog<String>("conformity","hint")=
			"Amount of roation similarity imposed on vertices of each hinge.";

	catalog<bool>("loft")=false;
	catalog<String>("loft","label")="Loft";
	catalog<bool>("loft","joined")=true;
	catalog<String>("loft","page")="Behavior";
	catalog<String>("loft","hint")=
			"Add rotation to each hinge on the first iteration.";

	catalog<Real>("loftAngle")=0.0;
	catalog<Real>("loftAngle","low")= -90.0;
	catalog<Real>("loftAngle","high")=90.0;
	catalog<Real>("loftAngle","min")= -360.0;
	catalog<Real>("loftAngle","max")=360.0;
	catalog<String>("loftAngle","label")="By";
	catalog<String>("loftAngle","page")="Behavior";
	catalog<String>("loftAngle","hint")=
			"Amount of additional rotation, in degrees.";

	catalog<bool>("attract")=false;
	catalog<String>("attract","label")="Attract";
	catalog<bool>("attract","joined")=true;
	catalog<String>("attract","page")="Behavior";
	catalog<String>("attract","hint")=
			"Pull vertices towards the collision surface.";

	catalog<Real>("attractiveness")=0.1;
	catalog<Real>("attractiveness","max")=1.0;
	catalog<String>("attractiveness","label")="Attractiveness";
	catalog<String>("attractiveness","page")="Behavior";
	catalog<String>("attractiveness","hint")=
			"Amount of attraction towards the collision surface.";

	//* page Display

	catalog<bool>("drawPivot")=false;
	catalog<String>("drawPivot","label")="Draw Pivot";
	catalog<String>("drawPivot","page")="Display";
	catalog<String>("drawPivot","hint")=
			"Show pivot and twist axes in the display.";

	catalog<I32>("showPass")= -1;
	catalog<I32>("showPass","min")= -1;
	catalog<I32>("showPass","high")=10;
	catalog<I32>("showPass","max")=1000;
	catalog<String>("showPass","label")="Show Pass";
	catalog<String>("showPass","page")="Display";
	catalog<String>("showPass","hint")=
			"Limit debug display to one iteration step."
			"  The default of -1 shows all iterations."
			"  The debug display is provoked by selecting a vertex"
			" interactively, where available.";

	catalog<bool>("isolate")=false;
	catalog<String>("isolate","label")="Isolate";
	catalog<bool>("isolate","joined")=true;
	catalog<String>("isolate","page")="Display";
	catalog<String>("isolate","hint")=
			"Shrink all hinges except."
			"  Note that this changes the actual output mesh.";

	catalog<String>("isolateFragment");
	catalog<String>("isolateFragment","label")="Fragment";
	catalog<String>("isolateFragment","page")="Display";
	catalog<String>("isolateFragment","hint")=
			"Which fragment to isolate, by name.";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","prompt")="Point at any input vertex.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Collider Surface");
	catalog<bool>("Collider Surface","optional")=true;

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog< sp<Component> >("Reference Input Surface");
	catalog<bool>("Reference Input Surface","optional")=true;

	catalog< sp<Component> >("Reference Driver Surface");
	catalog<bool>("Reference Driver Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;
	catalog<bool>("Output Surface","temporal")=true;

	m_spBrush=new DrawMode();
	m_spBrush->setDrawStyle(DrawMode::e_foreshadow);
	m_spBrush->setLineWidth(2.0);
	m_spBrush->setAntialias(TRUE);
	m_spBrush->setLit(FALSE);

	m_spOverlay=new DrawMode();
	m_spOverlay->setLineWidth(3.0);
	m_spOverlay->setLit(FALSE);
}

void HingeOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

#if FE_HGO_DEBUG
	feLog("HingeOp::handle \"%s\" brush %d brushed %d\n",
			name().c_str(),spDrawBrush.isValid(),m_brushed);
#endif

	if(spDrawBrush.isValid())
	{
		m_brushed=TRUE;

		const Color blue(0.0,0.0,1.0);
		const Color yellow(1.0,1.0,0.0);

		const Real littleDot=3.0;
		const Real bigDot=6.0;

		if(m_spOutput.isValid())
		{
			sp<DrawI> spDrawOverlay;
			if(!accessBrushOverlay(spDrawOverlay,a_rSignal)) return;

			spDrawBrush->pushDrawMode(m_spBrush);
			spDrawOverlay->pushDrawMode(m_spOverlay);

			sp<ViewI> spView=spDrawOverlay->view();

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			m_event.bind(windowEvent(a_rSignal));

#if FE_HGO_DEBUG
			feLog("HingeOp::handle event %s\n",c_print(m_event));
#endif

			if(!m_event.isExpose())
			{
				const BWORD leftPress=
						m_event.isMousePress(WindowEvent::e_itemLeft);
				const BWORD escapePress=
						m_event.isKeyPress(WindowEvent::e_keyEscape);

				if(escapePress)
				{
					m_selectIndex=-1;
				}

				if(escapePress || leftPress)
				{
					catalog<String>("Brush","cook")="once";
				}

				I32 pickIndex= -1;

				const Real maxDistance= -1.0;
				sp<SurfaceSearchable::Impact> spImpact=m_spOutput->rayImpact(
						rRayOrigin,rRayDirection,maxDistance);
				if(spImpact.isValid())
				{
#if FE_HGO_DEBUG
					const I32 primitiveIndex=spImpact->primitiveIndex();
					feLog("HingeOp::handle impact prim %d\n",primitiveIndex);
#endif

					I32 pointIndex= -1;

					sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
					if(spTriImpact.isValid())
					{
						pointIndex=nearestPointIndex(spTriImpact);
					}

					sp<SurfaceCurves::Impact> spCurveImpact=spImpact;
					if(spCurveImpact.isValid())
					{
						sp<SurfaceAccessorI> spOutputVertices;
						if(!access(spOutputVertices,m_spOutputAccessible,
								e_primitive,e_vertices)) return;

						pointIndex=nearestPointIndex(
								spOutputVertices,spCurveImpact);
					}

					if(pointIndex>=0)
					{
						pickIndex=pointIndex;
					}
				}

				sp<SurfaceAccessorI> spOutputPoints;
				if(!access(spOutputPoints,m_spOutputAccessible,
						e_point,e_position)) return;

				const I32 pointCount=spOutputPoints->count();

				if(pickIndex>=0 && pickIndex<pointCount)
				{
					if(leftPress)
					{
						if(m_selectIndex!=pickIndex)
						{
							feLog("HingeOp::handle select %d -> %d\n",
									m_selectIndex,pickIndex);
						}
						m_selectIndex=pickIndex;
					}

					const SpatialVector point=
							spOutputPoints->spatialVector(pickIndex);

					drawDot(spView,spDrawOverlay,point,bigDot,blue);
				}
				else if(leftPress)
				{
					m_selectIndex=-1;
				}

				if(m_selectIndex>=0 && m_selectIndex<pointCount)
				{
					const SpatialVector point=
							spOutputPoints->spatialVector(m_selectIndex);

					drawDot(spView,spDrawOverlay,point,littleDot,yellow);
				}
			}

			spDrawOverlay->popDrawMode();
			spDrawBrush->popDrawMode();
		}

		return;
	}

#if FE_HGO_PROFILE
	m_spProfiler=new Profiler("HingeOp");
	m_spProfileAccess=new Profiler::Profile(m_spProfiler,"Access");
	m_spProfileInit=new Profiler::Profile(m_spProfiler,"Init");
	m_spProfileClear=new Profiler::Profile(m_spProfiler,"Clear");
	m_spProfileCopy=new Profiler::Profile(m_spProfiler,"Copy");
	m_spProfileFragment=new Profiler::Profile(m_spProfiler,"Fragment");
	m_spProfilePartition=new Profiler::Profile(m_spProfiler,"Partition");
	m_spProfilePivots=new Profiler::Profile(m_spProfiler,"Pivots");
	m_spProfileLoop=new Profiler::Profile(m_spProfiler,"Loop");
	m_spProfileRun=new Profiler::Profile(m_spProfiler,"Run");
	m_spProfilePush=new Profiler::Profile(m_spProfiler,"Push");
	m_spProfilePull=new Profiler::Profile(m_spProfiler,"Pull");
	m_spProfileRotate=new Profiler::Profile(m_spProfiler,"Rotate");

	m_spProfiler->begin();
	m_spProfileAccess->start();
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="";

	const BWORD partition=catalog<bool>("partition");
	const String partitionAttr=catalog<String>("partitionAttr");
	const String fragmentAttr=catalog<String>("fragmentAttr");
	const String pivotMethod=catalog<String>("pivotMethod");
	const BWORD reversePivot=catalog<bool>("reversePivot");
	const BWORD dynamic=catalog<bool>("dynamic");
	const Real twistiness=catalog<Real>("twistiness");
	const BWORD twist=(twistiness>0.0 && catalog<bool>("twist"));
	const Real softness=catalog<Real>("softness")*degToRad;
	const BWORD soft=catalog<bool>("soft");
	const Real conformity=catalog<Real>("conformity");
	const BWORD conform=(conformity>0.0 && catalog<bool>("conform"));
	const Real loftAngle=catalog<Real>("loftAngle")*degToRad;
	const BWORD loft=(loftAngle!=0.0 && catalog<bool>("loft"));
	const Real attractiveness=catalog<Real>("attractiveness");
	const BWORD attract=(attractiveness>0.0 && catalog<bool>("attract"));
	const I32 passes=catalog<I32>("passes");
	const Real rate=catalog<Real>("rate");
	const BWORD drawPivot=catalog<bool>("drawPivot");
	const I32 showPass=catalog<I32>("showPass");
	const BWORD isolate=catalog<bool>("isolate");
	const String isolateFragment=catalog<String>("isolateFragment");

	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	const I32 pointCount=spOutputPoint->count();

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessibleI> spInputAccessibleRef;
	sp<SurfaceAccessorI> spInputPointRef;
	if(access(spInputAccessibleRef,"Reference Input Surface",e_quiet))
	{
		if(!access(spInputPointRef,spInputAccessibleRef,e_point,e_position))
		{
			spInputAccessibleRef=NULL;
		}
		else if(I32(spInputPointRef->count())!=pointCount)
		{
			catalog<String>("warning")+=
					"ignoring input reference with different number of points;";
			spInputAccessibleRef=NULL;
			spInputPointRef=NULL;
		}
	}

	BWORD transient=FALSE;
	sp<SurfaceAccessorI> spTransientAccessor;
	if(access(spTransientAccessor,spInputAccessible,
			e_detail,"transient",e_quiet))
	{
		transient=spTransientAccessor->integer(0);
	}

//	feLog("transient %d\n",transient);

	const BWORD inputRefReplaced=(m_spInputRef.isNull() ||
			catalogOrDefault<bool>("Reference Input Surface",
			"replaced",true));
	if(spInputAccessibleRef.isValid())
	{
		if(inputRefReplaced)
		{
			m_spInputRef=NULL;
			access(m_spInputRef,spInputAccessibleRef,e_quiet);
		}
	}
	else
	{
		m_spInputRef=NULL;
	}

	const BWORD driverReplaced=(m_spDriver.isNull() ||
			catalogOrDefault<bool>("Driver Surface","replaced",true));
	if(driverReplaced)
	{
		m_spDriver=NULL;
		access(m_spDriver,"Driver Surface",e_quiet);
	}
	if(m_spDriver.isValid())
	{
		m_spDriver->setRefinement(0);
	}

	const BWORD driverRefReplaced=(m_spDriverRef.isNull() ||
			catalogOrDefault<bool>("Reference Driver Surface",
			"replaced",true));
	if(driverRefReplaced)
	{
		m_spDriverRef=NULL;
		if(m_spDriver.isValid())
		{
			access(m_spDriverRef,"Reference Driver Surface",e_quiet);
		}

		m_colliderPartitionMap.clear();
	}
	if(m_spDriverRef.isValid())
	{
		m_spDriverRef->setRefinement(0);
	}
	else
	{
		m_spDriverRef=m_spDriver;
	}

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal,e_warning);

	const BWORD colliderReplaced=(m_spCollider.isNull() ||
			catalogOrDefault<bool>("Collider Surface","replaced",true));
	if(colliderReplaced)
	{
		m_spCollider=NULL;
		access(m_spCollider,"Collider Surface",e_quiet);

		m_colliderPartitionMap.clear();

		if(m_spCollider.isValid())
		{
			if(partition)
			{
				m_spCollider->partitionWith(partitionAttr);
			}
			m_spCollider->setRefinement(0);
			m_spCollider->prepareForSearch();
			if(partition)
			{
				m_spCollider->setPartitionFilter(".*");
			}
		}
	}

#if FE_HGO_PROFILE
	m_spProfileInit->replace(m_spProfileAccess);
#endif

	SpatialVector pivotA=catalog<SpatialVector>("offsetA");
	SpatialVector pivotB=catalog<SpatialVector>("offsetB");
	SpatialVector pivotC=catalog<SpatialVector>("offsetC");

	if(m_spDriver.isValid() && pivotMethod=="manual")
	{
		const I32 triangleIndexA=catalog<I32>("faceA");
		const SpatialBary baryA=catalog<SpatialVector>("baryA");
		const SpatialTransform xformA=
				m_spDriver->sample(triangleIndexA,baryA);
		transformVector(xformA,pivotA,pivotA);

		const I32 triangleIndexB=catalog<I32>("faceB");
		const SpatialBary baryB=catalog<SpatialVector>("baryB");
		const SpatialTransform xformB=
				m_spDriver->sample(triangleIndexB,baryB);
		transformVector(xformB,pivotB,pivotB);

		const I32 triangleIndexC=catalog<I32>("faceC");
		const SpatialBary baryC=catalog<SpatialVector>("baryC");
		const SpatialTransform xformC=
				m_spDriver->sample(triangleIndexC,baryC);
		transformVector(xformC,pivotC,pivotC);
	}

	if(reversePivot)
	{
		const SpatialVector store=pivotA;
		pivotA=pivotB;
		pivotB=store;
	}

	const BWORD temporal=catalog<bool>("Temporal");
	const Real response=catalog<Real>("Response");
	const Real radianLimit=catalog<Real>("angleLimit")*degToRad;

	const BWORD doTemporal=(temporal && response<1.0);

	if(doTemporal)
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"temporal";
	}
	if(twist)
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"twist";
	}
	if(soft)
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"soft";
	}
	if(conform)
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"conform";
	}
	if(loft)
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"loft";
	}
	if(attract)
	{
		rSummary+=String(rSummary.empty()? "": " + ")+"attract";
	}

	const I32 frame=currentFrame(a_rSignal);
	const BWORD outputReplaced=catalog<bool>("Output Surface","replaced");
	const BWORD useCache=doTemporal && m_pointAngle.size()==U32(pointCount) &&
			(frame==m_frame+1 || (frame==m_frame && !outputReplaced));

#if FE_HGO_DEBUG
	feLog("HingeOp::handle frame %d->%d outputReplaced %d"
			" pointCount %d->%d cache %d\n",
			m_frame,frame,outputReplaced,
			spInputPoint->count(),pointCount,m_pointAngle.size());
#endif

	const U32 primitiveCount=spOutputVertices->count();

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

#if FE_HGO_PROFILE
	m_spProfileClear->replace(m_spProfileInit);
#endif

	m_inputCache.resize(pointCount);
	m_outputCache.resize(pointCount);

	m_colliderHitTable.resize(pointCount);
	m_outputHitTable.resize(pointCount);

	//* NOTE only loose check when transient
	const BWORD inputReplaced=transient?
			(m_lastPointCount!=pointCount):
			catalogOrDefault<bool>("Input Surface","replaced",true);

	if(inputReplaced)
	{
		m_partitionMap.clear();
		m_nameOfFragment.clear();
		m_lastPointCount=pointCount;

		if(spInputPointRef.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				m_inputCache[pointIndex]=
						spInputPointRef->spatialVector(pointIndex);
			}
		}
		else
		{
			FEASSERT(spInputPoint.isValid());
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				m_inputCache[pointIndex]=
						spInputPoint->spatialVector(pointIndex);
			}
		}
	}

	const BWORD replacePivots=(inputRefReplaced || driverRefReplaced ||
			m_spInputRef.isNull() || m_spDriverRef.isNull());

#if FE_HGO_PROFILE
	m_spProfileCopy->replace(m_spProfileClear);
#endif

	const Real startAngle=loft? loftAngle: 0.0;

	//* presuming always replaced with frame change
	if(useCache)
	{
#if FE_HGO_DEBUG
		if(frame==m_frame+1)
		{
			feLog("HingeOp::handle temporal step\n");
		}
		else
		{
			feLog("HingeOp::handle temporal repeat\n");
		}
#endif

		FEASSERT(I32(m_totalAngle.size())==pointCount);
		FEASSERT(I32(m_pointAngle.size())==pointCount);
		FEASSERT(I32(m_lastAngle.size())==pointCount);

		const Real startResponse=startAngle*response;
		const Real response1=Real(1)-response;

		//* output = history:prior blended with pure input
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			m_pointAngle[pointIndex]=
					startResponse+m_totalAngle[pointIndex]*response1;

			if(m_pointAngle[pointIndex]>radianLimit)
			{
				m_pointAngle[pointIndex]=radianLimit;
			}

			m_totalAngle[pointIndex]=0.0;
			m_lastAngle[pointIndex]=0.0;
		}
	}
	else
	{
		m_totalAngle.resize(pointCount);
		m_pointAngle.resize(pointCount);
		m_lastAngle.resize(pointCount);

#if FE_HGO_DEBUG
		feLog("HingeOp::handle rebuild\n");
#endif

		//* output = pure input
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			m_totalAngle[pointIndex]=0.0;
			m_pointAngle[pointIndex]=startAngle;
			m_lastAngle[pointIndex]=0.0;
		}
	}
	m_frame=frame;

	if(!outputReplaced)
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spInputPoint->spatialVector(pointIndex);
			spOutputPoint->set(pointIndex,point);

			m_outputCache[pointIndex]=point;
		}
	}
	else
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			m_outputCache[pointIndex]=
					spOutputPoint->spatialVector(pointIndex);
		}
	}

#if FE_HGO_PROFILE
	m_spProfileFragment->replace(m_spProfileCopy);
#endif

	I32 fragmentCount=1;

	m_fragmented=(!fragmentAttr.empty() && catalog<bool>("fragment"));

	if(m_fragmented)
	{
		const String inputGroup="";	//* TODO

		spInputVertices->fragmentWith(SurfaceAccessibleI::e_primitive,
				fragmentAttr,inputGroup);
		fragmentCount=spInputVertices->fragmentCount();
		if(!fragmentCount)
		{
			fragmentCount=1;
			m_fragmented=FALSE;
		}
	}


#if FALSE
	m_fragPivotRefA.clear();
	m_fragPivotRefB.clear();
	m_fragPivotRefC.clear();
	m_fragPivotA.clear();
	m_fragPivotB.clear();
	m_fragPivotC.clear();
	m_fragLength.clear();
	m_fragFarIndex.clear();

	m_fragPivotA.resize(1);
	m_fragPivotB.resize(1);
	m_fragPivotC.resize(1);
	m_fragLength.resize(1);
	m_fragFarIndex.resize(1);
#else
	m_fragPivotRefA.resize(fragmentCount);
	m_fragPivotRefB.resize(fragmentCount);
	m_fragPivotRefC.resize(fragmentCount);
	m_fragPivotA.resize(fragmentCount);
	m_fragPivotB.resize(fragmentCount);
	m_fragPivotC.resize(fragmentCount);
	m_fragLength.resize(fragmentCount);
	m_fragFarIndex.resize(fragmentCount);
#endif

	if(replacePivots)
	{
		m_fragPivotA[0]=pivotA;
		m_fragPivotB[0]=pivotB;
		m_fragPivotC[0]=pivotC;
		m_fragLength[0]= -1.0;
		m_fragFarIndex[0]= -1;
	}

	if(fragmentCount>1)
	{
		rSummary.sPrintf("%d %s",fragmentCount,rSummary.c_str());
	}

	const BWORD fragmentCollide=
			(m_fragmented && catalog<bool>("fragmentCollide"));

	if(!transient)
	{
		m_spOutput=NULL;
	}

	if(I32(m_nameOfFragment.size())!=fragmentCount)
	{
		m_nameOfFragment.resize(fragmentCount);
		if(m_fragmented)
		{
			for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
			{
				const String fragmentName=
						spInputVertices->fragment(fragmentIndex);

				m_nameOfFragment[fragmentIndex]=fragmentName;
			}
		}
		else
		{
			m_nameOfFragment[0]="";
		}
	}

	const BWORD rebuildPartitionMap=
			(fragmentCollide && !m_partitionMap.size());

	if(rebuildPartitionMap)
	{
		m_spOutput=NULL;
		access(m_spOutput,m_spOutputAccessible);

		if(m_fragmented)
		{
			m_spOutput->partitionWith(fragmentAttr);
		}
		m_spOutput->setRefinement(0);
		m_spOutput->setTriangulation(SurfaceI::e_existingPoints);
		m_spOutput->prepareForSample();
		if(m_fragmented)
		{
			m_spOutput->setPartitionFilter(".*");

			std::map<String,I32> partitionOfName;

			const I32 partitionCount=m_spOutput->partitionCount();
			for(I32 partitionIndex=0;partitionIndex<partitionCount;
					partitionIndex++)
			{
				const String partitionName=
						m_spOutput->partitionName(partitionIndex);
				partitionOfName[partitionName]=partitionIndex;
			}

			m_fragmentOfPartition.resize(partitionCount);

			for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
			{
				const String fragmentName=m_nameOfFragment[fragmentIndex];

				const I32 partitionIndex=partitionOfName[fragmentName];
				m_fragmentOfPartition[partitionIndex]=fragmentIndex;
			}
		}
	}
	setAtomicRange(m_spOutputAccessible,e_pointsOnly);

	const BWORD driverPartitioned=(m_spDriverRef.isValid() &&
			!partitionAttr.empty() && partition);
	if(driverPartitioned)
	{
		m_spDriverRef->partitionWith(partitionAttr);
		m_spDriverRef->setPartitionFilter(".*");
	}

#if FE_HGO_PROFILE
	m_spProfilePivots->replace(m_spProfileFragment);
#endif

	m_fragmentOfPoint.resize(pointCount);
	m_driverOfFragment.resize(fragmentCount);

	Array< sp<SurfaceAccessibleI::FilterI> > fragFilter(fragmentCount);
	Array< sp<SpannedRange> > fragSpannedRange(fragmentCount);

	for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		I32 minPointIndex=0;

		sp<SurfaceAccessibleI::FilterI>& rspFilter=fragFilter[fragmentIndex];
		sp<SpannedRange>& rspFragmentRange=fragSpannedRange[fragmentIndex];
		rspFragmentRange=sp<SpannedRange>(new SpannedRange());

		I32 filterCount=primitiveCount;

		if(m_fragmented)
		{
			const String fragmentName=m_nameOfFragment[fragmentIndex];

			if(!spInputVertices->filterWith(fragmentName,rspFilter) ||
					rspFilter.isNull())
			{
				m_fragLength[fragmentIndex]= -2.0;
				continue;
			}

			filterCount=rspFilter->filterCount();
			if(!filterCount)
			{
				m_fragLength[fragmentIndex]= -3.0;
				continue;
			}

			minPointIndex=pointCount;

			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=rspFilter->filter(filterIndex);
				const U32 subCount=spInputVertices->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=
							spInputVertices->integer(reIndex,subIndex);
					rspFragmentRange->nonAtomic().add(pointIndex,pointIndex);

					if(minPointIndex>pointIndex)
					{
						minPointIndex=pointIndex;
					}
					m_fragmentOfPoint[pointIndex]=fragmentIndex;
				}
			}
		}
		else
		{
			rspFragmentRange=fullRange();
		}
		if(replacePivots)
		{
			SpatialVector midpointRef(0.0,0.0,0.0);
			SpatialVector midpoint(0.0,0.0,0.0);
			if(minPointIndex<I32(pointCount))
			{
				midpoint=spInputPoint->spatialVector(minPointIndex);
				midpointRef=m_inputCache[minPointIndex];
			}

			SpatialVector farPointRef=midpointRef;
			SpatialVector farPoint=midpoint;
			Real farDistance2=0.0;
			I32 farIndex= -1;

			if(m_fragmented)
			{
				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 reIndex=rspFilter->filter(filterIndex);
					const U32 subCount=
							spInputVertices->subCount(reIndex);

					for(U32 subIndex=(filterIndex==0);
							subIndex<subCount;subIndex++)
					{
						const I32 pointIndex=spInputVertices->integer(
								reIndex,subIndex);

						const SpatialVector otherPoint=
								spInputPoint->spatialVector(pointIndex);
						const SpatialVector otherPointRef=
								m_inputCache[pointIndex];
						const Real distance2=
								magnitudeSquared(otherPointRef-midpointRef);
						if(farDistance2<distance2)
						{
							farDistance2=distance2;
							farIndex=pointIndex;
							farPoint=otherPoint;
							farPointRef=otherPointRef;
						}
					}
				}
			}
			else
			{
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const SpatialVector otherPoint=
							spInputPoint->spatialVector(pointIndex);
					const SpatialVector otherPointRef=m_inputCache[pointIndex];
					const Real distance2=
							magnitudeSquared(otherPointRef-midpointRef);
					if(farDistance2<distance2)
					{
						farDistance2=distance2;
						farIndex=pointIndex;
						farPoint=otherPoint;
						farPointRef=otherPointRef;
					}
				}
			}

			SpatialVector pivotRefA=pivotA;
			SpatialVector pivotRefB=pivotB;
			SpatialVector pivotRefC=pivotC;

			if(m_spDriverRef.isValid())
			{
				sp<SurfaceI::ImpactI> spImpactRef=
						m_spDriverRef->nearestPoint(midpointRef);

				if(driverPartitioned && spImpactRef.isValid())
				{
					const I32 driverIndex=spImpactRef->partitionIndex();

					m_driverOfFragment[fragmentIndex]=
							m_spDriverRef->partitionName(driverIndex);

//					feLog("fragment %d \"%s\" driver %d \"%s\"\n",
//							fragmentIndex,
//							m_nameOfFragment[fragmentIndex].c_str(),
//							driverIndex,
//							m_driverOfFragment[fragmentIndex].c_str());
				}

				sp<SurfaceI::ImpactI> spImpact=
						(m_spDriver==m_spDriverRef)? spImpactRef:
						m_spDriver->nearestPoint(midpoint);
				if(spImpactRef.isValid() && spImpact.isValid())
				{
					if(pivotMethod=="du")
					{
						// HACK negative (should work with reversePivot)
						const SpatialVector duRef=spImpactRef->du();

						pivotRefA=midpointRef-duRef;
						pivotRefB=midpointRef+duRef;
						pivotRefC=farPointRef;

						const SpatialVector du=spImpact->du();

						pivotA=midpoint-du;
						pivotB=midpoint+du;
						pivotC=farPoint;
					}
					else if(pivotMethod=="normal")
					{
						const SpatialVector normRef=spImpactRef->normal();
						const SpatialVector armRef=
								unitSafe(farPointRef-midpointRef);
						const SpatialVector sideRef=cross(armRef,normRef);

						pivotRefA=midpointRef-sideRef;
						pivotRefB=midpointRef+sideRef;
						pivotRefC=farPointRef;

						const SpatialVector norm=spImpact->normal();
						const SpatialVector arm=
								unitSafe(farPoint-midpoint);
						const SpatialVector side=cross(arm,norm);

						pivotA=midpoint-side;
						pivotB=midpoint+side;
						pivotC=farPoint;
					}

					if(reversePivot)
					{
						SpatialVector store=pivotRefA;
						pivotRefA=pivotRefB;
						pivotRefB=store;

						store=pivotA;
						pivotA=pivotB;
						pivotB=store;
					}
				}
			}

//~			if(I32(m_fragPivotRefA.size())<=fragmentIndex)
//~			{
//~				m_fragPivotRefA.resize(fragmentIndex+1);
//~				m_fragPivotRefB.resize(fragmentIndex+1);
//~				m_fragPivotRefC.resize(fragmentIndex+1);
//~				m_fragPivotA.resize(fragmentIndex+1);
//~				m_fragPivotB.resize(fragmentIndex+1);
//~				m_fragPivotC.resize(fragmentIndex+1);
//~				m_fragLength.resize(fragmentIndex+1);
//~				m_fragFarIndex.resize(fragmentIndex+1);
//~			}

			m_fragPivotRefA[fragmentIndex]=pivotRefA;
			m_fragPivotRefB[fragmentIndex]=pivotRefB;
			m_fragPivotRefC[fragmentIndex]=pivotRefC;
			m_fragPivotA[fragmentIndex]=pivotA;
			m_fragPivotB[fragmentIndex]=pivotB;
			m_fragPivotC[fragmentIndex]=pivotC;
			m_fragLength[fragmentIndex]=sqrt(farDistance2);

			if(pivotMethod=="du" || pivotMethod=="normal")
			{
				m_fragFarIndex[fragmentIndex]=farIndex;
			}
		}
	}

#if FE_HGO_PROFILE
	m_spProfilePartition->replace(m_spProfilePivots);
#endif

	if(driverPartitioned)
	{
		std::map<String,String> restrictionMap;

		String buffer=catalog<String>("restrictions");
		while(!buffer.empty())
		{
			String phrase=buffer.parse();
			String token=phrase.parse("\"","=");
			String value=phrase.parse("\"","=");

			//* always include self
			value+=(value.empty()? "": " ")+token;

			restrictionMap[token]=value;
		}

		const I32 driverPartCount=m_spDriverRef->partitionCount();

		std::map< String,sp<PartitionI> > commonPartitionMap;

		for(I32 driverPartIndex=0;driverPartIndex<driverPartCount;
				driverPartIndex++)
		{
			const String driverPartName=
					m_spDriverRef->partitionName(driverPartIndex);

			sp<PartitionI>& rspColliderPartition=
					m_colliderPartitionMap[driverPartName];

			rspColliderPartition=m_spCollider->createPartition();

			if(restrictionMap.find(driverPartName)==restrictionMap.end())
			{
				rspColliderPartition->select(".*");
				continue;
			}

			rspColliderPartition->select("");

			sp<PartitionI>& rspTemplate=
					commonPartitionMap[driverPartName];

			if(rebuildPartitionMap)
			{
				rspTemplate=m_spOutput->createPartition();
				rspTemplate->select("");
			}

			String restriction=restrictionMap[driverPartName];
			while(!restriction.empty())
			{
				String onePartName=restriction.parse();

				rspColliderPartition->add(onePartName);

				if(rspTemplate.isValid())
				{
					for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;
							fragmentIndex++)
					{
						const String fragmentName=
								m_nameOfFragment[fragmentIndex];
						const String thisDriver=
								m_driverOfFragment[fragmentIndex];

						if(thisDriver==onePartName)
						{
							rspTemplate->add(fragmentName);
						}
					}
				}
			}
		}

		if(rebuildPartitionMap)
		{
			for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
			{
				const String fragmentName=m_nameOfFragment[fragmentIndex];

				sp<PartitionI>& rspPartition=
						m_partitionMap[fragmentName];

				if(rspPartition.isNull())
				{
					const String thisDriver=m_driverOfFragment[fragmentIndex];

					sp<PartitionI>& rspTemplate=
							commonPartitionMap[thisDriver];

					if(rspTemplate.isValid())
					{
						rspPartition=rspTemplate->clone();
					}
					else
					{
						rspPartition=m_spOutput->createPartition();
						rspPartition->select(".*");
					}

					rspPartition->remove(fragmentName);
				}
			}
		}
	}
	else if(rebuildPartitionMap)
	{
		for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
		{
			const String fragmentName=m_nameOfFragment[fragmentIndex];

			sp<PartitionI>& rspPartition=
					m_partitionMap[fragmentName];

			if(rspPartition.isNull())
			{
				rspPartition=m_spOutput->createPartition();

#if FALSE
				//* regex of 'all but fragmentName'
				rspPartition->select(".*(?<!"+fragmentName+")$");
#else
				rspPartition->select(".*");
				rspPartition->remove(fragmentName);
#endif
			}
		}
	}

#if FE_HGO_PROFILE
	m_spProfileLoop->replace(m_spProfilePartition);
#endif

	const I32 totalPasses=passes+loft;
	for(I32 pass=0;pass<totalPasses;pass++)
	{
		m_passage=loft?
				(pass>0? (pass-1)/Real(passes-1): Real(0)):
				((passes>1)? pass/Real(passes-1): Real(0));

		//* rebuild output search since it may have changed
		if((dynamic || (loft && pass==1)) && pass && fragmentCollide)
		{
			m_spOutput=NULL;	//* avoid any caching

			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				spOutputPoint->set(pointIndex,m_outputCache[pointIndex]);
			}
		}

		if(m_spOutput.isNull())
		{
			access(m_spOutput,m_spOutputAccessible);
			if(m_fragmented)
			{
				m_spOutput->partitionWith(fragmentAttr);
			}
			m_spOutput->setRefinement(0);
			m_spOutput->setTriangulation(SurfaceI::e_existingPoints);
			m_spOutput->prepareForSearch();
			if(m_fragmented)
			{
				m_spOutput->setPartitionFilter(".*");
			}
		}
		else if(!loft && !pass && fragmentCollide)
		{
			m_spOutput->prepareForSearch();
		}

#if FE_HGO_DEBUG
		feLog("HingeOp::handle fragmentCount %d\n",fragmentCount);
#endif

		//* NOTE skip collision on first pass if lofting is used
		const BWORD doCollision=(!loft || pass);

		const U32 colliderCount=(doCollision && fragmentCollide)? 2: 1;
		for(U32 colliderIndex=0;colliderIndex<colliderCount;colliderIndex++)
		{
#if FALSE
			m_spObstacle=
					(colliderIndex==colliderCount-1)? m_spCollider: m_spOutput;
#else
			m_spObstacle=colliderIndex? m_spOutput: m_spCollider;
#endif

			if(m_spObstacle.isNull())
			{
				continue;
			}

//			feLog(">>>> pass %d/%d collider %d\n",pass,passes,colliderIndex);

			m_selfCollide=(m_spObstacle==m_spOutput);

			m_replaceHits=(dynamic || (inputReplaced && pass==loft));

#if FE_HGO_PROFILE_RUN
			feLog("HingeOp::handle pass %d replace %d intercollide %d\n",
					pass,m_replaceHits,m_selfCollide);
#endif

#if FE_HGO_PROFILE
			m_spProfileRun->replace(m_spProfileLoop);
#endif

			if(doCollision)
			{
				if(showPass<0 || showPass==pass)
				{
					m_spDrawGuide=spDrawGuide;
				}

				adjustThreads();
				const I32 threadCount=catalog<I32>("Threads");

				if(threadCount>1)
				{
					OperatorThreaded::handle(a_rSignal);
				}
				else
				{
					//* serial
					run(-1,fullRange());
				}

				m_spDrawGuide=NULL;

			}

			//* avoid unintentional caching
			m_spObstacle=NULL;

#if FE_HGO_PROFILE
			m_spProfilePush->replace(m_spProfileRun);
#endif

			for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
			{
				sp<SpannedRange>& rspFragmentRange=
						fragSpannedRange[fragmentIndex];

				sp<SurfaceAccessibleI::FilterI>& rspFilter=
						fragFilter[fragmentIndex];
				const I32 filterCount=rspFilter.isValid()?
						rspFilter->filterCount(): primitiveCount;

#if FE_HGO_FRAGMENT_VERBOSE
				if(FE_HGO_FRAGMENT_CONDITION)
				{
					feLog("fragment %d/%d \"%s\" range %s\n",
							fragmentIndex,fragmentCount,
							m_nameOfFragment[fragmentIndex].c_str(),
							c_print(rspFragmentRange));
				}
#endif

				if(m_spDriverRef.isValid() &&
						(pivotMethod=="du" || pivotMethod=="normal"))
				{
					pivotA=m_fragPivotA[fragmentIndex];
					pivotB=m_fragPivotB[fragmentIndex];
					pivotC=m_fragPivotC[fragmentIndex];
				}

				const SpatialVector midpoint=0.5*(pivotA+pivotB);
				const SpatialVector axis=unitSafe(pivotB-pivotA);

				if(drawPivot && !pass &&
						m_selfCollide && spDrawGuide.isValid())
				{
					BWORD doDraw=TRUE;

					if(m_fragmented)
					{
						const String fragmentName=
								m_nameOfFragment[fragmentIndex];

						if(isolate && fragmentName!=isolateFragment)
						{
							doDraw=FALSE;
						}
					}

					if(doDraw)
					{
//						sp<DrawableI> spDrawable(m_spObstacle);
//						if(spDrawable.isValid())
//						{
//							spDrawable->draw(spDrawGuide,NULL);
//						}

						const Color red(1.0,0.0,0.0);
						const Color green(0.0,1.0,0.0);
						const Color blue(0.0,0.0,1.0);

						Color colors[2];
						colors[0]=green;
						colors[1]=blue;

						SpatialVector line[2];
						line[0]=pivotA;
						line[1]=pivotB;

						spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
								TRUE,colors);

						if(twist)
						{
							colors[0]=green;
							colors[1]=red;

							line[0]=midpoint;
							line[1]=pivotC;

							spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
									TRUE,colors);
						}
					}
				}

#if FE_HGO_PROFILE
				m_spProfilePull->replace(m_spProfilePush);
#endif

				if(soft)
				{
					const Real fragLength=m_fragLength[fragmentIndex];

					const U32 iterations=1;	//* TODO param
					for(U32 iteration=0;iteration<iterations;iteration++)
					{
						BWORD changed=FALSE;

						for(I32 filterIndex=0;filterIndex<filterCount;
								filterIndex++)
						{
							if(interrupted())
							{
								changed=FALSE;
								break;
							}

							const U32 primitiveIndex=m_fragmented?
									rspFilter->filter(filterIndex): filterIndex;

							const U32 subCount=
									spOutputVertices->subCount(primitiveIndex);
							for(U32 subIndex=0;subIndex<subCount;subIndex++)
							{
								//* TODO cache lookups

								const U32 subIndexB=(subIndex+1)%subCount;

								const U32 pointIndexA=
										spOutputVertices->integer(
										primitiveIndex,subIndex);
								const U32 pointIndexB=
										spOutputVertices->integer(
										primitiveIndex,subIndexB);

								Real angleA=m_totalAngle[pointIndexA]+
										m_pointAngle[pointIndexA];
								Real angleB=m_totalAngle[pointIndexB]+
										m_pointAngle[pointIndexB];

								if(fabs(angleA)<1e-6 && fabs(angleB)<1e-6)
								{
									continue;
								}

								const SpatialVector pointA=
										m_inputCache[pointIndexA];
								const SpatialVector pointB=
										m_inputCache[pointIndexB];

								const Real edgeLength2=
										magnitudeSquared(pointB-pointA);
								if(edgeLength2<1e-6)
								{
									continue;
								}

								const Real edgeLength=sqrtf(edgeLength2);
								const Real limit=softness*edgeLength/fragLength;

#if FE_HGO_FRAGMENT_VERBOSE
								if(FE_HGO_FRAGMENT_CONDITION)
								{
									feLog("soft %d/%d vert %d:%d point %d %d"
											" angle %.6G %.6G limit %.6G\n",
											iteration,iterations,
											primitiveIndex,subIndex,
											pointIndexA,pointIndexB,
											angleA,angleB,limit);
									feLog("  softness %.6G edgeLength %.6G"
											" fragLength[%d] %.6G%s\n",
											softness,edgeLength,
											fragmentIndex,fragLength,
											(pointIndexA==m_selectIndex ||
											pointIndexB==m_selectIndex)?
											" [31m<<<<[0m": "");
								}
#endif

								const Real difference=fabs(angleB-angleA);
								if(difference>limit)
								{
									const Real tweak=difference-limit;

#if FALSE
									//* simple method: meet halfway
									if(angleB>angleA)
									{
										angleA+=0.5*tweak;
										angleB-=0.5*tweak;
									}
									else
									{
										angleA-=0.5*tweak;
										angleB+=0.5*tweak;
									}
#else
									if(angleB>angleA)
									{
										if(angleA>0.0)
										{
											//* both positive
											angleA+=tweak;
#if FE_HGO_FRAGMENT_VERBOSE
											if(FE_HGO_FRAGMENT_CONDITION)
											{
												feLog("angleA += %.6G\n",
														tweak);
											}
#endif
										}
										else if(angleB<0.0)
										{
											//* both negative
											angleB-=tweak;
#if FE_HGO_FRAGMENT_VERBOSE
											if(FE_HGO_FRAGMENT_CONDITION)
											{
												feLog("angleB -= %.6G\n",
														tweak);
											}
#endif
										}
										else
										{
											const Real changeA=fabs(angleB)/
													(fabs(angleA)+
													fabs(angleB));

											angleA+=changeA*tweak;
											angleB-=(1.0-changeA)*tweak;
#if FE_HGO_FRAGMENT_VERBOSE
											if(FE_HGO_FRAGMENT_CONDITION)
											{
												feLog("angleA += %.6G*%.6G\n",
														changeA,tweak);
												feLog("angleB -= %.6G*%.6G\n",
														(1.0-changeA),tweak);
											}
#endif
										}
									}
									else //* (angleA>angleB)
									{
										if(angleB>0.0)
										{
											//* both positive
											angleB+=tweak;
#if FE_HGO_FRAGMENT_VERBOSE
											if(FE_HGO_FRAGMENT_CONDITION)
											{
												feLog("angleB += %.6G\n",
														tweak);
											}
#endif
										}
										else if(angleA<0.0)
										{
											//* both negative
											angleA-=tweak;
#if FE_HGO_FRAGMENT_VERBOSE
											if(FE_HGO_FRAGMENT_CONDITION)
											{
												feLog("angleA -= %.6G\n",
														tweak);
											}
#endif
										}
										else
										{
											const Real changeB=fabs(angleA)/
													(fabs(angleA)+
													fabs(angleB));

											angleB+=changeB*tweak;
											angleA-=(1.0-changeB)*tweak;
#if FE_HGO_FRAGMENT_VERBOSE
											if(FE_HGO_FRAGMENT_CONDITION)
											{
												feLog("angleB += %.6G*%.6G\n",
														changeB,tweak);
												feLog("angleA -= %.6G*%.6G\n",
														(1.0-changeB),tweak);
											}
#endif
										}
									}
#endif
									m_pointAngle[pointIndexA]=
											angleA-m_totalAngle[pointIndexA];
									m_pointAngle[pointIndexB]=
											angleB-m_totalAngle[pointIndexB];
									changed=TRUE;
								}
							}
						}

						if(!changed)
						{
							break;
						}
					}
				}

				if(conform)
				{
					std::set<I32> pointSet;

					for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
					{
						if(interrupted())
						{
							break;
						}

						const U32 primitiveIndex=m_fragmented?
								rspFilter->filter(filterIndex): filterIndex;

						const U32 subCount=
								spOutputVertices->subCount(primitiveIndex);

						for(U32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const I32 pointIndex=spOutputVertices->integer(
									primitiveIndex,subIndex);

							pointSet.insert(pointIndex);
						}
					}

					Real angleSum=0.0;
					I32 angleCount=0;

					for(std::set<I32>::iterator it=pointSet.begin();
							it!=pointSet.end();it++)
					{
						const I32 pointIndex= *it;
						angleSum+=m_pointAngle[pointIndex];
						angleCount++;
					}

					const Real angleAverage=angleSum/Real(angleCount);

					for(std::set<I32>::iterator it=pointSet.begin();
							it!=pointSet.end();it++)
					{
						const I32 pointIndex= *it;
						Real& rPointAngle=m_pointAngle[pointIndex];

						rPointAngle-=conformity*(rPointAngle-angleAverage);
					}
				}

#if FE_HGO_PROFILE
				m_spProfileRotate->replace(m_spProfilePull);
#endif

				Real maxAngle=0.0;
				for(SpannedRange::Iterator it=rspFragmentRange->begin();
						!it.atEnd();it.step())
				{
					const U32 pointIndex=it.value();

					const Real angle=m_pointAngle[pointIndex];
					if(fabs(maxAngle)<fabs(angle))
					{
						maxAngle=angle;
					}
				}

//				feLog("maxAngle %.6G\n",maxAngle);

				if(maxAngle!=0.0)
				{
					SpatialTransform conversion;
					SpatialTransform twistTransform;

					if(!soft)
					{
						calcConversion(conversion,rate*maxAngle,axis,midpoint);
					}

					if(twist)
					{
						const I32 farIndex=m_fragFarIndex[fragmentIndex];

						const SpatialVector farPoint=(farIndex>=0)?
								m_outputCache[farIndex]: pivotC;

						const SpatialVector crossAxis=
								unitSafe(farPoint-midpoint);

						Real maxArm=0;
						Real spinMin=0.0;
						Real spinMax=0.0;

						for(SpannedRange::Iterator it=rspFragmentRange->begin();
								!it.atEnd();it.step())
						{
							const U32 pointIndex=it.value();

							const SpatialVector point=m_outputCache[pointIndex];

							const SpatialVector toPoint=point-midpoint;
							const Real alongCrossAxis=dot(crossAxis,toPoint);

							const SpatialVector onCrossAxis=
									midpoint+alongCrossAxis*crossAxis;

							const SpatialVector arm=point-onCrossAxis;
							const Real length=magnitude(arm);
							if(length<1e-6)
							{
								continue;
							}

							const Real aligned=dot(axis,arm);
							const Real absAligned=fabs(aligned);

							if(maxArm<absAligned)
							{
								maxArm=absAligned;
							}

							SpatialTransform oneConversion;
							calcConversion(oneConversion,
									rate*m_pointAngle[pointIndex],
									axis,midpoint);

							SpatialVector transformed;
							transformVector(oneConversion,point,transformed);

							const SpatialVector arm2=transformed-onCrossAxis;

							SpatialQuaternion rotation;
							set(rotation,unitSafe(arm),unitSafe(arm2));

							Real radians;
							SpatialVector rotAxis;
							rotation.computeAngleAxis(radians,rotAxis);

							//* NOTE spin is an arc length, not an angle
							const Real spin= -aligned*radians;

//							feLog("angle %.6G axis %s arm %s arm2 %s"
//									" radians %.6G spin %.6G\n",
//									m_pointAngle[pointIndex],c_print(axis),
//									c_print(arm),c_print(arm2),radians,spin);

							spinMin=fe::minimum(spinMin,spin);
							spinMax=fe::maximum(spinMax,spin);
						}

						if(maxArm>0.0)
						{
							const Real averageSpin=0.5*(spinMax+spinMin);
							const Real angle=twistiness*averageSpin/maxArm;

//							feLog("spinMin %.6G spinMax %.6G"
//									" maxArm %.6G angle %.6G\n",
//									spinMin,spinMax,maxArm,angle/degToRad);

							const SpatialVector yAxis(0.0,1.0,0.0);
							const SpatialQuaternion rotation(angle,yAxis);

							SpatialTransform xformMidpoint;
							makeFrameNormalY(xformMidpoint,
									midpoint,axis,crossAxis);

							SpatialTransform invMidpoint;
							invert(invMidpoint,xformMidpoint);

							twistTransform=invMidpoint*
									SpatialTransform(rotation)*xformMidpoint;

							if(!soft)
							{
								conversion*=twistTransform;
							}
						}
					}

#if FE_HGO_FRAGMENT_VERBOSE
					if(FE_HGO_FRAGMENT_CONDITION)
					{
						feLog("conversion\n%s\n",c_print(conversion));
					}
#endif

					//* rotate all points
					for(SpannedRange::Iterator it=rspFragmentRange->begin();
							!it.atEnd();it.step())
					{
						const U32 pointIndex=it.value();

						if(soft)
						{
							calcConversion(conversion,
									rate*m_pointAngle[pointIndex],
									axis,midpoint);

							if(twist)
							{
								conversion*=twistTransform;
							}
						}

						const SpatialVector point=m_outputCache[pointIndex];

						SpatialVector transformed;
						transformVector(conversion,point,transformed);

						m_outputCache[pointIndex]=transformed;
					}
				}

				for(SpannedRange::Iterator it=rspFragmentRange->begin();
						!it.atEnd();it.step())
				{
					const U32 pointIndex=it.value();

					m_totalAngle[pointIndex]+=m_pointAngle[pointIndex];
					m_lastAngle[pointIndex]=m_pointAngle[pointIndex];
					m_pointAngle[pointIndex]=0.0;
				}

#if FE_HGO_PROFILE
				m_spProfilePush->replace(m_spProfileRotate);
#endif
			}
#if FE_HGO_PROFILE
			m_spProfileLoop->replace(m_spProfilePush);
#endif
		}
	}

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		spOutputPoint->set(pointIndex,m_outputCache[pointIndex]);
	}

	if(m_fragmented && isolate)
	{
		for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
		{
			sp<SurfaceAccessibleI::FilterI>& rspFilter=
					fragFilter[fragmentIndex];

			const String fragmentName=m_nameOfFragment[fragmentIndex];
			if(fragmentName==isolateFragment)
			{
				continue;
			}

			I32 filterCount=primitiveCount;

			filterCount=rspFilter->filterCount();
			if(!filterCount)
			{
				continue;
			}

			std::set<I32> pointSet;

			const I32 rootIndex=rspFilter->filter(0);
			const SpatialVector root=
					spOutputVertices->spatialVector(rootIndex,0);

			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=rspFilter->filter(filterIndex);
				const U32 subCount=spInputVertices->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=
							spOutputVertices->integer(reIndex,subIndex);
					if(pointSet.find(pointIndex)!=pointSet.end())
					{
						continue;
					}
					pointSet.insert(pointIndex);

					const SpatialVector point=
							spOutputVertices->spatialVector(reIndex,subIndex);
					spOutputVertices->set(reIndex,subIndex,
							root+0.2*(point-root));
				}
			}
		}
	}

	//* prepare for brush
	if(!transient && passes>1)
	{
		m_spOutput=NULL;	//* avoid any caching

		access(m_spOutput,m_spOutputAccessible);
		if(m_fragmented)
		{
			m_spOutput->partitionWith(fragmentAttr);
		}
		m_spOutput->setRefinement(0);
		m_spOutput->setTriangulation(SurfaceI::e_existingPoints);
		m_spOutput->prepareForSample();
		if(m_fragmented)
		{
			m_spOutput->setPartitionFilter(".*");
		}
	}

#if FE_HGO_DEBUG
	feLog("HingeOp::handle done\n");
#endif

#if FE_HGO_PROFILE
	m_spProfileLoop->finish();
	m_spProfiler->end();

	feLog("%s:\n%s\n",m_spProfiler->name().c_str(),
			m_spProfiler->report().c_str());
#endif
}

void HingeOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
#if FE_HGO_PROFILE_RUN
	sp<Profiler> spRunProfiler(new Profiler("HingeOp::run"));
	sp<Profiler::Profile> spRunProfilePrep
			(new Profiler::Profile(spRunProfiler,"Prep"));
	sp<Profiler::Profile> spRunProfilePointLoop
			(new Profiler::Profile(spRunProfiler,"PointLoop"));
	sp<Profiler::Profile> spRunProfilePartition
			(new Profiler::Profile(spRunProfiler,"Partition"));
	sp<Profiler::Profile> spRunProfileSearch
			(new Profiler::Profile(spRunProfiler,"Search"));
	sp<Profiler::Profile> spRunProfileHitLoop
			(new Profiler::Profile(spRunProfiler,"HitLoop"));
	sp<Profiler::Profile> spRunProfileImpact
			(new Profiler::Profile(spRunProfiler,"Impact"));
	sp<Profiler::Profile> spRunProfileCompare
			(new Profiler::Profile(spRunProfiler,"Compare"));
	sp<Profiler::Profile> spRunProfileEval
			(new Profiler::Profile(spRunProfiler,"Eval"));
	sp<Profiler::Profile> spRunProfileAngleAxis
			(new Profiler::Profile(spRunProfiler,"AngleAxis"));
	sp<Profiler::Profile> spRunProfileRotation
			(new Profiler::Profile(spRunProfiler,"Rotation"));
	sp<Profiler::Profile> spRunProfileResolve
			(new Profiler::Profile(spRunProfiler,"Resolve"));

	spRunProfiler->begin();
	spRunProfilePrep->start();
#endif

	const String collideMethod=catalog<String>("collideMethod");
	const BWORD dynamic=catalog<bool>("dynamic");
	const Real threshold=catalog<Real>("threshold");
	const Real thresholdFactor=catalog<Real>("thresholdFactor");
	const Real deadRadius=catalog<Real>("deadRadius");
	const Real deadRadiusFactor=catalog<Real>("deadRadiusFactor");
	const Real liveRadius=catalog<Real>("liveRadius");
	const Real liveRadiusFactor=catalog<Real>("liveRadiusFactor");
	const Real radianLimit=catalog<Real>("angleLimit")*degToRad;
	const Real attractiveness=catalog<Real>("attractiveness");
	const BWORD attract=(!m_selfCollide &&
			attractiveness>0.0 && catalog<bool>("attract"));

	//* TODO separate adjustments
	const I32 hitLimit=m_selfCollide? catalog<I32>("hitLimit"): 1;

	const BWORD useNearest=(collideMethod=="nearest");

	sp<PartitionI> spPartition(NULL);
	I32 lastFragmentIndex= -1;
	String fragmentName;

	const Real transition=pow(m_passage,2.0);
	const Real allowDown=0.5+0.5*transition;
	const Real allowUp=Real(1)-0.5*transition;

#if FE_HGO_PROFILE_RUN
	spRunProfilePointLoop->replace(spRunProfilePrep);
#endif

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			return;
		}

		const I32 pointIndex=it.value();

#if FE_HGO_POINT_VERBOSE
		BWORD lowSet=FALSE;
		BWORD highSet=FALSE;
#endif

		const Real oldAngle=m_pointAngle[pointIndex];

		Real lowAngle=fe::minimum(oldAngle,Real(0));
		Real highAngle=fe::maximum(oldAngle,Real(0));

		const I32 fragmentIndex=
				m_fragmented? m_fragmentOfPoint[pointIndex]: 0;
		FEASSERT(fragmentIndex<I32(m_fragPivotA.size()));

		const SpatialVector pivotA=m_fragPivotA[fragmentIndex];
		const SpatialVector pivotB=m_fragPivotB[fragmentIndex];
		const SpatialVector axis=unitSafe(pivotB-pivotA);

		const BWORD pivotRef=(m_fragPivotRefA.size()>0);
		const SpatialVector pivotRefA=
				pivotRef? m_fragPivotRefA[fragmentIndex]: pivotA;
		const SpatialVector pivotRefB=
				pivotRef? m_fragPivotRefB[fragmentIndex]: pivotB;
		const SpatialVector pivotRefC=
				pivotRef? m_fragPivotRefC[fragmentIndex]:
				m_fragPivotC[fragmentIndex];
		const SpatialVector midpointRef=0.5*(pivotRefA+pivotRefB);
		const SpatialVector midlineRef=unitSafe(pivotRefC-midpointRef);

		const SpatialVector point=m_outputCache[pointIndex];

#if FE_HGO_POINT_VERBOSE
		if(FE_HGO_POINT_CONDITION)
		{
			feLog("\npoint %d %s output %d fragment %d\n",
					pointIndex,c_print(point),
					m_selfCollide,fragmentIndex);
		}
#endif

		const Real along=dot(point-pivotA,axis);
		const SpatialVector pivot=pivotA+axis*along;

		const SpatialVector from=point-pivot;
		const Real radius=magnitude(from);

		const Real deadRadiusMod=
				deadRadius+deadRadiusFactor*m_fragLength[fragmentIndex];

		//* TODO check using ref
		if(radius<=0.0 || radius<=deadRadiusMod)
		{
			continue;
		}

		const Real liveRadiusMod=
				liveRadius+liveRadiusFactor*m_fragLength[fragmentIndex];

		const SpatialVector fromUnit=from*(1.0/radius);
		const SpatialVector fromSide=cross(axis,fromUnit);
		const SpatialVector rayDir=unitSafe(fromSide);

		//* TODO tweak
		const Real maxDistance=m_fragLength[fragmentIndex]*
				(m_selfCollide? (dynamic? 0.3: 1.0): 1.5);

		//* TODO ramp param
		Real thresholdMod=threshold+thresholdFactor*m_fragLength[fragmentIndex];
		if(!m_selfCollide)
		{
			//* HACK procedural ramp
//			thresholdMod*=Real(3)*pow(radius/m_fragLength[fragmentIndex],0.8);
			thresholdMod*=radius/m_fragLength[fragmentIndex];
		}

#if FE_HGO_POINT_VERBOSE
		if(FE_HGO_POINT_CONDITION)
		{
			feLog("rayDir %s axis %s\n",c_print(rayDir),c_print(axis));
			feLog("fragLength %.6G thresholdMod %.6G\n",
					m_fragLength[fragmentIndex],thresholdMod);
		}
#endif

#if FE_HGO_PROFILE_RUN
		spRunProfilePartition->replace(spRunProfilePointLoop);
#endif

		if(m_fragmented && lastFragmentIndex!=fragmentIndex)
		{
			if(m_selfCollide)
			{
				const String fragmentName=m_nameOfFragment[fragmentIndex];

				spPartition=m_partitionMap[fragmentName];
				lastFragmentIndex=fragmentIndex;
			}
			else
			{
				const String fragmentName=m_nameOfFragment[fragmentIndex];
				const String driverPartName=m_driverOfFragment[fragmentIndex];
				spPartition=m_colliderPartitionMap[driverPartName];

#if FE_HGO_POINT_VERBOSE
				if(FE_HGO_POINT_CONDITION)
				{
					feLog("driver part \"%s\"\n",driverPartName.c_str());
				}
#endif

			}
		}

#if FE_HGO_PROFILE_RUN
		spRunProfileSearch->replace(spRunProfilePartition);
#endif

		Array< sp<SurfaceI::ImpactI> > impactArray;

		Array<Hit>& rHitArray=m_selfCollide?
				m_outputHitTable[pointIndex]:
				m_colliderHitTable[pointIndex];

#if FE_HGO_POINT_VERBOSE
		if(FE_HGO_POINT_CONDITION)
		{
			feLog("impactArray size %d partition %d\n",
					impactArray.size(),spPartition.isValid());
		}
#endif

		const SpatialVector rayOrigin=
				useNearest? point: point-thresholdMod*rayDir;

		if(m_replaceHits)
		{
			if(useNearest)
			{
				impactArray=m_spObstacle->nearestPoints(
						rayOrigin,maxDistance,hitLimit,spPartition);

#if FALSE
				if(m_selfCollide)
				{
					std::set<I32> partitionSet;

					I32 impactCount=impactArray.size();
					for(I32 impactIndex=0;impactIndex<impactCount;impactIndex++)
					{
						sp<SurfaceI::ImpactI> spImpact=impactArray[impactIndex];
						const I32 partitionIndex=
								spImpact->partitionIndex();

						//* NOTE only keep one hit per neighbor partition

						if(partitionSet.find(partitionIndex)==partitionSet.end())
						{
							partitionSet.insert(partitionIndex);
							continue;
						}

						for(I32 moveIndex=impactIndex;
								moveIndex<impactCount-1;moveIndex++)
						{
							impactArray[moveIndex]=impactArray[moveIndex+1];
						}
						impactArray[--impactCount]=NULL;
						impactIndex--;
					}
					impactArray.resize(impactCount);
				}
#endif
			}
			else
			{
				impactArray=m_spObstacle->rayImpacts(
						rayOrigin,rayDir,maxDistance,hitLimit);
			}
		}

		U32 hitCount;

		if(m_replaceHits)
		{
			hitCount=impactArray.size();
			rHitArray.resize(hitCount);
		}
		else
		{
			hitCount=rHitArray.size();
		}

		const U32 hitStart=useNearest? 0: hitCount-1;

#if FE_HGO_POINT_VERBOSE
		if(FE_HGO_POINT_CONDITION)
		{
			feLog("hitCount %d\n",hitCount);
		}
#endif

#if FE_HGO_PROFILE_RUN
		spRunProfileHitLoop->replace(spRunProfileSearch);
#endif

		BWORD pushBack=FALSE;
		I32 lastPartitionIndex= -1;

		for(U32 hitIndex=hitStart;hitIndex<hitCount;hitIndex++)
		{
			Hit& rHit=rHitArray[hitIndex];

#if FE_HGO_PROFILE_RUN
			spRunProfileImpact->replace(spRunProfileHitLoop);
#endif

#if FE_HGO_POINT_VERBOSE
			I32 primitiveIndex= -1;
#endif

			I32 partitionIndex;
			SpatialVector intersection;
			Real distance;

			if(m_replaceHits)
			{
				sp<SurfaceI::ImpactI> spImpact=impactArray[hitIndex];

				partitionIndex=spImpact->partitionIndex();

#if FE_HGO_POINT_VERBOSE
				primitiveIndex=spImpact->primitiveIndex();
#endif

				sp<SurfaceTriangles::Impact> spTriangleImpact=spImpact;

				if(!dynamic && m_selfCollide && spTriangleImpact.isValid())
				{
					const SpatialBary bary=spImpact->barycenter();
					const I32 pointIndex0=spTriangleImpact->pointIndex0();
					const I32 pointIndex1=spTriangleImpact->pointIndex1();
					const I32 pointIndex2=spTriangleImpact->pointIndex2();

					rHit.m_barycenter=bary;
					rHit.m_pointIndex0=pointIndex0;
					rHit.m_pointIndex1=pointIndex1;
					rHit.m_pointIndex2=pointIndex2;

					if(pointIndex0>=0 && pointIndex1>=0 && pointIndex2>=0)
					{
						const SpatialVector point0=m_outputCache[pointIndex0];
						const SpatialVector point1=m_outputCache[pointIndex1];
						const SpatialVector point2=m_outputCache[pointIndex2];
						intersection=location(bary,point0,point1,point2);
					}
					else
					{
						intersection=spImpact->intersection();
					}
					distance=magnitude(intersection-point);
				}
				else
				{
					rHit.m_pointIndex0= -1;

					intersection=spImpact->intersection();
					distance=spImpact->distance();
				}

				if(!dynamic)
				{
					rHit.m_partitionIndex=partitionIndex;
					rHit.m_intersection=intersection;
				}
			}
			else
			{
				partitionIndex=rHit.m_partitionIndex;

				const I32 pointIndex0=rHit.m_pointIndex0;
				const I32 pointIndex1=rHit.m_pointIndex1;
				const I32 pointIndex2=rHit.m_pointIndex2;

				if(pointIndex0>=0 && pointIndex1>=0 && pointIndex2>=0)
				{
					const SpatialVector point0=m_outputCache[pointIndex0];
					const SpatialVector point1=m_outputCache[pointIndex1];
					const SpatialVector point2=m_outputCache[pointIndex2];

#if FALSE
					const SpatialBary bary=rHit.m_barycenter;
					intersection=location(bary,point0,point1,point2);
#else
					//* recompute barycenter
					SpatialVector direction;
					SpatialBary newBary;
					distance=PointTriangleNearest<Real>::solve(
							point0,point1,point2,
							rayOrigin,direction,intersection,newBary);
#endif
				}
				else
				{
					intersection=rHit.m_intersection;
				}

				distance=magnitude(intersection-point);
			}

#if FE_HGO_PROFILE_RUN
			spRunProfileCompare->replace(spRunProfileImpact);
#endif

			if(m_selfCollide && partitionIndex>=0)
			{
				if(lastPartitionIndex!=partitionIndex)
				{
					lastPartitionIndex=partitionIndex;
					pushBack=FALSE;

					const I32 otherFragmentIndex=
							m_fragmentOfPartition[partitionIndex];

					const SpatialVector otherPivotRefA=pivotRef?
							m_fragPivotRefA[otherFragmentIndex]:
							m_fragPivotA[otherFragmentIndex];
					const SpatialVector otherPivotRefB=pivotRef?
							m_fragPivotRefB[otherFragmentIndex]:
							m_fragPivotB[otherFragmentIndex];
					const SpatialVector otherPivotRefC=pivotRef?
							m_fragPivotRefC[otherFragmentIndex]:
							m_fragPivotC[otherFragmentIndex];
					const SpatialVector otherMidpointRef=
							0.5*(otherPivotRefA+otherPivotRefB);
					const SpatialVector otherMidlineRef=
							unitSafe(otherPivotRefC-otherMidpointRef);

//					feLog("%d hit %d/%d part %d frag %d mid %s other %s\n",
//							pointIndex,hitIndex,hitCount,
//							partitionIndex,fragmentIndex,
//							c_print(midpointRef),c_print(otherMidpointRef));

					//* smaller angle with connecting line is "higher"
					const SpatialVector between=
							unitSafe(otherMidpointRef-midpointRef);
					const Real oneDot=dot(midlineRef,between);
					const Real otherDot=dot(otherMidlineRef,-between);
					if(oneDot<otherDot)
					{
						pushBack=TRUE;
					}

					//* TEMP test
					if(oneDot==otherDot)
					{
						feLog("HingeOp::run %d %d matching dot %.6G %.6G\n",
								fragmentIndex,otherFragmentIndex,
								oneDot,otherDot);
					}
				}
			}

#if FE_HGO_PROFILE_RUN
			spRunProfileEval->replace(spRunProfileCompare);
#endif

			const SpatialVector reAxis=pushBack? -axis: axis;
			const SpatialVector reFromSide=pushBack? -fromSide: fromSide;
			const SpatialVector reRayDir=pushBack? -rayDir: rayDir;

#if FE_HGO_POINT_VERBOSE
			if(FE_HGO_POINT_CONDITION)
			{
				feLog(">>>> intersect %s pushBack %d primitiveIndex %d\n",
						c_print(intersection),pushBack,primitiveIndex);
			}
#endif

			if(m_spDrawGuide.isValid() && pointIndex==m_selectIndex)
			{
				const Color blue(0.0,0.0,1.0);
				const Color red(1.0,0.0,0.0);
				const Color cyan(0.0,1.0,1.0);
				const Color yellow(1.0,1.0,0.3);

				Color colors[2];
				SpatialVector line[2];

				//* Pivot To Hit - blue red
				colors[0]=blue;
				line[0]=pivot;
				colors[1]=red;
				line[1]=intersection;

				m_spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,TRUE,colors);

				//* Threshold To Hit - cyan red
				colors[0]=cyan;
				line[0]=intersection+reRayDir*thresholdMod;
				colors[1]=red;
				line[1]=intersection;

				m_spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,TRUE,colors);

				//* Point To Hit - yellow red
				colors[0]=yellow;
				line[0]=point;
				colors[1]=red;
				line[1]=intersection;

				m_spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,TRUE,colors);
			}

			Real alignRay=1.0;
			Real alignArm=1.0;
			SpatialVector goal;

			if(useNearest)
			{
//				const SpatialVector toImpact=spImpact->direction();
//				const SpatialVector toImpact=
//						unitSafe(intersection-point+reRayDir*thresholdMod);
				const SpatialVector toImpact=
						intersection-point+reRayDir*thresholdMod;
				alignRay=dot(reRayDir,toImpact);

				if(attract)
				{
					alignRay=fabs(alignRay);
				}
				else
				{
					if(alignRay<0.0)
					{
#if FE_HGO_POINT_VERBOSE
					if(FE_HGO_POINT_CONDITION)
					{
						feLog("reRayDir %s toImpact %s\n",
								c_print(reRayDir),c_print(toImpact));
						feLog("--SKIP-- alignRay %.6G\n",alignRay);
					}
#endif
#if FE_HGO_PROFILE_RUN
						spRunProfileHitLoop->replace(spRunProfileEval);
#endif
						continue;
					}
				}

				//* normalize
				const Real alignMag=magnitude(toImpact);
				if(alignMag>1e-6)
				{
					alignRay*=Real(1)/magnitude(toImpact);
				}

				const SpatialVector hitArm=unitSafe(intersection-pivot);
#if FALSE
				alignArm=dot(fromUnit,hitArm);
#else
				const SpatialVector fromCross=cross(rayDir,fromUnit);
				const SpatialVector fromTurn=unitSafe(cross(fromCross,rayDir));

				const SpatialVector hitCross=cross(rayDir,hitArm);
				const SpatialVector hitTurn=unitSafe(cross(hitCross,rayDir));

				alignArm=dot(fromTurn,hitTurn);
#endif

				if(alignArm<0.0)
				{
#if FE_HGO_POINT_VERBOSE
					if(FE_HGO_POINT_CONDITION)
					{
						feLog("--SKIP-- alignArm %.6G fromTurn %s hitTurn %s\n",
								alignArm,c_print(fromTurn),c_print(hitTurn));
					}
#endif
#if FE_HGO_PROFILE_RUN
					spRunProfileHitLoop->replace(spRunProfileEval);
#endif
					continue;
				}

				if(attract)
				{
					const SpatialVector fullGoal=
							intersection+thresholdMod*reRayDir;
					goal=point+alignArm*(fullGoal-point);
				}
				else
				{
					const Real alignedDistance=alignRay*
							magnitude(intersection-point+reRayDir*thresholdMod);

					goal=rayOrigin+alignedDistance*reRayDir;
				}
			}
			else
			{
				goal=intersection+thresholdMod*reRayDir;
			}

			const SpatialVector change=goal-point;
			if(fabs(change[0])<1e-3 && fabs(change[1])<1e-3 &&
					fabs(change[2])<1e-3)
			{
#if FE_HGO_PROFILE_RUN
				spRunProfileHitLoop->replace(spRunProfileEval);
#endif
				continue;
			}

			const SpatialVector toGoal=goal-pivot;

			//* TODO not always fully locked to axis
			const SpatialVector fromLocked=unitSafe(cross(reFromSide,reAxis));
			const SpatialVector toSide=cross(reAxis,toGoal);
			const SpatialVector toLocked=unitSafe(cross(toSide,reAxis));

#if FE_HGO_POINT_VERBOSE
			if(FE_HGO_POINT_CONDITION)
			{
				feLog("reRayDir %s goal %s\n",
						c_print(reRayDir),c_print(goal));
				feLog("alignRay %.6G alignArm %.6G toSide %s \n",
						alignRay,alignArm,c_print(toSide));
				feLog("reAxis %s toGoal %s\n",
						c_print(reAxis),c_print(toGoal));
				feLog("++HIT++ from %s to %s\n",
						c_print(fromLocked),c_print(toLocked));
			}
#endif

#if FE_HGO_PROFILE_RUN
			spRunProfileAngleAxis->replace(spRunProfileEval);
#endif

#if FALSE
			SpatialQuaternion rotation;
			set(rotation,fromLocked,toLocked);

			Real rotAngle=0.0;
			SpatialVector rotAxis;
			rotation.computeAngleAxis(rotAngle,rotAxis);
#else
			Real rotAngle=acosf(dot(fromLocked,toLocked));
			SpatialVector rotAxis=cross(fromLocked,toLocked);
#endif

#if FE_HGO_PROFILE_RUN
			spRunProfileRotation->replace(spRunProfileAngleAxis);
#endif

			const Real dotAxis=dot(reAxis,rotAxis);
			if(dotAxis<0.0)
			{
				rotAngle= -rotAngle;
			}

			Real fade=1.0;
			if(radius<liveRadiusMod && liveRadiusMod>deadRadiusMod)
			{
				fade=fe::minimum(Real(1),
						(radius-deadRadiusMod)/(liveRadiusMod-deadRadiusMod));
			}

			if(m_selfCollide)
			{
//				fade*=pow(alignRay,4.0)*pow(alignArm,4.0);
				fade*=alignRay*alignRay*alignRay*alignRay*
						alignArm*alignArm*alignArm*alignArm;

				fade*=fe::maximum(Real(0),(Real(1)-distance/maxDistance));
			}

#if FE_HGO_POINT_VERBOSE
			if(FE_HGO_POINT_CONDITION)
			{
//				feLog("rotation %s\n",c_print(rotation));
				feLog("angle %.6G axis %s fade %.6G prev %.6G last %.6G\n",
						rotAngle,c_print(rotAxis),
						fade,m_pointAngle[pointIndex],m_lastAngle[pointIndex]);
			}
#endif

			rotAngle*=fade;

			const Real rotLimit=radianLimit-m_totalAngle[pointIndex];
			if(rotAngle>rotLimit)
			{
				rotAngle=rotLimit;
			}

			if(attract && rotAngle<0.0)
			{
				rotAngle*=attractiveness;

				rotAngle*=allowUp;

				if(m_lastAngle[pointIndex]<0.0)
				{
					rotAngle-=m_lastAngle[pointIndex];
					if(rotAngle>0.0)
					{
						rotAngle=0.0;
					}
				}

				if(lowAngle>rotAngle)
				{
					lowAngle=rotAngle;

#if FE_HGO_POINT_VERBOSE
					lowSet=TRUE;
#endif
				}
			}
			else if(pushBack)
			{
				rotAngle= -rotAngle;

				rotAngle*=allowDown;

				if(lowAngle>rotAngle)
				{
					lowAngle=rotAngle;

#if FE_HGO_POINT_VERBOSE
					lowSet=TRUE;
#endif
				}
			}
			else
			{
				if(rotAngle<0.0)
				{
					rotAngle=0.0;
				}

				rotAngle*=allowUp;

				if(highAngle<rotAngle)
				{
					highAngle=rotAngle;

#if FE_HGO_POINT_VERBOSE
					highSet=TRUE;
#endif
				}
			}

			if(m_spDrawGuide.isValid() && pointIndex==m_selectIndex)
			{
				const Color red(1.0,0.0,0.0);
				const Color blue(0.0,0.0,1.0);
				const Color green(0.0,1.0,0.0);
				const Color darkgreen(0.0,0.5,0.0);
				const Color yellow(1.0,1.0,0.3);
				const Color darkyellow(0.5,0.5,0.0);
				const Color white(1.0,1.0,1.0);

				const Real length=m_fragLength[fragmentIndex];

				//* From Angle - blue green
				Color colors[2];
				colors[0]=blue;
				colors[1]=m_selfCollide? green: darkgreen;

				SpatialVector line[2];
				line[0]=pivot;
				line[1]=pivot+fromLocked*length;

				m_spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,TRUE,colors);

				//* To Angle - blue yellow
				colors[1]=m_selfCollide? yellow: darkyellow;
				line[1]=pivot+toLocked*length;

				m_spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,TRUE,colors);

				//* Goal To Start - white yellow
				colors[0]=white;
				line[0]=goal;
//				line[1]=point;
//
//				m_spDrawGuide->drawLines(line,NULL,2,
//						DrawI::e_strip,TRUE,colors);

				//* Goal To Hit - white red
				colors[1]=red;
				line[1]=intersection;

				m_spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,TRUE,colors);
			}

#if FE_HGO_PROFILE_RUN
			spRunProfileHitLoop->replace(spRunProfileRotation);
#endif
		}

#if FE_HGO_PROFILE_RUN
		spRunProfileResolve->replace(spRunProfileHitLoop);
#endif

#if FE_HGO_POINT_VERBOSE
		if(FE_HGO_POINT_CONDITION)
		{
			feLog("%d prev %.6G low %d %.6G high %d %.6G\n",pointIndex,
					m_pointAngle[pointIndex],lowSet,lowAngle,highSet,highAngle);
		}
#endif

#if FALSE
		//* HACK springiness
//		if(lowSet || highSet)
//		{
			const Real rampAngle=0.01+0.09*m_passage;	//* TODO tweak

			const Real lowRamp=
					fe::minimum(Real(1),Real(fabs(lowAngle/rampAngle)));
			const Real highRamp=
					fe::minimum(Real(1),Real(fabs(highAngle/rampAngle)));

			lowAngle*=lowRamp;
			highAngle*=highRamp;
//		}
#endif

#if FALSE
		if(lowSet && highSet)
		{
			m_pointAngle[pointIndex]=allowUp*highAngle+allowDown*lowAngle;
		}
		else if(highSet)
		{
			m_pointAngle[pointIndex]=allowUp*highAngle;
		}
		else if(lowSet)
		{
			m_pointAngle[pointIndex]=allowDown*lowAngle;
		}
#else
//		m_pointAngle[pointIndex]=allowUp*highAngle+allowDown*lowAngle;

//		m_pointAngle[pointIndex]=highAngle+lowAngle;

		const Real sumAngle=highAngle-lowAngle;
		const Real lowPortion=sumAngle>0.0? -lowAngle/sumAngle: 0.0;
		m_pointAngle[pointIndex]=highAngle+lowAngle*lowPortion;
#endif

//		if(fabs(lowAngle)>0.1 || fabs(highAngle)>0.1)
//		{
//			feLog("%d angle %.6G %.6G -> %.6G\n",
//					pointIndex,lowAngle,highAngle,m_pointAngle[pointIndex]);
//		}

		if(m_spDrawGuide.isValid() && pointIndex==m_selectIndex)
		{
			const Color magenta(1.0,0.0,1.0);
			const Color cyan(0.0,1.0,1.0);

			//* Start To Finish - blue pink
			Color colors[2];
			colors[0]=cyan;
			colors[1]=magenta;

			SpatialVector line[2];
			line[0]=point;
			line[1]=point+rayDir*radius*m_pointAngle[pointIndex];

//			feLog("ang %.6G %.6G %.6G\n",
//					lowAngle,highAngle,m_pointAngle[pointIndex]);

			m_spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,TRUE,colors);
		}
#if FE_HGO_PROFILE_RUN
		spRunProfilePointLoop->replace(spRunProfileResolve);
#endif
	}

#if FE_HGO_PROFILE_RUN
	spRunProfilePointLoop->finish();
	spRunProfiler->end();

	feLog("%s:\n%s\n",spRunProfiler->name().c_str(),
			spRunProfiler->report().c_str());
#endif
}

//* static
void HingeOp::calcConversion(SpatialTransform& a_rTransform,
	const Real a_angle,const SpatialVector& a_rAxis,
	const SpatialVector& a_rPivot)
{
	setIdentity(a_rTransform);

	translate(a_rTransform,a_rPivot);

	const SpatialQuaternion rotation(a_angle,a_rAxis);
	a_rTransform*=SpatialTransform(rotation);

	translate(a_rTransform,-a_rPivot);
}
