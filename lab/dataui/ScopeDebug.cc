/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ScopeDebug.h"
#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

ScopeDebug::ScopeDebug(void)
{
}

ScopeDebug::~ScopeDebug(void)
{
}

void ScopeDebug::initialize(void)
{
	cfg< sp<Component> >("scope");
}


void ScopeDebug::handleRegion(sp<Region> a_region)
{
	sp<LayoutRegion> spLR;
	spLR = a_region;
	if(spLR.isValid())
	{
		m_layout = spLR->m_layout;
		m_layoutMap[m_spScope] = m_layout;
	}

	sp<ScopeRegion> spSR;
	spSR = a_region;
	if(spSR.isValid())
	{
		m_spScope = spSR->m_scope;
		std::map< sp<Scope>, sp<Layout> >::iterator i_lm =
			m_layoutMap.find(m_spScope);
		if(i_lm != m_layoutMap.end())
		{
			m_layout = i_lm->second;
		}
		else
		{
			m_layout = sp<Layout>(NULL);
		}
	}
}

void ScopeDebug::handleHome(void)
{
	m_layout = NULL;
}

void ScopeDebug::handleDraw(Record &r_sig)
{
	if(!m_spScope.isValid())
	{
		m_spScope = cfg< sp<Component> >("scope");
		if(!m_spScope.isValid())
		{
			return;
		}
	}

	DrawView dv;
	if(!dv.handle(r_sig)) { return; }

	Color red = dv.getColor(	"theme0",		Color(1.0f,0.0f,0.0f,1.0f));
	Color white = dv.getColor(	"foreground",	Color(1.0f,1.0f,1.0f,1.0f));
	Color cyan = dv.getColor(	"theme1",		Color(0.0f,1.0f,1.0f,1.0f));

	m_regions.clear();

	sp<RecordGroup> rg_scopes =
		registry()->master()->catalog()->catalog< sp<RecordGroup> >("Scopes");
	for(RecordGroup::iterator i_rg = rg_scopes->begin();
		i_rg != rg_scopes->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		AsComponent asComponent;
		asComponent.bind(spRA->layout()->scope());
		if(asComponent.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				sp<Scope> scope = feCast(Scope, asComponent.component(spRA,i));
				sp<ScopeRegion> spRegion;
				spRegion = new ScopeRegion();
				m_regions.push_back(spRegion);
				spRegion->m_scope = scope;
				drawText(scope->name(), white);

				spRegion->m_lo[0] = (int)m_cursor[0];
				spRegion->m_hi[0] = (int)(m_cursor[0] + textWidth(scope->name()));
				spRegion->m_lo[1] = (int)m_cursor[1];
				spRegion->m_hi[1] = (int)(m_cursor[1]+(m_fontHeight-1));
				m_cursor[0] += textWidth(scope->name()) + textWidth("MM");
			}
		}
	}

	m_cursor[0] = 0;
	m_cursor[1] -= m_fontHeight;

	for(int i = 0; i < int(m_spScope->getAttributeCount()); i++)
	{
		sp<Attribute> spAttr = m_spScope->attribute(i);
		drawText(spAttr->name(), red);

		std::list<String> names;
		String namelist;
		m_spScope->typeMaster()->reverseLookup((spAttr)->type(),names);
		for(std::list<String>::iterator i_n = names.begin();
			i_n != names.end(); i_n++)
		{
			namelist.cat(" ");
			namelist.cat(*i_n);
		}

		m_cursor[0] = m_fontHeight*15.0;
		drawText(namelist, cyan);

		m_cursor[0] = 0.0;
		m_cursor[1] -= m_fontHeight;
	}

	m_cursor[0] = m_fontHeight*40.0;
	m_cursor[1] = m_height-2*m_fontHeight;

	// TODO: move this drawing into a callback in layout itself
#if 0
	std::map<Layout::Offset *, int>	addrmap;
	int cnt = 0;
	for(int i = 0; i < int(m_spScope->layouts().size()); i++)
	{
		sp<Layout> spLayout = m_spScope->layouts()[i];
		Layout::Offset *pO = const_cast<Layout::Offset *>(spLayout->rawOffsetTable());
		std::map<Layout::Offset *, int>::iterator i_addr =
			addrmap.find(pO);
		if(i_addr == addrmap.end())
		{
			addrmap[pO] = cnt++;
		}
	}
#endif

	for(int i = 0; i < int(m_spScope->layouts().size()); i++)
	{
		sp<Layout> spLayout = m_spScope->layouts()[i];
		String layoutText;
#if 0
		Layout::Offset *pO = const_cast<Layout::Offset *>(spLayout->rawOffsetTable());
		layoutText.sPrintf("0x%08x %2d %s", pO,
			addrmap[pO],
			spLayout->name().c_str());
#endif
		layoutText.sPrintf("XXXXXX XX %s", spLayout->name().c_str());
		if(m_layout.isValid() && spLayout == m_layout)
		{
			drawText(layoutText, white);
		}
		else
		{
			drawText(layoutText, red);
		}
		sp<LayoutRegion> spRegion;
		spRegion = new LayoutRegion();
		m_regions.push_back(spRegion);
		spRegion->m_layout = spLayout;

		spRegion->m_lo[0] = m_cursor[0];
		spRegion->m_hi[0] = m_cursor[0] + textWidth(layoutText);
		spRegion->m_lo[1] = m_cursor[1];
		spRegion->m_hi[1] = m_cursor[1]+(m_fontHeight-1);
		m_cursor[1] -= m_fontHeight;
	}

	m_cursor[1] -= m_fontHeight;

	if(m_layout.isValid())
	{
		drawText(m_layout->name(), red);
		//if(m_layout->locked())
		{
			for(FE_UWORD i = 0; i < m_layout->scope()->getAttributeCount(); i++)
			{
				if(m_layout->checkAttribute(i))
				{
					m_cursor[1] -= m_fontHeight;
					m_cursor[0] = m_fontHeight*40.0;
					String buf;
					//buf.sPrintf("%4d", m_layout->offsetTable()[i]);
					drawText(buf, cyan);
					m_cursor[0] = m_fontHeight*45.0;
					drawText(m_spScope->attribute(i)->name(), red);
				}
			}
		}
#if 0
		else
		{
			m_cursor[1] -= m_fontHeight;
			m_cursor[0] = m_fontHeight*42.0;
			drawText("not yet locked", red);
		}
#endif
	}
}

} /* namespace ext */
} /* namespace fe */


