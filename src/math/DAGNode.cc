/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

#define FE_DAG_DEBUG	FALSE

#define FE_DAG_VALIDATION	(FE_CODEGEN<FE_DEBUG)

#if FE_DAG_VALIDATION
#define FE_DAG_VALIDATE()	validate()
#else
#define FE_DAG_VALIDATE()
#endif

namespace fe
{

DAGNode::DAGNode(void)
{
#if FE_COUNTED_TRACK
	setName("DAGNode");
	registerRegion(this,sizeof(*this));
#endif
}

DAGNode::~DAGNode(void)
{
#if FE_COUNTED_TRACK
	deregisterRegion(this);
#endif
}

void DAGNode::validate(void) const
{
	//* verify that each parent references this node exactly once

	Array< hp<Connection> >::const_iterator parentIt(
			m_parentConnections.begin());
	while(parentIt!=m_parentConnections.end())
	{
		hp<Connection> hpConnection=*parentIt++;
		if(hpConnection.isNull())
		{
			continue;
		}
		FEASSERT(hpConnection->count()>0);

		Array< sp<Connection> >& rParentChildConnections=
				hpConnection->parent()->m_childConnections;
		Array< sp<Connection> >::const_iterator childIt(
				rParentChildConnections.begin());
		U32 count=0;
		while(childIt!=rParentChildConnections.end())
		{
			sp<Connection> spConnection=*childIt++;
			if(spConnection.isNull())
			{
				continue;
			}
			FEASSERT(spConnection->count()>0);

			if(spConnection->child().raw()==this)
			{
				count++;
			}
		}
		if(count!=1)
		{
			feX("DAGNode::validate",
					"parent connection has %d back references",count);
		}
	}

	Array< sp<Connection> >::const_iterator childIt(
			m_childConnections.begin());
	while(childIt!=m_childConnections.end())
	{
		sp<Connection> spConnection=*childIt++;
		if(spConnection.isNull())
		{
			continue;
		}
		FEASSERT(spConnection->count()>0);

		Array< hp<Connection> >& rChildParentConnections=
				spConnection->child()->m_parentConnections;
		Array< hp<Connection> >::const_iterator parentIt(
				rChildParentConnections.begin());
		U32 count=0;
		while(parentIt!=rChildParentConnections.end())
		{
			hp<Connection> hpConnection=*parentIt++;
			if(hpConnection.isNull())
			{
				continue;
			}
			FEASSERT(hpConnection->count()>0);

			if(hpConnection->parent().raw()==this)
			{
				count++;
			}
		}
		if(count!=1)
		{
			feX("DAGNode::validate",
					"child connection has %d back references",count);
		}
	}
}

Array<String>& DAGNode::parentConnectors(void)
{
	FE_DAG_VALIDATE();
	return m_parentConnectors;
}

Array<String>& DAGNode::childConnectors(void)
{
	FE_DAG_VALIDATE();
	return m_childConnectors;
}

U32 DAGNode::parentConnectorCount(void) const
{
	FE_DAG_VALIDATE();
	return m_parentConnectors.size();
}

U32 DAGNode::childConnectorCount(void) const
{
	FE_DAG_VALIDATE();
	return m_childConnectors.size();
}

String DAGNode::parentConnector(U32 a_index)
{
	FE_DAG_VALIDATE();
	return m_parentConnectors[a_index];
}

String DAGNode::childConnector(U32 a_index)
{
	FE_DAG_VALIDATE();
	return m_childConnectors[a_index];
}

const Array< hp<DAGNode::Connection> >& DAGNode::parentConnections(void) const
{
	FE_DAG_VALIDATE();
	return m_parentConnections;
}

const Array< sp<DAGNode::Connection> >& DAGNode::childConnections(void) const
{
	FE_DAG_VALIDATE();
	return m_childConnections;
}

Array< hp<DAGNode::Connection> >& DAGNode::parentConnections(void)
{
	FE_DAG_VALIDATE();
	return m_parentConnections;
}

Array< sp<DAGNode::Connection> >& DAGNode::childConnections(void)
{
	FE_DAG_VALIDATE();
	return m_childConnections;
}

U32 DAGNode::parentConnectionCount(void) const
{
	FE_DAG_VALIDATE();
	return m_parentConnections.size();
}

U32 DAGNode::childConnectionCount(void) const
{
	FE_DAG_VALIDATE();
	return m_childConnections.size();
}

sp<DAGNode::Connection> DAGNode::parentConnection(U32 a_index) const
{
	FE_DAG_VALIDATE();
	return m_parentConnections[a_index];
}

sp<DAGNode::Connection> DAGNode::childConnection(U32 a_index) const
{
	FE_DAG_VALIDATE();
	return m_childConnections[a_index];
}

void DAGNode::addParentConnector(const String a_connector)
{
	if(!hasParentConnector(a_connector))
	{
		m_parentConnectors.push_back(a_connector);
	}
}

void DAGNode::addChildConnector(const String a_connector)
{
	if(!hasChildConnector(a_connector))
	{
		m_childConnectors.push_back(a_connector);
	}
}

BWORD DAGNode::hasParentConnector(String a_connector)
{
	FE_DAG_VALIDATE();
	const I32 count=m_parentConnectors.size();
	for(I32 m=0;m<count;m++)
	{
		if(m_parentConnectors[m]==a_connector)
		{
			return TRUE;
		}
	}
	return FALSE;
}

BWORD DAGNode::hasChildConnector(String a_connector)
{
	FE_DAG_VALIDATE();
	const I32 count=m_childConnectors.size();
	for(I32 m=0;m<count;m++)
	{
		if(m_childConnectors[m]==a_connector)
		{
			return TRUE;
		}
	}
	return FALSE;
}

BWORD DAGNode::attachTo(sp<DAGNode> a_spParentNode,
	String a_localConnector,String a_remoteConnector)
{
	FE_DAG_VALIDATE();

	Array< sp<Connection> > rChildConnections=
			a_spParentNode->m_childConnections;
	Array< sp<Connection> >::const_iterator childIt(
			rChildConnections.begin());
	while(childIt!=rChildConnections.end())
	{
		sp<Connection> spConnection=*childIt++;
		if(spConnection.isNull())
		{
			continue;
		}
		if(spConnection->child()==sp<DAGNode>(this))
		{
			feLog("DAGNode::attachTo node already attached\n");
			return FALSE;
		}
	}

	sp<Connection> spNewConnection(new Connection(sp<DAGNode>(this),
			a_spParentNode,a_localConnector,a_remoteConnector));

	a_spParentNode->m_childConnections.push_back(spNewConnection);
	a_spParentNode->m_childMap[a_remoteConnector]=spNewConnection;

	m_parentConnections.push_back(spNewConnection);
	m_parentMap[a_localConnector]=spNewConnection;

	FE_DAG_VALIDATE();
#if FE_DAG_VALIDATION
	a_spParentNode->validate();
#endif
	return TRUE;
}

BWORD DAGNode::detachFrom(sp<DAGNode> a_spParentNode)
{
#if FE_DAG_DEBUG
	feLog("DAGNode::detachFrom %p of %p\n",a_spParentNode.raw(),this);
#endif

	FE_DAG_VALIDATE();

	Array< sp<Connection> >& rChildConnections=
			a_spParentNode->m_childConnections;
	Array< sp<Connection> >::const_iterator childIt(
			rChildConnections.begin());
	while(childIt!=rChildConnections.end())
	{
		sp<Connection> spConnection=*childIt;
		if(spConnection.isNull())
		{
			childIt++;
			continue;
		}
		if(spConnection->child()==sp<DAGNode>(this))
		{
			const String childConnector=spConnection->childConnector();
			a_spParentNode->m_childMap.erase(childConnector);

			childIt=rChildConnections.erase(childIt);

#if FE_DAG_DEBUG
			feLog("  at child \"%s\"\n",childConnector.c_str());
#endif
			continue;
		}
		childIt++;
	}

	Array< hp<Connection> >::const_iterator parentIt(
			m_parentConnections.begin());
	while(parentIt!=m_parentConnections.end())
	{
		hp<Connection> hpConnection=*parentIt;
		if(hpConnection.isNull())
		{
			parentIt++;
			continue;
		}
		if(hpConnection->parent()==a_spParentNode)
		{
			const String parentConnector=hpConnection->parentConnector();
			m_parentMap.erase(parentConnector);

			parentIt=m_parentConnections.erase(parentIt);

#if FE_DAG_DEBUG
			feLog("  at parent \"%s\"\n",parentConnector.c_str());
#endif
			continue;
		}
		parentIt++;
	}

	FE_DAG_VALIDATE();
#if FE_DAG_VALIDATION
	a_spParentNode->validate();
#endif
	return TRUE;
}

void DAGNode::detach(void)
{
	FE_DAG_VALIDATE();

	for(I32 parentIndex=parentConnectionCount()-1;parentIndex>=0;parentIndex--)
	{
		hp<Connection>& rhpConnection=m_parentConnections[parentIndex];
		if(rhpConnection.isNull())
		{
			m_parentConnections.pop_back();
			continue;
		}

		sp<DAGNode> spParent=rhpConnection->parent();
		detachFrom(spParent);

#if FE_DAG_VALIDATION
		spParent->validate();
#endif
	}

	m_parentConnections.clear();
	m_parentMap.clear();

	FE_DAG_VALIDATE();
}

BWORD DAGNode::detach(String a_localConnector)
{
	std::map< String, hp<Connection> >::const_iterator parentMapIt=
			m_parentMap.find(a_localConnector);
	if(parentMapIt==m_parentMap.end())
	{
		return FALSE;
	}
	hp<Connection> hpConnection=parentMapIt->second;
	FEASSERT(hpConnection.isValid());

	sp<DAGNode> spParent=hpConnection->parent();
	spParent->m_childMap.erase(hpConnection->parentConnector());

	Array< sp<Connection> >& rChildConnections=spParent->m_childConnections;
	Array< sp<Connection> >::const_iterator childIt(
			rChildConnections.begin());
	while(childIt!=rChildConnections.end())
	{
		sp<Connection> spConnection=*childIt;
		if(spConnection.isValid() && spConnection==hpConnection)
		{
			rChildConnections.erase(childIt);
			break;
		}
		childIt++;
	}

	m_parentMap.erase(a_localConnector);

	Array< hp<Connection> >::const_iterator parentIt(
			m_parentConnections.begin());
	while(parentIt!=m_parentConnections.end())
	{
		hp<Connection> hpConnection2=*parentIt;
		if(hpConnection2.isValid() && hpConnection2==hpConnection)
		{
			m_parentConnections.erase(parentIt);
			break;
		}
		parentIt++;
	}


	FE_DAG_VALIDATE();
#if FE_DAG_VALIDATION
	spParent->validate();
#endif
	return TRUE;
}

sp<DAGNode::Connection> DAGNode::parentConnection(
	String a_parentConnector) const
{
	FE_DAG_VALIDATE();

	std::map< String, hp<Connection> >::const_iterator it=
			m_parentMap.find(a_parentConnector);
	if(it==m_parentMap.end())
	{
		return sp<Connection>(NULL);
	}

	return it->second;
}

sp<DAGNode::Connection> DAGNode::childConnection(String a_childConnector) const
{
	FE_DAG_VALIDATE();

	std::map< String, sp<Connection> >::const_iterator it=
			m_childMap.find(a_childConnector);
	if(it==m_childMap.end())
	{
		return sp<Connection>(NULL);
	}
	return it->second;
}

} // namespace
