/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceAttrLabOp__
#define __operator_SurfaceAttrLabOp__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Manipulate and view an attribute on a Surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAttrLabOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceAttrLabOp>
{
	public:
						SurfaceAttrLabOp(void)								{}
virtual					~SurfaceAttrLabOp(void)								{}

		void			initialize(void);

						//* As HandlerI
virtual void			handle(Record& a_rSignal);

static	Color			colorOf(const String& a_rString);

	private:
		SpatialVector	primitiveCenter(I32 a_primitiveIndex);

		String			interpretElement(String a_attrType,I32 a_index);

		sp<SurfaceAccessorI>	m_spOutputPoint;
		sp<SurfaceAccessorI>	m_spOutputElement;
		sp<SurfaceAccessorI>	m_spOutputVertices;

		sp<DrawMode>			m_spSolid;
		sp<DrawMode>			m_spOverlay;

		BWORD					m_keepInput;
		sp<SurfaceI>			m_spInput;
		WindowEvent				m_event;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceAttrLabOp__ */
