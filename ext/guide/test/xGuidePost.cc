/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <thread>
#include <chrono>

#include "guide/guide.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<SingleMaster> spSingleMaster=SingleMaster::create();
			sp<Master> spMaster=spSingleMaster->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			sp<GuidePostI> spGuidePostI(spRegistry->create("GuidePostI"));

			if(!spGuidePostI->connect("localhost"))
			{
				feLog("failed to connect to beacon\n");
				spGuidePostI=NULL;
			}

			const String space("world");
			sp<StateCatalog> spWorldState;
			sp<StateCatalog> spClientState;

			if(spGuidePostI.isValid())
			{
				spWorldState=spGuidePostI->connectToSpace(space);
				feLog("accessed space %p\n",spWorldState.raw());

				//* start a client space
				String clientSpace;
				clientSpace.sPrintf("client%d",spGuidePostI->getID());
				spClientState=spGuidePostI->createSpace(
						clientSpace,sp<Catalog>(NULL));
			}

			if(spWorldState.isValid())
			{
				feLog("World State:\n");
				spWorldState->catalogDump();
			}

			UNIT_TEST(spClientState.isValid());

			if(spClientState.isValid())
			{
				spClientState->setState("intValue",123);

				I32 intValue(0);
				spClientState->getState("intValue",intValue);
				UNIT_TEST(intValue==123);
			}

			//* TODO test more stuff

			std::this_thread::sleep_for(std::chrono::seconds(10));

			if(spClientState.isValid())
			{
				spClientState->stop();
			}

			if(spGuidePostI.isValid())
			{
				feLog("shutdown guide post\n");
				spGuidePostI->shutdown();
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
