/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawShape_h__
#define __intelligence_DrawShape_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw shapes conveniently

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawShape
{
	protected:
		void	draw(sp<DrawI> spDrawI,Record shape,const Color& color);
		void	drawSphere(sp<DrawI> spDrawI,const SpatialVector& location,
						F32 radius,const Color& color);
		void	drawCircleXY(sp<DrawI> spDrawI,const SpatialVector& location,
						F32 radius,const Color& color);

	private:
		Cylinder			m_cylinderRV;

		SpatialTransform	m_transform;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawShape_h__ */
