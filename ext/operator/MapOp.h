/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MapOp_h__
#define __operator_MapOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Load a texture map into an attribute

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT MapOp:
	public OperatorSurfaceCommon,
	public Initialize<MapOp>
{
	public:

					MapOp(void)												{}
virtual				~MapOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	protected:

	class FE_DL_EXPORT Raster
	{
		public:
								Raster(sp<Registry> a_spRegistry):
									m_spRegistry(a_spRegistry)				{}

			void				setTemplate(String a_template)
								{	m_template=a_template; }

			void				setMinMax(Real a_min,Real a_max,Real a_random)
								{	m_min=a_min;
									m_range=a_max-a_min;
									m_random=a_random;
								}

			String				string(const Vector2& a_rUV);
			I32					integer(const Vector2& a_rUV);
			Real				real(const Vector2& a_rUV);
			SpatialVector		spatialVector(const Vector2& a_rUV);

		private:

			Real				low1(const Vector2& a_rUV);
			SpatialVector		low3(const Vector2& a_rUV);

			BWORD				lookup(const Vector2& a_rUV,U8* a_pData);

			sp<Registry>		m_spRegistry;

			Noise				m_noise;

			String				m_template;

			Real				m_min;
			Real				m_range;
			Real				m_random;

			Array< sp<ImageI> >	m_mapArray;
			Array<U32>			m_idArray;
	};

	private:
		sp<Scope>								m_spScope;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MapOp_h__ */
