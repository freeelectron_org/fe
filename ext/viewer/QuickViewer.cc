/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

#define	FE_QV_DRAW_ORIGIN		TRUE
#define	FE_QV_DRAW_INTEREST		FALSE
#define	FE_QV_DRAW_AUXILLARY	FALSE

namespace fe
{
namespace ext
{

QuickViewer::QuickViewer(void):
	m_opened(FALSE)
{
}

QuickViewer::~QuickViewer(void)
{
}

void QuickViewer::initialize(void)
{
	m_spObjectViewer=registry()->create("ViewerI.ObjectViewer");

	if(m_spObjectViewer.isNull())
	{
		feX("QuickViewer::initialize","could not create ViewerI");
	}

	if(m_spObjectViewer->getCameraControllerI().isNull())
	{
		feX("QuickViewer::initialize","need a valid CameraControllerI");
	}

	Result result=registry()->manage("fexNativeWindowDL");
	if(failure(result))
	{
		feX("QuickViewer::initialize","need fexNativeWindowDL");
	}

	result=registry()->manage("fexOpenGLDL");
	if(failure(result))
	{
		feX("QuickViewer::initialize","need fexOpenGLDL");
	}

	m_spWindowI=registry()->create("WindowI");

	if(!m_spWindowI.isValid() || !m_spObjectViewer.isValid())
	{
		feX("QuickViewer::initialize","couldn't create components");
	}

	String bgcolorstr;
	Color bgcolor(0.0,0.0,0.0,1.0);
	if(System::getEnvironmentVariable("FE_VIEWER_BG_COLOR",bgcolorstr))
	{
		I32 i=0;
		while(!bgcolorstr.empty())
		{
			bgcolor[i++]=atof(bgcolorstr.parse("\"", ",").c_str());
		}
	}
	else
	{
		bgcolor={0.2f,0.2f,0.2f};
	}
	m_spWindowI->setBackground(bgcolor);
}

void QuickViewer::reopen(void)
{
	if(!m_opened)
	{
		return;
	}

	const Box2i geometry=m_spWindowI->geometry();

	m_spWindowI=registry()->create("WindowI");
	m_spWindowI->setPosition(geometry[0],geometry[1]);
	m_spWindowI->setSize(width(geometry),height(geometry));
	m_spWindowI->open("QuickViewer");

	m_spObjectViewer->bind(m_spWindowI);
}

//* TODO close() and allow open again (mostly just unmap)

void QuickViewer::open(void)
{
	if(m_opened)
	{
		return;
	}

	//* NOTE EventContext doesn't care what signal it is
	insertBeatHandler(m_spWindowI->getEventContextI());

	//* if there is joystick support loaded, use it
	//	m_spJoyHandler=registry()->create("JoyI",TRUE);
	if (m_spJoyHandler.isValid())
	{
		insertEventHandler(m_spJoyHandler);
	}

	m_spWindowI->open("QuickViewer");
	m_opened=TRUE;

	const BWORD verbose=(System::getVerbose()=="all");

	sp<DrawI> spDrawI;
	const String draw_hint=
			registry()->master()->catalog()->catalogOrDefault<String>(
			"hint:DrawI", "");
	if(draw_hint != "")
	{
		if(verbose)
		{
			feLog("QuickViewer::open using \"%s\"\n", draw_hint.c_str());
		}
		spDrawI=registry()->create(draw_hint);
	}
	if(!spDrawI.isValid())
	{
		if(verbose)
		{
			feLog("QuickViewer::open using threaded draw\n");
		}
//		spDrawI=registry()->create("*.DrawThreaded");
		spDrawI=registry()->create("*.DrawCached");
	}
	if(!spDrawI.isValid())
	{
		if(verbose)
		{
			feLog("QuickViewer::open using any draw\n");
		}
		spDrawI=registry()->create("DrawI");
	}

	if(!spDrawI.isValid())
	{
		feX("QuickViewer::open","couldn't create DrawI");
	}

	bind(spDrawI);

	//* TODO proper create (perhaps a reusable component)
	m_spInterestHandler=new InterestHandler();
	m_spInterestHandler->setName("InterestHandler");
	m_spInterestHandler->bind(spDrawI);
	m_spInterestHandler->bind(m_spObjectViewer->getCameraControllerI());
	insertDrawHandler(m_spInterestHandler);
}

void QuickViewer::bind(sp<DrawI> spDrawI)
{
	m_spObjectViewer->bind(spDrawI);
	m_spObjectViewer->bind(m_spWindowI);
}

void QuickViewer::insertBeatHandler(sp<HandlerI> spHandlerI)
{
	m_spObjectViewer->insertBeatHandler(spHandlerI);
}

void QuickViewer::insertEventHandler(sp<HandlerI> spHandlerI)
{
	m_spObjectViewer->insertEventHandler(spHandlerI);
}

void QuickViewer::insertDrawHandler(sp<HandlerI> spHandlerI)
{
	m_spObjectViewer->insertDrawHandler(spHandlerI);
}

void QuickViewer::run(U32 frames)
{
	U32 count=0;
	while((!frames || count!=frames) &&
			!m_spObjectViewer->getCameraControllerI()->closeEvent())
	{
		const Real time(count);
		const Real timestep(1);
		m_spObjectViewer->render(time,timestep);

		if(!m_spObjectViewer->getDrawI()->isDirect())
		{
			//* don't flood a remote connection
			milliSleep(100);
		}

		count++;
	}
}

void QuickViewer::InterestHandler::handleBind(sp<SignalerI> spSignalerI,
	sp<Layout> spLayout)
{
	sp<Scope> spScope=spLayout->scope();
	FEASSERT(spScope.isValid());

	m_asViewer.bind(spScope);
}

void QuickViewer::InterestHandler::handle(Record& render)
{
	if(!m_spDrawI->drawMode()->uvSpace() &&
			m_asViewer.viewer_spatial.check(render) &&
			m_asViewer.viewer_spatial(render))
	{
		const Real pixelSize=m_spDrawI->view()->pixelSize(
				m_spCameraControllerI->interestMatrix().translation(),1.0f);
		FE_MAYBE_UNUSED(pixelSize);

#if FE_QV_DRAW_AUXILLARY
		const Real pixelDesiredAux=48.0f;
		const Real radiusAux=pixelDesiredAux/pixelSize;
		const Color lime(0.5f,1.0f,0.5f);
		m_spDrawI->drawTransformedMarker(
				m_spCameraControllerI->auxillaryMatrix(),
				radiusAux,lime);
#endif

#if FE_QV_DRAW_ORIGIN
		const Real pixelSizeOrigin=m_spDrawI->view()->pixelSize(
				SpatialVector(0,0,0),1.0f);
		const Real pixelDesiredOrigin=48.0f;
		const Real radiusOrigin=pixelDesiredOrigin/pixelSizeOrigin;
		SpatialTransform identity;
		setIdentity(identity);

		const Color black(0.0f,0.0f,0.0f);
		m_spDrawI->drawTransformedMarker(identity,
				radiusOrigin,black);
#endif

#if FE_QV_DRAW_INTEREST
		const Real pixelDesired=32.0f;
		const Real radius=pixelDesired/pixelSize;
		const Color lavender(1.0f,0.5f,1.0f);
		m_spDrawI->drawTransformedMarker(
				m_spCameraControllerI->interestMatrix(),
				radius,lavender);
#endif
	}
}

} /* namespace ext */
} /* namespace fe */
