/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_h__
#define __oplab_h__

#include "operate/operate.h"

#endif /* __oplab_h__ */
