/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

#include "platform/dlCore.cc"
#include "surface/surface.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("feDataDL"));
	list.append(new String("fexSolveDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	assertInterface<TireI>(spMaster, "TireI");
	assertInterface<SurfaceI>(spMaster, "SurfaceI");

	Library *pLibrary = Memory::instantiate<Library>();

	pLibrary->add<BrushTireSystem>(				"SystemI.BrushTireSystem.fe");
	pLibrary->add<BrushTire>(					"TireI.Brush.fe");
	pLibrary->add<LinearTire>(					"TireI.Linear.fe");
	pLibrary->add<StickTire>(					"TireI.Stick.fe");
	pLibrary->add<BlendTire>(					"TireI.Blend.fe");
	pLibrary->add<TireISystem>(					"SystemI.TireISystem.fe");
	pLibrary->add<GroundCollisionSystem>(		"SystemI.GroundCollisionSystem.fe");
	pLibrary->add<SurfaceClosestPointSystem>(	"SystemI.SurfaceContact.fe");
	pLibrary->add<RigSystem>(					"SystemI.RigSystem.fe");
	pLibrary->add<SoftSystem>(					"SystemI.SoftSystem.fe");
	pLibrary->add<MountSystem>(					"SystemI.MountSystem.fe");
	pLibrary->add<ParticleMountSystem>(			"SystemI.ParticleMountSystem.fe");
	pLibrary->add<RigParticleMountSystem>(		"SystemI.RigParticleMountSystem.fe");
	pLibrary->add<PointViewerSystem>(			"SystemI.PointViewer.fe");
	pLibrary->add<ClosestGroundContactSystem>(	"SystemI.ClosestGroundContact.fe");
	pLibrary->add<TireContactSystem>(			"SystemI.TireContact.fe");
	pLibrary->add<LocatorFromPointsSystem>(		"SystemI.LocatorFromPoints.fe");
	pLibrary->add<LocatorFromEulerSystem>(		"SystemI.LocatorFromEuler.fe");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

