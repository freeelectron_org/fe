/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer__OrthoViewer_h__
#define __viewer__OrthoViewer_h__

namespace fe
{
namespace ext
{

/**	Orthographic viewer

	@copydoc OrthoViewer_info
	*/
class FE_DL_EXPORT OrthoViewer : public Initialize<OrthoViewer>,
	public Config,
	virtual public SignalerViewerI,
	virtual public HandlerI
{
	public:
				OrthoViewer(void);
virtual			~OrthoViewer(void);
		void	initialize(void);

				// AS ViewerI

				using ViewerI::bind;

virtual	void	bind(sp<WindowI> spWindowI);
virtual void	render(Real a_time,Real a_timestep)							{}
virtual Real	frameRate(void) const	{ return Real(0); }

				// AS HandlerI

virtual	void	handle(Record& record);
virtual	void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> spLayout);

	private:
		hp<WindowI>				m_spWindowI;
		AsOrtho					m_asOrtho;
		AsSignal				m_asSignal;
		AsWindata				m_asWindata;
		sp<ViewI>				m_view;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer__OrthoViewer_h__ */

