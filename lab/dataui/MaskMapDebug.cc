/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "MaskMapDebug.h"
#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

MaskMapDebug::MaskMapDebug(void)
{
}

MaskMapDebug::~MaskMapDebug(void)
{
}

void MaskMapDebug::initialize(void)
{
}

void MaskMapDebug::handleRegion(sp<Region> a_region)
{
}

void MaskMapDebug::handleHome(void)
{
}

void MaskMapDebug::handleDraw(Record &r_sig)
{
	Color magenta(1.0f,0.0f,1.0f,1.0f);
	Color white(1.0f,1.0f,1.0f,1.0f);

	sp<MaskMapI> spMaskMap =
		registry()->master()->catalog()->catalogComponent("MaskMapI", "ViewerMaskMap");

	if(!spMaskMap.isValid())
	{
		return;
	}

	std::vector< std::pair<String, WindowEvent::Mask> > entries;

	spMaskMap->all(entries);

	for(unsigned int i = 0; i < entries.size(); i++)
	{
		String line;
		String description;
		description = "";
		WindowEvent::Mask &mask = entries[i].second;
		if(mask.source() & WindowEvent::e_sourceKeyboard)
		{
			if(mask.item() >= 32 && mask.item() <= 126)
			{
				description.sPrintf("key: %c", mask.item());
			}
			else
			{
				description.sPrintf("key: \\%d", mask.item());
			}
		}
		if(mask.source() & WindowEvent::e_sourceMouseButton)
		{
			description.cat("mb : ");
			if(mask.item() & WindowEvent::e_itemLeft)
			{
				description.cat("L");
			}
			if(mask.item() & WindowEvent::e_itemMiddle)
			{
				description.cat("M");
			}
			if(mask.item() & WindowEvent::e_itemRight)
			{
				description.cat("R");
			}
			if(mask.item() & WindowEvent::e_itemWheel)
			{
				description.cat("W");
			}
		}
		if(mask.source() & WindowEvent::e_sourceMousePosition)
		{
			description.cat("pos: ");
			if(mask.item() & WindowEvent::e_itemMove)
			{
				description.cat("move");
			}
			if(mask.item() & WindowEvent::e_itemDrag)
			{
				description.cat("drag");
			}
		}
		if(mask.source() & WindowEvent::e_sourceSystem)
		{
			description.cat("system");
		}
		if(mask.source() & WindowEvent::e_sourceUser)
		{
			description.cat("user");
		}
		if(mask.source() & WindowEvent::e_sourceJoy0)
		{
			description.cat("joy");
		}
		line.sPrintf("%-40s %-20s", entries[i].first.c_str(),
			description.c_str());
		drawText(line, magenta);
		m_cursor[1] -= m_fontHeight;
	}
}

} /* namespace ext */
} /* namespace fe */

