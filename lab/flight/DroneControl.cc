/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "flight/flight.pmh"

namespace fe
{
namespace ext
{

DroneControl::DroneControl(void)
{
};

DroneControl::~DroneControl(void)
{
};

void DroneControl::initialize(void)
{
}

void DroneControl::handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout)
{
}

void DroneControl::handle(Record& a_signal)
{
//	feLog("DroneControl::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	if(!m_asCatalog.bind(a_signal))
	{
		return;
	}

/*
	sp<Catalog>& rspStepCatalog=m_asCatalog.catalog(a_signal);

	const Real inputThrottle=rspStepCatalog->catalog<Real>("input.throttle");
	const Real inputYaw=rspStepCatalog->catalog<Real>("input.yaw");
	const Real inputPitch=rspStepCatalog->catalog<Real>("input.pitch");
	const Real inputBank=rspStepCatalog->catalog<Real>("input.bank");
*/
}

} /* namespace ext */
} /* namespace fe */
