/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceAttrConformOp_h__
#define __operator_SurfaceAttrConformOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Pick a single attribute value for each fragment

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAttrConformOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceAttrConformOp>
{
	public:

					SurfaceAttrConformOp(void)									{}
virtual				~SurfaceAttrConformOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

		Real		profile(Real a_fraction);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceAttrConformOp_h__ */

