import sys
forge = sys.modules["forge"]
import os.path

def setup(module):
    module.summary = []

    deplibs =   forge.corelibs+ [
                "fexSignalLib",
                "fexGeometryDLLib",
                "fexDataToolDLLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib" ]

    tests = [   'xMatrixPower',
                'xMatrixSqrt',
                'xSpline' ]

    forge.tests += [
        ("xMatrixPower.exe",    "",                         None,       None),
        ("xMatrixSqrt.exe",     "",                         None,       None),
        ("xSpline.exe",         "",                         None,       None) ]

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    if 'viewer' in forge.modules_confirmed:
        deplibs += [ "fexViewerDLLib" ]

        tests = [   'xTriangle',
                    'xTurtle' ]

        forge.tests += [
            ("xTriangle.exe",       "10",                   None,       None),
            ("xTurtle.exe",         "100",                  None,       None) ]

        for t in tests:
            module.Exe(t)
            forge.deps([t + "Exe"], deplibs)

    else:
        module.summary += [ "-viewer" ]
