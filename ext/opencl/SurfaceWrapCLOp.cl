/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_bindFace0,
	__global float4* a_position2,
	__global float4* a_normal2,
	__global int* a_vertStart2,__global int* a_vertCount2,
	__global float4* a_fromPosition2,
	__global float4* a_fromNormal2,
	__global float* a_bindT0,
	__global float4* a_debug)
{
	uint pointIndex=get_global_id(0);

	int primitiveIndex2=a_bindFace0[pointIndex];
	if(primitiveIndex2<0)
	{
		return;
	}

	int subStart2=a_vertStart2[primitiveIndex2];
	int subCount2=a_vertCount2[primitiveIndex2];

	float4 input=a_position[pointIndex];

	float floatIndex2=a_bindT0[pointIndex]*convert_float(subCount2-1)*0.999;

	int subIndex2A=convert_int(floatIndex2);
	float between=floatIndex2-convert_float(subIndex2A);
	float between1=1.0f-between;

	int subIndex2B=subIndex2A+1;
	if(subIndex2A>subCount2-1)
	{
		subIndex2A=subCount2-2;
		subIndex2B=subCount2-1;
		between=1.0f;
	}

	int pointIndex2A=subStart2+subIndex2A;
	int pointIndex2B=subStart2+subIndex2B;

	float4 fromPointA=a_fromPosition2[pointIndex2A];
	float4 toPointA=a_position2[pointIndex2A];

	float4 fromNormalA=a_fromNormal2[pointIndex2A];
	float4 toNormalA=a_normal2[pointIndex2A];

	float4 fromPointB=a_fromPosition2[pointIndex2B];
	float4 toPointB=a_position2[pointIndex2B];

	float4 fromNormalB=a_fromNormal2[pointIndex2B];
	float4 toNormalB=a_normal2[pointIndex2B];

	float4 fromPoint=between*fromPointB+between1*fromPointA;
	float4 toPoint=between*toPointB+between1*toPointA;

	float4 fromTangent=normalize(fromPointB-fromPointA);
	float4 toTangent=normalize(toPointB-toPointA);

	float4 fromNormal=normalize(between*fromNormalB+between1*fromNormalA);
	float4 toNormal=normalize(between*toNormalB+between1*toNormalA);

	float16 fromXform=makeFrameTangentX(fromPoint,fromTangent,fromNormal);
	float16 toXform=makeFrameTangentX(toPoint,toTangent,toNormal);

	float16 fromInv=invertMatrix(fromXform);

	float4 relative=transformVector(fromInv,input);

	float4 output=transformVector(toXform,relative);

	a_position[pointIndex]=output;
}
