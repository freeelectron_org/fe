/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_PointCurveNearest_h__
#define __geometry_PointCurveNearest_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find point nearest to a curve with radius

	@ingroup geometry
*//***************************************************************************/
template <typename T>
class PointCurveNearest
{
	public:
static	T	solve(const Vector<3,T>* a_pVertex,const U32 a_vertCount,
					const T a_radius,
					const Vector<3,T>& a_origin,Vector<3,T>& a_direction,
					T& a_along,Vector<3,T>& a_intersection);
};

template <typename T>
inline T PointCurveNearest<T>::solve(
	const Vector<3,T>* a_pVertex,const U32 a_vertCount,
	const T a_radius,
	const Vector<3,T>& a_origin, Vector<3,T>& a_direction,
	T& a_along,Vector<3,T>& a_intersection)
{
#if FALSE
	feLog("PointCurveNearest< %s >::solve %s radius %.6G\n",
			FE_TYPESTRING(T).c_str(),
			c_print(a_origin),a_radius);
	for(U32 m=0;m<a_vertCount;m++)
	{
		feLog("  %d/%d %s\n",m,a_vertCount,c_print(a_pVertex[m]));
	}
#endif

	if(a_vertCount<2)
	{
		return T(-1);
	}

	T bestAlong=0.0;
	T bestMag= -1.0;
	I32 bestIndex=0;
	for(U32 m=0;m<a_vertCount-1;m++)
	{
		const Vector<3,T> segment=a_pVertex[m+1]-a_pVertex[m];
		Vector<3,T> oneIntersection;
		T oneAlong;
		T oneMag=PointCylinderNearest<T>::solve(a_pVertex[m],segment,
				a_radius,a_origin,a_direction,oneAlong,oneIntersection);
		if(oneMag>=0.0 && (bestMag<0.0 || oneMag<bestMag))
		{
			const T length=magnitude(segment);
			bestAlong=oneAlong*(length>0.0? 1.0/length: 1.0);
			a_intersection=oneIntersection;
			bestMag=oneMag;
			bestIndex=m;
		}

#if FALSE
		feLog("  %d/%d mag %.6G along %.6G at %s\n",m,a_vertCount,
				oneMag,oneAlong,c_print(oneIntersection));
#endif

	}

	FEASSERT(bestAlong>=0.0);
	FEASSERT(bestAlong<=1.0);

	a_along=(bestMag<0.0)? 0.0: (bestIndex+bestAlong)/(a_vertCount-1.0);

#if FALSE
		feLog("  bestMag %.6G along %.6G \n",bestMag,a_along);
#endif

	return bestMag;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_PointCurveNearest_h__ */
