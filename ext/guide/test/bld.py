import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
    deplibs = forge.corelibs[:]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexSignalLib" ]

    tests = [   'xGuidePost' ]

    for t in tests:
        exe = module.Exe(t)

        forge.deps([t + "Exe"], deplibs)
