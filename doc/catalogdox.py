#!/usr/bin/python

import sys
import string
import os
import re

re_srcfile = re.compile(r'.*\.(h|cc)$')
re_attruse = re.compile(r'.*FE_CAT\(\s*"([^"]*)"\s*\)')
re_attrdoc = re.compile(r'.*FE_(CAT_SPEC)\(\s*"([^"]*)"\s*,\s*"([^"]*)"\s*\)')
re_attrstart = re.compile(r'.*FE_(CAT_SPEC)\(')
re_attrspec = re.compile(r'.*FE_CAT_SPEC\(')
re_prefix = re.compile(r'([A-Za-z]+):\w+')
re_ok = re.compile(r'(\w)+:\w+')
re_sys = re.compile(r':[A-Z]\w*')

tokens = {}
docs = {}
filenames = {}

output = open(sys.argv[2], 'w')
root = os.getcwd() + '/'
rootlen = len(root)

def process_file(filename):
    input = open(filename, 'r')
    line = input.readline()
    while line:
        match = re_attruse.match(line)
        if match:
            token = match.group(1)
            if tokens.has_key(token):
                tokens[token] += 1
                filenames[token].append(filename)
            else:
                tokens[token] = 1
                filenames[token] = [ filename ]

        match = re_attrdoc.match(line)
        if match:
            token = match.group(2)
            doc = match.group(3)
            if docs.has_key(token):
                docs[token].append(doc)
            else:
                docs[token] = [ doc ]
            if not tokens.has_key(token):
                tokens[token] = 0
                filenames[token] = [ ]

            match = re_attrspec.match(line)
            if match:
                tokens[token] += 1
                filenames[token].append(filename)
        else:
            #HACK  allow for two lines
            match = re_attrstart.match(line)
            if match:
                line += input.readline()
                match = re_attrdoc.match(line)
                if match:
                    token = match.group(2)
                    doc = match.group(3)
                    if docs.has_key(token):
                        docs[token].append(doc)
                    else:
                        docs[token] = [ doc ]
                    if not tokens.has_key(token):
                        tokens[token] = 0
                        filenames[token] = [ ]

                    match = re_attrspec.match(line)
                    if match:
                        tokens[token] += 1
                        filenames[token].append(filename)
        line = input.readline()

    input.close()

def walker(arg, dirname, names):
    for name in names:
        if re_srcfile.match(name):
            process_file(os.path.join(dirname,name))

os.walk(sys.argv[1], walker, None)

#for root, dirs, files in os.walk(sys.argv[1]):
#   for f in files:
#       if re_srcfile.match(f):
#           process_file(os.path.join(root,f))

keylist = []
for key in tokens.keys():
    keylist.append(key)

keylist.sort()

content = """ /**
@page catalog_names Known Catalog Names
"""

content += "<table>"
last_prefix = ""
for key in keylist:
    prefix = ""
    match = re_prefix.match(key)
    if match:
        prefix = match.group(1)
    if last_prefix != prefix:
        content += '<tr><td bgcolor="%s" colspan=4>%s</td></tr>' % ("#DDDDFF", prefix)
    last_prefix = prefix

    #print "%-20s %d" % (key, tokens[key])
    color = "#FFDDDD"
    if(re_ok.match(key)):
        color = "#DDDDDD"
    if(re_sys.match(key)):
        color = "#DDFFDD"
    if(len(key) > 15):
        color = "#FF0000"

    content += "<tr>"
    content += '<td bgcolor="%s">%s</td>' % (color, key)

    if docs.has_key(key) and len(docs[key]) > 0:
        color = "#FFFFFF"
        if len(docs[key]) > 1:
            color = "#FFFFDD"
        content += '<td bgcolor="%s">' % color
        multiple = 0;
        for doc in docs[key]:
            #print '    %s' % doc
            if multiple == 1:
                content += '<HR>'
            content += '%s' % doc
            multiple = 1;
        content += '</td>'
    else:
        content += "<td></td>"

    color = "#FFFFFF"
    if tokens[key] < 1:
        color = "#FFDDDD"
    if tokens[key] > 1:
        color = "#FFFFDD"
    content += '<td bgcolor="%s">%d</td>' % (color, tokens[key])
    content += "</tr>\n"

    if filenames.has_key(key) and len(filenames[key]) > 0:
        content += '<td>'
        multiple = 0;
        for filename in filenames[key]:
            localname = filename
            if filename.startswith(root):
                localname = filename[rootlen:]
            if multiple == 1:
                content += '<BR>'
            content += '%s' % localname
            multiple = 1;
        content += '</td>'
    else:
        content += "<td></td>"

content += "</table>\n"
content += "*/\n"

output.write(content)
output.close()

