/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terminal_Terminal_h__
#define __terminal_Terminal_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief respond to a string with a string

	@ingroup terminal
*//***************************************************************************/
class FE_DL_EXPORT Terminal:
	public Component,
	public Initialize<Terminal>
{
	public:
						Terminal(void);
virtual					~Terminal(void);

		void			initialize(void);

		String			execute(String a_commandline);

	private:

	class ComponentInfo
	{
		public:
									ComponentInfo(void):
										m_serial(0)							{}

			void					reset(void)
									{
										m_spComponent=NULL;
									}

			sp<Component>			m_spComponent;
			String					m_typeName;
			U32						m_serial;
			std::vector<String>		m_inputKey;
			std::vector<U32>		m_inputSerial;
	};

		std::string						(*m_fpCallback)(std::string);
		Real							m_frame;

		sp<Scope>						m_spScope;
		std::map<String,TerminalNode>	m_nodeMap;
		std::map<String,ComponentInfo>	m_surfaceMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terminal_Terminal_h__ */
