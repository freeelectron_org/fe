import sys
import os
forge = sys.modules["forge"]

def prerequisites():
    return [    "datatool",
                "native",
                "opengl",
                "window" ]

def setup(module):

    # the library
    srcList = [ "DebugController",
                "DrawSelection",
                "DrawView",
                "EventMap",
                "InspectController",
                "Intersectors",
                "Mask",
                "MaskMap",
                "ObjectViewer",
                "OrthoViewer",
                "PerspectiveViewer",
                "PickPoint",
                "ProjectPoint",
                "QuickViewer",
                "SelectController",
                "VideoRecorder",
                "ChannelFilter",
                "ChannelController",
                "ChannelBinder",
                "WindowController",
                "viewerLib"
                ]

    lib = module.Lib("fexViewer",srcList)

    # the plugin
    srcList = [ "viewer.pmh",
                "viewerDL" ]

    dll = module.DLL( "fexViewerDL", srcList )

    dll.linkmap = { "gfxlibs": forge.gfxlibs }

    deplibs = forge.corelibs+ [
                "fexSignalLib",
                "fexDataToolDLLib",
                "fexWindowLib",
                "fexWindowDLLib",
                "fexViewerLib" ]

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
        deplibs += [    "fexDrawDLLib" ]

    forge.deps( ["fexViewerDLLib"], deplibs )

    forge.tests += [
        ("inspect.exe",     "fexViewerDL",                  None,       None) ]

    module.Module( 'test' )
