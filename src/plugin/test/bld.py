import sys
import os

forge = sys.modules["forge"]

def setup(module):
    deplibs = forge.basiclibs + [ "fePluginLib" ]

    deplibs2 = deplibs + [ "xCatalogLibraryLib" ]

    tests = [   "xCatalog",
                "xCatalogOverlay",
                "xComponentHandle",
                "xHub",
                "xPluginLoader",
                "xPluginThreads",
                "xSingleMaster",
                "xStateCatalog",
                "xTypeSpeed" ]

    module.DLL( "xCatalogLibrary", ["xCatalogLibrary"] )
    module.DLL( "xPluginLibrary", ["xPluginLibrary"] )
    module.DLL( "xPluginLibrary2", ["xPluginLibrary2"] )

    if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64' or forge.fe_os == 'FE_OSX':
        forge.deps( ["xCatalogLibraryLib"], deplibs )
        forge.deps( ["xPluginLibraryLib"], deplibs )
        forge.deps( ["xPluginLibrary2Lib"], deplibs2 )
    else:
        forge.deps( ["xPluginLibrary2Lib"], [ "xCatalogLibraryLib" ] )

    for t in tests:
        module.Exe(t)
        forge.deps([t + "Exe"], deplibs)

    forge.tests += [
        ("xCatalog.exe",            "",                 None,       None),
        ("xCatalogOverlay.exe",     "",                 None,       None),
        ("xComponentHandle.exe",    "",                 None,       None),
        ("xHub.exe",                "",                 None,       None),
        ("xPluginLoader.exe",       "",                 None,       None),
        ("xPluginLoader.exe",       "retain",           None,       None),
        ("xPluginThreads.exe",      "7 13",             None,       None),
        ("xPluginThreads.exe",      "3 777",            None,       None),
        ("xSingleMaster.exe",       "",                 None,       None),
        ("xStateCatalog.exe",       "",                 None,       None) ]

    if not "FE_CI_BUILD" in os.environ or os.environ["FE_CI_BUILD"] == "0":
        forge.tests += [
            ("xTypeSpeed.exe",      "",                 None,       None) ]
